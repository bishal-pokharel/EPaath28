var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'main-bg',

		extratextblock:[{
			textdata: data.string.p2text1,
			textclass: "main-title",
		},
		{
			textdata: data.string.p2text2,
			textclass: "bottom-ins",
		},{
			textdata : data.string.f_1_2,
			textclass: 'main-frac-text frac-1',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'main-frac-text frac-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'main-frac-text frac-3',
			splitintofractionsflag: true,
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "pop-obj circle-mini",
					imgsrc : '',
					imgid : 'circle'
				},
				{
					imgclass : "pop-obj rect-mini",
					imgsrc : '',
					imgid : 'rect'
				},
				{
					imgclass : "pop-obj triangle-mini",
					imgsrc : '',
					imgid : 'triangle'
				}
			]
		}],

		popupblock:[{
			popupclass: '',
			headerdata: '',
		}]
	}
];

//content if rect is clicked
var rect_content = [
	//minislide 0
	{
		containerclass: 'rect-container',
		text:[{
			textdata : data.string.p2text3,
			textclass: 'ctext-1',
			splitintofractionsflag: true,
		}],
	},
	//minislide 1
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text4,
			textclass: 'ctext-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text5,
			textclass: 'part-text top-part',
		},{
			textdata : data.string.p2text6,
			textclass: 'part-text bottom-part',
		},{
			textdata : data.string.p2text7,
			textclass: 'part-text line-part',
		}],
	},
	//minislide 2
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text8,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'rect-svg',
		}]
	},
	//minislide 3
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text9,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1,
			textclass: 'block-text block-1 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-rect",
					imgsrc : '',
					imgid : 'arrow-rect'
				}
			]
		}],
		svgblock:[{
			svgblock: 'rect-svg',
		}]
	},
	//minislide 4
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text10,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'rect-svg',
		}]
	},
	//minislide 5
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text11,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1,
			textclass: 'block-text block-1 fade-in-1',
		},{
			textdata : data.string.p2,
			textclass: 'block-text block-2 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-rect-2",
					imgsrc : '',
					imgid : 'arrow-rect-2'
				}
			]
		}],
		svgblock:[{
			svgblock: 'rect-svg',
		}]
	},
	//minislide 6
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text12,
			textclass: 'ctext-3',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_rect_text'
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'rect-svg',
		}]
	},
];

//content if circle is clicked
var circle_content = [
	//minislide 0
	{
		containerclass: 'rect-container',
		text:[{
			textdata : data.string.p2text15,
			textclass: 'ctext-2',
			splitintofractionsflag: true,
		}],
	},
	//minislide 1
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text4,
			textclass: 'ctext-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text5,
			textclass: 'part-text top-part',
		},{
			textdata : data.string.p2text6,
			textclass: 'part-text bottom-part',
		},{
			textdata : data.string.p2text7,
			textclass: 'part-text line-part',
		}],
	},
	//minislide 2
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text8,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'circle-svg',
		}]
	},
	//minislide 3
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text9,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1,
			textclass: 'cblock-text cblock-1 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-circle",
					imgsrc : '',
					imgid : 'arrow-rect'
				}
			]
		}],
		svgblock:[{
			svgblock: 'circle-svg',
		}]
	},
	//minislide 4
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text10,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'circle-svg',
		}]
	},
	//minislide 5
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text16,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2,
			textclass: 'cblock-text cblock-1 fade-in-1',
		},{
			textdata : data.string.p1,
			textclass: 'cblock-text cblock-2 fade-in-1',
		},{
			textdata : data.string.p3,
			textclass: 'cblock-text cblock-3 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-circle-2",
					imgsrc : '',
					imgid : 'arrow-circle-2'
				}
			]
		}],
		svgblock:[{
			svgblock: 'circle-svg',
		}]
	},
	//minislide 6
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text17,
			textclass: 'ctext-3',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_rect_text'
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'circle-svg',
		}]
	},
];
//content if triangle is clicked
var tri_content = [
	//minislide 0
	{
		containerclass: 'rect-container',
		text:[{
			textdata : data.string.p2text20,
			textclass: 'ctext-2',
			splitintofractionsflag: true,
		}],
	},
	//minislide 1
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text4,
			textclass: 'ctext-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text5,
			textclass: 'part-text top-part',
		},{
			textdata : data.string.p2text6,
			textclass: 'part-text bottom-part',
		},{
			textdata : data.string.p2text7,
			textclass: 'part-text line-part',
		}],
	},
	//minislide 2
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text8,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'tri-svg',
		}]
	},
	//minislide 3
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text21,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1,
			textclass: 'cblock-text tblock-1 fade-in-1',
		},{
			textdata : data.string.p2,
			textclass: 'cblock-text tblock-2 fade-in-1',
		},{
			textdata : data.string.p3,
			textclass: 'cblock-text tblock-3 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-tri",
					imgsrc : '',
					imgid : 'arrow-tri'
				}
			]
		}],
		svgblock:[{
			svgblock: 'tri-svg',
		}]
	},
	//minislide 4
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text10,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'tri-svg',
		}]
	},
	//minislide 5
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text22,
			textclass: 'ctext-3',
			splitintofractionsflag: true,
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1,
			textclass: 'cblock-text tblock-1 fade-in-1',
		},{
			textdata : data.string.p2,
			textclass: 'cblock-text tblock-2 fade-in-1',
		},{
			textdata : data.string.p3,
			textclass: 'cblock-text tblock-3 fade-in-1',
		},{
			textdata : data.string.p4,
			textclass: 'cblock-text tblock-4 fade-in-1',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "arrow-tri-2",
					imgsrc : '',
					imgid : 'arrow-tri-2'
				}
			]
		}],
		svgblock:[{
			svgblock: 'tri-svg',
		}]
	},
	//minislide 6
	{
		containerclass: '',
		text:[{
			textdata : data.string.p2text23,
			textclass: 'ctext-3',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_rect_text'
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-center-2',
			splitintofractionsflag: true,
		}],
		svgblock:[{
			svgblock: 'tri-svg',
		}]
	},
];







$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var index = 0;
	var preload;
	var timeoutvar = null;
	var current_sound;


	var current_div_x;
	var current_div_y;
	var current_selection = 0;
	var popcount = 0;
	var popupcontent = '';
	var popuptotal = 0;
	var popup_is_close =  true;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "circle", src: imgpath+"new/circle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "rect", src: imgpath+"new/rectangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "triangle", src: imgpath+"new/triangle.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-rect", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-rect-2", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-circle-2", src: imgpath+"arrow02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "arrow-tri", src: imgpath+"arrow03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow-tri-2", src: imgpath+"arrow05.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "circle_1", src: soundAsset+"s2_p1(circle)_1.ogg"},
			{id: "circle_2", src: soundAsset+"s2_p1(circle)_2.ogg"},
			{id: "circle_3", src: soundAsset+"s2_p1(circle)_3.ogg"},
			{id: "circle_4", src: soundAsset+"s2_p1(circle)_4.ogg"},
			{id: "circle_5", src: soundAsset+"s2_p1(circle)_5.ogg"},
			{id: "circle_6", src: soundAsset+"s2_p1(circle)_6.ogg"},
			{id: "circle_7", src: soundAsset+"s2_p1(circle)_7.ogg"},

			{id: "rectangle_1", src: soundAsset+"s2_p1(rectangle)_1.ogg"},
			{id: "rectangle_2", src: soundAsset+"s2_p1(rectangle)_2.ogg"},
			{id: "rectangle_3", src: soundAsset+"s2_p1(rectangle)_3.ogg"},
			{id: "rectangle_4", src: soundAsset+"s2_p1(rectangle)_4.ogg"},
			{id: "rectangle_5", src: soundAsset+"s2_p1(rectangle)_5.ogg"},
			{id: "rectangle_6", src: soundAsset+"s2_p1(rectangle)_6.ogg"},
			{id: "rectangle_7", src: soundAsset+"s2_p1(rectangle)_7.ogg"},

			{id: "triangle_1", src: soundAsset+"s2_p1(triangle)_1.ogg"},
			{id: "triangle_2", src: soundAsset+"s2_p1(triangle)_2.ogg"},
			{id: "triangle_3", src: soundAsset+"s2_p1(triangle)_3.ogg"},
			{id: "triangle_4", src: soundAsset+"s2_p1(triangle)_4.ogg"},
			{id: "triangle_5", src: soundAsset+"s2_p1(triangle)_5.ogg"},
			{id: "triangle_6", src: soundAsset+"s2_p1(triangle)_6.ogg"},
			{id: "triangle_7", src: soundAsset+"s2_p1(triangle)_7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
			sound_player("s2_p1",0);
				$('.rect-mini').on("click", function(event){
					current_selection = 0;
					popupcontent = rect_content;
					popup_window($(this), event);
				});
				$('.circle-mini').on("click", function(event){
					current_selection = 1;
					popupcontent = circle_content;
					popup_window($(this), event);
				});
				$('.triangle-mini').on("click", function(event){
					current_selection = 2;
					popupcontent = tri_content;
					popup_window($(this), event);
				});
				current_sound = createjs.Sound.play('sound_1');
				current_sound.stop();
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_1');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,nextbtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nextbtn?$('.popupnext').show(0):$('.popupnext').hide(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function popupcreater(){
		var popsource = $("#popup-template").html();
		var poptemplate = Handlebars.compile(popsource);
		var pophtml = poptemplate(popupcontent[popcount]);
		$('.popuppage').html(pophtml);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($('.popuppage'));
		splitintofractions($('.popuppage'));

		vocabcontroller.findwords(countNext);
		put_image(popupcontent, popcount);

		$('.popupnext').hide(0);
		$('.popupprev').hide(0);


		$('.popupnext, .popupprev').unbind('click');
		$('.popupnext').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			current_sound.stop();
			popcount++;
			popupcreater();
		});
		$('.popupprev').click(function(){
			$('.popupnext').hide(0);
			$('.popupprev').hide(0);
			current_sound.stop();
			popcount--;
			popupcreater();
		});
		$('.wrong_button').click(function(){
			close_popup_window();
		});

		if(current_selection==0){
			switch(popcount){
				case 0:
					sound_player("rectangle_"+(popcount+1),1);
					// $('.popupnext').show(0);
					$('.popupdiv').addClass('rect-container');
					break;
				case 1:
					sound_player("rectangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center .top').addClass('top-anim');
					$('.frac-center .bottom').addClass('bt-anim');
					$('.top-part').fadeIn(4000, function(){
						$('.bottom-part').fadeIn(2000, function(){
							$('.frac-center *').css('border-color', '#A64D79');
							$('.line-part').fadeIn(2000, function(){
								// $('.popupnext').show(0);
							});
						});
					});
					// $('.popupnext').delay(7000).show(0);
					break;
				case 2:
				case 3:
					sound_player("rectangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .top').css('background-color', '#B6D7A8');
					// if(popcount==3){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#rect-svg');
					var svg = Snap.load(preload.getResult('rect').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var l_fill = Snap.select('#fill-l');
						var r_fill = Snap.select('#fill-r');
						l_fill.attr('fill', '#B6D7A8');
						r_fill.attr('fill', '#FFFFFF');
					} );
					break;
				case 4:
				case 5:
					sound_player("rectangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .bottom').css('background-color', '#A4C2F4');
					// if(popcount==5){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#rect-svg');
					var svg = Snap.load(preload.getResult('rect').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var l_fill = Snap.select('#fill-l');
						var r_fill = Snap.select('#fill-r');
						var cline = Snap.select('#centreline');

						cline.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						l_fill.attr({'fill': '#D9D9D9', 'stroke': '#A4C2F4', 'stroke-width': '8'});
						r_fill.attr({'fill': '#FFFFFF', 'stroke': '#A4C2F4', 'stroke-width': '8'});
					} );
					break;
				case 6:
					sound_player("rectangle_"+(popcount+1),0);
					$('.popupprev').show(0);
					$('.wrong_button').show(0);
					var s = Snap('#rect-svg');
					var svg = Snap.load(preload.getResult('rect').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var l_fill = Snap.select('#fill-l');
						var r_fill = Snap.select('#fill-r');
						var cline = Snap.select('#centreline');
						cline.attr({'stroke': '#606060', 'stroke-width': '8'});
						l_fill.attr({'fill': '#D9D9D9', 'stroke': '#606060', 'stroke-width': '8'});
						r_fill.attr({'fill': '#FFFFFF', 'stroke': '#606060', 'stroke-width': '8'});
					} );
					break;
				default:
					$('.popupprev').show(0);
					break;
			}
		} else if(current_selection==1){//circle case
			switch(popcount){
				case 0:
					sound_player("circle_"+(popcount+1),1);
					// $('.popupnext').show(0);
					$('.popupdiv').addClass('rect-container');
					break;
				case 1:
					sound_player("circle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center .top').addClass('top-anim');
					$('.frac-center .bottom').addClass('bt-anim');
					$('.top-part').fadeIn(2000, function(){
						$('.bottom-part').fadeIn(2000, function(){
							$('.frac-center *').css('border-color', '#A64D79');
							$('.line-part').fadeIn(2000, function(){
								// $('.popupnext').show(0);
							});
						});
					});
					// $('.popupnext').delay(7000).show(0);
					break;
				case 2:
				case 3:
					sound_player("circle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .top').css('background-color', '#B6D7A8');
					// if(popcount==3){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#circle-svg');
					var svg = Snap.load(preload.getResult('circle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_2 = Snap.select('#fill-2');
						fill_2.attr('fill', '#B6D7A8');
					} );
					break;
				case 4:
				case 5:
					sound_player("circle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .bottom').css('background-color', '#A4C2F4');
					// if(popcount==5){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#circle-svg');
					var svg = Snap.load(preload.getResult('circle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_2 = Snap.select('#fill-2');
						var l1 = Snap.select('#line-1');
						var l2 = Snap.select('#line-2');
						var l3 = Snap.select('#line-3');
						var c1 = Snap.select('#cur-1');
						var c2 = Snap.select('#cur-2');
						var c3 = Snap.select('#cur-3');

						l1.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						l2.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						l3.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						c1.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						c2.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						c3.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						fill_2.attr({'fill': '#D9D9D9', 'stroke': '#A4C2F4', 'stroke-width': '8'});
					} );
					break;
				case 6:
					sound_player("circle_"+(popcount+1),0);
					$('.popupprev').show(0);
					$('.wrong_button').show(0);
					var s = Snap('#circle-svg');
					var svg = Snap.load(preload.getResult('circle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_2 = Snap.select('#fill-2');
						var l1 = Snap.select('#line-1');
						var l2 = Snap.select('#line-2');
						var l3 = Snap.select('#line-3');
						var c1 = Snap.select('#cur-1');
						var c2 = Snap.select('#cur-2');
						var c3 = Snap.select('#cur-3');

						l1.attr({'stroke': '#606060', 'stroke-width': '8'});
						l2.attr({'stroke': '#606060', 'stroke-width': '8'});
						l3.attr({'stroke': '#606060', 'stroke-width': '8'});
						c1.attr({'stroke': '#606060', 'stroke-width': '8'});
						c2.attr({'stroke': '#606060', 'stroke-width': '8'});
						c3.attr({'stroke': '#606060', 'stroke-width': '8'});
						fill_2.attr({'fill': '#D9D9D9', 'stroke': '#606060', 'stroke-width': '8'});
					} );
					break;
				default:
					$('.popupprev').show(0);
					break;
			}
		} else if(current_selection==2){//triangle case
			switch(popcount){
				case 0:
					sound_player("triangle_"+(popcount+1),1);
					// $('.popupnext').show(0);
					$('.popupdiv').addClass('rect-container');
					break;
				case 1:
					sound_player("triangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center .top').addClass('top-anim');
					$('.frac-center .bottom').addClass('bt-anim');
					$('.top-part').fadeIn(2000, function(){
						$('.bottom-part').fadeIn(2000, function(){
							$('.frac-center *').css('border-color', '#A64D79');
							$('.line-part').fadeIn(2000, function(){
								// $('.popupnext').show(0);
							});
						});
					});
					// $('.popupnext').delay(7000).show(0);
					break;
				case 2:
				case 3:
					sound_player("triangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .top').css('background-color', '#B6D7A8');
					// if(popcount==3){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#tri-svg');
					var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_1 = Snap.select('#tfill-1');
						var fill_2 = Snap.select('#tfill-2');
						var fill_3 = Snap.select('#tfill-3');
						fill_1.attr('fill', '#B6D7A8');
						fill_2.attr('fill', '#B6D7A8');
						fill_3.attr('fill', '#B6D7A8');
					} );
					break;
				case 4:
				case 5:
					sound_player("triangle_"+(popcount+1),1);
					$('.popupprev').show(0);
					$('.frac-center-2 .bottom').css('background-color', '#A4C2F4');
					// if(popcount==5){
					// 	$('.popupnext').delay(4000).show(0);
					// } else{
					// 	$('.popupnext').show(0);
					// }
					var s = Snap('#tri-svg');
					var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_1 = Snap.select('#tfill-1');
						var fill_2 = Snap.select('#tfill-2');
						var fill_3 = Snap.select('#tfill-3');
						var l1 = Snap.select('#tline-1');
						var l2 = Snap.select('#tline-2');
						var l3 = Snap.select('#tline-3');

						l1.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						l2.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						l3.attr({'stroke': '#A4C2F4', 'stroke-width': '8'});
						fill_1.attr({'fill': '#D9D9D9', 'stroke': '#A4C2F4', 'stroke-width': '8'});
						fill_2.attr({'fill': '#D9D9D9', 'stroke': '#A4C2F4', 'stroke-width': '8'});
						fill_3.attr({'fill': '#D9D9D9', 'stroke': '#A4C2F4', 'stroke-width': '8'});
					} );
					break;
				case 6:
					sound_player("triangle_"+(popcount+1),0);
					$('.popupprev').show(0);
					$('.wrong_button').show(0);
					var s = Snap('#tri-svg');
					var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						var fill_1 = Snap.select('#tfill-1');
						var fill_2 = Snap.select('#tfill-2');
						var fill_3 = Snap.select('#tfill-3');
						var l1 = Snap.select('#tline-1');
						var l2 = Snap.select('#tline-2');
						var l3 = Snap.select('#tline-3');

						l1.attr({'stroke': '#606060', 'stroke-width': '8'});
						l2.attr({'stroke': '#606060', 'stroke-width': '8'});
						l3.attr({'stroke': '#606060', 'stroke-width': '8'});
						fill_1.attr({'fill': '#D9D9D9', 'stroke': '#606060', 'stroke-width': '8'});
						fill_2.attr({'fill': '#D9D9D9', 'stroke': '#606060', 'stroke-width': '8'});
						fill_3.attr({'fill': '#D9D9D9', 'stroke': '#606060', 'stroke-width': '8'});
					} );
					break;
				default:
					$('.popupprev').show(0);
					break;
			}
		}
	}
	function popup_window(click_class, event){
		if(popup_is_close){
			// console.log('before started   '+ popup_is_close);

			$('.click-on-map').fadeOut(1000);
			$( ".pop-obj " ).css('pointer-events', 'none');
			$prevBtn.hide(0);
			$nextBtn.hide(0);

			$('.wrong_button').hide(0);
			popuptotal = popupcontent.length;

			popupcreater();

			popup_is_close = false;
			popcount = 0;
			setTimeout(function(){
				console.log('started   '+ popup_is_close);

				current_div_x = click_class.position().left + click_class.width()/2;
				current_div_y = click_class.position().top + click_class.height()/2;

				$('.popupdiv').css({'display': 'block', 'width': "0%", 'height': '0%', 'left':current_div_x, 'top': current_div_y});
				$('.popuppage').hide(0);
				$('.popupdiv').animate({
					'width': "95%",
					'height': '95%',
					'left':'50%',
					'top': '50%'
				}, 500, function(){
					$('.popuppage').fadeIn(300);
				});
			}, 100);
		}
	}
	function close_popup_window(){
		popcount = 0;
		current_sound.stop();
		$(".drop-box").css('background-color', 'rgb(120,121,196)');
		$('.popuppage, .wrong_button').fadeOut(300);
		$('.popupdiv').fadeOut(500, function(){
			popup_is_close = true;
			console.log('completed   ' + popup_is_close);
			$( ".pop-obj " ).css('pointer-events', 'all');
			$('.click-on-map').fadeIn(1000);
		});
		$prevBtn.show(0);
		if( countNext == $total_page-1){
			ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$nextBtn.show(0);
		}
	}









	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

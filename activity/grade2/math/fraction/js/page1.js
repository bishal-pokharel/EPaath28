var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title chelseamarket",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "coverpg",
					imgsrc : '',
					imgid : 'coverpage'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-brown',
		svgblock: [{
			svgblock: 'svg-circle',
		}],

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "circ-title",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sunder-1",
					imgsrc : '',
					imgid : 'sunder'
				}
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-brown',
		svgblock: [{
			svgblock: 'svg-circle',
		}],

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "circ-title",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sunder-1",
					imgsrc : '',
					imgid : 'sunder'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-brown',
		svgblock: [{
			svgblock: 'svg-circle',
		}],

		extratextblock:[{
			textdata: data.string.p1text4,
			textclass: "circ-title",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sunder-1",
					imgsrc : '',
					imgid : 'sunder'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'svg-circle-mini',
		},
		{
			svgblock: 'svg-rect-mini',
		},
		{
			svgblock: 'svg-tri-mini',
		}],

		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "circ-title title-2",
		}],

	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		svgblock: [
		{
			svgblock: 'svg-rect-mini',
		}],

		extratextblock:[{
			textdata: data.string.p1t1,
			textclass: "last-title",
		},{
			textdata : data.string.f_1_2,
			textclass: 'frac-text frac-1 midtxt',
			splitintofractionsflag: true,
		}]

	},
	//slide 6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		svgblock: [
		{
			svgblock: 'svg-circle-mini',
		}],

		extratextblock:[{
			textdata: data.string.p1t2,
			textclass: "last-title",
		},{
			textdata : data.string.f_1_3,
			textclass: 'frac-text frac-1 midtxt1',
			splitintofractionsflag: true,
		}]
	},
	//slide 7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		svgblock: [
		{
			svgblock: 'svg-tri-mini',
		}],

		extratextblock:[{
			textdata: data.string.p1t3,
			textclass: "last-title",
		},{
			textdata : data.string.f_3_4,
			textclass: 'frac-text frac-1 midtxt1',
			splitintofractionsflag: true,
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "sunder", src: imgpath+"sunder.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-svg", src: imgpath+"circle.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "circle-1", src: imgpath+"circle_three_part.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "rect", src: imgpath+"rectangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "triangle", src: imgpath+"triangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_player("s1_p"+(countNext+1),1);
				// $nextBtn.show(0);
				break;
			case 1:
				sound_player("s1_p"+(countNext+1),1);
				$prevBtn.show(0);
				init_svg('#svg-circle');
				// nav_button_controls(100);
				break;
			case 2:
				sound_player("s1_p"+(countNext+1),1);
				$prevBtn.show(0);
				var s = Snap('#svg-circle');
				var svg = Snap.load(preload.getResult('circle-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					var centerline = Snap.select('#center-line');
					var rightpart = Snap.select('#right-circ');
					var leftpart = Snap.select('#left-circ');
					centerline.addClass('line-anim');
					timeoutvar2 = setTimeout(function(){
						$('.circ-line').show(0);
					}, 2000);
					rightpart.addClass('right-translate');
					leftpart.addClass('left-translate');
				} );
				break;
			case 3:
				sound_player("s1_p"+(countNext+1),1);
				$prevBtn.show(0);
				var s = Snap('#svg-circle');
				var svg = Snap.load(preload.getResult('circle-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					Snap.select('svg').attr({
						width: '100%',
						height: '100%',
					});
					var centerline = Snap.select('#center-line');
					centerline.attr('stroke-dashoffset', '0');
				} );
				break;
			case 4:
				sound_player("s1_p"+(countNext+1),0);
				$prevBtn.show(0);
				var rec_click = [0,0,0];
				var s = Snap('#svg-circle-mini');
				var svg = Snap.load(preload.getResult('circle-1').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var cir_part_1 = Snap.select('#pie01');
					var cir_part_2 = Snap.select('#pie02');
					var cir_part_3 = Snap.select('#pie03');

					var cline_1 = Snap.select('#line-1');
					var cline_2 = Snap.select('#line-2');
					var cline_3 = Snap.select('#line-3');

					var coutline_1 = Snap.select('#outline-1');
					var coutline_2 = Snap.select('#outline-2');
					var coutline_3 = Snap.select('#outline-3');
					var circle = Snap.select('#total-circ');
					cline_1.addClass('no-dash');
					cline_2.addClass('no-dash');
					cline_3.addClass('no-dash');
					circle.click(function(){
						rec_click[0]=1;
						check_click();
						cline_1.addClass('line-anim');
						cline_2.addClass('line-anim');
						cline_3.addClass('line-anim');
						timeoutvar2 = setTimeout(function(){
							coutline_1.attr('display', 'block');
							coutline_2.attr('display', 'block');
							coutline_3.attr('display', 'block');
						}, 2000);
						cir_part_1.addClass('c1-translate');
						cir_part_2.addClass('c2-translate');
						cir_part_3.addClass('c3-translate');
					});
				} );

				var s2 = Snap('#svg-rect-mini');
				var svg2 = Snap.load(preload.getResult('rect').src, function ( loadedFragment ) {
					s2.append(loadedFragment);
					//to resive whole svg
					var l_part = Snap.select('#left');
					var r_part = Snap.select('#right');
					var l_line = Snap.select('#l-line');
					var r_line = Snap.select('#r-line');
					var rect = Snap.select('#total-rect');
					var cline = Snap.select('#centreline');
					cline.addClass('no-dash');
					rect.click(function(){
						rec_click[1]=1;
						check_click();
						cline.addClass('line-anim');
						timeoutvar2 = setTimeout(function(){
							l_line.attr('display', 'block');
							r_line.attr('display', 'block');
						}, 2000);
						l_part.addClass('l-translate');
						r_part.addClass('r-translate');
					});
				} );

				var s3 = Snap('#svg-tri-mini');
				var svg3 = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s3.append(loadedFragment);
					//to resive whole svg
					var t_part_1 = Snap.select('#part-1');
					var t_part_2 = Snap.select('#part-2');
					var t_part_3 = Snap.select('#part-3');
					var t_part_4 = Snap.select('#part-4');

					var tline_1 = Snap.select('#tline-1');
					var tline_2 = Snap.select('#tline-2');
					var tline_3 = Snap.select('#tline-3');

					var toutline_1 = Snap.select('#toutline-1');
					var toutline_2 = Snap.select('#toutline-2');
					var toutline_3 = Snap.select('#toutline-3');
					var triangle = Snap.select('#total-tri');
					tline_1.addClass('no-dash');
					tline_2.addClass('no-dash');
					tline_3.addClass('no-dash');
					triangle.click(function(){
						rec_click[2]=1;
						check_click();
						tline_1.addClass('line-anim');
						tline_2.addClass('line-anim');
						tline_3.addClass('line-anim');
						timeoutvar2 = setTimeout(function(){
							toutline_1.attr('display', 'block');
							toutline_2.attr('display', 'block');
							toutline_3.attr('display', 'block');
							t_part_4.attr('stroke', '#595959');
						}, 2000);
						t_part_1.addClass('t1-translate');
						t_part_2.addClass('t2-translate');
						t_part_3.addClass('t3-translate');
					});
				} );
				function check_click(){
					for(var i=0; i<rec_click.length; i++){
						if(rec_click[i]==0){
							return false;
						}
					}
					nav_button_controls(1500)
				}
				break;
			case 5:
				sound_player("s1_p"+(countNext+1),1);
				var s2 = Snap('#svg-rect-mini');
				var svg2 = Snap.load(preload.getResult('rect').src, function ( loadedFragment ) {
					s2.append(loadedFragment);
					//to resive whole svg
					var l_fill = Snap.select('#fill-l');
					$("#svg-rect-mini").addClass("extmid");
					l_fill.attr('fill', 'white');
				} );

				break;
				case 6:
					sound_player("s1_p"+(countNext+1),1);
				// $prevBtn.show(0);
				var s = Snap('#svg-circle-mini');
				var svg = Snap.load(preload.getResult('circle-1').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_3 = Snap.select('#fill-3');
					fill_1.attr('fill', 'white');
					fill_3.attr('fill', 'white');
				} );
				break;
				case 7:
					sound_player("s1_p"+(countNext+1),1);
				var s3 = Snap('#svg-tri-mini');
				var svg3 = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s3.append(loadedFragment);
					//to resive whole svg
					var t_part_4 = Snap.select('#part-4');
						$("#svg-tri-mini").addClass("extmid1");
					t_part_4.attr('fill', 'white');
				} );
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();//
		current_sound.on('complete', function(){
			nextBtn?nav_button_controls():'';
		});
	}
	function init_svg( id, func ){
		var s = Snap(id);
		var svg = Snap.load(preload.getResult('circle-svg').src, function ( loadedFragment ) {
			s.append(loadedFragment);
			//to resive whole svg
			Snap.select('svg').attr({
				width: '100%',
				height: '100%',
			});
			func();
		} );
		return svg;
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

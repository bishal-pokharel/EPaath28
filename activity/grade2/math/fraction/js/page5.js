var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.string.p5text0,
			textclass: "diy-text",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'diy-bg'
				},
			]
		}],
	},
	//slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.p5text1,
			textclass: "instruction",
		},{
			textdata: data.string.p5text2,
			textclass: "hint-text hint-text-1",
		},{
			textdata: data.string.p5text3,
			textclass: "hint-text hint-text-2",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
			]
		}],
	},
	//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',

		svgblock:[{
			svgblock: 'circle-svg',
		}],

		extratextblock:[{
			textdata: data.string.p5text1,
			textclass: "instruction",
		},{
			textdata: data.string.p5text4,
			textclass: "hint-text hint-text-1",
		},{
			textdata: data.string.p5text5,
			textclass: "hint-text hint-text-2",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
			]
		}],
	},
	//slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.p5text1,
			textclass: "instruction",
		},{
			textdata: data.string.p5text6,
			textclass: "hint-text hint-text-1",
		},{
			textdata: data.string.p5text7,
			textclass: "hint-text hint-text-2",
		},{
			textdata: '',
			textclass: "line-frac",
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
		input:[{
			inputclass: 'value-input input-1'
		},{
			inputclass: 'value-input input-2'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "feedback-img",
					imgsrc : '',
					imgid : 'incorrect'
				},
			]
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "incorrect", src: "images/sundar/incorrect-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/sundar/normal.png", type: createjs.AbstractLoader.IMAGE},

			{id: "diy-bg", src: imgpath+"bg_for_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle", src: imgpath+"diy/circle-2.svg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(1500);
				break;
			case 1:
				sound_player("s5_p2");
				$prevBtn.show(0);
				for(var i=0; i<4; i++){
					var str_div_1 = '<div class="rect-block rect-b-1"> </div>';
					$('#center-rect').append(str_div_1);
					$('.rect-block').eq(i).attr('id', 'm-rect-'+i);
					if(i<3){
						$('#m-rect-'+i).addClass('filled-block');
					}
				}
				input_box('.value-input', 1, '.check-btn');
				$('.check-btn').click(function(){
					checker(3, 4);
				});
				break;
			case 2:
				var s = Snap('#circle-svg');
				var svg = Snap.load(preload.getResult('circle').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_2 = Snap.select('#fill-2');
					var fill_3 = Snap.select('#fill-3');
					fill_2.attr('fill', '#FFE599');
					fill_3.attr('fill', '#FFE599');
				} );
				input_box('.value-input', 1, '.check-btn');
				$('.check-btn').click(function(){
					checker(2, 3);
				});
				break;
			case 3:
				$prevBtn.show(0);
				for(var i=0; i<12; i++){
					var str_div_1 = '<div class="rect-block rect-b-2"> </div>';
					$('#center-rect').append(str_div_1);
					$('.rect-block').eq(i).attr('id', 'm-rect-'+i);
					if(i%4!=3){
						$('#m-rect-'+i).addClass('filled-block');
					}
				}
				input_box('.value-input', 2, '.check-btn');
				$('.check-btn').click(function(){
					checker(9, 12);
				});
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function checker(ans1, ans2){
		if($('.input-1').val()=='' && $('.input-2').val()==''){
			return false;
		}
		if($('.input-1').val()==ans1 && $('.input-2').val()==ans2){
			$('.feedback-img').attr('src', preload.getResult('correct').src);
			$('.feedback-img').show(0);
			$('.hint-text').show(0);
			play_correct_incorrect_sound(1);
			$('.value-input, .check-btn').css({
				'background-color': '#92AF3B',
				'border-color': '#DDE27B',
				'color': 'white',
				'pointer-events': 'none'
			});
			nav_button_controls(0);
		} else{
			$('.feedback-img').attr('src', preload.getResult('incorrect').src);
			$('.feedback-img').show(0);
			play_correct_incorrect_sound(0);
			$('.check-btn').css({
				'background-color': '#FF0000',
				'border-color': '#980000',
				'color': 'white'
			});
			if($('.input-1').val()==''){
				$('.input-1').css({
					'border-color': '#EEFF41',
					'background-color': 'white',
					'color': 'black',
				});
			} else if($('.input-1').val()==ans1){
				$('.hint-text-1').show(0);
				$('.input-1').css({
					'background-color': '#92AF3B',
					'border-color': '#DDE27B',
					'color': 'white',
					'pointer-events': 'none'
				});
			} else{
				$('.hint-text-1').show(0);
				$('.input-1').css({
					'background-color': '#FF0000',
					'border-color': '#980000',
					'color': 'white'
				});
			}

			if($('.input-2').val()==''){
				$('.input-2').css({
					'border-color': '#EEFF41',
					'background-color': 'white',
					'color': 'black',
				});
			} else if($('.input-2').val()==ans2){
				$('.hint-text-2').show(0);
				$('.input-2').css({
					'background-color': '#92AF3B',
					'border-color': '#DDE27B',
					'color': 'white',
					'pointer-events': 'none'
				});
			} else{
				$('.hint-text-2').show(0);
				$('.input-2').css({
					'background-color': '#FF0000',
					'border-color': '#980000',
					'color': 'white'
				});
			}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

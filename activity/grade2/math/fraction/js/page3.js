var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',
		svgblock: [{
			svgblock: 'triangle-svg',
		}],

		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "first-text",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "roti roti-1",
					imgsrc : '',
					imgid : 'roti'
				},
				{
					imgclass : "pencil",
					imgsrc : '',
					imgid : 'pencil'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',
		svgblock: [{
			svgblock: 'triangle-svg',
		}],

		extratextblock:[{
			textdata: data.string.p3text2,
			textclass: "first-text",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "roti roti-1",
					imgsrc : '',
					imgid : 'roti'
				},
				{
					imgclass : "roti roti-2",
					imgsrc : '',
					imgid : 'roti-1'
				},
				{
					imgclass : "roti roti-3",
					imgsrc : '',
					imgid : 'roti-2'
				},
				{
					imgclass : "roti roti-4",
					imgsrc : '',
					imgid : 'roti-4'
				},
				{
					imgclass : "pencil pencil-0",
					imgsrc : '',
					imgid : 'pencil'
				},
				{
					imgclass : "pencil pencil-1",
					imgsrc : '',
					imgid : 'pencil-1'
				}
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',
		svgblock: [{
			svgblock: 'triangle-svg',
		}],

		extratextblock:[{
			textdata: data.string.p3text3,
			textclass: "dd-text",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "roti roti-1",
					imgsrc : '',
					imgid : 'roti-1'
				},
				{
					imgclass : "pencil",
					imgsrc : '',
					imgid : 'pencil-1'
				}
			]
		}],
		dropblock: true,
		dragblock:[{
			splitintofractionsflag: true,
			dragdata1: data.string.f_1_3,
			dragclass1: '',
			dragdata2: data.string.f_2_3,
			dragclass2: '',
			dragdata3: data.string.f_1_4,
			dragclass3: '',
		}]
	},
	//slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',
		extratextblock:[{
			textdata: data.string.p3text5,
			textclass: "center-text center-text2",
		},{
			textdata: data.string.p3text6,
			textclass: "center-text center-text1",
		}],
	},
	//slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-green',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'diy-bg'
				},
			]
		}]
	},
	//slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.p3text7,
			textclass: "click-ins",
		},{
			textdata: data.string.f_1_4,
			textclass: "big-frac",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
	},
	//slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.p3text7,
			textclass: "click-ins",
		},{
			textdata: data.string.f_2_3,
			textclass: "big-frac",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
	},
	//slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		svgblock: [{
			svgblock: 'center-svg',
		}],
		extratextblock:[{
			textdata: data.string.p3text7,
			textclass: "click-ins",
		},{
			textdata: data.string.f_7_8,
			textclass: "big-frac",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
	},
	//slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-light',
		rectangleblock: 'center-rect',
		extratextblock:[{
			textdata: data.string.p3text7,
			textclass: "click-ins",
		},{
			textdata: data.string.f_3_10,
			textclass: "big-frac",
			splitintofractionsflag: true,
		},{
			textdata: data.string.pcheck,
			textclass: "check-btn",
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var index = 0;
	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;


	var click_number = 0;
	//1 means off
	var click_part = [1,1,1];
	var click_arr;
	var wrong_clicked = false;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "roti", src: imgpath+"roti_full.png", type: createjs.AbstractLoader.IMAGE},
			{id: "roti-1", src: imgpath+"roti01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "roti-2", src: imgpath+"roti02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "roti-3", src: imgpath+"roti03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "roti-4", src: imgpath+"roti04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "triangle", src: imgpath+"new/triangle.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-1", src: imgpath+"diy/circle_part_4.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-2", src: imgpath+"diy/circle-2.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "circle-3", src: imgpath+"diy/circle_part_8.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "pencil", src: imgpath+"pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil-1", src: imgpath+"pencil01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "diy-bg", src: imgpath+"diy/bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		put_image(content, countNext);

		switch(countNext) {
			case 0:
				sound_player("s3_p"+(countNext+1),1);
				var s = Snap('#triangle-svg');
				var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#tfill-1');
					var fill_2 = Snap.select('#tfill-2');
					var fill_3 = Snap.select('#tfill-3');
					var line_1 = Snap.select('#tline-1');
					var line_2 = Snap.select('#tline-2');
					var line_3 = Snap.select('#tline-3');
					fill_1.attr('fill', '#FFFFFF');
					fill_2.attr('fill', '#FFFFFF');
					fill_3.attr('fill', '#FFFFFF');
					line_1.attr('display', 'none');
					line_2.attr('display', 'none');
					line_3.attr('display', 'none');
				} );
				break;
			case 1:
				sound_player("s3_p"+(countNext+1),1);
				$prevBtn.show(0);
				// nav_button_controls(100);
				var tline_1, tline2, tline3, fill4;
				var s = Snap('#triangle-svg');
				var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#tfill-1');
					var fill_2 = Snap.select('#tfill-2');
					var fill_3 = Snap.select('#tfill-3');
					fill_4 = Snap.select('#part-4');
					tline_1 = Snap.select('#tline-1');
					tline_2 = Snap.select('#tline-2');
					tline_3 = Snap.select('#tline-3');
					fill_1.attr('fill', '#FFFFFF');
					fill_2.attr('fill', '#FFFFFF');
					fill_3.attr('fill', '#FFFFFF');
					fill_4.addClass('transition-class');
					tline_1.addClass('no-dash');
					tline_2.addClass('no-dash');
					tline_3.addClass('no-dash');
				} );
				timeoutvar2 = setTimeout(function(){
					$('.roti-4').fadeIn(500, function(){
						$('.roti-2, .roti-3').show(0);
						$('.roti-1').hide(0);
						timeoutvar2 = setTimeout(function(){
							$('.roti-4').fadeOut(500, function(){
								$('.roti-3').fadeOut(500, function(){//pencil anim
									timeoutvar2 = setTimeout(function(){
										$('.pencil-1').fadeIn(500, function(){
											$('.pencil-0').fadeOut(500, function(){//triangle
												tline_1.addClass('line-anim');
												tline_2.addClass('line-anim');
												tline_3.addClass('line-anim');
												fill_4.animate({'fill': '#DADADA'}, 500);
											});
										});
									}, 1000);
								});
							});
						}, 500);
					});
				}, 1000);
				break;
			case 4:
				sound_player("s3_p"+(countNext+1),1);
			break;
			case 2:
				sound_player("s3_p"+(countNext+1),0);
				$prevBtn.show(0);
				var s = Snap('#triangle-svg');
				var svg = Snap.load(preload.getResult('triangle').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#tfill-1');
					var fill_2 = Snap.select('#tfill-2');
					var fill_3 = Snap.select('#tfill-3');
					var fill_4 = Snap.select('#part-4');
					fill_1.attr('fill', '#FFFFFF');
					fill_2.attr('fill', '#FFFFFF');
					fill_3.attr('fill', '#FFFFFF');
					fill_4.attr('fill', '#DADADA');
				} );
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.drag-'+i).addClass('pos-'+positions[i-1]);
				}
				var item_count = 0;
				$('.dragblock').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');
					}
				});
				function dropper(dropped, droppedOn, hasclass){
					if(dropped.hasClass(hasclass)){
						dropped.draggable('option', 'revert', false);
						play_correct_incorrect_sound(1);
						item_count++;
						dropped.draggable('disable');
						dropped.detach().css({
							'top': '50%',
							'left': '0%',
							'width': '100%',
							'border': 'none',
							'background-color': 'transparent'
						}).appendTo(droppedOn);
						$(droppedOn).css({
							'background-color': '#92AF3B',
							'border-color': '#D4EF34'
						});
						if(item_count>2){
							$nextBtn.show(0);
						}
					} else{
						play_correct_incorrect_sound(0);
					}
				}
				break;
				case 3:
				$(".center-text2").show(0);
				$(".center-text1").delay(4500).show(0);
					sound_player("s3_p"+(countNext+1),1);
				break;
			case 5:
				sound_player("s3_p"+(countNext+1),0);
				$prevBtn.show(0);
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1,1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('circle-1').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_2 = Snap.select('#fill-2');
					var fill_3 = Snap.select('#fill-3');
					var fill_4 = Snap.select('#fill-4');
					click_arr = [fill_1, fill_2, fill_3, fill_4];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});

					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});

					fill_3.hover(function(){
						fill_3.attr('fill', "CFE2F3");
					}, function(){
						(click_part[2]==1)?fill_3.attr('fill', "transparent"):fill_3.attr('fill', "#FFE599");
					});
					fill_3.click(function(){
						clicker(2, fill_3);
					});

					fill_4.hover(function(){
						fill_4.attr('fill', "CFE2F3");
					}, function(){
						(click_part[3]==1)?fill_4.attr('fill', "transparent"):fill_4.attr('fill', "#FFE599");
					});
					fill_4.click(function(){
						clicker(3, fill_4);
					});
				} );
				$('.check-btn').click(function(){
					check_button_func(1);
				});
				break;
			case 6:
				// sound_player("s3_p"+(countNext+1),1);
				$prevBtn.show(0);
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('circle-2').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_2 = Snap.select('#fill-2');
					var fill_3 = Snap.select('#fill-3');
					click_arr = [fill_1, fill_2, fill_3];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});

					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});

					fill_3.hover(function(){
						fill_3.attr('fill', "CFE2F3");
					}, function(){
						(click_part[2]==1)?fill_3.attr('fill', "transparent"):fill_3.attr('fill', "#FFE599");
					});
					fill_3.click(function(){
						clicker(2, fill_3);
					});
				} );
				$('.check-btn').click(function(){
					check_button_func(2);
				});
				break;
			case 7:
				// sound_player("s3_p"+(countNext+1),1);
				$prevBtn.show(0);
				click_number = 0;
				//1 means off
				wrong_clicked = false;
				click_part = [1,1,1,1,1,1,1,1];
				var s = Snap('#center-svg');
				var svg = Snap.load(preload.getResult('circle-3').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var fill_1 = Snap.select('#fill-1');
					var fill_2 = Snap.select('#fill-2');
					var fill_3 = Snap.select('#fill-3');
					var fill_4 = Snap.select('#fill-4');
					var fill_5 = Snap.select('#fill-5');
					var fill_6 = Snap.select('#fill-6');
					var fill_7 = Snap.select('#fill-7');
					var fill_8 = Snap.select('#fill-8');
					click_arr = [fill_1, fill_2, fill_3, fill_4, fill_5, fill_6, fill_7, fill_8];
					fill_1.hover(function(){
						fill_1.attr('fill', "CFE2F3");
					}, function(){
						(click_part[0]==1)?fill_1.attr('fill', "transparent"):fill_1.attr('fill', "#FFE599");
					});
					fill_1.click(function(){
						clicker(0, fill_1);
					});

					fill_2.hover(function(){
						fill_2.attr('fill', "CFE2F3");
					}, function(){
						(click_part[1]==1)?fill_2.attr('fill', "transparent"):fill_2.attr('fill', "#FFE599");
					});
					fill_2.click(function(){
						clicker(1, fill_2);
					});

					fill_3.hover(function(){
						fill_3.attr('fill', "CFE2F3");
					}, function(){
						(click_part[2]==1)?fill_3.attr('fill', "transparent"):fill_3.attr('fill', "#FFE599");
					});
					fill_3.click(function(){
						clicker(2, fill_3);
					});

					fill_4.hover(function(){
						fill_4.attr('fill', "CFE2F3");
					}, function(){
						(click_part[3]==1)?fill_4.attr('fill', "transparent"):fill_4.attr('fill', "#FFE599");
					});
					fill_4.click(function(){
						clicker(3, fill_4);
					});

					fill_5.hover(function(){
						fill_5.attr('fill', "CFE2F3");
					}, function(){
						(click_part[4]==1)?fill_5.attr('fill', "transparent"):fill_5.attr('fill', "#FFE599");
					});
					fill_5.click(function(){
						clicker(4, fill_5);
					});

					fill_6.hover(function(){
						fill_6.attr('fill', "CFE2F3");
					}, function(){
						(click_part[5]==1)?fill_6.attr('fill', "transparent"):fill_6.attr('fill', "#FFE599");
					});
					fill_6.click(function(){
						clicker(5, fill_6);
					});

					fill_7.hover(function(){
						fill_7.attr('fill', "CFE2F3");
					}, function(){
						(click_part[6]==1)?fill_7.attr('fill', "transparent"):fill_7.attr('fill', "#FFE599");
					});
					fill_7.click(function(){
						clicker(6, fill_7);
					});

					fill_8.hover(function(){
						fill_8.attr('fill', "CFE2F3");
					}, function(){
						(click_part[7]==1)?fill_8.attr('fill', "transparent"):fill_8.attr('fill', "#FFE599");
					});
					fill_8.click(function(){
						clicker(7, fill_8);
					});
				} );
				$('.check-btn').click(function(){
					check_button_func(7);
				});
				break;
			case 8:
				sound_player("s3_p"+(countNext+1),1);
				$prevBtn.show(0);
				var rect_counter = 0;
				var rect_arr = [0,0,0,0,0, 0,0,0,0,0];
				for(var i=1; i<11; i++){
					var str_div_1 = '<div class="rect-block"> </div>';
					$('#center-rect').append(str_div_1);
					$('.rect-block').eq(i-1).attr('id', 'm-rect-'+i);
				}
				$('.rect-block').on('click', function(){
					var rec_id = $(this).attr('id').replace(/\D/g, '');
					if(rect_arr[rec_id-1]==0){
						$(this).css('background-color', '#FFE599');
						rect_arr[rec_id-1] = 1;
						rect_counter++;
					} else{
						$(this).css('background-color', 'transparent');
						rect_arr[rec_id-1] = 0;
						rect_counter--;
					}
				});
				$('.check-btn').click(function(){
					if(rect_counter == 3){
						$('.check-btn').css({
							'background-color': '#92AF3B',
							'border-color': '#D4EF34',
							'color': 'white',
							'pointer-events': 'none'
						});
						play_correct_incorrect_sound(1);
						for(var i=0; i<10; i++){
							if(rect_arr[i]==1){
								$('#m-rect-'+(parseInt(i)+1)).css('background-color', '#BED62F');
							}
						}
						$('#center-rect').css('pointer-events', 'none');
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$('.check-btn').css({
							'background-color': '#FF0000',
							'border-color': '#980000',
							'color':'black'
						});
						play_correct_incorrect_sound(0);
						for(var i=0; i<10; i++){
							if(rect_arr[i]==1){
								$('#m-rect-'+(parseInt(i)+1)).css('background-color', '#CC0000');
							}
						}
					}
				});
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
					sound_player("s3_p"+(countNext+1),1);
				// nav_button_controls(100);
				break;
		}
	}
	function clicker(index, class_snap){
		if(wrong_clicked == true){
			for(var i=0; i<click_part.length; i++){
				click_arr[i].attr('fill', 'transparent');
			}
		}
		if(click_part[index]==1){
			click_number++;
			click_part[index] = 0;
			class_snap.attr('fill', "#FFE599");
		} else {
			click_number--;
			click_part[index] = 1;
			class_snap.attr('fill', "transparent");
		}
		wrong_clicked = false;
	}
	function reset_color(){
		for(var i=0; i<click_part.length; i++){
			if(click_part[i]==0){
				click_arr[i].attr('fill', '#FFE599');
			}
		}
	}
	function check_button_func(correct_num){
		if(click_number==correct_num){
			wrong_clicked = false;
			$('.check-btn').css({
				'background-color': '#92AF3B',
				'border-color': '#D4EF34',
				'color': 'white',
				'pointer-events': 'none'
			});
			play_correct_incorrect_sound(1);
			for(var i=0; i<click_part.length; i++){
				if(click_part[i]==0){
					click_arr[i].attr('fill', '#BED62F');
				}
			}
			$('svg').css('pointer-events', 'none');
			$nextBtn.show(0);
		} else {
			wrong_clicked = false;
			$('.check-btn').css({
				'background-color': '#FF0000',
				'border-color': '#980000',
				'color':'black'
			});
			play_correct_incorrect_sound(0);
			for(var i=0; i<click_part.length; i++){
				if(click_part[i]==0){
					click_arr[i].attr('fill', '#CC0000');
				}
			}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,nextBtn){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nextBtn?nav_button_controls():'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

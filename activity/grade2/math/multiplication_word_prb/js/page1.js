var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
      //slide0
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "coverback",
          textblock: [
              {
                  textdiv:"chtitle",
                  textclass: "datafont centertext",
                  textdata: data.string.chapter
              },
          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "coverpage",
                      imgclass: "relativecls img1",
                      imgid: 'covermainImg',
                      imgsrc: ""
                  },

              ]
          }]
      },
      // slide1
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "magician",
                      imgclass: "relativecls img2",
                      imgid: 'magicianImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "dial",
                      imgclass: "dialimg img3",
                      imgid: 'dialImg',
                      imgsrc: "",
                      textblock:[
                          {
                              imgcls:"dialtxt fntsz4",
                              imgtxt:data.string.p1text1
                          }
                      ]
                  },
              ]
          }],
      },
      //slide 2
      {
              contentnocenteradjust: true,
              contentblockadditionalclass: "bg",
              textblock: [
                  {
                      datahighlightflag : true,
                      datahighlightcustomclass : 'onebyone',
                      textdiv:"maintitle",
                      textclass: "content centertext",
                      textdata: data.string.p1text2
                  },
                  {
                      textdiv:"belowtext",
                      textclass: "content centertext",
                      textdata: data.string.p1text3
                  },

              ],
              imageblock: [{
                  imagestoshow: [
                      {
                          imgdiv: "background",
                          imgclass: "relativecls img1",
                          imgid: 'bgImg',
                          imgsrc: ""
                      },
                      {
                          imgdiv: "hand h1 fadeInEffect",
                          imgclass: "relativecls img2",
                          imgid: 'hand1Img',
                          imgsrc: ""
                      },
                      {
                          imgdiv: "hand h2 fadeInEffect",
                          imgclass: "relativecls img3",
                          imgid: 'hand2Img',
                          imgsrc: ""
                      },
                  ]
              }],
            questionbox:[
                {
                    boxdiv:"mainquestionbox",
                    textblock:[
                        {
                            textdiv:"question",
                            textclass: "content2 centertext",
                            textdata: data.string.clickmsg
                        },
                        {
                            textdiv:"total",
                            textclass: "content centertext",
                            textdata: data.string.total
                        },
                        {
                            textdiv:"option opt1",
                            textclass: "content centertext",
                            textdata: data.string.p1text3_1,
                            ans:"correct"
                        },
                        {
                            textdiv:"option opt2",
                            textclass: "content centertext",
                            textdata: data.string.p1text3_2
                        },
                    ]
                }
            ]
          },
      //slide 3
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text4
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext text1",
                  textclass: "content centertext",
                  textdata: data.string.p1text5
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext showcorrect",
                  textclass: "content centertext",
                  textdata: data.string.cmsg3
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand h1 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand h2 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total
                      },
                      {
                          datahighlightflag : true,
                          datahighlightcustomclass : 'multiply',
                          textdiv:"option opt1",
                          textclass: "content centertext",
                          textdata: data.string.p1text5_1,
                          ans:"correct"
                      },
                      {
                          textdiv:"option opt2",
                          textclass: "content centertext",
                          textdata: data.string.p1text5_2
                      },
                  ]
              }
          ]
      },
      //slide 4
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'onebyone',
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text6
              },
              {
                  textdiv:"belowtext text1",
                  textclass: "content centertext",
                  textdata: data.string.p1text7
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext showcorrect",
                  textclass: "content centertext",
                  textdata: data.string.cmsg4
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h3 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h4 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h5 fadeInEffect",
                      imgclass: "relativecls img4",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total
                      },
                      {
                          textdiv:"option opt1",
                          textclass: "content centertext",
                          textdata: data.string.p1text7_1,
                          ans:"correct"
                      },
                      {
                          textdiv:"option opt2",
                          textclass: "content centertext",
                          textdata: data.string.p1text7_2
                      },
                  ]
              }
          ]
      },
      //slide 5
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text8
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext text1",
                  textclass: "content centertext",
                  textdata: data.string.p1text9
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext showcorrect",
                  textclass: "content centertext",
                  textdata: data.string.cmsg5
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h3 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h4 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand1 h5_1 fadeInEffect",
                      imgclass: "relativecls img4",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total
                      },
                      {
                          datahighlightflag : true,
                          datahighlightcustomclass : 'multiply',
                          textdiv:"option opt1",
                          textclass: "content centertext",
                          textdata: data.string.p1text9_1,
                          ans:"correct"
                      },
                      {
                          textdiv:"option opt2",
                          textclass: "content centertext",
                          textdata: data.string.p1text9_2
                      },
                  ]
              }
          ]
      },
  //                    slide 6
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [

              {
                  textdiv:"centerdiv",
                  textclass: "title centertext",
                  textdata: data.string.p1text10
              },

          ],
      },
  //    slide 7
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text11
              },
              {
                  textdiv:"belowtext text1",
                  textclass: "content centertext",
                  textdata: data.string.p1text12
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext showcorrect",
                  textclass: "content centertext",
                  textdata: data.string.cmsg7
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h6 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h7 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h8 fadeInEffect",
                      imgclass: "relativecls img4",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h9 fadeInEffect",
                      imgclass: "relativecls img5",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total
                      },
                      {
                          textdiv:"option opt1",
                          textclass: "content centertext",
                          textdata: data.string.p1text12_1,
                          ans:"correct"
                      },
                      {
                          textdiv:"option opt2",
                          textclass: "content centertext",
                          textdata: data.string.p1text12_2
                      },
                  ]
              }
          ]
      },
  //    slide 8
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text13
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext text1",
                  textclass: "content centertext",
                  textdata: data.string.p1text14
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext showcorrect",
                  textclass: "content centertext",
                  textdata: data.string.cmsg8
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h6 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h7 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h8 fadeInEffect",
                      imgclass: "relativecls img4",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h9_1 fadeInEffect",
                      imgclass: "relativecls img5",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total
                      },
                      {
                          datahighlightflag : true,
                          datahighlightcustomclass : 'multiply',
                          textdiv:"option opt1",
                          textclass: "content centertext",
                          textdata: data.string.p1text14_1,
                          ans:"correct"
                      },
                      {
                          textdiv:"option opt2",
                          textclass: "content centertext",
                          textdata: data.string.p1text14_2
                      },
                  ]
              }
          ]
      },
  //slide 9
      {
          contentnocenteradjust: true,
          contentblockadditionalclass: "bg",
          textblock: [
              {
                  textdiv:"maintitle fadeInEffect",
                  textclass: "content centertext",
                  textdata: data.string.p1text11
              },
              {
                  datahighlightflag : true,
                  datahighlightcustomclass : 'multiply',
                  textdiv:"belowtext",
                  textclass: "content centertext",
                  textdata: data.string.p1text15
              },

          ],
          imageblock: [{
              imagestoshow: [
                  {
                      imgdiv: "background",
                      imgclass: "relativecls img1",
                      imgid: 'bgImg',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h6 fadeInEffect",
                      imgclass: "relativecls img2",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h7 fadeInEffect",
                      imgclass: "relativecls img3",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h8 fadeInEffect",
                      imgclass: "relativecls img4",
                      imgid: 'hand1Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "hand2 h9_1 fadeInEffect",
                      imgclass: "relativecls img5",
                      imgid: 'hand2Img',
                      imgsrc: ""
                  },
                  {
                      imgdiv: "downarrow fadeInEffect",
                      imgclass: "relativecls img6",
                      imgid: 'arrowImg',
                      imgsrc: ""
                  },
              ]
          }],
          questionbox:[
              {
                  boxdiv:"mainquestionbox",
                  textblock:[
                      {
                          textdiv:"question",
                          textclass: "content2 centertext",
                          textdata: data.string.clickmsg
                      },
                      {
                          datahighlightflag : true,
                          datahighlightcustomclass : 'multiply',
                          textdiv:"total",
                          textclass: "content centertext",
                          textdata: data.string.total1
                      },
                      {
                          textdiv:"equal",
                          textclass: "content centertext",
                          textdata: data.string.equalto
                      },
                      {
                          textdiv:"inputbox",
                          textclass: "content centertext",
                          textdata: data.string.ten
                      },
                      {
                          textdiv:"submit",
                          textclass: "content centertext",
                          textdata: data.string.submit
                      }
                  ]
              }
          ]
      },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "bgImg", src: imgpath + "circus-new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "magicianImg", src: imgpath + "magician-nepali.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hand1Img", src: imgpath + "hand01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hand2Img", src: imgpath + "hand02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "down.png", type: createjs.AbstractLoader.IMAGE},
            {id: "covermainImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "s1_p1", src: soundAsset + "s1_p1.ogg"},
            {id: "s1_p2", src: soundAsset + "s1_p2.ogg"},
            {id: "s1_p3", src: soundAsset + "s1_p3.ogg"},
            {id: "s1_p4", src: soundAsset + "s1_p4.ogg"},
            {id: "s1_p5", src: soundAsset + "s1_p5.ogg"},
            {id: "s1_p6", src: soundAsset + "s1_p6.ogg"},
            {id: "s1_p7", src: soundAsset + "s1_p7.ogg"},
            {id: "s1_p8", src: soundAsset + "s1_p8.ogg"},
            {id: "s1_p9", src: soundAsset + "s1_p9.ogg"},
            {id: "s1_p10", src: soundAsset + "s1_p10.ogg"},
            {id: "s1_p3_1", src: soundAsset + "s1_p3_1.ogg"},
            {id: "s1_p3_2", src: soundAsset + "s1_p3_2.ogg"},
            // {id: "s1_p3_3", src: soundAsset + "s1_p3_3.ogg"},
            {id: "s1_p4_1", src: soundAsset + "s1_p4_1.ogg"},
            {id: "s1_p4_2", src: soundAsset + "s1_p4_2.ogg"},
            {id: "s1_p4_3", src: soundAsset + "s1_p4_3.ogg"},
            {id: "s1_p5_1", src: soundAsset + "s1_p5_1.ogg"},
            {id: "s1_p5_2", src: soundAsset + "s1_p5_2.ogg"},
            {id: "s1_p5_3", src: soundAsset + "s1_p5_3.ogg"},
            {id: "s1_p6_1", src: soundAsset + "s1_p6_1.ogg"},
            {id: "s1_p6_2", src: soundAsset + "s1_p6_2.ogg"},
            {id: "s1_p6_3", src: soundAsset + "s1_p6_3.ogg"},
            {id: "s1_p8_1", src: soundAsset + "s1_p8_1.ogg"},
            {id: "s1_p8_2", src: soundAsset + "s1_p8_2.ogg"},
            {id: "s1_p8_3", src: soundAsset + "s1_p8_3.ogg"},
            {id: "s1_p9_1", src: soundAsset + "s1_p9_1.ogg"},
            {id: "s1_p9_2", src: soundAsset + "s1_p9_2.ogg"},
            {id: "s1_p9_3", src: soundAsset + "s1_p9_3.ogg"},
            {id: "s1_p10_1", src: soundAsset + "s1_p10_1.ogg"},
            {id: "s1_p10_2", src: soundAsset + "s1_p10_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            case 1:
            case 6:
              sound_player("s1_p"+(countNext+1),1);
            break;
            case 9:
                count = 0;
                appeartextonebyone($(".onebyone"));
                $(".belowtext, .text1, .downarrow").hide(0);
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("s1_p"+(countNext+1));
                  current_sound.play();
                  current_sound.on('complete', function(){
                    $(".belowtext").fadeIn(0);
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
                    current_sound.play();
                    current_sound.on('complete', function(){
                      createjs.Sound.stop();
                      current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_2");
                      current_sound.play();
                      $(".mainquestionbox").animate({"opacity":"0.9","right":"0%"},1000);
                      $(".downarrow").fadeIn(1000);
                    });
                  });
                if(countNext>3){
                    $(".belowtext p").removeClass("content").addClass("content2");
                }
                checkans1();
            break;
            default:
              // sound_player("s1_p"+(countNext+1),0);
                $(".onebyone").css("opacity","1");
                $(".belowtext, .text1").hide(0);
                switch (countNext) {
                  case 2:
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
                    current_sound.play();
                    current_sound.on('complete', function(){
                      $(".belowtext").fadeIn(0);
                      createjs.Sound.stop();
                      current_sound = createjs.Sound.play("s1_p"+(countNext+1));
                      current_sound.play();
                      current_sound.on('complete', function(){
                        createjs.Sound.stop();
                        current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_2");
                        current_sound.play();
                        $(".mainquestionbox").animate({"opacity":"0.9","right":"0%"},1000);
                        checkans(countNext<4?data.string.correct:eval("data.string.cmsg"+countNext));
                      });
                    });
                  break;
                  case 3:
                  case 4:
                  case 5:
                  case 7:
                  case 8:
                    createjs.Sound.stop();
                    current_sound = createjs.Sound.play("s1_p"+(countNext+1));
                    current_sound.play();
                    current_sound.on('complete', function(){
                      $(".text1").fadeIn(1000);
                      // $(".belowtext").fadeIn(0);
                      createjs.Sound.stop();
                      current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
                      current_sound.play();
                      current_sound.on('complete', function(){
                        createjs.Sound.stop();
                        current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_2");
                        current_sound.play();
                        $(".mainquestionbox").animate({"opacity":"0.9","right":"0%"},1000);
                        checkans(countNext<4?data.string.correct:eval("data.string.cmsg"+countNext));
                      });
                    });
                  break;
                }
                // appeartextonebyone($(".onebyone"));
                $(".showcorrect").hide();
                shufflehint();
                countNext>2?$(".belowtext p").removeClass("content").addClass("content2"):"";
                if(countNext==7 || countNext== 8)
                   $(".option").css({"width":"67%","left":"11%"});
                break;
        }
    }


    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function sound_player(sound_id, navigate) {
      // alert(sound_id);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        createjs.Sound.stop();
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function appeartextonebyone(className) {
        var delaytime = 50;
        setTime =setTimeout(function(){
            vocabcontroller.findwords(countNext);
        },(className.length+2)*1000);
        for (var i = 0; i < className.length; i++) {
            delaytime = delaytime+1000;
            className.eq(i).delay(delaytime).animate({"opacity":"1"},1000);
        }
    }
    function shufflehint() {
        var optdiv = $(".mainquestionbox");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"];
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).attr("data-answer"));
        });
    }
    function checkans(correctmsg){
        $(".option").click(function(){
            if($(this).attr("data-answer")=="correct"){
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                countNext==2?play_correct_incorrect_sound(1):'';
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                $(".text1").hide();
                $(".showcorrect").show().css("color","#98c02e");
                countNext==2?navigationcontroller(countNext, $total_page):'';
                sound_player("s1_p"+(countNext+1)+"_3",1);
            }
            else{
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
            }
        })
    }
    function checkans1(){
        $(".inputbox p").text("");
        var array1 = [data.string.ten, data.string.five, data.string.twenty, data.string.fifteen];
        $(".downarrow").click(function(){
            array1 = optionselect(array1,$(".inputbox p"));
        });
        $(".submit").click(function(){
            if($(".inputbox p").text()==20){
                play_correct_incorrect_sound(1);
                $(".inputbox").addClass("correctans");
                navigationcontroller(countNext, $total_page,true);
            }
            else{
                play_correct_incorrect_sound(0);
                $(".inputbox").addClass("wrongans");

            }
        })
    }
    function optionselect(array,optionselectelem){
        optionselectelem.parent().removeClass("wrongans");
        array.splice(array.indexOf(optionselectelem.text()), 1);
        var random = array[Math.floor(Math.random() * array.length)];
        optionselectelem.text(random);
        count++;
        if (count > 1) {
            count = 0;
            return [data.string.ten, data.string.five, data.string.twenty, data.string.fifteen];
        }
        else
            return array;
    }

});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t2,
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt2,
            },
        ],
     imageblock: [{
        imagestoshow: [
            {
                imgdiv: "background",
                imgclass: "relativecls img1",
                imgid: 'bgImg',
                imgsrc: ""
            },
            {
                imgdiv: "qimage",
                imgclass: "relativecls img2",
                imgid: 'q1Img',
                imgsrc: ""
            }

        ]
     }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt3,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt4,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                textdiv:"equal",
                textclass: "content centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"inputbox",
                textclass: "content centertext",
                textdata: data.string.ten
            },
            {
                textdiv:"submit",
                textclass: "content centertext",
                textdata: data.string.submit
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t2,
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt3,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt4,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                textdiv:"equal",
                textclass: "content centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"inputbox",
                textclass: "content centertext",
                textdata: data.string.ten
            },
            {
                textdiv:"submit",
                textclass: "content centertext",
                textdata: data.string.submit
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t2,
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt3,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt4,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                textdiv:"equal",
                textclass: "content centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"inputbox",
                textclass: "content centertext",
                textdata: data.string.ten
            },
            {
                textdiv:"submit",
                textclass: "content centertext",
                textdata: data.string.submit
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t2,
            },
            {
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt1,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt2,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"option opt1",
                textclass: "content1 centertext",
                textdata: data.string.e1opt3,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "content1 centertext",
                textdata: data.string.e1opt4,
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"question ",
                textclass: "content2 centertext",
                textdata: data.string.e1t1,
            },
            {
                textdiv:"clickmsg",
                textclass: "content2 centertext",
                textdata: data.string.clickmsg,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'multiply',
                textdiv:"question2 ",
                textclass: "content1 centertext",
                textdata: data.string.e1t3,
            },
            {
                textdiv:"equal",
                textclass: "content centertext",
                textdata: data.string.equalto
            },
            {
                textdiv:"inputbox",
                textclass: "content centertext",
                textdata: data.string.ten
            },
            {
                textdiv:"submit",
                textclass: "content centertext",
                textdata: data.string.submit
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "qimage",
                    imgclass: "relativecls img2",
                    imgid: 'q1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "downarrow",
                    imgclass: "relativecls img6",
                    imgid: 'arrowImg',
                    imgsrc: ""
                }

            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var time;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var scoreupdate = 1;
    var score = new NumberTemplate();
    score.init($total_page);

    var randomset = [];

    var optionval = [
                     [2,4,6,8,10,12,14],
                     [4,8,12,16,20,24,32],
                     [4,8,12,16,20,24,32],
                     [9,18,27,36,45,54,63,72],
                     [8,16,24,36,40,48,56],
                     [5,10,15,20,25,30,35,40,45,50],
                     [3,6,9,12,15,18,21],
                     [5,10,15,20,25,30,35,40]
                    ]

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "arrowImg", src: imgpath + "down.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg_exercise.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q1Img", src: imgpath + "cycle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q2Img", src: imgpath + "student.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3Img", src: imgpath + "cow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q4Img", src: imgpath + "balls.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q5Img", src: imgpath + "spiders.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q6Img", src: imgpath + "oranges.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q7Img", src: imgpath + "notes.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q8Img", src: imgpath + "hands.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "ex_instr", src: soundAsset + "ex_instr.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    // Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        put_image(content, countNext, preload);
        score.numberOfQuestions();
        switch (countNext) {
            case 0:
                // sound_player("ex_instr",0);
                randomset[0] = generaterandomnum([1,2,3,4,5,6,7,8],[0]);
                randomset[1] = generaterandomnum([1,2,3,4,5,6,7,8],randomset);
                randomset[2] = generaterandomnum([1,2,3,4,5,6,7,8],randomset);
                randomset[3] = generaterandomnum([1,2,3,4,5,6,7,8],randomset);
                setQuestion(randomset[0],"t2",true);
                shufflehint();
                checkans();
                break;
            case 1:
                setQuestion(randomset[0],"t2",false);
                shufflehint();
                checkans();
                texthighlight($board);
                break;
            case 2:
                var randval = randomset[0];
                setQuestion(randval,"t3",false);
                checkans1(randval);
                texthighlight($board);
                break;
            case 3:
                setQuestion(randomset[1],"t2",true);
                shufflehint();
                checkans();
                break;
            case 4:
                setQuestion(randomset[1],"t2",false);
                shufflehint();
                checkans();
                texthighlight($board);
                break;
            case 5:
                var randval = randomset[1];
                setQuestion(randval,"t3",false);
                checkans1(randval);
                texthighlight($board);
                break;
            case 6:
                setQuestion(randomset[2],"t2",true);
                shufflehint();
                checkans();
                break;
            case 7:
                setQuestion(randomset[2],"t2",false);
                shufflehint();
                checkans();
                texthighlight($board);
                break;
            case 8:
                var randval = randomset[2];
                setQuestion(randval,"t3",false);
                checkans1(randval);
                texthighlight($board);
                break;
            case 9:
                setQuestion(randomset[3],"t2",true);
                shufflehint();
                checkans();
                break;
            case 10:
                setQuestion(randomset[3],"t2",false);
                shufflehint();
                checkans();
                texthighlight($board);
                break;
            case 11:
                var randval = randomset[3];
                setQuestion(randval,"t3",false);
                checkans1(randval);
                texthighlight($board);
                break;
            default:
                navigationcontroller();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });
    //
    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }


    function shufflehint() {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option opt1","option opt2"]
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }


        function navigationcontroller(islastpageflag) {
            if (countNext == 0 && $total_page != 1) {
                $nextBtn.show(0);
            }
            else if ($total_page == 1) {
                $nextBtn.css('display', 'none');

                ole.footerNotificationHandler.lessonEndSetNotification();
            }
            else if (countNext > 0 && countNext < $total_page) {

                $nextBtn.show(0);
            }
            else if (countNext == $total_page - 2) {

                $nextBtn.css('display', 'none');
                // if lastpageflag is true
                // ole.footerNotificationHandler.pageEndSetNotification();
            }

        }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

  function checkans(){
      scoreupdate == 1;
        $(".option").click(function(){
            if($(this).attr("data-answer")=="correct"){
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                navigationcontroller(countNext, $total_page);
                scoreupdate==1?score.update(true):'';
                navigationcontroller();
            }
            else{
                scoreupdate = 0;
                $(this).addClass("wrongans avoid-clicks");
                play_correct_incorrect_sound(0);
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                score.update(false);

            }
      });
  }
    function checkans1(randval){
      var array1 = optionval[randval-1];
      var condition = optionval[randval-1].length-1;
      var i = 0;
      var scoreupdate = 1;
        $(".inputbox p").text("");
        $(".downarrow").click(function(){
            if(i>condition){
                i = 0;
            }
              $(".inputbox p").text(array1[i]);
              i++;
        });
        $(".submit").click(function(){
            if($(".inputbox p").text()==eval("data.string.e"+randval+"ans")){
                play_correct_incorrect_sound(1);
                $(".inputbox").addClass("correctans").removeClass("wrongans");
                scoreupdate==1?score.update(true):'';
                navigationcontroller(countNext, $total_page,true);
            }
            else{
                score.update(false);
                play_correct_incorrect_sound(0);
                $(".inputbox").addClass("wrongans");

            }
        })
    }
    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        return random;
    }
    function setQuestion(randomset,question2,first){
        $(".question p").html(eval("data.string.e"+(randomset)+"t1"));
        $(".question2 p").html(eval("data.string.e"+(randomset)+question2));
        if(first) {
            $(".opt1 p").html(eval("data.string.e" + (randomset) + "opt1"));
            $(".opt2 p").html(eval("data.string.e" + (randomset) + "opt2"));
        }
        else {
            $(".opt1 p").html(eval("data.string.e" + (randomset) + "opt3"));
            $(".opt2 p").html(eval("data.string.e" + (randomset) + "opt4"));
        }
        $(".qimage img").attr("src",preload.getResult("q"+randomset+"Img").src)
    }
});

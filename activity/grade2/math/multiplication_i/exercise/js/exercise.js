var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//exercise 1
		{
			contentblockadditionalclass:"creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata:data.string.exc_q1,
				datahighlightflag:true,
				datahighlightcustomclass:"qnReplace"
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer",
				fruitcontainer:[{
					fruitcontainerclass:"container c0",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh0"
					}]
				},{
					fruitcontainerclass:"container c1",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh1"
					},]
				},{
					fruitcontainerclass:"container c2",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh2"
					},]
				},{
					fruitcontainerclass:"container c3",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh3"
					},]
				},{
					fruitcontainerclass:"container c4",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh4"
					},]
				}]
			}],
			exetype1: [{
		    datahighlightflag: true,
		    datahighlightcustomclass: "clickplace",
		    exeoptions:[
		      {
		        optaddclass: "correct",
		      },
		      {
		    }]
			}]
		},
		//exercise 2
		{
			contentblockadditionalclass:"creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata:data.string.exc_q2,
				datahighlightflag:true,
				datahighlightcustomclass:"qnReplace"
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer",
				fruitcontainer:[{
					fruitcontainerclass:"container c0",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh0"
					}]
				},{
					fruitcontainerclass:"container c1",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh1"
					},]
				},{
					fruitcontainerclass:"container c2",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh2"
					},]
				},{
					fruitcontainerclass:"container c3",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh3"
					},]
				},{
					fruitcontainerclass:"container c4",
					fruitholder:[{
						fruitholderclass:"fruits_holder fh4"
					},]
				}]
			}],
			exetype1: [{
		    datahighlightflag: true,
		    datahighlightcustomclass: "clickplace",
		    exeoptions:[
		      {
		        optaddclass: "correct",
		      },
		      {
		    }]
			}]

		},
		// slide 3
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s3 a1",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s3",
				fruitcontainer:[{
					fruitcontainerclass:"container-s3 c0-s3",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s3 c1-s3",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass: "plus",
					imgid: 'plus',
					imgsrc: '',
				},{
					imgclass: "equals_s2",
					imgid: 'equal',
					imgsrc: '',
				}]
			}]
		},
		// slide 4
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont topeqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s4 a1",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s4",
				fruitcontainer:[{
					fruitcontainerclass:"container-s4 c0-s3",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s4 c1-s3",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "equals",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 5
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont topeqn",
				equation:[{
					textclass:"eqn",
				}]
				},{
				eqndivclass:"enqCont mideqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s5 btm_fig_box a1",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s4 btm_fig_box",
				fruitcontainer:[{
					fruitcontainerclass:"container-s4 c0-s4",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s4 c1-s4",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "equals_s5",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 6
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s3 a1",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn btn_dwn_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s6",
				fruitcontainer:[{
					fruitcontainerclass:"container-s6",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s6",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s6",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s6",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "plus_small",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "plus_small ps_sec",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "plus_small ps_thrd",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "equals_s6_fst",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 7
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont topeqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s7 a1",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s7",
				fruitcontainer:[{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "equals_s7",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 8
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ1qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont topeqn",
				equation:[{
					textclass:"eqn",
				}]
				},{
				eqndivclass:"enqCont mideqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s8 a1 btm_fig_box",
				imageblock:[{
					imgdivclass:"firstBox-s3",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton btn_up_1",
						imgid: 'button',
						imgsrc: '',
					}]
					}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s7 btm_fig_box",
				fruitcontainer:[{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s7",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "equals_s8",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 9
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ2qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s9 a1",
				imageblock:[{
					imgdivclass:"arrowbox",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_1",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s9 btn_dwn_1",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_2 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_2",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s9 btn_dwn_2",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_3 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_3",
						imgid: 'button',
						imgsrc: '',
					}]
				}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s9",
				fruitcontainer:[{
					fruitcontainerclass:"container-s9",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s9",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "plus_s9",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "equals_s6",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 10
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ2qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont mideqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s9 a1",
				imageblock:[{
					imgdivclass:"arrowbox",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_1",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s9 btn_dwn_1",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_2 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_2",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s9 btn_dwn_2",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_3 txt",
					imagestoshow:[{
						imgclass: "buton_s9 btn_up_3",
						imgid: 'button',
						imgsrc: '',
					}]
				}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s9 top_5",
				fruitcontainer:[{
					fruitcontainerclass:"container-s9",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s9",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "plus_s10",
						imgid: 'multiply',
						imgsrc: '',
					},{
						imgclass: "equals_s10",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 11
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ2qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont mideqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s9 long a1",
				imageblock:[{
					imgdivclass:"arrowbox",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_1",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s11 btn_dwn_1",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_2 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_2",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s11 btn_dwn_2",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_3 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_3",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s11 btn_dwn_3",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_4 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_4",
						imgid: 'button',
						imgsrc: '',
					}]
				}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s11",
				fruitcontainer:[{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh2"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "plus_s10 s11_fst",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "plus_s10 s11_sec",
						imgid: 'plus',
						imgsrc: '',
					},{
						imgclass: "equals_s11",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		},
		// slide 12
		{
			contentblockadditionalclass: "creambg",
			tpqndiv:[{
				extratxtdivclass:"qntxtdiv",
				textclass:"qntext",
				textdata: data.string.typ2qn
			}],
			extratxtdiv:[{
				textclass:"submit",
				textdata: data.string.submit
			}],
			eqndiv:[{
				eqndivclass:"enqCont mideqn",
				equation:[{
					textclass:"eqn",
				}]
			}],
			arrow_nav:[
			{
				arrowdivclass:"arrowBoxContainer-s9 long a1",
				imageblock:[{
					imgdivclass:"arrowbox",
					textclass:"box_1 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_1",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s11 btn_dwn_1",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_2 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_2",
						imgid: 'button',
						imgsrc: '',
					},{
						imgclass: "buton_dwn_s11 btn_dwn_2",
						imgid: 'button',
						imgsrc: '',
					}]
				},{
					imgdivclass:"arrowbox",
					textclass:"box_3 txt",
					imagestoshow:[{
						imgclass: "buton_s11 btn_up_3",
						imgid: 'button',
						imgsrc: '',
					}]
				}]
			}],
			basketsdiv:[{
				imagecontainerclass:"basketcontainer-s9 top_5",
				fruitcontainer:[{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh0"
					}]
				},{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh1"
					}]
				},{
					fruitcontainerclass:"container-s11",
					fruitholder:[{
						fruitholderclass:"fruits_holder-s3 fh2"
					}]
				}]
				}],
				imageblock:[{
					imagestoshow:[{
						imgclass: "plus_s10 s12_fst",
						imgid: 'multiply',
						imgsrc: '',
					},{
						imgclass: "equals_s11 eq_s12",
						imgid: 'equal',
						imgsrc: '',
					}]
				}]
		}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var containerCount = 0;
	var containerContentCount = 0;
	var f_number = [2,3,4,5,6,7];
	var randomFruit;
	var test = false;

	/*for limiting the questions to 10*/
	var $total_page = 12;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "car", src: imgpath+"car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron", src: imgpath+"iron.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(12);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		var updateScore = 0;
		switch (countNext) {
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
				put_image_sec(content, countNext);
			break;
			default:

		}
		// put_image_sec(content, countNext);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			// alert(parent);
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var count = 0;
		var cor_count = 0;
		var updateScore = 0;

		function appendImage(className, imageNumber){
			for(var i= 0;i<imageNumber; i++){
				$(className+i).css({"opacity" : "1"});
			}
		}
		function  addImagesToContainer(className,containeCount,imageNumber,classToAdd, fruitToShow){
			for(var i=0; i<containeCount;i++){
				for(var j=0; j<imageNumber;j++){
					$(className+i).append("<img class='"+classToAdd+"' src='"+preload.getResult(fruitToShow).src+"'/>");
				}
			}
		}
		function imageCssController(imageClass, imageNumber,imgPerContainer, left, top, width){
			var count_css = 0;
			var count_css_sec = 0;
			for(var i =0; i<imageNumber; i++){
				if(count_css == imgPerContainer){
					count_css = 0;
					left = 0;
				}
				$(imageClass[i]).css({
					"position" :"relative",
					"width" : 30+"%",
					"height" : 30+"%"
				});
				count_css++;
				left+=25;
			}
		}

		var count_1 = 0;
		var count_2 = 0;
		var count_3 = 0;
		var count_4 = 0;
		var wrongClick = false;
		$(".qntext").prepend(countNext+1+". ");
	 	switch(countNext){
		 case 0:
		 case 1:
				var c_number = [2,3,4,5];
			 	basket_count = c_number[Math.floor(Math.random()*c_number.length)];
			 	apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				if(basket_count == apple_count){
					apple_count+=1;
				}
				containerCount = basket_count;
				containerContentCount = apple_count;
				appendImage(".c",basket_count);
				//to append fruits to basket
				var fruitGroup = ["apple", "orange", "mango"];
			 	randomFruit = fruitGroup[Math.floor(Math.random()*fruitGroup.length)];
				var classname = "apple";
				addImagesToContainer(".fh",basket_count,apple_count,classname,randomFruit);
				// for css of appended images in the container(...,left,top,width)
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 5, 30);
				$(".container").append("<p class='contentCount'>"+apple_count+"</p>");
				// $(".qntext").prepend((countNext+1)+" ");
				switch (countNext) {
					case 0:
						$(".qnReplace").html(apple_count);
						$(".buttonsel:eq(0)").html(basket_count + " " + eval("data.string.times"));
						$(".buttonsel:eq(1)").html(apple_count + " "+ eval('data.string.times'));
						break;
						case 1:
						$(".qnReplace:eq(0)").html(apple_count);
						$(".qnReplace:eq(1)").html(basket_count);

						$(".buttonsel:eq(0)").html(apple_count  +" &#215; "+basket_count );
						$(".buttonsel:eq(1)").html(apple_count +" + "+ apple_count);
						break;
					default:
					}
					randomize(".optionsdiv");
			break;
			case 2:
			 	apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				containerContentCount = apple_count;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".container-s3").append("<p class='contentCount'>"+apple_count+"</p>");
				checkAns(2);
			break;
			case 3:
				apple_count = containerContentCount ;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".basketcontainer-s4").append("<p class='contentCount'>"+apple_count+data.string.exc_4+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation'>"+apple_count +" + "+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult_s3'>"+apple_count*2+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid'>"+" = "+"</p>");				// up down click
				var count_1 = 0;
				checkAns(2);
			break;
			case 4:
				apple_count = containerContentCount;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".basketcontainer-s4").append("<p class='contentCount'>"+apple_count+" &#215; 2"+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation'>"+apple_count +" + "+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult_s3'>"+apple_count*2+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid'>"+" = "+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'equation'>"+apple_count +data.string.exc_4+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'signMid'>"+" = "+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'eqnResult_s3'>"+apple_count*2+"</p>");
				// up down
				var count_1 = 0;
				checkAns(2);
			break;
			case 5:
			 	apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				containerContentCount = apple_count;
				var fruitGroup = ["apple", "orange", "mango"];
			 	randomFruit = fruitGroup[Math.floor(Math.random()*fruitGroup.length)];
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname,randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".container-s6").append("<p class='contentCount'>"+apple_count+"</p>");
				var count_1 = 0;
				checkAns(4);
			break;
			case 6:
				apple_count = containerContentCount;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".basketcontainer-s7").append("<p class='contentCount'>"+apple_count+data.string.exc_7+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation'>"+apple_count +" + "+apple_count +" + "+apple_count +" + "+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult_4pics'>"+apple_count*4+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid_4pics'>"+" = "+"</p>");
				var count_1 = 0;
				checkAns(4);
			break;
			case 7:
				apple_count = containerContentCount;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".basketcontainer-s7").append("<p class='contentCount'>"+apple_count+data.string.exc_7+"</p>");
				$(".container-s3").append("<p class='contentCount'>"+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation'>"+apple_count +" + "+apple_count+" + "+apple_count+" + "+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult'>"+apple_count*4+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid_4pics'>"+" = "+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'equation'>"+apple_count +data.string.exc_7+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'signMid_4pics'>"+" = "+"</p>");
				$(".enqCont:eq(1)").append("<p class = 'eqnResult'>"+apple_count*4+"</p>");
				var count_1 = 0;
				checkAns(4);
			break;
			case 8:
				apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				containerContentCount = apple_count;
				var fruitGroup = ["apple", "orange", "mango"];
			 	randomFruit = fruitGroup[Math.floor(Math.random()*fruitGroup.length)];
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname,randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".container-s6").append("<p class='contentCount'>"+apple_count+"</p>");
				checkAnsSec(0,2);
			break;
			case 9:
				apple_count = containerContentCount;
				var classname = "apple";
				addImagesToContainer(".fh",2,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				// $(".container-s6").append("<p class='contentCount'>"+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation_img'>"+apple_count +"</p>");
				$(".enqCont:eq(0)").append("<p class = 'sign_1'>"+ "+" +"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation_img sec'>"+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult_s10'>"+apple_count*2+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid_s10'>"+" = "+"</p>");
				checkAnsThird(0, 2);
			break;
			case 10:
				apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				containerContentCount = apple_count;
				var fruitGroup = ["apple", "orange", "mango"];
			 	randomFruit = fruitGroup[Math.floor(Math.random()*fruitGroup.length)];
				var classname = "apple";
				addImagesToContainer(".fh",3,apple_count,classname,randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".container-s6").append("<p class='contentCount'>"+apple_count+"</p>");
				checkAnsFth(0,4);
			break;
			case 11:
				apple_count = containerContentCount;
				var classname = "apple";
				addImagesToContainer(".fh",3,apple_count,classname, randomFruit);
				var apple = $(".apple");
				imageCssController(apple, apple.length,apple_count,0, 40, 30);
				$(".enqCont:eq(0)").append("<p class = 'equation_img s12_1'>"+apple_count +"</p>");
				$(".enqCont:eq(0)").append("<p class = 'sign_1 s12_2'>"+ "+" +"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation_img s12_3'>"+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'sign_1 s12_4'>"+ "+" +"</p>");
				$(".enqCont:eq(0)").append("<p class = 'equation_img s12_5'>"+apple_count+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'eqnResult_s10 s12_7'>"+apple_count*3+"</p>");
				$(".enqCont:eq(0)").append("<p class = 'signMid_s10 s12_6'>"+" = "+"</p>");
				checkAnsThird(0, 3);
			break;
	 	}

	//handling up and down arrow click
	var i = 0, n=0;
	$(".btn_up_1").on("click",function(){
		switch (countNext) {
			case 2:
			case 3:
			case 4:
			 	n = containerContentCount;
				if(i > 3){i = 0;}
				var optnArr = [0, n , n*2, n*3];
				handleclick($(this),optnArr[i]);
				i++;
				break;
			case 5:
			case 6:
			case 7:
				n = containerContentCount;
				if(i > 5){i = 0;}
				var optnArr = [0, n , n*2, n*3, n*4, n*5];
				handleclick($(this),optnArr[i]);
				i++;
			break;
			case 8:
			case 9:
			case 10:
			case 11:
				(count_1 < 10)?count_1++:true;
				handleclick($(this),count_1);
			break;
			default:
		}
	});
	$(".btn_up_2").on("click", function(){
		(count_2 < 10)?count_2++:true;
		handleclick($(this),count_2);
	});
	$(".btn_up_3").on("click", function(){
		switch (countNext) {
			case 8:
			case 9:
			case 11:
				n = containerContentCount;
				if(i > 4){i = 0;}
				var optnArr = [0, n , n*2, n*3, n*4];
				handleclick($(this),optnArr[i]);
				i++;
			break;
			case 10:
				(count_3 < 10)?count_3++: true;
				handleclick($(this),count_3);
			break;
			default:
		}
	});
	$(".btn_up_4").on("click", function(){
		n = containerContentCount;
		if(i > 5){i = 0;}
		var optnArr = [0, n , n*2, n*3, n*4, n*5];
		handleclick($(this),optnArr[i]);
		i++;
	});
	$(".btn_dwn_1").on("click",function(){
		switch (countNext) {
			case 2:
			case 3:
			case 4:
				var n = containerContentCount;
				if(i < 0){i = 3;}
				var optnArr = [0, n , n*2, n*3];
				handleclick($(this),optnArr[i]);
				i--;
			break;
			case 5:
			case 6:
			case 7:
				var n = containerContentCount;
				i--;
				if(i < 0){i = 3;}
				var optnArr = [0, n , n*2, n*3];
				handleclick($(this),optnArr[i]);
			break;
			case 8:
			case 9:
			case 10:
			case 11:
				(count_1 > 0)?count_1--:true;
				handleclick($(this),count_1);
			break;
			default:

		}
	});
	$(".btn_dwn_2").on("click",function(){
		(count_2 > 0)?count_2--: true;
		handleclick($(this),count_2);
	});
	$(".btn_dwn_3").on("click",function(){
		switch (countNext) {
			case 8:
			case 9:
			case 11:
				n = containerContentCount;
				i--;
				if(i < 0){i = 4;}
				var optnArr = [0, n , n*2, n*3, n*4];
				handleclick($(this),optnArr[i]);
			break;
			case 10:
				(count_3 > 0)?count_3--: true;
				handleclick($(this),count_3);
			break;
			default:
		}
	});
	$(".btn_dwn_4").on("click",function(){
		var n = containerContentCount;
		i--;
		if(i < 0){i = 4;}
		var optnArr = [0, n , n*2, n*3, n*4];
		handleclick($(this),optnArr[i]);
	});

function checkAns(multiplyBy){
	$(".submit").on("click",function(){
			var firstTxt = $(".box_1").parent().find("p").text();
			if(firstTxt == (apple_count*multiplyBy)){correctSec(".box_1", wrongClick)}else{wrongClick=true;incorrect(".box_1")}
		});
}
function checkAnsSec(updateScore, multiplyBy){
	$(".submit").on("click",function(){
			var firstTxt = $(".box_1").parent().find("p").text();
			var secondTxt = $(".box_2").parent().find("p").text();
			var thirdTxt = $(".box_3").parent().find("p").text();
			updateScore = 0;
			if(firstTxt == (apple_count) ){secCorrect(".box_1");updateScore+=1;}else{incorrect(".box_1");	updateScore-=3;}
			if(secondTxt == (apple_count) ){secCorrect(".box_2");updateScore+=1;}else{incorrect(".box_2");updateScore-=3;}
			if(thirdTxt == (apple_count*multiplyBy) ){secCorrect(".box_3");updateScore+=1;}else{incorrect(".box_3");updateScore-=3;}
			if(updateScore == 3){
				play_correct_incorrect_sound(1);
				$(".submit").css({"pointer-events" : "none"});
				$nextBtn.show(0);
			}
			else {
				play_correct_incorrect_sound(0);
				test = true;
			}
			!test?testin.update(true):testin.update(false);
		});
}
function checkAnsFth(updateScore, multiplyBy){
	$(".submit").on("click",function(){
		var firstTxt = $(".box_1").parent().find("p").text();
		var secondTxt = $(".box_2").parent().find("p").text();
		var thirdTxt = $(".box_3").parent().find("p").text();
		var fourthTxt = $(".box_4").parent().find("p").text();
		updateScore = 0;
		if(firstTxt == (apple_count)){secCorrect(".box_1");updateScore+=1;}else {incorrect(".box_1");updateScore-=4; }
		if(secondTxt == (apple_count)){secCorrect(".box_2");updateScore+=1;}else {incorrect(".box_2");updateScore-=4; }
		if(thirdTxt == (apple_count)){secCorrect(".box_3");updateScore+=1;}else {incorrect(".box_3");updateScore-=4;}
		if(fourthTxt == (apple_count*3)){secCorrect(".box_4");updateScore+=1;}else {incorrect(".box_4");updateScore-=4;}
		if(updateScore == 4){
			play_correct_incorrect_sound(1);
			$(".submit").css({"pointer-events" : "none"});
			$nextBtn.show(0);
		}
		else {
			play_correct_incorrect_sound(0);
			test = true;
		}
		!test?testin.update(true):testin.update(false);
	});
}
function checkAnsThird(updateScore, multiply){
	$(".submit").on("click", function(){
		var firstTxt = $(".box_1").parent().find("p").text();
		var secondTxt = $(".box_2").parent().find("p").text();
		var thirdTxt = $(".box_3").parent().find("p").text();
		var fourthTxt = $(".box_4").parent().find("p").text();
		updateScore = 0;
	 	if (firstTxt == apple_count && secondTxt!== apple_count) {
			secCorrect(".box_1");updateScore+=1;
			if((secondTxt ==((apple_count*multiply)/(firstTxt)))){
					secCorrect(".box_2");updateScore+=1;
			}else{incorrect(".box_2");updateScore-=3;}
			}
	 	else if (secondTxt == apple_count && firstTxt != apple_count) {
				secCorrect(".box_2");updateScore+=1;
			if((firstTxt ==((apple_count*multiply)/(secondTxt)))){
				secCorrect(".box_1");updateScore+=1;
			}else{incorrect(".box_1");updateScore-=3;}
		}
		else{
			incorrect(".box_1");
			incorrect(".box_2");
			updateScore-=3;
		}
		if(thirdTxt == (apple_count*multiply)){secCorrect(".box_3");updateScore+=1;}else{incorrect(".box_3");updateScore-=3;}
		if(updateScore == 3){
			play_correct_incorrect_sound(1);
			$(".submit").css({"pointer-events" : "none"});
			$nextBtn.show(0);
		}
		else {
			play_correct_incorrect_sound(0);
			test = true;
		}
		!test?testin.update(true):testin.update(false);
	});
}

	function handleclick(this_class, count, count2,count3){
		var box = $(this_class).parent().find("p");
		box.html(count);
	}
	function correctSec(boxClass,incorFlag){
				$(boxClass).css({"background" : "#00ff00"});
				$(boxClass).siblings().css({"pointer-events" : "none"});
				incorFlag?testin.update(false):testin.update(true);
				$(".submit").css({"pointer-events" : "none"});
				play_correct_incorrect_sound(1);
				$nextBtn.show(0);
	}
	function correct(boxClass, updScr, corCount){
				$(boxClass).css({"background" : "#00ff00"});
				$(boxClass).siblings().css({"pointer-events" : "none"});
				testin.update(true);
				$(".submit").css({"pointer-events" : "none"});
				play_correct_incorrect_sound(1);
				$nextBtn.show(0);
	}
	function incorrect(boxClass){
		$(boxClass).css({"background" : "#ff0000"});
		play_correct_incorrect_sound(0);
	}
	function secCorrect(boxClass, corCount){
		$(boxClass).css({"background" : "#00ff00", "border" : "4px solid #00ff00"});
		$(boxClass).siblings().css({"pointer-events" : "none"});
		play_correct_incorrect_sound(1);
	}

	//handle mcq
	$(".buttonsel").click(function(){
		$(this).removeClass('forhover');
			if(ansClicked == false){
				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("correct")){
					if(wrngClicked == false){
						testin.update(true);
					}
					play_correct_incorrect_sound(1);
					$(this).css({
						"background": "#bed62fff",
						"border":"5px solid #deef3c",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".corctopt").show(0);
					$('.buttonsel').removeClass('forhover forhoverimg');
					$('.buttonsel').removeClass('clock-hover');
					$nextBtn.show(0);
					// navigationcontroller();
					ansClicked = true;
					// if(countNext != $total_page)
					// $nextBtn.show(0);

				}
				else{
					testin.update(false);
					play_correct_incorrect_sound(0);
					$(this).css({
						"background":"#FF0000",
						"border":"5px solid #980000",
						"color":"#000",
						'pointer-events': 'none'
					});
					$(this).siblings(".wrngopt").show(0);
					wrngClicked = true;
				}
			}
		});

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
 	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

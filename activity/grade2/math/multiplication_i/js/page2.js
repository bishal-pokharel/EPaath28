var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content=[
	//cover
	{
		imageblock:[{
			imagestoshow:[{
				imgclass: "bg",
				imgid : 'bgCv',
				imgsrc: ""
			}]
		}],
		singletext:[{
			textclass: "diytxt",
			textdata: data.string.diy
		}]
	},
	//slide 0
	{
		contentblockadditionalclass:"whitebg",
		speechbox:[{
			speechbox:"speechbx",
			imgid:"speechbox",
			imgclass:'',
			imgsrc:'',
			textclass:"speechboxString",
			textdata:data.string.p2s1txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girl",
				imgid : 'girl',
				imgsrc: ""
			}]
		}]
	},
//slide 1
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.q1top
		},{
			textclass: "submit show_3",
			textdata: data.string.submit
		},{
			textclass: "btmtxt",
			textdata: data.string.btminstrn
		}
	],
	arrow_nav:[
		{
		arrowdivclass :"arrowdivclass",
		imageblock:[
			{
			imgdivclass:"leftbox",
			textclass: "box_1 txt",
			imagestoshow:[{
				imgclass: "buton btn_up_1",
				imgid: 'button',
				imgsrc: '',
		  }]
			},
			{
				imgdivclass:"show_2 midbox",
				textclass: "txt box_2",
				imagestoshow:[{
					imgclass: "buton btn_up_2",
					imgid: 'button',
					imgsrc: '',
				}]
			},
			{
				imgdivclass:"show_3 lastbox",
				textclass: "txt box_3",
				imagestoshow:[{
					imgclass: "buton btn_up_3",
					imgid: 'button',
					imgsrc: '',
				}]
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid : 'table',
			imgsrc: ""
		},{
			imgclass: "basket",
			imgid : 'fiveapple',
			imgsrc: ""
		},{
			imgclass: "basket",
			imgid : 'fiveapple',
			imgsrc: ""
		},{
			imgclass: "basket bas_sec",
			imgid : 'fiveapple',
			imgsrc: ""
		},{
			imgclass: "show_3 equal",
			imgid : 'equal',
			imgsrc: ""
		},{
			imgclass: "show_2 plus",
			imgid : 'plus',
			imgsrc: ""
		}
	]
	}]
},
//slide 2
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.q2top
		},{
			textclass: "show_3 submit_sec",
			textdata: data.string.submit
		},{
			textclass: "tryagain",
			textdata: data.string.tryagain
		},{
			textclass: "btmtxt",
			textdata: data.string.btminstrn_mult
		}
	],
	arrow_nav:[
		{
		arrowdivclass :"arrowdivclass_sec",
		imageblock:[
			{
			imgdivclass:"leftbox",
			textclass: "box_1 txt",
			imagestoshow:[{
				imgclass: "buton btn_up_1",
				imgid: 'button',
				imgsrc: '',
		  }]
			},
			{
				imgdivclass:"show_2 midbox",
				textclass: "txt box_2",
				imagestoshow:[{
					imgclass: "buton btn_up_2",
					imgid: 'button',
					imgsrc: '',
				}]
			},
			{
				imgdivclass:"show_3 lastbox",
				textclass: "txt box_3",
				imagestoshow:[{
					imgclass: "buton btn_up_3",
					imgid: 'button',
					imgsrc: '',
				}]
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid : 'table',
			imgsrc: ""
		},{
			imgclass: "show_2 plus top",
			imgid : 'multiply',
			imgsrc: ""
		},{
			imgclass: "show_3 equal top_sec",
			imgid : 'equal',
			imgsrc: ""
		},{
			imgclass: "basket",
			imgid : 'fiveapple',
			imgsrc: ""
		},{
			imgclass: "basket bas_sec",
			imgid : 'fiveapple',
			imgsrc: ""
		}]
	}]
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "table", src:imgpath+'table.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox", src:imgpath+'speechbuuble1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src:imgpath+'girl3.png', type: createjs.AbstractLoader.IMAGE},
			{id: "fiveapple", src:imgpath+'five_apple01.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bgCv", src:imgpath+'bg_01.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch (countNext) {
			case 2:
			case 3:
				put_image_sec(content, countNext);
			break;
			default:

		}
		$(".show_2, .show_3, .submit_sec, .tryagain").hide(0);
		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
				sound_player_nav("s2_p"+(countNext+1));
			break;
			case 2:
				$(".btmtxt").hide(0);
				sound_player("s2_p"+(countNext+1));
				var count_1=0;
				var count_2=0;
				var count_3=[0,5,10,15], i=0;;
				var navCount;
				$(".btn_up_1").click(function(){
					(count_1 < 5)?(count_1++):true;
					$(".box_1").html(count_1);
					handleClick(".btn_up_1", count_1, count_2, count_3, $(this));
				});
				$(".btn_up_2").click(function(){
					(count_2 < 5)?count_2++:true;
					$(".box_2").html(count_2);
					handleClick(".btn_up_2", count_1, count_2, count_3, $(this));
				});
				$(".btn_up_3").click(function(){
					if(i > 3){i = 0;}
					$(".box_3").html(count_3[i]);
					handleClick(".btn_up_3", count_1, count_2, count_3[i], $(this));
					i++;
				});
				$(".submit").on("click",function(){
					var firsttxt = $(".box_3").parent().find("p").text();
					if(firsttxt==10){
						$(".box_3").css({"background":"#98c02e", "border":"4px solid #ff0","pointer-events":"none", "color" : "#fff"});
						$(".btn_up_3").css({"pointer-events":"none"});
						play_correct_incorrect_sound(1);
					navigationcontroller();
				}else{
					play_correct_incorrect_sound(0);
					$(".box_3").css({"background":"#ff0000", "border":"4px solid #ff0000","pointer-events":"auto", "color" : "#fff"});
				}
				});
			break;
			case 3:
				sound_player("s2_p"+(countNext+1));
				var count_1=0;
				var count_2=0;
				var count_3=[0,5,10,15], i=0;
				var navCount_2;
				$(".btn_up_1").click(function(){
					(count_1 < 5)?count_1++:true;
					$(".box_1").html(count_1);
					handleClick(".btn_up_1", count_1, count_2, count_3, $(this));
				});
				$(".btn_up_2").click(function(){
					(count_2 < 2)?count_2++:true;
					$(".box_2").html(count_2);
					handleClick(".btn_up_2", count_1, count_2, count_3, $(this));
				});
				$(".btn_up_3").click(function(){
					if(i > 3){i = 0;}
					$(".box_3").html(count_3[i]);
					handleClick(".btn_up_3", count_1, count_2, count_3[i], $(this));
					i++;
				});
				$(".submit_sec").on("click",function(){
					var fthtxt = $(".box_3").parent().find("p").text();
					if(count_1 == 5 && fthtxt == 10){
						$(".box_3").css({"background":"#98c02e", "border":"4px solid #ff0","pointer-events":"none", "color" : "#fff"});
						$(".btn_up_3").css({"pointer-events":"none"});
							play_correct_incorrect_sound(1);
						navigationcontroller();
					}else{
						play_correct_incorrect_sound(0);
						$(".box_3").css({"background":"#ff0000", "border":"4px solid #ff0000","pointer-events":"auto", "color" : "#fff"});
					}
				});
			break;
		}
	}
	function handleClick(btnClass, count1, count2, count3, thisClass){
		switch (countNext) {
			case 2:
				if(count1 == 5){$(".show_2").show(0);}
				if(count2 == 5){$(".show_3").show(0);}
			break;
			case 3:
				if(count1 == 5){$(".show_2").show(0);}
				if(count2 == 2){$(".show_3").show(0);}
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

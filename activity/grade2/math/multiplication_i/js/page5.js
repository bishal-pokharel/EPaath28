var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content=[
//slide 1
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p5q1
		},{
			textclass: "submit",
			textdata: data.string.submit
		}
	],
	arrow_nav:[
		{
		imageblock:[
			{
			imgdivclass:"leftbox",
			textclass: "box_1 txt",
			imagestoshow:[{
				imgclass: "buton btn_up_1",
				imgid: 'button',
				imgsrc: '',
		  },{
				imgclass: "buton_dwn btn_dwn_1",
				imgid: 'button',
				imgsrc: '',
		  }]
			}
		]
	}],
	basketsdiv:[{
		basketcontainerclass:"basketcontainer",
		fruitcontainer:[{
			fruitcontainerclass:"container c0",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh0"
			}]
		},{
			fruitcontainerclass:"container c1",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh1"
			},]
		},{
			fruitcontainerclass:"container c2",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh2"
			},]
		},{
			fruitcontainerclass:"container c3",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh3"
			},]
		},{
			fruitcontainerclass:"container c4",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh4"
			},]
		}]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid: 'table',
			imgsrc: '',
		}]
	}]
},
//slide 2
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p5q2
		},{
			textclass: "submit",
			textdata: data.string.submit
		}
	],
	arrow_nav:[
		{
		imageblock:[
			{
			imgdivclass:"leftbox",
			textclass: "box_1 txt",
			imagestoshow:[{
				imgclass: "buton btn_up_1",
				imgid: 'button',
				imgsrc: '',
		  },{
				imgclass: "buton_dwn btn_dwn_1",
				imgid: 'button',
				imgsrc: '',
		  }]
			}
		]
	}],
	basketsdiv:[{
		basketcontainerclass:"basketcontainer",
		fruitcontainer:[{
			fruitcontainerclass:"container c0",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh0"
			}]
		},{
			fruitcontainerclass:"container c1",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh1"
			},]
		},{
			fruitcontainerclass:"container c2",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh2"
			},]
		},{
			fruitcontainerclass:"container c3",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh3"
			},]
		},{
			fruitcontainerclass:"container c4",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh4"
			},]
		}]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid: 'table',
			imgsrc: '',
		}]
	}]
},
//slide 3
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p5q3
		}
	],
	basketsdiv:[{
		basketcontainerclass:"basketcontainer",
		fruitcontainer:[{
			fruitcontainerclass:"container c0",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh0"
			}]
		},{
			fruitcontainerclass:"container c1",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh1"
			},]
		},{
			fruitcontainerclass:"container c2",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh2"
			},]
		},{
			fruitcontainerclass:"container c3",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh3"
			},]
		},{
			fruitcontainerclass:"container c4",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh4"
			},]
		}]
	}],
	mcqdiv:[{
		reqtxt:[
			{
				textclass: "opntxt",
				textdata: data.string.p5q3txt
			}
			,{
				textclass: "class1 option op1",
			},{
				textclass: "class2 option op2",
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid: 'table',
			imgsrc: '',
		}]
	}]
},
//slide 4
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p5q3
		}
	],
	basketsdiv:[{
		basketcontainerclass:"basketcontainer",
		fruitcontainer:[{
			fruitcontainerclass:"container c0",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh0"
			}]
		},{
			fruitcontainerclass:"container c1",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh1"
			},]
		},{
			fruitcontainerclass:"container c2",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh2"
			},]
		},{
			fruitcontainerclass:"container c3",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh3"
			},]
		},{
			fruitcontainerclass:"container c4",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh4"
			},]
		}]
	}],
	mcqdiv:[{
		reqtxt:[
			{
				textclass: "opntxt",
				textdata: data.string.p5q3txt
			}
			,{
				textclass: "class2 option op1",
			},{
				textclass: "class1 option op2",
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid: 'table',
			imgsrc: '',
		}]
	}]
},
//slide 5
{
	contentblockadditionalclass:"whitebg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.p5q3
		}
	],
	basketsdiv:[{
		basketcontainerclass:"basketcontainer",
		fruitcontainer:[{
			fruitcontainerclass:"container c0",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh0"
			}]
		},{
			fruitcontainerclass:"container c1",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh1"
			},]
		},{
			fruitcontainerclass:"container c2",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh2"
			},]
		},{
			fruitcontainerclass:"container c3",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh3"
			},]
		},{
			fruitcontainerclass:"container c4",
			fruitholder:[{
				fruitholderclass:"fruits_holder fh4"
			},]
		}]
	}],
	mcqdiv:[{
		reqtxt:[
			{
				textclass: "opntxt",
			}
			,{
				textclass: "class2 option op1",
			},{
				textclass: "class1 option op2",
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "table",
			imgid: 'table',
			imgsrc: '',
		}]
	}]
},
//slide 6
{
	contentblockadditionalclass:"whitebg",
	speechbox:[{
		speechbox:"speechbx",
		imgid:"speechbox",
		imgclass:'rotate',
		imgsrc:'',
		textclass:"speechboxString",
		textdata:data.string.p5qntxt
	}],
	singletext:[{
			textclass: "submitSec",
			textdata: data.string.practise
		},{
			textclass: "plainTxt",
			textdata: data.string.p5clicktxt
		}
	],
	imageblock:[{
		imagestoshow:[{
		imgid:"girl",
		imgclass:'girl',
		imgsrc:'',
		}]
	}]
},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var basket_count = 0;
	var apple_count = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "basket", src:imgpath+'basket.png', type: createjs.AbstractLoader.IMAGE},
			{id: "table", src:imgpath+'table.png', type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src:imgpath+'apple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src:imgpath+'girl4.png', type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox", src:imgpath+'speechbuuble1.png', type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p6_1", src: soundAsset+"s5_p6_1.ogg"},
			{id: "s5_p6_2", src: soundAsset+"s5_p6_2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			case 1:
			put_image_sec(content, countNext);
			break;
		}
		put_speechbox_image(content, countNext);
		put_image(content, countNext);


		switch(countNext){
			case 0:
				sound_player("s5_p"+(countNext+1));
				var b_number = [2,3,4,5];
				var f_number = [2,3,4,5,6,7];
			 	basket_count = b_number[Math.floor(Math.random()*b_number.length)];
			 	apple_count = f_number[Math.floor(Math.random()*f_number.length)];
				//for random number of baskets
				for(var i= 0;i<basket_count; i++){
					$(".c"+i).append("<img class='basket' src='"+preload.getResult("basket").src+"'/>");
					$(".basket").addClass("bskt"+i);
				}
				var basket = $(".basket");
				var left = 5;
				var top = 30;
				var width = 90;
				for(var i =0; i<basket.length; i++){
					if(basket.length > 1){
						left = 5;
					}
					$(basket[i]).css({
						"position" :"absolute",
						"left" : left+"%",
						"top" : top+"%",
						"width" : width+"%"
					});
					left+=20;
				}
				//for ramdom number of apples in each basket
				for(var i=0; i<basket_count;i++){
					for(var j=0; j<apple_count;j++){
						$(".fh"+i).append("<img class='apple' src='"+preload.getResult("apple").src+"'/>");
					}
				}
				var apple = $(".apple");
				var count_css = 0;
				// var left_apple = 0;
				var top_apple = 5;
				var width_apple = 30;
				for(var i =0; i<apple.length; i++){
					$(apple[i]).css({
						"position" :"relative",
						"width" : 30+"%",
						"height" : 55+"%"
					});
					count_css++;
					left_apple+=15;
				}
				var count_1 = 0;
				$(".btn_up_1").on("click",function(){
					(count_1 < 10)?count_1++:true;
					handleclick($(this),count_1);
				});
				$(".btn_dwn_1").on("click",function(){
					(count_1 > 0)?count_1--: true;
					handleclick($(this),count_1);
				});
				$(".submit").on("click",function(){
					var firsttxt = $(".box_1").parent().find("p").text();
					if(firsttxt == apple_count){correct(".box_1")}else(incorrect(".box_1"))
				});
			break;
			case 1:
				sound_player("s5_p"+(countNext+1));
				for(var i= 0;i<basket_count; i++){
					$(".c"+i).append("<img class='basket' src='"+preload.getResult("basket").src+"'/>");
					$(".basket").addClass("bskt"+i);
				}
				var basket = $(".basket");
				var left = 5;
				var top = 30;
				var width = 90;
				for(var i =0; i<basket.length; i++){
					if(basket.length > 1){
						left = 5;
					}
					$(basket[i]).css({
						"position" :"absolute",
						"left" : left+"%",
						"top" : top+"%",
						"width" : width+"%"
					});
					left+=20;
				}
				//for ramdom number of apples in each basket
				for(var i=0; i<basket_count;i++){
					for(var j=0; j<apple_count;j++){
						$(".fh"+i).append("<img class='apple' src='"+preload.getResult("apple").src+"'/>");
					}
					$(".c"+i).append("<p class='fruitsQty'>"+apple_count+"</p>");
				}
				var apple = $(".apple");
				var count_css = 0;
				var left_apple = 0;
				var top_apple = 5;
				var width_apple = 30;
				for(var i =0; i<apple.length; i++){
					$(apple[i]).css({
						"position" :"relative",
						"width" : 30+"%",
						"height" : 55+"%"
					});
					count_css++;
					left_apple+=15;
				}
				var count_1 = 0;
				$(".btn_up_1").on("click",function(){
					(count_1 < 10)?count_1++:true;
					handleclick($(this),count_1);
				});
				$(".btn_dwn_1").on("click",function(){
					(count_1 > 0)?count_1--: true;
					handleclick($(this),count_1);
				});
				$(".submit").on("click",function(){
					var firsttxt = $(".box_1").parent().find("p").text();
					if(firsttxt == basket_count){correct(".box_1")}else(incorrect(".box_1"))
				});
			break;
			case 2:
				sound_player("s5_p"+(countNext+1));
			case 3:
			case 4:
			for(var i= 0;i<basket_count; i++){
				$(".c"+i).append("<img class='basket' src='"+preload.getResult("basket").src+"'/>");
				$(".basket").addClass("bskt"+i);
			}
			var basket = $(".basket");
			var left = 5;
			var top = 30;
			var width = 90;
			for(var i =0; i<basket.length; i++){
				if(basket.length > 1){
					left = 5;
				}
				$(basket[i]).css({
					"position" :"absolute",
					"left" : left+"%",
					"top" : top+"%",
					"width" : width+"%"
				});
				left+=20;
			}
			//for ramdom number of apples in each basket
			for(var i=0; i<basket_count;i++){
				for(var j=0; j<apple_count;j++){
					$(".fh"+i).append("<img class='apple' src='"+preload.getResult("apple").src+"'/>");
				}
				$(".c"+i).append("<p class='fruitsQty'>"+apple_count+"</p>");
			}
			var apple = $(".apple");
			var count_css = 0;
			var left_apple = 0;
			var top_apple = 5;
			var width_apple = 30;
			for(var i =0; i<apple.length; i++){
				$(apple[i]).css({
					"position" :"relative",
					"width" : 30+"%",
					"height" : 55+"%"
				});
				count_css++;
				left_apple+=15;
			}
			// for options
			switch (countNext) {
				case 2:
					var op1 = [];
					for(var i= 0;i<basket_count; i++){
						op1[i] = apple_count;
					}
					$(".op1").html(op1.join("+"));
					$(".op2").html(apple_count+"+"+basket_count);
					$(".option").on('click',function(){
						if($(this).hasClass("class1")){correct($(this))}else{incorrect($(this))}
					});
				break;
				case 3:
					$(".op1").html(apple_count+" + "+basket_count);
					$(".op2").html(apple_count+" &#215; "+basket_count);
					$(".option").on('click',function(){
						if($(this).hasClass("class1")){correct($(this))}else{incorrect($(this))}
					});
				break;
				case 4:
					$(".op2").html(apple_count*basket_count);
					$(".op1").html(apple_count+basket_count);
					$(".opntxt").html(data.string.p5q3txt+apple_count+" &#215; "+basket_count+"=")
					$(".option").on('click',function(){
						if($(this).hasClass("class1")){correct($(this))}else{incorrect($(this))}
					});
				break;
			}
			break;
			case 5:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s5_p6_1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s5_p6_2");
					current_sound.play();
					current_sound.on('complete', function(){
					nav_button_controls(1000);
					});
				});
				$(".submitSec").on("click",function(){
						$prevBtn.css('display', 'none');
						$nextBtn.css('display', 'none');
						location.reload();
				});
			break;
			default:
				nav_button_controls(1000);
			break;
		}
	}
	function correct(boxClass){
		$(boxClass).css({"background":"#98c02e", "border":"4px solid #ff0","pointer-events":"none", "color" : "#fff"});
		$(boxClass).siblings().css({"pointer-events" : "none"});
			play_correct_incorrect_sound(1);
		$nextBtn.show(0);
	}
	function incorrect(boxClass){
		$(boxClass).css({"background" : "#ff0000", "pointer-events" : "none"});
			play_correct_incorrect_sound(0);
	}
	function handleclick(this_class, count, className){
		var box = $(this_class).parent().find("p");
		box.html(count);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content=[
//slide 1
{
	contentblockadditionalclass:"brownbg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.topinstrn
		},{
			textclass: "submit",
			textdata: data.string.submit
		}
	],
	arrow_nav:[
		{
		imageblock:[
			{
			imgdivclass:"leftbox",
			textclass: "box_1 txt",
			imagestoshow:[{
				imgclass: "buton btn_up_1",
				imgid: 'button',
				imgsrc: '',
		  },{
				imgclass: "buton_dwn btn_dwn_1",
				imgid: 'button',
				imgsrc: '',
		  }]
			},
			{
				imgdivclass:" midbox",
				textclass: "txt box_2",
				imagestoshow:[{
					imgclass: "buton btn_up_2",
					imgid: 'button',
					imgsrc: '',
				},{
					imgclass: "buton_dwn btn_dwn_2",
					imgid: 'button',
					imgsrc: '',
			  }]
			},
			{
				imgdivclass:"lastbox",
				textclass: "txt box_3",
				imagestoshow:[{
					imgclass: "buton btn_up_3",
					imgid: 'button',
					imgsrc: '',
				},{
					imgclass: "buton_dwn btn_dwn_3",
					imgid: 'button',
					imgsrc: '',
			  }]
			},
			{
				imgdivclass:"forthbox",
				textclass: "txt box_4",
				imagestoshow:[{
					imgclass: "buton btn_up_4",
					imgid: 'button',
					imgsrc: '',
				}]
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "plus fst",
			imgid : 'plus',
			imgsrc: ""
		},{
			imgclass: "plus sec",
			imgid : 'plus',
			imgsrc: ""
		},{
			imgclass: "equal",
			imgid : 'equal',
			imgsrc: ""
		},{
			imgclass: "bird b1",
			imgid : 'bird',
			imgsrc: ""
		},{
			imgclass: "bird b2",
			imgid : 'bird',
			imgsrc: ""
		},{
			imgclass: "bird b3",
			imgid : 'bird',
			imgsrc: ""
		}]
	}]
},
//slide 2
{
	contentblockadditionalclass:"brownbg",
		singletext:[
		{
			textclass: "toptxt",
			textdata: data.string.topinstrn
		},{
			textclass: "submit",
			textdata: data.string.submit
		},{
			textclass: "addntxt",
			textdata: data.string.addntxt
		}
	],
	arrow_nav:[
		{
		imageblock:[
			{
				imgdivclass:" midbox",
				textclass: "txt box_2",
				imagestoshow:[{
					imgclass: "buton btn_up_2",
					imgid: 'button',
					imgsrc: '',
				}
			]
			},
			{
				imgdivclass:"lastbox",
				textclass: "txt box_3",
				imagestoshow:[{
					imgclass: "buton btn_up_3",
					imgid: 'button',
					imgsrc: '',
				}
			]
			},
			{
				imgdivclass:"forthbox",
				textclass: "txt box_4",
				imagestoshow:[{
					imgclass: "buton btn_up_4",
					imgid: 'button',
					imgsrc: '',
				}
			]
			}
		]
	}],
	imageblock:[{
		imagestoshow:[{
			imgclass: "plus sec",
			imgid : 'multiply',
			imgsrc: ""
		},{
			imgclass: "equal",
			imgid : 'equal',
			imgsrc: ""
		},{
			imgclass: "bird2 b1s",
			imgid : 'bird',
			imgsrc: ""
		},{
			imgclass: "bird2 b2s",
			imgid : 'bird',
			imgsrc: ""
		},{
			imgclass: "bird2 b3s",
			imgid : 'bird',
			imgsrc: ""
		}]
	}]
},


];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: 'images/right.png', type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			{id: "button", src:imgpath+'button.png', type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src:imgpath+'plus.png', type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src:imgpath+'equal.png', type: createjs.AbstractLoader.IMAGE},
			{id: "multiply", src:imgpath+'multiple.png', type: createjs.AbstractLoader.IMAGE},
			{id: "bird", src:imgpath+'six_bird.png', type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image_sec(content, countNext);
		put_speechbox_image(content, countNext);
		var cor_ans_arr = [6,6,6,18];
		var corCount = 0;
		switch(countNext){
			case 0:
				sound_player("s3_p1");
				var count_1 = 0;
				var count_2	= 0;
				var count_3 = 0;
				var count_4 = [0,6,12,18], i=0;
				$(".btn_up_1").on("click",function(){
					(count_1 < 10)?count_1++:true;
					handleclick($(this),count_1);
				});
				$(".btn_dwn_1").on("click",function(){
					(count_1 > 0)?count_1--: true;
					handleclick($(this),count_1);
				});
				$(".btn_up_2").on("click",function(){
					(count_2 < 10)?count_2++:true;
					handleclick($(this),count_2);
				});
				$(".btn_dwn_2").on("click",function(){
					(count_2 > 0)?count_2--: true;
					handleclick($(this),count_2);
				});
				$(".btn_up_3").on("click",function(){
					(count_3 < 10)?count_3++:true;
					handleclick($(this),count_3, "btn_up_3");
				});
				$(".btn_dwn_3").on("click",function(){
					(count_3 > 0)?count_3--: true;
					handleclick($(this),count_3,"btn_dwn_3");
				});
				$(".btn_up_4").on("click",function(){
					if(i > 3){
						i = 0;
					}
					handleclick($(this),count_4[i], "btn_up_4");
					i++;
				});
				$(".btn_dwn_4").on("click",function(){
					if(i < 0){i = 3;}
					handleclick($(this),count_4[i], "btn_dwn_4");
					i--;
				});
				$(".submit").on("click",function(){
					var firsttxt = $(".box_1").parent().find("p").text();
					var sectxt = $(".box_2").parent().find("p").text();
					var thirdtxt = $(".box_3").parent().find("p").text();
					var fthtxt = $(".box_4").parent().find("p").text();
					corCount = 0;
					if(firsttxt == 6){
						corCount+=1;correct(".box_1");
					}else{
						corCount-=4;incorrect(".box_1");
					}
					if(sectxt == 6){
						corCount+=1;correct(".box_2");
					}else{
						corCount-=4;
						incorrect(".box_2");
					}
					if(thirdtxt == 6){
						corCount+=1;correct(".box_3");
					}else{
						corCount-=4;
						incorrect(".box_3");
					}
					if(fthtxt == 18){
						corCount+=1;correct(".box_4");
					}else{
						corCount-=4;
						incorrect(".box_4");
					}
					//for sounds and nav
					// console.log(corCount);
					if(corCount == 4){
						play_correct_incorrect_sound(1);
						$(".submit").css({"pointer-events" : "none"});
						$nextBtn.show(0);
					}
					else {
						play_correct_incorrect_sound(0);
					}
				});
			break;
			case 1:
				var count_2	= 0;
				var count_3 = [0,1,2,3,4,5], j=0;
				var count_4 = [0,6,12,18], i=0;
				var cor1=false,cor2=false,cor3=false;
				$(".btn_up_2").on("click",function(){
					if(i > 2){i = 0;}
					handleclick($(this),count_4[i], "btn_up_2");
					i++;
				});
				$(".btn_up_3").on("click",function(){
					if(j > 4){ j= 0;}
					handleclick($(this),count_3[j], "btn_up_3");
					j++;
				});
				$(".btn_up_4").on("click",function(){
					if(i > 3){i = 0;}
					handleclick($(this),count_4[i], "btn_up_4");
					i++;
				});
				$(".submit").on("click",function(){
					var sectxt = $(".box_2").parent().find("p").text();
					var thirdtxt = $(".box_3").parent().find("p").text();
					var fthtxt = $(".box_4").parent().find("p").text();
					if(sectxt == 6){
						correct(".box_2");
						cor1 = true;
					}else{
						incorrect(".box_2");
					}

					if(thirdtxt == 3){
						correct(".box_3");
						cor2 = true;
					}else{
						incorrect(".box_3");
					}

					if(fthtxt == 18){
						correct(".box_4");
						cor3 = true;
					}else{
						incorrect(".box_4");
					}
					if(cor1==true && cor2==true && cor3==true){
						play_correct_incorrect_sound(1);
							$(".submit").css({"pointer-events" : "none"});
							nav_button_controls(100);
					}else{
							play_correct_incorrect_sound(0);

					}
				});
			break;
		}
	}
	function correct(boxClass, wrongClk1, wrongClk2, wrongClk3, wrongClk4,cnt){
		$(boxClass).css({"background":"#98c02e", "border":"4px solid #ff0","pointer-events":"none", "color" : "#fff"});
		$(boxClass).siblings().css({"pointer-events" : "none"});
	}
	function incorrect(boxClass){
		$(boxClass).css({"border" : "4px solid #ff0000", "background":"#ff0000", "color":"#fff"});
	}
	function handleclick(this_class, count, className){
		var box = $(this_class).parent().find("p");
		box.html(count);
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		// current_sound.on('complete', function(){
		// 	if(next)
		// 	navigationcontroller();
		// });
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].arrow_nav[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].arrow_nav[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
    }

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if(countNext == 0)
		// navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	// coverpage
	{
		uppertextblock:[{
			textdata:data.lesson.chapter,
			textclass:'top_text'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg01',
				imgclass:'bg_full'
			}]
		}]
	},
	// slide1
	{
		extratextblock:[{
			textclass:'bigbx top pink'
		},{
			textdata:data.string.p1s1txt,
			textclass:'bigbx mid'
		},{
			textclass:'bigbx btm purple'
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s2txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'girl1',
				imgclass:'girl1'
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:"spImage",
			imgid:'speechbuuble1',
			imgsrc:'',
			textclass:"spTxt",
			textdata:data.string.txtbox,
		}]
	},
	// slide3
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s2txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s3txt_2,
			textclass:'slideinleft'
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s2txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3 visible'
			},{
				textdata:data.string.five,
				textclass:'five five4'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s4txt_2,
			textclass:'slideinleft'
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s5txt_2,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3 visible'
			},{
				textdata:data.string.five,
				textclass:'five five4 visible'
			},{
				textdata:data.string.five,
				textclass:'five five5'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s5txt_2,
			textclass:'slideinleft'
		}]
	},
	// slide6
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3 visible'
			},{
				textdata:data.string.five,
				textclass:'five five4 visible'
			},{
				textdata:data.string.five,
				textclass:'five five5 visible'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s5txt_2,
			textclass:'slideinleft'
		}]
	},
	// slide7
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3 visible'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s7txt,
			textclass:'slideinright bottom'
		},{
			textclass:'orangebox obFirst_1'
		}]
	},
	// slide8
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.five,
				textclass:'five five3 visible'
			},{
				textdata:data.string.ten,
				textclass:'five ten fadeIn'
			}]
		}],
		extratextblock:[{
			textdata:data.string.p1s7txt,
			textclass:'btmright bottom'
		},{
			textclass:'orangebox obsec'
		}]
	},
	// slide9
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.five,
					textclass:'five five3 visible'
				},{
					textdata:data.string.ten,
					textclass:'five ten visible'
				},{
				textdata:data.string.fif,
				textclass:'five fif fadeIn'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s10txt,
			textclass:'slideinright bottom'
		},{
			textclass:'orangebox obthird'
		}]
	},
	// slide11
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fvplus,
					textclass:'five ten fadeInLate'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s11txt,
			textclass:'slideinright  top_slide'
		},{
			textclass:'orangebox obFirst'
		}]
	},
	// slide12
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fv_2_plus,
					textclass:'five ten fadeIn'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s11txt,
			textclass:'topright top_slide'
		},{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		}]
	},
	// slide13
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fv_3_plus,
					textclass:'five ten fadeIn'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s11txt,
			textclass:'topright top_slide'
		},{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		},{
			textclass:'orangebox sec_ob_third'
		}]
	},
	// slide14
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fv_3_plus,
					textclass:'five ten visible'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s11txt,
			textclass:'topright top_slide'
		},{
			textdata:data.string.p1s10txt,
			textclass:'slideinright slide_right_anim bottom'
		},{
			textclass:'orangebox obthird'
		}]
	},
	// slide15
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fv_3_plus,
					textclass:'five ten visible'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s15txt,
			textclass:'slideinright top_slide'
		},{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		},{
			textclass:'orangebox sec_ob_third'
		}]
	},
	// slide16
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
					textdata:data.string.fv_3_plus,
					textclass:'five ten visible'
				}]
		}],
		extratextblock:[{
			textdata:data.string.p1s15txt,
			textclass:'topright top_slide'
		},{
			textdata:data.string.p1s16txt,
			textclass:'slideinright bottom'
		},{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		},{
			textclass:'orangebox sec_ob_third'
		},{
			textclass:'transparentTxt_sec transttxt1 fadeIn',
			textdata:data.string.one
		},{
			textclass:'transparentTxt_sec transttxt2 fadeIn_sec',
			textdata:data.string.two
		},{
			textclass:'transparentTxt_sec transttxt3 fadeInLate',
			textdata:data.string.three
		}]
	},
	// slide17
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
			}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				textdata:data.string.p1s17txt,
				textclass:'rght_txt'
			}]
		}],
		extratextblock:[{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		},{
			textclass:'orangebox sec_ob_third'
		},{
			textclass:'transparentTxt transttxt1',
			textdata:data.string.one
		},{
			textclass:'transparentTxt transttxt2',
			textdata:data.string.two
		},{
			textclass:'transparentTxt transttxt3',
			textdata:data.string.three
		}]
	},
	// slide18
	{
		contentblockadditionalclass: "purple",
		uppertextblock:[{
			textdata:data.string.p1s6txt_1,
			textclass:'top_text pink'
		}],
		imgcontainer :[{
			img_containerclass: "container",
		}],
		righttextdiv:[{
			number_container_class:"right_box",
			righttxt:[{
				datahighlightflag:'true',
				datahighlightcustomclass:"underline",
				textdata:data.string.p1s18txt,
				textclass:'rght_txt'
			}]
		}],
		extratextblock:[{
			textclass:'orangebox obFirst'
		},{
			textclass:'orangebox sec_ob_sec'
		},{
			textclass:'orangebox sec_ob_third'
		},{
			textclass:'transparentTxt transttxt1',
			textdata:data.string.one
		},{
			textclass:'transparentTxt transttxt2',
			textdata:data.string.two
		},{
			textclass:'transparentTxt transttxt3',
			textdata:data.string.three
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"girl1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbuuble1", src: imgpath+"speechbuuble1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "basket01", src: imgpath+'basket02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "candy", src: imgpath+'candy.png', type: createjs.AbstractLoader.IMAGE},
			{id: "flower02", src: imgpath+'flower02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "flower03", src: imgpath+'flower03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "flower04", src: imgpath+'flower03.png', type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+'line-png-28.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3_1", src: soundAsset+"s1_p3_1.ogg"},
			{id: "s1_p3_2", src: soundAsset+"s1_p3_2.ogg"},
			{id: "s1_p4_1", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p4_2", src: soundAsset+"s1_p4_2.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			// {id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			// {id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
			{id: "s1_p15", src: soundAsset+"s1_p15.ogg"},
			{id: "s1_p16", src: soundAsset+"s1_p16.ogg"},
			{id: "s1_p17", src: soundAsset+"s1_p17.ogg"},
			{id: "s1_p18", src: soundAsset+"s1_p18.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		function highlight_number(count){
			$(".number"+count).addClass("zoomin");
			$(".flower_"+count).addClass("zoomin");
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_"+count);
			current_sound.play();
			current_sound.on("complete",function(){
				count++;
				if(count < 4)
					highlight_number(count);
			});

		}
		var number = 0;

		function show_image(img_no, soundlet, soundSlide){
			$(img_no[number]).addClass("fadeIn");
			number++;
			if(number <= 4){
				setTimeout(function(){
					$(img_no[number]).addClass("fadeIn");
					show_image(img_no, soundlet, soundSlide);
				},600);
			}else{
				number =0;
				setTimeout(function(){
					$(".five"+(countNext)).addClass("fadeIn");
					setTimeout(function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play(soundlet);
						current_sound.play();
						current_sound.on('complete', function(){
							$(".slideinleft").addClass("slideLftAnim");
							setTimeout(function(){sound_player_nav(soundSlide);},2000);
						});
					},1000);
				},300);
		}
	}
		switch(countNext) {
			case 0:
				sound_player_nav("s1_p1");
        nav_button_controls(1000);
				break;
			case 1:
				sound_player_nav("s1_p2");
			break;
			case 2:
				sweetsContMang();
				$(".speechbox").hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p3_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".speechbox").fadeIn(300);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p3_2");
					current_sound.play();
					current_sound.on('complete',function(){
						nav_button_controls(100);
					});
				});
				break;
			case 3:
				var container = $(".container");
				var img_arr = [];
				for(i=0; i<=4; i++){
					container.append("<img class = 'sweet' src ='"+preload.getResult('candy').src+"'/>");

				}
				var sweets = $(".sweet");
				var left = 10;
				var width = 17;
				var top = 5;
				for(var i= 0; i< sweets.length; i++){
					$(sweets[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"opacity": "0",
						"position" :"absolute"
					});
					left+=15;
					$(sweets[i]).addClass("count"+[i]);
					img_arr[i] = $(".container img:eq("+i+")");
				}
				show_image(img_arr, "s1_p4_1", "s1_p4_2");
			break;
			case 4:
				var container = $(".container");
				var img_arr = [];
				for(i=0; i<=9; i++){
					container.append("<img class = 'sweet' src ='"+preload.getResult('candy').src+"'/>");

				}
				var sweets = $(".sweet");
				var left = 10;
				var width = 17;
				var top = 5;
				for(var i= 0; i< sweets.length; i++){
					if(i==5){
						top+=30;
						left = 10;
					}
					$(sweets[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=15;
					// $(sweets[i]).addClass("count"+[i]);
					$(".container img:eq("+(i+5)+")").css({"opacity" : "0"});
					img_arr[i] = $(".container img:eq("+(i+5)+")");
				}
				show_image(img_arr, "s1_p4_1", "s1_p5");
			break;
			case 5:
				var container = $(".container");
				var img_arr = [];
				for(i=0; i<=14; i++){
					container.append("<img class = 'sweet' src ='"+preload.getResult('candy').src+"'/>");

				}
				var sweets = $(".sweet");
				var left = 10;
				var width = 17;
				var top = 5;
				for(var i= 0; i< sweets.length; i++){
					if(i== 5 || i== 10){
						top+=30;
						left = 10;
					}
					$(sweets[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=15;
					// $(sweets[i]).addClass("count"+[i]);
					$(".container img:eq("+(i+10)+")").css({"opacity" : "0"});
					img_arr[i] = $(".container img:eq("+(i+10)+")");
				}
				show_image(img_arr, "s1_p4_1", "s1_p6");
			break;
			case 6:
				sweetsContMang();
				nav_button_controls(1000);
				sound_player_nav("s1_p7");
			break;
			case 7:
			case 9:
				sweetsContMang();
				setTimeout(function(){
					$(".slideinright").addClass("slideRightAnim");
					setTimeout(function(){sound_player_nav("s1_p"+(countNext+1));},2000);
				},2000);
			break;
			case 10:
			case 12:
				sweetsContMang();
				$(".slideinright").addClass("slideRightAnim");
				setTimeout(function(){sound_player_nav("s1_p"+(countNext+1));},2000);
			break;
			case 8:
			case 11:
			case 13:
				sweetsContMang();
				nav_button_controls(2000);
			break;
			case 13:
			case 14:
			case 15:
				sweetsContMang();
				$(".slideinright").addClass("slideRightAnim");
				setTimeout(function(){sound_player_nav("s1_p"+(countNext+1));},2000);
			break;
			case 16:
			case 17:
				sweetsContMang();
				$(".slideinright").addClass("slideRightAnim");
				setTimeout(function(){sound_player_nav("s1_p"+(countNext+1));},2000);
			break;
			case 18:
				sweetsContMang();
			break;
		}
		function sweetsContMang(){
			var container = $(".container");
			var img_arr = [];
			for(i=0; i<=14; i++){
				container.append("<img class = 'sweet' src ='"+preload.getResult('candy').src+"'/>");

			}
			var sweets = $(".sweet");
			var left = 10;
			var width = 17;
			var top = 3;
			for(var i= 0; i< sweets.length; i++){
				if(i== 5 || i== 10){
					top+=35;
					left = 10;
				}
				$(sweets[i]).css({
					"left" : left+"%",
					"width" :width+"%",
					"top" :top+"%",
					"position" :"absolute"
				});
				left+=15;
			}

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				// alert(image_src)
				// console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

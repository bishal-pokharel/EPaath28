var soundAsset = $ref+"/sounds/"+ $lang + "/";
var imgpath = $ref+"/images/";


var content=[
	//slide 0
	{
		contentblockadditionalclass:'greenbg',
		speechbox:[{
			speechbox:"speechbx",
			imgid:"speechbox",
			imgclass:'',
			imgsrc:'',
			textclass:"speechboxString",
			textdata:data.string.p4s1txt
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "girl",
				imgid : 'girl',
				imgsrc: ""
			}]
		}]
	},
	// slide 0
	{
		uppertextblock:[{
			textdata:data.string.p4s2txt,
			textclass:'top_text'
		}],
		imgcontainer :[{
			img_containerclass: "container",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog_bg',
				imgclass:'bg_full'
			}]
		}]
	},
	// slide1
	{
	contentblockadditionalclass: "purple",
	uppertextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'fadeInlater',
		textdata:data.string.p4s3top_txt,
		textclass:'top_text pink'
	}],
	imgcontainer :[{
		img_containerclass: "container",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'dog_bg',
			imgclass:'bg_full'
		}]
	}]
},
	// slide2
	{
	contentblockadditionalclass: "purple",
	uppertextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'show',
		textdata:data.string.p4s3top_txt,
		textclass:'top_text pink'
	}],
	imgcontainer :[{
		img_containerclass: "container",
		}],
		numberholder:[{}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog_bg',
				imgclass:'bg_full'
			}]
	}]
},
// slide3
	{
	contentblockadditionalclass: "purple",
	uppertextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'show',
		textdata:data.string.p4s3top_txt,
		textclass:'top_text pink'
	}],
	imgcontainer :[{
		img_containerclass: "container_sec",
	}],
	extratextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'bold',
		textdata:data.string.p4s3txt,
		textclass:'slide_right_box'
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'dog_bg',
			imgclass:'bg_full'
		}]
	}]
	},
	// slide4
	{
	contentblockadditionalclass: "purple",
	uppertextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'show',
		textdata:data.string.p4s3top_txt,
		textclass:'top_text pink'
	}],
	imgcontainer :[{
		img_containerclass: "container_sec",
		}],
		extratextblock:[{
			datahighlightflag: 'true',
			datahighlightcustomclass: "blue",
			textdata:data.string.p4s4txt,
			textclass:'slide_right_box'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog_bg',
				imgclass:'bg_full'
			}]
		}]
	},
	// slide5
	{
	contentblockadditionalclass: "purple",
	uppertextblock:[{
		datahighlightflag:'true',
		datahighlightcustomclass:'show',
		textdata:data.string.p4s3top_txt,
		textclass:'top_text pink'
	}],
	imgcontainer :[{
		img_containerclass: "container_sec",
		}],
		extratextblock:[{
			datahighlightflag: 'true',
			datahighlightcustomclass: "box",
			textdata:data.string.p4s5txt,
			textclass:'slide_right_box'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'dog_bg',
				imgclass:'bg_full'
			}]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "basket01", src: imgpath+'basket02.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dog_bg", src: imgpath+'bg_for_dog.png', type: createjs.AbstractLoader.IMAGE},
			{id: "line", src: imgpath+'line-png-28.png', type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+'dog.png', type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src:imgpath+'girl3.png', type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox", src:imgpath+'speechbuuble1.png', type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
			{id: "s4_p4", src: soundAsset+"s4_p4.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p6", src: soundAsset+"s4_p6.ogg"},
			{id: "s4_p7", src: soundAsset+"s4_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
					navigationcontroller();
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		function highlight_number(count){
			$(".number"+count).addClass("zoomin");
			$(".flower_"+count).addClass("zoomin");
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_"+count);
			current_sound.play();
			current_sound.on("complete",function(){
				count++;
				if(count < 4)
					highlight_number(count);
			});

		}
		var number = 0;
		var noCount = 0;
		var noCount_sec = 0;
		function show_number(number_no){
			$(number_no[number]).addClass("fadeIn");
			number++;
			if(number <= 6){
			setTimeout(function(){
				$(number_no[number]).addClass("fadeIn");
				show_number(number_no);
			},500);
			}else{
				number =0;
				setTimeout(function(){
					$(".five"+(countNext)).addClass("fadeIn");
			},300);
			}
		}
		function animateNumber(numCount){
			$(numCount[noCount]).animate({
					top: "0%",
					left: "+=5%"
			},1000);
			noCount++;
			if(noCount <= 6){
				setTimeout(function(){
				animateNumber(numCount);
			},1000);
			}
		}
		function animateNumber_top(numHolderNo){
			$(".numberholder").html(numHolderNo[noCount_sec]);
			noCount_sec++;
			if(noCount <= 7){
				setTimeout(function(){
				animateNumber_top(numHolderNo);
			},1900);
			}
		}
		switch(countNext) {
			case 0:
				sound_player_nav("s4_p"+(countNext+1));
			break;
			case 1:
				sound_player_nav("s4_p"+(countNext+1));
				var container = $(".container");
				var img_arr = [];
				var numArr = [];
				for(i=0; i<=6; i++){
					container.append("<img class = 'dog' src ='"+preload.getResult('dog').src+"'/>");

				}
				var dogs = $(".dog");
				var numbers = $(".number");
				var left = 20;
				var left_num = 24;
				var width = 17;
				var width_num = 10;
				var top = 20;
				var top_num = 49;
				for(var i= 0; i< dogs.length; i++){
					if(i == 3){
						top += 40;
						left = 10;
					}
					$(dogs[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=20;
				}
				// nav_button_controls(1000);
				break;
			case 2:
				sound_player_nav("s4_p"+(countNext+1));
				var four = 4;
				var container = $(".container");
				var img_arr = [];
				var numArr = [];
				for(i=0; i<=6; i++){
					container.append("<img class = 'dog' src ='"+preload.getResult('dog').src+"'/>");
					container.append("<p class = 'number'>"+four+"</p>");

				}
				var dogs = $(".dog");
				var numbers = $(".number");
				var left = 20;
				var left_num = 24;
				var width = 17;
				var width_num = 10;
				var top = 20;
				var top_num = 52;
				for(var i= 0; i< dogs.length; i++){
					if(i == 3){
						top += 40;
						left = 10;
					}
					$(dogs[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=20;
				}
				for(var i= 0; i< numbers.length; i++){
					if(i == 3){
						top_num += 40;
						left_num = 14;
					}
					$(numbers[i]).css({
						"left" : left_num+"%",
						"width" :width_num+"%",
						"top" :top_num+"%",
						"opacity" : "0",
						"position" :"absolute"
					});
					left_num+=20;
					img_arr[i] = ($(".container p:eq("+(i)+")"));
				}
				show_number(img_arr);
			break;
			case 3:
				// sound_player_nav("s4_p"+(countNext+1));
				var four = 4;
				var container = $(".container");
				var img_arr = [];
				var numArr = [];
				for(i=0; i<=6; i++){
					container.append("<img class = 'dog' src ='"+preload.getResult('dog').src+"'/>");
					container.append("<p class = 'number'>"+four+"</p>");

				}
				var dogs = $(".dog");
				var numbers = $(".number");
				var left = 20;
				var left_num = 24;
				var width = 17;
				var width_num = 10;
				var top = 20;
				var top_num = 52;
				for(var i= 0; i< dogs.length; i++){
					if(i == 3){
						top += 40;
						left = 10;
					}
					$(dogs[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=20;
				}
				for(var i= 0; i< numbers.length; i++){
					if(i == 3){
						top_num += 40;
						left_num = 14;
					}
					$(numbers[i]).css({
						"left" : left_num+"%",
						"width" :width_num+"%",
						"top" :top_num+"%",
						"position" :"absolute"
					});
					left_num+=20;
					img_arr[i] = ($(".container p:eq("+(i)+")"));
					$(".container p:eq("+(i)+")").addClass("number"+i)
				}
				//for number animation

				for(var i=0; i<img_arr.length;i++){
					numArr[i] = $(".number"+i);
					$(".number"+i).css({"opacity":"1"});
				}
				var topNum = ["4","4+4","4+4+4","4+4+4+4","4+4+4+4+4","4+4+4+4+4+4","4+4+4+4+4+4+4"];
				show_number(img_arr);
				// animateNumber(numArr);
				setTimeout(function(){
					animateNumber_top(topNum);
				},2000);
				setTimeout(function(){
                    nav_button_controls();
                },14000)
			break;
			case 4:
			case 5:
			case 6:
				setTimeout(function(){sound_player_nav("s4_p"+(countNext+1));},3000);
				var four = 4;
				var container = $(".container_sec");
				var img_arr = [];
				for(i=0; i<=6; i++){
					container.append("<img class = 'dog' src ='"+preload.getResult('dog').src+"'/>");
					container.append("<p class = 'number'>"+four+"</p>");

				}
				var dogs = $(".dog");
				var numbers = $(".number");
				var left = 20;
				var left_num = 24;
				var width = 17;
				var width_num = 10;
				var top = 31;
				var top_num = 51;
				for(var i= 0; i< dogs.length; i++){
					if(i == 3){
						top += 40;
						left = 10;
					}
					$(dogs[i]).css({
						"left" : left+"%",
						"width" :width+"%",
						"top" :top+"%",
						"position" :"absolute"
					});
					left+=20;
				}
				for(var i= 0; i< numbers.length; i++){
					if(i == 3){
						top_num += 40;
						left_num = 14;
					}
					$(numbers[i]).css({
						"left" : left_num+"%",
						"width" :width_num+"%",
						"top" :top_num+"%",
						"position" :"absolute"
					});
					left_num+=20;
					img_arr[i] = ($(".container p:eq("+(i)+")"));
				}
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

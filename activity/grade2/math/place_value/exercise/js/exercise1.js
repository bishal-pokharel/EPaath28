var imgpath = $ref+"/exercise/images/";
var imageArray = ["01","02","03","04","05","06","07","08","09","10"];
var prev_answers = [];
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
		//slide 0
		{
			additionalclasscontentblock:"pnkBg",
			imgexerciseblock: [
				{
					uppertextblock: [
						{
							textdata : data.string.e1text1,
							textclass: 'question_asked'
						}],
					imageblock : [{
						imagestoshow : [{
							imgclass : "block100 block100_0",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_1",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_2",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_3",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_4",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_5",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_6",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_7",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block100 block100_8",
							imgsrc : imgpath + "100BLOCKS.png",
						},
						{
							imgclass : "block10 block10_0",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_1",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_2",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_3",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_4",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_5",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_6",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_7",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block10 block10_8",
							imgsrc : imgpath + "10BLOCKS.png",
						},
						{
							imgclass : "block1 block1_0",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_1",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_2",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_3",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_4",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_5",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_6",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_7",
							imgsrc : imgpath + "1BLOCK.png",
						},
						{
							imgclass : "block1 block1_8",
							imgsrc : imgpath + "1BLOCK.png",
						}
						],
					}],

					answeroption: [{
						answer_outline: 'question_outline',
						hundreddata : data.string.p4text8,
						tendata : data.string.p4text7,
						onedata : data.string.p4text6,
					},
					{
						answer_outline: 'answer_outline answer_1',
						hundreddata : "a",
						tendata : "b",
						onedata : "c",
					},
					{
						answer_outline: 'answer_outline answer_2',
						hundreddata : "a",
						tendata : "b",
						onedata : "c",
					},
					{
						answer_outline: 'answer_outline answer_3',
						hundreddata : "a",
						tendata : "b",
						onedata : "c",
					},
					{
						answer_outline: 'answer_outline answer_4',
						hundreddata : "a",
						tendata : "b",
						onedata : "c",
					}],
				}
			]

		},
];


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 10;
	loadTimelineProgress($total_page,countNext+1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "sound_1", src: soundAsset + "ex.ogg"}
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ?  $nextBtn.show(0): "";
        });
    }

    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }


	var rhino = new EggTemplate();

	rhino.init($total_page);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);
        countNext==0?sound_player("sound_1",false):"";
		/*generate question no at the beginning of question*/
		switch(countNext) {
			default:
				var  random_number = ole.getRandom(4, 999, 100);
				for( var i=1; i<4; i++){
					if(random_number[0]==random_number[i]){
						random_number[i]++;
					}
				}
				random_number[0] = check_prev_ans(random_number[0]);
				prev_answers.push(random_number[0]);
				var digit100 = [], digit10 = [], digit1 = [];
				for(var i=0; i<4; i++) {
					digit100[i] = Math.floor(random_number[i]/100);
					digit10[i] = Math.floor((random_number[i]-100*digit100[i])/10);
					digit1[i] = random_number[i]%10;
				}
				digit100[1] = digit100[0];
				digit10[1] = digit10[0];
				if(digit1[1] == digit1[0]){
					digit1[1]++;
					if(digit1[1]==10){
						digit1[1] = ole.getRandom(1,8,0);
					}
				};
				digit10[2] = digit10[0];
				if((digit1[2] == digit1[0] || digit1[2] == digit1[1]) && digit100[2] == digit100[0]){
					digit1[2]++;
					if(digit1[2]==10){
						digit1[2] = ole.getRandom(1,8,0);
					}
				};
				digit1[3] = digit1[0];
				if((digit10[3] == digit10[0] || digit10[3] == digit10[1] || digit10[3] == digit10[2]) && digit100[3] == digit100[0]){
					digit10[3]++;
					if(digit10[3]==10){
						digit10[3] = ole.getRandom(1,8,0);
					}
				};
				var answer_order = sandy_random_display(4);
				for(var i = 0; i<4; i++){
					/* set denominator as element from frac_random */
					var class_name_100 = '.answer_'+(answer_order[i])+'>.answer_begin';
					var class_name_10 = '.answer_'+(answer_order[i])+'>.answer_mid';
					var class_name_1 = '.answer_'+(answer_order[i])+'>.answer_end';
					if(i==0){
						$('.answer_'+(answer_order[i])).addClass('class1');
					}
					$(class_name_100).html(digit100[i]);
					$(class_name_10).html(digit10[i]);
					$(class_name_1).html(digit1[i]);
				}
				// Hide images
				$(".block1").hide(0);
				$(".block10").hide(0);
				$(".block100").hide(0);
 				// Show images according to input
				for (var i = 0; i < digit100[0]; i++){
					var strStart = $(".block100_"+i);
 					strStart.show(0);
				}
				for (var i = 0; i < digit10[0]; i++){
					var strStart = $(".block10_"+i);
 					strStart.show(0);
				}
				for (var i = 0; i < digit1[0]; i++){
					var strStart = $(".block1_"+i);
 					strStart.show(0);
				}
				break;
		}


		var ansClicked = false;
		var wrngClicked = false;

		$(".answer_outline").click(function(){
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						$(this).addClass('correct_answer');
						$(".answer_outline").css('pointer-events', 'none');
						$(this).children(".corctopt").show(0);
						current_sound.stop();
						play_correct_incorrect_sound(1);
						if(wrngClicked == false){
							rhino.update(true);
						}
						ansClicked = true;
						if(countNext < 10){
							$nextBtn.show(0);
						}
					}
					else{
						rhino.update(false);
						$(this).children(".wrngopt").show(0);
                        current_sound.stop();
						play_correct_incorrect_sound(0);
						$(this).addClass('incorrect_answer');
						$(this).css('pointer-events', 'none');
						wrngClicked = true;
					}
				}
			});

		/*for randomizing the options*/
		/*var parent = $(".optionsdiv_img");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }*/

	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

	}


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		if(countNext<10){
			templateCaller();
		} else{
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function check_prev_ans(answer){
		for(var m=0; m<prev_answers.length; m++){
			if(answer == prev_answers[m]){
				answer = ole.getRandom(1,999,100);
				answer = check_prev_ans(answer);
			}
		}
		return answer;
	}

	/*  function used to generate numbers from 1 to range in random order and store it in array
	 *	This function pushes all the numbers upto range -1 in an array, generates the random no upto range-1
	 *  moves the randomly generated index from that array to new array and repeats the process till every number < range is generated	*/
	function sandy_random_display(range){
		var random_arr = [];
		var return_arr = [];
		var random_max = range;
		for(var i=1; i<=range; i++){
			random_arr.push(i);
		}

		while(random_max)
		{
			var index = ole.getRandom(1, random_max-1, 0);
			return_arr.push(random_arr[index]);
			random_arr.splice((index),1);
			random_max--;
		}
		return return_arr;
	}
/*=====  End of Templates Controller Block  ======*/
});

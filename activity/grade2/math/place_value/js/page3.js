var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content = [

//slide 0
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text1,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		},
		{	//07, 08 and 09 converges or represent single apple at end
			imgclass : "floorappleimage07 roll",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage08 hit",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage09",
			imgsrc : imgpath + "redapple01.png",
		},
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
	}],

},

//slide 1
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text1,
	},
	{
		textdata : data.string.p3text2,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari03.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage10",
			imgsrc : imgpath + "redapple01.png",
		},
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
	}],

},

//slide 2
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text1,
	},
	{
		textdata : data.string.p3text2,
	},
	{
		textdata : data.string.p3text3,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari-cool.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage10",
			imgsrc : imgpath + "redapple01.png",
		},
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box',
		tensdata : '',
		hastensdata : true,
		tensclass : 'tens-box',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		buttondata : data.string.check,
		buttonclass : 'check-button',
	}],

	lowertextblockadditionalclass : "check-answer",

	lowertextblock : [{
		textdata : data.string.p3text4,
		textclass : 'incorrect-answer'
	}, {
		textdata : data.string.p3text5,
		textclass : 'correct-answer'
	}]

},

//slide 3
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text6,
	},
	{
		textdata : data.string.p3text7,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari01.png",
		}, {
			imgclass : "floorbasketimage00",
			imgsrc : imgpath + "apple_basket.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '0',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '1',

		tensclass : 'tens-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
	}],

},

//slide 4
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text6,
	},
	{
		textdata : data.string.p3text7,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage disappear",
			imgsrc : imgpath + "sundari01.png",
		}, {
			imgclass : "floorbasketimage01 slide-right",
			imgsrc : imgpath + "apple_basket.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '0',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '1',
		tensclass : 'tens-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
	}],

},

//slide 5
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text8,
	},
	{
		textdata : data.string.p3text9,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "floorbasketimage01",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage02",
			imgsrc : imgpath + "apple_basket.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '0',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '2',
		tensclass : 'tens-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
	}],

},

//slide 6
{
	hasheaderblock : false,

	contentblockadditionalclass : 'pnkBg',

	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text8,
	},
	{
		textdata : data.string.p3text10,
	},
	{
		textdata : data.string.p3text11,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "floorbasketimage01",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage02",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage03",
			imgsrc : imgpath + "apple_basket.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '0',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '3',
		tensclass : 'tens-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
	}],

},

//slide 7
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text12,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "floorbasketimage01",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage02",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage03",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage04",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage05",
			imgsrc : imgpath + "apple_basket.png",
		},
		 {
			imgclass : "topappleimage00",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "topappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "topappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box',
		hastensdata : true,
		tensdata : '',
		tensclass : 'tens-box',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		buttondata : data.string.check,
		buttonclass : 'check-button',
	}],

	lowertextblockadditionalclass : "check-answer",

	lowertextblock : [{
		textdata : data.string.p3text4,
		textclass : 'incorrect-answer'
	}, {
		textdata : data.string.p3text5,
		textclass : 'correct-answer'
	}],

},

//slide 8
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',


	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p3text13,
	},
	{
		textdata : data.string.p3text14,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "floorbasketimage01",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage02",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage03",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage04",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage05",
			imgsrc : imgpath + "apple_basket.png",
		},
		 {
			imgclass : "topappleimage00",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "topappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "topappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		],
	}],

	digitblock : [{
		onesdata : '3',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '5',
		tensclass : 'tens-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		buttondata : data.string.list,
		buttonclass : 'check-button',
	}],

},

//slide 9
{
	hasheaderblock : false,
	contentblocknocenteradjust: true,
	contentblockadditionalclass : 'pnkBg',

	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [],

	imageblock : [],

	// for the table in the end
	bigtableblock : [{
			bigtableclass: "ones-table",
			tenstabledata: [
			{
				digitnumber : '0',
				digitname : data.string.list0,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '1',
				digitname : data.string.list1,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '2',
				digitname : data.string.list2,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '3',
				digitname : data.string.list3,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '4',
				digitname : data.string.list4,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '5',
				digitname : data.string.list5,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '6',
				digitname : data.string.list6,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '7',
				digitname : data.string.list7,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '8',
				digitname : data.string.list8,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '9',
				digitname : data.string.list9,
				digitclass : 'digit-tablecell'
	}]
	},
	{
			bigtableclass: "tens-table",
			tenstabledata: [
			{
				digitnumber : '10',
				digitname : data.string.list10,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '11',
				digitname : data.string.list11,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '12',
				digitname : data.string.list12,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '13',
				digitname : data.string.list13,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '14',
				digitname : data.string.list14,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '15',
				digitname : data.string.list15,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '16',
				digitname : data.string.list16,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '17',
				digitname : data.string.list17,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '18',
				digitname : data.string.list18,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '19',
				digitname : data.string.list19,
				digitclass : 'digit-tablecell'
			}]
	},
	{
			bigtableclass: "twentys-table",
			tenstabledata: [
			{
				digitnumber : '20',
				digitname : data.string.list20,
				digitclass : 'digit-tablecell'
			},
			{
				digitnumber : '30',
				digitname : data.string.list30,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '40',
				digitname : data.string.list40,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '50',
				digitname : data.string.list50,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '60',
				digitname : data.string.list60,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '70',
				digitname : data.string.list70,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '80',
				digitname : data.string.list80,
				digitclass : 'digit-tablecell'
			}
			,
			{
				digitnumber : '90',
				digitname : data.string.list90,
				digitclass : 'digit-tablecell'
			}
			]

	},
	],
			uppertextblockadditionalclass:'toptext',
			uppertextblock : [
			{
			textdata : data.string.p3text15,
			}
			],
},];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var mistakes = 0;
	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var dropedImgCount = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			// {id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			// {id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			// {id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			// {id: "sound_8", src: soundAsset+"s3_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("digitcontent", $("#digitcontent-partial").html());
	Handlebars.registerPartial("bigtablecontent", $("#bigtablecontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext == 0 || countNext == 2 || countNext == 4 || countNext == 7 || countNext == 8) {
			$nextBtn.hide(0);
			$prevBtn.show(0);
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		/* to allow only single numeric character in each digit box */
		input_box('.ones-box', 1, '.check-button');
		input_box('.tens-box', 1, '.check-button');

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

			$nextBtn.hide(0);
			$prevBtn.hide(0);
		//count number of mistakes
		switch (countNext) {
		case 0:
			sound_player_nav("sound_"+countNext);
			$('.ones-box1').prop("disabled", true);
			// Timing function to make apple appear as one after falling and hitting
			setTimeout(function() {
    			$('.floorappleimage08').hide(0);
    			$('.sideimage').attr('src', imgpath + "sundari-crying.png");
				}, 480);
			setTimeout(function() {
    			$('.floorappleimage09').show(0);
    			$('.sideimage').attr('src', imgpath + "sundari-upset.png");
    			// $nextBtn.show(0);
				}, 2000);
			break;
		case 1:
			sound_player_nav("sound_"+countNext);
			$('.ones-box1').prop("disabled", true);
			break;
		case 2:
			sound_player("sound_"+countNext);
			mistakes = 0;
			$('.check-button').on("click", function() {
				var ones_answer = $('.ones-box').val();
				var tens_answer = $('.tens-box').val();
				$('.incorrect-answer').hide(0);
				$('.correct-answer').hide(0);
				$nextBtn.hide(0);
				// Check if mistake is done 3 times
				if (mistakes >= 2) {
					$('.tens-box').val('1');
					$('.ones-box').val('0');
					$('.sideimage').attr('src', imgpath + "sundari-smirk.png");
					$('.check-button').css("background", '#93C496');
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					$nextBtn.show(0);
				} else if (tens_answer == '' || ones_answer==''){
					return true;
				}
				// Check if answer is correct
				else if (tens_answer == 1 && ones_answer == 0) {
					$('.sideimage').attr('src', imgpath + "sundari-cool.png");
					$('.check-button').css("background", '#93C496');
					$('.correct-answer').show(0);
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					// $(".digit-box").css({
					// 	"background":"#98c02e",
					// 	"border":"4px solid #ff0 !important",
					// 	"color":"#fff"
					// });
					$nextBtn.show(0);
					play_correct_incorrect_sound(1);
				}
				// Check if answer is correct
				else {
					$('.sideimage').attr('src', imgpath + "sundari05.png");
					$('.check-button').css("background", '#E06666');
					$('.incorrect-answer').show(0);
					mistakes++;
					play_correct_incorrect_sound(0);
				}
			});
			break;
		case 3:
			sound_player_nav("sound_"+countNext);
			$('.tens-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			break;
		case 4:
			sound_player_nav("sound_"+countNext);
			$('.tens-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			setTimeout(function() {
    			$('.sideimage').hide(0);
    			$nextBtn.show(0);
				}, 1500);
			break;
		case 5:
			sound_player_nav("sound_"+countNext);
			$('.tens-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			break;
		case 6:
			sound_player_nav("sound_"+countNext);
			$('.tens-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			break;
		case 7:
			sound_player("sound_"+countNext);
			// logic similar to case 2
			mistakes = 0;
			$('.check-button').on("click", function() {
				var ones_answer = $('.ones-box').val();
				var tens_answer = $('.tens-box').val();
				$('.incorrect-answer').hide(0);
				$('.correct-answer').hide(0);
				$nextBtn.hide(0);
				if (mistakes >= 2) {
					$('.tens-box').val('5');
					$('.ones-box').val('3');
					$('.sideimage').attr('src', imgpath + "sundari-smirk.png");
					$('.check-button').css("background", '#93C496');
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					$nextBtn.show(0);
				} else if (tens_answer == '' || ones_answer==''){
					return true;
				} else if (tens_answer == 5 && ones_answer == 3) {
					$('.sideimage').attr('src', imgpath + "sundari02.png");
					$('.check-button').css("background", '#93C496');
					$('.correct-answer').show(0);
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					$nextBtn.show(0);
					play_correct_incorrect_sound(1);
				} else {
					$('.sideimage').attr('src', imgpath + "sundari05.png");
					$('.check-button').css("background", '#E06666');
					$('.incorrect-answer').show(0);
					mistakes++;
					play_correct_incorrect_sound(0);
				}
			});
			break;
		case 8:
			sound_player("sound_"+countNext);
			$nextBtn.hide(0);
			$('.tens-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			$('.check-button').on("click", function() {
				countNext++;
				templateCaller();
			});
			break;
		case 9:
			break;
		default:
			$('.digit-box').prop("disabled", true);
			break;
		}
	}


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(100);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// document.addEventListener('xmlLoad', function(e) {
	total_page = content.length;
	// templateCaller();
	// });
		/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

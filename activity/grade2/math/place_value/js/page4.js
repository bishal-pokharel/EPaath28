var soundAsset = $ref+"/sounds/"+ $lang + "/";

//array of image
var randomImageNumeral = ole.getRandom(1, 3, 0);

var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref + "/images/";

var content = [

//slide 0
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',

	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p4text1,
	},
	{
		textdata : data.string.p4text2,
	},
	{
		textdata : data.string.p4text3,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "drumapple00",
			imgsrc : imgpath + "apple_drum.png",
		}
		],
	}],
	digitblockadditionalclass:'digitblock1',
	digitblock : [{
		onesdata : '0',
		onesclass : 'ones-box1',
		hastensdata : true,
		tensdata : '0',
		tensclass : 'tens-box1',
		hashundredsdata : true,
		hundredsdata : '1',
		hundredsclass : 'hundreds-box1',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		hundredslabel : data.string.p4text8,
		hundredslabelclass : 'hundreds-label-box'
	}],

},

//slide 1
{
	hasheaderblock : false,
	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p4text4,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "drumapple01",
			imgsrc : imgpath + "apple_drum.png",
		},
		{
			imgclass : "drumapple02",
			imgsrc : imgpath + "apple_drum.png",
		},
		{
			imgclass : "drumapple03",
			imgsrc : imgpath + "apple_drum.png",
		},
		{
			imgclass : "floorbasketimage00",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage01",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage02",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage03",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage04",
			imgsrc : imgpath + "apple_basket.png",
		}, {
			imgclass : "floorbasketimage05",
			imgsrc : imgpath + "apple_basket.png",
		},
		{
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage07",
			imgsrc : imgpath + "redapple01.png",
		},
		{
			imgclass : "floorappleimage08",
			imgsrc : imgpath + "greenapple01.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box',
		hastensdata : true,
		tensdata : '',
		tensclass : 'tens-box',
		hashundredsdata : true,
		hundredsdata : '',
		hundredsclass : 'hundreds-box',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		hundredslabel : data.string.p4text8,
		hundredslabelclass : 'hundreds-label-box',
		buttondata : data.string.check,
		buttonclass : 'check-button-bottom',
	}],

	lowertextblockadditionalclass : "check-answer",

	lowertextblock : [{
		textdata : data.string.p3text4,
		textclass : 'incorrect-answer'
	}, {
		textdata : data.string.p4text5_1,
		textclass : 'correct-answer'
	}]

},

//slide 2
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',

	contentblocknocenteradjust: true,
	uppertextblockadditionalclass : "baseblock",

	uppertextblock : [
	{
		contentblocknocenteradjust: false,
		textdata : ' '	//empty string used for newline
	},
	{
		contentblocknocenteradjust: false,
		textdata : data.string.p4text5,
	}],

	imageblock : [
	{
		imagestoshow : [{
			imgclass : "transitionapple apple-fade",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "transitionbox block-fade",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "transition-arrow01",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow02",
			imgsrc : imgpath + "arrow-right.png",
		}
		],
	}],

	lowertextblockadditionalclass : "label-transition",

	lowertextblock : [{
		textdata : data.string.p4text6,
		textclass : 'ones-transition'
	}]

},

//slide 3
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',

	contentblocknocenteradjust: true,
	uppertextblockadditionalclass : "baseblock",

	uppertextblock : [
	{
		contentblocknocenteradjust: false,
		textdata : ' '	//empty string used for newline
	},
	{
		contentblocknocenteradjust: false,
		textdata : data.string.p4text5,
	}],

	imageblock : [
	{
		imagestoshow : [{
			imgclass : "transitionapple",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "transitionbox",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "transitionbasket apple-fade",
			imgsrc : imgpath + "apple_basket.png",
		},
		{
			imgclass : "transitionblock10 block10-fade",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "transition-arrow01v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow02v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow03",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow04",
			imgsrc : imgpath + "arrow-right.png",
		}
		],
	}],

	lowertextblockadditionalclass : "label-transition",

	lowertextblock : [{
		textdata : data.string.p4text6,
		textclass : 'ones-transitionv'
	},
	{
		textdata : data.string.p4text7,
		textclass : 'tens-transition'
	}]

},

//slide 4
{
	hasheaderblock : false,
	contentblockadditionalclass : 'pnkBg',

	contentblocknocenteradjust: true,
	uppertextblockadditionalclass : "baseblock",

	uppertextblock : [
	{
		contentblocknocenteradjust: false,
		textdata : ' '	//empty string used for newline
	},
	{
		contentblocknocenteradjust: false,
		textdata : data.string.p4text5,
	}],

	imageblock : [
	{
		imagestoshow : [{
			imgclass : "transitionapple",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "transitionbox",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "transitionbasket",
			imgsrc : imgpath + "apple_basket.png",
		},
		{
			imgclass : "transitionblock10",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "transitiondrum drum-fade",
			imgsrc : imgpath + "apple_drum.png",
		},
		{
			imgclass : "transitionblock100 block100-fade",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "transition-arrow01v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow02v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow03v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow04v",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow05",
			imgsrc : imgpath + "arrow-right.png",
		},
		{
			imgclass : "transition-arrow06",
			imgsrc : imgpath + "arrow-right.png",
		}
		],
	}],

	lowertextblockadditionalclass : "label-transition",

	lowertextblock : [{
		textdata : data.string.p4text6,
		textclass : 'ones-transitionv'
	},
	{
		textdata : data.string.p4text7,
		textclass : 'tens-transitionv'
	},
	{
		textdata : data.string.p4text8,
		textclass : 'hundreds-transition'
	}]

},

//slide 5
{
	hasheaderblock : false,
	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p4text9,
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "block100-00",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-01",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-02",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-03",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-04",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-05",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-06",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-07",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block100-08",
			imgsrc : imgpath + "100BLOCKS.png",
		},
		{
			imgclass : "block10-00",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-01",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-02",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-03",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-04",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-05",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-06",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-07",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block10-08",
			imgsrc : imgpath + "10BLOCKS.png",
		},
		{
			imgclass : "block1-00",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-01",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-02",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-03",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-04",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-05",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-06",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-07",
			imgsrc : imgpath + "1BLOCK.png",
		},
		{
			imgclass : "block1-08",
			imgsrc : imgpath + "1BLOCK.png",
		}
		],
	}],

	digitblock : [{
		onesdata : '',
		onesclass : 'ones-box',
		hastensdata : true,
		tensdata : '',
		tensclass : 'tens-box',
		hashundredsdata : true,
		hundredsdata : '',
		hundredsclass : 'hundreds-box',
		oneslabel : data.string.p4text6,
		oneslabelclass : 'ones-label-box',
		tenslabel : data.string.p4text7,
		tenslabelclass : 'tens-label-box',
		hundredslabel : data.string.p4text8,
		hundredslabelclass : 'hundreds-label-box',
		buttondata : data.string.show,
		buttonclass : 'check-button-bottom',
	},
	{
		onesclass : 'nodisplay',
		buttondata : data.string.clear,
		buttonclass : 'play-again-button',
	}],

}
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var mistakes = 0;
	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var dropedImgCount = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("digitcontent", $("#digitcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if (countNext == 1 || countNext == 2 || countNext == 3 || countNext == 4) {
			$nextBtn.hide(0);
			$prevBtn.show(0);
		}
		 else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		/* Allow only single numeric character in digit box */
		input_box('.ones-box', 1, '.check-button-bottom');
		input_box('.tens-box', 1, '.check-button-bottom');
		input_box('.hundreds-box', 1, '.check-button-bottom');

		// Count number of mistakes
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch (countNext) {
		case 0:
			sound_player_nav("sound_"+countNext);
			$('.tens-box1').prop("disabled", true);
			$('.hundreds-box1').prop("disabled", true);
			$('.ones-box1').prop("disabled", true);
			break;
		case 1:
			sound_player("sound_"+countNext);
			// Generate random number for each digit position
			var random_hundred = ole.getRandom(1, 3, 1);
			var random_ten = ole.getRandom(1, 5, 0);
			var random_one = ole.getRandom(1, 8, 0);
			// Hide all the images
			for (var i = 1; i < 4; i++) {
 					var strStart = $(".drumapple0"+i);
 					strStart.hide(0);
			}
			for (var i = 0; i < 6; i++) {
 					var strStart = $(".floorbasketimage0"+i);
 					strStart.hide(0);
			}
			for (var i = 0; i < 9; i++) {
 					var strStart = $(".floorappleimage0"+i);
 					strStart.hide(0);
			}

			//Display images according to random numbers
			for(var i = 1; i <= random_hundred; i++){
				var strStart = $(".drumapple0"+i);
 					strStart.show(0);
			}
			for(var i = 0; i < random_ten; i++){
				var strStart = $(".floorbasketimage0"+i);
 					strStart.show(0);
			}
			for(var i = 0; i < random_one; i++){
				var strStart = $(".floorappleimage0"+i);
 					strStart.show(0);
			}

			// Similar answer checking block as in previous pages
			mistakes = 0;
			var redo = 0;
			$('.check-button-bottom').on("click", function() {
				$('.incorrect-answer').hide(0);
				$('.correct-answer').hide(0);
				$nextBtn.hide(0);
				// Allow user to redo the counting of number multiple times
				if(redo > 0) {
					templateCaller();
				}

				// Check number of mistakes
				else if (mistakes > 1) {
					redo = 1;
					$('.hundreds-box').val(random_hundred);
					$('.tens-box').val(random_ten);
					$('.ones-box').val(random_one);
					$('.check-button-bottom').html(data.string.retry);
					$('.check-button-bottom').css("background", '#93C496');
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					$nextBtn.show(0);
				} else if ($('.hundreds-box').val() == '' || $('.tens-box').val() == '' || $('.ones-box').val() == '') {
					return true;
				}
				// Check if answer is correct
				else if ($('.hundreds-box').val() == random_hundred && $('.tens-box').val() ==random_ten && $('.ones-box').val() == random_one) {
					$('.check-button-bottom').css("background", '#93C496');
					$('.correct-answer').show(0);
					$('.ones-box').prop("disabled", true);
					$('.tens-box').prop("disabled", true);
					$('.ones-box').prop("disabled", true);
					$('.hundreds-box').prop("disabled", true);
					$('.check-button-bottom').html(data.string.retry);
					$nextBtn.show(0);
					redo = 1;
					play_correct_incorrect_sound(1);
				}
				// Check if answer is incorrect
				else {
					$('.check-button-bottom').css("background", '#E06666');
					$('.incorrect-answer').show(0);
					mistakes++;
					play_correct_incorrect_sound(0);
				}
			});
			break;
		case 2:
			sound_player_nav("sound_"+countNext);
			// make arrows and text appear after animation
			setTimeout(function() {
    			$('.transition-arrow02').show(0);
    			$('.transition-arrow01').show(0);
    			$('.ones-transition').show(0);
    			// $nextBtn.show(0);
				}, 2000);
			break;
		case 3:
			sound_player_nav("sound_"+countNext);
			// make arrows and text appear after animation
			setTimeout(function() {
    			$('.transition-arrow03').show(0);
    			$('.transition-arrow04').show(0);
    			$('.tens-transition').show(0);
    			// $nextBtn.show(0);
				}, 2000);
			break;
		case 4:
			sound_player_nav("sound_"+countNext);
			// make arrows and text appear after animation
			setTimeout(function() {
    			$('.transition-arrow05').show(0);
    			$('.transition-arrow06').show(0);
    			$('.hundreds-transition').show(0);
    			// $nextBtn.show(0);
				}, 2000);
			break;
		case 5:
			sound_player("sound_"+countNext);
			// to trigger next exercise page only if user atleast enters a digit to any of digit boxes
			var if_done = 0;
			$('.check-button-bottom').on("click", function() {
				$('.imageblock').show(0);
				var ones_answer = $('.ones-box').val() | 0;
				var tens_answer = $('.tens-box').val() | 0;
				var hundreds_answer = $('.hundreds-box').val() | 0;

				if_done = ones_answer | tens_answer | hundreds_answer;
				// for ones
				// Hide images
				for (var i = 0; i < 9; i++){
					var strStart = $(".block1-0"+i);
 					strStart.hide(0);
				}
				// Show images according to input
				for (var i = 0; i < ones_answer; i++){
					var strStart = $(".block1-0"+i);
 					strStart.show(0);
				}
				// for Tens
				for (var i = 0; i < 9; i++){
					var strStart = $(".block10-0"+i);
 					strStart.hide(0);
				}
				for (var i = 0; i < tens_answer; i++){
					var strStart = $(".block10-0"+i);
 					strStart.show(0);
				}
				// for Ones
				for (var i = 0; i < 9; i++){
					var strStart = $(".block100-0"+i);
 					strStart.hide(0);
				}
				for (var i = 0; i < hundreds_answer; i++){
					var strStart = $(".block100-0"+i);
 					strStart.show(0);
				}
				// Display the footer for exercise page
				if(if_done > 0){
				ole.footerNotificationHandler.lessonEndSetNotification();
				}
			});
			$('.play-again-button').on("click", function(){
				$('input').val('');
				$('.imageblock').hide(0);
			})

			break;
		default:
			$('.digit-box').prop("disabled", true);
			break;
		}
	}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}
		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				if(typeof click_class != 'undefined'){
					$(click_class).click(function(){
						current_sound.play();
					});
				}
				nav_button_controls(100);
			});
		}
		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// document.addEventListener('xmlLoad', function(e) {
	total_page = content.length;
	// templateCaller();
	// });
	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 95 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}

});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+ $lang + "/";

var content = [
//slide 0
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text1,
		datahighlightflag : true,
		datahighlightcustomclass : 'textgreen',
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari01.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}],
	}],

	digitblock : [{
		digitdata : '1',
		digitclass : 'digit-box1',
		labeldata : data.string.p4text6,
		labelclass : 'label-box'
	}]

},

//slide 1
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text1,
		datahighlightflag : true,
		datahighlightcustomclass : 'textgreen',
	}, {
		textdata : data.string.p2text2,
		datahighlightflag : true,
		datahighlightcustomclass : 'textred',
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage falling",
			imgsrc : imgpath + "redapple01.png",
		}],
	}],

	digitblock : [{
		digitdata : '',
		digitclass : 'digit-box1',
		labeldata : data.string.p4text6,
		labelclass : 'label-box'
	}]
},

//slide 2
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text1,
		datahighlightflag : true,
		datahighlightcustomclass : 'textgreen',
	}, {
		textdata : data.string.p2text2,
		datahighlightflag : true,
		datahighlightcustomclass : 'textred',
	}, {
		textdata : ' '	//empty string used for newline
	}, {
		textdata : ' '
	}, {
		textdata : data.string.p2text3
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		}],
	}],

	digitblock : [{
		digitdata : '',
		digitclass : 'digit-box',
		labeldata : data.string.p4text6,
		labelclass : 'label-box',
		buttondata : data.string.check,
		buttonclass : 'check-button',
	}],

	lowertextblockadditionalclass : "check-answer",

	lowertextblock : [{
		textdata : data.string.p2text4,
		textclass : 'incorrect-answer'
	}, {
		textdata : data.string.p2text5,
		textclass : 'correct-answer'
	}]
},

//slide 3
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text6,
		datahighlightflag : true,
		datahighlightcustomclass : 'textgreen',
	}, {
		textdata : data.string.p2text7,
		datahighlightflag : true,
		datahighlightcustomclass : 'textred',
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		}],
	}],

	digitblock : [{
		digitdata : '3',
		digitclass : 'digit-box1',
		labeldata : data.string.p4text6,
		labelclass : 'label-box'
	}],

},

//slide 4
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text6
	}, {
		textdata : data.string.p2text8,
		datahighlightflag : true,
		datahighlightcustomclass : 'textred',
	}, {
		textdata : data.string.p2text9
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		}],
	}],

	digitblock : [{
		digitdata : '4',
		digitclass : 'digit-box1',
		labeldata : data.string.p4text6,
		labelclass : 'label-box'
	}],

},

//slide 5
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	}, {
		textdata : data.string.p2text10,
		datahighlightflag : true,
		datahighlightcustomclass : 'textgreen',
	}, {
		textdata : data.string.p2text11,
		datahighlightflag : true,
		datahighlightcustomclass : 'textred',
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage falling",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		}
		],
	}],

	digitblock : [{
		digitdata : '',
		digitclass : 'digit-box',
		labeldata : data.string.p4text6,
		labelclass : 'label-box',
		buttondata : data.string.check,
		buttonclass : 'check-button',
	}],

	lowertextblockadditionalclass : "check-answer",

	lowertextblock : [{
		textdata : data.string.p2text4,
		textclass : 'incorrect-answer'
	}, {
		textdata : data.string.p2text5,
		textclass : 'correct-answer'
	}]
},

//slide 6
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [{
		textdata : ' '	//empty string used for newline
	},
	{
		textdata : data.string.p2text12,
		textclass : 'not-visible'
	},
	{
		textdata : data.string.p2text13
	}],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		}
		],
	}],

	digitblock : [{
		digitdata : '9',
		digitclass : 'digit-box1',
		labeldata : data.string.p4text6,
		labelclass : 'label-box',
		buttondata : data.string.list,
		buttonclass : 'check-button'
	}],

},

//slide 7
{
	hasheaderblock : false,

	uppertextblockadditionalclass : "sidepara",
	contentblockadditionalclass : 'pnkBg',

	uppertextblock : [],

	imageblock : [{
		imagestoshow : [{
			imgclass : "sideimage",
			imgsrc : imgpath + "sundari02.png",
		}, {
			imgclass : "leftappleimage",
			imgsrc : imgpath + "greenapple01.png",
		}, {
			imgclass : "rightappleimage",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage00",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage01",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage02",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage03",
			imgsrc : imgpath + "redapple01.png",
		},
		 {
			imgclass : "floorappleimage04",
			imgsrc : imgpath + "greenapple01.png",
		},
		 {
			imgclass : "floorappleimage05",
			imgsrc : imgpath + "greenapple01.png",
		},
		{
			imgclass : "floorappleimage06",
			imgsrc : imgpath + "redapple01.png",
		}],
	}],

	tableblockadditionalclass : "ones-table",
	tableblock : [{
		digitnumber : '0',
		digitname : data.string.list0,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '1',
		digitname : data.string.list1,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '2',
		digitname : data.string.list2,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '3',
		digitname : data.string.list3,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '4',
		digitname : data.string.list4,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '5',
		digitname : data.string.list5,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '6',
		digitname : data.string.list6,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '7',
		digitname : data.string.list7,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '8',
		digitname : data.string.list8,
		digitclass : 'digit-tablecell'
	},
	{
		digitnumber : '9',
		digitname : data.string.list9,
		digitclass : 'digit-tablecell'
	}],

},];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var mistakes = 0;
	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var dropedImgCount = 0;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images

			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("digitcontent", $("#digitcontent-partial").html());
	Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext == 2 || countNext == 5 || countNext == 6) {
			$nextBtn.hide(0);
			$prevBtn.show(0);
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true

		texthighlight($board);

		vocabcontroller.findwords(countNext);
		/* Function to limit number of digit and type of character in the digit box */
		input_box('.digit-box', 1, '.check-button');

			$nextBtn.hide(0);
			$prevBtn.hide(0);
		//count number of mistakes
		switch (countNext) {
		case 0:
			sound_player_nav("sound_"+countNext);
			$('.digit-box1').prop("disabled", true);
			break;
		case 1:
			sound_player_nav("sound_"+countNext);
			$('.digit-box1').prop("disabled", true);
			break;
		case 2:
			sound_player("sound_"+countNext);
			mistakes = 0;
			$('.check-button').on("click", function() {
				var answer = $('.digit-box').val();
				$('.incorrect-answer').hide(0);
				$('.correct-answer').hide(0);
				$nextBtn.hide(0);
				// Check if mistake is done 3 times
				if (mistakes >= 2) {
					$('.digit-box').val('2');
					$('.check-button').css("background", '#93C496');
					$('.sideimage').attr('src', imgpath + "sundari-smirk.png");
					$('.ones-box').prop("disabled", true);
					$nextBtn.show(0);
				} else if(answer==''){
					return true;
				}
				// Check if answer is correct
				else if (answer == 2) {
					$('.sideimage').attr('src', imgpath + "sundari02.png");
					$('.check-button').css("background", '#93C496');
					$('.correct-answer').show(0);
					$('.digit-box').prop("disabled", true);
					$nextBtn.show(0);
					play_correct_incorrect_sound(1);
				}
				// Check if answer is incorrect
				else {
					$('.sideimage').attr('src', imgpath + "sundari05.png");
					$('.check-button').css("background", '#E06666');
					$('.incorrect-answer').show(0);
					play_correct_incorrect_sound(0);
					mistakes++;
				}
			});
			break;
		case 3:
			sound_player_nav("sound_"+countNext);
			$('.digit-box1').prop("disabled", true);
			break;
		case 4:
			sound_player_nav("sound_"+countNext);
			$('.digit-box1').prop("disabled", true);
			break;
		case 5:
			sound_player("sound_"+countNext);
			mistakes = 0;
			$('.check-button').on("click", function() {
				var answer = $('.digit-box').val();
				$('.incorrect-answer').hide(0);
				$('.correct-answer').hide(0);
				$nextBtn.hide(0);
				// Check if mistake is done 3 times
				if (mistakes >= 2) {
					$('.digit-box').val('9');
					$('.check-button').css("background", '#93C496');
					$('.sideimage').attr('src', imgpath + "sundari-smirk.png");
					$('.ones-box').prop("disabled", true);
					$nextBtn.show(0);
				} else if(answer==''){
					return true;
				}
				// Check if answer is correct
				else if (answer == 9) {
					$('.sideimage').attr('src', imgpath + "sundari02.png");
					$('.check-button').css("background", '#93C496');
					$('.correct-answer').show(0);
					$('.digit-box').prop("disabled", true);
					$nextBtn.show(0);
					play_correct_incorrect_sound(1);
				}
				// Check if answer is incorrect
				else {
					$('.sideimage').attr('src', imgpath + "sundari05.png");
					$('.check-button').css("background", '#E06666');
					$('.incorrect-answer').show(0);
					play_correct_incorrect_sound(0);
					mistakes++;
				}
			});
			break;
		case 6:
			sound_player("sound_"+countNext);
			$('.not-visible').hide(0);
			$('.digit-box1').prop("disabled", true);
			if (mistakes < 2){
				$('.not-visible').show(0);
			}
			$('.check-button').on("click", function() {
				countNext++;
				templateCaller();
			});
			break;
		case 7:
			$('.digit-box1').prop("disabled", true);
			break;
		default:
			$('.digit-box1').prop("disabled", true);
			break;
		}
	}


	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(100);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// document.addEventListener('xmlLoad', function(e) {
	total_page = content.length;
	// templateCaller();

	/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
	 * event.key reurns the value of key pressed by user and it is converted to integer
	 * event.target gets the element where event is occuring (usually a div)
	 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
	 * input_class and button_classes should be something like '.class_name'
	 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
	 */
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}
	// });
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

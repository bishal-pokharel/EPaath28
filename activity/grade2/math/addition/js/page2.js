var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p2text2,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'twopeople'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'threepeople'
				}
			]
		}]

	},
	// slide1
	{
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p2text4,
			textclass: "template-dialougebox2-top-flipped-white lft",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'prem05'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'threepeople'
				}
			]
		}]

	},
	// slide2
	{
		uppertextblock:[
		{
			textdata: data.string.p2text5,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'prem05'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'sagar03'
				},
				{
					imgclass : "cloud02",
					imgsrc : '',
					imgid : 'cloud02'
				},
				{
					imgclass : "cloud01",
					imgsrc : '',
					imgid : 'cloud01'
				},
				{
					imgclass : "fairy",
					imgsrc : '',
					imgid : 'fairy'
				}
			]
		}]

	},
	// slide3
	{
		uppertextblock:[
		{
			textdata: data.string.p2text6,
			textclass: "question",
		},
		{
			textdata: data.string.p2text7,
			textclass: "instuction",
		},
		{
			textdata: data.string.p2text8,
			textclass: "option1 wrong",
		},
		{
			textdata: data.string.p2text9,
			textclass: "option2 correct",
		},
		{
			textdata: data.string.p2text10,
			textclass: "option3 wrong",
		},
		{
			textdata: data.string.p2text11,
			textclass: "option4 wrong",
		},
		{
			textdata: data.string.p2text12,
			textclass: "hint",
			datahighlightflag:true,
			datahighlightcustomclass:'largeandunderlined'
		},
		{
			textdata: data.string.p2text13,
			textclass: "a1",
		},
		{
			textdata: data.string.p2text14,
			textclass: "a2",
		},
		{
			textdata: data.string.p2text15,
			textclass: "a3",
		},
		{
			textclass: "basket1",
		},
		{
			textclass: "basket2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "marbledraggable one",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable two",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable three",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable four",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable fifth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable sixth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable seven",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable eight",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable nine",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable one",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable two",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable three",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable four",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable fifth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable sixth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable seven",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable eight",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable nine",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b1",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b2",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b3",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b4",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b5",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b6",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b7",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b8",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b9",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c1",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c2",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c3",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c4",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c5",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c6",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c7",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c8",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c9",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				}
			]
		}]

	},
	// slide4
	{
		uppertextblock:[{
			textdata: data.string.p2text17,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p2text18,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'prem05'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'rumi'
				}
			]
		}]

	},
	// slide5
	{
		uppertextblock:[
		{
			textdata: data.string.p2text5,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'prem05'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "cloud02",
					imgsrc : '',
					imgid : 'cloud03'
				},
				{
					imgclass : "cloud01",
					imgsrc : '',
					imgid : 'cloud04'
				},
				{
					imgclass : "fairy",
					imgsrc : '',
					imgid : 'fairy'
				}
			]
		}]

	},
	// slide6
	{
		uppertextblock:[
		{
			textdata: data.string.p2text21,
			textclass: "question",
		},
		{
			textdata: data.string.p2text7,
			textclass: "instuction",
		},
		{
			textdata: data.string.p2text8,
			textclass: "option1 wrong",
		},
		{
			textdata: data.string.p2text20,
			textclass: "option2 correct",
		},
		{
			textdata: data.string.p2text10,
			textclass: "option3 wrong",
		},
		{
			textdata: data.string.p2text11,
			textclass: "option4 wrong",
		},
		{
			textdata: data.string.p2text12,
			textclass: "hint",
			datahighlightflag:true,
			datahighlightcustomclass:'largeandunderlined'
		},
		{
			textdata: data.string.p2text13,
			textclass: "a1",
		},
		{
			textdata: data.string.p2text14,
			textclass: "a2",
		},
		{
			textdata: data.string.p2text15,
			textclass: "a3",
		},
		{
			textclass: "basket1",
		},
		{
			textclass: "basket2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "marbledraggable one",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable two",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable three",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable four",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable fifth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable sixth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable seven",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable eight",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable nine",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable one",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable two",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable three",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable four",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable fifth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable sixth",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable seven",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable eight",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marbledraggable nine",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b1",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b2",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b3",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b4",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b5",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b6",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b7",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b8",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble b9",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c1",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c2",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c3",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c4",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c5",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c6",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c7",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c8",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "marble c9",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				}
			]
		}]

	},
	// slide7
	{
		uppertextblock:[{
			textdata: data.string.p2text22,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p2text23,
			textclass: "template-dialougebox2-top-flipped-white",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "left_image",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "right_image",
					imgsrc : '',
					imgid : 'suraj'
				}
			]
		}]

	},
	// slide8
	{
		contentblockadditionalclass:'ole-background-gradient-shore',
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "instuction",
		},
		{
			textdata: data.string.p2text25,
			textclass: "theone numbers",
		},
		{
			textdata: data.string.p2text26,
			textclass: "thetwo numbers",
		},
		{
			textdata: data.string.p2text27,
			textclass: "thethree numbers",
		},
		{
			textdata: data.string.p2text28,
			textclass: "thefour numbers",
		},
		{
			textdata: data.string.p2text29,
			textclass: "thefive numbers",
		},
		{
			textdata: data.string.p2text30,
			textclass: "thesix numbers",
		},
		{
			textdata: data.string.p2text31,
			textclass: "theseven numbers",
		},
		{
			textdata: data.string.p2text32,
			textclass: "theeight numbers",
		},
		{
			textdata: data.string.p2text33,
			textclass: "thenine numbers",
		},
		{
			textdata: data.string.p2text34,
			textclass: "theten numbers",
		},
		{
			textdata: data.string.p2text25,
			textclass: "firstinput",
		},
		{
			textdata: data.string.p2text13,
			textclass: "thecross",
		},
		{
			textdata: data.string.p2text25,
			textclass: "secondinput",
		},
		{
			textdata: data.string.p2text14,
			textclass: "isequals",
		},
		{
			textdata: data.string.p2text25,
			textclass: "output",
		},],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "input1marble1 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble2 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble3 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble4 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble5 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble6 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble7 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble8 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input1marble9 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble1 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble2 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble3 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble4 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble5 marbles" ,
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble6 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble7 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble8 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "input2marble9 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble1 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble2 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble3 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble4 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble5 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble6 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble7 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble8 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble9 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble10 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble11 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble12 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble13 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble14 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble15 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble16 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble17 marbles",
					imgsrc : '',
					imgid : 'marble01'
				},
				{
					imgclass : "outputmarble18 marbles",
					imgsrc : '',
					imgid : 'marble01'
				}
			]
		}]

	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "twopeople", src: imgpath+"premand-sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "threepeople", src: imgpath+"kids03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem05", src: imgpath+"prem05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar03", src: imgpath+"sagar03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj", src: imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud01", src: imgpath+"cloud01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud02", src: imgpath+"cloud02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud03", src: imgpath+"three_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud04", src: imgpath+"four_marble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: imgpath+"flying-chibi-fairy-animation.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "marble01", src: imgpath+"marble02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s2_p1_1.ogg"},
			{id: "sound_1a", src: soundAsset+"s2_p1_2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2_1.ogg"},
			{id: "sound_2a", src: soundAsset+"s2_p2_2.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p4_2.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p5_1.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p5_2.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_9", src: soundAsset+"s2_p8_1.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p8_2.ogg"},
			{id: "sound_11", src: soundAsset+"s2_p9.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var marblecount1=0;
		var marblecount2=0;
		var count=0;
		var input1=0
		var input2=0
		$('.marbledraggable').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                cursor : "move",
                zIndex: 100000,
                helper: 'clone'
            });

		$(".basket1").droppable({
                accept: ".marbledraggable",
                drop: function (event, ui){
                	marblecount1++;
                	if(marblecount1>8){
                		$(".basket1").droppable('destroy');
                	}
                	$(this).html(marblecount1).css({"font-family":"chelseamarket","font-size":"12vmin","color":"white"});
                	$('.b'+marblecount1).animate({"opacity":".5"},300);
        			// ui.draggable.css({"display":"none"});
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });
		$(".basket2").droppable({
                accept: ".marbledraggable",
                drop: function (event, ui){
                	marblecount2++;
                	console.log(marblecount2);
                	if(marblecount2>8){
                		$(".basket2").droppable('destroy');
                	}
                	$(this).html(marblecount2).css({"font-family":"chelseamarket","font-size":"12vmin","color":"white"});
                	$('.c'+marblecount2).animate({"opacity":".5"},300);
        			// ui.draggable.css({"display":"none"});
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });
		function dropfunc(event, ui, $droppedOn){
        	count++;
        	$('.a3').html(count);

        }
		$('.numbers').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                cursor : "move",
                zIndex: 100000,
                helper: 'clone'
            });
		$(".firstinput").droppable({
                accept: ".numbers",
                drop: function (event, ui){
        			input1=parseInt(ui.draggable.html());
        			for (var i = 1; i <=9; i++) {
						$('.input1marble'+i).css({"opacity":"0"});
					}
        			for (var i = 1; i <=input1; i++) {
        				$('.input1marble'+i).css({"opacity":"1"});
        			}
                	$(this).html(input1).css({"font-family":"chelseamarket","font-size":"8vmin","color":"white"});
                	$('.b'+marblecount1).animate({"opacity":".5"},300);
        			// ui.draggable.css({"display":"none"});
                    $this = $(this);
                    dropfunc2(event, ui, $this);
                }
            });
		$(".secondinput").droppable({
                accept: ".numbers",
                drop: function (event, ui){
        			input2=parseInt(ui.draggable.html());
        			for (var i = 1; i <=9; i++) {
						$('.input2marble'+i).css({"opacity":"0"});
					}
        			for (var i = 1; i <= input2; i++) {
        				$('.input2marble'+i).css({"opacity":"1"});
        			}
                	$(this).html(input2).css({"font-family":"chelseamarket","font-size":"8vmin","color":"white"});
                	$('.c'+marblecount2).animate({"opacity":".5"},300);
        			// ui.draggable.css({"display":"none"});
                    $this = $(this);
                    dropfunc2(event, ui, $this);
                }
            });
		function dropfunc2(event, ui, $droppedOn){
        	var output=input1+input2;
        	$('.output').html(output);
        	for (var i = 1; i <=18; i++) {
				$('.outputmarble'+i).css({"opacity":"0"});
			}
        	for (var i = 1; i <=output; i++) {
        				$('.outputmarble'+i).css({"opacity":"1"});
        			}
        }



		switch(countNext) {
			case 0:
			sound_player_seq(['sound_1','sound_1a'],1);
			break;
			case 1:
			sound_player_seq(['sound_2','sound_2a'],1);
			break;
			case 2:
			$('.right_image').css({"right": "45%"});
			$('.cloud01').css({"left": "35%"});
			$('.cloud02').css({"right": "66%"});
			$('.template-dialougebox2-top-flipped-white').css({"top":"9%"});
			sound_player('sound_3',1);
			break;
			case 5:
				sound_player('sound_8',1);

				$('.right_image').css({"right": "45%"});
				$('.cloud01').css({"left": "35%"});
				$('.cloud02').css({"right": "66%"});
				$('.template-dialougebox2-top-flipped-white').css({"top":"9%"});
				break;
			case 3:
			sound_player_seq(['sound_4','sound_5'],0);
			$('.wrong').click(function(){
				createjs.Sound.stop();
				$(this).css({"background":"rgb(224,102,102)","pointer-events":"none"});
				play_correct_incorrect_sound(0);
			});
			$('.correct').click(function(){
				createjs.Sound.stop();
				$(this).css({"background":"rgb(124,196,77)","pointer-events":"none"});
				$('.wrong').css({"pointer-events":"none"});
				play_correct_incorrect_sound(1);
				$('.big').html(data.string.p1text21a).css({"background":"rgb(146,175,59)"});
				nav_button_controls(200);
			});
			break;
			case 4:
			sound_player_seq(['sound_6','sound_7'],1);

				break;
			case 6:
				sound_player_seq(['sound_4','sound_5'],0);

				$('.wrong').click(function(){
					createjs.Sound.stop();

					$(this).css({"background":"rgb(224,102,102)","pointer-events":"none"});
	 				play_correct_incorrect_sound(0);
				});
				$('.correct').click(function(){
					createjs.Sound.stop();

					$(this).css({"background":"rgb(124,196,77)","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
	 				play_correct_incorrect_sound(1);
	 				$('.big').html(data.string.p1text21a).css({"background":"rgb(146,175,59)"});
					nav_button_controls(200);
				});
				break;

			case 7:
					sound_player_seq(['sound_9','sound_10'],1);

					$('.template-dialougebox2-top-white').css({"left": "10%",
																"width": "41%",
																 "top": "12%"});
					break;
			case 6:
				$('.template-dialougebox2-top-white').css({"left": "49%",
																"width": "30%",
																"height": "16%",
																 "top": "16%"});
					break;
			case 8:
				sound_player('sound_11',0);
				nav_button_controls(9000);

				break;
			case 12:
				$('.center_image').css({"width":"23%"});
				nav_button_controls(100);
				break;

			default:

				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

		function sound_player(sound_id, next){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				if(next)
				nav_button_controls(100);
			});
		}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice( 0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									nav_button_controls(0);
					}

			});
	}


	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[{
			textdata: data.string.p3text1,
			textclass: "template-dialougebox2-top-white",
		}
		],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			}
		]
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text2,
			textclass: "template-dialougebox2-top-white",
		},],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "bird3",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "bird4",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text3,
			textclass: "template-dialougebox2-top-white",
		},],
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "bird3",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "bird4",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide3
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textclass: "onetext",
			textdata: data.string.p3text4
		},
		{
			textclass: "twotext",
			textdata: data.string.p3text5
		},
		{
			textclass: "threetext",
			textdata: data.string.p3text6
		},
		{
			textclass: "fourtext",
			textdata: data.string.p3text7
		},],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "bird3",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "bird4",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text8,
			textclass: "template-dialougebox2-top-white",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "bird3",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "bird4",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'bg_full',
		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
		]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text3,
			textclass: "template-dialougebox2-top-white",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
		]
		}]
	},
	// slide7
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textclass: "onetext",
			textdata: data.string.p3text4
		},
		{
			textclass: "twotext",
			textdata: data.string.p3text5
		},
		{
			textclass: "threetext",
			textdata: data.string.p3text6
		},
		{
			textclass: "fourtext",
			textdata: data.string.p3text7
		},
		{
			textclass: "fivetext",
			textdata: data.string.p3text9
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
		]
		}]
	},
	// slide8
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text10,
			textclass: "template-dialougebox2-top-white",
		},
		],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
		]
		}]
	},
	// slide9
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text10,
			textclass: "template-dialougebox2-top-white",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
			{
				imgclass: "birda",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "birdb",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "birdc",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "birdd",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide10
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text11,
			textclass: "template-dialougebox2-top-white",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
			{
				imgclass: "birda",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "birdb",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "birdc",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "birdd",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide11
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text12,
			textclass: "template-dialougebox2-top-white",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
			{
				imgclass: "birda",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "birdb",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "birdc",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "birdd",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide12
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text13,
			textclass: "instruction",
		},
		{
			textdata: data.string.p3text7,
			textclass: "totalbirds1",
		},
		{
			textdata: data.string.p3text9,
			textclass: "totalbirds2",
		},
		{
			textdata: data.string.p2text13,
			textclass: "plus",
		},
		{
			textdata: data.string.p2text14,
			textclass: "isquals",
		},
		{
			textdata: data.string.p3text14,
			textclass: "questionmark",
		},
		{
			textdata: '',
			textclass: "output",
		},],



		imageblock:[{
			imagestoshow:[
			{
				imgclass: "fivebirds",
				imgsrc : '',
				imgid: "fivebirds"
			},
			{
				imgclass: "fourbirds",
				imgsrc : '',
				imgid: "fourbirds"
			},
			{
				imgclass: "birds birdy1",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy2",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy3",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy4",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy5",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy6",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy7",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy8",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy9",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy10",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy11",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy12",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy13",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "birds birdy14",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya1",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya2",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya3",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya4",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya5",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya6",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya7",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya8",
				imgsrc : '',
				imgid: "onebird"
			},
			{
				imgclass: "smallbirds birdya9",
				imgsrc : '',
				imgid: "onebird"
			}
		]
		}]
	},
	// slide13
	{
		contentblockadditionalclass:'bg_full',
		uppertextblock:[
		{
			textdata: data.string.p3text15,
			textclass: "template-dialougebox2-top-white ext",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl1"
			},
			{
				imgclass: "bird1",
				imgsrc : '',
				imgid: "bird5"
			},
			{
				imgclass: "bird2",
				imgsrc : '',
				imgid: "bird6"
			},
			{
				imgclass: "bird3a",
				imgsrc : '',
				imgid: "bird7"
			},
			{
				imgclass: "bird4a",
				imgsrc : '',
				imgid: "bird8"
			},
			{
				imgclass: "bird9",
				imgsrc : '',
				imgid: "bird9"
			},
			{
				imgclass: "birda",
				imgsrc : '',
				imgid: "bird1"
			},
			{
				imgclass: "birdb",
				imgsrc : '',
				imgid: "bird2"
			},
			{
				imgclass: "birdc",
				imgsrc : '',
				imgid: "bird3"
			},
			{
				imgclass: "birdd",
				imgsrc : '',
				imgid: "bird4"
			},
		]
		}]
	},
	// slide14
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text16,
			textclass: "toptext",
		},
		{
			textdata: data.string.p3text17,
			textclass: "middleone",
		},
		{
			textdata: data.string.p3text18,
			textclass: "bottomwithbg",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl2"
			}
		]
		}]
	},
	// slide15
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text19,
			textclass: "toptext",
		},
		{
			textdata: data.string.p3text20,
			textclass: "middleone",
		},
		{
			textdata: data.string.p3text21,
			textclass: "bottomwithbg",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl2"
			}
		]
		}]
	},
	// slide16
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text22,
			textclass: "toptext",
		},
		{
			textdata: data.string.p3text23,
			textclass: "middleone",
		},
		{
			textdata: data.string.p3text24,
			textclass: "bottomwithbg",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl2"
			}
		]
		}]
	},
	// slide17
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[
		{
			textdata: data.string.p3text25,
			textclass: "bottomwithbg",
		}],

		imageblock:[{
			imagestoshow:[
			{
				imgclass: "girl1",
				imgsrc : '',
				imgid: "girl2"
			}
		]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl1", src: imgpath+"thinkinggirl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"deepa01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird1", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird2", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird3", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird4", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird5", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird6", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird7", src: imgpath+"07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird8", src: imgpath+"08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird9", src: imgpath+"09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fivebirds", src: imgpath+"fivebird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fourbirds", src: imgpath+"four_bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "onebird", src: imgpath+"10.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4_1.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p4_2.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p4_3.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p4_4.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p8_5.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_12", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_13", src: soundAsset+"s3_p12.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p13.ogg"},
			{id: "sound_15", src: soundAsset+"s3_p14.ogg"},
			{id: "sound_16", src: soundAsset+"s3_p15.ogg"},
			{id: "sound_17", src: soundAsset+"s3_p16.ogg"},
			{id: "sound_18", src: soundAsset+"s3_p17.ogg"},
			{id: "sound_19", src: soundAsset+"s3_p18.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);



		switch(countNext) {
			case 0:
				sound_player('sound_1',1);
				break;
			case 1:
				sound_player('sound_2',1);
			break;
			case 2:
				sound_player('sound_3',1);
				break;
			case 3:
			sound_player_seq(['sound_4','sound_5','sound_6','sound_7'],1);
				$('.onetext,.bird1').css({"opacity":"0.2"}).delay(1000).animate({"opacity":"1"},1000);
				$('.twotext,.bird2').css({"opacity":"0.2"}).delay(2000).animate({"opacity":"1"},1000);
				$('.threetext,.bird3').css({"opacity":"0.2"}).delay(3000).animate({"opacity":"1"},1000);
				$('.fourtext,.bird4').css({"opacity":"0.2"}).delay(4000).animate({"opacity":"1"},1000);
				break;
			case 4:
				sound_player('sound_8',1);
				break;
			case 5:
				nav_button_controls(2000);
				$('.bird1').css({"left": "-16%","top": "16%"}).animate({"left": "6%","top": "16%"},3000);
				$('.bird2').css({"left": "-16%","top": "5%"}).animate({"left": "30%","top": "5%"},3000);
				$('.bird3a').css({"top": "5%","left": "-16%"}).animate({"top": "5%","left": "57%"},3000);
				$('.bird4a').css({"left": "-16%","top": "18%"}).animate({"left": "80%","top": "18%"},3000);
				$('.bird9').css({"left": "-16%","top": "18%"}).animate({"left": "44%","top": "25%"},3000);
				break;
			case 6:

				$('.bird1').css({"left": "6%","top": "10%"});
				$('.bird2').css({"left": "30%","top": "5%"});
				$('.bird3a').css({"top": "5%","left": "57%"});
				$('.bird4a').css({"left": "80%","top": "18%"});
				$('.bird9').css({"left": "50%","top": "25%"});
				$('.girl1').css({"left": "0%","bottom": "-25%"}).animate({"left": "0%","bottom": "0%"},1000);
				$('.template-dialougebox2-top-white').css({"opacity":"0"}).delay(1000).animate({"opacity":"1"},1000);
				sound_player('sound_9',1);

				break;
			case 7:
				sound_player_seq(['sound_4','sound_5','sound_6','sound_7','sound_10'],1);

				$('.bird1,.onetext').css({"left": "6%","top": "16%","opacity":"0.2"}).delay(1000).animate({"opacity":"1"},1000);;
				$('.bird2,.twotext').css({"left": "30%","top": "5%","opacity":"0.2"}).delay(2000).animate({"opacity":"1"},1000);;
				$('.bird3a,.threetext').css({"top": "5%","left": "57%","opacity":"0.2"}).delay(3000).animate({"opacity":"1"},1000);;
				$('.bird4a,.fourtext').css({"left": "80%","top": "18%","opacity":"0.2"}).delay(4000).animate({"opacity":"1"},1000);;
				$('.bird9,.fivetext').css({"left": "44%","top": "25%","opacity":"0.2"}).delay(5000).animate({"opacity":"1"},1000);;
				break;
			case 8:
			sound_player('sound_11',1);

				$('.bird1,.onetext').css({"left": "6%","top": "10%"});
				$('.bird2,.twotext').css({"left": "30%","top": "5%"});
				$('.bird3a,.threetext').css({"top": "5%","left": "57%"});
				$('.bird4a,.fourtext').css({"left": "80%","top": "18%"});
				$('.bird9,.fivetext').css({"left": "47%","top": "25%"});
				$('.girl1').css({"left": "0%","bottom": "-25%"}).animate({"left": "0%","bottom": "0%"},1000);
				$('.template-dialougebox2-top-white').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				break;
			case 9:
				$('.bird1,.onetext').css({"left": "6%","top": "10%"});
				$('.bird2,.twotext').css({"left": "30%","top": "5%"});
				$('.bird3a,.threetext').css({"top": "5%","left": "57%"});
				$('.bird4a,.fourtext').css({"left": "80%","top": "18%"});
				$('.bird9,.fivetext').css({"left": "44%","top": "25%"});
				$('.girl1').css({"left": "0%","bottom": "0%"}).animate({"left": "0%","bottom": "-50%"},1000);
				$('.template-dialougebox2-top-white').css({"opacity":"1"}).animate({"opacity":"0"},500);
				$('.birda').css({"left": "-16%","top": "60%"}).delay(1000).animate({"left": "14%","top": "60%"},3000);
				$('.birdb').css({"left": "-16%","top": "50%"}).delay(1000).animate({"left": "30%","top": "50%"},3000);
				$('.birdc').css({"top": "50%","left": "-16%","transform": "scaleX(-1)"}).delay(1000).animate({"top": "50%","left": "57%"},3000);
				$('.birdd').css({"left": "-16%","top": "60%","transform": "scaleX(-1)"}).delay(1000).animate({"left": "80%","top": "60%"},3000);
				nav_button_controls(2000);
				break;
			case 10:
				$('.bird1,.onetext').css({"left": "6%","top": "10%"});
				$('.bird2,.twotext').css({"left": "30%","top": "5%"});
				$('.bird3a,.threetext').css({"top": "5%","left": "57%"});
				$('.bird4a,.fourtext').css({"left": "80%","top": "18%"});
				$('.bird9,.fivetext').css({"left": "44%","top": "25%"});
				$('.girl1').css({"left": "0%","bottom": "-50%","z-index":"9999"}).animate({"left": "0%","bottom": "0%"},1000);
				$('.template-dialougebox2-top-white').css({"opacity":"0","z-index":"9999"}).delay(1000).animate({"opacity":"1"},500);
				$('.birda').css({"left": "14%","top": "60%"});
				$('.birdb').css({"left": "30%","top": "56%"});
				$('.birdc').css({"top": "50%","left": "57%","transform": "scaleX(-1)"});
				$('.birdd').css({"left": "80%","top": "60%","transform": "scaleX(-1)"});
				sound_player('sound_12',1);

				break;
			case 11:
			sound_player('sound_13',1);

			$('.bird1,.onetext').css({"left": "6%","top": "0%"});
			$('.bird2,.twotext').css({"left": "30%","top": "0%"});
			$('.bird3a,.threetext').css({"top": "5%","left": "57%"});
			$('.bird4a,.fourtext').css({"left": "80%","top": "18%"});
			$('.bird9,.fivetext').css({"left": "57%","top": "28%"});
			$('.girl1').css({"z-index":"9999"});
			$('.template-dialougebox2-top-white').css({"z-index":"9999"});
			$('.birda').css({"left": "14%","top": "60%"});
			$('.birdb').css({"left": "30%","top": "55%"});
			$('.birdc').css({"top": "50%","left": "57%","transform": "scaleX(-1)"});
			$('.birdd').css({"left": "80%","top": "60%","transform": "scaleX(-1)"});
			break;
			case 13:
			sound_player('sound_15',1);

				$('.bird1,.onetext').css({"left": "6%","top": "0%"});
				$('.bird2,.twotext').css({"left": "30%","top": "0%"});
				$('.bird3a,.threetext').css({"top": "5%","left": "57%"});
				$('.bird4a,.fourtext').css({"left": "80%","top": "18%"});
				$('.bird9,.fivetext').css({"left": "53%","top": "25%"});
				$('.girl1').css({"z-index":"9999"});
				$('.template-dialougebox2-top-white').css({"z-index":"9999"});
				$('.birda').css({"left": "14%","top": "60%"});
				$('.birdb').css({"left": "30%","top": "55%"});
				$('.birdc').css({"top": "50%","left": "57%","transform": "scaleX(-1)"});
				$('.birdd').css({"left": "80%","top": "60%","transform": "scaleX(-1)"});
				break;
			case 12:
					sound_player('sound_14',0);

					var count = 0;
					$('.birds').click(function(){
						$('.questionmark').animate({"opacity":"0"},200);
						$(this).animate({"opacity":"0"},200).css({"display":"none"});
						count++;
						$('.output').css({"opacity":"1"}).html(count);
						$('.birdya'+count).animate({"opacity":"1"},200);
						if(count==9)
						{
							nav_button_controls(100);
							$('.birds').css({"pointer-events":"none"});
						}
					});

					break;
			case 14:
			sound_player('sound_16',1);
				break;
			case 15:
			sound_player('sound_17',1);
				break;
			case 16:
				sound_player('sound_18',1);
					break;
			case 17:
				$('.bottomwithbg').css({"top":"30%"});
				sound_player('sound_19',1);
				break;
			default:

				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			nav_button_controls(100);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice( 0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									nav_button_controls(0);
					}

			});
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();


		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

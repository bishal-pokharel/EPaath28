var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'orangebg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "elephant",
					imgsrc : '',
					imgid : 'elephant'
				}
			]

		}],
	},
	// slide1
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'prem01'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p1text7,
			textclass: "template-dialougebox2-top-flipped-white",
		}]
	},
	// slide2
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'prem03'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar03'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p1text9,
			textclass: "template-dialougebox2-top-flipped-white",
		}]
	},
	// slide3
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'prem02'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar03'
				},
				{
					imgclass : "pari",
					imgsrc : '',
					imgid : 'chari'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text10,
			textclass: "template-dialougebox2-top-white",
		}]
	},
	// slide4
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'prem02'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar03'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text11,
			textclass: "countone",
		},
		{
			textdata: data.string.p1text12,
			textclass: "counttwo",
		}]
	},
	// slide5
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'prem02'
				},
				{
					imgclass : "sagar",
					imgsrc : '',
					imgid : 'sagar03'
				},
				{
					imgclass : "pari",
					imgsrc : '',
					imgid : 'chari'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text13,
			textclass: "template-dialougebox2-top-white",
		}]
	},
	// slide6
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "leftpeople",
					imgsrc : '',
					imgid : 'twopeople'
				},
				{
					imgclass : "rightpeople",
					imgsrc : '',
					imgid : 'threepeople'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text14,
			textclass: "template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p1text15,
			textclass: "template-dialougebox2-top-flipped-white",
		}]
	},
	// slide7
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'threepeople'
				},
				{
					imgclass : "pari",
					imgsrc : '',
					imgid : 'chari'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text16,
			textclass: "template-dialougebox2-top-white",
		}]
	},
	// slide8
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "one",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "two",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "three",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text11,
			textclass: "onetext",
		},
		{
			textdata: data.string.p1text12,
			textclass: "twotext",
		},
		{
			textdata: data.string.p1text17,
			textclass: "threetext",
		}]
	},
	// slide9
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "prem",
					imgsrc : '',
					imgid : 'threepeople'
				},
				{
					imgclass : "pari",
					imgsrc : '',
					imgid : 'chari'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text18,
			textclass: "template-dialougebox2-top-white",
		}]
	},
	//slide10
	{
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "cloud",
					imgsrc : '',
					imgid : 'cloud03'
				},
				{
					imgclass : "pari",
					imgsrc : '',
					imgid : 'chari'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text19,
			textclass: "template-dialougebox2-top-white",
		}]
	},

	// slide11
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "question1",
					imgsrc : '',
					imgid : 'twopeople'
				},
				{
					imgclass : "question2",
					imgsrc : '',
					imgid : 'threepeople'
				},
				{
					imgclass : "wrong option1",
					imgsrc : '',
					imgid : 'threepeople'
				},
				{
					imgclass : "wrong option2",
					imgsrc : '',
					imgid : 'fourpeople'
				},
				{
					imgclass : "option3 correctans",
					imgsrc : '',
					imgid : 'fivepeople'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text21,
			textclass: "subquestion",
			datahighlightflag:true,
			datahighlightcustomclass:'big'
		},
		{
			textdata: data.string.p1text20,
			textclass: "mainquestion",
		}]
	},
	// slide12
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'sagar04'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'prem04'
				},
				{
					imgclass : "third",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text23,
			textclass: "mainquestion",
		}]
	},
	// slide13
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'sagar04'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'prem04'
				},
				{
					imgclass : "third",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text11,
			textclass: "textone",
		},
		{
			textdata: data.string.p1text12,
			textclass: "texttwo",
		},
		{
			textdata: data.string.p1text17,
			textclass: "textthree",
		},
		{
			textdata: data.string.p1text24,
			textclass: "textfour",
		},
		{
			textdata: data.string.p1text25,
			textclass: "textfive",
		}]
	},
	// slide14
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'sagar04'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'prem04'
				},
				{
					imgclass : "third",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text26,
			textclass: "toptext",
		}]
	},
	// slide15
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'sagar04'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'prem04'
				},
				{
					imgclass : "third",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text27,
			textclass: "uptext",
		}]
	},
	// slide16
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "first",
					imgsrc : '',
					imgid : 'sagar04'
				},
				{
					imgclass : "second",
					imgsrc : '',
					imgid : 'prem04'
				},
				{
					imgclass : "third",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "fourth",
					imgsrc : '',
					imgid : 'rumi'
				},
				{
					imgclass : "fifth",
					imgsrc : '',
					imgid : 'suraj02'
				}
			]

		}],
		uppertextblock:[{
			textdata: data.string.p1text28,
			textclass: "liketext",
			datahighlightflag:true,
			datahighlightcustomclass:'focus'
		},{
			textdata: data.string.p1text29,
			textclass: "uptext",
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chari", src: imgpath+"flying-chibi-fairy-animation.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "prem01", src: imgpath+"prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem02", src: imgpath+"prem02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem03", src: imgpath+"prem03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem04", src: imgpath+"prem04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar01", src: imgpath+"sagar01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar02", src: imgpath+"sagar02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar03", src: imgpath+"sagar03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar04", src: imgpath+"sagar04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "twopeople", src: imgpath+"premand-sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "threepeople", src: imgpath+"kids03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fourpeople", src: imgpath+"kids04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fivepeople", src: imgpath+"kids05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "suraj02", src: imgpath+"suraj02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud03", src: imgpath+"cloud03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p2_pradeep.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p3_pradeep.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p5_1.ogg"},

			{id: "sound_8", src: soundAsset+"s1_p5_2.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p7_1.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p7_2.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p8.ogg"},

			{id: "sound_13", src: soundAsset+"s1_p9_1.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p9_2.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p9_3.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p11.ogg"},

			{id: "sound_18", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_19", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_20", src: soundAsset+"s1_p14_1.ogg"},
			{id: "sound_21", src: soundAsset+"s1_p14_2.ogg"},
			{id: "sound_22", src: soundAsset+"s1_p14_3.ogg"},

			{id: "sound_23", src: soundAsset+"s1_p14_4.ogg"},
			{id: "sound_24", src: soundAsset+"s1_p14_5.ogg"},
			{id: "sound_25", src: soundAsset+"s1_p15.ogg"},
			{id: "sound_26", src: soundAsset+"s1_p16.ogg"},
			{id: "sound_27", src: soundAsset+"s1_p17.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_player('sound_1',1);
				break;
			case 1:
				sound_player_seq(['sound_2','sound_3'],1);
				break;
			case 2:
				$('.template-dialougebox2-top-white').css({"left":"15%"});
				$('.template-dialougebox2-top-flipped-white').css({"left":"52%","top": "3%","width": "31%"});
				sound_player_seq(['sound_4','sound_5'],1);
				break;
			case 3:
			sound_player('sound_6',1);

			$('.prem').css({"left":"30%"});
			$('.sagar').css({"left":"51%"});
			$('.template-dialougebox2-top-white').css({"left":"6%","top": "2%"});

			break;
			case 5:
			sound_player('sound_9',1);

			$('.prem').css({"left":"30%"});
			$('.sagar').css({"left":"51%"});
			$('.template-dialougebox2-top-white').css({"left":"6%","top": "2%"});

			break;
			case 7:
			sound_player('sound_12',1);

			$('.prem').css({"left":"30%"});
			$('.sagar').css({"left":"51%"});
			$('.template-dialougebox2-top-white').css({"left":"6%","top": "2%"});

			break;
			case 9:
				sound_player('sound_16',1);

				$('.prem').css({"left":"30%"});
				$('.sagar').css({"left":"51%"});
				$('.template-dialougebox2-top-white').css({"left":"6%","top": "2%"});

				break;
			case 4:
			sound_player_seq(['sound_7','sound_8'],1);

				$('.prem').css({"left":"30%","opacity":"0.4"}).animate({"opacity":"1"});
				$('.sagar').css({"left":"51%","opacity":"0.4"}).delay(1000).animate({"opacity":"1"});
				$('.template-dialougebox2-top-white').css({"left":"6%","top": "2%"});
				$('.countone').css({"opacity":"0"}).animate({"opacity":"1"});
				$('.counttwo').css({"opacity":"0"}).delay(1000).animate({"opacity":"1"});
				nav_button_controls(2000);
				break;
			case 6:
			sound_player_seq(['sound_10','sound_11'],1);

				$('.template-dialougebox2-top-white').css({"width":"45%"});
				$('.template-dialougebox2-top-flipped-white').css({"left":"60%","top": "12%","width": "31%"});
				// nav_button_controls(100);
				break;
			case 8:
			sound_player_seq(['sound_13','sound_14','sound_15'],1);

				$('.one,.onetext').css({"opacity":"0.2"}).animate({"opacity":"1"},1000);
				$('.two,.twotext').css({"opacity":"0.2"}).delay(1000).animate({"opacity":"1"},1000);
				$('.three,.threetext').css({"opacity":"0.2"}).delay(2000).animate({"opacity":"1"},1000);
				nav_button_controls(3000);
				break;
			case 10:
			sound_player('sound_17',1);

				$('.template-dialougebox2-top-white').css({"left":"5%","top": "4%"});
				// nav_button_controls(100);
				break;
			case 11:
					sound_player('sound_18',0);

				$('.wrong').click(function(){
					$(this).css({"background":"rgb(224,102,102)","pointer-events":"none"});
	 				play_correct_incorrect_sound(0);
				});
				$('.correctans').click(function(){
					$(this).css({"background":"rgb(146,175,59)","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
	 				play_correct_incorrect_sound(1);
	 				$('.big').html(data.string.p1text21a).css({"background":"rgb(146,175,59)"});
					$nextBtn.show(0);
					$prevBtn.show(0);
				});
				break;
			case 12:
				$('.mainquestion').css({"top": "8%"});
				sound_player('sound_19',1);

				break;
			case 13:
			sound_player_seq(['sound_20','sound_21','sound_22','sound_23','sound_24'],1);

				$('.first,.textone').css({"opacity":"0.3"}).animate({"opacity":"1"},1000);
				$('.second,.texttwo').css({"opacity":"0.3"}).delay(1000).animate({"opacity":"1"},1000);
				$('.third,.textthree').css({"opacity":"0.3"}).delay(2000).animate({"opacity":"1"},1000);
				$('.fourth,.textfour').css({"opacity":"0.3"}).delay(3000).animate({"opacity":"1"},1000);
				$('.fifth,.textfive').css({"opacity":"0.3"}).delay(4000).animate({"opacity":"1"},1000);
				nav_button_controls(5000);
				break;
			case 14:
				sound_player('sound_25',1);

				break;
				case 15:
					sound_player('sound_26',1);

					break;

			case 16:
			sound_player('sound_27',1);

				break;

			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			nav_button_controls(100);
		});
	}

	function sound_player_seq(soundarray,navflag){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(soundarray[0]);
			soundarray.splice( 0, 1);
			current_sound.on("complete", function(){
					if(soundarray.length > 0){
							sound_player_seq(soundarray, navflag);
					}else{
							if(navflag)
									nav_button_controls(0);
					}

			});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

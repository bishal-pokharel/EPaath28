Array.prototype.shufflearray = function() {
  var i = this.length,
    j,
    temp;
  while (--i > 0) {
    j = Math.floor(Math.random() * (i + 1));
    temp = this[j];
    this[j] = this[i];
    this[i] = temp;
  }
  return this;
};

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/audio_" + $lang + "/";
var soundAsset1 = $ref + "/sounds/" + $lang + "/";

var content = [
  {
    // slide0
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "pumkin"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q1_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q1_o1,
            dataname: "4kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q1_o2,
            dataname: "20kg"
          }
        ]
      }
    ]
  },
  {
    // slide1
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_add",
            imgid: "salt"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q2_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q2_o1,
            dataname: "1kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q2_o2,
            dataname: "50kg"
          }
        ]
      }
    ]
  },
  //50gm
  {
    // slide2
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_add3",
            imgid: "orange"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q3_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q3_o1,
            dataname: "50gm"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q3_o2,
            dataname: "3kg"
          }
        ]
      }
    ]
  },
  {
    // slide3
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "chicken"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q4_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q4_o1,
            dataname: "2kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q4_o2,
            dataname: "20kg"
          }
        ]
      }
    ]
  },
  //50gm
  {
    // slide4
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_ad3",
            imgid: "eraser"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q5_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q5_o1,
            dataname: "50gm"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q5_o2,
            dataname: "500gm"
          }
        ]
      }
    ]
  },
  {
    // slide5
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "rabbit"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q6_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q6_o1,
            dataname: "2kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q6_o2,
            dataname: "15kg"
          }
        ]
      }
    ]
  },
  //500gm
  {
    // slide6
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_add",
            imgid: "laptop"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q7_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q7_o1,
            dataname: "500gm"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q7_o2,
            dataname: "10kg"
          }
        ]
      }
    ]
  },
  //200gm
  {
    // slide7
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "mouse"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q8_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q8_o1,
            dataname: "200gm"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q8_o2,
            dataname: "20kg"
          }
        ]
      }
    ]
  },
  {
    // slide8
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "bull"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q9_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q9_o1,
            dataname: "50kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q9_o2,
            dataname: "500kg"
          }
        ]
      }
    ]
  },
  {
    // slide9
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "goat"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q10_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q10_o1,
            dataname: "50kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q10_o2,
            dataname: "500kg"
          }
        ]
      }
    ]
  },
  {
    // slide10
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit",
            imgid: "rice_sacks"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q11_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text yestext",
            imagelabeldata: data.string.e2_q11_o1,
            dataname: "25kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text notext",
            imagelabeldata: data.string.e2_q11_o2,
            dataname: "500kg"
          }
        ]
      }
    ]
  },
  {
    // slide11
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_add",
            imgid: "dog"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q12_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text",
            imagelabeldata: data.string.e2_q12_o1,
            dataname: "12kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text",
            imagelabeldata: data.string.e2_q12_o2,
            dataname: "300kg"
          }
        ]
      }
    ]
  },
  {
    // slide12
    contentnocenteradjust: true,
    contentblockadditionalclass: "bluebg",
    headerblock: [
      {
        textclass: "descriptionheader",
        textdata: data.string.e2_question
      }
    ],

    weightblock: [
      {
        weighblocksubclass: "beambalance_base"
      },
      {
        weighblocksubclass: "beambalance_left"
      },
      {
        weighblocksubclass: "beambalance_right"
      },
      {
        weighblocksubclass: "beambalance_needle"
      },
      {
        weighblocksubclass: "drop_left"
      },
      {
        weighblocksubclass: "drop_right"
      }
    ],

    imageblockadditionalclass: "draggablecontainer",
    imageblock: [
      {
        imageblockclass: "mangoescollection",
        imagestoshow: [
          {
            imgclass: "fruit image2_add",
            imgid: "ball"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "imagelabel_description",
            imagelabeldata: data.string.e2_q13_hint
          }
        ]
      },
      {
        imageblockclass: "option yes",
        imagestoshow: [
          {
            imgclass: "correct_sign",
            imgid: "correct"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text",
            imagelabeldata: data.string.e2_q13_o1,
            dataname: "500kg"
          }
        ]
      },
      {
        imageblockclass: "option",
        imagestoshow: [
          {
            imgclass: "incorrect_sign",
            imgid: "wrong"
          }
        ],
        imagelabels: [
          {
            imagelabelclass: "text",
            imagelabeldata: data.string.e2_q13_o2,
            dataname: "10kg"
          }
        ]
      }
    ]
  }
];

// content.shufflearray();

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  //for preload
  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      {
        id: "pumkin",
        src: imgpath + "pumkin.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "salt",
        src: imgpath + "salt.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "orange",
        src: imgpath + "orange2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "chicken",
        src: imgpath + "chicken.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "eraser",
        src: imgpath + "eraser.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rabbit",
        src: imgpath + "rabbit.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "laptop",
        src: imgpath + "xo.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mouse",
        src: imgpath + "mouse.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bull",
        src: imgpath + "rice.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "goat",
        src: imgpath + "goat.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rice_sacks",
        src: imgpath + "rice_sacks.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dog",
        src: imgpath + "dog.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "ball",
        src: imgpath + "ball.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "needle",
        src: imgpath + "needle.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "weightmachinebase",
        src: imgpath + "weightmachinebase.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "weight_plate",
        src: imgpath + "weight_plate.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "needle",
        src: imgpath + "needle.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hand-icon",
        src: "images/hand-icon.gif",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "correct",
        src: "images/correct.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "wrong",
        src: "images/wrong.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "1kg",
        src: imgpath + "1kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "2kg",
        src: imgpath + "2kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "3kg",
        src: imgpath + "3kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "4kg",
        src: imgpath + "4kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "10kg",
        src: imgpath + "10kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "12kg",
        src: imgpath + "12kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "15kg",
        src: imgpath + "15kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "20kg",
        src: imgpath + "20kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "25kg",
        src: imgpath + "25kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "50kg",
        src: imgpath + "50kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "120kg",
        src: imgpath + "120kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "200kg",
        src: imgpath + "200kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "500kg",
        src: imgpath + "500kg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "50gm",
        src: imgpath + "50gm.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "200gm",
        src: imgpath + "200gm.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "300gm",
        src: imgpath + "300gm.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "500gm",
        src: imgpath + "500gm.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "ding", src: soundAsset + "ding.ogg" },
      { id: "coin_drag", src: soundAsset + "coin_drag.ogg" },
      { id: "sound_1", src: soundAsset1 + "ex2.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    // call main function
    templateCaller();
  }
  //initialize
  init();
  // loadTimelineProgress($total_page,countNext+1);

  /*==================================================
=            Handlers and helpers Block            =
==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
     =            data highlight function            =
     ===============================================*/
  /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }

  /*=====  End of data highlight function  ======*/

  /*===============================================
     =            user notification function        =
     ===============================================*/
  /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

  /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
  /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      // $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      // $nextBtn.show(0);
      // $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }
  /*=====  End of user navigation controller function  ======*/

  /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
  /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

  /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }
  /*=====  End of InstructionBlockController  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var score = 0;
  var testin = new EggTemplate();

  //eggTemplate.eggMove(countNext);
  testin.init(10);
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    // vocabcontroller.findwords(countNext);
    // splitintofractions($(".fractionblock"));
    if (countNext == 0) {
      createjs.Sound.stop();
      current_sound = createjs.Sound.play("sound_1");
      current_sound.play();
    }
    var $option = $(".option");
    var randidx;
    while ($option.length > 0) {
      if ($option.length == 1) {
        $($option[0]).addClass("option_right");
        randidx = 0;
      } else {
        randidx = Math.floor(Math.random() * $option.length);
        $($option[randidx]).addClass("option_left");
      }
      $option.splice(randidx, 1);
    }

    $(".option > img").hide(0);

    function setupbalance(correct_weight, incorrect_weight) {
      console.log(correct_weight);
      console.log(incorrect_weight);
      $(".beambalance_base").css(
        "background-image",
        'url("' + preload.getResult("weightmachinebase").src + '")'
      );
      $(".beambalance_left").css(
        "background-image",
        'url("' + preload.getResult("weight_plate").src + '")'
      );
      $(".beambalance_right").css(
        "background-image",
        'url("' + preload.getResult("weight_plate").src + '")'
      );
      $(".beambalance_needle").css(
        "background-image",
        'url("' + preload.getResult("needle").src + '")'
      );

      var $drop_right = $(".drop_right");
      var $drop_left = $(".drop_left");
      var $leftpan = $(".beambalance_left");
      var $rightpan = $(".beambalance_right");
      var $needle = $(".beambalance_needle");

      var answered = false;
      var wrongfirst = false;
      $(".option").click(function() {
        if (answered) {
          return answered;
        }

        var $this = $(this);
        $($this.find("img")).show(0);
        $(".weightblock").show(0);
        $(".imagelabel_description").hide(0);

        $(".fruit")
          .detach()
          .css({
            bottom: "2%",
            width: "90%",
            left: "5%"
          })
          .appendTo($drop_right);

        if ($this.hasClass("yes")) {
          $drop_left.html();
          $drop_left.html(
            '<img class="weight_left_pan" src=' +
              preload.getResult(correct_weight).src +
              " >"
          );

          if (wrongfirst) {
            $leftpan.addClass("neutral_left_pan");
            $rightpan.addClass("neutral_right_pan");
            $needle.addClass("neutral_needle");
            $drop_left.addClass("neutral_drop_left");
            $drop_right.addClass("neutral_drop_right");
          } else {
            testin.update(true);
          }
          current_sound.stop();
          play_correct_incorrect_sound(1);
          $(".option").addClass("disable");
          $this.addClass("correct");
          answered = true;
          if ($total_page > countNext + 1) {
            $nextBtn.show(0);
          }
        } else {
          testin.update(false);
          wrongfirst = true;

          $drop_left.html(
            '<img class="weight_left_pan" src=' +
              preload.getResult(incorrect_weight).src +
              " >"
          );
          $leftpan.addClass("left_heavy_left_pan");
          $rightpan.addClass("left_heavy_right_pan");
          $needle.addClass("left_heavy_needle");
          $drop_left.addClass("left_heavy_drop_left");
          $drop_right.addClass("left_heavy_drop_right");

          current_sound.stop();
          play_correct_incorrect_sound(0);
          $this.addClass("incorrect");
        }
      });
    }

    switch (countNext) {
      default:
        var $option = $(".option");
        var string1;
        var string2;
        var fig1 = $(".yestext").attr("data-name");
        var fig2 = $(".notext").attr("data-name");
        setupbalance(fig1, fig2);
        // setupbalance( string1.replace(/\s/g, ''), string2.replace(/\s/g, ''));
        break;
    }
  }

  /*=====  End of Templates Block  ======*/

  function sound_player(sound_id) {
    createjs.Sound.stop();
    var current_sound = createjs.Sound.play(sound_id);
    current_sound.on("complete", function() {
      // if($total_page > (countNext+1)){
      // $nextBtn.show(0);
      // }else{
      // ole.footerNotificationHandler.pageEndSetNotification();
      // }
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      for (var j = 0; j < content[count].imageblock.length; j++) {
        var imageblock = content[count].imageblock[j];
        console.log("imageblock", imageblock);
        if (imageblock.hasOwnProperty("imagestoshow")) {
          var imageClass = imageblock.imagestoshow;
          for (var i = 0; i < imageClass.length; i++) {
            var image_src = preload.getResult(imageClass[i].imgid).src;
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = "." + classes_list[classes_list.length - 1];
            $(selector).attr("src", image_src);
          }
        }
      }
    }
  }

  /*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

    //call the slide indication bar handler for pink indicators
    // loadTimelineProgress($total_page,countNext+1);
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    testin.gotoNext();
    if (countNext < 10) {
      templateCaller();
    } else {
      $(".headerblock").hide(0);
      $nextBtn.hide(0);
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
  /*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
        contentnocenteradjust: "true",
		uppertextblock:[{
			textclass: "description1 title",
			textdata: data.string.title
		}],
        imageblockadditionalclass:"",

        imageblock:[{
            imagestoshow: [{
                imgclass: "coverpage",
                imgid: "coverpage"
            }]
        }]
	},{
		// slide1
		contentblockadditionalclass: "brownbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		imageblock:[{
			imgcontainerdiv: "imagecenter1",
			imagestoshow: [{
				imgclass: "image90",
				imgid: "pencilbox"
			}]
		}]
	},{
		// slide2
		contentblockadditionalclass: "brownbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s1
		}],
		imageblock:[{
			imgcontainerdiv: "imagecenter1",
			imagestoshow: [{
				imgclass: "image90",
				imgid: "pencilbox"
			}]
		}]
	},{
		// slide3
		contentblockadditionalclass: "brownbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s2
		},{
			textclass: "description fade_in",
			textdata: data.string.p1_s4
		},{
			textclass: "description fade_in2",
			datahighlightflag: "true",
			datahighlightcustomclass: "green_hilight",
			textdata: data.string.p1_s5
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "image20",
				imgid: "pencilbox"
			},{
				imgclass: "click_indicator",
				imgid: "hand-icon"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_blue",
				imagelabeldata: data.string.p1_s3
			}]
		}]
	},{
		// slide4
		contentblockadditionalclass: "brownbg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description fade_in",
			textdata: data.string.p1_s6
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "image20",
				imgid: "pencilbox"
			}]
		}]
	},{
		// slide5
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s7
		}],
		imageblock:[{
			imgcontainerdiv: "imagecenter1",
			imagestoshow: [{
				imgclass: "image90",
				imgid: "bag"
			}]
		}]
	},{
		// slide6
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s8
		}],
		imageblock:[{
			imgcontainerdiv: "imagecenter1",
			imagestoshow: [{
				imgclass: "image90",
				imgid: "bag"
			}]
		}]
	},{
		// slide7
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p1_s9
		},{
			textclass: "description fade_in",
			textdata: data.string.p1_s15
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "image20_a",
				imgid: "pencilbox"
			},{
				imgclass: "image20",
				imgid: "bag"
			},{
				imgclass: "click_indicator",
				imgid: "hand-icon"
			}],
			imagelabels: [{
				imagelabelclass: "imagelabel_yellow",
				imagelabeldata: data.string.p1_s10
			}]
		}]
	},{
		// slide8
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description fade_in",
			textdata: data.string.p1_s16
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblock:[{
			imagestoshow: [{
				imgclass: "image20_a",
				imgid: "pencilbox"
			},{
				imgclass: "image20",
				imgid: "bag"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "50gm", src: imgpath+"50gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "100gm", src: imgpath+"100gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "200gm", src: imgpath+"200gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "500gm", src: imgpath+"500gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1kg", src: imgpath+"1kg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencilbox", src: imgpath+"pencilbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weightmachinebase", src: imgpath+"weightmachinebase.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weight_plate", src: imgpath+"weight_plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "ding", src: soundAsset+"ding.ogg"},
			{id: "coin_drag", src: soundAsset+"coin_drag.ogg"},
			{id: "sound_1", src: soundAsset1+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset1+"s1_p2.ogg"},
			{id: "sound_3", src: soundAsset1+"s1_p3.ogg"},
			{id: "sound_4", src: soundAsset1+"s1_p4.ogg"},
			{id: "sound_4_1", src: soundAsset1+"s1_p4_1.ogg"},
			{id: "sound_4_2", src: soundAsset1+"s1_p4_2_aferclick.ogg"},
			{id: "sound_5", src: soundAsset1+"s1_p5.ogg"},
			{id: "sound_6", src: soundAsset1+"s1_p6.ogg"},
			{id: "sound_7", src: soundAsset1+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset1+"s1_p8.ogg"},
			{id: "sound_8_1", src: soundAsset1+"s1_p8_1.ogg"},
			{id: "sound_8_2", src: soundAsset1+"s1_p8_2.ogg"},
			{id: "sound_9", src: soundAsset1+"s1_p9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
			case 0:
	    	case 1:
	    	case 2:
    	 	case 5:
    	 	case 6:
    	 		sound_player("sound_"+(countNext+1),true);
	    		// $nextBtn.delay(400).show(0);
	    		break;
	    	case 3:
	    		sound_player_seq(["sound_4","sound_4_1"],false);
    	 		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $description = $(".description");
    	 		$($description[1]).hide(0);
    	 		$($description[2]).hide(0);
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		
	    		$(".image20, .click_indicator").click(function(){
                    sound_player("sound_4_2",true);
                    $(".click_indicator, .imagelabel_blue").hide(0);
	    			$($description[0]).hide(0);
	    			$($description[1]).show(0);
	    			$($description[2]).show(0);
	    			var $img = $(".image20");
	    			$img.detach().css({
	    				"top" : "unset", 
	    				"bottom" : "1%", 
	    				"width" : "40%", 
	    				"left" : "30%",
						"transform" : "unset"
	    				}).appendTo($dropleft);
	    			$leftpan.addClass("left_heavy_left_pan");
	    			$rightpan.addClass("left_heavy_right_pan");
	    			$needle.addClass("left_heavy_needle");
	    			$dropleft.addClass("left_heavy_drop_left");
	    		});
	    		break;
	    	case 4:
                sound_player("sound_"+(countNext+1),true);
                var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		var $img = $(".image20");
    			$img.detach().css({
    				"top" : "unset", 
    				"bottom" : "1%", 
    				"width" : "40%", 
    				"left" : "30%",
					"transform" : "unset"
    				}).appendTo($dropleft);
    			
    			$leftpan.css("top", "71.5%");
    			$rightpan.css("top", "56%");
    			$needle.css("transform", "rotate(322deg)");
    			$dropleft.css("top", "9%");
	    		break;
	    		
	    	case 7:
                sound_player_seq(["sound_8","sound_8_1"],false);

                var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $dropright = $(".drop_right");
    	 		var $description = $(".description");
    	 		$($description[1]).hide(0);
    	 		$($description[2]).hide(0);
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		
	    		$leftpan.css("top", "71.5%");
    			$rightpan.css("top", "56%");
    			$needle.addClass("right_needle_new_pos");
    			$dropleft.css("top", "9%");
    			
    			var $img = $(".image20_a");
    			$img.detach().css({
    				"top" : "unset", 
    				"bottom" : "1%", 
    				"width" : "40%", 
    				"left" : "30%",
					"transform" : "unset"
    				}).appendTo($dropleft);
	    		
	    		$dropright.css("top", "-10%");
	    		$(".image20, .click_indicator").click(function(){
                    sound_player("sound_8_2",true);
                    $(".click_indicator, .imagelabel_yellow").hide(0);
	    			$($description[0]).hide(0);
	    			$($description[1]).show(0);
	    			$($description[2]).show(0);
	    			var $img = $(".image20");
	    			$img.detach().css({
	    				"top" : "unset", 
	    				"bottom" : "2%", 
	    				"width" : "80%", 
	    				"left" : "10%",
						"transform" : "unset"
	    				}).appendTo($dropright);
	    			$leftpan.addClass("right_heavy_left_pan");
	    			$rightpan.addClass("right_heavy_right_pan");
	    			$needle.addClass("right_heavy_needle");
	    			$dropleft.addClass("right_heavy_drop_left");
	    			$dropright.addClass("right_heavy_drop_right");
	    		});
	    		break;
    	 	case 8:
    	 		sound_player("sound_9",true);
    	 		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $dropright = $(".drop_right");
    	 		var $description = $(".description");
    	 		$($description[1]).hide(0);
    	 		$($description[2]).hide(0);
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		
	    		$leftpan.css("top", "56%");
    			$rightpan.css("top", "71.5%");
    			$needle.css("transform", "rotate(406deg)");
    			$dropright.css("top", "9%");
    			$dropleft.css("top", "-10%");
    			
    			var $img = $(".image20_a");
    			$img.detach().css({
    				"top" : "unset", 
    				"bottom" : "1%", 
    				"width" : "40%", 
    				"left" : "30%",
					"transform" : "unset"
    				}).appendTo($dropleft);
	    		
	    		$img = $(".image20");
	    			$img.detach().css({
	    				"top" : "unset", 
	    				"bottom" : "2%", 
	    				"width" : "80%", 
	    				"left" : "10%",
						"transform" : "unset"
	    				}).appendTo($dropright);
	    		break;
	    	default:
	    		break;
	   }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id,nav){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
	  nav?navigationcontroller():""
	});
}
    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    navigationcontroller();
            }

        });
    }

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}					
				}
			}
		}
	}
	
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');


    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

//direct addition for jq animate portion
	   					// function animatefunction2($component, value){
	   						// $component.animate({  borderSpacing: value }, {
							    // step: function(now,fx) {
							      // $(this).css('top', now+'%');
							    // },
							    // duration: 400
							// },'linear');
	   					// }
//
	   					// function animatefunction($component, value, changerange){
	   						// $component.animate({  borderSpacing: changerange }, {
							    // step: function(now,fx) {
							      // $(this).css('top', (value+now)+'%');
							    // },
							    // duration: 400
							// },'linear');
	   					// }
//
	   				// if(weight_difference == 0){
   						// $beambalance_needle.animate({  borderSpacing: 0 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration: 400
						// },'linear');
						// animatefunction($beambalance_left, 64);
						// animatefunction($beambalance_right, 64);
						// animatefunction($drop_left, 64);
						// animatefunction($drop_right, 64);
//
//
//
	   				// }else if(weight_difference <= -2 || ((weightdropped == 0 || fruitsdropped == 0) && isleft)){
	   					// $beambalance_needle.animate({  borderSpacing: -42 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration:400
						// },'linear');
						// animatefunction($beambalance_left, 64, 8);
						// animatefunction($beambalance_right, 64, -8);
						// animatefunction($drop_left, 12, 8);
						// animatefunction($drop_right, 12, -8);
	   				// }else if(weight_difference >= 2 || ((weightdropped == 0 || fruitsdropped == 0) && isright)){
	   					// $beambalance_needle.animate({  borderSpacing: 42 }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration:400
						// },'linear');
						// animatefunction($beambalance_left, 64, -8);
						// animatefunction($beambalance_right, 64, 8);
						// animatefunction($drop_left, 12, -8);
						// animatefunction($drop_right, 12, 8);
	   				// }else{
	   					// var angle = Math.floor(21 * weight_difference);
//
	   					// var factor = fruitsdropped/weightdropped;
	   					// console.log("factor", factor);
	   					// $beambalance_needle.animate({  borderSpacing: angle }, {
						    // step: function(now,fx) {
						      // $(this).css('-webkit-transform','rotate('+now+'deg)');
						      // $(this).css('-moz-transform','rotate('+now+'deg)');
						      // $(this).css('transform','rotate('+now+'deg)');
						    // },
						    // duration: 1000
						// },'linear');
//
						// var valuechange = Math.floor(8 * weight_difference);
						// animatefunction2($beambalance_left, 64 - valuechange);
						// animatefunction2($beambalance_right, 64 + valuechange);
						// animatefunction2($drop_left, 64 - valuechange);
						// animatefunction2($drop_right, 64 + valuechange);
	   				// }



// function clearclasslist($class, length){
	   					// for (var i = 1 ; i < length; i++){
	   						// $class[0].classList[i] = "";
	   					// }
	   				// }
	   				// // var length = $beambalance_needle[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_needle, length): 1;
// //
	   				// // length = $beambalance_left[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_left, length): 1;
// //
	   				// // length = $beambalance_right[0].classList.length;
	   				// // (length > 1)? clearclasslist($beambalance_right, length): 1;
// //
	   				// // length = $drop_left[0].classList.length;
	   				// // (length > 1)? clearclasslist($drop_left, length): 1;
// //
	   				// // length = $drop_right[0].classList.length;
	   				// // (length > 1)? clearclasslist($drop_right, length): 1;
//
	   				// // $beambalance_needle[0].className = $beambalance_needle[0].classList[0] +" "+ $beambalance_needle[0].classList[1];
	   				// // $beambalance_left[0].className = $beambalance_left[0].classList[0] + $beambalance_left[0].classList[1];
	   				// // $beambalance_right[0].className = $beambalance_right[0].classList[0] + $beambalance_right[0].classList[1];
	   				// // $drop_left[0].className = $drop_left[0].classList[0] + $drop_left[0].classList[1];
	   				// // $drop_right[0].className = $drop_right[0].classList[0] + $drop_right[0].classList[1];
//
	   				// if(weight_difference == 0){
						// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate0");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_0");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_0");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_0");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_0");
	   				// }else if(weight_difference <= -2 || ((weightdropped == 0 || fruitsdropped == 0) && isleft)){
	   					// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8_ve");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8_ve");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8_ve");
	   				// }else if(weight_difference >= 2 || ((weightdropped == 0 || fruitsdropped == 0) && isright)){
						// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8");
						// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8_ve");
						// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8");
						// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8_ve");
						// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8");
	   				// }else{
	   					// if(fruitsdropped> weightdropped){
	   						// factor = Math.round((fruitsdropped / weightdropped )*10);
	   					// }else{
	   						// factor = Math.round(( weightdropped / fruitsdropped )*10);
	   					// }
//
	   					// switch (factor){
	   						// case 10:
	   						// case 11:
	   						// case 12:
	   						// case 13:
	   						// case 14:
	   						// case 15:
	   						// case 16:
	   						// case 17:
	   						// case 18:
	   						// case 19:
	   						// case 20:
//
	   							// $beambalance_needle.css({"-webkit-animation": "rotate_0 0.4s linear 1 forwards",
														// "animation": "rotate_0 0.4s linear 1 forwards" });
								// $beambalance_left.addClass("top_64_0").removeClass($beambalance_left[0].classList[1]);
								// $beambalance_right.addClass("top_64_0").removeClass($beambalance_right[0].classList[1]);
								// $drop_left.addClass("top_12_0").removeClass($drop_left[0].classList[1]);
								// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_0");
	   							// // break;
	   						// // case 11:
	   						// // case 9:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate1_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_1");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_1_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_1");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_1_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_1 0.4s linear 1 forwards",
															// "animation": "rotate_1 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_1_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_1").removeClass($beambalance_right[0].classList[1]);
									// $drop_left.addClass("top_12_1_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_1").removeClass($drop_right[0].classList[1]);
	   							// // }
// //
	   							// // break;
	   						// // case 12:
	   						// // case 8:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate2_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_2");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_2_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_2");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_2_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_2 0.4s linear 1 forwards",
															// "animation": "rotate_2 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_2_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_2").removeClass($beambalance_right[0]);
									// $drop_left.addClass("top_12_2_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_2").removeClass($drop_right[0].classList[1]);
	   							// // }
	   							// // break;
	   						// // case 13:
	   						// // case 7:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate3_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_3");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_3_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_3");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_3_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_3 0.4s linear 1 forwards",
															// "animation": "rotate_3 1.4s linear 1 forwards" });
									// $beambalance_left.addClass("top_64_3_ve").removeClass($beambalance_left[0].classList[1]);
									// $beambalance_right.addClass("top_64_3").removeClass($beambalance_right[0].classList[1]);
									// $drop_left.addClass("top_12_3_ve").removeClass($drop_left[0].classList[1]);
									// $drop_right.addClass("top_12_3").removeClass($drop_right[0].classList[1]);
	   							// // }
	   							// // break;
	   						// // case 14:
	   						// // case 6:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate4_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_4");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_4_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_4");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_4_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_4 0.4s linear 1 forwards",
															// "animation": "rotate_4 1.4s linear 1 forwards" });
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_4_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_4");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_4_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_4");
	   							// // }
	   							// // break;
	   						// // case 15:
	   						// // case 5:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate5_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_5");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_5_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_5");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_5_ve");
	   							// // }else{
	   								// $beambalance_needle.css({"-webkit-animation": "rotate_5 0.4s linear 1 forwards",
															// "animation": "rotate_5 1.4s linear 1 forwards" });
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_5_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_5");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_5_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_5");
	   							// // }
	   							// // break;
	   						// // case 16:
	   						// // case 4:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate6_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_6");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_6_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_6");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_6_ve");
	   							// // }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate6");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_6_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_6");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_6_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_6");
	   							// // }
	   							// // break;
	   						// // case 17:
	   						// // case 3:
	   							// // if(isleft){
	   								// // $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate7_ve");
									// // $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_7");
									// // $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_7_ve");
									// // $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_7");
									// // $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_7_ve");
	   							// // }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate7");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_7_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_7");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_7_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_7");
	   							// // }
	   							// break;
	   						// default:
	   							// if(isleft){
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8_ve");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8_ve");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8_ve");
	   							// }else{
	   								// $beambalance_needle.removeClass($beambalance_needle[0].classList[1]).addClass("rotate8");
									// $beambalance_left.removeClass($beambalance_left[0].classList[1]).addClass("top_64_8_ve");
									// $beambalance_right.removeClass($beambalance_right[0].classList[1]).addClass("top_64_8");
									// $drop_left.removeClass($drop_left[0].classList[1]).addClass("top_12_8_ve");
									// $drop_right.removeClass($drop_right[0].classList[1]).addClass("top_12_8");
	   							// }
	   							// break;
	   					// }
	   					// console.log("factor", factor);
	   				// }

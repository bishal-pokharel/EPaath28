var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+ $lang+"/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			}]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p5_s0
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p5_s1
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p5_s2
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p5_s3
		}],
//
//
		// uppertextblockadditionalclass: "utb_info",
		// uppertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s6
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s7
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s8
		// }],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "brocaulicollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "applescollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[
			{
				imgclass: "dhak5 weightdrop",
				imgid: "1kg"
			},{
				imgclass: "click_indicator",
				imgid: "hand-icon"
			}]
		}],
		// lowertextblockadditionalclass: "orange_ltb",
		// lowertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s5
		// }]
	},{
		// slide4
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p5_s6
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p6_s1
		}]
	},{
		// slide6
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p6_s2
		}],
//
//
		// uppertextblockadditionalclass: "utb_info",
		// uppertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s6
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s7
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s8
		// }],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "brocaulicollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "applescollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[
			{
				imgclass: "dhak4 weightdrop",
				imgid: "500gm"
			},{
				imgclass: "click_indicator",
				imgid: "hand-icon"
			}]
		}],
		// lowertextblockadditionalclass: "orange_ltb",
		// lowertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s5
		// }]
	},{
		// slide7
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p6_s5
		}]
	},{
		// slide8
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p7_s1
		}]
	},{
		// slide9
		contentnocenteradjust: true,
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p7_s2
		}],
//
//
		// uppertextblockadditionalclass: "utb_info",
		// uppertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s6
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s7
		// },{
			// textclass: "description2",
			// textdata: data.string.p1_s8
		// }],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "brocaulicollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "applescollection",
			imagestoshow:[{
				imgclass: "platebrocauli",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak3 weightdrop",
				imgid: "200gm"
			},{
				imgclass: "click_indicator",
				imgid: "hand-icon"
			}]
		}],
		// lowertextblockadditionalclass: "orange_ltb",
		// lowertextblock:[{
			// textclass: "description",
			// textdata: data.string.p1_s5
		// }]
	},{
		// slide10
		contentnocenteradjust: true,
		imageblock:[{
			imagestoshow: [{
				imgclass: "sunder_class",
				imgid: "sunder"
			},{
				imgclass: "rack_class",
				imgid: "rack"
			}]
		},{
			imageblockclass: "container_1"
		},{
			imageblockclass: "container_2"
		},{
			imageblockclass: "container_3"
		},{
			imageblockclass: "container_4"
		}],
		containsdialog: [{
			dialogcontainer: "dialog1",
			dialogimageclass: "dialog_image1",
			dialogueimgid: "dialogue_bubble",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p7_s5
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
    var fruit = 1;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
	var text1;
//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "50gm", src: imgpath+"50gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "100gm", src: imgpath+"100gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "200gm", src: imgpath+"200gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "500gm", src: imgpath+"500gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1kg", src: imgpath+"1kg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brocauli", src: imgpath+"brocauli.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"cauliflwoer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plate", src: imgpath+"plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weightmachinebase", src: imgpath+"weightmachinebase.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weight_plate", src: imgpath+"weight_plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sunder", src: imgpath+"sunder.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rack", src: imgpath+"rack.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialogue_bubble", src: imgpath+"dialogue_bubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
		  {id: "ding", src: soundAsset+"ding.ogg"},
      {id: "coindrag", src: soundAsset+"coin_drag.ogg"},
      {id: "sound_1", src: soundAsset1+"s5_p1.ogg"},
      {id: "sound_2", src: soundAsset1+"s5_p2.ogg"},
      {id: "sound_3", src: soundAsset1+"s5_p3.ogg"},
      {id: "sound_4", src: soundAsset1+"s5_p4.ogg"},
      {id: "sound_4_1", src: soundAsset1+"s5_p4_afterclick.ogg"},
      {id: "sound_4_2", src: soundAsset1+"s5_p4_afterclick_1.ogg"},
      {id: "sound_5", src: soundAsset1+"s5_p5.ogg"},
      {id: "sound_6", src: soundAsset1+"s5_p6.ogg"},
      {id: "sound_7", src: soundAsset1+"s5_p7.ogg"},
      {id: "sound_7_1", src: soundAsset1+"s5_p7_afterclick.ogg"},
      {id: "sound_8", src: soundAsset1+"s5_p8.ogg"},
      {id: "sound_9", src: soundAsset1+"s5_p9.ogg"},
      {id: "sound_10", src: soundAsset1+"s5_p10.ogg"},
      {id: "sound_a10", src: soundAsset1+"s5_p10_1_apple.ogg"},
      {id: "sound_o10", src: soundAsset1+"s5_p10_1_orange.ogg"},
      {id: "sound_a11", src: soundAsset1+"s5_p11_apple.ogg"},
      {id: "sound_o11", src: soundAsset1+"s5_p11_orange.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var $mangocollection;
 var $orangecollection;
 var $applecollection;
 var $brocaulicollection;

var fronttag = "<img class = '";
var midtag = "' src = ";
var endtag = "></img>";

 function fillcollection(){
	for(var i = 1; i <= 6; i++){
		$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult("mango").src + endtag);
		$orangecollection.append(fronttag +"orange fruit"+ midtag + preload.getResult("orange").src + endtag);
		$applecollection.append(fronttag +"apple fruit"+ midtag + preload.getResult("apple").src + endtag);
		$brocaulicollection.append(fronttag +"brocauli fruit"+ midtag + preload.getResult("brocauli").src + endtag);
	}

	positioncalcn();

 }

 function fillcollection2(){
 	var $container_1 = $(".container_1");
 	var $container_2 = $(".container_2");
 	var $container_3 = $(".container_3");
 	var $container_4 = $(".container_4");
	for(var i = 1; i <= 6; i++){
		$container_1.append(fronttag +"mango fruit"+ midtag + preload.getResult("mango").src + endtag);
		$container_2.append(fronttag +"orange fruit"+ midtag + preload.getResult("orange").src + endtag);
		$container_3.append(fronttag +"apple fruit"+ midtag + preload.getResult("apple").src + endtag);
		$container_4.append(fronttag +"brocauli fruit"+ midtag + preload.getResult("brocauli").src + endtag);
	}

	positioncalcn();

 }

 function positioncalcn(){
 	var $mango = $(".mango");
	var $orange = $(".orange");
	var $apple = $(".apple");
	var $brocauli = $(".brocauli");

	var bottom = 14;
	var left = 10;
	var z_index = 110;
	var bottom_addition = 0;
	for(var i = 0; i < $mango.length; i++){
		if(i== 3){
			bottom = 28;
			left = 19.6;
			z_index = 109;
			bottom_addition = 5;
		}else if(i == 5){
			bottom = 40;
			left = 28.5;
			z_index = 108;
			bottom_addition += 5;
		}else if(i == 6){
			bottom = 45;
			left = 36;
			z_index = 107;
			bottom_addition += 5;
		}else if(i == 14){
			bottom = 50;
			left = 42.4;
			z_index = 106;
			bottom_addition += 5;
		}
		$($mango[i]).css({
			"position": "absolute",
			"width": "40%",
			"left": left+"%",
			"bottom": bottom+"%",
			"z-index": z_index
		});

		$($orange[i]).css({
			"position": "absolute",
			"width": "35%",
			"left": left+"%",
			"bottom": (bottom+bottom_addition)+"%",
			"z-index": z_index
		});

		$($apple[i]).css({
			"position": "absolute",
			"width": "38%",
			"left": left+"%",
			"bottom": (bottom+bottom_addition)+"%",
			"z-index": z_index
		});

		$($brocauli[i]).css({
			"position": "absolute",
			"width": "38%",
			"left": left+"%",
			"bottom": bottom+"%",
			"z-index": z_index
		});
		left += 22;
	}
 }
 var clicked_fruit_index = 0;
 var text, text1;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));

	    switch(countNext){
	    	case 0:
	    		sound_player("sound_1",true);
	    		$(".contentblock").css({ "background-size": "100% 100%",
	    						  "background-image": "url('"+ preload.getResult("bg01").src +"')",
	    						  "": ""});
	    		break;
	    	case 1:
	    	case 2:
	    	case 4:
	    	case 5:
	    	case 7:
                sound_player("sound_"+(countNext+1),true);
                fillcollection2();
	    		$(".contentblock").css({ "background-size": "100% 100%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "": ""});
	    		break;

	    	case 8:
	    		sound_player("sound_9",false);
	    		fillcollection2();
	    		$(".contentblock").css({ "background-size": "100% 100%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "": ""});
	    		$(".container_2>img, .container_3>img").addClass("highlight");
	    		$(".container_2, .container_3").click(function(){
	    			var $this = $(this);
	    			if($this.hasClass("container_2")){
	    				clicked_fruit_index = 1;
	    			}else{
	    				clicked_fruit_index = 2;
	    			}
	    			$(".rack_class, .container_1, .container_2, .container_3, .container_4").addClass("slideout");
		    		timeoutcontroller = setTimeout(function(){
						$nextBtn.trigger('click');
		    		}, 1300);
	    		});
	    		break;
	    	case 3:
                sound_player("sound_"+(countNext+1),false);
	    	/*
				mango is caluliflower
			*/

				lefthasweight = false;
	   			righthasweight = false;
	   			fruitsdropped = 0;
	   			weightdropped = 0;
	    		$(".contentblock").css({ "background-size": "110% 120%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "background-position": "0% 92%",
	    						  "background-repeat": "no-repeat" });

	    		$mangocollection = $(".mangoescollection");
	    		$orangecollection = $(".orangescollection");
	    		$applecollection = $(".applescollection");
	    		$brocaulicollection = $(".brocaulicollection");

	    		fillcollection();

	    		$(".orangescollection, .brocaulicollection, .applescollection").hide(0);
	    		$(".dhak5").css({
	    			    "left" : "20%",
    					"bottom" : "50%"
	    		});
	    		$(".click_indicator").css({
	    			    "left" : "21%",
					    "z-index" : "110",
					    "top" : "40%"

	    		});

	    		var count = 0;

	    		$(".click_indicator, .dhak5").click(function(){
	    			setTimeout(function () {
                        sound_player("sound_"+(countNext+1)+"_1",false);
                    },1500);
                    $(".click_indicator").hide(0);
	    			handleCardDrop2( $(".dhak5"),/*ui,*/ false, true);
	    			$(".descriptionheader").animate({
					    opacity: 0
					  }, 1000, function() {
	    				$(".descriptionheader").html(data.string.p5_s4);
	    				$(".descriptionheader").animate({
					    	opacity: 1
					  	}, 1000, 'easeOutBounce');
					  });
	    			$(".mango").click(function(){
	    				if(count >= 2){
	    					return true;
	    				}
	    				count++;
	    				if(count == 2){
                            setTimeout(function () {
                                sound_player("sound_4_2",true);
                            },1500);
	    					$(".descriptionheader").html(data.string.p5_s5).css({
	    						"color" : "#303030",
	    						"background-color" : "#FFF2CC",
	    						"width" : "100%",
	    						"border-radius" : "0",
	    						"top" : "0",
	    						"padding" : "10px",
	    						"left" : "0"
	    					});
	    				}
		    			handleCardDrop2( $(this).clone(),/*ui,*/ true, false);
		    			$(this).hide(0);
	    			});
	    		});

	    		break;

	    	case 6:
	    		sound_player("sound_7",false);
	    	/*
				mango is caluliflower
			*/

				lefthasweight = false;
	   			righthasweight = false;
	   			fruitsdropped = 0;
	   			weightdropped = 0;
	    		$(".contentblock").css({ "background-size": "110% 120%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "background-position": "0% 92%",
	    						  "background-repeat": "no-repeat" });

	    		$mangocollection = $(".mangoescollection");
	    		$orangecollection = $(".orangescollection");
	    		$applecollection = $(".applescollection");
	    		$brocaulicollection = $(".brocaulicollection");

	    		fillcollection();

	    		$(".orangescollection, .mangoescollection, .applescollection").hide(0);
	    		$(".dhak4").css({
	    			    "left" : "20%",
    					"bottom" : "50%"
	    		});
	    		$(".click_indicator").css({
	    			    "left" : "21%",
					    "z-index" : "110",
					    "top" : "40%"

	    		});

	    		var count = 0;

	    		$(".click_indicator, .dhak4").click(function(){
	    			setTimeout(function () {
                        sound_player("sound_7_1",false);
                    },2000);
                    $(".click_indicator").hide(0);
	    			handleCardDrop2( $(".dhak4"),/*ui,*/ false, true);
	    			$(".descriptionheader").animate({
					    opacity: 0
					  }, 1000, function() {
	    				$(".descriptionheader").html(data.string.p6_s3);
	    				$(".descriptionheader").animate({
					    	opacity: 1
					  	}, 1000, 'easeOutBounce');
					  });
	    			$(".brocauli").click(function(){
	    				if(count >= 4){
	    					return true;
	    				}
	    				count++;
	    				if(count == 4){
	    					// $(".descriptionheader").html(data.string.p5_s5).css({
	    						// "color" : "#303030",
	    						// "background-color" : "#FFF2CC",
	    						// "width" : "100%",
	    						// "border-radius" : "0",
	    						// "top" : "0",
	    						// "padding" : "10px",
	    						// "left" : "0"
	    					// });
	    					$nextBtn.show(0);
	    				}
		    			handleCardDrop2( $(this).clone(),/*ui,*/ true, false);
		    			$(this).hide(0);
	    			});
	    		});

	    		break;
	    	case 9:
                sound_player("sound_10",false);
                /*
                    mango is caluliflower
                */

				lefthasweight = false;
	   			righthasweight = false;
	   			fruitsdropped = 0;
	   			weightdropped = 0;
	    		$(".contentblock").css({ "background-size": "110% 120%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "background-position": "0% 92%",
	    						  "background-repeat": "no-repeat" });

	    		$mangocollection = $(".mangoescollection");
	    		$orangecollection = $(".orangescollection");
	    		$applecollection = $(".applescollection");
	    		$brocaulicollection = $(".brocaulicollection");

	    		fillcollection();
	    		var $fruit;
	    		switch (clicked_fruit_index){
	    			case 1:
                        $(".brocaulicollection, .mangoescollection, .applescollection").hide(0);
			    		$fruit = $(".orange");
							text = "orange";
			    		 text1 = data.string.ornagema;
			    		limit = 2;
	    				break;
	    			case 2:
	    				fruit=2;
                        $(".orangescollection, .mangoescollection, .brocaulicollection").hide(0);
			    		$fruit = $(".apple");
			    		text = "apple";
			    		 text1 = data.string.applema;
			    		limit = 4;
	    				break;
	    			default:
	    			 	break;
	    		}
	    		$(".dhak3").css({
	    			    "left" : "20%",
    					"bottom" : "50%"
	    		});
	    		$(".click_indicator").css({
	    			    "left" : "22%",
					    "z-index" : "110",
					    "top" : "44%"
	    		});

	    		var count = 0;

	    		$(".click_indicator, .dhak3").click(function(){
                    setTimeout(function () {
                        fruit==1?sound_player("sound_o10",false):sound_player("sound_a10",false);
                    },1500);
	    			$(".click_indicator").hide(0);
	    			handleCardDrop2( $(".dhak3"),/*ui,*/ false, true);
	    			$(".descriptionheader").animate({
					    opacity: 0
					  }, 1000, function() {
	    				$(".descriptionheader").html(data.string.p7_s4_a+ text1 +data.string.p7_s4_b);
	    				$(".descriptionheader").animate({
					    	opacity: 1
					  	}, 1000, 'easeOutBounce');
					  });
	    			$fruit.click(function(){
	    				if(count >= limit){
	    					return true;
	    				}
	    				count++;
	    				if(count == limit){
	    					// $(".descriptionheader").html(data.string.p5_s5).css({
	    						// "color" : "#303030",
	    						// "background-color" : "#FFF2CC",
	    						// "width" : "100%",
	    						// "border-radius" : "0",
	    						// "top" : "0",
	    						// "padding" : "10px",
	    						// "left" : "0"
	    					// });
	    					$nextBtn.show(0);
	    				}
		    			handleCardDrop2( $(this).clone(),/*ui,*/ true, false);
		    			$(this).hide(0);
	    			});
	    		});

	    		break;
	    	case 10:
                fruit==1?sound_player("sound_o11",false):sound_player("sound_a11",false);
                fillcollection2();
	    		$(".contentblock").css({ "background-size": "100% 100%",
	    						  "background-image": "url('"+ preload.getResult("bg02").src +"')",
	    						  "": ""});
	    		// $(".rack_class, .container_1, .container_2, .container_3, .container_4").addClass("slideout");
	    		$lang=="en"?$(".textfill").html(data.string.p7_s5 + text1+"."):$(".textfill").html(data.string.p7_s5 + text1+data.string.p7_s5_1);
	    		// $(".textfill").html(data.string.p7_s5);
	    		ole.footerNotificationHandler.pageEndSetNotification();
	    		break;
	    	default:
	    		break;

	    }
  }

/*=====  End of Templates Block  ======*/


				var lefthasweight = false;
	   			var righthasweight = false;
	   			var fruitsdropped = 0;
	   			var weightdropped = 0;

	   			function handleCardDrop2( $droppedclone, isleft, isright){
	   				// var $droppedclone = $($(ui.helper).clone());
	   				// var $droppedclone = $($(ui.helper).clone());
	   				var droppedisweight = $droppedclone.hasClass("weightdrop");

	   				if(lefthasweight || righthasweight){
	   					if(droppedisweight){
	   						if(righthasweight && isleft){
	   							return true;
	   						}else if(lefthasweight && isright){
	   							return true;
	   						}
	   					}else {
	   						if(isleft && lefthasweight){
	   							return true;
	   						}else if(isright && righthasweight){
	   							return true;
	   						}
	   					}
	   				}else{
	   					if(droppedisweight) {
	   						lefthasweight = (isleft)? true: false;
	   						righthasweight = (isright)? true: false;
	   					} else {
	   						lefthasweight = (isleft)? false: true;
	   						righthasweight = (isright)? false: true;
	   					}
	   				}


	   				var $leftimages = $(".drop_left> img");
	   				var $rightimages = $(".drop_right> img");

	   				if(droppedisweight){

	   					var weightclassName = $droppedclone[0].classList[0];
	   					var z_index = 0;
	   					var bottom = 0;
	   					var left = 0;
	   					var width = 0;
							sound_player("coindrag");
	   					switch(weightclassName){
		   					case "dhak1":
		   						weightdropped += 50;
		   						z_index = 20;
		   						width = 20;
		   						break;

		   					case "dhak2":
		   						weightdropped += 100;
		   						z_index = 18;
		   						width = 20;
		   						break;

		   					case "dhak3":
		   						weightdropped += 200;
		   						z_index = 16;
		   						width = 14;
		   						break;

		   					case "dhak4":
		   						weightdropped += 500;
		   						z_index = 14;
		   						width = 17;
		   						break;

		   					case "dhak5":
		   						weightdropped += 1000;
		   						z_index = 12;
		   						width = 20;
		   						break;

		   					default:
		   						break;
		   				}

		   				if(lefthasweight){
		   					bottom = 8 + ($leftimages.length/5) * 5;
		   					left = 12.6 + 15.2 * ($leftimages.length % 5);
		   					$droppedclone.detach().css({
		   							"z-index":z_index,
		   							"top":"initial",
		   							"bottom": bottom+"%",
		   							"left":left+"%",
		   							"width": width+"%"
		   						}).appendTo($(".drop_left"));
		   				}else{
		   					bottom = 8 + ($rightimages.length/5) * 5;
		   					left = 12.6 + 15.2 * ($rightimages.length % 5);
		   					$droppedclone.detach().css({
		   							"z-index":z_index,
		   							"top":"initial",
		   							"bottom": bottom+"%",
		   							"left":left+"%",
		   							"width": width+"%"
		   						}).appendTo($(".drop_right"));
		   				}

		   				var kg_dropped = Math.floor(weightdropped/1000);
		   				$(".kg_class").html(kg_dropped);
		   				$(".gm_class").html(weightdropped - (kg_dropped*1000));

		   					$droppedclone.click(function (){
			   					var $this = $(this);
									sound_player("coindrag");
		   						switch($this[0].classList[0]){
				   					case "dhak1":
				   						weightdropped -= 50;
				   						break;

				   					case "dhak2":
				   						weightdropped -= 100;
				   						break;

				   					case "dhak3":
				   						weightdropped -= 200;
				   						break;

				   					case "dhak4":
				   						weightdropped -= 500;
				   						break;

				   					case "dhak5":
				   						weightdropped -= 1000;
				   						break;

				   					default:
				   						break;
				   				}

			   					$this.remove();
			   					if(weightdropped == 0 && fruitsdropped == 0){
			   						lefthasweight = false;
			   						righthasweight = false;
			   					}
			   					var kg_dropped = Math.floor(weightdropped/1000);
				   				$(".kg_class").html(kg_dropped);
				   				$(".gm_class").html(weightdropped - (kg_dropped*1000));
				   				if(lefthasweight){
				   					rearangeimages("drop_left");
			   					}else{
			   						rearangeimages("drop_right");
			   					}

			   					recalculatepositions();

			   				});

	   				}else{
	   					var fruitclassName = $droppedclone[0].classList[0];
		   				switch(fruitclassName){
		   					case "mango":
		   						fruitsdropped += 500;
		   						break;

		   					case "orange":
		   						fruitsdropped += 100;
		   						break;

		   					case "apple":
			   						fruitsdropped += 50;
			   						break;

		   					case "brocauli":
		   						fruitsdropped += 125;
		   						break;

		   					default:
		   						break;
		   				}
		   				if(lefthasweight){
		   					$droppedclone.detach().appendTo($(".drop_right"));
		   					rearangeimages("drop_right");
		   					addclicklistener($droppedclone, "drop_right");

		   				}else{
		   					$droppedclone.detach().appendTo($(".drop_left"));
		   					rearangeimages("drop_left");
		   					addclicklistener($droppedclone, "drop_right");

		   				}

	   				}

	   				//direct addition for jq animate portion
	   				recalculatepositions();
	   			}

	   			var leftplatetop = 64;
	   			var rightplatetop = 64;
	   			var anglevalue = 360;
	   			var rightdroppabletop = 2;
	   			var leftdroppabletop = 2;

	   			function recalculatepositions(){
	   				var weight_difference = weightdropped - fruitsdropped;
	   				var $beambalance_needle = $(".beambalance_needle");
	   				var $beambalance_left = $(".beambalance_left");
	   				var $beambalance_right = $(".beambalance_right");
	   				var $drop_left = $(".drop_left");
	   				var $drop_right = $(".drop_right");
	   				console.log("weight : "+ weightdropped, " items : "+ fruitsdropped);

	   				var weightfactor = 10;
	   				var weightismore = false;
	   				if(weightdropped == fruitsdropped){
	   					if(weightdropped > 0 && fruitsdropped > 0){
								sound_player("ding");
	   						// $nextBtn.show(0);
	   					}

	   					weightfactor = 10;
	   				}else if(weightdropped > fruitsdropped){
	   					if(fruitsdropped == 0){
	   						weightfactor = 25;
	   					}else{
	   						weightfactor = Math.round((weightdropped/fruitsdropped)*10);
	   					}

	   				}else{
	   					if(weightdropped == 0){
	   						weightfactor = 25;
	   					}else{
	   						weightfactor = Math.round((fruitsdropped/weightdropped)*10);
	   					}

	   				}

		   				$beambalance_needle.removeClass($beambalance_needle[0].classList[1]);
						$beambalance_left.removeClass($beambalance_left[0].classList[1]);
						$beambalance_right.removeClass($beambalance_right[0].classList[1]);
						$drop_left.removeClass($drop_left[0].classList[1]);
						$drop_right.removeClass($drop_right[0].classList[1]);

	   				var rotateflag = false;
					if(lefthasweight){
						if(weightdropped > fruitsdropped){
							rotateflag = true;
						}else{
							rotateflag = false;
						}
					}else{
						if(weightdropped > fruitsdropped){
							rotateflag = false;
						}else{
							rotateflag = true;
						}
					}


	   				switch(weightfactor){
	   					case 10:
	   							$beambalance_needle.addClass("rotate0");
								$beambalance_left.addClass("top_64_0");
								$beambalance_right.addClass("top_64_0");
								$drop_left.addClass("top_12_0");
								$drop_right.addClass("top_12_0");
								anglevalue = 360;
								leftplatetop = 64;
					   			rightplatetop = 64;
					   			rightdroppabletop = 2;
					   			leftdroppabletop = 2;
								break;
						case 11:
						case 12:
							if(rotateflag){
								$beambalance_needle.addClass("rotate1_ve");
								$beambalance_left.addClass("top_64_1");
								$beambalance_right.addClass("top_64_1_ve");
								$drop_left.addClass("top_12_1");
								$drop_right.addClass("top_12_1_ve");
								anglevalue = 355;
								leftplatetop = 65;
					   			rightplatetop = 63;
					   			leftdroppabletop = 3;
					   			rightdroppabletop = 1;
							}else{
								$beambalance_needle.addClass("rotate1");
								$beambalance_left.addClass("top_64_1_ve");
								$beambalance_right.addClass("top_64_1");
								$drop_left.addClass("top_12_1_ve");
								$drop_right.addClass("top_12_1");
								anglevalue = 365;
								leftplatetop = 63;
					   			rightplatetop = 65;
					   			leftdroppabletop = 1;
					   			rightdroppabletop = 3;
							}
								break;
						case 13:
						case 14:
							if(rotateflag){
								$beambalance_needle.addClass("rotate2_ve");
								$beambalance_left.addClass("top_64_2");
								$beambalance_right.addClass("top_64_2_ve");
								$drop_left.addClass("top_12_2");
								$drop_right.addClass("top_12_2_ve");
								anglevalue = 350;
								leftplatetop = 66;
					   			rightplatetop = 62;
					   			leftdroppabletop = 4;
					   			rightdroppabletop = 0;
							}else{
								$beambalance_needle.addClass("rotate2");
								$beambalance_left.addClass("top_64_2_ve");
								$beambalance_right.addClass("top_64_2");
								$drop_left.addClass("top_12_2_ve");
								$drop_right.addClass("top_12_2");
								anglevalue = 370;
								leftplatetop = 62;
					   			rightplatetop = 66;
					   			leftdroppabletop = 0;
					   			rightdroppabletop = 4;
							}

								break;
						case 15:
						case 16:
							if(rotateflag){
								$beambalance_needle.addClass("rotate3_ve");
								$beambalance_left.addClass("top_64_3");
								$beambalance_right.addClass("top_64_3_ve");
								$drop_left.addClass("top_12_3");
								$drop_right.addClass("top_12_3_ve");
								anglevalue = 345;
								leftplatetop = 67;
					   			rightplatetop = 61;
					   			leftdroppabletop = 5;
					   			rightdroppabletop = -1;
							}else{
								$beambalance_needle.addClass("rotate3");
								$beambalance_left.addClass("top_64_3_ve");
								$beambalance_right.addClass("top_64_3");
								$drop_left.addClass("top_12_3_ve");
								$drop_right.addClass("top_12_3");
								anglevalue = 375;
								leftplatetop = 61;
					   			rightplatetop = 67;
					   			leftdroppabletop = -1;
					   			rightdroppabletop = 5;
							}
								break;
						case 17:
						case 18:
							if(rotateflag){
								$beambalance_needle.addClass("rotate4_ve");
								$beambalance_left.addClass("top_64_4");
								$beambalance_right.addClass("top_64_4_ve");
								$drop_left.addClass("top_12_4");
								$drop_right.addClass("top_12_4_ve");
								anglevalue = 340;
								leftplatetop = 68;
					   			rightplatetop = 60;
					   			leftdroppabletop = 6;
					   			rightdroppabletop = -2;
							}else{
								$beambalance_needle.addClass("rotate4");
								$beambalance_left.addClass("top_64_4_ve");
								$beambalance_right.addClass("top_64_4");
								$drop_left.addClass("top_12_4_ve");
								$drop_right.addClass("top_12_4");
								anglevalue = 380;
								leftplatetop = 60;
					   			rightplatetop = 68;
					   			leftdroppabletop = -2;
					   			rightdroppabletop = 6;
							}

								break;
						case 19:
						case 20:
							if(rotateflag){

								$beambalance_needle.addClass("rotate5_ve");
								$beambalance_left.addClass("top_64_5");
								$beambalance_right.addClass("top_64_5_ve");
								$drop_left.addClass("top_12_5");
								$drop_right.addClass("top_12_5_ve");
								anglevalue = 335;
								leftplatetop = 69;
					   			rightplatetop = 59;
					   			leftdroppabletop = 7;
					   			rightdroppabletop = -3;
							}else{
								$beambalance_needle.addClass("rotate5");
								$beambalance_left.addClass("top_64_5_ve");
								$beambalance_right.addClass("top_64_5");
								$drop_left.addClass("top_12_5_ve");
								$drop_right.addClass("top_12_5");
								anglevalue = 385;
								leftplatetop = 59;
					   			rightplatetop = 69;
					   			leftdroppabletop = -3;
					   			rightdroppabletop = 7;
							}
								break;
						case 21:
						case 22:
							if(rotateflag){
								$beambalance_needle.addClass("rotate6_ve");
								$beambalance_left.addClass("top_64_6");
								$beambalance_right.addClass("top_64_6_ve");
								$drop_left.addClass("top_12_6");
								$drop_right.addClass("top_12_6_ve");
								anglevalue = 330;
								leftplatetop = 70;
					   			rightplatetop = 58;
					   			leftdroppabletop = 8;
					   			rightdroppabletop = -4;
							}else{
								$beambalance_needle.addClass("rotate6");
								$beambalance_left.addClass("top_64_6_ve");
								$beambalance_right.addClass("top_64_6");
								$drop_left.addClass("top_12_6_ve");
								$drop_right.addClass("top_12_6");
								anglevalue = 390;
								leftplatetop = 58;
					   			rightplatetop = 70;
					   			leftdroppabletop = -4;
					   			rightdroppabletop = 8;
							}

								break;
						case 22:
						case 23:
							if(rotateflag){
								$beambalance_needle.addClass("rotate7_ve");
								$beambalance_left.addClass("top_64_7");
								$beambalance_right.addClass("top_64_7_ve");
								$drop_left.addClass("top_12_7");
								$drop_right.addClass("top_12_7_ve");
								anglevalue = 325;
								leftplatetop = 71;
					   			rightplatetop = 57;
					   			leftdroppabletop = 9;
					   			rightdroppabletop = -5;
							}else{
								$beambalance_needle.addClass("rotate7");
								$beambalance_left.addClass("top_64_7_ve");
								$beambalance_right.addClass("top_64_7");
								$drop_left.addClass("top_12_7_ve");
								$drop_right.addClass("top_12_7");
								anglevalue = 395;
								leftplatetop = 57;
					   			rightplatetop = 71;
					   			leftdroppabletop = -5;
					   			rightdroppabletop = 9;
							}
								break;
						default:
								if(rotateflag){
									$beambalance_needle.addClass("rotate8_ve");
									$beambalance_left.addClass("top_64_8");
									$beambalance_right.addClass("top_64_8_ve");
									$drop_left.addClass("top_12_8");
									$drop_right.addClass("top_12_8_ve");
									anglevalue = 318;
									leftplatetop = 72;
						   			rightplatetop = 56;
						   			leftdroppabletop = 10;
						   			rightdroppabletop = -6;
								}else{
									$beambalance_needle.addClass("rotate8");
									$beambalance_left.addClass("top_64_8_ve");
									$beambalance_right.addClass("top_64_8");
									$drop_left.addClass("top_12_8_ve");
									$drop_right.addClass("top_12_8");
									anglevalue = 402;
									leftplatetop = 56;
						   			rightplatetop = 72;
						   			leftdroppabletop = -6;
						   			rightdroppabletop = 10;
								}
							break;
	   				}
	   				setTimeout(function(){
	   					$beambalance_needle.css("transform","rotate("+anglevalue+"deg)");
	   					$beambalance_left.css("top", leftplatetop+"%");
	   					$beambalance_right.css("top", rightplatetop+"%");
	   					$drop_left.css("top", leftdroppabletop+"%");
	   					$drop_right.css("top",rightdroppabletop+"%");
	   				},450);

	   			}

	   			function addclicklistener($droppedclone, classname){
	   					// $droppedclone.click(function (){
		   					// var $this = $(this);
	   						// switch($this[0].classList[0]){
			   					// case "mango":
			   						// fruitsdropped -= 500;
			   						// break;
//
			   					// case "orange":
			   						// fruitsdropped -= 100;
			   						// break;
//
			   					// case "apple":
			   						// fruitsdropped -= 50;
			   						// break;
//
			   					// case "brocauli":
			   						// fruitsdropped -= 125;
			   						// break;
//
			   					// default:
			   						// break;
			   				// }
//
		   					// $this.remove();
		   					// if(weightdropped == 0 && fruitsdropped == 0){
			   						// lefthasweight = false;
			   						// righthasweight = false;
			   					// }
		   					// if(lefthasweight){
		   						// rearangeimages("drop_right");
		   					// }else{
		   						// rearangeimages("drop_left");
		   					// }
//
		   					// recalculatepositions();
		   				// });
	   			}


	   			function rearangeimages(classname){
	   				var $content = $("."+classname+"> img");
	   				bottom = 8;
		    		left = 10;
		    		z_index = 110;
		    		bottom_addition = 0;
	   				for (var i = 0; i < $content.length; i++){

	   					if(i== 3){
		    				bottom = 28;
		    				left = 19.6;
		    				z_index = 109;
		    				bottom_addition = 5;
		    			}else if(i == 5){
		    				bottom = 40;
		    				left = 28.5;
		    				z_index = 108;
		    				bottom_addition += 5;
		    			}else if(i == 6){
		    				bottom = 45;
		    				left = 36;
		    				z_index = 107;
		    				bottom_addition += 5;
		    			}else if(i == 14){
		    				bottom = 50;
		    				left = 42.4;
		    				z_index = 106;
		    				bottom_addition += 5;
		    			}
	   					$($content[i]).css({
		    				"position": "absolute",
		    				"width": "35%",
		    				"left": left+"%",
		    				"bottom": (bottom+bottom_addition)+"%",
		    				"z-index": z_index,
		    				"top": "initial"
	    				});
	    				left += 22;
	   				}
	   			}


function sound_player(sound_id,navigate){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		navigate?navigationcontroller():"";
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}

		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			switch(countNext){
		    	case 2:
		    	case 5:
		    		$(".rack_class, .container_1, .container_2, .container_3, .container_4").addClass("slideout");
		    		countNext++;
		    		$nextBtn.hide(0);
		    		timeoutcontroller = setTimeout(function(){
						templateCaller();
		    		}, 1300);
		    		break;
		    	default:
					countNext++;
					templateCaller();
		    		break;
			}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

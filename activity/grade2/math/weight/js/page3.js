var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s0
		}],
		imageblock: [{
			imagestoshow: [{
				imgclass: "calibrated_weight1",
				imgid: "1kg"
			},{
				imgclass: "calibrated_weight2",
				imgid: "500gm"
			},{
				imgclass: "calibrated_weight3",
				imgid: "200gm"
			},{
				imgclass: "calibrated_weight4",
				imgid: "100gm"
			}]
		}]
	},
	{
		// slide1
		contentblockadditionalclass: "purplebg",
		uppertextblock:[{
			textclass: "description1 title",
			textdata: data.string.p3_s1
		}]
	},{
		// slide2
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s2
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left",
			contentimgclass: "image50_a",
			contentimgid: "200gm",
			correctflag: true
		},{
			weighblocksubclass: "drop_right",
			contentimgclass: "image50_b",
			contentimgid: "100gm",
			correctflag: false
		}],
	},{
		// slide3
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.p3_s2
		},{
			textclass: "description fade_in",
			textdata: data.string.p3_s3
		},{
			textclass: "description fade_in2",
			textdata: data.string.p3_s4
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left",
			contentimgclass: "image50_a",
			contentimgid: "1kg",
			correctflag: true
		},{
			weighblocksubclass: "drop_right",
			contentimgclass: "image50_c_1",
			contentimgid: "500gm",
			correctflag: false
		},{
			weighblocksubclass: "drop_right",
			contentimgclass: "image50_c_2",
			contentimgid: "500gm",
			correctflag: false
		}],
		imageblock: [{
			imagestoshow: [{
				imgclass: "click_500gm",
				imgid: "500gm"
			},{
				imgclass: "click_hand_icon",
				imgid: "hand-icon"
			}]
		}]
	},{
		// slide4
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb_30",
		uppertextblock:[{
			textclass: "description fade_in",
			textdata: data.string.p3_s5
		}],
		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left",
			contentimgclass: "image50_a",
			contentimgid: "1kg",
			correctflag: true
		},{
			weighblocksubclass: "drop_right",
			contentimgclass: "image50_c_1",
			contentimgid: "500gm",
			correctflag: false
		},{
			weighblocksubclass: "drop_right",
			contentimgclass: "image50_c_2",
			contentimgid: "500gm",
			correctflag: false
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg", src: "images/diy_bg/a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weightmachinebase", src: imgpath+"weightmachinebase.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weight_plate", src: imgpath+"weight_plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1kg", src: imgpath+"1kg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "500gm", src: imgpath+"500gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "200gm", src: imgpath+"200gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "100gm", src: imgpath+"100gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "ding", src: soundAsset+"ding.ogg"},
			{id: "coin_drag", src: soundAsset+"coin_drag.ogg"},
            {id: "sound_1", src: soundAsset1+"s3_p1.ogg"},
            {id: "sound_3", src: soundAsset1+"s3_p3.ogg"},
            {id: "sound_4", src: soundAsset1+"s3_p4_afterclick.ogg"},
            {id: "sound_4_1", src: soundAsset1+"s3_p4_afterclick_1.ogg"},
            {id: "sound_5", src: soundAsset1+"s3_p5.ogg"},


        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	    	case 1:
	    		play_diy_audio();
	    		$(".contentblock").css({"background-image": "url('"+ preload.getResult('bg').src +"')",
	    								"background-size": "100% 100%"});
	    		$nextBtn.delay(400).show(0);
	    		break;
	    	case 2:
	    		sound_player("sound_3",false);
    	 		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $dropright = $(".drop_right");
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		
	    		setTimeout(function(){
		    		$leftpan.addClass("left_heavy_left_pan");
	    			$rightpan.addClass("left_heavy_right_pan");
	    			$needle.addClass("left_heavy_needle");
	    			$dropleft.addClass("left_heavy_drop_left");
	    			$dropright.addClass("left_heavy_drop_right");
	    		}, 200);
	    		
	    		var $wrong = $("#wrong");
	    		var $correct = $("#correct");
	    		var answered = false;
	    		$dropright.click(function(){
	    			if (answered)
	    				return answered;
	    			current_sound.stop();
	    			play_correct_incorrect_sound(false);
	    			$wrong.show(0);
	    		});
	    		
	    		$dropleft.click(function(){
	    			if (answered)
	    				return answered;
                    current_sound.stop();
	    			play_correct_incorrect_sound(true);
	    			$correct.show(0);
	    			answered = true;
	    			$nextBtn.show(0);
	    		});
	    		
	    		break;
	    		
	    	case 3:
    	 		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $dropright = $(".drop_right");
    	 		var $description = $(".description");
    	 		
    	 		$($description[1]).hide(0);
    	 		$($description[2]).hide(0);
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		
	    		setTimeout(function(){
		    		$leftpan.addClass("left_heavy_left_pan");
	    			$rightpan.addClass("left_heavy_right_pan");
	    			$needle.addClass("left_heavy_needle");
	    			$dropleft.addClass("left_heavy_drop_left");
	    			$dropright.addClass("left_heavy_drop_right");
	    		}, 200);
	    		
	    		var $wrong = $("#wrong");
	    		var $correct = $("#correct");
	    		var answered = false;
	    		
	    		var $click_500gm = $(".click_500gm");
	    		var $click_hand_icon = $(".click_hand_icon");
	    		$dropright.click(function(){
	    			if (answered)
	    				return answered;
	    			play_correct_incorrect_sound(false);
	    			$wrong.show(0);
	    		});
	    		
	    		$dropleft.click(function(){
	    			setTimeout(function () {
                        sound_player("sound_4",false);
                    },1500);
	    			if (answered)
	    				return answered;	    			
	    			play_correct_incorrect_sound(true);
	    			$correct.show(0);
	    			answered = true;
	    			$($description[0]).hide(0);
	    			$($description[1]).show(0);
	    			$click_500gm.show(0);
	    			$click_hand_icon.show(0);
	    		});
				
				$(".click_500gm , .click_hand_icon ").click(function(){
					current_sound.stop();
                    $click_500gm.hide(0);
					$click_hand_icon.hide(0);
					$(".image50_c_2").show(0);
					sound_player("coin_drag");
					setTimeout(function () {
                        sound_player("sound_4_1",true);
                    },2700);
					$leftpan.addClass("neutral_left_pan");
	    			$rightpan.addClass("neutral_right_pan");
	    			$needle.addClass("neutral_needle");
	    			$dropleft.addClass("neutral_drop_left");
	    			$dropright.addClass("neutral_drop_right");
    				$($description[2]).show(0);
				});
	    		break;
	    	case 4:
                sound_player("sound_5",true);
                var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
    	 		var $dropleft = $(".drop_left");
    	 		var $dropright = $(".drop_right");
    	 		
	    		$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');
	    		$(".image50_c_2").show(0);
	    		break;
	    	default:
				sound_player("sound_"+(countNext+1),true);
				break;
	   }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id,navigate){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		navigate?navigationcontroller():"";
	});
}

function put_image(content, count){
		
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}					
				}
			}
		}
	
		if(content[count].hasOwnProperty('weightblock')){
			for(var j = 0; j < content[count].weightblock.length; j++){
				var weightblock_a = content[count].weightblock[j];
				console.log("weightblock", weightblock_a);
				if(weightblock_a.hasOwnProperty('contentimgclass')){
					var imageClass = weightblock_a.contentimgclass;
					if(preload.getResult(weightblock_a.contentimgid) == null){
						console.log("null id:", weightblock_a.contentimgid);
					}
					var image_src = preload.getResult(weightblock_a.contentimgid).src;
					$("."+imageClass ).attr('src', image_src);
					if(weightblock_a.correctflag){
						$("#correct").attr('src', preload.getResult('correct').src);
					}else{
						$("#wrong").attr('src', preload.getResult('wrong').src);
					}
				}
			}
		}
	}
	
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');


    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

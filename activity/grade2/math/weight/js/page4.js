var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var soundAsset1 = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s0
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			}]
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s1
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			},
			{
				imgclass: "click_pointer",
				imgid: "hand-icon"
			}]
		}]
	},{
		// slide2
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s2
		},{
			textclass: "descriptionheader fade_in",
			textdata: data.string.p4_s3
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			},
			{
				imgclass: "click_pointer2",
				imgid: "hand-icon"
			}]
		}]
	},{
		// slide3
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s4
		},{
			textclass: "descriptionheader",
			textdata: data.string.p4_s5
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			}]
		}]
	},{
		// slide4
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s6
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			}]
		}]
	},{
		// slide5
		contentnocenteradjust: true,
		contentblockadditionalclass : "brownbg",
		headerblock: [{
			textclass: "descriptionheader",
			textdata: data.string.p4_s7
		}],

		weightblock:[{
			weighblocksubclass: "beambalance_base"
		},{
			weighblocksubclass: "beambalance_left"
		},{
			weighblocksubclass: "beambalance_right"
		},{
			weighblocksubclass: "beambalance_needle"
		},{
			weighblocksubclass: "drop_left"
		},{
			weighblocksubclass: "drop_right"
		}],
		imageblockadditionalclass: "draggablecontainer",
		imageblock:[{
			imageblockclass: "mangoescollection",
			imagestoshow:[{
				imgclass: "platemango",
				imgid: "plate"
			}]
		},{
			imageblockclass: "orangescollection",
			imagestoshow:[{
				imgclass: "plateorange",
				imgid: "plate"
			}]
		},{
			imageblockclass: "potatocollection",
			imagestoshow:[{
				imgclass: "platepotato",
				imgid: "plate"
			}]
		},{
			imageblockclass: "calibrated_weight4",
			imagestoshow:[{
				imgclass: "dhak1",
				imgid: "50gm"
			},
			{
				imgclass: "dhak2",
				imgid: "100gm"
			},
			{
				imgclass: "dhak3",
				imgid: "200gm"
			},
			{
				imgclass: "dhak4",
				imgid: "500gm"
			},
			{
				imgclass: "dhak5",
				imgid: "1kg"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "50gm", src: imgpath+"50gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "100gm", src: imgpath+"100gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "200gm", src: imgpath+"200gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "500gm", src: imgpath+"500gm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "1kg", src: imgpath+"1kg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato", src: imgpath+"potato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plate", src: imgpath+"plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weightmachinebase", src: imgpath+"weightmachinebase.png", type: createjs.AbstractLoader.IMAGE},
			{id: "weight_plate", src: imgpath+"weight_plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "needle", src: imgpath+"needle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand-icon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "ding", src: soundAsset+"ding.ogg"},
			{id: "coindrag", src: soundAsset+"coin_drag.ogg"},
            {id: "sound_1", src: soundAsset1+"s4_p1.ogg"},
            {id: "sound_2", src: soundAsset1+"s4_p2.ogg"},
            {id: "sound_3", src: soundAsset1+"s4_p3.ogg"},
            {id: "sound_3_1", src: soundAsset1+"s4_p3_1_afterclick.ogg"},
            {id: "sound_4", src: soundAsset1+"s4_p4.ogg"},
            {id: "sound_4_1", src: soundAsset1+"s4_p4_1_afterclick.ogg"},
            {id: "sound_5", src: soundAsset1+"s4_p5.ogg"},
            {id: "sound_6", src: soundAsset1+"s4_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			 ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
 var timeoutcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	    	case 0:
	    		sound_player("sound_"+(countNext+1),true);
	    		for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		break;
	    	case 1:
                sound_player("sound_"+(countNext+1),false);
                for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
	    		var $drop_left = $(".drop_left");
	    		var $drop_right = $(".drop_right");

	    		var $dhak2 = $(".dhak2");
	    		var $click_pointer = $(".click_pointer");
	    		$(".dhak2, .click_pointer").click(function(){
	    			$click_pointer.hide(0);
	    			$dhak2.detach().css({
	    				"bottom":"5%",
	    				"left": "35%",
	    				"width": "30%"
	    			}).appendTo($drop_right);
	    			sound_player("coindrag");
	    			$leftpan.addClass("right_heavy_left_pan");
	    			$rightpan.addClass("right_heavy_right_pan");
	    			$needle.addClass("right_heavy_needle");
	    			$drop_left.addClass("right_heavy_drop_left");
	    			$drop_right.addClass("right_heavy_drop_right");

	    			$nextBtn.show(0);
	    		});

	    		break;
	    	case 2:
                sound_player("sound_"+(countNext+1),false);
                for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

	    		var $descriptionheader = $(".descriptionheader");
	    		$($descriptionheader[1]).hide(0);

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
	    		var $drop_left = $(".drop_left");
	    		var $drop_right = $(".drop_right");

	    		var leftfactor = 10;
	    		var $click_pointer2 = $(".click_pointer2");

	    		var clickconsumed = false;
	    		$(".mango, .click_pointer2").click(function(){
							$(".mango").css("pointer-events", "none");
              sound_player("coindrag");
              setTimeout(function () {
                  sound_player("sound_3_1",true);
              },1500);
              if(clickconsumed){
	    				return clickconsumed;
	    			}
	    			clickconsumed = true;
	    			var $this = $(this);
	    			if ($this.hasClass("click_pointer2")){
	    				$this = $($(".mango")[4]);
	    			}
	    			$click_pointer2.hide(0);
	    			$($descriptionheader[0]).hide(0);
	    			var length = $drop_left.find(".mango").length;
	    			$this.detach().css({
	    				"left": (10+ length*leftfactor)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    			$($descriptionheader[1]).show(0);
	    			$leftpan.addClass("right_heavy_left_pan2");
	    			$rightpan.addClass("right_heavy_right_pan2");
	    			$needle.addClass("right_heavy_needle2");
	    			$drop_left.addClass("right_heavy_drop_left2");
	    			$drop_right.addClass("right_heavy_drop_right2");
	    		});

	    		$(".dhak2").detach().css({
	    				"bottom":"5%",
	    				"left": "35%",
	    				"width": "30%"
	    			}).appendTo($drop_right);
    			$leftpan.addClass("right_heavy_lp");
    			$rightpan.addClass("right_heavy_rp");
    			$needle.addClass("right_heavy_n");
    			$drop_left.addClass("right_heavy_dl");
    			$drop_right.addClass("right_heavy_dr");
	    		break;
	    	case 3:
                sound_player("sound_4",false);
                for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

	    		var $descriptionheader = $(".descriptionheader");
	    		$($descriptionheader[1]).hide(0);

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
	    		var $drop_left = $(".drop_left");
	    		var $drop_right = $(".drop_right");

	    		var leftfactor = 10;
	    		var $click_pointer2 = $(".click_pointer2");
	    		var clickconsumed = false;
	    		$(".mango, .click_pointer2").click(function(){
							$(".mango").css("pointer-events", "none");
                    sound_player("coindrag",false);
                    setTimeout(function () {
                        sound_player("sound_4_1",true);
                    },1500);
                    if(clickconsumed){
	    				return clickconsumed;
	    			}
	    			clickconsumed = true;
	    			var $this = $(this);
	    			if ($this.hasClass("click_pointer2")){
	    				$this = $($(".mango")[4]);
	    			}
	    			$click_pointer2.hide(0);
	    			$($descriptionheader[0]).hide(0);
	    			var length = $drop_left.find(".mango").length;
	    			$this.detach().css({
	    				"left": (10+ length*leftfactor)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    			$($descriptionheader[1]).show(0);
	    			$leftpan.addClass("neutral_left_pan");
	    			$rightpan.addClass("neutral_right_pan");
	    			$needle.addClass("neutral_needle");
	    			$drop_left.addClass("neutral_drop_left");
	    			$drop_right.addClass("neutral_drop_right");
	    			// $nextBtn.show(0);
	    		});

	    		$($mango[4]).detach().css({
	    				"left": (10)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);

	    		$(".dhak2").detach().css({
	    				"bottom":"5%",
	    				"left": "35%",
	    				"width": "30%"
	    			}).appendTo($drop_right);
    			$leftpan.addClass("right_heavy_lp2");
    			$rightpan.addClass("right_heavy_rp2");
    			$needle.addClass("right_heavy_n2");
    			$drop_left.addClass("right_heavy_dl2");
    			$drop_right.addClass("right_heavy_dr2");

	    		break;
	    	case 4:
                sound_player("sound_5",true);
                for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

	    		var $descriptionheader = $(".descriptionheader");
	    		$($descriptionheader[1]).hide(0);

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
	    		var $drop_left = $(".drop_left");
	    		var $drop_right = $(".drop_right");

	    		var leftfactor = 10;

	    		$($mango[4]).detach().css({
	    				"left": (10)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    		$($mango[3]).detach().css({
	    				"left": (10+ 1*leftfactor)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    		$(".dhak2").detach().css({
	    				"bottom":"5%",
	    				"left": "35%",
	    				"width": "30%"
	    			}).appendTo($drop_right);


	    		break;
	    	case 5:
                sound_player("sound_6",true);
                for(var i= 1; i <=5; i++){
	    			$(".dhak"+i).addClass("weightdrop");
	    		}
	    		var $mangocollection = $(".mangoescollection");
	    		var fronttag = "<img class = '";
	    		var midtag = "' src = ";
	    		var endtag = "></img>";
	    		for(var i = 1; i <= 5; i++){
	    			$mangocollection.append(fronttag +"mango fruit"+ midtag + preload.getResult('orange').src + endtag);
	    		}

	    		var $mango = $(".mango");

	    		var bottom = 14;
	    		var left = 10;
	    		var z_index = 110;
	    		var bottom_addition = 0;
	    		for(var i = 0; i < $mango.length; i++){
	    			$($mango[i]).css({
	    				"position": "absolute",
	    				"width": "20%",
	    				"left": left+"%",
	    				"bottom": bottom+"%",
	    				"z-index": z_index
	    			});

	    			left += 15.2;
	    		}

	    		var $descriptionheader = $(".descriptionheader");
	    		$($descriptionheader[1]).hide(0);

    			$(".beambalance_base").css('background-image', 'url("'+ preload.getResult("weightmachinebase").src +'")');
	    		$(".beambalance_left").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_right").css('background-image', 'url("'+ preload.getResult("weight_plate").src +'")');
	    		$(".beambalance_needle").css('background-image', 'url("'+ preload.getResult("needle").src +'")');

	    		var $leftpan = $(".beambalance_left");
    	 		var $rightpan = $(".beambalance_right");
    	 		var $needle = $(".beambalance_needle");
	    		var $drop_left = $(".drop_left");
	    		var $drop_right = $(".drop_right");

	    		var leftfactor = 10;

	    		$($mango[4]).detach().css({
	    				"left": (10)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    		$($mango[3]).detach().css({
	    				"left": (10+ 1*leftfactor)+"%",
	    				"bottom": "6%"
	    			}).appendTo($drop_left);
	    		$(".dhak2").detach().css({
	    				"bottom":"5%",
	    				"left": "35%",
	    				"width": "30%"
	    			}).appendTo($drop_right);
	    		break;
			default:
				break;

	    }
  }

/*=====  End of Templates Block  ======*/

function sound_player(sound_id,nav){
	createjs.Sound.stop();
	 current_sound = createjs.Sound.play(sound_id);
	current_sound.on("complete", function(){
		nav?navigationcontroller():"";
	});
}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				console.log("imageblock", imageblock);
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');


    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			clearTimeout(timeoutcontroller);
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

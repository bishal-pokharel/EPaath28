var imgpath = $ref + "/images/";
soundAsset = $ref + "/sounds/";

var dialog1  = new buzz.sound(soundAsset + "nach1.ogg");
var dialog2  = new buzz.sound(soundAsset + "nach2.ogg");
var dialog3  = new buzz.sound(soundAsset + "nach1.ogg");
var dialog4  = new buzz.sound(soundAsset + "nach3.ogg");
var dialog5  = new buzz.sound(soundAsset + "nach1.ogg");

var sound_group_p1  = [dialog1, dialog2, dialog3, dialog4, dialog5];
var content;
var content=[
	{
		// slide0
		contentnocenteradjust: true,
		headerblock:[{
			textclass: "description_title",
			textdata: data.string.lesson_title
		},{
			textclass: "writer",
			textdata: data.string.writer
		}],
		uppertextblockadditionalclass: "left_50",
		uppertextblock:[{
			textclass: "description",
			textdata: data.string.para1_stanza1
		},{
			textclass: "description",
			textdata: data.string.para2_stanza1,
			breakafterp: true
		},{
			textclass: "description",
			textdata: data.string.para3_stanza1
		},{
			textclass: "description",
			textdata: data.string.para4_stanza1
		},{
			textclass: "description",
			textdata: data.string.para5_stanza1
		}]
	}
];

// }
$(function(){
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/


 function navigationcontroller(islastpageflag) {
	 typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.hide(0);
      	}else if(countNext > 0 && countNext < $total_page-1) {
                // $nextBtn.show(0);
                // $prevBtn.show(0);
        }else if(countNext == $total_page - 1) {
            // $nextBtn.hide(0);
            // $prevBtn.show(0);
            // setTimeout(function() {
				// // if lastpageflag is true
				// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 1000);
		}
 }
	/*=====  End of user navigation controller function  ======*/

	/*=====  function to control sound play activity  ======*/
	// function playSound(soundIndex1, highlight){
		// console.log(highlight);
// 
         // soundcontent[soundIndex1].play().bind("playing", function() {
		    // $(".text_story").css({color : "#EF6515"}).unbind("click");
		    // $(".text_story1, .text_story2, .text_story3, .text_story4, .text_story5").css({
		    	// cursor: "wait",
		    	// backgroundColor: "transparent",
		    	// opacity: "1"
		    // });
		    // highlight.css({
		    	// backgroundColor: "#FFEA77",
		    	// opacity: "1"
		    // });
          // }).bind('ended', function() {
          	// navigationcontroller();
// 
          	// $(".text_story").css({color : ""})
// 
			// $(".text_story1, .text_story2, .text_story3, .text_story4, .text_story5").css({
		    	// cursor: "pointer",
		    	// backgroundColor: "",
		    	// opacity: ""
		    // });
// 
          	// $(".text_story").css({color : "black"});
		    // highlight.removeClass("text_story1_highlighter");
			// highlight = null;
          // });
    // }
	var audiocontroller = new AudioController($total_page, false,  null, null, vocabcontroller);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		// splitintofractions($(".fractionblock"));
		$description = $(".description");
		vocabcontroller.findwords(countNext);
		
		// $(document).on('click','.voacbunderline',function(){
			// alert('meme');
		// });
		
		// $(document).on('mouseenter','.voacbunderline',function(){
					// alert('asd');
				// });

		audiocontroller.init($description, sound_group_p1, countNext);
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	// setTimeout(function(){
		total_page = content.length;
		templateCaller();
	// }, 250);


});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class = " + stylerulename + " >";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

//page 1

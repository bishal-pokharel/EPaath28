var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "ex2.ogg"));

var box_colors_1 = [['#20948B','#3FA39B'],
					['#CA466E','#D16082'],
					['#6A70A4','#7F84B0'],
					['#E67F5C','#E99173'],
					['#89A537','#99B153'],
					['#9566B1','#A47BBC'],
					['#A64D79','#B2668B'],
					['#5384C6','#6B95CE']];
var content=[
	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_instruction_match,
				draggableblock:[{
					draggabletextadditionalclass: 'drag class_1',
					textdata: data.string.e1_a1,
				  dataname : 'qn1'
				},{
					draggabletextadditionalclass: 'drag class_2',
					textdata: data.string.e1_a2,
				  dataname : 'qn2'
				},{
					draggabletextadditionalclass: 'drag class_3',
					textdata: data.string.e1_a3,
				  dataname : 'qn3'
				},{
					draggabletextadditionalclass: 'drag class_4',
					textdata: data.string.e1_a4,
				  dataname : 'qn4'
				},{
					draggabletextadditionalclass: 'drag class_5',
					textdata: data.string.e1_a5,
				  dataname : 'qn5'
				},{
					draggabletextadditionalclass: 'drag class_6',
					textdata: data.string.e1_a6,
				  dataname : 'qn6'
				},{
					draggabletextadditionalclass: 'drag class_7',
					textdata: data.string.e1_a7,
				  dataname : 'qn7'
				},{
					draggabletextadditionalclass: 'drag class_8',
					textdata: data.string.e1_a8,
				  dataname : 'qn8'
				}],
				droppableblock:[{
					droppabledivadditionalclass: 'drop_class_1',
					imgsrc: imgpath + '1.jpg',
					p1_data: data.string.e1_q1,
				  dataname : 'qn1'
				},{
					droppabledivadditionalclass: 'drop_class_2',
					imgsrc: imgpath + '2.jpg',
					p1_data: data.string.e1_q2,
				  dataname : 'qn2'
				},{
					droppabledivadditionalclass: 'drop_class_3',
					imgsrc: imgpath + '3.jpg',
					p1_data: data.string.e1_q3,
				  dataname : 'qn3'
				},{
					droppabledivadditionalclass: 'drop_class_4',
					imgsrc: imgpath + '4.jpg',
					p1_data: data.string.e1_q4,
				  dataname : 'qn4'
				},{
					droppabledivadditionalclass: 'drop_class_5',
					imgsrc: imgpath + '5.jpg',
					p1_data: data.string.e1_q5,
				  dataname : 'qn5'
				},{
					droppabledivadditionalclass: 'drop_class_6',
					imgsrc: imgpath + '6.jpg',
					p1_data: data.string.e1_q6,
				  dataname : 'qn6'
				},{
					droppabledivadditionalclass: 'drop_class_7',
					imgsrc: imgpath + '7.jpg',
					p1_data: data.string.e1_q7,
				  dataname : 'qn7'
				},{
					droppabledivadditionalclass: 'drop_class_8',
					imgsrc: imgpath + '8.jpg',
					p1_data: data.string.e1_q8,
				  dataname : 'qn8'
				}]

			}
		]
	},
];

/*remove this for non random questions*/
//
// content[0].exerciseblock[0].draggableblock.shufflearray();
// content[0].exerciseblock[0].droppableblock.shufflearray();
// box_colors_1.shufflearray();

// content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;
	var is_dragging =  false;

	/*for limiting the questions to 10*/
	var $total_page = 1;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }
 	loadTimelineProgress($total_page,1);

		var score = 0;
		var testin = new NumberTemplate();
	 	testin.init(8);
		$("#activity-page-total-slide").html(eval("data.string.one"));
		var qnArray = ['1','2','3','4','5','6','7','8'];
		qnArray.shufflearray();
		var wrongClick = false;
		var qnCount = 0;

	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		// give_colors_to_boxes();
		$(".nep_droppablediv").append("<img class='rightWrong right' src='"+(imgpath+"right.png")+"' />")
		$(".nep_droppablediv").append("<img class='rightWrong wrong' src='"+(imgpath+"wrongicon.png")+"' />")

		// create_div_for_hover();
		var drop_count = 0;
		sound_l_1.play();
		$(".class_"+qnArray[0]).show(0);
		$(".drag").draggable({
			cursor:'pointer',
			revert:true
		});

		$(".p2_class").droppable({
			accept:".drag",
			hoverClass:"drop-hover",
			drop: function(event, ui){
				dropfunction(event, ui, ui.draggable, $(this));
			}
		});
	}

	function dropfunction(event, ui, dragclass, dropClass){
		if(dragclass.attr("data-name") == dropClass.attr("data-name")){
			play_correct_incorrect_sound(1);
			$('.wrong').hide(0);
			dropClass.siblings(".right").show(0);
			$(".class_"+qnArray[0]).hide(0);
			qnArray.splice(0,1);
			$(".class_"+qnArray[0]).show();
			dropClass.html(dragclass.text()).css({"background":"#9eda73","color":"#fff","border":"4px solid #ff0"});
			!wrongClick?testin.update(true):testin.update(false);
			wrongClick = false;
			qnCount+=1;
			qnCount==8?	create_exercise_menu_bar_nep_matching():'';
		}else{
			play_correct_incorrect_sound(0);
			dropClass.siblings(".wrong").show(0);
			wrongClick = true;
		}
	}

	function create_div_for_hover(){
		for(var index=1; index<=$('.nep_droppablediv').length; index++){
			// var x_position = $('.drop_class_'+index).position().left*100/$board.width()+'%';
			var x_position = '30%';
			var y_position = ( $('.drop_class_'+index).position().top + $('.nep_droppableblock').position().top )*100/$board.height()+'%';
			var new_width = '50%';
			// var new_width = $('.drop_class_'+index).width()*100/$board.width()+'%';
			var new_height = $('.drop_class_'+index).height()*100/$board.height()+'%';
			$('.contentblock').append('<div class="invisiblehover"></div>');
			$('.invisiblehover').eq(index-1).addClass('hover-class-'+index);
			$('.invisiblehover').eq(index-1).css({
				'width': new_width,
				'height': new_height,
				'left': x_position,
				'top':y_position
			});
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

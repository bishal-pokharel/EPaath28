var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "ex2.ogg"));

var box_colors_1 = [['#20948B','#3FA39B'],
					['#CA466E','#D16082'],
					['#6A70A4','#7F84B0'],
					['#E67F5C','#E99173'],
					['#89A537','#99B153'],
					['#9566B1','#A47BBC'],
					['#A64D79','#B2668B'],
					['#5384C6','#6B95CE']];
var content=[
	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_instruction_match,
				draggableblock:[{
					draggabletextadditionalclass: 'drag class_1',
					textdata: data.string.e1_a1
				},{
					draggabletextadditionalclass: 'drag class_2',
					textdata: data.string.e1_a2
				},{
					draggabletextadditionalclass: 'drag class_3',
					textdata: data.string.e1_a3
				},{
					draggabletextadditionalclass: 'drag class_4',
					textdata: data.string.e1_a4
				},{
					draggabletextadditionalclass: 'drag class_5',
					textdata: data.string.e1_a5
				},{
					draggabletextadditionalclass: 'drag class_6',
					textdata: data.string.e1_a6
				},{
					draggabletextadditionalclass: 'drag class_7',
					textdata: data.string.e1_a7
				},{
					draggabletextadditionalclass: 'drag class_8',
					textdata: data.string.e1_a8
				}],
				droppableblock:[{
					droppabledivadditionalclass: 'drop_class_1',
					imgsrc: imgpath + '1.jpg',
					p1_data: data.string.e1_q1
				},{
					droppabledivadditionalclass: 'drop_class_2',
					imgsrc: imgpath + '2.jpg',
					p1_data: data.string.e1_q2
				},{
					droppabledivadditionalclass: 'drop_class_3',
					imgsrc: imgpath + '3.jpg',
					p1_data: data.string.e1_q3
				},{
					droppabledivadditionalclass: 'drop_class_4',
					imgsrc: imgpath + '4.jpg',
					p1_data: data.string.e1_q4
				},{
					droppabledivadditionalclass: 'drop_class_5',
					imgsrc: imgpath + '5.jpg',
					p1_data: data.string.e1_q5
				},{
					droppabledivadditionalclass: 'drop_class_6',
					imgsrc: imgpath + '6.jpg',
					p1_data: data.string.e1_q6
				},{
					droppabledivadditionalclass: 'drop_class_7',
					imgsrc: imgpath + '7.jpg',
					p1_data: data.string.e1_q7
				},{
					droppabledivadditionalclass: 'drop_class_8',
					imgsrc: imgpath + '8.jpg',
					p1_data: data.string.e1_q8
				}]

			}
		]
	},
];

/*remove this for non random questions*/
//
// content[0].exerciseblock[0].draggableblock.shufflearray();
// content[0].exerciseblock[0].droppableblock.shufflearray();
// box_colors_1.shufflearray();

// content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;
	var is_dragging =  false;

	/*for limiting the questions to 10*/
	var $total_page = 1;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }
 	loadTimelineProgress($total_page,1);

		var score = 0;
		var testin = new NumberTemplate();
	 	testin.init(10);
		$("#activity-page-total-slide").html(eval("data.string.one"));
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		give_colors_to_boxes();

		create_div_for_hover();

		var drop_count = 0;
		sound_l_1.play();
		$(".nep_draggabletext").draggable({
            containment: ".board",
            revert: "invalid",
            appendTo: "body",
						helper:'clone',
            zIndex: 100,
            start: function(event, ui) {
                $(this).css({
                    "cursor": "grabbing"
                });
                is_dragging =  true;
            },
            stop: function(event, ui) {
                $(this).css({
                    "cursor": "grab"
                });
                is_dragging =  false;
            }
        });
        $('.invisiblehover').hover(function(){
        	if(is_dragging){
        		var current_idx = $(this).attr('class');
        		current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
        		$('.drop_class_'+current_idx).addClass('hoverd_match');
        	}
        }, function(){
        	if(is_dragging){
	        	var current_idx = $(this).attr('class');
	        	current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
	        	$('.drop_class_'+current_idx).removeClass('hoverd_match');
	        }
        });
        $('.nep_droppablediv').hover(function(){
        	$(this).addClass('hoverd_match');
        },function(){
        	$(this).removeClass('hoverd_match');
        });
		$(".drag").hide();
		$(".class_1").show();
        for(var index=1; index<=$('.nep_droppablediv').length; index++){
        	$('.drop_class_'+index).droppable({
	            accept : ".class_"+index,
	            // hoverClass: "hoverd_match",
	            drop: function(event, ui) {
								sound_l_1.stop();
	            	$('.insturction').fadeOut(1000);
	            	play_correct_incorrect_sound(1);
	            	var current_idx = $(this).attr('class');
	            	var $current_class_star = $(this);
	            	current_idx = parseInt(current_idx.replace(/[^\d]/g,''));
	            	$(this).css('width', '100%');
	            	$(this).find('.p1_class').css('width', '17.5%');
	            	$(this).find('.p2_class').html($(ui.draggable).html());
  							var dt = new Date();
	            	$(this).find('.p2_class').css({
	            		'display': 'inline',
	            		'width': 'auto',
	            		'background-color': box_colors_1[current_idx-1][1],
	            		'background-image': 'url("images/star_1.gif?' + dt.getTime() + '")',
	            		'background-size': 'auto 100%',
	            		'background-repeat': 'no-repeat',
	            		'background-position': 'center center'
	            	});
	            	$(ui.draggable).hide(0);
	            	drop_count++;
                    $(".class_"+(drop_count+1)).show();
                    if(drop_count>7){
	            		create_exercise_menu_bar_nep_matching();
	            	}
	            }
	        });
        }
	}
	function give_colors_to_boxes(){
		for(var index=1; index<=$('.nep_droppablediv').length; index++){
			$('.drop_class_'+index).find('.p1_class').css('background-color', box_colors_1[index-1][0]);
		}
	}
	function create_div_for_hover(){
		for(var index=1; index<=$('.nep_droppablediv').length; index++){
			// var x_position = $('.drop_class_'+index).position().left*100/$board.width()+'%';
			var x_position = '30%';
			var y_position = ( $('.drop_class_'+index).position().top + $('.nep_droppableblock').position().top )*100/$board.height()+'%';
			var new_width = '50%';
			// var new_width = $('.drop_class_'+index).width()*100/$board.width()+'%';
			var new_height = $('.drop_class_'+index).height()*100/$board.height()+'%';
			$('.contentblock').append('<div class="invisiblehover"></div>');
			$('.invisiblehover').eq(index-1).addClass('hover-class-'+index);
			$('.invisiblehover').eq(index-1).css({
				'width': new_width,
				'height': new_height,
				'left': x_position,
				'top':y_position
			});
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

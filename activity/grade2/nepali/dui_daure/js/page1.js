var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		extratextblock:[{
			textdata: data.string.lesson_title,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1',
		uppertextblock:[{
			textdata: data.string.p1text1,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-1',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "",
		}],
		svgblock: [{
			svgblock: 'bg-d-3',
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		svgblock: [{
			svgblock: 'bg-double',
			svgclass: 'bg-anim-a'
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-1 its_hidden',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "",
		}],
		svgblock: [{
			svgblock: 'bg-double-2',
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p1text4,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full-2",
					imgsrc : '',
					imgid : 'bg-4'
				},{
					imgclass : "sweat sweat-1",
					imgsrc : '',
					imgid : 'sweat-1'
				},{
					imgclass : "sweat sweat-2",
					imgsrc : '',
					imgid : 'sweat-2'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-3',
		uppertextblock:[{
			textdata: data.string.p1text5,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full-3",
					imgsrc : '',
					imgid : 'bg-4'
				},{
					imgclass : "bg-cry",
					imgsrc : '',
					imgid : 'crying'
				},{
					imgclass : "sweat sweat-1",
					imgsrc : '',
					imgid : 'sweat-1'
				},{
					imgclass : "sweat sweat-2",
					imgsrc : '',
					imgid : 'sweat-2'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-4 its_hidden',
		uppertextblock:[{
			textdata: data.string.p1text6,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv: 'full-div',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-devi'
				},{
					imgclass : "devi devi-anim",
					imgsrc : '',
					imgid : 'devi'
				},{
					imgclass : "wave wave-anim",
					imgsrc : '',
					imgid : 'wave'
				},{
					imgclass : "waterover",
					imgsrc : '',
					imgid : 'water'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-5 its_hidden',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "",
		}],
		imagedivblock:[{
			imagediv: 'full-div-2 pan-1',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-05'
				},{
					imgclass : "devi",
					imgsrc : '',
					imgid : 'devi'
				},{
					imgclass : "wave ",
					imgsrc : '',
					imgid : 'wave'
				},{
					imgclass : "waterover",
					imgsrc : '',
					imgid : 'water'
				},{
					imgclass : "daure",
					imgsrc : '',
					imgid : 'daure'
				}
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-6-re',
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-06'
				},{
					imgclass : "devi-2",
					imgsrc : '',
					imgid : 'devi-1'
				}
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-7',
		uppertextblock:[{
			textdata: data.string.p1text9,
			textclass: "text-1",
		},
		{
			textdata: data.string.p1text10,
			textclass: "text-2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-06'
				},{
					imgclass : "devi-2 its_hidden fade-in-magic",
					imgsrc : '',
					imgid : 'silver-1'
				}
			]
		}]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-8',
		uppertextblock:[{
			textdata: data.string.p1text11,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-07'
				},
				{
					imgclass : "sante",
					imgsrc : '',
					imgid : 'sante-1'
				}
			]
		}]
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story  ts-7',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "",
		},
		{
			textdata: data.string.p1text13,
			textclass: "text-3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-06'
				},{
					imgclass : "devi-2 fade-out-half",
					imgsrc : '',
					imgid : 'silver-3'
				}
			]
		}]
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-9',
		uppertextblock:[{
			textdata: data.string.p1text14,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-07'
				},
				{
					imgclass : "sante",
					imgsrc : '',
					imgid : 'sante-1'
				}
			]
		}]
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story  ts-7 its_hidden',
		uppertextblock:[{
			textdata: data.string.p1text15,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-06'
				},{
					imgclass : "devi-2 fade-in-magic",
					imgsrc : '',
					imgid : 'iron-1'
				}
			]
		}]
	},
	// slide15
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gray',

		uppertextblockadditionalclass: 'text-story ts-9',
		uppertextblock:[{
			textdata: data.string.p1text16,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-07'
				},
				{
					imgclass : "sante",
					imgsrc : '',
					imgid : 'sante-1'
				}
			]
		}]
	},
	// slide16
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',

		uppertextblockadditionalclass: 'text-story  ts-7',
		uppertextblock:[{
			textdata: data.string.p1text17,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'final'
				},{
					imgclass : "axe-pop axe-g",
					imgsrc : '',
					imgid : 'axe-g'
				},{
					imgclass : "axe-pop axe-s",
					imgsrc : '',
					imgid : 'axe-s'
				},{
					imgclass : "axe-pop axe-i",
					imgsrc : '',
					imgid : 'axe-i'
				},{
					imgclass : "stars",
					imgsrc : '',
					imgid : 'stars'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for request animation
	var fps = 5;
	var global_animation = null;
	var time_init = 0;
	var time_elapsed = 0;
	var time_then = Date.now();;
	var global_counter = 0;
	var frame_arr = [];
	var spriteImg = '';

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"im-0.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"im-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"im-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"im-4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-05", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-06", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-07", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sweat-1", src: imgpath+"pasena-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sweat-2", src: imgpath+"pasena-2.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-svg", src: imgpath+"bg.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "bubbles", src: imgpath+"bubbles.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "devi", src: imgpath+"jal-devi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "devi-1", src: imgpath+"jaldevi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "devi-2", src: imgpath+"jaldevi02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sante-3", src: imgpath+"sante01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sante-1", src: imgpath+"sante02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sante-2", src: imgpath+"sante03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "silver-2", src: imgpath+"devi07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silver-1", src: imgpath+"devi08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "silver-3", src: imgpath+"devi09.png", type: createjs.AbstractLoader.IMAGE},

			{id: "gold-2", src: imgpath+"devi04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gold-1", src: imgpath+"devi05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gold-3", src: imgpath+"devi06.png", type: createjs.AbstractLoader.IMAGE},

			{id: "iron-2", src: imgpath+"devi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron-1", src: imgpath+"devi02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "iron-3", src: imgpath+"devi03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "axe-g", src: imgpath+"goldenax.png", type: createjs.AbstractLoader.IMAGE},
			{id: "axe-s", src: imgpath+"silverax.png", type: createjs.AbstractLoader.IMAGE},
			{id: "axe-i", src: imgpath+"ironax.png", type: createjs.AbstractLoader.IMAGE},

			{id: "stars", src: imgpath+"stars.png", type: createjs.AbstractLoader.IMAGE},

			{id: "crying", src: imgpath+"crying.png", type: createjs.AbstractLoader.IMAGE},
			{id: "final", src: imgpath+"final_img.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-devi", src: imgpath+"bg_for_jaldevi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave", src: imgpath+"waterwave.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water", src: imgpath+"waterfront.png", type: createjs.AbstractLoader.IMAGE},
			{id: "daure", src: imgpath+"daure-2.png", type: createjs.AbstractLoader.IMAGE},


			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s10.ogg"},
			{id: "sound_11a", src: soundAsset+"p1_s10a.ogg"},
			{id: "sound_12", src: soundAsset+"p1_s11.ogg"},
			{id: "sound_13", src: soundAsset+"p1_s12.ogg"},
			{id: "sound_13a", src: soundAsset+"p1_s12a.ogg"},
			{id: "sound_14", src: soundAsset+"p1_s13.ogg"},
			{id: "sound_15", src: soundAsset+"p1_s14.ogg"},
			{id: "sound_16", src: soundAsset+"p1_s15.ogg"},
			{id: "sound_17", src: soundAsset+"p1_s16.ogg"},
			{id: "sound_17a", src: soundAsset+"p1_s16a.ogg"},

			{id: "magic-appear", src: soundAsset+"magic-appear.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		// to hide display none content
		$('.its_hidden').hide(0);
		switch(countNext) {
			case 0:
				$nextBtn.show(0);
				sound_nav('sound_1');
				break;
			case 1:
				sound_nav('sound_2');
				break;
			case 2:
				$prevBtn.show(0);
				sound_nav('sound_3');
				var s = Snap('#bg-d-3');
				var svg = Snap.load(preload.getResult('bg-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				} );
				break;
			case 3:
				$prevBtn.show(0);
				$('#bg-double').addClass('bg-move-1');
				var s = Snap('#bg-double');
				var svg = Snap.load(preload.getResult('bg-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					var centerline = Snap.select('#center-line');
					var axe			= Snap.select("#axe");
					var axe2		= Snap.select("#axe2");
					var axebox		= axe.getBBox();
					var path		= Snap.select("#axe-path");

					var water		= Snap.select("#water-layer");
					var dust		= Snap.select("#dust01");
					var dust2		= Snap.select("#dust02");
					var dustpath	= dust2.attr('path');
					var waves		= Snap.select("#waves");
					var ripple		= Snap.select("#ripple");


					var b_01		= Snap.select("#b01");
					var b_02		= Snap.select("#b02");
					var b_03		= Snap.select("#b03");
					var b_04		= Snap.select("#b04");
					var b_05		= Snap.select("#b05");
					var bubble = [b_01, b_02, b_03, b_04, b_05];

					var anim_path 	= path.attr('path');
					var path_length	= Snap.path.getTotalLength(anim_path);
					var last_point	= Snap.path.getPointAtLength(path_length);
					console.log(path_length);
					// timeoutvar2 = setTimeout(function(){
						Snap.animate(0, path_length, function(step) {
							var moveToPoint = Snap.path.getPointAtLength(anim_path, step);
							var x = moveToPoint.x - 270;
							var y = moveToPoint.y - 450;
							if(step>350 && step<450){
								axe.attr('display', 'block');
								axe2.attr('display', 'none');
							}
							if(step>450 && step<600){
								ripple.attr('display', 'block');
							}
							if(step>600 && step<700){
								bubble[0].attr('display', 'block');
							}
							if(step>700 && step<800){
								bubble[1].attr('display', 'block');
							}
							if(step>800 && step<900){
								bubble[2].attr('display', 'block');
							}
							if(step>900 && step<1000){
								bubble[3].attr('display', 'block');
							}
							if(step>1000 && step<1050){
								bubble[4].attr('display', 'block');
								ripple.attr('display', 'none');
							}
							if(step>1050){
								for(var i=0; i<5; i++){
									bubble[i].attr('display', 'none');
								}
								dust.attr('display', 'block');
								expanddust();
							}
							axe.transform('translate(' + x + ',' + y + ') ');
						}, 4000, mina.easeinout, function() {
							// ship_move_up();
						});
					// }, 0);
					function expanddust(){
						dust.animate({ d: dustpath, 'opacity': '0' }, 800, mina.linear, hidedust);
					}
					function hidedust(){
						dust.attr('display', 'none');
						$nextBtn.show(0);
					}
				} );
				//transform="matrix(1,0,0,1,-96.36,727.46)"
				break;
			case 4:
				$prevBtn.show(0);
				$('#bg-double-2').addClass('bg-move-2');
				var s = Snap('#bg-double-2');
				var svg = Snap.load(preload.getResult('bg-svg').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					var centerline = Snap.select('#center-line');
					var axe			= Snap.select("#axe");
					var axe2		= Snap.select("#axe2");
					axe2.attr('transform', 'matrix(1,0,0,1,-96.36,727.46)');
				} );
				timeoutvar = setTimeout(function(){
					$('.ts-1').fadeIn(500, function(){
						sound_nav('sound_5');
					});
				}, 2000);
				break;
			case 5:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_6');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bg-full-2').addClass('zoom-1');
					nav_button_controls(2000);
					timeoutvar2 = setTimeout(function(){
						$('.sweat-1, .sweat-2').show(0);
					}, 2050);
				});
				break;
			case 6:
				$prevBtn.show(0);
				$('.sweat').show(0);
				current_sound = createjs.Sound.play('sound_7');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.sweat').hide(0);
					$('.bg-cry').show(0);
					nav_button_controls(0);
				});
				break;
			case 7:
				$prevBtn.show(0);
				current_sound = createjs.Sound.play('magic-appear');
				current_sound.play();
				timeoutvar2 = setTimeout(function(){
					$('.full-div').addClass('zoom-2');
				}, 2050);
				timeoutvar = setTimeout(function(){
					$('.text-story').show(0);
					sound_nav('sound_8');
				}, 4100);
				break;
			case 8:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					$('.text-story').show(0);
					sound_nav('sound_9');
				}, 2050);
				break;
			case 9:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_10');
				current_sound.play();
				timeoutvar = setTimeout(function(){
					$('.devi-2').attr('src', preload.getResult('devi-2').src);
					// global_animation = requestAnimationFrame(spriteAnim);
					// spriteImg = '.devi-2';
					// frame_arr = ['devi-2', 'devi-1', 'devi', 'devi-happy'];
				}, 2050);
				current_sound.on("complete", function(){
					$('.devi-2').addClass('fade-out-1');
					// cancelAnimationFrame(global_animation);
					nav_button_controls(0);
				});
				break;
			case 10:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_11');
				current_sound.play();
				current_sound.on("complete", function(){
					current_sound = createjs.Sound.play('magic-appear');
					current_sound.play();
					$('.devi-2').show(0);
					current_sound.on("complete", function(){
						$('.devi-2').attr('src', preload.getResult('silver-2').src);
						$('.text-2').css('opacity', '1');
						sound_nav('sound_11a');
					});
				});
				break;
			case 11:
				$prevBtn.show(0);
				sound_nav('sound_12');
				timeoutvar = setTimeout(function(){
					$('.sante').attr('src', preload.getResult('sante-2').src);
				}, 2050);
				break;
			case 12:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_13');
				current_sound.play();
				current_sound.on("complete", function(){
					current_sound = createjs.Sound.play('magic-appear');
					current_sound.play();
					$('.devi-2').attr('src', preload.getResult('gold-1').src);
					$('.devi-2').removeClass('fade-out-half').addClass('fade-in-magic');
					current_sound.on("complete", function(){
						$('.devi-2').attr('src', preload.getResult('gold-2').src);
						$('.text-3').css('opacity', '1');
						sound_nav('sound_13a');
					});
				});
				break;
			case 13:
				$prevBtn.show(0);
				sound_nav('sound_14');
				timeoutvar = setTimeout(function(){
					$('.sante').attr('src', preload.getResult('sante-2').src);
				}, 3050);
				break;
			case 14:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('magic-appear');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.devi-2').attr('src', preload.getResult('iron-2').src);
					$('.text-story').show(0);
					sound_nav('sound_15');
				});
				break;
			case 15:
				$prevBtn.show(0);
				sound_nav('sound_16');
				timeoutvar = setTimeout(function(){
					$('.sante').attr('src', preload.getResult('sante-3').src);
				}, 500);
				break;
			case 16:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_17');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bg-full, .devi-2').addClass('fade-out-05');
					sound_nav('sound_17a');
					timeoutvar = setTimeout(function(){
						$('.axe-pop, .stars').show(0);
					}, 1300);
				});
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_2');
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}


	function spriteAnim() {
		global_animation = requestAnimationFrame(spriteAnim);
		time_init = Date.now();
		time_elapsed = time_init - time_then;
		if (time_elapsed > 1000/fps)
		{
			time_then = time_init - (time_elapsed % (1000/fps));
			$(spriteImg).attr('src', preload.getResult(frame_arr[global_counter]).src);
			global_counter++;
			if(global_counter==frame_arr.length) global_counter = 0;
			// if (global_counter >= 19){
				// cancelAnimationFrame(global_animation);
			// }
		}
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

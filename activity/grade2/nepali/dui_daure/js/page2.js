var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story  ts-1 its_hidden',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text2,
			textclass: "text-op-0",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-1 bg-1-anim",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "drama drama-anim",
					imgsrc : '',
					imgid : 'drama'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: ' from-dark-anim',

		uppertextblockadditionalclass: 'text-story  ts-2',
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story-2',
		uppertextblock:[{
			textdata: data.string.p2text4,
			textclass: "its_hidden tx-1",
		},{
			textdata: data.string.p2text5,
			textclass: "its_hidden tx-2",
		},{
			textdata: data.string.p2text6,
			textclass: "its_hidden tx-3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "slide-top bg-p3-1",
					imgsrc : '',
					imgid : 'bg-3-1'
				},{
					imgclass : "slide-top bg-p3-2",
					imgsrc : '',
					imgid : 'bg-3-2'
				},{
					imgclass : "slide-top bg-p3-3",
					imgsrc : '',
					imgid : 'bg-3-3'
				},
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story-3',
		uppertextblock:[{
			textdata: data.string.p2text7,
			textclass: "its_hidden tx-1",
		},{
			textdata: data.string.p2text8,
			textclass: "its_hidden tx-2",
		},{
			textdata: data.string.p2text9,
			textclass: "its_hidden tx-3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "slide-bottom bg-p4-1",
					imgsrc : '',
					imgid : 'bg-4-1'
				},{
					imgclass : "slide-bottom bg-p4-2",
					imgsrc : '',
					imgid : 'bg-4-2'
				},{
					imgclass : "slide-bottom bg-p4-3",
					imgsrc : '',
					imgid : 'bg-4-3'
				},
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story  ts-3',
		uppertextblock:[{
			textdata: data.string.p2text10,
			textclass: "",
		},{
			textdata: data.string.p2text11,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"page2/bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"page2/bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"page2/endbg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drama", src: imgpath+"page2/drama.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-3-1", src: imgpath+"page2/pg-3-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3-2", src: imgpath+"page2/pg-3-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3-3", src: imgpath+"page2/pg-3-3.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-4-1", src: imgpath+"page2/pg-4-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4-2", src: imgpath+"page2/pg-4-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4-3", src: imgpath+"page2/pg-4-3.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1a", src: soundAsset+"p2_s0a.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3a", src: soundAsset+"p2_s2a.ogg"},
			{id: "sound_3b", src: soundAsset+"p2_s2b.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4a", src: soundAsset+"p2_s3a.ogg"},
			{id: "sound_4b", src: soundAsset+"p2_s3b.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},

			{id: "magic-appear", src: soundAsset+"magic-appear.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				$('.ts-1').delay(1000).fadeIn(500, function(){
					current_sound = createjs.Sound.play('sound_1');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.text-op-0').delay(100).css('opacity', '1');
						$('.drama').show(0);
						sound_nav('sound_1a');
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				$('.tx-1').delay(700).fadeIn(500, function(){
					current_sound = createjs.Sound.play('sound_3');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.bg-p3-2').show(0);
						$('.tx-2').delay(700).fadeIn(500, function(){
							current_sound = createjs.Sound.play('sound_3a');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.bg-p3-3').show(0);
								$('.tx-3').delay(700).fadeIn(500, function(){
									sound_nav('sound_3b');
								});
							});
						});
					});
				});
				break;
			case 3:
				$prevBtn.show(0);
				$('.tx-1').delay(700).fadeIn(500, function(){
					current_sound = createjs.Sound.play('sound_4');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.bg-p4-2').show(0);
						$('.tx-2').delay(700).fadeIn(500, function(){
							current_sound = createjs.Sound.play('sound_4a');
							current_sound.play();
							current_sound.on("complete", function(){
								$('.bg-p4-3').show(0);
								$('.tx-3').delay(700).fadeIn(500, function(){
									sound_nav('sound_4b');
								});
							});
						});
					});
				});
				break;
			case 4:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play('sound_5');
					current_sound.play();
					current_sound.on("complete", function(){
						//add if go dark needed
						// $('.contentblock').addClass('go-dark-anim');
						nav_button_controls(1000);
					});
				}, 1000);
				break;
			default:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_2');
				}, 1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		createjs.Sound.stop();
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

/**
 * TODO
 * This function is already declared in the library so no need to redefine it 
 */
Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
	//slide 0	
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q1opt1,
				correctoption :[{}]


			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q1opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q1opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q1opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			},
			],
			
			textwithblank:[{
				textdata : data.string.ques1,
				remain : data.string.remain1
			}]

			
		}
		]
	},
	//slide 1
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q2opt1,
				correctoption :[{}]
				
			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q2opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q2opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q2opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques2,
				remain : data.string.remain2
			}]

			
		}
		]
	},
	//slide 2
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q3opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q3opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q3opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q3opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques3,
				remain : data.string.remain3
			}]

			
		}
		]
	},
	//slide 3
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q4opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q4opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q4opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q4opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques4,
				remain : data.string.remain4
			}]

			
		}
		]
	},
	
	//slide 4
		{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q5opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q5opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q5opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q5opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques5,
				remain : data.string.remain5
			}]

			
		}
		]
	},
	//slide 5
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q6opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q6opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q6opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q6opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques6,
				remain : data.string.remain6
			}]

			
		}
		]
	},
	//slide 6
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q7opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q7opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q7opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q7opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques7,
				remain : data.string.remain7
			}]

			
		}
		]
	},
	//slide 7
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q8opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q8opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q8opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q8opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques8,
				remain : data.string.remain8
			}]

			
		}
		]
	},
	//slide 8
	{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q9opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q9opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q9opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q9opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques9,
				remain : data.string.remain9
			}]

			
		}
		]
	},
	//slide 9
	
{
		uppertextblock : [
			
			{
				textclass : "upper",
				textdata : data.string.top
			}
			
			
		],
		exerciseblock: [
		{
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				labeltext: data.string.q10opt1,
				correctoption :[{}]

			},
			{
				forshuffle: "options_sign incorrectTwo",
				labeltext: data.string.q10opt2,
				wrongoption :[{
					wrongs : "wrongic2",
				}]
			},
			{
				forshuffle: "options_sign incorrectThree",
				labeltext: data.string.q10opt3,
				wrongoption :[{
					wrongs : "wrongic3",
				}]
	
			},
			{
				forshuffle: "options_sign incorrectOne",
				labeltext: data.string.q10opt4,
				wrongoption :[{
					wrongs : "wrongic1",
				}]
	
			}
			],
			textwithblank:[{
				textdata : data.string.ques10,
				remain : data.string.remain10
			}]

			
		}
		]
	},
	];

/*remove this for non random questions*/
// content.shufflearray();

$(function (){	
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;
	var current_sound;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // sounds
            {id: "sound_1", src: soundAsset+"ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        /**
         * TODO
         * This part is not needed so better to remove it
         * I used this for testing soundjs initially and forgot to remove it later
         */
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
	var score = 0;

	/*values in this array is same as the name of images of eggs in image folder*/
	var testin = new RhinoTemplate();
   
 	testin.init(10);

    function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigationcontroller();
        });
    }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$('.rightic').hide(0);
		$('.wrongic1').hide(0);
		$('.wrongic2').hide(0);
		$('.wrongic3').hide(0);
		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		
		switch(countNext) {
			default:
				countNext==0?sound_player("sound_1"):"";
				/**
				 * most of the variables are not used, only incorrect is being used
				 */
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;
				var incorrect = false; 
				
				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					$('.rightic').show(0);
					$('.optionscontainer').removeClass('forHover');
					$(current_btn ).find('p').addClass('option_true');
					$nextBtn.show(0);
					if(!incorrect){
					testin.update(true);
					}
				}
				function incorrect_btn1(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).find('p').addClass('option_false');
					testin.update(false);
					incorrect = true; 
					
				}
				function incorrect_btn2(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).find('p').addClass('option_false');
					testin.update(false);
					$('.wrongic').show(0);
					incorrect = true; 
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).find('p').addClass('option_false');
					$('.wrongic').show(0);
					testin.update(false);
					incorrect = true; 
				}
				break;
		}

		$('.correctOne').click(function(){
			correct_btn(this);
			var texto = $(this).find(".quesLabel");
			var textoo = $(texto).text();
			$('.dashedLine').html(textoo);
			$('.dashedLine').html(textoo).addClass('dsLine');
			current_sound.stop();
			play_correct_incorrect_sound(1);
		});

		$('.incorrectOne').click(function(){
			incorrect_btn(this);
			$('.wrongic1').show(0);
            current_sound.stop();
			play_correct_incorrect_sound(0);
		});
		$('.incorrectTwo').click(function(){
			incorrect_btn(this);
			$('.wrongic2').show(0);
            current_sound.stop();
			play_correct_incorrect_sound(0);
		});
		$('.incorrectThree').click(function(){
			incorrect_btn1(this);
			$('.wrongic3').show(0);
            current_sound.stop();
			play_correct_incorrect_sound(0);
		});
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}


	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		testin.gotoNext();
		countNext++;
			templateCaller();	
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
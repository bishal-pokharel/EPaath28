var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref + "/sound/";

var ex_ins = new buzz.sound(soundAsset + "ex1.ogg");

var content=[

	//ex1
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e1_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e1_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e1_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e1_3,
				}],
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e2_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e2_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e2_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e2_3,
				}],
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e3_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e3_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e3_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e3_3,
				}],
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e4_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e4_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e4_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e4_3,
				}],
			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e5_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e5_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e5_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e5_3,
				}],
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e6_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e6_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e6_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e6_3,
				}],
			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e7_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e7_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e7_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e7_3,
				}],
			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e8_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e8_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e8_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e8_3,
				}],
			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e9_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e9_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e9_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e9_3,
				}],
			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
				instructiondata: data.string.ex_ins_1,
				textdata: data.string.e10_q,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.e10_1,
					},
					{
						forshuffle: "class2",
						optdata: data.string.e10_2,
					},
					{
						forshuffle: "class3",
						optdata: data.string.e10_3,
				}],
			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		countNext==0 ? soundplayer(ex_ins) : '';
		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css("background","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$('.buttonsel').removeClass('forhover forhoverimg');
						$(this).children('.wrngopt').hide(0);
						$(this).children('.corctopt').show(0);
						$(this).siblings('.wrngopt').hide(0);
						$(this).siblings('.corctopt').show(0);
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#a30b21");
						wrngClicked = true;
						$(this).children('.wrngopt').show(0);
						$(this).siblings('.wrngopt').show(0);
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}

	function soundplayer(inputsound){
		$nextBtn.hide(0);
		inputsound.play();
		inputsound.bind('ended', function(){
			// $nextBtn.show(0);
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

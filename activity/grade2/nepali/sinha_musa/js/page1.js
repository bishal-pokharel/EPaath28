var imgpath = $ref + "/images/";

var soundAsset = $ref + "/sound/";

var sound1 = new buzz.sound(soundAsset + "s1p1.ogg");

var content;
content = [{
        contentblocknocenteradjust: true,
        addclass: "content2",
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "01.jpg"
            }]
        }],
        headerblock: [{

            textclass : 'firsttext',
            textdata: data.lesson.chapter

        }
        ]

    },
    //1st
    {
        hasheaderblock: true,
		storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p1text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "02.jpg"
            }]
        }],
    },
    //2nd
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p2text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "03.jpg"
            }]
        }],
    },
    //3rd
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p3text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "04.jpg"
            }]
        }],
    },
    //4th page
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p4text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "09.jpg"
            }]
        }],
    },
    //5th page
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p5text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "05.jpg"
            }]
        }],
    },
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p6text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "06.jpg"
            }]
        }],
    },
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p7text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "07.jpg"
            }]
        }],
    },
    {
        hasheaderblock: true,

        storytextblockadditionalclass : "content2 my_font_medium textblock",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_on",
			textdata : data.string.p8text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_text',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "08.jpg"
            }]
        }],
    }
];


// }
$(function() {
    var $board = $(".board");

    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var $nexx = $(".footerClick");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);


	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    /*
    	TODO:
    */
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
		 What it does:
		 - If not explicitly overriden the method for navigation button
			 controls, it shows the navigation buttons as required,
			 according to the total count of pages and the countNext variable
		 - If for a general use it can be called from the templatecaller
			 function
		 - Can be put anywhere in the template function as per the need, if
			 so should be taken out from the templatecaller function
		 - If the total page number is
		*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.delay(800).show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
		vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true

        switch (countNext) {
            case 0:
                $nextBtn.hide(0);
                sound1.play();
                sound1.bind('ended', function(){
            			$nextBtn.show(0);
            		});
                break;
            case 8:
                for_all_slides('text_story', true, 80);
                break;
            default:
                for_all_slides('text_story', false, 80);
                break;

        }
        texthighlight($board);

    }
    /*
    	TODO:
    */
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });
    /*
    	TODO:
    */
    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);



	function for_all_slides(text_class, last_page_flag, interval){
		var $textblack = $("."+text_class);
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text, interval);
		change_slides(last_page_flag);
	}
	function change_slides(last_page_flag){
		var checking_interval = setInterval(function(){
			if(textanimatecomplete){
				if(!last_page_flag){
    				$nextBtn.show(0);
	    		} else{
	    			ole.footerNotificationHandler.pageEndSetNotification();
	    		}
    			$prevBtn.show(0);
    			clearInterval(checking_interval);
			} else{
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}
		},50);
	}

/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		var stags_counter = 1;
		var stags = message.split(/[<>]/);
		var gt_encounters=[];
	  	if (0 < message.length) {
		  	textanimatecomplete = false;
		  	var nextText = message.substring(0,1);
		  	var brText = message.substring(0,4);
		  	if(brText =='<br>'){
		  		$span_speec_text.append("<br>");
	  			message = message.substring(4, message.length);
		  	}
		  	else if(nextText == "<" ){
		  		gt_encounters.push($span_speec_text.html().length);
		  		// $span_speec_text.append('<'+stags[stags_counter]+'>');
		  		message = message.substring(stags[stags_counter].length+2, message.length);
		  		stags_counter+=2;
		  	}else{
		  		$span_speec_text.append(nextText);
		  		message = message.substring(1, message.length);
		  	}
		  	$this.html($span_speec_text);
		  	$this.append(message);
		    setTimeout(function () {
		    	show_text($this,  $span_speec_text, message, interval);
		  	}, interval);
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	var original_text;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text, interval){
		original_text =  text;
		if(isFirefox){
			$this.html("<p id='span_speec_text'></p>"+text);
			$prevBtn.hide(0);
			var $span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, interval);	// 65 ms is the interval found out by hit and trial
			ternimatesound_play_animate(text);
		} else {
			$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			// $('.text_story').addClass('for_chrome_text');
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				textanimatecomplete =  true;
				animationinprogress = false;
			});
		}

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete){
					$this.html($span_speec_text.html(text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
       				vocabcontroller.findwords(countNext);
				}
			}, 250);
		}
	}
});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

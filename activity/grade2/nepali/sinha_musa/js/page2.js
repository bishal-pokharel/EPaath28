var imgpath = $ref + "/images/";
var content;
var soundAsset = $ref + "/sound/";
var soundAsset1 = $ref + "/sound/textbox/";

var sound1 = new buzz.sound(soundAsset + "les0.mp3");
var sound2 = new buzz.sound(soundAsset + "les1.mp3");
var sound3 = new buzz.sound(soundAsset + "les2.mp3");
var sound4 = new buzz.sound(soundAsset + "les3.mp3");
var sound5 = new buzz.sound(soundAsset + "les4.mp3");
var sound6 = new buzz.sound(soundAsset + "les5.mp3");
var sound7 = new buzz.sound(soundAsset + "les6.mp3");
var sound8 = new buzz.sound(soundAsset + "les7.mp3");
var sound9 = new buzz.sound(soundAsset + "les8.mp3");


var sound_data = sound9;

content = [
    //1st
    {
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_1',
	
		storytextblockadditionalclass : "dialog my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p9text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],

        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "02.jpg"
            }]
        }],
    },
    //2nd
    {
        hasheaderblock: true,
        
        storytextblockadditionalclass : "dialog dialog1 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p10text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "02.jpg"
            }]
        }],
    },
    //3rd
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "ddialog dialog2 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p11text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "03.jpg"
            }]
        }],
    },
    //4th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "ddialog dialog10 dialog3 my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p12text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "04.jpg"
            }]
        }],
    },
    //5th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "dialog dialog4 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p13text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "05.jpg"
            }]
        }],
    },
    //6th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "ddialog dialog5 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p14text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "06.jpg"
            }]
        }],
    },
    //7th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "dialog dialog6 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p15text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "07.jpg"
            }]
        }],
    },
    //8th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "dialog dialog7 my_font_medium",
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p16text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "08.jpg"
            }]
        }],
    },
    //9th page
    {
        hasheaderblock: true,
        storytextblockadditionalclass : "ddialog1 dialog8 my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p17text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
        imageblock: [{
            imagetoshow: [{
                imgclass: "contentwithbg",
                imgsrc: imgpath + "08.jpg"
            }]
        }],
    }
];


// }
$(function() {
    var $board = $(".board");
    
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    /*
    	TODO:
    */
    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
    	 last page of lesson
     - If called from last page set the islastpageflag to true such that
    	 footernotification is called for continue button to navigate to exercise
     */

    /**
		 What it does:
		 - If not explicitly overriden the method for navigation button
			 controls, it shows the navigation buttons as required,
			 according to the total count of pages and the countNext variable
		 - If for a general use it can be called from the templatecaller
			 function
		 - Can be put anywhere in the template function as per the need, if
			 so should be taken out from the templatecaller function
		 - If the total page number is
		*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if (countNext == 6 || countNext == 7) {
            $nextBtn.hide(0);
            $prevBtn.show(0);
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            // $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        switch (countNext) {
            case 0:
                $nextBtn.hide(0);
                for_all_slides('text_story', sound1, false, 100);
                break;
            case 1:
                for_all_slides('text_story', sound2, false, 90);
                break;
            case 2:
                for_all_slides('text_story', sound3, false, 90);
                break;
            case 3:
                for_all_slides('text_story', sound4, false, 98);
                break;
            case 4:
                for_all_slides('text_story', sound5, false, 130);
                break;
            case 5:
                for_all_slides('text_story', sound6, false, 95);
                break;
            case 6:
                for_all_slides('text_story', sound7, false, 90);
                break;
            case 7:
                for_all_slides('text_story', sound8, false, 85);
                break;
            case 8:
                for_all_slides('text_story', sound9, true, 95);
                break;
        }


    }
    /*
    	TODO:
    */
    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationcontroller();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    }

    $nextBtn.on("click", function() {
		sound_data.stop();
        countNext++;
        templateCaller();
    });
    /*
    	TODO:
    */
    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		sound_data.stop();
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);

    
	function for_all_slides(text_class, my_sound_data, last_page_flag, interval){
		var $textblack = $("."+text_class);
		sound_data =  my_sound_data;
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text, interval);
		sound_data.bind('ended', function(){
			change_slides(last_page_flag);
		});
	}
	function change_slides(last_page_flag){
		var checking_interval = setInterval(function(){
			if(textanimatecomplete){
				if(!last_page_flag){
    				$nextBtn.show(0);
	    		} else{
	    			ole.footerNotificationHandler.pageEndSetNotification();
	    		}
    			$prevBtn.show(0);
    			clearInterval(checking_interval);
			} else{
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}
		},50);
	}
	
/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		var stags_counter = 1;
		var stags = message.split(/[<>]/);
		var gt_encounters=[];
	  	if (0 < message.length) {
		  	textanimatecomplete = false;
		  	var nextText = message.substring(0,1);
		  	var brText = message.substring(0,4);
		  	if(brText =='<br>'){
		  		$span_speec_text.append("<br>");
	  			message = message.substring(4, message.length);
		  	}
		  	else if(nextText == "<" ){
		  		gt_encounters.push($span_speec_text.html().length);
		  		// $span_speec_text.append('<'+stags[stags_counter]+'>');
		  		message = message.substring(stags[stags_counter].length+2, message.length);
		  		stags_counter+=2;
		  	}else{
		  		$span_speec_text.append(nextText);
		  		message = message.substring(1, message.length);
		  	}
		  	$this.html($span_speec_text);
		  	$this.append(message);
		    setTimeout(function () {
		    	show_text($this,  $span_speec_text, message, interval);
		  	}, interval);
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	var original_text;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text, interval){
		original_text =  text;
		if(isFirefox){
			$this.html("<span id='span_speec_text'></span>"+text);
			$prevBtn.hide(0);
			var $span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, interval);	// 65 ms is the interval found out by hit and trial
			sound_data.play();
			sound_data.bind('ended', function(){
				// $this.removeClass('text_on');
				// $this.addClass('text_off');
				sound_data.unbind('ended');
				soundplaycomplete = true;
				ternimatesound_play_animate(text);
			});
		} else {
			$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				textanimatecomplete =  true;
				animationinprogress = false;
			});
		}

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
       				vocabcontroller.findwords(countNext);
				}
			}, 250);
		}
	}
	
	/* Typing animation ends */
	
	/*	Function to check if the answer is correct or incorrect as decided by answer_bool
	 *  Display button green or red accordingly with correct sound as well 	 */
	function click_answer(play_class, answer_bool){
		sound_data.stop();
		$('.text_story').removeClass('text_active');
		$('.text_story').addClass('text_inactive');
		$(play_class).removeClass('ans_button_inactive');
		if( answer_bool){
			$(play_class).addClass('ans_button_right');
			$('.next_ques').removeClass('next_ques_inactive');
			$('.next_ques').addClass('next_ques_active');
			sound_correct.play();
			$('.next_ques').css('pointer-events','all');
			$('.text_qna').css({
				'pointer-events': 'none',
			});
			if(last_question){
				ole.footerNotificationHandler.pageEndSetNotification();
				toggle++;
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			}
		} else {
			$(play_class).addClass('ans_button_wrong');
			sound_incorrect.play();
		}
	}
	
	/* function to allow user to click the text and play sound of each sentence clicked
	 * then allow appropriate animation	*/
	function click_text(play_class, my_sound_data){
		sound_data.stop();
		$('.text_story').removeClass('text_active');
		$('.text_story').addClass('text_inactive');
		sound_data = my_sound_data;
		sound_data.play();
		$(play_class).removeClass('text_inactive');
		$(play_class).addClass('text_active');
		sound_data.bind('ended', function(){
			$(play_class).removeClass('text_active');
			$(play_class).addClass('text_inactive');
		});
	}
});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/";
var imgpath1 = $ref+"/images/";


var content=[
		// slide0
		{
		  contentblockadditionalclass: 'purplebg',
			uppertextblock:[{
				textclass:'chaptertitle',
				textdata:data.lesson.chapter
			}],
			imageblock:[{
				imagestoshow:[
				{
					imgclass:'boy',
					imgid:'boy'
				},
				{
					imgclass:'boy1',
					imgid:'boy1'
				},
				{
					imgclass:'boy2',
					imgid:'boy2'
				},
				{
					imgclass:'girl1',
					imgid:'girl1'
				},
				{
					imgclass:'girl2',
					imgid:'girl2'
				}]
			}]
		},
		// slide1
		{
			contentnocenteradjust: true,
		  contentblockadditionalclass: 'purplebg',
			slidingdiv:[{
				definitionblockadditionalclass:'left',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text1,
				},{
					textclass: "text2",
					textdata: data.string.p1text2
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'book.png'
			},
			{
				definitionblockadditionalclass:'right',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text4,
				},{
					textclass: "text2",
					textdata: data.string.p1text5
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'books.png'
			}]


		},
		// slide2
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'purplebg1',
			slidingdiv:[{
				definitionblockadditionalclass:'bottom',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text1,
				},{
					textclass: "text2",
					textdata: data.string.p1text6
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'dog.png'
			},
			{
				definitionblockadditionalclass:'top',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text4,
				},{
					textclass: "text2",
					textdata: data.string.p1text7
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'dogs.png'
			}]


		},
		// slide3
		{
			contentnocenteradjust: true,
		  contentblockadditionalclass: 'purplebg2',
			slidingdiv:[{
				definitionblockadditionalclass:'left',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text1,
				},{
					textclass: "text2",
					textdata: data.string.p1text8
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'glass.png'
			},
			{
				definitionblockadditionalclass:'right',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text4,
				},{
					textclass: "text2",
					textdata: data.string.p1text9
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'glasses.png'
			}]


		},
		// slide4
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'purplebg3',
			slidingdiv:[{
				definitionblockadditionalclass:'bottom',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text1,
				},{
					textclass: "text2",
					textdata: data.string.p1text10
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'boy_new.png'
			},
			{
				definitionblockadditionalclass:'top',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text4,
				},{
					textclass: "text2",
					textdata: data.string.p1text11
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'kids_new.png'
			}]


		},
		// slide5
		{
			contentnocenteradjust: true,
		  contentblockadditionalclass: 'purplebg4',
			slidingdiv:[{
				definitionblockadditionalclass:'left',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text1,
				},{
					textclass: "text2",
					textdata: data.string.p1text12
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'gorse.png'
			},
			{
				definitionblockadditionalclass:'right',
				textblocks:[{
					textclass: "text1",
					textdata: data.string.p1text4,
				},{
					textclass: "text2",
					textdata: data.string.p1text13
				}],
				imgclass: 'singular',
				imgsrc: imgpath+'gorses.png'
			}]


		},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			//images

			// {id: "p01", src: imgpath+"p01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p02", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p03", src: imgpath+"p03.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p04", src: imgpath+"img01a.png", type: createjs.AbstractLoader.IMAGE},

			{id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy1", src: imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy2", src: imgpath+"niraj.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl1", src: imgpath+"asha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"puja.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "books", src: imgpath+"books.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dogs", src: imgpath+"dogs.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_1b", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_1c", src: soundAsset+"s1_p2_2.ogg"},
			{id: "sound_1d", src: soundAsset+"s1_p2_3.ogg"},
			{id: "sound_2a", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_2b", src: soundAsset+"s1_p3_1.ogg"},
			{id: "sound_3a", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_3b", src: soundAsset+"s1_p4_1.ogg"},
			{id: "sound_4a", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_4b", src: soundAsset+"s1_p5_1.ogg"},
			{id: "sound_5a", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_5b", src: soundAsset+"s1_p6_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
    vocabcontroller.findwords(countNext);

		switch(countNext) {
			case 0:
			sound_player("sound_"+countNext);
			break;
			case 1:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1a");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1b");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1c");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1d");
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(200);
				});
			});
		});
	});
},1500);
			break;
			default:
			sound_nav("sound_"+(countNext)+"a","sound_"+(countNext)+"b");
			break;

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_nav(sound_id,sound_id1){
		setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id1);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		});
	},1200);

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

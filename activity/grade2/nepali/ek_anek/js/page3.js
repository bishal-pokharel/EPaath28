var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/";

var content=[

	//slide0
	{
		contentblockadditionalclass: 'purplebg1',

		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text1,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-2',
			imgclass: 'flipped-h',
			imgid : 'textbox2',
			imgsrc: '',
		}],
	},

	// slide1
	{
		contentblockadditionalclass: 'purplebg1',
		extratextblock:[{
			textclass:'text1',
			textdata: data.string.p3text2,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		},
		{
			textclass:'text2',
			textdata: data.string.p3text3,
			datahighlightflag: true,
			datahighlightcustomclass: 'textcolor'
		}]

	},

	//slide2
	{
		contentblockadditionalclass: 'purplebg1',
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text4,
				textclass : 'text_inside',
				datahighlightflag: true,
				datahighlightcustomclass: 'textcolor'
			}],
			speechbox: 'sp-3',
			imgclass: 'flipped-h',
			imgid : 'textbox2',
			imgsrc: '',
		}],
	},

	//slide 3

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text6,
				textclass : 'txt1',
			},
			{
				textdata : data.string.p3text7,
				textclass : 'txt2',
			},
			{
				textdata : data.string.p3text8,
				textclass : 'txt3',
			},
			{
				textdata : data.string.p3text9,
				textclass : 'txt4',
			},
			{
				textdata : data.string.p3text10,
				textclass : 'txt5',
			}],
			containerbox: 'containerbox',
			imageblock:[{
				imagestoshow:[{
					imgclass:'arrow1',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow2',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow3',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'niraj',
					imgsrc: imgpath+'niraj.png'
				},
				{
					imgclass:'prem',
					imgsrc: imgpath+'sagar.png'
				},
				{
					imgclass:'suraj',
					imgsrc: imgpath+'suraj.png'
				},
				{
					imgclass:'sagar',
					imgsrc: imgpath+'prem.png'
				},]
			}]

		}],
	},

	//slide 4

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text11,
				textclass : 'txt1',
			},
			{
				textdata : data.string.p3text7,
				textclass : 'txt2',
			},
			{
				textdata : data.string.p3text12,
				textclass : 'txt3',
			},
			{
				textdata : data.string.p3text13,
				textclass : 'txt4',
			},
			{
				textdata : data.string.p3text10,
				textclass : 'txt5',
			}],
			containerbox: 'containerbox',
			imageblock:[{
				imagestoshow:[{
					imgclass:'arrow1',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow2',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow3',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'cow1',
					imgsrc: imgpath+'cow01.png'
				},
				{
					imgclass:'cow2',
					imgsrc: imgpath+'cow02.png'
				},
				{
					imgclass:'cow3',
					imgsrc: imgpath+'cow03.png'
				}]
			}]

		}],
	},

	//slide 5

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text14,
				textclass : 'txt1',
			},
			{
				textdata : data.string.p3text7,
				textclass : 'txt2',
			},
			{
				textdata : data.string.p3text15,
				textclass : 'txt3',
			},
			{
				textdata : data.string.p3text16,
				textclass : 'txt4',
			},
			{
				textdata : data.string.p3text10,
				textclass : 'txt5',
			}],
			containerbox: 'containerbox',
			imageblock:[{
				imagestoshow:[{
					imgclass:'arrow1',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow2',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'arrow3',
					imgsrc: imgpath+'arrow.png'
				},
				{
					imgclass:'girl1',
					imgsrc: imgpath+'puja.png'
				},
				{
					imgclass:'girl2',
					imgsrc: imgpath+'asha.png'
				},
				{
					imgclass:'girl3',
					imgsrc: imgpath+'rumi.png'
				}]
			}]

		}],
	},

	//slide 6

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[
			{
					textblock:[
						{
							textdata : data.string.p3text7,
							textclass : 'ek1',
						},
						{
							textdata : data.string.p3text6,
							textclass : 'ek2',
						},
						{
							textdata : data.string.p3text11,
							textclass : 'ek3',
						},
						{
							textdata : data.string.p3text17,
							textclass : 'ek4',
						},
						{
							textdata : data.string.p3text14,
							textclass : 'ek5',
						},
						{
							textdata : data.string.p3text19,
							textclass : 'ek6',
						},
						{
							textdata : data.string.p3text21,
							textclass : 'ek7',
						},
						{
							textdata : data.string.p3text23,
							textclass : 'ek8',
						}
				],
						containerbox: 'containerbox2',
						imageblock:[{
							imagestoshow:[{
								imgclass:'arr1',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr2',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr3',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr4',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr5',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr6',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr7',
								imgsrc: imgpath+'arrow.png'
							}]
						}]
			},
			{
					textblock:[
						{
							textdata : data.string.p3text10,
							textclass : 'ba1',
						},
						{
							textdata : data.string.p3text9,
							textclass : 'ba2',
						},
						{
							textdata : data.string.p3text13,
							textclass : 'ba3',
						},
						{
							textdata : data.string.p3text18,
							textclass : 'ba4',
						},
						{
							textdata : data.string.p3text16,
							textclass : 'ba5',
						},
						{
							textdata : data.string.p3text20,
							textclass : 'ba6',
						},
						{
							textdata : data.string.p3text22,
							textclass : 'ba7',
						},
						{
							textdata : data.string.p3text24,
							textclass : 'ba8',
						}
				],
					containerbox: 'containerbox1'
			}

		],
	},

	//slide 7
	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text25,
				textclass : 'text25',
			},
			{
				textdata : data.string.p3text26,
				textclass : 'text26',
			},
			{
				textdata : data.string.p3text261,
				textclass : 'text27',
			}],
			containerbox: 'containerbox3',
			imageblock:[{
				imagestoshow:[{
					imgclass:'textbox',
					imgsrc: imgpath+'text_box.png'
				},{
					imgclass:'arrow',
					imgsrc: imgpath+'arrow.png'
				}]
			}]

		}],
	},

	//slide 8
	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text27,
				textclass : 'text25',
			},
		],
			containerbox: 'containerbox3',
			imageblock:[{
				imagestoshow:[{
					imgclass:'textbox',
					imgsrc: imgpath+'text_box.png'
				}]
			}]

		}],
	},

	//slide 9

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[
			{
					textblock:[
						{
							textdata : data.string.p3text7,
							textclass : 'ek1',
						},
						{
							textdata : data.string.p3text28,
							textclass : 'ek2',
						},
						{
							textdata : data.string.p3text30,
							textclass : 'ek3',
						},
						{
							textdata : data.string.p3text32,
							textclass : 'ek4',
						},
						{
							textdata : data.string.p3text34,
							textclass : 'ek5',
						},
						{
							textdata : data.string.p3text36,
							textclass : 'ek6',
						},

				],
						containerbox: 'containerbox2',
						imageblock:[{
							imagestoshow:[{
								imgclass:'arr1',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr2',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr3',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr4',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr5',
								imgsrc: imgpath+'arrow.png'
							}]
						}]
			},
			{
					textblock:[
						{
							textdata : data.string.p3text10,
							textclass : 'ba1',
						},
						{
							textdata : data.string.p3text29,
							textclass : 'ba2',
						},
						{
							textdata : data.string.p3text31,
							textclass : 'ba3',
						},
						{
							textdata : data.string.p3text33,
							textclass : 'ba4',
						},
						{
							textdata : data.string.p3text35,
							textclass : 'ba5',
						},
						{
							textdata : data.string.p3text37,
							textclass : 'ba6',
						}
				],
					containerbox: 'containerbox1'
			}

		],
	},

	//slide 10
	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[{
			textblock:[
			{
				textdata : data.string.p3text38,
				textclass : 'text25',
			},
		],
			containerbox: 'containerbox3',
			imageblock:[{
				imagestoshow:[{
					imgclass:'textbox',
					imgsrc: imgpath+'text_box.png'
				}]
			}]

		}],
	},

	//slide 11

	{
		contentblockadditionalclass: 'purplebg3',
		containerbox:[
			{
					textblock:[
						{
							textdata : data.string.p3text7,
							textclass : 'ek1',
						},
						{
							textdata : data.string.p3text39,
							textclass : 'ek2',
						},
						{
							textdata : data.string.p3text40,
							textclass : 'ek3',
						},
						{
							textdata : data.string.p3text41,
							textclass : 'ek4',
						},
						{
							textdata : data.string.p3text42,
							textclass : 'ek5',
						},
						{
							textdata : data.string.p3text43,
							textclass : 'ek6',
						},

				],
						containerbox: 'containerbox2',
						imageblock:[{
							imagestoshow:[{
								imgclass:'arr1',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr2',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr3',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr4',
								imgsrc: imgpath+'arrow.png'
							},
							{
								imgclass:'arr5',
								imgsrc: imgpath+'arrow.png'
							}]
						}]
			},
			{
					textblock:[
						{
							textdata : data.string.p3text10,
							textclass : 'ba1',
						},
						{
							textdata : data.string.p3text44,
							textclass : 'ba2',
						},
						{
							textdata : data.string.p3text45,
							textclass : 'ba3',
						},
						{
							textdata : data.string.p3text46,
							textclass : 'ba4',
						},
						{
							textdata : data.string.p3text47,
							textclass : 'ba5',
						},
						{
							textdata : data.string.p3text48,
							textclass : 'ba6',
						},
				],
					containerbox: 'containerbox1'
			}

		],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var clicked1 = false;
	var clicked2 = false;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			// {id: "font-css", src: "css/sandeep_fonts.css", type: createjs.AbstractLoader.CSS},
			//images

      {id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},


			{id: "textbox0", src: imgpath+"text_box06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox1", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox2", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox3", src: imgpath+"text_box05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi1", src: imgpath+"rumi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumiwithdog", src: imgpath+"rumi_with_dog_food01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumiwithdoggif", src: imgpath+"rumi_with_dog_food01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl", src: imgpath+"dog_bowl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tick", src: imgpath+"tick_mark.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curtainl", src: imgpath+"curtain_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curtainr", src: imgpath+"curtain_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_filter", src: imgpath+"water_filter01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_filter_gif", src: imgpath+"water_filter.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bag1", src: imgpath+"bag01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb1", src: imgpath+"bulb_off.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb2", src: imgpath+"bulb_on.png", type: createjs.AbstractLoader.IMAGE},
			{id: "switchon", src: imgpath+"switch_on.png", type: createjs.AbstractLoader.IMAGE},
			{id: "switchoff", src: imgpath+"switch_off.png", type: createjs.AbstractLoader.IMAGE},


			//textboxesp01
			{id: "coverpage", src: imgpath+'coverpage.png', type: createjs.AbstractLoader.IMAGE},
			{id: "icon-orange", src: imgpath+'icon-orange.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sunImg", src: imgpath+'sun.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_1b", src: soundAsset+"s3_p2_1.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_6a", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_6b", src: soundAsset+"s3_p7_1.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_9a", src: soundAsset+"s3_p10.ogg"},
			{id: "sound_9b", src: soundAsset+"s3_p10_1.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_11a", src: soundAsset+"s3_p12.ogg"},
			{id: "sound_11b", src: soundAsset+"s3_p12_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
        vocabcontroller.findwords(countNext);

    $('.txt1,.txt2,.txt3,.txt4,.txt5,.txt6,.arrow1,.arrow2,.arrow3,.niraj,.prem,.suraj,.sagar,.cow1,.cow2,.cow3,.girl1,.girl2,.girl3').hide();
   $('.ek2,.ek3,.ek4,.ek5,.ek6,.ek7,.ek8,.arr1,.arr2,.arr3,.arr4,.arr5,.arr6,.arr7,.arr8,.ba2,.ba3,.ba4,.ba5,.ba6,.ba7,.ba8').hide();

		switch(countNext) {

				case 1:
					sound_nav("sound_"+countNext+"a","sound_"+countNext+"b");
					break;

				case 3:
					$('.txt1,.arrow1').delay(800).fadeIn(1500);
					$('.txt2,.niraj').delay(1500).fadeIn(1500);
					$('.txt3,.arrow2').delay(2500).fadeIn(1500);
					$('.txt4,.arrow3').delay(3000).fadeIn(1500);
					$('.txt5,.prem,.suraj,.sagar').delay(3500).fadeIn(1500);
					sound_player("sound_"+countNext);
					break;
				case 4:
          $('.txt1,.arrow1').delay(800).fadeIn(1500);
					$('.txt2,.cow1').delay(1500).fadeIn(1500);
					$('.txt3,.arrow2').delay(2500).fadeIn(1500);
					$('.txt4,.arrow3').delay(3000).fadeIn(1500);
					$('.txt5,.cow2,.cow3').delay(3500).fadeIn(1500);
					sound_player("sound_"+countNext);
					break;
				case 5:
          $('.txt1,.arrow1').delay(800).fadeIn(1500);
					$('.txt2,.girl1').delay(1500).fadeIn(1500);
					$('.txt3,.arrow2').delay(2500).fadeIn(1500);
					$('.txt4,.arrow3').delay(3000).fadeIn(1500);
					$('.txt5,.girl2,.girl3').delay(3500).fadeIn(1500);
					sound_player("sound_"+countNext);
					break;

				case 6:
				  animateThings(800,600,1500,2,1)
                    sound_player("sound_"+countNext+"a");
					break;

				case 8:
				case 10:
					sound_player("sound_"+countNext);
					$('.text25').css('width','95%');
					break;

				case 9:

				  $('.containerbox1,.containerbox2').css('width','50%');
					$('.arr1,.arr2,.arr3,.arr4,.arr5').css({'right':'-11%','width':'40%'});

					increasecss(20,1);
                    sound_player("sound_"+countNext+"a");
				  animateThings(800,800,1500,2,1);
					break;

				case 11:

				  $('.ek1,.ek2,.ek3,.ek4,.ek5,.ek6,.ek7,.ek8,.ba1,.ba2,.ba3,.ba4,.ba5,.ba6,.ba7,.ba8').css('padding','3%');
					animateThings(800,800,1500,2,1)
					increasecss(21,1);
                    sound_player("sound_"+countNext+"a");
					break;

				default:
                    sound_player("sound_"+countNext);
					break;

		}
	}

	function increasecss(initialtop,count){

    console.log('initialtop '+(initialtop+'%'));

		$('.arr'+count).css('top',initialtop+'%');
		count = count + 1;
		initialtop = initialtop + 13;

		if (count<6){
			increasecss(initialtop,count);
		}

	}

  function animateThings(delaytime,delaytime1,fadeInTime,countek,countba){

		$('.ek'+countek).delay(delaytime).fadeIn(fadeInTime);
		$('.arr'+countba).delay(delaytime).fadeIn(fadeInTime);
	 	$('.ba'+countek).delay(delaytime1).fadeIn(fadeInTime);

		delaytime = delaytime + 1000;
		delaytime1 = delaytime1 + 1000;
		countek = countek + 1;
		countba = countba + 1;

    if(countek<9){
	    animateThings(delaytime,delaytime1,fadeInTime,countek,countba);
		}

	}


	function dragdrop(){
		  console.log('dragdrop');
			$(".draggable").draggable({
					containment: "body",
					revert: true,
					appendTo: "body",
					zindex: 10,
			});
			$('.droppable').droppable({
					accept : ".draggable",
					hoverClass: "hovered",
					drop: function(event, ui) {
             $('.bag1').show();
						 $('.book').hide();
						 nav_button_controls(100);
					}
			});
	}

	function check(){
		if (clicked1 === true && clicked2 === true){
				console.log('true');
				$('.option').css('animation-name','pulsfs');
				$('.option').click(function(){
					nav_button_controls(100);
				});
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
			setTimeout(function(){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
			},1200);
	}
	function sound_nav(sound_id,sound_id1){
		setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(sound_id1);
				current_sound.play();
				current_sound.on("complete", function(){
				nav_button_controls(200);
			});
		});
	},1200);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		clicked1=false,clicked2 = false;

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

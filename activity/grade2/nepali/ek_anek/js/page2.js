var imgpath = $ref+"/images/diy/";
var soundAsset = $ref+"/sounds/";

var no_of_draggable = 4; //no of draggable to display at a time
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var content=[

  {
    contentblockadditionalclass: 'purplebg',
    uppertextblockadditionalclass: 'test',
    uppertextblock:[{
      textclass:'chaptertitle',
      textdata:data.string.p2text1
    }],
    imageblock:[{
			imagestoshow:[{
				imgclass: 'coverpage',
				imgsrc: imgpath+'a_08.png'
			}]
		}]
  },

	//2th slide
	{
		// contentblockadditionalclass: "simplebg",
  		contentnocenteradjust: true,
  		textblockadditionalclass: 'instruction',
	  	textblock: [{
	  		textdata: data.string.p2text2,
	  		textclass: 'head-title'
  		}],
      imageblock:[{
  			imagestoshow:[
        {
          imgclass: 'monkey',
  				imgsrc: imgpath+'welldone01.png'
        }]
  		}],
	  	// draggableblockadditionalclass: 'frac_ques',
	  	draggableblock:[{
        		draggables:[{
      	  			draggableclass:"class_1 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "a-horse.png",
                      label:data.string.p2text5
      			},{
      	  			draggableclass:"class_1 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "a-mouse.png",
                      label:data.string.p2text6
      			},{
      	  			draggableclass:"class_1 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "asha01.png",
                      label:data.string.p2text8//need to change
      			},{
      	  			draggableclass:"class_1 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "ball.png",
                      label:data.string.p2text7
      			},{
      	  			draggableclass:"class_2 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "bids.png",
                      label:data.string.p2text12
      			},{
      	  			draggableclass:"class_2 sliding hidden",
      	  			has_been_dropped : false,
        				imgclass: "",
        				imgsrc: imgpath + "corns.png",
                      label:data.string.p2text11
      			},{
      	  			draggableclass:"class_2 sliding hidden",
      	  			has_been_dropped : false,
        				imgclass: "",
        				imgsrc: imgpath + "four_kids.png",
                label:data.string.p2text14
                  },
                  {
      	  			draggableclass:"class_1 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "pen.png",
                      label:data.string.p2text9
      			},{
      	  			draggableclass:"class_2 sliding hidden",
      	  			has_been_dropped : false,
      				imgclass: "",
      				imgsrc: imgpath + "pens.png",
                      label:data.string.p2text10
      			},
            {
      	  			draggableclass:"class_2 sliding hidden",
      	  			has_been_dropped : false,
      		      imgclass: "",
      		      imgsrc: imgpath + "three_glasses.png",
                label:data.string.p2text13
      			},]
	  	    }],
  		droppableblock:[{
      	  		droppables:[{
      	  			headerdata: data.string.p1text1,
      	  			headerclass: "identity",
      	  			droppablecontainerclass:"",
      	  			droppableclass:"drop_class_1",
      				  imgclass: "",
      				   imgsrc: imgpath + "100BLOCKS.png"
      			   },{
      	  			headerdata: data.string.p1text4,
      	  			headerclass: "identity",
      	  			droppablecontainerclass:"",
      	  			droppableclass:"drop_class_2",
      				imgclass: "",
      				imgsrc: imgpath + "100BLOCKS.png"
      			}]
      }]
	}

];

var dummy_class = {
	draggableclass:"dummy_class hidden",
	imgclass: "",
	imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */

console.log('content '+content.length);

content[1].draggableblock[0].draggables.shufflearray();
for( var i = 1; i<no_of_draggable+1; i++){
	var asd = content[1].draggableblock[0].draggables[i-1].draggableclass.split('"')[0].split('hidden');
	content[1].draggableblock[0].draggables[i-1].draggableclass = asd[0]+'position_'+i;
	content[1].draggableblock[0].draggables.push(dummy_class);
}

$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [

      //images

      // {id: "p01", src: imgpath+"p01.png", type: createjs.AbstractLoader.IMAGE},
      // {id: "p02", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
      // {id: "p03", src: imgpath+"p03.png", type: createjs.AbstractLoader.IMAGE},
      // {id: "p04", src: imgpath+"img01a.png", type: createjs.AbstractLoader.IMAGE},

      {id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boy1", src: imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boy2", src: imgpath+"niraj.png", type: createjs.AbstractLoader.IMAGE},
      {id: "girl1", src: imgpath+"asha.png", type: createjs.AbstractLoader.IMAGE},
      {id: "girl2", src: imgpath+"puja.png", type: createjs.AbstractLoader.IMAGE},
      {id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
      {id: "books", src: imgpath+"books.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dogs", src: imgpath+"dogs.png", type: createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_1", src: soundAsset+"s2_p2.ogg"},
      {id: "sound_2", src: soundAsset+"badhai.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded*100)+'%');
  }
  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

	Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());
	Handlebars.registerPartial("uppertextcontent", $("#uppertextcontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}


	var score = 0;
	var drop = 0;
	/*random scoreboard eggs*/
	var wrngClicked = [false, false, false, false, false, false, false, false, false, false];

	// var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 	// testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(".draggable").hover(function() {
			var datacontent = $(this).attr("data-answer");
			$(this).find("label").html(datacontent);
        }, function() {
			$(this).find("label").html('');
    });
		/*generate question no at the beginning of question*/
		switch(countNext) {
      case 0:
         play_diy_audio();
         nav_button_controls(2000);
         break;
			case 1:
        sound_player("sound_1");
				$(".draggable").draggable({
					containment : "body",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(this).css({"opacity": "0.5"});
						$(ui.helper).addClass("ui-draggable-helper");
						$(ui.helper).removeClass("sliding");
            console.log('m in start dropping here');
					},
					stop: function(event, ui){
						$(this).css({"opacity": "1"});
					}
				});
				$('.drop_class_1').droppable({
					// accept : ".class_1",
					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_1")){
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									// testin.update(true);
								}
							}
							drop++;
              play_correct_incorrect_sound(1);
              gotoscorepage(drop);
							console.log(drop);
							handleCardDrop(event, ui, ".class_1" , ".drop_class_1");
						}else{
							if(($(ui.draggable).data("dropped"))==false){
								wrngClicked[countNext] = true;
								// testin.update(false);
							}
              play_correct_incorrect_sound(0);
						}

						if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				$('.drop_class_2').droppable({
					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_2")){
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									// testin.update(true);
								}
							}
							drop++;
              play_correct_incorrect_sound(1);
							console.log(drop);
                            gotoscorepage(drop);
                            handleCardDrop(event, ui, ".class_2" , ".drop_class_2");
						}else{
							if(($(ui.draggable).data("dropped"))==false){
								wrngClicked[countNext] = true;
								// testin.update(false);
							}
              play_correct_incorrect_sound(0);
						}

						if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				function handleCardDrop(event, ui, classname, droppedOn) {
          createjs.Sound.stop();
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					// var to count no. of divs in the droppable div
					var drop_index = $(droppedOn+">div").length;
					var top_position = 10*drop_index;
					var left_position = 18*drop_index;
					$(ui.draggable).removeClass("sliding");
					$(ui.draggable).detach().css({
						"cursor": 'auto',
						"width": "30%",
						"max-height": "45%",
						"flex": "0 0 32%"
					}).appendTo(droppedOn);
					var $newEntry = $(".draggableblock> .hidden").eq(0);

					var $draggable3;
					var $draggable2;
					var $draggable1;
					if(dropped.hasClass("position_4")){
						dropped.removeClass("position_4");
						$draggable3 = $(".position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_3")){
						dropped.removeClass("position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_2")){
						dropped.removeClass("position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_1")){
						dropped.removeClass("position_1");
					}

					if($draggable3 != null){
						 $draggable3.removeClass("position_3").addClass("position_4");
						 $draggable3.removeClass('sliding');
						 setTimeout(function() {
							    $draggable3.addClass('sliding');
						},1);
					}
					if($draggable2 != null){
						 $draggable2.removeClass("position_2").addClass("position_3");
						 $draggable2.removeClass('sliding');
						 setTimeout(function() {
							    $draggable2.addClass('sliding');
						},1);
					}
					if($draggable1 != null){
						 $draggable1.removeClass("position_1").addClass("position_2");
						 $draggable1.removeClass('sliding');
						 setTimeout(function() {
							    $draggable1.addClass('sliding');
						},1);
					}
					if($newEntry != null){
						 $newEntry.removeClass("hidden").addClass("position_1");
					}
					if(drop == 10) {
            createjs.Sound.stop();

            $('.monkey').css('visibility','visible');
            $('.head-title').text(data.string.congrates);
            setTimeout(()=>sound_player1("sound_2"),800);
					}
				}
			break;
			default:
				break;
		}
	}

    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          console.log('in here countnext'+countNext);
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    	function sound_player(sound_id){
    		createjs.Sound.stop();
    		current_sound = createjs.Sound.play(sound_id);
    		current_sound.play();
    	}
      function sound_player1(sound_id){
    		createjs.Sound.stop();
    		current_sound = createjs.Sound.play(sound_id);
    		current_sound.play();
    		current_sound.on("complete", function(){
           $prevBtn.show(0);
           ole.footerNotificationHandler.pageEndSetNotification();
    		});
    	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
    loadTimelineProgress($total_page, countNext + 1);
		// call navigation controller
		// navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		      });
		    }
		*/

	}


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){

    console.log('countNext '+countNext,'drop '+drop);

    switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	//
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});



	function gotoscorepage(drop){
        if(drop==10){
            $nextBtn.click(function(){
                // testin.gotoNext();
            });
        }
        else{
            // testin.gotoNext();
        }
	}
/*=====  End of Templates Controller Block  ======*/
});

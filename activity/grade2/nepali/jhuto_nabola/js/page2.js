var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-1',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				}
			]
		}],
		svgblock: [{
			svgblock: 'bush-svg',
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-2',
		uppertextblock:[{
			textdata: data.string.p2text2,
			textclass: "",
		},{
			textdata: data.string.p2text2a,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-2'
				}
			]
		}],
		svgblock: [{
			svgblock: 'sound-svg',
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-3',
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "",
		},{
			textdata: data.string.p2text3a,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-3'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-4',
		uppertextblock:[{
			textdata: data.string.p2text4,
			textclass: "its_hidden text-1",
		},{
			textdata: data.string.p2text5,
			textclass: "its_hidden text-2",
		}],
		imagedivblock:[{
			imagediv: 'total-cover',
			svgdiv: 'boy-svg',
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-4'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-5',
		uppertextblock:[{
			textdata: data.string.p2text6,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-7'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-6',
		uppertextblock:[{
			textdata: data.string.p2text7,
			textclass: "",
		},{
			textdata: data.string.p2text7a,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-8'
				}
			]
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-7',
		uppertextblock:[{
			textdata: data.string.p2text8,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-9'
				}
			]
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-8',
		uppertextblock:[{
			textdata: data.string.p2text9,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-10'
				}
			]
		}],
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story ts-9',
		uppertextblock:[{
			textdata: data.string.p2text10,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-11'
				}
			]
		}],
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'text-story text-font-2 ts-10',
		uppertextblock:[{
			textdata: data.string.p2text11,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-13'
				}
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var my_interval = null;
	var my_interval2 = null;
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"15.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"bg_16.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-4", src: imgpath+"17a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-7", src: imgpath+"20.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-8", src: imgpath+"21.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-9", src: imgpath+"22.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-10", src: imgpath+"24.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-11", src: imgpath+"25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-12", src: imgpath+"26.png", type: createjs.AbstractLoader.IMAGE},
			{id: "im-13", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bush", src: imgpath+"bush.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "hurr", src: imgpath+"hurr.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy.svg", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1a", src: soundAsset+"p2_s0a.ogg"},
			{id: "sound_1b", src: soundAsset+"p2_s0_bg.ogg"},
			{id: "sound_2bg", src: soundAsset+"p2_s1_bg.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4a", src: soundAsset+"p2_s3a.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);

		switch(countNext) {
			case 0:
				sound_player("sound_1b");
				var s = Snap('#bush-svg');
				var bush, bush_path_1, bush_path_2;
				var svg = Snap.load(preload.getResult('bush').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					bush			= Snap.select('#path01');
					var path_2		= Snap.select("#path02");
					bush_path_1		= bush.attr('path');
					bush_path_2		= path_2.attr('path');
				} );
				function move_to_2(){
					bush.animate({ d: bush_path_2 }, 400, mina.linear, move_to_1);
				}
				function move_to_1(){
					bush.animate({ d: bush_path_1}, 400, mina.linear, move_to_2);
				}
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					move_to_2();
					current_sound = createjs.Sound.play('sound_1a');
					current_sound.play();
					current_sound.on("complete", function(){
						$('.contentblock').addClass('zoom-1');
						$nextBtn.delay(8000).show(0);
					});
				});
				break;
			case 1:
				sound_player("sound_2bg");
				$prevBtn.show(0);
				var s = Snap('#sound-svg');
				var countLine = 0;
				var countDot = 0;
				var line = [], dot = [];
				var svg = Snap.load(preload.getResult('hurr').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					for(var i=1; i<10; i++){
						line.push(Snap.select('#line0'+i));
					}
					for(var j=10; j<16; j++){
						line.push(Snap.select('#line'+j));
					}
					for(var k=1; k<7; k++){
						dot.push(Snap.select('#dot0'+k));
					}
					// console.log(line);
				} );
				function animate_lines(){
					my_interval =  setInterval(function(){
						for(var i=0; i<line.length; i++){
							if(countLine%4==0){
								if(i%3==0) line[i].attr('display','block');
							} else if(countLine%4==1){
								if(i%3==1) line[i].attr('display','block');
							} else if(countLine%4==2){
								if(i%3==2) line[i].attr('display','block');
							} else{
								line[i].attr('display','none');
							}
						}
						countLine++;
						if(countLine==4){
							countDot = 0;
						}
					}, 150);
				}
				function animate_dots(){
					my_interval2 =  setInterval(function(){
						for(var i=0; i<dot.length; i++){
							if(countDot%4==0){
								if(i%3==0) dot[i].attr('display','block');
							} else if(countDot%4==1){
								if(i%3==1) dot[i].attr('display','block');
							} else if(countDot%4==2){
								if(i%3==2) dot[i].attr('display','block');
							} else{
								dot[i].attr('display','none');
							}
						}
						countDot++;
						if(countDot==4){
							countDot = 0;
						}
					}, 200);
				}
				$prevBtn.show(0);
				current_sound = createjs.Sound.play('sound_2');
				current_sound.play();
				current_sound.on("complete", function(){
					$('#sound-svg').show(0);
					$('.contentblock').addClass('flashit');
					animate_lines();
					animate_dots();
					$nextBtn.delay(3000).show(0);
				});
				break;
			case 3:
				$prevBtn.show(0);
				var s = Snap('#boy-svg');
				var countLine = 0;
				var countDot = 0;
				var eye1, eye2, leg1, leg2, pleg1, pleg2, tear;
				var svg = Snap.load(preload.getResult('boy').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					eye1			= Snap.select('#eyeball-1');
					eye2			= Snap.select('#eyeball-2');
					leg1			= Snap.select('#path01');
					pleg1			= leg1.attr('path');
					leg2			= Snap.select('#path02');
					pleg2			= leg2.attr('path');
					tear			= Snap.select('#tear');
					// tear.attr('display','none');
					console.log(leg1);
				} );
				function shake_1(){
					leg1.animate({ d: pleg2 }, 100, mina.linear, shake_2);
				}
				function shake_2(){
					leg1.animate({ d: pleg1}, 100, mina.linear, shake_1);
				}
				function red_eye(){
					eye1.animate({ fill: '#F9B1B4'}, 300, mina.linear);
					eye2.animate({ fill: '#F9B1B4'}, 300, mina.linear);
				}
				$('.total-cover').addClass('zoom-in');
				timeoutvar = setTimeout(function(){
					$('.text-1').fadeIn(100,function(){
						current_sound = createjs.Sound.play('sound_4');
						current_sound.play();
						shake_1();
						current_sound.on("complete", function(){
							// $nextBtn.show(0);
							$('.total-cover').removeClass('zoom-in').addClass('pan-up');
							timeoutvar = setTimeout(function(){
								$('.text-2').fadeIn(100,function(){
									red_eye();
									sound_nav('sound_4a');
								});
							},1500);
						});
					});
				},3000);
				break;
			case 8:
				$prevBtn.show(0);
				current_sound = createjs.Sound.play('sound_9');
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bg-full').attr('src', preload.getResult('im-12').src);
					$nextBtn.show(0);
				});
				break;
			default:
				$prevBtn.show(0);
				sound_nav('sound_'+(countNext+1));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		case 1:
			clearInterval(my_interval);
			clearInterval(my_interval2);
			countNext++;
			templateCaller();
			break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

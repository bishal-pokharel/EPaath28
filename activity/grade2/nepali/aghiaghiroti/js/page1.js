var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"chtitle",
                textclass:"chapter centertext",
                textdata:data.string.lesson_title
            },
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontextfirst",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [

                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text ",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtextfirst",
                textclass:"content centertext",
                textdata:data.string.p1text2
            },
            {
                textdiv:"topdiv"
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim",
                    imgclass: "relativecls img2",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text2
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text3
            },
        ]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text4
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext"
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text3
            },
        ]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg1",
                    imgclass: "relativecls img1",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boximg wobbleEffect",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv1",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone1",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone1",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text4
            },
            {
                textdiv:"thinkbox",
                textclass:"content centertext",
                textdata:data.string.query1
            },
            {
                textdiv:"wholediv",
            },
        ]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg",
                    imgclass: "relativecls img1",
                    imgid: 'img5Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text5
            },
            {
                textdiv:"leftdiv1 hidetext"
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text1",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text1",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text4
            },
        ]
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img6Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img5Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                textdiv:"leftdiv1 hidetext"
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text5
            },
        ]
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img7Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img6Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text6
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img8Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img7Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text7
            },
            {
                textdiv:"topdiv"
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text6
            },
        ]
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img9Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img8Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text8
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text7
            },
        ]
    },
//    slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img10Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img9Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text8
            },
        ]
    },
//    slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext ",
                    imgclass: "relativecls img1",
                    imgid: 'img11Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img10Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text9
            },
            {
                textdiv:"topdiv"
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
        ]
    },
//    slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg",
                    imgclass: "relativecls img1",
                    imgid: 'img12Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img11Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text10
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text9
            },
        ]
    },
//    slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img13Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img12Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_2
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text11
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text10
            },
        ]
    },
//    slide 15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg1",
                    imgclass: "relativecls img1",
                    imgid: 'img13Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boximg wobbleEffect",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv1",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone1",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_2
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone1",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text11
            },
            {
                textdiv:"thinkbox",
                textclass:"content centertext",
                textdata:data.string.query2
            },
            {
                textdiv:"wholediv",
            },
        ]
    },
//    slide 16
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg",
                    imgclass: "relativecls img1",
                    imgid: 'img14Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img13Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text12
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_2
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text11
            },
        ]
    },
//    slide 17
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img15Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img14Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text13
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text12
            },
        ]
    },
//    slide 18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img16Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img15Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_3
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text13
            },
        ]
    },
//    slide 19
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img17Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img16Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text14
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_3
            },
        ]
    },
//    slide 20
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg1",
                    imgclass: "relativecls img1",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boximg wobbleEffect",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv1",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone1",
                textdiv:"momtextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text14
            },
            {
                textdiv:"thinkbox",
                textclass:"content centertext",
                textdata:data.string.query3
            },
            {
                textdiv:"wholediv",
            },
        ]
    },
//    slide 21
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg",
                    imgclass: "relativecls img1",
                    imgid: 'img18Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img17Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_3
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text15
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"leftdiv1 hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text14
            },
        ]
    },
//    slide 22
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg",
                    imgclass: "relativecls img1",
                    imgid: 'img19Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img18Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text16
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_3
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text15
            },
        ]
    },
//    slide 23
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img20Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img19Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text17
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text16
            },
        ]
    },
//    slide 24
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimgnext",
                    imgclass: "relativecls img1",
                    imgid: 'img21Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img20Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text1_5
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"momtext vocab",
                textclass:"content centertext",
                textdata:data.string.p1text18
            },
            {
                textdiv:"topdiv"
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text17
            },
        ]
    },
    //slide 25
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "rightimg ",
                    imgclass: "relativecls img1",
                    imgid: 'img22Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "openimage openanim ",
                    imgclass: "relativecls img2",
                    imgid: 'img21Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"line"
            },
            {
                textdiv:"leftdiv openanim",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "onebyone",
                textdiv:"sontextfirst vocab",
                textclass:"content centertext",
                textdata:data.string.p1text19
            },
            {
                textdiv:"leftdiv1 hidetext",
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"sontext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text1_5
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "text",
                textdiv:"momtext hidetext",
                textclass:"content centertext",
                textdata:data.string.p1text18
            },
            {
                textdiv:"topdiv"
            },
        ]
    },
//    slide 26
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "lastimg",
                    imgclass: "relativecls img1",
                    imgid: 'img23Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "boy boy01",
                    imgclass: "relativecls img2",
                    imgid: 'b1',
                    imgsrc: ""
                },
                {
                    imgdiv: "boy boy02",
                    imgclass: "relativecls img3",
                    imgid: 'b2',
                    imgsrc: ""
                },
                {
                    imgdiv: "boy boy03",
                    imgclass: "relativecls img4",
                    imgid: 'b3',
                    imgsrc: ""
                },
                {
                    imgdiv: "boy boy04",
                    imgclass: "relativecls img5",
                    imgid: 'b4',
                    imgsrc: ""
                },
                {
                    imgdiv: "roti",
                    imgclass: "relativecls img6",
                    imgid: 'rotiImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"lasttext fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1text20
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var setTime;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverpageImg", src: imgpath+"coverpage_new.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath+"thinkingbox.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img1Img", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img2Img", src: imgpath+"2.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img3Img", src: imgpath+"3.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img4Img", src: imgpath+"4.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img5Img", src: imgpath+"5.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img6Img", src: imgpath+"6.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img7Img", src: imgpath+"7.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img8Img", src: imgpath+"8.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img9Img", src: imgpath+"9.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img10Img", src: imgpath+"10.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img11Img", src: imgpath+"11.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img12Img", src: imgpath+"12a.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img13Img", src: imgpath+"12.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img14Img", src: imgpath+"13.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img15Img", src: imgpath+"14.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img16Img", src: imgpath+"15.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img17Img", src: imgpath+"16.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img18Img", src: imgpath+"17.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img19Img", src: imgpath+"18.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img20Img", src: imgpath+"19.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img21Img", src: imgpath+"20.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img22Img", src: imgpath+"21.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "img23Img", src: imgpath+"bg_final.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b1", src: imgpath+"boy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b2", src: imgpath+"boy02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b3", src: imgpath+"boy03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b4", src: imgpath+"boy04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rotiImg", src: imgpath+"rotianimation.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "roti1Img", src: imgpath+"rotianimation.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"S1_P1.ogg"},
            {id: "sound_2", src: soundAsset+"S1_P2.ogg"},
            {id: "sound_3", src: soundAsset+"S1_P3.ogg"},
            {id: "sound_4", src: soundAsset+"S1_P4.ogg"},
            {id: "sound_5", src: soundAsset+"S1_P5.ogg"},
            {id: "sound_6", src: soundAsset+"S1_P6.ogg"},
            {id: "sound_7", src: soundAsset+"S1_P7.ogg"},
            {id: "sound_8", src: soundAsset+"S1_P8.ogg"},
            {id: "sound_9", src: soundAsset+"S1_P9.ogg"},
            {id: "sound_10", src: soundAsset+"S1_P10.ogg"},
            {id: "sound_11", src: soundAsset+"S1_P11.ogg"},
            {id: "sound_12", src: soundAsset+"S1_P12.ogg"},
            {id: "sound_13", src: soundAsset+"S1_P13.ogg"},
            {id: "sound_14", src: soundAsset+"S1_P14.ogg"},
            {id: "sound_15", src: soundAsset+"S1_P15.ogg"},
            {id: "sound_16", src: soundAsset+"S1_P16.ogg"},
            {id: "sound_17", src: soundAsset+"S1_P17.ogg"},
            {id: "sound_18", src: soundAsset+"S1_P18.ogg"},
            {id: "sound_19", src: soundAsset+"S1_P19.ogg"},
            {id: "sound_20", src: soundAsset+"S1_P20.ogg"},
            {id: "sound_21", src: soundAsset+"S1_P21.ogg"},
            {id: "sound_22", src: soundAsset+"S1_P22.ogg"},
            {id: "sound_23", src: soundAsset+"S1_P23.ogg"},
            {id: "sound_24", src: soundAsset+"S1_P24.ogg"},
            {id: "sound_25", src: soundAsset+"S1_P25.ogg"},
            {id: "sound_26", src: soundAsset+"S1_P26.ogg"},
            {id: "sound_27", src: soundAsset+"S1_P27.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_1",true);
                vocabcontroller.findwords(countNext);
                break;
            case 5:
            case 15:
            case 20:
                setTime = setTimeout(function(){
                    sound_player("sound_"+(countNext+1),true);
                },2500);
                vocabcontroller.findwords(countNext);
                $('.coverboardfull div').not('div .boximg,div .thinkbox').css({"opacity":"0.5"});
                $(".line").css({"opacity":"0.1"});
                // navigationcontroller(countNext,$total_page);
                break;
            case 26:
                $(".boy02,.boy03,.boy04").hide();
                $(".boy01").delay(900).fadeOut(100);
                $(".boy02").delay(1000).fadeIn(1000);
                $(".boy02").delay(700).fadeOut(100);
                $(".boy03").delay(2500).fadeIn(1000);
                $(".boy03").delay(700).fadeOut(100);
                $(".boy04").delay(4000).fadeIn(1000);
                clearTimeout(setTime);
                setTime = setTimeout(function(){
                    sound_player("sound_27",true);
                },2000);
                setTime = setTimeout(function(){
                    $(".roti img").attr("src",preload.getResult("roti1Img").src);
                },5000);
                break;
            default:
                clearTimeout(setTime);
                setTime = setTimeout(function(){
                    sound_player("sound_"+(countNext+1),true);
                },2500);
                countNext==1?$(".line").hide().delay(2000).fadeIn(1000):"";
                $(".hidetext").delay(2000).animate({"opacity":"0"},100);
                $(".openimage").delay(1700).fadeOut(500);
                $(".topdiv").delay(2000).animate({"background-color":"white"},100);
                appeartextonebyone($(".onebyone"));
                break;
        }
    }


    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function appeartextonebyone(className) {
        var delaytime = 1000;
        setTime =setTimeout(function(){
            vocabcontroller.findwords(countNext);
        },(className.length+2)*1700);
        for (var i = 0; i < className.length; i++) {
            delaytime = delaytime+1700;
            className.eq(i).delay(delaytime).animate({"opacity":"1"},1000);
        }
    }
});

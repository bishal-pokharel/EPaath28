var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"diy",
                textclass:"chapter centertext",
                textdata:data.string.diy
            },
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'boyImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"diy1",
                textclass:"diyfont centertext",
                textdata:data.string.diy1
            },
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        videoblock:[{
            videoid: "tutorial",
            videoclass: "video_player",
        }],
        // table:[
        //     {
        //         firstrowdata:[
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text1
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text2
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text3
        //             }
        //         ],
        //         secondrowdata:[
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text4
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text5
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text6
        //             }
        //         ],
        //         thirdrowdata:[
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text7
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text8
        //             },
        //             {
        //                 textclass:"content",
        //                 textdata:data.string.p2text9
        //             }
        //         ]
        //     }
        // ],
        textblock: [
            {   textdiv:"chtitle",
                textclass: "content centertext",
                textdata: data.string.diytitle
            },
            // {   textdiv:"ansdiv",
            //     textclass: "content centertext",
            //
            // },
            // {   textdiv:"delete commonbtn",
            //     textclass: "content centertext",
            //     textdata: data.string.delete
            // },
            // {   textdiv:"ok commonbtn",
            //     textclass: "content centertext",
            //     textdata: data.string.ok
            // },
         ]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        table:[
            {
                secondrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text1
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text2
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text3
                    }
                ],
                firstrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text4
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text5
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text6
                    }
                ],
                thirdrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text7
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text8
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text9
                    }
                ]
            }
        ],
        textblock: [
            {   textdiv:"chtitle",
                textclass: "content centertext",
                textdata: data.string.diytitle1
            },
            {   textdiv:"ansdiv",
                textclass: "content centertext",

            },
            {   textdiv:"delete commonbtn",
                textclass: "content centertext",
                textdata: data.string.delete
            },
            {   textdiv:"ok commonbtn",
                textclass: "content centertext",
                textdata: data.string.ok
            },
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        table:[
            {
                thirdrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text1
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text2
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text3
                    }
                ],
                secondrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text4
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text5
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text6
                    }
                ],
                firstrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text7
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text8
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text9
                    }
                ]
            }
        ],
        textblock: [
            {   textdiv:"chtitle",
                textclass: "content centertext",
                textdata: data.string.diytitle1
            },
            {   textdiv:"ansdiv",
                textclass: "content centertext",

            },
            {   textdiv:"delete commonbtn",
                textclass: "content centertext",
                textdata: data.string.delete
            },
            {   textdiv:"ok commonbtn",
                textclass: "content centertext",
                textdata: data.string.ok
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        table:[
            {
                firstrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text1
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text2
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text3
                    }
                ],
                secondrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text4
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text5
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text6
                    }
                ],
                thirdrowdata:[
                    {
                        textclass:"content",
                        textdata:data.string.p2text7
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text8
                    },
                    {
                        textclass:"content",
                        textdata:data.string.p2text9
                    }
                ]
            }
        ],
        textblock: [
            {   textdiv:"chtitle",
                textclass: "content centertext",
                textdata: data.string.diytitle1
            },
            {   textdiv:"ansdiv",
                textclass: "content centertext",

            },
            {   textdiv:"delete commonbtn",
                textclass: "content centertext",
                textdata: data.string.delete
            },
            {   textdiv:"ok commonbtn",
                textclass: "content centertext",
                textdata: data.string.ok
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var video_object;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "coverpageImg", src: imgpath+"DIY.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "boyImg", src: imgpath+"boy_eating_roti.jpg", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"S2_P1.ogg"},
            {id: "sound_1", src: soundAsset+"S2_P2.ogg"},
            {id: "sound_2", src: soundAsset+"S2_P3.ogg"},
            // {id: "tutorial", src: imgpath+"test.webm", type: createjs.AbstractLoader.VIDEO},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
        // if(event.item.id === "tutorial"){
        //     video_object = event.item;
        //     console.log("handleFile "+video_object)
        // }
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        playvideo();
        switch(countNext){
            case 2:
                // $(".tdbox,.commonbtn").addClass("avoid-clicks");
                // $(".ansdiv").append("<span class='paddingspan content'></span> <span class='paddingspan content'></span> <span class='paddingspan content'></span>");
                // correctansdemo();
                // setTimeout(function(){
                //     wrongansdemo();
                // },8000)
                // playvideo();
                navigationcontroller(countNext,$total_page);
                sound_player("sound_"+countNext,true);
                break;
            case 3:
            case 4:
            case 5:
                count = 0;
                $(".tdbox").click(function(){
                    count++;
                    $(this).addClass("avoid-clicks");
                    $(this).find("p").css("background-color","#ffd966");
                    var textval = $(this).text();
                    $(".ansdiv p").append("<span class='paddingspan'>" + textval + "</span>");
                    $(".delete").click(function(){
                        count=0;
                        resetchoices();
                    });
                    if(count >2){
                        $("table td").addClass("avoid-clicks");
                        count = 0;
                        checkans();
                    }
                });
                break;
            default:
                sound_player("sound_"+countNext,true);
                // navigationcontroller(countNext,$total_page);
                break;
        }
    }


    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function checkans(){
        $(".ok").click(function(){
                var answer = $(".ansdiv p span").eq(0).text().toString().replace(/\ /g,'').trim();
                answer += $(".ansdiv p span").eq(1).text().toString().replace(/\ /g,'').trim();
                answer += $(".ansdiv p span").eq(2).text().toString().replace(/\ /g,'').trim();
                var conditionArray = [data.string.c1,data.string.c2,data.string.c3,data.string.c4,data.string.c5,
                    data.string.c6,data.string.c7,data.string.c8,data.string.c9,data.string.c10,data.string.c11,data.string.c12];
                if (conditionArray.indexOf(answer)>-1) {
                    play_correct_incorrect_sound(1);
                    navigationcontroller(countNext,$total_page);
                    $(".ansdiv").addClass("correctans");
                }
                else {
                    play_correct_incorrect_sound(0);
                    $(".ansdiv").addClass("wrongans");
                }
        });

    }
    function resetchoices(){
        $(".tdbox").removeClass("avoid-clicks");
        $("table td").removeClass("avoid-clicks");
        $(".ansdiv").removeClass("wrongans").removeClass("correctans");
        $(".ansdiv span").remove();
        $(".tdbox").find("p").css("background-color","white");
        $(".ok").css({"width":"18%","height":"15%"});

    }

    // function correctansdemo(){
    //     $(".tdbox p:first").delay(1000).animate({"background-color":"#ffd966"},500);
    //     var textval = $(".tdbox:first").text();
    //     setTimeout(function(){
    //         $(".paddingspan").eq(0).text( textval);
    //     },1500);
    //     $(".tdbox p").eq(7).delay(2000).animate({"background-color":"#ffd966"},500);
    //     setTimeout(function(){
    //         textval = $(".tdbox").eq(7).text();
    //         $(".paddingspan").eq(1).text( textval);
    //     },2500);
    //     $(".tdbox p").eq(2).delay(3000).animate({"background-color":"#ffd966"},500);
    //     setTimeout(function(){
    //         textval = $(".tdbox").eq(2).text();
    //         $(" .paddingspan").eq(2).text( textval);
    //     },3500);
    //     $(".ok").delay(4000).animate({"width":"20%","height":"16%"});
    //     setTimeout(function(){
    //         $(".ansdiv").addClass("correctans");
    //     },5000);
    //     setTimeout(function(){
    //         resetchoices()
    //     },5500);
    // }
    // function wrongansdemo(){
    //     $(".tdbox:first p").delay(1000).animate({"background-color":"#ffd966"},500);
    //     var textval = $(".tdbox:first").text();
    //     setTimeout(function(){
    //         $(".paddingspan").eq(0).text( textval);
    //     },1500);
    //     $(".tdbox p").eq(7).delay(2000).animate({"background-color":"#ffd966"},500);
    //     setTimeout(function(){
    //         textval = $(".tdbox").eq(7).text();
    //         $(".paddingspan").eq(1).text( textval);
    //     },2500);
    //     $(".tdbox p").eq(8).delay(3000).animate({"background-color":"#ffd966"},500);
    //     setTimeout(function(){
    //         textval = $(".tdbox").eq(8).text();
    //         $(" .paddingspan").eq(2).text( textval);
    //     },3500);
    //
    //     $(".delete").delay(4000).animate({"width":"20%","height":"16%"});
    //     setTimeout(function(){
    //         $(".tdbox p").eq(8).animate({"background-color":"white"},500);
    //         $(" .paddingspan").eq(2).text("");
    //     },5000);
    //     $(".tdbox p").eq(5).delay(6000).animate({"background-color":"#ffd966"},500);
    //     setTimeout(function(){
    //         textval = $(".tdbox").eq(5).text();
    //         $(" .paddingspan").eq(2).text( textval);
    //         $(".delete").css({"width":"18%","height":"15%"});
    //     },7000);
    //     $(".ok").delay(8000).animate({"width":"20%","height":"16%"});
    //     setTimeout(function(){
    //         $(".ansdiv").addClass("correctans");
    //         navigationcontroller(countNext,$total_page);
    //     },9000);
    // }
    function playvideo(){
        if(content[countNext].hasOwnProperty('videoblock')){
            var videoblock = content[countNext].videoblock;
            var id = videoblock[0].videoid;
            if(video_object != null){
                $("#"+id+" > source ").attr('src', video_object.src);
            } else {
                $("#"+id).html(' <source src="activity/grade2/nepali/aghiaghiroti/images/roti.webm" type="video/ogg">Your browser does not support the video tag.');
            }
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var content=[
    //slide 0
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q1opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q1opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q1opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q1opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q1opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q1opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q1ans
            }
        ]
    },
    //slide 1
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q2opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q2opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q2opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q2opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q2opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q2opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q2ans
            }
        ]
    },
    //slide 2
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q3opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q3opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q3opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q3opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q3opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q3opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q3ans
            }
        ]
    },
    //slide 3
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q4opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q4opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q4opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q4opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q4opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q4opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q4ans
            }
        ]
    },
    //slide4
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q5opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q5opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q5opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q5opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q5opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q5opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q5ans
            }
        ]
    },
//    slide 5
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q6opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q6opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q6opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q6opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q6opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q6opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q6ans
            }
        ]
    },
    //slide 6
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q7opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q7opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q7opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q7opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q7opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q7opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q7ans
            }
        ]
    },
//    slide 7
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q8opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q8opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q8opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q8opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q8opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q8opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q8ans
            }
        ]
    },
//    slide 8
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q9opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q9opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q9opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q9opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q9opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q9opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q9ans
            }
        ]
    },
//    slide 9
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        quesblock:[
            {
                textdiv:"question",
                textclass:"content centertext",
                textdata:data.string.question
            },
        ],
        textblock:[

            {
                textdiv:"droppable ans1",
                textclass:"subtopic1 centertext",
                ans:data.string.q10opt1
            },
            {
                textdiv:"droppable ans2",
                textclass:"subtopic1 centertext",
                ans:data.string.q10opt2
            },
            {
                textdiv:"droppable ans3",
                textclass:"subtopic1 centertext",
                ans:data.string.q10opt3
            },
            {
                textdiv:"draggable opt1",
                textclass:"subtopic1 centertext",
                textdata:data.string.q10opt1
            },
            {
                textdiv:"draggable opt2",
                textclass:"subtopic1 centertext",
                textdata:data.string.q10opt2
            },
            {
                textdiv:"draggable opt3",
                textclass:"subtopic1 centertext",
                textdata:data.string.q10opt3
            },
            {
                textdiv:"ok commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.ok
            },
            {
                textdiv:"delete commonbtn",
                textclass:"subtopic1 centertext",
                textdata:data.string.delete
            },
            {
                textdiv:"answer",
                textclass:"title centertext",
                textdata:data.string.q10ans
            }
        ]
    },
];
// content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;
    var scoreval=0;

    var $total_page = content.length;

    var preload;
    var timeoutvar = null;
    var current_sound;
    var egg = new EggTemplate();
    egg.init($total_page);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset+"instruction.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        $(".answer").hide();
        var optionclass = ["draggable opt1","draggable opt2","draggable opt3"];
        shufflehint(optionclass,"draggable");
        dragdrop();
        egg.numberOfQuestions($total_page);
        scoreval=0;
        countNext==0?sound_player("sound_1"):"";
    }
    function put_image(content, count,preload) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload)
    }

    function dynamicimageload(imageblockcontent,contentCount,preload){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
        }

    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                egg.gotoNext();
                templateCaller();
                break;
        }
    });
    //
    // $refreshBtn.click(function(){
    //     templateCaller();
    // });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(optionclass,option) {
        var optdiv = $(".contentblock");
        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                // $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.text();
                $(this).find("p").text(draggableans);
                ui.draggable.hide(0);
                $(".ok").click(function(){
                    $(".droppable,.draggable").addClass("avoid-clicks");
                    count = 0;
                    if($(".ans1").text().toString().trim()==$(".ans1").attr("data-answer").toString().trim()){
                       $(".ans1").addClass("correctans");
                        $(".ans1").append("<img class='correctwrongimg' src='images/right.png'/>");
                        count++;
                        scoreval++;
                    }
                    else{
                        count =0;
                        scoreval =0;
                        $(".ans1").addClass("wrongans");
                        $(".ans1").append("<img class='correctwrongimg' src='images/wrong.png'/>");
                    }
                    if($(".ans2").text().toString().trim()==$(".ans2").attr("data-answer").toString().trim()){
                        $(".ans2").addClass("correctans");
                        count++;
                        scoreval++;
                        $(".ans2").append("<img class='correctwrongimg' src='images/right.png'/>");
                    }
                    else{
                        $(".ans2").addClass("wrongans");
                        count = 0;
                        scoreval =0;
                        $(".ans2").append("<img class='correctwrongimg' src='images/wrong.png'/>");
                    }
                    if($(".ans3").text().toString().trim()==$(".ans3").attr("data-answer").toString().trim()) {
                        $(".ans3").addClass("correctans");
                        count++;
                        scoreval++;
                        $(".ans3").append("<img class='correctwrongimg' src='images/right.png'/>");
                    }
                    else{
                        $(".ans3").addClass("wrongans");
                        count = 0;
                        scoreval =0;
                        $(".ans3").append("<img class='correctwrongimg' src='images/wrong.png'/>");
                    }
                    scoreval == 3?egg.update(true):egg.update(false);
                    if(count == 3){
                        play_correct_incorrect_sound(1);
                        setTimeout(function(){
                            navigationcontroller(countNext,$total_page,true);
                            $(".ok,.delete,.droppable").hide();
                            $(".answer").fadeIn(1000);
                        },1000);
                    }
                    else
                        play_correct_incorrect_sound(0);
                });
                $(".delete").click(function(){
                    ui.draggable.show();
                    $(".droppable").removeClass("correctans").removeClass("wrongans").find("p").text("");
                    count = 0;
                    $(".droppable img").remove();
                    $(".droppable,.draggable").removeClass("avoid-clicks");

                })
            }
        });
    }

});

var imgpath = $ref + "/images/playtime/";
var imgpath2 = $ref + "/images/playtime/forhover/";
var soundAsset = $ref+"/audio_en/";

var content=[
	// slide0
	{
			contentnocenteradjust: true,
			uppertextblock: [
					{
							textclass: "playtime",
							textdata: data.string.playtime
					}
			],
			imageblock:[{
					imagestoshow: [
							{
									imgclass: "rhinoimgdiv",
									imgid: "rhinodance",
									imgsrc: "",
							},
							{
									imgclass: "squirrelisteningimg",
									imgid: "squirrel",
									imgsrc: "",
							},
							{
									imgclass: "bg_full",
									imgid: "mainbg",
									imgsrc: "",
							}]
			}]
	},
	// slide1
	{
			contentnocenteradjust: true,
			contentblockadditionalclass:'purple',
			uppertextblock: [
					{
							textclass: "top_text",
							textdata: data.string.clicktext
					}
			],
			hoverdiv:[{
					divname:'scene1',
					bgimgclass:'png1',
					singleimageclass:'png1-1',
			},{
					divname:'scene2',
					bgimgclass:'png2',
					singleimageclass:'png2-2',
			},{
					divname:'scene3',
					bgimgclass:'png3',
					singleimageclass:'png3-3',
			},{
					divname:'scene4',
					bgimgclass:'png4',
					singleimageclass:'png4-4',
					imgclass:'desk',
					thirdimage:[{

					}]
			}]


	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//  
			//images
			{id: "rhinodance", src: imgpath+"rhino_dancing.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src:imgpath +"squirrel-listening.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "mainbg", src: imgpath+"bg_play-time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gif1", src: imgpath2+"mina.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "png1-1", src: imgpath2+"mina.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png1", src: imgpath+"p04-2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png2", src: imgpath+"p05-4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png2-2", src: imgpath2+"pressurecooker.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gif2", src: imgpath2+"pressurecooker.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "png3", src: imgpath+"p6-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png3-3", src: imgpath2+"football.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gif3", src: imgpath2+"football.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "png4", src: imgpath2+"class_room_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png4-a", src: imgpath2+"desk.png", type: createjs.AbstractLoader.IMAGE},
			{id: "png4-4", src: imgpath2+"pageturn.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gif4", src: imgpath2+"pageturn.gif", type: createjs.AbstractLoader.IMAGE},
			// {id: "football", src: imgpath+"football.gif", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"flute.ogg"},
			{id: "sound_2", src: soundAsset+"cooker_whistle.ogg"},
			{id: "sound_3", src: soundAsset+"page_flip.mp3"},
			{id: "sound_4", src: soundAsset+"page_flip.ogg"},
			{id: "a1", src: soundAsset+"pen.mp3"},
			{id: "a2", src: soundAsset+"bun.mp3"},
			{id: "a3", src: soundAsset+"cat.mp3"},
			{id: "a4", src: soundAsset+"pot.mp3"},
			{id: "a5", src: soundAsset+"pin.mp3"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
firstPagePlayTime(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$('.png1').attr('src',preload.getResult('png1').src);
		$('.png1-1').attr('src',preload.getResult('png1-1').src);
		$('.png2').attr('src',preload.getResult('png2').src);
		$('.png2-2').attr('src',preload.getResult('png2-2').src);
		$('.png3').attr('src',preload.getResult('png3').src);
		$('.png3-3').attr('src',preload.getResult('png3-3').src);
		$('.png4').attr('src',preload.getResult('png4').src);
		$('.png4-4').attr('src',preload.getResult('png4-4').src);
		$('.desk').attr('src',preload.getResult('png4-a').src);
		$('.scene1').click(function(){
				$('.linkClick:nth-child(2)').trigger('click');
		});
		$('.scene2').click(function(){
				$('.linkClick:nth-child(3)').trigger('click');
		});
		$('.scene3').click(function(){
				$('.linkClick:nth-child(4)').trigger('click');
		});
		$('.scene4').click(function(){
				$('.linkClick:nth-child(5)').trigger('click');
		});
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(2)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(3)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(4)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(5)').css('display','none');
		$('#activity-page-list-page-container>.exerciseTab2>span:nth-child(6)').html('1').css({
			"border": "3px solid #F99774",
		"background": "#F99774",
		"pointer-events":"none",
		'color':'white'
		});

		hovereffect('.scene1','.png1-1','gif1','png1-1','sound_1');
		hovereffect('.scene2','.png2-2','gif2','png2-2','sound_2');
		hovereffect('.scene3','.png3-3','gif3','png3-3','sound_3');
		hovereffect('.scene4','.png4-4','gif4','png4-4','sound_4');
		function hovereffect(scene_class,png_class,gif_id,png_id,sound_id){
			$(scene_class).mouseenter(function(){
				$(png_class).attr('src',preload.getResult(gif_id).src);
				sound_player(sound_id);
			});
			$(scene_class).mouseleave(function(){
				$(png_class).attr('src',preload.getResult(png_id).src);
				createjs.Sound.stop();
			});
		}

		$('.arrowLine').hide(0);
		switch (countNext) {
			case 0:
				nav_button_controls(100);
				break;
			default:

		}

	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
			if(content[count].hasOwnProperty('imageblock')){
				for(var j = 0; j < content[count].imageblock.length; j++){
					var imageblock = content[count].imageblock[j];
					if(imageblock.hasOwnProperty('imagestoshow')){
						var imageClass = imageblock.imagestoshow;
						for(var i=0; i<imageClass.length; i++){
							var image_src = preload.getResult(imageClass[i].imgid).src;
							//get list of classes
							var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]);
							$(selector).attr('src', image_src);
						}
					}
				}
			}
		}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
        if(window.location.href.split('=').pop() == 'page2') {
                countNext = 1;
        }
        $prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

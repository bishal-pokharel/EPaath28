var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide 1
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"asha04",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"q1",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"q1op1",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"q1op2",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn wtrShow",
				imgid:"watering_plant_gif",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"q1op3",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q1
		}]
	},
	// slide 2
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg",
				imgid:"grandma_talking",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"flower_pot",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"door",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"table",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"table01",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"window",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q2
		}]
	},
	// slide 3
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"rumi",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"cat_food",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"dog",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"cat",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"cat1",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"rabbit",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q3
		}]
	},
	// slide 4
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"niti",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"sponge",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"chair",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"table03",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"table04",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow spngImg",
				imgid:"sponge",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"rack",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q4
		}]
	},
	// slide 5
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"niti",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"knife",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"pinaapple",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"apple",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"cut_apple",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"orange",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q5
		}]
	},
	// slide 6
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"suraj",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"chappel",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"bed",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"rack",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"chappel_on_rack",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"table01",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q6
		}]
	},
	// slide 7
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"suraj",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"book",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"socks",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"bag02",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"bag01",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"shoes",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q7
		}]
	},
	// slide 8
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"sagar",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"rumal",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"rumal03",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"rumal02",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"rumal01",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"rumal04",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q8
		}]
	},
	// slide 9
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"sagar",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"galbandi",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"bag02",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"galbandi01",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"galbandi02",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"ball01",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q9
		}]
	},
	// slide 10
	{
		contentblockadditionalclass : "blueBg",
		imgload:true,
		imgexc:true,
		qntext:[{
			qntextdivclass:"topInstrn secInstrn",
			qntextclass:"qn_top",
			qndata:data.string.etext1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:"spkrimg-wdth20",
				imgid:"upaya",
				imgsrc:'',
			},{
				imgclass:"draggable drgblImg",
				imgid:"food",
				imgsrc:'',
			}]
		}],
		imgexc:[{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim1",
				imgid:"cup",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable correct",
			imgexcoptions:[{
				imgclass:"boximg bim2",
				imgid:"plate_empty",
				imgsrc:'',
			},{
				imgclass:"boximg-hidn tblshow",
				imgid:"plate",
				imgsrc:'',
			}]
		},{
			imgexcontainerclass:"droppable",
			imgexcoptions:[{
				imgclass:"boximg bim3",
				imgid:"earthern_pot",
				imgsrc:'',
			}]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgclass:'',
			imgid:"spchbx",
			imgsrc:'',
			textclass:"textInSp bgWHt",
			textdata:data.string.exc_q10
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "q1", src: imgpath+"water_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op1", src: imgpath+"jug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op2", src: imgpath+"flower_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q1op3", src: imgpath+"bucket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spchbx", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "watering_plant_gif", src: imgpath+"watering_plant.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "asha04", src: imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grandma_talking", src: imgpath+"grandma-talking.png", type: createjs.AbstractLoader.IMAGE},

			{id: "door", src: imgpath+"door.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table", src: imgpath+"table.png", type: createjs.AbstractLoader.IMAGE},
			{id: "window", src: imgpath+"window.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower_pot", src: imgpath+"flower_vase.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table01", src: imgpath+"table01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat_food", src: imgpath+"cat_food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabbit", src: imgpath+"rabbit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"cat01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat1", src: imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},

			{id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rack", src: imgpath+"rack.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table03", src: imgpath+"table03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "table04", src: imgpath+"table04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chair", src: imgpath+"chair.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sponge", src: imgpath+"sponge.png", type: createjs.AbstractLoader.IMAGE},

			{id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"apple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pinaapple", src: imgpath+"pinaapple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "knife", src: imgpath+"knife.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cut_apple", src: imgpath+"cut_apple.png", type: createjs.AbstractLoader.IMAGE},

			{id: "suraj", src: imgpath+"suraj.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chappel", src: imgpath+"chappel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bed", src: imgpath+"bed.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chappel_on_rack", src: imgpath+"chappel_on_rack.png", type: createjs.AbstractLoader.IMAGE},

			{id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "socks", src: imgpath+"socks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoes", src: imgpath+"shoes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag02", src: imgpath+"bag02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag01", src: imgpath+"bag01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumal", src: imgpath+"rumal_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumal02", src: imgpath+"rumal_new01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumal01", src: imgpath+"rumal_new02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumal03", src: imgpath+"rumal_new03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumal04", src: imgpath+"rumal_new04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galbandi", src: imgpath+"rumal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galbandi02", src: imgpath+"rumal01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galbandi01", src: imgpath+"rumal02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galbandi03", src: imgpath+"rumal03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "galbandi04", src: imgpath+"rumal04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ball01", src: imgpath+"ball01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "upaya", src: imgpath+"upaya.png", type: createjs.AbstractLoader.IMAGE},
			{id: "food", src: imgpath+"food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plate", src: imgpath+"plate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plate_empty", src: imgpath+"plate_empty.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earthern_pot", src: imgpath+"earthern_pot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cup", src: imgpath+"cup.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag01", src: imgpath+"bag01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "exer", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var corcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	testin.numberOfQuestions();

	 	var ansClicked = false;
	 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		// $(".qn_top").prepend(countNext+1+". ");

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		//for sounds
		if(countNext==0){
			sound_nav("exer");
		}
		function dropFunction(event, ui, droppable, drgagable){
			if($(droppable).hasClass("correct")){
				play_correct_incorrect_sound(1);
				ui.draggable.detach();
				// $(droppable).children(".corctopt").show(0);
				$(".droppable").css({"pointer-events":"none"});
				$(droppable).css({"pointer-events":"none","background-color":"#93c47d"});
				!wrngClicked?testin.update(true):'';
				$nextBtn.show(0);
				switch (countNext) {
					case 0:

						$(".wtrShow").show(0);
						$(".bim2").hide(0);
					break;
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
					case 7:
					case 8:
					case 9:
						$(".tblshow").show(0);
						$(".bim2").hide(0);
					break;
					default:
				}
			}else{
				play_correct_incorrect_sound(0);
				// $(droppable).children(".wrngopt").show(0);
				$(droppable).css({"pointer-events":"none","background-color":"#f00"});
				wrngClicked=true;
			}
		}

		// to control the exercise drag and drop
		 	$(".draggable").draggable({
				revert:true,
				start: function(event,ui){
					$(".draggable").css("cursor","all-scroll");
				},
				stop: function(event,ui){
					$(".draggable").css("cursor","pointer");
				}
			});
			$(".droppable").droppable({
				hoverClass:"dropHover",
				accept:".draggable",
				drop:  function(event,ui){
					dropFunction(event, ui, $(this),".draggable");
				}
			});
			randomize(".imgexcmaincontainer ");

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				// ole.footerNotificationHandler.pageEndSetNotification();
				$nextBtn.show(0);
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			for(var j=0;j<content[count].imgexc.length;j++){
				var imageblock = content[count].imgexc[j];
				if(imageblock.hasOwnProperty('imgexcoptions')){
					var imageClass = imageblock.imgexcoptions;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

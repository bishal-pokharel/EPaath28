var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/";
var imgpath1 = $ref+"/images/";


var content=[
		// slide0
		{
			// contentblockadditionalclass: 'purplebg',
			uppertextblock:[{
				textclass:'chaptertitle',
				textdata:data.lesson.chapter
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg3'
				}]
			}]
		},
		// slide1
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg1'
				}]
			}],
				speechbox:[{
					txtblock:[{
						textdata : data.string.p1text1,
						textclass : 'text_inside',
					}],
					speechbox: 'sp-7',
					imgclass: 'flipped-h',
					imgid : 'bubble',
					imgsrc: '',
					// audioicon: true,
				}],
		},
    //slide 2
    {
        // contentblockadditionalclass: 'purplebg',
        imageblock:[{
            imagestoshow:[{
                imgclass:'bg_full',
                imgid:'bg2'
            }]
        }],
			  speechbox:[{
						txtblock:[{
							textdata : data.string.p1text2,
							textclass : 'text_inside',
						}],
            speechbox: 'sp-6',
            imgclass: 'flipped-h',
            imgid : 'bubble1',
            imgsrc: '',
            // audioicon: true,
        }],
    },
		// slide3
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg1'
				}]
			}],
				speechbox:[{
					txtblock:[{
						textdata : data.string.p1text4,
						textclass : 'text_inside',
					}],
					speechbox: 'sp-2',
					imgclass: 'flipped-h',
					imgid : 'bubble',
					imgsrc: '',
					// audioicon: true,
				}],
		},
		// slide4
		{
        // contentblockadditionalclass: 'purplebg',
        imageblock:[{
            imagestoshow:[{
                imgclass:'bg_full',
                imgid:'bg2'
            }]
        }],
			  speechbox:[{
						txtblock:[{
							textdata : data.string.p1text5,
							textclass : 'text_inside',
						}],
            speechbox: 'sp-1',
            imgclass: 'flipped-h',
            imgid : 'bubble1',
            imgsrc: '',
            // audioicon: true,
        }],
    },
		// slide5
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg1'
				}]
			}],
				speechbox:[{
					txtblock:[{
						textdata : data.string.p1text7,
						textclass : 'text_inside',
					}],
					speechbox: 'sp-2',
					imgclass: 'flipped-h',
					imgid : 'bubble',
					imgsrc: '',
					// audioicon: true,
				}],
		},

		// slide6
		{
        // contentblockadditionalclass: 'purplebg',
        imageblock:[{
            imagestoshow:[{
                imgclass:'bg_full',
                imgid:'bg4'
            }]
        }],
			  speechbox:[{
						txtblock:[{
							textdata : data.string.p1text8,
							textclass : 'text_inside',
						}],
            speechbox: 'sp-1',
            imgclass: 'flipped-h',
            imgid : 'bubble1',
            imgsrc: '',
            // audioicon: true,
        }],
    },

		// slide7
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg5'
				}]
			}],
		},

		//slide8
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'balloon_moving'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text9,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		//slide9
		{
					// contentblockadditionalclass: 'purplebg',
					imageblock:[{
							imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg7'
							},
						]
					}],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text10,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-4',
							imgclass: 'flipped-h',
							imgid : 'bubble2',
							imgsrc: '',
							// audioicon: true,
					}],
			},

		//slide10
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'balloon_moving'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text11,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		// slide11
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
					imgclass:'bg_full',
					imgid:'bg7'
				}]
			}],
		},

		//slide12
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'balloon_moving'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text12,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		// slide13
		{
			// contentblockadditionalclass: 'purplebg',
			imageblock:[{
				imagestoshow:[{
						imgclass:'bg_full',
						imgid:'bg6'
				},
				{
						imgclass:'balloon_moving',
						imgid:'clapping'
				}]
			}],
		},

		//slide14
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'balloon_moving'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text13,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		//slide15
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg9'
							},
							{
	                imgclass:'reading',
	                imgid:'handsup'
	            },
						]
	        }],
	    },

		//slide16
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'clapping'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text14,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		//slide17
		{
					// contentblockadditionalclass: 'purplebg',
					imageblock:[{
							imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg9'
							},
							{
									imgclass:'reading',
									imgid:'handsdown'
							},
						]
					}],
			},

		//slide18
		{
					// contentblockadditionalclass: 'purplebg',
					imageblock:[{
							imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg6'
							},
							{
									imgclass:'balloon_moving',
									imgid:'balloon_moving'
							},
						]
					}],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text15,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
			},

		//slide19
		{
					// contentblockadditionalclass: 'purplebg',
					imageblock:[{
							imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg9'
							},
							{
									imgclass:'reading',
									imgid:'reading'
							},
						]
					}],
			},

		//slide20
		{
					// contentblockadditionalclass: 'purplebg',
					imageblock:[{
							imagestoshow:[{
									imgclass:'bg_full',
									imgid:'bg6'
							},
							{
									imgclass:'balloon_moving',
									imgid:'balloon_moving'
							},
						]
					}],
			},

		//slide21
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg6'
	            },
							{
	                imgclass:'balloon_moving',
	                imgid:'balloon_moving'
	            },
						]
	        }],
					speechbox:[{
						 txtblock:[{
							 textdata : data.string.p1text16,
							 textclass : 'text_inside',
						 }],
							speechbox: 'sp-3',
							imgclass: 'flipped-h',
							imgid : 'bubble1',
							imgsrc: '',
							// audioicon: true,
					}],
	    },

		//slide22
	  {
	        // contentblockadditionalclass: 'purplebg',
	        imageblock:[{
	            imagestoshow:[{
	                imgclass:'bg_full',
	                imgid:'bg10'
	            }

						]
	        }],

	    },

			//slide23
		  {
						// contentblockadditionalclass: 'purplebg',
					 imageblock:[{
							 imagestoshow:[{
									 imgclass:'bg_full',
									 imgid:'bg6'
							 },
							 {
 	                imgclass:'balloon_moving',
 	                imgid:'balloon_moving1'
 	            	},
						 ]
					 }],
					 speechbox:[{
							txtblock:[{
								textdata : data.string.p1text18,
								textclass : 'text_inside',
							}],
							 speechbox: 'sp-3',
							 imgclass: 'flipped-h',
							 imgid : 'bubble1',
							 imgsrc: '',
							 // audioicon: true,
					 }],
		    },

			//slide24
			{
						// contentblockadditionalclass: 'purplebg',
					 imageblock:[{
							 imagestoshow:[{
									 imgclass:'bg_full',
									 imgid:'bg6'
							 },
							 {
									imgclass:'balloon_moving',
									imgid:'givingballoons'
								},
						 ]
					 }],
					 speechbox:[{
							txtblock:[{
								textdata : data.string.p1text19,
								textclass : 'text_inside',
							}],
							 speechbox: 'sp-3',
							 imgclass: 'flipped-h',
							 imgid : 'bubble1',
							 imgsrc: '',
							 // audioicon: true,
					 }],
				},

			//slide25
			{
						// contentblockadditionalclass: 'purplebg',
					 imageblock:[{
							 imagestoshow:[{
									 imgclass:'bg_full',
									 imgid:'bg12'
							 }
						 ]
					 }]
			}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [

			//images

			// {id: "p01", src: imgpath+"p01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p02", src: imgpath+"img01.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p03", src: imgpath+"p03.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "p04", src: imgpath+"img01a.png", type: createjs.AbstractLoader.IMAGE},

			{id:'clapping', src: imgpath+"clapping_hand02.png",type: createjs.AbstractLoader.IMAGE},
			{id:'givingballoons', src: imgpath+"giving_balloons.gif",type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg_balloon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon_moving", src: imgpath+"balloon_moving.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "balloon_moving1", src: imgpath+"balloon_moving.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bg8", src: imgpath+"bg09.png", type: createjs.AbstractLoader.IMAGE},
		  {id: "bg9", src: imgpath+"bg06_09_empty.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg10", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg11", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg12", src: imgpath+"kids_outside_with_balloons.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble1", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble2", src: imgpath+"text_box07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "reading", src: imgpath+"reading.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "handsdown", src: imgpath+"hands_down.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "handsup", src: imgpath+"hands_up.gif", type: createjs.AbstractLoader.IMAGE},

			//soundAsset

			// {id: "clappingSound", src: soundAsset+"clapping.ogg"},
			// {id: "bellringing", src: soundAsset+"school bell ring effect.ogg"},
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p15.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p17.ogg"},
			{id: "sound_18", src: soundAsset+"s1_p19.ogg"},
			{id: "sound_20", src: soundAsset+"s1_p21.ogg"},
			{id: "sound_21", src: soundAsset+"s1_p22.ogg"},
			{id: "sound_23", src: soundAsset+"s1_p24.ogg"},
			{id: "sound_24", src: soundAsset+"s1_p25.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
    vocabcontroller.findwords(countNext);

		switch(countNext) {
				case 7:
				case 11:
				case 15:
				case 19:
				case 22:
				case 25:
				nav_button_controls(1000);
				break;

        case 15:
				case 17:
			 	$('.reading').delay(3500);
				nav_button_controls(100);
				break;
				break;
				default:
				sound_player("sound_"+countNext);
				break;

		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id, navigate) {
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function () {
					nav_button_controls(0);
			});
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

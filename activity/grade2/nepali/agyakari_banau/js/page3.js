var soundAsset = $ref+"/sounds/";
var imgpath = $ref+"/images/diy/";

var content=[

  //slide0

	{
		uppertextblockadditionalclass:'title',
		uppertextblock:[{
			textclass:'chaptertitle',
			textdata:data.string.p3text1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg0'
			}]
		}]
	},

	// slide1
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg1'
			}]
		}]
	},

	//slide2
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg3'
			},
			{
				imgclass:'bg_full',
				imgid:'bg3'
			},
			{
				imgclass:'notepad0',
				imgid:'notepad'
			},
			{
				imgclass:'telephone',
				imgid:'telephone_ringing'
			}
		]
		}]
	},

	//slide3
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg3'
			},
			{
				imgclass:'notepad0',
				imgid:'notepad'
			},
			{
				imgclass:'telephone',
				imgid:'talking1'
			}]
		}],
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text2,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-2',
			imgclass: 'flipped-h',
			imgid : 'textbox1',
			imgsrc: '',
		}],
	},

	//slide4
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg3'
			},
			{
				imgclass:'notepad0',
				imgid:'notepad'
			},
			{
				imgclass:'telephone',
				imgid:'talking1'
			}]
		}],
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text3,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-1',
			imgclass: 'speech1',
			imgid : 'textbox2',
			imgsrc: '',
		}],
	},

	//slide5
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg3'
			},
			{
				imgclass:'notepad0',
				imgid:'notepad'
			},
			{
				imgclass:'telephone',
				imgid:'talking1'
			}]
		}],
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text4,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-2',
			imgclass: 'flipped-h',
			imgid : 'textbox1',
			imgsrc: '',
		}],
	},

	//slide6
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg2'
			},
			{
				imgclass:'notepad',
				imgid:'notepad'
			},
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}
		],
        lowertextblockadditionalclass: 'clickmsg',
        lowertextblock:[{
            textclass:	'clickmsg1',
            textdata:	data.string.p3text19
        }]
	},

	//slide7
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg2'
			},
			{
				imgclass:'notepad',
				imgid:'notepad'
			},
			{
				imgclass:'rumi',
				imgid:'rumi'
			},
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}],

			speechbox:[{
				txtblock:[{
					textdata : data.string.p3text10,
					textclass : 'text_inside',
				}],
				speechbox: 'sp-3',
				imgclass: 'flipped-h',
				imgid : 'textbox3',
				imgsrc: '',
			}],

	},

	//slide8
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg4'
			},
			{
				imgclass:'rumi1',
				imgid:'rumiwithdog'
			},
			{
				imgclass:'bowl',
				imgid:'bowl'
			},
			{
				imgclass:'rumigif',
				imgid:'rumiwithdoggif'
			}]
		}],
		extratextblock:[{
			textclass: 'testdiv',
			textdata: ''
		}],
		uppertextblockadditionalclass: 'instructadditional',
		uppertextblock:[
			{
				textclass:'instruction',
				textdata: data.string.p3text11
			}]
	},

	//slide9
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5,
				imgclass:'tick1',
				imgid: 'tick'
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}],
	},

	//slide10
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg5'
			},{
				imgclass:'curtainl',
				imgid:'curtainl'
			},{
				imgclass:'curtainr',
				imgid:'curtainr'
			}]
		}],

		uppertextblockadditionalclass: 'instructadditional',
		uppertextblock:[
			{
				textclass:'instruction',
				textdata: data.string.p3text12
			}],

			lowertextblockadditionalclass: 'option',
			lowertextblock:[{
				textclass:	'choice',
				textdata:	data.string.p3text13
			}],

	},

	//slide11
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5,
				imgclass:'tick1',
				imgid: 'tick'
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6,
				imgclass:'tick2',
				imgid: 'tick'
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}],
	},

	//slide12
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg6'
			},{
				imgclass:'water_filter',
				imgid:'water_filter'
			},
			{
				imgclass:'water_filtergif',
				imgid:'water_filter_gif'
			}]
		}],
		extratextblock:[{
			textclass: 'highlight',
			textdata: ''
		}],
		uppertextblockadditionalclass: 'instructadditional',
		uppertextblock:[
			{
				textclass:'instruction',
				textdata: data.string.p3text14
			}],
	},

	//slide13
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5,
				imgclass:'tick1',
				imgid: 'tick'
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6,
				imgclass:'tick2',
				imgid: 'tick'
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7,
				imgclass:'tick3',
				imgid: 'tick'
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}],
	},

	//slide14
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg7'
			},
			{
				imgclass:'draggable book',
				imgid:'book'
			},
			{
				imgclass:'droppable bag',
				imgid:'bag'
			},
			{
				imgclass:'bag1',
				imgid:'bag1'
			}
		]
		}],
		uppertextblockadditionalclass: 'instructadditional',
		uppertextblock:[
			{
				textclass:'instruction',
				textdata: data.string.p3text15
			}],
	},

	//slide15
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5,
				imgclass:'tick1',
				imgid: 'tick'
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6,
				imgclass:'tick2',
				imgid: 'tick'
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7,
				imgclass:'tick3',
				imgid: 'tick'
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8,
				imgclass:'tick4',
				imgid: 'tick'
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9
			}],
	},

	//slide16
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg8'
			},
			{
				imgclass:'bulb1',
				imgid:'bulb1'
			},
			{
				imgclass:'bulb2',
				imgid:'bulb2'
			},
			{
				imgclass:'switchoff',
				imgid:'switchoff'
			},
			{
				imgclass:'switchon',
				imgid:'switchon'
			}
		]
		}],
		uppertextblockadditionalclass: 'instructadditional',
		uppertextblock:[
			{
				textclass:'instruction',
				textdata: data.string.p3text16
			}],
	},

	//slide17
	{
		imageblock:[{
			imagestoshow:[
			{
				imgclass:'notepad1',
				imgid:'notepad1'
			}]
		}],
		uppertextblockadditionalclass: 'test0',
		uppertextblock:[
			{
				textclass:'test1',
				textdata: data.string.p3text5,
				imgclass:'tick1',
				imgid: 'tick'
			},
			{
				textclass:'test2',
				textdata:data.string.p3text6,
				imgclass:'tick2',
				imgid: 'tick'
			},
			{
				textclass:'test3',
				textdata:data.string.p3text7,
				imgclass:'tick3',
				imgid: 'tick'
			},
			{
				textclass:'test4',
				textdata:data.string.p3text8,
				imgclass:'tick4',
				imgid: 'tick'
			},
			{
				textclass:'test5',
				textdata:data.string.p3text9,
				imgclass:'tick5',
				imgid: 'tick'
			}],
	},

	//slide18
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg9'
			}]
		}],
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text17,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-4',
			imgclass: 'flipped-h',
			imgid : 'textbox1',
			imgsrc: '',
		}],
	},

	//slide19
	{
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'bg10'
			}]
		}],
		speechbox:[{
			txtblock:[{
				textdata : data.string.p3text18,
				textclass : 'text_inside',
			}],
			speechbox: 'sp-5',
			imgclass: 'flipped-h',
			imgid : 'textbox1',
			imgsrc: '',
		}],
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var colors = ['green','pink','orange','yellow','purple','lightgreen'];
	var shuffledcolors=colors.shufflearray();
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var clicked1 = false;
	var clicked2 = false;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg0", src: imgpath+"a_37.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg_sitting_room01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg_sitting_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg_dog02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"room_without_curtains.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg_water_filter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg_bed_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"room_main01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg9", src: imgpath+"bg_69.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg10", src: imgpath+"bg_70.png", type: createjs.AbstractLoader.IMAGE},
			{id: "notepad", src: imgpath+"note_pad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "notepad1", src: imgpath+"note_pad01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "telephone_ringing", src: imgpath+"telephone_ringing_with_girl.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "talking1", src: imgpath+"talking_on_phone02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking2", src: imgpath+"talking_on_phone01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "talking3", src: imgpath+"talking_on_phone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox0", src: imgpath+"text_box06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox1", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox2", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "textbox3", src: imgpath+"text_box05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi1", src: imgpath+"rumi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumiwithdog", src: imgpath+"rumi_with_dog_food01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumiwithdoggif", src: imgpath+"rumi_with_dog_food01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bowl", src: imgpath+"dog_bowl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tick", src: imgpath+"tick_mark.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curtainl", src: imgpath+"curtain_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curtainr", src: imgpath+"curtain_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_filter", src: imgpath+"water_filter01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "water_filter_gif", src: imgpath+"water_filter.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "book", src: imgpath+"book01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bag1", src: imgpath+"bag01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb1", src: imgpath+"bulb_off.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bulb2", src: imgpath+"bulb_on.png", type: createjs.AbstractLoader.IMAGE},
			{id: "switchon", src: imgpath+"switch_on.png", type: createjs.AbstractLoader.IMAGE},
			{id: "switchoff", src: imgpath+"switch_off.png", type: createjs.AbstractLoader.IMAGE},


			//textboxesp01
			{id: "coverpage", src: imgpath+'coverpage.png', type: createjs.AbstractLoader.IMAGE},
			{id: "icon-orange", src: imgpath+'icon-orange.png', type: createjs.AbstractLoader.IMAGE},
			{id: "sunImg", src: imgpath+'sun.png', type: createjs.AbstractLoader.IMAGE},
			// soundsicon-orange
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_5_1", src: soundAsset+"s3_p6_1.ogg"},
			{id: "sound_7", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s3_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s3_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s3_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s3_p15.ogg"},
			{id: "sound_15", src: soundAsset+"s3_p16.ogg"},
			{id: "sound_16", src: soundAsset+"s3_p17.ogg"},
			{id: "sound_17", src: soundAsset+"s3_p18.ogg"},
			{id: "sound_18", src: soundAsset+"s3_p19.ogg"},
			{id: "sound_19", src: soundAsset+"s3_p20.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
        vocabcontroller.findwords(countNext);

		$('.tick1').attr('src',preload.getResult('tick').src);
		$('.tick2').attr('src',preload.getResult('tick').src);
		$('.tick3').attr('src',preload.getResult('tick').src);
		$('.tick4').attr('src',preload.getResult('tick').src);
		$('.tick5').attr('src',preload.getResult('tick').src);

		switch(countNext) {
				case 0:
				play_diy_audio();
				nav_button_controls(1800);
				break;
				case 1:
				nav_button_controls(1000);
				break;
				case 4:
			  $('.speechbg').css('transform','rotate(0deg)');
				sound_player("sound_"+countNext,true);
				break;
				case 6:
                    sound_player("sound_5_1",false);
                    $('.test0,.notepad1').hide();
				 	$('.notepad').click(function(){
                        $(".clickmsg").hide();
                        $('.test0,.notepad1').show();
							$('.bg_full,.notepad').hide();
							$('.coverboardfull').css('background-color','#99b2dd');
					sound_player("sound_"+countNext,true);
					});
					break;

				case 7:
				  $('.test0,.notepad1').css({'left':'10%'});
				  $('.bg_full,.notepad').hide();
				  $('.coverboardfull').css('background-color','#99b2dd');
				  	sound_player("sound_"+countNext,true);
				 	break;

        case 8:
				sound_nav("sound_"+countNext);
				  $('.rumigif').hide();
					$('.testdiv').click(function(){
					  $('.rumigif').show();
						$(this).hide();
						nav_button_controls(1500);
					});
				 	break;

				case 9:
				case 11:
				case 13:
				case 15:
				case 17:
					$('.coverboardfull').css('background-color','#99b2dd');
					nav_button_controls(1000);
				 	break;

				case 10:
				sound_nav("sound_"+countNext);
					$('.rumigif').hide();

					$('.curtainl').click(function(){
						console.log('clicked');
					  $(this).animate({left:'18%','top':'-5%'});
						clicked1 = true;
						check();
					});
					$('.curtainr').click(function(){
						console.log('clicked');
					  $(this).animate({right:'55%'});
			 			clicked2 = true;
						check();
					});
 					break;

				case 12:
					sound_nav("sound_"+countNext);
					$('.water_filtergif').hide();
					$('.highlight').click(function(){
					  $('.water_filtergif').show();
						$(this).hide();
						nav_button_controls(2200);
					});
				 	break;

			  case 14:
				sound_nav("sound_"+countNext);
					$('.bag1').hide();
					dragdrop();
					break;

				case 16:
				sound_nav("sound_"+countNext);
				  $('.switchon').hide();
					$('.bulb2').hide();

          $('.switchoff').click(function(){
						$('.switchon').show();
						$(this).hide();
						$('.bulb2').show();
						nav_button_controls(100);
					});
					break;

				case 19:
				  $('.speechbg').css('transform','rotate(360deg)');
					sound_player("sound_"+countNext,true);
					break;

				default:
					sound_player("sound_"+countNext,true);
					break;

		}
	}

	function dragdrop(){
		  console.log('dragdrop');
			$(".draggable").draggable({
					containment: "body",
					revert: true,
					appendTo: "body",
					zindex: 10,
			});
			$('.droppable').droppable({
					accept : ".draggable",
					hoverClass: "hovered",
					drop: function(event, ui) {
             $('.bag1').show();
						 $('.book').hide();
						 nav_button_controls(100);
					}
			});
	}

	function check(){
		if (clicked1 === true && clicked2 === true){
				console.log('true');
				$('.option').css('animation-name','pulsfs');
				$('.option').click(function(){
					nav_button_controls(100);
				});
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id,nav){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav?nav_button_controls(0):"";
		});
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		clicked1=false,clicked2 = false;

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

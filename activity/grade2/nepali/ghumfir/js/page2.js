var imgpath = $ref + "/images/ghumfir/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide1
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s1_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg_main',
					imgsrc: ""
				}
			]
		}],
		// speechbox:[{
		// 	speechbox: 'sp-1 sld4_sp',
		// 	textdata : data.string.p1s4_box_txt,
		// 	textclass : 'txt1',
		// 	imgclass: 'box',
		// 	imgid : 'together',
		// 	imgsrc: '',
		// }]
	},
	// slide2
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg_main',
					imgsrc: ""
				}
			]
		}],
		// speechbox:[{
		// 	speechbox: 'sp-1 sld4_sp',
		// 	textdata : data.string.p1s4_box_txt,
		// 	textclass : 'txt1',
		// 	imgclass: 'box',
		// 	imgid : 'together',
		// 	imgsrc: '',
		// }]
	},
	// slide3
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s3_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_getdwn_bus hidn",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "scbusSec",
					imgid : 'school_bus01',
					imgsrc: ""
				},{
					imgclass: "school_bus",
					imgid : 'school_bus',
					imgsrc: ""
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s4_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_getdwn_bus",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted hidn gocGif",
					imgid : 'girl_on_cycle_gif',
					imgsrc: ""
				}
			]
		}]
	},
	// slide5
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s5_txt
		},{
			textclass: "papayaClk",
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_papayaShop",
					imgid : 'rajan02a',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
	// slide6 --->zoomAnim left
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s6_txt
		},{
			textclass: "tcktClk",
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_ticket",
					imgid : 'rajan03',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
	// slide7 --->zoomAnim left
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s7_txt
		},{
			textclass: "hrsClk",
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_horse",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
	// slide8 --->zoomAnim left
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s8_txt
		},{
			textclass: "rollerClk",
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_horse rjnRote",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
	// slide9 --->zoomAnim left
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s9_txt
		},{
			textclass: "gasClk",
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_horse rjnBln",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
	// slide10 --->zoomAnim left
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2_s2_txt
		},{
				txtindiv:true,
				textdivclass:"rjntlks",
				textclass: "txtindiv",
				textdata: data.string.p2_s10_txt
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'bg01',
					imgsrc: ""
				},{
					imgclass: "rjn_horse rjnBln",
					imgid : 'rajan04',
					imgsrc: ""
				},{
					imgclass: "girl_on_cycle inverted goc",
					imgid : 'girl_on_cycle',
					imgsrc: ""
				}
			]
		}]
	},
]

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var st1;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg_main", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bus", src: imgpath+"school_bus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school_bus01", src: imgpath+"school_bus01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rajan04", src: imgpath+"rajan04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rajan02a", src: imgpath+"rajan02a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rajan03", src: imgpath+"rajan03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_on_cycle", src: imgpath+"girl_in_cycle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_on_cycle_gif", src: imgpath+"girl_in_cycle_gif.gif", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "speechbox_fst", src: imgpath+"bubble-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbox_sec", src: imgpath+"bubble-1-10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s2_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext){
			case 2:
			sound_nav("sound_"+(countNext+1));
				$(".school_bus").on('click', function(){
					$(".school_bus").animate({
						right:"50%"
					},1000,function(){
						$(".rjn_getdwn_bus").fadeIn(100);
						$(".scbusSec").show(0);
						$(".school_bus").delay(50).hide(0);
						 st1 = setTimeout(function(){
							$(".scbusSec").animate({
								right:"100%"
							},1000, function(){
								nav_button_controls();
							});
						},1000);
					});
				});
			break;
			case 3:
			sound_nav("sound_"+(countNext+1));
				$(".girl_on_cycle").on("click", function(){
					$(".gocGif").show(0);
					$(".goc").hide(0);
					$(".gocGif").animate({
						left:"100%"
					},10000, function(){
					nav_button_controls(500);
				});
			});
			break;
			case 4:
			sound_nav("sound_"+(countNext+1));
				$(".papayaClk").on('click', function(){
					$(".coverboardfull").addClass("papayaZoom");
						nav_button_controls(3500);
				});
			break;
			case 5:
			sound_nav("sound_"+(countNext+1));
				$(".tcktClk").on('click', function(){
					$(".coverboardfull").addClass("ticketZoom");
					nav_button_controls(3500);
				});
			break;
			case 6:
			sound_nav("sound_"+(countNext+1));
				$(".hrsClk").on('click', function(){
					$(".coverboardfull").addClass("horseZoom");
					nav_button_controls(3500);
				});
			break;
			case 7:
			sound_nav("sound_"+(countNext+1));
				$(".rollerClk").on('click', function(){
					$(".coverboardfull").addClass("rollerZoom");
					nav_button_controls(3500);
				});
			break;
			case 8:
			sound_nav("sound_"+(countNext+1));
				$(".gasClk").on('click', function(){
					$(".coverboardfull").addClass("balloonZoom");
					nav_button_controls(3500);
				});
			break;
			default:
			sound_player("sound_"+(countNext+1));
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
		nav_button_controls(0);
		});
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		// clearTimeout(st1);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		// clearTimeout(st1);
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

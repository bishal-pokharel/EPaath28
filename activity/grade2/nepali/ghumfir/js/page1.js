var imgpath = $ref + "/images/ghumfir/";
var soundAsset = $ref+"/sounds/";

var content=[
	// coverpage
	{
		contentblockadditionalclass: "thebg1",
		extratextblock:[
			{
				textclass: "cvChpter",
				textdata: data.lesson.chapter
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "background",
					imgid : 'cover_page',
					imgsrc: ""
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: "yelowBg",
		extratextblock:[
			{
				textclass: "whiteCnvs",
				// textdata: data.lesson.chapter
		},{
				textclass: "rjnFeatureTxt headtxt",
				textdata: data.string.p1headtxt
		},{
				textclass: "rjnFeatureTxt clthtxt",
				textdata: data.string.p1clthstxt
		},{
				textclass: "rjnFeatureTxt shoetxt",
				textdata: data.string.p1shoetxt
		}],
		svgblock:[{
			svgid:"rajanSvg"
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "yelowBg",
		extratextblock:[
			{
				textclass: "whiteCnvs",
				// textdata: data.lesson.chapter
		},{
				textclass: "rjnFeatureTxt",
				textdata: data.string.p1lasttxt
		}],
		svgblock:[{
			svgid:"rajanSvg"
		}]
	},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "rajan", src: imgpath+"rajan.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1a", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_1b", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_1c", src: soundAsset+"s1_p2_2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		function spchBoxAfterTxt(soundId, spBoxClass){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play(soundId);
				current_sound.play();
				current_sound.on('complete', function(){
					$(spBoxClass).addClass("fadeInFast");
					navigationcontroller();
				});
		}
		switch(countNext){
			case 0:
			sound_player("sound_0");
			break;
			case 1:
				var s = Snap('#rajanSvg');
				var svg = Snap.load(preload.getResult("rajan").src,function(loadedFragment){
				s.append(loadedFragment);
					$("#rajan_x5F_main_1_").css("display","none");
				$("#hair_1_, #shoes_1_,#shirt_x5F_pant").css("filter","grayscale(100%)");
				$(".headtxt").css("opacity","1");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1a");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".clthtxt").css("opacity","1");
					$("#shirt_x5F_pant").css("filter","grayscale(0%)");
					$("#hair_1_, #shoes_1_").css({
						"filter":"grayscale(100%)",
						"opacity":"0.7"
					});
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_1b");
						current_sound.play();
						current_sound.on('complete', function(){
							$(".shoetxt").css("opacity","1");
							$("#shoes_1_").css({
								"filter":"grayscale(0%)",
								"opacity":"1"
							});
							$("#hair_1_, #shirt_x5F_pant").css({
								"filter":"grayscale(100%)",
								"opacity":"0.7"
							});
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("sound_1c");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(100);
							});
						});
				});

				});
			break;
			case 2:
				$(".rjnFeatureTxt").css("opacity","1");
				var s = Snap('#rajanSvg');
				$("#rajanSvg").css("left","48%");
				var svg = Snap.load(preload.getResult("rajan").src,function(loadedFragment){
				s.append(loadedFragment);
				// $("#hair_1_, #shoes_1_,#shirt_x5F_pant").css("display","none");
				$("#body_1_").css("display","none");

				});
				sound_player("sound_2");
		}
	}

  function appeartextonebyone(className) {
      var delaytime = 1000;
      setTimeout(function(){
          vocabcontroller.findwords(countNext);
					navigationcontroller(true);
      },(className.length+2)*1000);
      for (var i = 0; i < className.length; i++) {
          delaytime = delaytime+1000;
          $(className).eq(i).delay(delaytime).animate({"opacity":"1"},1000);

      }
  }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

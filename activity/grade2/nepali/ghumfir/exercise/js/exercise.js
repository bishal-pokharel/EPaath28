var imgpath = $ref+"/images/exercise/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide 1
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_1_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_1_opn1
			},{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_1_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"day",
				imgsrc:'',
			}]
		}]
	},
	// slide 2
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_2_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_2_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_2_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"watering_plant",
				imgsrc:'',
			}]
		}]
	},
	// slide 3
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_3_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_3_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_3_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"playing_football",
				imgsrc:'',
			}]
		}]
	},
	// slide 4
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_4_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_4_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_4_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"bird_roof_top",
				imgsrc:'',
			}]
		}]
	},
	// slide 5
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_5_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_5_opn1
			},{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_5_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"sticks",
				imgsrc:'',
			}]
		}]
	},
	// slide 6
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_6_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_6_opn1
			},{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_6_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"bed_room",
				imgsrc:'',
			}]
		}]
	},
	// slide 7
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_7_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_7_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_7_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"father_daughter",
				imgsrc:'',
			}]
		}]
	},
	// slide 8
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_8_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_8_opn1
			},{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_8_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"four_people",
				imgsrc:'',
			}]
		}]
	},
	// slide 9
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_9_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_9_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_9_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"holding_paper",
				imgsrc:'',
			}]
		}]
	},
	// slide 10
	{
		contentblockadditionalclass : "blueBg",
		exetype1:true,
		qntext:[{
			qntextdivclass:"topInstrn",
			qntextclass:"qn_top",
			qndata:data.string.excqn
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"anstoreplace",
			qntextdivclass:"secTyp_SecInstrn",
			qntextclass:"qn_top",
			qndata:data.string.exc_10_qn1
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv optionsdivSecType",
			exeoptions:[{
				optaddclass:"class1",
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_10_opn1
			},{
				optcontainerextra:"opnContLong",
				optdata:data.string.exc_10_opn2
			}]
		}],
		imageblock:[{
			imgindiv:true,
			imgindivclass:"singleImgCont",
			imagestoshow:[{
				imgclass:"singleImg",
				imgid:"hot_tea",
				imgsrc:'',
			}]
		}]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "day", src: imgpath+"day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "watering_plant", src: imgpath+"watering_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "playing_football", src: imgpath+"playing_football.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bird_roof_top", src: imgpath+"bird_roof_top.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sticks", src: imgpath+"sticks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bed_room", src: imgpath+"bed_room.png", type: createjs.AbstractLoader.IMAGE},
			{id: "father_daughter", src: imgpath+"father_daughter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "four_people", src: imgpath+"four_people.png", type: createjs.AbstractLoader.IMAGE},
			{id: "holding_paper", src: imgpath+"holding_paper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hot_tea", src: imgpath+"hot_tea.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		content[countNext].imgload?put_image_sec(content, countNext):"";
	 	var corcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	testin.numberOfQuestions();

	 	var ansClicked = false;
	 	var wrngClicked = false;

	  function rand_generator(limit){
	    var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
	    return randNum;
	  }
		// $(".qn_top").prepend(countNext+1+". ");

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}


	 	randomize(".optionsdiv");
	 	randomize(".twoImgsCont");

		if(countNext==0){
			sound_player("sound_0");
		}

		$(".buttonsel").click(function(){
			if($(this).hasClass("class1")){
				switch (countNext) {
					case 5:
					case 9:
						var ans = $(this).text();
						$(".anstoreplace").html(ans).css({"color":"#8eba2d","text-decoration":"underline dashed"});
						// alert(ans);
					break;
					default:

				}
				play_correct_incorrect_sound(1);
				$(this).css({
					"pointer-events":"none",
					"background":"#8eba2d",
					"border":"5px solid #ff0"
				});
				$(".buttonsel").css("pointer-events",'none');
				$(this).siblings(".corctopt").show();
				!wrngClicked?testin.update(true):testin.update(false);
				$nextBtn.show(0);
			}else{
				play_correct_incorrect_sound(0);
				$(this).css({
					"pointer-events":"none",
					"background":"#f00",
					"border":"5px solid #b81021"
				});
				$(this).siblings(".wrngopt").show();
				wrngClicked = true;
			}
		});
	}
	// function nav_button_controls(delay_ms){
	// 	timeoutvar = setTimeout(function(){
	// 		if(countNext==0){
	// 			$nextBtn.show(0);
	// 		} else if( countNext>0 && countNext == $total_page-1){
	// 			$prevBtn.show(0);
	// 			$nextBtn.show(0);
	// 			// ole.footerNotificationHandler.pageEndSetNotification();
	// 		} else{
	// 			$prevBtn.show(0);
	// 			$nextBtn.show(0);
	// 		}
	// 	},delay_ms);
	// }
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}

	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgexc')){
			for(var j=0;j<content[count].imgexc.length;j++){
				var imageblock = content[count].imgexc[j];
				if(imageblock.hasOwnProperty('imgexcoptions')){
					var imageClass = imageblock.imgexcoptions;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector);
						$(selector).attr('src',image_src);
					}
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	//  }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref + "/sounds/";

var ex  = new buzz.sound(soundAsset + "ex.ogg");
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var ctx;

var baseContent = {
    uppertextblock: [{
        textclass: 'title-text',
        textdata: data.string.ex_titleText
    }],
    poemblock: [{
        textclass: 'poem-text',
        textdata: data.string.ex_poem_1
    }, {
        textclass: 'poem-text',
        textdata: data.string.ex_poem_2
    }]
};

var content=[
    // FIrst slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q1_a1, additionalAnswerClass: 'correct'},
                {answer: data.string.ex_p1_q1_a2},
                {answer: data.string.ex_p1_q1_a3}
            ]
        }],
    }),
    // Second slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q2_a1},
                {answer: data.string.ex_p1_q2_a2, additionalAnswerClass: 'correct'},
                {answer: data.string.ex_p1_q2_a3}
            ]
        }],
    }),
    // Third slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q3_a1},
                {answer: data.string.ex_p1_q3_a2},
                {answer: data.string.ex_p1_q3_a3, additionalAnswerClass: 'correct'}
            ]
        }],
    }),
    // Fourth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q4_a1},
                {answer: data.string.ex_p1_q4_a2},
                {answer: data.string.ex_p1_q4_a3, additionalAnswerClass: 'correct'}
            ]
        }],
    }),
    // Fifth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q5_a1, additionalAnswerClass: 'correct'},
                {answer: data.string.ex_p1_q5_a2},
                {answer: data.string.ex_p1_q5_a3}
            ]
        }],
    }),
    // Sixth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q6_a1},
                {answer: data.string.ex_p1_q6_a2, additionalAnswerClass: 'correct'},
                {answer: data.string.ex_p1_q6_a3}
            ]
        }],
    }),
    // Seventh slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q7_a1},
                {answer: data.string.ex_p1_q7_a2},
                {answer: data.string.ex_p1_q7_a3, additionalAnswerClass: 'correct'}
            ]
        }],
    }),
    // Eighth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q8_a1},
                {answer: data.string.ex_p1_q8_a2},
                {answer: data.string.ex_p1_q8_a3, additionalAnswerClass: 'correct'}
            ]
        }],
    }),
    // Ninth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q9_a1},
                {answer: data.string.ex_p1_q9_a2, additionalAnswerClass: 'correct'},
                {answer: data.string.ex_p1_q9_a3}
            ]
        }],
    }),
    // Tenth slide
    $.extend({}, baseContent, {
        questionblock: [{
            textclass: 'question-text',
            textdata: data.string.ex_p1_q1,
            listclass: 'answers',
            answers: [
                {answer: data.string.ex_p1_q10_a1},
                {answer: data.string.ex_p1_q10_a2},
                {answer: data.string.ex_p1_q10_a3, additionalAnswerClass: 'correct'}
            ]
        }],
    }),


];

$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
    var $contentBlock = $('.contentblock');
	var countNext = 0;

	var $total_page = 10;

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

      var testin = new EggTemplate();
   
        //eggTemplate.eggMove(countNext);
        testin.init(10);
 //     /*random scoreboard eggs*/
	// var score = 0;
	// imageArray.shufflearray();

    // General template
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

        // Navigation buttons
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
        $('.exefin').hide(0);

        var onceIncorrect = false;

		switch(countNext) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                countNext==0?ex.play():"";
                $('#egg' + countNext).addClass('eggmove');
                $('.answer').addClass('hover-effect');
                $('.answer').on('click', function() {
                    if ($(this).hasClass('correct')) {
                        if (!onceIncorrect) {
                            // Animate egggs
                        testin.update(true);
                        }
                        $(this).addClass('correct-effect');
                        $('.answer').removeClass('hover-effect');
                        $('.answer').off('click');
                        play_correct_incorrect_sound(true);
                        $nextBtn.show(0);
                    } else {
                        if (!onceIncorrect) {
                        }
                        $(this).addClass('incorrect-effect');
                        $(this).removeClass('hover-effect');

                        onceIncorrect = true;
                        play_correct_incorrect_sound(false);
                        // Animate eggs
                        //$("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
                        testin.update(false);
        				$(this).addClass('incorrect_answer');
                    }
                });
                break;
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 11){
			templateCaller();
		}
        testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
 * event.key reurns the value of key pressed by user and it is converted to integer
 * event.target gets the element where event is occuring (usually a div)
 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
 * input_class and button_classes should be something like '.class_name'
 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
 */
function integer_box(input_class, max_number, button_class) {
	$(input_class).keydown(function(event){
		var charCode = (event.which) ? event.which : event.keyCode;
		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys,, 46=> del */
		if(charCode === 13 && button_class!=null) {
	        $(button_class).trigger("click");
		}
		var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
		//check if user inputs del, backspace or arrow keys
			if (!condition) {
			return true;
		}
		//check . and 0-9 separately after checking arrow and other keys
		if((charCode < 48 || charCode > 57) && (charCode < 97 || charCode > 105)){
			return false;
		}
		//check max no of allowed digits
		if (String(event.target.value).length >= max_number) {
			return false;
		}
			return true;
	});
}

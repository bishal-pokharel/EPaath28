var imgpath = $ref + "/images/";
soundAsset = $ref + "/sounds/";

var dialog1  = new buzz.sound(soundAsset + "1.ogg");
var dialog2  = new buzz.sound(soundAsset + "2.ogg");
var dialog3  = new buzz.sound(soundAsset + "3.ogg");
var dialog4  = new buzz.sound(soundAsset + "4.ogg");
var dialog5  = new buzz.sound(soundAsset + "5.ogg");
var dialog6  = new buzz.sound(soundAsset + "p1_s1.ogg");

var sound_data  = dialog5;

var soundcontent = [dialog1, dialog2, dialog3, dialog4, dialog5]; 

var content;
	 content=[
	//1st slide
	{
		hasbgimg: true,
		hasheaderblock : true,
		maintextblock:[
			{
				texttoshow: [
					{
						textdata: data.string.p1_0,
						textclass: 'chapter-title'
					},
					{
						textdata: data.string.p1_1,
						textclass: 'writer'
					}

				]
			}
		]
	},

	//2nd slide
	{
		hasheaderblock: true,
		hasbgimg: true,
		
		storytextblockadditionalclass : "bottom_para",
		storytextblock : [
		{
			textclass : "text_story",
			textdata : data.string.p2_0,
		}],
		imageblock: [{
			imagetoshow:[{
				imgsrc: imgpath + 'speaker.png',
				imgclass: 'speaker'
			}]
		}]		
	},
	
	//3rd slide
	{
		hasheaderblock: true,
		hasbgimg: true,
		
		storytextblockadditionalclass : "bottom_para",
		storytextblock : [
		{
			textclass : "text_story",
			textdata : data.string.p2_1,
		}],
		imageblock: [{
			imagetoshow:[{
				imgsrc: imgpath + 'speaker.png',
				imgclass: 'speaker'
			}]
		}]		
	},
	
	//4th slide
	{
		hasheaderblock: true,
		hasbgimg: true,
		
		storytextblockadditionalclass : "bottom_para",
		storytextblock : [
		{
			textclass : "text_story",
			textdata : data.string.p2_2,
		}],
		imageblock: [{
			imagetoshow:[{
				imgsrc: imgpath + 'speaker.png',
				imgclass: 'speaker'
			}]
		}]		
	},
	
	//5th slide
	{
		hasheaderblock: true,
		hasbgimg: true,
		
		storytextblockadditionalclass : "bottom_para",
		storytextblock : [
		{
			textclass : "text_story",
			textdata : data.string.p2_3,
		}],
		imageblock: [{
			imagetoshow:[{
				imgsrc: imgpath + 'speaker.png',
				imgclass: 'speaker'
			}]
		}]		
	},
	
	//6th slide
	{
		hasheaderblock: true,
		hasbgimg: true,
		
		storytextblockadditionalclass : "bottom_para",
		storytextblock : [
		{
			textclass : "text_story",
			textdata : data.string.p2_4,
		}],
		imageblock: [{
			imagetoshow:[{
				imgsrc: imgpath + 'speaker.png',
				imgclass: 'speaker'
			}]
		}]		
	},
];


// }
$(function(){
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var isFirefox = typeof InstallTrigger !== 'undefined';


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/


 function navigationcontroller(islastpageflag) {
	 typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	 if (countNext == 0 && $total_page != 1) {
		 $nextBtn.show(0);
		 $prevBtn.hide(0);
	 } else if ($total_page == 1) {
		 $prevBtn.hide(0);
		 $nextBtn.hide(0);
	 } else if (countNext > 0 && countNext < $total_page - 1) {
		 $nextBtn.show(0);
		 $prevBtn.show(0);
	 } else if (countNext == $total_page - 1) {
		 $nextBtn.hide(0);
		 $prevBtn.show(0);
				
		setTimeout(function() {
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}, 8000);
	 }
 }
	/*=====  End of user navigation controller function  ======*/

	/*=====  function to control sound play activity  ======*/
	function playSound(soundIndex1){
      setTimeout(function(){
         soundcontent[soundIndex1].play().bind("playing", function() {
			$(".text-highlighter").show(0).promise().done(function() {
				$(".text-highlighter2").delay(2000).show(0);				
			});
          }).bind('ended', function() {
          	if(countNext > 0 && countNext < $total_page-1) {
	            setTimeout(function() {
	                $nextBtn.show(0);
	                $prevBtn.show(0);
	            }, 1000);
	        }else if(countNext == $total_page - 1) {
                $nextBtn.hide(0);
                $prevBtn.show(0);
			}

          	$(".definitiontextstyle").hide(0);
          	$(".definitiontextstyle2").hide(0);
          });
        }, 1000);
    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		$board.html(html);
		var $totaltext = $(".definitiontextstyle").text().length;
		// console.log($totaltext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				dialog6.play();
                dialog6.bindOnce('ended', function(){
                    $nextBtn.show(0);
                });
			break;
			case 1:
				for_all_slides('text_story', soundcontent[0], false);
			break;
			case 2:
				for_all_slides('text_story', soundcontent[1], false);
				$(".bg-img").css('background-image', 'url(' + imgpath + "03.jpg" + ')');
			break;
			case 3:
				for_all_slides('text_story', soundcontent[2], false);
				$(".bg-img").css('background-image', 'url(' + imgpath + "05.jpg" + ')');
			break;
			case 4:
				for_all_slides('text_story', soundcontent[3], false);
			break;
			case 5:
				for_all_slides('text_story', soundcontent[4], true);
				$(".bg-img").css('background-image', 'url(' + imgpath + "06.jpg" + ')');
			break;
		}
		function for_all_slides(text_class, my_sound_data, last_page_flag){
			var $textblack = $("."+text_class);
			sound_data =  my_sound_data;
			var current_text = $textblack.html();
			// current_text.replace(/<.*>/, '');
			play_text($textblack, current_text);
    		sound_data.bindOnce('ended', function(){
    			change_slides(last_page_flag, text_class);
			});
			$("."+text_class).click(function(){
				sound_data.play();
				$("."+text_class).css('pointer-events','none');
				$(".speaker").css('pointer-events','none');
				$('#span_speec_text').addClass('is_playing');
    			sound_data.bindOnce('ended', function(){
	    			$prevBtn.show(0);
    				if(countNext!=content.length-1){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
					$('#span_speec_text').removeClass('is_playing');
				});
			});
		}
		function change_slides(last_page_flag, text_class){
			var checking_interval = setInterval(function(){
				if(textanimatecomplete){
					if(!last_page_flag){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
	    			$prevBtn.show(0);
					$("."+text_class).css('pointer-events','all');
					$(".speaker").css('pointer-events','all');
       				vocabcontroller.findwords(countNext);
					textanimatecomplete = false;
	    			clearInterval(checking_interval);
				} else{
					$("."+text_class).css('pointer-events','none');
					$(".speaker").css('pointer-events','none');
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}
			},50);
		}
	}
	/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		if (0 < message.length && !textanimatecomplete) {
			var nextText = message.substring(0, 1);
			var additionalinterval = 0;
			if (nextText == "<") {
				additionalinterval = 800;
				$span_speec_text.append("<br>");
				message = message.substring(4, message.length);
			} else {
				$span_speec_text.append(nextText);
				message = message.substring(1, message.length);
			}
			$this.html($span_speec_text);
			$this.append(message);
			setTimeout(function() {
				show_text($this, $span_speec_text, message, interval);
			}, (interval + additionalinterval));
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	var original_text;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text){
		original_text =  text;
		if(isFirefox){
			$this.html("<span id='span_speec_text'></span>"+text);
			$prevBtn.hide(0);
			var $span_speec_text = $("#span_speec_text");
			// $this.css("background-color", "#faf");
			show_text($this, $span_speec_text,text, 145);	// 65 ms is the interval found out by hit and trial
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				ternimatesound_play_animate(text);
			});
		} else {
			$this.html("<span id='span_speec_text'>"+original_text+"</span>");
			sound_data.play();
			sound_data.bind('ended', function(){
				sound_data.unbind('ended');
				soundplaycomplete = true;
				textanimatecomplete =  true;
				animationinprogress = false;
			});
		}

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(original_text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
				}
			}, 250);
		}
	}
	

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	$nextBtn.on("click", function(){
		sound_data.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		sound_data.stop();
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	// setTimeout(function(){
		total_page = content.length;
		templateCaller();
	// }, 250);


});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class = " + stylerulename + " >";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

//page 1

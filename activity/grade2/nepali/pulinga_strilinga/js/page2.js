var imgpath = $ref + "/images/Images_for_DIY/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintext",
        uppertextblock: [
            {
                textclass: "content centertext p2text1",
                textdata: data.string.p2text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "mainpage",
                    imgclass: "relativecls img",
                    imgid: 'mainImg',
                    imgsrc: "",
                },
            ]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintext",
        uppertextblock: [
            {
                textclass: "subtopic centertext p2text1",
                textdata: data.string.p2text2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "mainimg",
                    imgclass: "relativecls imgbg",
                    imgid: 'bgImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "manstanding",
                    imgclass: "relativecls imageclick manstandingimg showbuba",
                    imgid: 'manstandingImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg buba showbuba",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.buba
                        }

                    ]
                },
                {
                    imgdiv: "ladystanding",
                    imgclass: "relativecls imageclick ladystandingimg showama",
                    imgid: 'ladystandingImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg ama showama",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.ama
                        }

                    ]
                },
                {
                    imgdiv: "ladyseating",
                    imgclass: "relativecls imageclick ladyseatingimg showdidi",
                    imgid: 'ladyseatingImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg didi showdidi",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.didi
                        }

                    ]
                },
                {
                    imgdiv: "boycrawl",
                    imgclass: "relativecls imageclick boycrawlimg showbabu",
                    imgid: 'boycrawlImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg babu showbabu",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.babu
                        }

                    ]
                },
                {
                    imgdiv: "momandgirl",
                    imgclass: "relativecls imageclick momandgirlimg showfupu",
                    imgid: 'momandgirlImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg fupu showfupu",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.fupu
                        },

                    ]
                },
                {
                    imgdiv: "daughter",
                    imgclass: "relativecls imageclick daughterimg shownani",
                    imgid: 'daughterImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg nani shownani",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.nani
                        },

                    ]
                },

                {
                    imgdiv: "girlplay",
                    imgclass: "relativecls imageclick girlplayimg showbahini",
                    imgid: 'girlplayImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg bahini showbahini",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.bahini
                        }

                    ]
                },
                {
                    imgdiv: "ball",
                    imgclass: "relativecls imageclick ballimg",
                    imgid: 'ballImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "boyplay",
                    imgclass: "relativecls imageclick boyplayimg showbhai",
                    imgid: 'boyplayImg',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg bhai showbhai",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.bhai
                        }

                    ]
                },
                {
                    imgdiv: "boyplay1",
                    imgclass: "relativecls imageclick boyplay1img showdai",
                    imgid: 'boyplay1Img',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg dai showdai",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.dai
                        }

                    ]
                },
                {
                    imgdiv: "mansitting1",
                    imgclass: "relativecls imageclick mansitting1img showfupaju",
                    imgid: 'mansitting1Img',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg fupaju showfupaju",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.fupaju
                        }

                    ]
                },
                {
                    imgdiv: "mansitting2",
                    imgclass: "relativecls imageclick mansitting2img showkaka",
                    imgid: 'mansitting2Img',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg kaka showkaka",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.kaka
                        }

                    ]
                },
                {
                    imgdiv: "ladystanding02",
                    imgclass: "relativecls imageclick ladystanding02img showkaki",
                    imgid: 'ladystanding02Img',
                    imgsrc: "",
                    subimg:[
                        {
                            imgdiv:"subimg kaki showkaki",
                            imgclass: "boxdiv relativecls",
                            imgsrc:imgpath+"text_box01.png",
                            imgtextclass:"box boxtext relativecls",
                            imgtext:data.string.kaki
                        }

                    ]
                },
            ]
        }]
    },



    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "mainimg1",
                        imgclass: "relativecls imgbg",
                        imgid: 'bgImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "manstanding1 d1",
                        imgclass: "relativecls imageclick manstandingimg",
                        imgid: 'manstandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding1",
                        imgclass: "relativecls ladystandingimg",
                        imgid: 'ladystandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladyseating1",
                        imgclass: "relativecls ladyseatingimg",
                        imgid: 'ladyseatingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boycrawl1 d2",
                        imgclass: "relativecls imageclick boycrawlimg",
                        imgid: 'boycrawlImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "momandgirl1",
                        imgclass: "relativecls  momandgirlimg",
                        imgid: 'momandgirlImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "girlplay1",
                        imgclass: "relativecls girlplayimg",
                        imgid: 'girlplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ball1",
                        imgclass: "relativecls ballimg",
                        imgid: 'ballImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay11 d3",
                        imgclass: "relativecls imageclick boyplayimg",
                        imgid: 'boyplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay2 d4",
                        imgclass: "relativecls imageclick boyplay1img",
                        imgid: 'boyplay1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting11 d5",
                        imgclass: "relativecls imageclick mansitting1img",
                        imgid: 'mansitting1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting3 d6",
                        imgclass: "relativecls imageclick mansitting2img",
                        imgid: 'mansitting2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding022",
                        imgclass: "relativecls ladystanding02img",
                        imgid: 'ladystanding02Img',
                        imgsrc: "",
                    },
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    divtxt:data.string.p2text3,
                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "mdiv1 d1",
                            imgclass: "relativecls manstandingimg",
                            imgid: 'manstandingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv2 d2",
                            imgclass: "relativecls boycrawlimg",
                            imgid: 'boycrawlImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv3 d3",
                            imgclass: "relativecls boyplayimg",
                            imgid: 'boyplayImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv4 d4",
                            imgclass: "relativecls boyplay1img",
                            imgid: 'boyplay1Img',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv5 d5",
                            imgclass: "relativecls mansitting1img",
                            imgid: 'mansitting1Img',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv6 d6",
                            imgclass: "relativecls mansitting2img",
                            imgid: 'mansitting2Img',
                            imgsrc: "",
                        },

                    ]
                }]
            }
        ],
        textblock:[{
            textdiv:"congrats",
            textdata:data.string.congratulationmale
        }]
    },
    // slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdivSec",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "mainimg1",
                        imgclass: "relativecls imgbg",
                        imgid: 'bgImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "manstanding1 d1",
                        imgclass: "relativecls imageclick manstandingimg",
                        imgid: 'manstandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding1",
                        imgclass: "relativecls ladystandingimg",
                        imgid: 'ladystandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladyseating1",
                        imgclass: "relativecls ladyseatingimg",
                        imgid: 'ladyseatingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boycrawl1 d2",
                        imgclass: "relativecls imageclick boycrawlimg",
                        imgid: 'boycrawlImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "momandgirl1",
                        imgclass: "relativecls  momandgirlimg",
                        imgid: 'momandgirlImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "girlplay1",
                        imgclass: "relativecls girlplayimg",
                        imgid: 'girlplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ball1",
                        imgclass: "relativecls ballimg",
                        imgid: 'ballImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay11 d3",
                        imgclass: "relativecls imageclick boyplayimg",
                        imgid: 'boyplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay2 d4",
                        imgclass: "relativecls imageclick boyplay1img",
                        imgid: 'boyplay1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting11 d5",
                        imgclass: "relativecls imageclick mansitting1img",
                        imgid: 'mansitting1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting3 d6",
                        imgclass: "relativecls imageclick mansitting2img",
                        imgid: 'mansitting2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding022",
                        imgclass: "relativecls ladystanding02img",
                        imgid: 'ladystanding02Img',
                        imgsrc: "",
                    },
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    tclass:"textcls1",
                    divtxt:"",
                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "mdiv1 d1",
                            imgclass: "relativecls manstandingimg",
                            imgid: 'manstandingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv2 d2",
                            imgclass: "relativecls boycrawlimg",
                            imgid: 'boycrawlImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv3 d3",
                            imgclass: "relativecls boyplayimg",
                            imgid: 'boyplayImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv4 d4",
                            imgclass: "relativecls boyplay1img",
                            imgid: 'boyplay1Img',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv5 d5",
                            imgclass: "relativecls mansitting1img",
                            imgid: 'mansitting1Img',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "mdiv6 d6",
                            imgclass: "relativecls mansitting2img",
                            imgid: 'mansitting2Img',
                            imgsrc: "",
                        },

                    ]
                }]
            }
        ],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textdesign",
                textdiv:"subtopic congratstitle",
                cntrtextaddnalclass:"longerwidth",
                textdata:data.string.male1
            },

            {
            datahighlightflag: true,
            datahighlightcustomclass: "textdesign",
            textdiv:"congrats1",
            textdata:data.string.p2text5
        }

        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdiv",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "mainimg1",
                        imgclass: "relativecls imgbg",
                        imgid: 'bgImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "manstanding1",
                        imgclass: "relativecls manstandingimg",
                        imgid: 'manstandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding1 d1",
                        imgclass: "relativecls imageclick ladystandingimg",
                        imgid: 'ladystandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladyseating1 d2",
                        imgclass: "relativecls imageclick ladyseatingimg",
                        imgid: 'ladyseatingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boycrawl1",
                        imgclass: "relativecls boycrawlimg",
                        imgid: 'boycrawlImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "momandgirl1 d3",
                        imgclass: "relativecls imageclick momandgirlimg",
                        imgid: 'momandgirlImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "girlplay1 d4",
                        imgclass: "relativecls imageclick girlplayimg",
                        imgid: 'girlplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ball1",
                        imgclass: "relativecls ballimg",
                        imgid: 'ballImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay11",
                        imgclass: "relativecls boyplayimg",
                        imgid: 'boyplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay2",
                        imgclass: "relativecls boyplay1img",
                        imgid: 'boyplay1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting11",
                        imgclass: "relativecls mansitting1img",
                        imgid: 'mansitting1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting3",
                        imgclass: "relativecls mansitting2img",
                        imgid: 'mansitting2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding022 d5",
                        imgclass: "relativecls imageclick ladystanding02img",
                        imgid: 'ladystanding02Img',
                        imgsrc: "",
                    },
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    divtxt:data.string.p2text4,
                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "div1 d1",
                            imgclass: "relativecls imageclick ladystandingimg",
                            imgid: 'ladystandingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div2 d2",
                            imgclass: "relativecls imageclick ladyseatingimg",
                            imgid: 'ladyseatingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div3 d3",
                            imgclass: "relativecls imageclick momandgirlimg",
                            imgid: 'momandgirlImg1',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div4 d4",
                            imgclass: "relativecls imageclick girlplayimg",
                            imgid: 'girlplayImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div5 d5",
                            imgclass: "relativecls imageclick ladystanding02img",
                            imgid: 'ladystanding02Img',
                            imgsrc: "",
                        },

                    ]
                }]
            }
        ],
        textblock:[{
            textdiv:"congrats",
            textdata:data.string.congratulationfemale
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imgwithdiv: [{
            divcls: "quesdivSec",
            imageblock: [{
                imagestoshow: [
                    {
                        imgdiv: "mainimg1",
                        imgclass: "relativecls imgbg",
                        imgid: 'bgImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "manstanding1",
                        imgclass: "relativecls manstandingimg",
                        imgid: 'manstandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding1 d1",
                        imgclass: "relativecls imageclick ladystandingimg",
                        imgid: 'ladystandingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladyseating1 d2",
                        imgclass: "relativecls imageclick ladyseatingimg",
                        imgid: 'ladyseatingImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boycrawl1",
                        imgclass: "relativecls boycrawlimg",
                        imgid: 'boycrawlImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "momandgirl1 d3",
                        imgclass: "relativecls imageclick momandgirlimg",
                        imgid: 'momandgirlImg1',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "girlplay1 d4",
                        imgclass: "relativecls imageclick girlplayimg",
                        imgid: 'girlplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ball1",
                        imgclass: "relativecls ballimg",
                        imgid: 'ballImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay11",
                        imgclass: "relativecls boyplayimg",
                        imgid: 'boyplayImg',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "boyplay2",
                        imgclass: "relativecls boyplay1img",
                        imgid: 'boyplay1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting11",
                        imgclass: "relativecls mansitting1img",
                        imgid: 'mansitting1Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "mansitting3",
                        imgclass: "relativecls mansitting2img",
                        imgid: 'mansitting2Img',
                        imgsrc: "",
                    },
                    {
                        imgdiv: "ladystanding022 d5",
                        imgclass: "relativecls imageclick ladystanding02img",
                        imgid: 'ladystanding02Img',
                        imgsrc: "",
                    },
                ]
            }]
        },
            {
                extradiv:[{
                    divcls:"outerdiv",
                    tclass:"textcls1",
                    divtxt:"",

                }],
                divcls: "ansdiv",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv: "div1 d1",
                            imgclass: "relativecls imageclick ladystandingimg",
                            imgid: 'ladystandingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div2 d2",
                            imgclass: "relativecls imageclick ladyseatingimg",
                            imgid: 'ladyseatingImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div3 d3",
                            imgclass: "relativecls imageclick momandgirlimg",
                            imgid: 'momandgirlImg1',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div4 d4",
                            imgclass: "relativecls imageclick girlplayimg",
                            imgid: 'girlplayImg',
                            imgsrc: "",
                        },
                        {
                            imgdiv: "div5 d5",
                            imgclass: "relativecls imageclick ladystanding02img",
                            imgid: 'ladystanding02Img',
                            imgsrc: "",
                        },

                    ]
                }]
            }
        ],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textdesign",
                textdiv:"subtopic congratstitle",
                cntrtextaddnalclass:"longerwidth",
                textdata:data.string.female1
            },
            {
            datahighlightflag: true,
            datahighlightcustomclass: "textdesign",
            textdiv:"congrats1",
            textdata:data.string.p2text6
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "mainImg", src: imgpath+"main_bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg1", src: imgpath+"bg_new_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "manstandingImg", src: imgpath+"manstanding.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladystandingImg", src: imgpath+"ladystanding01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladyseatingImg", src: imgpath+"ladysitting.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boycrawlImg", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "momandgirlImg", src: imgpath+"mum-sitting.png", type: createjs.AbstractLoader.IMAGE},
            {id: "momandgirlImg1", src: imgpath+"mumandgirl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlplayImg", src: imgpath+"girlplay.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ballImg", src: imgpath+"ball.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyplayImg", src: imgpath+"boyplay.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyplay1Img", src: imgpath+"boyplay01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mansitting1Img", src: imgpath+"mansitting01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mansitting2Img", src: imgpath+"mansitting02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ladystanding02Img", src: imgpath+"ladystanding02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "daughterImg", src: imgpath+"daughter.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p2_s1.ogg"},
            {id: "sound_2_1", src: soundAsset+"p2_s2_1.ogg"},
            {id: "sound_2_2", src: soundAsset+"p2_s2_2.ogg"},
            {id: "sound_3", src: soundAsset+"p2_s3.ogg"},
            {id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
            {id: "sound_4_2", src: soundAsset+"p2_s4_2.ogg"},
            {id: "sound_5", src: soundAsset+"p2_s5.ogg"},
            {id: "camera", src: soundAsset+"camera.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_1",true);
                showname();
                break;
            case 2:
                sound_player("sound_2_1",false);
                put_image2(content, countNext);
                checkans();
                break;
            case 3:
                sound_player("sound_3",true);
                $(".ansdiv").css("top",'10%');
                put_image2(content,countNext);
                break;
            case 4:
                sound_player("sound_4_1",false);
                put_image2(content, countNext);
                checkans2();
                break;
            case 5:
                sound_player("sound_5",true);
                $(".ansdiv").css("top",'10%');
                put_image2(content,countNext);
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):'';
        });
    }




    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function  checkans(){
        $(".mdiv1,.mdiv2,.mdiv3,.mdiv4,.mdiv5,.mdiv6,.congrats").hide(0);

        $(".imageclick").click(function(){
          var imgtoshow = $(this).parent().attr('class').split(' ').pop();
            sound_player("camera",false);
            $("."+imgtoshow).show().addClass("done");
                showNav("male","sound_2_2");
        });


    }
    function checkans2(){
        $(".div1,.div2,.div3,.div4,.div5,.div6,.congrats").hide(0);

        $(".imageclick").click(function(){
            var imgtoshow = $(this).parent().attr('class').split(' ').pop();
            sound_player("camera",false);
            $("."+imgtoshow).show().addClass("done");
            showNav("female","sound_4_2");
        });
    }
    function showNav(gender,soundId){
        $('.quesdiv').css("opacity","0");
        setTimeout( function(){
            $('.quesdiv').css("opacity","1");
        } ,100);
        var sizeval = (gender=='male'?6:5);
        if($(".outerdiv").find(".done").size()==sizeval) {
            $(".congrats").show(0);
            $(".quesdiv,.imageclick").addClass("settodefault");
           setTimeout(function(){
               sound_player(soundId,true);
           },1000);
        }
    }



    function showname() {
        $(".subimg").hide();
        $(".imageclick").hover(function(){
        $(this).css("cursor","pointer");
          var classname =   $(this).attr("class").toString().split(" ").pop();
          $("."+classname).show();
        });
        $(".imageclick").mouseout(function(){
            $(".subimg").hide();
        })
    }
    function put_image(content, count) {
        var contentCount = content[count];
        var imageblockcontent = contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent, contentCount)
    }

    function put_image2(content, count) {
        if (content[count].hasOwnProperty("imgwithdiv")) {
            console.log("Hello I am here");
            var contentCount = content[count].imgwithdiv[0];
            var imageblockcontent = contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent, contentCount);

            var contentCount1 = content[count].imgwithdiv[1];
            var imageblockcontent1 = contentCount1.hasOwnProperty('imageblock');
            console.log("image " + imageblockcontent1);
            dynamicimageload(imageblockcontent1, contentCount1);
            if(contentCount1.hasOwnProperty("extradiv")){
                console.log("This is test");
                var contentCount2 = contentCount1.extradiv[0];
                var imageblockcontent2 = contentCount2.hasOwnProperty('imageblock');
                dynamicimageload(imageblockcontent2, contentCount2);

            }
        }
    }

    function dynamicimageload(imageblockcontent, contentCount) {
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "covepage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.male,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls zoomInEffect boyimg",
                    imgid: 'boyImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q1
            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.female,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls zoomInEffect girlimg",
                    imgid: 'girlImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q2
            }
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.male,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "kaka",
                    imgclass: "relativecls zoomInEffect kakaimg",
                    imgid: 'kakaImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q3
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.female,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "kaka",
                    imgclass: "relativecls zoomInEffect kakiimg",
                    imgid: 'kakiImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q4
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.female,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls zoomInEffect ramaimg",
                    imgid: 'ramaImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q5
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        extrawrappingdiv:true,
        uppertextblockadditionalclass:"topic",
        ans:data.string.male,
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p1text1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls zoomInEffect umeshimg",
                    imgid: 'umeshImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"commondiv boydiv",
                textclass:"content centertext",
                textdata:data.string.male
            },
            {
                textdiv:"commondiv girldiv",
                textclass:"content centertext",
                textdata:data.string.female
            },
            {
                textdiv:"questiondiv fadeInEffect",
                textclass:"content centertext",
                textdata:data.string.p1q6
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblockadditionalclass:"topic1",
        uppertextblock: [
            {
                textclass: "content centertext slideR",
                textdata: data.string.p1text2,
            }

        ],
        listedblock:[
            {
                textdiv:"commondiv1 boydiv1 fadeInEffect",
                title:data.string.male,
                titleclass:"content gender",
                textdata:[
                    {
                        contenttext:data.string.bhai,
                    },
                    {
                        contenttext:data.string.kaka,
                    },
                    {
                        contenttext:data.string.umesh,
                    }
                ]
            },
            {
                textdiv:"commondiv1 girldiv1 fadeInEffect",
                title:data.string.female,
                titleclass:"content gender",
                textdata:[
                    {
                        contenttext:data.string.bahini,
                    },
                    {
                        contenttext:data.string.kaki,
                    },
                    {
                        contenttext:data.string.rama,
                    }
                ]
            },
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblockadditionalclass:"topic1",
        uppertextblock: [
            {
                textclass: "content centertext slideL",
                textdata: data.string.p1text5,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "hand hand1 divclick fadeInEffect",
                    imgclass: "relativecls handimg",
                    imgid: 'handImg',
                    imgsrc: ""
                },
            ]
        }],
        listedblock:[
            {
                textdiv:"commondiv1 boydiv1 divclick",
                title:data.string.male,
                titleclass:"content gender",
                textdata:[
                    {
                        contenttext:data.string.bhai,
                    },
                    {
                        contenttext:data.string.kaka,
                    },
                    {
                        contenttext:data.string.umesh,
                    }
                ],

            },
            {
                textdiv:"commondiv1 girldiv1 divclick",
                title:data.string.female,
                titleclass:"content gender",
                textdata:[
                    {
                        contenttext:data.string.bahini,
                    },
                    {
                        contenttext:data.string.kaki,
                    },
                    {
                        contenttext:data.string.rama,
                    }
                ],

            },
        ],
        afterClick:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textclr",
                textdiv:"changediv div1",
                textclass:"subtopic centertext",
                textdata:data.string.p1text3
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textclr",
                textdiv:"changediv div2",
                textclass:"subtopic centertext",
                textdata:data.string.p1text4
            }

        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "boyImg", src: imgpath+"boy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlImg", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "kakaImg", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
            {id: "kakiImg", src: imgpath+"girl02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ramaImg", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ramaImg", src: imgpath+"girl03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "umeshImg", src: imgpath+"boy03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_ques", src: soundAsset+"ex.ogg"},
            {id: "sound_0", src: soundAsset+"p1_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p1_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p1_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p1_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p1_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p1_s5.ogg"},
            {id: "sound_6", src: soundAsset+"p1_s6.ogg"},
            {id: "sound_7", src: soundAsset+"p1_s7.ogg"},
            {id: "sound_8_click", src: soundAsset+"p1_s8_click.ogg"},
            {id: "sound_8_1", src: soundAsset+"p1_s8_1.ogg"},
            {id: "sound_8_2", src: soundAsset+"p1_s8_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_ques",false);
                current_sound.on('complete', function () {
                    sound_player("sound_1", false);
                });
                shufflehint();
                checkans();
                break;
            case 2:
                sound_player("sound_2",false);
                shufflehint();
                checkans();
                break;
            case 3:
                sound_player("sound_3",false);
                shufflehint();
                checkans();
                break;
            case 4:
                sound_player("sound_4",false);
                shufflehint();
                checkans();
                break;
            case 5:
                sound_player("sound_5",false);
                shufflehint();
                checkans();
                break;
            case 6:
                sound_player("sound_6",false);
                shufflehint();
                checkans();
                break;
            case 7:
                sound_player("sound_7",true);
                break;
            case 8:
                sound_player("sound_8_click",false);
                clickdiv();
                break;
            default:

                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optiondiv = $(".extradivwraaper");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find(".commondiv").eq(Math.random() * i | 0));
        }
        optiondiv.find(".commondiv").removeClass().addClass("current");
        var a = ["commondiv boydiv","commondiv girldiv"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".commondiv").removeClass("current");


    }
    function checkans(){
        $(".commondiv ").on("click",function () {
            createjs.Sound.stop();
            if($(this).find('p').text().trim()==$(".topic").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".commondiv").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

    function clickdiv(){
        $(".div1,.div2").hide();
        $(".divclick").click(function(){
            if($(this).hasClass("boydiv1")||$(this).hasClass("hand1")) {
                $(".boydiv1").empty();
                $(".div1").show();
                $(".hand").hide();
                sound_player("sound_8_1",false,"hand");
                setTimeout(function(){
                    $(".hand").show().addClass("hand2");
                },10000);
            }
            else if($(this).hasClass("girldiv1")||$(this).hasClass("hand2")) {
                $(".girldiv1").empty();
                $(".hand").remove();
                $(".div2").show();
                sound_player("sound_8_2",true);
            }
        });
    }
});

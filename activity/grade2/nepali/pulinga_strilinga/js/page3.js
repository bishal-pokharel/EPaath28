var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textclr",
                textclass: "newfont",
                textdata: data.string.p3text1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textdesign",
                textclass: "newfont secondptag fadeInEffect",
                textdata: data.string.p3text2
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boyeating",
                    imgclass: "relativecls boyeatingimg",
                    imgid: 'boyeatingImg',
                    imgsrc: ""
                },
                // {
                //     imgdiv: "arrow",
                //     imgclass: "relativecls arrowimg",
                //     imgid: 'arrrowImg',
                //     imgsrc: ""
                // }
            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textclr",
                textclass: "content centertext",
                textdata: data.string.p3text3,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyeatingImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl fstgrlRght",
                    imgclass: "relativecls girlimg",
                    imgid: 'girleatingImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv boydiv",
                textclass:"content centertext zoomInEffect",
                textdata:data.string.p3text4
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv girldiv",
                textclass:"grlTpTXt tp20",
                textdata:data.string.p3text5
            }
        ]
    },

//slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy boy1 boyLft lftImgAnim",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyfootballImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl girl1 fstgrlRght",
                    imgclass: "relativecls girlimg",
                    imgid: 'girlfootballImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv boydiv1",
                textclass:"boyTpTxt tp-11 topTxtLeftAnim",
                textdata:data.string.p3text6
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv girldiv1",
                textclass:"grlTpTXt tp-11",
                textdata:data.string.p3text7
            }
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "boy boy1 boyLft lftImgAnim",
                    imgclass: "relativecls boyimg",
                    imgid: 'boyrunningImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "girl girl1 fstgrlRght",
                    imgclass: "relativecls girlimg",
                    imgid: 'girlrunningImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv boydiv1",
                textclass:"boyTpTxt tp-11 topTxtLeftAnim",
                textdata:data.string.p3text8
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "subtextclr",
                textdiv:"commondiv girldiv1",
                textclass:"grlTpTXt tp-11",
                textdata:data.string.p3text9
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        textblock:[
            {
                textdiv:"diydiv",
                textclass:"chapter centertext",
                textdata:data.string.diy
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "diydiv1",
                    imgclass: "relativecls img1",
                    imgid: 'diyImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text11
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls img1",
                    imgid: 'girlreadingImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p3text12,
                ans:data.string.ans1
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text11
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls img1",
                    imgid: 'boyfootballImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p3text13,
                ans:data.string.ans2
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgdiy",
        uppertextblockadditionalclass:"titletext",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text11
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv id1",
                    imgclass: "relativecls img1",
                    imgid: 'girl05Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "option",
                textdiv:"popupdiv",
                textclass:"content centertext",
                textdata:data.string.p3text14,
                ans:data.string.ans3
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "boyeatingImg", src: imgpath+"boy-eating.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girleatingImg", src: imgpath+"girl_eating.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyfootballImg", src: imgpath+"boy02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlfootballImg", src: imgpath+"girl_football.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyrunningImg", src: imgpath+"boy_running.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlrunningImg", src: imgpath+"girl_running.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlreadingImg", src: imgpath+"girl_reading.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girl05Img", src: imgpath+"girl05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrrowImg", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "diyImg", src: imgpath+"bg_diy01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p3_s0.ogg"},
            {id: "sound_1_1", src: soundAsset+"p3_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p3_s1_2.ogg"},
            {id: "sound_2_1", src: soundAsset+"p3_s2-01.ogg"},
            {id: "sound_2_2", src: soundAsset+"p3_s2-02.ogg"},
            {id: "sound_3_01", src: soundAsset+"p3_s3_01.ogg"},
            {id: "sound_3_02", src: soundAsset+"p3_s3_02.ogg"},
            {id: "sound_3", src: soundAsset+"p3_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p3_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p3_s5.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
                sound_player("sound_0",true);
                animateArrow();
               break;
            case 1:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_1_1");
                current_sound.play();
                current_sound.on('complete',function(){
                  $(".grlTpTXt").addClass("topTxtRghtAnim");
                  $(".fstgrlRght").addClass("rightImgAnim");
                  setTimeout(function(){
                    sound_player("sound_1_2",true);
                  },1000);
                });
                break;
            case 2:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_2_1");
                current_sound.play();
                current_sound.on('complete',function(){
                  $(".grlTpTXt").addClass("topTxtRghtAnim");
                  $(".fstgrlRght").addClass("rightImgAnim");
                  setTimeout(function(){
                    sound_player("sound_2_2",true);
                  },1000);
                });
                break;
            case 3:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("sound_3_01");
                current_sound.play();
                current_sound.on('complete',function(){
                  $(".grlTpTXt").addClass("topTxtRghtAnim");
                  $(".fstgrlRght").addClass("rightImgAnim");
                  setTimeout(function(){
                    sound_player("sound_3_02",true);
                  },1000);
                });
                break;
            case 4:
                sound_player("sound_4",true);
                break;
            case 5:
                sound_player("sound_5",true);
                checkans();
                break;
            case 6:
                checkans();
                break;
            case 7:
                checkans();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function animateArrow() {
        $(".arrow").hide();
        setTimeout(function(){
            $(".textdesign").addClass("textdesign1", 500, "linear");
            setTimeout(function() {
                $(".arrow").show().addClass("zoomInEffect");
            },1000);
        },8000);
    }
    function checkans(){
        $(".option").eq(1).removeClass("option").addClass("slash");
        $(".option ").on("click",function () {
            createjs.Sound.stop();
            if($(this).text().trim()==$(".popupdiv").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                navigationcontroller(countNext,$total_page);
                $(".slash").remove();
                $(".option").not($(this)).remove();
            }
            else{
                $(this).addClass("wrongans");
                $(this).addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

    function clickdiv(){
        $(".div1,.div2").hide();
        $(".divclick").click(function(){
            if($(this).hasClass("boydiv1")||$(this).hasClass("hand1")) {
                $(".boydiv1").empty();
                $(".hand").addClass("hand2");
                $(".div1").show();

            }
            else if($(this).hasClass("girldiv1")||$(this).hasClass("hand2")) {
                $(".girldiv1").empty();
                $(".hand").remove();
                $(".div2").show();
                navigationcontroller(countNext,$total_page);

            }
        });
    }
});

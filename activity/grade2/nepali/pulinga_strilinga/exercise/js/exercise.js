var imgpath = $ref + "/images/images_for_exercise/";
var soundAsset = $ref+"/sounds/";
var content=[
    //slide 0
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q1opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls img",
                    imgid: 'q1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
               textdiv:"question beforeclick",
               textclass:"subtopic centertext",
               textdata:data.string.q1a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q1a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q1opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q1opt2
            }
        ]
    },
    //slide 1
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q2opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q2img",
                    imgid: 'q2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q2a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q2a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q2opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q2opt2
            }
        ]
    },
    //slide 2
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q3opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q3img",
                    imgid: 'q3Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q3a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q3a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q3opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q3opt2
            }
        ]
    },
    //slide 3
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q4opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q4img",
                    imgid: 'q4Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q4a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q4a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q4opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q4opt2
            }
        ]
    },
    //slide 4
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q5opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q5img",
                    imgid: 'q5Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q5a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q5a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q5opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q5opt2
            }
        ]
    },
    //slide 5
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q6opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q6img",
                    imgid: 'q6Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q6a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q6a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q6opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q6opt2
            }
        ]
    },
    //slide 6
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q7opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q7img",
                    imgid: 'q7Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q7a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q7a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q7opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q7opt2
            }
        ]
    },
    //slide 7
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q8opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q8img",
                    imgid: 'q8Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q8a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q8a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q8opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q8opt2
            }
        ]
    },
    //slide 8
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q9opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q9img",
                    imgid: 'q9Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q9a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q9a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q9opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q9opt2
            }
        ]
    },
    //slide 9
    {
        contentblockadditionalclass:'',
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"mainquery",
        ans:data.string.q10opt1,
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.q1,
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "img1",
                    imgclass: "relativecls q10img",
                    imgid: 'q10Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"question beforeclick",
                textclass:"subtopic centertext",
                textdata:data.string.q10a1
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "blank",
                textdiv:"question afterclick",
                textclass:"subtopic centertext",
                textdata:data.string.q10a2
            },
            {
                textdiv:"commonbtn option1",
                textclass:"subtopic centertext opt1",
                textdata:data.string.q10opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass:"subtopic centertext opt2",
                textdata:data.string.q10opt2
            }
        ]
    },
];
// content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var score = new NumberTemplate();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "q1Img", src: imgpath+"q01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q2Img", src: imgpath+"q02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3Img", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q4Img", src: imgpath+"q04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q5Img", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q6Img", src: imgpath+"q06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q7Img", src: imgpath+"q07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q8Img", src: imgpath+"q08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q9Img", src: imgpath+"q09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q10Img", src: imgpath+"q10.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"ex1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        score.init(content.length);
        templateCaller();
    }

    //initialize
    init();



    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        put_image(content, countNext,preload);
        score.numberOfQuestions();
        switch(countNext){
            default:
                countNext==0?sound_player("sound_1",false):"";
                $(".afterclick").hide();
                shufflehint();
                checkans();
                break;
        }
    }
    function put_image(content, count,preload) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload)
    }

    function dynamicimageload(imageblockcontent,contentCount,preload){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate?navigationcontroller(countNext,$total_page):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint() {
        var optiondiv = $(".bg");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find(".commonbtn").eq(Math.random() * i | 0));
        }
        optiondiv.find(".commonbtn").removeClass().addClass("current");
        var a = ["commonbtn option1","commonbtn option2"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".commonbtn").removeClass("current");


    }
    function checkans(){
        var wrongclick = false;
        $(".commonbtn ").on("click",function () {
            createjs.Sound.stop();
            if($(this).find('p').text().trim()==$(".mainquery").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                !wrongclick?score.update(true):'';
                navigationcontroller();
                $(".beforeClick").hide();
                $(".afterClick").show();
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
                score.update(false);
                wrongclick = true;
            }
        });
    }

});

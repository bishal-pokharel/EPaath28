var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var soundcontent;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass: "description_title",
			textdata: data.string.lesson_title
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'start_end_page',
			}]
		}]
	},{
		// slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: "darktolight",
		uppertextblockadditionalclass: "utb pos_left",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.p1_s0
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'p1_s1_bg01',
			},{
				imgclass: "deer",
				imgsrc: "",
				imgid : 'p1_s1_deer',
			},{
				imgclass: "grass02",
				imgsrc: "",
				imgid : 'p1_s1_grass02',
			},{
				imgclass: "tiger",
				imgsrc: "",
				imgid : 'p1_s1_tiger',
			},{
				imgclass: "grass01",
				imgsrc: "",
				imgid : 'p1_s1_grass01',
			},{
				imgclass: "lion",
				imgsrc: "",
				imgid : 'p1_s1_lion',
			},{
				imgclass: "bear",
				imgsrc: "",
				imgid : 'p1_s1_bear',
			},{
				imgclass: "monkey",
				imgsrc: "",
				imgid : 'p1_s1_monkey',
			}
			,{
				imgclass: "kachuwa",
				imgsrc: "",
				imgid : 'p1_s1_kachuwa',
			},{
				imgclass: "rabit",
				imgsrc: "",
				imgid : 'p1_s1_rabit',
			}
			]
		}]
		// ,
		// svgblock:[{
		// 	svgblock: 'p1_s1_kachuwa'
		// },{
		// 	svgblock: 'p1_s1_rabit'
		// }]

	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb pos_center",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.p1_s1
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'p1_s2',
			}]
		}]

	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb pos_center",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.p1_s2
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'p1_s3',
			}]
		}]

	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb pos_center",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.p1_s3
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'p1_s4',
			}]
		}]

	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb pos_center",
		uppertextblock: [{
			textclass: "title",
			textdata: data.string.p1_s4
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass: "background-img",
				imgsrc: "",
				imgid : 'p1_s4',
			}]
		}]

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "start_end_page", src: imgpath+"start_end_page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_bear", src: imgpath+"p1_s1_bear.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_bg01", src: imgpath+"p1_s1_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_deer", src: imgpath+"p1_s1_deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_grass01", src: imgpath+"p1_s1_grass01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_grass02", src: imgpath+"p1_s1_grass02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_kachuwa", src: imgpath+"p1_s1_kachuwa.png", type: createjs.AbstractLoader.IMAGE},
			//{id: "p1_s1_kachuwa", src: imgpath+"kachuwa.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_lion", src: imgpath+"p1_s1_lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_monkey", src: imgpath+"p1_s1_monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_rabit", src: imgpath+"p1_s1_rabit.png", type: createjs.AbstractLoader.IMAGE},
			//{id: "p1_s1_rabit", src: imgpath+"kharayo.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s1_tiger", src: imgpath+"p1_s1_tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s2", src: imgpath+"p1_s2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s3", src: imgpath+"p1_s3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p1_s4", src: imgpath+"p1_s4.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"}

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
	var timeourcontroller;
	var index = 0;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    vocabcontroller.findwords(countNext);
	    put_image(content, countNext);
	    (countNext > 0)?$prevBtn.show(0): true;
		// splitintofractions($(".fractionblock"));
	    switch(countNext){
	    	case 0:
	    		sound_player("sound_"+countNext);
	    	 	break;
	    	case 1:
	    		timeourcontroller = setTimeout(function(){
					sound_player("sound_"+countNext);
					$(".tiger").delay(1500).show(0);
		    		$(".lion").delay(2500).show(0);
		    		$(".bear").delay(3600).show(0);
		    		$(".deer").delay(4700).show(0);
		    		$(".monkey").delay(5800).show(0);
		    		$(".kachuwa").delay(7800).show(0);
		    		$(".rabit").delay(9000).show(0);

			    		/*var s = Snap('#p1_s1_kachuwa');
					var s2 = Snap('#p1_s1_rabit');
			    		var lefteye;
			    		var righteye;
			    		var lefteye_close;
			    		var righteye_close;
					var svg = Snap.load(preload.getResult('p1_s1_kachuwa').src, function ( loadedFragment ) {
						s.append(loadedFragment);
						// lefteye = Snap.select("#openeye01");
			    			// righteye = Snap.select("#openeye02");
			    			// lefteye_close = Snap.select("#closeeye01");
			    			// righteye_close = Snap.select("#closeeye02");
			    			lefteye = $("#openeye01");
			    			righteye = $("#openeye02");
			    			lefteye_close = $("#closeeye01");
			    			righteye_close = $("#closeeye02");

			    			function recursivefunction(){
			    				if(countNext == 1){
			    					if(index%2 == 0){
			    						lefteye_close.show(0);
			    						righteye_close.show(0);
			    						index++;
			    						setTimeout(function(){
				    						recursivefunction();
			    						},300);
			    					} else {
			    						lefteye_close.hide(0);
			    						righteye_close.hide(0);
			    						index++;
			    						setTimeout(function(){
				    						recursivefunction();
			    						},2000);
			    					}
			    				}
			    			}

		    				recursivefunction();

					} );
					var svg = Snap.load(preload.getResult('p1_s1_rabit').src, function ( loadedFragment ) {
						s2.append(loadedFragment);
					} );*/
		    	}, 2000);
	    		$(".textblock").css({"top": "17%", "text-align": "left",});
	    		break;
	    	case 2:
	    		sound_player("sound_"+countNext);
	    		$(".textblock").css("top", "15%");
	    		break;
	    	case 5:
	    		timeourcontroller = setTimeout(function(){
		    		sound_player("sound_"+countNext);
		    		$(".textblock").show(0);
	    		}, 2300);
	    		$(".textblock").css({"top": "15%", "width": "50%", "left": "25%"}).hide(0);
	    		$(".background-img").addClass("zoom_in");
	    		break;
	    	case 4:
	    		sound_player("sound_"+countNext);
	    		$(".textblock").css({"top": "15%", "width": "50%", "left": "25%"});
	    		break;
	    	case 3:
	    		sound_player("sound_"+countNext);
	    		break;
	    	default:
	    		break;
	    }
	}



/*=====  End of Templates Block  ======*/
function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if($total_page > (countNext+1)){
				$nextBtn.show(0);
			}else{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
	}

function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

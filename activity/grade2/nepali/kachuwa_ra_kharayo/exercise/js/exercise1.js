var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
	//slide 0
	{
				bgimage: imgpath+ "01.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e1_q,
				optionsblock: [{
						option: "class2",
						optiondata: data.string.e1_2,
						imgsrc: "images/wrong.png"
					},{	
						option: "class1",
						optiondata: data.string.e1_1,
						imgsrc: "images/correct.png"
					}],
			}
		]
	},
	//slide 1
	{
				bgimage: imgpath+ "04.png",
				position: "ex1_pos4",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e2_q,
				optionsblock: [
					{
						option: "class1",
						optiondata: data.string.e2_1,
						imgsrc: "images/correct.png"
					},
					{
						option: "class2",
						optiondata: data.string.e2_2,
						imgsrc: "images/wrong.png"
					}],
			}
		]
	},
	//slide 2
	{
				bgimage: imgpath+ "friendly.png",
				position: "ex1_pos3",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e3_q,
				optionsblock: [
					{
						option: "class1",
						optiondata: data.string.e3_1,
						imgsrc: "images/correct.png"
					},
					{
						option: "class2",
						optiondata: data.string.e3_2,
						imgsrc: "images/wrong.png"
					}],
			}
		]
	},
	//slide 3
	{
				bgimage: imgpath+ "sleep.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e4_q,
				optionsblock: [{
						option: "class2",
						optiondata: data.string.e4_2,
						imgsrc: "images/wrong.png"
				},{
						option: "class1",
						optiondata: data.string.e4_1,
						imgsrc: "images/correct.png"
				}],
			}
		]
	},
	//slide 4
	{
				bgimage: imgpath+ "03.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e5_q,
				optionsblock: [{
						option: "class2",
						optiondata: data.string.e5_2,
						imgsrc: "images/wrong.png"
				},{
						option: "class1",
						optiondata: data.string.e5_1,
						imgsrc: "images/correct.png"
				}],
			}
		]
	},
	//slide 5
	{
				bgimage: imgpath+ "riverbank.png",
				position: "ex1_pos2",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e6_q,
				optionsblock: [
					{
						option: "class1",
						optiondata: data.string.e6_1,
						imgsrc: "images/correct.png"
					},
					{
						option: "class2",
						optiondata: data.string.e6_2,
						imgsrc: "images/wrong.png"
					}],
			}
		]
	},
	//slide 6
	{
				bgimage: imgpath+ "winner.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e7_q,
				optionsblock: [
					{
						option: "class1",
						optiondata: data.string.e7_1,
						imgsrc: "images/correct.png"
					},
					{
						option: "class2",
						optiondata: data.string.e7_2,
						imgsrc: "images/wrong.png"
					}],
			}
		]
	},
	//slide 7
	{
				bgimage: imgpath+ "p3_s8.png",
				position: "ex1_pos2",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e8_q,
				optionsblock: [{
						option: "class2",
						optiondata: data.string.e8_2,
						imgsrc: "images/wrong.png"
					},{
						option: "class1",
						optiondata: data.string.e8_1,
						imgsrc: "images/correct.png"
					}],
			}
		]
	},
	//slide 8
	{
				bgimage: imgpath+ "start_end_page.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e9_q,
				optionsblock: [
					{
						option: "class1",
						optiondata: data.string.e9_1,
						imgsrc: "images/correct.png"
					},
					{
						option: "class2",
						optiondata: data.string.e9_2,
						imgsrc: "images/wrong.png"
					}],
			}
		]
	},
	//slide 9
	{
				bgimage: imgpath+ "start_end_page.png",
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.ex_ins_1,
				questiondata: data.string.e10_q,
				optionsblock: [{
						option: "class2",
						optiondata: data.string.e10_2,
						imgsrc: "images/wrong.png"
					},
					{
						option: "class1",
						optiondata: data.string.e10_1,
						imgsrc: "images/correct.png"
					}],
			}
		]
	},

];
content.shufflearray();

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();
  	
  	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "p3_s0", src: imgpath+"p3_s0.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boubble01", src: imgpath+"boubble01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boubble02", src: imgpath+"boubble02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble03", src: imgpath+"bubble03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s1", src: imgpath+"p3_s1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s2", src: imgpath+"p2_s2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s3", src: imgpath+"p3_s3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s4", src: imgpath+"p3_s4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s5", src: imgpath+"p3_s5.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s6", src: imgpath+"p3_s6.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s7", src: imgpath+"p3_s7.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s8", src: imgpath+"p3_s8.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s9", src: imgpath+"p3_s9.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s10", src: imgpath+"p3_s10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "p3_s11", src: imgpath+"start_end_page.png", type: createjs.AbstractLoader.IMAGE},
		//	sounds
            {id: "sound_1", src: soundAsset+"ex.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		var exercise = new template_exercise_mcq_monkey(content, testin, true);
		exercise.create_exercise();
	}
	//initialize
	init();
	
});
function sound_player(sound_id){
    createjs.Sound.stop();
     current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
}
function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $('#activity-page-next-btn-enabled');
	var $prevBtn		= $('#activity-page-prev-btn-enabled');
	var countNext		= 0;
	var testin			= new EggTemplate();
   
	var wrong_clicked 	= false;

	var total_page = content.length;

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		countNext==0?sound_player("sound_1",false):"";
		var $exercise_no = $(".ex-number-template-score"); 
		if($(".exerciseblock").data("positionflag") != ""){
			$(".ex-number-template-score").addClass($(".exerciseblock").data("positionflag"));
		} else {
			$exercise_no.removeAttr("class");
			$exercise_no.addClass("ex-number-template-score");
		}
		// testin.numberOfQuestions();

		/*for randomizing the options*/
		var option_position = [3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}

		var wrong_clicked = 0;
		var answered = false;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".class1, .class2").click(function(){
			if(answered)
				return answered;
			$(this).find("img").show(0);
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				current_sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct');
				$(".class2").addClass('disable');
				wrong_clicked = 0;
				answered = true;
				if(countNext != total_page)
					$nextBtn.show(0);
			}
			else{
				testin.update(false);
                current_sound.stop();
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect');
				wrong_clicked++;
			}
		});
	};


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	};

	this.create_exercise = function(){
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

imageAsset = $ref+"/images/";
soundAsset = $ref + "/sounds/";


var dialog00  = new buzz.sound(soundAsset + "famtree.ogg");
var dialog0  = new buzz.sound(soundAsset + "p1_s0.ogg");
var dialog1  = new buzz.sound(soundAsset + "sound1.ogg");
var dialog2  = new buzz.sound(soundAsset + "sound2.ogg");
var dialog3  = new buzz.sound(soundAsset + "sound3.ogg");
var dialog4  = new buzz.sound(soundAsset + "sound4.ogg");
var dialog5  = new buzz.sound(soundAsset + "sound5.ogg");
var dialog6  = new buzz.sound(soundAsset + "sound6.ogg");
var dialog7  = new buzz.sound(soundAsset + "sound7.ogg");
var dialog8  = new buzz.sound(soundAsset + "sound8.ogg");
var dialog9  = new buzz.sound(soundAsset + "sound9.ogg");
var dialog10 = new buzz.sound(soundAsset + "sound10.ogg");
var dialog11 = new buzz.sound(soundAsset + "sound11.ogg");
var dialog12 = new buzz.sound(soundAsset + "sound12.ogg");


//This will have all the sound 
var soundcontent = [dialog1, dialog2, dialog3, dialog4, dialog5, dialog6,
                    dialog7, dialog8, dialog9, dialog10, dialog11, dialog12,dialog0];

var content=[
  // Slide 0
    {
        textblock:[{
            textclass:"titletext",
            textdata:data.lesson.chapter
        }
        ],
        imageblock: [
            {
                mainimg: [
                    {
                        imgsrc: imageAsset + "cover_page.png",
                        imgclass: "img coverpage"
                    }
                ],
            }
        ]
    },
    // slide 1
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
      	mainimg: [
          {
        		imgsrc: imageAsset + "meMainIntr.png",
        		imgclass: "img slideimg1"
        	},
        	{
        		imgsrc: imageAsset + "my_family-01.png",
        		imgclass: "img slideimg2"
        	},
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
        ], 
      }   
    ],

    definitionblock: [{
  		definitiondata: data.string.intro1,
  		definitionclass: "intro1"
  	}],
  },

  // Slide 2
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [
          {
            imgsrc: imageAsset + "meMainIntr.png",
            imgclass: "img slideimg1 "
          },
          {
            imgsrc: imageAsset + "my_family-01.png",
            imgclass: "img slideimg2"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
        ],
      }   
    ],

    definitionblock: [{
      definitiondata: data.string.intro2,
      definitionclass: "intro2"
    }],
  },

  // Slide 3
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },

         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          }
         ], 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.grandfather,
      definitionclass: "family-intro family-intro1"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    }]
  },

  // Slide 4
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          }
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          }
         ], 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.grandfather,
      definitionclass: "family-intro family-intro1"
    },{
      definitiondata: data.string.grandmother,
      definitionclass: "family-intro family-intro2"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    }]
  },


  // Slide 5
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          },
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          }
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.father,
      definitionclass: "family-intro family-intro3"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    }]
  },

  // Slide 6
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          }
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          }
         ] 
      }
    ],

    definitionblock: [{
       definitiondata: data.string.mother,
       definitionclass: "family-intro family-intro4"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    }]
  },
    
  // Slide 7
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          }
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
         ] 
      }
    ],

    definitionblock: [ {
        definitiondata: data.string.aunt,
        definitionclass: "family-intro family-intro5"
      }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    }]
  },


// Slide 8
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          }
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
          {
            imgspriteclass: "family uncle"
          },
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.uncle,
      definitionclass: "family-intro family-intro6"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    },{
      textdata: data.string.uncle1,
      textclass: "relation uncle1"
    }]
  },

  // Slide 9
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node2"
          },
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
          {
            imgspriteclass: "family uncle"
          },
          {
            imgspriteclass: "family me"
          },
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.myself,
      definitionclass: "family-intro family-intro7"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    },{
      textdata: data.string.uncle1,
      textclass: "relation uncle1"
    },{
      textdata: data.string.myself1,
      textclass: "relation myself1"
    }]
  },

  // Slide 10
  {
    uppertextblock: [
      {
        textdata: data.string.headtext1,
        textclass: "head-text"
      }
    ],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node2"
          },
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
          {
            imgspriteclass: "family uncle"
          },
          {
            imgspriteclass: "family me"
          },
          {
            imgspriteclass: "family mybro"
          },
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.mybro,
      definitionclass: "family-intro family-intro8"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    },{
      textdata: data.string.mybro1,
      textclass: "relation mybro1"
    },{
      textdata: data.string.uncle1,
      textclass: "relation uncle1"
    },{
      textdata: data.string.myself1,
      textclass: "relation myself1"
    }]
  },

  // Slide 11
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node3"
          },
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
          {
            imgspriteclass: "family uncle"
          },
          {
            imgspriteclass: "family me"
          },
          {
            imgspriteclass: "family mybro"
          },
          {
            imgspriteclass: "family cousinbro"
          },
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.coubrother,
      definitionclass: "family-intro family-intro9"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    },{
      textdata: data.string.myself1,
      textclass: "relation myself1"
    },{
      textdata: data.string.mybro1,
      textclass: "relation mybro1"
    },{
      textdata: data.string.uncle1,
      textclass: "relation uncle1"
    },{
      textdata: data.string.coubrother1,
      textclass: "relation cousinbro1"
    }]
  },

  // Slide 12
  {
    uppertextblock: [{
      textdata: data.string.headtext1,
      textclass: "head-text"
    }],

    imageblock: [
      {
        mainimg: [    
          { 
            imgsrc: imageAsset + "meMainExp.png",
            imgclass: "img slideimg1"
          },
          {
            imgsrc: imageAsset + "textbox/textbox_purple.png",
            imgclass: "dialog-box dialog-box2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node1"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node2"
          },
          {
            imgsrc: imageAsset + "node.png",
            imgclass: "node node3"
          },
         ],
         
         spriteimg: [
          {
            imgspriteclass: "family grandfather"
          },
          {
            imgspriteclass: "family grandmother"
          },
          {
            imgspriteclass: "family father"
          },
          {
            imgspriteclass: "family mother"
          },
          {
            imgspriteclass: "family aunt"
          },
          {
            imgspriteclass: "family uncle"
          },
          {
            imgspriteclass: "family me"
          },
          {
            imgspriteclass: "family mybro"
          },
          {
            imgspriteclass: "family cousinbro"
          },
          {
            imgspriteclass: "family cousinsis"
          },
         ] 
      }
    ],

    definitionblock: [{
      definitiondata: data.string.cousister,
      definitionclass: "family-intro family-intro10"
    }],

    textblock: [{
      textdata: data.string.grandfather1,
      textclass: "relation grandfather1"
    },{
      textdata: data.string.grandmother1,
      textclass: "relation grandmother1"
    },{
      textdata: data.string.father1,
      textclass: "relation father1"
    },{
      textdata: data.string.mother1,
      textclass: "relation mother1"
    },{
      textdata: data.string.aunt1,
      textclass: "relation aunt1"
    },{
      textdata: data.string.uncle1,
      textclass: "relation uncle1"
    },{
      textdata: data.string.myself1,
      textclass: "relation myself1"
    },{
      textdata: data.string.mybro1,
      textclass: "relation mybro1"
    },{
      textdata: data.string.coubrother1,
      textclass: "relation cousinbro1"
    },{
      textdata: data.string.cousister1,
      textclass: "relation cousinsis1"
    }]
  }

  // // Slide 13
  // {
  //   uppertextblock: [{
  //     textdata: data.string.headtext1,
  //     textclass: "head-text"
  //   }],
  //
  //   imageblock: [
  //     {
  //       mainimg: [
  //         {
  //           imgsrc: imageAsset + "meMainExp.png",
  //           imgclass: "img slideimg1"
  //         },
  //         {
  //           imgsrc: imageAsset + "textbox/textbox_purple.png",
  //           imgclass: "dialog-box dialog-box2"
  //         },
  //         {
  //           imgsrc: imageAsset + "node.png",
  //           imgclass: "node node1"
  //         },
  //         {
  //           imgsrc: imageAsset + "node.png",
  //           imgclass: "node node2"
  //         },
  //         {
  //           imgsrc: imageAsset + "node.png",
  //           imgclass: "node node3"
  //         },
  //        ],
  //
  //        spriteimg: [
  //         {
  //           imgspriteclass: "family grandfather"
  //         },
  //         {
  //           imgspriteclass: "family grandmother"
  //         },
  //         {
  //           imgspriteclass: "family father"
  //         },
  //         {
  //           imgspriteclass: "family mother"
  //         },
  //         {
  //           imgspriteclass: "family uncle"
  //         },
  //         {
  //           imgspriteclass: "family aunt"
  //         },
  //         {
  //           imgspriteclass: "family mybro"
  //         },
  //         {
  //           imgspriteclass: "family me"
  //         },
  //         {
  //           imgspriteclass: "family cousinbro"
  //         },
  //         {
  //           imgspriteclass: "family cousinsis"
  //         }
  //       ]
  //     }
  //   ],
  //
  //   definitionblock: [{
  //     definitiondata: data.string.grandfather,
  //     definitionclass: "family-intro family-intro1"
  //   },{
  //     definitiondata: data.string.grandmother,
  //     definitionclass: "family-intro family-intro2"
  //   },{
  //     definitiondata: data.string.father,
  //     definitionclass: "family-intro family-intro3"
  //   },{
  //     definitiondata: data.string.mother,
  //     definitionclass: "family-intro family-intro4"
  //   },{
  //     definitiondata: data.string.uncle,
  //     definitionclass: "family-intro family-intro5"
  //   },{
  //     definitiondata: data.string.aunt,
  //     definitionclass: "family-intro family-intro6"
  //   },{
  //     definitiondata: data.string.myself,
  //     definitionclass: "family-intro family-intro7"
  //   },{
  //     definitiondata: data.string.mybro,
  //     definitionclass: "family-intro family-intro8"
  //   },{
  //     definitiondata: data.string.coubrother,
  //     definitionclass: "family-intro family-intro9"
  //   },{
  //     definitiondata: data.string.cousister,
  //     definitionclass: "family-intro family-intro10"
  //   },{
  //     definitiondata: data.string.listenagain,
  //     definitionclass: "family-intro listenagain"
  //   }],
  //
  //   textblock: [{
  //     textdata: data.string.grandfather1,
  //     textclass: "relation grandfather1"
  //   },{
  //     textdata: data.string.grandmother1,
  //     textclass: "relation grandmother1"
  //   },{
  //     textdata: data.string.father1,
  //     textclass: "relation father1"
  //   },{
  //     textdata: data.string.mother1,
  //     textclass: "relation mother1"
  //   },{
  //     textdata: data.string.aunt1,
  //     textclass: "relation aunt1"
  //   },{
  //     textdata: data.string.uncle1,
  //     textclass: "relation uncle1"
  //   },{
  //     textdata: data.string.myself1,
  //     textclass: "relation myself1"
  //   },{
  //     textdata: data.string.mybro1,
  //     textclass: "relation mybro1"
  //   },{
  //     textdata: data.string.coubrother1,
  //     textclass: "relation cousinbro1"
  //   },{
  //     textdata: data.string.cousister1,
  //     textclass: "relation cousinsis1"
  //   }]
  // },

]


    


$(function() {

  var $board = $(".board");
  
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var total_page = 0;
  
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

    /*
      inorder to use the handlebar partials we need to register them
      to their respective handlebar partial pointer first
    */
  // Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
  // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());


	function navigationController(){
		if(countNext == 0 && total_page != 1){
			$nextBtn.hide(0);
		}else if(countNext > 0 && countNext < (total_page-1)){
      $nextBtn.hide(0);
      $prevBtn.hide(0);  
		}else if(countNext == total_page-1){
			$prevBtn.show(0);
      $nextBtn.hide(0);
		}
	}

  function generalTemplate(){
    //playThisDialog[0].pause();
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    loadTimelineProgress($total_page,countNext+1);
    $board.html(html);

     // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        vocabcontroller.findwords(countNext);


    var $intro1       = $('.intro1');
    var $intro2       = $('.intro2');
    var $grandfather  = $('.grandfather');
    var $grandmother  = $('.grandmother');
    var $father       = $('.father');
    var $mother       = $('.mother');
    var $uncle        = $('.uncle');
    var $aunt         = $('.aunt');
    var $me           = $('.me');
    var $mybro        = $('.mybro');
    var $cousinbro    = $('.cousinbro');
    var $cousinsis    = $('.cousinsis');

    var $line1  = $('.line1');
    var $line2  = $('.line2');
    var $line3  = $('.line3');
    var $line4  = $('.line4');
    var $line5  = $('.line5');
    var $line6  = $('.line6');
    var $line7  = $('.line7');
    var $line8  = $('.line8');
    var $line9  = $('.line9');
    var $line10 = $('.line10');
    var $line11 = $('.line11');

    var $node1 = $(".node1");
    var $node2 = $(".node2");
    var $node3 = $(".node3");

    var $btnDelay1 = 500;
    var $btnDelay2 = 500;
    var $startTime = 500

    function slideImage1() {
       $(".slideimg1").css({
            left: "9%",
            opacity: "1"
          });
    }

    function dialogBox(timeout){
      $(".dialog-box").css({
            opacity: "1",
      });
        
      setTimeout(function() {
          $(".dialog-box").addClass("dialog-box-blink");
      }, timeout);
    }

    function playSound($fam1, soundIndex1, startTime, btnDelay, $node ){
      $fam1.css({
          transform: "scale(1.1)"
      });

      $node.show(0);
      setTimeout(function(){
         soundcontent[soundIndex1].play().bind("playing", function() {
          $fam1.css({ backgroundColor: "#FFE478", borderRadius: "50%", border: "2px solid #EF9C37" });
          $nextBtn.hide(0);
          }).bind('ended', function() {
            $fam1.css({ backgroundColor: "#C9F7FF", transform: "scale(1)", border: "2px solid #95DCDD"});
            setTimeout(function() {
              if(countNext == 0 ){
                $nextBtn.show(0);
                $prevBtn.hide(0);
              }else if(countNext > 0 && countNext < (total_page-1)){
                $nextBtn.show(0);
                $prevBtn.show(0);  
              }else if(countNext == total_page-1){
                $prevBtn.show(0);
                $nextBtn.hide(0);
              }
            }, btnDelay);
           
          });
        }, startTime);
    }

    function LastSlidePlaySound($fam, $familyintro, soundIndex){
      $(".dialog-box").show(0);
      soundcontent[soundIndex].play().bind("playing", function(){
        $(".family").css({cursor: "auto", pointerEvents : "none"});;
        $familyintro.fadeIn(0).css({ zIndex: "8888"});
        $fam.css({ backgroundColor: "#FFE478", borderRadius: "50%", border: "2px solid #EF9C37" })
      }).bind("ended", function() {
        $fam.css({ backgroundColor: "#C9F7FF", border: "2px solid #95DCDD" });
        $(".family").css({cursor: "pointer", pointerEvents : "auto"});;
        $familyintro.fadeOut(0).css({ zIndex: "8888"});;
        $(".dialog-box").fadeOut();
      });
      $nextBtn.hide(0);
    }
    // ole.footerNotificationHandler.pageEndSetNotification();
    switch (countNext) {
        case 0:
            dialog00.play().bind("ended", function() {
                $nextBtn.show();
            });
            break;
    	case 1:
        $prevBtn.hide(0);
    		setTimeout(function() {
    			$(".slideimg1").animate({
    				left: "9%",
    				opacity: "1"
    			});
    			$(".intro1").delay(3000).fadeIn(500);
          setTimeout(function() {
            $(".dialog-box").addClass("dialog-box-blink");
          }, 3000);

          $nextBtn.delay(5000).show(0);
    		}, 2000);
    	soundcontent[12].play();
        setTimeout(function() {
          playSound($grandmother, 0, $startTime, $btnDelay2, $node1);          
        }, 5500);

        $('hr').hide(0);
    	break;

      case 2:
        slideImage1();

        dialogBox(2000);

        $(".slideimg2").animate({
          animation: "none",
        });

        $(".slideimg2").css({
          opacity: "1",
          top: "20%"
        });
  
        setTimeout(function() {
          playSound($grandmother, 1, $startTime, $btnDelay2, $node1);          
        }, 100)

        setTimeout(function() {
          $(".intro2").fadeIn(100);
          $nextBtn.delay(4000).show(0);
          $prevBtn.delay(4000).show(0);
        }, 100)

        $('hr').hide(0);
      break;

      case 3:
        slideImage1();  
        dialogBox(2000);

        $node1.hide(0);
        $grandfather.fadeIn(1000);
        $(".grandfather1").fadeIn(1000);
        $(".family-intro1").fadeIn(400);

        setTimeout(function() {
            $(".dialog-box").addClass("dialog-box-blink");
        }, 2000);

        // Play Sound 
        playSound($grandfather, 2, $startTime, $btnDelay2, $node1);
      break;


      case 4:
        slideImage1();  
        dialogBox(2000);
        $(".grandfather, .grandfather1").show(0).animate({
          animation: "none"
        });

        $grandmother.fadeIn(1000);
        $(".grandmother1").fadeIn(1000);
        $(".family-intro2").fadeIn(400);
        
        // Play Sound 
        $node1.hide(0);
        playSound($grandmother, 3, $startTime, $btnDelay2, $node1);
      break;

      case 5:
        slideImage1();  
        
        dialogBox(2000);

        $(".grandfather, .grandfather1, .grandmother, .grandmother1").css({ animation: "none" }).show(0);
        $(".father1").delay(2000).fadeIn(1000);

        $line2.css({
          top: "28.3%",
          left: "59.55%",
          width: "0.3%",
          height: "0%",
        },1000).show(0).animate({ height: "10%"});
        setTimeout(function() {
          $line3.css({
            top: "38.3%",
            right: "40.2%",
            width: "0%"
          }, 3000).show(0).animate({  width: "25.4%"}); 
        },700)
        

        setTimeout(function(){
          $line4.css({
            top: "38.8%",
            left: "34.4%",
            width: "0.3%",
            height: "0"
          }).show(0).animate({ height: "4.6%" });
        },1400);

        $father.delay(2000).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro3").delay(2000).fadeIn(400);


        // Play Sound 
        setTimeout(function(){
          playSound($father, 4, $startTime, $btnDelay1, $node1);
        },1600);

      break;

      case 6:
       slideImage1();  
        
        dialogBox(2000);
        $('.node1').show(0).animate({animation: "none"});

        $(".grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1").css({ animation: "none" }).show(0);
        $(".mother1").delay(1000).fadeIn(1000);

        $line2.show(0);
        $line3.show(0);
        $line4.show(0);

        $mother.delay(1000).fadeIn(1000);
        $(".family-intro4").delay(1000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($mother, 5, $startTime, $btnDelay1, $node1);
        },1600);

      break


      case 7:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1').show(0).animate({animation: "none"});
        $(".aunt1").delay(2000).fadeIn(1000);
        $(".grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
          
        $line1.show(0).css({
          top: "38.3%", 
          left: "59.5%", 
          width: "0%"
        }).animate({ width: "21%"})
        setTimeout(function(){
          $line5.css({
          top: "38.3%",
          right: "19.5%",
          width: "0.3%",
          height: "0"
          }, 2000).show(0).animate({ height: "5.2%" });
        },1000);

        $aunt.delay(2000).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro5").delay(2000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($aunt, 6, $startTime, $btnDelay1, $node1);
        },1800);
      break;

      case 8:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1').show(0).animate({animation: "none"});
        $(".uncle1").delay(1000).fadeIn(1000);
        $(".grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);

        $uncle.delay(1000).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro6").delay(2000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($uncle, 7, $startTime, $btnDelay1, $node1);
        },1600);
      break;

      case 9:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1').show(0).animate({animation: "none"});
        $('.node2').show(0);
        $(".myself1").delay(2000).fadeIn(1000);
        $(".grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1, .uncle, .uncle1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);
        $line8.delay(1000).css({
          top: "53.4%",  
          left: "40.4%", 
          width: "0.31%", 
          height: "0"
        }).show(0).animate({ height: "22%"});        

        $me.delay(2000).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro7").delay(2000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($me, 8, $startTime, $btnDelay1, $node2);
        },1800);
      break;

      case 10:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1, .node2').show(0).animate({animation: "none"});
        $(".mybro1").delay(2000).fadeIn(1000);

        $(".me, .myself1, .grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1, .uncle, .uncle1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);
        $line8.show(0);

        $line6.delay(1000).show(0).css({
          bottom: "17%", 
          left: "39%", 
          width: "0%"
        }).animate({ width: "3.1%"});

        $mybro.delay(1500).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro8").delay(1000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($mybro, 9, $startTime, $btnDelay1, $node2);
        },1600);
      break;

      case 11:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1, .node2').show(0).animate({animation: "none"});
        $(".cousinbro1").delay(1500).fadeIn(1000);

        $(".mybro, .mybro1, .me, .myself1, .grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1, .uncle, .uncle1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);
        $line8.show(0);
        $line6.show(0);        
        
        $line9.delay(1000).css({
          top: "53.4%",  
          right: "25.3%", 
          width: "0.33%", 
          height: "0"
        }).show(0).animate({ height: "22%" });

        $cousinbro.delay(1500).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro9").delay(1500).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($cousinbro, 10, $startTime, $btnDelay1, $node3);
        },1600);
      break;
      
      case 12:
        slideImage1();  
        
        dialogBox(2000);
        $('.node1, .node2, .node3').show(0).animate({animation: "none"});
        $(".cousinsis1").delay(1000).fadeIn(1000);

        $(".cousinbro, .cousinbro1, .mybro, .mybro1, .grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1, .uncle, .uncle1, .me, .myself1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);
        $line8.show(0);
        $line6.show(0);        
        $line9.show(0);

        $line11.delay(600).css({
          bottom: "17%", 
          right: "24%", 
          width: "0%"
        }).show(0).animate({ width: "3.1%"});

        $cousinsis.delay(1000).fadeIn(1000).css({ animationDelay: "1s"});
        $(".family-intro10").delay(1000).fadeIn(400);

        // Play Sound 
        setTimeout(function(){
          playSound($cousinsis, 11, $startTime, $btnDelay1, $node3);
        },1600);
        setTimeout(function () {
            ole.footerNotificationHandler.pageEndSetNotification();
        },4000);
          break;

      case 13:
        slideImage1();  
        dialogBox(2000);

        $(".dialog-box").hide(0);

        $(".listenagain").css({
          height: "8%",
          width: "72%",
          top: "91%",
          left: "22%",
          backgroundColor: "#93C47D",
          padding: "1.2% 0"
        })

        $(".family").css({ 
          animation: "none",
          cursor: "pointer"
        }).show(0);

        $('.node1, .node2, .node3').show(0).animate({animation: "none"});      

        $(".family-intro11, .family-intro12").css({fontSize: "2.7vmin"})
        $(".cousinsis, .cousinsis1, .cousinbro, .cousinbro1, .mybro, .mybro1, .grandfather, .grandfather1, .grandmother, .grandmother1, .father, .father1, .mother, .mother1, .aunt, .aunt1, .uncle, .uncle1, .me, .myself1").css({ animation: "none" }).show(0);
        $line2.show(0);
        $line3.show(0);
        $line4.show(0);
        $line1.show(0);
        $line5.show(0);
        $line8.show(0);
        $line6.show(0);        
        $line9.show(0);
        $line11.show(0);

        $grandfather.click(function(){       
          LastSlidePlaySound($grandfather, $(".family-intro1"), 2);
        });
        
        $grandmother.click(function(){
          LastSlidePlaySound($grandmother, $(".family-intro2"), 3);
        });

        $father.click(function(){
          LastSlidePlaySound($father, $(".family-intro3"), 4);
        });
        $mother.click(function(){
          LastSlidePlaySound($mother, $(".family-intro4"), 5);
        });
        $uncle.click(function(){
          LastSlidePlaySound($uncle, $(".family-intro5"), 7);
        });
        $aunt.click(function(){
          LastSlidePlaySound($aunt, $(".family-intro6"), 6);
        });
        $me.click(function(){
          LastSlidePlaySound($me, $(".family-intro7"), 8);
        });
        $mybro.click(function(){
          LastSlidePlaySound($mybro, $(".family-intro8"), 9);
        });
        $cousinbro.click(function(){
          LastSlidePlaySound($cousinbro, $(".family-intro9"), 10);
        });
        $cousinsis.click(function(){
          LastSlidePlaySound($cousinsis, $(".family-intro10"), 11);
        });

        $('.listenagain').show(0).css({
          zIndex: "-1",
        });

        ole.footerNotificationHandler.pageEndSetNotification();

      break;
    };
  };

  function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);		
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
		
	};

	 $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
/*
	TODO: move this up to it's old location
*/
    // document.addEventListener('xmlLoad', function(e) {
        total_page = content.length;
        templateCaller();
});

function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
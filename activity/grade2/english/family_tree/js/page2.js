var imgpath = $ref+"/images/diyimages/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "s2_p1.ogg"));
var current_sound = sound_l_1;

var content=[
    {
        // slide0
        contentblockadditionalclass: "firstpagebg",
        contentnocenteradjust : true,
        uppertextblock : [{
            textdata : data.string.diytitle,
            textclass : 'lesson-title1'
        },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.diy1,
                textclass : 'question q1',
                ans:data.string.grandf,
                ans1:data.string.diy1ans
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.diy2,
                textclass : 'question q2',
                ans:data.string.fath,
                ans1:data.string.diy2ans
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.diy3,
                textclass : 'question q3',
                ans:data.string.moth,
                ans1:data.string.diy3ans
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.diy4,
                textclass : 'question q4',
                ans:data.string.sister,
                ans1:data.string.diy4ans
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.diy5,
                textclass : 'question q5',
                ans:data.string.brother,
                ans1:data.string.diy5ans
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "ans",
                textdata : data.string.happy,
                textclass : 'q6',
            }
        ],
        imageblockadditionalclass: 'right',
        imageblock : [{
            imagestoshow : [{

                imgclass : "rightimage",
                imgsrc : imgpath + "img01.png"

            }]
        }],
        menutext:[{
            divclass:"menudiv",
            menulist:[
                {
                    textclass:"option option1",
                    textdata:data.string.grandf
                },
                {
                    textclass:"option  option2",
                    textdata:data.string.grandm
                },
                {
                    textclass:"option  option3",
                    textdata:data.string.fath
                },
                {
                    textclass:"option  option4",
                    textdata:data.string.moth
                },
                {
                    textclass:"option  option5",
                    textdata:data.string.brother
                },
                {
                    textclass:"option  option6",
                    textdata:data.string.sister
                }
            ]
        }],
        textlist:[
            {
                textclass:"name  grandfather",
                textdata:data.string.gpname,
            },
            {
                textclass:"name grandmother",
                textdata:data.string.gmname,
            },
            {
                textclass:"name father",
                textdata:data.string.fname,
            },
            {
                textclass:"name mother",
                textdata:data.string.mname,
            },
            {
                textclass:"name sister",
                textdata:data.string.sname,
            },
            {
                textclass:"name ravi",
                textdata:data.string.ravi,
            }
        ]
    },
];


$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = 5;
    loadTimelineProgress($total_page,countNext+1);

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";
                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside){
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ?
            alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
            null ;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if($allnotifications.length > 0){
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }
    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        } else if ($total_page == 1) {
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        } else if (countNext > 0 && countNext < $total_page - 1) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == $total_page - 1) {
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification()

            // if lastpageflag is true
            // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
        }
    }
    /*=====  End of user navigation controller function  ======*/

    /*==================================================
   =            InstructionBlockController            =
   ==================================================*/
    /**
     How to:
     - Just call instructionblockcontroller() from the template
     */

    /**
     What it does:
     - It inserts and handles closing and opening of instruction block
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function instructionblockcontroller(){
        var $instructionblock = $board.find("div.instructionblock");
        if($instructionblock.length > 0){
            var $contentblock = $board.find("div.contentblock");
            var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
            var instructionblockisvisibleflag;

            $contentblock.css('pointer-events', 'none');

            $toggleinstructionblockbutton.on('click', function() {
                instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
                if(instructionblockisvisibleflag == 'true'){
                    instructionblockisvisibleflag = 'false';
                    $contentblock.css('pointer-events', 'auto');
                }
                else if(instructionblockisvisibleflag == 'false'){
                    instructionblockisvisibleflag = 'true';
                    $contentblock.css('pointer-events', 'none');
                }

                $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
            });
        }
    }
    /*=====  End of InstructionBlockController  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
    =            Templates Block            =
    =======================================*/
    /*=================================================
    =            general template function            =
    =================================================*/
    var sound_playing;
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[0]);
        $board.html(html);
        $(".option").removeClass("correctans");
        $(".option").removeClass("wrongans");
        $(".question,.q6").hide();

        // highlight any text inside board div with datahighlightflag set true
        // splitintofractions($(".fractionblock"));
        shufflehint();
        switch(countNext){
            case 0:
                current_sound.play();
                checkans("q1","grand_father.png");
                texthighlight($board);
                $(".q1").show();
                break;
            case 1:
                checkans("q3","mother.png");
                $(".q3").show();
                texthighlight($board);
                $(".question").html(data.string.diy3);
                texthighlight($board);
                break;
            case 2:
                checkans("q2","father.png");
                $(".q2").show();
                $(".question").html(data.string.diy2);
                texthighlight($board);
                break;
            case 3:
                checkans("q4","sister.png");
                $(".q4").show();
                $(".question").html(data.string.diy4);
                texthighlight($board);
                break;
            case 4:
                checkans("q5","family.png");
                $(".q5").show();
                $(".question").html(data.string.diy5);
                texthighlight($board);
                break;
            default:
                break;
        }
    }

    function shufflehint() {
        var optdiv = $(".menudiv");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        var optionclass = ["option option1","option option2","option option3","option option4","option option5","option option6"]
        optdiv.find(".option").removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function  checkans(question,showImage) {
       $(".option").click(function(){
           current_sound.stop(); 
           if($(this).text() == $("."+question).attr("data-check")){
               $(".name,.rightimage").fadeOut(700);
               play_correct_incorrect_sound(1);
               $(".ans").text($("."+question).attr("data-answer"))
               $(this).addClass("correctans");
               $(".option").addClass(" avoid-clicks");
               countNext == 4?$(".q6").show():"";
               navigationcontroller();
               setTimeout(function () {
                   $(".rightimage").fadeIn(100).attr("src",imgpath+showImage).addClass("zoomInClass");
               },700);
           }
           else{
               play_correct_incorrect_sound(0);
               $(this).addClass("wrongans");
               $(".option").removeClass(" avoid-clicks");
           }
       })
    }
    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/


    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

    function templateCaller(){
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');


        loadTimelineProgress($total_page,countNext+1);
        // call the template
        generalTemplate();
        /*
        for (var i = 0; i < content.length; i++) {
          slides(i);
          $($('.totalsequence')[i]).html(i);
          $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        }
        function slides(i){
            $($('.totalsequence')[i]).click(function(){
              countNext = i;
              templateCaller();
              templateCaller();
            });
          }
      */


        //call the slide indication bar handler for pink indicators

    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    /*=====  End of Templates Controller Block  ======*/
});

Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";

/*for questions*/
var memberForQues = ["grandfather", "grandmother", "father", "mother", "uncle", "aunt", "self", "brother", "cousin1", "cousin2"];

/*for options*/
var familyMembers = [
		["grandfather", "grandf", "Grand Father"], 
		["grandmother", "grandm", "Grand Mother"], 
		["father", "fath", "Father"], 
		["mother", "moth", "Mother"], 
		["uncle", "unc", "Uncle"], 
		["aunt", "aun", "Aunty"], 
		["self", "me", "Me"], 
		["brother", "bro", "Brother"], 
		["cousin1", "cou1", "Cousin"], 
		["cousin2", "cou2", "Cousin"]
	];
memberForQues.shufflearray();
soundAsset = $ref + "/sounds/";
var dialog0  = new buzz.sound(soundAsset + "Ex_title.ogg");
var content=[	
	//slide 1
	{
		familytreeblock: [
		{
			personname: "grandfather",
			imgspriteclass: "family grandf",
			textdata: data.string.grandf,
      		textclass: "relation"
		},
		{
			personname: "grandmother",
			imgspriteclass: "family grandm",
			textdata: data.string.grandm,
      		textclass: "relation"
		},
		{
			personname: "father",
			imgspriteclass: "family fath",
			textdata: data.string.fath,
      		textclass: "relation"
		},
		{
			personname: "mother",
			imgspriteclass: "family moth",
			textdata: data.string.moth,
      		textclass: "relation"
		},
		{
			personname: "uncle",
			imgspriteclass: "family unc",
			textdata: data.string.unc,
      		textclass: "relation"
		},
		{
			personname: "aunt",
			imgspriteclass: "family aun",
			textdata: data.string.aun,
      		textclass: "relation"
		},
		{
			personname: "self",
			imgspriteclass: "family me",
			textdata: data.string.me,
      		textclass: "relation"
		},
		{
			personname: "brother",
			imgspriteclass: "family bro",
			textdata: data.string.bro,
      		textclass: "relation"
		},
		{
			personname: "cousin1",
			imgspriteclass: "family cou1",
			textdata: data.string.cou1,
      		textclass: "relation"
		},
		{
			personname: "cousin2",
			imgspriteclass: "family cou2",
			textdata: data.string.cou2,
      		textclass: "relation"
		},
		],

		optblock: [
		{
			personname: "option1",
			imgspriteclass: "family",
      		textclass: "relation"
		},
		{
			personname: "option2",
			imgspriteclass: "family",
      		textclass: "relation"
		},
		{
			personname: "option3",
			imgspriteclass: "family",
      		textclass: "relation"
		},
		{
			personname: "option4",
			imgspriteclass: "family",
      		textclass: "relation"
		},
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	
	 //create eggs
	 var testin = new EggTemplate();
   
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[0]);
	 	$board.html(html);

         countNext==0?dialog0.play():"";
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
	 	$('.congratulation').hide(0);
	 	$('.exefin').hide(0);

	 	/*for family tree*/

	 	/*count next used cause split wont work when array is empty at end and js error will occur*/
	 	if(countNext < 10){
	 	/*memberforques has been shuffled. add handledrop to 1st index f the array*/
	 		$("." + memberForQues[0]).addClass("handleDrop");
	 		var corClass = $(".handleDrop").children(".family").attr("class").split(" ");
	 		var corName = $(".handleDrop").children(".relation").text();

	 		/*for randomizing options' poistion*/
	 		var optShuffleArray = ["option1", "option2", "option3", "option4"];
	 		optShuffleArray.shufflearray();

	 		var myArray = [];

	 		/*get three unique wrong random answers*/
	 		var answerindex = -1;

	 		for(var x = 0; x < familyMembers.length; x++){
	 			if(familyMembers[x].indexOf(memberForQues[0]) != -1){
	 				answerindex = x;
	 				break;
	 			}
	 		}
	 		
	 		if(answerindex != -1){
	 			myArray.push(answerindex);
	 		}

	 		var i = 0;
	 		while(i < 3) {
	 			var rand = Math.floor(Math.random() * 10);

	 			if(!(myArray.indexOf(rand) != -1)){
	 				myArray.push(rand);
	 				i++;
	 			}
	 		}

			/**
			 * Returns a random number between min (inclusive) and max (exclusive)
			 */
			 function generateRandomNumb(min, max) {
			 	return Math.floor(Math.random() * (max - min) + min);
			 }

	 		for(i = 0; i <= 3; i++){
				if(i == 0){
					$("." + optShuffleArray[i]).addClass("correctAns");
				}

					$("." + optShuffleArray[i] +" > .family").addClass(familyMembers[myArray[i]][1]);
	 				$("." + optShuffleArray[i] +" > .relation").html(familyMembers[myArray[i]][2]);
	 			
	 		}

	 		var $selectedMember = $(".handleDrop");
	 		$selectedMember.children(".family").addClass("fordropping");
	 		$selectedMember.children(".relation").html("_____________");

	 		var captionAns = $(".correctAns").children(".relation").text();

	 		$(".dropperson").draggable({
                containment: "body",
                revert: true,
                appendTo: "body",
                zindex: 10,
	 		});

	 		$(".handleDrop").droppable({
                hoverClass: "hovered",
	 			drop: function(event, ui){
	 				dropEvent(event, ui, captionAns);
	 			}
	 		});

	 		function dropEvent(event, ui, ans){
	 			var dropped = ui.draggable;
	 			var droppedOn = $(".handleDrop");
	 			if(dropped.hasClass("correctAns")){
		 			droppedOn.children(".family").removeClass("fordropping");
		 			droppedOn.children(".relation").html(ans);
		 			testin.update(true);
		 			$nextBtn.show(0);
	 				memberForQues.splice(0,1);
                    dropped.hide(0);
                    $(".dropperson").addClass("avoid-clicks");
                    play_correct_incorrect_sound(1);
	 			}
	 			else {
	 			    dropped.append("<img class='correctwrongimg' src='images/wrong.png'/>");
	 			    dropped.addClass("avoid-clicks");
                    testin.update(false);
                    play_correct_incorrect_sound(0);
				}
	 		}

	 	/*end for family tree*/
	 	}
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;			
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
		testin.gotoNext();
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
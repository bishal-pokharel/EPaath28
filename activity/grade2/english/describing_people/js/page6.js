var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/6/";

var sound_1 = new buzz.sound((soundAsset + "1.mp3"));
var sound_2 = new buzz.sound((soundAsset + "2.mp3"));
var sound_3 = new buzz.sound((soundAsset + "3.mp3"));
var sound_4 = new buzz.sound((soundAsset + "4.mp3"));
var sound_5 = new buzz.sound((soundAsset + "5.mp3"));
var sound_6 = new buzz.sound((soundAsset + "6.mp3"));
var current_sound = sound_1;
var sound_arr = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6];
var name = "";
var age;
var sex;
var glass;
var haircolor;
var hairlength;
var hobby;

var content = [
	//slide0
	{
		uppertextblock: [
			{
				textclass: "title",
				textdata: data.string.p6text1
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : imgpath + "bg05.png",
				}
			]
		}],
	},

	//slide1
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "toptext",
				textdata: data.string.p6text2
			},
			{
				textclass: "buttonsubmit",
				textdata: data.string.p6text2a
			},
		],
		textinputbox:[{}]
	},
	//slide2
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "question",
				textdata: data.string.p6text4
			},
			{
				textclass: "description1",
				textdata: data.string.p6text5
			},
			{
				textclass: "description2",
				textdata: data.string.p6text6
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "leftimage",
					imgsrc : imgpath + "boy01.png",
				},
				{
					imgclass : "rightimage",
					imgsrc : imgpath + "girl01.png",
				},
			]
		}],
	},
	//slide3
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "question",
				textdata: data.string.p6text7
			},
			{
				textclass: "description1",
				textdata: data.string.p6text8
			},
			{
				textclass: "description2",
				textdata: data.string.p6text9
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "centerimage",
					imgsrc : imgpath + "glasses.png",
				}
			]
		}],
	},
	//slide4
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "question",
				textdata: data.string.p6text10
			},
			{
				textclass: "description1",
				textdata: data.string.p6text11
			},
			{
				textclass: "description2",
				textdata: data.string.p6text12
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "leftimage",
					imgsrc : imgpath + "boyhair.png",
				},
				{
					imgclass : "rightimage",
					imgsrc : imgpath + "boyhairbrown.png",
				},
			]
		}],
	},
	//slide5
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "question",
				textdata: data.string.p6text13
			},
			{
				textclass: "description1",
				textdata: data.string.p6text14
			},
			{
				textclass: "description2",
				textdata: data.string.p6text15
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "leftimage",
					imgsrc : imgpath + "hairblack.png",
				},
				{
					imgclass : "rightimage",
					imgsrc : imgpath + "boyhair.png",
				},
			]
		}],
	},
	//slide6
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "question top1",
				textdata: data.string.p6text16
			},
			{
				textclass: "opt1",
				textdata: data.string.p6text17
			},
			{
				textclass: "opt2",
				textdata: data.string.p6text18
			},
			{
				textclass: "opt3",
				textdata: data.string.p6text19
			},
			{
				textclass: "opt4",
				textdata: data.string.p6text20
			}
		]
	},
	//slide6
	{
		contentblockadditionalclass : 'bg',
		uppertextblock: [
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text21
			},
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text22
			},
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text23
			},
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text24
			},
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text25
			},
			{
				textclass: "aboutfriend",
				textdata: data.string.p6text26
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "insideimage",
					imgsrc : imgpath + "boy.png",
				}
			],
			imagelabels:[{
						imagelabelclass: "friendsname"
					}]
			}]
	}];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;


	var has_imagination = false;
	var imagination = null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$("[type='number']").keypress(function (evt) {
		    evt.preventDefault();
		});
		switch (countNext) {
			case 0:
				play_diy_audio();
                $nextBtn.show(0);
                break;
			case 1:
						$('.buttonsubmit').hide(0);
						$nextBtn.hide(0);

						document.querySelector("#agetext").addEventListener("keypress", function (evt) {
						    if (evt.which > 57)
						    {
						        evt.preventDefault();
						    }
						});
					input_box('#nametext','#activity-page-next-btn-enabled');
					break;
			case 2:
					$nextBtn.hide(0);
					$('.description1').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description1').css("background","#6f8629");
						sex = true;
						$nextBtn.show(0);
					});
					$('.description2').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description2').css("background","#6f8629");
						sex = false;
						$nextBtn.show(0);
					});
					break;
			case 3:
					$nextBtn.hide(0);
					$('.description1').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description1').css("background","#6f8629");
						glass = true;
						$nextBtn.show(0);
					});
					$('.description2').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description2').css("background","#6f8629");
						glass = false;
						$nextBtn.show(0);
					});
					break;
			case 4:
					$nextBtn.hide(0);
					$('.description1').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description1').css("background","#6f8629");
						haircolor = true;
						$nextBtn.show(0);
					});
					$('.description2').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description2').css("background","#6f8629");
						haircolor = false;
						$nextBtn.show(0);
					});
					break;
			case 5:
					$nextBtn.hide(0);
					$('.description1').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description1').css("background","#6f8629");
						hairlength = true;
						$nextBtn.show(0);
					});
					$('.description2').click(function(){
						$('.description1,.description2').css("pointer-events","none");
						$('.description2').css("background","#6f8629");
						hairlength = false;
						$nextBtn.show(0);
					});
					break;
			case 6:
					$nextBtn.hide(0);
					$('.opt1').click(function(){
						$('.opt1,.opt2,.opt3,.opt4').css("pointer-events","none");
						$('.opt1').css("background","#6f8629");
						hobby = 1;
						$nextBtn.show(0);
					});
					$('.opt2').click(function(){
						$('.opt1,.opt2,.opt3,.opt4').css("pointer-events","none");
						$('.opt2').css("background","#6f8629");
						hobby = 2;
						$nextBtn.show(0);
					});
					$('.opt3').click(function(){
						$('.opt1,.opt2,.opt3,.opt4').css("pointer-events","none");
						$('.opt3').css("background","#6f8629");
						hobby = 3;
						$nextBtn.show(0);
					});
					$('.opt4').click(function(){
						$('.opt1,.opt2,.opt3,.opt4').css("pointer-events","none");
						$('.opt4').css("background","#6f8629");
						hobby = 4;
						$nextBtn.show(0);
					});
					break;
			case 7:
					$(".textblock").css({"top": "26%","left": "12%"});
					$('.imageblock').css({"width": "25%","border": ".1em solid aliceblue","height": "44%","top": "28%","left": "62%"});
					$('.name').html(name);

					if(sex==true)
					{
						$('.heorshe').html("He");
					}
					else{
						$('.heorshe').html("She");
					}

					$('.age').html(age);


					if(glass==true)
					{
						$('.glass').html("wears");
					}
					else{
						$('.glass').html("does not wear");
					}

					if(haircolor==true)
					{
						$('.haircolor').html("black");
					}
					else{
						$('.haircolor').html("brown");
					}

					if(hairlength==true)
					{
						$('.longshort').html("long");
					}
					else{
						$('.longshort').html("short");
					}

					if(hobby==1)
					{
						$('.hobby').html("sing");
					}
					if(hobby==2)
					{
						$('.hobby').html("dance");
					}
					if(hobby==3)
					{
						$('.hobby').html("play");
					}
					if(hobby==4)
					{
						$('.hobby').html("read");
					}

					$('.friendsname').html(name);

					if(sex==true && glass==false && haircolor==false && hairlength==false)
					{
						 $('.insideimage').attr('src',imgpath + 'boy05.png');
					}
					if(sex==true && glass==false && haircolor==false && hairlength==true)
					{
						 $('.insideimage').attr('src',imgpath + 'boy07.png');
					}
					if(sex==true && glass==false && haircolor==true && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'boy01.png');
					}
					if(sex==true && glass==false && haircolor==true && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'boy03.png');
					}
					if(sex==true && glass==true && haircolor==false && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'boy06.png');
					}
					if(sex==true && glass==true && haircolor==false && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'boy08.png');
					}
					if(sex==true && glass==true && haircolor==true && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'boy02.png');
					}
					if(sex==true && glass==true && haircolor==true && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'boy04.png');
					}


					if(sex==false && glass==false && haircolor==false && hairlength==false)
					{
						 $('.insideimage').attr('src',imgpath + 'girl05.png');
					}
					if(sex==false && glass==false && haircolor==false && hairlength==true)
					{
						 $('.insideimage').attr('src',imgpath + 'girl07.png');
					}
					if(sex==false && glass==false && haircolor==true && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'girl01.png');
					}
					if(sex==false && glass==false && haircolor==true && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'girl03.png');
					}
					if(sex==false && glass==true && haircolor==false && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'girl06.png');
					}
					if(sex==false && glass==true && haircolor==false && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'girl08.png');
					}
					if(sex==false && glass==true && haircolor==true && hairlength==false)
					{
						$('.insideimage').attr('src',imgpath + 'girl02.png');
					}
					if(sex==false && glass==true && haircolor==true && hairlength==true)
					{
						$('.insideimage').attr('src',imgpath + 'girl04.png');
					}
					setTimeout(function(){
							ole.footerNotificationHandler.pageEndSetNotification();
					},500);
					break;

			default:
					nav_button_controls(500);
					break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	// for entering the value in input box
	function input_box(input_class, button_class) {
		$(button_class).css('display','none');
		$(input_class).keyup(function(e){
			if($(input_class).val().length<2)
			{
				$(button_class).css('display','none');
			}
			if($(input_class).val().length>=2)
			{
				$(button_class).css('display','block');
			}
		});
		$(input_class).keydown(function (e) {
			$this=$(this);
			$(button_class).click(function(){
				// $('.left_dialogue').html(data.string.p7text9);
			});
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$(button_class).click();
		}
		// Ensure that it is a number and stop the keypress
		if (!(e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			case 1:
					name = $('#nametext').val();
					console.log(name);
					age = $('#agetext').val();
					$('#inputtext').css("pointer-events","none");
					current_sound.stop();
					countNext++;
					templateCaller();
			break;
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

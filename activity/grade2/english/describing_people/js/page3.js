var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  //slide0
  {
    contentblockadditionalclass: "girlschool"
  },

  //slide1
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      }
    ]
  },
  //slide2
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a2",
            imgsrc: imgpath + "arrow_left.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      },
      {
        textclass: "descriptionclass seconddesc dg2",
        textdata: data.string.p3text2
      }
    ]
  },
  //slide3
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a2",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a3",
            imgsrc: imgpath + "arrow_left.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      },
      {
        textclass: "descriptionclass seconddesc dg2",
        textdata: data.string.p3text2
      },
      {
        textclass: "descriptionclass thirddesc dg3",
        textdata: data.string.p3text3
      }
    ]
  },
  //slide4
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a2",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a3",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a4",
            imgsrc: imgpath + "arrow_right.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      },
      {
        textclass: "descriptionclass seconddesc dg2",
        textdata: data.string.p3text2
      },
      {
        textclass: "descriptionclass thirddesc dg3",
        textdata: data.string.p3text3
      },
      {
        textclass: "descriptionclass fourthdesc dg4",
        textdata: data.string.p3text4
      }
    ]
  },
  //slide5
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a2",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a3",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a4",
            imgsrc: imgpath + "arrow_right.png"
          },
          {
            imgclass: "a5",
            imgsrc: imgpath + "arrow_right.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      },
      {
        textclass: "descriptionclass seconddesc dg2",
        textdata: data.string.p3text2
      },
      {
        textclass: "descriptionclass thirddesc dg3",
        textdata: data.string.p3text3
      },
      {
        textclass: "descriptionclass fourthdesc dg4",
        textdata: data.string.p3text4
      },
      {
        textclass: "descriptionclass fifthdesc dg5",
        textdata: data.string.p3text5
      }
    ]
  },
  //slide6
  {
    contentblockadditionalclass: "onlyschoolbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "centergirl",
            imgsrc: imgpath + "farmer01.png"
          },
          {
            imgclass: "a1",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a2",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a3",
            imgsrc: imgpath + "arrow_left.png"
          },
          {
            imgclass: "a4",
            imgsrc: imgpath + "arrow_right.png"
          },
          {
            imgclass: "a5",
            imgsrc: imgpath + "arrow_right.png"
          },
          {
            imgclass: "a6",
            imgsrc: imgpath + "arrow_right.png"
          }
        ]
      }
    ],
    uppertextblock: [
      {
        textclass: "descriptionclass name dg1",
        textdata: data.string.p3text1
      },
      {
        textclass: "descriptionclass seconddesc dg2",
        textdata: data.string.p3text2
      },
      {
        textclass: "descriptionclass thirddesc dg3",
        textdata: data.string.p3text3
      },
      {
        textclass: "descriptionclass fourthdesc dg4",
        textdata: data.string.p3text4
      },
      {
        textclass: "descriptionclass fifthdesc dg5",
        textdata: data.string.p3text5
      },
      {
        textclass: "descriptionclass sixthdesc dg6",
        textdata: data.string.p3text6
      }
    ]
  }
];

$(function() {
  var $board = $(".board");

  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var $total_page = content.length;
  var current_sound;
  var myTimeout = null;
  var timeoutvar = null;
  var preload;

  var has_imagination = false;
  var imagination = null;
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // sounds
      { id: "sound_1", src: soundAsset + "p3_s1.ogg" },
      { id: "sound_2", src: soundAsset + "p3_s2.ogg" },
      { id: "sound_3", src: soundAsset + "p3_s3.ogg" },
      { id: "sound_4", src: soundAsset + "p3_s4.ogg" },
      { id: "sound_5", src: soundAsset + "p3_s5.ogg" },
      { id: "sound_6", src: soundAsset + "p3_s6.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }

  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();
  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      case 1:
        textboxclicksound();
        sound_player("sound_1", true);
        $(".dg1").addClass("fadein");
        $(".a1").addClass("fadein1");
        break;
      case 2:
        textboxclicksound();
        sound_player("sound_2", true);
        $(".dg2").addClass("fadein");
        $(".a2").addClass("fadein1");
        break;
      case 3:
        textboxclicksound();
        sound_player("sound_3", true);
        $(".dg3").addClass("fadein");
        $(".a3").addClass("fadein1");
        break;
      case 4:
        textboxclicksound();
        sound_player("sound_4", true);
        $(".dg4").addClass("fadein");
        $(".a4").addClass("fadein1");
        break;
      case 5:
        textboxclicksound();
        sound_player("sound_5", true);
        $(".dg5").addClass("fadein");
        $(".a5").addClass("fadein1");
        break;
      case 6:
        textboxclicksound();
        sound_player("sound_6", true);
        $(".dg6").addClass("fadein");
        $(".a6").addClass("fadein1");
        break;

      default:
        nav_button_controls(500);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id, navigate) {
    console.log("sound id is " + sound_id);
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      navigate ? nav_button_controls(500) : "";
    });
  }
  function textboxclicksound() {
    $(".descriptionclass").click(function() {
      var countVal = $(this).hasClass("dg1")
        ? 1
        : $(this).hasClass("dg2")
        ? 2
        : $(this).hasClass("dg3")
        ? 3
        : $(this).hasClass("dg4")
        ? 4
        : $(this).hasClass("dg5")
        ? 5
        : 6;
      var sound_id = "sound_" + countVal;
      sound_player(sound_id, true);
    });
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        current_sound.stop();
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    clearTimeout(timeoutvar);
    current_sound.stop();
    countNext--;
    templateCaller();
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  total_page = content.length;
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

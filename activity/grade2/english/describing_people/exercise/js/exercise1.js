var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var correc = new buzz.sound(soundAsset+"corre.ogg");
var content=[
	//slide 0
	{
		contentblockadditionalclass:'',
		exerciseblock: [
			{
				centerimgsrc: imgpath+'01.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext1,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e1ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e1ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e1ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e1ansd,
					}]
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 1
	{
		exerciseblock: [
			{
				centerimgsrc: imgpath+'02.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext2,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e2ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e2ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e2ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e2ansd,
					}],

			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 2
	{
		only2: true,
		exerciseblock: [
			{
				centerimgsrc: imgpath+'03.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext3,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e3ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e3ansb,
					}]
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 3
	{
		exerciseblock: [
			{
				centerimgsrc: imgpath+'04.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext4,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e4ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e4ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e4ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e4ansd,
					}],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 4
	{
		only2: true,
		exerciseblock: [
			{
				centerimgsrc: imgpath+'05.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext5,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e5ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e5ansb,
					},
					],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 5
	{
		exerciseblock: [
			{
				centerimgsrc: imgpath+'06.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext6,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e6ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e6ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e6ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e6ansd,
					}],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 6
	{
		exerciseblock: [
			{
				centerimgsrc: imgpath+'07.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext7,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e7ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e7ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e7ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e7ansd,
					}],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 7
	{
		only2: true,
		exerciseblock: [
			{
				centerimgsrc: imgpath+'08.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext8,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e8ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e8ansb,
					}],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 8
	{
		only2: true,
		exerciseblock: [
			{
				centerimgsrc: imgpath+'09.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext9,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e9ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e9ansb,
					},
					],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	},
	//slide 9
	{
		exerciseblock: [
			{
				centerimgsrc: imgpath+'10.png',
				textclass : 'instruction my_font_big',
				questiondata: data.string.etext10,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.e10ansa,
					},
					{
						option_class: "class2",
						optiondata: data.string.e10ansb,
					},
					{
						option_class: "class3",
						optiondata: data.string.e10ansc,
					},
					{
						option_class: "class4",
						optiondata: data.string.e10ansd,
					}],
			}
		],
		extratextblock:[{
			textclass:'topquestion',
			textdata: data.string.einstruction
		}]
	}
];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var $refreshBtn = $("");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}


	var scoring = new EggTemplate();
	scoring.init($total_page);


	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){

		});
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		scoring.numberOfQuestions();
		/*for randomizing the options*/
		if(countNext==0) soundplayer(correc);
		if(countNext<$total_page){
			if(content[countNext].only2){
				var option_position = [5,6];
				option_position.shufflearray();
				for(var op=0; op<2; op++){
					$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
				}
			} else{
				var option_position = [1,2,3,4];
				option_position.shufflearray();
				for(var op=0; op<4; op++){
					$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
				}
			}
		}

		var wrong_clicked = 0;
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();
		// call the template
		generalTemplate();
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			scoring.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + '/images/';
var soundAsset = $ref+"/audio/4/";
var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

var sound_dg1 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p4_s8.ogg"));


var content;
content = [{
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
         characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-04.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-05.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 2
    contentblockadditionalclass: 'bg-01',
    uppertextblock:[
        {
            textclass: 'dialogueboxradha1 template-dialougebox2-top-yellow fadein1 1d',
            textdata : data.string.p4_text1
        }
    ],
    conversationblock: [{
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-04.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-03.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 3
    contentblockadditionalclass: 'bg-01',
    uppertextblock:[
        {
            textclass: 'dialogueboxneeraj1 template-dialougebox2-top-flipped-yellow fadein1 2d',
            textdata : data.string.p4_text2
        }
    ],
    conversationblock: [{
       characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-01.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-05.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 4
    contentblockadditionalclass: 'bg-01',
    uppertextblock:[
        {
            textclass: 'dialogueboxradha1 template-dialougebox2-top-yellow fadein1 3d',
            textdata : data.string.p4_text3
        }
    ],
    conversationblock: [{
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-04.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-03.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 5
    contentblockadditionalclass: 'bg-01',
     uppertextblock:[
        {
            textclass: 'dialogueboxneeraj2 template-dialougebox2-top-flipped-yellow fadein1 4d',
            textdata : data.string.p4_text4
        }
    ],
    conversationblock: [{
      characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-01.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-05.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 6
    contentblockadditionalclass: 'bg-01',
     uppertextblock:[
        {
            textclass: 'dialogueboxradha2 template-dialougebox2-top-yellow fadein1 5d',
            textdata : data.string.p4_text5
        }
    ],
    conversationblock: [{
       characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-04.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-03.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 7
    contentblockadditionalclass: 'bg-01',
      uppertextblock:[
        {
            textclass: 'dialogueboxneeraj2 template-dialougebox2-top-flipped-yellow fadein1 6d',
            textdata : data.string.p4_text6
        }
    ],
    conversationblock: [{
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-01.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-05.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 8
    contentblockadditionalclass: 'bg-01',
     uppertextblock:[
        {
            textclass: 'dialogueboxneeraj3 template-dialougebox2-top-flipped-yellow fadein1 7d',
            textdata : data.string.p4_text7
        }
    ],
    conversationblock: [{
       characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'artist-01.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'artist-05.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'artist.png'
        }]
    }]
}, {
    // slide 9
     uppertextblock:[
        {
            textclass: 'toptext',
            textdata : data.string.p4_text8
        }
    ],
    imageblock: [{
        imagestoshow: [{
            imgclass: 'professiondisc',
            imgsrc: imgpath + 'paintings-06.png'
        }]
    }],
}];
// }

$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");

    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;

var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        switch (countNext) {
            case 0:
                $nextBtn.show(0);
                break;
            case 1:
                $prevBtn.show(0);
                playaudio(sound_dg1, $(".1d"));
                break;
            case 2:
                $prevBtn.show(0);
                playaudio(sound_dg2, $(".2d"));
                break;
            case 3:
                $prevBtn.show(0);
                playaudio(sound_dg3, $(".3d"));
                break;
            case 4:
                $prevBtn.show(0);
                playaudio(sound_dg4, $(".4d"));
                break;
            case 5:
                $prevBtn.show(0);
                playaudio(sound_dg5, $(".5d"));
                break;
            case 6:
                $prevBtn.show(0);
                playaudio(sound_dg6, $(".6d"));
                break;
            case 7:
                $prevBtn.show(0);
                playaudio(sound_dg7, $(".7d"));
                break;
            case 8:
                $prevBtn.show(0);
                playaudio(sound_dg8, $(".7d"));
                setTimeout(()=>ole.footerNotificationHandler.pageEndSetNotification(),3000);
                break;
        }
    }


    function playaudio(sound_data, $dialog_container){
        var playing = true;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
            if(!playing){
                playaudio(sound_data, $dialog_container);
            }
            return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
            ole.footerNotificationHandler.hideNotification();
        }else{
            $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
            setTimeout(function(){
                $prevBtn.show(0);
                $dialog_container.addClass("playable");
                playing = false;
                sound_data.unbind('ended');
                if((countNext+1) == content.length){
                    ole.footerNotificationHandler.pageEndSetNotification();
                }else{
                    $nextBtn.show(0);
                }
            }, 1000);
        });
    }

    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    function playaudio(sound_data, $dialog_container){
        var playing = true;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
            if(!playing){
                playaudio(sound_data, $dialog_container);
            }
            return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
            ole.footerNotificationHandler.hideNotification();
        }else{
            $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
            setTimeout(function(){
                $prevBtn.show(0);
                $dialog_container.addClass("playable");
                playing = false;
                sound_data.unbind('ended');
                if((countNext+1) == content.length){
                    ole.footerNotificationHandler.pageEndSetNotification();
                }else{
                    $nextBtn.show(0);
                }
            }, 1000);
        });
    }


    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

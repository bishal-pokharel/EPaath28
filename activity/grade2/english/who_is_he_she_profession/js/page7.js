var imgpath = $ref + '/images/';
var soundAsset = $ref+"/audio/slide6/";
var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

var sound_dg1 = new buzz.sound((soundAsset + "hello_radha.mp3"));
var sound_dg2 = new buzz.sound((soundAsset + "hello_neeraj.mp3"));
var sound_dg3 = new buzz.sound((soundAsset + "who_is_she.mp3"));
var sound_dg4 = new buzz.sound((soundAsset + "she_is_my_mother.mp3"));
var sound_dg5 = new buzz.sound((soundAsset + "what_does_your_mother_do.mp3"));
var sound_dg6 = new buzz.sound((soundAsset + "she_is_a_nurse.mp3"));
var sound_dg7 = new buzz.sound((soundAsset + "she_is_a_nurse.mp3"));

var content;
content = [{
    contentblockadditionalclass: 'pink_bg',
    uppertextblock:[
        {
            textclass: 'question',
            textdata : data.string.p7_text1
        },
         {
            textclass: 'class1',
            textdata : data.string.p7_text2
        },
        {
            textclass: 'class2',
            textdata : data.string.p7_text3
        },
        {
            textclass: 'class3',
            textdata : data.string.p7_text4
        },
        {
            textclass: 'class4',
            textdata : data.string.p7_text5
        },
    ],
    options:[,
    {
        optiondivclass:'option1',
        imgsrc:imgpath + 'teacher.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option2',
        imgsrc:imgpath + 'student.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option3',
        imgsrc:imgpath + 'nurse.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option4',
        imgsrc:imgpath + 'police.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    ]
},

{
    contentblockadditionalclass: 'pink_bg',
    uppertextblock:[
        {
            textclass: 'question',
            textdata : data.string.p7_text1
        },
         {
            textclass: 'class1',
            textdata : data.string.p7_text6
        },
        {
            textclass: 'class2',
            textdata : data.string.p7_text7
        },
        {
            textclass: 'class3',
            textdata : data.string.p7_text8
        },
        {
            textclass: 'class4',
            textdata : data.string.p7_text9
        },
    ],
    options:[,
    {
        optiondivclass:'option1',
        imgsrc:imgpath + 'doctor.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option2',
        imgsrc:imgpath + 'farmer.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option3',
        imgsrc:imgpath + 'driver.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    {
        optiondivclass:'option4',
        imgsrc:imgpath + 'tailor.png',
        imgclass:'optionimg',
        textclass:'imagetag'
    },
    ]
}];
// }

$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");

    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;

var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        var count = 0;
        $('.class1,.class2,.class3,.class4').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                cursor : "auto",
                zIndex: 100000,
            });

        drops("1");
        drops("2");
        drops("3");
        drops("4");
        function drops(yo)
        {
             $(".option"+yo).droppable({
                hoverClass: "hovered",
                accept: ".class1,.class2,.class3,.class4",
                drop: function (event, ui){
                    if(ui.draggable.hasClass('class'+yo))
                    {
                        var textyo = ui.draggable.text();
                        ui.draggable.css({"display": "none"});
                        $(this).children('.imagetag').html(textyo).css({"background":"green"});
                        $this = $(this);
                        play_correct_incorrect_sound(1);
                        // ui.droppable.removeClass('hovered')
                        count++;
                        console.log(count);
                        if(count==4)
                        {
                            nav_button_controls(100);
                        }
                    }
                    else{
                        play_correct_incorrect_sound(0);
                        if(ui.draggable.hasClass('class1'))
                        {
                            ui.draggable.css({"background":"red"});
                            ui.draggable.animate({"left": "4%",
                                                     "top": "14%"});
                        }
                        if(ui.draggable.hasClass('class2'))
                        {
                            ui.draggable.css({"background":"red"});
                            ui.draggable.animate({"left": "28%",
                                                     "top": "14%"});
                        }
                        if(ui.draggable.hasClass('class3'))
                        {
                            ui.draggable.css({"background":"red"});
                            ui.draggable.animate({"left": "52%",
                                                     "top": "14%"});
                        }
                        if(ui.draggable.hasClass('class4'))
                        {
                            ui.draggable.css({"background":"red"});
                            ui.draggable.animate({"left": "76%",
                                                     "top": "14%"});
                        }
                    }


                }
            });

        }
        // switch(countNext){
        //     case 0:
        //     break;
        //     case 1:
        //     nav_button_controls(4000);
        //     break;
        // }

    }


        function nav_button_controls(delay_ms){
                timeoutvar3 = setTimeout(function(){
                    if(countNext==0){
                        $nextBtn.show(0);
                    } else if( countNext>0 && countNext == $total_page-1){
                        $prevBtn.show(0);
                        ole.footerNotificationHandler.pageEndSetNotification();
                    } else{
                        $prevBtn.show(0);
                        $nextBtn.show(0);
                    }
                },delay_ms);
            }
    function playaudio(sound_data, $dialog_container){
        var playing = true;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
            if(!playing){
                playaudio(sound_data, $dialog_container);
            }
            return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
            ole.footerNotificationHandler.hideNotification();
        }else{
            $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
            setTimeout(function(){
                $prevBtn.show(0);
                $dialog_container.addClass("playable");
                playing = false;
                sound_data.unbind('ended');
                if((countNext+1) == content.length){
                    ole.footerNotificationHandler.pageEndSetNotification();
                }else{
                    $nextBtn.show(0);
                }
            }, 1000);
        });
    }

    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    }

    function playaudio(sound_data, $dialog_container){
        var playing = true;
        $dialog_container.removeClass("playable");
        $dialog_container.click(function(){
            if(!playing){
                playaudio(sound_data, $dialog_container);
            }
            return false;
        });
        $prevBtn.hide(0);
        if((countNext+1) == content.length){
            ole.footerNotificationHandler.hideNotification();
        }else{
            $nextBtn.hide(0);
        }
        sound_data.play();
        sound_data.bind('ended', function(){
            setTimeout(function(){
                $prevBtn.show(0);
                $dialog_container.addClass("playable");
                playing = false;
                sound_data.unbind('ended');
                if((countNext+1) == content.length){
                    ole.footerNotificationHandler.pageEndSetNotification();
                }else{
                    $nextBtn.show(0);
                }
            }, 1000);
        });
    }


    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

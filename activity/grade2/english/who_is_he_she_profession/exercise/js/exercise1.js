var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/audio/";

var sound_1 = new buzz.sound((soundAsset + "Click_on_the_correct_answer.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q1.png',
		uppertextblock : [
		{
			textdata : data.string.etext1,
			textclass : 'instruction',
		},{
			textdata : data.string.eshe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext1a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext1b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext1c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext1d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q2.png',
		uppertextblock : [
		{
			textdata : data.string.etext2,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext2a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext2b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext2c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext2d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q3.png',
		uppertextblock : [
		{
			textdata : data.string.etext3,
			textclass : 'instruction',
		},{
			textdata : data.string.eshe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext3a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext3b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext3c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext3d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q4.png',
		uppertextblock : [
		{
			textdata : data.string.etext4,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext4a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext4b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext4c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext4d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q5.png',
		uppertextblock : [
		{
			textdata : data.string.etext5,
			textclass : 'instruction',
		},{
			textdata : data.string.eshe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext5a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext5b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext5c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext5d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q6.png',
		uppertextblock : [
		{
			textdata : data.string.etext6,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext6a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext6b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext6c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext6d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q7.png',
		uppertextblock : [
		{
			textdata : data.string.etext7,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext7a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext7b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext7c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext7d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q8.png',
		uppertextblock : [
		{
			textdata : data.string.etext8,
			textclass : 'instruction',
		},{
			textdata : data.string.eshe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext8a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext8b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext8c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext8d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q9.png',
		uppertextblock : [
		{
			textdata : data.string.etext9,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext9a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext9b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext9c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext9d,
			textclass : 'class4 options'
		}
		],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main-bg',
		imagesrc: imgpath+'q10.png',
		uppertextblock : [
		{
			textdata : data.string.etext10,
			textclass : 'instruction',
		},{
			textdata : data.string.ehe,
			textclass : 'instruction instruction-2',
		}],
		optionsblockadditionalclass: '',
		optionsblock : [{
			textdata : data.string.etext10a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.etext10b,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.etext10c,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.etext10d,
			textclass : 'class4 options'
		}
		],
	}
];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}


	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new EggTemplate();
	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(".contentblock").append("<p class='instrn'> Click on the correct answer. </p>")
		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var wrong_clicked = false;
		countNext==0?sound_1.play():'';
		$(".options").click(function(){
			sound_1.stop();
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").parent().css('pointer-events', 'none');
				$(this).parent().css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				$(this).parent().children('.cor').show(0);
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).parent().css({
					'background-color': '#BA6B82',
					'pointer-events': 'none',
					'border': 'none'
				});
				$(this).parent().children('.incor').show(0);
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sound/";

var sound_1 = new buzz.sound((soundAsset + "diyins.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: '',
		bgcolor: '#E6B8AF',
		fontcolor: '#5B0F00',
		optcolor: '#BC7C76',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 1
	{
		contentblockadditionalclass: '',
		bgcolor: '#FCE5CD',
		fontcolor: '#5B0F00',
		optcolor: '#CBA681',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e2a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e2b,
			textclass : 'class2 options'
		},
		],
	},
	//slide2
	{
		contentblockadditionalclass: '',
		bgcolor: '#B6D7A8',
		fontcolor: '#274E13',
		optcolor: '#86A976',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e3a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e3b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 3
	{
		contentblockadditionalclass: '',
		bgcolor: '#D0E0E3',
		fontcolor: '#0C343D',
		optcolor: '#98AFB3',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e4a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e4b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 4
	{
		contentblockadditionalclass: '',
		bgcolor: '#D9D2E9',
		fontcolor: '#20124D',
		optcolor: '#9F95B8',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e5a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e5b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 5
	{
		contentblockadditionalclass: '',
		bgcolor: '#EAD1DC',
		fontcolor: '#4C1130',
		optcolor: '#BB97A8',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e6a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e6b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 6
	{
		contentblockadditionalclass: '',
		bgcolor: '#DD7E6B',
		fontcolor: '#5B0F00',
		optcolor: '#AF5745',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e7a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e7b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 7
	{
		contentblockadditionalclass: '',
		bgcolor: '#F9CB9C',
		fontcolor: '#5B0F00',
		optcolor: '#C99663',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e8a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e8b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 8
	{
		contentblockadditionalclass: '',
		bgcolor: '#FFE599',
		fontcolor: '#5B0F00',
		optcolor: '#CCA661',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e9a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e9b,
			textclass : 'class2 options'
		},
		],
	},
	//slide 9
	{
		contentblockadditionalclass: '',
		bgcolor: '#9FC5E8',
		fontcolor: '#073763',
		optcolor: '#638DB4',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction',
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e10a,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e10b,
			textclass : 'class2 options'
		},
		],
	},
];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}


	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new LampTemplate();
	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext==0) sound_1.play();
		//randomize options

		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		if(countNext<$total_page){
			$('.contentblock').css('background-color', content[countNext].bgcolor);
			$('.options').css('background-color', content[countNext].optcolor);
			$('.options, .instruction').css('color', content[countNext].fontcolor);
		}
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				$(this).parent().children('.cor').show(0);
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({
					'background-color': '#BA6B82',
					'pointer-events': 'none',
					'border': 'none'
				});
				$(this).parent().children('.incor').show(0);
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

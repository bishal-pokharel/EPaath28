var imgpath = $ref+"/images/exerciseImage/";

// Array.prototype.shufflearray = function() {
//     var i = this.length,
//         j, temp;
//     while (--i > 0) {
//         j = Math.floor(Math.random() * (i + 1));
//         temp = this[j];
//         this[j] = this[i];
//         this[i] = temp;
//     }
//     return this;
// };

var content=[
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques1,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "play.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "reading.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "sitting-on-the-tree.png",
			}
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques2,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "sitting-on-the-tree.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "runningh.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fly-kite.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques3,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "runningh.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "sitting-on-the-tree.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fishning.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fly-kite.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques4,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "reading.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fishning.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "swim.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques5,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "fly-kite.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "swim.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "sitting-on-the-tree.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques6,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "runningh.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "swim.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "play.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques7,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "cleaning-house.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "playgame.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "runningh.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques8,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "fishning.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "reading.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fly-kite.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques9,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "swim.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "reading.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "playgame.png",
			},
			]
		}
		]
	},
	{
		//slide 0	
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques10,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "playgame.png",
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cooking.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "fishning.png",
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "reading.png",
			},
			]
		}
		]
	},
];

/*remove this for non random questions*/
// content.shufflearray();

$(function (){	
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();
   
	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;
	 	
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".correctOne").click(function(){
			correct_btn(this);
	 		testin.update(true);
			play_correct_incorrect_sound(1);
	 		$nextBtn.show(0);
	 	});

	 	$(".incorrectOne").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	$(".incorrectTwo").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	function correct_btn(current_btn){
            $(current_btn).append("<img class='correctwrongImg' src='images/correct.png'>");
            $('.options_sign').addClass('disabled');
	 		$('.hidden_sign').html($(current_btn).html());
	 		$('.hidden_sign').addClass('fade_in');
	 		$('.optionscontainer').removeClass('forHover');
	 		$(current_btn).addClass('option_true');
	 	}

	 	function incorrect_btn(current_btn){
            $(current_btn).append("<img class='correctwrongImg' src='images/wrong.png'>");
            $(current_btn).addClass('disabled');
	 		$(current_btn).addClass('option_false');
	 	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

		//call the slide indication bar handler for pink indicators
		

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
		
	});


	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
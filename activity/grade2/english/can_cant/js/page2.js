var imgpath = $ref + "/images/";

var soundPath = $ref + "/sounds/";

var sound_1 = new buzz.sound((soundPath + "diy_2.ogg"));

var content = [

//slide0

{
	hasheaderblock : false,
	imageblockadditionalclass: "full",
	imageblock : [{
		imagestoshow : [{

			imgclass : "lokharke",
			imgsrc : imgpath + "lokharke.png",

		}],
		imagelabels : [{
		imagelabelclass:"lokharkelabel2",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label2",
		imagelabeldata:data.string.p1text27,
	}]
	}]
},

//slide1

{hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "textpara",

	uppertextblock : [{
		textdata : "<span style='color:#f26c00;font-size:1.5em;'>Can</span> or <span style='color:#a64d79;font-size:1.5em;'>Can't</span>? Select the right option.",
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	imageblock : [{
		imagestoshow : [{

			imgclass : "monkeysolti",
			imgsrc : imgpath + "monkey.png",

		}]

	}],


	questionblock:[{
		questiondata : data.string.p1text17,
		questionclass:" questionpara my_font_very_big",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",
		wrongsrc:imgpath+'wrong.png',
		rightsrc:imgpath +'correct.png'
	}]
},

//slide2

{
	hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "textpara",

	uppertextblock : [{
		textdata : "<span style='color:#f26c00;font-size:1.5em;'>Can</span> or <span style='color:#a64d79;font-size:1.5em;'>Can't</span>? Select the right option.",
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	imageblock : [{
		imagestoshow : [{

			imgclass : "monkeysolti",
			imgsrc : imgpath + "cows.png",

		}]

	}],


	questionblock:[{
		questiondata : data.string.p1text18,
		questionclass:" questionpara my_font_very_big",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",
		wrongsrc:imgpath+'wrong.png',
		rightsrc:imgpath +'correct.png'

	}]
}
,
//slide3
{

	hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "textpara",

	uppertextblock : [{
		textdata : "<span style='color:#BF6221; font-size:1.5em;'>Can</span> or <span style='color:#a64d79;font-size:1.5em;'>Can't</span>? Select the right option.",
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	imageblock : [{
		imagestoshow : [{

			imgclass : "shipsolti",
			imgsrc : imgpath + "sailboats.png",

		}]

	}],


	questionblock:[{
		questiondata : data.string.p1text19,
		questionclass:" questionpara my_font_very_big",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",
		wrongsrc:imgpath+'wrong.png',
		rightsrc:imgpath +'correct.png'

	}]

},
//slide4
{

	hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "textpara",

	uppertextblock : [{
		textdata : "<span style='color:#BF6221; font-size:1.5em;'>Can</span> or <span style='color:#a64d79;font-size:1.5em;'>Can't</span>? Select the right option.",
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	imageblock : [{
		imagestoshow : [{

			imgclass : "preetisoltini",
			imgsrc : imgpath + "pen.png",

		}]

	}],


	questionblock:[{
		questiondata : data.string.p1text20,
		questionclass:" questionpara my_font_very_big",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",
		wrongsrc:imgpath+'wrong.png',
		rightsrc:imgpath +'correct.png'

	}]


},
//slide5
{


	hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "textpara",

	uppertextblock : [{
		textdata : "<span style='color:#BF6221; font-size:1.5em;'>Can</span> or <span style='color:#a64d79;font-size:1.5em;'>Can't</span>? Select the right option.",
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	imageblock : [{
		imagestoshow : [{

			imgclass : "preetisoltini",
			imgsrc : imgpath + "preetisoltini.png",

		}]

	}],


	questionblock:[{
		questiondata : data.string.p1text21,
		questionclass:" questionpara my_font_very_big",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",
		wrongsrc:imgpath+'wrong.png',
		rightsrc:imgpath +'correct.png'

	}]


}

];



$(function() {
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("questioncontent", $("#questioncontent-partial").html());



	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(0).show(0);
			$prevBtn.css('display','none');
		}
		else if (countNext > 0 &&countNext != ($total_page - 1)) {

			$nextBtn.delay(0).show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			play_diy_audio();
			break;
		case 1:
			$nextBtn.hide(0);
			soundPlayer(sound_1);
			$(".wrong").hide(0);
			$(".correct").hide(0);
		   $(".cantpos").click(function(){
				 currentSound.stop();
				play_correct_incorrect_sound(1);
				$(this).css("transform","translate(-100%,-130%)");
				$(".correct").show(0);
				$nextBtn.show(0);
				$(".cantpos, .canpos").css('pointer-events', 'none');
			});
			$(".canpos").click(function(){
				currentSound.stop();
				play_correct_incorrect_sound(0);
				$(".wrong").show(0);
				$(".wrong").css("transform", "translate(-165%, 0%)");
			});
			break;

		case 2:
		$nextBtn.hide(0);
		$(".wrong").hide(0);
		$(".correct").hide(0);
		    $(".cantpos").click(function(){
				$(this).css("transform","translate(-100%,-130%)");
				play_correct_incorrect_sound(1);
				$(".correct").show(0);
				$nextBtn.show(0);
				$(".cantpos, .canpos").css('pointer-events', 'none');
			});
			$(".canpos").click(function(){
				play_correct_incorrect_sound(0);
				$(".wrong").show(0);
				$(".wrong").css("transform", "translate(-170%, 0%)");

			});
		break;
		case 3 :
			$nextBtn.hide(0);
			$(".wrong").hide(0);
			$(".correct").hide(0);
			    $(".canpos").click(function(){
			    	play_correct_incorrect_sound(1);
					$(this).css("transform","translate(20%,-130%)");
					$(".correct").show(0);
					$nextBtn.show(0);
					$(".cantpos, .canpos").css('pointer-events', 'none');
				});
				$(".cantpos").click(function(){
					play_correct_incorrect_sound(0);
					$(".wrong").show(0);

				});
			break;
		case 4 :
			$nextBtn.hide(0);
			$('.canpos').css("transform","translate(140%,0%)");
			$('.cantpos').css("transform","translate(120%,0%)");

			$(".wrong").hide(0);
			$(".correct").hide(0);
		    $(".cantpos").click(function(){
		    	play_correct_incorrect_sound(1);
				$(this).css("transform","translate(-10%,-130%)");
				$(".correct").show(0);
				$nextBtn.show(0);
				$(".cantpos, .canpos").css('pointer-events', 'none');
			});
			$(".canpos").click(function(){
				$(".wrong").show(0);
				play_correct_incorrect_sound(0);
				$(".wrong").css("transform", "translate(-20%, 0%)");
			});
			break;
		case 5 :
			$nextBtn.hide(0);
			$('.canpos').css("transform","translate(150%,0%)");
			$('.cantpos').css("transform","translate(120%,0%)");
			$(".wrong").hide(0);
			$(".correct").hide(0);
		    $(".canpos").click(function(){
		    	play_correct_incorrect_sound(1);
				$(this).css("transform","translate(133%,-120%)");
				$(".correct").show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
				$(".cantpos, .canpos").css('pointer-events', 'none');
			});
			$(".cantpos").click(function(){
				$(".wrong").show(0);
				play_correct_incorrect_sound(0);
				$('.wrong').css("transform","translate(170%,0%)");
			});
		break;
		default:
			$('.digit-box').prop("disabled",true);
			break;
		}
	}

	function soundPlayer(soundId){
		currentSound = soundId;
		currentSound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		if(countNext==1){
		$(".clickclick").on("click",function(){
			countNext++;
		templateCaller();
		});
	}
		navigationController();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// document.addEventListener('xmlLoad', function(e) {
	total_page = content.length;
	templateCaller();
	// });

	});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + ">";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}


/*=====  End of data highlight function  ======*/

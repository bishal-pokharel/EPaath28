var imgpath = $ref + "/images/";
var soundPath = $ref + "/sounds/";

var sound_1 = new buzz.sound((soundPath + "s1_p1.ogg"));
var sound_2_1 = new buzz.sound((soundPath + "canAbility.ogg"));
var sound_2_2 = new buzz.sound((soundPath + "cantAbility.ogg"));

var sound_3_1 = new buzz.sound((soundPath + "s1_p3_1.ogg"));
var sound_3_2 = new buzz.sound((soundPath + "s1_p3_2.ogg"));

var sound_4_1 = new buzz.sound((soundPath + "s1_p4_1.ogg"));
var sound_4_2 = new buzz.sound((soundPath + "s1_p4_2.ogg"));

var sound_5_1 = new buzz.sound((soundPath + "s1_p5_1.ogg"));
var sound_5_2 = new buzz.sound((soundPath + "s1_p5_2.ogg"));

var sound_6_1 = new buzz.sound((soundPath + "s1_p6_1.ogg"));
var sound_6_2 = new buzz.sound((soundPath + "s1_p6_2.ogg"));
var currentSound = sound_1;


var content = [

//slide0

{
	contentblockadditionalclass: "firstpagebg",
	hasheaderblock : false,
	//contentblockadditionalclass:false,

uppertextblockadditionalclass:'vertical-horizontal-center',

	uppertextblock : [{
		textdata : data.string.chaptertitle1,
		textclass : 'lesson-title1'
	},
	{
		textdata : data.string.chaptertitle2,
		textclass : 'lesson-title2'
	}
	],
	imageblockadditionalclass: 'imageclass',
	imageblock : [{
		imagestoshow : [{

			imgclass : "elephant",
			imgsrc : imgpath + "elephant.gif"

		},
		{
			imgclass : "kangaroo",
			imgsrc : imgpath + "kangaroo.gif"

		}]
		}]
},

//slide1

{

	contentblockadditionalclass: "secondpagebg",
	hasheaderblock:false,

	//contentblockadditionalclass:false,
	uppertextblockadditionalclass : "uppara",

	uppertextblock : [{
		textdata : data.string.p1text2,
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],

	flipcard : [{
		flipcards : [{
      extracardclass:"card1",
			front: "front",
			back: "back",
			frontfirstdata:data.string.p1text25,
			frontseconddata:data.string.p1text6,
			backfirstdata:data.string.p1text25,
			backseconddata:data.string.p1text8,
			backthirddata:data.string.p1text9

		},
		{ extracardclass:"card2",
			front: "front",
		back: "back",
		frontfirstdata:data.string.p1text26,
		frontseconddata:data.string.p1text7,
		backfirstdata:data.string.p1text26,
		backseconddata:data.string.p1text22,
		backthirddata:data.string.p1text23

		}]

	}]


},

//slide2

{
    contentblockadditionalclass: "secondpagebg",
    hasheaderblock:false,

	//contentblockadditionalclass:'bg_blue',
	//uppertextblockadditionalclass : "uppara",

	/*uppertextblock : [{
		textdata : data.string.p1text3,
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}],*/
    imageblockadditionalclass: "full",
	imageblock : [{
		imagestoshow : [{

			imgclass : "animatebird",
			imgsrc : imgpath + "animate-bird.gif",

		},
		{

			imgclass : "harryanimation",
			imgsrc : imgpath + "HarryAnimation.gif",

		}],
	imagelabels : [{
		imagelabelclass:"birdlabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label1",
		imagelabeldata:data.string.p1text4,
	},{
		imagelabelclass:"doglabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label2",
		imagelabeldata:data.string.p1text5,
	}]

	}],

	/*lowertextblockadditionalclass:'downpara',
	lowertextblock:[{textdata : data.string.p1text1,
		textclass:"text_red",
		datahighlightflag:false,
		datahighlightcustomclass:"text_green",

	}]*/
},
//slide3
{
    contentblockadditionalclass: "secondpagebg",

	hasheaderblock:false,
	 imageblockadditionalclass: "full",
	imageblock : [{
		imagestoshow : [{

			imgclass : "fish",
			imgsrc : imgpath + "fish.gif",

		},
		{

			imgclass : "mouse",
			imgsrc : imgpath + "mouse.gif",

		}],
	imagelabels : [{
		imagelabelclass:"fishlabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label1",
		imagelabeldata:data.string.p1text10,
	},{
		imagelabelclass:"mouselabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label2",
		imagelabeldata:data.string.p1text11,
	}]

	}]

},
//slide4
{
    contentblockadditionalclass: "secondpagebg",
	hasheaderblock:false,
	 imageblockadditionalclass: "full",
	imageblock : [{
		imagestoshow : [{

			imgclass : "fish",
			imgsrc : imgpath + "Running-Mouse.gif",

		},
		{

			imgclass : "snail",
			imgsrc : imgpath + "snail.gif",

		}],
	imagelabels : [{
		imagelabelclass:"fishlabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label1",
		imagelabeldata:data.string.p1text12,
	},{
		imagelabelclass:"mouselabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label2",
		imagelabeldata:data.string.p1text13,
	}]

	}]

},
//slide5
{
    contentblockadditionalclass: "secondpagebg",
	hasheaderblock:false,
	 imageblockadditionalclass: "full",
	imageblock : [{
		imagestoshow : [{

			imgclass : "ant",
			imgsrc : imgpath + "ant.gif",

		},
		{

			imgclass : "banana",
			imgsrc : imgpath + "snake.gif",

		}],
	imagelabels : [{
		imagelabelclass:"antlabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label1",
		imagelabeldata:data.string.p1text14,
	},
	{
		imagelabelclass:"bananalabel fade_in my_font_ultra_big",
		datahighlightflag:true,
		datahighlightcustomclass:"text_label2",
		imagelabeldata:data.string.p1text15,
	}]

	}]

}

];



$(function() {
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	 var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var my_timeout = null;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("flipcontent", $("#flipcontent-partial").html());



	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.show(0);
			$prevBtn.css('display','none');
		} else if (countNext == $total_page - 1) {
			// $nextBtn.css('display', 'none');
			// $prevBtn.show(0);
			// // if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else{
			$nextBtn.hide(0);
			$prevBtn.hide(0);
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		switch (countNext) {
		case 0:
			soundPlayer(sound_1, 1);
		break;
		case 1:
			clearTimeout(my_timeout);
			sound_2_1.play();
			sound_2_1.bind('ended', function(){
				sound_2_2.play();
				sound_2_2.bind('ended', function(){
					$(".card1").click(function(){
						$(this).toggleClass("flipped");
					});
					$(".card2").click(function(){
		       			$(this).toggleClass("flipped");
					});
					$nextBtn.show(0);
				});
			});
			break;
		case 2:
			$(".harryanimation").hide(0);
			$(".birdlabel").hide(0);
			$(".doglabel").hide(0);
			$(".birdlabel").delay(6000).show(0, function(){
				soundPlayer(sound_3_1, 0);
			});
			$(".harryanimation").delay(8000).show(0);
			$(".doglabel").delay(14000).show(0, function(){
				soundPlayer(sound_3_2, 1);
			});
			clearTimeout(my_timeout);
			$prevBtn.show(0);
			break;
		case 3 :
			$(".mouse").hide(0);
			$(".mouselabel").hide(0);
				soundPlayer(sound_4_1, 0);
			$(".mouse").delay(3000).show(0);
			$(".mouselabel").delay(4000).show(0, function(){
				soundPlayer(sound_4_2, 1);
			});
			clearTimeout(my_timeout);
			$prevBtn.show(0);
		break;
		case 4 :
			$(".snail").hide(0);
			$(".mouselabel").hide(0);
				soundPlayer(sound_5_1, 0);
			$(".snail").delay(3000).show(0);
			$(".mouselabel").delay(4000).show(0, function(){
				soundPlayer(sound_5_2, 1);
			});
			clearTimeout(my_timeout);
			$prevBtn.show(0);
		break;
		case 5 :
			$(".bananalabel").hide(0);
				soundPlayer(sound_6_1, 0);
			$(".bananalabel").delay(8000).show(0, function(){
				soundPlayer(sound_6_2, 1);
			});;
			clearTimeout(my_timeout);
			// my_timeout=setTimeout(function(){
			// 	ole.footerNotificationHandler.pageEndSetNotification();
			// }, 9000);
			$prevBtn.show(0);

		break;
		default:
			$('.digit-box').prop("disabled",true);
			break;
		}
	}
	function soundPlayer(soundId, nextBtnFlag){
		currentSound = soundId;
		currentSound.play();
		currentSound.bind('ended', function(){
			// alert(nextBtnFlag);
			if(nextBtnFlag){
				countNext!=5?$nextBtn.show(0):ole.footerNotificationHandler.pageEndSetNotification();

			}
			else
				$nextBtn.hide(0)
			// nextBtnFlag?$nextBtn.show(0):$nextBtn.hide(0);
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		if(countNext==1){
		$(".clickclick").on("click",function(){
			countNext++;
		templateCaller();
		});
	}
		navigationController();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function() {
		countNext++;
		currentSound.stop();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// document.addEventListener('xmlLoad', function(e) {
	total_page = content.length;
	templateCaller();
	// });

	});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + ">";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}


/*=====  End of data highlight function  ======*/

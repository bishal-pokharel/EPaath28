var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var sound_dg1 = new buzz.sound(soundAsset + "s4_p1.ogg");
var sound_dg2 = new buzz.sound(soundAsset + "s4_p2.ogg");
var sound_dg3 = new buzz.sound(soundAsset + "s4_p3.ogg");
var sound_dg4 = new buzz.sound(soundAsset + "s4_p4.ogg");
var sound_dg5 = new buzz.sound(soundAsset + "s4_p5.ogg");
var sound_dg6 = new buzz.sound(soundAsset + "s4_p6.ogg");
var sound_dg7 = new buzz.sound(soundAsset + "s4_p7.ogg");
var sound_dg8 = new buzz.sound(soundAsset + "s4_p8.ogg");
var sound_dg9 = new buzz.sound(soundAsset + "s4_p9.ogg");
var sound_dg10 = new buzz.sound(soundAsset + "s4_p10.ogg");
var sound_dg11 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg12 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg13 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg14 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg15 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg16 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg17 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg18 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg19 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg20 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg21 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg22 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg23 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg24 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg25 = new buzz.sound(soundAsset + "1dg4.ogg");
var sound_dg26 = new buzz.sound(soundAsset + "1dg4.ogg");

var soundsarr = [
  sound_dg1,
  sound_dg2,
  sound_dg3,
  sound_dg4,
  sound_dg5,
  sound_dg6,
  sound_dg7,
  sound_dg8,
  sound_dg9,
  sound_dg10,
  sound_dg11,
  sound_dg12,
  sound_dg13,
  sound_dg14,
  sound_dg15,
  sound_dg16,
  sound_dg17,
  sound_dg18,
  sound_dg19,
  sound_dg20,
  sound_dg21,
  sound_dg22,
  sound_dg23,
  sound_dg24,
  sound_dg25,
  sound_dg26
];

var current_sound = sound_dg1;

var content = [
  //slide0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text3,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana02.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30",
        minclass: "hr-6",
        dotclass: ""
      }
    ]
  },
  //slide1
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text4,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana02.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30",
        minclass: "hr-6",
        halfclassclass: "patrickhand my_font_big",
        halfdata: data.string.half
      }
    ]
  },
  //slide2
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text5,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "boy boy-1",
            imgsrc: imgpath + "rahul01.png"
          },
          {
            imgclass: "girl girl-1",
            imgsrc: imgpath + "anjana04.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30",
        minclass: "hr-6"
      }
    ]
  },
  //slide3
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text6,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana02.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30",
        minclass: "hr-6"
      }
    ]
  },
  //slide4
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text7,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "boy boy-1",
            imgsrc: imgpath + "rahul01.png"
          },
          {
            imgclass: "girl girl-1",
            imgsrc: imgpath + "anjana04.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30",
        minclass: "hr-6"
      }
    ]
  },
  //slide5
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text8,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana02.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clock.png",
        hrclass: "hr-7-30 hl-hand",
        minclass: "hr-6"
      }
    ]
  },
  //slide6
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text9,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana03.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clocknew02.png",
        clockframesrc: imgpath + "clocknew01.png",
        clocktextsrc: imgpath + "numbers.png",
        hrclass: "hr-7-30 hl-hand",
        dottedclass: "hr-7-30",
        minclass: "hr-6",
        intervalclass: "interval78",
        intervalsrc: imgpath + "yellowpatch.png"
      }
    ]
  },

  //slide7
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text10,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana01.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clocknew02.png",
        clockframesrc: imgpath + "clocknew01.png",
        clocktextsrc: imgpath + "numbers.png",
        hrclass: "hr-7-30 hl-hand",
        dottedclass: "hr-7-30",
        minclass: "hr-6",
        intervalclass: "interval78",
        intervalsrc: imgpath + "yellowpatch.png"
      }
    ]
  },

  //slide8
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text11,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana03.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clockbody.png",
        clocktextclass: "",
        clocktextsrc: imgpath + "numbers.png",
        hrclass: "hr-7-30",
        minclass: "hr-6",
        halfclassclass: "patrickhand my_font_big",
        halfdata: data.string.half,
        hashl: true,
        hlclass: "hl-hl-2"
      }
    ]
  },

  //slide9
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p6text12,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "i"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana03.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clockbody.png",
        clocktextclass: "",
        clocktextsrc: imgpath + "numbers.png",
        hrclass: "hr-7-30",
        minclass: "hr-6",
        hashl: true,
        hlclass: "hl-hl-2",
        halfclassclass: "patrickhand my_font_big",
        halfdata: data.string.half
      }
    ]
  }
];

$(function() {
  var $board = $(".board");

  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var $total_page = content.length;

  var timeoutvar = null;
  var timeoutvar2 = null;
  var timeoutvar3 = null;

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);
    // vocabcontroller.findwords(countNext);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch (countNext) {
      default:
        if (countNext > 0) {
          $prevBtn.show(0);
        }
        sound_and_nav(soundsarr[countNext], click_dg, ".dg-1");
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar3 = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }

  function sound_and_nav(sound_data, clickfunction, a) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
    current_sound.bindOnce("ended", function() {
      if (typeof clickfunction != "undefined") {
        clickfunction(a, sound_data);
      }
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    });
  }
  function click_dg(dg_class, audio) {
    $(dg_class).click(function() {
      sound_player(audio);
    });
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    switch (countNext) {
      default:
        current_sound.stop();
        clearTimeout(timeoutvar);
        clearTimeout(timeoutvar2);
        clearTimeout(timeoutvar3);
        timeoutvar = null;
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    current_sound.stop();
    clearTimeout(timeoutvar);
    clearTimeout(timeoutvar2);
    clearTimeout(timeoutvar3);
    timeoutvar = null;
    countNext--;
    templateCaller();
  });

  total_page = content.length;
  templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

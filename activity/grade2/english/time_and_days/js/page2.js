var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var sound_dg1 = new buzz.sound(soundAsset + "s3_p6.ogg");

var current_sound = sound_dg1;

var content1 = [
  //slide0
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-diy",

    uppertextblockadditionalclass:
      "center-text my_font_super_big bold-text patrickhand",
    uppertextblock: [
      {
        textdata: data.string.diytext,
        textclass: "diy-text"
      }
    ]
  }
];
var content2 = [
  //slide 1
  {
    contentblockadditionalclass: "monkey-bg",
    exerciseblock: [
      {
        textclass: "instruction my_font_big",
        questiondata: data.string.p5text1,
        option: [
          {
            optioncontainer: "option class-1",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-4",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-2",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-6",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-3",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-10",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-4",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-1",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //slide 2
  {
    contentblockadditionalclass: "monkey-bg",
    exerciseblock: [
      {
        textclass: "instruction my_font_big",
        questiondata: data.string.p5text2,
        option: [
          {
            optioncontainer: "option class-1",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-5",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-2",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-9",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-3",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-7",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-4",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-4",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //slide 3
  {
    contentblockadditionalclass: "monkey-bg",
    exerciseblock: [
      {
        textclass: "instruction my_font_big",
        questiondata: data.string.p5text3,
        option: [
          {
            optioncontainer: "option class-1",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-1",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-2",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-5",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-3",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-11",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-4",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-8",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //slide 4
  {
    contentblockadditionalclass: "monkey-bg",
    exerciseblock: [
      {
        textclass: "instruction my_font_big",
        questiondata: data.string.p5text4,
        option: [
          {
            optioncontainer: "option class-1",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-10",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-2",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-1",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-3",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-5",
            minclass: "hr-12",
            dotclass: ""
          },
          {
            optioncontainer: "option class-4",
            clockblockclass: "clock-option",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clockbody.png",
            clocktextclass: "",
            clocktextsrc: imgpath + "numbers.png",
            hrclass: "hr-2",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  }
];
var content3 = [
  //slide5
  {
    hasheaderblock: false,
    contentblocknocenteradjust: true,
    contentblockadditionalclass: "bg-blue",

    extratextblock: [
      {
        textdata: data.string.p5text5,
        textclass:
          "template-dialougebox2-top-yellow dg-1 patrickhand my_font_big",
        datahighlightflag: true,
        datahighlightcustomclass: "ul"
      }
    ],

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "girl girl-3",
            imgsrc: imgpath + "anjana02.png"
          }
        ]
      }
    ],

    clockblock: [
      {
        clockblockclass: "clock-right",
        clockbodyclass: "",
        clockbodysrc: imgpath + "clockbody.png",
        clocktextclass: "",
        clocktextsrc: imgpath + "numbers.png",
        hrclass: "hr-7",
        minclass: "hr-12",
        dotclass: ""
      }
    ]
  }
];

content2.shufflearray();
var content = content1.concat(content2);
content = content.concat(content3);

$(function() {
  var $board = $(".board");

  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var countNext = 0;
  var $total_page = content.length;

  var timeoutvar = null;

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  /*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
  Handlebars.registerPartial(
    "definitioncontent",
    $("#definitioncontent-partial").html()
  );
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

  // controls the navigational state of the program
  // next btn is disabled for this page
  function navigationController(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
    // if lastpageflag is true
    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);

    $board.html(html);
    // vocabcontroller.findwords(countNext);
    loadTimelineProgress($total_page, countNext + 1);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    countNext == 0 ? play_diy_audio() : "";

    switch (countNext) {
      case 1:
      case 2:
      case 3:
      case 4:
        /*for randomizing the options*/
        var option_position = [1, 2, 3, 4];
        option_position.shufflearray();
        for (var op = 0; op < 4; op++) {
          $(".main-container")
            .eq(op)
            .addClass("option-pos-" + option_position[op]);
        }
        //top-left
        $(".option-pos-1").hover(
          function() {
            $(".center-sundar").attr("src", "images/sundar/top-right.png");
            $(".center-sundar").css("transform", "scaleX(-1)");
          },
          function() {}
        );
        //bottom-left
        $(".option-pos-3").hover(
          function() {
            $(".center-sundar").attr("src", "images/sundar/bottom-right.png");
            $(".center-sundar").css("transform", "scaleX(-1)");
          },
          function() {}
        );
        //top-right
        $(".option-pos-2").hover(
          function() {
            $(".center-sundar").attr("src", "images/sundar/top-right.png");
            $(".center-sundar").css("transform", "none");
          },
          function() {}
        );
        //bottom-right
        $(".option-pos-4").hover(
          function() {
            $(".center-sundar").attr("src", "images/sundar/bottom-right.png");
            $(".center-sundar").css("transform", "none");
          },
          function() {}
        );

        var wrong_clicked = 0;
        var correct_images = [
          "correct-1.png",
          "correct-2.png",
          "correct-3.png"
        ];
        $(".option").click(function() {
          if ($(this).hasClass("class-1")) {
            var rand_img = Math.floor(Math.random() * correct_images.length);
            $(".option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4").off(
              "mouseenter mouseleave"
            );
            $(".center-sundar").attr(
              "src",
              "images/sundar/" + correct_images[rand_img]
            );
            $(".option").css("pointer-events", "none");
            play_correct_incorrect_sound(1);
            $(this).addClass("correct-ans");
            $(this)
              .children(".correct-icon1")
              .show(0);
            wrong_clicked = 0;
            if (countNext != total_page) $nextBtn.show(0);
          } else {
            var classname_monkey = $(this)
              .parent()
              .attr("class")
              .replace(/main-container/, "");
            classname_monkey = classname_monkey.replace(/ /g, "");
            $("." + classname_monkey).off("mouseenter mouseleave");
            if (wrong_clicked == 0) {
              $(".center-sundar").attr("src", "images/sundar/incorrect-1.png");
            } else if (wrong_clicked == 1) {
              $(".center-sundar").attr("src", "images/sundar/incorrect-2.png");
            } else {
              $(".center-sundar").attr("src", "images/sundar/incorrect-3.png");
            }
            play_correct_incorrect_sound(0);
            $(this).addClass("incorrect-ans");
            $(this)
              .children(".incorrect-icon1")
              .show(0);
            wrong_clicked++;
          }
        });
        break;
      case 5:
        $prevBtn.show(0);
        sound_player(sound_dg1);
        // sound_and_nav(sound_dg1, click_dg, ".dg-1");
        break;
      default:
        nav_button_controls(100);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
    ole.footerNotificationHandler.pageEndSetNotification();
  }

  function sound_and_nav(sound_data, clickfunction, a) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
    current_sound.bindOnce("ended", function() {
      if (typeof clickfunction != "undefined") {
        clickfunction(a, sound_data);
      }
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    });
  }
  function click_dg(dg_class, audio) {
    $(dg_class).click(function() {
      sound_player(audio);
    });
  }

  function templateCaller() {
    //convention is to always hide the prev and next button and show them based
    //on the convention or page index
    $prevBtn.hide(0);
    $nextBtn.hide(0);
    navigationController();

    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    switch (countNext) {
      default:
        current_sound.stop();
        clearTimeout(timeoutvar);
        timeoutvar = null;
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    current_sound.stop();
    clearTimeout(timeoutvar);
    timeoutvar = null;
    countNext--;
    templateCaller();
  });

  total_page = content.length;
  templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
  //check if $highlightinside is provided
  typeof $highlightinside !== "object"
    ? alert(
        "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
      )
    : null;

  var $alltextpara = $highlightinside.find("*[data-highlight='true']");
  var stylerulename;
  var replaceinstring;
  var texthighlightstarttag;
  var texthighlightendtag = "</span>";

  if ($alltextpara.length > 0) {
    $.each($alltextpara, function(index, val) {
      /*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
      $(this).attr(
        "data-highlightcustomclass"
      ) /*if there is data-highlightcustomclass defined it is true else it is not*/
        ? (stylerulename = $(this).attr("data-highlightcustomclass"))
        : (stylerulename = "parsedstring");

      texthighlightstarttag = "<span class = " + stylerulename + " >";

      replaceinstring = $(this).html();
      replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
      replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

      $(this).html(replaceinstring);
    });
  }
}

/*=====  End of data highlight function  ======*/

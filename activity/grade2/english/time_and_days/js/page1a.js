var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg1 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "1dg2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "1dg3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg15 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg16 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg17 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg18 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_dg19 = new buzz.sound((soundAsset + "1dg4.ogg"));

var soundsarr = [
				sound_dg1, sound_dg2, sound_dg3, sound_dg4, 
				sound_dg6, sound_dg7, sound_dg8, sound_dg9, sound_dg10, 
				sound_dg11, sound_dg12, sound_dg13, sound_dg14, sound_dg15, 
				sound_dg16, sound_dg17, sound_dg18, sound_dg19,  
				];
				

var sound_t1 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t2 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t3 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t4 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t5 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t6 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t7 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t8 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t9 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t10 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t11 = new buzz.sound((soundAsset + "1dg4.ogg"));
var sound_t12 = new buzz.sound((soundAsset + "1dg4.ogg"));

var soundstime = [
					sound_t1, sound_t2, sound_t3, sound_t4, sound_t5, sound_t6,
					sound_t7, sound_t8, sound_t9, sound_t10, sound_t11, sound_t12
				];
				
var current_sound = sound_dg1;

var content = [
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text10,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana03.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: 'hl-numbers',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'its_hidden',
			minclass: 'hr-12 its_hidden',
			dotclass: ''
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text14,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana02.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: ''
		}]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text15,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana02.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			hashl: true,
			hlclass: 'hl-hl-2',
			dotclass: ''
		}]
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text16,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana03.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: '',
			hashl: true,
			hlclass: 'hl-hl',
			oclockclass: 'top-oclock patrickhand my_font_big',
			oclockdata: data.string.o12,
		}]
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4add4,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana03.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: '',
			hashl: true,
			hlclass: 'hl-hl',
			oclockclass: 'top-oclock patrickhand my_font_big',
			oclockdata: data.string.o12,
		}]
	},
	//slide15	
	{ 	
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		extratextblock : [{ 			
			textdata : data.string.p4text12, 			
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big', 			
			datahighlightflag: true, 			
			datahighlightcustomclass: 'ul' 		
		}], 				
		imageblock : [{ 			
			imagestoshow : [ 				
				{ 					
					imgclass : "girl girl-3", 					
					imgsrc : imgpath + "anjana02.png", 				
				} 			
			], 		
		}], 				
		clockblock :[{
			clockblockclass : 'clock-right',
			clockbodyclass : '',
			clockbodysrc : imgpath + 'clockbody.png',
			clocktextclass : '',
			clocktextsrc : imgpath + 'numbers.png',
			hrclass : 'hr-7',
			minclass : 'hr-12',
			dotclass : ''
		}]
	}, 
	//slide16 	
	{ 
		hasheaderblock : false, 		
		contentblocknocenteradjust : true, 		
		contentblockadditionalclass : 'bg-blue', 			
		extratextblock : [{ 			
			textdata : data.string.p4text13, 			
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big', 			
			datahighlightflag: true, 			
			datahighlightcustomclass: 'ul' 		
		}], 				
		imageblock : [{ 			
			imagestoshow : [ 				
				{ 					
					imgclass : "girl girl-3", 					
					imgsrc : imgpath + "anjana02.png", 				
				} 			
			], 		
		}], 				
		clockblock :[{
			clockblockclass : 'clock-right',
			clockbodyclass : '',
			clockbodysrc : imgpath + 'clockbody.png',
			clocktextclass : 'fronttext',
			clocktextsrc : imgpath + 'numbers.png',
			hrclass : 'hr-7',
			hashl : true,
			dottedclass : 'hr-7',
			hlclass : ' hl-hl-3',
			minclass : 'hr-12',
			dotclass : ''
		}]
	},

	//slide17
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text18,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana03.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: '',
			oclockclass: 'top-oclock patrickhand my_font_big',
			oclockdata: data.string.o12,
		}]
	},
	//slide18
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text17,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big its_hidden',
			datahighlightflag: true,
			datahighlightcustomclass: 'hr-number'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana03.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7 ',
			minclass: 'hr-12',
			dotclass: '',
			oclockclass: 'top-oclock patrickhand my_font_big',
			oclockdata: data.string.o12,
		}]
	},
	//slide19
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
	
		extratextblock : [{
			textdata : data.string.p4text19,
			textclass : 'template-dialougebox2-top-yellow dg-1 patrickhand my_font_big',
			datahighlightflag: true,
			datahighlightcustomclass: 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "girl girl-3",
					imgsrc : imgpath + "anjana02.png",
				}
			],
		}],
		
		clockblock : [{
			clockblockclass: 'clock-right',
			clockbodyclass: '',
			clockbodysrc: imgpath + 'clockbody.png',
			clocktextclass: '',
			clocktextsrc: imgpath + 'numbers.png',
			hrclass: 'hr-7',
			minclass: 'hr-12',
			dotclass: '',
			oclockclass: 'top-oclock patrickhand my_font_big',
			oclockdata: data.string.o12,
		}]
	},
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	
	var timeoutvar = null;
	var timeoutvar2 = null;
	var timeoutvar3 = null;
	var timeouts = [];
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch(countNext) {
			case 8:
				$prevBtn.show(0);
				var i =1;
				function doSetTimeout(i) {
					timeouts.push(setTimeout(function(){
						$('.hr-hand').removeClass('hr-anim-'+parseInt(i)).addClass('hr-anim-'+parseInt(i+1));
							$('.min-hand').removeClass('min-anim-'+parseInt(i%2)).addClass('min-anim-'+parseInt((i+1)%2));
							$('.hr-number').html(i+1);
							$('.dg-1').fadeIn(100, function(){
								if(i==11){
									sound_no_nav(soundstime[i], '.dg-1', true);
								}
								sound_no_nav(soundstime[i], '.dg-1', false);
							});
							if(i==11){
								$nextBtn.show(0);
							}
					}, i*3000));
				}
				for(var i=0; i<12; i++){
					doSetTimeout(i);
				}
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				sound_and_nav(soundsarr[countNext], click_dg, '.dg-1');
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar3 = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_no_nav(sound_data, a, not_hide){
		current_sound.stop();
		current_sound = sound_data;
		clearTimeout(timeoutvar3);
		timeoutvar3 = setTimeout(function(){
			current_sound.play();
		}, 1000);
		current_sound.bindOnce('ended', function(){
			if(!not_hide){
				$(a).hide(0);
			}
		});
	}
	
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}

	$nextBtn.on("click", function() {
		switch(countNext){
			case 17:
				for (var i=0; i<timeouts.length; i++) {
					clearTimeout(timeouts[i]);
				}
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
			default:
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		switch(countNext){
			case 17:
				for (var i=0; i<timeouts.length; i++) {
					clearTimeout(timeouts[i]);
				}
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext++;
				templateCaller();
				break;
			default: 
				current_sound.stop();
				clearTimeout(timeoutvar);
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar3);
				timeoutvar = null;
				countNext--;
				templateCaller();
				break;
		}
	});
	
	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  //slide_1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text13
      }
    ]
  },
  //slide_2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text14
      }
    ]
  },
  //slide_3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text15
      }
    ]
  },
  //slide_4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text16
      }
    ]
  },
  //slide_5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text17
      }
    ]
  },
  //slide_6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text18
      }
    ],
    svgblock: [
      {
        svg_block: "svg_clock"
      }
    ]
  },
  //slide_7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text20
      }
    ],
    optionsblock: [
      {
        option: [
          {
            textclass: "fnt_4 class_1",
            textdata: data.string.three_thrty
          },
          {
            textclass: "fnt_4",
            textdata: data.string.four_thrty
          }
        ]
      }
    ]
  },
  //slide_8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text22
      }
    ]
  },
  //slide_9
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock_png",
            imgsrc: ""
          }
        ]
      }
    ],
    blank_div: [
      {
        div_class: "hour_div half_ten"
      },
      {
        div_class: "min_div half_past"
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text20
      }
    ],
    optionsblock: [
      {
        option: [
          {
            textclass: "fnt_4 class_1",
            textdata: data.string.ten_thrty
          },
          {
            textclass: "fnt_4",
            textdata: data.string.eleven_thrty
          }
        ]
      }
    ]
  },
  //slide_10
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana01",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock_png",
            imgsrc: ""
          }
        ]
      }
    ],
    blank_div: [
      {
        div_class: "hour_div half_five"
      },
      {
        div_class: "min_div half_past"
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p6text20
      }
    ],
    optionsblock: [
      {
        option: [
          {
            textclass: "fnt_4 class_1",
            textdata: data.string.five_thrty
          },
          {
            textclass: "fnt_4",
            textdata: data.string.six_thrty
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      {
        id: "anjana",
        src: imgpath + "anjana.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana01",
        src: imgpath + "anjana01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana02",
        src: imgpath + "anjana02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana03",
        src: imgpath + "anjana03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana04",
        src: imgpath + "anjana04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana04",
        src: imgpath + "anjana04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rahul",
        src: imgpath + "rahul.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rahul01",
        src: imgpath + "rahul01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock",
        src: imgpath + "clock_3.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_3",
        src: imgpath + "clock_3.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_1",
        src: imgpath + "dialog_box_1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_2",
        src: imgpath + "dialog_box_2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_3",
        src: imgpath + "dialog_box_3.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "s3a_p0", src: soundAsset + "s5_p1.ogg" },
      { id: "s3a_p1", src: soundAsset + "s5_p2.ogg" },
      { id: "s3a_p2", src: soundAsset + "s5_p3.ogg" },
      { id: "s3a_p3", src: soundAsset + "s5_p4.ogg" },
      { id: "s3a_p4", src: soundAsset + "s5_p5.ogg" },
      { id: "s3a_p5", src: soundAsset + "s5_p6.ogg" },
      { id: "s3a_p5_1", src: soundAsset + "s5_p6_1.ogg" },
      { id: "s3a_p6", src: soundAsset + "s5_p7.ogg" },
      { id: "s3a_p7", src: soundAsset + "s5_p8.ogg" },
      { id: "s3a_p8", src: soundAsset + "s5_p9.ogg" },
      { id: "s3a_p9", src: soundAsset + "s5_p10.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);

    function load_svg(img_id, numclick_flag, cor_ans) {
      var s = Snap("#svg_clock");
      var svg = Snap.load(preload.getResult(img_id).src, function(
        loadedFragment
      ) {
        s.append(loadedFragment);

        $("#nmbers")
          .children()
          .click(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            var circle = $("#circle" + id_contents[1]);
            // to control the clickable numbers as only few should be allowed to click
            if ($.inArray(id_contents[1], cor_ans) != -1) {
              // put the first item of cor_ans array as the correct ans
              if (parseInt(id_contents[1]) == cor_ans[0]) {
                $(this)
                  .children()
                  .attr("class", "correct");
                $("#nmbers").css("pointer-events", "none");
                correct_behaviors();
                $(this)
                  .children()
                  .css("fill", "#fff");
                $(circle).attr("class", "circle_correct");
              } else {
                $(this)
                  .children()
                  .attr("class", "incorrect");
                $(circle).attr("class", "circle_incorrect");
                $(this)
                  .children()
                  .css("fill", "#000");
                play_correct_incorrect_sound(0);
              }
            }
          });

        $("#nmbers")
          .children()
          .mouseover(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            if ($.inArray(id_contents[1], cor_ans) != -1) {
              $(this).css("cursor", "pointer");
              $("#circle" + id_contents[1]).css("display", "block");
            }
          })
          .mouseout(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            $("#circle" + id_contents[1]).css("display", "none");
          });
      });
    }

    function correct_behaviors() {
      if (countNext == 5) {
        $(".sp_text").html(eval("data.string.p6text19"));
        setTimeout(function() {
          sound_player("s3a_p" + countNext + "_1", 1);
        }, 1500);
      } else {
        $(".sp_text").html(eval("data.string.p4text13"));
        setTimeout(function() {
          sound_player("demo_sound", 1);
        }, 1500);
      }
    }

    /*for randomizing the options*/
    function randomize(parent) {
      // alert(parent);
      var parent = $(parent);
      var divs = parent.children();
      while (divs.length) {
        parent.append(
          divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
        );
      }
    }
    function mcq() {
      $(".opn_txt").click(function() {
        if ($(this).hasClass("class_1")) {
          $(this).addClass("correct");
          play_correct_incorrect_sound(1);
          $(this)
            .siblings(".correct-icon")
            .show(0);
          $(".opn_txt").css("pointer-events", "none");
          nav_button_controls(0);
        } else {
          $(this).addClass("incorrect");
          play_correct_incorrect_sound(0);
          $(this)
            .siblings(".incorrect-icon")
            .show(0);
        }
      });
    }
    $();
    switch (countNext) {
      case 5:
        load_svg("clock_3", 1, ["3", "4"]);
        sound_player("s3a_p" + countNext, 0);
        break;
      case 6:
      case 8:
      case 9:
        sound_player("s3a_p" + countNext, 0);
        randomize(".optioncontainer");
        mcq();
        break;
      default:
        sound_player("s3a_p" + countNext, 1);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      next ? navigationcontroller() : "";
    });
  }

  function sound_player_duo(sound_id, sound_id_2) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    //current_sound_2 = createjs.Sound.play(sound_id_2);
    current_sound.play();
    current_sound.on("complete", function() {
      $(".dotext").show(0);
      sound_player(sound_id_2);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    if (countNext == 0) navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";

var content = [
  //slide_1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4text10
      }
    ]
  },
  //slide_2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana02",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4text14
      }
    ],
    svgblock: [
      {
        svg_block: "svg_clock"
      }
    ]
  },
  //slide_3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4text16
      }
    ]
  },
  //slide_4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4add4
      }
    ]
  },
  //slide_5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4add5
      }
    ],
    svgblock: [
      {
        svg_block: "svg_clock"
      }
    ]
  },
  //slide_6---> this is the naimantion page
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana03",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock_png",
            imgsrc: ""
          }
        ]
      }
    ],
    blank_div: [
      {
        div_class: "hour_div"
      },
      {
        div_class: "min_div"
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        datahighlightflag: true,
        datahighlightcustomclass: "time_now",
        textdata: data.string.p4text17
      }
    ]
  },
  //slide_7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "thebg1",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "anjana_1",
            imgid: "anjana02",
            imgsrc: ""
          },
          {
            imgclass: "clock clock_big",
            imgid: "clock",
            imgsrc: ""
          }
        ]
      }
    ],
    speechbox: [
      {
        speechbox: "sp-1",
        imgclass: "sp_img",
        imgid: "dialog_box_1",
        imgsrc: "",
        textclass: "sp_text",
        textdata: data.string.p4text19
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      {
        id: "anjana",
        src: imgpath + "anjana.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana01",
        src: imgpath + "anjana01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana02",
        src: imgpath + "anjana02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana03",
        src: imgpath + "anjana03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana04",
        src: imgpath + "anjana04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "anjana04",
        src: imgpath + "anjana04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rahul",
        src: imgpath + "rahul.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rahul01",
        src: imgpath + "rahul01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock",
        src: imgpath + "clock.svg",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock_png",
        src: imgpath + "clock.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_1",
        src: imgpath + "dialog_box_1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_2",
        src: imgpath + "dialog_box_2.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dialog_box_3",
        src: imgpath + "dialog_box_3.png",
        type: createjs.AbstractLoader.IMAGE
      },
      // sounds
      { id: "s1a_p0", src: soundAsset + "s2_p1.ogg" },
      { id: "s1a_p1", src: soundAsset + "s2_p2.ogg" },
      { id: "s1a_p1_1", src: soundAsset + "s2_p2_1.ogg" },
      { id: "s1a_p2", src: soundAsset + "s2_p3.ogg" },
      { id: "s1a_p3", src: soundAsset + "s2_p4.ogg" },
      { id: "s1a_p4", src: soundAsset + "s2_p5.ogg" },
      { id: "s1a_p4_1", src: soundAsset + "s2_p5_1.ogg" },
      { id: "s1a_p5_1", src: soundAsset + "s2_p6_1.ogg" },
      { id: "s1a_p5_2", src: soundAsset + "s2_p6_2.ogg" },
      { id: "s1a_p5_3", src: soundAsset + "s2_p6_3.ogg" },
      { id: "s1a_p5_4", src: soundAsset + "s2_p6_4.ogg" },
      { id: "s1a_p5_5", src: soundAsset + "s2_p6_5.ogg" },
      { id: "s1a_p5_6", src: soundAsset + "s2_p6_6.ogg" },
      { id: "s1a_p5_7", src: soundAsset + "s2_p6_7.ogg" },
      { id: "s1a_p5_8", src: soundAsset + "s2_p6_8.ogg" },
      { id: "s1a_p5_9", src: soundAsset + "s2_p6_9.ogg" },
      { id: "s1a_p5_10", src: soundAsset + "s2_p6_10.ogg" },
      { id: "s1a_p5_11", src: soundAsset + "s2_p6_11.ogg" },
      { id: "s1a_p5_12", src: soundAsset + "s2_p6_12.ogg" },
      { id: "s1a_p6", src: soundAsset + "s2_p7.ogg" }
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if ($total_page == 1) {
      $prevBtn.css("display", "none");
      $nextBtn.css("display", "none");

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.lessonEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag
        ? ole.footerNotificationHandler.lessonEndSetNotification()
        : ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext);
    put_speechbox_image(content, countNext);

    function load_svg(img_id, numclick_flag, cor_ans, correctaudio) {
      var s = Snap("#svg_clock");
      var svg = Snap.load(preload.getResult(img_id).src, function(
        loadedFragment
      ) {
        s.append(loadedFragment);

        $("#numbers")
          .children()
          .mouseover(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            $(this).css("cursor", "pointer");
            $("#circle" + id_contents[1]).css("display", "block");
          })
          .mouseout(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            $("#circle" + id_contents[1]).css("display", "none");
          });

        $("#numbers")
          .children()
          .click(function() {
            var id = $(this)
              .children()
              .attr("id");
            var id_contents = id.split("_");
            var circle = $("#circle" + id_contents[1]);
            if (parseInt(id_contents[1]) == cor_ans) {
              $(this)
                .children()
                .attr("class", "correct");
              $(this)
                .children()
                .css("fill", "#fff");
              $(circle).attr("class", "circle_correct");
              sound_player(correctaudio, 1);
              $("#numbers").css("pointer-events", "none");
              if (countNext == 1) {
                $(".sp_text").html(eval("data.string.p4text15"));
              } else {
                $(".sp_text").html(eval("data.string.p4text13"));
              }
              // nav_button_controls(300);
            } else {
              $(this)
                .children()
                .attr("class", "incorrect");
              $(circle).attr("class", "circle_incorrect");
              $(this)
                .children()
                .css("fill", "#000");
              play_correct_incorrect_sound(0);
            }
          });
      });
    }

    switch (countNext) {
      case 1:
        load_svg("clock", 1, 5, "s1a_p1_1");
        sound_player("s1a_p" + countNext, 0);
        break;
      case 4:
        load_svg("clock", 1, 9, "s1a_p4_1");
        sound_player("s1a_p" + countNext, 0);
        break;
      case 5:
        function clock_function(time) {
          $(".time_now").text(time);
          $(".min_div")
            .removeClass("min-anim-" + (time % 2))
            .addClass("min-anim-" + ((time + 1) % 2));
          $(".hour_div")
            .removeClass("hr-anim-" + (time - 1))
            .addClass("hr-anim-" + time);
          createjs.Sound.stop();
          // ----> reminder--> add audios with respect to time in palce of demo sound id
          current_sound = createjs.Sound.play("s1a_p" + countNext + "_" + time);
          current_sound.play();
          current_sound.on("complete", function() {
            time += 1;
            if (time <= 12) {
              clock_function(time);
            } else {
              nav_button_controls();
            }
          });
        }

        clock_function(1, 0);
        break;
      default:
        sound_player("s1a_p" + countNext, 1);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, next) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function() {
      next ? navigationcontroller() : "";
    });
  }

  function sound_player_duo(sound_id, sound_id_2) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    //current_sound_2 = createjs.Sound.play(sound_id_2);
    current_sound.play();
    current_sound.on("complete", function() {
      $(".dotext").show(0);
      sound_player(sound_id_2);
    });
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_speechbox_image(content, count) {
    if (content[count].hasOwnProperty("speechbox")) {
      var speechbox = content[count].speechbox;
      for (var i = 0; i < speechbox.length; i++) {
        var image_src = preload.getResult(speechbox[i].imgid).src;
        //get list of classes
        var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
        var selector =
          "." + classes_list[classes_list.length - 1] + ">.speechbg";
        // console.log(selector);
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    if (countNext == 0) navigationcontroller();

    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

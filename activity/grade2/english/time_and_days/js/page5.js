var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var soundAsset1 = $ref+"/sounds/page1/";

var sound_3_00 = new buzz.sound((soundAsset1 + "3clock.ogg"));
var sound_5_30 = new buzz.sound((soundAsset1 + "half past five.ogg"));
var sound_6_00 = new buzz.sound((soundAsset1 + "6clock.ogg"));
var sound_7_00 = new buzz.sound((soundAsset1 + "7clock.ogg"));
var sound_8_30 = new buzz.sound((soundAsset1 + "half past eight.ogg"));
var sound_12_30 = new buzz.sound((soundAsset1 + "half past twelve.ogg"));
var sound_ques = new buzz.sound((soundAsset + "ques.mp3"));

var sound_t1 = new buzz.sound((soundAsset1 + "time to wake up.ogg"));
var sound_t2 = new buzz.sound((soundAsset1 + "time for school.ogg"));
var sound_t3 = new buzz.sound((soundAsset1 + "time to do homework.ogg"));
var sound_t4 = new buzz.sound((soundAsset1 + "time to go home.ogg"));
var sound_t5 = new buzz.sound((soundAsset1 + "time for lunch.ogg"));
var sound_t6 = new buzz.sound((soundAsset1 + "time for dinner.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [

// //slide0
// {
	// hasheaderblock : false,
	// contentblockadditionalclass: 'firstpagebackground',
	// additionalclasscontentblock : '',
	// uppertextblock : [{
		// textdata : data.lesson.chapter,
		// textclass : 'lesson-title'
	// }]
// },

//slide1
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_6am',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2_no_hover',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_1'
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundari_sprite',
	},
	{
		spritecss : 'sundar_sprite',
	}
	],
},

//slide2
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_6am',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2_no_hover',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_1'
	},
	{
		speechboxclass : 'speech_div speech_1_no_hover',
		speechcontent: data.string.p1text1,
		speechcontentclass: 'speech_content_2'
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide3
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_6am',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p1text1,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go ',
		speechcontent: data.string.p1text2,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide4
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_8-30am',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p2text1,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go',
		speechcontent: data.string.p2text2,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide5
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_12-30pm',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p3text1,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go',
		speechcontent: data.string.p3text2,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide6
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_3pm',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p41,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go',
		speechcontent: data.string.p42,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide7
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_5-30pm',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p51,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go',
		speechcontent: data.string.p52,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

//slide8
{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass: 'bg_7pm',

	speechboxblockadditionalclass: '',
	speechboxblock : [{
		speechboxclass : 'speech_div speech_2',
		speechcontent: data.string.pasktext1,
		speechcontentclass: 'speech_content_2',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'speech_div speech_1',
		speechcontent: data.string.p61,
		speechcontentclass: 'speech_content_1',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	},
	{
		speechboxclass : 'time_to_go',
		speechcontent: data.string.p62,
		speechcontentclass: '',
		soundclass : "sound_icon",
		soundimgsrc : imgpath + "audio_icon_white.png",
	}
	],
	spritetoshow : [
	{
		spritecss : 'sundar_sprite',
	},
	{
		spritecss : 'sundari_sprite',
	}
	],
},

];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.pageEndSetNotification() : ole.footerNotificationHandler.lessonEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.pageEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		// case 0:
			// $nextBtn.show(0);
			// break;
		case 0:
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1_no_hover', sound_ques, true, true, 'sundari', 1);
			break;
		case 1:
			$('.sundar_sprite').addClass('talking');
			$('.speech_1_no_hover').css({
				'filter': 'drop-shadow(2px 2px 2px #222)', 'opacity': '1'
			});
			play_green_show_next('.speech_2_no_hover', sound_6_00, false, true, 'sundar', 1);
			break;
		case 2:
			$('.speech_2, .speech_1').css({
				'filter': 'drop-shadow(2px 2px 2px #222)', 'opacity': '1'
			});
			$('.speech_div').css({
				'pointer-events': 'none',
			});
			$('.time_to_go').css({
					'pointer-events': 'none',
			});
			$('.time_to_go').addClass('big_big');
			$nextBtn.hide(0);
			sound_t1.play();
			sound_t1.bindOnce('ended', function(){
				$('.speech_div').css({
					'pointer-events': 'all',
				});
				$('.time_to_go').css({
					'pointer-events': 'all',
				});
				$('.time_to_go').removeClass('big_big');
				$('.time_to_go').css({
					'filter': 'drop-shadow(2px 2px 2px #222)', 'opacity': '1'
				});
				$('.speech_2, .speech_1').addClass('blink_and_big');
				$('.time_to_go').addClass('blink_big');

				//hover function
				var entered_once = false;
				$('.time_to_go, .speech_1, .speech_2').mouseenter(function(){
					if(!entered_once){
						$nextBtn.show(0);
					}
					entered_once = true;
				});
				$('.speech_div').css({
					'pointer-events': 'none',
				});
				$('.time_to_go').css({
					'pointer-events': 'none',
				});
				$('.time_to_go').css('pointer-events', 'none');
				$('.speech_2').click(function(){
					$('.sundari_sprite').addClass('talking');
					play_green_show_next('.speech_1', sound_ques, true, true, 'sundari', 1);
				});
				$('.speech_1').click(function(){
					$('.sundar_sprite').addClass('talking');
					play_green_show_next('.speech_2', sound_6_00, false, true, 'sundar', 1);
				});
				$('.time_to_go').click(function(){
					play_bottom_show_next('.time_to_go', sound_t1);
				});
				setTimeout(function() {
	    			$('.speech_2, .speech_1').removeClass('blink_and_big');
	    			$('.time_to_go').removeClass('blink_big');
	    			$('.speech_div').css({
						'pointer-events': 'all',
					});
					$('.time_to_go').css({
						'pointer-events': 'all',
					});
					}, 3000);
			});
			break;
		case 3:
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1', sound_ques, true, false, 'sundari', 0);
			setTimeout(function() {
				$('.sundar_sprite').addClass('talking');
    			play_green_show_next('.speech_2', sound_8_30, false, false, 'sundar', 0);
				}, 2000);
			setTimeout(function() {
    			play_bottom_show_next('.time_to_go', sound_t2);
				}, 5200);
			$('.speech_2').click(function(){
				$('.sundari_sprite').addClass('talking');
				play_green_show_next('.speech_1', sound_ques, true, true, 'sundari', 1);
			});
			$('.speech_1').click(function(){
				$('.sundar_sprite').addClass('talking');
				play_green_show_next('.speech_2', sound_8_30, false, true, 'sundar', 1);
			});
			$('.time_to_go').click(function(){
				play_bottom_show_next('.time_to_go', sound_t2);
			});

			break;
		case 4:
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1', sound_ques, true, false, 'sundari', 0);
			setTimeout(function() {
				$('.sundar_sprite').addClass('talking');
    			play_green_show_next('.speech_2', sound_12_30, false, false, 'sundar', 0);
				}, 2000);
			setTimeout(function() {
    			play_bottom_show_next('.time_to_go', sound_t5);
				}, 5200);
			$('.speech_2').click(function(){
				$('.sundari_sprite').addClass('talking');
				play_green_show_next('.speech_1', sound_ques, true, true, 'sundari', 1);
			});
			$('.speech_1').click(function(){
				$('.sundar_sprite').addClass('talking');
				play_green_show_next('.speech_2', sound_12_30, false, true, 'sundar', 1);
			});
			$('.time_to_go').click(function(){
				play_bottom_show_next('.time_to_go', sound_t5);
			});
			break;
		case 5:
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1', sound_ques, true, false, 'sundari', 0);
			setTimeout(function() {
				$('.sundar_sprite').addClass('talking');
    			play_green_show_next('.speech_2', sound_3_00, false, false, 'sundar', 0);
				}, 2000);
			setTimeout(function() {
    			play_bottom_show_next('.time_to_go', sound_t4);
				}, 5200);
			$('.speech_2').click(function(){
				$('.sundari_sprite').addClass('talking');
				play_green_show_next('.speech_1', sound_ques, true, true, 'sundari', 1);
			});
			$('.speech_1').click(function(){
				$('.sundar_sprite').addClass('talking');
				play_green_show_next('.speech_2', sound_3_00, false, true, 'sundar', 1);
			});
			$('.time_to_go').click(function(){
				play_bottom_show_next('.time_to_go', sound_t4);
			});
			break;
		case 6:
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1', sound_ques, true, false, 'sundari', 0);
			setTimeout(function() {
				$('.sundar_sprite').addClass('talking');
    			play_green_show_next('.speech_2', sound_5_30, false, false, 'sundar', 0);
				}, 2000);
			setTimeout(function() {
    			play_bottom_show_next('.time_to_go', sound_t3);
				}, 5200);
			$('.speech_2').click(function(){
				$('.sundari_sprite').addClass('talking');
				play_green_show_next('.speech_1', sound_ques, true, true, 'sundari', 1);
			});
			$('.speech_1').click(function(){
				$('.sundar_sprite').addClass('talking');
				play_green_show_next('.speech_2', sound_5_30, false, true, 'sundar', 1);
			});
			$('.time_to_go').click(function(){
				play_bottom_show_next('.time_to_go', sound_t3);
			});
			break;
		case 7:
			last_page = true;
			$('.sundari_sprite').addClass('talking');
			play_green_show_next('.speech_1', sound_ques, true, false, 'sundari', 0);
			setTimeout(function() {
				$('.sundar_sprite').addClass('talking');
    			play_bottom_show_next('.time_to_go', sound_t6, true, false, 'sundar');
				}, 5200);
			setTimeout(function() {
    			play_green_show_next('.speech_2', sound_7_00, 0 );
				}, 2000);
			$('.speech_2').click(function(){
				$('.sundari_sprite').addClass('talking');
				play_green_show_next('.speech_1', sound_ques, true, false, 'sundari',1);
			});
			$('.speech_1').click(function(){
				$('.sundar_sprite').addClass('talking');
				play_green_show_next('.speech_2', sound_7_00, false, false, 'sundar',1);
			});
			$('.time_to_go').click(function(){
				play_bottom_show_next('.time_to_go', sound_t6);
			});
			break;
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	/* --- function that make objects appear ---- */
	function sandy_fade_caller(fade_class, count){
		var time_delay = 0;
		for(var i = 0; i<count; i++){
			time_delay += 1000;
			var class_name = $(fade_class+i);
    		class_name.delay(time_delay).show(0);
		}
	}

	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}

	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	// flipped is for the one which is rotated 180deg about Y-axis
	// next_one used to control display of next button
	function play_green_show_next(sound_class, sound_var, flipped, next_one, monkey, play_flag){
		var last = next_one || false;
		//change value of x according to the rotateY in css
		// var x = '1';
		// if(flipped){
			// x = '2';
		// }
		// $(sound_class).css({
			// 'transform': 'translate(2%, -2%) scale(1.05, 1.05) rotateY('+x+'deg)',
			// 'background-image': 'url('+imgpath+'textbox/textbox_green.png)',
		// });
		$(sound_class+'>div').addClass('audio_play');
		$('.speech_div').css({
			'pointer-events': 'none',
		});
		$('.time_to_go').css({
				'pointer-events': 'none',
			});
		$nextBtn.hide(0);

		// adding blinking animation
		// $(sound_class).addClass('big_big');

		sound_var.play();
		sound_var.bindOnce('ended', function(){
			$(sound_class).css({
				'filter': 'drop-shadow(2px 2px 2px #222)', 'opacity': '1'
			});

			$(sound_class+'>div').removeClass('audio_play');
			if(play_flag){
				$('.time_to_go').css({
					'pointer-events': 'all',
				});
				$('.speech_div').css({
					'pointer-events': 'all',
				});
			}

			next_btn_caller (last);
			last = false;					// don't remove this line!!!!!!
			$('.'+ monkey + '_sprite').removeClass('talking');
			// $(sound_class).removeClass('big_big');
		});
	}


	// similar to play_green_show_next with minor variation
	function play_bottom_show_next(sound_class, sound_var){
		$('.speech_div').css({
			'pointer-events': 'none',
		});
		$('.time_to_go').css({
				'pointer-events': 'none',
		});
		$(sound_class).addClass('big_big');
		$nextBtn.hide(0);

		sound_var.play();
		sound_var.bindOnce('ended', function(){
			$('.speech_div').css({
				'pointer-events': 'all',
			});
			$('.time_to_go').css({
				'filter': 'drop-shadow(2px 2px 2px #222)', 'opacity': '1'
			});
			$('.time_to_go').css({
				'pointer-events': 'all',
			});
			$(sound_class).removeClass('big_big');
			$prevBtn.show(0);
			$nextBtn.show(0);
			//if last page show page end button
			if(last_page) {
				ole.footerNotificationHandler.pageEndSetNotification();
				$nextBtn.hide(0);
				$prevBtn.show(0);
			}
		});
	}

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	function hover_caller(speech_box_class, source_class, original_image_src, hover_image_src){
		$(speech_box_class).hover(function(){
			$(source_class).attr('src', imgpath + hover_image_src);
		},
		function(){
			$(source_class).attr('src', imgpath + original_image_src);
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

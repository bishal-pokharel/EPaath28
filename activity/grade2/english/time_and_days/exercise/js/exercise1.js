var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";
var sound_dg1 = new buzz.sound(soundAsset + "exercise s1_p1.ogg");
var current_sound = sound_dg1;
var content1 = [
  //ex1
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques1,

        drc7: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-7",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex2
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques1,

        drc3: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-3",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex3
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques1,

        drc12: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-12",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex4
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques1,

        drc2: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex5
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques1,

        drc10: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-10",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  }
];

var content2 = [
  //ex1
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques2,

        drc12: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-7",
            minclass: "hr-12",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex2
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques2,

        drc9: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hmin-9",
            minclass: "hr-9",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex3
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques2,

        drc6: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hmin-6",
            minclass: "hr-6",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex4
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques2,

        drc4: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hmin-4",
            minclass: "hr-4",
            dotclass: ""
          }
        ]
      }
    ]
  },
  //ex5
  {
    exerciseblockadditionalclass: "mainbg",
    exerciseblock: [
      {
        instructionclass: "my_font_medium",
        instructiondata: data.string.eins1,
        questionclass: "my_font_medium",
        questiondata: data.string.eques2,

        drc1: "correct-drag",
        dragblock: true,
        clockblock: [
          {
            clockblockclass: "clock-ex",
            clockbodyclass: "",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hmin-1",
            minclass: "hr-1",
            dotclass: ""
          }
        ]
      }
    ]
  }
];

// content1.shufflearray();
// content2.shufflearray();
var content = content1.concat(content2);
content.shufflearray();

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var countNext = 0;

  /*for limiting the questions to 10*/
  var $total_page = content.length;

  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
  }

  /*values in this array is same as the name of images of eggs in image folder*/

  //create eggs
  var testin = new NumberTemplate();

  //eggTemplate.eggMove(countNext);
  testin.init($total_page);
  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    var testcount = 0;

    $nextBtn.hide(0);
    $prevBtn.hide(0);
    countNext == 0 ? sound_player(sound_dg1) : "";

    /*generate question no at the beginning of question*/
    testin.numberOfQuestions();

    var wrngClicked = false;
    $(".q_no").html(parseInt(countNext + 1) + ". ");
    $(".drag-number").click(function() {
      if ($(this).hasClass("correct-drag")) {
        if (!wrngClicked) {
          testin.update(true);
        }
        play_correct_incorrect_sound(1);
        if (countNext != $total_page) $nextBtn.show(0);
        play_correct_incorrect_sound(1);
        $(this).css({
          "pointer-events": "none",
          "background-color": "#A4FBA6",
          "border-color": "#006203"
        });
        $(this).append("<img class='corincor-img' src='images/correct.png'>");
        $(".drag-number").css("pointer-events", "none");
      } else {
        if (!wrngClicked) {
          testin.update(false);
        }
        wrngClicked = true;
        $(this).css({
          "pointer-events": "none",
          "background-color": "#FFBABA",
          "border-color": "#A70000"
        });
        $(this).append("<img class='corincor-img' src='images/wrong.png'>");
        play_correct_incorrect_sound(0);
      }
    });
  }

  /*======= SCOREBOARD SECTION ==============*/

  function templateCaller() {
    /*always hide next and previous navigation button unless
		explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");
    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    templateCaller();
    testin.gotoNext();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  /*=====  End of Templates Controller Block  ======*/
});

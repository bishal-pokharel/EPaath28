var imgpath = $ref + "/images/forexe/";
var soundAsset = $ref + "/sounds/";
var sound_dg1 = new buzz.sound(soundAsset + "ex_2.ogg");
var current_sound = sound_dg1;
var content = [
  //slide1
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques1,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-4",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-12-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-10-30",
            minclass: "hr-6"
          }
        ]
      }
    ]
  },
  //slide2
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques2,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-1-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2",
            minclass: "hr-12"
          }
        ]
      }
    ]
  },
  //slide 3
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques3,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-4-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-4",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-8",
            minclass: "hr-12"
          }
        ]
      }
    ]
  },
  //slide4
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques4,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-6",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-12-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-3",
            minclass: "hr-12"
          }
        ]
      }
    ]
  },
  //slide5
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques5,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-8-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-11",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-7-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-8",
            minclass: "hr-12"
          }
        ]
      }
    ]
  },
  //slide6
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques6,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-11",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-3",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-10-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-4",
            minclass: "hr-12"
          }
        ]
      }
    ]
  },
  //slide7
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques7,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-5",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-5-30",
            minclass: "hr-6"
          }
        ]
      }
    ]
  },
  //slide8
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques8,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-6",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-3-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9-30",
            minclass: "hr-6"
          }
        ]
      }
    ]
  },
  //slide 9
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques9,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-3",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-9-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-12-30",
            minclass: "hr-6"
          }
        ]
      }
    ]
  },
  //slide10
  {
    extratextblock: [
      {
        textdata: data.string.eins2,
        textclass: "instruction"
      }
    ],
    exerciseblock: [
      {
        textdata: data.string.ques10,
        imageoptions: [
          {
            forshuffle: "options_sign correctOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-10-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectOne",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-4",
            minclass: "hr-12"
          },
          {
            forshuffle: "options_sign incorrectTwo",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-2-30",
            minclass: "hr-6"
          },
          {
            forshuffle: "options_sign incorrectThree",
            clockbodysrc: imgpath + "clock.png",
            hrclass: "hr-10",
            minclass: "hr-12"
          }
        ]
      }
    ]
  }
];

content.shufflearray();

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var countNext = 0;

  var $total_page = content.length;

  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;
  }

  /*values in this array is same as the name of images of eggs in image folder*/

  //create eggs
  var testin = new EggTemplate();

  testin.init(10);
  function sound_player(sound_data) {
    current_sound.stop();
    current_sound = sound_data;
    current_sound.play();
  }
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    var testcount = 0;

    $nextBtn.hide(0);
    $prevBtn.hide(0);
    /*generate question no at the beginning of question*/
    $("#num_ques").html(countNext + 1 + ". ");
    countNext == 0 ? sound_player(sound_dg1) : "";

    /*for randomizing the options*/
    var parent = $(".optionsdiv");
    var divs = parent.children();
    while (divs.length) {
      parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    }

    /*======= SCOREBOARD SECTION ==============*/
    var ansClicked = false;
    var wrngClicked = false;

    $(".correctOne").click(function() {
      correct_btn(this);
      testin.update(true);
      $nextBtn.show(0);
    });

    $(".incorrectOne, .incorrectTwo, .incorrectThree").click(function() {
      incorrect_btn(this);
      testin.update(false);
    });

    function correct_btn(current_btn) {
      $(".optionscontainer").css("pointer-events", "none");
      $(".hidden_sign").html($(current_btn).html());
      $(".hidden_sign").addClass("fade_in");
      $(".optionscontainer").removeClass("forHover");
      $(current_btn).addClass("option_true");
      play_correct_incorrect_sound(1);
      $(current_btn)
        .children()
        .children(".correct-icon")
        .show(0);
    }

    function incorrect_btn(current_btn) {
      $(current_btn).addClass("disabled");
      $(current_btn).addClass("option_false");
      play_correct_incorrect_sound(0);
      $(current_btn)
        .children()
        .children(".incorrect-icon")
        .show(0);
    }
  }

  function templateCaller() {
    /*always hide next and previous navigation button unless
		explicitly called from inside a template*/
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");
    // call navigation controller
    navigationcontroller();
    // call the template
    generalTemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
  }

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on("click", function() {
    countNext++;
    templateCaller();
    testin.gotoNext();
  });

  // $refreshBtn.click(function(){
  // 	templateCaller();
  // });

  $prevBtn.on("click", function() {
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });

  /*=====  End of Templates Controller Block  ======*/
});

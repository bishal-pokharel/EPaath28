var imgpath = $ref+"/exercise/images/ex_2/";
var soundAsset = $ref + "/sounds/";


var exere = new buzz.sound((soundAsset + "exer.ogg"));

// var content1=[
// 	//slide 0
// 	{
// 		contentblockadditionalclass: 'instruction_bg',
// 		uppertextblockadditionalclass: 'ex_main_text',
// 		uppertextblock : [
// 		{
// 			textdata : data.string.einstruction,
// 			textclass : 'my_font_very_big',
// 		}],
// 	},
// ];
var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		uppertextblock : [
		{
			textdata : data.string.etext1,
			textclass : 'instruction my_font_big',
		},
	],
		hintimageblock:[
		{
			imgsrc : imgpath + "1.png",
		}],

		optionsblock : [{
			textdata : data.string.e1ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e1ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e1ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e1ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext2,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "2.png",
		}],

		optionsblock : [{
			textdata : data.string.e2ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e2ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e2ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e2ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext3,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "3.png",
		}],

		optionsblock : [{
			textdata : data.string.e3ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e3ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e3ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e3ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext4,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "4.png",
		}],

		optionsblock : [{
			textdata : data.string.e4ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e4ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e4ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e4ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext5,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "5.png",
		}],

		optionsblock : [{
			textdata : data.string.e5ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e5ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e5ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e5ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext6,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "6.png",
		}],

		optionsblock : [{
			textdata : data.string.e6ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e6ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e6ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e6ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext7,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "7.png",
		}],

		optionsblock : [{
			textdata : data.string.e7ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e7ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e7ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e7ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext8,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "8.png",
		}],

		optionsblock : [{
			textdata : data.string.e8ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e8ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e8ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e8ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext9,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "9.png",
		}],

		optionsblock : [{
			textdata : data.string.e9ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e9ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e9ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e9ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext10,
			textclass : 'instruction my_font_big',
		}],
		extratextblock : [
			{
				textdata1 : data.string.einstruction,
				textclass1 : 'topins',
			},
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "10.png",
		}],

		optionsblock : [{
			textdata : data.string.e10ansa,
			textclass : 'class1 options my_font_medium option_1'
		},
		{
			textdata : data.string.e10ansb,
			textclass : 'class2 options my_font_medium option_2'
		},
		{
			textdata : data.string.e10ansc,
			textclass : 'class3 options my_font_medium option_3'
		},
		{
			textdata : data.string.e10ansd,
			textclass : 'class2 options my_font_medium option_4'
		}],
	},
];

// content.shufflearray();
// // var content = content2;
// var content = content1.concat(content2);

$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

  function soundplayer(i){
    buzz.all().stop();
    i.play().bind("ended",function(){
        navigationcontroller();
    });
  }

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//create eggs
	var eggs = new EggTemplate();

 	//eggTemplate.eggMove(countNext);
	eggs.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext==0){
			soundplayer(exere);
		}
		//randomize options
		if(countNext>=0){
			for( var mm =1; mm<5; mm++){
				$('.options').removeClass('option_'+mm);
			}
			var option_position = [1,2,3,4];
			option_position.shufflearray();
			for(var op=0; op<4; op++){
				$('.options').eq(op).addClass('option_'+option_position[op]);
			}
			var wrong_clicked = false;
			$(".options").click(function(){
				if($(this).hasClass("class1")){
					if(!wrong_clicked){
						eggs.update(true);
					}
					$(".options").css('pointer-events', 'none');
					$(this).css({
						'border': '3px solid #FCD172',
						'background-color': '#6EB260',
						'color': 'white'
					});
					play_correct_incorrect_sound(1);
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img id="corr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:3%;transform:translate(365%,0%)" src="images/correct.png" />').insertAfter(this);
					if(countNext != $total_page)
						$nextBtn.show(0);
				}
				else{
					if(!wrong_clicked){
						eggs.update(false);
					}
					$(this).css({
						'background-color': '#BA6B82',
						'border': 'none'
					});
					wrong_clicked = true;
					play_correct_incorrect_sound(0);
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img id="corr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:3%;transform:translate(365%,0%)" src="images/wrong.png" />').insertAfter(this);
					wrong_clicked = true;
				}
			});
		} else{
			$nextBtn.show(0);
		}
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		if(countNext>=1){
			eggs.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

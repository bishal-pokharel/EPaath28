var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/";


var titlepage = new buzz.sound((soundAsset + "titlepage.ogg"));
var sound_behind = new buzz.sound((soundAsset + "behind the tree.ogg"));
var sound_next_to = new buzz.sound((soundAsset + "next to the balloon.ogg"));
var sound_on_top_of = new buzz.sound((soundAsset + "on top of the hill.ogg"));
var sound_in = new buzz.sound((soundAsset + "in the box.ogg"));

var sound_outside = new buzz.sound((soundAsset + "outside the house.ogg"));
var sound_inside = new buzz.sound((soundAsset + "inside the house.ogg"));
var sound_in_front_of = new buzz.sound((soundAsset + "in front of the tree.ogg"));
var sound_on = new buzz.sound((soundAsset + "on the carpet !!!.ogg"));

var sound_under = new buzz.sound((soundAsset + "under the table !!!!!!!!.ogg"));
var sound_near = new buzz.sound((soundAsset + "near the dog.ogg"));
// Dummy sound is used to fill a gap between slide 6 and slide 7
var sound_dummy = new buzz.sound((soundAsset + "behind the tree.ogg"));

var sound_group_prep = [sound_behind, sound_next_to, sound_on_top_of, sound_in, sound_outside, sound_inside, sound_dummy, sound_in_front_of, sound_on, sound_under, sound_near];

// position of clouds and sprite
var cloud_pos_0 = 0;
var cloud_pos_1 = 60;
var cloud_pos_2 = 35;
var obj_position = -40;
// variable to hold the class
var object_class = 0;
// counter to count no. of steps the object has moved, and other count to make object stop
var counter = 0;
var stop_count = 0;
var final_count = 0;
var obj_bottom = 0;

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust: true,
		contentblockadditionalclass : 'coverbg',
		uppertextblockadditionalclass: 'lesson-title patrickhand',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}],
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "tree move_left_mid_1",
				imgsrc : imgpath + "tree.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",		//audio tag to play the sound using js
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "balloon balloony",
				imgsrc : imgpath + "balloon.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "hill_front move_left_mid_3",
				imgsrc : imgpath + "hill_front.png",
			},
			{
				imgclass : "hill_back move_left_mid_3",
				imgsrc : imgpath + "hill_back.png",
			},
			{
				imgclass : "hill_side move_left_mid_3",
				imgsrc : imgpath + "hill_side.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "hill_front move_box",
				imgsrc : imgpath + "hill_front.png",
			},
			{
				imgclass : "hill_back move_box",
				imgsrc : imgpath + "hill_back.png",
			},
			{
				imgclass : "hill_side move_box",
				imgsrc : imgpath + "hill_side.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "house move_left_mid_1",
				imgsrc : imgpath + "house/housemain.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text6,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "house_0 ",
				imgsrc : imgpath + "house/hosuewithoutwindow.png",
			},
			{
				imgclass : "window_0 window",
				imgsrc : imgpath + "house/window01.png",
			},
			{
				imgclass : "window_1 window",
				imgsrc : imgpath + "house/window02.png",
			},
			{
				imgclass : "window_2 window",
				imgsrc : imgpath + "house/window03.png",
			},
			{
				imgclass : "elephant_sit",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite",
				imagelabeldata : ''
			}]
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "house house_0 move_out_1",
				imgsrc : imgpath + "house/housemain.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence",
			textdata : data.string.p1text7,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "tree_1 move_left_mid_1",
				imgsrc : imgpath + "tree.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}]
		}]
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text8,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "carpet move_left_mid_1",
				imgsrc : imgpath + "carpet.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text9,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "table_front table move_left_mid_1",
				imgsrc : imgpath + "tablefront.png",
			},
			{
				imgclass : "table_back table move_left_mid_1",
				imgsrc : imgpath + "tableback.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'main_bg',
		uppertextblockadditionalclass : "spriteblock",

		uppertextblock : [
		{
			textclass : "sentence audio_tag",
			textdata : data.string.p1text10,
			datahighlightflag : true,
			datahighlightcustomclass : 'prep_word',
		}],

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "moon",
				imgsrc : imgpath + "moon1.png",
			},
			{
				imgclass : "cloud_0",
				imgsrc : imgpath + "cloud01.png",
			},
			{
				imgclass : "cloud_1",
				imgsrc : imgpath + "cloud02.png",
			},
			{
				imgclass : "cloud_2",
				imgsrc : imgpath + "cloud03.png",
			},
			{
				imgclass : "doggie move_left_mid_5",
				imgsrc : imgpath + "puppy.png",
			},
			{
				imgclass : "elephant_sit its_hidden",
				imgsrc : imgpath + "sitting.png",
			},
			{
				imgclass : "audio_icon audio_tag",
				imgsrc : imgpath + "audio_icon.png",
			}],
			imagelabels : [{
				imagelabelclass : "grass_sprite grass_moving",
				imagelabeldata : ''
			},
			{
				imagelabelclass : "elephant_sprite walking",
				imagelabeldata : ''
			}]
		}]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 3;
	var $label = $(".label-box");
	var $total_page = content.length;
	 var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var keep_moving = true;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			// $prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		 vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			// setTimeout(function(){$nextBtn.show(0);},1500);
			soundplayer(titlepage);
			break;
		case 1:
			// set keep_moving true to enable sprite and cloud movement
			keep_moving = true;
			sprite_mover();
			// change the object_class to point to tree
			// stop_count when object reaches where the sprite is i.e near the right: 50%
			stop_count = 175;
			// final count is when object is completely out of screen
			final_count = 375;
			object_mover();
			background_mover();
			// onclick functionality
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 2:
			keep_moving = true;
			sprite_mover();
			stop_count = 160;
			final_count = 375;
			object_mover();
			background_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 3:
			keep_moving = true;
			sprite_mover();
			obj_bottom = 1;
			stop_count = 250;
			final_count = 500;
			hill_climber(125, 195, 298, 338, 338);
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 4:
			keep_moving = true;
			sprite_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 5:
			keep_moving = true;
			sprite_mover();
			stop_count = 175;
			final_count = 375;
			object_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 6:
			// animation to zoom out elephant sitting
			var window_cnt = 0;
			$('.elephant_sit').addClass('go_home');
			sound_btn_clicker('.audio_tag', countNext-1);
			// change image and play sound during hover
			setTimeout(function(){
				if(!window_cnt){
					sound_group_prep[(countNext-1)].play();
					$nextBtn.hide(0);
					$('.audio_icon').show(0);
					$('.sentence').show(0);
					$('.window_0').attr('src', imgpath + "house/window01_e.png");
					sound_group_prep[(countNext-1)].bind('ended', function(){
						$nextBtn.show(0);
						$('.window').css('pointer-events','none');
						window_cnt++;
					});
				}
			},3000);
			break;
		case 8:
			keep_moving = true;
			sprite_mover();
			stop_count = 175;
			final_count = 375;
			object_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 9:
			keep_moving = true;
			sprite_mover();
			stop_count = 170;
			final_count = 375;
			object_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 10:
			keep_moving = true;
			sprite_mover();
			object_class = '.table';
			stop_count = 175;
			final_count = 375;
			object_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		case 11:
			keep_moving = true;
			sprite_mover();
			object_class = '.doggie';
			stop_count = 160;
			final_count = 375;
			object_mover();
			sound_btn_clicker('.audio_tag', countNext-1);
			break;
		default:
			break;
		}
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			// in most cases nextbtn.click doesnt increase the countNext and call Templete caller
			// we need to switch page only when background object leaves the screen and is thus called in obj_mover
			case 1:
				keep_moving = true;
				$('.tree').removeClass('move_left_mid_1');
				$('.tree').addClass('move_out_1');
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 2:
				keep_moving = true;
				$('.balloon').removeClass('balloony');
				$('.balloon').attr('src', imgpath + "balloon_burst.png");
				$('.balloon').addClass('gone_wild');	// add balloon blasting effect
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 3:
				keep_moving = true;
				sprite_mover();
				countNext++;
				templateCaller();
				$nextBtn.hide(0);
				break;
			case 4:
				keep_moving = true;
				sprite_mover();
				$('.hill_side').removeClass('move_box');
				$('.hill_front').removeClass('move_box');
				$('.hill_back').removeClass('move_box');
				$('.hill_back').addClass('move_out_3');
				$('.hill_front').addClass('move_out_3');
				$('.hill_side').addClass('side_fall');
				$nextBtn.hide(0);
				break;
			case 5:
				// keep_moving = true;
				// sprite_mover();
				countNext++;
				templateCaller();
				$nextBtn.hide(0);
				break;
			case 6:
				keep_moving = true;
				countNext++;
				templateCaller();
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 7:
				keep_moving = true;
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 8:
				$('.tree_1').removeClass('move_left_mid_1');
				$('.tree_1').addClass('move_out_1');
				keep_moving = true;
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 9:
				$('.carpet').removeClass('move_left_mid_1');
				$('.carpet').addClass('move_out_1');
				keep_moving = true;
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 10:
				$('.table').removeClass('move_left_mid_1');
				$('.table').addClass('move_out_1');
				keep_moving = true;
				sprite_mover();
				$nextBtn.hide(0);
				break;
			case 11:
				// $('.doggie').removeClass('move_left_mid_5');
				// $('.doggie').addClass('move_out_5');
				// keep_moving = true;
				// sprite_mover();
				$nextBtn.hide(0);
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	// function used to move clouds
	function background_mover() {
		setInterval(function() {
			if(!keep_moving) {
				return false;
			}
			if(cloud_pos_1 > 100){
				cloud_pos_1 = -20;
			}
			if(cloud_pos_0 > 115){
				cloud_pos_0 = -20;
			}
			if(cloud_pos_2 > 110){
				cloud_pos_2 = -20;
			}
			cloud_pos_0+= 0.01;
			cloud_pos_1+= 0.01;
			cloud_pos_2+= 0.01;
			$('.cloud_0').css('right', (cloud_pos_0+'%'));
			$('.cloud_1').css('right', (cloud_pos_1+'%'));
			$('.cloud_2').css('right', (cloud_pos_2+'%'));
		}, 20);
	}

	// object_mover changes the position of object class by 1 for every 50 millisecond as by setInterval function
	function object_mover() {
		var interval_obj = setInterval(function() {
			// used to stop object
			if(!keep_moving) {
				return false;
			}
			// exit the loop, rest obj_position and change page when the object leaves the screen i.e when it reaches final count
			if(counter == final_count) {
				counter = 0;
				countNext++;
				if(countNext>=content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
					// countNext=1;
				}
				templateCaller();
				clearInterval(interval_obj);
			}
			// stop the object, background and play the sound
			if(counter == stop_count){
				sprite_stopper();
				keep_moving = false;
				$nextBtn.hide(0);
				$('.elephant_sprite').hide(0);
				$('.elephant_sit').show(0);
				$('.sentence').show(0);
				$('.audio_icon').show(0);
				sound_group_prep[(countNext-1)].play();
				sound_group_prep[(countNext-1)].bind('ended', function(){
					if(countNext==content.length-1){
						ole.footerNotificationHandler.pageEndSetNotification();
					} else{
						$nextBtn.show(0);
					}
				});
			} else {
				$('.elephant_sprite').show(0);
				$('.elephant_sit').hide(0);
				$('.sentence').hide(0);
				$('.audio_icon').hide(0);
			}
			// move object by changing obj_position
			counter++;
		}, 20);
	}

	//function used to make elephant climb the hill, with counts to start climbing, stop climbing and decent as well
	function hill_climber(start_climb, stop_climb, start_fall, stop_fall, second_stop) {
		var interval_hill = setInterval(function() {
			if(!keep_moving) {
				return false;
			}
			if(counter == final_count) {
				counter = 0;
				countNext++;
				templateCaller();
				clearInterval(interval_hill);
			}
			// play the audio and stop sprites and movements when stopped
			if(counter == stop_count || counter == second_stop){
				keep_moving = false;
				sprite_stopper();
				$('.elephant_sprite').hide(0);
				$nextBtn.hide(0);
				$('.elephant_sit').show(0);
				$('.sentence').show(0);
				$('.audio_icon').show(0);
				sound_group_prep[(countNext-1)].play();
				sound_group_prep[(countNext-1)].bind('ended', function(){
					$nextBtn.show(0);
				});
			} else {
				$('.elephant_sprite').show(0);
				$('.elephant_sit').hide(0);
				$('.sentence').hide(0);
				$('.audio_icon').hide(0);
			}

			if(counter > start_climb && counter < stop_climb){
				obj_bottom += 0.4;
				counter++;
				$('.elephant_sprite').css({'bottom': (obj_bottom+'%'),'transform' : 'rotate(-35deg)'});
				$('.elephant_sit').css({'bottom': (obj_bottom+'%')});
			} else if(counter > start_fall && counter < stop_fall){
				obj_bottom -= .8;
				counter++;
				$('.elephant_sprite').css({'bottom': (obj_bottom+'%'),'transform' : 'rotate(45deg)'});
				$('.elephant_sit').css({'bottom': (obj_bottom+'%')});
			}
			else {
				counter++;
				$('.elephant_sprite').css({'bottom': (obj_bottom+'%'),'transform': 'rotate(0deg)'});
				$('.elephant_sit').css({'bottom': (obj_bottom+'%')});
			}
		}, 19);
	}


  function soundplayer(i){
    buzz.all().stop();
    i.play().bind("ended",function(){
        	$nextBtn.show(0);
    });
	}

	// Function that adds onClick sound effect
	function sound_btn_clicker(sound_class, index) {
		$(sound_class).click(function(){
			sound_group_prep[index].play();
			$nextBtn.hide(0);
			sound_group_prep[index].bind('ended', function(){
					$nextBtn.show(0);
				});
		});
	}

	// To start movement of sprite and stop it respectively
	function sprite_mover() {
		$('.elephant_sprite').addClass("walking");
		$('.grass_sprite').addClass("grass_moving");
	}
	function sprite_stopper() {
		$('.elephant_sprite').removeClass("walking");
		$('.grass_sprite').removeClass("grass_moving");
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

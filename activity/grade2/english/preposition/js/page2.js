var imgpath = $ref+"/images/";
var soundAsset = $ref + "/sounds/";


var s2ins = new buzz.sound((soundAsset + "s2_instruction.ogg"));

var s2drop1 = new buzz.sound((soundAsset + "clothes.ogg"));
var s2drop2 = new buzz.sound((soundAsset + "football.ogg"));
var s2drop3 = new buzz.sound((soundAsset + "shoes.ogg"));
var s2drop4 = new buzz.sound((soundAsset + "poster.ogg"));
var s2drop5 = new buzz.sound((soundAsset + "suitcase.ogg"));
var s2drop6 = new buzz.sound((soundAsset + "socks.ogg"));
var s2drop7 = new buzz.sound((soundAsset + "puppy.ogg"));
var s2drop8 = new buzz.sound((soundAsset + "toybear.ogg"));
var s2drop9 = new buzz.sound((soundAsset + "cat.ogg"));
var s2drop10 = new buzz.sound((soundAsset + "flowervase.ogg"));

var s2drop = [s2drop1, s2drop2, s2drop3, s2drop4, s2drop5, s2drop6, s2drop7, s2drop8, s2drop9, s2drop10]
var content=[
	{
		contentblockadditionalclass: 'bgdiy',
		lowertextblockadditionalclass: 'diy-page',
		lowertextblock : [
			{
				textdata : data.string.diytext,
				textclass : '',
			}
		],
	},
	{
		contentblockadditionalclass: 'main_bg',
		lowertextblockadditionalclass: 'instruction-top',
		lowertextblock : [
			{
				textdata : data.string.exins,
				textclass : 'my_font_big',
			}
		],

		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
			{
				textdata : data.string.ectext1,
				textclass : 'instruction my_font_medium pos_1',
			},
			{
				textdata : data.string.ectext2,
				textclass : 'instruction my_font_medium pos_2',
			},
			{
				textdata : data.string.ectext3,
				textclass : 'instruction my_font_medium pos_3',
			},
			{
				textdata : data.string.ectext4,
				textclass : 'instruction my_font_medium pos_4',
			},
			{
				textdata : data.string.ectext5,
				textclass : 'instruction my_font_medium pos_5',
			},
			{
				textdata : data.string.ectext6,
				textclass : 'instruction my_font_medium pos_6',
			},
			{
				textdata : data.string.ectext7,
				textclass : 'instruction my_font_medium pos_7',
			},
			{
				textdata : data.string.ectext8,
				textclass : 'instruction my_font_medium pos_8',
			},
			{
				textdata : data.string.ectext9,
				textclass : 'instruction my_font_medium pos_9',
			},
			{
				textdata : data.string.ectext10,
				textclass : 'instruction my_font_medium pos_10',
			}
		],
		imageblockadditionalclass: '',
		imageblock:[
			{
				imgclass : "bed_pos",
				imgsrc : imgpath + "bed.png",
			},
		],
		draggableblockadditionalclass: 'aligned_bottom',
		draggableblock:[
			{
				imgclass : "draggable pos_1",
				imgsrc : imgpath + "1.png",
			},
			{
				imgclass : "draggable pos_2",
				imgsrc : imgpath + "2.png",
			},
			{
				imgclass : "draggable pos_3",
				imgsrc : imgpath + "3.png",
			},
			{
				imgclass : "draggable pos_4",
				imgsrc : imgpath + "4.png",
			},
			{
				imgclass : "draggable pos_5",
				imgsrc : imgpath + "5.png",
			},
			{
				imgclass : "draggable pos_6",
				imgsrc : imgpath + "6.png",
			},
			{
				imgclass : "draggable pos_7",
				imgsrc : imgpath + "7.png",
			},
			{
				imgclass : "draggable pos_8",
				imgsrc : imgpath + "8.png",
			},
			{
				imgclass : "draggable pos_9",
				imgsrc : imgpath + "9.png",
			},
			{
				imgclass : "draggable pos_10",
				imgsrc : imgpath + "10.png",
			}
		],
		droppableblockadditionalclass: 'cover_board',
		droppableblock:[
			{
				droppablecontainerclass: 'drop_1',
			},
			{
				droppablecontainerclass: 'drop_2',
			},
			{
				droppablecontainerclass: 'drop_3',
			},
			{
				droppablecontainerclass: 'drop_4',
			},
			{
				droppablecontainerclass: 'drop_5',
			},
			{
				droppablecontainerclass: 'drop_6',
			},
			{
				droppablecontainerclass: 'drop_8',
			},
			{
				droppablecontainerclass: 'drop_9',
			},
			{
				droppablecontainerclass: 'drop_10',
			}
		],
	}
];

$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
	            /*if there is a data-highlightcustomclass attribute defined for the text element
	            use that or else use default 'parsedstring'*/
	            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
	              (stylerulename = $(this).attr("data-highlightcustomclass")) :
	              (stylerulename = "parsedstring") ;

	            texthighlightstarttag = "<span class='"+stylerulename+"'>";
	            replaceinstring       = $(this).html();
	            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
	            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
	            $(this).html(replaceinstring);
          	});
        }
	}

    var count =0;
    var item_is_set=[0, 0,0,0,0,0 ,0,0,0,0,0];
	// //create eggs
	// var eggs = new EggTemplate();

 	// //eggTemplate.eggMove(countNext);
	// eggs.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		loadTimelineProgress($total_page, countNext + 1);

		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		switch(countNext) {
		case 0:
			play_diy_audio();
			setTimeout(function(){ $nextBtn.show(0);},1300);
			break;
		case 1:
			soundplayer(s2ins);
			// $prevBtn.show(0);
			function hover_func(i) {
				$('.pos_' + i).mouseenter(function() {
					if (!item_is_set[i] && !($('.pos_' + i).hasClass('ui-draggable-dragging'))) {
						$('p' + '.pos_' + i).show(0);
							soundplayer(s2drop[i-1]);
					}
				});
				$('.pos_' + i).mouseleave(function() {
					$('p' + '.pos_' + i).hide(0);
				});
			}
			setTimeout(function(){
			for (var b = 1; b < 11; b++) {
				hover_func(b);
			}
		},5000);
			$(".draggable").draggable({
				cursorAt: { left: 50,
				 						top:50 },
				containment : "body",
				revert : "invalid",
				appendTo : "body",
				zindex : 1000,
				start : function(event, ui) {

					buzz.all().stop();
					$('.aligned_bottom>*').css({
						'pointer-events' : 'none',
						'opacity' : '0.5',
						'z-index' : '-1'
					});
					$(this).css({
						'pointer-events' : 'all',
						'opacity' : '1',
						'z-index' : '1'
					});
				},
				stop : function(event, ui) {
					buzz.all().stop();
					$('.instruction').hide(0);
					$('.aligned_bottom>*').css({
						'pointer-events' : 'all',
						'opacity' : '1',
						'z-index' : 'none'
					});
					$(this).css({
						'z-index' : '0'
					});
				}
			});
			$('.drop_1').droppable({
				accept : ".pos_1",
				hoverClass : "drop_1_hovered",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'display' : 'none',
					}).appendTo($('.imageblock'));
					item_is_set[1] = 1;
					drop_function();
					$('.drop_1').css('background-image', 'url(activity/grade2/english/preposition/images/daraj02.png)');
				}
			});
			$('.drop_2').droppable({
				accept : ".pos_2",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '33%',
						'top' : '59%',
						'width' : '5%',
						'height': 'auto'
					}).appendTo($('.imageblock'));
					item_is_set[2] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_3').droppable({
				accept : ".pos_3",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '80%',
						'top' : '76%',
						'width' : '5%',
						'height' : 'auto'
					}).appendTo($('.imageblock'));
					item_is_set[3] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_4').droppable({
				accept : ".pos_4",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '38%',
						'top' : '14%',
						'width' : '23%',
						'height' : 'auto'
					}).appendTo($('.imageblock'));
					item_is_set[4] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_5').droppable({
				accept : ".pos_5",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '5.5%',
						'top' : '13%',
						'width' : '15%',
						'height' : 'auto'
					}).appendTo($('.imageblock'));
					item_is_set[5] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_6').droppable({
				accept : ".pos_6, .pos_7",
				hoverClass : "drop_6_hovered",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					if (ui.draggable.hasClass("pos_7")) {
						item_is_set[7] = 1;
						$('.drop_6').css('background-image', 'url(activity/grade2/english/preposition/images/box_2.png)');
					} else {
						item_is_set[6] = 1;
					}
					drop_function();
					ui.draggable.detach().css({
						'position' : 'absolute',
						'display' : 'none',
					}).appendTo($('.imageblock'));
				}
			});
			$('.drop_8').droppable({
				accept : ".pos_8",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '81%',
						'top' : '50%',
						'width' : '7%',
					}).appendTo($('.imageblock'));
					item_is_set[8] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_9').droppable({
				accept : ".pos_9",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '80%',
						'top' : '28.5%',
						'width' : '8%',
					}).appendTo($('.imageblock'));
					item_is_set[9] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
				}
			});
			$('.drop_10').droppable({
				accept : ".pos_10",
				hoverClass : "",
				drop : function(event, ui) {
					ui.draggable.draggable('disable');
					ui.draggable.detach().css({
						'position' : 'absolute',
						'left' : '32%',
						'top' : '40.5%',
						'width' : '7%',
					}).appendTo($('.imageblock'));
					item_is_set[10] = 1;
					drop_function();
					ui.draggable.detach().appendTo($('.imageblock'));
                }
			});
			function drop_function() {
				count++;
				if (count == 10) {
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			}

			break;
		}
	}


  function soundplayer(i){
    buzz.all().stop();
    i.play();
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		if(countNext>1){
			eggs.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

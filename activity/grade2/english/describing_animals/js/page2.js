var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/";
// var content;

var sound_l_0 = new buzz.sound((soundAsset + "s2_p1.ogg"));
var sound_l_1 = new buzz.sound((soundAsset + "cat.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "elephant.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "sparrow.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "14.mp3"));
var sound_1 = new buzz.sound((soundAsset + "s2_p2.ogg"));
var sound_2 = new buzz.sound((soundAsset + "s2_p3.ogg"));
var sound_3 = new buzz.sound((soundAsset + "s2_p4.ogg"));
var sound_4 = new buzz.sound((soundAsset + "s2_p5.ogg"));
var sound_5 = new buzz.sound((soundAsset + "s2_p6.ogg"));
var sound_6 = new buzz.sound((soundAsset + "s2_p7.ogg"));
var sound_7 = new buzz.sound((soundAsset + "s2_p8.ogg"));
var sound_8 = new buzz.sound((soundAsset + "s2_p9.ogg"));
var sound_9 = new buzz.sound((soundAsset + "s2_p10.ogg"));
var sound_10 = new buzz.sound((soundAsset + "s2_p11.ogg"));
var sound_11 = new buzz.sound((soundAsset + "s2_p12.ogg"));
var sound_12 = new buzz.sound((soundAsset + "s2_p13.ogg"));
var sound_13 = new buzz.sound((soundAsset + "s2_p14.ogg"));
var sound_14 = new buzz.sound((soundAsset + "s2_p15.ogg"));
var sound_15 = new buzz.sound((soundAsset + "s2_p16.ogg"));
var sound_16 = new buzz.sound((soundAsset + "s2_p17.ogg"));
var sound_17 = new buzz.sound((soundAsset + "s2_p18.ogg"));
var sound_18 = new buzz.sound((soundAsset + "s2_p19.ogg"));
var sound_19 = new buzz.sound((soundAsset + "s2_p20.ogg"));
var sound_20 = new buzz.sound((soundAsset + "s2_p21.ogg"));

var sound_correct = new buzz.sound((soundAsset + "correct.mp3"));
var sound_incorrect = new buzz.sound((soundAsset + "incorrect.mp3"));


var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4];

// function getContent(data){
var content=[

	//1st slide
	{
		uppertextblock:[{

				textclass : "canyouidentify",
				textdata : data.string.p2text1,

			},
               {

				textclass : "canyouidentify2",
				textdata : data.string.p2text2,

			}],

		imageblock: [{
				imagetoshow: [{
								imgclass: "slide1img",
								imgsrc : imgpath + "images/guessanimal.gif"
					},{
						imgclass: "coverimg",
						imgsrc : imgpath + "images/cover.jpg"
					}],

				}],


		},

    //------------animal cat-----------//
    //next slide fully covered no clue
	{
        	uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/cat.png",
					},
                    {
                      imgclass: "cover1",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],

    },

	//2nd slide  clue1
	{
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/cat.png",
					},
                    {
                      imgclass: "cover2",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text4,

			}       ]

	},
    //3rd slide clue2
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/cat.png",
					},
                    {
                      imgclass: "cover3",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text4,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text5,

			}
                       ]

	},
    // 3rd clue
    {
        uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/cat.png",
					},
                    {
                      imgclass: "cover4",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text4,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text5,

			},
                       {

				textclass : "third_clue",
				textdata : data.string.p2text6,

			},
                            ]
    },
    //4th slide final answer
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/cat.png",
					},
                    {
                      imgclass: "cover5",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue coloured",
				textdata : data.string.p2text4,

			},
               {

				textclass : "second_clue coloured",
				textdata : data.string.p2text5,

			},
                       {

				textclass : "third_clue coloured",
				textdata : data.string.p2text6,

			},
                        {

				textclass : "final_clue",
				textdata : data.string.p2text7,

			},
                       ]
	},
    //---------animal elephant--------//
{
        	uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/elephant_cartoon.png",
					},
                    {
                      imgclass: "cover1",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],

    },

	//2nd slide  clue1
	{
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/elephant_cartoon.png",
					},
                    {
                      imgclass: "cover2",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text8,

			}       ]

	},
    //3rd slide clue2
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/elephant_cartoon.png",
					},
                    {
                      imgclass: "cover3",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text8,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text9,

			}
                       ]

	},
    // 3rd clue
    {
        uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/elephant_cartoon.png",
					},
                    {
                      imgclass: "cover4",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text8,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text9,

			},
                       {

				textclass : "third_clue",
				textdata : data.string.p2text10,

			},
                            ]
    },
    //4th slide final answer
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/elephant_cartoon.png",
					},
                    {
                      imgclass: "cover5",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue coloured",
				textdata : data.string.p2text8,

			},
               {

				textclass : "second_clue coloured",
				textdata : data.string.p2text9,

			},
                       {

				textclass : "third_clue coloured",
				textdata : data.string.p2text10,

			},
                        {

				textclass : "final_clue",
				textdata : data.string.p2text11,

			},
                       ]
	},
    //-------animal sparrow-------//
    {
        	uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/sparrow_cartoon.png",
					},
                    {
                      imgclass: "cover1",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],

    },

	//2nd slide  clue1
	{
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/sparrow_cartoon.png",
					},
                    {
                      imgclass: "cover2",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text12,

			}       ]

	},
    //3rd slide clue2
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/sparrow_cartoon.png",
					},
                    {
                      imgclass: "cover3",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text12,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text13,

			}
                       ]

	},
    // 3rd clue
    {
        uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/sparrow_cartoon.png",
					},
                    {
                      imgclass: "cover4",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text12,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text13,

			},
                       {

				textclass : "third_clue",
				textdata : data.string.p2text14,

			},
                            ]
    },
    //4th slide final answer
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/sparrow_cartoon.png",
					},
                    {
                      imgclass: "cover5",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue coloured",
				textdata : data.string.p2text12,

			},
               {

				textclass : "second_clue coloured",
				textdata : data.string.p2text13,

			},
                       {

				textclass : "third_clue coloured",
				textdata : data.string.p2text14,

			},
                        {

				textclass : "final_clue",
				textdata : data.string.p2text15,

			},
                       ]
	},

    //-----------animal spider---------//
    {
        	uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/spider01.jpg",
					},
                    {
                      imgclass: "cover1",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],

    },

	//2nd slide  clue1
	{
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/spider01.jpg",
					},
                    {
                      imgclass: "cover2",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text16,

			}       ]

	},
    //3rd slide clue2
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/spider01.jpg",
					},
                    {
                      imgclass: "cover3",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text16,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text17,

			}
                       ]

	},
    // 3rd clue
    {
        uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/spider01.jpg",
					},
                    {
                      imgclass: "cover4",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue",
				textdata : data.string.p2text16,

			},
               {

				textclass : "second_clue",
				textdata : data.string.p2text17,

			},
                       {

				textclass : "third_clue",
				textdata : data.string.p2text18,

			},
                            ]
    },
    //4th slide final answer
    {
		uppertextblock:[
			{
				textclass : "slide2label",
				textdata : data.string.p2text3,
			}
		],

		imageblock: [
			{
				imagetoshow: [

					{
								imgclass: "slide2img",
								imgsrc : imgpath + "images/spider01.jpg",
					},
                    {
                      imgclass: "cover5",
                      imgsrc : imgpath + "images/cover.jpg"
                    },


				],

			}
		],
        lowertextblock:[{

				textclass : "first_clue coloured",
				textdata : data.string.p2text16,

			},
               {

				textclass : "second_clue coloured",
				textdata : data.string.p2text17,

			},
                       {

				textclass : "third_clue coloured",
				textdata : data.string.p2text18,

			},
                        {

				textclass : "final_clue",
				textdata : data.string.p2text19,

			},
                       ]
	},


];
// 	return content;
// }


$(function(){


	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var total_page = 0;
	loadTimelineProgress($total_page, countNext + 1);
  var soundid = sound_l_0;

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	/*
		TODO: same comment
	*/

	function navigationController(){
		if(countNext == 0 && total_page != 1){
			$nextBtn.show(0);
		}
		else if((countNext == 1)||(countNext == 6)||(countNext == 11)) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext > 0 && countNext < (total_page-1)){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}else if(countNext == total_page-1){
			//$prevBtn.show(0);
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

    function sound_player(sound_id,navigate){
        soundid = sound_id;
        soundid.stop();
        soundid.play();
        soundid.bind("ended", function() {
            navigate?navigationController():"";
        });
    }


	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		var anim_count = 0;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
        soundid.stop();
		switch (countNext) {
		case 0:
			sound_player(sound_l_0,true);
			$(".imageblock").css("height", "50%");
			break;

		case 1:
            sound_player(sound_group_p1[0],false);
            setTimeout(function(){
                sound_player(sound_1,true);
            },1000);
            $(".contentblock").css("background-color", "#62558E");
			break;
		case 2:
            sound_player(sound_2,true);
            $(".contentblock").css("background-color", "#62558E");
			break;
		case 3:
            sound_player(sound_3,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 4:
            sound_player(sound_4,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 5:
            sound_player(sound_5,true);
            $(".contentblock").css("background-color", "#62558E");
			break;
		case 6:
            setTimeout(function(){
                sound_player(sound_6,true);
            },1000);
            $(".contentblock").css("background-color", "#62558E");
			sound_group_p1[1].play();
			break;
		case 7:
            sound_player(sound_7,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 8:
            sound_player(sound_8,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 9:
            sound_player(sound_9,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 10:
            sound_player(sound_10,true);
            $(".contentblock").css("background-color", "#62558E");
			break;
		case 11:
            setTimeout(function(){
                sound_player(sound_11,true);
            },1000);
			$(".contentblock").css("background-color", "#62558E");
			sound_group_p1[2].play();
			break;
		case 12:
            sound_player(sound_12,true);
            $(".contentblock").css("background-color", "#62558E");
			break;
		case 13:
            sound_player(sound_13,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 14:
            sound_player(sound_14,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 15:
            sound_player(sound_15,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 16:
            sound_player(sound_16,true);
            $(".contentblock").css("background-color", "#62558E");

			break;
		case 17:
            sound_player(sound_17,true);

            $(".contentblock").css("background-color", "#62558E");

			break;
		case 18:
            sound_player(sound_18,true);

            $(".contentblock").css("background-color", "#62558E");

			break;
		case 19:
            sound_player(sound_19,true);

            $(".contentblock").css("background-color", "#62558E");

			break;
		case 20:
            sound_player(sound_20,true);

            $(".contentblock").css("background-color", "#62558E");

			break;

		}



	}

	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	function activitycomplete(){
		if(cowflag == true && dogflag == true && goatflag == true && sheepflag == true && horseflag == true && pigflag == true)
				$nextBtn.show(0);
	}

	function activitycomplete2(){
		if(tigerflag == true && elephantflag == true && giraffeeflag == true && deerflag == true && rhinoflag == true && foxflag == true)
				ole.footerNotificationHandler.pageEndSetNotification();
	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

    /* For typing animation appends the text to an element specified by target class or id */
	function show_text(target, message, index, interval) {
	  if (index < message.length) {
	    $(target).append(message[index++]);
	    setTimeout(function () {
	    	show_text(target, message, index, interval);
	  	}, interval);
	  }
	}

	// uses the show text to add typing effect with sound and glowing animations
	function play_text(play_class, data, sound_data){
		show_text(play_class, data, 0, 65);	// 65 ms is the interval found out by hit and trial
		sound_data.play();
		$prevBtn.hide(0);
		sound_data.bind('ended', function(){
			// $(play_class).removeClass('text_on');
			// $(play_class).addClass('text_off');
			// $(play_class).addClass('go_off');
			if(!last_page) {
				$nextBtn.show(0);
			} else {
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
	}


});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

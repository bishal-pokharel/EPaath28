var imgpath = $ref + "/";

var soundAsset = $ref+"/sounds/";

var title = new buzz.sound(soundAsset + "s1_p1.ogg");
var ant_0 = new buzz.sound(soundAsset + "ant/ant.ogg");
var ant_1 = new buzz.sound(soundAsset + "ant/line1_1.ogg");
var ant_2 = new buzz.sound(soundAsset + "ant/line1_2.ogg");
var ant_3 = new buzz.sound(soundAsset + "ant/line1_3.ogg");

var ant_sounds = [ant_0, ant_1, ant_2, ant_3];

var duck_0 = new buzz.sound(soundAsset + "duck/duck.ogg");
var duck_1 = new buzz.sound(soundAsset + "duck/line5_1.ogg");
var duck_2 = new buzz.sound(soundAsset + "duck/line5_2.ogg");
var duck_3 = new buzz.sound(soundAsset + "duck/line5_3.ogg");

var duck_sounds = [duck_0, duck_1, duck_2, duck_3];

var pig_0 = new buzz.sound(soundAsset + "pig/pig.ogg");
var pig_1 = new buzz.sound(soundAsset + "pig/line9_1.ogg");
var pig_2 = new buzz.sound(soundAsset + "pig/line9_2.ogg");
var pig_3 = new buzz.sound(soundAsset + "pig/line9_3.ogg");

var pig_sounds = [pig_0, pig_1, pig_2, pig_3];

var rabbit_0 = new buzz.sound(soundAsset + "rabbit/rabbit.ogg");
var rabbit_1 = new buzz.sound(soundAsset + "rabbit/line11_1.ogg");
var rabbit_2 = new buzz.sound(soundAsset + "rabbit/line11_2.ogg");
var rabbit_3 = new buzz.sound(soundAsset + "rabbit/line11_3.ogg");
var rabbit_4 = new buzz.sound(soundAsset + "rabbit/line11_4.ogg");

var rabbit_sounds = [rabbit_0, rabbit_1, rabbit_2, rabbit_3, rabbit_4];

var chicken_0 = new buzz.sound(soundAsset + "chicken/chicken.ogg");
var chicken_1 = new buzz.sound(soundAsset + "chicken/line3_1.ogg");
var chicken_2 = new buzz.sound(soundAsset + "chicken/line3_2.ogg");
var chicken_3 = new buzz.sound(soundAsset + "chicken/line3_3.ogg");

var chicken_sounds = [chicken_0, chicken_1, chicken_2,chicken_3];

var buffalo_0 = new buzz.sound(soundAsset + "buffalo/buffalo.ogg");
var buffalo_1 = new buzz.sound(soundAsset + "buffalo/line2_1.ogg");
var buffalo_2 = new buzz.sound(soundAsset + "buffalo/line2_2.ogg");
var buffalo_3 = new buzz.sound(soundAsset + "buffalo/line2_3.ogg");
var buffalo_4 = new buzz.sound(soundAsset + "buffalo/line2_4.ogg");

var buffalo_sounds = [buffalo_0, buffalo_1, buffalo_2, buffalo_3, buffalo_4];

var pigeon_0 = new buzz.sound(soundAsset + "pigeon/pigeon.ogg");
var pigeon_1 = new buzz.sound(soundAsset + "pigeon/line10_1.ogg");
var pigeon_2 = new buzz.sound(soundAsset + "pigeon/line10_2.ogg");
var pigeon_3 = new buzz.sound(soundAsset + "pigeon/line10_3.ogg");

var pigeon_sounds = [pigeon_0, pigeon_1, pigeon_2, pigeon_3];

var fish_0 = new buzz.sound(soundAsset + "fish/fish.ogg");
var fish_1 = new buzz.sound(soundAsset + "fish/line6_1.ogg");
var fish_2 = new buzz.sound(soundAsset + "fish/line6_2.ogg");
var fish_3 = new buzz.sound(soundAsset + "fish/line6_3.ogg");

var fish_sounds = [fish_0, fish_1, fish_2, fish_3];

var zebra_0 = new buzz.sound(soundAsset + "zebra/zebra.ogg");
var zebra_1 = new buzz.sound(soundAsset + "zebra/line15_1.ogg");
var zebra_2 = new buzz.sound(soundAsset + "zebra/line15_2.ogg");
var zebra_3 = new buzz.sound(soundAsset + "zebra/line15_3.ogg");

var zebra_sounds = [zebra_0, zebra_1, zebra_2, zebra_3];

var sheep_0 = new buzz.sound(soundAsset + "sheep/sheep.ogg");
var sheep_1 = new buzz.sound(soundAsset + "sheep/line12_1.ogg");
var sheep_2 = new buzz.sound(soundAsset + "sheep/line12_2.ogg");
var sheep_3 = new buzz.sound(soundAsset + "sheep/line12_3.ogg");

var sheep_sounds = [sheep_0, sheep_1, sheep_2, sheep_3];

var monkey_0 = new buzz.sound(soundAsset + "monkey/monkey.ogg");
var monkey_1 = new buzz.sound(soundAsset + "monkey/line7_1.ogg");
var monkey_2 = new buzz.sound(soundAsset + "monkey/line7_2.ogg");
var monkey_3 = new buzz.sound(soundAsset + "monkey/line7_3.ogg");

var monkey_sounds = [monkey_0, monkey_1, monkey_2, monkey_3];

var donkey_0 = new buzz.sound(soundAsset + "donkey/donkey.ogg");
var donkey_1 = new buzz.sound(soundAsset + "donkey/line4_1.ogg");
var donkey_2 = new buzz.sound(soundAsset + "donkey/line4_2.ogg");
var donkey_3 = new buzz.sound(soundAsset + "donkey/line4_3.ogg");

var donkey_sounds = [donkey_0, donkey_1, donkey_2, donkey_3];

var mosquito_0 = new buzz.sound(soundAsset + "mosquito/mosquito.ogg");
var mosquito_1 = new buzz.sound(soundAsset + "mosquito/line8_1.ogg");
var mosquito_2 = new buzz.sound(soundAsset + "mosquito/line8_2.ogg");
var mosquito_3 = new buzz.sound(soundAsset + "mosquito/line8_3.ogg");
var mosquito_4 = new buzz.sound(soundAsset + "mosquito/line8_4.ogg");

var mosquito_sounds = [mosquito_0, mosquito_1, mosquito_2, mosquito_3, mosquito_4];

var tiger_0 = new buzz.sound(soundAsset + "tiger/tiger.ogg");
var tiger_1 = new buzz.sound(soundAsset + "tiger/line14_1.ogg");
var tiger_2 = new buzz.sound(soundAsset + "tiger/line14_2.ogg");
var tiger_3 = new buzz.sound(soundAsset + "tiger/line14_3.ogg");
var tiger_4 = new buzz.sound(soundAsset + "tiger/line14_4.ogg");

var tiger_sounds = [tiger_0, tiger_1, tiger_2, tiger_3, tiger_4];

var content=[
	//1st slide
    {
        additionalclasscontentblock:"firstslidebg",
        uppertexttopclass:'uppertexttop',
         uppertextblock:[{
            textclass : "firsslidetext",
            textdata : data.lesson.chapter
        }],
         imageblock: [{
                imagetoshow: [{
                        imgclass: "pigdance",
                        imgsrc : imgpath + "images/pig_dancing.gif"
                    }]
        }],
    },
	{


        imageblock: [{
			imagetoshow: [{
						imgclass: "slide1img",
						imgsrc : imgpath + "images/duck.jpg"
					}],
            imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text1,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text2_1,
        },{
            textclass : "description",
            textdata : data.string.p1text2_2,
        },{
            textclass : "description",
            textdata : data.string.p1text2_3,
        }
        ],


	},

	//2nd slide
	{
        imageblock: [{
				imagetoshow: [{
						imgclass: "slide2img",
						imgsrc : imgpath + "images/pig.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text3,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text4_1,
        },{
            textclass : "description",
            textdata : data.string.p1text4_2,
        },
            {
                textclass : "description",
                textdata : data.string.p1text4_3,
            }
        ],

	},


//3rd slide
	{
        imageblock: [{
				imagetoshow: [{
						imgclass: "slide3img",
						imgsrc : imgpath + "images/rabbit.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text5,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text6_1,
        },{
            textclass : "description",
            textdata : data.string.p1text6_2,
        },{
            textclass : "description",
            textdata : data.string.p1text6_3,
        },{
            textclass : "description",
            textdata : data.string.p1text6_4,
        }

        ],

	},

		//4th slide
	{
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide1img",
						imgsrc : imgpath + "images/chicken.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text7,
        }],
			}],
        lowertextblock:[{

            textclass : "description",
            textdata : data.string.p1text8_1,
        },{

            textclass : "description",
            textdata : data.string.p1text8_2,
        },
            {

                textclass : "description",
                textdata : data.string.p1text8_3,
            }
        ],
	},
	   //5th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide2img",
						imgsrc : imgpath + "images/buffalo.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text9,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text10_1,
        },{

            textclass : "description",
            textdata : data.string.p1text10_3,
        },{

            textclass : "description",
            textdata : data.string.p1text10_4,
        },{

            textclass : "description",
            textdata : data.string.p1text10_5,
        }
        ],
	},
    //6th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide3img",
						imgsrc : imgpath + "images/pigeon.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text11,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text12_1,
        },{
            textclass : "description",
            textdata : data.string.p1text12_2,
        },
            {
                textclass : "description",
                textdata : data.string.p1text12_3,
            }

        ],
	},
        //7th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide1img_fish",
						imgsrc : imgpath + "images/fish.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text13,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text14_1,
        },{
            textclass : "description",
            textdata : data.string.p1text14_2,
        },
            {
                textclass : "description",
                textdata : data.string.p1text14_3,
            }
        ],
	},
    //8th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide2img",
						imgsrc : imgpath + "images/zebra.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text15,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text16_1,
        },{
            textclass : "description",
            textdata : data.string.p1text16_2,
        },{
            textclass : "description",
            textdata : data.string.p1text16_3,
        }
        ],
	},
    //9th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide3img",
						imgsrc : imgpath + "images/ant.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text17,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text18_1,
        },{
            textclass : "description",
            textdata : data.string.p1text18_2,
        },{
            textclass : "description",
            textdata : data.string.p1text18_3,
        }

        ],
	},
    //10th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide1img_sheep",
						imgsrc : imgpath + "images/sheep.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text19,
       			 }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text20_1,
        },{

            textclass : "description",
            textdata : data.string.p1text20_2,
        },{

            textclass : "description",
            textdata : data.string.p1text20_3,
        }
        ],
	},
    //11th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide2img",
						imgsrc : imgpath + "images/monkey.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text21,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text22_1,
        },{
            textclass : "description",
            textdata : data.string.p1text22_2,
        },{
            textclass : "description",
            textdata : data.string.p1text22_3,
        },

        ],
	},
    //12th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide3img",
						imgsrc : imgpath + "images/donkey.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text23,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text24_1,
        },{
            textclass : "description",
            textdata : data.string.p1text24_2,
        },
            {
                textclass : "description",
                textdata : data.string.p1text24_3,
            }
        ],
	},
    //13th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide1img_mosquito",
						imgsrc : imgpath + "images/mosquito.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text25,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text26_1,
        },{
            textclass : "description",
            textdata : data.string.p1text26_2,
        },{
            textclass : "description",
            textdata : data.string.p1text26_3,
        },{
            textclass : "description",
            textdata : data.string.p1text26_4,
        }
        ],
	},
    //14th slide
    {
		imageblock: [{
				imagetoshow: [{
						imgclass: "slide2img",
						imgsrc : imgpath + "images/tiger.jpg"
					}],
                 imagelabels:[{
                     imagelabelclass:"imglabelcss1",
                     imagelabeldata:data.string.p1text27,
        }],
			}],
        lowertextblock:[{
            textclass : "description",
            textdata : data.string.p1text28_1,
        },{
            textclass : "description",
            textdata : data.string.p1text28_2,
        },{
            textclass : "description",
            textdata : data.string.p1text28_3,
        },{
            textclass : "description",
            textdata : data.string.p1text28_4,
        }
        ],
	},
];
// 	return content;
// }


$(function(){


	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*recalculateHeightWidth();*/
	var $total_page = content.length;

	var total_page = 0;
   var soundid = title;
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program


	function playaudio(array, index, $text){
        soundid.stop();
        if(index >= array.length){
			if(countNext < (total_page-1)){
				$nextBtn.show(0);
			}else{
				 ole.footerNotificationHandler.pageEndSetNotification();
			}
			$prevBtn.show(0);
			return true;
		}else{
		    soundid = array[index];
            soundid.play();
            soundid.bind("ended", function(){
				$($text[index]).addClass("fade_in1");
				index++;
				playaudio(array, index, $text);
			});
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var clickcount = 0;

		$board.html(html);
		vocabcontroller.findwords(countNext);
		$(".card").click(function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
					$nextBtn.show(0);
					/*
						TODO: .show(0) uses 400 as default value that is problematic in certain scenarios so inorder to avoid such things we need to explicitly state 0;
					*/
		});


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		/*
		 TODO: .show(0)/ .hide(0) uses 400 as default value that is problematic in certain scenarios so inorder to avoid such things we need to explicitly state 0;
		 */
        soundid.stop();
        switch (countNext) {
            case 0:
                soundid =title;
                soundid.play();
                soundid.bind("ended", function(){
                    $nextBtn.show(0);
                });
            break;
			case 1:
			$(".contentblock").css("background","#9EDED3");
			playaudio(duck_sounds, 0, $(".description"));
            $prevBtn.hide(0);
			break;
			case 2:
			$(".contentblock").css("background","#ACDEB6");
			playaudio(pig_sounds, 0, $(".description"));
			break;
			case 3:
			$(".contentblock").css("background","#FCE5CD");
			playaudio(rabbit_sounds, 0, $(".description"));
			break;
			case 4:
		    $(".contentblock").css("background","#9EDED3");
		    playaudio(chicken_sounds, 0, $(".description"));
			break;
            case 5:
			$(".contentblock").css("background","#ACDEB6");
			playaudio(buffalo_sounds, 0, $(".description"));
			break;
            case 6:
			$(".contentblock").css("background","#FCE5CD");
			playaudio(pigeon_sounds, 0, $(".description"));
			break;
            case 7:
			$(".contentblock").css("background","#9EDED3");
			playaudio(fish_sounds, 0, $(".description"));
			break;
            case 8:
			$(".contentblock").css("background","#ACDEB6");
			playaudio(zebra_sounds, 0, $(".description"));
			break;
            case 9:
			$(".contentblock").css("background","#FCE5CD");
			playaudio(ant_sounds, 0, $(".description"));
			break;
            case 10:
			$(".contentblock").css("background","#9EDED3");
			playaudio(sheep_sounds, 0, $(".description"));
			break;
            case 11:
			$(".contentblock").css("background","#ACDEB6");
			playaudio(monkey_sounds, 0, $(".description"));
			break;
            case 12:
			$(".contentblock").css("background","#FCE5CD");
			playaudio(donkey_sounds, 0, $(".description"));
			break;
            case 13:
			$(".contentblock").css("background","#9EDED3");
			playaudio(mosquito_sounds, 0, $(".description"));
			break;
            case 14:
			$(".contentblock").css("background","#ACDEB6");
			playaudio(tiger_sounds, 0, $(".description"));
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();

			break;

			}



	}
	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}



	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/";
var soundAsset = $ref+"/sounds/";
var sound_1 = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "s3_p2.ogg"));
// var content;

// function getContent(data){
	var content=[
	//slide 0
	{
		imageblock: [
		{
			imagetoshow: [{
				imgclass: "naming",
				imgsrc : imgpath + "images/name_cartoon.png"				
			}],
			imagelabels: [{
					imagelabelclass: "fronttext1",	
					imagelabeldata: data.string.p3_s0
			}]

		}
		],
	},
	//slide 1
	{
        uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color1",
				imgsrc : imgpath + "images/fish.jpg"				
			},


			{
				imgclass: "top1 droppable_f",
				imgsrc : imgpath + "images/letters/container.png"				
			},

			{
				imgclass: "top1 static_i",
				imgsrc : imgpath + "images/letters/i.png"	
			},
			{
				imgclass: "top1 static_s",
				imgsrc : imgpath + "images/letters/s.png"	
			},
			{
				imgclass: "top1 droppable_h",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top2 static_a",
				imgsrc : imgpath + "images/letters/a.png"	
			},
			{
				imgclass: "top2 static_p",
				imgsrc : imgpath + "images/letters/p.png"	
			},
			{
				imgclass: "top2 draggable_f",
				imgsrc : imgpath + "images/letters/f.png"	
			},
			{
				imgclass: "top2 static_g",
				imgsrc : imgpath + "images/letters/g.png"	
			},
			{
				imgclass: "top2 draggable_h",
				imgsrc : imgpath + "images/letters/h.png"	
			},
                {
				imgclass: "top1 correct",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text2,
            }
        ]
	},

//slide 2
{
	 uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color2",
				imgsrc : imgpath + "images/rabbit.jpg"				
			},


			{
				imgclass: "top1 static_r2",
				imgsrc : imgpath + "images/letters/r.png"				
			},

			{
				imgclass: "top1 droppable_a2",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 static_b2",
				imgsrc : imgpath + "images/letters/b.png"	
			},
			{
				imgclass: "top1 static_bb2",
				imgsrc : imgpath + "images/letters/b.png"	
			},
			{
				imgclass: "top1 droppable_i2",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 droppable_t2",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top2 static_q2",
				imgsrc : imgpath + "images/letters/q.png"	
			},
			{
				imgclass: "dtop2 draggable_i2",
				imgsrc : imgpath + "images/letters/i.png"	
			},
			{
				imgclass: "dtop2 draggable_a2",
				imgsrc : imgpath + "images/letters/a.png"	
			},
                {
				imgclass: "dtop2 draggable_t2",
				imgsrc : imgpath + "images/letters/t.png"	
			},{
				imgclass: "dtop2 draggable_tt2",
				imgsrc : imgpath + "images/letters/t.png"	
			},{
				imgclass: "top2 static_z2",
				imgsrc : imgpath + "images/letters/z.png"	
			},
                {
				imgclass: "top1 correct2",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text3,
            }
        ]
},

//slide 3
{
	uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color3",
				imgsrc : imgpath + "images/chicken.jpg"				
			},


			{
				imgclass: "top1 droppable_c3",
				imgsrc : imgpath + "images/letters/container.png"				
			},

			{
				imgclass: "top1 droppable_h3",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 droppable_i3",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 static_c3",
				imgsrc : imgpath + "images/letters/c.png"	
			},
			{
				imgclass: "top1 static_k3",
				imgsrc : imgpath + "images/letters/k.png"	
			},
			{
				imgclass: "top1 droppable_e3",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 droppable_n3",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "dtop2 draggable_c3",
				imgsrc : imgpath + "images/letters/c.png"	
			},
			{
				imgclass: "dtop2 draggable_n3",
				imgsrc : imgpath + "images/letters/n.png"	
			},
                {
				imgclass: "dtop2 draggable_i3",
				imgsrc : imgpath + "images/letters/i.png"	
			},{
				imgclass: "dtop2 draggable_e3",
				imgsrc : imgpath + "images/letters/e.png"	
			},{
				imgclass: "top2 static_f3",
				imgsrc : imgpath + "images/letters/f.png"	
			},{
				imgclass: "dtop2 draggable_h3",
				imgsrc : imgpath + "images/letters/h.png"	
			},
                {
				imgclass: "top1 correct3",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text4,
            }
        ]
},



//slide 4
{
	uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color4",
				imgsrc : imgpath + "images/pig.jpg"				
			},


			{
				imgclass: "top1 static_p4",
				imgsrc : imgpath + "images/letters/p.png"				
			},

			{
				imgclass: "top1 static_i4",
				imgsrc : imgpath + "images/letters/i.png"	
			},
			{
				imgclass: "top1 droppable_g4",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top2 static_q4",
				imgsrc : imgpath + "images/letters/q.png"	
			},
			{
				imgclass: "top2 static_l4",
				imgsrc : imgpath + "images/letters/l.png"	
			},
                {
				imgclass: "top2 static_a4",
				imgsrc : imgpath + "images/letters/a.png"	
			},{
				imgclass: "top2 static_t4",
				imgsrc : imgpath + "images/letters/t.png"	
			},{
				imgclass: "dtop2 draggable_g4",
				imgsrc : imgpath + "images/letters/g.png"	
			},{
				imgclass: "top2 static_z4",
				imgsrc : imgpath + "images/letters/z.png"	
			},
                {
				imgclass: "top1 correct",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text5,
            }
        ]
},

	//slide 5
	{
		uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color4",
				imgsrc : imgpath + "images/tiger.jpg"				
			},


			{
				imgclass: "top1 droppable_t5",
				imgsrc : imgpath + "images/letters/container.png"				
			},

			{
				imgclass: "top1 static_i5",
				imgsrc : imgpath + "images/letters/i.png"	
			},
			{
				imgclass: "top1 static_g5",
				imgsrc : imgpath + "images/letters/g.png"	
			},
			{
				imgclass: "top1 droppable_e5",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 droppable_r5",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "dtop2 draggable_t5",
				imgsrc : imgpath + "images/letters/t.png"	
			},
			{
				imgclass: "top2 static_q5",
				imgsrc : imgpath + "images/letters/q.png"	
			},
                {
				imgclass: "dtop2 draggable_r5",
				imgsrc : imgpath + "images/letters/r.png"	
			},{
				imgclass: "dtop2 draggable_e5",
				imgsrc : imgpath + "images/letters/e.png"	
			},{
				imgclass: "top2 static_l5",
				imgsrc : imgpath + "images/letters/l.png"	
			},{
				imgclass: "top2 static_o5",
				imgsrc : imgpath + "images/letters/o.png"	
			},
                {
				imgclass: "top1 correct3",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text6,
            }
        ]
	},

	//slide 6
	{
		uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "image_main color4",
				imgsrc : imgpath + "images/monkey.jpg"				
			},


			{
				imgclass: "top1 droppable_m6",
				imgsrc : imgpath + "images/letters/container.png"				
			},

			{
				imgclass: "top1 droppable_o6",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 static_n6",
				imgsrc : imgpath + "images/letters/n.png"	
			},
			{
				imgclass: "top1 static_k6",
				imgsrc : imgpath + "images/letters/k.png"	
			},
			{
				imgclass: "top1 droppable_e6",
				imgsrc : imgpath + "images/letters/container.png"	
			},
                {
				imgclass: "top1 droppable_y6",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "dtop2 draggable_e6",
				imgsrc : imgpath + "images/letters/e.png"	
			},
			{
				imgclass: "top2 static_q6",
				imgsrc : imgpath + "images/letters/q.png"	
			},
                {
				imgclass: "dtop2 draggable_o6",
				imgsrc : imgpath + "images/letters/o.png"	
			},{
				imgclass: "dtop2 draggable_m6",
				imgsrc : imgpath + "images/letters/m.png"	
			},{
				imgclass: "top2 static_a6",
				imgsrc : imgpath + "images/letters/a.png"	
			},{
				imgclass: "dtop2 draggable_y6",
				imgsrc : imgpath + "images/letters/y.png"	
			},
                {
				imgclass: "top1 correct3",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text7,
            }
        ]
	},

	//slide 7
	{
        uppertextblock :[
            {
                textclass: "ubt",
                textdata : data.string.p3text1,
            }
        ],
	   imageblock: [
		{
			imagetoshow: [
                {
				imgclass: "center color4",
				imgsrc : imgpath + "images/buffalo.jpg"				
			},


			{
				imgclass: "top1 droppable_b7",
				imgsrc : imgpath + "images/letters/container.png"				
			},

			{
				imgclass: "top1 droppable_u7",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "top1 static_f7",
				imgsrc : imgpath + "images/letters/f.png"	
			},
			{
				imgclass: "top1 static_ff7",
				imgsrc : imgpath + "images/letters/f.png"	
			},
			{
				imgclass: "top1 droppable_a7",
				imgsrc : imgpath + "images/letters/container.png"	
			},
                	{
				imgclass: "top1 droppable_l7",
				imgsrc : imgpath + "images/letters/container.png"	
			},
                	{
				imgclass: "top1 droppable_o7",
				imgsrc : imgpath + "images/letters/container.png"	
			},
			{
				imgclass: "dtop2 draggable_l7",
				imgsrc : imgpath + "images/letters/l.png"	
			},
			{
				imgclass: "top2 static_t7",
				imgsrc : imgpath + "images/letters/t.png"	
			},
                {
				imgclass: "dtop2 draggable_u7",
				imgsrc : imgpath + "images/letters/u.png"	
			},{
				imgclass: "dtop2 draggable_a7",
				imgsrc : imgpath + "images/letters/a.png"	
			},{
				imgclass: "dtop2 draggable_o7",
				imgsrc : imgpath + "images/letters/o.png"	
			},{
				imgclass: "dtop2 draggable_b7",
				imgsrc : imgpath + "images/letters/b.png"	
			},
                {
				imgclass: "top1 correct3",
				imgsrc : "images/correct.png",
			},


			],

		}
		],
        lowertextblock:[
            {
                textclass:"lbt",
                textdata:data.string.p3text8,
            }
        ]
	}
	];
// 	return content;
// }


$(function(){

	
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var $correct = $(".correct");
    var $correct_text = $(".lbt");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*recalculateHeightWidth();*/
	
	var total_page = 0;
	loadTimelineProgress($total_page, countNext + 1);
	
	var soundid = sound_1;
	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

		
	//controls the navigational state of the program
	function navigationController(){
        
		if(countNext == 0 && total_page != 1){
			$nextBtn.show(0);
		}else if(countNext > 0 && countNext < (total_page-1)){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}else if(countNext == total_page-1){
			$prevBtn.show(0);
            
		}
	}

    function sound_player(sound_id,navigate){
        soundid = sound_id;
        soundid.stop();
        soundid.play();
        soundid.bind("ended", function() {
            navigate?navigationController():"";
        });
    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
                sound_player(sound_1,true);
                break;
            case 1:
                sound_player(sound_2,false);
                $nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
	
				function nextcheck() {
					if (p1 && p2) {
						play_correct_incorrect_sound(1);
						$nextBtn.show(0);
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}
	
				$(".draggable_f").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_h").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_a").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_p").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_g").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 
	
				$(".droppable_f").droppable({
					accept : ".draggable_f",
					drop : function(event, ui) {
						$(".draggable_f").css({
							"left": "",
							"z-index" : "auto",
							"right" : "52.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_f").css({
							"height" : "0%"
						});
						$(".draggable_f").removeClass('top2');
						p1 = true;
						nextcheck();
					}
				});
	
				$(".droppable_h").droppable({
					accept : ".draggable_h",
					drop : function(event, ui) {
						$(".draggable_h").css({
							"left": "",
							"z-index" : "auto",
							"right" : "34%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_h").css({
							"height" : "0%"
						});
						$(".draggable_h").removeClass('top2');
						p2 = true;
						nextcheck();
					}
				}); 
				break;
			case 2:
				$nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
				var p3_1 = false;
				var p3_2 = false; 

				function nextcheck2() {
					if (p1 && p2 && (p3_1 || p3_2)) {
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(0);
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct2").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
						
						if(p3_1){
							$(".draggable_tt2").hide(0);
						}else{
							$(".draggable_t2").hide(0);
						}
					}
				}

				$(".static_q2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_i2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_a2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_t2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_tt2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".static_z2").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".droppable_a2").droppable({
					accept : ".draggable_a2",
					drop : function(event, ui) {
						$(".draggable_a2").css({
							"left": "",
							"z-index" : "auto",
							"right" : "52%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_a2").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck2();
					}
				});
	
				$(".droppable_i2").droppable({
					accept : ".draggable_i2",
					drop : function(event, ui) {
						$(".draggable_i2").css({
							"left": "",
							"z-index" : "auto",
							"right" : "34%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_i2").css({
							"height" : "0%"
						});
	
						p2 = true;
						nextcheck2();
					}
				});
				$(".droppable_t2").droppable({
					accept : ".draggable_t2, .draggable_tt2",
					drop : function(event, ui) {
						
						var $dropped = $(ui.draggable[0]);
						if($dropped.hasClass("draggable_t2")){
							$(".draggable_t2").css({
								"left": "",
								"z-index" : "auto",
								"right" : "28%",
								"top" : "57%",
								"height" : "7%",
							});
							p3_1 = true;
						}else{
							$(".draggable_tt2").css({
								"left": "",
								"z-index" : "auto",
								"right" : "28%",
								"top" : "57%",
								"height" : "7%",
							});
							p3_2 = true;
						}
						
						
						$(".droppable_t2").css({
							"height" : "0%"
						});
	
						nextcheck2();
					}
				}); 
				break;
            case 3:
				$nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
				var p3 = false;
				var p4 = false;
				var p5 = false; 
		
				function nextcheck3() {
					if (p1 && p2 && p3 && p4 && p5) {
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(0);
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct3").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}

				$(".draggable_n3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_i3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_e3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_f3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_h3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".draggable_c3").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 
				
				$(".droppable_c3").droppable({
					accept : ".draggable_c3",
					drop : function(event, ui) {
						$(".draggable_c3").css({
							"left": "",
							"z-index" : "auto",
							"right" : "60.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_c3").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck3();
					}
				});
	
				$(".droppable_h3").droppable({
					accept : ".draggable_h3",
					drop : function(event, ui) {
						$(".draggable_h3").css({
							"left": "",
							"z-index" : "auto",
							"right" : "54.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_h3").css({
							"height" : "0%"
						});
	
						p2 = true;
						nextcheck3();
					}
				});
				$(".droppable_i3").droppable({
					accept : ".draggable_i3",
					drop : function(event, ui) {
						$(".draggable_i3").css({
							"left": "",
							"z-index" : "auto",
							"right" : "48%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_i3").css({
							"height" : "0%"
						});
	
						p3 = true;
						nextcheck3();
					}
				});
				$(".droppable_e3").droppable({
					accept : ".draggable_e3",
					drop : function(event, ui) {
						$(".draggable_e3").css({
							"left": "",
							"z-index" : "auto",
							"right" : "30.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_e3").css({
							"height" : "0%"
						});
	
						p4 = true;
						nextcheck3();
					}
				});
				$(".droppable_n3").droppable({
					accept : ".draggable_n3",
					drop : function(event, ui) {
						$(".draggable_n3").css({
							"left": "",
							"z-index" : "auto",
							"right" : "25%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_n3").css({
							"height" : "0%"
						});
	
						p5 = true;
						nextcheck3();
					}
				}); 


                
                break;
            case 4:
				$nextBtn.hide(0);
				var p1 = false; 

				function nextcheck4() {
					if (p1) {
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(0);
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}
	
				$(".static_q4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_l4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_a4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_t4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_g4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".static_z4").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 
	
				$(".droppable_g4").droppable({
					accept : ".draggable_g4",
					drop : function(event, ui) {
						$(".draggable_g4").css({
							"left": "",
							"z-index" : "auto",
							"right" : "38%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_g4").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck4();
					}
				}); 

                break;
            case 5:
	            
				$nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
				var p3 = false; 

				function nextcheck5() {
					if (p1 && p2 && p3) {
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(0);
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct3").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}

				
				$(".draggable_t5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_q5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_r5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_e5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_l5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".static_o5").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 

				$(".droppable_t5").droppable({
					accept : ".draggable_t5",
					drop : function(event, ui) {
						$(".draggable_t5").css({
							"left": "",
							"z-index" : "auto",
							"right" : "56%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_t5").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck5();
					}
				});
	
				$(".droppable_e5").droppable({
					accept : ".draggable_e5",
					drop : function(event, ui) {
						$(".draggable_e5").css({
							"left": "",
							"z-index" : "auto",
							"right" : "38.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_e5").css({
							"height" : "0%"
						});
	
						p2 = true;
						nextcheck5();
					}
				});
				$(".droppable_r5").droppable({
					accept : ".draggable_r5",
					drop : function(event, ui) {
						$(".draggable_r5").css({
							"left": "",
							"z-index" : "auto",
							"right" : "32.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_r5").css({
							"height" : "0%"
						});
	
						p3 = true;
						nextcheck5();
					}
				}); 


            	break;
            case 6:
	           
				$nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
				var p3 = false;
				var p4 = false;
	
				function nextcheck6() {
					if (p1 && p2 && p3 && p4) {
                        play_correct_incorrect_sound(1);
                        $nextBtn.show(0);
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct3").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}
	
	
				$(".draggable_e6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_q6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_o6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_m6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_a6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".draggable_y6").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 

				$(".droppable_m6").droppable({
					accept : ".draggable_m6",
					drop : function(event, ui) {
						$(".draggable_m6").css({
							"left": "",
							"z-index" : "auto",
							"right" : "58%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_m6").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck6();
					}
				});
	
				$(".droppable_o6").droppable({
					accept : ".draggable_o6",
					drop : function(event, ui) {
						$(".draggable_o6").css({
							"left": "",
							"z-index" : "auto",
							"right" : "52%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_o6").css({
							"height" : "0%"
						});
	
						p2 = true;
						nextcheck6();
					}
				});
				$(".droppable_e6").droppable({
					accept : ".draggable_e6",
					drop : function(event, ui) {
						var $this = $(this);
						$(".draggable_e6").css({
							"left": "",
							"z-index" : "auto",
							"right" : "34.5%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_e6").css({
							"height" : "0%"
						});
	
						p3 = true;
						nextcheck6();
					}
				});
				$(".droppable_y6").droppable({
					accept : ".draggable_y6",
					drop : function(event, ui) {
						$(".draggable_y6").css({
							"left": "",
							"z-index" : "auto",
							"right" : "28%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_y6").css({
							"height" : "0%"
						});
	
						p4 = true;
						nextcheck6();
					}
				});
				break;

            case 7:
	           
				$nextBtn.hide(0);
				var p1 = false;
				var p2 = false;
				var p3 = false;
				var p4 = false;
				var p5 = false; 

		
				function nextcheck7() {
					if (p1 && p2 && p3 && p4 && p5) {
                        play_correct_incorrect_sound(1);
                        ole.footerNotificationHandler.pageEndSetNotification();
						//                     $(".draggable_a2").removeClass('top2');
						//                     $(".draggable_i2").removeClass('top2');
						//                         $(".draggable_t2").removeClass('top2');
						$(".top2").css({
							"visibility" : "hidden"
						});
						$(".correct3").css({
							"visibility" : "visible"
						});
						$(".lbt").css({
							"visibility" : "visible"
						});
					}
				}

				$(".draggable_l7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".static_t7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_u7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_a7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
	
				$(".draggable_o7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				});
				$(".draggable_b7").draggable({
					cursor : "move",
					revert : "invalid",
					zIndex : 1000,
				}); 



				
				$(".droppable_b7").droppable({
					accept : ".draggable_b7",
					drop : function(event, ui) {
						$(".draggable_b7").css({
							"left": "",
							"z-index" : "auto",
							"right" : "60%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_b7").css({
							"height" : "0%"
						});
	
						p1 = true;
						nextcheck7();
					}
				});
	
				$(".droppable_u7").droppable({
					accept : ".draggable_u7",
					drop : function(event, ui) {
						$(".draggable_u7").css({
							"left": "",
							"z-index" : "auto",
							"right" : "54%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_u7").css({
							"height" : "0%"
						});
	
						p2 = true;
						nextcheck7();
					}
				});
				$(".droppable_a7").droppable({
					accept : ".draggable_a7",
					drop : function(event, ui) {
						$(".draggable_a7").css({
							"left": "",
							"z-index" : "auto",
							"right" : "36%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_a7").css({
							"height" : "0%"
						});
	
						p3 = true;
						nextcheck7();
					}
				});
				$(".droppable_l7").droppable({
					accept : ".draggable_l7",
					drop : function(event, ui) {
						$(".draggable_l7").css({
							"left": "",
							"z-index" : "auto",
							"right" : "30%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_l7").css({
							"height" : "0%"
						});
	
						p4 = true;
						nextcheck7();
					}
				});
				$(".droppable_o7").droppable({
					accept : ".draggable_o7",
					drop : function(event, ui) {
						$(".draggable_o7").css({
							"left": "",
							"z-index" : "auto",
							"right" : "24%",
							"top" : "57%",
							"height" : "7%",
						});
						$(".droppable_o7").css({
							"height" : "0%"
						});
	
						p5 = true;
						nextcheck7();
					}
				});
				break;
			default:
				break;
			}
            
	}
	
	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });
	
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);
		
		

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
		
	}
	

	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});
	
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });
	
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			
			
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;
					
					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					
					
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


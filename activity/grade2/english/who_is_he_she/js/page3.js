var imgpath = $ref + "/images/fvf/";
var imgpath2 = $ref + "/images/clothes/";
var imgpath3 = $ref + "/images/img/";
var content;
	content = [{
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img",
		addclass: "midtext2 animated fadeInUp",
		uppertextblock: [{
		texttoshow: [
			{
				textdata: data.string.p12text1
			}
			]
		}],
	},

	{
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img",
		addclass: "midtext2 animated fadeInUp",
		uppertextblock: [{
		texttoshow: [
			{
				textdata: data.string.p13text1
			}
			]
		}],
	},

	{
		
		hasheaderblock: true,
		additionalclasscontentblock:"bg-img",
		addclass: "head3",
		uppertextblock: [{
			texttoshow: [{
				textclass: "dialog",
				textdata: data.string.p14text1
			}, {
				textclass: "dialog2",
				textdata: data.string.p14text2
			}, {
				textclass: "dialog3",
				textdata: data.string.p14text3
			}, {
				textclass: "dialog4",
				textdata: data.string.p14text4
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "cloud",
				imgsrc: imgpath3 + "cloud.png"
			},
			{
				imgclass: "cotton",
				imgsrc: imgpath2 + "cotton-plant.jpg"
			}, {
				imgid: "cotton-zoom",
				imgsrc: imgpath2 + "cotton-zoom.jpg"
			}, {
				imgclass: "cottondrop",
				imgid: "cotton-shirt",
				imgsrc: imgpath2 + "cotton-shirt.png"
			}, {
				imgclass: "cottondrop",
				imgid: "cotton-pillow",
				imgsrc: imgpath2 + "cotton-pillow.png"
			}, {
				imgclass: "cottondrop",
				imgid: "cotton-dhaka",
				imgsrc: imgpath2 + "dhaka.png"
			}]
		},{
			imagelabels: [{
				imagelabelclass: "dialog5",
				imagelabeldata: data.string.p14text5
			}]
		}],
	},
	{
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img2",
		addclass: "midtext2 animated fadeIn",
		uppertextblock: [{
		    texttoshow: [
			    {
				    textdata: data.string.p15text1
			    }
			]
		}],
	},
    {
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img2",
		newclass : "bg-img",
		uppertextblock: [{
		    texttoshow: [
			    {
                    textclass: "plant-hemp animated fadeIn",
				    textdata: data.string.p16text1
			    }
			]
		}],
        imageblock: [{
			imagetoshow: [{
				imgclass: "cotton animated fadeIn",
				imgsrc: imgpath2 + "Hemp-Plants.jpg"
			},
            {
				imgclass: "hemp-shirt",
				imgsrc: imgpath2 + "hemp-t-shirt.png"
			}
            ]
		}]
	},
    {
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img3",
		uppertextblock: [{
		    texttoshow: [
			    {
                    textclass: "plant-hemp animated fadeIn",
				    textdata: data.string.p17text1
			    }
			]
		}],
        imageblock: [{
			imagetoshow: [{
				imgclass: "cotton animated fadeIn",
				imgsrc: imgpath2 + "juteplant.jpg"
			},
            {
				imgclass: "hemp-shirt",
				imgsrc: imgpath2 + "jute-bag.png"
			}
            ]
		}]
	},
    {
		hasheaderblock:true,
		additionalclasscontentblock:"bg-img4",
		uppertextblock: [{
		    texttoshow: [
			    {
                    textclass: "plant-hemp animated fadeIn",
				    textdata: data.string.p18text1
			    }
			]
		}],
        imageblock: [{
			imagetoshow: [{
				imgclass: "cotton animated fadeIn",
				imgsrc: imgpath2 + "nettle-plant-closeup.jpg"
			},
            {
				imgclass: "hemp-shirt",
				imgsrc: imgpath2 + "nettle-coat.png"
			}
            ]
		}]
	}

	];


$(function() {
	// var height = $(window).height();
	// var width = $(window).width();
	// $("#board").css({"width": width, "height": (height*580/960)});
	// function recursion(){
	// if(data.string != null){
	// } else{
	// recursion();
	// }   	
	// }
	// recursion();

	$(window).resize(function() {
		recalculateHeightWidth();
	});

	function recalculateHeightWidth() {
		var heightresized = $(window).height();
		var widthresized = $(window).width();
		var factor = 960 / 580;
		var equivalentwidthtoheight = widthresized / factor;

		if (heightresized >= equivalentwidthtoheight) {
			$(".shapes_activity").css({
				"width": widthresized,
				"height": equivalentwidthtoheight
			});
		} else {
			$(".shapes_activity").css({
				"height": heightresized,
				"width": heightresized * 960 / 580
			});
		}
		// $(".shapes_activity").css({"left": "50%" , 
		// "height": "50%" ,
		// "-webkit-transform" : "translate(-50%, - 50%)",
		// "transform" : "translate(-50%, - 50%)"});
	}
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	recalculateHeightWidth();


	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController() {
		if (countNext == 0 && total_page != 1) {
			$nextBtn.show(0);
		} else if (countNext > 0 && countNext < (total_page - 1)) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == total_page - 1) {
			$prevBtn.show(0);
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		$(".midtext2").css({
				"font-size":"1.7em"
			})
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 2:
			$(".midtext2").css({
				"font-size":"1.5em"
			})
            $nextBtn.hide(0)
			$(".cotton").fadeIn(2000)
			$(".head3").delay(1500).fadeIn(2000)
				$("#cotton-zoom").delay(3000).fadeIn(10).animate({
					'left': '70%',
					'opacity': '1',
				}, 2000, function() {
					$(".dialog2").toggleClass("animated fadeInUp")
					$("#cotton-shirt").delay(3000).fadeIn(10).animate({
						'opacity': '1',
						'top': '55%'
					}, 2000, function() {
						$(".box1").fadeIn();
						$(".dialog2").toggleClass("animated fadeOut")
					});
					$(".dialog3").toggleClass("animated fadeInUp")
						$("#cotton-pillow").delay(7000).fadeIn(10).animate({
							    'left': '39%',
								'top': '60%',
								'opacity': '1',
								'height': '17%',
						},2000, function() {
							$(".box2").fadeIn();
							$(".dialog3").toggleClass("animated fadeOut")
						});
					$(".dialog4").toggleClass("animated fadeInUp").animate({
						'opacity': '1',
						'top': '270%'
					}, 2000, function() {
						$("#cotton-dhaka").delay(9000).fadeIn(10).animate({
							'left': '6%',
							'top': '55%',
							'opacity': '1'
						}, 2000, function() {
							$(".box3").fadeIn()
							$(".dialog4").toggleClass("animated fadeOut")
							$('.dialog5,.cloud').delay(2000).toggleClass("animated lightSpeedIn")
							$nextBtn.show(7000)
						})
							
				})
						
				});
				
				break;
				case 4:
				case 5:
					$nextBtn.hide(0)
                    $(".cotton").css({
						'display':'block',
                        'top':'12%'
						
                    })
                    $(".hemp-shirt").delay(1000).fadeIn(10).animate({
                        'left': '70%',
                        'opacity': '1',
						 'top':'18%'
                    }, 1500)
					$("#activity-page-next-btn-enabled").delay(2000).fadeIn()					
                    break;
					
                    case 6:
                    $(".cotton").css({
						'display':'block',
                        'top':'12%'
						
                    })
                    $(".hemp-shirt").delay(1000).fadeIn(10).animate({
						'top':'18%',
                        'left': '70%',
                        'opacity': '1'
                    }, 1500)

                    if (countNext>= 5) {
								ole.footerNotificationHandler.pageEndSetNotification();
					}
                    break;
		}
		function onDragElev(left) {

			if (left > -300) {
				left = 22;
			}
			return left;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	setTimeout(function() {
		total_page = content.length;
		templateCaller();
	}, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";


	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring");

			texthighlightstarttag = "<span>";


			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}
/*=====  End of data highlight function  ======*/

//page 1
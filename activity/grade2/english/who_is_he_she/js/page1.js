var imgpath = $ref + '/images/';
var audioPath = $ref + '/audio/slide1/'
var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

var content;
content = [{
    // starting page
    centertextblock: [{
        textclass: 'chapter-title',
        textdata: data.string.chapter_title
    }]
	// slide 1
}, {
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        characters: [{
			imgclass: 'radha-pic',
			imgsrc: imgpath + 'radha.png'
		}, {
			imgclass: 'neeraj-pic',
			imgsrc: imgpath + 'neeraj.png'
		},  {
			imgclass: 'mother-pic',
			imgsrc: imgpath + 'mother.png'
		}]
    }]
}, {
	// slide 2
	contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: false,
        	content: data.string.p1_text1
        }],
        characters: [{
			imgclass: 'radha-pic',
			imgsrc: imgpath + 'radha.png'
		}, {
			imgclass: 'neeraj-pic',
			imgsrc: imgpath + 'neeraj.png'
		},  {
			imgclass: 'mother-pic',
			imgsrc: imgpath + 'mother.png'
		}]
    }]
}, {
	// slide 3
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: true,
        	content: data.string.p1_text2
        }],
        characters: [{
			imgclass: 'radha-pic',
			imgsrc: imgpath + 'radha.png'
		}, {
			imgclass: 'neeraj-pic',
			imgsrc: imgpath + 'neeraj.png'
		},  {
			imgclass: 'mother-pic',
			imgsrc: imgpath + 'mother.png'
		}]
    }]
}, {
	// slide 4
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: false,
        	content: data.string.p1_text3
        }],
        characters: [{
			imgclass: 'radha-pic',
			imgsrc: imgpath + 'radha.png'
		}, {
			imgclass: 'neeraj-pic',
			imgsrc: imgpath + 'neeraj.png'
		},  {
			imgclass: 'mother-pic',
			imgsrc: imgpath + 'mother.png'
		}]
    }]
}, {
	// slide 5
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: true,
        	content: data.string.p1_text4
        }],
        characters: [{
			imgclass: 'radha-pic',
			imgsrc: imgpath + 'radha.png'
		}, {
			imgclass: 'neeraj-pic',
			imgsrc: imgpath + 'neeraj.png'
		},  {
			imgclass: 'mother-pic',
			imgsrc: imgpath + 'mother.png'
		}]
    }]
}, {
	// slide 6
    contentblockadditionalclass: 'bg-01',
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: false,
            content: data.string.p1_text5
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }]
}, {
	// slide 7
    contentblockadditionalclass: 'bg-01',
    imageblock: [{
        imagestoshow: [{
            imgclass: 'nurse-pic',
            imgsrc: imgpath + 'nurse_bg02.png'
        }]
    }],
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: true,
            content: data.string.p1_text6
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }]
}, {
	// slide 8
    contentblockadditionalclass: 'bg-01',
    imageblock: [{
        imagestoshow: [{
            imgclass: 'nurse-pic',
            imgsrc: imgpath + 'nurse_bg02.png'
        }]
    }],
    conversationblock: [{
        speechbubbles: [{
            speechbubblesreversed: true,
            content: data.string.p1_text7
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }]
}, {
    //slide 9
    conversationblock: [{
        summaryconversation: true,
        speechbubbles: [{
            speechbubblesreversed: false,
            content: data.string.p1_text1
        }, {
            speechbubblesreversed: true,
            content: data.string.p1_text2
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }, {
        summaryconversation: true,
        speechbubbles: [{
            speechbubblesreversed: false,
            content: data.string.p1_text3
        }, {
            speechbubblesreversed: true,
            content: data.string.p1_text4
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }, {
        summaryconversation: true,
        speechbubbles: [{
            speechbubblesreversed: false,
            content: data.string.p1_text5
        }, {
            speechbubblesreversed: true,
            content: data.string.p1_text6
        }],
        characters: [{
            imgclass: 'radha-pic',
            imgsrc: imgpath + 'radha.png'
        }, {
            imgclass: 'neeraj-pic',
            imgsrc: imgpath + 'neeraj.png'
        },  {
            imgclass: 'mother-pic',
            imgsrc: imgpath + 'mother.png'
        }]
    }],
    imageblock: [{
        imagestoshow: [{
            imgclass: 'nurse-summary',
            imgsrc: imgpath + 'nurse_bg02.png'
        }]
    }],
}];


// }

$(function() {
    // var height = $(window).height();
    // var width = $(window).width();
    // $("#board").css({"width": width, "height": (height*580/960)});
    // function recursion(){
    // if(data.string != null){
    // } else{
    // recursion();
    // }
    // }
    // recursion();
    // objectifyActivityData("data.xml");

    $(window).resize(function() {
        recalculateHeightWidth();
    });

    function recalculateHeightWidth() {
        var heightresized = $(window).height();
        var widthresized = $(window).width();
        var factor = 960 / 580;
        var equivalentwidthtoheight = widthresized / factor;

        if (heightresized >= equivalentwidthtoheight) {
            $(".shapes_activity").css({
                "width": widthresized,
                "height": equivalentwidthtoheight
            });
        } else {
            $(".shapes_activity").css({
                "height": heightresized,
                "width": heightresized * 960 / 580
            });
        }
        // $(".shapes_activity").css({"left": "50%" ,
        // "height": "50%" ,
        // "-webkit-transform" : "translate(-50%, - 50%)",
        // "transform" : "translate(-50%, - 50%)"});
    }
    var $board = $(".board");
    
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    recalculateHeightWidth();


    /*
    	inorder to use the handlebar partials we need to register them
    	to their respective handlebar partial pointer first
    */
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


    //controls the navigational state of the program
    function navigationController() {
        if (countNext == 0 && total_page != 1) {
            $nextBtn.show(0);
        } else if (countNext > 0 && countNext < (total_page - 1)) {
            $nextBtn.show(0);
            $prevBtn.show(0);
        } else if (countNext == total_page - 1) {
            $prevBtn.show(0);
        }
    }
    
    var sound1 = new buzz.sound(audioPath + 'hello_radha.mp3');
    var sound2 = new buzz.sound(audioPath + 'hello_neeraj.mp3');
    var sound3 = new buzz.sound(audioPath + 'who_is_she.mp3');
    var sound4 = new buzz.sound(audioPath + 'she_is_my_mother.mp3');
    var sound5 = new buzz.sound(audioPath + 'what_does_your_mother_do.mp3');
    var sound6 = new buzz.sound(audioPath + 'she_is_a_nurse.mp3');
    var sound7 = new buzz.sound(audioPath + 'she_is_a_nurse.mp3');

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);
        
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        
        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        switch (countNext) {
            case 0:
            case 1:
                $nextBtn.show(0);
                break;
            case 2:
                sound1.bindOnce('ended', handleSoundEnd);
                sound1.play();
                break;
            case 3:
                sound2.bindOnce('ended', handleSoundEnd);
                sound2.play();
                break;
            case 4:
                sound3.bindOnce('ended', handleSoundEnd);
                sound3.play();
                break;
            case 5:
                sound4.bindOnce('ended', handleSoundEnd);
                sound4.play();
                break;
            case 6:
                sound5.bindOnce('ended', handleSoundEnd);
                sound5.play();
                break;
            case 7:
                sound6.bindOnce('ended', handleSoundEnd);
                sound6.play();
                break;
            case 8:
                sound6.bindOnce('ended', handleSoundEnd);
                sound6.play();
                break;
            case 9:
                var speechbubbles = $('.speechbubble');
                speechbubbles.addClass('speechbubble-filter');
                speechbubbles.css({'cursor': 'pointer'});
                
                var playing = false;
                var sounds = [sound1, sound2, sound3, sound4, sound5, sound6];
                
                speechbubbles.each(function(index) {
                    var $this = $(this);
                    $this.on('click', function() {
                        if (!playing) {
                            playing = true; 
                            sounds[index].bindOnce('ended', function() {
                                playing = false;
                                $this.addClass('speechbubble-filter');
                            });
                            sounds[index].play();
                        }
                    });
                    $this.on('mouseenter', handleMouseEnter);
                    $this.on('mouseleave', handleMouseLeave);
                });
                
                function handleMouseEnter() {
                    if (!playing) {
                        $(this).removeClass('speechbubble-filter');
                    }
                }
                
                function handleMouseLeave() {
                    if (!playing) {
                        $(this).addClass('speechbubble-filter');
                    }
                }
                
        }
        
        function handleSoundEnd() {
            $nextBtn.show(0);
            $prevBtn.show(0);
        }


    }
    // Handlebars.registerHelper('listItem', function (from, to, context, options){
    // var item = "";
    // for (var i = from, j = to; i <= j; i++) {
    // item = item + options.fn(context[i]);
    // }
    // return item;
    // });

    function templateCaller() {
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        navigationController();

        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    }

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
        countNext--;
        templateCaller();
    });
    // setTimeout(function(){
    total_page = content.length;
    templateCaller();
    // }, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";


    if ($alltextpara.length > 0) {
        $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass")) :
                (stylerulename = "parsedstring");

            texthighlightstarttag = "<span>";


            replaceinstring = $(this).html();
            replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
            replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


            $(this).html(replaceinstring);
        });
    }
}
/*=====  End of data highlight function  ======*/

//page 1

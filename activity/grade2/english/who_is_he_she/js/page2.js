var imgpath = $ref + "/images/fvf/";
var imgpath2 = $ref + "/images/clothes/";
var content;
	content = [{
		//1st
		hasheaderblock: false,
		addclass: "textonly",
		uppertextblock: [{
			texttoshow: [{
				textclass: "hightext animated  fadeInUp",
				textdata: data.string.p5text1
			},
			 {
				textclass: "hightext2 animated fadeIn",
				textdata: data.string.p5text2
			}]
		}]
	}, 
	//2nd
	{
		hasheaderblock: false,
		addclass: "head2 animated zoomIn",
		uppertextblock: [{
			texttoshow: [{

				textdata: data.string.p19text2
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "img1 animated fadeIn ",
				imgsrc: imgpath + "apple.jpg"
			}, {
				imgclass: "img2 animated fadeIn",
				imgsrc: imgpath + "orange.jpg"
			}, {
				imgclass: "img3 animated fadeIn",
				imgsrc: imgpath + "banana.jpg"
			}, {
				imgclass: "img4 animated fadeIn",
				imgsrc: imgpath + "grapes.jpg"
			}]
		}, {
			imagelabels: [{
				imagelabelclass: "img1lbl animated fadeInup",
				imagelabeldata: data.string.p6text1
			}, {
				imagelabelclass: "img2lbl animated fadeInup",
				imagelabeldata: data.string.p6text2
			}, {
				imagelabelclass: "img3lbl animated fadeInup",
				imagelabeldata: data.string.p6text3
			}, {
				imagelabelclass: "img4lbl animated fadeInup",
				imagelabeldata: data.string.p6text4
			}]
		}]
	},

	//3rd
	{

		hasheaderblock: true,
		addclass: "head2 animated zoomIn",
		uppertextblock: [{
			texttoshow: [{

				textdata: data.string.p7text0
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "img1 animated fadeIn",
				imgsrc: imgpath +"radish.jpg"
			}, {
				imgclass: "img2 animated fadeIn",
				imgsrc: imgpath + "carrot.jpg"
			}, {
				imgclass: "img3 animated fadeIn",
				imgsrc: imgpath + "onion01.jpg"
			}, {
				imgclass: "img4 animated fadeIn",
				imgsrc: imgpath + "turnip.jpg"
			}]
		}, {
			imagelabels: [{
				imagelabelclass: "img1lbl animated fadeInUp",
				imagelabeldata: data.string.p7text1
			}, {
				imagelabelclass: "img2lbl animated fadeInUp",
				imagelabeldata: data.string.p7text2
			}, {
				imagelabelclass: "img3lbl animated fadeInUp",
				imagelabeldata: data.string.p7text3
			}, {
				imagelabelclass: "img4lbl animated fadeInUp",
				imagelabeldata: data.string.p7text4
			}]
		}]
	},
	//4th
	{
		hasheaderblock: true,
		addclass: "head2 animated zoomIn",
		uppertextblock: [{
			texttoshow: [{

				textdata: data.string.p8text0
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "img1 animated fadeIn",
				imgsrc: imgpath +"onion.jpg"
			}, {
				imgclass: "img2 animated fadeIn",
				imgsrc: imgpath + "asparagus.jpg"
			}, {
				imgclass: "img3 animated fadeIn",
				imgsrc: imgpath + "bambooshoots.jpg"
			}, {
				imgclass: "img4 animated fadeIn",
				imgsrc: imgpath + "sugarcane.jpg"
			}]
		}, {
			imagelabels: [{
				imagelabelclass: "img1lbl animated fadeInUp",
				imagelabeldata: data.string.p8text1
			}, {
				imagelabelclass: "img2lbl animated fadeInUp",
				imagelabeldata: data.string.p8text2
			}, {
				imagelabelclass: "img3lbl shoots animated fadeInUp",
				imagelabeldata: data.string.p8text3
			}, {
				imagelabelclass: "img4lbl  animated fadeInUp",
				imagelabeldata: data.string.p8text4
			}]
		}]
	},
	//5th
	{
		hasheaderblock: true,
		addclass: "head2 animated zoomIn",
		uppertextblock: [{
			texttoshow: [{

				textdata: data.string.p9text0
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "img1 animated fadeIn",
				imgsrc: imgpath +"palak.jpg"
			}, {
				imgclass: "img2 animated fadeIn",
				imgsrc: imgpath + "cabbage.jpg"
			}, {
				imgclass: "img3 animated fadeIn",
				imgsrc: imgpath + "nettle.jpg"
			}, {
				imgclass: "img4 animated fadeIn",
				imgsrc: imgpath + "mint.jpg"
			}]
		}, {
			imagelabels: [{
				imagelabelclass: "img1lbl animated fadeInUp",
				imagelabeldata: data.string.p9text1
			}, {
				imagelabelclass: "img2lbl animated fadeInUp",
				imagelabeldata: data.string.p9text2
			}, {
				imagelabelclass: "img3lbl  animated fadeInUp",
				imagelabeldata: data.string.p9text3
			}, {
				imagelabelclass: "img4lbl  animated fadeInUp",
				imagelabeldata: data.string.p9text4
			}]
		}]
	},
	//6th
	{
		hasheaderblock: true,
		addclass: "head2 animated zoomIn",
		uppertextblock: [{
			texttoshow: [{

				textdata: data.string.p10text0
			}]
		}],
		imageblock: [{
			imagetoshow: [{
				imgclass: "img1 animated fadeIn",
				imgsrc: imgpath +"rice.jpg"
			}, {
				imgclass: "img2 animated fadeIn",
				imgsrc: imgpath + "maize.jpg"
			}, {
				imgclass: "img3 animated fadeIn",
				imgsrc: imgpath + "millet.jpg"
			}, {
				imgclass: "img4 animated fadeIn",
				imgsrc: imgpath + "buckwheat.jpg"
			}]
		}, {
			imagelabels: [{
				imagelabelclass: "img1lbl animated fadeInUp",
				imagelabeldata: data.string.p10text1
			}, {
				imagelabelclass: "img2lbl animated fadeInUp",
				imagelabeldata: data.string.p10text2
			}, {
				imagelabelclass: "img3lbl  animated fadeInUp",
				imagelabeldata: data.string.p10text3
			}, {
				imagelabelclass: "img4lbl  animated fadeInUp",
				imagelabeldata: data.string.p10text4
			}]
		}]
	
	},
	//7th 
	{
		hasheaderblock:true,
		addclass: "midtext",
		uppertextblock: [{
		texttoshow: [{
			textdata: data.string.p11text1
			}]
		}],
		imageblock:[{
			imagetoshow:[{
				imgclass:"img1a",
				imgsrc:imgpath + "apple.jpg"
			},{
				imgclass:"img2a",
				imgsrc:imgpath +"grapes.jpg"
			},
			{
				imgclass:"img3a",
				imgsrc:imgpath +"carrot.jpg"
			},
			{
				imgclass:"img4a",
				imgsrc:imgpath +"asparagus.jpg"
			},
			{
				imgclass:"img5a",
				imgsrc:imgpath +"maize.jpg"
			},
			{
				imgclass:"img6a",
				imgsrc: imgpath + "cabbage.jpg"
			},
			{
				imgclass:"img7a",
				imgsrc: imgpath + "pea.jpg"
			},
			{
				imgclass:"img8a",
				imgsrc: imgpath + "onion.jpg"
			},
			{
				imgclass:"img9a",
				imgsrc: imgpath + "radish.jpg"
			},
			{
				imgclass:"img10a",
				imgsrc: imgpath + "garlic.jpg"
			}

			]
		}]
	
	}
	];


$(function() {
	// var height = $(window).height();
	// var width = $(window).width();
	// $("#board").css({"width": width, "height": (height*580/960)});
	// function recursion(){
	// if(data.string != null){
	// } else{
	// recursion();
	// }   	
	// }
	// recursion();

	$(window).resize(function() {
		recalculateHeightWidth();
	});

	function recalculateHeightWidth() {
		var heightresized = $(window).height();
		var widthresized = $(window).width();
		var factor = 960 / 580;
		var equivalentwidthtoheight = widthresized / factor;

		if (heightresized >= equivalentwidthtoheight) {
			$(".shapes_activity").css({
				"width": widthresized,
				"height": equivalentwidthtoheight
			});
		} else {
			$(".shapes_activity").css({
				"height": heightresized,
				"width": heightresized * 960 / 580
			});
		}
		// $(".shapes_activity").css({"left": "50%" , 
		// "height": "50%" ,
		// "-webkit-transform" : "translate(-50%, - 50%)",
		// "transform" : "translate(-50%, - 50%)"});
	}
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	recalculateHeightWidth();


	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	function navigationController() {
		if (countNext == 0 && total_page != 1) {
			$nextBtn.show(0);
		} else if (countNext > 0 && countNext < (total_page - 1)) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == total_page - 1) {
			$prevBtn.show(0);
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				$nextBtn.hide(0)
				$nextBtn.show(4000)
			break;
			case 1:
			break;
			case 2:
			$(".img1").css({
				"left":"11%"
			})
			break;

			case 3:
				$(".img2lbl").css({
					"left":"35%"
				})
			break;
			case 6:
				$(".img1a").delay(1000).fadeIn(200)
				$(".img2a").delay(2000).fadeIn(200)
				$(".img3a").delay(3000).fadeIn(200)
				$(".img4a").delay(3800).fadeIn(200)
				$(".img5a").delay(4300).fadeIn(200)
				$(".img6a").delay(4800).fadeIn(200)
				$(".img7a").delay(5600).fadeIn(200)
				$(".img8a").delay(6400).fadeIn(200)
				$(".img9a").delay(7200).fadeIn(200)
				$(".img10a").delay(8000).fadeIn(200)
						
				$(".midtext").delay(8000).toggleClass("animated fadeIn").css({
					"font-size":"1.7em"
				}).fadeIn(function(){
					if (countNext>= 5) {
								ole.footerNotificationHandler.pageEndSetNotification();
					}
					
				})
			
				break;
				
		}
		function onDragElev(left) {

			if (left > -300) {
				left = 22;
			}
			return left;
		}
	}
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page,countNext+1);

		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	setTimeout(function() {
		total_page = content.length;
		templateCaller();
	}, 250);


});



/*===============================================
	 =            data highlight function            =
	 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";


	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring");

			texthighlightstarttag = "<span>";


			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}
/*=====  End of data highlight function  ======*/

//page 1
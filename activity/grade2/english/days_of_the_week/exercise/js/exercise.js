var imgpath = $ref+"/images/forexe/";
var soundAsset = $ref+"/sounds/";
var hov_sound = new buzz.sound(("sounds/common/woodhoversound.wav"));
var cor_sound = new buzz.sound(("sounds/common/grade1/correct.ogg"));
var wrn_sound = new buzz.sound(("sounds/common/grade1/incorrect.ogg"));
var s0 = new buzz.sound((soundAsset + "exinstr.ogg"));
var s1 = new buzz.sound((soundAsset + "tada.ogg"));

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
		uppertextblock:[
		{
			textclass: "instext",
			textdata: data.string.exeins
		},
    {
			textclass: "daystext",
			textdata: "days"
		},
		{
			textclass: "baartext nepword",
			textdata: "बार"
		}
		],
		levelbar:[
		{
			levellistclass: "levelselect",
			leveltext: "level 1"
		},
		{
			leveltext: "level 2"
		},
		{
			leveltext: "level 3"
		}
		],

		dayscontainer:[
		{
			datscontaineradditionalclass: "englishdays",
			dayslist:[
			{
				dayname: "Sunday"
			},
			{
				extraextra: "drophere",
				dayname: "Monday"
			},
			{
				dayname: "Tuesday"
			},
			{
				dayname: "Wednesday"
			},
			{
				dayname: "Thursday"
			},
			{
				extraextra: "drophere",
				dayname: "Friday"
			},
			{
				dayname: "Saturday"
			},
			]

		},
		{
			datscontaineradditionalclass: "nepalidays",
		dayslist:[
			{
				dayname: "आइतबार"
			},
			{
				dayname: "सोमबार"
			},
			{
				dayname: "मंगलबार"
			},
			{
				extraextra: "drophere",
				dayname: "बुधबार"
			},
			{
				dayname: "बिहिबार"
			},
			{
				dayname: "शुक्रबार"
			},
			{
				extraextra: "drophere",
				dayname: "शनिबार"
			},
			]
		},

		],
		letsdrag:[
		{
      dragaddclass: "nepword",
			dayname: "शनिबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "बुधबार"
		},
		{
			dayname: "Friday"
		},
		{
			dayname: "Monday"
		},
		]
	},
	{
		//slide 1
    uppertextblock:[
		{
			textclass: "instext",
			textdata: data.string.exeins
		},
    {
			textclass: "daystext",
			textdata: "days"
		},
		{
      textclass: "baartext nepword",
			textdata: "बार"
		}
		],
		levelbar:[
		{
			levellistclass: "leveldone",
			leveltext: "level 1"
		},
		{
			levellistclass: "levelselect",
			leveltext: "level 2"
		},
		{
			leveltext: "level 3"
		}
		],

		dayscontainer:[
		{
			datscontaineradditionalclass: "englishdays",
			dayslist:[
			{
				extraextra: "drophere",
				dayname: "Sunday"
			},
			{
				dayname: "Monday"
			},
			{
				extraextra: "drophere",
				dayname: "Tuesday"
			},
			{
				dayname: "Wednesday"
			},
			{
				dayname: "Thursday"
			},
			{
				extraextra: "drophere",
				dayname: "Friday"
			},
			{
				extraextra: "drophere",
				dayname: "Saturday"
			},
			]

		},
		{
			datscontaineradditionalclass: "nepalidays",
		dayslist:[
			{
				dayname: "आइतबार"
			},
			{
				extraextra: "drophere",
				dayname: "सोमबार"
			},
			{
				dayname: "मंगलबार"
			},
			{
				extraextra: "drophere",
				dayname: "बुधबार"
			},
			{
				dayname: "बिहिबार"
			},
			{
				extraextra: "drophere",
				dayname: "शुक्रबार"
			},
			{
				extraextra: "drophere",
				dayname: "शनिबार"
			},
			]
		},

		],
		letsdrag:[
		{
      dragaddclass: "nepword",
			dayname: "शनिबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "बुधबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "सोमबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "शुक्रबार"
		},
		{
			dayname: "Friday"
		},
		{
			dayname: "Tuesday"
		},
		{
			dayname: "Sunday"
		},
		{
			dayname: "Saturday"
		},
		]
	},
	{
		//slide 2
    uppertextblock:[
		{
			textclass: "instext",
			textdata: data.string.exeins
		},
    {
			textclass: "daystext",
			textdata: "days"
		},
		{
      textclass: "baartext nepword",
			textdata: "बार"
		}
		],
    	exercisetxt: data.string.exetxt,
		levelbar:[
		{
			levellistclass: "leveldone",
			leveltext: "level 1"
		},
		{
			levellistclass: "leveldone",
			leveltext: "level 2"
		},
		{
			levellistclass: "levelselect",
			leveltext: "level 3"
		}
		],

		dayscontainer:[
		{
			datscontaineradditionalclass: "englishdays",
			dayslist:[
			{
				extraextra: "drophere",
				dayname: "Sunday"
			},
			{
				extraextra: "drophere",
				dayname: "Monday"
			},
			{
				extraextra: "drophere",
				dayname: "Tuesday"
			},
			{
				extraextra: "drophere",
				dayname: "Wednesday"
			},
			{
				extraextra: "drophere",
				dayname: "Thursday"
			},
			{
				extraextra: "drophere",
				dayname: "Friday"
			},
			{
				extraextra: "drophere",
				dayname: "Saturday"
			},
			]

		},
		{
			datscontaineradditionalclass: "nepalidays",
		dayslist:[
			{
				extraextra: "drophere",
				dayname: "आइतबार"
			},
			{
				extraextra: "drophere",
				dayname: "सोमबार"
			},
			{
				extraextra: "drophere",
				dayname: "मंगलबार"
			},
			{
				extraextra: "drophere",
				dayname: "बुधबार"
			},
			{
				extraextra: "drophere",
				dayname: "बिहिबार"
			},
			{
				extraextra: "drophere",
				dayname: "शुक्रबार"
			},
			{
				extraextra: "drophere",
				dayname: "शनिबार"
			},
			]
		},

		],
		letsdrag:[
		{
      dragaddclass: "nepword",
			dayname: "आइतबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "सोमबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "मंगलबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "बुधबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "बिहिबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "शुक्रबार"
		},
		{
      dragaddclass: "nepword",
			dayname: "शनिबार"
		},
		{
			dayname: "Sunday"
		},
		{
			dayname: "Monday"
		},
		{
			dayname: "Tuesday"
		},
		{
			dayname: "Wednesday"
		},
		{
			dayname: "Thursday"
		},
		{
			dayname: "Friday"
		},
		{
			dayname: "Saturday"
		},
		]
	}
];

$(function (){
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;
	var finCount = 0;
	var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			if(countNext == 2){
				$prevBtn.hide(0);
				$('.exenextbtn').show(0);
			}

		}
	 }

	 /*values in this array is same as the name of images of eggs in image folder*/
   var exTemp = new NumberTemplate();
   exTemp.init(26);
   var wrongClick = false;
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;
	 	var endValue;

       	$('.exenextbtn').hide(0);
	 	switch(countNext){
	 		case 0:
                s0.play();
                finCount = 0;
				endValue = 4;
	 		break;
	 		case 1:
				finCount = 0;
				endValue = 8;
	 		break;
	 		case 2:
				finCount = 0;
				endValue = 14;
	 		break;
	 	}

	 	$('.exenextbtn').click(function(){
          $("#titleToolbar").trigger("click");
        });
         $('.nxtbtntext').click(function(){
            // exTemp.init(26);
             go_to_menu_page(true)
         });
	 	/*for randomizing the options*/
	 	var parent = $(".letsdrag");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}

	 	$(".dragme").draggable({
	 		containment : ".generalTemplateblock",
      revert : true,
	 		cursor : "move",
	 		zIndex: 100000,
	 	});
	 	$(".drophere").droppable({
      hoverClass: "hovered",
	 		// accept: function(d){
	 		// 	var dragText = d.text();
	 		// 	var dropText = $(this).text();
	 		// 	if(dragText == dropText)
	 		// 		 return true;
	 		// 	},
	 		drop: function (event, ui){
	 			$this = $(this);
	 			dropfunc(event, ui, $this);
	 		}
	 	});

	 	function dropfunc(event, ui, $droppedOn){
	 		var dragText = ui.draggable.text();
	 		var dropText = $droppedOn.text();
	 		console.log("drag:"+ dragText);
	 		console.log("drop:"+ dropText);
	 		if(dragText == dropText){
        cor_sound.play();
        !wrongClick?exTemp.update(true):exTemp.update(false);
        wrongClick = false;
        ui.draggable.draggable('option', 'revert', false);
		 		ui.draggable.draggable('disable');
		 		ui.draggable.hide(0);
		 		/*var droppedOn = $(this);*/
		 		$droppedOn.html(dragText);
		 		$droppedOn.css('color','#333');
		 		finCount++;
				if(finCount == endValue)
					activityFinish();
	 		}
      else{
        wrn_sound.play();
        exTemp.update(false);
        wrongClick = true;
      }
	 	}

	 	var animCount = 1;
	 	function activityFinish(){
	 		setTimeout(function(){
	 			$(".englishdays >ol > li:nth-of-type("+ animCount +") > .engdays, .nepalidays > ol > li:nth-of-type("+ animCount +") > .engdays").css({
	 				'background':'#6AB187',
	 				'border':'3px solid #F9898F'
	 			});
        hov_sound.play();
                setTimeout(function(){
                   s1.play();
                },3000)
	 			animCount++;
	 			if(animCount == 8)
	 				navigationcontroller();
	 			else
	 				activityFinish();
	 		}, 500);

	 	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];

	 	/*======= SCOREBOARD SECTION ==============*/

	 }

	  function generateRandomNumb(min, max) {
			 	return Math.floor(Math.random() * (max - min) + min);
		}

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		//navigationcontroller();

		// call the template
		generalTemplate();
         loadTimelineProgress($total_page, countNext + 1);

         /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var s0 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var s1 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var s2 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var s3 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var s4 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var s5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var s6 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var content=[
{
	contentblockadditionalclass: "ole-backrgound-gradient-pool",
	flexblock: [
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sun
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.mon
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.tue
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.wed
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.thu
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.fri
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sat
	}
	],
	uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
	uppertextblock:[{
		textdata: data.string.p2text2
	}],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "beeimg",
			imgsrc: imgpath + "flying-fly-gif.gif"
		}
		]
	}],
	lowertextblock:[{
		textclass: "todaytxt",
		textdata: data.string.tod
	}]
},
{
	contentblockadditionalclass: "ole-backrgound-gradient-pool",
	flexblock: [
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sun
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.mon
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.tue
	},
	{
		flextextstyle: "flexitem today",
		flexdata: data.string.wed
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.thu
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.fri
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sat
	}
	],
	uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
	uppertextblock:[{
		textdata: data.string.p2text3
	}],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "beeimg2",
			imgsrc: imgpath + "flying-fly-gif.gif"
		}
		]
	}],
	lowertextblock:[{
		textclass: "todaytxt",
		textdata: data.string.tod
	},
	{
		textclass: "yesttxt",
		textdata: data.string.yes
	}]
},
{
	contentblockadditionalclass: "ole-backrgound-gradient-pool",
	flexblock: [
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sun
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.mon
	},
	{
		flextextstyle: "flexitem yest",
		flexdata: data.string.tue
	},
	{
		flextextstyle: "flexitem today",
		flexdata: data.string.wed
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.thu
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.fri
	},
	{
		flextextstyle: "flexitem",
		flexdata: data.string.sat
	}
	],
	uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
	uppertextblock:[{
		textdata: data.string.p2text4
	}],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "beeimg3",
			imgsrc: imgpath + "flying-fly-gif.gif"
		}
		]
	}],
	lowertextblock:[{
		textclass: "todaytxt",
		textdata: data.string.tod
	},
	{
		textclass: "yesttxt",
		textdata: data.string.yes
	},
	{
		textclass: "tommtxt",
		textdata: data.string.tom
	}]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1 forhover",
			imgid: "Sunday",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2 forhover",
			imgid: "Monday",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3 forhover",
			imgid: "Tuesday",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4 forhover",
			imgid: "Wednesday",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5 forhover",
			imgid: "Thursday",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6 forhover",
			imgid: "Friday",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7 forhover",
			imgid: "Saturday",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p2text5
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgid: "Sunday",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgid: "Monday",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgid: "Tuesday",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgid: "Wednesday",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgid: "Thursday",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgid: "Friday",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgid: "Saturday",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p2text6
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgid: "Sunday",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgid: "Monday",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgid: "Tuesday",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgid: "Wednesday",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgid: "Thursday",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgid: "Friday",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgid: "Saturday",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p2text7
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgid: "Sunday",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgid: "Monday",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgid: "Tuesday",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgid: "Wednesday",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgid: "Thursday",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgid: "Friday",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgid: "Saturday",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p2text8
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
}
];

$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	var today;
	var yesterday;
	var tomorrow;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

		Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
		Handlebars.registerPartial("flexcontent", $("#flexcontent-partial").html());
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var moveOn = 0;

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        vocabcontroller.findwords(countNext);

        switch(countNext){
			case 0:
			playaudio(s0);
			$(".todaytxt").hide(0);
			setTimeout(function() {
				$(".flexitem:nth-of-type(4)").addClass("today");
				$(".todaytxt").fadeIn(1500);
			}, 2000);
			break;
			case 1:
			playaudio(s1);
			$(".yesttxt").hide(0);
			setTimeout(function() {
				$(".flexitem:nth-of-type(3)").addClass("yest");
				$(".yesttxt").fadeIn(1500);
			}, 2000);
			break;
			case 2:
			playaudio(s2);
			$(".tommtxt").hide(0);
			setTimeout(function() {
				$(".flexitem:nth-of-type(5)").addClass("tomm");
				$(".tommtxt").fadeIn(1500);
			}, 2000);
			break;
			case 3:
			playaudio(s3, "false");
				$(".forhover").mouseenter(function(){
					console.log($(this).attr("id"));
					var audio = new buzz.sound((soundAsset + $(this).attr("id").toLowerCase()+".ogg"));
			 			if($(this).hasClass("forhover")){
							buzz.all().stop();
			 				audio.play();
			 			}
			 	});

			 	$(".forhover").click(function(){
						if($(this).hasClass("forhover")){
				 		today = $(this).attr("id");
				 		$(this).addClass("selectme");
				 		$(".forhover").removeClass("forhover");
				 		navigationcontroller();
					}
			 	});
			break;
			case 4:
			daysaudio(s4, today);
				var toNum;
				$(".highli").text(today);
				$("#"+today).addClass("selectme");

				toNum = $("#"+today).attr('class').split(' ')[0].split('-')[1];
				if(toNum == 1)
					toNum = 8;
				yesterday = $(".day-"+(toNum-1)).attr("id");

				toNum = $("#"+today).attr('class').split(' ')[0].split('-')[1];
				if(toNum == 7)
					toNum = 0;
				tomorrow = $(".day-"+(parseInt(toNum)+1)).attr("id");
			break;
			case 5:
			daysaudio(s5, yesterday);
				$("#"+yesterday).addClass("selectme");
				$(".highli").text(yesterday);
			break;
			case 6:
			daysaudio(s6, tomorrow);
				$("#"+tomorrow).addClass("selectme");
				$(".highli").text(tomorrow);
			break;
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
				moveOn = 0;
				$(".draggablecontainer").draggable({
					containment : ".generalTemplateblock",
					revert : true,
					cursor : "move",
					zIndex: 100000,
				});

				$(".droppable-1").droppable({
					accept: ".draggablecontainer",
					hoverClass : 'hover-active',
					drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				function dropfunc(event, ui, $droppedOn){
					if(ui.draggable.hasClass("yest-corr")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						ui.draggable.css({
						"position" : "absolute",
						"left": "14%",
						"top": "52.5%",
						"width": "13.5%"
					}).appendTo(".coverboardfull");
					moveOn++;
					checkNav();
					}
				}

				$(".droppable-3").droppable({
					accept: ".draggablecontainer",
					hoverClass : 'hover-active',
					drop: function (event, ui){
					$this = $(this);
					dropfunc2(event, ui, $this);
				}
				});

				function dropfunc2(event, ui, $droppedOn){
					if(ui.draggable.hasClass("tomm-corr")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						ui.draggable.css({
						"position" : "absolute",
						"left": "74%",
						"top": "52.5%",
						"width": "13.5%"
					}).appendTo(".coverboardfull");
					moveOn++;
					checkNav();
					}
				}

				function checkNav(){
					if(moveOn == 2)
						navigationcontroller();
				}
			break;

		}
		function daysaudio(sound1, sound2){
			sound1.play();
			sound1.bind('ended', function(){
				var audio = new buzz.sound((soundAsset + sound2.toLowerCase()+".ogg"));
						audio.play();
						audio.bind('ended', function(){
							navigationcontroller();
						});
			});


		}

		function playaudio(sound_data, next){
				var playing = true;
					if(!playing){
						playaudio(sound_data);
					}

				sound_data.play();
				sound_data.bind('ended', function(){
					setTimeout(function(){
						playing = false;
						sound_data.unbind('ended');
						if(next == null)
						navigationcontroller();
					}, 1000);
				});
			}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

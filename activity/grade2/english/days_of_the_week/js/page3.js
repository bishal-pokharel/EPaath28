var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var s0 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var s1 = new buzz.sound((soundAsset + "p3_s1.ogg"));

var content=[
{
	contentblockadditionalclass: "contentwithbg",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.string.diy
	}
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "catter",
			imgsrc: imgpath + "caterpillar.png"
		}
		]
	}],
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.tue
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.fri
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.mon
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.sun
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.thu
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
},
{
	contentblockadditionalclass: "diybg",
	draggableblock: [
	{
		dcextra: "tomm-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sun
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.mon
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.tue
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.wed
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.thu
	},
	{
		dcextra: "yest-corr",
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.fri
	},
	{
		draggabletextstyle: "draggableitem",
		draggabledata: data.string.sat
	},
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "droppable-1",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-2",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "droppable-3",
			imgsrc: imgpath + "cart.png"
		},
		{
			imgclass: "ansbox-today",
			imgsrc: imgpath + "giftbox.png"
		}
		],
		imagelabels:[
			{
				imagelabelclass: "labeldr-1",
				imagelabeldata: data.string.yes
			},
			{
				imagelabelclass: "labeldr-2",
				imagelabeldata: data.string.tod
			},
			{
				imagelabelclass: "labeldr-3",
				imagelabeldata: data.string.tom
			},
			{
				imagelabelclass: "anstext-today",
				imagelabeldata: data.string.sat
			}
		]
	}],
	uppertextblock:[
	{
		textclass: "inst-text",
		textdata: data.string.p2text9
	}
	]
}
];

$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	var today;
	var yesterday;
	var tomorrow;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

		Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
		Handlebars.registerPartial("flexcontent", $("#flexcontent-partial").html());
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var moveOn = 0;

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        vocabcontroller.findwords(countNext);
        switch(countNext){
			case 0:
		 		$nextBtn.hide(0);
			playaudio(s0);
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			/*for randomizing the options*/
			playaudio(s1,1);
				var parent = $(".draggableblock");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}

				moveOn = 0;
				$(".draggablecontainer").draggable({
					containment : ".generalTemplateblock",
					revert : true,
					cursor : "move",
					zIndex: 100000,
				});

				$(".droppable-1").droppable({
					accept: ".draggablecontainer",
					hoverClass : 'hover-active',
					drop: function (event, ui){
					$this = $(this);
					dropfunc(event, ui, $this);
				}
				});

				function dropfunc(event, ui, $droppedOn){
					if(ui.draggable.hasClass("yest-corr")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						ui.draggable.css({
						"position" : "absolute",
						"left": "14%",
						"top": "52.5%",
						"width": "13.5%"
					}).appendTo(".coverboardfull");
					moveOn++;
					play_correct_incorrect_sound(1);
					checkNav();
				}else{
					play_correct_incorrect_sound(0);
				}
				}

				$(".droppable-3").droppable({
					accept: ".draggablecontainer",
					hoverClass : 'hover-active',
					drop: function (event, ui){
					$this = $(this);
					dropfunc2(event, ui, $this);
				}
				});

				function dropfunc2(event, ui, $droppedOn){
					if(ui.draggable.hasClass("tomm-corr")){
						ui.draggable.draggable('option', 'revert', false);
						ui.draggable.draggable('disable');
						ui.draggable.css({
						"position" : "absolute",
						"left": "74%",
						"top": "52.5%",
						"width": "13.5%"
					}).appendTo(".coverboardfull");
					moveOn++;
					checkNav();
					play_correct_incorrect_sound(1);
					}else{
						play_correct_incorrect_sound(0);
					}
				}

				function checkNav(){
					if(moveOn == 2)
						navigationcontroller();
				}
			break;

		}
		function playaudio(sound_data, next){
				var playing = true;
					if(!playing){
						playaudio(sound_data);
					}

				sound_data.play();
				sound_data.bind('ended', function(){
					setTimeout(function(){
						playing = false;
						sound_data.unbind('ended');
						if(next == null)
						navigationcontroller();
					}, 1000);
				});
			}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext==0)
				navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

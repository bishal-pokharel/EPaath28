var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var s0 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var s1 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var s2 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var s3 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var s4 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var s5 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var s6 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var s7 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var s9 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var s10 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var song = new buzz.sound((soundAsset + "ole_song.ogg"));

var content=[
{
	contentblockadditionalclass: "contentwithbg",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	}
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "catter",
			imgsrc: imgpath + "caterpillar.png"
		}
		]
	}],
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1 selectme",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-1 fadeIn",
		textdata: data.string.p1text1
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2 selectme",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-2 fadeIn",
		textdata: data.string.p1text2
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3 selectme",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-3 fadeIn",
		textdata: data.string.p1text3
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4 selectme",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-4 fadeIn",
		textdata: data.string.p1text4
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5 selectme",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-5 fadeIn",
		textdata: data.string.p1text5
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6 selectme",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-6 fadeIn",
		textdata: data.string.p1text6
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7 selectme",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "dtext-7 fadeIn",
		textdata: data.string.p1text7
	},
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",

	imageblock:[{
		imagetoshow:[
		{
			imgclass: "day-0 rotate",
			imgsrc: imgpath + "caterpillar.gif"
		},
		{
			imgclass: "day-1",
			imgsrc: imgpath + "sunday.png"
		},
		{
			imgclass: "day-2",
			imgsrc: imgpath + "monday.png"
		},
		{
			imgclass: "day-3",
			imgsrc: imgpath + "tuesday.png"
		},
		{
			imgclass: "day-4",
			imgsrc: imgpath + "wednesday.png"
		},
		{
			imgclass: "day-5",
			imgsrc: imgpath + "thursday.png"
		},
		{
			imgclass: "day-6",
			imgsrc: imgpath + "friday.png"
		},
		{
			imgclass: "day-7",
			imgsrc: imgpath + "saturday.png"
		}
		]
	}],
	uppertextblock:[
	{
		textclass: "leg-1",
	},
	{
		textclass: "leg-2",
	},
	{
		textclass: "leg-3",
	},
	{
		textclass: "leg-4",
	},
	{
		textclass: "leg-5",
	},
	{
		textclass: "leg-6",
	},
	{
		textclass: "leg-7",
	},
	]
},
{
	contentblockadditionalclass: "contentwithbg",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.string.p1text8
	}
	],
	imageblock:[{
		imagetoshow:[
		{
			imgclass: "catter",
			imgsrc: imgpath + "caterpillar.png"
		}
		]
	}],
},
{
	contentblockadditionalclass: "ole-background-gradient-apple",
	flipcontainer:[
		{
			sectioncontainer : "section d1",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card1,
		},
		{
			sectioncontainer : "section d2",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card2,
		},
		{
			sectioncontainer : "section d3",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card3,
		},
		{
			sectioncontainer : "section d4",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card4,
		},
		{
			sectioncontainer : "section d5",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card5,

		},
		{
			sectioncontainer : "section d6",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card6,
		},
		{
			sectioncontainer : "section d7",
			imgsrc : imgpath + "caterpillar.png",
			animalname : data.string.card7,
		},
		],
	uppertextblock:[
	{
		textclass: "uptext",
		textdata: data.string.p1text9
	}
	]
}
];

$(function(){
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
        vocabcontroller.findwords(countNext);
        switch(countNext){
			case 0:
				playaudio(s0);
			break;
			case 1:
				playaudio(s1);
			break;
			case 2:
				playaudio(s2);
			break;
			case 3:
				playaudio(s3);
			break;
			case 4:
				playaudio(s4);
			break;
			case 5:
				playaudio(s5);
			break;
			case 6:
				playaudio(s6);
			break;
			case 7:
				playaudio(s7);
			break;
			case 8:
				song.play();
				setTimeout(function(){
					$(".day-1").addClass("selectme");
				}, 3450);
				setTimeout(function(){
					$(".day-1").removeClass("selectme");
					$(".day-2").addClass("selectme");
				}, 4210);
				setTimeout(function(){
					$(".day-2").removeClass("selectme");
					$(".day-3 ").addClass("selectme");
				}, 5580);
				setTimeout(function(){
					$(".day-3").removeClass("selectme");
					$(".day-4 ").addClass("selectme");
				}, 6078);
				setTimeout(function(){
					$(".day-4").removeClass("selectme");
					$(".day-5 ").addClass("selectme");
				}, 7481);
				setTimeout(function(){
					$(".day-5").removeClass("selectme");
					$(".day-6 ").addClass("selectme");
				}, 8085);
				setTimeout(function(){
					$(".day-6").removeClass("selectme");
					$(".day-7 ").addClass("selectme");
				}, 9300);

				setTimeout(function(){
					$(".day-7 ").removeClass("selectme");
					$(".day-1").addClass("selectme");
				}, 11192 );
				setTimeout(function(){
					$(".day-1").removeClass("selectme");
					$(".day-2").addClass("selectme");
				}, 11777);
				setTimeout(function(){
					$(".day-2").removeClass("selectme");
					$(".day-3 ").addClass("selectme");
				}, 13044);
				setTimeout(function(){
					$(".day-3").removeClass("selectme");
					$(".day-4 ").addClass("selectme");
				}, 13686);
				setTimeout(function(){
					$(".day-4").removeClass("selectme");
					$(".day-5 ").addClass("selectme");
				}, 14917);
				setTimeout(function(){
					$(".day-5").removeClass("selectme");
					$(".day-6 ").addClass("selectme");
				}, 15557);
				setTimeout(function(){
					$(".day-6").removeClass("selectme");
					$(".day-7 ").addClass("selectme");
				}, 16826);
				setTimeout(function(){
					$(".day-7").removeClass("selectme");
				}, 18714);


				setTimeout(function(){
					$(".day-7 ").removeClass("selectme");
					$(".day-1").addClass("selectme");
				}, 33565 );
				setTimeout(function(){
					$(".day-1").removeClass("selectme");
					$(".day-2").addClass("selectme");
				}, 34318);
				setTimeout(function(){
					$(".day-2").removeClass("selectme");
					$(".day-3 ").addClass("selectme");
				}, 35632);
				setTimeout(function(){
					$(".day-3").removeClass("selectme");
					$(".day-4 ").addClass("selectme");
				}, 36242);
				setTimeout(function(){
					$(".day-4").removeClass("selectme");
					$(".day-5 ").addClass("selectme");
				}, 37481);
				setTimeout(function(){
					$(".day-5").removeClass("selectme");
					$(".day-6 ").addClass("selectme");
				}, 38139);
				setTimeout(function(){
					$(".day-6").removeClass("selectme");
					$(".day-7 ").addClass("selectme");
				}, 39403);


				setTimeout(function(){
					$(".day-7 ").removeClass("selectme");
					$(".day-1").addClass("selectme");
				}, 41238);
				setTimeout(function(){
					$(".day-1").removeClass("selectme");
					$(".day-2").addClass("selectme");
				}, 41838);
				setTimeout(function(){
					$(".day-2").removeClass("selectme");
					$(".day-3 ").addClass("selectme");
				}, 43083);
				setTimeout(function(){
					$(".day-3").removeClass("selectme");
					$(".day-4 ").addClass("selectme");
				}, 43721);
				setTimeout(function(){
					$(".day-4").removeClass("selectme");
					$(".day-5 ").addClass("selectme");
				}, 44967);
				setTimeout(function(){
					$(".day-5").removeClass("selectme");
					$(".day-6 ").addClass("selectme");
				}, 45555);
				setTimeout(function(){
					$(".day-6").removeClass("selectme");
					$(".day-7 ").addClass("selectme");
				}, 46871);
				setTimeout(function(){
					$(".day-7").removeClass("selectme");
					navigationcontroller();
				}, 10000);
			break;
			case 9:
				playaudio(s9);
			break;
			case 10:
			playaudio(s10, "false");
			var clickcount = 0;
			$(".card").one("click", function(){
			$(this).toggleClass("flipped");
			buzz.all().stop();
			var audio = new buzz.sound((soundAsset + $(this).parent().attr('class').split(' ')[1].toLowerCase()+".ogg"));
			//var audio = $(this).parent().attr('class').split(' ')[1];
			audio.play();
			clickcount++;
			if(clickcount == 7)
				navigationcontroller();
		});
			break;
		}

		function playaudio(sound_data, next){
			buzz.all().stop();
				var playing = true;
					if(!playing){
						playaudio(sound_data);
					}

				sound_data.play();
				sound_data.bind('ended', function(){
					setTimeout(function(){
						playing = false;
						sound_data.unbind('ended');
						if(next == null)
						navigationcontroller();
					}, 1000);
				});
			}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

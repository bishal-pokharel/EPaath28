var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var exe_instruction = new buzz.sound((soundAsset + "exe_instruction.ogg"));

	var content=[
		//slide 0
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext1,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "sit.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "jump.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "run.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 1
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext2,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "talk.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "dinner.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "read.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 2
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext3,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "study.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "dance.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "cycle.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 3
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext4,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "teach.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "study.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "read.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 4
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext5,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "cycle.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "write.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "jump.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 5
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext6,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "write.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "water.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "run.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 6
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext7,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "computer.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "discuss.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "talk.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext8,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "sing.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "dance.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "water.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 8
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext9,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "kick.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "computer.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "cycle.png",
				containerclass : 'class3 options'
			}],
		},
		//slide 9
		{
			contentblockadditionalclass: 'main_bg',
			uppertextblockadditionalclass: 'instruction_div',
			uppertextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.etext10,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "eat.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "jump.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "sing.png",
				containerclass : 'class3 options'
			}],
		}
	];

	content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 10;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();

	rhino.init(10);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		countNext==0?exe_instruction.play():"";
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var wrong_clicked = false;
		$(".option_container").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".option_container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).css("background","#6AA84F");
				$(this).css("border","5px solid #B6D7A8");
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css("background","#EA9999");
				$(this).css("border","5px solid #efb3b3");
				$(this).css("color","#F66E20");
	 			play_correct_incorrect_sound(0);
				wrong_clicked = true;
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 11){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

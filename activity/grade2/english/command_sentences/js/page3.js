var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page3/";

var p3_s1 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var p3_s2 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var p3_s3 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var p3_s4 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var p3_s5 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var p3_s6 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var p3_s7 = new buzz.sound((soundAsset + "p3_s7.ogg"));
var p3_s8 = new buzz.sound((soundAsset + "p3_s8.ogg"));
var p3_s9 = new buzz.sound((soundAsset + "p3_s9.ogg"));
var p3_s10 = new buzz.sound((soundAsset + "p3_s10.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var cm_current_audio = p3_s1;

var content = [

	//slide0
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'commandbackground',

		imageblock : [{
			imagestoshow : [
			{
				imgclass : "squirrel",
				imgsrc : "images/lokharke/2.png",
			}],
			imagelabels: [{
				imagelabelclass : "para_diy my_font_ultra_big happymonkey",
				imagelabeldata : data.string.diytext
			}]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_on",
		},
		{
			switchclass: "switch_on",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "window",
				imgsrc : imgpath + "room/window01.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light01.png",
			},
			{
				imgclass : "bed",
				imgsrc : imgpath + "room/bed.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp01.png",
			},
			{
				imgclass : "dirt_1",
				imgsrc : imgpath + "room/dirty01.png",
			},
			{
				imgclass : "dirt_3",
				imgsrc : imgpath + "room/dirty03.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}],
		imagelabels:[
			{
				imagelabelclass : "snoring",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "blank_light",
				imagelabeldata : '',
			}]
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_on",
		},
		{
			switchclass: "switch_on",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text2,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/window01.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light01.png",
			},
			{
				imgclass : "bed blink_highlight",
				imgsrc : imgpath + "room/bed.png",
			},
			{
				imgclass : "boy_bed",
				imgsrc : imgpath + "room/boywakeup.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp01.png",
			},
			{
				imgclass : "dirt_1",
				imgsrc : imgpath + "room/dirty01.png",
			},
			{
				imgclass : "dirt_3",
				imgsrc : imgpath + "room/dirty03.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}],
		imagelabels:[
			{
				imagelabelclass : "snoring",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "blank_light",
				imagelabeldata : '',
			}]
		}],
	},
	//slide3 lights off
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1 blink_highlight',
		switchblock: [{
			switchclass: "switch_on switch_btn_1",
		},
		{
			switchclass: "switch_on switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text3,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/window01.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light01.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp01.png",
			},
			{
				imgclass : "dirt_1",
				imgsrc : imgpath + "room/dirty01.png",
			},
			{
				imgclass : "dirt_3",
				imgsrc : imgpath + "room/dirty03.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}],
			imagelabels:[{
				imagelabelclass : "blank_light",
				imagelabeldata : '',
			}]
		}],
	},
	//slide4 window
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text4,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window blink_highlight",
				imgsrc : imgpath + "room/window01.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "dirt_1",
				imgsrc : imgpath + "room/dirty01.png",
			},
			{
				imgclass : "dirt_3",
				imgsrc : imgpath + "room/dirty03.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}]
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text5,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "broom blink_highlight",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "dirt_1 dirt_highlight",
				imgsrc : imgpath + "room/dirty01.png",
			},
			{
				imgclass : "dirt_3 dirt_highlight",
				imgsrc : imgpath + "room/dirty03.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}]
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text6,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "brushingtable blink_highlight",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}]
		}],
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text7,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "homework",
				imgsrc : imgpath + "room/homework.png",
			},
			{
				imgclass : "table blink_highlight",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "dress",
				imgsrc : imgpath + "room/dress.png",
			}]
		}],
	},

	//slide8 get ready
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text8,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "door",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "homework",
				imgsrc : imgpath + "room/homework.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "dress blink_highlight",
				imgsrc : imgpath + "room/dress.png",
			},
			{
				imgclass : "boy_2",
				imgsrc : imgpath + "room/boy1.png",
			}]
		}],
	},
	//slide9 opendoor
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text9,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "door blink_highlight",
				imgsrc : imgpath + "room/closedoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "boy_1",
				imgsrc : imgpath + "room/toschool.png",
			}]
		}],
	},
	//slide10 leave
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_main',
		switchdivclass : 'switchdiv switch_1',
		switchblock: [{
			switchclass: "switch_off switch_btn_1",
		},
		{
			switchclass: "switch_off switch_btn_2",
		}],
		// contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'blue_bg',
		uppertextblock : [{
			textdata : data.string.p3text10,
			textclass : 'topic my_font_very_big sniglet'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "window",
				imgsrc : imgpath + "room/openwindow.png",
			},
			{
				imgclass : "table",
				imgsrc : imgpath + "room/Studytable.png",
			},
			{
				imgclass : "door_open",
				imgsrc : imgpath + "room/opendoor.png",
			},
			{
				imgclass : "brushingtable",
				imgsrc : imgpath + "room/brushingtable.png",
			},
			{
				imgclass : "broom",
				imgsrc : imgpath + "room/broom.png",
			},
			{
				imgclass : "clock",
				imgsrc : imgpath + "room/clock8.png",
			},
			{
				imgclass : "lamp",
				imgsrc : imgpath + "room/tablelamp02.png",
			},
			{
				imgclass : "light",
				imgsrc : imgpath + "room/light02.png",
			},
			{
				imgclass : "boy_1 blink_highlight",
				imgsrc : imgpath + "room/toschool.png",
			}]
		}],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.pageEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_diy_audio();
			$nextBtn.show(0);
			break;
		case 1:
			cm_current_audio = p3_s1;
			cm_current_audio.play();
			cm_current_audio.bindOnce('ended', function(){
				$nextBtn.show(0);
			});
			break;
		case 2:
			cm_current_audio = p3_s2;
			cm_current_audio.play();
				$('.bed').click(function(){
				$nextBtn.show(0);
				$('.bed').hide(0);
				$('.snoring').hide(0);
				$('.boy_bed').show(0);
			});
			break;
		case 3:
			cm_current_audio = p3_s3;
			cm_current_audio.play();
			$('.switch_1').mouseenter(function(){
				$('.switch_1').removeClass('blink_highlight');
				$('.switch_1').css('pointer-events', 'all');
			});
			$('.switch_btn_1').click(function(){
				if($(this).hasClass('off_to_on')){
					$(this).removeClass('off_to_on');
					$('.light').attr('src',imgpath + "room/light02.png");
					$('.blank_light').hide(0);
					$(this).addClass('on_to_off');
				} else if($(this).hasClass('on_to_off')){
					$(this).removeClass('on_to_off');
					$(this).addClass('off_to_on');
					$('.blank_light').show(0);
					$('.light').attr('src',imgpath + "room/light01.png");
				} else {
					$(this).addClass('on_to_off');
					$('.blank_light').hide(0);
					$('.light').attr('src',imgpath + "room/light02.png");
				}
				if($('.switch_btn_1').hasClass('on_to_off')){
					$nextBtn.show(0);
				} else {
					$nextBtn.hide(0);
				}
			});
			$('.switch_btn_2').click(function(){
				if($(this).hasClass('off_to_on')){
					$(this).removeClass('off_to_on');
					$('.lamp').attr('src',imgpath + "room/tablelamp02.png");
					$(this).addClass('on_to_off');
				} else if($(this).hasClass('on_to_off')){
					$(this).removeClass('on_to_off');
					$(this).addClass('off_to_on');
					$('.lamp').attr('src',imgpath + "room/tablelamp01.png");
				} else {
					$(this).addClass('on_to_off');
					$('.lamp').attr('src',imgpath + "room/tablelamp02.png");
				}
				if($('.switch_btn_1').hasClass('on_to_off')){
					$nextBtn.show(0);
				} else {
					$nextBtn.hide(0);
				}
			});
			break;
		case 4:
			cm_current_audio = p3_s4;
			cm_current_audio.play();
			$('.window').click(function(){
				$nextBtn.show(0);
				$('.window').attr('src',imgpath + "room/openwindow.png");
				$('.window').removeClass('blink_highlight');
			});
			break;
		case 5:
			cm_current_audio = p3_s5;
			cm_current_audio.play();
			var pick_broom = 0;
			$('.broom').click(function(){
				$('.textblock').addClass('broom_cursor');
				$('.dirt_highlight').addClass('broom_cursor');
				$('.dirt_3').removeClass('broom_cursor');
				$('.dirt_3').addClass('no_cursor');
				// $('.broom').removeClass('blink_highlight');
				$('.broom').hide(0);
				pick_broom = 1;
			});
			$('.dirt_1').click(function(){
				if(pick_broom) {
					$('.textblock').removeClass('broom_cursor');
					$('.textblock').addClass('no_cursor');
					$('.dirt_1').removeClass('dirt_highlight');
					$('.dirt_1').css({
						'height': '20%',
						'left': '18%',
						'top': '55%'
					});
					$('.dirt_1').attr('src',imgpath + "room/cleaning.gif");
					setTimeout(function(){
						if(pick_broom==1){
							$('.textblock').removeClass('no_cursor');
							$('.textblock').addClass('broom_cursor');
							$('.dirt_3').removeClass('no_cursor');
							$('.dirt_3').addClass('broom_cursor');
						}
						if(pick_broom==2){
							$('.textblock').removeClass('no_cursor');
							$nextBtn.show(0);
						}
						$('.dirt_1').hide(0);
						pick_broom++;
					},2000);
				}
			});
			$('.dirt_3').click(function(){
				if(pick_broom) {
					$('.textblock').removeClass('broom_cursor');
					$('.textblock').addClass('no_cursor');
					$('.dirt_1').removeClass('broom_cursor');
					$('.dirt_1').addClass('no_cursor');
					$('.dirt_3').removeClass('dirt_highlight');
					$('.dirt_3').css({
						'height': '20%',
						'left': '78%',
						'top': '58%',
					});
					$('.dirt_3').attr('src',imgpath + "room/cleaning.gif");
					setTimeout(function(){
						if(pick_broom==1){
							$('.textblock').removeClass('no_cursor');
							$('.textblock').addClass('broom_cursor');
							$('.dirt_1').removeClass('no_cursor');
							$('.dirt_1').addClass('broom_cursor');
						}
						if(pick_broom==2){
							$('.textblock').removeClass('no_cursor');
							$nextBtn.show(0);
						}
						pick_broom++;
						$('.dirt_3').hide(0);
					},2000);
				}
			});
			break;
		case 6:
			cm_current_audio = p3_s6;
			cm_current_audio.play();
			$('.brushingtable').click(function(){
				$nextBtn.show(0);
				$('.brushingtable').attr('src',imgpath + "room/brushingteeth.png");
				$('.brushingtable').removeClass('blink_highlight');
			});
			break;
		case 7:
			cm_current_audio = p3_s7;
			cm_current_audio.play();
			$('.table').click(function(){
				$nextBtn.show(0);
				$('.homework').show(0);
				$('.table').removeClass('blink_highlight');
			});
			break;
		case 8:
			cm_current_audio = p3_s8;
			cm_current_audio.play();
			$('.dress').click(function(){
				$nextBtn.show(0);
				$('.boy_2').attr('src',imgpath + "room/boy2.png");
				$('.dress').hide(0);
			});
			break;
		case 9:
			cm_current_audio = p3_s9;
			cm_current_audio.play();
			$('.door').click(function(){
				$nextBtn.show(0);
				$('.door').attr('src',imgpath + "room/opendoor.png");
				$('.door').css({
					'z-index': '50',
					'height': '50%',
					'left': '19%',
					'top': '24.9%'
				});
				$('.door').removeClass('blink_highlight');
			});
			break;
		case 10:
			cm_current_audio = p3_s10;
			cm_current_audio.play();
			$('.boy_1').click(function(){
				ole.footerNotificationHandler.pageEndSetNotification();
				$('.contentblock').removeClass('bg_main');
				$('.contentblock').addClass('bg_school');
				$('.coverboardfull').hide(0);
				$('.contentblock').append('<p class= "topic_top my_font_very_big sniglet">'+data.string.p3text11+'</p');
			});
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				cm_current_audio.stop();
				countNext++;
				templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		cm_current_audio.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

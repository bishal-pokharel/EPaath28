var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page1/";

var intro_1 = new buzz.sound((soundAsset + "this is tara.ogg"));
var intro_2= new buzz.sound((soundAsset + "she gives commands.ogg"));
var intro_3 = new buzz.sound((soundAsset + "she wants everyone to have fun.ogg"));
var clap = new buzz.sound((soundAsset + "clap your hands.ogg"));
var dance = new buzz.sound((soundAsset + "dance.ogg"));
var have_fun = new buzz.sound((soundAsset + "everybody have fun.ogg"));
var hug_friend = new buzz.sound((soundAsset + "hug your friend.ogg"));
var move_body = new buzz.sound((soundAsset + "move your body.ogg"));
var play_bucket = new buzz.sound((soundAsset + "play the bucket.ogg"));
var play_guitar = new buzz.sound((soundAsset + "play the guitar.ogg"));
var read_joke = new buzz.sound((soundAsset + "read a joke.ogg"));
var title = new buzz.sound((soundAsset + "p1_s0.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var cm_current_audio = intro_1;

var content = [

	//slide0
	{

		hasheaderblock : false,
		contentblockadditionalclass :  'cream_bg',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "blackboard",
				imgsrc : imgpath + "blackboard.png",
			}]
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass :  'bg_light_1',

		uppertextblockadditionalclass : 'introtext',
		uppertextblock : [{
			textdata : data.string.pintro1,
			textclass : 'introduction hidden_intro sniglet'
		},
		{
			textdata : data.string.pintro2,
			textclass : 'introduction hidden_intro sniglet'
		},{
			textdata : data.string.pintro3,
			textclass : 'introduction hidden_intro sniglet'
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "tara",
				imgsrc : imgpath + "tara.png",
			}]
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_light_1',
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "fairy move_to_1",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif",
			},
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "giphydance.png",
			},
			{
				imgclass : "images_middle img_2",
				imgsrc : imgpath + "giphyclapping.png",
			},
			{
				imgclass : "images_middle img_3",
				imgsrc : imgpath + "giphyreading.png",
			}],
			imagelabels : [
			]
		}],

		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_1 speech_bg its_hidden',
			speechcontent: data.string.p1text1,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_2 speech_bg its_hidden',
			speechcontent: data.string.p1text2,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_3 speech_bg its_hidden',
			speechcontent: data.string.p1text3,
			speechcontentclass: 'speech_content_1'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		contentblockadditionalclass: 'bg_light_1',
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "fairy move_to_1",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif",
			},
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "guitar.png",
			},
			{
				imgclass : "images_middle img_2",
				imgsrc : imgpath + "drum.png",
			},
			{
				imgclass : "images_middle img_3",
				imgsrc : imgpath + "fun06.png",
			}],
			imagelabels : [
			]
		}],

		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_1 speech_bg its_hidden',
			speechcontent: data.string.p1text4,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_2 speech_bg its_hidden',
			speechcontent: data.string.p1text5,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_3 speech_bg its_hidden',
			speechcontent: data.string.p1text6,
			speechcontentclass: 'speech_content_1'
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		contentblockadditionalclass: 'bg_light_1',
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "fairy_top move_to_top",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif",
			},
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "giphyhugging01.png",
			},
			{
				imgclass : "images_middle img_2",
				imgsrc : imgpath + "giphyhugging02.png",
			},
			{
				imgclass : "images_middle img_3",
				imgsrc : imgpath + "giphyhugging03.png",
			}],
			imagelabels : [
			]
		}],

		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_middle speech_bg its_hidden',
			speechcontent: data.string.p1text7,
			speechcontentclass: 'speech_content_1'
		}],
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		contentblockadditionalclass: 'bg_light_1',
		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "fairy_top move_to_top2",
				imgsrc : imgpath + "flying-chibi-fairy-animation.gif",
			},
			{
				imgclass : "images_bottom img_4",
				imgsrc : imgpath + "fun01.png",
			},
			{
				imgclass : "images_top img_5",
				imgsrc : imgpath + "fun02.png",
			},
			{
				imgclass : "images_middle1 img_6",
				imgsrc : imgpath + "fun03.png",
			},
			{
				imgclass : "images_middle1 img_7",
				imgsrc : imgpath + "fun04.png",
			},
			{
				imgclass : "images_middle1 img_8",
				imgsrc : imgpath + "fun05.png",
			},
			{
				imgclass : "images_bottom img_9",
				imgsrc : imgpath + "fun06.png",
			}],
			imagelabels : [
			]
		}],

		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_middle speech_bg its_hidden',
			speechcontent: data.string.p1text8,
			speechcontentclass: 'speech_content_1'
		}],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 $nextBtn.show(0);
			 $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			var cm_current_audio = title;
			cm_current_audio.play();
            cm_current_audio.bindOnce('ended', function(){
                $nextBtn.show(0);
            });
			break;
		case 1:
			$prevBtn.show(0);
			$nextBtn.hide(0);
			cm_current_audio = intro_1;
			$('.hidden_intro').eq(0).fadeIn(1000);
			cm_current_audio.play();
			cm_current_audio.bindOnce('ended', function(){
				cm_current_audio = intro_2;
				$('.hidden_intro').eq(1).fadeIn(1000);
				cm_current_audio.play();
				cm_current_audio.bindOnce('ended', function(){
					cm_current_audio = intro_3;
					$('.hidden_intro').eq(2).fadeIn(1000);
					cm_current_audio.play();
                    cm_current_audio.bindOnce('ended', function() {
                        $nextBtn.show(0);
                    });
				});
			});
			break;
		case 2:
			$prevBtn.show(0);
			$nextBtn.hide(0);
			not_next_slide = 0;
			setTimeout(function(){
				$('.speech_1').show(0);
				gif_and_sound(".img_1", 'giphydance.gif', move_body,  1);
			}, 2000);
			break;
		case 3:
			not_next_slide = 0;
			$nextBtn.hide(0);
			setTimeout(function(){
				$('.speech_1').show(0);
				gif_and_sound(".img_1", 'guitar.gif', play_guitar,  1);
			}, 2000);
			break;
		case 4:
			not_next_slide = 0;
			$nextBtn.hide(0);
			setTimeout(function(){
				$('.speech_middle').show(0);
				gifs_and_sounds([".img_1",".img_2",".img_3"], ['giphyhugging01.gif', 'giphyhugging02.gif', 'giphyhugging03.gif'], hug_friend,  0);
			}, 2000);
			break;
		case 5:
			not_next_slide = 0;
			$nextBtn.hide(0);
			last_page = true;
			setTimeout(function(){
				$('.speech_middle').show(0);
				gifs_and_sounds([".img_4",".img_5",".img_6",".img_7",".img_8",".img_9"], ['fun01.gif', 'fun02.gif', 'fun03.gif', 'fun04.gif', 'fun05.gif', 'fun06.gif'], have_fun,  0);
			}, 2000);
			break;
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		cm_current_audio.stop();
		switch(countNext){
			case 2:
				if(not_next_slide){
					if(not_next_slide == 1){
						$nextBtn.hide(0);
						$('.fairy').addClass('move_to_2');
						$('.speech_1').hide(0);
						setTimeout(function(){
							$('.fairy').removeClass('move_to_1');
							$('.speech_2').show(0);
							gif_and_sound(".img_2", 'giphyclapping.gif', clap, 1);
						}, 2000);
					}
					if(not_next_slide == 2){
						$nextBtn.hide(0);
						$('.fairy').addClass('move_to_3');
						$('.speech_2').hide(0);
						setTimeout(function(){
							$('.fairy').removeClass('move_to_2');
							$('.speech_3').show(0);
							gif_and_sound(".img_3", 'giphyreading.gif', read_joke, 0);
						}, 2000);
					}
				} else {
					countNext++;
					templateCaller();
				}
				break;
			case 3:
				if(not_next_slide){
					if(not_next_slide == 1){
						$nextBtn.hide(0);
						$('.fairy').addClass('move_to_2');
						$('.speech_1').hide(0);
						setTimeout(function(){
							$('.fairy').removeClass('move_to_1');
							$('.speech_2').show(0);
							gif_and_sound(".img_2", 'drum.gif', play_bucket, 1);
						}, 2000);
					}
					if(not_next_slide == 2){
						$nextBtn.hide(0);
						$('.fairy').addClass('move_to_3');
						$('.speech_2').hide(0);
						setTimeout(function(){
							$('.fairy').removeClass('move_to_2');
							$('.speech_3').show(0);
							gif_and_sound(".img_3", 'fun06.gif', dance, 0);
						}, 2000);
					}
				} else {
					countNext++;
					templateCaller();
				}
				break;
			default:
				countNext++;
				templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		cm_current_audio.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();


	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	function gif_and_sound(image_class, gif_name, sound_name, increment_flag){
		cm_current_audio = sound_name;
		cm_current_audio.play();
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		cm_current_audio.bindOnce('ended', function(){
			$(image_class).attr('src', imgpath +"gifs/" + gif_name);
			$nextBtn.show(0);
			if(increment_flag){
				not_next_slide++;
			} else {
				not_next_slide = 0;
				$prevBtn.show(0);
			}
		});
	}

	function gifs_and_sounds(image_class, gif_name, sound_name, increment_flag){
		cm_current_audio = sound_name;
		cm_current_audio.play();
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		cm_current_audio.bindOnce('ended', function(){
			for(var i =0; i<image_class.length; i++){
				$(image_class[i]).attr('src', imgpath +"gifs/" + gif_name[i]);
			}
			if(!last_page){
				$nextBtn.show(0);
			} else {
				ole.footerNotificationHandler.pageEndSetNotification();
			}

			if(increment_flag){
				not_next_slide++;
			} else {
				not_next_slide = 0;
				$prevBtn.show(0);
			}
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

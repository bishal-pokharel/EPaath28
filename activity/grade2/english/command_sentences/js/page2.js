var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/page2/";

var p2_s1 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var p2_s2 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var p2_s3 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var p2_s4 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var p2_s5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var p2_s6 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var p2_s7 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var p2_s8 = new buzz.sound((soundAsset + "instruction.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];
var cm_current_audio = p2_s1;

var content = [

	//slide0
	{
		hasheaderblock : false,

		contentblockadditionalclass : 'commandbackground',
		uppertextblock : [{
			textdata : data.string.p2text0,
			textclass : 'introduction'
		}]
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "standing.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text2,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "sitting.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle_2 img_1",
				imgsrc : imgpath + "reading.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text4,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "writename.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text5,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle_2 img_1",
				imgsrc : imgpath + "drawing.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text6,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "singing.png",
			}],
			imagelabels : [
			]
		}],
	},
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		// contentblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.p2text7,
			textclass : 'topic'
		}],

		imageblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "images_middle img_1",
				imgsrc : imgpath + "jumping.png",
			}],
			imagelabels : [
			]
		}],
	}
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	var not_next_slide = 0;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 // $nextBtn.show(0);
			  $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			$nextBtn.hide(0);
			p2_s8.play();
			p2_s8.bindOnce('ended', function(){
				$nextBtn.show(0);
			});
			break;
		case 1:
			gif_and_sound(".img_1", 'standing.gif', p2_s1,  1);
			break;
		case 2:
			gif_and_sound(".img_1", 'sitting.gif', p2_s2,  1);
			break;
		case 3:
			gif_and_sound(".img_1", 'reading.gif', p2_s3,  1);
			break;
		case 4:
			gif_and_sound(".img_1", 'writename.gif', p2_s4,  1);
			break;
		case 5:
			gif_and_sound(".img_1", 'drawing.gif', p2_s5,  1);
			break;
		case 6:
			gif_and_sound(".img_1", 'singing.gif', p2_s6,  1);
			break;
		case 7:
			last_page = true;
			gif_and_sound(".img_1", 'jumping.gif', p2_s7,  1);
			break;
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();cm_current_audio = sound_name;
		cm_current_audio.play();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				cm_current_audio.stop();
				countNext++;
				templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		cm_current_audio.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}

	function gif_and_sound(image_class, gif_name, sound_name, increment_flag){
		cm_current_audio = sound_name;
		cm_current_audio.play();
		cm_current_audio.bindOnce('ended', function(){
			$prevBtn.show(0);
			$(image_class).attr('src', imgpath +"gifs/" + gif_name);
			if(!last_page){
				$nextBtn.show(0);
			} else {
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			if(increment_flag){
				not_next_slide++;
			} else {
				not_next_slide = 0;
				$prevBtn.show(0);
			}
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

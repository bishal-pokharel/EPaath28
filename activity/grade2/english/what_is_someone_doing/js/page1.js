var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_intro_0 = new buzz.sound((soundAsset+'s1_p1.ogg'));
var sound_intro_1 = new buzz.sound((soundAsset+'intro/she is preeti.ogg'));
var sound_intro_2 = new buzz.sound((soundAsset+'intro/she is a school teacher.ogg'));
var sound_intro_3 = new buzz.sound((soundAsset+'intro/she is a mother of two children.ogg'));
var sound_intro_4 = new buzz.sound((soundAsset+'intro/preeti is an active woman.ogg'));
var sound_intro_5 = new buzz.sound((soundAsset+'intro/she does a lot of things daily.ogg'));
var sound_intro_6 = new buzz.sound((soundAsset+'intro/let us find out what they are.ogg'));

var sound_exercise = new buzz.sound((soundAsset + "exercising.ogg"));
var sound_cook = new buzz.sound((soundAsset + "cooking.ogg"));
var sound_eat = new buzz.sound((soundAsset + "preeti_eating.ogg"));


//sound_ques is used due to same bind event gets triggered in two different functions=> sound_caller and play_qna
var sound_ques = new buzz.sound((soundAsset + "question.ogg"));

var global_current_sound = sound_intro_1;
var global_current_sound2 = sound_intro_1;

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [
	
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'firstpagebg',
		uppertextblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson_title'
		}]
	},
	
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'contentwithbg',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'preeti_intro',
					imgsrc: imgpath + "preeti.png",
				},
			]
			},
		]
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		// contentblockadditionalclass: 'bg_6am',
		contentblockadditionalclass: 'contentwithbg',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'preeti_intro',
					imgsrc: imgpath + "preeti1.png",
				},{
					imgclass:'blackboard',
					imgsrc: imgpath + "board.png",
				},
			]
			},
		]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		// contentblockadditionalclass: 'bg_6am',
		contentblockadditionalclass: 'contentwithbg',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'children',
					imgsrc: imgpath + "children.png",
				},
			]
			},
		]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		// contentblockadditionalclass: 'bg_6am',
		contentblockadditionalclass: 'contentwithbg',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'preeti_intro',
					imgsrc: imgpath + "preeti.png",
				}
			]
			},
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblockadditionalclass: 'contentwithbg',
		contentblocknocenteradjust : true,
	
		// contentblockadditionalclass: 'bg_6am',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'lesson_intro'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'preeti_intro',
					imgsrc: imgpath + "preeti.png",
				}
			]
			},
		]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		// contentblockadditionalclass: 'bg_6am',
		contentblockadditionalclass: 'contentwithbg',
		uppertextblockadditionalclass: 'intro_block',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'lesson_intro'
		},
		{
			textdata : data.string.p1text5,
			textclass : 'lesson_intro'
		},
		{
			textdata : data.string.p1text6,
			textclass : 'lesson_intro'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'preeti_intro',
					imgsrc: imgpath + "preeti.png",
				}
			]
			},
		]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_1',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p1text7,
			textclass : 'text_qna answer'
		}],
		
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_2',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p1text8,
			textclass : 'text_qna answer'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'steam_cook_1 steam_animation',
					imgsrc: imgpath + "steam.png",
				}
			]
			},
		]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_3',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p1text9,
			textclass : 'text_qna answer'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'steam_cook_2 steam_animation',
					imgsrc: imgpath + "steam.png",
				}
			]
		}
		]
	},
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var my_timeout = null;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);
	
	
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			intro_caller(sound_intro_0);
			setTimeout(function(){
                $nextBtn.show(0);
            },3000);
			break;
		case 1:
			intro_caller(sound_intro_1);
			$prevBtn.show(0);
			break;
		case 2:
			intro_caller(sound_intro_2);
			$prevBtn.show(0);
			break;
		case 3:
			intro_caller(sound_intro_3);
			$prevBtn.show(0);
			break;
		case 4:
			intro_caller(sound_intro_4);
			$prevBtn.show(0);
			break;
		case 5:
			intro_caller(sound_intro_5);
			$prevBtn.show(0);
			break;
		case 6:
			intro_caller(sound_intro_6);
			$prevBtn.show(0);
			break;
		case 7:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_exercise, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_exercise, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 8:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_cook, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_cook, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 9:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_eat, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_eat, '.answer', 'blink_and_big', '.text_qna');
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(my_timeout);
		global_current_sound2.stop();
		global_current_sound.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(my_timeout);
		global_current_sound2.stop();
		global_current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	

	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	function intro_caller(sound_var){
		$nextBtn.hide(0);
		global_current_sound.stop();
		global_current_sound = sound_var;
		global_current_sound.play();
		global_current_sound.bindOnce('ended', function(){
			$nextBtn.show(0);
		});
	}
	function sound_caller(sound_var, sound_box_class, sound_box_animation, common_class){
		$(sound_box_class).click(function(){		
			$(common_class).removeClass(sound_box_animation);
			global_current_sound.stop();
			global_current_sound = sound_var;
			global_current_sound.play();
			$(sound_box_class).addClass(sound_box_animation);
			global_current_sound.bindOnce('ended', function(){
				$(sound_box_class).removeClass(sound_box_animation);
			});
		});
	}
	function play_qna(sound_class_ques, sound_class_text, ques_class, text_class, ques_animation, text_animation, common_class){
		clearTimeout(my_timeout);
		global_current_sound.stop();
		global_current_sound2.stop();
		global_current_sound2 = sound_class_text;
		$(common_class).css('pointer-events', 'none');
		$(ques_class).addClass(ques_animation);
		$(text_class).hide(0);
		global_current_sound = sound_class_ques;
		$nextBtn.hide(0);
		global_current_sound.play();
		global_current_sound.bindOnce('ended', function(){
			setTimeout(function(){
				$(text_class).show(0);
				$(ques_class).removeClass(ques_animation);
				$(text_class).addClass(text_animation);
				global_current_sound2.play();
				global_current_sound2.bindOnce('ended', function(){	
					$(text_class).removeClass(text_animation);
					if(countNext == content.length-1)
					{
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				$(common_class).css({'pointer-events': 'all', 'box-shadow': '0.5vmin 0.5vmin 0.5vmin #999999'});
				});
			}, 100);
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

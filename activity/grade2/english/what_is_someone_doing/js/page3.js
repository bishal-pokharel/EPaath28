var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_garden = new buzz.sound((soundAsset + "preeti_gardening.ogg"));
var sound_dinner = new buzz.sound((soundAsset + "preeti_dinner.ogg"));
var sound_eat = new buzz.sound((soundAsset + "preeti is eating dinner.ogg"));
var sound_story = new buzz.sound((soundAsset + "tellingstory.ogg"));
var sound_read = new buzz.sound((soundAsset + "preeti is reading a book.ogg"));
var sound_sleep = new buzz.sound((soundAsset + "preeti is sleeping.ogg"));

//sound_ques is used due to same bind event gets triggered in two different functions=> sound_caller and play_qna
var sound_ques = new buzz.sound((soundAsset + "question.ogg"));

var global_current_sound = sound_ques;
var global_current_sound2 = sound_ques;

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_1 backmove',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text1,
			textclass : 'text_qna answer'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'butterfly butterfly_move',
					imgsrc: imgpath + "butterfly.gif",
				},
				{
					imgclass:'bee bee_move',
					imgsrc: imgpath + "bee.gif",
				},
			],
			imagelabels : [
				{
					imagelabelclass: ' water_can',
					imagelabeldata: ''
				}
			]}
		]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_2',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text2,
			textclass : 'text_qna answer'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'steam_cook_1 steam_animation',
					imgsrc: imgpath + "steam.png",
				}
			]
			},
		]

	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_3',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text3,
			textclass : 'text_qna answer'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'steam_cook_3 steam_animation',
					imgsrc: imgpath + "steam.png",
				}
			]
			},
		]
	},


	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_4',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text4,
			textclass : 'text_qna answer'
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_5',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text5,
			textclass : 'text_qna answer'
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'bg_story_6',
		uppertextblockadditionalclass: 'conversation',
		uppertextblock : [{
			textdata : data.string.pqtext,
			textclass : 'text_qna question'
		},
		{
			textdata : data.string.p3text6,
			textclass : 'text_qna answer'
		}],
		imageblock : [{
			imagelabels:[
				{
					imagelabelclass : "snoring",
					imagelabeldata : '',
				}]
		}],
	}
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var my_timeout = null;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);
	
	
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_qna(sound_ques, sound_garden, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_garden, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 1:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_dinner, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_dinner, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 2:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_eat, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_eat, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 3:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_story, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_story, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 4:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_read, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_read, '.answer', 'blink_and_big', '.text_qna');
			break;
		case 5:
			$prevBtn.show(0);
			play_qna(sound_ques, sound_sleep, '.question', '.answer','blink_and_big', 'blink_and_big', '.text_qna');
			sound_caller(sound_ques, '.question', 'blink_and_big', '.text_qna');
			sound_caller(sound_sleep, '.answer', 'blink_and_big', '.text_qna');
			break;	
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		global_current_sound.stop();
		global_current_sound2.stop();
		clearTimeout(my_timeout);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		global_current_sound2.stop();
		global_current_sound.stop();
		clearTimeout(my_timeout);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	

	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	function sound_caller(sound_var, sound_box_class, sound_box_animation, common_class){
		$(sound_box_class).click(function(){		
			$(common_class).removeClass(sound_box_animation);
			global_current_sound.stop();
			global_current_sound = sound_var;
			global_current_sound.play();
			$(sound_box_class).addClass(sound_box_animation);
			global_current_sound.bindOnce('ended', function(){
				$(sound_box_class).removeClass(sound_box_animation);
			});
		});
	}
	function play_qna(sound_class_ques, sound_class_text, ques_class, text_class, ques_animation, text_animation, common_class){
		clearTimeout(my_timeout);
		global_current_sound.stop();
		global_current_sound2.stop();
		global_current_sound2 = sound_class_text;
		$(common_class).css('pointer-events', 'none');
		$(ques_class).addClass(ques_animation);
		$(text_class).hide(0);
		global_current_sound = sound_class_ques;
		$nextBtn.hide(0);
		global_current_sound.play();
		global_current_sound.bindOnce('ended', function(){
			setTimeout(function(){
				$(text_class).show(0);
				$(ques_class).removeClass(ques_animation);
				$(text_class).addClass(text_animation);
				global_current_sound2.play();
				global_current_sound2.bindOnce('ended', function(){	
					$(text_class).removeClass(text_animation);
					if(countNext == content.length-1)
					{
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				$(common_class).css({'pointer-events': 'all', 'box-shadow': '0.5vmin 0.5vmin 0.5vmin #999999'});
				});
			}, 100);
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

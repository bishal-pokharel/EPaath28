var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"
var instruction = new buzz.sound((soundAsset+'click_on_the.ogg'));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext1,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "1.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext2,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "2.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e2ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e2ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e2ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext3,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "3.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e3ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e3ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e3ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext4,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "4.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e4ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e4ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e4ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext5,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "5.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e5ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e5ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e5ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext6,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "6.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e6ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e6ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e6ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext7,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "7.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e7ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e7ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e7ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext8,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "8.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e8ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e8ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e8ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext9,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "9.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e9ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e9ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e9ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext10,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "10.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e10ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e10ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e10ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 10
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext11,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "11.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e11ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e11ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e11ansc,
			textclass : 'class3 options'
		}],
	},
	//slide 11
	{
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [
		{
			textdata : data.string.etext12,
			textclass : 'instruction my_font_big',
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "12.png",
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e12ansa,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e12ansb,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e12ansc,
			textclass : 'class3 options'
		}],
	},
];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();

	rhino.init(12);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
    rhino.numberOfQuestions();
			if(countNext==0){
				instruction.play();
			}

        //randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html(data.string.einstruction);
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({
					'background-color': '#BA6B82',
					'pointer-events': 'none',
					'border': 'none'
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

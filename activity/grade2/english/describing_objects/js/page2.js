var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "float-in",
		}],
		speechbox:[{
			speechbox: 'textboxsp sp-1',
			textdata : data.string.p2text2,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag-1 fade-in-4",
					imgsrc : '',
					imgid : 'bag-1'
				},
				{
					imgclass : "bag-2 fade-in-4",
					imgsrc : '',
					imgid : 'bag-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'asha-1'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp sp-2',
			textdata : data.string.p2text3,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag-1",
					imgsrc : '',
					imgid : 'bag-1'
				},
				{
					imgclass : "bag-2",
					imgsrc : '',
					imgid : 'bag-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'asha-2'
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp sp-3',
			textdata : data.string.p2text4,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag-1",
					imgsrc : '',
					imgid : 'bag-1'
				},
				{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-bag-1'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'asha-3'
				},
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp sp-4',
			textdata : data.string.p2text5,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag-2",
					imgsrc : '',
					imgid : 'bag-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-bag-2'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'asha-3'
				},
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p2text6,
			textclass: "last-tag tag-1 fade-in its_hidden",
		},{
			textdata: data.string.p2text7,
			textclass: "last-tag tag-2 fade-in its_hidden",
		}],
		speechbox:[{
			speechbox: 'textboxsp fade-out sp-4',
			textdata : data.string.p2text5,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag-anim-1 bag-1",
					imgsrc : '',
					imgid : 'bag-1'
				},
				{
					imgclass : "bag-anim-2 bag-2",
					imgsrc : '',
					imgid : 'bag-2'
				},
				{
					imgclass : "fade-out girl-1",
					imgsrc : '',
					imgid : 'girl-3'
				},
				{
					imgclass : "fade-out asha-1",
					imgsrc : '',
					imgid : 'asha-3'
				},
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spx-1',
			textdata : data.string.p2text8,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "house-1",
					imgsrc : '',
					imgid : 'house-1'
				},
				{
					imgclass : "house-2",
					imgsrc : '',
					imgid : 'house-2'
				},
				{
					imgclass : "girla-1",
					imgsrc : '',
					imgid : 'girl-4'
				},
				{
					imgclass : "sagar-1",
					imgsrc : '',
					imgid : 'sagar-1'
				},
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spx-2',
			textdata : data.string.p2text9,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "house-1",
					imgsrc : '',
					imgid : 'house-1'
				},
				{
					imgclass : "house-2",
					imgsrc : '',
					imgid : 'house-2'
				},
				{
					imgclass : "girla-1",
					imgsrc : '',
					imgid : 'girl-6'
				},
				{
					imgclass : "sagar-1",
					imgsrc : '',
					imgid : 'sagar-new'
				},
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spx-3',
			textdata : data.string.p2text10,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "house-1",
					imgsrc : '',
					imgid : 'house-1'
				},
				{
					imgclass : "house-2",
					imgsrc : '',
					imgid : 'house-2'
				},
				{
					imgclass : "girla-1",
					imgsrc : '',
					imgid : 'girl-4'
				},
				{
					imgclass : "sagar-1",
					imgsrc : '',
					imgid : 'sagar-2'
				},
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-gr',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p2text11,
			textclass: "last-tag tag-3 fade-in its_hidden",
		},{
			textdata: data.string.p2text12,
			textclass: "last-tag tag-4 fade-in its_hidden",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fade-out-slow bg-full",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "house-1",//house-anim-1
					imgsrc : '',
					imgid : 'house-1'
				},
				{
					imgclass : "house-2",//house-anim-2
					imgsrc : '',
					imgid : 'house-2'
				},
				{
					imgclass : "girla-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
				{
					imgclass : "sagar-1",
					imgsrc : '',
					imgid : 'sagar-4'
				},
			]
		}]
	},
	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spz-1',
			textdata : data.string.p2text13,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dog-1",
					imgsrc : '',
					imgid : 'dog-1'
				},
				{
					imgclass : "dog-2",
					imgsrc : '',
					imgid : 'dog-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'prem-1'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'sagar-4'
				},
			]
		}]
	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spz-2',
			textdata : data.string.p2text14,
			imgclass: '',
			textclass : '',
			imgid : 'textbox-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dog-1",
					imgsrc : '',
					imgid : 'dog-1'
				},
				{
					imgclass : "dog-2",
					imgsrc : '',
					imgid : 'dog-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'prem-2'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'sagar-5'
				},
			]
		}]
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spz-1',
			textdata : data.string.p2text15,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dog-1",
					imgsrc : '',
					imgid : 'dog-1'
				},
				{
					imgclass : "dog-2",
					imgsrc : '',
					imgid : 'dog-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'prem-3'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'sagar-6'
				},
			]
		}]
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		speechbox:[{
			speechbox: 'textboxsp spz-1',
			textdata : data.string.p2text16,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dog-1",
					imgsrc : '',
					imgid : 'dog-1'
				},
				{
					imgclass : "dog-2",
					imgsrc : '',
					imgid : 'dog-2'
				},
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'prem-1'
				},
				{
					imgclass : "asha-1",
					imgsrc : '',
					imgid : 'sagar-6'
				},
			]
		}]
	},
	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		uppertextblockadditionalclass: 'title-text',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "",
		},{
			textdata: data.string.p2text1a,
			textclass: "",
		}],
		extratextblock:[{
			textdata: data.string.p2text17,
			textclass: "last-tag tag-1 fade-in its_hidden",
		},{
			textdata: data.string.p2text18,
			textclass: "last-tag tag-2 fade-in its_hidden",
		}],
		speechbox:[{
			speechbox: 'textboxsp  fade-out spz-1',
			textdata : data.string.p2text16,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "dog-anim-1 dog-1",
					imgsrc : '',
					imgid : 'dog-1'
				},
				{
					imgclass : "dog-anim-2 dog-2",
					imgsrc : '',
					imgid : 'dog-2'
				},
				{
					imgclass : "fade-out girl-1",
					imgsrc : '',
					imgid : 'prem-1'
				},
				{
					imgclass : "fade-out asha-1",
					imgsrc : '',
					imgid : 'sagar-6'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl-1", src: imgpath+"page2/rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"page2/rumi01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"page2/rumi02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-4", src: imgpath+"page2/rumi03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5", src: imgpath+"page2/rumi04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-6", src: imgpath+"page2/rumi05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "asha-1", src: imgpath+"page2/asha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha-2", src: imgpath+"page2/asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha-3", src: imgpath+"page2/asha02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bag-1", src: imgpath+"page2/lightbag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag-2", src: imgpath+"page2/bag.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg", src: imgpath+"page2/bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house-1", src: imgpath+"page2/house-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house-2", src: imgpath+"page2/house-2.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sagar-1", src: imgpath+"page2/sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-2", src: imgpath+"page2/sagar01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-3", src: imgpath+"page2/sagar02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sagar-4", src: imgpath+"page2/sagar03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-5", src: imgpath+"page2/sagar04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-6", src: imgpath+"page2/sagar05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar-new", src: imgpath+"page2/sagar_new.png", type: createjs.AbstractLoader.IMAGE},

			{id: "prem-1", src: imgpath+"page2/prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem-2", src: imgpath+"page2/prem02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem-3", src: imgpath+"page2/prem03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "dog-1", src: imgpath+"page2/dog_small.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog-2", src: imgpath+"page2/dog.png", type: createjs.AbstractLoader.IMAGE},

			{id: "girl-bag-1", src: imgpath+"page2/rumiwithbag02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-bag-2", src: imgpath+"page2/rumiwithbag01.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "textbox", src: imgpath+'textbox.svg', type: createjs.AbstractLoader.IMAGE},
			{id: "textbox-2", src: imgpath+'textbox-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_0a", src: soundAsset+"p2_s0_1.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12_new.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				current_sound = createjs.Sound.play('sound_0');
				current_sound.play();
				current_sound.on("complete", function(){
					conversation('.textboxsp', 'sound_0a');
				});
				break;
			case 1:
				$prevBtn.show(0);
				conversation('.textboxsp', 'sound_1');
				break;
			case 4:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_4');
					$('.its_hidden').show(0);
				}, 3000);
				break;
			case 8:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_8');
					$('.its_hidden').show(0);
				}, 3000);
				break;
			case 13:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function(){
					sound_nav('sound_13');
					$('.its_hidden').show(0);
				}, 3000);
				break;
			default:
				$prevBtn.show(0);
				conversation('.textboxsp', 'sound_'+countNext);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function conversation(speech_class, sound_data){
		$(speech_class).fadeIn(1000, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				$(speech_class).click(function(){
					sound_player(sound_data);
				});
			});
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

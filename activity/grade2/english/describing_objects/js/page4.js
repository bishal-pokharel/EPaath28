var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',

		uppertextblockadditionalclass: 'slide-down diy-text',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "",
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "top-instruction",
		}],
		option:[
		{
			textdata: data.string.p4text2,
			optclass: "choices correct choice-1",
		},
		{
			textdata: data.string.p4text3,
			optclass: "choices choice-2",
		}],
		uppertextblockadditionalclass: 'textbox speech-text',
		uppertextblock: [{
			textdata: data.string.p4text4,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'shirt'
				},
				{
					imgclass : "sq-bottom",
					imgsrc : '',
					imgid : 'squirrel'
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "top-instruction",
		}],
		option:[
		{
			textdata: data.string.p4text2,
			optclass: "choices choice-1",
		},
		{
			textdata: data.string.p4text3,
			optclass: "choices correct choice-2",
		}],
		uppertextblockadditionalclass: 'textbox speech-text',
		uppertextblock: [{
			textdata: data.string.p4text7,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'clock'
				},
				{
					imgclass : "sq-bottom",
					imgsrc : '',
					imgid : 'squirrel'
				},
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "top-instruction",
		}],
		option:[
		{
			textdata: data.string.p4text2,
			optclass: "choices choice-1",
		},
		{
			textdata: data.string.p4text3,
			optclass: "choices correct choice-2",
		}],
		uppertextblockadditionalclass: 'textbox speech-text',
		uppertextblock: [{
			textdata: data.string.p4text10,
			textclass: "",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "center-img",
					imgsrc : '',
					imgid : 'shoe'
				},
				{
					imgclass : "sq-bottom",
					imgsrc : '',
					imgid : 'squirrel'
				},
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var choice_arr_1 = ['a beautiful', 'a brown', 'a nice', 'a lovely', 'an old', 'a yellow'];
	var choice_arr_2 = ['a red', 'a juicy', 'a round', 'a small', 'a tasty', 'a crunchy'];
	var choice_idx_arr;
	var count_choice = 0;

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "squirrel", src: imgpath+'sq-pointing.png', type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel-2", src: imgpath+'squirrel.png', type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel-correct", src: imgpath+'sq-correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel-incorrect", src: imgpath+'sq-incorrect.png', type: createjs.AbstractLoader.IMAGE},

			{id: "slider", src: imgpath+"page4/slider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shirt", src: imgpath+"page4/greenshirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "clock", src: imgpath+"page4/clock.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoe", src: imgpath+"page4/old_shoe.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "textbox", src: imgpath+'textbox.svg', type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_drop", src: soundAsset+"drop.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1_0.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s1_1.ogg"},
			{id: "sound_4", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_5", src: soundAsset+"p4_s3.ogg"},

			{id: "s_correct_1", src: soundAsset+"correct1.ogg"},
			{id: "s_correct_2", src: soundAsset+"correct2.ogg"},
			{id: "s_correct_3", src: soundAsset+"correct3.ogg"},
			{id: "s_incorrect_1", src: soundAsset+"wrong1.ogg"},
			{id: "s_incorrect_2", src: soundAsset+"wrong2.ogg"},
			{id: "s_incorrect_3", src: soundAsset+"wrong3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext) {
			case 0:
				$('.diy-text').css({
					'background-image': 'url('+preload.getResult('slider').src+')',
					'background-size': '100% 100%',
				});
				current_sound = createjs.Sound.play('sound_drop');
				current_sound.play();
				current_sound.on("complete", function(){
					current_sound = createjs.Sound.play('sound_1');
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
					});
				});
				break;
			case 1:
				$prevBtn.show(0);
				init_diy();
				current_sound = createjs.Sound.play('sound_2');
				current_sound.play();
				current_sound.on("complete", function(){
					current_sound = createjs.Sound.play('sound_3');
					current_sound.play();
					current_sound.on("complete", function(){
						choose_y_n(data.string.p4text5, data.string.p4text6, 's_correct_1', 's_incorrect_1');
						// $('.speech-text').click(function(){
						// 	sound_player('sound_3');
						// });
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				init_diy();
				current_sound = createjs.Sound.play('sound_4');
				current_sound.play();
				current_sound.on("complete", function(){
					choose_y_n(data.string.p4text8, data.string.p4text9, 's_correct_2', 's_incorrect_2');
					// $('.speech-text').click(function(){
					// 	sound_player('sound_4');
					// });
				});
				break;
			case 3:
				$prevBtn.show(0);
				init_diy();
				current_sound = createjs.Sound.play('sound_5');
				current_sound.play();
				current_sound.on("complete", function(){
					choose_y_n(data.string.p4text11, data.string.p4text12, 's_correct_3', 's_incorrect_3');
					// $('.speech-text').click(function(){
					// 	sound_player('sound_5');
					// });
				});
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function init_diy(){
		$('.textbox').css({
			'background-image': 'url('+preload.getResult('textbox').src+')',
			'background-size': '100% 100%',
		});
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
	}
	function choose_y_n(correct_data, incorrect_data, sound_1, sound_2){
		$('.choices').css('pointer-events', 'all');
		$('.choices').click(function(){
			if($(this).hasClass('correct')){
				sound_player(sound_1);
				$('.sq-bottom').attr('src', preload.getResult('squirrel-correct').src).css('left','10%');
				$('.textbox>p').html(correct_data);
				$('.choices').css('pointer-events', 'none');
				$(this).css({
					'background-color': '#4CAF50',
					'color': 'white'
				});
				$(this).children('.correct-icon').show(0);
				nav_button_controls(0);
			} else{
				sound_player(sound_2);
				$('.sq-bottom').attr('src', preload.getResult('squirrel-incorrect').src).css('left','10%');
				$('.textbox>p').html(incorrect_data);
				$(this).css({
					'pointer-events': 'none',
					'background-color': '#BA6B82',
					'color': 'white'
				});
				$(this).children('.incorrect-icon').show(0);
			}
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

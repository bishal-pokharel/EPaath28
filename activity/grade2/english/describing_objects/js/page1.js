var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sq-cover",
					imgsrc : '',
					imgid : 'squirrel'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1text2,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sq-bottom",
					imgsrc : '',
					imgid : 'squirrel-2'
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text3,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text4,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'elephant'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'mouse'
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text6,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'giraffe'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'pig'
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text7,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text8,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'room-1'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'room-2'
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text9,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text10,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'glass-1'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'glass-2'
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text11,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text12,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'shoe-1'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'shoe-2'
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		
		extratextblock:[{
			textdata: data.string.p1text13,
			textclass: "label-card fade-in label-1",
		},{
			textdata: data.string.p1text14,
			textclass: "label-card fade-in label-2",
		}],
		
		imagedivblock:[{
			imgdiv : 'card flip-anim card-1',
			imgclass : "item-1",
			imgid : 'girl-1'
		},{
			imgdiv : 'card flip-anim card-2',
			imgclass : "item-2",
			imgid : 'girl-2'
		}]
	}
];


$(function () { 
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "squirrel", src: imgpath+"sq-board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel-2", src: imgpath+"squirrel.png", type: createjs.AbstractLoader.IMAGE},
			
			{id: "elephant", src: imgpath+"bigtree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"smalltree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "giraffe", src: imgpath+"wideroad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"narrowroad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "room-2", src: imgpath+"lightroom.png", type: createjs.AbstractLoader.IMAGE},
			{id: "room-1", src: imgpath+"darkroom.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass-1", src: imgpath+"empty-glass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "glass-2", src: imgpath+"glass-with-water.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoe-1", src: imgpath+"old_shoe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shoe-2", src: imgpath+"new_shoe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-1", src: imgpath+"long-hair.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"shorthair.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "textbox", src: imgpath+'textbox.svg', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2_0.ogg"},
			{id: "sound_3a", src: soundAsset+"p1_s2_1.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3_0.ogg"},
			{id: "sound_4a", src: soundAsset+"p1_s3_1.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4_0.ogg"},
			{id: "sound_5a", src: soundAsset+"p1_s4_1.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s5_0.ogg"},
			{id: "sound_6a", src: soundAsset+"p1_s5_1.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s6_0.ogg"},
			{id: "sound_7a", src: soundAsset+"p1_s6_1.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s7_0.ogg"},
			{id: "sound_8a", src: soundAsset+"p1_s7_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		
		switch(countNext) {
			case 0:
				$nextBtn.show(0);
				sound_nav('sound_1');
				break;
			case 1:
				$prevBtn.show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_2');
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
					$('.sp-1').css('cursor', 'pointer');
					$('.sp-1').click(function(){
						sound_player('sound_2');
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				sound_card('sound_3', 'sound_3a');
				break;
			case 3:
				$prevBtn.show(0);
				sound_card('sound_4', 'sound_4a');
				break;
			case 4:
				$prevBtn.show(0);
				sound_card('sound_5', 'sound_5a');
				break;
			case 5:
				$prevBtn.show(0);
				sound_card('sound_6', 'sound_6a');
				break;
			case 6:
				$prevBtn.show(0);
				sound_card('sound_7', 'sound_7a');
				break;
			case 7:
				$prevBtn.show(0);
				sound_card('sound_8', 'sound_8a');
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function sound_card(sound_data1, sound_data2){
		$('.card-1').show(0);
		timeoutvar = setTimeout(function(){
			$('.label-1').show(0);
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				$('.card-2').show(0);
				timeoutvar = setTimeout(function(){
					$('.label-2').show(0);
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$('.label-card').addClass('label-card-hover');
						$('.label-1').click(function(){
							sound_player(sound_data1);
						});
						$('.label-2').click(function(){
							sound_player(sound_data2);
						});
					});
				}, 500);
			});
		}, 500);
	}
	
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageClass = content[count].imagedivblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	}); 

});

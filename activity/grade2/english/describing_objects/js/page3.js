var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p3text1,
			imgclass: '',
			textclass : '',
			imgid : 'textbox',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "sq-bottom",
					imgsrc : '',
					imgid : 'squirrel'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p3text2,
			textclass: "top-instruction",
		},
		{
			textdata: data.string.p3text3,
			textclass: "choices choice-1",
		},
		{
			textdata: data.string.p3text4,
			textclass: "choices choice-2",
		},
		{
			textdata: data.string.p3text5,
			textclass: "choices choice-3",
		},
		{
			textdata: data.string.p3text6,
			textclass: "choices choice-4",
		},
		{
			textdata: data.string.p3text7,
			textclass: "choices choice-5",
		},
		{
			textdata: data.string.p3text8,
			textclass: "choices choice-6",
		}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "temple",
					imgsrc : '',
					imgid : 'temple'
				},
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		
		uppertextblockadditionalclass: 'fade-in-fast speech-text',
		uppertextblock: true,
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "temple",
					imgsrc : '',
					imgid : 'temple'
				},
				{
					imgclass : "sq-side fade-in-fast",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: data.string.p3text12,
			textclass: "top-instruction-2",
		},
		{
			textdata: data.string.p3text13,
			textclass: "choices choices-tb  choice-1",
		},
		{
			textdata: data.string.p3text14,
			textclass: "choices choices-tb choice-2",
		},
		{
			textdata: data.string.p3text15,
			textclass: "choices choices-tb choice-3",
		},
		{
			textdata: data.string.p3text16,
			textclass: "choices choices-tb choice-4",
		},
		{
			textdata: data.string.p3text17,
			textclass: "choices choices-tb choice-5",
		},
		{
			textdata: data.string.p3text18,
			textclass: "choices choices-tb choice-6",
		}],
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "apple",
					imgsrc : '',
					imgid : 'apple'
				},
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		
		uppertextblockadditionalclass: 'fade-in-fast speech-text',
		uppertextblock: true,
		
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "apple",
					imgsrc : '',
					imgid : 'apple'
				},
				{
					imgclass : "sq-side fade-in-fast",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}]
	}
];


$(function () { 
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	var choice_arr_1 = ['a <span class="hl-blue">beautiful</span>', 'a <span class="hl-blue">brown</span>', 'a <span class="hl-blue">nice</span>', 'a <span class="hl-blue">lovely</span>', 'an <span class="hl-blue">old</span>', 'a <span class="hl-blue">yellow</span>'];
	var choice_arr_2 = ['a <span class="hl-blue">red</span>', 'a <span class="hl-blue">juicy</span>', 'a <span class="hl-blue">round</span>', 'a <span class="hl-blue">small</span>', 'a <span class="hl-blue">tasty</span>', 'a <span class="hl-blue">crunchy</span>'];
	var choice_idx_arr;
	var count_choice = 0;
	
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "squirrel", src: imgpath+"squirrel.png", type: createjs.AbstractLoader.IMAGE},
			
			{id: "temple", src: imgpath+"page3/temple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple", src: imgpath+"page3/apple.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "textbox", src: imgpath+'textbox.svg', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"let_us_learn.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			
			{id: "s_a", src: soundAsset+"p3_s2_0.ogg"},
			{id: "s_an", src: soundAsset+"p3_s2_0a.ogg"},
			{id: "s_temple", src: soundAsset+"p3_s2_1.ogg"},
			{id: "s_a1", src: soundAsset+"p3_s4_0.ogg"},
			{id: "s_apple", src: soundAsset+"p3_s4_1.ogg"},
			
			{id: "s_beautiful", src: soundAsset+"beautiful.ogg"},
			{id: "s_brown", src: soundAsset+"brown.ogg"},
			{id: "s_crunchy", src: soundAsset+"crunchy.ogg"},
			{id: "s_juicy", src: soundAsset+"juicy.ogg"},
			{id: "s_lovely", src: soundAsset+"lovely.ogg"},
			{id: "s_nice", src: soundAsset+"nice.ogg"},
			{id: "s_old", src: soundAsset+"old.ogg"},
			{id: "s_red", src: soundAsset+"red.ogg"},
			{id: "s_round", src: soundAsset+"round.ogg"},
			{id: "s_small", src: soundAsset+"small.ogg"},
			{id: "s_tasty", src: soundAsset+"tasty.ogg"},
			{id: "s_yellow", src: soundAsset+"yellow.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		
		switch(countNext) {
			case 0:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
					$('.sp-1').css('cursor', 'pointer');
					$('.sp-1').click(function(){
						sound_player('sound_1');
					});
				});
				break;
			case 1:
			case 3:
				$prevBtn.show(0);
				if(countNext==1){
					sound_player('sound_2');
				}else{
					sound_player('sound_3');
				}
				count_choice = 0;
				choice_idx_arr = [0,0,0,0,0,0];
				var selected = 'selected';
				if(countNext==3){
					selected = 'selected-2';
				}
				$('.choices').click(function(){
					var index = $(this).attr('class').replace(/\D/g,'')-1;
					if($(this).hasClass(selected)){
						$(this).removeClass(selected);
						choice_idx_arr[index] = 0;
						count_choice--;
						if(count_choice<3){
							$nextBtn.hide(0);
						}
					} else{
						$(this).addClass(selected);
						choice_idx_arr[index] = 1;
						count_choice++;
						if(count_choice>2){
							$nextBtn.show(0);
						}
					}
				});
				break;
			case 2:
			case 4:
				if(countNext==2){
					// sound_player('sound_2');
				}else{
					// sound_player('sound_3');
				}
				$prevBtn.show(0);
				$('.speech-text').css({
					'background-image': 'url('+preload.getResult('textbox').src+')',
					'background-size': '100% 100%',
				});
				var sound_arr_orig = [
										['s_a','s_beautiful','s_temple'],
										['s_a','s_brown','s_temple'],
										['s_a','s_nice','s_temple'],
										['s_a','s_lovely','s_temple'],
										['s_an','s_old','s_temple'],
										['s_a','s_yellow','s_temple']
									];
				var sound_arr_orig_2 = [
										['s_a1','s_red','s_apple'],
										['s_a1','s_juicy','s_apple'],
										['s_a1','s_round','s_apple'],
										['s_a1','s_small','s_apple'],
										['s_a1','s_tasty','s_apple'],
										['s_a1','s_crunchy','s_apple']
									];
				var sound_arr = [];
				var new_count = 0;
				for(var i=0; i<choice_idx_arr.length; i++){
					if(choice_idx_arr[i]==1){
						var current_p = '';
						if(countNext==4){
							current_p = '<p>This is '+choice_arr_2[i]+' apple.</p>';
							sound_arr.push(sound_arr_orig_2[i]);
						} else{
							current_p = '<p>This is '+choice_arr_1[i]+' temple.</p>';
							sound_arr.push(sound_arr_orig[i]);
						}
						$('.speech-text').append(current_p);
						new_count++;
					} else{
					}
				}
				timeoutvar = setTimeout(function(){
					count_player('.speech-text', 0, sound_arr);
				}, 1000);
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function count_player(classname, counter, sound_data){
		var no_of_children = $(classname).children().length;
		$(classname).children().eq(counter).addClass('fade-in-fast');
		current_sound = createjs.Sound.play(sound_data[counter][0]);
		console.log(sound_data);
		console.log(sound_data[counter][0]);
		current_sound.play();
		current_sound.on("complete", function(){
			current_sound = createjs.Sound.play(sound_data[counter][1]);
			current_sound.play();
			current_sound.on("complete", function(){
				current_sound = createjs.Sound.play(sound_data[counter][2]);
				current_sound.play();
				current_sound.on("complete", function(){
					counter++;
					if(counter<no_of_children) {
						count_player(classname, counter, sound_data);
					} else{
						nav_button_controls(0);
					}
				});
			});
		});
	}
	
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	}); 

});

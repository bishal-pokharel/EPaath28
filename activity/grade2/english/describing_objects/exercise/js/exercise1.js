var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content = [
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q1,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-1a',
		optimg2: 'im-1b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans1_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans1_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans1_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans1_d,
			}
		],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q2,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-2a',
		optimg2: 'im-2b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans2_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans2_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans2_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans2_d,
			}
		],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q3,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-3a',
		optimg2: 'im-3b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans3_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans3_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans3_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans3_d,
			}
		],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q4,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-4a',
		optimg2: 'im-4b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans4_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans4_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans4_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans4_d,
			}
		],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q5,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-5a',
		optimg2: 'im-5b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans5_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans5_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans5_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans5_d,
			}
		],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q6,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-6a',
		optimg2: 'im-6b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans6_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans6_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans6_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans6_d,
			}
		],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q7,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-7a',
		optimg2: 'im-7b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans7_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans7_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans7_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans7_d,
			}
		],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',

		instructiondata: data.string.eins,
		questiondata: data.string.e1_q8,
        datahighlightflag: true,
        datahighlightcustomclass: "subtextclr",

		optimg1: 'im-8a',
		optimg2: 'im-8b',

		exeoptions: [
			{
				optclass: "class1",
				optdata: data.string.e1_ans8_a,
			},
			{
				optclass: "class2",
				optdata: data.string.e1_ans8_b,
			},
			{
				optclass: "class3",
				optdata: data.string.e1_ans8_c,
			},
			{
				optclass: "class4",
				optdata: data.string.e1_ans8_d,
			}
		],
	}
];
content.shufflearray();

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = 8;//content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new EggTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "cloud", src: imgpath+'cloud.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-1a", src: imgpath+'01a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-1b", src: imgpath+'01b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-2a", src: imgpath+'02a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-2b", src: imgpath+'02b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-3a", src: imgpath+'03a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-3b", src: imgpath+'03b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-4a", src: imgpath+'04a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-4b", src: imgpath+'04b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-5a", src: imgpath+'05a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-5b", src: imgpath+'05b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-6a", src: imgpath+'06a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-6b", src: imgpath+'06b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-7a", src: imgpath+'07a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-7b", src: imgpath+'07b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "im-8a", src: imgpath+'08a.png', type: createjs.AbstractLoader.IMAGE},
			{id: "im-8b", src: imgpath+'08b.png', type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"exercise.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
        texthighlight($board);
        if(countNext<$total_page){
				countNext==0?sound_player("sound_1"):"";
			var option_position = [1,2,3,4];
			option_position.shufflearray();
			for(var op=0; op<4; op++){
				$('.optionscontainer').eq(op).addClass('opt-position-'+option_position[op]);
			}
			$('#num_ques').html(parseInt(countNext+1)+'. ');
			$('.optionscontainer').css({
				'background-image': 'url('+preload.getResult('cloud').src+')',
				'background-size': '100% 100%'
			});
			$('.ex-image-1').attr('src', preload.getResult(content[countNext].optimg1).src);
			$('.ex-image-2').attr('src', preload.getResult(content[countNext].optimg2).src);
			$('.rightwrong').attr('src', preload.getResult('incorrect').src);
		} else{
			createjs.Sound.stop();
			$('.contentblock').hide(0);
		}

		var wrong_clicked = 0;
		var item_count = 0;
		$('.optionscontainer').draggable({
			containment: ".board",
			cursor: "move",
			revert: true,
			appendTo: "body",
			zIndex: 50,
			start: function( event, ui ){
			},
			stop: function( event, ui ){
			}
		});
		$('.blank-space').droppable({
			hoverClass: "blank-hover",
			drop:function(event, ui) {
				var dropped = ui.draggable;
				var droppedOn = $(this);
				if(dropped.hasClass('class1')){
					dropped.draggable('option', 'revert', false);
					play_correct_incorrect_sound(1);
					dropped.draggable('disable');
					dropped.detach().css({
						'position': 'relative',
						'bottom': 'auto',
						'top': 'auto',
						'height': '100%',
						'left': '0%',
						'width': '100%',
						'border': 'none',
						'background': 'none',
						'color': 'white',
					}).appendTo(droppedOn);
					dropped.children('p').css({
						'top': '50%',
					});
					$('.blank-data').html('');
					$(droppedOn).removeClass('incorrect-box');
					$(droppedOn).addClass('correct-box');
					if(wrong_clicked<1){
						scoring.update(true);
					}
					$nextBtn.show(0);
					$('.optionscontainer').css({
						'pointer-events':'none'
					});
					$('.rightwrong').css('opacity', '1');
					$('.rightwrong').attr('src', preload.getResult('correct').src);
				} else{
					play_correct_incorrect_sound(0);
					$(droppedOn).addClass('incorrect-box');
					$('.blank-data').html($(dropped).children('p').html());
					dropped.css({
						'pointer-events': 'none',
						'filter': 'invert(100%)',
						'-webkit-filter': 'invert(100%)',
					});
					scoring.update(false);
		 			play_correct_incorrect_sound(0);
					$('.rightwrong').css('opacity', '1');
				}
			}
		});
		$prevBtn.hide(0);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});



    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_thiss = new buzz.sound((soundAsset + "p1_0.ogg"));
var sound_this = new buzz.sound((soundAsset + "p2_7.ogg"));
var sound_that = new buzz.sound((soundAsset + "p2_10.ogg"));

var sound_ask = new buzz.sound((soundAsset + "p2_6.ogg"));

var sound_whatthis = new buzz.sound((soundAsset + "s2_p6_1.ogg"));
var sound_whatthat = new buzz.sound((soundAsset + "s2_p6_2.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p1_11.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_3.ogg"));

var sound_this_bottle = new buzz.sound((soundAsset + "p2_1.ogg"));
var sound_this_ball = new buzz.sound((soundAsset + "p2_2.ogg"));
var sound_that_table = new buzz.sound((soundAsset + "p2_4.ogg"));
var sound_that_sock = new buzz.sound((soundAsset + "p2_5.ogg"));

var global_var_sound = sound_this;


//sound_ques is used due to same bind event gets triggered in two different functions=> sound_caller and play_qna
var sound_ques = new buzz.sound((soundAsset + "what_is_preeti_doing.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'bottle',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p2text1,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped',
					imgsrc: imgpath + "asha-2.png",
				},
				{
					imgclass:'bottle',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p2text2,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'bottle',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'bottle2 animation_bottle',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table2 animation_table',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball2 animation_ball',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock2 animation_sock',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango2 animation_mango',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p2text3,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped',
					imgsrc: imgpath + "asha-3.png",
				},
				{
					imgclass:'bottle2',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table2',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball2',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock2',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango2',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p2text4,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl',
					imgsrc: imgpath + "asha-3.png",
				},
				{
					imgclass:'bottle2',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table2',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'ball2',
					imgsrc: imgpath + "ball.png",
				},
				{
					imgclass:'sock2',
					imgsrc: imgpath + "sock.png",
				},
				{
					imgclass:'mango2',
					imgsrc: imgpath + "mango.png",
				},
			]
			},
		]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblockadditionalclass: 'weepy-background',
		contentblocknocenteradjust : true,

		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [
			{
				textdata : data.string.p2text5,
				textclass : 'lesson_intro_2',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_blue'
			}
		],
		lowertextblockadditionalclass: 'intro_block_r',
		lowertextblock : [
			{
				textdata : data.string.p2text6,
				textclass : 'lesson_intro_2',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [{
				imgclass:'angel',
				imgsrc: "images/fairy/fairy_fly.gif",
			},{
				imgclass: 'bg_full',
				imgsrc: 'images/backgrounds/bg_images/tree_hill_cloud.png'
			}],
			imagelabels : [
				{
					imagelabelclass : "header_text pangolin_white pangolin",
					imagelabeldata : data.string.chap
				}]
			},
		]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_1',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_1',
					imgsrc: imgpath + "duck-2.png",
				},
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_2',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_2',
					imgsrc: imgpath + "duck-1.png",
				},
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		uppertextblockadditionalclass: 'intro_block_middle',
		uppertextblock : [
			{
				textdata : data.string.p2text7,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_purple'
			}
		],
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_3',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_3',
					imgsrc: imgpath + "duck-3.png",
				},
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_4',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_4',
					imgsrc: imgpath + "duck-2.png",
				},
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_4',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_4',
					imgsrc: imgpath + "duck-1.png",
				},
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg-purple',
		uppertextblockadditionalclass: 'intro_block_middle',
		uppertextblock : [
			{
				textdata : data.string.p2text8,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_purple'
			}
		],
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1 speech_pos_4',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'squirrel sq_position_4',
					imgsrc: imgpath + "duck-1.png",
				},,
				{
					imgclass:'bottle3',
					imgsrc: imgpath + "bottle.png",
				},
				{
					imgclass:'table3',
					imgsrc: imgpath + "table.png",
				},
				{
					imgclass:'guitar',
					imgsrc: imgpath + "guitar.png",
				},
				{
					imgclass:'lamp',
					imgsrc: imgpath + "lamp.png",
				},
				{
					imgclass:'umbrella',
					imgsrc: imgpath + "umbrella.png",
				},
				{
					imgclass:'chair',
					imgsrc: imgpath + "chair.png",
				}
			]
			},
		]
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'weepy-background',

		uppertextblockadditionalclass: 'intro_block_l a',
		uppertextblock : [
			{
				textdata : data.string.p2text9,
				textclass : 'lesson_intro_2 my_font_medium ',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_blue'
			},
		],
		lowtxtblockadditionalclass:"newdiv",
		lowtxtblock:[
			{
				textdata : data.string.p2text11,
				textclass : 'lesson_intro_2 my_font_medium b',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_blue'
			}
		],
		lowertextblockadditionalclass: 'intro_block_r',
		lowertextblock : [
			{
				textdata : data.string.p2text10,
				textclass : 'lesson_intro_2 my_font_medium' ,
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}],
			lefttextblockadditionalclass:"newdiv1",
			lefttextblock:[
			{
				textdata : data.string.p2text12,
				textclass : 'lesson_intro_2 my_font_medium',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblock : [{
			imagestoshow : [{
				imgclass:'angel',
				imgsrc: "images/fairy/fairy_fly.gif",
			},{
				imgclass: 'bg_full',
				imgsrc: 'images/backgrounds/bg_images/tree_hill_cloud.png'
			}],
			imagelabels : [
				{
					imagelabelclass : "header_text pangolin_white pangolin",
					imagelabeldata : data.string.summary
				}]
			},
		]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
			$prevBtn.hide(0);
			break;
		case 1:
			play_sound(sound_this_bottle, '.speech_1','blink_and_big', '.bottle', 'highlightobject');
			sound_caller(sound_this_bottle, '.speech_1', 'blink_and_big', '.bottle', 'highlightobject', 'speech_1');
			break;
		case 2:
			play_sound(sound_this_ball, '.speech_1','blink_and_big', '.ball', 'highlightobject');
			sound_caller(sound_this_ball, '.speech_1', 'blink_and_big', '.ball', 'highlightobject', 'speech_1');
			break;
		case 3:
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			setTimeout(function(){
				$nextBtn.show(0);
				$prevBtn.show(0);
			}, 3000);
			break;
		case 4:
			play_sound(sound_that_table, '.speech_1', 'blink_and_big', '.table2', 'highlightobject');
			sound_caller(sound_that_table, '.speech_1', 'blink_and_big', '.table2', 'highlightobject', 'speech_1');
			break;
		case 5:
			play_sound(sound_that_sock, '.speech_1', 'blink_and_big', '.sock2', 'highlightobject');
			sound_caller(sound_that_sock, '.speech_1', 'blink_and_big', '.sock2', 'highlightobject', 'speech_1');
			break;
		case 6:
			play_audio_nav( sound_thiss );
			setTimeout(function(){
				play_sound(sound_whatthis, '.speech_1', 'blink_and_big', '.intro_block_l', 'highlightobject');
				sound_caller(sound_whatthis, '.speech_1', 'blink_and_big', '.intro_block_l', 'highlightobject', 'speech_1');
				setTimeout(function(){
					play_sound(sound_whatthat, '.speech_1', 'blink_and_big', '.intro_block_r', 'highlightobject');
					sound_caller(sound_whatthat, '.speech_1', 'blink_and_big', '.intro_block_r', 'highlightobject', 'speech_1');
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					},5000);
				},6000);
					},3500);
			break;
		case 7:
			play_sound(sound_this, '.speech_1', 'blink_and_big', '.chair', 'highlightobject');
			sound_caller(sound_this, '.speech_1', 'blink_and_big', '.chair', 'highlightobject', 'speech_1');
			break;
		case 8:
			play_sound(sound_this, '.speech_1', 'blink_and_big', '.lamp', 'highlightobject');
			sound_caller(sound_this, '.speech_1', 'blink_and_big', '.lamp', 'highlightobject', 'speech_1');
			break;
		case 9:
			play_sound(sound_this, '.speech_1', 'blink_and_big', '.umbrella', 'highlightobject');
			sound_caller(sound_this, '.speech_1', 'blink_and_big', '.umbrella', 'highlightobject', 'speech_1');
			setTimeout(function(){
				$(".intro_block_middle").fadeIn();
				play_sound(sound_whatthis, '', 'blink_and_big', '.intro_block_middle', 'highlightobject');
				setTimeout(function(){
					$nextBtn.show(0);
					$prevBtn.show(0);
				},6000);
			},3000);
			break;
		case 10:
			play_sound(sound_that, '.speech_1', 'blink_and_big', '.table3', 'highlightobject');
			sound_caller(sound_that, '.speech_1', 'blink_and_big', '.table3', 'highlightobject', 'speech_1');
			break;
		case 11:
			play_sound(sound_that, '.speech_1', 'blink_and_big', '.guitar', 'highlightobject');
			sound_caller(sound_that, '.speech_1', 'blink_and_big', '.guitar', 'highlightobject', 'speech_1');
			break;
		case 12:
			play_sound(sound_that, '.speech_1', 'blink_and_big', '.bottle3', 'highlightobject');
			sound_caller(sound_that, '.speech_1', 'blink_and_big', '.bottle3', 'highlightobject', 'speech_1');
			setTimeout(function(){
				$(".intro_block_middle").fadeIn();
				play_sound(sound_whatthat, '', 'blink_and_big', '.intro_block_middle', 'highlightobject');
				setTimeout(function(){
					$nextBtn.show(0);
					$prevBtn.show(0);
				},6000);
			},3000);
			break;
		case 13:
			// setTimeout(function(){
			// 	ole.footerNotificationHandler.pageEndSetNotification();
			// },3000);
			setTimeout(function(){
				play_sound(sound_1, '', 'blink_and_big', '.intro_block_l', 'highlightobject');
				setTimeout(function(){
					play_sound(sound_2, '', 'blink_and_big', '.intro_block_r', 'highlightobject');
					setTimeout(function(){
						play_sound(sound_whatthis, '', 'blink_and_big', '.newdiv', 'highlightobject');
						setTimeout(function(){
							play_sound(sound_whatthat, '', 'blink_and_big', '.newdiv1', 'highlightobject');
							setTimeout(function(){
								$prevBtn.show(0);
								ole.footerNotificationHandler.pageEndSetNotification();
							},5000);
					},6000);
				},5000);
				},6000);
			},1000);
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		global_var_sound.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		global_var_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	function play_audio( var_sound ){
		global_var_sound.stop();
		global_var_sound = var_sound;
		global_var_sound.play();
	}
	function play_audio_nav( var_sound ){
		global_var_sound.stop();
		global_var_sound = var_sound;
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			$nextBtn.show(0);
		});
	}

	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	function sound_caller(sound_var, sound_box_class, sound_box_animation, highlight_class, highlight_animation , common_class){
		$(sound_box_class).click(function(){
			global_var_sound.stop();
			global_var_sound = sound_var;
			global_var_sound.play();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$(common_class).css('pointer-events', 'none');
			$(sound_box_class).addClass(sound_box_animation);
			$(highlight_class).addClass(highlight_animation);
			global_var_sound.bindOnce('ended', function(){
				$(highlight_class).removeClass(highlight_animation);
				$(sound_box_class).removeClass(sound_box_animation);
				$(common_class).css('pointer-events', 'all');
				if(countNext == content.length-1)
				{
					// ole.footerNotificationHandler.pageEndSetNotification();
					if(countNext != 0){
						// $prevBtn.show(0);
					}
				} else if(countNext==9 || countNext==12 || countNext==6){
					$nextBtn.hide(0);
					$prevBtn.hide(0);
					} else{
					$nextBtn.show(0);
					if(countNext != 0){
						$prevBtn.show(0);
					}
				}
			});
		});
	}
	function play_sound(sound_class_text, text_class, text_animation, highlight_class, highlight_animation){
		global_var_sound.stop();
		global_var_sound = sound_class_text;
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(sound_class_text).css('pointer-events', 'none');
		$(text_class).show(0);
		$(text_class).addClass(text_animation);
		$(highlight_class).addClass(highlight_animation);
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			$(text_class).removeClass(text_animation);
			if(countNext == content.length-1)
			{
				// ole.footerNotificationHandler.pageEndSetNotification();
				if(countNext != 0){
						// $prevBtn.show(0);
					}
			} else if(countNext==9 || countNext==12 || countNext==6){
				$nextBtn.hide(0);
				$prevBtn.hide(0);

				} else{
				$nextBtn.show(0);
				if(countNext != 0){
						$prevBtn.show(0);
					}
			}
			$(highlight_class).removeClass(highlight_animation);
			$(sound_class_text).css('pointer-events', 'all');
		});
	}
	function play_qna(sound_class_ques, sound_class_text, ques_class, text_class, ques_animation, text_animation, common_class){
		global_var_sound.stop();
		global_var_sound = sound_class_ques;
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(common_class).css('pointer-events', 'none');
		$(ques_class).addClass(ques_animation);
		$(text_class).hide(0);
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			$(text_class).show(0);
			$(ques_class).removeClass(ques_animation);
			$(text_class).addClass(text_animation);
			global_var_sound = sound_class_text;
			global_var_sound.play();
			global_var_sound.bindOnce('ended', function(){
				$(text_class).removeClass(text_animation);
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}

			$(common_class).css('pointer-events', 'all');
			});
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

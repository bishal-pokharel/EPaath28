var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_title = new buzz.sound((soundAsset + "p1_0.ogg"));

var sound_intro = new buzz.sound((soundAsset + "p1_1.ogg"));
var sound_intro_2 = new buzz.sound((soundAsset + "p1_3.ogg"));


var sound_this = new buzz.sound((soundAsset + "p1_11.ogg"));
var sound_that = new buzz.sound((soundAsset + "p1_12.ogg"));

var sound_this_bench = new buzz.sound((soundAsset + "p1_2.ogg"));
var sound_that_tree = new buzz.sound((soundAsset + "p1_4.ogg"));
var sound_that_apple = new buzz.sound((soundAsset + "p1_6.ogg"));
var sound_this_flower = new buzz.sound((soundAsset + "p1_5.ogg"));
var sound_this_butterfly = new buzz.sound((soundAsset + "p1_7.ogg"));
var sound_this_balloon = new buzz.sound((soundAsset + "p1_9.ogg"));
var sound_that_balloon = new buzz.sound((soundAsset + "p1_10.ogg"));
var sound_that_bird = new buzz.sound((soundAsset + "p1_8.ogg"));


//sound_ques is used due to same bind event gets triggered in two different functions=> sound_caller and play_qna
var sound_ques = new buzz.sound((soundAsset + "what_is_preeti_doing.ogg"));

var global_var_sound = sound_this;

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'firstpagebg',
		uppertextblockadditionalclass: '',
		uppertextblock : [{
			textdata : data.string.chapter,
			textclass : 'lesson_title'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'piggy',
					imgsrc: imgpath + "pig.gif",
				}
			]
			},
		]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'lesson_intro',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_blue'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'position_1 girl_flipped',
					imgsrc: imgpath + "asha-0.png",
				}
			]
			},
		]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'lesson_intro',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_blue'
		}],
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text2,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
			]
			},
		]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [
			{
				textdata : data.string.p1text1,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_blue'
			}
		],
		lowertextblockadditionalclass: 'intro_block_r',
		lowertextblock : [
			{
				textdata : data.string.p1text3,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
			]
			},
		]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [
			{
				textdata : data.string.p1text1,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_blue'
			}
		],
		lowertextblockadditionalclass: 'intro_block_r',
		lowertextblock : [
			{
				textdata : data.string.p1text3,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_red'
			}
		],
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text4,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-2.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
			]
			},
		]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text5,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_in_hand',
					imgsrc: imgpath + "flower.png",
				},
			]
			},
		]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text6,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-2.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
			]
			},
		]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text7,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'butterfly butterfly_move',
					imgsrc: imgpath + "butterfly.gif",
				},

			]
			},
		]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text8,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-1.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			]
			},
		]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text9,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'balloon_in_hand vibrate',
					imgsrc: imgpath + "balloon.png",
				}
			]
			},
		]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_1',
			speechcontent: data.string.p1text11,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-1.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'balloon_in_tree vibrate',
					imgsrc: imgpath + "balloon.png",
				}
			]
			},
		]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [
			{
				textdata : data.string.p1text12,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_purple'
			}
		],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'balloon_in_tree vibrate',
					imgsrc: imgpath + "balloon.png",
				}
			]
			},
		]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		uppertextblockadditionalclass: 'intro_block_l',
		uppertextblock : [
			{
				textdata : data.string.p1text12,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_purple'
			}
		],
		lowertextblockadditionalclass: 'intro_block_r',
		lowertextblock : [
			{
				textdata : data.string.p1text13,
				textclass : 'lesson_intro',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_purple'
			}
		],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'balloon_in_tree vibrate',
					imgsrc: imgpath + "balloon.png",
				}
			]
			},
		]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			// $nextBtn.hide(0);
			play_audio_nav( sound_title );
			break;
		case 1:
			play_audio_nav( sound_intro );
			$prevBtn.show(0);
			break;
		case 2:
			play_sound(sound_this_bench, '.speech_1','blink_and_big', '.bench', 'highlightobject');
			sound_caller(sound_this_bench, '.speech_1', 'blink_and_big', '.bench', 'highlightobject', 'speech_1');
			break;
		case 3:
			play_audio_nav( sound_intro_2 );
			$prevBtn.show(0);
			break;
		case 4:
			play_sound(sound_that_tree, '.speech_1', 'blink_and_big', '.tree', 'highlightobject');
			sound_caller(sound_that_tree, '.speech_1', 'blink_and_big', '.tree', 'highlightobject', 'speech_1');
			break;
		case 5:
			play_sound(sound_this_flower, '.speech_1', 'blink_and_big', '.flower_in_hand', 'highlightobject');
			sound_caller(sound_this_flower, '.speech_1', 'blink_and_big', '.flower_in_hand', 'highlightobject', 'speech_1');
			break;
		case 6:
			play_sound(sound_that_apple, '.speech_1', 'blink_and_big', '.apple', 'highlightobject');
			sound_caller(sound_that_apple, '.speech_1', 'blink_and_big', '.apple', 'highlightobject', 'speech_1');
			break;
		case 7:
			$('.speech_1').hide(0);
			setTimeout(function(){
				$('.speech_1').show(0);
				$('.butterfly').removeClass('butterfly_move');
				$('.butterfly').addClass('butterfly2');
				play_sound(sound_this_butterfly, '.speech_1', 'blink_and_big', '.butterfly', 'highlightbutterfly');
				sound_caller(sound_this_butterfly, '.speech_1', 'blink_and_big', '.butterfly', 'highlightbutterfly', 'speech_1');
			}, 6000);
			break;
		case 8:
			play_sound(sound_that_bird, '.speech_1', 'blink_and_big', '.bird', 'highlightobject');
			sound_caller(sound_that_bird, '.speech_1', 'blink_and_big', '.bird', 'highlightobject', 'speech_1');
			break;
		case 9:
			play_sound(sound_this_balloon, '.speech_1', 'blink_and_big', '.balloon_in_hand', 'highlightballoon');
			sound_caller(sound_this_balloon, '.speech_1', 'blink_and_big', '.balloon_in_hand', 'highlightballoon', 'speech_1');
			break;
		case 10:
			play_sound(sound_that_balloon, '.speech_1', 'blink_and_big', '.balloon_in_tree', 'highlightballoon');
			sound_caller(sound_that_balloon, '.speech_1', 'blink_and_big', '.balloon_in_tree', 'highlightballoon', 'speech_1');
			break;
		case 11:
			play_audio_nav( sound_this );
			$prevBtn.show(0);
			break;
		case 12:
			$nextBtn.hide(0);
			play_audio_nav( sound_that );
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		if(countNext == 7){
			global_var_sound.stop();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$('.speech_1').hide(0);
			$('.butterfly').removeClass('butterfly2');
			$('.butterfly').addClass('butterfly_move2');
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 4000);
		} else if(countNext == 9){
			global_var_sound.stop();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			// $('.speech_1>label').html(data.string.p1text10);
			$('.balloon_in_hand').removeClass('vibrate');
			$('.balloon_in_hand').addClass('balloon_move');
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 4500);
		} else {
			global_var_sound.stop();
			countNext++;
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		global_var_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

	function play_audio( var_sound ){
		global_var_sound.stop();
		global_var_sound = var_sound;
		global_var_sound.play();
	}
	function play_audio_nav( var_sound ){
		global_var_sound.stop();
		global_var_sound = var_sound;
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			if(countNext == content.length-1){
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$nextBtn.show(0);
			}
		});
	}

	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	function sound_caller(sound_var, sound_box_class, sound_box_animation, highlight_class, highlight_animation , common_class){
		$(sound_box_class).click(function(){
			global_var_sound.stop();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$(common_class).css('pointer-events', 'none');
			$(sound_box_class).addClass(sound_box_animation);
			$(highlight_class).addClass(highlight_animation);
			global_var_sound = sound_var;
			global_var_sound.play();
			global_var_sound.bindOnce('ended', function(){
				$(highlight_class).removeClass(highlight_animation);
				$(sound_box_class).removeClass(sound_box_animation);
				$(common_class).css('pointer-events', 'all');
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}
			});
		});
	}
	function play_sound(sound_class_text, text_class, text_animation, highlight_class, highlight_animation){
		global_var_sound.stop();
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(sound_class_text).css('pointer-events', 'none');
		$(text_class).show(0);
		$(text_class).addClass(text_animation);
		$(highlight_class).addClass(highlight_animation);
		global_var_sound = sound_class_text;
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			$(text_class).removeClass(text_animation);
			if(countNext == content.length-1)
			{
				ole.footerNotificationHandler.pageEndSetNotification();
				$prevBtn.show(0);
			} else {
				$nextBtn.show(0);
				$prevBtn.show(0);
			}
			$(highlight_class).removeClass(highlight_animation);
			$(sound_class_text).css('pointer-events', 'all');
		});
	}
	function play_qna(sound_class_ques, sound_class_text, ques_class, text_class, ques_animation, text_animation, common_class){
		global_var_sound.stop();
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(common_class).css('pointer-events', 'none');
		$(ques_class).addClass(ques_animation);
		$(text_class).hide(0);
		global_var_sound = sound_class_ques;
		global_var_sound.play();
		global_var_sound.bindOnce('ended', function(){
			$(text_class).show(0);
			$(ques_class).removeClass(ques_animation);
			$(text_class).addClass(text_animation);
			global_var_sound = sound_class_text;
			global_var_sound.play();
			global_var_sound.bindOnce('ended', function(){
				$(text_class).removeClass(text_animation);
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}

			$(common_class).css('pointer-events', 'all');
			});
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var p1_audio = new buzz.sound((soundAsset + "s3_p1.ogg"));
var sound_this = new buzz.sound((soundAsset + "p3_1.ogg"));
var sound_that = new buzz.sound((soundAsset + "p3_12.ogg"));

var sound_this_bench = new buzz.sound((soundAsset + "p3_4.ogg"));
var sound_that_tree = new buzz.sound((soundAsset + "p3_3.ogg"));
var sound_that_apple = new buzz.sound((soundAsset + "p3_2.ogg"));
var sound_this_flower = new buzz.sound((soundAsset + "p3_7.ogg"));
var sound_that_butterfly = new buzz.sound((soundAsset + "p3_6.ogg"));
// var sound_this_butterfly = new buzz.sound((soundAsset + "this_butterfly.mp3"));
var sound_that_bird = new buzz.sound((soundAsset + "p3_5.ogg"));
var sound_that_restro = new buzz.sound((soundAsset + "p3_9.ogg"));

var sound_hungry = new buzz.sound((soundAsset + "p3_10.ogg"));
var sound_let_eat = new buzz.sound((soundAsset + "p3_11.ogg"));


//sound_ques is used due to same bind event gets triggered in two different functions=> sound_caller and play_qna
var sound_ques = new buzz.sound((soundAsset + "what_is_preeti_doing.ogg"));

// var sound_group_b = [sound_ques, sound_3_00, sound_5_30, sound_6_00, sound_7_00, sound_8_30, sound_12_30];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		additionalclasscontentblock : '',
		uppertextblockadditionalclass: 'intro_block_middle',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'lesson_intro',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_blue'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'granny_intro',
					imgsrc: imgpath + "granny.png",
				}
			]
			},
		]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [{
			speechboxclass : 'speech_div speech_2 granny_text_1',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			],
			},
		],
		spriteblockadditionalclass: "g_position_1 comes_in",
		spriteblock : [
		{
			imagelabelclass : "granny walking",
			imagelabeldata : ''
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_2 granny_text_1',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text3,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-3.png",
				},
				{
					imgclass:'hand hand_left',
					imgsrc: imgpath + "hand.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			],
			},
		],
		spriteblockadditionalclass: "g_position_2",
		spriteblock : [
		{
			imagelabelclass : "granny",
			imagelabeldata : ''
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_2 granny_text_1',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text4,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-3.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			]
			},
		],
		spriteblockadditionalclass: "g_position_2",
		spriteblock : [
		{
			imagelabelclass : "granny",
			imagelabeldata : ''
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_2 granny_text_1',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text5,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			]
			},
		],
		spriteblockadditionalclass: "g_position_2",
		spriteblock : [
		{
			imagelabelclass : "granny",
			imagelabeldata : ''
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_2',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text6,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-1.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				}
			]
			},
		],
		spriteblockadditionalclass: "g_position_2 comes_middle",
		spriteblock : [
		{
			imagelabelclass : "granny walking",
			imagelabeldata : ''
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_2',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text7,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl position_1',
					imgsrc: imgpath + "asha-3.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'butterfly butterfly_move',
					imgsrc: imgpath + "butterfly.gif",
				},
			],
			},
		],
		spriteblockadditionalclass: "g_position_3",
		spriteblock : [
		{
			imagelabelclass : "granny",
			imagelabeldata : ''
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_3',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text8,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot flowers',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot flowers',
					imgsrc: imgpath + "flower.png",
				}
			]
			},
		],
		spriteblockadditionalclass: "g_position_3 comes_bench",
		spriteblock : [
		{
			imagelabelclass : "granny walking",
			imagelabeldata : ''
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_3',
			speechcontent: data.string.pthis,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text9,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-5.png",
				},
				{
					imgclass:'tree',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot flowers',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot flowers',
					imgsrc: imgpath + "flower.png",
				}
			],
			},
		],
		spriteblockadditionalclass: "g_position_4",
		spriteblock : [
		{
			imagelabelclass : "granny",
			imagelabeldata : ''
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main_2 bg_move',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_3',
			speechcontent: data.string.pthat,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text10,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'tree tree_move',
					imgsrc: imgpath + "tree.png",
				},
				{
					imgclass:'bench bench_move',
					imgsrc: imgpath + "bench.png",
				},
				{
					imgclass:'apple apple_move',
					imgsrc: imgpath + "apple.png",
				},
				{
					imgclass:'bird bird_move',
					imgsrc: imgpath + "bird02.png",
				},
				{
					imgclass:'flower_pot flowers pot_move',
					imgsrc: imgpath + "flowerpot.png",
				},
				{
					imgclass:'flower_in_pot flowers flower_move',
					imgsrc: imgpath + "flower.png",
				},
				{
					imgclass:'restro restro_move',
					imgsrc: imgpath + "restro.png",
				}
			],
			},
		],
		spriteblockadditionalclass: "g_position_5",
		spriteblock : [
			{
				imagelabelclass : "granny walking",
				imagelabeldata : ''
			},
			],
		spriteblock2additionalclass: "position_1",
		spriteblock2 : [
			{
				imagelabelclass : "girl_sprite walking_2",
				imagelabeldata : ''
			}]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,

		contentblockadditionalclass: 'bg_main_2 bg_move',
		speechboxblockadditionalclass: '',
		speechboxblock : [
		{
			speechboxclass : 'speech_div speech_1 granny_text_3',
			speechcontent: data.string.p3text11,
			speechcontentclass: 'speech_content_1'
		},
		{
			speechboxclass : 'speech_div speech_1 girl_text_1',
			speechcontent: data.string.p3text12,
			speechcontentclass: 'speech_content_1'
		}],
		imageblockadditionalclass: '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass:'girl_flipped position_1',
					imgsrc: imgpath + "asha-0.png",
				},
				{
					imgclass:'restro restro_1',
					imgsrc: imgpath + "restro.png",
				}
			],
			},
		],
		spriteblockadditionalclass: "g_position_5",
		spriteblock : [
			{
				imagelabelclass : "granny",
				imagelabeldata : ''
			}]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			$prevBtn.hide(0);
			$nextBtn.hide(0);
			p1_audio.play();
			p1_audio.bindOnce('ended', function(){
					$nextBtn.show(0);
				});
		break;
		case 1:
			$('.speech_2').hide(0);
			setTimeout(function(){
				$('.speech_2').show(0);
				$('.granny').removeClass('walking');
				play_sound(sound_this, '.granny_text_1', 'blink_and_big', '.apple', 'highlightobject', '.speech_div');
				sound_caller(sound_this, '.granny_text_1', 'blink_and_big', '.apple', 'highlightobject', '.speech_div');
			}, 3000);
			break;
		case 2:
			play_sound(sound_that_apple, '.girl_text_1', 'blink_and_big', '.apple', 'highlightobject', '.speech_div');
			sound_caller(sound_that_apple, '.girl_text_1', 'blink_and_big', '.apple', 'highlightobject', '.speech_div');
			sound_caller(sound_this, '.granny_text_1', 'blink_and_big', '.apple', 'highlightobject', '.speech_div');
			break;
		case 3:
			play_qna(sound_this, sound_that_tree, '.granny_text_1', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.tree', 'highlightobject', '.speech_div');
			sound_caller(sound_that_tree, '.girl_text_1', 'blink_and_big', '.tree', 'highlightobject', '.speech_div');
			sound_caller(sound_this, '.granny_text_1', 'blink_and_big', '.tree', 'highlightobject', '.speech_div');
			break;
		case 4:
			play_qna(sound_that, sound_this_bench, '.granny_text_1', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			sound_caller(sound_this_bench, '.girl_text_1', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			sound_caller(sound_that, '.granny_text_1', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			break;
		case 5:
			$('.speech_div').hide(0);
			setTimeout(function(){
				$('.granny').removeClass('walking');
				$('.granny_text_2').show(0);
				play_qna(sound_that, sound_that_bird, '.granny_text_2', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.bird', 'highlightobject', '.speech_div');
				sound_caller(sound_that_bird, '.girl_text_1', 'blink_and_big', '.bird', 'highlightobject', '.speech_div');
				sound_caller(sound_that, '.granny_text_2', 'blink_and_big', '.bird', 'highlightobject', '.speech_div');
			},6000);
			break;
		case 6:
			$('.speech_div').hide(0);
			setTimeout(function(){
				$('.granny_text_2').show(0);
				$('.butterfly').removeClass('butterfly_move');
				$('.butterfly').addClass('butterfly2');
				play_qna(sound_that, sound_that_butterfly, '.granny_text_2', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.butterfly', 'highlightbutterfly', '.speech_div');
				sound_caller(sound_that_butterfly, '.girl_text_1', 'blink_and_big', '.butterfly', 'highlightbutterfly', '.speech_div');
				sound_caller(sound_that, '.granny_text_2', 'blink_and_big', '.butterfly', 'highlightbutterfly', '.speech_div');
			},1600);
			break;
		case 7:
			$('.speech_div').hide(0);
			setTimeout(function(){
				$('.granny').removeClass('walking');
				$('.granny_text_3').show(0);
				play_qna(sound_this, sound_this_flower, '.granny_text_3', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.flowers', 'highlightobject', '.speech_div');
				sound_caller(sound_this_flower, '.girl_text_1', 'blink_and_big', '.flowers', 'highlightobject', '.speech_div');
				sound_caller(sound_this, '.granny_text_3', 'blink_and_big', '.flowers', 'highlightobject', '.speech_div');
			},3000);
			break;
		case 8:
			play_qna(sound_this, sound_this_bench, '.granny_text_3', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			sound_caller(sound_this_bench, '.girl_text_1', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			sound_caller(sound_this, '.granny_text_3', 'blink_and_big', '.bench', 'highlightobject', '.speech_div');
			break;
		case 9:
			$prevBtn.hide(0);
			$('.speech_div').hide(0);
			setTimeout(function(){
				$('.granny').removeClass('walking');
				$('.girl_sprite').removeClass('walking_2');
				$('.granny_text_3').show(0);
				$('.restro').removeClass('restro_move');
				$('.restro').addClass('restro_1');
				play_qna(sound_that, sound_that_restro, '.granny_text_3', '.girl_text_1', 'blink_and_big', 'blink_and_big', '.restro', 'highlightobject', '.speech_div');
				sound_caller(sound_that_restro, '.girl_text_1', 'blink_and_big', '.restro', 'highlightobject', '.speech_div');
				sound_caller(sound_that, '.granny_text_3', 'blink_and_big', '.restro', 'highlightobject', '.speech_div');
			},6000);
			break;
		case 10:
			play_qna(sound_hungry, sound_let_eat, '.granny_text_3', '.girl_text_1', 'blink_and_big', 'blink_and_big', '', '', '.speech_div');
			sound_caller(sound_hungry, '.girl_text_1', 'blink_and_big', '', '', '.speech_div');
			sound_caller(sound_let_eat, '.granny_text_3', 'blink_and_big', '', '', '.speech_div');
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		if(countNext == 6){
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$('.speech_div').hide(0);
			$('.butterfly').removeClass('butterfly2');
			$('.butterfly').addClass('butterfly_move2');
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 5500);
		} else {
			countNext++;
			templateCaller();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();


	/* ------ Function that will make the div look green and make sound play accrodingly -------- */
	function sound_caller(sound_var, sound_box_class, sound_box_animation, highlight_class, highlight_animation , common_class){
		$(sound_box_class).click(function(){
			sound_var.play();
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			$(common_class).css('pointer-events', 'none');
			$(sound_box_class).addClass(sound_box_animation);
			$(highlight_class).addClass(highlight_animation);
			sound_var.bindOnce('ended', function(){
				$(highlight_class).removeClass(highlight_animation);
				$(sound_box_class).removeClass(sound_box_animation);
				$(common_class).css('pointer-events', 'all');
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.lessonEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}
			});
		});
	}
	function play_sound(sound_class_text, text_class, text_animation, highlight_class, highlight_animation, common_class){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(common_class).css('pointer-events', 'none');
		$(text_class).show(0);
		$(text_class).addClass(text_animation);
		$(highlight_class).addClass(highlight_animation);
		sound_class_text.play();
		sound_class_text.bindOnce('ended', function(){
			$(text_class).removeClass(text_animation);
			if(countNext == content.length-1)
			{
				ole.footerNotificationHandler.lessonEndSetNotification();
				$prevBtn.show(0);
			} else {
				$nextBtn.show(0);
				$prevBtn.show(0);
			}
			$(highlight_class).removeClass(highlight_animation);
			$(common_class).css('pointer-events', 'all');
		});
	}
	function play_qna(sound_class_ques, sound_class_text, ques_class, text_class, ques_animation, text_animation, highlight_class, highlight_animation, common_class){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(common_class).css('pointer-events', 'none');
		$(ques_class).addClass(ques_animation);
		$(text_class).hide(0);
		$(highlight_class).addClass(highlight_animation);
		sound_class_ques.play();
		sound_class_ques.bindOnce('ended', function(){
			$(text_class).show(0);
			$(ques_class).removeClass(ques_animation);
			$(text_class).addClass(text_animation);
			sound_class_text.play();
			sound_class_text.bindOnce('ended', function(){
				$(text_class).removeClass(text_animation);
				if(countNext == content.length-1)
				{
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				} else {
					$nextBtn.show(0);
					$prevBtn.show(0);
				}
				$(highlight_class).removeClass(highlight_animation);
				$(common_class).css('pointer-events', 'all');
			});
		});
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

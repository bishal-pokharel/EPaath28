var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var sound_s0 = new buzz.sound((soundAsset + "p1_0.ogg"));
var sound_s1 = new buzz.sound((soundAsset + "exe_1.ogg"));
var sound_s2 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s3 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s4 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s5 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s6 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s7 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s8 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s9 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s10 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s11 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s12 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s13 = new buzz.sound((soundAsset + "exe_inst.ogg"));
var sound_s14 = new buzz.sound((soundAsset + "exe_inst.ogg"));

var sound_group_p1 = [sound_s0, sound_s1, sound_s2,sound_s3, sound_s4,sound_s5,sound_s6,sound_s7,sound_s8,sound_s9,sound_s10,sound_s11,sound_s12,sound_s13,sound_s14];

var content1=[
	//slide 0
	{
		contentblockadditionalclass: 'instruction_bg',
		uppertextblockadditionalclass: 'ex_main_text',
		uppertextblock : [
		{
			textdata : data.string.einstruction1,
			textclass : 'my_font_very_big',
		}
		],
	},
	//slide 1
	{
		contentblockadditionalclass: 'gradient_bg',
		uppertextblockadditionalclass: 'ex_ins_text',
		uppertextblock : [
		{
			textdata : data.string.einstruction,
			textclass : 'my_font_very_big',
		}],
	},
];
var content2=[
	// dog
	//slide 2
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_1",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_1",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "dog item_highlight",
				imgsrc : imgpath + "dog.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_1',
			textclass : 'text_first',
			textdata : data.string.etext1,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			this_class : 'correct_ans',
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_1",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_1",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "dog item_highlight",
				imgsrc : imgpath + "dog.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_2',
			textclass : 'text_last',
			textdata : data.string.etext2,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			this_class : 'correct_ans',
		}],
	},

	// pond
	//slide 4
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_1",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_2",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "pond item_highlight",
				imgsrc : imgpath + "pond.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_1',
			textclass : 'text_first',
			textdata : data.string.etext4,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			this_class : 'correct_ans',
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_1",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_2",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "pond item_highlight",
				imgsrc : imgpath + "pond.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_3',
			textclass : 'text_last',
			textdata : data.string.etext3,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			that_class : 'correct_ans',
		}],
	},

	// bench
	//slide 6
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_3",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "bench item_highlight",
				imgsrc : imgpath + "bench.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_5',
			textclass : 'text_first',
			textdata : data.string.etext5,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			this_class : 'correct_ans',
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny gpos_3",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "bench item_highlight",
				imgsrc : imgpath + "bench.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_4',
			textclass : 'text_last',
			textdata : data.string.etext6,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			this_class : 'correct_ans',
		}],
	},

	//squirrel
	//slide 8
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid gpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "tree",
				imgsrc : imgpath + "tree.png",
			},
			{
				imgclass : "squirrel item_highlight",
				imgsrc : imgpath + "squirrel.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_4',
			textclass : 'text_first',
			textdata : data.string.etext7,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			that_class : 'correct_ans',
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid gpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "tree",
				imgsrc : imgpath + "tree.png",
			},
			{
				imgclass : "squirrel item_highlight",
				imgsrc : imgpath + "squirrel.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_3',
			textclass : 'text_last',
			textdata : data.string.etext8,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			that_class : 'correct_ans',
		}],
	},

	// shop
	//slide 10
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid gpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "shop item_highlight",
				imgsrc : imgpath + "shop.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_3',
			textclass : 'text_first',
			textdata : data.string.etext4,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			that_class : 'correct_ans',
		}],
	},
	//slide 11
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid gpos_3",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "shop item_highlight",
				imgsrc : imgpath + "shop.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_4',
			textclass : 'text_last',
			textdata : data.string.etext9,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			that_class : 'correct_ans',
		}],
	},

	// ball
	//slide 12
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_7",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "ball item_highlight",
				imgsrc : imgpath + "ball.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_3',
			textclass : 'text_first',
			textdata : data.string.etext4,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ethis,
			choicedata_b : data.string.ethat,
			qmarkclass : '',
			qmarkdata : data.string.eqmark,
			that_class : 'correct_ans',
		}],
	},
	//slide 13
	{
		contentblockadditionalclass: 'story_bg',
		uppertextblockadditionalclass:'uppertext',
		uppertextblock :[
		{
			textdata : data.string.etext11,
			textclass : 'instructiontext',
		}],
		imageblockadditionalclass: 'cover_board',
		imageblock : [
			{
				imgclass : "granny kpos_2",
				imgsrc : imgpath + "granny.png",
			},
			{
				imgclass : "kid kpos_7",
				imgsrc : imgpath + "kid.png",
			},
			{
				imgclass : "ball item_highlight",
				imgsrc : imgpath + "ball.png",
			}
		],

		thisthatblockadditionalclass: 'cover_board my_font_medium tt_block',
		thisthatblock : [
		{
			speechboxclass: 'speech_common speech_7',
			textclass : 'text_last',
			textdata : data.string.etext10,
			blankclass : '',
			blankdata : ' ',
			choiceclass : '',
			choicedata_a : data.string.ebthis,
			choicedata_b : data.string.ebthat,
			this_class : 'correct_ans',
		}],
	},
];

// content2.shufflearray();
// var content = content2;
var content = content1.concat(content2);

$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//create eggs
	var eggs = new EggTemplate();

 	//eggTemplate.eggMove(countNext);
	eggs.init(12);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		function playaudio(audioindex, show_hide_next_prev_btns, length){
				var audio = sound_group_p1[audioindex];

				show_hide_next_prev_btns = (show_hide_next_prev_btns == null)? true: show_hide_next_prev_btns;
				// if(show_hide_next_prev_btns){

				// }
				// if(countNext > 11 && countNext < $total_page){
				// 	$(".imageclose").hide(0);
				// }

				audio.play();
				audio.bind("ended", function(){
					// if(length == 12){
						if(show_hide_next_prev_btns){
						// (countNext < ($total_page-1) ) ? $nextBtn.show(0):  true;
							(countNext < 2 ) ? $nextBtn.show(0):  true;
							(countNext > 0 && countNext <2) ? $prevBtn.show(0):  true;
						// }

						if( countNext == ($total_page-1)){
							$(".imageclose").show(0);
						}
						$(this).unbind("ended");
					} else {
						$(this).unbind("ended");
						playaudio(audioindex+1, show_hide_next_prev_btns, length-1);
					}
				});
			}

		switch (countNext) {
			// case 0:
			// break;
			case 0:
					playaudio(countNext, true, 1);
				break;
			case 1:
				playaudio(countNext, true, 1);
				break;
			case 2:
					playaudio(countNext, true, 1);
					break;
			case 3:
				playaudio(countNext, true, 1);
				break;
				case 4:
				playaudio(countNext, true, 1);
				break;
				case 5:
				playaudio(countNext, true, 1);
				break;
				case 6:
					playaudio(countNext, true, 1);
					break;

					case 7:
							playaudio(countNext, true, 1);
						break;
					case 8:
						playaudio(countNext, true, 1);
						break;
					case 9:
							playaudio(countNext, true, 1);
							break;
					case 10:
						playaudio(countNext, true, 1);
						break;
						case 11:
						playaudio(countNext, true, 1);
						break;
						case 12:
						playaudio(countNext, true, 1);
						break;
						case 13:
						playaudio(countNext, true, 1);
						break;
			default:
			break;
		}

		//randomize options
			var wrong_clicked = false;
			$(".click_ans").click(function(){
				if($(this).hasClass("correct_ans")){
					if(!wrong_clicked){
						eggs.update(true);
					}
					$(this).css({
						'background-color': '#6EB260',
						'color': 'white'
					});
					$('.choice_class').delay(500).fadeOut(500);
					setTimeout(function(){
						$('.blank_class').html($('.correct_ans').html());
						$('.blank_class').css('height','auto');
						if(countNext != $total_page)
							$nextBtn.show(0);
							$prevBtn.show(0);
					}, 1000);
					$(".click_ans").css('pointer-events','none');
					play_correct_incorrect_sound(1);
				}
				else{
					if(!wrong_clicked){
						eggs.update(false);
						$(this).css({
							'background-color': '#BA6B82',
							'pointer-events': 'none'
						});
					}
					wrong_clicked = true;
					play_correct_incorrect_sound(0);
				}
			});

	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		if(countNext>2){
			eggs.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

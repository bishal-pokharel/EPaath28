var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var soundcontent;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s1
		}],
		imageblock: [{
			imgcontainerdiv: "circle_zero",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "mane"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s2
			}]
		}],
		exe:[{
			mcqqn:data.string.p3_question,
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.e,
				optaddclass:"class1"
			},{
				optdata:data.string.a
			}]
		}]

	},
	{
		// slide1
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s1
		}],
		imageblock: [{
			imgcontainerdiv: "circle_zero",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "beet"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s4
			}]
		}],
		exe:[{
			mcqqn:data.string.p3_question,
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.e,
				optaddclass:"class1"
			},{
				optdata:data.string.a
			}]
		}]
	},
	{
		// slide2
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s1
		}],
		imageblock: [{
			imgcontainerdiv: "circle_zero",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "pine"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s7
			}]
		}],
		exe:[{
			mcqqn:data.string.p3_question,
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.i,
				optaddclass:"class1"
			},{
				optdata:data.string.e
			}]
		}]
	},
	{
		// slide3
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s1
		}],
		imageblock: [{
			imgcontainerdiv: "circle_zero",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "rode"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s9
			}]
		}],
		exe:[{
			mcqqn:data.string.p3_question,
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.o,
				optaddclass:"class1"
			},{
				optdata:data.string.a
			}]
		}]
	},
	{
		// slide4
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s1
		}],
		imageblock: [{
			imgcontainerdiv: "circle_zero",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "cube"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s11
			}]
		}],
		exe:[{
			mcqqn:data.string.p3_question,
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optdata:data.string.u,
				optaddclass:"class1"
			},{
				optdata:data.string.e
			}]
		}]
	},
	{
		// slide5
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s13
		}],
		imageblock: [{
			imgcontainerdiv: "circle_1",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "pine"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s7_a
			}]
		},{
			imgcontainerdiv: "circle_2",
			imagestoshow: [{
				imgclass: "image2",
				imgid: "beet"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s4
			}]
		},{
			imgcontainerdiv: "circle_3",
			imagestoshow: [{
				imgclass: "image3",
				imgid: "cube"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s11_a
			}]
		},{
			imgcontainerdiv: "circle_4",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "rode"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s9_a
			}]
		},{
			imgcontainerdiv: "circle_5",
			imagestoshow: [{
				imgclass: "image5",
				imgid: "mane"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s2_a
			}]
		}]
	},
	{
		// slide6
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p3_s0
		},{
			textclass: "description",
			textdata: data.string.p3_s14
		}],
		imageblock: [{
			imgcontainerdiv: "circle_1",
			imagestoshow: [{
				imgclass: "image1",
				imgid: "pine"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s7_a
			}]
		},{
			imgcontainerdiv: "circle_2",
			imagestoshow: [{
				imgclass: "image2",
				imgid: "beet"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				imagelabeldata: data.string.p3_s4
			}]
		},{
			imgcontainerdiv: "circle_3",
			imagestoshow: [{
				imgclass: "image3",
				imgid: "cube"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s11_a
			}]
		},{
			imgcontainerdiv: "circle_4",
			imagestoshow: [{
				imgclass: "image4",
				imgid: "rode"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s9_a
			}]
		},{
			imgcontainerdiv: "circle_5",
			imagestoshow: [{
				imgclass: "image5",
				imgid: "mane"
			}],
			imagelabels:[{
				imagelabelclass: "imagelabel",
				datahighlightflag: true,
				imagelabeldata: data.string.p3_s2_a
			}]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "mane", src: imgpath+"correctedImages/MANE.png", type: createjs.AbstractLoader.IMAGE},
			{id: "beet", src: imgpath+"correctedImages/BEET.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pine", src: imgpath+"correctedImages/tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rode", src: imgpath+"correctedImages/rode.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cube", src: imgpath+"cube.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dialog", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_0_1", src: soundAsset+"p3_s0_1.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_1_1", src: soundAsset+"p3_s1_1.ogg"},
			{id: "sound_2_1", src: soundAsset+"p3_s2_1.ogg"},
			{id: "sound_3_1", src: soundAsset+"p3_s3_1.ogg"},
			{id: "sound_4_1", src: soundAsset+"p3_s4_1.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "mane_sound", src: soundAsset+"mane.ogg"},
			{id: "beet_sound", src: soundAsset+"beet.ogg"},
			{id: "pine_sound", src: soundAsset+"pine.ogg"},
			{id: "rode_sound", src: soundAsset+"rode.ogg"},
			{id: "cube_sound", src: soundAsset+"cube.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
  var timeourcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		(countNext > 0)?$prevBtn.show(0): true;
		$("[class*='circle']").append('&nbsp;&nbsp; <span class="speaker_icon glyphicon glyphicon-volume-up"></span>');

	    var audio_seq_array = ["mane", "beet", "pine", "rode", "cube"];
	    var audio_seq_array2 = ["pine", "beet", "cube", "rode", "mane"];
			/* for randomizing the options */
			function random(parent) {
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}
			function mcq(){
				$(".buttonsel").click(function(){
					if($(this).hasClass("class1")){
						play_correct_incorrect_sound(1);
						$(this).addClass("cor_class");
						$(".buttonsel").css("pointer-events","none");
						$(this).siblings(".corctopt").show(0);
						navigationcontroller();
					}else{
						play_correct_incorrect_sound(0);
						$(this).addClass("incor_class");
						$(this).siblings(".wrngopt").show(0);
					}
				});
			}
			countNext>=0&&countNext<5?mcq():'';
			countNext>=0&&countNext<5?random(".optionsdiv"):'';
	    switch(countNext){
	    	case 0:
	    		var arraysound = ["sound_0", "sound_0_1", audio_seq_array[countNext]+"_sound"];
	    		play_sound_sequence(arraysound);
	    		$(".circle_zero *").click(function(){
	    			sound_player(audio_seq_array[countNext]+"_sound");
	    		});
    		break;
	    	case 1:
	    	case 2:
	    	case 3:
	    	case 4:
	    		$($(".imagelabel2")[1]).css("opacity", "0");
	    		var arraysound = ["sound_"+countNext, audio_seq_array[countNext]+"_sound" ,"sound_"+countNext+"_1"];
	    		play_sound_sequence(arraysound);
	    		$(".circle_zero *").click(function(){
	    			sound_player(audio_seq_array[countNext]+"_sound");
	    		});
	    		break;
	    	case 5:
	    	case 6:
	    		var arraysound = ["sound_"+countNext];
	    		//this is temporary solution, need to fix vocab.js to resolve this issue, remove this once it is fixed
    			$('.imagelabel').addClass('no-underline');
	    		setTimeout(function(){
	    			$('.imagelabel span').removeClass('voacbunderline');
	    			$('.imagelabel span').unbind('mouseenter mouseleave');
	    		}, 500);
	    		for (var i = 0; i < audio_seq_array2.length; i++){
	    			arraysound.push(audio_seq_array2[i]+"_sound");
	    		}
	    		play_sound_sequence(arraysound);
	    		$(".circle_1 *").click(function(){
	    			sound_player(audio_seq_array2[0]+"_sound");
	    		});
	    		$(".circle_2 *").click(function(){
	    			sound_player(audio_seq_array2[1]+"_sound");
	    		});
	    		$(".circle_3 *").click(function(){
	    			sound_player(audio_seq_array2[2]+"_sound");
	    		});
	    		$(".circle_4 *").click(function(){
	    			sound_player(audio_seq_array2[3]+"_sound");
	    		});
	    		$(".circle_5 *").click(function(){
	    			sound_player(audio_seq_array2[4]+"_sound");
	    		});
	    		break;
	    	default:
	    		var arraysound = ["sound_"+countNext, "bet_sound", "sound_0_1"];
	    		play_sound_sequence(arraysound);
				//sound_player("sound_"+countNext);
	    		break;
	    }
   }


/*=====  End of Templates Block  ======*/
function play_sound_sequence(soundarray){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(soundarray[0]);
	soundarray.splice( 0, 1);
	current_sound.on("complete", function(){
		if(soundarray.length > 0){
			if(soundarray.length == 1){
				$($(".imagelabel2")[1]).animate({ opacity :1 }, 2000);
			}
			play_sound_sequence(soundarray);
		}else{
			if($total_page > (countNext+1)){
				countNext>5?$nextBtn.show(0):'';
			}else{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		}

	});
}

function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if($total_page > (countNext+1)){
				$nextBtn.show(0);
			}else{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
	}


function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/DIY/";

var soundcontent;

var content=[
	{
		//slide 0
		uppertextblock: [{
			textclass: "description_main2",
			textdata: data.string.p4_s0
		}]
	},{
		//slide 1
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "cake"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s2
		},{
			textclass: "description pos1 option yes",
			textdata: data.string.p4_option_a
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_i
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_k
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_o
		}
		]
	},{
		//slide 2
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "tube"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s3
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_i
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_e
		},{
			textclass: "description pos3 option yes",
			textdata: data.string.p4_option_u
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_t
		}
		]
	},{
		//slide 3
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "time"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s4
		},{
			textclass: "description pos1 option yes",
			textdata: data.string.p4_option_i
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_m
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_a
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_e
		}
		]
	},{
		//slide 4
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "tree"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s5
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_r
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_i
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_m
		},{
			textclass: "description pos4 option yes",
			textdata: data.string.p4_option_e
		}
		]
	},{
		//slide 5
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "home"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s6
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_u
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_h
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_a
		},{
			textclass: "description pos4 option yes",
			textdata: data.string.p4_option_o
		}
		]
	},{
		//slide 6
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "train"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s7
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_e
		},{
			textclass: "description pos2 option",
			textdata: data.string.p4_option_n
		},{
			textclass: "description pos3 option yes",
			textdata: data.string.p4_option_a
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_l
		}
		]
	},{
		//slide 7
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "play"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s8
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_e
		},{
			textclass: "description pos2 option yes",
			textdata: data.string.p4_option_a
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_o
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_c
		}
		]
	},{
		//slide 8
		contentnocenteradjust: true,
		uppertextblockadditionalclass: "utb",
		uppertextblock: [{
			textclass: "description_main",
			textdata: data.string.p4_s1
		}],
		imageblock: [{
			imagestoshow:[{
				imgclass: "image",
				imgid: "pole"
			}]
		}],
		lowertextblockadditionalclass: "ltb",
		lowertextblock: [{
			textclass: "description pos0",
			textdata: data.string.p4_s9
		},{
			textclass: "description pos1 option",
			textdata: data.string.p4_option_e
		},{
			textclass: "description pos2 option yes",
			textdata: data.string.p4_option_o
		},{
			textclass: "description pos3 option",
			textdata: data.string.p4_option_y
		},{
			textclass: "description pos4 option",
			textdata: data.string.p4_option_c
		}
		]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "a_25", src: "images/diy_bg/a_25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cake", src: imgpath+"cake.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tube", src: imgpath+"tube.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree", src: imgpath+"tree1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "time", src: imgpath+"time.png", type: createjs.AbstractLoader.IMAGE},
			{id: "home", src: imgpath+"home.png", type: createjs.AbstractLoader.IMAGE},
			{id: "train", src: imgpath+"train.png", type: createjs.AbstractLoader.IMAGE},
			{id: "play", src: imgpath+"play.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pole", src: imgpath+"pole.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: imgpath+"incorrect.png", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "cake_sound", src: soundAsset+"cake.ogg"},
			{id: "home_sound", src: soundAsset+"home.ogg"},
			{id: "play_sound", src: soundAsset+"play.ogg"},
			{id: "pole_sound", src: soundAsset+"pole.ogg"},
			{id: "time_sound", src: soundAsset+"time.ogg"},
			{id: "train_sound", src: soundAsset+"train.ogg"},
			{id: "tree_sound", src: soundAsset+"tree.ogg"},
			{id: "tube_sound", src: soundAsset+"tube.ogg"},
			{id: "DIYquestion", src: soundAsset+"DIYquestion.ogg"},
			
			
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			// $prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
  var timeourcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		(countNext > 0)?$prevBtn.show(0): true;
		
		// $(".image_label1").append('&nbsp;&nbsp; <span class="glyphicon glyphicon-volume-up"></span>');
			countNext==1 ? sound_player("DIYquestion") : '';
		if(countNext > 0){
			var soundid;
			switch(countNext){
				case 1:
					soundid = "cake_sound";
					break;
				case 2:
					soundid = "tube_sound";
					break;
				case 3:
					soundid = "time_sound";
					break;
				case 4:
					soundid = "tree_sound";
					break;
				case 5:
					soundid = "home_sound";
					break;
				case 6:
					$(".imageblock").css("height","100%");
					soundid = "train_sound";
					break;
				case 7:
					soundid = "play_sound";
					break;
				case 8:
					soundid = "pole_sound";
					break;
				default:
					break;
			}
			// play_sound_sequence(["DIYquestion", soundid]);
			$(".imageblock").append('&nbsp;&nbsp; <span class="speaker_icon glyphicon glyphicon-volume-up"></span>');
			$(".pos0").append("<img class='correct_sign' src= '"+ preload.getResult('correct').src +"'>  <img class='incorrect_sign' src= '"+ preload.getResult('incorrect').src +"'>");
			var draggables = $(".option").draggable({
				containment : "body",
				cursor : "grabbing",
				revert : "invalid",
				appendTo : "body",
				helper : "clone",
				zindex : 10000,
				start: function(event, ui){	
					var $clone = $(ui.helper);
					$clone.css({"background-color":"#F1C232", "border":"0.1em solid #F1C232", "text-align": "center"});
					$(this).css({"opacity": "0.5"});
				},
				stop: function(event, ui){
					$(this).css({"opacity": "1"});
				}
			});
			$('.dropinto ').droppable({
				accept : ".option",
				hoverClass : "hovered",
				drop : handleCardDrop
			});
			function handleCardDrop(event, ui){
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					
					dropped.css({"background": "#e0e0e0", "border": "0.1em solid #e0e0e0"});
					
				 	droppedOn.html(dropped.html());
					if(dropped.hasClass("yes")){
						 droppedOn.removeClass("incorrect").addClass("correct");
						 // dropped.draggable('option', 'revert', false);
						 $(".incorrect_sign").hide(0);
						 $(".correct_sign").show(0);
						 play_correct_incorrect_sound(true);
						 draggables.draggable('disable');
						 draggables.css({"background": "#e0e0e0", "border": "0.1em solid #e0e0e0"});
						 if (countNext == $total_page - 1){
						 	ole.footerNotificationHandler.pageEndSetNotification();
						 }else{
						 	$nextBtn.show(0);
						 }
					} else {
						droppedOn.addClass("incorrect");
						 $(".incorrect_sign").show(0);
						 // dropped.draggable('option', 'revert', true);
						 play_correct_incorrect_sound(false);
					}
			}	
			$(".imageblock *").click(function(){
				sound_player(soundid);
			});
		} else {
			sound_player("sound_0");
			$(".contentblock").css({"background-image": "url("+ preload.getResult('a_25').src +")", "background-size": "100% 100%"});
		}
   }	    

	    
/*=====  End of Templates Block  ======*/
function play_sound_sequence(soundarray){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(soundarray[0]);
	soundarray.splice( 0, 1);
	current_sound.on("complete", function(){
		if(soundarray.length > 0){
			play_sound_sequence(soundarray);
		}else{
			// if($total_page > (countNext+1)){
				// $nextBtn.show(0);
			// }else{
				// ole.footerNotificationHandler.pageEndSetNotification();
			// }
		}
		
	});
}

function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(countNext == 0)
				 $nextBtn.show(0);
			// if($total_page > (countNext+1)){
				// $nextBtn.show(0);
			// }else{
				// ole.footerNotificationHandler.pageEndSetNotification();
			// }
		});
	}


function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}					
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

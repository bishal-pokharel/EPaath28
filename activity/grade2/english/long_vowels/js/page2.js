var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var soundcontent;

var content=[
	{
		// slide0
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		lowertextblockadditionalclass: "info_ltb",
		lowertextblock: [{
			textclass: "description2",
			textdata: data.string.p2_s0
		}]

	},{
		// slide1
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		imageblock: [{
			imgcontainerdiv: "container_left",
			imagestoshow: [{
				imgclass: "image_left",
				imgid: "bet"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s1
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s7
			}]
			
		},{
			imgcontainerdiv: "container_right",
			imagestoshow: [{
				imgclass: "image_right",
				imgid: "beet"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s2
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s8
			}]
			
		}]

	},{
		// slide2
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		imageblock: [{
			imgcontainerdiv: "container_left",
			imagestoshow: [{
				imgclass: "image_left",
				imgid: "pin"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s3
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s7
			}]
			
		},{
			imgcontainerdiv: "container_right",
			imagestoshow: [{
				imgclass: "image_right",
				imgid: "pine"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s4
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s8
			}]
			
		}]

	},{
		// slide3
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		imageblock: [{
			imgcontainerdiv: "container_left",
			imagestoshow: [{
				imgclass: "image_left",
				imgid: "rod"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s5
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s7
			}]
			
		},{
			imgcontainerdiv: "container_right",
			imagestoshow: [{
				imgclass: "image_right",
				imgid: "rode"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s6
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s8
			}]
			
		}]

	},{
		// slide4
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		}],
		imageblock: [{
			imgcontainerdiv: "container_left",
			imagestoshow: [{
				imgclass: "image_left",
				imgid: "cub"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s7
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s7
			}]
			
		},{
			imgcontainerdiv: "container_right",
			imagestoshow: [{
				imgclass: "image_right",
				imgid: "cube"
			}],
			imagelabels: [{
				imagelabelclass: "image_label1",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p2_s8
			},{
				imagelabelclass: "image_label2",
				datahighlightflag: true,
				datahighlightcustomclass: "blue_higlight",
				imagelabeldata: data.string.p1_s8
			}]
			
		}]

	},{
		// slide5
		contentnocenteradjust: true,
		uppertextblock: [{
			textclass: "description",
			textdata: data.string.p1_s0
		},
		{
			textclass: "instruction",
			textdata: data.string.instruction
		}],
		tableblock: true,
		tablerow: [{
			tdclass1: "heading",
			tddata1: data.string.p2_s10,
			tdclass2: "heading",
			tddata2: data.string.p2_s11,
		},{
			tdclass1: "brown",
			audioans1: data.string.p2_s12,
			tddata1: data.string.p2_s12,
			tdclass2: "brown2",
			audioans2: data.string.p2_s13,
			tddata2: data.string.p2_s13,
		},{
			tdclass1: "lightblue",
			audioans1: data.string.p2_s1,
			tddata1: data.string.p2_s1,
			tdclass2: "lightblue2",
			audioans2: data.string.p2_s2,
			tddata2: data.string.p2_s2,
		},{
			tdclass1: "pink",
			audioans1: data.string.p2_s3,
			tddata1: data.string.p2_s3,
			tdclass2: "pink2",
			audioans2: data.string.p2_s4,
			tddata2: data.string.p2_s4,
		},{
			tdclass1: "green",
			audioans1: data.string.p2_s5,
			tddata1: data.string.p2_s5,
			tdclass2: "green2",
			audioans2: data.string.p2_s6,
			tddata2: data.string.p2_s6,
		},{
			tdclass1: "yellow",
			audioans1: data.string.p2_s7,
			tddata1: data.string.p2_s7,
			tdclass2: "yellow2",
			audioans2: data.string.p2_s8,
			tddata2: data.string.p2_s8,
		}]

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var isFirefox = typeof InstallTrigger !== 'undefined';
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cub", src: imgpath+"cub.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cube", src: imgpath+"cube.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pin", src: imgpath+"pin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pine", src: imgpath+"pine.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rod", src: imgpath+"pipe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rode", src: imgpath+"rode.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bet", src: imgpath+"bet.png", type: createjs.AbstractLoader.IMAGE},
			{id: "beet", src: imgpath+"beet.png", type: createjs.AbstractLoader.IMAGE},
			
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "bet_sound", src: soundAsset+"bet.ogg"},
			{id: "beet_sound", src: soundAsset+"beet.ogg"},
			{id: "pin_sound", src: soundAsset+"pin.ogg"},
			{id: "pine_sound", src: soundAsset+"pine.ogg"},
			{id: "rod_sound", src: soundAsset+"rod.ogg"},
			{id: "rode_sound", src: soundAsset+"rode.ogg"},
			{id: "cub_sound", src: soundAsset+"cub.ogg"},
			{id: "cube_sound", src: soundAsset+"cube.ogg"},
			{id: "man_sound", src: soundAsset+"man.ogg"},
			{id: "mane_sound", src: soundAsset+"mane.ogg"},
			{id: "instruction", src: soundAsset+"p2_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var animationinprogress = false;

  var sound_data;
  var timeourcontroller;
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
	    put_image(content, countNext);
	    vocabcontroller.findwords(countNext);
		// splitintofractions($(".fractionblock"));
		(countNext > 0)?$prevBtn.show(0): true;
		$(".image_label1").append('&nbsp;&nbsp; <span class="glyphicon glyphicon-volume-up"></span>');
	    switch(countNext){
	    	case 0:
				sound_player("sound_"+countNext);
	    		$(".contentblock").css("background-color", "#FFFCED");
	    		// $(".contentblock").css("background-image", 'url('+preload.getResult("bg").src+')');
	    		break;
	    	case 1:
	    		var arraysound = ["bet_sound", "beet_sound"];
	    		play_sound_sequence(arraysound);
	    		$(".container_left *").click(function(){
	    			sound_player("bet_sound");
	    		});
	    		
	    		$(".container_right *").click(function(){
	    			sound_player("beet_sound");
	    		});
	    		
	    		break;
	    	case 2:
	    		var arraysound = ["pin_sound", "pine_sound"];
	    		play_sound_sequence(arraysound);
	    		$(".container_left *").click(function(){
	    			sound_player("pin_sound");
	    		});
	    		
	    		$(".container_right *").click(function(){
	    			sound_player("pine_sound");
	    		});
	    		break;
	    	case 3: 
	    		var arraysound = ["rod_sound", "rode_sound"];
	    		play_sound_sequence(arraysound);
	    		$(".container_left *").click(function(){
	    			sound_player("rod_sound");
	    		});
	    		
	    		$(".container_right *").click(function(){
	    			sound_player("rode_sound");
	    		});
	    		break;
	    	case 4:
	    		var arraysound = ["cub_sound", "cube_sound"];
	    		play_sound_sequence(arraysound);
	    		$(".container_left *").click(function(){
	    			sound_player("cub_sound");
	    		});
	    		
	    		$(".container_right *").click(function(){
	    			sound_player("cube_sound");
	    		});
	    		break;
	    	case 5:
				sound_player("instruction");
	    		var $td = $("td");
	    		for(var i = 0; i < $td.length; i++){
	    			var $td_x = $($td[i]);
	    			if(!$td_x.hasClass("heading")){
	    				$td_x.append('&nbsp;&nbsp; <span class="glyphicon glyphicon-volume-up"></span>');
	    			}
	    		}
	    		
	    		$td.click(function(){
	    			var $this = $(this);
	    			var audiodata = ($this.data("audioans")).toLowerCase();
	    			if( audiodata != null){
	    				sound_player(audiodata+"_sound");
	    			}
	    		});
	    		break;
	    	default:
	    		break;
	    }
   }	    

	    
/*=====  End of Templates Block  ======*/
function play_sound_sequence(soundarray){
	createjs.Sound.stop();
	var current_sound = createjs.Sound.play(soundarray[0]);
	soundarray.splice( 0, 1);
	current_sound.on("complete", function(){
		if(soundarray.length > 0){
			play_sound_sequence(soundarray);
		}else{
			if($total_page > (countNext+1)){
				$nextBtn.show(0);
			}else{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		}
		
	});
}

function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if($total_page > (countNext+1)){
				$nextBtn.show(0);
			}else{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		});
	}


function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			for(var j = 0; j < content[count].imageblock.length; j++){
				var imageblock = content[count].imageblock[j];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}					
				}
			}
		}
	}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    // navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		if(sound_data != null){
			sound_data.stop();
			sound_data.unbind('ended');
		}
		clearTimeout( timeourcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

Array.prototype.shufflearray = function() {
    var i = this.length,
    j,
    temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "ex_0.ogg"));

var content = [
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q1p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption correctans",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "sleep.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q2p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption correctans",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "leaf.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q3p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption correctans",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "bee.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q4p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption correctans",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "peas.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q5p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption correctans",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "wheat.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q6p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption correctans",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "sea.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q7p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption correctans",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "chair.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q8p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption correctans",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "sheep.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q9p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption correctans",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "knee.png"
},
{
    questionblock: [
    {
        textclass: "left",
        textdata: data.string.q10p1
    },
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            textclass: "optionscontainer leftoption correctans",
            textdata: data.string.is
        }, {
            textclass: "optionscontainer rightoption",
            textdata: data.string.are
        }
        ]
    }
    ],
    imgsrc: imgpath + "sweet.png"
}
];
//
// /*remove this for non random questions*/
content.shufflearray();

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
  	loadTimelineProgress($total_page,countNext+1);

    /*for limiting the questions to 10*/
    var $total_page = content.length;
    var preload;
    var timeoutvar = null;
    var current_sound;

    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
        // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
        //  
        //images
        //sounds
        {id: "sound_1", src: soundAsset+"ex_0.ogg"},

      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
      // console.log(event.item);
    }
    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
      $('#loading-wrapper').hide(0);

      // call main function
      templateCaller();
    }
    //initialize
    init();

    /*var $total_page = content.length;*/

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined"
        ? islastpageflag = false
        : typeof islastpageflag != 'boolean'
        ? alert("NavigationController : Hi Master, please provide a boolean parameter")
        : null;
    }

    var testin = new RhinoTemplate();

    testin.init(10);

    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /*===============================================
        =            data highlight function            =
        ===============================================*/
    /**

         What it does:
         - send an element where the function has to see
         for data to highlight
         - this function searches for all nodes whose
         data-highlight element is set to true
         -searches for # character and gives a start tag
         ;span tag here, also for @ character and replaces with
         end tag of the respective
         - if provided with data-highlightcustomclass value for highlight it
         applies the custom class or else uses parsedstring class

         E.g: caller : texthighlight($board);
         */
         function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object"
        ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted")
        : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass")
                ?/*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass"))
                : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $('.congratulation').hide(0);
        $('.exefin').hide(0);

        texthighlight($board);

        if(countNext==0){
          sound_player("sound_1");
        }
        var parent = $(".droparea");
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        }
        checkyour();

        }
        function checkyour() {
                var clickFlag = false;

                $(".optionscontainer").click(function(){
                    if(clickFlag == false){
                        var cortext = $(this).text();
                        if($(this).hasClass("correctans")){
                            setTimeout(function(){
                                $(".unsc").hide(0).text(cortext.replace(/ /g,'')).addClass("corcol").fadeIn();
                                $nextBtn.show(0);
                            }, 1500);
                            $(this).addClass("movefc");
                            testin.update(true);
                            play_correct_incorrect_sound(1);
                            clickFlag = true;
                        }
                        else{
                            testin.update(false);
                            play_correct_incorrect_sound(0);
                            $(this).addClass("wrongans");

                        }
                    }
                });

        }

        function sound_player(sound_id){
      		createjs.Sound.stop();
      		current_sound = createjs.Sound.play(sound_id);
      		current_sound.play();
      	}

        function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


        //call the slide indication bar handler for pink indicators

    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
        testin.gotoNext();

    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page
        ? ole.footerNotificationHandler.hideNotification()
        : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "page2/recording1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "page2/recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "page2/recording3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "page2/recording4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "page2/recording5.ogg"));
var sound_click = new buzz.sound((soundAsset + "page2/s2_p22.ogg"));

var sound_bean = new buzz.sound((soundAsset + "bean.ogg"));
var sound_pea = new buzz.sound((soundAsset + "peas.ogg"));
var sound_seat = new buzz.sound((soundAsset + "seat.ogg"));
var sound_meat = new buzz.sound((soundAsset + "meat.ogg"));
var sound_wheat = new buzz.sound((soundAsset + "wheat.ogg"));
var sound_steam = new buzz.sound((soundAsset + "steam.ogg"));

var ea_gr = [sound_bean,sound_pea,sound_seat,sound_meat,sound_wheat,sound_steam];

var sound_sleep = new buzz.sound((soundAsset + "sleep.ogg"));
var sound_sheep = new buzz.sound((soundAsset + "sheep.ogg"));
var sound_wheel = new buzz.sound((soundAsset + "wheel.ogg"));
var sound_knee = new buzz.sound((soundAsset + "knee.ogg"));
var sound_jeep = new buzz.sound((soundAsset + "jeep.ogg"));
var sound_feet = new buzz.sound((soundAsset + "feet.ogg"));

var ee_gr = [sound_sleep,sound_sheep,sound_wheel,sound_knee ,sound_jeep, sound_feet];

var rand_colors = ['#FFB85F','#FF7A5A','#B2D145','#FF828B','#00AAA0','#EACF0E'];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole-background-gradient-shore',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_unique',
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole-background-gradient-shore',

		uppertextblockadditionalclass: 'top_text',
		uppertextblock : [
			{
				textdata : data.string.p2text3,
				textclass : 'sniglet my_font_very_big header_ea'
			},
			{
				textdata : data.string.p2text2,
				textclass : 'sniglet my_font_medium hinttext'
			}
		],

		imagetextblockadditionalclass: 'coverboard front',
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: '',
				imgsrc: imgpath + "bean.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pbean,
			},
			{
				imagediv: 'image_tags img_tag_2',
				imgclass: '',
				imgsrc: imgpath + "peas.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.ppea,
			},
			{
				imagediv: 'image_tags img_tag_3',
				imgclass: '',
				imgsrc: imgpath + "chair.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pseat,
			},
			{
				imagediv: 'image_tags img_tag_4',
				imgclass: '',
				imgsrc: imgpath + "meat.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pmeat2,
			},
			{
				imagediv: 'image_tags img_tag_5',
				imgclass: '',
				imgsrc: imgpath + "wheat.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pwheat,
			},
			{
				imagediv: 'image_tags img_tag_6',
				imgclass: '',
				imgsrc: imgpath + "steam.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.psteam,
			},
		],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole-background-gradient-shore',

		uppertextblockadditionalclass: 'top_text',
		uppertextblock : [
			{
				textdata : data.string.p2text4,
				textclass : 'sniglet my_font_very_big header_ee'
			},
			{
				textdata : data.string.p2text2,
				textclass : 'sniglet my_font_medium hinttext'
			}
		],

		imagetextblockadditionalclass: 'coverboard front',
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: '',
				imgsrc: imgpath + "sleep.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.psleep,
			},
			{
				imagediv: 'image_tags img_tag_2',
				imgclass: '',
				imgsrc: imgpath + "sheep.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.psheep,
			},
			{
				imagediv: 'image_tags img_tag_3',
				imgclass: '',
				imgsrc: imgpath + "wheel.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pwheel,
			},
			{
				imagediv: 'image_tags img_tag_4',
				imgclass: '',
				imgsrc: imgpath + "knee.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pknee,
			},
			{
				imagediv: 'image_tags img_tag_5',
				imgclass: '',
				imgsrc: imgpath + "jeep.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pjeep,
			},
			{
				imagediv: 'image_tags img_tag_6',
				imgclass: '',
				imgsrc: imgpath + "feet.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pfeet,
			},
		],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_sleep;
	var sound_counter =0;
	var is_last_page= false;
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		var imgClk1,imgClk2,imgClk3,imgClk4,imgClk5,imgClk6  = false;

		switch (countNext) {
		case 0:
			sound_player(sound_1);
			setTimeout(function(){
				$nextBtn.show(0);
			},6000);
			break;
		case 1:
			// $prevBtn.show(0);
			// rand_colors.shufflearray();
			for(var m=1; m<7; m++){
				$('.img_tag_'+m).css('background-color', rand_colors[m-1]);
			}
			current_sound.stop();
			current_sound = sound_2;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				current_sound.stop();
				current_sound = sound_click;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					sound_counter =0;
					$prevBtn.show(0);
					for(var m=1; m<7; m++){
						img_click('.img_tag_'+m, ea_gr);
					}
					// $nextBtn.show(0);
				});
			});
			break;
		case 2:
			$prevBtn.show(0);
			is_last_page= true;
			// rand_colors.shufflearray();
			for(var m=1; m<7; m++){
				$('.img_tag_'+m).css('background-color', rand_colors[m-1]);
			}
			current_sound.stop();
			current_sound = sound_5;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				current_sound.stop();
				current_sound = sound_click;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					sound_counter =0;
				$prevBtn.show(0);
				for(var m=1; m<7; m++){
					img_click('.img_tag_'+m, ee_gr);
				}
				// ole.footerNotificationHandler.pageEndSetNotification();
				});
			});
			break;
		default:
			// $nextBtn.show(0);
			// $prevBtn.show(0);
			break;
		}
		function play_chain(sound_gr_custom){
			if(sound_counter<sound_gr_custom.length){
				current_sound=sound_gr_custom[sound_counter];
				current_sound.play();
				sound_counter++;
				$('.img_tag_'+sound_counter+'>*').addClass('highlight_obj');
				current_sound.bindOnce('ended', function(){
					setTimeout(function(){
						$('.img_tag_'+sound_counter+'>*').removeClass('highlight_obj');
						play_chain(sound_gr_custom);
					}, 1000);
				});
			}
			else{
				if(!is_last_page){
					$nextBtn.show(0);
				} else {
					ole.footerNotificationHandler.lessonEndSetNotification();
				}
				$('.image_tags').css('pointer-events', 'all');
				return true;
			}
		}
		function img_click(class_name, sound_gr_custom){
			$(class_name).click(function(){
				current_sound.stop();
				var str_name = $(class_name).attr('class').toString().replace(/image_tags/g, '');
				str_name = str_name.replace(/ /g, '');
				var new_count = parseInt(str_name.substr(str_name.length - 1))-1;
				console.log(new_count);
				new_count==0?imgClk1=true:new_count==1?imgClk2=true:new_count==2?imgClk3=true:
				new_count==3?imgClk4=true:new_count==4?imgClk5=true:new_count==5?imgClk6=true:"";
				if((imgClk1==true) &&(imgClk2==true) &&(imgClk3==true) &&(imgClk4==true) &&(imgClk5==true) &&(imgClk6==true)){
					countNext==1?$nextBtn.show(0):ole.footerNotificationHandler.pageEndSetNotification();;
				}
				current_sound = sound_gr_custom[new_count];
				current_sound.play();
			});
		}
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}


	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

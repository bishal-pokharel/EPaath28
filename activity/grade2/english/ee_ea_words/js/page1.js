var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "page1/recording1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "page1/recording2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "page1/recording3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "page1/recording4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "page1/recording5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "page1/recording6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "page1/recording7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "eaasin.ogg"));
var sound_9 = new buzz.sound((soundAsset + "eeasin.ogg"));
var sound_10 = new buzz.sound((soundAsset + "page1/recording10.ogg"));


var sound_but = new buzz.sound((soundAsset + "page1/but.ogg"));

var sound_meat = new buzz.sound((soundAsset + "meat.ogg"));
var sound_sweet = new buzz.sound((soundAsset + "sweet.ogg"));

var sound_sea = new buzz.sound((soundAsset + "sea.ogg"));
var sound_leaf = new buzz.sound((soundAsset + "leaf.ogg"));
var sound_tea = new buzz.sound((soundAsset + "tea.ogg"));
var sound_bee = new buzz.sound((soundAsset + "bee.ogg"));
var sound_tree = new buzz.sound((soundAsset + "tree.ogg"));
var sound_teeth = new buzz.sound((soundAsset + "teeth.ogg"));

var ea_ee_gr = [sound_8, sound_sea,sound_leaf,sound_tea,sound_9, sound_bee,sound_tree,sound_teeth];
var ea_ee_gr2 = [ sound_sea,sound_leaf,sound_tea,sound_9, sound_bee,sound_tree,sound_teeth];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'ole-background-gradient-amethyst',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'blue1',
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_b_1',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : 'sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		audiotextblockadditionalclass : "coverboard front ",
		audiotextblock : [
			{
				audioclass: 'item_label label_left',
				speakerclass: 'speaker_right',
				speakersrc: imgpath + "audio_icon.png",
				textclass : "sniglet sentence my_font_ultra_big",
				textdata : data.string.pmeat,
			},
			{
				audioclass: 'item_label label_right',
				speakerclass: 'speaker_right',
				speakersrc: imgpath + "audio_icon.png",
				textclass : "sniglet sentence my_font_ultra_big",
				textdata : data.string.psweet,
			}
		],

		imageblockadditionalclass : "coverboard",
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "meat",
				imgsrc : imgpath + "meat.png",
			},
			{
				imgclass : "sweet",
				imgsrc : imgpath + "sweet.png",
			}],
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_b_1',

		uppertextblockadditionalclass: 'center-text its_hidden',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : 'sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_b_1',

		uppertextblockadditionalclass: 'center-text its_hidden',
		uppertextblock : [{
			textdata : data.string.p1text3,
			textclass : 'sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_b_1',

		uppertextblockadditionalclass: 'coverboard front',
		uppertextblock : [{
			textdata : data.string.p1text4,
			textclass : 'center-text its_hidden sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}],

		imageblockadditionalclass : "coverboard_hidden",
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "meat_1",
				imgsrc : imgpath + "meat.png",
			},
			{
				imgclass : "sweet_1",
				imgsrc : imgpath + "sweet.png",
			}],
			imagelabels : [{
				imagelabelclass : "label_meat my_font_ultra_big sniglet",
				imagelabeldata : data.string.pmeat,
			},
			{
				imagelabelclass : "label_sweet my_font_ultra_big sniglet",
				imagelabeldata : data.string.psweet,
			}]
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'bottom-text',
		uppertextblock : [{
			textdata : data.string.p1text10,
			textclass : 'sniglet my_font_ultra_big description',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}],

		imageblockadditionalclass : "coverboard",
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "meat_1",
				imgsrc : imgpath + "meat.png",
			},
			{
				imgclass : "sweet_1",
				imgsrc : imgpath + "sweet.png",
			}],
			imagelabels : [
				{
					imagelabelclass : "label_meat my_font_ultra_big sniglet",
					imagelabeldata : data.string.p1text8,
					datahighlightflag : true,
					datahighlightcustomclass : 'hl_unique'
				},
				{
					imagelabelclass : "label_sweet my_font_ultra_big sniglet",
					imagelabeldata : data.string.p1text9,
					datahighlightflag : true,
					datahighlightcustomclass : 'hl_unique'
				}
			]
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'orange-text',
		uppertextblock : [{
			textdata : data.string.p1text5,
			textclass : 'sniglet my_font_ultra_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl_orange',
		}],
		imageblockadditionalclass : "coverboard",
		imageblock : [{
			imagelabels : [
				{
					imagelabelclass : "label_ea sniglet",
					imagelabeldata : data.string.pea,
				},
				{
					imagelabelclass : "label_ee sniglet",
					imagelabeldata : data.string.pee
				}
			]
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'coverboard',
		uppertextblock : [
			{
				textdata : data.string.p1text6,
				textclass : 'sniglet my_font_very_big header_ea'
			},
			{
				textdata : data.string.p1text7,
				textclass : 'sniglet my_font_very_big header_ee'
			}
		],

		imagetextblockadditionalclass: 'coverboard front',
		imagetextblock: [
			{
				imagediv: 'image_tags img_tag_1',
				imgclass: '',
				imgsrc: imgpath + "sea.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.psea,
			},
			{
				imagediv: 'image_tags img_tag_2',
				imgclass: '',
				imgsrc: imgpath + "leaf.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pleaf,
			},
			{
				imagediv: 'image_tags img_tag_3',
				imgclass: '',
				imgsrc: imgpath + "tea.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.ptea,
			},
			{
				imagediv: 'image_tags img_tag_5',
				imgclass: '',
				imgsrc: imgpath + "bee.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pbee,
			},
			{
				imagediv: 'image_tags img_tag_6',
				imgclass: '',
				imgsrc: imgpath + "tree.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.ptree,
			},
			{
				imagediv: 'image_tags img_tag_7',
				imgclass: '',
				imgsrc: imgpath + "teeth.png",
				textclass : "sniglet my_font_big",
				textdata : data.string.pteeth,
			},
			{
				imagediv: 'bottom_hinter',
				imgclass: '',
				imgsrc: imgpath + "worm.png",
				textdata : data.string.p1text11,
				textclass : 'sniglet my_font_medium'
			}
		],
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var current_sound = sound_meat;

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			sound_player(sound_1);
			setTimeout(function() {
				$nextBtn.show(0);
			},3000);
			break;
		case 1:
			sound_player(sound_2);
			setTimeout(function() {
				$nextBtn.show(0);
				$prevBtn.show(0);
			},5000);
			break;
		case 2:

			$('.item_label').css('pointer-events', 'none');
			$('.meat').fadeIn(2000, function(){
				sound_player(sound_meat);
				$('.label_left').fadeIn(2000, function(){
					$('.sweet').fadeIn(2000, function(){
						sound_player(sound_sweet);
						$('.label_right').fadeIn(2000, function(){
							$('.item_label').css('pointer-events', 'all');
							$nextBtn.show(0);
							$prevBtn.show(0);
						});
					});
				});
			});
			$('.label_left').click(function(){
				$('.item_label').css('pointer-events', 'none');
				sound_player(sound_meat);
				current_sound.bindOnce('ended', function(){
					$('.item_label').css('pointer-events', 'all');
				});
			});
			$('.label_right').click(function(){
				$('.item_label').css('pointer-events', 'none');
				sound_player(sound_sweet);
				current_sound.bindOnce('ended', function(){
					$('.item_label').css('pointer-events', 'all');
				});
			});
			break;
		case 3:

			$('.its_hidden').fadeIn(1500, function(){
				sound_player(sound_4);
			setTimeout(function () {
				$nextBtn.show(0);
				$prevBtn.show(0);
			},3500);

		});
			break;
		case 4:
			$('.its_hidden').fadeIn(1500, function(){
				// sound_player(sound_5);
				sound_player(sound_but);
				$nextBtn.show(0);
					$prevBtn.show(0);
			});
			break;
		case 5:

			$('.its_hidden').fadeIn(1500, function(){
				sound_player(sound_5);
				$('.its_hidden').delay(1000).animate({'top': '90%'}, 1500, function(){
					$('.coverboard_hidden').fadeIn(1000,function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					});
				});
			});
			break;
		case 6:

			sound_player(sound_6);
			$('.label_meat>.hl_unique:nth-child(2)').addClass('border_animation');
			$('.label_meat>.hl_unique:nth-child(2)').addClass('border_red');
			setTimeout(function(){
				$('.label_meat>.hl_unique:nth-child(2)').removeClass('border_animation');
			}, 2000);
			setTimeout(function(){
				$('.label_sweet>.hl_unique:nth-child(2)').addClass('border_animation');
				$('.label_sweet>.hl_unique:nth-child(2)').addClass('border_red');
			}, 2500);
			setTimeout(function(){
				$('.label_sweet>.hl_unique:nth-child(2)').removeClass('border_animation');
			}, 4500);
			setTimeout(function(){
				$('.description, .meat_1, .sweet_1, .label_sweet>.hl_unique:nth-child(1), .label_sweet>.hl_unique:nth-child(3), .label_meat>.hl_unique:nth-child(1), .label_meat>.hl_unique:nth-child(3)').addClass('fade_away_2');
			}, 5000);
			setTimeout(function(){
				$('.label_sweet, .label_meat').animate({'top':'50%'}, 700, function(){
					$nextBtn.show(0);
					$prevBtn.show(0);
				});
			}, 8000);
			break;
		case 7:
			sound_player(sound_7);
			setTimeout(function() {
				$nextBtn.show(0);
				$prevBtn.show(0);
			},5200);

			break;
		case 8:

			var sound_counter =0;
			play_chain();
			for(var m=1; m<8; m++){
				img_click('.img_tag_'+m);
			}
			// sound_player(sound_8);
			function play_chain(){
				if(sound_counter<ea_ee_gr.length){
					current_sound=ea_ee_gr[sound_counter];
					current_sound.play();
					$('.img_tag_'+sound_counter+'>img').addClass('highlight_image');
					sound_counter++;
					current_sound.bindOnce('ended', function(){
						setTimeout(function(){
							$('.img_tag_'+parseInt(sound_counter-1)+'>img').removeClass('highlight_image');
							play_chain();
						}, 1000);
					});
				}
				else{
					$('.bottom_hinter').delay(1000).fadeIn(500, function(){
						sound_player(sound_10);
						setTimeout(function(){
							ole.footerNotificationHandler.pageEndSetNotification();
								$prevBtn.show(0);
						},4000);

						$('.image_tags>img').css('pointer-events', 'all');
					});
					return true;
				}
			}
			function img_click(class_name){
				$(class_name+'>img').click(function(){
					current_sound.stop();
					var str_name = $(class_name).attr('class').toString().replace(/image_tags/g, '');
					str_name = str_name.replace(/ /g, '');
					var new_count = parseInt(str_name.substr(str_name.length - 1))-1;
					current_sound = ea_ee_gr2[new_count];
					current_sound.play();
				});
			}
			break;
		default:
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
		}
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 3:
			case 4:
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				$('.its_hidden').fadeOut(1500, function(){
					current_sound.stop();
					countNext++;
					templateCaller();
				});
				break;
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

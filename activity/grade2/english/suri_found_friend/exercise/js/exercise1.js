var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q1_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q1,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q1_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q1_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q1_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q1_o4,
			textclass : 'class4 options'
		}],
	},
	//slide 1	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q2_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q2,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q2_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q2_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q2_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q2_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 2
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q3_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q3,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q3_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q3_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q3_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q3_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 3
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q4_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q4,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q4_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q4_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q4_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q4_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 4
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q5_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q5,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q5_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q5_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q5_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q5_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 5
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q6_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q6,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q6_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q6_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q6_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q6_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 6
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q7_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q7,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q7_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q7_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q7_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q7_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 7
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q8_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q8,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q8_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q8_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q8_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q8_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 8
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q9_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q9,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q9_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q9_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q9_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q9_o4,
			textclass : 'class3 options'
		}],
	},
	//slide 9	
	{	
		contentblockadditionalclass: 'main_bg',
		uppertextblockadditionalclass: 'instruction_div',
		uppertextblock : [{}],
		hintimageblock:[
		{
			hintdata : data.string.e1_q10_hint
		}],
		optionsblockadditionalclass: 'img_options',
		optionsblock : [{
			textdata : data.string.e1_q10,
			textclass : 'instruction my_font_big',
		},{
			textdata : data.string.e1_q10_o1,
			textclass : 'class1 options'
		},
		{
			textdata : data.string.e1_q10_o2,
			textclass : 'class2 options'
		},
		{
			textdata : data.string.e1_q10_o3,
			textclass : 'class3 options'
		},
		{
			textdata : data.string.e1_q10_o4,
			textclass : 'class3 options'
		}],
	}
];

// content.shufflearray();


$(function () 
{	
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();
   
	rhino.init(10);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		
		//randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		parent.append(divs.splice(0, 1)[0]);
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html(data.string.e1_title);
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$(this).css({
					'border': '3px solid #FCD172',
					'background-color': '#6EB260',
					'color': 'white'
				});
                $(this).append("<img class='correctwrongImg' src='images/correct.png'>");
                play_correct_incorrect_sound(1);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(".hint").css("background-color", "#A8CAE1");
				$(this).css({
					'background-color': '#BA6B82',
					'border': 'none'
				});
				wrong_clicked = true;
                $(this).append("<img class='correctwrongImg' src='images/wrong.png'>");
                play_correct_incorrect_sound(0);
			}
		}); 
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 13){
			templateCaller();
			rhino.gotoNext();
		}		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
});
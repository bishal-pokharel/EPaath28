var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "suri.ogg"));
var sound_1 = new buzz.sound((soundAsset + "sound1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "sound2.ogg"));
var sound_2b = new buzz.sound((soundAsset + "sound3.ogg"));
var sound_3 = new buzz.sound((soundAsset + "sound4.ogg"));
var sound_3b = new buzz.sound((soundAsset + "sound5.ogg"));
var sound_4 = new buzz.sound((soundAsset + "sound6.ogg"));



var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblockadditionalclass: 'bg_story_0',
		contentblocknocenteradjust : true,
	
		uppertextblockadditionalclass : 'lesson-title',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_1',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_2',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text2b,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_5',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text3b,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_6',
	
		storytextblockadditionalclass : "bottom_para my_font_ultra_big",
	
		storytextblock : [
		{
			textclass : "text_story text_1 text_on",
			textdata : data.string.p1text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "audio_icon",
					imgsrc : imgpath + "audio_icon.png",
				}
			]
		}]
	},

];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
    loadTimelineProgress($total_page,countNext+1);
	var countNext = 0;
	var $total_page = 7;
	var last_page = false;
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		vocabcontroller.findwords(countNext);
        switch (countNext) {
		case 0:
			sound_0.play().bind("ended",function(){
                $nextBtn.show(0);
            });
			break;
		case 1:
			for_all_slides(1, 'text_1', sound_1, false);
			break;
		case 2:
			for_all_slides(2, 'text_1', sound_2, false);
			break;
		case 3:
			for_all_slides(3, 'text_1', sound_2b, false);
			break;
		case 4:
			for_all_slides(4, 'text_1', sound_3, false);
			break;
		case 5:
			for_all_slides(5, 'text_1', sound_3b, false);
			break;
		case 6:
			for_all_slides(6, 'text_1', sound_4, true);
			break;
		default:
			break;
		}
		
		function for_all_slides(slide_no, text_class, my_sound_data, last_page_flag){
			var $textblack = $("."+text_class);
			sound_data =  my_sound_data;
			var current_text = $textblack.html();
			// current_text.replace(/<.*>/, '');
			play_text($textblack, current_text);
    		sound_data.bind('ended', function(){
    			change_slides(last_page_flag);
			});
			$('.audio_icon').click(function(){
    			$prevBtn.hide(0);
    			$nextBtn.hide(0);
				sound_data.play();
				$('.audio_icon').css('pointer-events','none');
				$('#span_speec_text').addClass('is_playing');
    			sound_data.bind('ended', function(){
	    			change_slides(last_page_flag);
					$('.audio_icon').css('pointer-events','all');
					$('#span_speec_text').removeClass('is_playing');
				});
			});
		}
		function change_slides(last_page_flag){
			var checking_interval = setInterval(function(){
				if(textanimatecomplete){
					if(!last_page_flag){
	    				$nextBtn.show(0);
		    		} else{
		    			ole.footerNotificationHandler.pageEndSetNotification();
		    		}
    				$('.audio_icon').show(0);
	    			$prevBtn.show(0);
					$('.audio_icon').css('pointer-events','all');
	    			clearInterval(checking_interval);
				} else{
					$('.audio_icon').css('pointer-events','none');
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}
			},50);
		}
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
        loadTimelineProgress($total_page,countNext+1);

		generalTemplate();

	}


	$nextBtn.on("click", function() {
		countNext++;
        templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	// total_page = 29;
	templateCaller();

	/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		var stags_counter = 1;
		var stags = message.split(/[<>]/);
		var gt_encounters=[];
	  	if (0 < message.length) {
		  	textanimatecomplete = false;
		  	var nextText = message.substring(0,1);
		  	if(nextText == "<" ){
		  		gt_encounters.push($span_speec_text.html().length);
		  		// $span_speec_text.append('<'+stags[stags_counter]+'>');
		  		message = message.substring(stags[stags_counter].length+2, message.length);
		  		stags_counter+=2;
		  	}else{
		  		$span_speec_text.append(nextText);
		  		message = message.substring(1, message.length);
		  	}
		  	$this.html($span_speec_text);
		  	$this.append(message);
		    setTimeout(function () {
		    	show_text($this,  $span_speec_text, message, interval);
		  	}, interval);
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text/*, sound_data*/){
		$this.html("<span id='span_speec_text'></span>"+text);
		$prevBtn.hide(0);
		var $span_speec_text = $("#span_speec_text");
		// $this.css("background-color", "#faf");
		show_text($this, $span_speec_text,text, 65);	// 65 ms is the interval found out by hit and trial
		sound_data.play();
		sound_data.bind('ended', function(){
			// $this.removeClass('text_on');
			// $this.addClass('text_off');
			sound_data.unbind('ended');
			soundplaycomplete = true;
			ternimatesound_play_animate(text);
		});

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
       				vocabcontroller.findwords(countNext);
					// if((countNext+1) == content.length){
						// ole.footerNotificationHandler.pageEndSetNotification();
					// }else{
						// $nextBtn.show(0);
					// }
					// if(countNext>0){
						// $prevBtn.show(0);
					// }
				}
			}, 250);
		}
	}
	
	
	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

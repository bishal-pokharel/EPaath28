Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques1,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q1ansd,
					}]
			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques2,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q2ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q2ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q2ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q2ansd,
					}]
			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques3,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q3ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q3ansd,
					}]
			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques4,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q4ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q4ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q4ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q4ansd,
					}]
			}
		]
	},
	//ex5
	{
    flexblockcontainers: [
  		{
  			flexblockadditionalclass: "flexcontainerblock100",
  			flexblock:[
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "41"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "42"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "43"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column blank",
  					flexblockcolumn:[{
  						textdata: ""
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "45"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "46"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "47"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "48"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "49"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "50"
  					}
  					]
  				}

  			]
  		}
  	],
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques5,
        optdivextra: "horidiv",
        ques_class: "newques",
				exeoptions: [
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class3",
						optdata: data.string.q5ansc,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class4",
						optdata: data.string.q5ansd,
					}]
			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques6,
        sentdata: data.string.sentques6,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q6ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q6ansd,
					}]
			}
		]
	},
	//ex7
	{
    flexblockcontainers: [
  		{
  			flexblockadditionalclass: "flexcontainerblock200",
  			flexblock:[
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "2"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "22"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "32"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2 blank",
  					flexblockcolumn:[{
  						textdata: "_"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "52"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "62"
  					}
  					]
  				}
  			]
  		}
  	],
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques7,
        sentdata: data.string.sentques7,
        optdivextra: "sidediv",
        ques_class: "thinques",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q7ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q7ansd,
					}]
			}
		]
	},
	//ex8
  {
    flexblockcontainers: [
  		{
  			flexblockadditionalclass: "flexcontainerblock200",
  			flexblock:[
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "27"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2 blank",
  					flexblockcolumn:[{
  						textdata: "_"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "47"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "57"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "67"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column2",
  					flexblockcolumn:[{
  						textdata: "77"
  					}
  					]
  				}
  			]
  		}
  	],
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques8,
        sentdata: data.string.sentques8,
        optdivextra: "sidediv",
        ques_class: "thinques",

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q8ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q8ansd,
					}]
			}
		]
	},
	//ex9
  {
    flexblockcontainers: [
  		{
  			flexblockadditionalclass: "flexcontainerblock100",
  			flexblock:[
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "31"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "32"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "33"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "34"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "35"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "36"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "37"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "38"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column blank",
  					flexblockcolumn:[{
  						textdata: "_"
  					}
  					]
  				},
  				{
  					flexboxcolumnclass: "column",
  					flexblockcolumn:[{
  						textdata: "40"
  					}
  					]
  				}

  			]
  		}
  	],
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques9,
        optdivextra: "horidiv",
        ques_class: "newques",
				exeoptions: [
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class3",
						optdata: data.string.q9ansc,
					},
					{
            startposextra: "newstart",
            optcontainerextra: "horicontain",
						forshuffle: "class4",
						optdata: data.string.q9ansd,
					}]
			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques10,
        sentdata: data.string.sentques10,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q10ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q10ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q10ansc,
					},
					{
						forshuffle: "class4",
						optdata: data.string.q10ansd,
					}]
			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $("#activity-page-next-btn-enabled");
	var $prevBtn  = $("#activity-page-prev-btn-enabled");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new TreasureTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }


		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62fff");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						$(this).siblings(".corctopt").show(0);
						//$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});


	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		singletext:[
			{
				textclass: "covertext",
				textdata: data.lesson.chapter
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cover",
					imgid : 'cover',
					imgsrc: ""
				},
				{
					imgclass: "lowfish",
					imgid : 'fishgif',
					imgsrc: ""
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: "bg",
		speechbox:[{
			speechbox: 'sp-1',
			textclass: "answer",
			textdata: data.string.p1text1,
			imgclass: '',
			imgid : 'tb-1',
			imgsrc: '',
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "lowfish",
					imgid : 'fishgif',
					imgsrc: ""
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass: "bg",
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexboxcolumnclass: "column",
						flexblockcolumn:[{
							flexboxrowclass :"rownormal1 bext1",
							textdata: "1"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[{
						flexboxrowclass :"rownormal1 bext2",
						textdata: "2"
					}
				]
			},
			{
				flexboxcolumnclass: "column",
				flexblockcolumn:[{
					flexboxrowclass :"rownormal1 bext3",
					textdata: "3"
				}
			]
		},
		{
			flexboxcolumnclass: "column",
			flexblockcolumn:[{
				flexboxrowclass :"rownormal1 bext4",
				textdata: "4"
			}
		]
	},
	{
		flexboxcolumnclass: "column",
		flexblockcolumn:[{
			flexboxrowclass :"rownormal1 bext5",
			textdata: "5"
		}
	]
},
{
	flexboxcolumnclass: "column",
	flexblockcolumn:[{
		flexboxrowclass :"rownormal1 bext6",
		textdata: "6"
	}
]
},
{
	flexboxcolumnclass: "column",
	flexblockcolumn:[{
		flexboxrowclass :"rownormal1 bext7",
		textdata: "7"
	}
]
},
{
	flexboxcolumnclass: "column",
	flexblockcolumn:[{
		flexboxrowclass :"rownormal1 bext8",
		textdata: "8"
	}
]
},
{
	flexboxcolumnclass: "column",
	flexblockcolumn:[{
		flexboxrowclass :"rownormal1 bext9",
		textdata: "9"
	}
]
},
{
	flexboxcolumnclass: "column",
	flexblockcolumn:[{
		flexboxrowclass :"rownormal1 bext10",
		textdata: "10"
	}
]
}

]
}
]
},
// slide3
{
	contentblockadditionalclass: "bg",

	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p1text2,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "lowfish",
				imgid : 'fishgif',
				imgsrc: ""
			}
		]
	}]
},
// slide4
{
	contentblockadditionalclass: "bg",
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext1",
							textdata: "1"
						},
						{
							flexboxrowclass :"rownormal2 bext11",
							textdata: "11"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext2",
							textdata: "2"
						},
						{
							flexboxrowclass :"rownormal2 bext12",
							textdata: "12"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext3",
							textdata: "3"
						},
						{
							flexboxrowclass :"rownormal2 bext13",
							textdata: "13"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext4",
							textdata: "4"
						},
						{
							flexboxrowclass :"rownormal2 bext14",
							textdata: "14"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext5",
							textdata: "5"
						},
						{
							flexboxrowclass :"rownormal2 bext15",
							textdata: "15"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext6",
							textdata: "6"
						},
						{
							flexboxrowclass :"rownormal2 bext16",
							textdata: "16"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext7",
							textdata: "7"
						},
						{
							flexboxrowclass :"rownormal2 bext17",
							textdata: "17"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext8",
							textdata: "8"
						},
						{
							flexboxrowclass :"rownormal2 bext18",
							textdata: "18"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext9",
							textdata: "9"
						},
						{
							flexboxrowclass :"rownormal2 bext19",
							textdata: "19"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"rownormal1 bext10",
							textdata: "10"
						},
						{
							flexboxrowclass :"rownormal2 bext20",
							textdata: "20"
						}
					]
				}

			]
		}
	]
},
// slide5
{
	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p1text3,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "lowfish",
				imgid : 'fishgif',
				imgsrc: ""
			}
		]
	}]
},
// slide6
{
	contentblockadditionalclass: "bg",
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock200",
			flexblock:[
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow11",
							textdata: "11"
						},
						{
							flexboxrowclass :"tabletext tbrow12",
							textdata: "12"
						},
						{
							flexboxrowclass :"tabletext tbrow13",
							textdata: "13"
						},
						{
							flexboxrowclass :"tabletext tbrow14",
							textdata: "14"
						},
						{
							flexboxrowclass :"tabletext tbrow15",
							textdata: "15"
						},
						{
							flexboxrowclass :"tabletext tbrow16",
							textdata: "16"
						},
						{
							flexboxrowclass :"tabletext tbrow17",
							textdata: "17"
						},
						{
							flexboxrowclass :"tabletext tbrow18",
							textdata: "18"
						},
						{
							flexboxrowclass :"tabletext tbrow19",
							textdata: "19"
						},
						{
							flexboxrowclass :"tabletext tbrow20",
							textdata: "20"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow11",
							textdata: "ELEVEN"
						},
						{
							flexboxrowclass :"tabletext tbrow12",
							textdata: "TWELVE"
						},
						{
							flexboxrowclass :"tabletext tbrow13",
							textdata: "THIRTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow14",
							textdata: "FOURTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow15",
							textdata: "FIFTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow16",
							textdata: "SIXTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow17",
							textdata: "SEVENTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow18",
							textdata: "EIGHTEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow19",
							textdata: "NINETEEN"
						},
						{
							flexboxrowclass :"tabletext tbrow20",
							textdata: "TWENTY"
						}
					]
				}

			]
		}
	]
},
// slide7
{
	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p1text4,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "lowfish",
				imgid : 'fishgif',
				imgsrc: ""
			}
		]
	}]
},
// slide8
{
	contentblockadditionalclass: "bg",
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock200",
			flexblock:[
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow11",
							textdata: "11"
						},
						{
							flexboxrowclass :"tabletext tbrow12",
							textdata: "12"
						},
						{
							flexboxrowclass :"tabletext tbrow13",
							textdata: "13"
						},
						{
							flexboxrowclass :"tabletext tbrow14",
							textdata: "14"
						},
						{
							flexboxrowclass :"tabletext tbrow15",
							textdata: "15"
						},
						{
							flexboxrowclass :"tabletext tbrow16",
							textdata: "16"
						},
						{
							flexboxrowclass :"tabletext tbrow17",
							textdata: "17"
						},
						{
							flexboxrowclass :"tabletext tbrow18",
							textdata: "18"
						},
						{
							flexboxrowclass :"tabletext tbrow19",
							textdata: "19"
						},
						{
							flexboxrowclass :"tabletext tbrow20",
							textdata: "20"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow11",
							textdata: "ELEVEN"
						},
						{
							flexboxrowclass :"tabletext tbrow12",
							textdata: "TWELVE"
						},
						{
							flexboxrowclass :"tabletext tbrow13",
							textdata: "THIR#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow14",
							textdata: "FOUR#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow15",
							textdata: "FIF#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow16",
							textdata: "SIX#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow17",
							textdata: "SEVEN#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow18",
							textdata: "EIGH#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow19",
							textdata: "NINE#TEEN@",
							datahighlightflag: true,
							datahighlightcustomclass:"blueteen"
						},
						{
							flexboxrowclass :"tabletext tbrow20",
							textdata: "TWENTY"
						}
					]
				}

			]
		}
	]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var countCounter;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fishgif", src: imgpath+"talking-fish_yellow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fishpng", src: imgpath+"talking-fish_yellow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fishgif2", src: imgpath+"talking-fish.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"talking-fish.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s8.ogg"},
			{id: "s1", src: soundAsset+"1.ogg"},
			{id: "s2", src: soundAsset+"2.ogg"},
			{id: "s3", src: soundAsset+"3.ogg"},
			{id: "s4", src: soundAsset+"4.ogg"},
			{id: "s5", src: soundAsset+"5.ogg"},
			{id: "s6", src: soundAsset+"6.ogg"},
			{id: "s7", src: soundAsset+"7.ogg"},
			{id: "s8", src: soundAsset+"8.ogg"},
			{id: "s9", src: soundAsset+"9.ogg"},
			{id: "s10", src: soundAsset+"10.ogg"},
			{id: "s11", src: soundAsset+"11.ogg"},
			{id: "s12", src: soundAsset+"12.ogg"},
			{id: "s13", src: soundAsset+"13.ogg"},
			{id: "s14", src: soundAsset+"14.ogg"},
			{id: "s15", src: soundAsset+"15.ogg"},
			{id: "s16", src: soundAsset+"16.ogg"},
			{id: "s17", src: soundAsset+"17.ogg"},
			{id: "s18", src: soundAsset+"18.ogg"},
			{id: "s19", src: soundAsset+"19.ogg"},
			{id: "s20", src: soundAsset+"20.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player_fish("sound_0", 1, 'lowfish', 1);
			break;
			case 1:
			sound_player_fish("sound_1", 1, 'lowfish', 1);
			break;
			case 2:
			$(".rownormal1").css("opacity","0");
			countCounter = 1;
			countNums(10);
			break;
			case 3:
			sound_player_fish("sound_2", 1, 'lowfish', 1);
			break;
			case 4:
			$(".rownormal2").css("opacity","0");
			countCounter = 11;
			countNums(20);
			break;
			case 5:
			sound_player_fish("sound_3", 1, 'lowfish', 1);
			break;
			case 6:
			$(".tabletext").css("opacity","0");
			countCounter = 11;
			countTabs(20);
			break;
			case 7:
			sound_player_fish("sound_4", 1, 'lowfish', 1);
			break;
			case 8:
			countCounter = 13;
			countTabs2(19);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_fish(sound_id, next,classname,fishmike){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(fishmike){
				$('.'+classname).attr('src',preload.getResult('fishpng').src);
			}
			else{
				$('.'+classname).attr('src',preload.getResult('fish').src);
			}
			if(next) navigationcontroller();
		});
	}


	function countNums(count){
		$(".bext"+countCounter).css("opacity","1");
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
			countNums(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function countTabs(count){
		$(".tbrow"+countCounter).css("opacity","1");
		$(".tbrow"+(countCounter-1) + " .fishmove").remove(0);
		$(".tbrow"+countCounter).append("<img class='fishmove' src= '"+ preload.getResult('fish').src +"'>");
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
			countTabs(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function countTabs2(count){
		$(".tbrow"+countCounter).addClass("hightab");
		$(".tbrow"+(countCounter-1) + " .fishmove").remove(0);
		$(".tbrow"+countCounter).append("<img class='fishmove' src= '"+ preload.getResult('fish').src +"'>");
		console.log("s"+countCounter);
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
			countTabs2(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		current_sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

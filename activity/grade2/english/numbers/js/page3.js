var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
// slide0
{
	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p3text1,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "lowfish32",
			imgid : 'fishgif',
			imgsrc: ""
		}
	]
}]
},
// slide1
{
	contentblockadditionalclass: "bg",
	headerblock:[
		{
			textdata: data.string.p3text2
		}
	],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "1"
						},
						{
							textdata: "11"
						},
						{
							textdata: "21"
						},
						{
							textdata: "31"
						},
						{
							textdata: "41"
						},
						{
							textdata: "51"
						},
						{
							textdata: "61"
						},
						{
							textdata: "71"
						},
						{
							textdata: "81"
						},
						{
							textdata: "91"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "2"
						},
						{
							textdata: "12"
						},
						{
							textdata: "22"
						},
						{
							textdata: "32"
						},
						{
							textdata: "42"
						},
						{
							textdata: "52"
						},
						{
							textdata: "62"
						},
						{
							textdata: "72"
						},
						{
							textdata: "82"
						},
						{
							textdata: "92"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "3"
						},
						{
							textdata: "13"
						},
						{
							textdata: "23"
						},
						{
							textdata: "33"
						},
						{
							textdata: "43"
						},
						{
							textdata: "53"
						},
						{
							textdata: "63"
						},
						{
							textdata: "73"
						},
						{
							textdata: "83"
						},
						{
							textdata: "93"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "4"
						},
						{
							textdata: "14"
						},
						{
							textdata: "24"
						},
						{
							textdata: "34"
						},
						{
							textdata: "44"
						},
						{
							textdata: "54"
						},
						{
							textdata: "64"
						},
						{
							textdata: "74"
						},
						{
							textdata: "84"
						},
						{
							textdata: "94"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "5"
						},
						{
							textdata: "15"
						},
						{
							textdata: "25"
						},
						{
							textdata: "35"
						},
						{
							textdata: "45"
						},
						{
							textdata: "55"
						},
						{
							textdata: "65"
						},
						{
							textdata: "75"
						},
						{
							textdata: "85"
						},
						{
							textdata: "95"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "6"
						},
						{
							textdata: "16"
						},
						{
							textdata: "26"
						},
						{
							textdata: "36"
						},
						{
							textdata: "46"
						},
						{
							textdata: "56"
						},
						{
							textdata: "66"
						},
						{
							textdata: "76"
						},
						{
							textdata: "86"
						},
						{
							textdata: "96"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "7"
						},
						{
							textdata: "17"
						},
						{
							textdata: "27"
						},
						{
							textdata: "37"
						},
						{
							textdata: "47"
						},
						{
							textdata: "57"
						},
						{
							textdata: "67"
						},
						{
							textdata: "77"
						},
						{
							textdata: "87"
						},
						{
							textdata: "97"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "8"
						},
						{
							textdata: "18"
						},
						{
							textdata: "28"
						},
						{
							textdata: "38"
						},
						{
							textdata: "48"
						},
						{
							textdata: "58"
						},
						{
							textdata: "68"
						},
						{
							textdata: "78"
						},
						{
							textdata: "88"
						},
						{
							textdata: "98"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "9"
						},
						{
							textdata: "19"
						},
						{
							textdata: "29"
						},
						{
							textdata: "39"
						},
						{
							textdata: "49"
						},
						{
							textdata: "59"
						},
						{
							textdata: "69"
						},
						{
							textdata: "79"
						},
						{
							textdata: "89"
						},
						{
							textdata: "99"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							flexboxrowclass :"bext1",
							textdata: "10"
						},
						{
							flexboxrowclass :"bext2",
							textdata: "20"
						},
						{
							flexboxrowclass :"bext3",
							textdata: "30"
						},
						{
							flexboxrowclass :"bext4",
							textdata: "40"
						},
						{
							flexboxrowclass :"bext5",
							textdata: "50"
						},
						{
							flexboxrowclass :"bext6",
							textdata: "60"
						},
						{
							flexboxrowclass :"bext7",
							textdata: "70"
						},
						{
							flexboxrowclass :"bext8",
							textdata: "80"
						},
						{
							flexboxrowclass :"bext9",
							textdata: "90"
						},
						{
							flexboxrowclass :"bext10",
							textdata: "100"
						}
					]
				}

			]
		}
	]
},
// slide2
{
	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p2text2,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "lowfish1",
			imgid : 'fishgif',
			imgsrc: ""
		},
		{
			imgclass: "lowfish2",
			imgid : 'fishgif',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentblockadditionalclass: "bg",
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock200",
			flexblock:[
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow1",
							textdata: "10"
						},
						{
							flexboxrowclass :"tabletext tbrow2",
							textdata: "20"
						},
						{
							flexboxrowclass :"tabletext tbrow3",
							textdata: "30"
						},
						{
							flexboxrowclass :"tabletext tbrow4",
							textdata: "40"
						},
						{
							flexboxrowclass :"tabletext tbrow5",
							textdata: "50"
						},
						{
							flexboxrowclass :"tabletext tbrow6",
							textdata: "60"
						},
						{
							flexboxrowclass :"tabletext tbrow7",
							textdata: "70"
						},
						{
							flexboxrowclass :"tabletext tbrow8",
							textdata: "80"
						},
						{
							flexboxrowclass :"tabletext tbrow9",
							textdata: "90"
						},
						{
							flexboxrowclass :"tabletext tbrow10",
							textdata: "100"
						}
					]
				},
				{
					flexboxcolumnclass: "column",
					flexblockcolumn:[
						{
							flexboxrowclass :"tabletext tbrow1",
							textdata: "TEN"
						},
						{
							flexboxrowclass :"tabletext tbrow2",
							textdata: "TWENTY"
						},
						{
							flexboxrowclass :"tabletext tbrow3",
							textdata: "THIRTY"
						},
						{
							flexboxrowclass :"tabletext tbrow4",
							textdata: "FORTY"
						},
						{
							flexboxrowclass :"tabletext tbrow5",
							textdata: "FIFTY"
						},
						{
							flexboxrowclass :"tabletext tbrow6",
							textdata: "SIXTY"
						},
						{
							flexboxrowclass :"tabletext tbrow7",
							textdata: "SEVENTY"
						},
						{
							flexboxrowclass :"tabletext tbrow8",
							textdata: "EIGHTY"
						},
						{
							flexboxrowclass :"tabletext tbrow9",
							textdata: "NINETY"
						},
						{
							flexboxrowclass :"tabletext tbrow10",
							textdata: "HUNDRED"
						}
					]
				}

			]
		}
	]
},
// slide4
{
	contentblockadditionalclass: "bg",
	speechbox:[{
		speechbox: 'sp-1',
		textclass: "answer",
		textdata: data.string.p3text3,
		imgclass: '',
		imgid : 'tb-1',
		imgsrc: '',
	}],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "lowfish32",
			imgid : 'fishgif',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	contentblockadditionalclass: "bg",
	singletext:[
	{
		textclass: "numtext",
		textdata: "FORTY-ONE"
	},
	{
		textclass: "thenumb",
		textdata: "41"
	}
]
},
// slide6
{
	contentblockadditionalclass: "bg",
	singletext:[
	{
		textclass: "text1",
		textdata: data.string.p3text4
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "lowfish32",
			imgid : 'fishgif',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	contentblockadditionalclass: "bg",
	singletext:[
	{
		textclass: "numtext",
		textdata: "SIXTY-SEVEN"
	},
	{
		textclass: "thenumb",
		textdata: "67"
	}
]
},
// slide8
{
	contentblockadditionalclass: "bg",
	singletext:[
	{
		textclass: "text1",
		textdata: data.string.p3text5
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "lowfish32",
			imgid : 'fishgif',
			imgsrc: ""
		}
	]
}]
},
// slide9
{
	contentblockadditionalclass: "bg",
	headerblock:[
		{
			textdata: data.string.p3text2
		}
	],
	flexblockcontainers: [
		{
			flexblockadditionalclass: "flexcontainerblock100",
			flexblock:[
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "1"
						},
						{
							textdata: "11"
						},
						{
							textdata: "21"
						},
						{
							textdata: "31"
						},
						{
							textdata: "41"
						},
						{
							textdata: "51"
						},
						{
							textdata: "61"
						},
						{
							textdata: "71"
						},
						{
							textdata: "81"
						},
						{
							textdata: "91"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "2"
						},
						{
							textdata: "12"
						},
						{
							textdata: "22"
						},
						{
							textdata: "32"
						},
						{
							textdata: "42"
						},
						{
							textdata: "52"
						},
						{
							textdata: "62"
						},
						{
							textdata: "72"
						},
						{
							textdata: "82"
						},
						{
							textdata: "92"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "3"
						},
						{
							textdata: "13"
						},
						{
							textdata: "23"
						},
						{
							textdata: "33"
						},
						{
							textdata: "43"
						},
						{
							textdata: "53"
						},
						{
							textdata: "63"
						},
						{
							textdata: "73"
						},
						{
							textdata: "83"
						},
						{
							textdata: "93"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "4"
						},
						{
							textdata: "14"
						},
						{
							textdata: "24"
						},
						{
							textdata: "34"
						},
						{
							textdata: "44"
						},
						{
							textdata: "54"
						},
						{
							textdata: "64"
						},
						{
							textdata: "74"
						},
						{
							textdata: "84"
						},
						{
							textdata: "94"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "5"
						},
						{
							textdata: "15"
						},
						{
							textdata: "25"
						},
						{
							textdata: "35"
						},
						{
							textdata: "45"
						},
						{
							textdata: "55"
						},
						{
							textdata: "65"
						},
						{
							textdata: "75"
						},
						{
							textdata: "85"
						},
						{
							textdata: "95"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "6"
						},
						{
							textdata: "16"
						},
						{
							textdata: "26"
						},
						{
							textdata: "36"
						},
						{
							textdata: "46"
						},
						{
							textdata: "56"
						},
						{
							textdata: "66"
						},
						{
							textdata: "76"
						},
						{
							textdata: "86"
						},
						{
							textdata: "96"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "7"
						},
						{
							textdata: "17"
						},
						{
							textdata: "27"
						},
						{
							textdata: "37"
						},
						{
							textdata: "47"
						},
						{
							textdata: "57"
						},
						{
							textdata: "67"
						},
						{
							textdata: "77"
						},
						{
							textdata: "87"
						},
						{
							textdata: "97"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "8"
						},
						{
							textdata: "18"
						},
						{
							textdata: "28"
						},
						{
							textdata: "38"
						},
						{
							textdata: "48"
						},
						{
							textdata: "58"
						},
						{
							textdata: "68"
						},
						{
							textdata: "78"
						},
						{
							textdata: "88"
						},
						{
							textdata: "98"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							textdata: "9"
						},
						{
							textdata: "19"
						},
						{
							textdata: "29"
						},
						{
							textdata: "39"
						},
						{
							textdata: "49"
						},
						{
							textdata: "59"
						},
						{
							textdata: "69"
						},
						{
							textdata: "79"
						},
						{
							textdata: "89"
						},
						{
							textdata: "99"
						}
					]
				},
				{
					flexboxcolumnclass: "column1",
					flexblockcolumn:[
						{
							flexboxrowclass :"bext1",
							textdata: "10"
						},
						{
							flexboxrowclass :"bext2",
							textdata: "20"
						},
						{
							flexboxrowclass :"bext3",
							textdata: "30"
						},
						{
							flexboxrowclass :"bext4",
							textdata: "40"
						},
						{
							flexboxrowclass :"bext5",
							textdata: "50"
						},
						{
							flexboxrowclass :"bext6",
							textdata: "60"
						},
						{
							flexboxrowclass :"bext7",
							textdata: "70"
						},
						{
							flexboxrowclass :"bext8",
							textdata: "80"
						},
						{
							flexboxrowclass :"bext9",
							textdata: "90"
						},
						{
							flexboxrowclass :"bext10",
							textdata: "100"
						}
					]
				}

			]
		}
	]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;
	var countCounter;
	var secondCounter;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"talking-fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fishgif", src: imgpath+"talking-fish_yellow.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fishpng", src: imgpath+"talking-fish_yellow.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s7.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s8.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s9.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s10.ogg"},
			{id: "s1", src: soundAsset+"10.ogg"},
			{id: "s2", src: soundAsset+"20.ogg"},
			{id: "s3", src: soundAsset+"30.ogg"},
			{id: "s4", src: soundAsset+"40.ogg"},
			{id: "s5", src: soundAsset+"50.ogg"},
			{id: "s6", src: soundAsset+"60.ogg"},
			{id: "s7", src: soundAsset+"70.ogg"},
			{id: "s8", src: soundAsset+"80.ogg"},
			{id: "s9", src: soundAsset+"90.ogg"},
			{id: "s10", src: soundAsset+"100.ogg"}
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.pageEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.pageEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext){
			case 0:
			sound_player_fish("sound_0", 1, 'lowfish32', 1);
			break;
			case 1:
			countCounter = 1;
			countTabs2(10);
			break;
			case 2:
			sound_player_fish("sound_1", 1, 'lowfish1', 1);
			setTimeout(function(){	fishChanger('lowfish2',1);},2700);
			break;
			case 3:
			$(".tabletext").css("opacity","0");
			countCounter = 1;
			countTabs(10);
			break;
			case 4:
			sound_player_fish("sound_2", 1, 'lowfish32', 1);
			break;
			case 5:
			sound_player("sound_3", 1);
			break;
			case 6:
			sound_player_fish("sound_4", 1, 'lowfish32', 1);
			break;
			case 7:
			sound_player("sound_5", 1);
			break;
			case 8:
			sound_player_fish("sound_6", 1, 'lowfish32', 1);
			break;
			case 9:
			sound_player("sound_7", 1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}


	function sound_player_fish(sound_id, next,classname,fishmike){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(fishmike){
				$('.'+classname).attr('src',preload.getResult('fishpng').src);
			}
			else{
				$('.'+classname).attr('src',preload.getResult('fish').src);
			}
			if(next) navigationcontroller();
		});
	}
 function fishChanger(classname, fishmike){
	 if(fishmike){
		 $('.'+classname).attr('src',preload.getResult('fishpng').src);
	 }
	 else{
		 $('.'+classname).attr('src',preload.getResult('fish').src);
	 }
 }

	function countTabs(count){
		$(".tbrow"+countCounter).css("opacity","1");
		$(".tbrow"+(countCounter-1) + " .fishmove2").remove(0);
		$(".tbrow"+countCounter).append("<img class='fishmove2' src= '"+ preload.getResult('fish').src +"'>");
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
				countTabs(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function countTabs2(count){
		$(".bext"+countCounter).addClass("hightab");
		$(".bext"+(countCounter-1) + " .fishmove").remove(0);
		$(".bext"+countCounter).append("<img class='fishmove' src= '"+ preload.getResult('fish').src +"'>");
		console.log("s"+countCounter);
		sound_player("s"+countCounter, 0);
		setTimeout(function(){
			countCounter++;
			if(countCounter <= count)
				countTabs2(count);
			else {
				countCounter = 1;
				navigationcontroller();
			}
		}, 1500);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

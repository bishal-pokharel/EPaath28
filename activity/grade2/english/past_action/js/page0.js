var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-front',

		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title sniglet'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "frontboard",
					imgsrc : imgpath + "board.png",
				}
			],
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		flipperBlock: [{
			flipperclass: 'float-in',
			balloonSrc: imgpath + 'balloon.png',
			flippers: [{
				flipperID: 'present-flipper',
				sides: [{
					sideClass: 'front my_font_medium sniglet',
					titleText: data.string.p0text1,
					textdata: data.string.p0text2
				}]
			}]
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		flipperBlock: [{
			flipperclass: 'float-in',
			balloonSrc: imgpath + 'balloon.png',
			flippers: [{
				flipperID: 'present-flipper',
				sides: [{
					sideClass: 'front my_font_medium sniglet',
					titleText: data.string.p0text1,
					textdata: data.string.p0text3
				}]
			}]
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',

		flipperBlock: [{
			flipperclass: 'float-in',
			balloonSrc: imgpath + 'balloon.png',
			flippers: [{
				flipperID: 'present-flipper',
				sides: [{
					sideClass: 'front my_font_medium sniglet',
					titleText: data.string.p0text1,
					textdata: data.string.p0text4
				}]
			}]
		}]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var timeoutvar2 = null;


	var has_imagination = false;
	var imagination = null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			sound_player(sound_1);
			nav_button_controls(1500);
			break;
		case 1:
			$prevBtn.show(0);
			timeoutvar2 = setTimeout(function(){
				sound_player(sound_2);
			}, 4000);
			nav_button_controls(12000);
			break;
		case 2:
			$prevBtn.show(0);
			timeoutvar2 = setTimeout(function(){
				sound_player(sound_3);
			}, 4000);
			nav_button_controls(17000);
			break;
		case 3:
			$prevBtn.show(0);
			timeoutvar2 = setTimeout(function(){
				sound_player(sound_4);
			}, 4000);
			nav_button_controls(10000);
			break;
		default:
			$prevBtn.show(0);
			nav_button_controls(4000);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		current_sound.stop();
		switch(countNext){
			case 1:
			case 2:
				$prevBtn.hide(0);
				$nextBtn.hide(0);
				$('.fliper-block').removeClass('float-in').addClass('float-out');
				setTimeout(function(){
					countNext++;
					templateCaller();
				}, 4000);
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

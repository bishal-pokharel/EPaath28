var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p2_s0_1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p2_s1_1.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p2_s2_1.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p2_s3_1.ogg"));

var content = [
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 's-girl-1 sp-1',
			textdata : data.string.p1text1,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-2.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-1 sp-2',
			textdata : data.string.p1text2,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "cloud1",
					imgsrc : imgpath + "cloud1.png",
				},{
					imgclass : "cloud2",
					imgsrc : imgpath + "cloud2.png",
				},{
					imgclass : "cloud3",
					imgsrc : imgpath + "cloud3.png",
				},{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "maya1.png",
				},{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "pemba1.png",
				}
			],
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 's-girl-2 sp-1',
			textdata : data.string.p1text3,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-2.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-2 sp-2',
			textdata : data.string.p1text4,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "cloud1",
					imgsrc : imgpath + "cloud1.png",
				},{
					imgclass : "cloud2",
					imgsrc : imgpath + "cloud2.png",
				},{
					imgclass : "cloud3",
					imgsrc : imgpath + "cloud3.png",
				},{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "maya1.png",
				},{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "pemba2.png",
				}
			],
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 's-girl-3 sp-1',
			textdata : data.string.p1text5,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-3.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-3 sp-2',
			textdata : data.string.p1text6,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-2.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "cloud1",
					imgsrc : imgpath + "cloud1.png",
				},{
					imgclass : "cloud2",
					imgsrc : imgpath + "cloud2.png",
				},{
					imgclass : "cloud3",
					imgsrc : imgpath + "cloud3.png",
				},{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "maya1.png",
				},{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "pemba2.png",
				},{
					imgclass : "monkey monkey-1",
					imgsrc : imgpath + "monkey1.png",
				}
			],
		}]
	},
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 's-girl-3 sp-2',
			textdata : data.string.p1text8,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-3.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-3 sp-1',
			textdata : data.string.p1text7,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-2.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "cloud1",
					imgsrc : imgpath + "cloud1.png",
				},{
					imgclass : "cloud2",
					imgsrc : imgpath + "cloud2.png",
				},{
					imgclass : "cloud3",
					imgsrc : imgpath + "cloud3.png",
				},{
					imgclass : "girl girl-1",
					imgsrc : imgpath + "maya2.png",
				},{
					imgclass : "boy boy-1",
					imgsrc : imgpath + "pemba1.png",
				},{
					imgclass : "monkey monkey-2",
					imgsrc : imgpath + "monkey1.png",
				}
			],
		}]
	},
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	
	
	var has_imagination = false;
	var imagination = null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
		case 0:
			has_imagination = false;
			conversation('.sp-1', sound_1, '.sp-2', sound_2, 0);
			break;
		case 1:
			$prevBtn.show(0);
			has_imagination = false;
			conversation('.sp-1', sound_3, '.sp-2', sound_4, 1);
			break;
		case 2: 
			$prevBtn.show(0);
			has_imagination = true;
			imagination = '.monkey';
			conversation('.sp-1', sound_5, '.sp-2', sound_6, 1);
			break;
		case 3:
			$prevBtn.show(0);
			has_imagination = true;
			imagination = '.monkey';
			conversation('.sp-1', sound_7, '.sp-2', sound_8, 2);
			break;
		default:
			$prevBtn.show(0);
			has_imagination = false;
			conversation('.sp-1', sound_1, '.sp-2', sound_2, 0);
			break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		
		console.log(imagination);
		$(class1).fadeIn(500, function(){
			current_sound.stop();
			current_sound = sound_data1;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				if(change_src==1){
					$('.boy').attr('src', imgpath+'pemba1.png');
					$('.girl').attr('src', imgpath+'maya2.png');
				} else if(change_src==2){
					$('.boy').attr('src', imgpath+'pemba2.png');
					$('.girl').attr('src', imgpath+'maya1.png');
				}
				$(class2).fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_data2;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						if(has_imagination){
							//adding imagination animation here
							$(imagination).show(0);
							nav_button_controls(1500);
						} else {
							nav_button_controls(0);
						}
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
		
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

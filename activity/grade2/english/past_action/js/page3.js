var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p4_s0_1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p4_s1_1.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p4_s2_1.ogg"));

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-3',
		
		speechbox:[{
			speechbox: 's-dad-1 sp-1',
			textdata : data.string.p3text1,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-1 sp-2',
			textdata : data.string.p3text2,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-3.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rack",
					imgsrc : imgpath + "rack.png",
				},
				{
					imgclass : "stand",
					imgsrc : imgpath + "stand.png",
				},
				{
					imgclass : "bed",
					imgsrc : imgpath + "bed.png",
				},
				{
					imgclass : "boysleep",
					imgsrc : imgpath + "boysleeping.png",
				},
				{
					imgclass : "father father-1",
					imgsrc : imgpath + "father.png",
				},
			],
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-3',
		
		speechbox:[{
			speechbox: 's-dad-1 sp-1',
			textdata : data.string.p3text3,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-1 sp-2',
			textdata : data.string.p3text4,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-3.png',
			// audioicon: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rack",
					imgsrc : imgpath + "rack.png",
				},
				{
					imgclass : "stand",
					imgsrc : imgpath + "stand.png",
				},
				{
					imgclass : "bed",
					imgsrc : imgpath + "bed.png",
				},
				{
					imgclass : "boysleep",
					imgsrc : imgpath + "boyopeneye.png",
				},
				{
					imgclass : "father father-1",
					imgsrc : imgpath + "father2.png",
				},
			],
		}]
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-3',
		
		speechbox:[{
			speechbox: 's-dad-1 sp-1',
			textdata : data.string.p3text5,
			textclass : '',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		},
		{
			speechbox: 's-boy-1 sp-2',
			textdata : data.string.p3text6,
			textclass : '',
			imgsrc: 'images/textbox/white/tl-3.png',
			// audioicon: true,
		}],
		bubbleblock:[{
			bubbleblock: 'bubble-1 its_hidden',
			bubbleclass: 'its_hidden',
			bubblesrc: imgpath + 'bubble.png',
			dot1: 'its_hidden',
			dot2: 'its_hidden',
			imgclass : "its_hidden",
			imgsrc : imgpath + "granny3.png",
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "rack",
					imgsrc : imgpath + "rack.png",
				},
				{
					imgclass : "stand",
					imgsrc : imgpath + "stand.png",
				},
				{
					imgclass : "bed",
					imgsrc : imgpath + "bed.png",
				},
				{
					imgclass : "boysleep",
					imgsrc : imgpath + "boyopeneye.png",
				},
				{
					imgclass : "father father-1",
					imgsrc : imgpath + "father2.png",
				},
			],
		}]
	},
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$('.sp-1').fadeIn(10, function(){
					current_sound.stop();
					current_sound = sound_1;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						$('.boysleep').attr('src',  imgpath+'boyopeneye.png');
							// $('.boy').attr('src', imgpath+'suraj1.png');
							// $('.granny').attr('src', imgpath+'maya1.png');
						$('.sp-2').fadeIn(500, function(){
							current_sound.stop();
							current_sound = sound_2;
							current_sound.play();
							current_sound.bindOnce('ended', function(){
								nav_button_controls(0);
								$('.sp-1').click(function(){
									sound_player(sound_1);
								});
								$('.sp-2').click(function(){
									sound_player(sound_2);
								});
							});
						});
					});
				});
				break;
			case 1:
				$prevBtn.show(0);
				$('.sp-1').fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_3;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						$('.sp-2').fadeIn(500, function(){
							current_sound.stop();
							current_sound = sound_4;
							current_sound.play();
							current_sound.bindOnce('ended', function(){
								nav_button_controls(0);
								$('.sp-1').click(function(){
									sound_player(sound_3);
								});
								$('.sp-2').click(function(){
									sound_player(sound_4);
								});
							});
						});
					});
				});
				break;
			case 2:
				$prevBtn.show(0);
				$('.sp-1').fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_5;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
							$('.boy').attr('src', imgpath+'pemba2.png');
							// $('.granny').attr('src', imgpath+'maya1.png');
						$('.sp-2').fadeIn(500, function(){
							current_sound.stop();
							current_sound = sound_6;
							current_sound.play();
							current_sound.bindOnce('ended', function(){
								$('.bubble-1').fadeIn(50, function(){
									$('.bubble-dot-2').fadeIn(200, function(){
										$('.bubble-dot-1').fadeIn(200, function(){
											$('.bubbleimg').fadeIn(500);
											$('.bubble').fadeIn(500, function(){
												nav_button_controls(0);
												$('.sp-1').click(function(){
													sound_player(sound_5);
												});
												$('.sp-2').click(function(){
													sound_player(sound_6);
												});
											});
										});
									});
								});
							});
						});
					});
				});
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				$nextBtn.show(0);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

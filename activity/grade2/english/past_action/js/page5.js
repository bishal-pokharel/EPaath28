var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p6_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p6_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p6_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p6_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p6_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p6_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p6_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p6_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p6_s8.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p6_s9.ogg"));

var content = [
	// slide 0
	{
		contentblockadditionalclass: "bg_diy",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'bg_title'
		}],
	},
	// slide 1
	{
		contentblockadditionalclass: "bg_zoo_1",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text1,
			textclass : 'zoo-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text'
		}],
	},
	// slide 2
	{
		contentblockadditionalclass: "bg_zoo_2",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text2,
			textclass : 'zoo-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text'

		}],
	},

	// slide 3
	{
		contentblockadditionalclass: "bg_mcq",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text3,
			textclass : 'mcq-question-num my_font_big',
		},{
			textdata : data.string.p5text3a,
			textclass : 'mcq-question my_font_big',
		}],

		optionblockclass: 'mcq-options my_font_big o-1',
		option : [{
			textdata : data.string.p5text3b,
			textclass: 'my_font_big',
			optionclass : 'mcq-option opt-1'
		},{
			textdata : data.string.p5text3c,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		},{
			textdata : data.string.p5text3d,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		},{
			textdata : data.string.p5text3e,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		}],
	},

	// slide 4
	{
		contentblockadditionalclass: "bg_mcq",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text4,
			textclass : 'mcq-question-num my_font_big',
		},{
			textdata : data.string.p5text4a,
			textclass : 'mcq-question my_font_big',
		}],

		optionblockclass: 'mcq-options my_font_big o-1',
		option : [{
			textdata : data.string.p5text4b,
			textclass: 'my_font_big',
			optionclass : 'mcq-option opt-1'
		},{
			textdata : data.string.p5text4c,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		},{
			textdata : data.string.p5text4d,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		},{
			textdata : data.string.p5text4e,
			textclass: 'my_font_big',
			optionclass : 'mcq-option'
		}],
	},
	// slide 5
	{
		contentblockadditionalclass: "bg_zoo_3",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text5,
			textclass : 'zoo-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text'
		}],
	},
	// slide 6
	{
		contentblockadditionalclass: "bg_zoo_4",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "hoddcam",
					imgsrc : imgpath + "holding_camera.png",
				}
			],
		}],

		extratextblock : [{
			textdata : data.string.p5text6,
			textclass : 'zoo-text my_font_very_big sniglet fadezoo-text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text'
		}],
	},
	// slide 7
	{
		contentblockadditionalclass: "bg_zoo_5",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text7,
			textclass : 'zoo-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text'
		}],
	},
	// slide 8
	{
		contentblockadditionalclass: "bg_zoo_6",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',

		extratextblock : [{
			textdata : data.string.p5text8,
			textclass : 'zoo-text my_font_very_big sniglet',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-zoo-text-2'
		}],
	},
	//slide 9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_dd',

		uppertextblockadditionalclass: 'instruction-text my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text9,
			textclass : '',
		}],

		draggableblock: [{
			dragclass: 'class-2',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text10
		},{
			dragclass: 'class-1',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text11
		},{
			dragclass: 'class-3',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text12
		},{
			dragclass: 'class-4',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text13
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			textdata1: data.string.p5text14,
			textclass1: 'my_font_big',
			textdata2: data.string.p5text15,
			textclass2: 'my_font_big',
		}]
	},
	//slide 10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_dd',

		uppertextblockadditionalclass: 'instruction-text my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text9,
			textclass : '',
		}],

		draggableblock: [{
			dragclass: 'class-1',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text10
		},{
			dragclass: 'class-2',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text11
		},{
			dragclass: 'class-3',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text12
		},{
			dragclass: 'class-4',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text13
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			textdata1: data.string.p5text16,
			textclass1: 'my_font_big',
			textdata2: data.string.p5text17,
			textclass2: 'my_font_big',
		}]
	},
	//slide 11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_dd',

		uppertextblockadditionalclass: 'instruction-text my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text9,
			textclass : '',
		}],

		draggableblock: [{
			dragclass: 'class-2',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text10
		},{
			dragclass: 'class-3',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text11
		},{
			dragclass: 'class-1',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text12
		},{
			dragclass: 'class-4',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text13
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			textdata1: data.string.p5text18,
			textclass1: 'my_font_big',
			textdata2: data.string.p5text19,
			textclass2: 'my_font_big',
		}]
	},
	//slide 12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_dd',

		uppertextblockadditionalclass: 'instruction-text my_font_medium',
		uppertextblock : [{
			textdata : data.string.p5text9,
			textclass : '',
		}],

		draggableblock: [{
			dragclass: 'class-2',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text10
		},{
			dragclass: 'class-4',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text11
		},{
			dragclass: 'class-3',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text12
		},{
			dragclass: 'class-1',
			textclass : 'sniglet my_font_big',
			imgsrc: imgpath + 'cloud4.png',
			textdata: data.string.p5text13
		}],

		droppableblock: [{
			dropclass: 'dropclass-1',
			textdata1: data.string.p5text20,
			textclass1: 'my_font_big',
			textdata2: data.string.p5text21,
			textclass2: 'my_font_big',
		}]
	},
];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var myTimeout1 =  null;
	var timeouts = [];

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_player2(sound_1);
				break;
			case 1:
				$prevBtn.show(0);
				sound_player2(sound_2);
				break;
			case 2:
				$prevBtn.show(0);
				sound_player2(sound_3);
				break;
			case 3:
			case 4:
				$prevBtn.show(0);
										$('.option').css({'pointer-events': 'none'});
					setTimeout(function(){
						$('.option').css({'pointer-events': 'auto'});
					},4000);
				if(countNext==3){
					sound_player(sound_4);
				} else if(countNext==4){
					sound_player(sound_5);
				}
				var parent = $(".optionblock");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50', 'color':'white'});
						$nextBtn.show(0);
						$(this).children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E',
									'color':'white'});
						$(this).children('.incorrect').show(0);
					}
					$('.hint-1').fadeIn(1000);
				});
				break;
			case 5:
				$prevBtn.show(0);
				sound_player2(sound_6);
				break;
			case 6:
				$prevBtn.show(0);
				$('.fadezoo-text').fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_7;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						$('.hoddcam').fadeIn(500, function(){
									nav_button_controls(0);
								});
							});
						});
				break;
			case 7:
				$prevBtn.show(0);
				sound_player2(sound_8);
				break;
			case 8:
				$prevBtn.show(0);
				sound_player2(sound_9);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
				if(countNext==9){
					sound_player(sound_10);
				}
				$prevBtn.show(0);
				var positions = [1,2,3,4];
				positions.shufflearray();
				for(var i=1; i<=4; i++){
					$('.class-'+i).addClass('pos-'+positions[i-1]);
				}
				setTimeout(function() {
					$( '.dragclass' ).draggable({
						containment: ".board",
						cursor: "move",
						revert: "invalid",
						appendTo: "body",
						zIndex: '200',
						start: function( event, ui ){
							// $(this).addClass('selected');
						},
						stop: function( event, ui ){
							// $(this).removeClass('selected');
						}
					});
				},3500);

				$('.dropclass').droppable({
						accept : ".class-1",
						drop:function(event, ui) {
							ui.draggable.draggable('disable');
							$( ".dragclass" ).draggable( "disable" );
							$(".dropclass").css("border","2px solid #0f0");
							play_correct_incorrect_sound(1);
							nav_button_controls(0);
							ui.draggable.detach().css({
								'pointer-events': 'none',
								'z-index': '10',
								'position': 'absolute',
								'top': '0%',
								'width': '100%',
								'height': '100%',
								'left': '0%',
								'margin': '0%',
							}).appendTo($(this));
							ui.draggable.children('img').fadeOut(1000);
						}
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}

	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_player2(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			$nextBtn.show(0);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

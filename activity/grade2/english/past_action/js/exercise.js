var imgpath = $ref+"/images/diy/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "ex_0.ogg"));

	var content=[
		//slide 0
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text2,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "1a.jpg",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "1b.jpg",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "1c.jpg",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "1d.jpg",
				containerclass : 'class4 options'
			}],
		},
		//slide 1
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text3,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "2a.jpg",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "2b.jpg",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "2c.jpg",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "2d.jpg",
				containerclass : 'class4 options'
			}],
		},
		//slide 2
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text4,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "3a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "3b.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "3c.png",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "3d.png",
				containerclass : 'class4 options'
			}],
		},
		//slide 3
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text5,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "4a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "4b.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "4c.png",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "4d.png",
				containerclass : 'class4 options'
			}],
		},
		//slide 4
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text6,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "5a.jpg",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "5b.jpg",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "5c.jpg",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "5d.jpg",
				containerclass : 'class4 options'
			}],
		},
		//slide 5
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text7,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "6a.jpg",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "6b.jpg",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "6c.jpg",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "6d.jpg",
				containerclass : 'class4 options'
			}],
		},
		//slide 6
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text8,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "7a.jpg",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "7b.jpg",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "7c.jpg",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "7d.jpg",
				containerclass : 'class4 options'
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.p6text1,
				textclass : 'instruction  sniglet'
			},
			{
				textdata : data.string.p6text9,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "8a.png",
				containerclass : 'class1 options'
			},
			{
				imgsrc : imgpath + "8b.png",
				containerclass : 'class2 options'
			},
			{
				imgsrc : imgpath + "8c.png",
				containerclass : 'class3 options'
			},
			{
				imgsrc : imgpath + "8d.png",
				containerclass : 'class4 options'
			}],
		}
	];

	content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 8;
	var score = 0;
	var current_sound = sound_0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new RhinoTemplate();
	rhino.init($total_page);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		var parent = $(".optionsblock");
		$('.command_question').html(parseInt(countNext+1)+') '+$('.command_question').html());
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		if(countNext==0){
				sound_player(sound_0);
		}
		var wrong_clicked = false;
		$(".option_container").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".option_container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).children('.correctans').show(0);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incorrectans').show(0);
				$(this).css('pointer-events', 'none');
				wrong_clicked = true;
	 			play_correct_incorrect_sound(0);
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		rhino.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


});

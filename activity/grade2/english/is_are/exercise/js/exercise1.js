Array.prototype.shufflearray = function() {
    var i = this.length,
    j,
    temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_ex = new buzz.sound((soundAsset + "ex.ogg"));

var content = [
{
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q1p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q1p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption  wrongans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption correctans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "birds.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q2p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q2p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption wrongans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption correctans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "apples.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q3p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q3p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption  correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption wrongans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "house.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q4p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q4p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption wrongans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption correctans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "balloons.png"
},{
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q5p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q5p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption wrongans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "cat.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q6p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q6p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption wrongans ',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption correctans ',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "cows.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q7p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q7p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption wrongans ',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption correctans ',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "balls.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q8p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q8p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption  correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption wrongans ',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "fish.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q9p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q9p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption wrongans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "balloon.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q10p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q10p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption  correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption  wrongans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "chair.png"
}, {
    questionblock: [
    {
        addquestionclass: "left",
        textdata: data.string.q11p1
    }, {
        addquestionclass: "blank"
    }, {
        addquestionclass: "right",
        textdata: data.string.q11p2
    }
    ],
    exerciseblock: [
    {
        headerblock: [
        {
            datahighlightflag: true,
            textclass: "instruclass",
            textdata: data.string.ex1
        }
        ],
        options: [
        {
            optionadd: 'leftoption  correctans',
            id: 'is',
            textdata: data.string.is1
        }, {
            optionadd: 'rightoption  wrongans',
            id: 'are',
            textdata: data.string.are1
        }
        ]
    }
    ],
    imgsrc: imgpath + "bee.png"
},
];
//
// /*remove this for non random questions*/
content.shufflearray();

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    /*for limiting the questions to 10*/
    var $total_page = content.length;
    /*var $total_page = content.length;*/

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined"
        ? islastpageflag = false
        : typeof islastpageflag != 'boolean'
        ? alert("NavigationController : Hi Master, please provide a boolean parameter")
        : null;
    }

    var testin = new EggTemplate();
   
    testin.init(10);

    // Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /*===============================================
        =            data highlight function            =
        ===============================================*/
    /**

         What it does:
         - send an element where the function has to see
         for data to highlight
         - this function searches for all nodes whose
         data-highlight element is set to true
         -searches for # character and gives a start tag
         ;span tag here, also for @ character and replaces with
         end tag of the respective
         - if provided with data-highlightcustomclass value for highlight it
         applies the custom class or else uses parsedstring class

         E.g: caller : texthighlight($board);
         */
         function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object"
        ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted")
        : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass")
                ?/*if there is data-highlightcustomclass defined it is true else it is not*/
                (stylerulename = $(this).attr("data-highlightcustomclass"))
                : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        $('.congratulation').hide(0);
        $('.exefin').hide(0);

        texthighlight($board);

        /*generate question no at the beginning of question*/
        // $('#num_ques').html(countNext + 1 + '. ');

        /*for randomizing the options*/
        var parent = $(".droparea");
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        }
        var arrayset = [];
        switch (countNext) {
                case 0:
                    sound_ex.play().bind("ended",function(){
                        checkyour();
                    });
                    break;
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                checkyour();
                break;

            }

        }
        function checkyour() {
            $('.leftoption').on('click', function() {
                 
                    if ($('#is').hasClass('wrongans')) {
                        testin.update(false);
                        $(this).append("<img class='correctwrongImg' src='images/wrong.png'>");
                        play_correct_incorrect_sound(0);
                        $('.leftoption').css({'background': '#EC1C24', 'color': 'white'});
                       
                    } else if ($('#is').hasClass('correctans')) {
                        $nextBtn.show(0);
                    $(".leftoption").animate({
                        'background': 'none',
                        'left': '27%',
                        'top': '-160%'
                    }, 1500);
                        $('.leftoption').css({'background': 'none', 'color': 'black'})
                        play_correct_incorrect_sound(1);
                        $('.rightoption').unbind('click');
                        testin.update(true);
                    }
                });


            $('.rightoption').on('click', function() {
                    if ($('#are').hasClass('wrongans')) {
                        testin.update(false);
                        $(this).append("<img class='correctwrongImg' src='images/wrong.png'>");
                        play_correct_incorrect_sound(0);
                        $('.rightoption').css({'background': '#EC1C24', 'color': 'white'})
                       
                    } else if ($('#are').hasClass('correctans')) {
                        $nextBtn.show(0);

                        $(".rightoption").animate({
                            'background': 'none',
                            'left': '-24%',
                            'top': '-160%'
                        }, 1500);
                        $('.rightoption').css({'background': 'none', 'color': 'black'})
                        play_correct_incorrect_sound(1);
                        $('.wrongans').unbind('click');
                        testin.update(true);
                    }
                });

        }
        function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

        //call the slide indication bar handler for pink indicators

    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        templateCaller();
        testin.gotoNext();

    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page
        ? ole.footerNotificationHandler.hideNotification()
        : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

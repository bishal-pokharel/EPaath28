var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/page4/";
var soundAsset1 = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p1s1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p1s2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p1s3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p1s4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p1s5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p1s6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "P2S1.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "P2S2.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "P2S3.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "P2S4.ogg"));
var sound_l_11 = new buzz.sound((soundAsset + "P2S3.ogg"));
var sound_l_12 = new buzz.sound((soundAsset + "P2S5.ogg"));
var sound_l_13 = new buzz.sound((soundAsset + "P2S6.ogg"));
var sound_l_14 = new buzz.sound((soundAsset + "P3S1.ogg"));
var sound_l_15 = new buzz.sound((soundAsset + "P3S2.ogg"));
var sound_l_16 = new buzz.sound((soundAsset + "P3S3.ogg"));
var sound_l_17 = new buzz.sound((soundAsset + "P3S4.ogg"));
var sound_l_18 = new buzz.sound((soundAsset + "P3S5.ogg"));
var sound_l_19 = new buzz.sound((soundAsset + "P3S6.ogg"));
var sound_for = new buzz.sound((soundAsset1 + "for2.ogg"));
var sound_for1 = new buzz.sound((soundAsset1 + "for.ogg"));
var sound_summary = new buzz.sound((soundAsset + "summary.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4,
					  sound_l_5, sound_l_6, sound_l_7, sound_l_8,
					  sound_l_9, sound_l_10, sound_l_11, sound_l_12,
					  sound_l_13, sound_l_14, sound_l_15, sound_l_16,
					  sound_l_17, sound_l_18, sound_l_19];


var content=[
	{
		//page 0
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,
		uppertextblockadditionalclass : "additionaltextblock",
		uppertextblock:[
			{
				textclass: "heading",
				textdata: data.string.p1_s19
			}
		]
	},
	{
		//page 1
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description animate_is_are",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		}]
	},{
		//page 2
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single",
				imgsrc : imgpath+ "book02.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s1,
			}]
		}]
	},{
		//page 3
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
            textclass: "are_description animate_is_are",
            datahighlightflag: true,
            textdata: data.string.p1_s22
        }],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single_noanimation",
				imgsrc : imgpath+ "book02.png"
			}],
			imagelabels : [{
			labelcontainer: "labelcontainer1",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s1,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1",
				imgsrc : imgpath+ "book01.png"
			},{
				imgclass : "image_quadro_2",
				imgsrc : imgpath+ "book02.png"
			},{
				imgclass : "image_quadro_3",
				imgsrc : imgpath+ "book03.png"
			},{
				imgclass : "image_quadro_4",
				imgsrc : imgpath+ "book04.png"
			}],
			imagelabels : [
			{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s2,
			}]
		}]
	},
	{
		//page 4
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single",
				imgsrc : imgpath+ "pencil02.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s3,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1",
				imgsrc : imgpath+ "pencil01.png"
			},{
				imgclass : "image_quadro_2",
				imgsrc : imgpath+ "pencil02.png"
			},{
				imgclass : "image_quadro_3",
				imgsrc : imgpath+ "pencil03.png"
			},{
				imgclass : "image_quadro_4",
				imgsrc : imgpath+ "pencil04.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s4,
			}]
		}]
	},
	{
		//page 5
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single",
				imgsrc : imgpath+ "banana01.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s5,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1",
				imgsrc : imgpath+ "banana01.png"
			},{
				imgclass : "image_quadro_2",
				imgsrc : imgpath+ "banana02.png"
			},{
				imgclass : "image_quadro_3",
				imgsrc : imgpath+ "banana03.png"
			},{
				imgclass : "image_quadro_4",
				imgsrc : imgpath+ "banana04.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s6,
			}]
		}]
	},
	{
		//page 6
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single2",
				imgsrc : imgpath+ "monkey01.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s7,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_tri_1",
				imgsrc : imgpath+ "monkey01.png"
			},{
				imgclass : "image_tri_2",
				imgsrc : imgpath+ "monkey02.png"
			},{
				imgclass : "image_tri_3",
				imgsrc : imgpath+ "monkey03.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s8,
			}]
		}]
	},
	{
		//page 7
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single2",
				imgsrc : imgpath+ "bird04.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s9,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_tri_1",
				imgsrc : imgpath+ "bird01.png"
			},{
				imgclass : "image_tri_2",
				imgsrc : imgpath+ "bird02.png"
			},{
				imgclass : "image_tri_3",
				imgsrc : imgpath+ "bird03.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s10,
			}]
		}]
	},
	{
		//page 8
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single",
				imgsrc : imgpath+ "rocket02.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s11,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_tri_1",
				imgsrc : imgpath+ "rocket01.png"
			},{
				imgclass : "image_tri_2",
				imgsrc : imgpath+ "rocket02.png"
			},{
				imgclass : "image_tri_3",
				imgsrc : imgpath+ "rocket04.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s12,
			}]
		}]
	},
	{
		//page 9
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single3",
				imgsrc : imgpath+ "boy02.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s13,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1_2",
				imgsrc : imgpath+ "boy04.png"
			},{
				imgclass : "image_quadro_2_2",
				imgsrc : imgpath+ "boy03.png"
			},{
				imgclass : "image_quadro_3_2",
				imgsrc : imgpath+ "boy02.png"
			},{
				imgclass : "image_quadro_4_2",
				imgsrc : imgpath+ "boy01.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s14,
			}]
		}]
	},
	{
		//page 10
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single3",
				imgsrc : imgpath+ "girl01.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s15,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1_2",
				imgsrc : imgpath+ "girl01.png"
			},{
				imgclass : "image_quadro_2_2",
				imgsrc : imgpath+ "girl02.png"
			},{
				imgclass : "image_quadro_3_2",
				imgsrc : imgpath+ "girl03.png"
			},{
				imgclass : "image_quadro_4_2",
				imgsrc : imgpath+ "girl04.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s16,
			}]
		}]
	},
	{
		//page 11
		contentblockadditionalclass: "purplebg",
		contentnocenteradjust: true,

		uppertextblockadditionalclass: "additionaltextblock",
		uppertextblock:[{
			textclass: "header",
			textdata: data.string.p1_s20
		},{
			textclass: "is_description",
			datahighlightflag: true,
			textdata: data.string.p1_s21
		},{
			textclass: "are_description",
			datahighlightflag: true,
			textdata: data.string.p1_s22
		}],

		imageblockadditionalclass: "additionalimageblock",
		imageblock:[{
			imageblockclass: "doubleimage_description_left",
			imagestoshow : [{
				imgclass : "image_single3",
				imgsrc : imgpath+ "police02.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer1 clickme",
				imgclass2 : "speaker1",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 left",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s17,
			}]
		}, {
			imageblockclass: "doubleimage_description_right",
			imagestoshow : [{
				imgclass : "image_quadro_1_2",
				imgsrc : imgpath+ "police02.png"
			},{
				imgclass : "image_quadro_2_2",
				imgsrc : imgpath+ "police04.png"
			},{
				imgclass : "image_quadro_3_2",
				imgsrc : imgpath+ "police04.png"
			},{
				imgclass : "image_quadro_4_2",
				imgsrc : imgpath+ "police03.png"
			}],
			imagelabels : [{
				labelcontainer: "labelcontainer2 clickme",
				imgclass2 : "speaker2",
				imgsrc2 : imgpath+ "speaker.png",
				imagelabelclass : "image_single_label2 right",
				datahighlightflag: true,
				imagelabeldata : data.string.p1_s18,
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 3;
  var last_page = false;
	countNext= 0;
  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
	var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			 	$nextBtn.show(0);
				$prevBtn.show(0);
				last_page = false;
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			last_page = true;
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/

   var soundtoplay1 = sound_group_p1[countNext%4];
   var soundtoplay2 = sound_group_p1[countNext%4 + 1];

  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);
	    // $nextBtn.hide(0);
        vocabcontroller.findwords(countNext);

	    // highlight any text inside board div with datahighlightflag set true

	    texthighlight($board);
	    $prevBtn.hide(0);
		switch(countNext) {
			case 0:
                $nextBtn.hide(0);
                sound_summary.play().bind("ended",function(){
                    $nextBtn.show(0);
                })
				break;
		case 1:
            $nextBtn.hide(0);
            setTimeout(function () {
                sound_for.play();
                $nextBtn.delay(4000).show(0);
                $prevBtn.delay(4000).show(0);
            },2000);
			break;
		case 2:
			$nextBtn.hide(0);
			var $doubleimage_description_left = $(".labelcontainer1");
			$doubleimage_description_left.hide(0);
			setTimeout(function(){
				$doubleimage_description_left.show(0);
				split_p_andshow('image_single_label2');
				audioPlayController(null, true, 2, null);
			}, 1500);
			break;
		case 3:
			$nextBtn.hide(0);
			var $doubleimage_description_left = $(".labelcontainer1");
			var $doubleimage_description_right = $(".labelcontainer2");
			$doubleimage_description_right.hide(0);
			setTimeout(function(){
				sound_for1.play();
			},1500);
			setTimeout(function(){
				$doubleimage_description_right.show(0);
				split_p_andshow('image_single_label2', 0);
				audioPlayController(true, true, 3, 2);
			}, 5000);
			break;

		case 4:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, 2, 1);
		break;
		case 5:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, 1, 0);
		break;
		case 6:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, 0, -1);
		break;
		case 7:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, -1, -2);
		break;
		case 8:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, -3, -4);
		break;
		case 9:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, -4, -5);
		break;
		case 10:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, -5, -6);
		break;
		case 11:
			$nextBtn.hide(0);
			split_p_andshow('image_single_label2');
			audioPlayController(true, true, -6, -7);
		break;
		default:
			break;
		}

  }

/*=====  End of Templates Block  ======*/

function audioPlayController(audio1Clickedvalue, audio2Clickedvalue, no1, no2){
	var $speaker1 = $('.speaker1');
	var $leftlabel = $('.labelcontainer1');
	var $rightlabel = $('.labelcontainer2');

	var $speaker2 = $('.speaker2');

	soundtoplay1 = sound_group_p1[countNext - no1];
	soundtoplay2 = sound_group_p1[countNext - no2];
	console.log(countNext - no1);
	console.log(countNext - no2);
    var audio1Clicked = (audio1Clickedvalue != null)? audio1Clickedvalue : false;
    var audio2Clicked = (audio2Clickedvalue != null)? audio2Clickedvalue : false;
    var audioplaying =false;

    soundtoplay1.bind('ended', function(){
    	audioplaying = false;
    	$leftlabel.removeClass('big_big');
    	if(audio1Clicked && audio2Clicked){
    		if(!last_page) {
				$nextBtn.show(0);
				$prevBtn.show(0);
			} else {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
			}
    	}
	});

    soundtoplay2.bind('ended', function(){
    	audioplaying = false;
    	$rightlabel.removeClass('big_big');
		if(audio1Clicked && audio2Clicked){
    		if(!last_page) {
				$nextBtn.show(0);
				$prevBtn.show(0);
			} else {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
			}
    	}
	});

    $speaker1.click(function(){
    	$leftlabel.trigger('click');
    });

    $leftlabel.click(function(){
    	if (audioplaying){
    		return false;
    	}
    	audioplaying = true;
    	audio1Clicked = true;
    	$leftlabel.addClass('big_big');
    	$leftlabel.removeClass('clickme');
    	soundtoplay1.play();
    });

    $speaker2.click(function(){
    	$rightlabel.trigger('click');
    });

    $rightlabel.click(function(){
    	if (audioplaying){
    		return false;
    	}
    	audioplaying = true;
    	audio2Clicked = true;
    	$rightlabel.addClass('big_big');
    	$rightlabel.removeClass('clickme');
    	soundtoplay2.play();
    });
}




/*==================================================
=            Templates Controller Block            =
==================================================*/

/* this will split the text into three halves and show them in intervals*/
	function split_p_andshow(classname, skipindex){
		var $pClass = $("."+classname);
		skipindex = (skipindex != null)? skipindex : -1;
		$pClass.each(function(index){
			if(skipindex == index){
				return true;//continue will not work on for each loop of js
			}
			var $this = $(this);
			var string = $this.html();

			var string1 = string.substring(0, string.indexOf("<span "));
			var string2 = string.substring(string1.length,  string.indexOf("/span> ") + 6);
			var string3 = string.substring(string.indexOf("/span> ") + 6, string.length);
			$this.html("");
			$this.show(0);
			show_text( $this, string1, string2, string3, 0, 90);
			console.log(string1+"     acdf          "+ string2+"     acdf          "+ string3);

		});
	}

/* For typing animation appends the text to an element specified by target class or id */
	function show_text(target, message1, message2, message3, index, interval) {
	  if (message1 != null) {
	    target.append(message1[index++]);
	    setTimeout(function () {
	    	if(index < message1.length){
	    		show_text(target, message1, message2, message3, index, interval);
	    	}else{
	    		show_text(target, null, message2, message3, index, interval);
	    	}

	  	}, interval);
	  } else if( message2 != null){
	  	target.append(message2);
	  	setTimeout(function () {
	    	show_text(target, null, null, message3, 0, interval);
	  	}, interval);
	  }else{
	  	target.append(message3[index++]);
	  	setTimeout(function () {
	    	show_text(target, null, null, message3, index, interval);
	  	}, interval);
	  }
	}

	/* Typing animation ends */



  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller(true);

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page,countNext+1);

  }


  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
  $nextBtn.on('click', function() {
  		soundtoplay1.stop();
		soundtoplay2.stop();
		soundtoplay1.unbind('ended');
		soundtoplay2.unbind('ended');
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		soundtoplay1.stop();
		soundtoplay2.stop();
		soundtoplay1.unbind('ended');
		soundtoplay2.unbind('ended');
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

$(function(){
    var $container = $('#generalTemplateblock');
    var game = new Phaser.Game($container.width(), $container.height(), Phaser.auto, 'generalTemplateblock', { preload: preload, create: create, update: update});

    var imgpath = $ref + "/exercise/";
    var score = 0;
    var scoreText;
    var sky;
    var randNum;
    var spriteText;
    function preload() {
        var load_sky = game.load.image('sky', imgpath + 'assets/sky.png');
        game.load.image('fish', imgpath + 'assets/fish2.png');
        game.load.image('naramro', imgpath + 'assets/fish3.png');
        game.load.spritesheet('dude', imgpath + 'assets/shark.png', 120, 100);
    }

    function create(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
    	cursors = game.input.keyboard.createCursorKeys();
        sky = game.add.sprite(0, 0, 'sky');
        sky.scale.setTo($container.width()/game.cache.getImage("sky").width, $container.height()/game.cache.getImage("sky").height);

        player = game.add.sprite(32, game.world.height*0.5, 'dude');
        player.scale.setTo($container.width()*0.4/game.cache.getImage("dude").width, $container.height()*0.15/game.cache.getImage("dude").height);
        player.anchor.setTo(0.5);

        game.physics.arcade.enable(player);

        player.body.collideWorldBounds = true;
        
        player.animations.add('move', [0, 3], 10, true);
        player.animations.add('eat', [0, 1], 10, false);
        player.animations.add('die', [0, 2], 10, false);
        player.animations.play('move');

        spriteGroup = game.add.group();

        scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });
    }
    var count = 0;
    setInterval(function(){
        if(count%2 == 0){
        fish = game.add.sprite(0, Math.random() * $container.height(), 'naramro');
        fish.scale.setTo($container.width()*0.4/game.cache.getImage("dude").width, $container.height()*0.15/game.cache.getImage("dude").height);
        fish.anchor.setTo(0.6, 0.5);

        var fishno = Math.floor(Math.random() * 50);
        spriteText = game.add.text(fish.x, fish.y, fishno, {fill: '#fff'});  
        spriteText.anchor.setTo(0.5, 0.5);
        spriteGroup = game.add.group();

        spriteGroup.add(fish);
        spriteGroup.add(spriteText);
        game.physics.arcade.enable(spriteGroup);
        fish.body.moves = false;

        game.add.tween(spriteGroup).to( { x:1100, y: 0}, 12000, Phaser.Easing.Linear.None, true);
    }
    else{
        fish = game.add.sprite($container.width(), Math.random() * $container.height(), 'fish');
        //fish.scale.setTo($container.width()*0.4/game.cache.getImage("dude").width, $container.height()*0.15/game.cache.getImage("dude").height);
        fish.anchor.setTo(0.6, 0.5);

        var fishno = Math.floor(Math.random() * 50);
        spriteText = game.add.text(fish.x, fish.y, fishno, {fill: '#fff'});  
        spriteText.anchor.setTo(0.5, 0.5);
        spriteGroup = game.add.group();

        spriteGroup.add(fish);
        spriteGroup.add(spriteText);
        game.physics.arcade.enable(spriteGroup);
        fish.body.moves = false;

        game.add.tween(spriteGroup).to( { x:-1100, y: 0}, 12000, Phaser.Easing.Linear.None, true);
    }
    count++;
    }, 3000);
    
    var lookingRight = true;

    function update(){
        game.physics.arcade.overlap(player, spriteGroup, collectFish, null, this);
        player.body.velocity.x = 0;
        player.body.velocity.y = 0;

        if (cursors.left.isDown)
        {
            player.body.velocity.x = -150;
            if (lookingRight){
                player.scale.x *=-1;
            }
            lookingRight = false;
        }
        else if (cursors.right.isDown)
        {
            player.body.velocity.x = 150;
            if (!lookingRight){
                player.scale.x *=-1;
            }
            lookingRight = true;
        }

        if (cursors.up.isDown )
        {
           player.body.velocity.y = -150;
        }

        if (cursors.down.isDown )
        {
           player.body.velocity.y = 150;
        }
    }
    function collectFish (player, fish) {
        console.log(fish);
        fish.kill();
        spriteText.destroy();
        score += 10;
        scoreText.text = 'Score: ' + score;
        player.animations.play('eat');
        game.time.events.add(Phaser.Timer.SECOND * 0.3, eatfin, this);
    }

    function badFish (player, fish) {
        fish.destroy();
        score += 10;
        scoreText.text = 'Score: ' + score;
        player.animations.play('die');
        game.time.events.add(Phaser.Timer.SECOND * 5, eatfin, this);
    }

    function eatfin() {
        player.animations.play('move');
    }

    $(window).resize(function() {
    	var $canvas = $('#generalTemplateblock> canvas');
    	$canvas.height( $container.height());
    	$canvas.width($container.width());
    });
});
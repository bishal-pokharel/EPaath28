$(function(){
	
var $container = $('#generalTemplateblock');
var game = new Phaser.Game($container.width(), $container.height(), Phaser.auto, 'generalTemplateblock', { preload: preload, create: create, update: update});

var imgpath = $ref + "/exercise/";
var platforms;
var platforms2;
var score = 0;
var scoreText;
var sky;
var jump;


function preload() {
    var load_sky = game.load.image('sky', imgpath + 'assets/sky.png');
    var load_ground = game.load.image('ground', imgpath + 'assets/platform.png');
    game.load.image('star', imgpath + 'assets/star.png');
    game.load.image('diamond', imgpath + 'assets/diamond.png');
    game.load.spritesheet('dude', imgpath + 'assets/shark.png', 120, 100);
}


function create(){
    sky = game.add.sprite(0, 0, 'sky');

    star = game.add.image(50,50, 'star');
    sky.scale.setTo($container.width()/game.cache.getImage("sky").width, $container.height()/game.cache.getImage("sky").height);
    //  Enable p2 physics
    game.physics.startSystem(Phaser.Physics.P2JS);
    game.physics.p2.setImpactEvents(true);
    game.physics.p2.restitution = 0.8;


    //  Create our collision groups. One for the player, one for the pandas
    var playerCollisionGroup = game.physics.p2.createCollisionGroup();
    var pandaCollisionGroup = game.physics.p2.createCollisionGroup();

     game.physics.p2.updateBoundsCollisionGroup();

    //  Add a sprite
    sprite = game.add.sprite(200, 200, 'dude');

    //  Enable if for physics. This creates a default rectangular body.
    game.physics.p2.enable(sprite);

    sprite.animations.add('move', [0, 3], 10, true);
    sprite.animations.play('move');

    //  Modify a few body properties
    sprite.body.setZeroDamping();
    sprite.body.fixedRotation = true;

    cursors = game.input.keyboard.createCursorKeys();



    star.body.setCollisionGroup(pandaCollisionGroup);
    sprite.body.setCollisionGroup(playerCollisionGroup);

        //  Pandas will collide against themselves and the player
        //  If you don't set this they'll not collide with anything.
        //  The first parameter is either an array or a single collision group.
    star.body.collides([pandaCollisionGroup, playerCollisionGroup]);
    sprite.body.collides(pandaCollisionGroup, hitPanda, this);

    game.camera.follow(sprite);

}
    var lookingRight = true;
function hitPanda(body1, body2) {

    //  body1 is the space ship (as it's the body that owns the callback)
    //  body2 is the body it impacted with, in this case our panda
    //  As body2 is a Phaser.Physics.P2.Body object, you access its own (the sprite) via the sprite property:
    body2.sprite.alpha -= 0.1;

}
function update(){
    sprite.body.setZeroVelocity();
    if (cursors.left.isDown)
    {
        sprite.body.moveLeft(400);
        if (lookingRight){
            sprite.scale.x *=-1;
        }
        lookingRight = false;

        //sprite.scale.x *=-1;
    }
    else if (cursors.right.isDown)
    {
        sprite.body.moveRight(400);
        if (!lookingRight){
            sprite.scale.x *=-1;
        }
        lookingRight = true;
    }
    else if (cursors.up.isDown)
    {
        sprite.body.moveUp(400);
    }
    else if (cursors.down.isDown)
    {
        sprite.body.moveDown(400);
    }
    else
    {
        sprite.frame = 4;
    }

    // game.scale.scaleMode = Phaser.ScaleManager(game, $container.width(), $container.height());
}


$(window).resize(function() {
	// Phaser.ScaleManager = new ScaleManager(game, $('#generalTemplateblock').width(), $('#generalTemplateblock').height());
	// game.scale.scaleMode = Phaser.ScaleManager(game, $container.width(), $container.height());
	var $canvas = $('#generalTemplateblock> canvas');
	$canvas.height( $container.height());
	$canvas.width($container.width());
	// game.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
});


});
var imgpath = $ref+"/images/lesson2/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
 {
 	//page 0
 	contentblockadditionalclass: "additonalcontentblock",
 	uppertextblockadditionalclass: "additionalutb1",
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p2_s1
  	}]
 },{
 	//page 1
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"1-01.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer2_regular",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubbleboy-17.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1_2",
  			imagelabeldata: data.string.p2_s3
  		}]
  	}]
 },{
 	//page 2
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"1-01.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer2_mirror",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror",
  			imgsrc: imgpath+"bubbleboy-17.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1_2",
  			imagelabeldata: data.string.p2_s4
  		}]
  	}]
 },{
 	//page 3
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"1-01.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer2_regular",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubbleboy-17.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1_2",
  			imagelabeldata: data.string.p2_s5
  		}]
  	},{
  		containerdiv: "dialogcontainer3",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror_full",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel2",
  			imagelabeldata: data.string.p2_s6
  		}]
  	}]
 },{
 	//page 4
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"1-01.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer2_regular",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubbleboy-17.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1_2",
  			imagelabeldata: data.string.p2_s7
  		}]
  	},{
  		containerdiv: "dialogcontainer3",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror_full",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel2",
  			imagelabeldata: data.string.p2_s8
  		}]
  	}]
 },{
 	//page 5
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"2-05.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s9
  		}]
  	},{
  		containerdiv: "applecontainer",
  		imagestoshow:[{
  			imgclass: "apple",
  			imgsrc: imgpath+"2b-20.png"
  		},{
  			imgclass: "magnifyingglass",
  			imgsrc: imgpath+"2c-25.png"
  		},{
  			imgclass: "germ1",
  			imgsrc: imgpath+"2c-26.png"
  		},{
  			imgclass: "germ2",
  			imgsrc: imgpath+"2c-27.png"
  		},{
  			imgclass: "germ3",
  			imgsrc: imgpath+"2c-28.png"
  		}]
  	}]
  },{
 	//page 6
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"3-19.png"
  		}]
  	},{
  		containerdiv: "imagine_container",
  		imagestoshow:[{
  			imgclass: "bubble2",
  			imgsrc: imgpath+"3b-29.png"
  		},{
  			imgclass: "bubble1",
  			imgsrc: imgpath+"3b-30.png"
  		}]
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"3-19.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4_mirror",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s10
  		}]
  	}]
  },{
  	//page 8
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"4-11.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4_1",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s11
  		}]
  	},{
  		containerdiv: "applecontainer",
  		imagestoshow:[{
  			imgclass: "apple fade_in",
  			imgsrc: imgpath+"4b-31.png"
  		}]
  	}]
  },{
  	//page 9
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"5-12.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4_mirror",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s12
  		}]
  	},{
  		containerdiv: "applecontainer",
  		imagestoshow:[{
  			imgclass: "apple fade_in",
  			imgsrc: imgpath+"5b-32.png"
  		}]
  	}]
  },{
  	//page 10
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"6-13.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4_2",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s13
  		}]
  	},{
  		containerdiv: "applecontainer",
  		imagestoshow:[{
  			imgclass: "apple fade_in",
  			imgsrc: imgpath+"6b-33.png"
  		}]
  	}]
  },{
  	//page 11
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"6-13.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer4_mirror_1",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s14
  		}]
  	},{
  		containerdiv: "applecontainer",
  		imagestoshow:[{
  			imgclass: "apple fade_in",
  			imgsrc: imgpath+"7b-34.png"
  		}]
  	}]
  },{
  	//page 12
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"5-12.png"
  		}]
  	},{
  		containerdiv: "coverfood_container",
  		imagestoshow:[{
  			imgclass: "food1 fade_in",
  			imgsrc: imgpath+"8b-35.png"
  		},{
  			imgclass: "food2 fade_in",
  			imgsrc: imgpath+"8b-36.png"
  		},{
  			imgclass: "food3 fade_in",
  			imgsrc: imgpath+"8b-36-17.png"
  		},{
  			imgclass: "cover",
  			imgsrc: imgpath+"8b-36-18.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer5_mirror",
  		imagestoshow:[{
  			imgclass: " newcss",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s15
  		}]
  	}]
  },{
  	//page 13
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"9-15.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer6_mirror",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s16
  		}]
  	}/*
	  ,{
				containerdiv: "applecontainer",
				imagestoshow:[{
					imgclass: "bell",
					imgsrc: imgpath+"fruits.jpg"
				}]
			}*/
	  ]
  },{
  	//page 14
  	contentnocenteradjust: true,
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "background_img",
  			imgsrc: imgpath+"9-16.png"
  		}]
  	},{
  		containerdiv: "dialogcontainer5",
  		imagestoshow:[{
  			imgclass: "dialog1",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel1",
  			imagelabeldata: data.string.p2_s17
  		}]
  	},{
  		containerdiv: "dialogcontainer6",
  		imagestoshow:[{
  			imgclass: "dialog1_mirror_full",
  			imgsrc: imgpath+"bubblesc.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "dialoglabel2",
  			imagelabeldata: data.string.p2_s18
  		}]
  	}/*
	  ,{
				containerdiv: "applecontainer",
				imagestoshow:[{
					imgclass: "bell",
					imgsrc: imgpath+"fruits.jpg"
				}]
			}*/
	  ]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_3_1", src: soundAsset+"p2_s3_1.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_4_1", src: soundAsset+"p2_s4_1.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p2_s14.ogg"},
			{id: "sound_14_1", src: soundAsset+"p2_s14_1.ogg"},
      {id: "bell", src: soundAsset+"bell.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	if(countNext < 9){
		sound_player("sound_"+ countNext);
	}
    switch(countNext){
    	case 0:
    	case 1:
    	case 2:
    	case 5:
    	case 7:
    	case 8:
    	case 9:
    	case 10:
    	case 12:
    	case 13:
    	case 11:

    		sound_player1("sound_"+countNext);
    		break;
    	case 3:
    	case 4:
      if(countNext == 3 || countNext == 4){
      	$secondContainer = $(".dialogcontainer3").hide(0);
      }else {
      	$secondContainer = $(".dialogcontainer6").hide(0);
      }
      createjs.Sound.stop();
      current_sound = createjs.Sound.play("sound_"+countNext);
      current_sound.on("complete", function(){
        sound_player("sound_"+countNext+"_1");
        $secondContainer.show(0);
        current_sound.on("complete", function(){
          nav_button_controls(200);
        });
      });

      break;
    	case 14:
    		var $secondContainer;
        $secondContainer = $(".dialogcontainer3").hide(0);
        $secondContainer = $(".dialogcontainer6").hide(0);
        createjs.Sound.stop();
        current_sound = createjs.Sound.play("bell");
        current_sound.play();
      current_sound.on("complete", function(){
    	createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_"+countNext);
			current_sound.on("complete", function(){
				sound_player("sound_"+countNext+"_1");
				$secondContainer.show(0);
        current_sound.on("complete", function(){
          nav_button_controls(200);
        });
      });
			});
			break;
    	case 6:
    		$(".contentblock").addClass("slight_magnification");
        nav_button_controls(3000);
    		break;
    	default:
    		break;
    }
  }
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}
function sound_player1(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
    current_sound.on("complete", function(){
      nav_button_controls(200);
    });
}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
   			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */
	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

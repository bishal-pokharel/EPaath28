var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
 {
  	//start
  	// contentblockadditionalclass: "firstbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "covertitle",
  		textdata: data.string.p6_heading
  	}],
    imageblock: [{
      imagestoshow:[{
        imgclass: "coverpage",
        imgid: "",
        imgsrc: imgpath+"cover_page.png"
      }]
    }]
  },{
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "food",
  			imgid: "food1",
  			imgsrc: imgpath+"cooked-rice.png"
  		},{
  			imgclass: "food",
  			imgid: "food2",
  			imgsrc: imgpath+"greensaag.png"
  		},{
  			imgclass: "food",
  			imgid: "food3",
  			imgsrc: imgpath+"yellow-dal.png"
  		},{
  			imgclass: "food",
  			imgid: "food4",
  			imgsrc: imgpath+"gundruk02.png"
  		},{
  			imgclass: "food",
  			imgid: "food5",
  			imgsrc: imgpath+"roti.png"
  		},{
  			imgclass: "food",
  			imgid: "food6",
  			imgsrc: imgpath+"banana.png"
  		},{
  			imgclass: "food",
  			imgid: "food7",
  			imgsrc: imgpath+"sael_roti.png"
  		}]
  	}],
  	lowertextblock: [{
  		textclass: "description fade_in",
  		textdata: data.string.p1_s1
  	}]
  },{
  	//page 2
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s1
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"fruits.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s3
  		}]
  	},{
  		containerdiv: "imagediv2",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"dairy-products.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s4
  		}]
  	},{
  		containerdiv: "imagediv3",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"food-grains.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s5
  		}]
  	},{
  		containerdiv: "imagediv4",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"meat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s6
  		}]
  	},{
  		containerdiv: "imagediv5",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"vegetable-mix.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s7
  		}]
  	}]
  },{
  	//page 3
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s8
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"apple.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s9
  		}]
  	},{
  		containerdiv: "imagediv2_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"orange.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s10
  		}]
  	},{
  		containerdiv: "imagediv3_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"watermelon.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s11
  		}]
  	},{
  		containerdiv: "imagediv4_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"pinaapple.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s12
  		}]
  	},{
  		containerdiv: "imagediv5_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"mango.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s13
  		}]
  	},{
  		containerdiv: "imagediv6_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"banana.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s14
  		}]
  	},{
  		containerdiv: "imagediv7_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"grapes.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s15
  		}]
  	}]
  },{
  	//page 4
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s16
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"broccoli.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s17
  		}]
  	},{
  		containerdiv: "imagediv2_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"brinjal.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s18
  		}]
  	},{
  		containerdiv: "imagediv3_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"cauliflower.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s19
  		}]
  	},{
  		containerdiv: "imagediv4_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"mushrooms.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s20
  		}]
  	},{
  		containerdiv: "imagediv5_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"pumpkin.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s21
  		}]
  	},{
  		containerdiv: "imagediv6_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"turnips.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s22
  		}]
  	},{
  		containerdiv: "imagediv7_7",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"mustard.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s23
  		}]
  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s24
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"maize.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s25
  		}]
  	},{
  		containerdiv: "imagediv2_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"millet01.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s26
  		}]
  	},{
  		containerdiv: "imagediv3_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"buckwheat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s27
  		}]
  	},{
  		containerdiv: "imagediv4_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"wheat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s28
  		}]
  	},{
  		containerdiv: "imagediv5_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"cooked-rice.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s29
  		}]
  	},{
  		containerdiv: "imagediv6_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"oats.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s30
  		}]
  	}]
  },{
	//page 6
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s31
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1_3",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"egg.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s32
  		}]
  	},{
  		containerdiv: "imagediv2_3",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"meat01.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s33
  		}]
  	},{
  		containerdiv: "imagediv3_3",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"fish.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s34
  		}]
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s35
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		containerdiv: "imagediv1_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"paneer.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s36
  		}]
  	},{
  		containerdiv: "imagediv2_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"butter.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s37
  		}]
  	},{
  		containerdiv: "imagediv3_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"milk.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s38
  		}]
  	},{
  		containerdiv: "imagediv4_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"ghee.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s39
  		}]
  	},{
  		containerdiv: "imagediv5_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"curd.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s40
  		}]
  	},{
  		containerdiv: "imagediv6_6",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"cheese.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p1_s41
  		}]
  	}]
  },{
  	//page 8
  	contentblockadditionalclass: "firstbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p1_s42
  	}],
  },{
  	//page 9
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block",
	  	imageblock: [{
	  		containerdiv: "imagediv_last",
	  		imagelabels:[{
	  			imagelabelclass: "label_last",
	  			imagelabeldata: data.string.p1_s3
	  		}]
	  	},{
	  		containerdiv: "imagediv_last",
	  		imagelabels:[{
	  			imagelabelclass: "label_last",
	  			imagelabeldata: data.string.p1_s7
	  		}]
	  	},{
	  		containerdiv: "imagediv_last",
	  		imagelabels:[{
	  			imagelabelclass: "label_last",
	  			imagelabeldata: data.string.p1_s5
	  		}]
	  	},{
	  		containerdiv: "imagediv_last",
	  		imagelabels:[{
	  			imagelabelclass: "label_last",
	  			imagelabeldata: data.string.p1_s6
	  		}]
	  	},{
	  		containerdiv: "imagediv_last",
	  		imagelabels:[{
	  			imagelabelclass: "label_last",
	  			imagelabeldata: data.string.p1_s4
	  		}]
	  	}]
  	}

];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();


/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
   	var fruitsarray = [];
   	var veggiesarray = [];
   	var cerealsarray = [];
   	var meatarray = [];
   	var milkarray = [];

  function containsObject(obj, list) {
	    for (var i = 0; i < list.length; i++) {
	    	console.log(list[i].parent().attr('class') , obj.parent().attr('class'));
	        if (list[i].parent().attr('class') == obj.parent().attr('class')) {
	            return i;
	        }
	    }

	    return -1;
	}
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    var div_ratio =$(".frogspritecontainer").width()/$(".frogspritecontainer").parent().width();
    $(".frogspritecontainer").width(Math.round($(".frogspritecontainer").parent().width()*div_ratio));
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);

    switch(countNext){
    	case 0:
    	case 2:
    	case 8:
			sound_player1("sound_"+ countNext);
			break;
    	case 1:
        setTimeout(function(){
            sound_player("sound_"+ countNext);
        },1000);

			timeoutcontroller= setTimeout(function(){
	   	nav_button_controls(200);
			}, 11500);
    		break;
    	case 3:
			sound_player("sound_"+ countNext);
			var $this;
			$nextBtn.hide(0);
			fruitsarray = [];
    		$(".imageblock > div").click(function(){
    			$this = $(this);
          $this.css({"pointer-events":"none",
                      "transform":"scale(1.5)"});
    			var $img = $($this.find("img"));
    			var index = containsObject($img, fruitsarray);
          $this.addClass("hvr");
    			if( index == -1){
    				fruitsarray[fruitsarray.length] = $img;
    			} else {
    				fruitsarray.splice(index, 1);
    			}
	         nav_button_controls(200);
    		});
    		break;
    	case 4:
			sound_player("sound_"+ countNext);
    		var $this;
			veggiesarray = [];
	    	$(".imageblock > div").click(function(){
	    			$this = $(this);
            $this.css({"pointer-events":"none",
                        "transform":"scale(1.5)"});
	    			var $img = $($this.find("img"));
            $this.addClass("hvr");
	    			var index = containsObject($img, veggiesarray);
	    			if( index == -1){
	    				veggiesarray[veggiesarray.length] = $img;
	    			} else {
	    				veggiesarray.splice(index, 1);
	    			}
					nav_button_controls(200);
	    		});
	    		break;
    	case 5:
			sound_player("sound_"+ countNext);
    		var $this;
			cerealsarray = [];
   	    	$(".imageblock > div").click(function(){
	    			$this = $(this);
            $this.css({"pointer-events":"none",
                        "transform":"scale(1.5)"});
	    			var $img = $($this.find("img"));
                $this.addClass("hvr");
	    			var index = containsObject($img, cerealsarray);
	    			if( index == -1){
	    				cerealsarray[cerealsarray.length] = $img;
	    			} else {
	    				cerealsarray.splice(index, 1);
	    			}
            nav_button_controls(200);
	    		});
	    		break;
    	case 6:
			sound_player("sound_"+ countNext);
    		var $this;
    		meatarray = [];
   	    	$(".imageblock > div").click(function(){
	    			$this = $(this);
            $this.css({"pointer-events":"none",
                        "transform":"scale(1.5)"});
	    			var $img = $($this.find("img"));
            $this.addClass("hvr");
	    			var index = containsObject($img, meatarray);
	    			if( index == -1){
	    				meatarray[meatarray.length] = $img;
	    			} else {
	    				meatarray.splice(index, 1);
	    			}
          	nav_button_controls(200);
	    		});
	    		break;
    	case 7:
			sound_player("sound_"+ countNext);
    		var $this;

    		milkarray = [];
   	    	$(".imageblock > div").click(function(){
	    			$this = $(this);
            $this.css({"pointer-events":"none",
                        "transform":"scale(1.5)"});
	    			var $img = $($this.find("img"));
	    			var index = containsObject($img, milkarray);
            $this.addClass("hvr");
	    			if( index == -1){
	    				milkarray[milkarray.length] = $img;
	    			} else {
	    				milkarray.splice(index, 1);
	    			}
            	nav_button_controls(200);
	    		});
    		break;
    	case 9:
      $nextBtn.hide(0);
    		var $imagediv_last = $(".imagediv_last");
    		function append_images_to_div(div, images){
				for (var j = 0; j < images.length; j++){
					div.append(images[j].addClass("image_last animate_image_last"));
				}
    		}

    		for(var i = 0; i <$imagediv_last.length ; i++ ){
    			switch(i){
    				case 0:
    					append_images_to_div($($imagediv_last[i]), fruitsarray);
    					break;
    				case 1:
    					append_images_to_div($($imagediv_last[i]), veggiesarray);
    					break;
    				case 2:
    					append_images_to_div($($imagediv_last[i]), cerealsarray);
    					break;
    				case 3:
    					append_images_to_div($($imagediv_last[i]), meatarray);
    					break;
    				case 4:
    					append_images_to_div($($imagediv_last[i]), milkarray);
    					break;
    				default:
    					break;
    			}
    		}
          nav_button_controls(4000);
    		break;
    	default:
    		break;
    }

  }

/*=====  End of Templates Block  ======*/
function nav_button_controls(delay_ms){
  timeoutvar = setTimeout(function(){
    if(countNext==0){
      $nextBtn.show(0);
    } else if( countNext>0 && countNext == $total_page-1){
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else{
      $prevBtn.show(0);
      $nextBtn.show(0);
    }
  },delay_ms);
}
function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}
function sound_player1(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
    current_sound.on("complete", function(){
      nav_button_controls(200);
    });
}
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  // templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/lesson3/";
var soundAsset = $ref+"/audio_"+$lang+"/";

var content=[
  {
  	//page 0
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s1
  	}],
  	imageblockadditionalclass: "background_color1",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		}]
  	}]
 },{
 	//page 1
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s2
  	}],
  	imageblockadditionalclass: "background_color2",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"washing-42.png"
  		}]
  	}]
 },{
 	//page 2
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s3
  	}],
  	imageblockadditionalclass: "background_color3",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"wataaa-40.png"
  		}]
  	}]
 },{
 	//page 3
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s4
  	}],
  	imageblockadditionalclass: "background_color1",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"phohor-39-39.png"
  		}]
  	}]
 },{
 	//page 4
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s5
  	}],
  	imageblockadditionalclass: "background_color2",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"chips-39.png"
  		}]
  	}]
 },{
 	//page 5
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s6
  	}],
  	imageblockadditionalclass: "background_color3",
  	imageblock:[{
  		containerdiv: "container1",
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"phohor-39-39.png"
  		},{
  			imgclass: "fly1",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly2",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly3",
  			imgsrc: imgpath+"phohor-40.png"
  		}]
  	},{
  		containerdiv: "container2",
  		imagestoshow:[{
  			imgclass: "image_pasang",
  			imgsrc: imgpath+"surprised-38.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"phohor-39-39.png"
  		},{
  			imgclass: "fly1",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly2",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly3",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
	  		imgclass: "bacteria1",
	  		imgsrc: imgpath+"2c-27.png"
	  	},{
	  		imgclass: "bacteria2",
	  		imgsrc: imgpath+"2c-28.png"
	  	}],
  		// imagelabels:[{
	  		// imagelabelclass: "bacteria1"
	  	// },{
	  		// imagelabelclass: "bacteria2"
	  	// }]
  	},{
  		containerdiv: "container3",
  		imagestoshow:[{
  			imgclass: "image_pasang_2",
  			imgsrc: imgpath+"sick-41.png"
  		},{
  			imgclass: "image_2",
  			imgsrc: imgpath+"phohor-39-39.png"
  		},{
  			imgclass: "fly1",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly2",
  			imgsrc: imgpath+"phohor-40.png"
  		},{
  			imgclass: "fly3",
  			imgsrc: imgpath+"phohor-40.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock:[{
  		textclass: "ltb_text_1",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_2",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_3",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_4",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_5",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_6",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_7",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_8",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_9",
  		textdata: "&nbsp"
  	},{
  		textclass: "ltb_text_10",
  		textdata: "&nbsp"
  	}]
 },{
 	//page 6
  	contentnocenteradjust: true,
	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p3_s7
  	}],
  	imageblockadditionalclass: "background_color4",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image_pasang_3",
  			imgsrc: imgpath+"healthypasang.png"
  		},{
  			imgclass: "basket1",
  			imgsrc: imgpath+"8b-35.png"
  		},{
  			imgclass: "basket2",
  			imgsrc: imgpath+"8b-36.png"
  		}]
  	}]
 },{
 	//page 7
  	contentnocenteradjust: true,
    uppertextblockadditionalclass:"toptxt",
	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p3_s8
  	}],
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "container1 fadein1",
  			imgsrc : imgpath+"50-39.png"
  		},{
  			imgclass: "container2 fadein2",
  			imgsrc : imgpath+"51-43.png"
  		},{
  			imgclass: "container3 fadein3",
  			imgsrc : imgpath+"52-37.png"
  		}]
  	}]
 }
];


$(function() {
	var $board = $('.board');
	var $board_prev = $('.board_prev');
	var $board_next = $('.board_next');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p3_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var interval;
	function generalTemplate(slideleft) {
		var html = template(content[countNext]);
		var html_prev;
		var html_next = template(content[countNext+1]);;
		$board_next.html(html_next);

		if(countNext > 0){
			html_prev = template(content[countNext-1]);
		}

		$board_prev.html(html_prev);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);


		switch(countNext){
			case 0:
			case 2:
			case 3:
				sound_player1("sound_"+ countNext);
				break;
			case 1:
				sound_player1("sound_"+ countNext);
				break;
			case 4:
				sound_player1("sound_"+ countNext);

				break;
			case 5:
				sound_player1("sound_"+ countNext);
				var count = 0;
				var $container1 = $(".container1");
				var $container2 = $(".container2").hide(0);
				var $container3 = $(".container3").hide(0);
				interval = setInterval(function(){
									count++;
									count %= 5;
									switch(count){
										case 0:
											$container2.hide(0);
											$container3.addClass("fadeaway");
											setTimeout(function(){
												$container3.removeClass("fadeaway").hide(0);
											}, 2000);
											$container1.show(0);
											break;
										case 1:
											$container2.show(0);
											$container3.hide(0);
											$container1.hide(0);
											break;
										case 3:
											$container2.addClass("fadeaway");
											setTimeout(function(){
												$container2.removeClass("fadeaway").hide(0);

											}, 2000);
											$container1.hide(0);
											$container3.show(0);
											break;
										default:
											break;
									}

								}, 2000);
				break;
			case 6:
				sound_player1("sound_"+ countNext);
				break;
			case 7:
				sound_player1("sound_"+ countNext);
				$(".imageblock > img").show(0);
				break;
			default:
				break;
		}
	}

	/*=====  End of Templates Block  ======*/
  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.lessonEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}
function sound_player1(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
    current_sound.on("complete", function(){
      nav_button_controls(200);
    });
}
	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template

			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		if(countNext < 5){
			countNext++;
			$(this).hide(0);
			$board_next.addClass("move_left_2");
			$board.addClass("move_left_1");
			setTimeout(function(){
				$board_next.removeClass("move_left_2");
				$board.removeClass("move_left_1");
				templateCaller();
			}, 4000);
		}else {
			countNext++;
			 if(countNext == 6){
			 	 $(".additional_ltb > p").show(0).addClass("expand");
				 setTimeout(function(){
					 templateCaller();
				 }, 1550);
			 }else{
				templateCaller();
			 }
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(countNext < 6){
			countNext--;
			$(this).hide(0);
			$board.addClass("move_right_2");
			$board_prev.addClass("move_right_1");
			setTimeout(function(){
				$board.removeClass("move_right_2");
				$board_prev.removeClass("move_right_1");
				templateCaller();
			}, 4000);
		}else{
			countNext--;
			templateCaller();
		}

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

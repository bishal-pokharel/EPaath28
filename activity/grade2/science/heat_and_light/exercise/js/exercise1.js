var imgpath = $ref+"/exercise/images/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

 var sound_e_1 = new buzz.sound((audioPath + "exer1.ogg"));


var no_of_draggable = 4; //no of draggable to display at a time
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var content=[
	//1st slide
	{
		uppertextblock: [{
			textdata: data.string.ques1,
			textclass: "head-text"
		}],

		imageblock: [
		 {
			clickImg: [{
					imgsrc: imgpath +"candel.png",
					imgclass: "img img1 right",
					figcapdata: data.string.cap1,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"table.png",
					imgclass: "img img2",
					figcapdata:data.string.cap2,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"stove.png",
					imgclass: "img img3 right",
					figcapdata:data.string.cap3,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"bottle.png",
					imgclass: "img img4",
					figcapdata:data.string.cap4,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"firewood.png",
					imgclass: "img img5 right",
					figcapdata:data.string.cap5,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"book.png",
					imgclass: "img img6",
					figcapdata:data.string.cap6,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"heater.png",
					imgclass: "img img7 right",
					figcapdata:data.string.cap7,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"tudi.png",
					imgclass: "img img8 right",
					figcapdata:data.string.cap8,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"sun.png",
					imgclass: "img img9 right",
					figcapdata:data.string.cap9,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"tubelight.png",
					imgclass: "img img10 right",
					figcapdata:data.string.cap10,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"tablelamp.png",
					imgclass: "img img11 right",
					figcapdata:data.string.cap11,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"goat.png",
					imgclass: "img img12",
					figcapdata:data.string.cap12,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"lantin.png",
					imgclass: "img img13 right",
					figcapdata:data.string.cap13,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"cow.png",
					imgclass: "img img14",
					figcapdata:data.string.cap14,
					figcapclass: "cap"
				},{
					imgsrc: imgpath +"tourchlight.png",
					imgclass: "img img15 right",
					figcapdata:data.string.cap15,
					figcapclass: "cap"
				}],

			/*imgRightShow: [{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg1"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg2"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg3"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg4"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg5"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg6"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg7"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg8"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg9"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg10"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg11"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg12"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg13"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg14"
			},{
				imgsrc: imgpath + "correct.png",
				imgclass: "ansImg rightImg15"
			}],

			imgWrongShow: [{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg1"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg2"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg3"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg4"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg5"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg6"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg7"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg8"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg9"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg10"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg11"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg12"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg13"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg14"
			},{
				imgsrc: imgpath + "wrong.png",
				imgclass: "ansImg wrongImg15"
			}]	*/
		}
		]
	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 5;
	Handlebars.registerPartial("uppertextcontent", $("#uppertextcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }


	var testin = new LampTemplate();

 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);
		var showing = false;

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		$('.ansImg').hide(0);

		$(".exenextbtn").click(function(){
			$nextPage = $(".headfooter-next > span");
				if($nextPage.length == 0){
					$nextPage = $currentSection.next().next("span.imsectionClass");
					}
			/*console.log($nextPage + $nextPage.length);*/
			$nextPage.trigger('click');
		});

		$('.img').click(function(){
				if($(this).hasClass('right')){
					testin.update(true);
					testin.gotoNext();
					play_correct_incorrect_sound(1);
				}	else{
	 				testin.update(false);
					play_correct_incorrect_sound(0);
				}
		});

		switch(countNext) {
			case 0:
        play_audio(sound_e_1);
				$(".img1").click(function() {
					//$(".rightImg1").show(0);
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img1').unbind("click").css({cursor: "auto"});
				});

				$(".img2").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"wrong.png'>");
					$('.img2').unbind("click").css({cursor: "auto"});
				});

				$(".img3").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img3').unbind("click").css({cursor: "auto"});
				});

				$(".img4").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"wrong.png'>");
					$('.img4').unbind("click").css({cursor: "auto"});
				});

				$(".img5").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img5').unbind("click").css({cursor: "auto"});
				});

				$(".img6").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"wrong.png'>");
					$('.img6').unbind("click").css({cursor: "auto"});
				});

				$(".img7").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img7').unbind("click").css({cursor: "auto"});
				});

				$(".img8").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img8').unbind("click").css({cursor: "auto"});
				});

				$(".img9").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img9').unbind("click").css({cursor: "auto"});
				});

				$(".img10").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img10').unbind("click").css({cursor: "auto"});
				});

				$(".img11").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img11').unbind("click").css({cursor: "auto"});
				});

				$(".img12").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"wrong.png'>");
					$('.img12').unbind("click").css({cursor: "auto"});
				});

				$(".img13").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img13').unbind("click").css({cursor: "auto"});
				});

				$(".img14").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"wrong.png'>");
					$('.img14').unbind("click").css({cursor: "auto"});
				});

				$(".img15").click(function() {
					$(this).parent().append("<img class='corincimg' src='"+imgpath+"correct.png'>");
					$('.img15').unbind("click").css({cursor: "auto"});
				});
			break;
		}
	}
  function play_audio(audio){
  	audio.play();
  	audio.bindOnce('ended', function(){
  			navigationcontroller();
  			//$("#audiorep").addClass("enableclick");
  	});
  }

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/exercise/images/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

 var sound_e_2 = new buzz.sound((audioPath + "exer2.ogg"));


var no_of_draggable = 4; //no of draggable to display at a time
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var content=[
	//2th slide
	{
		// contentblockadditionalclass: "simplebg",
  		contentnocenteradjust: true,
  		textblockadditionalclass: 'instruction',
	  	textblock: [{
	  		textdata: data.string.draganddrop,
	  		textclass: 'head-title'
  		}],
	  	// draggableblockadditionalclass: 'frac_ques',
	  	draggableblock:[{
	  		draggables:[{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "heater.png",
                label:data.string.cap7
			},{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "tablelamp.png",
                label:data.string.cap11
			},{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "fire.png",
                label:data.string.cap17//need to change
			},{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "tourchlight.png",
                label:data.string.cap15
			},{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "koila.png",
                label:data.string.cap16
			},{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
  				imgclass: "",
  				imgsrc: imgpath + "candel.png",
                label:data.string.cap1
			},{
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
  				imgclass: "",
  				imgsrc: imgpath + "stove.png",
          label:data.string.cap3
            },
            {
	  			draggableclass:"class_1 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "firewood.png",
                label:data.string.cap5
			},{
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
				imgclass: "",
				imgsrc: imgpath + "tubelight.png",
                label:data.string.cap10
			},
      {
	  			draggableclass:"class_2 sliding hidden",
	  			has_been_dropped : false,
		      imgclass: "",
		      imgsrc: imgpath + "lantin.png",
          label:data.string.cap13
			},]
	  	}],
		droppableblock:[{
	  		droppables:[{
	  			headerdata: data.string.heat,
	  			headerclass: "identity",
	  			droppablecontainerclass:"",
	  			droppableclass:"drop_class_1",
				imgclass: "",
				imgsrc: imgpath + "100BLOCKS.png"
			},{
	  			headerdata: data.string.light,
	  			headerclass: "identity",
	  			droppablecontainerclass:"",
	  			droppableclass:"drop_class_2",
				imgclass: "",
				imgsrc: imgpath + "100BLOCKS.png"
			}]
    }],
    imageblock: [
     {
      clickImg: [{
          imgsrc: imgpath +"monkey.png",
          imgclass: "monkeyimg",
        }]
      }]

	}
];

var dummy_class = {
	draggableclass:"dummy_class hidden",
	imgclass: "",
	imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */
content[0].draggableblock[0].draggables.shufflearray();
for( var i = 1; i<no_of_draggable+1; i++){
	var asd = content[0].draggableblock[0].draggables[i-1].draggableclass.split('"')[0].split('hidden');
	content[0].draggableblock[0].draggables[i-1].draggableclass = asd[0]+'position_'+i;
	content[0].draggableblock[0].draggables.push(dummy_class);
}


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 10;
	Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());
	Handlebars.registerPartial("uppertextcontent", $("#uppertextcontent-partial").html());
  	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var drop = 0;
	/*random scoreboard eggs*/
	var wrngClicked = [false, false, false, false, false, false, false, false, false, false];

	var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$(".draggable").hover(function() {
			var datacontent = $(this).attr("data-answer");
			$(this).find("label").html(datacontent);
        }, function() {
			$(this).find("label").html('');
    });
		/*generate question no at the beginning of question*/
		switch(countNext) {
			case 0:
      play_audio(sound_e_2);

				$(".draggable").draggable({
					containment : "body",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(this).css({"opacity": "0.5"});
						$(ui.helper).addClass("ui-draggable-helper");
						$(ui.helper).removeClass("sliding");
					},
					stop: function(event, ui){
						$(this).css({"opacity": "1"});
					}
				});
				$('.drop_class_1').droppable({
					// accept : ".class_1",

					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_1")){
              play_correct_incorrect_sound(1);
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									testin.update(true);
								}
							}
							drop++;
                           gotoscorepage(drop);
							console.log(drop);
							handleCardDrop(event, ui, ".class_1" , ".drop_class_1");
						}else{
                  play_correct_incorrect_sound(0);
							if(($(ui.draggable).data("dropped"))==false){
								wrngClicked[countNext] = true;
								testin.update(false);
							}
						}

						if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				$('.drop_class_2').droppable({

					hoverClass : "hovered",
					drop:function(event, ui) {
						if (ui.draggable.hasClass("class_2")){
              play_correct_incorrect_sound(1);
							if(wrngClicked[countNext] == false){
								if(($(ui.draggable).data("dropped"))==false){
									testin.update(true);
								}
							}
							drop++;
							console.log(drop);
                            gotoscorepage(drop);
                            handleCardDrop(event, ui, ".class_2" , ".drop_class_2");
						}else{
                  play_correct_incorrect_sound(0);
							if(($(ui.draggable).data("dropped"))==false){
								wrngClicked[countNext] = true;
								testin.update(false);
							}
						}

						if(($(ui.draggable).data("dropped"))==false){
                countNext++;
                $(ui.draggable).data("dropped",true);
          	}
					}
				});

				function handleCardDrop(event, ui, classname, droppedOn) {
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					// var to count no. of divs in the droppable div
					var drop_index = $(droppedOn+">div").length;
					var top_position = 10*drop_index;
					var left_position = 18*drop_index;
					$(ui.draggable).removeClass("sliding");
					$(ui.draggable).detach().css({
						"cursor": 'auto',
						"width": "20%",
						"max-height": "45%",
						"flex": "0 0 20%"
					}).appendTo(droppedOn);
					var $newEntry = $(".draggableblock> .hidden").eq(0);

					var $draggable3;
					var $draggable2;
					var $draggable1;
					if(dropped.hasClass("position_4")){
						dropped.removeClass("position_4");
						$draggable3 = $(".position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_3")){
						dropped.removeClass("position_3");
						$draggable2 = $(".position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_2")){
						dropped.removeClass("position_2");
						$draggable1 = $(".position_1");
					}else if(dropped.hasClass("position_1")){
						dropped.removeClass("position_1");
					}

					if($draggable3 != null){
						 $draggable3.removeClass("position_3").addClass("position_4");
						 $draggable3.removeClass('sliding');
						 setTimeout(function() {
							    $draggable3.addClass('sliding');
						},1);
					}
					if($draggable2 != null){
						 $draggable2.removeClass("position_2").addClass("position_3");
						 $draggable2.removeClass('sliding');
						 setTimeout(function() {
							    $draggable2.addClass('sliding');
						},1);
					}
					if($draggable1 != null){
						 $draggable1.removeClass("position_1").addClass("position_2");
						 $draggable1.removeClass('sliding');
						 setTimeout(function() {
							    $draggable1.addClass('sliding');
						},1);
					}
					if($newEntry != null){
						 $newEntry.removeClass("hidden").addClass("position_1");
					}
					if(drop == 10) {
            $(".monkeyimg").show(0);
						/*$('#score').html(score);
						$('[select=yes]').fadeTo(1000,0).hide(0);
						$('.exefin').show(0);
						$('.contentblock').hide(0);
						$('.congratulation').show(0);*/
						//$nextBtn.show(0);
					}
				}
			break;
		}
	}

  function play_audio(audio){
  	audio.play();
  	audio.bindOnce('ended', function(){
  			navigationcontroller();
  			//$("#audiorep").addClass("enableclick");
  	});
  }

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(drop == 10) {
			$('#score').html(score);
			$('[select=yes]').fadeTo(1000,0).hide(0);
			$('.exefin').show(0);
			$('.contentblock').hide(0);
			$('.congratulation').show(0);
			$('.exenextbtn').show(0);
			$nextBtn.hide(0);
		}
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });
	//
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});



	function gotoscorepage(drop){
        if(drop==10){
            $nextBtn.show(0);
            $nextBtn.click(function(){
                testin.gotoNext();
            });
        }
        else{
            testin.gotoNext();
        }
	}
/*=====  End of Templates Controller Block  ======*/
});

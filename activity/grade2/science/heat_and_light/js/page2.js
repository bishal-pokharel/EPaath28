var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

var sound_c_1 = new buzz.sound((audioPath + "p2_s0.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "p2_s1.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "p2_s2.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "p2_s3.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "p2_s4.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "p2_s6.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "p2_s7.ogg"));
var sound_c_8 = new buzz.sound((audioPath + "p2_s8.ogg"));
var sound_c_9 = new buzz.sound((audioPath + "p2_s9.ogg"));
var content=[
	//1st slide
	{
		additionalclasscontentblock: "bluebackground",
		uppertextblock:[
		{
			textclass : "headingonly lokh1 cssfadein",
			textdata : data.string.p1text31,
		},
		{
			textclass : "headingonly lokh2 cssfadein",
			textdata : data.string.p1text32,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "squiwhat",
				imgsrc : imgpath + "images/squirrel_what.svg"
			},
			],
		}
		]
	},

	//2nd slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}
		],
		uppertextblock:[
		{
			textclass : "leftdivtext cssfadein",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext cssfadein",
			textdata : data.string.p1text25,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft cssfadein",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright cssfadein",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			],
		}
		]
	},

	//3rd slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		{
			textclass : "headingonly dragtext opa",
			textdata : data.string.p1text33,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},

			{
				imgclass: "flowerpot2 cssfadein",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],
		}
		]
	},

	//4th slide v2
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		{
			textclass : "headingonly dragtext opa",
			textdata : data.string.p1text34,
		},
		],
		imageblock: [
		{
			imagetoshow: [

			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 cssfadein",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],
		}
		]
	},


	//5th slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",

		filterdiv: true,
		filterdivclass: "purplefilterdiv",
		filtertextblock:[
		{
			textclass : "lefttopcaption opa moveright",
			textdata : data.string.p1text28,
		},
		{
			textclass : "calenno",
			textdata : "1",
		},
		{
			textclass : "days",
			textdata : data.string.p1text35,
		}
		],

		filterimageblock : [{
			imagetoshow : [
			{
				imgclass : "animclock",
				imgsrc : imgpath + "images/notepad.png"
			},
			]
		}],

		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 alreadydropped1 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],

		}
		]
	},

	//6th slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		{
			textclass : "lefttopcaption opa alreadymoveright",
			textdata : data.string.p1text28,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 alreadydropped1 cssfadeinzoom",
				imgsrc : imgpath + "images/deadflower.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],
		}
		]
	},

	//7th slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		{
			textclass : "leftdivcaption cssfadeinzoom",
			textdata : data.string.p1text26,
		},
		{
			textclass : "lefttopcaption opa alreadymoveright",
			textdata : data.string.p1text28,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 alreadydropped1 opa",
				imgsrc : imgpath + "images/deadflower.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],
		}
		]
	},
	//8th slide
	{
		leftsidediv: "card1",
		rightsidediv: "card2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opa",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opa",
			textdata : data.string.p1text25,
		},
		{
			textclass : "leftdivcaption opa",
			textdata : data.string.p1text26,
		},
		{
			textclass : "rightdivcaption cssfadeinzoom",
			textdata : data.string.p1text27,
		},
		{
			textclass : "lefttopcaption opa alreadymoveright",
			textdata : data.string.p1text28,
		}
		],
		imageblock: [
		{
			imagetoshow: [

			{
				imgclass: "imageleft opa",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 alreadydropped1 opa",
				imgsrc : imgpath + "images/deadflower.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			],
		}
		]
	},


	//9th slide
	{
		leftsidediv: "card1 cardmove",
		rightsidediv: "card2 cardmove2",
		pagedividevertical: [
		{
		}

		],
		uppertextblock:[
		{
			textclass : "leftdivtext opatemp cssfadeout",
			textdata : data.string.p1text24,
		},
		{
			textclass : "rightdivtext opatemp cssfadeout",
			textdata : data.string.p1text25,
		},

		{
			textclass : "leftdivcaption opatemp cssfadeout",
			textdata : data.string.p1text26,
		},
		{
			textclass : "rightdivcaption opatemp cssfadeout",
			textdata : data.string.p1text27,
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "plants",
			textclass : "plantsneed cssfadedelayed",
			textdata : data.string.p1text29
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "imageleft opatemp cssfadeout",
				imgsrc : imgpath + "images/darkroom.png"
			},
			{
				imgclass: "imageright opa cssscale",
				imgsrc : imgpath + "images/full-sunlight.png"
			},
			{
				imgclass: "flowerpot1 alreadydropped1 opatemp cssfadeout",
				imgsrc : imgpath + "images/deadflower.png"
			},
			{
				imgclass: "flowerpot2 alreadydropped2 opa cssscale2",
				imgsrc : imgpath + "images/sunflower01.png"
			},
			{
				imgclass: "squiimg cssfadedelayed",
				imgsrc : imgpath + "images/squirrel.png"
			},
			],
		}
		]
	},

	//10th slide
	{
		additionalclasscontentblock: "contentwithbg4",
		uppertextblock:[
		{
			textclass : "firstheading caption4 cssfadein",
			textdata : data.string.p1text30,
		}
		],
	},
	];

	$(function(){
		var $board = $(".board");

		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
		var countNext = 0;
		var total_page = 0;
		var $total_page = content.length;

		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();

		loadTimelineProgress($total_page, countNext + 1);
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
      */
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 			alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 			null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
				ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
				ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	 }
	 $(".activityPageTopTitleBar").append("<img id='audiorep' src='images/audioicons/icon-orange.png'/>");
	 var $soundreplay;
	 $soundreplay = $("#audiorep");
	 $soundreplay.click(function(){
	 	templateCaller();
	 });


	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				play_audio(sound_c_1);
			break;
			case 1:
				play_audio(sound_c_2);
			break;
			case 2:
			play_audio(sound_c_3);
				$nextBtn.hide(0);

				$(".flowerpot2").draggable({
					containment : ".generalTemplateblock",
					revert : "invalid",
					cursor : "move",
					zIndex: 1000,
				});

				$(".imageright").droppable({
					accept: ".flowerpot2",
					drop: function(event, ui){
						potdrop(event, ui);
					}
				});

				function potdrop(event, ui){
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $(this);

					if(dropped.hasClass("flowerpot2")) {
						dropped.addClass("floweradded");
							$(dropped.context).css({
								"z-index": "auto",
								"left": "80%",
								"top": "45%",
								"transform": "scale(0.7)",
								"filter": "drop-shadow(0 0 5px yellow)"
							});
						$nextBtn.show(0);
						}
					}
				break;

				case 3:
				play_audio(sound_c_4);
					$nextBtn.hide(0);

					$(".flowerpot1").draggable({
						containment : ".generalTemplateblock",
						revert : "invalid",
						cursor : "move",
						zIndex: 1000,
					});

					$(".imageleft").droppable({
						accept: ".flowerpot1",
						drop: ".flowerpot2",
					drop: function(event, ui){
						potdrop1(event, ui);
					}
					});

					function potdrop1(event, ui){
						ui.draggable.draggable('disable');
						var dropped = ui.draggable;
						var droppedOn = $(this);

						if(dropped.hasClass("flowerpot1")) {

							$(dropped.context).css({
								"z-index": "auto",
								"left": "30%",
								"top": "45%",
								"transform": "scale(0.7)",
								"filter": "grayscale(50%)"
							});
							$nextBtn.show(0);
						}
					}
				break;

				case 4:
					$nextBtn.hide(0);
					/*timer code*/
					var doUpdate = function() {
						$('.calenno').each(function() {
							var count = parseInt($(this).html());
							if (count !== 7) {
								$(this).html(count + 1);
							}
							else {
								$nextBtn.show(0);
                $prevBtn.show(0);
							}
						});
					};

			  	// Schedule the update to happen once every second
			  	var intervalStart = setInterval(doUpdate, 1000);
			 	 break;
            case 5:
                play_audio(sound_c_5);
                break;
				 case 6:
         setTimeout(function(){
           	play_audio(sound_c_6);
         },1000);

	 			break;
				case 7:
         setTimeout(function(){
				 play_audio(sound_c_7);
              },1000);
			 break;
			 case 8:
			 setTimeout(function(){
				play_audio(sound_c_8);
			}, 3000);
			break;
			case 9:
			 play_audio(sound_c_9);
		 break;

		}
	}
	function play_audio(audio){
		audio.play();
		audio.bindOnce('ended', function(){
				navigationcontroller();
				$("#audiorep").addClass("enableclick");
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

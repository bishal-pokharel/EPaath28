var imgpath = $ref + "/images/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

var sound_c_1 = new buzz.sound((audioPath + "p1_s0.ogg"));
var sound_c_2 = new buzz.sound((audioPath + "p1_s1.ogg"));
var sound_c_3 = new buzz.sound((audioPath + "p1_s2.ogg"));
var sound_c_4 = new buzz.sound((audioPath + "p1_s3.ogg"));
var sound_c_5 = new buzz.sound((audioPath + "p1_s4.ogg"));
var sound_c_6 = new buzz.sound((audioPath + "p1_s5.ogg"));
var sound_c_7 = new buzz.sound((audioPath + "p1_s6.ogg"));
var sound_c_8 = new buzz.sound((audioPath + "p1_s7.ogg"));
var sound_c_9 = new buzz.sound((audioPath + "p1_s9.ogg"));
var sound_c_10 = new buzz.sound((audioPath + "p1_s10.ogg"));
var sound_c_11 = new buzz.sound((audioPath + "p1_s11.ogg"));
var sound_c_12 = new buzz.sound((audioPath + "p1_s12.ogg"));
var sound_c_13 = new buzz.sound((audioPath + "p1_s13.ogg"));
var sound_c_14 = new buzz.sound((audioPath + "p1_s14.ogg"));
var sound_c_15 = new buzz.sound((audioPath + "p1_s15.ogg"));
var sound_c_16 = new buzz.sound((audioPath + "p1_s16.ogg"));
var sound_c_17 = new buzz.sound((audioPath + "p1_s17.ogg"));
var content=[
	//1st slide

	{

	hasheaderblock : false,
	contentblocknocenteradjust : true,
	additionalclasscontentblock : 'firstpagebg',
	uppertextblock : [{
		textdata : data.lesson.chapter,
		textclass : 'lesson-title vertical-horizontal-center'
		}]
	},
		{
		uppertextblock:[
		{
			textclass : "firstheading caption1",
			textdata : data.string.p1text1,
		}
		],
		extraimagearray:[
		{
			extraimage: "darkback cssfadeoutlong",
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bgfront",
				imgsrc : imgpath + "bgfront.png"
			},
			{
				imgclass: "bgback",
				imgsrc : imgpath + "bgback.png"
			},
			],
		}
		],
		curvedanim: [
		{
			animouter: "sunouter",
			imgclass: "sunrise sunanimation opa",
			imgsrc : imgpath + "sun.png"
		},
		]
	},

	//2nd slide
	{
		additionalclasscontentblock: "contentwithbg",
		uppertextblock:[
		{
			textclass : "firstheading caption1-1",
			textdata : data.string.p1text2,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sunfinal opa",
				imgsrc : imgpath + "sun.png"
			},
			{
				imgclass: "bgfront",
				imgsrc : imgpath + "bgfront.png"
			},
			{
				imgclass: "bgback",
				imgsrc : imgpath + "bgback.png"
			},
			],
		}
		]
	},
	//3rd slide
	{
		additionalclasscontentblock: "contentwithbg2",
		uppertextblock:[

		{
			textclass : "firstheading caption1-2",
			textdata : data.string.p1text3,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sunfinal2 opa",
				imgsrc : imgpath + "sun.png"
			},
			{
				imgclass: "cloud cloudanim3",
				imgsrc : imgpath + "clouds.png"
			},
			{
				imgclass: "cloud2 cloudanim2",
				imgsrc : imgpath + "clouds.png"
			},

			{
				imgclass: "cloud3 cloudanim",
				imgsrc : imgpath + "clouds.png"
			},
			],
		}
		],
	},
	//4th slide
	{
		additionalclasscontentblock: "contentwithbg3",
		uppertextblock:[

		{
			textclass : "firstheading caption1-2",
			textdata : data.string.p1text4,
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sunfinal2 opa",
				imgsrc : imgpath + "sun.png"
			},
			{
				imgclass: "cloud4 cloudanim4",
				imgsrc : imgpath + "clouds.png"
			},
			{
				imgclass: "cloud5 cloudanim5",
				imgsrc : imgpath + "clouds.png"
			},
			],
		}
		]
	},
	//5th slide
	{
		additionalclasscontentblock: "bluebackground",
		uppertextblock:[

		{
			textclass : "headingonly cssfadein",
			textdata : data.string.p1text5,
		}
		],
	},
	//6th slide
	{
		additionalclasscontentblock: "bluebackground",
		uppertextblock:[
		{
			textclass : "headingonly opa moveup",
			textdata : data.string.p1text5,
		},
		{
			textclass : "headingonly smallfont cssfadein",
			textdata : data.string.p1text6,
		}
		],
	},
	//7th slide
	{
		additionalclasscontentblock: "bluebackground",
		uppertextblock:[
		{
			textclass : "headingonly cssfadein",
			textdata : data.string.p1text7,
		}
		],
	},
	//8th slide
	{
		additionalclasscontentblock: "bluebackground",
		imageblockadditionalclass : "firstpage",
		extraimagearray:[
		{
			extraimage: "sourceheater sourceheaterfront",
		},
		{
			extraimage: "sourcewood sourcewoodfront",
		},

		{
			extraimage: "sourcelamp sourcelampfront",
		},
		{
			extraimage: "sourcetorch sourcetorchfront",
		},
		{
			extraimage: "sourcedung sourcedungfront",
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "heatimage2 heatfade2",
				imgsrc : imgpath + "tuki.png"
			},
			{
				imgclass: "sourceflame2 flamestart",
				imgsrc : imgpath + "tukiflame01.png"
			},
			],
		}
		]
	},
	//9th slide
	{
		additionalclasscontentblock: "bluebackground",
		imageblockadditionalclass : "firstpage",
		uppertextblock:[
		{
			textclass : "firstheading caption3 cssfadein",
			textdata : data.string.p1text8,
		}
		],
		extraimagearray:[
		{
			extraimage: "sourceheater sourceback",
		},
		{
			extraimage: "sourcewood sourceback",
		},
		{
			extraimage: "sourcelamp sourceback",
		},
		{
			extraimage: "sourcetorch sourceback",
		},
		{
			extraimage: "sourcedung sourceback",
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "heatimage2 opa",
				imgsrc : imgpath + "tuki.png"
			},
			{
				imgclass: "sourceflame3 flamestart",
				imgsrc : imgpath + "tukiflame01.png"
			},
			],
		}
		]
	},
	//10th slide
	{
		extraimagearray:[
		{
			extraimage: "sourceanimfire sourceback",
		},
		],
		uppertextblock:[
		{
			textclass : "sourcebackground",

		},
		{
			textclass : "sourcehead sourceheadwood cssfadein",
			textdata : data.string.p1text9,
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text10,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa wood",
				imgsrc : imgpath + "woodenchulo_withoutflame.png"
			},
			],
		}
		]
	},
	//11th slide
	{
		uppertextblock:[
		{
			textclass : "sourcebackground",
		},
		{
			textclass : "sourcehead sourceheadcandle cssfadein",
			textdata : data.string.p1text12,
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text13,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa girl",
				imgsrc : imgpath + "girl.png"
			},
			{
				imgclass: "sourcecandle opa",
				imgsrc : imgpath + "candle01.png"
			},
			{
				imgclass: "flame opa",
				imgsrc : imgpath + "flame.png"
			},
			{
				imgclass: "sourceflame flamesingle opa",
				imgsrc : imgpath + "candleflame.png"
			},
			],
		}
		]
	},
	//12th slide
	{
		extraimagearray:[
		{
			extraimage: "sourcetlamp sourceback",
		},
		],
		uppertextblock:[
		{
			textclass : "sourcebackground",
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text18,
		},
		{
			textclass : "sourcehead sourceheadstudy cssfadein",
			textdata : data.string.p1text17,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa study",
				imgsrc : imgpath + "studyroom02.png"
			},
			],
		}
		]
	},
	//13th slide
	{
		extraimagearray:[
		{
			extraimage: "sourcetorchlight sourceback",
		},
		],
		uppertextblock:[
		{
			textclass : "sourcebackground",
		},
		{
			textclass : "sourcehead sourceheadtorch cssfadein",
			textdata : data.string.p1text19,
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text20,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa torch",
				imgsrc : imgpath + "torch.png"
			},
			],
		}
		]
	},
	//14th slide
	{
		uppertextblock:[
		{
			textclass : "sourcebackground",
		},
		{
			textclass : "sourcehead sourceheadheater cssfadein",
			textdata : data.string.p1text15,
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text16,
		},
		],
		extraimagearray:[
		{
			extraimage: "sourceheater sourceheaterback",
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa heater",
				imgsrc : imgpath + "heater-room.png"
			},
			],
		}
		]
	},
	//15th slide image needs to be changed later
	{
		extraimagearray:[
		{
			extraimage: "sourceanimfire sourceback",
		},
		],
		uppertextblock:[
		{
			textclass : "sourcebackground",
		},
		{
			textclass : "sourcehead sourceheadwood cssfadein",
			textdata : data.string.p1text21,
		},
		{
			textclass : "sourcebody cssfadein",
			textdata : data.string.p1text22,
		},
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "sourceimage opa wood",
				imgsrc : imgpath + "cowdunkchulo_withoutflame.png"
			},
			],
		}
		]
	},
	//16th slide
	{
		additionalclasscontentblock: "bluebackground",
		imageblockadditionalclass : "firstpage",
		uppertextblock:[
		{
			textclass : "firstheading caption3 cssfadein",
			textdata : data.string.p1text23,
		}
		],
		extraimagearray:[
		{
			extraimage: "sourceheater sourceback",
		},
		{
			extraimage: "sourcewood sourceback",
		},
		{
			extraimage: "sourcelamp sourceback",
		},
		{
			extraimage: "sourcetorch sourceback",
		},
		{
			extraimage: "sourcedung sourceback",
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "heatimage2 opa",
				imgsrc : imgpath + "tuki.png"
			},
			{
				imgclass: "sourceflame3 flamestart",
				imgsrc : imgpath + "tukiflame01.png"
			},
			],
		}
		]
	},
//16th slide
{
	additionalclasscontentblock: "dogbackground",
	uppertextblock:[
	{
		textclass : "firstheading caption1-3 cssfadein",
		textdata : data.string.p1textdog,
	}
	],
}
];

$(function(){

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;
	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	loadTimelineProgress($total_page, countNext + 1);

/*
	inorder to use the handlebar partials we need to register them
	to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


  /*======================================================
=            Navigation Controller Function            =
======================================================*/
/**
How To:
- Just call the navigation controller if it is to be called from except the
  last page of lesson
- If called from last page set the islastpageflag to true such that
  footernotification is called for continue button to navigate to exercise
  */

/**
  What it does:
  - If not explicitly overriden the method for navigation button
    controls, it shows the navigation buttons as required,
    according to the total count of pages and the countNext variable
  - If for a general use it can be called from the templatecaller
    function
  - Can be put anywhere in the template function as per the need, if
    so should be taken out from the templatecaller function
  - If the total page number is
  */
  function navigationcontroller(islastpageflag) {
  	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

  	if (countNext == 0 && $total_page != 1) {
  		$nextBtn.delay(800).show(0);
  		$prevBtn.css('display', 'none');
  	} else if ($total_page == 1) {
  		$prevBtn.css('display', 'none');
  		$nextBtn.css('display', 'none');

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	} else if (countNext > 0 && countNext < $total_page - 1) {
		$nextBtn.show(0);
		$prevBtn.show(0);
	} else if (countNext == $total_page - 1) {
		$nextBtn.css('display', 'none');
		$prevBtn.show(0);

		// if lastpageflag is true
		islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
}
$(".activityPageTopTitleBar").append("<img id='audiorep' src='images/audioicons/icon-orange.png'/>");
var $soundreplay;
$soundreplay = $("#audiorep");
$soundreplay.click(function(){
	templateCaller();
});
function generalTemplate(){
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var html = template(content[countNext]);

	$board.html(html);

	// highlight any text inside board div with datahighlightflag set true
	texthighlight($board);
	vocabcontroller.findwords(countNext);

	//$("#audiorep").removeClass("enableclick");


	// var div_ratio =$(".sourceheater").width()/$(".sourceheater").parent().width();
	// $(".sourceheater").width(Math.round($(".sourceheater").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcewood").width()/$(".sourcewood").parent().width();
	// $(".sourcewood").width(Math.round($(".sourcewood").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcelamp").width()/$(".sourcelamp").parent().width();
	// $(".sourcelamp").width(Math.round($(".sourcelamp").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcetorch").width()/$(".sourcetorch").parent().width();
	// $(".sourcetorch").width(Math.round($(".sourcetorch").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcedung").width()/$(".sourcedung").parent().width();
	// $(".sourcedung").width(Math.round($(".sourcedung").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcetlamp").width()/$(".sourcetlamp").parent().width();
	// $(".sourcetlamp").width(Math.round($(".sourcetlamp").parent().width()*div_ratio));
  //
	// var div_ratio =$(".sourcetorchlight").width()/$(".sourcetorchlight").parent().width();
	// $(".sourcetorchlight").width(Math.round($(".sourcetorchlight").parent().width()*div_ratio));


	switch(countNext){
		case 0:
			play_audio(sound_c_1);
		break;
		case 1:
		 setTimeout(function(){
			 play_audio(sound_c_2);
		 }, 6000);
		break;
		case 2:
			play_audio(sound_c_3);
		break;
		case 3:
			play_audio(sound_c_4);
		break;
		case 4:
			play_audio(sound_c_5);
		break;
		case 5:
			play_audio(sound_c_6);
		break;
		case 6:
			play_audio(sound_c_7);
		break;
		case 7:
			play_audio(sound_c_8);
		break;
		case 8:
			$nextBtn.hide(0);
      $prevBtn.hide(0);
      $prevBtn.delay(6000).show(0);
			$nextBtn.delay(6000).show(0);
		break;
		case 9:
			play_audio(sound_c_9);
		break;
		case 10:
			play_audio(sound_c_10);
		break;
		case 11:
			play_audio(sound_c_11);
		break;
		case 12:
			play_audio(sound_c_12);
		break;
		case 13:
			play_audio(sound_c_13);
		break;
		case 14:
			play_audio(sound_c_14);
		break;
		case 15:
			play_audio(sound_c_15);
		break;
		case 16:
			play_audio(sound_c_16);
		break;
		case 17:
			play_audio(sound_c_17);
		break;
	}
}

function play_audio(audio){
	audio.play();
	audio.bindOnce('ended', function(){
			navigationcontroller();
			$("#audiorep").addClass("enableclick");
	});
}

function templateCaller(){
	//convention is to always hide the prev and next button and show them based
	//on the convention or page index
	$prevBtn.hide(0);
	$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

	//navigationcontroller();

	generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


}

$nextBtn.on("click", function(){
	countNext++;
	templateCaller();
});

$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
	countNext--;
	templateCaller();
	/* if footerNotificationHandler pageEndSetNotification was called then on click of
	previous slide button hide the footernotification */
	countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});
total_page = content.length;
templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

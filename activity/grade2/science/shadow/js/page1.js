var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "chapter coverpagetext",
        uppertextblock: [
            {
                textclass: "datafont centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock:[{
        imagestoshow: [
            {
                imgdiv: "coverpage",
                imgclass: "relativecls coverpageimg",
                imgid: 'coverpageImg',
                imgsrc: ""
            },

        ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi hide2",
                    imgclass: "relativecls img3",
                    imgid: 'rubiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi hide1",
                    imgclass: "relativecls img5",
                    imgid: 'rubi1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img4",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
              textdiv:"dial1msg fadeInEffect",
              textclass:"centertext content",
              textdata:data.string.p1text1
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi hide2",
                    imgclass: "relativecls img3",
                    imgid: 'sagarImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi hide1",
                    imgclass: "relativecls img3",
                    imgid: 'sagar1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial2 fadeInEffect",
                    imgclass: "relativecls img4",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"dial2msg fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text2
            },
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar hide2",
                    imgclass: "relativecls img3",
                    imgid: 'sagar2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sagar hide1",
                    imgclass: "relativecls img3",
                    imgid: 'sagargifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial3 fadeInEffect",
                    imgclass: "relativecls img4",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"dial3msg fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text3
            },
        ]
    },
    // slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi1 hide2",
                    imgclass: "relativecls img3",
                    imgid: 'rubi2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "rubi1 hide1",
                    imgclass: "relativecls img5",
                    imgid: 'rubi3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4 fadeInEffect",
                    imgclass: "relativecls img4",
                    imgid: 'dialImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"dial4msg fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text4
            },
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls bg1 img1",
                    imgid: 'bg2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun hide1",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "pimg",
                    imgclass: "relativecls img3",
                    imgid: 'sagarhandImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circle1 hide1",
                    imgclass: "relativecls img4",
                    imgid: 'circleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circle2 hide1",
                    imgclass: "relativecls img5",
                    imgid: 'circleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "circle3 hide1",
                    imgclass: "relativecls img6",
                    imgid: 'circleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "bubble hide1",
                    imgclass: "relativecls img7",
                    imgid: 'bubbleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1 hide1",
                    imgclass: "relativecls img8",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow2 hide1",
                    imgclass: "relativecls img9",
                    imgid: 'arrow2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow3 hide1",
                    imgclass: "relativecls img10",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "newcloud hide1",
                    imgclass: "relativecls img11",
                    imgid: 'newcloudImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"lightdiv hide1",
                textclass:"relativecls centertext content",
                textdata:data.string.light
            },
            {
                textdiv:"treediv hide1",
                textclass:"relativecls centertext content",
                textdata:data.string.tree
            },
            {
                textdiv:"shadowdiv hide1",
                textclass:"relativecls centertext content",
                textdata:data.string.shadow
            },
            {
                textdiv:"clouddiv hide1",
                textclass:"relativecls centertext content",
                textdata:data.string.p1text5
            }
        ]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bg5Img",
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: "bg5Img",
                    imgsrc: ""
                },
                {
                    imgdiv: "hand clickdiv",
                    imgclass: "relativecls img2",
                    imgid: "handImg",
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"maintext hide2 clickdiv",
                textclass:"relativecls centertext content1",
                textdata:data.string.p1text7
            },
            {
                textdiv:"maintext hide1",
                textclass:"relativecls centertext content1",
                textdata:data.string.p1text6
            }
        ]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src: imgpath + "bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg1Img", src: imgpath + "bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg2Img", src: imgpath + "bg-11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg3Img", src: imgpath + "bg-12.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg4Img", src: imgpath + "bg-12.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg5Img", src: imgpath + "shadows.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src: imgpath + "sun01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rubiImg", src: imgpath + "rubi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rubi1Img", src: imgpath + "rubi.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rubi2Img", src: imgpath + "rubi01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rubi3Img", src: imgpath + "rubi01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagarImg", src: imgpath + "sagar.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagar1Img", src: imgpath + "sagar.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath + "text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial2Img", src: imgpath + "text_box02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagar2Img", src: imgpath + "sagar01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagargifImg", src: imgpath + "sagar01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sagarhandImg", src: imgpath + "sagar02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "circleImg", src: imgpath + "circles.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bubbleImg", src: imgpath + "clouds.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath + "arrow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow2Img", src: imgpath + "arrow02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "newcloudImg", src: imgpath + "clouds02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "coverpageImg", src:imgpath+"images_for_diy/hand.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "S1_P1", src: soundAsset + "S1_P1.ogg"},
            {id: "S1_P2", src: soundAsset + "S1_P2.ogg"},
            {id: "S1_P3", src: soundAsset + "S1_P3.ogg"},
            {id: "S1_P4", src: soundAsset + "S1_P4.ogg"},
            {id: "S1_P5", src: soundAsset + "S1_P5.ogg"},
            {id: "S1_P6_1", src: soundAsset + "S1_P6_1.ogg"},
            {id: "S1_P6_2", src: soundAsset + "S1_P6_2.ogg"},
            {id: "S1_P6_3", src: soundAsset + "S1_P6_3.ogg"},
            {id: "S1_P6_4", src: soundAsset + "S1_P6_4.ogg"},
            {id: "S1_P8_1", src: soundAsset + "S1_P8_1.ogg"},
            {id: "S1_P8_2", src: soundAsset + "S1_P8_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
              sound_player("S1_P"+(countNext+1),1);
                showhideelm($(".hide1"),$(".hide2"),3000,3000);
                // navigationcontroller(countNext, $total_page);
                break;
            case 5:
                $(".hide1").hide();
                $(".circle1").delay(1000).fadeIn(1000);
                $(".circle2").delay(1500).fadeIn(1000);
                $(".circle3").delay(2000).fadeIn(1000);
                $(".bubble").delay(2500).fadeIn(1000);
                var image_src = preload.getResult("bg3Img").src;
                $(".bubble").delay(500).animate({"width":"200%","right":"35%","left":"-35%","top":"-45%","opacity":"0.3"},2000);
                setTimeout(function(){
                    $(".bg1").attr("src",image_src);
                    $(".pimg").hide();
                    $(".hide1").hide();
                    $(".sun").fadeIn(1000);
                        createjs.Sound.stop();
                        current_sound = createjs.Sound.play("S1_P6_1");
                        current_sound.play();
                        $(".arrow1,.lightdiv").fadeIn(1000);
                        current_sound.on('complete', function(){
                            setTimeout(function(){
                                $(".arrow2,.treediv").fadeIn(1000);
                                createjs.Sound.stop();
                                current_sound = createjs.Sound.play("S1_P6_2");
                                current_sound.play();
                                current_sound.on('complete', function(){
                                    setTimeout(function(){
                                        $(".arrow3,.shadowdiv").fadeIn(1000);
                                        createjs.Sound.stop();
                                        current_sound = createjs.Sound.play("S1_P6_3");
                                        current_sound.play();
                                        current_sound.on('complete', function(){
                                            setTimeout(function(){
                                                $(".newcloud,.clouddiv").fadeIn(1000);
                                                createjs.Sound.stop();
                                                current_sound = createjs.Sound.play("S1_P6_4");
                                                current_sound.play();
                                                current_sound.on('complete', function(){
                                                    nav_button_controls(1000);
                                                });
                                            },1500)
                                        });
                                    },1500)
                                });
                            },1500)
                        });
                },5000);
                break;
            case 7:
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("S1_P8_1");
                current_sound.play();
                current_sound.on('complete', function(){
                  $(".hand").fadeIn(1000);
                  $(".clickdiv").click(function(){
                      $(".hide2,.hand").hide();
                      $(".hide1").addClass("flipdiv").show();
                      sound_player("S1_P8_2",1);
                  });
                });
                $(".hide1,.hand").hide();
            break;
            case 6:
              nav_button_controls(3500);
            break;
            default:
              sound_player("S1_P"+(countNext+1),1);
                // navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(100): "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "middletext zoomInEffect",
        uppertextblock: [
            {
                textclass: "title centertext",
                textdata: data.string.p2text1
            }

        ],
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "rabbit fadeInEffect ",
                    imgclass: "relativecls img3",
                    imgid: 'rabbitImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "elephant hide1 ",
                    imgclass: "relativecls img4",
                    imgid: 'elephantImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"div1 hide1",
                textclass:"centertext content",
                textdata:data.string.p2text2
            },
            {
                textdiv:"div2 hide1",
                textclass:"centertext content",
                textdata:data.string.p2text3
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun1",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"div3 slideL",
                textclass:"centertext content",
                textdata:data.string.p2text4
            },
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bg1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun2",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "clouds moveClouds",
                    imgclass: "relativecls img3",
                    imgid: 'cloudsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "hand1 clickdiv",
                    imgclass: "relativecls img4",
                    imgid: "handImg",
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"div3 hide2 clickdiv",
                textclass:"centertext content4",
                textdata:data.string.p2text5
            },
            {
                textdiv:"div3 hide1",
                textclass:"centertext content4",
                textdata:data.string.p2text6
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src: imgpath + "bg_elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg1Img", src: imgpath + "village.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src: imgpath + "sun01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbitImg", src: imgpath + "rabbit.png", type: createjs.AbstractLoader.IMAGE},
            {id: "elephantImg", src: imgpath + "elephant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cloudsImg", src: imgpath + "clouds_with_shadow01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "S2_P1", src: soundAsset + "S2_P1.ogg"},
            {id: "S2_P2_1", src: soundAsset + "S2_P2_1.ogg"},
            {id: "S2_P2_2", src: soundAsset + "S2_P2_2.ogg"},
            {id: "S2_P3", src: soundAsset + "S2_P3.ogg"},
            {id: "S2_P4_1", src: soundAsset + "S2_P4_1.ogg"},
            {id: "S2_P4_2", src: soundAsset + "S2_P4_2.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 1:
              $(".hide1").hide();
              setTimeout(function(){
                  createjs.Sound.stop();
                  current_sound = createjs.Sound.play("S2_P2_1");
                  current_sound.play();
                  $(".div1").fadeIn(1000);
                  current_sound.on('complete', function(){
                    $(".elephant").fadeIn(1000);
                    $(".div2").fadeIn(1000);
                    sound_player("S2_P2_2",1);
                  });
              },2000);
            break;
            case 3:
            $(".hide1,.hand1,.hide2").hide();
              setTimeout(function(){
                createjs.Sound.stop();
                current_sound = createjs.Sound.play("S2_P4_1");
                current_sound.play();
                $(".hide2").fadeIn(1000);
                current_sound.on('complete', function(){
                  $(".hand1").fadeIn(1000);
                $(".clickdiv").click(function(){
                    $(".hide2,.hand1").hide();
                    $(".hide1").addClass("flipdiv").show();
                    sound_player("S2_P4_2",1);
                });
              });
              },5000);
            break;
            default:
                sound_player('S2_P'+(countNext+1),1);
              break;
        }
    }


    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ?nav_button_controls(100) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }

});

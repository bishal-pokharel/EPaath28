var imgpath = $ref + "/images/images_for_diy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "middletext",
        uppertextblock: [
            {
                textclass: "datafont centertext",
                textdata: data.string.fun
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'funImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background zoomImg",
                    imgclass: "relativecls img2",
                    imgid: 'handImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"div1",
                textclass:"centertext content",
                textdata:data.string.p4text1
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "option girl",
                    imgclass: "relativecls img1",
                    imgid: 'girlImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt1Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt2Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt3Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "option opt4",
                    imgclass: "relativecls img5",
                    imgid: 'opt4Img',
                    imgsrc: "",
                },
                {
                    imgdiv: "option box fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'boxImg',
                    imgsrc: "",
                },
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv fadeInEffect",
                textclass:"centertext content3",
                textdata:data.string.p4text2
            },
            {
                textdiv:"leftdiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"rightdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "funImg", src: imgpath + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src: imgpath + "hand.png", type: createjs.AbstractLoader.IMAGE},
            {id: "girlImg", src: imgpath + "asha.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt1Img", src: imgpath + "hand01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt2Img", src: imgpath + "hand02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt3Img", src: imgpath + "hand03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt4Img", src: imgpath + "hand04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "bubble-2.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "S4_P1", src: soundAsset + "S4_P1.ogg"},
            {id: "S4_P2", src: soundAsset + "S4_P2.ogg"},
            {id: "S4_P3", src: soundAsset + "S4_P3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 2:
              sound_player("S4_P"+(countNext+1),1);
                // navigationcontroller(countNext, $total_page,true);
                break;
            default:
              sound_player("S4_P"+(countNext+1),1);
                // navigationcontroller(countNext, $total_page);
                break;
        }
    }


        function nav_button_controls(delay_ms){
          timeoutvar = setTimeout(function(){
            if(countNext==0){
              $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
              $prevBtn.show(0);
              ole.footerNotificationHandler.lessonEndSetNotification();
            } else{
              $prevBtn.show(0);
              $nextBtn.show(0);
            }
          },delay_ms);
        }
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(100) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }

    function shufflehint(){
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        var optionclass = ["option opt1","option opt2","option opt3"]
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).find("img").attr("data-answer"));
        });
    }
    function checkans(){
        $(".option").on("click",function () {
            if($(this).find("img").attr("data-answer").toString().trim() == $(".question").find("img").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctWrongImg' src='images/right.png'/>")
                $(this).find(".corctopt").show();
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).append("<img class='correctWrongImg' src='images/wrong.png'/>")
                $(this).find(".wrngopt").show();
                play_correct_incorrect_sound(0);
            }
        });
    }
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

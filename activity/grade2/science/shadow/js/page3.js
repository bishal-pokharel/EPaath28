var imgpath = $ref + "/images/images_for_diy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "middletext",
        uppertextblock: [
            {
                textclass: "datafont centertext",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'diyImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "noticeboard",
                    imgclass: "relativecls img2",
                    imgid: 'boardImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "duck",
                    imgclass: "relativecls img1",
                    imgid: 'duckImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"div1",
                textclass:"centertext content",
                textdata:data.string.p3text1
            },
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q1Img',
                    imgsrc: "",
                    ans:"bowl"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt1Img',
                    imgsrc: "",
                    ans:"bowl"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt2Img',
                    imgsrc: "",
                    ans:"book"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt3Img',
                    imgsrc: "",
                    ans:"pot"
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext chapter",
                textdata:data.string.p3text2
            },
            {
                textdiv:"leftdiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"rightdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q2Img',
                    imgsrc: "",
                    ans:"pot"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt1Img',
                    imgsrc: "",
                    ans:"bowl"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt2Img',
                    imgsrc: "",
                    ans:"book"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt3Img',
                    imgsrc: "",
                    ans:"pot"
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext chapter",
                textdata:data.string.p3text2
            },
            {
                textdiv:"leftdiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"rightdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q3Img',
                    imgsrc: "",
                    ans:"book"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt1Img',
                    imgsrc: "",
                    ans:"bowl"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt2Img',
                    imgsrc: "",
                    ans:"book"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt3Img',
                    imgsrc: "",
                    ans:"pot"
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext chapter",
                textdata:data.string.p3text2
            },
            {
                textdiv:"leftdiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"rightdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide5
    // {
    //     contentnocenteradjust: true,
    //     contentblockadditionalclass: "bg",
    //     imageblock: [{
    //         imagestoshow: [
    //             {
    //                 imgdiv: "noticeboard1",
    //                 imgclass: "relativecls img2",
    //                 imgid: 'board1Img',
    //                 imgsrc: ""
    //             },
    //             {
    //                 imgdiv: "duck1",
    //                 imgclass: "relativecls img1",
    //                 imgid: 'duckImg',
    //                 imgsrc: ""
    //             }
    //         ]
    //     }],
    //     textblock:[
    //         {
    //             datahighlightflag:true,
    //             datahighlightcustomclass:'colorcss',
    //             textdiv:"div2",
    //             textclass:"centertext content",
    //             textdata:data.string.p3text3
    //         },
    //     ]
    // },
    // //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "noticeboard2",
                    imgclass: "relativecls img2",
                    imgid: 'boardImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "duck2",
                    imgclass: "relativecls img1",
                    imgid: 'duckImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"div3",
                textclass:"centertext content",
                textdata:data.string.p3text4
            },
            {
                textdiv:"div4",
                textclass:"centertext diyfont",
                textdata:data.string.conclusion
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath + "a_08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duckImg", src: imgpath + "duckling-11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boardImg", src: imgpath + "board.png", type: createjs.AbstractLoader.IMAGE},
            {id: "board1Img", src: imgpath + "clouds.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q1Img", src: imgpath + "bowl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q2Img", src: imgpath + "pot.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3Img", src: imgpath + "book.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt1Img", src: imgpath + "bowl01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt2Img", src: imgpath + "book01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt3Img", src: imgpath + "pot01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "S3_P2", src: soundAsset + "S3_P2.ogg"},
            {id: "S3_P3", src: soundAsset + "S3_P3.ogg"},
            {id: "S3_P6", src: soundAsset + "S3_P6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
              play_diy_audio();
              nav_button_controls(2000);
            break;
            case 2:
            case 3:
            case 4:
              countNext==2?sound_player("S3_P"+(countNext+1),0):'';
                shufflehint();
                checkans();
                break;
            default:
              sound_player("S3_P"+(countNext+1),1);
                // navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.pageEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(100) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }

    function shufflehint(){
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        var optionclass = ["option opt1","option opt2","option opt3"]
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).find("img").attr("data-answer"));
        });
    }
    function checkans(){
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).find("img").attr("data-answer").toString().trim() == $(".question").find("img").attr("data-answer").toString().trim()) {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctWrongImg' src='images/right.png'/>")
                $(this).find(".corctopt").show();
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).append("<img class='correctWrongImg' src='images/wrong.png'/>")
                $(this).find(".wrngopt").show();
                play_correct_incorrect_sound(0);
            }
        });
    }
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
});

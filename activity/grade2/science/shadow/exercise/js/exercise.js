var imgpath = $ref + "/images/exercise/";
var imgpath1 = $ref + "/images/exercise/option/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q1Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'boyImg',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'rabbitImg',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question hide2",
                    imgclass: "relativecls img1",
                    imgid: 'q2Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "question hide1",
                    imgclass: "relativecls img5",
                    imgid: 'q2aImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'potImg',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'bookImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'cupImg',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q3Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'b1Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'b2Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'b3Img',
                    imgsrc: "",
                    ans:"correct"
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question hide2",
                    imgclass: "relativecls img1",
                    imgid: 'q4Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "question hide1",
                    imgclass: "relativecls img5",
                    imgid: 'q4aImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt10Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt11Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt12Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question hide2",
                    imgclass: "relativecls img1",
                    imgid: 'q5Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "question hide1",
                    imgclass: "relativecls img5",
                    imgid: 'q5aImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt13Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt14Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt15Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question hide2",
                    imgclass: "relativecls img1",
                    imgid: 'q6Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "question hide1",
                    imgclass: "relativecls img5",
                    imgid: 'q6aImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt13Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt14Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt15Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q7Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'boyImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt17Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'b2Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q8Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt19Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt20Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt21Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question",
                    imgclass: "relativecls img1",
                    imgid: 'q9Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'elephantImg',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'boyImg',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'rabbitImg',
                    imgsrc: "",
                    ans:"correct"
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "question hide2",
                    imgclass: "relativecls img1",
                    imgid: 'q10Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "question hide1",
                    imgclass: "relativecls img5",
                    imgid: 'q10aImg',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt1",
                    imgclass: "relativecls img2",
                    imgid: 'opt17Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "option opt2",
                    imgclass: "relativecls img3",
                    imgid: 'opt14Img',
                    imgsrc: "",
                    ans:""
                },
                {
                    imgdiv: "option opt3",
                    imgclass: "relativecls img4",
                    imgid: 'opt22Img',
                    imgsrc: "",
                    ans:""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"topdiv",
                textclass:"centertext content",
                textdata:data.string.question
            },
            {
                textdiv:"questiondiv",
                textclass:"",
                textdata:""
            },
            {
                textdiv:"answerdiv",
                textclass:"",
                textdata:""
            }
        ]
    },
];
content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var scoreupdate = 11;
    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var score = new NumberTemplate();
    score.init($total_page-1);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "q1Img", src: imgpath + "q01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q2Img", src: imgpath + "q02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q2aImg", src: imgpath + "q02a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3Img", src: imgpath + "q03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q4Img", src: imgpath + "q04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q4aImg", src: imgpath + "q04a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q5Img", src: imgpath + "q05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q5aImg", src: imgpath + "q05a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q6Img", src: imgpath + "q06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q6aImg", src: imgpath + "q06a.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q7Img", src: imgpath + "q07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q8Img", src: imgpath + "q08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q9Img", src: imgpath + "q09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q10Img", src: imgpath + "q10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q10aImg", src: imgpath + "q10a.png", type: createjs.AbstractLoader.IMAGE},


            {id: "elephantImg", src: imgpath1 + "option01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boyImg", src: imgpath1 + "option02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rabbitImg", src: imgpath1 + "option03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "potImg", src: imgpath1 + "option04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bookImg", src: imgpath1 + "option05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "cupImg", src: imgpath1 + "option06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b1Img", src: imgpath1 + "option07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b2Img", src: imgpath1 + "option08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b3Img", src: imgpath1 + "option09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "b3Img", src: imgpath1 + "option09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt10Img", src: imgpath1 + "option10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt11Img", src: imgpath1 + "option11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt12Img", src: imgpath1 + "option12.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt13Img", src: imgpath1 + "option13.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt14Img", src: imgpath1 + "option14.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt15Img", src: imgpath1 + "option15.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt16Img", src: imgpath1 + "option16.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt17Img", src: imgpath1 + "option17.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt18Img", src: imgpath1 + "option18.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt19Img", src: imgpath1 + "option19.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt20Img", src: imgpath1 + "option20.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt21Img", src: imgpath1 + "option21.png", type: createjs.AbstractLoader.IMAGE},
            {id: "opt22Img", src: imgpath1 + "option22.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "instruction", src: soundAsset + "instruction.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        score.init(content.length);
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        put_image();
        // var questionnumber = countNext +1;
        score.numberOfQuestions();
        // $("#num_ques").text(questionnumber+". ");
        countNext==0?sound_player("instruction",0):'';
        switch (countNext) {
            default:
                $(".hide1").hide();
                shufflehint();
                checkans();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });

    // $refreshBtn.click(function(){
    //     templateCaller();
    // });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }

    function shufflehint(){
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find(".option").length; i >= 0; i--) {
            optdiv.append(optdiv.find(".option").eq(Math.random() * i | 0));
        }
        optdiv.find(".option").removeClass().addClass("current");
        var optionclass = ["option opt1","option opt2","option opt3"]
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
            $(this).addClass($(this).find("img").attr("data-answer"));
        });
    }
    function checkans(){
        var wrongclick = false;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).find("img").attr("data-answer").toString().trim() == $(".question").find("img").attr("data-answer").toString().trim()) {
                $(this).addClass("correctcss");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctWrongImg' src='images/right.png'/>")
                $(this).find(".corctopt").show();
                play_correct_incorrect_sound(1);
                $(".hide1").fadeIn(1000);
                $(".hide2").fadeOut(1000);
                if(!wrongclick && scoreupdate != countNext){
                    score.update(true);
                    scoreupdate = countNext;
                }
                navigationcontroller(countNext, $total_page);
            }
            else{
                $(this).addClass("wrongcss");
                $(this).append("<img class='correctWrongImg' src='images/wrong.png'/>")
                $(this).find(".wrngopt").show();
                play_correct_incorrect_sound(0);
                wrongclick = true;
                scoreupdate = countNext;
                score.update(false);
            }
        });
    }
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }
    function put_image() {
        var contentCount = content[countNext];
        var imageblockcontent=contentCount?contentCount.hasOwnProperty('imageblock'):null;
        imageblockcontent? dynamicimageload(imageblockcontent,contentCount):"";
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = "";
                imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

});

var imgpath = $ref+"/exercise/images/";

/*
	for rhino
*/
var checkpoint_positions = [];
var rhino_animation;
var screen_factor;
var current_question = 0;
var screen_position = 0;
var full_width;
var total_question = 10;

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
	//slide 0
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques1,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/mosquito.png",
				labeltext: data.string.mos
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/cat.png",
				labeltext: data.string.cat
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/croc.png",
				labeltext: data.string.croc
			},

			]
		}
		]
	},
	{
	//slide 1
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques2,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/tiger.png",
				labeltext: data.string.tig
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/vulture.png",
				labeltext: data.string.vul
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/elephant.png",
				labeltext: data.string.ele
			},

			]
		}
		]
	},
	{
	//slide 2
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques3,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/bee.png",
				labeltext: data.string.bee
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/cat.png",
				labeltext: data.string.cat
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/cow.png",
				labeltext: data.string.cow
			},

			]
		}
		]
	},
	{
	//slide 3
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques4,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/butterfly_diy.png",
				labeltext: data.string.but
			},
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/monkey.png",
				labeltext: data.string.mon
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/mosquito.png",
				labeltext: data.string.mos
			},

			]
		}
		]
	},
	{
	//slide 4
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques5,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/hen.png",
				labeltext: data.string.hen
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/rat.png",
				labeltext: data.string.rat
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/cat.png",
				labeltext: data.string.cat
			},

			]
		}
		]
	},
	{
	//slide 5
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques6,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/vulture.png",
				labeltext: data.string.vul
			},
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/cat.png",
				labeltext: data.string.cat
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/bee.png",
				labeltext: data.string.bee
			},

			]
		}
		]
	},
	{
	//slide 6
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques7,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/hen.png",
				labeltext: data.string.hen
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/rat.png",
				labeltext: data.string.rat
			},
			{
				forshuffle: "options_sign correctone",
				imgsrc: imgpath + "animals/dog.png",
				labeltext: data.string.dog
			},

			]
		}
		]
	},
	{
	//slide 7
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques8,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/cater.png",
				labeltext: data.string.cater
			},
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/tigerlick.png",
				labeltext: data.string.tig
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/elephant.png",
				labeltext: data.string.ele
			},

			]
		}
		]
	},
	{
	//slide 8
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques9,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/vulture.png",
				labeltext: data.string.vul
			},
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/cater.png",
				labeltext: data.string.cater
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "animals/bee.png",
				labeltext: data.string.bee
			},

			]
		}
		]
	},
	{
	//slide 9
		exerciseblock: [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letHigh",
			textdata: data.string.ques10,

			imageoptions: [
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/mosquito.png",
				labeltext: data.string.mos
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "animals/elephant.png",
				labeltext: data.string.ele
			},
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "animals/vulture.png",
				labeltext: data.string.vul
			},

			]
		}
		]
	}
	];

/*remove this for non random questions*/
content.shufflearray();

$(function ()
{
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	var $total_page = 10;
	var score = 0;
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/
	var rhino = new RhinoTemplate();

	rhino.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		//$('.congratulation').hide(0);
		//$('.exefin').hide(0);

		/*generate question no at the beginning of question*/
		rhino.numberOfQuestions();

		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		switch(countNext) {
			default:
				var wrong_clicked = 0;
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;

				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					$('.optionscontainer').removeClass('forHover');
					$(current_btn).addClass('option_true');
					$nextBtn.show(0);
					play_correct_incorrect_sound(true);
					if(!wrong_clicked){
						rhino.update(true);
					}
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					play_correct_incorrect_sound(false);
					if(!wrong_clicked){
						rhino.update(false);
					}
					wrong_clicked++;
					if(wrong_clicked==1){
						show_hint_1();
					} else if(wrong_clicked==2){
						show_hint_2();
					}
				}
				function show_hint_1(){
					$('.hint_div').show(0);
					$('.hint_top>.hint_a>.fraction2>.top').html(frac_1_n/rand_multiplier_1);
					$('.hint_top>.hint_a>.fraction2>.bottom').html(frac_1_d/rand_multiplier_1);
					$('.hint_bottom>.hint_b>.fraction2>.top').html(frac_2_n/rand_multiplier_2);
					$('.hint_bottom>.hint_b>.fraction2>.bottom').html(frac_2_d/rand_multiplier_2);
				}
				function show_hint_2(){
					var lcm = lcm_two_numbers(frac_1_d,frac_2_d);
					var top_1= frac_1_n*lcm/frac_1_d;
					var top_2 = frac_1_n*lcm/frac_1_d;
					$('.hint_top>.hint_a>.fraction2>.top').html(top_1);
					$('.hint_top>.hint_a>.fraction2>.bottom').html(lcm);
					$('.hint_bottom>.hint_b>.fraction2>.top').html(top_2);
					$('.hint_bottom>.hint_b>.fraction2>.bottom').html(lcm);
				}
				break;
		}

		$('.correctOne').click(function(){
			correct_btn(this);
		});

		$('.incorrectOne, .incorrectTwo').click(function(){
			incorrect_btn(this);
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
			templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

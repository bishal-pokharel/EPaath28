var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var flashtohtmlpath = $ref+"/flash_to_html/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
 {
  	//start
  	contentblockadditionalclass: "simplebg2",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p2_s1
  	}]
  },{
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "additionaluppertextblock",
  	uppertextblock:[{
  		textclass: "cowspritecontainer"
  	}],
  	/*
	  imageblockadditionalclass: "additional_image_block02",
			imageblock: [{
				imagestoshow: [{
					imgclass: "cow",
					imgsrc: imgpath+"bg.png"
				}]
			}],*/

  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s2
  	}]
  },{
  	//page 2
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowanimate",
  			imgsrc: imgpath+"cow-eating-grass.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headeresquelabel",
  			imagelabeldata: data.string.p2_s3
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s2
  	}],
  	examplecontainer: [{
  		containingchildclass: "child1 animalanimate",
  		imgclass: "elephant animalanimate",
		imgsrc: imgpath+"elephant.png",
  		imagelabelclass: "elephantlabel animalanimate",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s4
  	}]
  },{
  	//page 3
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"cow-eating-grass.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s3
  		}]
  	}],
  	examplecontainer: [{
  		containingchildclass: "child1",
  		imgclass: "elephant",
  		imgsrc: imgpath+"elephant.png",
  		imagelabelclass: "elephantlabel",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s4
  	},{
  		containingchildclass: "child2 animalanimate_2",
  		imgclass: "monkey2 animalanimate_2",
		imgsrc: imgpath+"monkey.png",
		imagelabelclass: "monkeylabel animalanimate_2",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s5
  	}]

  },{
  	//page 4
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"cow-eating-grass.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s3
  		}]
  	}],
  		examplecontainer: [{
  		containingchildclass: "child1",
  		imgclass: "elephant",
  		imgsrc: imgpath+"elephant.png",
  		imagelabelclass: "elephantlabel",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s4
  	},{
  		containingchildclass: "child2",
  		imgclass: "monkey2",
		imgsrc: imgpath+"monkey.png",
		imagelabelclass: "monkeylabel",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s5
  	},{
  		containingchildclass: "child3 animalanimate_2",
  		imgclass: "tiger animalanimate_2",
		imgsrc: imgpath+"tiger.png",
  		imagelabelclass: "tigerlabel animalanimate_2",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s6
  	}]
  },{
  	//page 5
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
    imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow: [{
  			imgclass: "newcat",
  			imgsrc: imgpath+"cat.png"
  		}]
    }],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s7
  	}]
  },{
  	//page 6
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowanimate",
  			imgsrc: imgpath+"cat.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headeresquelabel",
  			imagelabeldata: data.string.p2_s8
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s7
  	}],
  	examplecontainer: [{
  		containingchildclass: "child1 animalanimate",
		imgclass: "elephant animalanimate",
		imgsrc: imgpath+"dog_diy.png",
  		imagelabelclass: "elephantlabel animalanimate",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s9
  	}]
  },{
  	//page 7
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"cat.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s8
  		}]
  	}],
  		examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"dog_diy.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s9
  		},{
	  		containingchildclass: "child2 animalanimate_2",
			imgclass: "monkey animalanimate_2",
  			imgsrc: imgpath+"tiger_lick_water.png",
	  		imagelabelclass: "monkeylabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s10
  		}]
  },{
  	//page 8
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"cat.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s8
  		}]
  	}],
  	examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"dog_diy.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s9
  		},{
	  		containingchildclass: "child2",
			imgclass: "monkey",
  			imgsrc: imgpath+"tiger_lick_water.png",
	  		imagelabelclass: "monkeylabel",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s10
  		},{
	  		containingchildclass: "child3 animalanimate_2",
			imgclass: "tiger animalanimate_2",
  			imgsrc: imgpath+"horse_lick_salt.png",
	  		imagelabelclass: "tigerlabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s11
  		}]
  },{
  	//page 9
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "additionaluppertextblock",
  	uppertextblock:[{
  		textclass: "frogspritecontainer"
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s12
  	}]
  },{
  	//page 10
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowanimate",
  			imgsrc: imgpath+"frog.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headeresquelabel",
  			imagelabeldata: data.string.p2_s13
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s12
  	}],
  	examplecontainer: [{
  		containingchildclass: "child1 animalanimate",
		imgclass: "elephant animalanimate",
		imgsrc: imgpath+"snake.png",
  		imagelabelclass: "elephantlabel animalanimate",
		datahighlightflag: true,
		datahighlightcustomclass: "pinkhighlightcolor",
		imagelabeldata: data.string.p2_s14
  	}]
  },{
  	//page 11
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"frog.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s13
  		}]
  	}],
  	examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"snake.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s14
  		},{
	  		containingchildclass: "child2 animalanimate_2",
			imgclass: "monkey animalanimate_2",
  			imgsrc: imgpath+"croc.png",
	  		imagelabelclass: "monkeylabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s15
  	}]
  },{
  	//page 12
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "cowfixed",
  			imgsrc: imgpath+"frog.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s13
  		}]
  	}],
	examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"snake.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s14
  		},{
	  		containingchildclass: "child2",
			imgclass: "monkey",
  			imgsrc: imgpath+"croc.png",
	  		imagelabelclass: "monkeylabel",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s15
  		},{
	  		containingchildclass: "child3 animalanimate_2",
			imgclass: "monkey animalanimate_2",
  			imgsrc: imgpath+"hen.png",
	  		imagelabelclass: "monkeylabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s16
  	}]
  },{
  	//page 13
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "additionaluppertextblock",
  	uppertextblock:[{
  		textclass: "beespritecontainer"
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s17
  	}]
  },{
  	//page 14
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "beeanimate",
  			imgsrc: imgpath+"bee01.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headeresquelabel",
  			imagelabeldata: data.string.p2_s18
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "description1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pinkhighlightcolor",
  		textdata: data.string.p2_s17
  	}],
  	examplecontainer: [{
	  		containingchildclass: "child1 animalanimate",
			imgclass: "elephant animalanimate",
  			imgsrc: imgpath+"musquito_diy.png",
	  		imagelabelclass: "elephantlabel animalanimate",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s19
  		}]
  },{
  	//page 15
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "beefixed",
  			imgsrc: imgpath+"bee01.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s18
  		}]
  	}],
  	examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"musquito_diy.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s19
  		},{
	  		containingchildclass: "child2 animalanimate_2",
			imgclass: "monkey animalanimate_2",
  			imgsrc: imgpath+"leach_diy.png",
	  		imagelabelclass: "monkeylabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s20
  		}]
  },{
  	//page 16
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		// svgcontent:[
  			// {
  				// svgcontainerclass: "cowanimate",
  				// flashtohtmlflag: true,
  				// htmlfilename: flashtohtmlpath+"cow_new"
  			// }
  		// ],

  		imagestoshow: [{
  			imgclass: "beefixed",
  			imgsrc: imgpath+"bee01.png"
  		}],
  		imagelabels: [{
  			imagelabelclass: "headerlabel",
  			imagelabeldata: data.string.p2_s18
  		}]
  	}],
  	examplecontainer: [{
	  		containingchildclass: "child1",
			imgclass: "elephant",
  			imgsrc: imgpath+"musquito_diy.png",
	  		imagelabelclass: "elephantlabel",
			datahighlightflag: true,
			datahighlightcustomclass: "pinkhighlightcolor",
			imagelabeldata: data.string.p2_s19
  		},{
	  		containingchildclass: "child2",
			imgclass: "monkey",
  			imgsrc: imgpath+"leach_diy.png",
	  		imagelabelclass: "monkeylabel",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s20
  		},{
	  		containingchildclass: "child3 animalanimate_2",
			imgclass: "monkey animalanimate_2",
  			imgsrc: imgpath+"butterfly_diy.png",
	  		imagelabelclass: "monkeylabel animalanimate_2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "pinkhighlightcolor",
  			imagelabeldata: data.string.p2_s21
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p2_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p2_s15.ogg"},
			{id: "sound_16", src: soundAsset+"p2_s16.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

   /*
       containts flash to html converted code --- start----
   */
	 // function checkforflashtohtml($highlightinside){
        // //check if $highlightinside is provided
        // typeof $highlightinside !== "object" ?
        // alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        // null ;
//
        // var $alltextpara = $highlightinside.find("*[data-flashtohtml='true']");
        // if($alltextpara.length > 0){
          // $.each($alltextpara, function(index, val) {
            // /*if there is a data-highlightcustomclass attribute defined for the text element
            // use that or else use default 'parsedstring'*/
           // if( $(this).attr("data-htmlfilename")){ /*if there is data-highlightcustomclass defined it is true else it is not*/
	            // var stylerulename = $(this).attr("data-htmlfilename");
	            // $(this).load( stylerulename+".html", function() {
					// console.log( "The html page is loaded : ", stylerulename+".html");
				// });
          // }
          // });
        // }
      // }

	/*
	       containts flash to html converted code ---end----
	*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    var div_ratio =$(".cowspritecontainer").width()/$(".cowspritecontainer").parent().width();
    $(".cowspritecontainer").width(Math.round($(".cowspritecontainer").parent().width()*div_ratio));

    // var div_ratio =$(".catspritecontainer").width()/$(".catspritecontainer").parent().width();
    // $(".catspritecontainer").width(Math.round($(".catspritecontainer").parent().width()*div_ratio));

    var div_ratio =$(".frogspritecontainer").width()/$(".frogspritecontainer").parent().width();
    $(".frogspritecontainer").width(Math.round($(".frogspritecontainer").parent().width()*div_ratio));

    var div_ratio =$(".beespritecontainer").width()/$(".beespritecontainer").parent().width();
    $(".beespritecontainer").width(Math.round($(".beespritecontainer").parent().width()*div_ratio));

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch(countNext){
    	case 0:
    		sound_player('sound_0');
    		break;
    	case 2:
    	case 6:
    	case 10:
    	case 14:
    		setTimeout(function(){sound_player('sound_'+countNext)},3800);
    		$(".description1").delay(1000).hide(0);
    		// checkforflashtohtml($board);
    		break;
    	case 1:
    	case 5:
    	case 9:
    	case 13:
    		sound_player('sound_'+countNext);
    		break;
    	case 4:
    	case 3:
    	case 7:
    	case 8:
    	case 11:
    	case 12:
    	case 14:
    	case 15:
    		setTimeout(function(){sound_player('sound_'+countNext)},1500);
    		break;
		case 16:
			setTimeout(function(){sound_player('sound_'+countNext)},1500);
			break;


    	default:
    	break;
    }
  }

/*=====  End of Templates Block  ======*/
function sound_player_d(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  setTimeout(function(){current_sound.play()},4100);
}

function nav_button_controls(delay_ms){
  timeoutvar = setTimeout(function(){
    if(countNext==0){
      $nextBtn.show(0);
    } else if( countNext>0 && countNext == $total_page-1){
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else{
      $prevBtn.show(0);
      $nextBtn.show(0);
    }
  },delay_ms);
}
function sound_player(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on("complete", function(){
        nav_button_controls(0);
    });
}

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
   			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  /* navigation buttons event handlers */
	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

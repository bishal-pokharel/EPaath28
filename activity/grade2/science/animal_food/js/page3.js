var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
  {
  	//start
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p3_s1
  	}],
   	imageblockadditionalclass: "diycoverpage",
  	imageblock: [{
		imagestoshow: [{
      imgclass: "diycover",
      imgsrc: imgpath+ "diy_cover.png"
    }]
  }]
  },{
  	//page 1
  		contentblockadditionalclass: "simplebg",
  		contentnocenteradjust: true,
	  	imageblockadditionalclass: "upperimageblockholder",
	  	imageblock: [{
	  		imagestoshow: [{
	  			//1
	  			imgclass: "draggable chew draggableposition1",
	  			imgsrc: imgpath+ "mouse.png"
	  		},{
	  			//2
	  			imgclass: "draggable suck draggableposition2",
	  			imgsrc: imgpath+ "leach_diy.png"
	  		},{
	  			//3
	  			imgclass: "draggable chew draggableposition3",
	  			imgsrc: imgpath+ "cow.png"
	  		},{
	  			//4
	  			imgclass: "draggable swallow draggableposition4",
	  			imgsrc: imgpath+ "snake.png"
	  		},{
	  			//2_1
	  			imgclass: "draggable lick hide",
	  			imgsrc: imgpath+ "cat.png"
	  		},{
	  			//2_2
	  			imgclass: "draggable lick hide",
	  			imgsrc: imgpath+ "dog_diy.png"
	  		},{
	  			//2_3
	  			imgclass: "draggable swallow hide",
	  			imgsrc: imgpath+ "hen_diy.png"
	  		},{
	  			//2_4
	  			imgclass: "draggable chew hide",
	  			imgsrc: imgpath+ "elephant_diy.png"
	  		},{
	  			//3_1
	  			imgclass: "draggable suck hide",
	  			imgsrc: imgpath+ "musquito_diy.png"
	  		},{
	  			//3_2
	  			imgclass: "draggable lick hide",
	  			imgsrc: imgpath+ "tiger_lick_water.png"
	  		},{
	  			//3_3
	  			imgclass: "draggable swallow hide",
	  			imgsrc: imgpath+ "bird_diy.png"
	  		},{
	  			//3_4
	  			imgclass: "draggable suck hide",
	  			imgsrc: imgpath+ "butterfly_diy.png"
	  		}],
	  		imagelabels: [{
	  			imagelabelclass: "draganddropinst",
	  			imagelabeldata: data.string.p3_s2
	  		}]
	  	}],
	  	specialdropdiv:[{
	  		droplabelclass: "chewlabel",
	  		droplabeldata: data.string.p3_s3,
	  		dropdiv: "chewdrop"
	  	},{
	  		droplabelclass: "licklabel",
	  		droplabeldata: data.string.p3_s4,
	  		dropdiv: "lickdrop"
	  	},{
	  		droplabelclass: "swallowlabel",
	  		droplabeldata: data.string.p3_s5,
	  		dropdiv: "swallowdrop"
	  	},{
	  		droplabelclass: "sucklabel",
	  		droplabeldata: data.string.p3_s6,
	  		dropdiv: "suckdrop"
	  	}]
  }
];


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p3_s1.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);

    vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call notifyuser
		// notifyuser($anydiv);

		switch(countNext){
			case 0:
				play_diy_audio();
				break;
			case 1:
				sound_player('sound_1');
				$(".draggable").draggable({
					containment : "body",
					cursor : "grab",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 1000,
					start: function(event, ui){
						$(ui.helper).addClass("disableanimation");
						$(ui.helper).css({"max-width": "18%",
										  "max-height": "15%"});

					},
					stop: function(event, ui){
						$(ui.helper).removeClass("disableanimation");
						$(ui.helper).css("max-width", "");
					}
					// drag: function(){
						// $(this).css("z-index", "10000");
						// console.log($(this).css("z-index"));
					// }
				});

				$('.chewdrop').droppable({
					accept : ".chew",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "chew" , "chewdrop");
					}
				});

				$('.lickdrop').droppable({
					accept : ".lick",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "lick" , "lickdrop");
					}
				});

				$('.swallowdrop').droppable({
					accept : ".swallow",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "swallow" , "swallowdrop");
					}
				});

				$('.suckdrop').droppable({
					accept : ".suck",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui, "suck" , "suckdrop");
					}

				});
				var divsdroppedcount = 0;
				function handleCardDrop(event, ui, classname, droppedon) {
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $("."+droppedon);
					var count = 0;
					var top = 0;
          play_correct_incorrect_sound(1);

						count = $("."+droppedon+"> ."+classname).length;
						top = (count > 0)? 35 + 30 * (count-1) : 5;
						// $(dropped.context.children[0]).css({
							// "width": "40%",
							// "height": "auto",
							// "max-height": "70%"
						// });
						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "20%",
							"top" : top+"%",
							"width": "60%",
							"height": "auto",
							"max-height": "70%"
						}).appendTo(droppedOn);

					// var $classtoshow;
					var $newEntry = $(".upperimageblockholder> .hide").eq(0);
					var $draggable3;
					var $draggable2;
					var $draggable1;
					if(dropped.hasClass("draggableposition4")){
						dropped.toggleClass("draggableposition4");
						$draggable3 = $(".draggableposition3");
						$draggable2 = $(".draggableposition2");
						$draggable1 = $(".draggableposition1");
						// $classtoshow = $(".draggableposition4").eq(0);

					}else if(dropped.hasClass("draggableposition3")){
						dropped.toggleClass("draggableposition3");
						$draggable2 = $(".draggableposition2");
						$draggable1 = $(".draggableposition1");
						// $classtoshow = $(".draggableposition3").eq(0);
					}else if(dropped.hasClass("draggableposition2")){
						dropped.toggleClass("draggableposition2");
						$draggable1 = $(".draggableposition1");
						// $classtoshow = $(".draggableposition2").eq(0);
					}else if(dropped.hasClass("draggableposition1")){
						dropped.toggleClass("draggableposition1");
						// $classtoshow = $(".draggableposition1").eq(0);
					}

					if($draggable3 != null){
						 $draggable3.removeClass("draggableposition3").addClass("draggableposition4");
					}
					if($draggable2 != null){
						 $draggable2.removeClass("draggableposition2").addClass("draggableposition3");
					}
					if($draggable1 != null){
						 $draggable1.removeClass("draggableposition1").addClass("draggableposition2");
					}
					if($newEntry != null){
						 $newEntry.removeClass("hide").addClass("draggableposition1");
					}


					// if($classtoshow != null){
							// $classtoshow.toggleClass("hide");
					// }

					divsdroppedcount++;
					if(divsdroppedcount == 12){
						ole.footerNotificationHandler.lessonEndSetNotification();
					}
				}
				break;
			default:
				break;
		}

	}

	/*=====  End of Templates Block  ======*/
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		switch(countNext) {
		case 1:
			$nextBtn.hide(0);
			$(".container2, .container3").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant1").toggleClass("plant1animate");
			// $(".plant2").toggleClass("plant2animate");
			// $(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 2:
			$nextBtn.hide(0);
			// $(".container2, .container3").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant1").toggleClass("plant1animate");
			$(".plant2").toggleClass("plant2animate");
			$(".plant3").toggleClass("plant3animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 4:
			$nextBtn.hide(0);
			$(".container3, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant2_1").toggleClass("plant2_1animate");
			// $(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 5:
			$nextBtn.hide(0);
			// $(".container3, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant2_1").toggleClass("plant2_1animate");
			$(".plant2_2").toggleClass("plant2_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		case 7:
			$nextBtn.hide(0);
			$(".container2, .container1").hide(0);
			// $(".specialimagediv").toggleClass("classfadeout");
			// $(".plant3_1").toggleClass("plant3_1animate");
			// $(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 500);
			break;
		case 8:
			$nextBtn.hide(0);
			// $(".container2, .container1").hide(0);
			$(".specialimagediv").toggleClass("classfadeout");
			$(".plant3_1").toggleClass("plant3_1animate");
			$(".plant3_2").toggleClass("plant3_2animate");
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 3180);
			break;
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

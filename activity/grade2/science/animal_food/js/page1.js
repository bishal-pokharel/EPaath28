var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
 {
  	//start
  	contentblockadditionalclass: "firstbackground",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p6_heading
  	},
    {
      textclass: "frogspritecontainer"
    }],
  },
   {
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "title",
  		textdata: data.string.p1_s1
  	}],
  	imageblockadditionalclass: "additional_image_block",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animal",
  			imgid: "egg",
  			imgsrc: imgpath+"egg.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "bread",
  			imgsrc: imgpath+"roti.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "curd",
  			imgsrc: imgpath+"curd.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "lentil",
  			imgsrc: imgpath+"dal.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "fish",
  			imgsrc: imgpath+"fish.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "vegetables",
  			imgsrc: imgpath+"vegetables.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "millet",
  			imgsrc: imgpath+"millet.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "maize",
  			imgsrc: imgpath+"maize.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "milk",
  			imgsrc: imgpath+"milk.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "beaten_rice",
  			imgsrc: imgpath+"beaten_rice.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "meat",
  			imgsrc: imgpath+"meat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "egglabel",
  			imagelabeldata: data.string.p1_s2
  		},{
  			imagelabelclass: "breadlabel",
  			imagelabeldata: data.string.p1_s3
  		},{
  			imagelabelclass: "curdlabel",
  			imagelabeldata: data.string.p1_s4
  		},{
  			imagelabelclass: "lentillabel",
  			imagelabeldata: data.string.p1_s5
  		},{
  			imagelabelclass: "fishlabel",
  			imagelabeldata: data.string.p1_s6
  		},{
  			imagelabelclass: "vegetableslabel",
  			imagelabeldata: data.string.p1_s7
  		},{
  			imagelabelclass: "milletlabel",
  			imagelabeldata: data.string.p1_s8
  		},{
  			imagelabelclass: "maizelabel",
  			imagelabeldata: data.string.p1_s9
  		},{
  			imagelabelclass: "milklabel",
  			imagelabeldata: data.string.p1_s10
  		},{
  			imagelabelclass: "beaten_ricelabel",
  			imagelabeldata: data.string.p1_s11
  		},{
  			imagelabelclass: "meatlabel",
  			imagelabeldata: data.string.p1_s12
  		}]
  	}],
  },{
  	//page 2
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,
  	imageblockadditionalclass: "additional_image_block03",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "animal",
  			imgid: "egg",
  			imgsrc: imgpath+"egg.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "bread",
  			imgsrc: imgpath+"roti.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "curd",
  			imgsrc: imgpath+"curd.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "lentil",
  			imgsrc: imgpath+"dal.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "fish",
  			imgsrc: imgpath+"fish.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "vegetables",
  			imgsrc: imgpath+"vegetables.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "millet",
  			imgsrc: imgpath+"millet.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "maize",
  			imgsrc: imgpath+"maize.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "milk",
  			imgsrc: imgpath+"milk.jpg"
  		},{
  			imgclass: "plant",
  			imgid: "beaten_rice",
  			imgsrc: imgpath+"beaten_rice.jpg"
  		},{
  			imgclass: "animal",
  			imgid: "meat",
  			imgsrc: imgpath+"meat.jpg"
  		}],
  		imagelabels:[{
  			imagelabelclass: "egglabel",
  			imagelabeldata: data.string.p1_s2
  		},{
  			imagelabelclass: "breadlabel",
  			imagelabeldata: data.string.p1_s3
  		},{
  			imagelabelclass: "curdlabel",
  			imagelabeldata: data.string.p1_s4
  		},{
  			imagelabelclass: "lentillabel",
  			imagelabeldata: data.string.p1_s5
  		},{
  			imagelabelclass: "fishlabel",
  			imagelabeldata: data.string.p1_s6
  		},{
  			imagelabelclass: "vegetableslabel",
  			imagelabeldata: data.string.p1_s7
  		},{
  			imagelabelclass: "milletlabel",
  			imagelabeldata: data.string.p1_s8
  		},{
  			imagelabelclass: "maizelabel",
  			imagelabeldata: data.string.p1_s9
  		},{
  			imagelabelclass: "milklabel",
  			imagelabeldata: data.string.p1_s10
  		},{
  			imagelabelclass: "beaten_ricelabel",
  			imagelabeldata: data.string.p1_s11
  		},{
  			imagelabelclass: "meatlabel",
  			imagelabeldata: data.string.p1_s12
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock",
  	lowertextblock: [{
  		textclass: "title1",
  		textdata: data.string.p1_s14
  	},{
  		textclass: "title2",
  		textdata: data.string.p1_s15
  	}]
  },{
  	//page 5
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "boy",
  			imgsrc: imgpath+"boy-eating.png"
  		},{
  			imgclass: "dog",
  			imgsrc: imgpath+"dog.png"
  		},{
  			imgclass: "cat",
  			imgsrc: imgpath+"cat.png"
  		},,{
  			imgclass: "chicken",
  			imgsrc: imgpath+"hen.png"
  		},{
  			imgclass: "rat wid",
  			imgsrc: imgpath+"mouse.png"
  		},,{
  			imgclass: "cow",
  			imgsrc: imgpath+"cow.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
	lowertextblock:[{
		textclass : "title3_a",
		textdata : data.string.p1_s16_a
	},{
		textclass : "title3",
		textdata : data.string.p1_s16
	}, {
		textclass : "title4",
		textdata : data.string.p1_s17
	}, {

	}, {
		textclass : "title5",
		textdata : data.string.p1_s18
	}, {
		textclass : "title6",
		textdata : data.string.p1_s19
	}, {
		textclass : "title7",
		textdata : data.string.p1_s20
	}]
  },{
  	//page 6
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "omnivore ratfadein",
  			imgid: "rat",
  			imgsrc: imgpath+"mouse.png"
  		},{
  			imgclass: "carnivore eaglefadein",
  			imgid: "eagle",
  			imgsrc: imgpath+"eagle.png"
  		},{
  			imgclass: "herbivore elephantfadein",
  			imgid: "elephant",
  			imgsrc: imgpath+"elephant.png"
  		},{
  			imgclass: "carnivore snakefadein",
  			imgid: "snake",
  			imgsrc: imgpath+"snake.png"
  		},{
  			imgclass: "omnivore chickenfadein",
  			imgid: "chicken",
  			imgsrc: imgpath+"hen.png"
  		},{
  			imgclass: "omnivore dogfadein",
  			imgid: "dog",
  			imgsrc: imgpath+"beer.png"
  		},{
  			imgclass: "herbivore cowfadein",
  			imgid: "cow",
  			imgsrc: imgpath+"cow.png"
  		},{
  			imgclass: "omnivore monkeyfadein",
  			imgid: "monkey",
  			imgsrc: imgpath+"monkey.png"
  		},{
  			imgclass: "carnivore catfadein",
  			imgid: "cat",
  			imgsrc: imgpath+"tiger.png"
  		},{
  			imgclass: "omnivore frogfadein",
  			imgid: "frog",
  			imgsrc: imgpath+"frog.png"
  		},{
  			imgclass: "carnivore crocodilefadein",
  			imgid: "crocodile",
  			imgsrc: imgpath+"croc.png"
  		},{
  			imgclass: "herbivore caterpillarfadein",
  			imgid: "caterpillar",
  			imgsrc: imgpath+"worm.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "title9_1",
  		textdata: data.string.p1_s21
  	},{
  		textclass: "title9_2",
  		textdata: data.string.p1_s22
  	}]
  },{
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "omnivore ratfadein",
  			imgid: "rat",
  			imgsrc: imgpath+"mouse.png"
  		},{
  			imgclass: "carnivore eaglefadein",
  			imgid: "eagle",
  			imgsrc: imgpath+"eagle.png"
  		},{
  			imgclass: "herbivore elephantfadein",
  			imgid: "elephant",
  			imgsrc: imgpath+"elephant.png"
  		},{
  			imgclass: "carnivore snakefadein",
  			imgid: "snake",
  			imgsrc: imgpath+"snake.png"
  		},{
  			imgclass: "omnivore chickenfadein",
  			imgid: "chicken",
  			imgsrc: imgpath+"hen.png"
  		},{
  			imgclass: "omnivore dogfadein",
  			imgid: "dog",
  			imgsrc: imgpath+"beer.png"
  		},{
  			imgclass: "herbivore cowfadein",
  			imgid: "cow",
  			imgsrc: imgpath+"cow.png"
  		},{
  			imgclass: "omnivore monkeyfadein",
  			imgid: "monkey",
  			imgsrc: imgpath+"monkey.png"
  		},{
  			imgclass: "carnivore catfadein",
  			imgid: "cat",
  			imgsrc: imgpath+"tiger.png"
  		},{
  			imgclass: "omnivore frogfadein",
  			imgid: "frog",
  			imgsrc: imgpath+"frog.png"
  		},{
  			imgclass: "carnivore crocodilefadein",
  			imgid: "crocodile",
  			imgsrc: imgpath+"croc.png"
  		},{
  			imgclass: "herbivore caterpillarfadein",
  			imgid: "caterpillar",
  			imgsrc: imgpath+"worm.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "title8",
  		textdata: data.string.p1_s23
  	}]
  },{
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "omnivore ratfadein",
  			imgid: "rat",
  			imgsrc: imgpath+"mouse.png"
  		},{
  			imgclass: "carnivore eaglefadein",
  			imgid: "eagle",
  			imgsrc: imgpath+"eagle.png"
  		},{
  			imgclass: "herbivore elephantfadein",
  			imgid: "elephant",
  			imgsrc: imgpath+"elephant.png"
  		},{
  			imgclass: "carnivore snakefadein",
  			imgid: "snake",
  			imgsrc: imgpath+"snake.png"
  		},{
  			imgclass: "omnivore chickenfadein",
  			imgid: "chicken",
  			imgsrc: imgpath+"hen.png"
  		},{
  			imgclass: "omnivore dogfadein",
  			imgid: "dog",
  			imgsrc: imgpath+"beer.png"
  		},{
  			imgclass: "herbivore cowfadein",
  			imgid: "cow",
  			imgsrc: imgpath+"cow.png"
  		},{
  			imgclass: "omnivore monkeyfadein",
  			imgid: "monkey",
  			imgsrc: imgpath+"monkey.png"
  		},{
  			imgclass: "carnivore catfadein",
  			imgid: "cat",
  			imgsrc: imgpath+"tiger.png"
  		},{
  			imgclass: "omnivore frogfadein",
  			imgid: "frog",
  			imgsrc: imgpath+"frog.png"
  		},{
  			imgclass: "carnivore crocodilefadein",
  			imgid: "crocodile",
  			imgsrc: imgpath+"croc.png"
  		},{
  			imgclass: "herbivore caterpillarfadein",
  			imgid: "caterpillar",
  			imgsrc: imgpath+"worm.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "title8",
  		textdata: data.string.p1_s24
  	}]
  },{
  	contentblockadditionalclass: "simplebg",
  	contentnocenteradjust: true,

  	imageblockadditionalclass: "additional_image_block02",
  	imageblock: [{
  		imagestoshow:[{
  			imgclass: "omnivore ratfadein",
  			imgid: "rat",
  			imgsrc: imgpath+"mouse.png"
  		},{
  			imgclass: "carnivore eaglefadein",
  			imgid: "eagle",
  			imgsrc: imgpath+"eagle.png"
  		},{
  			imgclass: "herbivore elephantfadein",
  			imgid: "elephant",
  			imgsrc: imgpath+"elephant.png"
  		},{
  			imgclass: "carnivore snakefadein",
  			imgid: "snake",
  			imgsrc: imgpath+"snake.png"
  		},{
  			imgclass: "omnivore chickenfadein",
  			imgid: "chicken",
  			imgsrc: imgpath+"hen.png"
  		},{
  			imgclass: "omnivore dogfadein",
  			imgid: "dog",
  			imgsrc: imgpath+"beer.png"
  		},{
  			imgclass: "herbivore cowfadein",
  			imgid: "cow",
  			imgsrc: imgpath+"cow.png"
  		},{
  			imgclass: "omnivore monkeyfadein",
  			imgid: "monkey",
  			imgsrc: imgpath+"monkey.png"
  		},{
  			imgclass: "carnivore catfadein",
  			imgid: "cat",
  			imgsrc: imgpath+"tiger.png"
  		},{
  			imgclass: "omnivore frogfadein",
  			imgid: "frog",
  			imgsrc: imgpath+"frog.png"
  		},{
  			imgclass: "carnivore crocodilefadein",
  			imgid: "crocodile",
  			imgsrc: imgpath+"croc.png"
  		},{
  			imgclass: "herbivore caterpillarfadein",
  			imgid: "caterpillar",
  			imgsrc: imgpath+"worm.png"
  		}]
  	}],
  	lowertextblockadditionalclass: "additionallowertextblock02",
  	lowertextblock: [{
  		textclass: "title8",
  		textdata: data.string.p1_s25
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

  var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "mrs_sharma", src: imgpath+"mrs_sharma.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "temple_bg", src: imgpath+"temple_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "swimming_bg", src: imgpath+"swimming_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "restaurant", src: imgpath+"restaurant.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "elephant_bg", src: imgpath+"elephant_bg.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "school_bg", src: imgpath+"school_bg.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			//textboxes
			// {id: "dialog1_img", src: imgpath+'bubble-10.png', type: createjs.AbstractLoader.IMAGE},
			// {id: "bubblesc", src: imgpath+'bubblesc.png', type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// current_sound = createjs.Sound.play('sound_0');
		// call main function
		templateCaller();
	}
	//initialize
	init();
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  	var animalarray =[];
   	var plantarray = [];
   	var timeoutcontroller;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    var div_ratio =$(".frogspritecontainer").width()/$(".frogspritecontainer").parent().width();
    // $(".frogspritecontainer").width(Math.round($(".frogspritecontainer").parent().width()*div_ratio));
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch(countNext){
    	case 0:
    		sound_player("sound_0");
    		break;
    	case 1:
    		var clickcount = 0;
    		animalarray = [];
   			plantarray = [];
    		$(".plant, .animal").click(function(){

    			var $this = $(this);
    			$this.toggleClass("clicked");
	    		if($this.hasClass("clicked")){
	    			clickcount++;
	    			if($this.hasClass("animal")){
	    				animalarray.push($this.attr('id'));
	    			}else{
	    				plantarray.push($this.attr('id'));
	    			}
    			}else{
    				var index;
					clickcount--;
					if($this.hasClass("animal")){
						index = animalarray.indexOf($this.attr('id'));
	    				// animalarray.push($this.attr('id'));
	    				if (index > -1) {
					     	animalarray.splice(index, 1);
						}
	    			}else{
	    				index = plantarray.indexOf($this.attr('id'));
	    				// plantarray.push($this.attr('id'));
	    				if (index > -1) {
					     	plantarray.splice(index, 1);
						}
	    			}
    			}

    			if(clickcount >= 6){
    				nav_button_controls(0);
    			}else{
    				$nextBtn.hide(0);
    			}
    		});
    		attachlabeltoimage("egglabel", "egg");
    		attachlabeltoimage("breadlabel", "bread");
    		attachlabeltoimage("curdlabel", "curd");
    		attachlabeltoimage("lentillabel", "lentil");
    		attachlabeltoimage("fishlabel", "fish");
    		attachlabeltoimage("vegetableslabel", "vegetables");
    		attachlabeltoimage("milletlabel", "millet");
    		attachlabeltoimage("maizelabel", "maize");
    		attachlabeltoimage("milklabel", "milk");
    		attachlabeltoimage("beaten_ricelabel", "beaten_rice");
    		attachlabeltoimage("meatlabel", "meat");

    		function attachlabeltoimage(label, imageid){
    			$("."+label).click(function(){
    				var $this = $("#"+imageid);
	    			$this.toggleClass("clicked");
		    		if($this.hasClass("clicked")){
		    			clickcount++;
		    			if($this.hasClass("animal")){
		    				animalarray.push($this.attr('id'));
		    			}else{
		    				plantarray.push($this.attr('id'));
		    			}
	    			}else{
	    				var index;
						clickcount--;
						if($this.hasClass("animal")){
							index = animalarray.indexOf($this.attr('id'));
		    				// animalarray.push($this.attr('id'));
		    				if (index > -1) {
						     	animalarray.splice(index, 1);
							}
		    			}else{
		    				index = plantarray.indexOf($this.attr('id'));
		    				// plantarray.push($this.attr('id'));
		    				if (index > -1) {
						     	plantarray.splice(index, 1);
							}
		    			}
	    			}

	    			if(clickcount >= 6){
	    				$nextBtn.show(0);
	    			}else{
	    				$nextBtn.hide(0);
	    			}
	    			});
	    		}
    		sound_player('sound_1');
    		break;
    	case 2:
            $(".imageblock").addClass("avoid-clicks");
            $nextBtn.hide(0);
    		$nextBtn.delay(1000).show(0);
    		var $animals = $(".animal");
    		var $plant = $(".plant");
    		$nextBtn.hide(0);

    		$plant.hide(0);
    		$animals.hide(0);
    		var $anim;
    		$(".additional_image_block03>img").css({"padding-bottom": "0", "border":"0px solid #ffffff"});
    		$(".imageblock > label").toggleClass("fadelabel");
    		for(var i = 0; i <= animalarray.length; i++){
    			$anim = $("#"+animalarray[i]);
    			$anim.show(0);
    			$anim.addClass("animateanimalfood0"+(i+1));
    		}

    		for(var j = 0; j <= plantarray.length; j++){
    			$anim = $("#"+plantarray[j]);
    			$anim.show(0);
    			$anim.addClass("animateplantfood0"+(j+1));
    		}
        setTimeout(function(){
          sound_player('sound_2');
        },3000);
    		break;
    	case 3:
    		sound_player('sound_3');
    		break;

    	case 4:
    		timeoutcontroller = setTimeout(function(){
	    		sound_player('sound_4');
    		}, 13000);
    		break;
    	case 5:
    		$(".carnivore, .omnivore").addClass("slightlyhidden");
    		$(".herbivore").addClass("slightlymagnify");
    		sound_player('sound_5');
    		break;
    	case 6:
    		$(".herbivore, .omnivore").addClass("slightlyhidden");
    		$(".carnivore").addClass("slightlymagnify");
    		sound_player('sound_6');
    		break;
    	case 7:
    		$(".carnivore, .herbivore").addClass("slightlyhidden");
    		$(".omnivore").addClass("slightlymagnify");
    		sound_player('sound_7');
    		break;
    	default:
    		break;
    }
  }

/*=====  End of Templates Block  ======*/
function nav_button_controls(delay_ms){
  timeoutvar = setTimeout(function(){
    if(countNext==0){
      $nextBtn.show(0);
    } else if( countNext>0 && countNext == $total_page-1){
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else{
      $prevBtn.show(0);
      $nextBtn.show(0);
    }
  },delay_ms);
}
function sound_player(sound_id){
  createjs.Sound.stop();
  current_sound = createjs.Sound.play(sound_id);
  current_sound.play();
  current_sound.on("complete", function(){
    if(countNext==1){
      $nextBtn.hide(0);
      $prevBtn.hide(0);
    }
    else{
        nav_button_controls(0);
    }
    });
}

	// function sound_player(sound_id){
	// 	createjs.Sound.stop();
	// 	current_sound = createjs.Sound.play(sound_id);
	// 	current_sound.play();
	// }
/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		clearTimeout(timeoutcontroller);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

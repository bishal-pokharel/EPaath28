var imgpath = $ref+"/images/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"jungle",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img drptiger',
			imgid:"tiger",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring1,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'tiger',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.tiger
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'cat',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cat
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'cow',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cow
					}]
				}]
			}]
	},
	//slide 1
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"jungle",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img drpelephant',
			imgid:"elephant",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring1,
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'elephant',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.elephant
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'sittingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				},{
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'cow',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cow
					}]
				}]
			}]
	},
	//slide2
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"hive",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropbee',
			imgid:"bee",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring2,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'tiger',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.tiger
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				}]
			}]
	},
	//slide 3
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"pond",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropfish',
			imgid:"fishpng",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring3,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'cat',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cat
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'fishpng',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.fish
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				}]
			}]
	},
	//slide 4
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"house",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img drophuman',
			imgid:"humans",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring4,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'humans',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.human
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				}]
			}]
	},
	//slide 5
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"cowshed",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropcow',
			imgid:"cow",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring5,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'tiger',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.tiger
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'cow',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cow
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				}]
			}]
	},
	//slide 6
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"nest",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropbird',
			imgid:"flyingbird",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring6,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'elephant',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.elephant
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'spider',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.spider
					}]
				}]
			}]
	},
	//slide 7
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"spidernet",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropspider',
			imgid:"spider",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring7,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'spider',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.spider
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				}]
			}]
	},
	//slide 8
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg',
			imgid:"house",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropcat',
			imgid:"cat",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring4,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'flyingbird',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bird
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'snake',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.snake
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'cat',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.cat
					}]
				}]
			}]
	},
	//slide 9
	{
		contentblockadditionalclass:'ole-background-gradient-weepysky',
		optionsblock:[{
			optionsdivclass:'imgdiv',
			imgclass:'optionimg newcss',
			imgid:"bgmouse",
			imgsrc:" ",
		},{
			optionsdivclass:'imgdiv',
			imgclass:'after-drag-img dropmouse',
			imgid:"mouse",
			imgsrc:" ",
		}],
		exerciseblock : [{
			imageblock:[{
				textclass:"uptext",
				textdata:data.string.excstring9,
				imageblockadditionalclass:"box1",
				imgoptions:[{
						imgclass : "options rct4 ",
						imgid : 'bee',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.bee
					}]
				},{
				imageblockadditionalclass:"box2",
				imgoptions:[{
						imgclass : "options rct2 ",
						imgid : 'tiger',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.tiger
					}]
				},{
				imageblockadditionalclass:"box3",
				imgoptions:[{
						imgclass : "options rct3 ",
						imgid : 'humans',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.human
					}]
				},{
				imageblockadditionalclass:"box4",
				imgoptions:[{
						imgclass : "options rct1 ",
						imgid : 'mouse',
						imgsrc : "",
						textclass:"rcttxt",
						textdata:data.string.mouse
					}]
				}]
			}]
	}


];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "flyingbird", src: imgpath+"q06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sittingbird", src: imgpath+"sitting-bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hive", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nest", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bee", src: imgpath+"q05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "humans", src: imgpath+"q12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fishpng", src: imgpath+"fish_big.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond", src: imgpath+"pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cowshed", src: imgpath+"cowshed.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jungle", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgmouse", src: imgpath+"mouse_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snake", src: imgpath+"q07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"q11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spidernet", src: imgpath+"spider_net.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new monkeyscoreTemplate();

	rhino.init($total_page);
	//alert("total pages"+content.length);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		//put_image(content, countNext);
		put_image2(content, countNext);
		put_image3(content, countNext);
		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

    	switch(countNext){
    		default:
    		var wrong_dropped = false;
		var correct_count=0;
		var counterfornext=0;
		$('.rct1,.rct2,.rct3,.rct4').draggable({
                containment : ".generalTemplateblock",
                revert : true,
                cursor : "all-scroll",
                zIndex: 100000,
				// helper : "clone"
            });
		$(".imgdiv").droppable({
                accept: ".rct1,.rct2,.rct3,.rct4",
                drop: function (event, ui){
                	if(ui.draggable.hasClass('rct1'))
                	{
						rhino.update(true);
                		counterfornext++;
		 				play_correct_incorrect_sound(1);
                		$(".after-drag-img").show(0);
                		ui.draggable.parent().css({"border":"4px solid #38761D"});
						$(ui.helper).addClass("disableanimation");
	                	$('.options').css({"pointer-events": "none"});
						ui.draggable.siblings(".corctopt").show(0);
						$(".buttonsel").removeClass("forhover");
	                    $this = $(this);
						$nextBtn.show(0);
                	}
                	else{
						if(!wrong_dropped){
							wrong_dropped = true;
							rhino.update(false);
						}
	                	ui.draggable.css({"pointer-events": "none"});
            			ui.draggable.parent().css({"border":"4px solid #FF0000"});
						ui.draggable.siblings(".wrngopt").show(0);
		 				play_correct_incorrect_sound(0);
                	}
                }
            });
    		function dropfunc(event, ui, $droppedOn){
				if(counterfornext==4){
					setTimeout(function(){
						$nextBtn.show(0);
					},700);
				}
				if(!wrong_dropped){
					correct_count++;
					console.log(correct_count);
				}
				if(correct_count==4)
				{
					rhino.update(true);
					$('.plate').animate({"opacity":"0"},500);
					$('.congratsmonkey,.congratstext').animate({"opacity":"1"},500);
		 			// nav_button_controls(500);
		 			setTimeout(function(){
						$nextBtn.show(0);
					},700);
				}
            }
    	}

	}

	// function nav_button_controls(delay_ms){
	// 	timeoutvar = setTimeout(function(){
	// 		if(countNext==0){
	// 			$nextBtn.show(0);
	// 		} else if( countNext>0 && countNext == $total_page-1){
	// 			$prevBtn.show(0);
	// 			ole.footerNotificationHandler.lessonEndSetNotification();
	// 		} else{
	// 			$prevBtn.show(0);
	// 			$nextBtn.show(0);
	// 		}
	// 	},delay_ms);
	// }

		function put_image2(content, count){
		if(content[count].hasOwnProperty('optionsblock')){
			var imageClass = content[count].optionsblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
		function put_image3(content, count){
				if(content[count].hasOwnProperty('exerciseblock')){
					var lncontent = content[count].exerciseblock[0];
						if(lncontent.hasOwnProperty('imageblock')){
							var imageClass = lncontent.imageblock;
							for(var i=0; i<imageClass.length; i++){
								var image_src = preload.getResult(imageClass[i].imgoptions[0].imgid).src;
								//get list of classes
								// console.log(image_src)
								var classes_list = imageClass[i].imgoptions[0].imgclass.match(/\S+/g) || [];
								var selector = ('.'+classes_list[classes_list.length-1]);
								// console.log(selector)
								$(selector).attr('src', image_src);
							}
						}
					}
			}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	//templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

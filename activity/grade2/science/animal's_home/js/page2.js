var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s0
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg",
					imgid : 'flyingbird',
					imgsrc: ""
			}]
		}]
	},
// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s1
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "treebg",
					imgid : 'treebg',
					imgsrc: ""
				},
				{
					imgclass: "nest",
					imgid : 'nest',
					imgsrc: ""
				},
				{
					imgclass: "flyingbird",
					imgid : 'eagleanim',
					imgsrc: ""
				}]
		}]

	},
// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s2
		},
		{
			textclass: "blanks2",
			textdata: data.string.blank
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "fishgif",
					imgid : 'fishgif',
					imgsrc: ""
				},
				{
					imgclass: "midfishimg",
					imgid : 'fishpng',
					imgsrc: ""
				},
				{
					imgclass: "sun3",
					imgid : 'sun',
					imgsrc: ""
				}]
		}]

	},
// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s3
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "pond",
					imgid : 'pond',
					imgsrc: ""
				},{
					imgclass: "fishgifs3",
					imgid : 'fishgif',
					imgsrc: ""
				}]
		}]

	},
// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s4
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg cow",
					imgid : 'cow',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgcow',
					imgsrc: ""
				},{
					imgclass: "sunright",
					imgid : 'sun',
					imgsrc: ""
				}]
		}]

	},
// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s5
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "cows5",
					imgid : 'cow',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'cowshed',
					imgsrc: ""
				}]
		}]

	},
// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s6
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg tiger",
					imgid : 'tiger',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgtiger',
					imgsrc: ""
				},{
					imgclass: "sunright",
					imgid : 'sun',
					imgsrc: ""
				}]
		}]

	},
// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s7
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "tiger-s7",
					imgid : 'tiger',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'jungle',
					imgsrc: ""
				}]
		}]

	},
// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s8
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mouse",
					imgid : 'mouse',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgmouse',
					imgsrc: ""
				}]
		}]

	},
// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s9
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "mouse",
					imgid : 'mousegif',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgmouse',
					imgsrc: ""
				}]
		}]

	},
// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s10
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "snake",
					imgid : 'snake',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgsnake',
					imgsrc: ""
				}]
		}]

	},
// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s11
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "snake",
					imgid : 'snakegif',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgsnake',
					imgsrc: ""
				}]
		}]

	},
// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s12
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg elephant",
					imgid : 'elephant',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'bgtiger',
					imgsrc: ""
				},{
					imgclass: "sunright",
					imgid : 'sun',
					imgsrc: ""
				}]
		}]

	},
// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s13
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "elephant-s13",
					imgid : 'elephant',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'jungle',
					imgsrc: ""
				}]
		}]

	},
// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s14
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg",
					imgid : 'spider',
					imgsrc: ""
				}]
		}]

	},
// slide15
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s15
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "spideranm",
					imgid : 'spidergif',
					imgsrc: ""
				},{
					imgclass: "background",
					imgid : 'spidernet',
					imgsrc: ""
				}]
		}]

	},
// slide16
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s16
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "midimg",
					imgid : 'cat',
					imgsrc: ""
				}]
		}]

	},
// slide17
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "toptxt",
			textdata: data.string.p2s17
		}],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "house-s13",
					imgid : 'house',
					imgsrc: ""
				},{
					imgclass: "catgif",
					imgid : 'catgif',
					imgsrc: ""
				}]
		}]

	}

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "flyingbird", src: imgpath+"flying-bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sittingbird", src: imgpath+"sitting-bird.png", type: createjs.AbstractLoader.IMAGE},
			{id: "treebg", src: imgpath+"treebg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nest", src: imgpath+"nest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fishgif", src: imgpath+"fish.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fishpng", src: imgpath+"fish_big.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pond", src: imgpath+"pond.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgcow", src: imgpath+"bg_cow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cowshed", src: imgpath+"cowshed.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tiger", src: imgpath+"tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgtiger", src: imgpath+"bg_tiger.png", type: createjs.AbstractLoader.IMAGE},
			{id: "jungle", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mouse", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mousegif", src: imgpath+"mouse.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bgmouse", src: imgpath+"mouse_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bgsnake", src: imgpath+"snake_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snake", src: imgpath+"snake.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snakegif", src: imgpath+"snakegif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "elephant", src: imgpath+"elephant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"q11.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spidernet", src: imgpath+"spider_net.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spidergif", src: imgpath+"spider.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cat", src: imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "catgif", src: imgpath+"catgif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "eagleanim", src: imgpath+"Eagle-Animation-3.gif", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p2_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p2_s10.ogg"},
			{id: "sound_11", src: soundAsset+"p2_s11.ogg"},
			{id: "sound_12", src: soundAsset+"p2_s12.ogg"},
			{id: "sound_13", src: soundAsset+"p2_s13.ogg"},
			{id: "sound_14", src: soundAsset+"p2_s14.ogg"},
			{id: "sound_15", src: soundAsset+"p2_s15.ogg"},
			{id: "sound_16", src: soundAsset+"p2_s16.ogg"},
			{id: "sound_17", src: soundAsset+"p2_s17.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		var currentSound;
		$(".toptxt").hide(0);
		switch(countNext){
			case 0:
				soundAnim("sound_"+countNext);
 				$nextBtn.hide(0);
			break;
			case 1:
				$(".flyingbird").hide(0);
				$(".toptxt").show(0);
				sound_player("sound_"+countNext);
				setTimeout(function(){
				$(".flyingbird").show(0);
				},1500);
				setTimeout(function(){
					navigationcontroller();
				},4700);
			break;
			case 2:
				setTimeout(function(){
					sound_player("sound_"+countNext);
					$(".toptxt").show(0);
				},2800);
				$(".fishgif").hide(0);
				setTimeout(function(){
					$(".midfishimg").animate({
					top:'60%',
					left:'79%',
					width:'20%',
					height:'20%'
					},3200,function(){
						$(".midfishimg").hide(0);
						$(".fishgif").show(0);
					});
				},5000);
				setTimeout(function(){
					navigationcontroller();
				},9000);

			break;
			case 3:
				$(".toptxt").show(0);
				sound_player("sound_"+countNext);
				setTimeout(function(){
				navigationcontroller();
				},2000);
			break;
			case 4:
				soundAnim("sound_"+countNext);
			break;
			case 5:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
			break;
			case 6:
				soundAnim("sound_"+countNext);
			break;
			case 7:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
			break;
			case 8:
				soundAnim("sound_"+countNext);
			break;
			case 9:
				$(".toptxt").show(0);
				sound_player("sound_"+countNext);
				$(".mouse").animate({
					left:'6%'
				},5000,function(){
					$(".mouse").hide(0);
				});
				setTimeout(function(){
				navigationcontroller();
				},5500);
			break;
			case 10:
				soundAnim("sound_"+countNext);
			break;
			case 11:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
				$(".snake").animate({
					left:'12%'
				},3000,function(){
					$(".snake").hide(0);
				});
			break;
			case 12:
				soundAnim("sound_"+countNext);
			break;
			case 13:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
			break;
			case 14:
				soundAnim("sound_"+countNext);
			break;
			case 15:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
			break;
			case 16:
				soundAnim("sound_"+countNext);
			break;
			case 17:
				$(".toptxt").show(0);
				sound_player1("sound_"+countNext);
			break;
			default:
				nav_button_controls(1000);
			break;

		}
	}
	function soundAnim(sndId){
		$(".midimg").addClass("pulseanm");
		setTimeout(function(){
		current_sound = createjs.Sound.play(sndId);
		current_sound.play();
		$(".toptxt").show(0);
		current_sound.on('complete',function(){
			navigationcontroller();
		});
		},2000);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			navigationcontroller();

		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content = [
	//coverpage
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bgwhite",
		extratextblock : [{
			textclass : "diytxt",
			textdata : data.string.diytxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:"bg",
				imgclass:"diy_cover",
				imgsrc:''
			}]
		}]
	},
	//slide0
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg",
		extratextblock : [{
			textclass : "question",
			textdata : data.string.diyq1
		}],

		exerciseblock : [{
			imageblock:[{
				imageblockadditionalclass:"class1 box1",
				imgoptions:[{
					imgclass : "rct",
					imgid : 'jungle',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q1op1
					}],
				},
				{
				imageblockadditionalclass:"class2 box2",
				imgoptions:[{
					imgclass : "rct1",
					imgid : 'shed',
					imgsrc : "",
					forshuffle: "btmtxt ",
					optdata : data.string.q1op2
					}],
				}]
			}]
	},
	//slide1
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg",
		extratextblock : [{
			textclass : "question",
			textdata : data.string.diyq2
		}],

		exerciseblock : [{
			imageblock:[{
				imageblockadditionalclass:"class1 box2",
				imgoptions:[{
					imgclass : "rct",
					imgid : 'nest',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q2op1
					}],
				},
				{
				imageblockadditionalclass:"class2 box1",
				imgoptions:[{
					imgclass : "rct1",
					imgid : 'shed',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q2op2
					}],
				}]
			}]
	},
	//slide2
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg",
		extratextblock : [{
			textclass : "question",
			textdata : data.string.diyq3
		}],

		exerciseblock : [{
			imageblock:[{
				imageblockadditionalclass:"class1 box2",
				imgoptions:[{
					imgclass : "rct1",
					imgid : 'hole',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q3op1
					}],
				},
				{
				imageblockadditionalclass:"class2 box1",
				imgoptions:[{
					imgclass : "rct",
					imgid : 'shed',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q3op2
					}],
				}]
			}]
	},
	//slide4
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg",
		extratextblock : [{
			textclass : "question",
			textdata : data.string.diyq4
		}],

		exerciseblock : [{
			imageblock:[{
				imageblockadditionalclass:"class1 box1",
				imgoptions:[{
					imgclass : "rct1",
					imgid : 'jungle',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q4op1
					}],
				},
				{
				imageblockadditionalclass:"class2 box2",
				imgoptions:[{
					imgclass : "rct",
					imgid : 'shed',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q4op2
					}],
				}]
			}]
	},
	//slide5
	{
		contentnocenteradjust : true,
		contentblockadditionalclass : "bg",
		extratextblock : [{
			textclass : "question",
			textdata : data.string.diyq5
		}],

		exerciseblock : [{
			imageblock:[{
				imageblockadditionalclass:"class1 box2",
				imgoptions:[{
					imgclass : "rct",
					imgid : 'house',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q5op1
					}],
				},
				{
				imageblockadditionalclass:"class2 box1",
				imgoptions:[{
					imgclass : "rct1",
					imgid : 'nest',
					imgsrc : "",
					forshuffle: "btmtxt",
					optdata : data.string.q5op2
					}],
				}]
			}]
	}
	];

//content.shufflearray();
$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
	// var $nextBtn = $('.nextBtn');
	// var $prevBtn = $('.prevBtn');
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "jungle", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shed", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "nest", src: imgpath+"nest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hole", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"diy.ogg"},
			{id: "tiger", src: soundAsset+"tiger.ogg"},
			{id: "bird", src: soundAsset+"bird.ogg"},
			{id: "mouse", src: soundAsset+"mouse.ogg"},
			{id: "elephant", src: soundAsset+"elephant.ogg"},
			{id: "cat", src: soundAsset+"cat.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		var currentSound;
		//tick qns start
		var wrngClicked = false;
		var corrCounter=0;

		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		//tick qns start
		$(".buttonsel").click(function(){
			/*class 1 is always for the right answer. updates scoreboard and disables other click if
			right answer is clicked*/
			if($(this).hasClass("class1") && $(this).hasClass("forhover")){
				$(".buttonsel").removeClass('forhover');
                corrCounter++;
				$(this).children(".corctopt").show(0);

					createjs.Sound.stop();
				play_correct_incorrect_sound(1);
				nav_button_controls(0);
			  }
			else{
				if($(this).hasClass("forhover"))
				{
					$(this).removeClass('forhover');
					$(this).children().first().css({
						"filter": "grayscale(90%)"
					});
					$(this).children(".wrngopt").show(0);
						createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					wrngClicked = true;
					}
			}
		});
		//tick qns end
	switch(countNext){
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_player('tiger');
			break;
			case 2:
			sound_player('bird');
			break;
			case 3:
			sound_player('mouse');
			break;
			case 4:
			sound_player('elephant');
			break;
			case 5:
			sound_player('cat');
			break;
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('exerciseblock')){
			var lncontent = content[count].exerciseblock[0];
				if(lncontent.hasOwnProperty('imageblock')){
					var imageClass = lncontent.imageblock;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgoptions[0].imgid).src;
						//get list of classes
						// console.log(image_src)
						var classes_list = imageClass[i].imgoptions[0].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						// console.log(selector)
						$(selector).attr('src', image_src);
					}
				}
			}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}

	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
			templatecaller();
	});


	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/Experiment/";
var soundAsset = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"experimenttime fadeInEffect",
                textclass:"centertext diyfont",
                textdata:data.string.experimenttime
            },
        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img1",
                    imgid: 'sunImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img3",
                    imgid: 'surajImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img4",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial1msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text1
            },
        ]
    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg2',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial2 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial2msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text2
            },
        ]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial3 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "torch hide1",
                    imgclass: "relativecls img3",
                    imgid: 'torchImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial3msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.torch
            },
        ]
    },
//slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg2',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial2 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "torch",
                    imgclass: "relativecls img3",
                    imgid: 'torchImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "beaker hide1",
                    imgclass: "relativecls img4",
                    imgid: 'beakerImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial2msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text3
            },
        ]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial3 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "torch",
                    imgclass: "relativecls img3",
                    imgid: 'torchImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "beaker ",
                    imgclass: "relativecls img4",
                    imgid: 'beakerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mirror hide1",
                    imgclass: "relativecls img5",
                    imgid: 'mirrorImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial3msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.mirror
            },
        ]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg4',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial4 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "torch",
                    imgclass: "relativecls img3",
                    imgid: 'torchImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "beaker ",
                    imgclass: "relativecls img4",
                    imgid: 'beakerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mirror",
                    imgclass: "relativecls img5",
                    imgid: 'mirrorImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial4msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text4
            },
        ]
    },
//    slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial5 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "torch",
                    imgclass: "relativecls img3",
                    imgid: 'torchImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "beaker droppable",
                    imgclass: "relativecls img4",
                    imgid: 'beakerImg',
                    imgsrc: "",
                    ans:data.string.mirror
                },
                {
                    imgdiv: "mirror draggable",
                    imgclass: "relativecls img5",
                    imgid: 'mirrorImg',
                    imgsrc: "",
                    ans:data.string.mirror
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial5msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text5
            },
        ]
    },
    // slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg5',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial6 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial6msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text7
            },
        ]
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg5',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial6 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "hand fadeInEffect",
                    imgclass: "relativecls img3",
                    imgid: 'handImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial6msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text8
            },
            {
                textdiv:"emptydiv",
                textclass:"",
                textdata:""
            },
        ]
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg6',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial7 fadeInEffect",
                    imgdiv: "dial7 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial3Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial7msg fadeInEffect",
                textclass:"centertext content1",
                textdata:data.string.p4text9
            },
        ]
    },
//    slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg6',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial6 fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial6msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p4text10
            },
        ]
    },
//    slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg6',
                    imgsrc: ""
                },
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src:imgpath1+"experiment-time.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg1", src:imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg2", src:imgpath1+"bg01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg3", src:imgpath1+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg4", src:imgpath1+"bg03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg5", src:imgpath1+"bg_for_result.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg6", src:imgpath1+"result.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src:imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial2Img", src:imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial3Img", src:imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "surajImg", src:imgpath+"suraj03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src:imgpath+"asha06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "torchImg", src:imgpath1+"torch.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beakerImg", src:imgpath1+"biker.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mirrorImg", src:imgpath1+"mirrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "beakerwithmirrorImg", src:imgpath1+"biker_with_mirrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "expresultImg", src:imgpath1+"experiment_result.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s4_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s4_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s4_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s4_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s4_p8.ogg"},
            {id: "sound_7a", src: soundAsset + "s4_p8_1.ogg"},
            {id: "sound_8", src: soundAsset + "s4_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s4_p10.ogg"},
            {id: "sound_10", src: soundAsset + "s4_p11.ogg"},
            {id: "sound_11", src: soundAsset + "s4_p12.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            sound_player("sound_0");
            break;
            case 1:
            sound_player("sound_1");
            break;
            case 2:
            sound_player("sound_2");
            break;
            case 3:
            sound_player("sound_3");
            break;
            case 4:
            sound_player("sound_4");
            break;
            case 5:
            $(".hide1").hide();
            $(".hide1").delay(1000).fadeIn(100);
            sound_player("sound_5");
            break;
            case 6:
            sound_player("sound_6");
            break;
            case 7:
            dragdrop();
            sound_player1("sound_7");
            $(".mirror").addClass("hovercursor");
            break;
            case 8:
            sound_player("sound_8");
            break;
            case 9:
            sound_player1("sound_9");
            $(".emptydiv,.hand").click(function(){
                $(".background img").attr("src",preload.getResult("expresultImg").src)
                nav_button_controls(2000);
                $(".hand").remove();
            });
            break;
            case 10:
            sound_player("sound_10");
            break;
            case 11:
            sound_player("sound_11");
            break;
            case 12:
            $(".background").delay(1000).animate({"width":"165%","left":"-48%","bottom":"-21%"},2000);
            nav_button_controls(3000);
            break;
            default:
            nav_button_controls(0);
            break;
        }
    }


        function nav_button_controls(delay_ms){
      		timeoutvar = setTimeout(function(){
      			if(countNext==0){
      				$nextBtn.show(0);
      			} else if( countNext>0 && countNext == $total_page-1){
      				$prevBtn.show(0);
      				ole.footerNotificationHandler.pageEndSetNotification();
      			} else{
      				$prevBtn.show(0);
      				$nextBtn.show(0);
      			}
      		},delay_ms);
      	}
        function sound_player(sound_id){
          createjs.Sound.stop();
          current_sound = createjs.Sound.play(sound_id);
          current_sound.play();
          current_sound.on("complete", function(){
                nav_button_controls(0);
          });
        }
        function sound_player1(sound_id){
          createjs.Sound.stop();
          current_sound = createjs.Sound.play(sound_id);
          current_sound.play();
        }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function dragdrop(){
        $(".droppable").attr("disabled","disabled");
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            cursor:"all-scroll",
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                var droppableans = $(this).attr("data-answer")
                if(draggableans.toString().trim() == droppableans.toString().trim()) {
                    ui.draggable.remove();
                    // navigationcontroller(countNext,$total_page,true);
                    $(".draggable").addClass("avoid-clicks");
                    $(".beaker img").attr("src",preload.getResult("beakerwithmirrorImg").src)
                    $(".dial5msg p").text(data.string.p4text6);
                    sound_player("sound_7a");
                }
            }
        });
    }
});

var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/Page01/";
var soundAsset = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "background",
                    imgclass: "relativecls img2",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p5text1
            },
        ]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "background",
                    imgclass: "relativecls img2",
                    imgid: 'bgImg1',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p5text2
            },
        ]
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "background",
                    imgclass: "relativecls img2",
                    imgid: 'bgImg2',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p5text3
            },
        ]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "box1 zoomInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'box1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "box2 zoomInEffect",
                    imgclass: "relativecls img3",
                    imgid: 'box2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "box3 zoomInEffect",
                    imgclass: "relativecls img4",
                    imgid: 'box3Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p5text4
            },
        ]
    },
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bgfinal",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img4",
                    imgid: 'ashaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img5",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"text2 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p5text5
            },
        ]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "bgfinal",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg3',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img4",
                    imgid: 'ashaImg1',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img5",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"text2 fadeInEffect",
                textclass:"centertext content1",
                textdata:data.string.p5text6
            },
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src:imgpath1+"img01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg1", src:imgpath1+"img02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg2", src:imgpath1+"img03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg3", src:imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "surajImg", src:imgpath+"suraj02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "surajImg1", src:imgpath+"suraj03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src:imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg1", src:imgpath+"asha05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashasmallImg", src:imgpath+"asha07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src:imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src:imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "box1Img", src:imgpath1+"img04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "box2Img", src:imgpath1+"img05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "box3Img", src:imgpath1+"img06.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s5_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s5_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s5_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s5_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s5_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s5_p6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
          case 0:
          sound_player("sound_0");
          break;
          case 1:
          sound_player("sound_1");
          break;
          case 2:
          sound_player("sound_2");
          break;
          case 3:
          sound_player("sound_3");
          $(".box1,.box2,.box3").hide();
          $(".box1").delay(4000).fadeIn(100);
          $(".box2").delay(5500).fadeIn(100);
          $(".box3").delay(7000).fadeIn(100);
          break;
          case 4:
          sound_player("sound_4");
          break;
          case 5:
          sound_player("sound_5");
          break;
          default:
          nav_button_controls(0);
          break;
        }
    }

    function nav_button_controls(delay_ms){
      timeoutvar = setTimeout(function(){
        if(countNext==0){
          $nextBtn.show(0);
        } else if( countNext>0 && countNext == $total_page-1){
          $prevBtn.show(0);
          ole.footerNotificationHandler.lessonEndSetNotification();
        } else{
          $prevBtn.show(0);
          $nextBtn.show(0);
        }
      },delay_ms);
    }
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
            nav_button_controls(0);
      });
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }
    function loadsvg(imgid){
        var s1 = Snap('#coloursvg');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            var array = [1,2,3,4,5,6,7]
            $(".clr").click(function(){
                var colorcode = $(this).attr("fill");
                $(".part"+array[0]).attr("fill",colorcode);
                array.splice(0,1);
                $(this).css("pointer-events","none");
            })
        });

    }
    function rotatethewheel(){
        $(".wheel,.wheel1").css("top","23%");
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
});

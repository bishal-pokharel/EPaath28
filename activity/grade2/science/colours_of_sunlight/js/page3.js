var imgpath = $ref + "/images/diy/";
var soundAsset = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"diytext fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.diy
            },
        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg1',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },

        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg2',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },

        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg3',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },

        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg4',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },

        ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg5',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },

        ],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dropimage",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg6',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1",
                textclass:"centertext chapter",
                textdata:data.string.dragdrop
            },
            {
                textdiv:"rightdiv",
            },
            {
                textdiv:"violet draggable",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo draggable",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue draggable",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green draggable",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow draggable",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange draggable",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red draggable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
            {
                textdiv:"droppable",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },

        ],
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg7',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"violet1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.violet,
                ans:data.string.violet
            },
            {
                textdiv:"indigo1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.indigo,
                ans:data.string.indigo
            },
            {
                textdiv:"blue1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.blue,
                ans:data.string.blue
            },
            {
                textdiv:"green1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.green,
                ans:data.string.green
            },
            {
                textdiv:"yellow1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.yellow,
                ans:data.string.yellow
            },
            {
                textdiv:"orange1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.orange,
                ans:data.string.orange
            },
            {
                textdiv:"red1 colourtext",
                textclass:"centertext chapter",
                textdata:data.string.red,
                ans:data.string.red
            },
        ],
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src:imgpath+"a_02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg", src:imgpath+"rainbow_voilet.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg1", src:imgpath+"rainbow_indigo.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg2", src:imgpath+"rainbow_blue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg3", src:imgpath+"rainbow_green.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg4", src:imgpath+"rainbow_yellow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg5", src:imgpath+"rainbow_orange.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg6", src:imgpath+"rainbow_red.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg7", src:imgpath+"rainbow.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s3_p2.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        if(countNext==1){
          sound_player("sound_0");
        }
        switch (countNext) {
            case 0:
            play_diy_audio();
            nav_button_controls(2000);
            break;
            case 8:
            nav_button_controls(0);
            break;
            default:
                var classoption = ["draggable violet","draggable indigo","draggable blue","draggable green","draggable yellow","draggable orange","draggable red"]
                shufflehint(classoption,"draggable");
                dragdrop();
                break;
        }
    }


    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function shufflehint(optionclass,option) {
        var optdiv = $(".coverboardfull");

        for (var i = optdiv.find("."+option).length; i >= 0; i--) {
            optdiv.append(optdiv.find("."+option).eq(Math.random() * i | 0));
        }
        optdiv.find("."+option).removeClass().addClass("current");
        optdiv.find(".current").each(function (index) {
            $(this).addClass(optionclass[index]);
        });
    }
    function loadsvg(imgid){
        var s1 = Snap('#sevencoloursvg');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            $("#sevencoloursvg").hide();
            $("#sevencoloursvg").delay(1490).fadeIn(20);
        });

    }
    function rotatethewheel(){
        $(".wheel,.wheel1").css("top","23%");
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
    function dragdrop(){
        $(".droppable").attr("disabled","disabled");
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                var draggableans = ui.draggable.attr("data-answer");
                var droppableans = $(this).attr("data-answer")
                if(draggableans.toString().trim() == droppableans.toString().trim()) {
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    navigationcontroller(countNext,$total_page,true);
                    $(".draggable").addClass("avoid-clicks");
                    $(this).addClass("correctans");
                }
                else {
                    current_sound.stop();
                    ui.draggable.addClass("wrongans avoid-clicks");
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }
});

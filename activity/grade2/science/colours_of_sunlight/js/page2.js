var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/Animaton/";
var soundAsset = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        imageblock:[{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img1",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly2 ",
                    imgclass: "relativecls img2",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly3 ",
                    imgclass: "relativecls img3",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img4",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img5",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial1msg fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p2text1
            },
        ]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img3",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img4",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img5",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p2text2
            },
        ],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img3",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img4",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img5",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p2text3
            },
        ],
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img3",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img4",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img5",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p2text4
            },
        ],
    },
    // slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background1",
                    imgclass: "relativecls img1",
                    imgid: 'rainbowbgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img3",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img4",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img5",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p2text5
            },
        ],
    },
    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sevencolour sevencolour1  show2",
                    imgclass: "relativecls img3",
                    imgid: 'sevencolour1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p2text6
            },
            {
                textdiv:"violet colourtext",
                textclass:"centertext chapter",
                textdata:data.string.violet
            },
            {
                textdiv:"indigo colourtext",
                textclass:"centertext chapter",
                textdata:data.string.indigo
            },
            {
                textdiv:"blue colourtext",
                textclass:"centertext chapter",
                textdata:data.string.blue
            },
            {
                textdiv:"green colourtext",
                textclass:"centertext chapter",
                textdata:data.string.green
            },
            {
                textdiv:"yellow colourtext",
                textclass:"centertext chapter",
                textdata:data.string.yellow
            },
            {
                textdiv:"orange colourtext",
                textclass:"centertext chapter",
                textclass:"centertext chapter",
                textdata:data.string.orange
            },
            {
                textdiv:"red colourtext",
                textclass:"centertext chapter",
                textdata:data.string.red
            },
        ],

    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sevencolur2  show2",
                    imgclass: "relativecls img3",
                    imgid: 'sevencolour1Img',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p2text7
            },
            {
                textdiv:"violet colourtext",
                textclass:"centertext chapter",
                textdata:data.string.violet
            },
            {
                textdiv:"indigo colourtext",
                textclass:"centertext chapter",
                textdata:data.string.indigo
            },
            {
                textdiv:"blue colourtext",
                textclass:"centertext chapter",
                textdata:data.string.blue
            },
            {
                textdiv:"green colourtext",
                textclass:"centertext chapter",
                textdata:data.string.green
            },
            {
                textdiv:"yellow colourtext",
                textclass:"centertext chapter",
                textdata:data.string.yellow
            },
            {
                textdiv:"orange colourtext",
                textclass:"centertext chapter",
                textclass:"centertext chapter",
                textdata:data.string.orange
            },
            {
                textdiv:"red colourtext",
                textclass:"centertext chapter",
                textdata:data.string.red
            },
            {
                textdiv:"violet colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.V
            },
            {
                textdiv:"indigo colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.I
            },
            {
                textdiv:"blue colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.B
            },
            {
                textdiv:"green colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.G
            },
            {
                textdiv:"yellow colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.Y
            },
            {
                textdiv:"orange colourtext1",
                textclass:"centertext chapter",
                textclass:"centertext chapter",
                textdata:data.string.O
            },
            {
                textdiv:"red colourtext1",
                textclass:"centertext chapter",
                textdata:data.string.R
            },
        ],

    },


];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "bgImg", src:imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src:imgpath+"asha05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src:imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src:imgpath+"butterfly_yellow.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly1Img", src:imgpath+"butterfly_orange.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "rainbowbgImg", src:imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashasmallImg", src:imgpath+"asha07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sevencolourImg", src:imgpath+"sevencolour.svg", type: createjs.AbstractLoader.IMAGE},
            {id: "sevencolour1Img", src:imgpath+"sevencolour.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_1", src: soundAsset + "s2_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p7.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
          case 0:
          sound_player("sound_0");
          break;
          case 1:
          sound_player("sound_1");
          break;
          case 2:
          sound_player("sound_2");
          break;
          case 3:
          sound_player("sound_3");
          break;
          case 4:
          sound_player("sound_4");
          break;
          case 5:
          sound_player("sound_5");
          $(".colourtext,.colourtext1").hide();
          $(".violet").delay(10000).fadeIn(100);
          $(".indigo").delay(11900).fadeIn(100);
          $(".blue").delay(14000).fadeIn(100);
          $(".green").delay(15000).fadeIn(100);
          $(".yellow").delay(16900).fadeIn(100);
          $(".orange").delay(18400).fadeIn(100);
          $(".red").delay(20000).fadeIn(100);
          break;
          case 6:
          sound_player("sound_6");
          $(".colourtext,.colourtext1").hide();
          $(".violet").delay(9000).fadeIn(100);
          $(".indigo").delay(10000).fadeIn(100);
          $(".blue").delay(11000).fadeIn(100);
          $(".green").delay(12000).fadeIn(100);
          $(".yellow").delay(13000).fadeIn(100);
          $(".orange").delay(14000).fadeIn(100);
          $(".red").delay(15000).fadeIn(100);
          nav_button_controls(16000);
          break;
          default:
          nav_button_controls(0);
          break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
        if(countNext==6){
          $nextBtn.hide(0);
          $prevBtn.hide(0);
        }
        else{
            nav_button_controls(0);
        }
      });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }
    function loadsvg(imgid){
        var s1 = Snap('#sevencoloursvg');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            $("#sevencoloursvg").hide();
            $("#sevencoloursvg").delay(1490).fadeIn(20);
        });

    }
    function rotatethewheel(){
        $(".wheel,.wheel1").css("top","23%");
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
});

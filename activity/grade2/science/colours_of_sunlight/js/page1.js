var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/Animaton/";
var soundAsset = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass: "coverpagetext",
        uppertextblock: [
            {
                textclass: "datafont centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock:[{
        imagestoshow: [
            {
                imgdiv: "coverpage",
                imgclass: "relativecls coverpageimg",
                imgid: 'coverpageImg',
                imgsrc: ""
            }
        ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly2 ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly3 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
              textdiv:"dial1msg fadeInEffect",
              textclass:"centertext chapter",
              textdata:data.string.p1text1
            },
        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'ashaImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial2 fadeInEffect",
                    imgclass: "relativecls img6",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial2msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text2
            },
        ]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial1msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text3
            },
        ]
    },
    // slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'surajImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial3 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial3msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text4
            },
        ]
    },
    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial4msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text5
            },
        ]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial5 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial5msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text6
            },
        ]
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial4msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text7
            },
        ]
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial5 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial5msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text8
            },
        ]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial4msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text9
            },
        ]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text10
            },
            {
                textdiv:"text2 fadeInEffect",
                textclass:"centertext content2",
                textdata:data.string.p1text11
            },
        ],
        svgblock:[
            {
                svgblock: 'coloursvg',
                handclass:"relativecls centertext"
            }
        ]
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel0 hide3",
                    imgclass: "relativecls img5 crs",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text12
            },
        ]
    },
    //slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                // {
                //     imgdiv: "wheel1 hide1",
                //     imgclass: "relativecls img3",
                //     imgid: 'wheel2Img',
                //     imgsrc: ""
                // },
                // {
                //     imgdiv: "wheel hide2",
                //     imgclass: "relativecls img4",
                //     imgid: 'wheel1Img',
                //     imgsrc: ""
                // },
                // {
                //     imgdiv: "wheel hide3",
                //     imgclass: "relativecls img5",
                //     imgid: 'wheelImg',
                //     imgsrc: ""
                // },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text13
            },
            {
                textdiv:"opt1 option ",
                textclass:"centertext chapter",
                textdata:data.string.whitecolour
            },
            {
                textdiv:"opt2 option ",
                textclass:"centertext chapter",
                textdata:data.string.allcolours
            }
        ]
    },
    //slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide3",
                    imgclass: "relativecls img5 crs",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text14
            }
        ]
    },
    //slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel0 hide3",
                    imgclass: "relativecls img5",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text15
            }
        ]
    },
    //slide 15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide3",
                    imgclass: "relativecls img5",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text16
            },
            {
                textdiv:"bottomtext1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text17
            }
        ]
    },
    //slide 16
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide3",
                    imgclass: "relativecls img5",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text16
            },
            {
                textdiv:"bottomtext1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text18
            }
        ]
    },
    //slide 17
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "ashasmall",
                    imgclass: "relativecls img1",
                    imgid: 'ashasmallImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide3",
                    imgclass: "relativecls img5",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
        textblock:[
            {
                textdiv:"text1 ",
                textclass:"centertext chapter",
                textdata:data.string.p1text16
            },
            {
                textdiv:"bottomtext1 fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text19
            }
        ]
    },
    //slide 18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial5 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial5msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text20
            },
        ]
    },
    //slide 19
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'niti1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial6 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial2Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial6msg fadeInEffect",
                textclass:"centertext content",
                textdata:data.string.p1text21
            },
        ]
    },
    //slide 20
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial1msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text22
            },
        ]
    },
    //slide21
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial7 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dial1Img',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial7msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text23
            },
        ]
    },
    //slide22
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img2",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly ",
                    imgclass: "relativecls img3",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterfly1 ",
                    imgclass: "relativecls img4",
                    imgid: 'butterfly1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "asha",
                    imgclass: "relativecls img5",
                    imgid: 'asha3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "niti",
                    imgclass: "relativecls img6",
                    imgid: 'nitiImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "suraj",
                    imgclass: "relativecls img7",
                    imgid: 'suraj1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial8 fadeInEffect",
                    imgclass: "relativecls img8",
                    imgid: 'dialImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"dial8msg fadeInEffect",
                textclass:"centertext chapter",
                textdata:data.string.p1text24
            },
        ]
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src:imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src:imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashaImg", src:imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha1Img", src:imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha2Img", src:imgpath+"asha04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "asha3Img", src:imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "ashasmallImg", src:imgpath+"asha07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src:imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src:imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial2Img", src:imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src:imgpath+"butterfly_yellow.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "butterfly1Img", src:imgpath+"butterfly_orange.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "nitiImg", src:imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "niti1Img", src:imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "surajImg", src:imgpath+"suraj01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "suraj1Img", src:imgpath+"suraj02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheelImg", src:imgpath1+"wheelanimation.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel1Img", src:imgpath1+"wheelanimation_blur.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel2Img", src:imgpath1+"wheelanimation_blur_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel3Img", src:imgpath1+"wheelanimation_white01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "colourssvgImg", src:imgpath +"coloursofwheel.svg", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_4a", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p11.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p12.ogg"},
            {id: "sound_12", src: soundAsset + "s1_p13.ogg"},
            {id: "sound_13", src: soundAsset + "s1_p14.ogg"},
            {id: "sound_14", src: soundAsset + "s1_p15.ogg"},
            {id: "sound_15", src: soundAsset + "s1_p16.ogg"},
            {id: "sound_16", src: soundAsset + "s1_p17.ogg"},
            {id: "sound_17", src: soundAsset + "s1_p18.ogg"},
            {id: "sound_18", src: soundAsset + "s1_p19.ogg"},
            {id: "sound_19", src: soundAsset + "s1_p20.ogg"},
            {id: "sound_20", src: soundAsset + "s1_p21.ogg"},
            {id: "sound_21", src: soundAsset + "s1_p22.ogg"},
            {id: "sound_22", src: soundAsset + "s1_p23.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            sound_player("sound_0");
            break;
            case 1:
            sound_player("sound_2");
            break;
            case 2:
            sound_player("sound_3");
            break;
            case 3:
            sound_player("sound_4");
            break;
            case 4:
            sound_player("sound_4a");
            break;
            case 5:
            sound_player("sound_5");
            break;
            case 6:
            sound_player("sound_6");
            break;
            case 7:
            sound_player("sound_7");
            break;
            case 8:
            sound_player("sound_8");
            break;
            case 9:
            sound_player("sound_9");
            break;
            case 10:
            loadsvg("colourssvgImg");
            sound_player("sound_10");
            break;
            case 11:
            sound_player("sound_11");
            $(".wheel0").click(function(){
                $(this).addClass("wheel");
                rotatethewheel();
                nav_button_controls(4000);
           });
                break;
            case 12:
            sound_player("sound_12");
                rotatethewheel();
                    $(".option").click(function(){
                        if($(this).hasClass("opt1")){
                            $(".option").addClass("avoid-clicks");
                            $(this).addClass("correctans");
                            $(this).append("<img class='correctwrongimg' src='images/right.png'/>");
                            play_correct_incorrect_sound(1);
                            nav_button_controls(0);
                        }
                        else{
                            $(this).addClass("wrongans avoid-clicks");
                            $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>");
                            play_correct_incorrect_sound(0);
                        }
                    });
                break;
            case 13:
            sound_player("sound_13");
                rotatethewheel();
                $(".wheel").click(function () {
                    rotatebackwheel();
                    nav_button_controls(4000);
                });
                break;
            case 14:
            sound_player("sound_14");
            break;
            case 15:
            sound_player("sound_15");
            rotatethewheel(true);
            break;
            case 16:
            sound_player("sound_16");
            rotatethewheel(true);
            break;
            case 17:
            sound_player("sound_17");
            rotatethewheel(true);
            break;
            case 18:
            sound_player("sound_18");
            break;
            case 19:
            sound_player("sound_19");
            break;
            case 20:
            sound_player("sound_20");
            break;
            case 21:
            sound_player("sound_21");
            break;
            case 22:
            sound_player("sound_22");
            break;
            default:
            nav_button_controls(0);
            break;
        }
    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
        if(countNext==10 || countNext==11 || countNext==12 || countNext==13){
          $nextBtn.hide(0);
          $prevBtn.hide(0);
        }else{
        nav_button_controls(0);
        }
      });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });



    function showhideelm(hideelm,showelm,hideshowtime1,hideshowtime2){
        hideelm.delay(hideshowtime1).fadeOut(1000);
        showelm.delay(hideshowtime2).fadeIn(1000);
    }
    function loadsvg(imgid){
        var s1 = Snap('#coloursvg');
        var svg = Snap.load(preload.getResult(imgid).src, function ( loadedFragment ) {
            s1.append(loadedFragment);
            var count=0;
            var array = [1,2,3,4,5,6,7]
            $(".clr").click(function(){
                var colorcode = $(this).attr("fill");
                $(".part"+array[0]).attr("fill",colorcode);
                array.splice(0,1);
                $(this).css("pointer-events","none");
                count++;
                if(count>=7){
                  nav_button_controls(1000);
                }
            })
        });

    }
    function rotatethewheel(moveup){
        moveup? $(".wheel,.wheel1").css("top","23%"):'';
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }
    function rotatebackwheel(){
        $(".hide3").removeClass("wheel").addClass("wheel0");
        $(".hide1").delay(300).animate({"opacity":"1"},1000);
        $(".hide2").delay(2000).animate({"opacity":"1"},1000);
        $(".hide3").delay(2500).animate({"opacity":"1"},1000);
    }
});

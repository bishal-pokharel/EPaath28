var imgpath1 = $ref + "/images/Animaton/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q1opt4,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q1opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q1opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q1opt3
            },
            {
                textdiv:"commonbtn option4 correct",
                textclass: "content2 centertext",
                textdata: data.string.q1opt4
            }

        ],
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q2opt3,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q2opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q2opt2
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "content2 centertext",
                textdata: data.string.q2opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q2opt4
            }

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q3opt3,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q3opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q3opt2
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "content2 centertext",
                textdata: data.string.q3opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q3opt4
            }

        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass:"bg",
        ans:data.string.q4opt3,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q4
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q4opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q4opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q4opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q4opt4
            }

        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q5opt1,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q5
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q5opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q5opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q5opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q5opt4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "wheel",
                    imgclass: "relativecls img2",
                    imgid: 'wheel3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel1 hide1",
                    imgclass: "relativecls img3",
                    imgid: 'wheel2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel hide2",
                    imgclass: "relativecls img4",
                    imgid: 'wheel1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "wheel0 hide3",
                    imgclass: "relativecls img5",
                    imgid: 'wheelImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q6opt1,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q6
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q6opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q6opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q6opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q6opt4
            }

        ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q7opt1,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q7
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q7opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q7opt2
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "waterbubble",
                    imgclass: "relativecls img1",
                    imgid: 'waterbubbleImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q8opt1,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q8
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn opt1",
                textclass: "content2 centertext",
                textdata: data.string.q8opt1
            },
            {
                textdiv:"commonbtn opt2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q8opt2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "waterdroplet",
                    imgclass: "relativecls img1",
                    imgid: 'waterdropletImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q9opt3,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q9
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q9opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q9opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q9opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q9opt4
            }

        ],
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        ans:data.string.q10opt1,
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q10
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q10opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q10opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q10opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q10opt4
            }

        ],
    }
];
// content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var lamp = new LampTemplate();
    lamp.init(content.length);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "wheelImg", src:imgpath1+"wheelanimation.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel1Img", src:imgpath1+"wheelanimation_blur.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel2Img", src:imgpath1+"wheelanimation_blur_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wheel3Img", src:imgpath1+"wheelanimation_white01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "waterbubbleImg", src:imgpath1+"girl_blowing_colour.png", type: createjs.AbstractLoader.IMAGE},
            {id: "waterdropletImg", src:imgpath1+"leaf.png", type: createjs.AbstractLoader.IMAGE},


            // sounds

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        /*generate question no at the beginning of question*/
        lamp.numberOfQuestions();
        switch (countNext) {
            case 0:
                shufflehint(data.string.q1opt4);
                checkans();
                break;
            case 1:
                shufflehint(data.string.q2opt3);
                checkans();
                break;
            case 2:
                shufflehint(data.string.q3opt3);
                checkans(data.string.q3_1);
                break;
            case 3:
                shufflehint(data.string.q4opt3);
                checkans();
                break;
            case 4:
                put_image();
                rotatethewheel();
                shufflehint(data.string.q5opt1);
                checkans();
                break;
            case 5:
                shufflehint(data.string.q6opt1);
                checkans();
                break;
            case 6:
                put_image();
                shufflehint1(data.string.q7opt1);
                checkans();
                break;
            case 7:
                put_image();
                shufflehint1(data.string.q8opt1);
                checkans();
                break;
            case 8:
                shufflehint(data.string.q9opt3);
                checkans();
                break;
            case 9:
                shufflehint(data.string.q10opt1);
                checkans();
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                lamp.gotoNext();
                templateCaller();
                break;
        }
    });

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }
    function shufflehint(correctans) {
        var correctans = $(".question").attr("data-answer");
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });

    }
    function shufflehint1(correctans) {
        var correctans = $(".question").attr("data-answer");
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn opt1","commonbtn opt2"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });

    }
    function checkans(questiontext){
        $(".commonbtn ").on("click",function () {
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                lamp.update(true);
                questiontext?$(".question span").eq(1).text(questiontext):"";
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
                lamp.update(false);
            }
        });
    }
    function rotatethewheel(){
        $(".hide3").delay(300).animate({"opacity":"0"},500);
        $(".hide2").delay(2000).animate({"opacity":"0"},500);
        $(".hide1").delay(2500).animate({"opacity":"0"},500);
    }

    function put_image() {
        var contentCount=content[countNext];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount,preload);
        imageblockcontent=contentCount.hasOwnProperty('imageblock1');
        contentCount = imageblockcontent?contentCount.imageblock1[0]:false;
        imageblockcontent?dynamicimageload(imageblockcontent,contentCount,preload):'';
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

});

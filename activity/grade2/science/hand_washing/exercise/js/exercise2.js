var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/audio/"+$lang+"/";

var sound_dg1 = new buzz.sound((soundAsset + "ex.ogg"));

var content=[

	//ex1
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q1a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q1b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q1c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q1d.png",
					}
				]
			}
		],
	},
	//ex2
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q2a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q2b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q2c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q2d.png",
					}
				]
			}
		],
	}
	,
	//ex3
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q3a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q3b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q3c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q3d.png",
					}
				]
			}
		],
	},
	//ex4
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q4a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q4b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q4c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q4d.png",
					}
				]
			}
		],
	},
	//ex5
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "2",
						imgsrc: imgpath + "q5a.png",
					},
					{
						optid: "1",
						imgsrc: imgpath + "q5b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q5c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q5d.png",
					}
				]
			}
		],
	},
	//ex6
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q6a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q6b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q6c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q6d.png",
					}
				]
			}
		],
	},
	//ex7
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q7a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q7b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q7c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q7d.png",
					}
				]
			}
		],
	},
	//ex8
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q8a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q8b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q8c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q8d.png",
					}
				]
			}
		],
	},
	//ex9
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q9a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q9b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q9c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q9d.png",
					}
				]
			}
		],
	},
	//ex10
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		exerciseblock: [
			{
				textdata: data.string.e2_ins,
				btndata: data.string.e2_submit,
				exeoptions: [
					{
						optid: "1",
						imgsrc: imgpath + "q10a.png",
					},
					{
						optid: "2",
						imgsrc: imgpath + "q10b.png",
					},
					{
						optid: "3",
						imgsrc: imgpath + "q10c.png",
					},
					{
						optid: "4",
						imgsrc: imgpath + "q10d.png",
					}
				]
			}
		],
	}
];

/*remove this for non random questions*/
// content.shufflearray();

$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var correctans = 0;
	var countNext = 0;
	var scoreupdate = 0;

	/*for limiting the questions to 10*/
	var $total_page = content.length;
	/*var $total_page = content.length;*/

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

	var testin = new NumberTemplate();

	testin.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		countNext==0?sound_dg1.play():'';
		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}

		var ansClicked = false;
		var wrngClicked = false;
		var functionreturn = 0;
		var ansorder = [];
		$(".optionsdiv").sortable({
			placeholder: "drop-highlight",
			stop: function(event, ui) {
				ansorder = $(this).sortable('toArray');
				$(".submitbtn").show(0);
				// $(".instruction").html(productOrder);
			}
		});
		$(".submitbtn").click(function(){
			functionreturn = check_order(ansorder);
			correctans= correctans + functionreturn;
		});
		function check_order(array){
			var correctones = 0;
			for(i=1; i<=array.length; i++){
				if(parseInt(array[i-1])==i){
					correctones++;
					$('#'+i).removeClass('incorrect').addClass('correct');
				} else{
					$('#'+i).removeClass('correct').addClass('incorrect');
				}
			}

			if(correctones==array.length){
				$( ".optionsdiv" ).sortable( "disable" );
				$('.optionscontainer').css('pointer-events', 'none');
				play_correct_incorrect_sound(1);
				$(".submitbtn").hide(0);
				$nextBtn.show(0);
			}
			else{
				play_correct_incorrect_sound(0);
				wrngClicked = true;
			}
			if(!ansClicked && !wrngClicked){
				scoreupdate++;
				testin.updatescore(scoreupdate);
				ansClicked = true;
			}
		}
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});



	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

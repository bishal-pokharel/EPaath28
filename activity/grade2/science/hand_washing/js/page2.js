var imgpath = $ref + "/images/";
if($lang=='np')
{
	var soundAsset = $ref+"/audio/np/";
}

if($lang=='en')
{
	var soundAsset = $ref+"/audio/en/";
}

var sound_dg1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var content=[
	{
	//slide 0
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "p1t2class",
			textdata : data.string.p2t1,
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "squirrel1",
						imgsrc: imgpath + "squirrel01.png"
					}
				]
			}
		]
	},

	{
	//slide 1
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t6,
		},
		{
			textclass : "number",
			textdata : '1',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "2step.gif"
					}
				]
			}
		]
	},


	{
	//slide 2
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t7,
		},
		{
			textclass : "number",
			textdata : '2',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "1step.gif"
					}
				]
			}
		]
	},

	{
	//slide 3
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t8,
		},
		{
			textclass : "number",
			textdata : '3',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "3step.gif"
					}
				]
			}
		]
	},

	{
	//slide 4
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t9,
		},
		{
			textclass : "number",
			textdata : '4',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "4step.gif"
					}
				]
			}
		]
	},

	{
	//slide 5
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t10,
		},
		{
			textclass : "number",
			textdata : '5',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "5step.gif"
					}
				]
			}
		]
	},

	{
	//slide 6
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t11,
		},
		{
			textclass : "number",
			textdata : '6',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "6step.gif"
					}
				]
			}
		]
	},

	{
	//slide 7
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t12,
		},
		{
			textclass : "number",
			textdata : '7',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "7step.gif"
					}
				]
			}
		]
	},

	{
	//slide 8
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t13,
		},
		{
			textclass : "number",
			textdata : '8',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "8step.gif"
					}
				]
			}
		]
	},

	{
	//slide 9
		additionalclasscontentblock:'ole-background-gradient-charoite',
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p2t2,
		},
		{
			textclass : "toptext",
			textdata : data.string.p2t14,
		},
		{
			textclass : "number",
			textdata : '9',
		}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "stepgif",
						imgsrc: imgpath + "9step.gif"
					}
				]
			}
		]
	},
];

$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	//Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);


		var div_ratio =$(".spriteimage").width()/$(".spriteimage").parent().width();
		$(".spriteimage").width(Math.round($(".spriteimage").parent().width()*div_ratio));

		switch(countNext){
			case 0:
			playaudio(sound_dg1, $(".1d"));
			break;

			case 1:
			playaudio(sound_dg2, $(".1d"));
			break;

			case 2:
			playaudio(sound_dg3, $(".1d"));
			break;

			case 3:
			playaudio(sound_dg4, $(".1d"));
			break;

			case 4:
			playaudio(sound_dg5, $(".1d"));
			break;

			case 5:
			playaudio(sound_dg6, $(".1d"));
			break;

			case 6:
			playaudio(sound_dg7, $(".1d"));
			break;

			case 7:
			playaudio(sound_dg8, $(".1d"));
			break;

			case 8:
			playaudio(sound_dg9, $(".1d"));
			break;

			case 9:
			playaudio(sound_dg10, $(".1d"));
			break;


		}

	}

	function playaudio(sound_data, $dialog_container){
			buzz.all().stop();
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
					if(!playing){
							playaudio(sound_data, $dialog_container);
					}
					return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
					ole.footerNotificationHandler.hideNotification();
			}else{
					$nextBtn.hide(0);
			}
			setTimeout(function(){
							sound_data.play();
							sound_data.bind('ended', function(){
							setTimeout(function(){
									$prevBtn.show(0);
									$dialog_container.addClass("playable");
									playing = false;
									sound_data.unbind('ended');
									if((countNext+1) == content.length){
											ole.footerNotificationHandler.lessonEndSetNotification();
									}else{
											$nextBtn.show(0);
									}
							}, 1000);
				});
			}, 1000);
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

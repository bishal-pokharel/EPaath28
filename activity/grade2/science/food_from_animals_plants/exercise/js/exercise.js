var imgpath = $ref+"/images/";
var imgpath1 = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
// slide 1
	{
		contentblockadditionalclass:'bg',
		extratextblock:[{
			textclass:"topTxt",
			textdata:data.string.question1
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'q01',
				imgclass:"qnImage",
				imgsrc:''
			}]
		}],
		textoptionscontainer:[{
			rhsdivclass:"rhstextdiv",
			textoption:[{
				textclass:"options",
				textdata:data.string.plants
			},{
				textclass:"options class1",
				textdata:data.string.animals
			}]
		}]
	},
	// slide 2
		{
			contentblockadditionalclass:'bg',
			extratextblock:[{
				textclass:"topTxt",
				textdata:data.string.question1
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'q02',
					imgclass:"qnImage",
					imgsrc:''
				}]
			}],
			textoptionscontainer:[{
				rhsdivclass:"rhstextdiv",
				textoption:[{
					textclass:"options class1",
					textdata:data.string.plants
				},{
					textclass:"options",
					textdata:data.string.animals
				}]
			}]
		},
// slide 3
	{
		contentblockadditionalclass:'bg',
		extratextblock:[{
			textclass:"topTxt",
			textdata:data.string.question1
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'q03',
				imgclass:"qnImage",
				imgsrc:''
			}]
		}],
		textoptionscontainer:[{
			rhsdivclass:"rhstextdiv",
			textoption:[{
				textclass:"options class1",
				textdata:data.string.plants
			},{
				textclass:"options",
				textdata:data.string.animals
			}]
		}]
	},
// slide 4
{
	contentblockadditionalclass:'bg',
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q04',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	textoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		textoption:[{
			textclass:"options class1",
			textdata:data.string.plants
		},{
			textclass:"options",
			textdata:data.string.animals
		}]
	}]
},
// slide 5
{
contentblockadditionalclass:'bg',
extratextblock:[{
	textclass:"topTxt",
	textdata:data.string.question1
}],
imageblock:[{
	imagestoshow:[{
		imgid:'q05',
		imgclass:"qnImage",
		imgsrc:''
	}]
}],
textoptionscontainer:[{
	rhsdivclass:"rhstextdiv",
	textoption:[{
		textclass:"options",
		textdata:data.string.plants
	},{
		textclass:"options class1",
		textdata:data.string.animals
	}]
}]
},
// slide 5
{
	contentblockadditionalclass:'bg',
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q06',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	textoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		textoption:[{
			textclass:"options",
			textdata:data.string.plants
		},{
			textclass:"options class1",
			textdata:data.string.animals
		}]
	}]
},
// slide 6
{
	contentblockadditionalclass:'bg',
	imageload:true,
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q07',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	imageoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		imageblock:[{
			imagestoshow:[{
				imgid:'hen',
				imgclass:"opnImage class1 oi1",
				imgsrc:''
			},{
				imgid:'potato_plant',
				imgclass:"opnImage oi2",
				imgsrc:''
			}]
		}]
	}]
},
// slide 7
{
	contentblockadditionalclass:'bg',
	imageload:true,
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q08',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	imageoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		imageblock:[{
			imagestoshow:[{
				imgid:'hen',
				imgclass:"opnImage oi1",
				imgsrc:''
			},{
				imgid:'potato_plant',
				imgclass:"opnImage class1 oi2",
				imgsrc:''
			}]
		}]
	}]
},
// slide 8
{
	contentblockadditionalclass:'bg',
	imageload:true,
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q09',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	imageoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		imageblock:[{
			imagestoshow:[{
				imgid:'cow',
				imgclass:"opnImage oi1",
				imgsrc:''
			},{
				imgid:'maize_plant',
				imgclass:"opnImage class1 oi2",
				imgsrc:''
			}]
		}]
	}]
},
// slide 9
{
	contentblockadditionalclass:'bg',
	imageload:true,
	extratextblock:[{
		textclass:"topTxt",
		textdata:data.string.question1
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'q10',
			imgclass:"qnImage",
			imgsrc:''
		}]
	}],
	imageoptionscontainer:[{
		rhsdivclass:"rhstextdiv",
		imageblock:[{
			imagestoshow:[{
				imgid:'maize_plant',
				imgclass:"opnImage oi1",
				imgsrc:''
			},{
				imgid:'cow',
				imgclass:"opnImage class1 oi2",
				imgsrc:''
			}]
		}]
	}]
}
];
content.shufflearray();
$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var item_txt ='';
	var countNext = 0;
	var img_src_1;
	var img_src_2;
	var img_src_3;
	var score_count = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "basketball", src: imgpath+"basket-ball.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "spinach", src: imgpath+"green-saag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cheese", src: imgpath+"cheese.png", type: createjs.AbstractLoader.IMAGE},
			{id: "egg", src: imgpath+"egg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "carrot", src: imgpath+"carrot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corn", src: imgpath+"maize.png", type: createjs.AbstractLoader.IMAGE},
			{id: "meat", src: imgpath+"meat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bacon", src: imgpath+"bacon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bananatree", src: imgpath+"banana_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato_plant", src: imgpath+"potato_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "meat_1", src: imgpath+"meat_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato", src: imgpath+"potato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curd", src: imgpath+"curd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "milk", src: imgpath+"milk.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cauliflower", src: imgpath+"cauliflower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "maize_plant", src: imgpath+"maize_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q01", src: imgpath1+"q01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q02", src: imgpath1+"q02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q03", src: imgpath1+"q03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q04", src: imgpath1+"q04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q05", src: imgpath1+"q05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q06", src: imgpath1+"q06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q07", src: imgpath1+"q07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q08", src: imgpath1+"q08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q09", src: imgpath1+"q09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "q10", src: imgpath1+"q10.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"exe_ins.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		content[countNext].imageload?put_image_sec(content, countNext):"";
	 	var testcount = 0;
	 if(countNext==0){sound_player('sound_1',1);}
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
}

	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var count = 0;
		var cor_count = 0;
		var updateScore = 0;
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
	 	switch(countNext){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			randomize(".rhstextdiv");
			break;
	 	}
  // buttonclick behavior
		$(".button").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){
					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							testin.update(true);
						}
						createjs.Sound.stop();

						play_correct_incorrect_sound(1);
						$(this).siblings(".corctopt").show(0);
						$('.button').css({'pointer-events': 'none'});
						ansClicked = true;
						if(countNext != $total_page)
						$nextBtn.show(0);
						if($(".rhstextdiv").children().hasClass("imgoptionscontainer")){
							$(this).css({
								"border":"5px solid #98C02E",
							});
						}else{
							$(this).css({
								"background": "#bed62fff",
								"border":"5px solid #deef3c",
								"color":"white",
							});
						}
					}
					else{
						testin.update(false);
						createjs.Sound.stop();
						
						play_correct_incorrect_sound(0);
						if($(".rhstextdiv").children().hasClass("imgoptionscontainer")){
							$(this).css({
								"border":"5px solid #980000",
								'pointer-events': 'none'
							});
						}else{
							$(this).css({
								"background":"#FF0000",
								"border":"5px solid #980000",
								"color":"white",
								'pointer-events': 'none'
							});
						}
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].imageoptionscontainer[0].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageoptionscontainer[0].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function sound_player(sound_id, navigate){
		 createjs.Sound.stop();
		 current_sound = createjs.Sound.play(sound_id);
		 current_sound.play();
		 current_sound.on('complete', function(){
			 navigate?navigationcontroller():"";
		 });
	 }

	 function templateCaller(){
		/*always hide next and previousloadimage navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller(); loadimage($(".myimg3").find("img"),$(".myimg3").find("p"),preload.getResult('ex1typ3qn'+quesNo).src,eval('data.string.ex2typ1op'+quesNo));


	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	function loadimage(imgrep,imgtext,imgsrc,quesNo){
		imgrep.attr("src",imgsrc);
		imgtext.text(quesNo);
	}

/*=====  End of Templates Controller Block  ======*/
});

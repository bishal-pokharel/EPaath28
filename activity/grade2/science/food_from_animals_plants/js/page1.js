var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//coverpage
		{
			contentnocenteradjust: true,
			extratextblock:[{
				textclass:"cvPgTxt",
				textdata:data.lesson.chapter,
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:"coverpage",
					imgclass:'coverpage',
					imgsrc:''
				}]
			}]
	},
//slide0
	{
		contentnocenteradjust: true,
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.p1top_txt,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer",
			imageblock:[{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"spinach",
					imgclass:'boxImage bi1',
					imgsrc:'',
				}],
				textclass:"boxtxt",
				textdata:data.string.spinach,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"banana",
					imgclass:'boxImage bi2',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.banana,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"cheese",
					imgclass:'boxImage bi3',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.cheese,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"egg",
					imgclass:'boxImage bi4',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.egg,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"carrot",
					imgclass:'boxImage bi5',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.carrot,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"fish",
					imgclass:'boxImage bi6',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.fish,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"corn",
					imgclass:'boxImage bi7',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.corn,
			},{
				imageblockclass:'imageblockclass',
				imagestoshow:[{
					imgid:"meat",
					imgclass:'boxImage bi8',
					imgsrc:''
				}],
				textclass:"boxtxt",
				textdata:data.string.meat,
			}]
		}],
	imageblock:[{
		imagestoshow:[{
			imgid:"pari",
			imgclass:'pari ',
			imgsrc:''
		}]
	}],
	speechbox:[{
		speechbox:'sp-1',
		imgclass:"spImage",
		imgid:'speechBox',
		imgsrc:'',
		datahighlightflag:'true',
		datahighlightcustomclass:'hide',
		textclass:"spTxt",
		textdata:data.string.p1s2_txt,
	}]
},
//slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"spinach",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"spinach"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"bacon",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"bacon"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"cheese",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"cheese"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"egg",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"egg"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"pig",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"bacon",
		}]
},
//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"banana",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"banana"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"meat_1",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"meat_1"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"potato",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"potato"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"curd",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"curd"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"bananatree",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"banana",
		}]
},
//slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"banana",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"banana"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"milk",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"milk"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"corn",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"corn"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"egg",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"egg"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"cow",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"milk",
		}]
},
//slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"orange",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"orange"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"potato",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"potato"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"curd",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"curd"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"egg",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"egg"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"potato_plant",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"potato",
		}]
},
//slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"spinach",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"spinach"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"potato",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"potato"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"egg",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"egg"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"cheese",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"cheese"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"hen",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"egg",
		}]
},
//slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"bluebg",
		uppertextblock:[{
			textclass:"toptxt",
			textdata:data.string.dnd_instrn,
		}],
		multipleimagecontainer:[{
			imagecontainerclass:"flexImageContainer_rhs",
			imageblock:[{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"orange",
					imgclass:'draggable boxImage_sec bi1',
					imgsrc:'',
					answer:"orange"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"cauliflower",
					imgclass:'draggable boxImage_sec bi2',
					imgsrc:'',
					answer:"cauliflower"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"milk",
					imgclass:'draggable boxImage_sec bi3',
					imgsrc:'',
					answer:"milk"
				}]
			},{
				imageblockclass:'imageblockclass_sec',
				imagestoshow:[{
					imgid:"corn",
					imgclass:'draggable boxImage_sec bi4',
					imgsrc:'',
					answer:"corn"
				}]
			}]
		}],
		dropdiv:[{
			dropdivclass:"dropContainer",
			imageblock:[{
				imagestoshow:[{
					imgid:"maize_plant",
					imgclass:'bigImage',
					imgsrc:''
				}]
			}],
			answer:"corn",
		}]
},
//slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:"greenbg",
		uppertextblock:[{
			textclass:"toptxtLast",
			textdata:data.string.p1lasttop,
		}],
		extratextblock:[{
			datahighlightflag:'true',
			datahighlightcustomclass:"pinkTxt",
			textclass:"btmtxtLast",
			textdata:data.string.p1lastbtm,
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:"boyLast",
				imgclass:'boyLast',
				imgsrc:''
			},{
				imgid:"food_animals",
				imgclass:'food animals',
				imgsrc:''
			},{
				imgid:"food_plants",
				imgclass:'food plants',
				imgsrc:''
			}]
		}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "basketball", src: imgpath+"basket-ball.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spinach", src: imgpath+"green-saag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cheese", src: imgpath+"cheese.png", type: createjs.AbstractLoader.IMAGE},
			{id: "egg", src: imgpath+"egg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "carrot", src: imgpath+"carrot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fish", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corn", src: imgpath+"maize.png", type: createjs.AbstractLoader.IMAGE},
			{id: "meat", src: imgpath+"meat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bacon", src: imgpath+"bacon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bananatree", src: imgpath+"banana_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cow", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato_plant", src: imgpath+"potato_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "meat_1", src: imgpath+"meat_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato", src: imgpath+"potato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "curd", src: imgpath+"curd.png", type: createjs.AbstractLoader.IMAGE},
			{id: "milk", src: imgpath+"milk.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cauliflower", src: imgpath+"cauliflower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "maize_plant", src: imgpath+"maize_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pari", src: imgpath+"pari.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boyLast", src: imgpath+"boy03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "food_animals", src: imgpath+"food_from_animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "food_plants", src: imgpath+"food_from_plants.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_l", src: imgpath+"arrow_l.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow_r", src: imgpath+"arrow_r.png", type: createjs.AbstractLoader.IMAGE},
			// speechbox
			{id: "speechBox", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2_1", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_2_2", src: soundAsset+"s1_p2_2.ogg"},
            {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
            {id: "sound_9", src: soundAsset+"s1_p9.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// put_image_sec(content, countNext);
		switch (countNext) {
			case 0:
                sound_player("sound_"+(countNext+1),true);
                break;
            case 1:
                put_image_sec(content, countNext);
			break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
                countNext==2?sound_player("sound_"+(countNext+1),false):"";
                put_image_sec(content, countNext);
			put_image_third(content, countNext);
			break;
			case 8:
                sound_player("sound_"+(countNext+1),true);
                break;
			default:
             break;
		}
		put_speechbox_image(content, countNext);
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		var count = 0, i, j;
		$(".pari, .sp-1").hide(0);
		function hideImages(){
				$(".grayout").fadeOut(3000, function(){ $(this).remove();});
				$(".pari").delay(4000).fadeIn();
				$(".sp-1").delay(4000).fadeIn();
				setTimeout(function () {
                    $(".toptxt").text("");
                },4000)
		}
		switch(countNext){
			case 0:
			// nextBtn.show(0);
			break;
			case 1:
                sound_player("sound_"+(countNext+1),false);
                $nextBtn.hide(0);
				$(".boxImage").on("click", function(){
					current_sound.stop();
					$(this).addClass("highlight");
					$(this).parent().addClass('borderfy');
					$(this).css({"pointer-events": "none"});
					count++;
					if(count == 4){
                        setTimeout(function(){
                            current_sound.stop();
                            sound_player("sound_2_2",true);
                        },5000);
						$(".boxImage").css({"pointer-events" : "none"});
						for(var i = 1; i<=8; i++){
							if(!$(".bi"+i).hasClass("highlight")){
								$(".bi"+i).parent().addClass("grayout");
									hideImages();
                            }
						}
					}
					});
			break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			randomize(".flexImageContainer_rhs");
				$(".draggable").draggable({
					revert : true,
				});
				$(".droppable").droppable({
					accept : ".draggable",
					hoverClass : "hovered",
					drop: function(event, ui){
						handleDrop(event, ui, ".draggable", $(this));
					}
				});
			break;
			case 8:
				break;
			default:
				navigationcontroller();
			break;
		}
	}
// drag drop
	function handleDrop(event, ui, dragClass, dropClass){
		var dragAns = ui.draggable.parent().find("img").attr("answer");
		var dropAns = $(dropClass).parent().find("div").attr("answer");
		if(dragAns.toString().trim() == dropAns.toString().trim()){
		$(".draggable").draggable("disable");
		play_correct_incorrect_sound(1);
		$(dropClass).css({
			"background" : "#98C02E",
			"border" : "4px solid #EEFF41"
		});
		$(dropClass).siblings().css({
			"display" : "block"
		});
		ui.draggable.detach();
		switch (countNext) {
			case 2:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("bacon").src+"'/>")
			break;
			case 3:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("banana").src+"'/>")
			break;
			case 4:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("milk").src+"'/>")
			break;
			case 5:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("potato").src+"'/>")
			break;
			case 6:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("egg").src+"'/>")
			break;
			case 7:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("corn").src+"'/>")
			break;
			default:

		}
		navigationcontroller();
		}
		else{
		play_correct_incorrect_sound(0);
		ui.draggable.siblings().css({
			"display" : "block"
		});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, navigate){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			navigate?navigationcontroller():"";
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].multipleimagecontainer[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].multipleimagecontainer[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_image_third(content, count){
		if(content[count].dropdiv[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].dropdiv[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//console.log("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "bg",
		uppertextblock:[
		{
			textclass: "covertext",
			textdata: data.string.diytxt
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'diybg',
				imgsrc: ""
			}
		]
	}]
},
//slide 1 onwards
{
	contentblockadditionalclass: "diybg",
		uppertextblock:[
			{
				textclass: "diyinstext",
				textdata: data.string.diyqn
			},
			{
				textclass: "congo-text",
				textdata: data.string.p3text9
			},
			{
				textclass: "diyobjtext obj1",
				textdata: data.string.pumpkin
			},
			{
				textclass: "diyobjtext obj2",
				textdata: data.string.ghee
			},
			{
				textclass: "diyobjtext obj3",
				textdata: data.string.meat
			},
			{
				textclass: "diyobjtext obj4",
				textdata: data.string.banana
			},
			{
				textclass: "diyobjtext obj5",
				textdata: data.string.cheese
			},
			{
				textclass: "diyobjtext obj6",
				textdata: data.string.spinach
			},
			{
				textclass: "diyobjtext obj7",
				textdata: data.string.potato
			},
			{
				textclass: "diyobjtext obj8",
				textdata: data.string.milk
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "dragme plant obj1",
					imgid : 'dragimg-1',
					imgsrc: ""
				},
				{
					imgclass: "dragme animal obj2",
					imgid : 'dragimg-2',
					imgsrc: ""
				},
				{
					imgclass: "dragme animal obj3",
					imgid : 'dragimg-3',
					imgsrc: ""
				},
				{
					imgclass: "dragme plant obj4",
					imgid : 'dragimg-4',
					imgsrc: ""
				},
				{
					imgclass: "dragme animal obj5",
					imgid : 'dragimg-5',
					imgsrc: ""
				},
				{
					imgclass: "dragme plant obj6",
					imgid : 'dragimg-6',
					imgsrc: ""
				},
				{
					imgclass: "dragme plant obj7",
					imgid : 'dragimg-7',
					imgsrc: ""
				},
				{
					imgclass: "dragme animal obj8",
					imgid : 'dragimg-8',
					imgsrc: ""
				},
				{
					imgclass: "congo-monkey",
					imgid : 'monkey',
					imgsrc: ""
				},
				{
					imgclass: "wrong-ico",
					imgid : 'wrong',
					imgsrc: ""
				},
				{
					imgclass: "girlLeft",
					imgid : 'girl',
					imgsrc: ""
				}
			]
		}],
		specialdropdiv : [{
			droplabelclass : "plantlabel my_font_big",
			droplabeldata : data.string.opn1,
			dropdiv : "plantdrop"
		}, {
			droplabelclass : "animallabel my_font_big",
			droplabeldata : data.string.opn2,
			dropdiv : "animaldrop"
		}]
},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var finCount = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybg", src: "images/diy_bg/a_25.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-1", src: imgpath+"pumpkin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-2", src: imgpath+"ghee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-3", src: imgpath+"chicken_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-4", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-5", src: imgpath+"cheese.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-6", src: imgpath+"green-saag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-7", src: imgpath+"potato.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dragimg-8", src: imgpath+"milk.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"girl_3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlLast", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: "images/sundar/correct-1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
        // sound_player("sound_1",false);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		var dndCounter = 1;
		function dndmaker(){
			$(".obj"+dndCounter).fadeIn();
		}

		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(3000);
			break;
			case 1:
			dndmaker();
			break;

		}

		$(".dragme").draggable({
	 		containment : ".generalTemplateblock",
      revert : true,
	 		cursor : "alias",
	 		zIndex: 100000,
	 	});
		$(".plantdrop").droppable({
      hoverClass: "hovered",
	 		drop: function (event, ui){
	 			$this = $(this);
	 			dropfunc(event, ui, $this, "plant");
	 		}
	 	});

		$(".animaldrop").droppable({
      hoverClass: "hovered",
	 		drop: function (event, ui){
	 			$this = $(this);
	 			dropfunc(event, ui, $this, "animal");
	 		}
	 	});

	 	function dropfunc(event, ui, $droppedOn, classname){
			if(ui.draggable.hasClass(classname)){
				$(".wrong-ico").hide();
        //cor_sound.play();
				current_sound.stop();
				play_correct_incorrect_sound(true);
        ui.draggable.draggable('option', 'revert', false);
		 		ui.draggable.draggable('disable');
				var count;
				count = $droppedOn.children().length;
				var top = 20;
				var left = 0;
				if(count==0) left = 2;
				if(count==1) left = 25;
				if(count==2) left = 50;
				if(count==3) left = 75;
		 		$droppedOn.css('color','#333');
		 		finCount++;
				$(ui.draggable).detach().css({
					"position" : "absolute",
					"top" : top+"%",
					"left": left + "%",
					"width": "25%",
					"height": "auto",
					"max-height": "70%"
				}).appendTo($droppedOn);
				if(finCount == 8)
					// nav_button_controls();
					activityFinish();
				dndCounter++;
				dndmaker();
	 		}
			else{
                current_sound.stop();
                play_correct_incorrect_sound(false);
				$(".wrong-ico").fadeIn();
			}
	 	}
	}

	function activityFinish(){
		$(".diyobjtext").hide(0);
		$(".diyinstext").hide(0);
		$(".plantlabel").animate({
			"top": "29%",
			"left": "28%",
			"width" : "34%"
		},2000);
		$(".plantdrop").animate({
			"top": "37%",
			"left": "28%",
			"height":"49%",
			"width" : "34%"
		},2000, function(){
			nav_button_controls();
		}
	);
	$(".animallabel").animate({
		"top" : "29%",
		"left" : "63%",
		"width" : "34%"
	},2000);
	$(".animaldrop").animate({
		"top": "37%",
		"left": "63%",
		"height":"49%",
		"width" : "34%",
	},2000);
	$(".animal:eq(0), .plant:eq(0) ").animate({
		"left" : "3%",
		"top" : "10%",
		"width": "40%"
	},2000);
	$(".animal:eq(1), .plant:eq(1)").animate({
		"left" : "60%",
		"top" : "10%",
		"width": "40%"
	},2000);
	$(".animal:eq(2), .plant:eq(2)").animate({
		"left" : "3%",
		"top" : "50%",
		"width": "40%"
	},2000);
	$(".animal:eq(3), .plant:eq(3)").animate({
		"left" : "60%",
		"top" : "50%",
		"width": "40%"
	},2000);
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, navigate){
		console.log("This is sound_id"+sound_id);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(navigate)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$refreshBtn.click(function(){
		templatecaller();
	});

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

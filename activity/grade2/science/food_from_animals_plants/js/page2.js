var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide1
		{
			// contentnocenteradjust: true,
			contentblockadditionalclass: "bluebg",
			uppertextblock:[
			{
				textclass: "toptxt",
				textdata: data.string.p2s0
			}],
			extratextblock:[{
					textclass: "plantTxt",
					textdata: data.string.plant
				},
				{
					textclass: "animalTxt",
					textdata: data.string.animal
				}
			],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "girl_left",
						imgid : 'girl_1',
						imgsrc: ""
				},{
					imgclass: "circleImage zoomIn cIm1",
					imgid : 'plants',
					imgsrc: ""
				},{
					imgclass: "circleImage zoomIn1 cIm2",
					imgid : 'animals',
					imgsrc: ""
				}]
			}]
		},
// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: "bluebg",
		uppertextblock:[
		{
			datahighlightflag:'true',
			datahighlightcustomclass:"pink",
			textclass: "toptxt",
			textdata: data.string.p2s1
		}],
		extratextblock:[{
				textclass: "ptSec",
				textdata: data.string.plant
			}
		],
		imageblock:[{
			imagestoshow:[
				{
					imgclass: "girl_left",
					imgid : 'girl_2',
					imgsrc: ""
				},{
					imgclass: "plants bouncein",
					imgid : 'plants',
					imgsrc: ""
			}]
		}]
	},
// slide3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "orangebg",
	uppertextblock:[
	{
		textclass: "toptxt",
		textdata: data.string.p2s2
	},{
		textclass: "btmtxt",
		textdata: data.string.p2s2_1
	}],
	multipleimagecontainer:[{
		imagecontainerclass:"ImageContainer",
		imageblock:[{
			imageblockclass:'imagebgAnim zoomInLeft',
			imagestoshow:[{
				imgid:"image_box",
				imgclass:'CirBgImage',
				imgsrc:''
			}]
			},{
			imageblockclass:"imageholder p3_1",
			imagestoshow:[{
				imgid:"rice",
				imgclass:'boxImage bi1',
				imgsrc:''
			}],
				textclass: "boxTxt",
				textdata: data.string.rice
		},{
			imageblockclass:'imageholder p3_2',
			imagestoshow:[{
				imgid:"wheat",
				imgclass:'boxImage bi2',
				imgsrc:''
			}],
				textclass: "boxTxt",
				textdata: data.string.wheat
		},{
			imageblockclass:'imageholder p3_3',
			imagestoshow:[{
				imgid:"maize",
				imgclass:'boxImage bi3',
				imgsrc:''
			}],
				textclass: "boxTxt",
				textdata: data.string.maize
		}]
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:"boy",
			imgclass:'boyImage',
			imgsrc:''
		}]
	}]
},
// slide4
{
contentnocenteradjust: true,
contentblockadditionalclass: "orangebg",
uppertextblock:[
{
	textclass: "toptxt",
	textdata: data.string.p2s4
}],
multipleimagecontainer:[{
	imagecontainerclass:"ImageContainer",
	imageblock:[{
		imageblockclass:'imagebgAnim zoomInLeft',
		imagestoshow:[{
			imgid:"image_box",
			imgclass:'CirBgImage',
			imgsrc:''
		}]
		},{
		imageblockclass:"imageholder p3_1",
		imagestoshow:[{
			imgid:"orange",
			imgclass:'boxImage_sec bi1',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.orange
	},{
		imageblockclass:'imageholder p3_2',
		imagestoshow:[{
			imgid:"mango",
			imgclass:'boxImage_sec bi2',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.mango
	},{
		imageblockclass:'imageholder p3_3',
		imagestoshow:[{
			imgid:"banana",
			imgclass:'boxImage_sec bi3',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.banana
	},{
		imageblockclass:'imageholder p3_4',
		imagestoshow:[{
			imgid:"pineapple",
			imgclass:'boxImage_sec bi4',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.pineapple
	}]
}],
imageblock:[{
	imagestoshow:[{
		imgid:"boy",
		imgclass:'boyImage',
		imgsrc:''
	}]
}]
},
// slide5
{
contentnocenteradjust: true,
contentblockadditionalclass: "yellowbg",
uppertextblock:[
{
	textclass: "toptxt",
	textdata: data.string.p2s5
}],
multipleimagecontainer:[{
	imagecontainerclass:"ImageContainer",
	imageblock:[{
		imageblockclass:'imagebgAnim zoomInLeft',
		imagestoshow:[{
			imgid:"image_box",
			imgclass:'CirBgImage',
			imgsrc:''
		}]
		},{
		imageblockclass:'imageholder p3_1_1',
		imagestoshow:[{
			imgid:"spinach",
			imgclass:'boxImage_sec bi1',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.spinach
	},{
		imageblockclass:'imageholder p3_2_1',
		imagestoshow:[{
			imgid:"corriander",
			imgclass:'boxImage_sec bi2',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.corriander
	},{
		imageblockclass:'imageholder p3_3_1',
		imagestoshow:[{
			imgid:"broccoli",
			imgclass:'boxImage_sec bi3',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.broccoli
	},{
		imageblockclass:'imageholder p3_4_1',
		imagestoshow:[{
			imgid:"cauliflower",
			imgclass:'boxImage_sec bi4',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.cauliflower
	},{
		imageblockclass:'imageholder p3_5',
		imagestoshow:[{
			imgid:"cabbage",
			imgclass:'boxImage_sec bi5',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.cabbage
	}]
}],
imageblock:[{
	imagestoshow:[{
		imgid:"boy",
		imgclass:'boyImage',
		imgsrc:''
	}]
}]
},
// slide6
{
contentnocenteradjust: true,
contentblockadditionalclass: "yellowbg",
uppertextblock:[
{
	textclass: "toptxt",
	textdata: data.string.p2s6
}],
multipleimagecontainer:[{
	imagecontainerclass:"ImageContainer",
	imageblock:[{
		imageblockclass:'imagebgAnim zoomInLeft',
		imagestoshow:[{
			imgid:"image_box",
			imgclass:'CirBgImage',
			imgsrc:''
		}]
		},{
		imageblockclass:'imageholder p3_1_6',
		imagestoshow:[{
			imgid:"sugarcane",
			imgclass:'boxImage_sec bi1',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.sugarcane
	},{
		imageblockclass:'imageholder p3_2_6',
		imagestoshow:[{
			imgid:"fern",
			imgclass:'boxImage_sec bi2',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.fern
	},{
		imageblockclass:'imageholder p3_3_6',
		imagestoshow:[{
			imgid:"asparagus",
			imgclass:'boxImage_sec bi3',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.asparagus
	}]
}],
imageblock:[{
	imagestoshow:[{
		imgid:"boy",
		imgclass:'boyImage',
		imgsrc:''
	}]
}]
},
// slide7
{
contentnocenteradjust: true,
contentblockadditionalclass: "yellowbg",
uppertextblock:[
{
	textclass: "toptxt",
	textdata: data.string.p2s7
}],
multipleimagecontainer:[{
	imagecontainerclass:"ImageContainer",
	imageblock:[{
		imageblockclass:'imagebgAnim zoomInLeft',
		imagestoshow:[{
			imgid:"image_box",
			imgclass:'CirBgImage_7',
			imgsrc:''
		}]
		},{
		imageblockclass:'imageholder p3_1',
		imagestoshow:[{
			imgid:"potato",
			imgclass:'boxImage_sec bi1',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.potato
	},{
		imageblockclass:'imageholder p3_2sev',
		imagestoshow:[{
			imgid:"carrot",
			imgclass:'boxImage_sec bi2',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.carrot
	},{
		imageblockclass:'imageholder p3_3',
		imagestoshow:[{
			imgid:"turnip",
			imgclass:'boxImage_sec bi3',
			imgsrc:''
		}],
			textclass: "boxTxt",
			textdata: data.string.turnip
	}]
}],
imageblock:[{
	imagestoshow:[{
		imgid:"boy",
		imgclass:'boyImage',
		imgsrc:''
	}]
}]
},
// slide8
{
contentnocenteradjust: true,
contentblockadditionalclass: "bluebg",
multipleimagecontainer:[{}],
extratextblock:[
{
	textclass: "lastTxt",
	textdata: data.string.p2s8
}],
imageblock:[{
	imagestoshow:[{
		imgid:"girl_1",
		imgclass:'girlLast',
		imgsrc:''
	}]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl_1", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl_2", src: imgpath+"girl_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plants", src: imgpath+"groups_of_food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "animals", src: imgpath+"group_of_animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rice", src: imgpath+"rice.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wheat", src: imgpath+"wheat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "maize", src: imgpath+"maize_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pineapple", src: imgpath+"pineapple.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mango", src: imgpath+"mango.png", type: createjs.AbstractLoader.IMAGE},
			{id: "banana", src: imgpath+"banana.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spinach", src: imgpath+"green-saag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "corriander", src: imgpath+"corriander.png", type: createjs.AbstractLoader.IMAGE},
			{id: "broccoli", src: imgpath+"brocauli.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cauliflower", src: imgpath+"cauliflower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cabbage", src: imgpath+"cabbage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sugarcane", src: imgpath+"sugarcane.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fern", src: imgpath+"fiddlehead-fern.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asparagus", src: imgpath+"asparagus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "carrot", src: imgpath+"carrots.png", type: createjs.AbstractLoader.IMAGE},
			{id: "potato", src: imgpath+"potato_plant.png", type: createjs.AbstractLoader.IMAGE},
			{id: "turnip", src: imgpath+"turnips.png", type: createjs.AbstractLoader.IMAGE},
			{id: "image_box", src: imgpath+"image_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mixedfood", src: imgpath+"mixedfood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "image_box", src: imgpath+"image_box.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_7_2", src: soundAsset+"s2_p7-2.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p8.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
        sound_player("sound_"+(countNext+1),true);
        switch (countNext) {
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				put_image_sec(content, countNext);
			break;
			default:

		}
		put_image2(content, countNext);
		var currentSound;
		switch(countNext){
			case 0:
			// $(".plantTxt").addClass("zoomIn");
			// $(".animalTxt").addClass("zoomIn1");
				$(".plantTxt, .animalTxt").hide(0);
				$(".plantTxt").delay(3000).fadeIn(1000);
				$(".animalTxt").delay(6000).fadeIn(1000);
                break;
			case 1:
			$(".ptSec").addClass("bouncein");
			break;
			case 2:
				$(".p3_1").delay(2000).animate({"opacity":"1"},100);
					$(".p3_2").delay(3000).animate({"opacity":"1"},100);
						$(".p3_3").delay(4000).animate({"opacity":"1"},100);
			break;
			case 3:
                $(".p3_1").delay(2000).animate({"opacity":"1"},100);
                $(".p3_2").delay(3000).animate({"opacity":"1"},100);
                $(".p3_3").delay(4000).animate({"opacity":"1"},100);
                $(".p3_4").delay(5000).animate({"opacity":"1"},100);
			break;
			case 4:
			$(".p3_1_1").delay(2000).animate({"opacity":"1"},100);
				$(".p3_4_1").delay(3000).animate({"opacity":"1"},100);
					$(".p3_5").delay(4000).animate({"opacity":"1"},100);
						$(".p3_2_1").delay(5000).animate({"opacity":"1"},100);
							$(".p3_3_1").delay(6000).animate({"opacity":"1"},100);
			break;
			case 5:
				$(".p3_1_6").delay(2000).animate({"opacity":"1"},100);
					$(".p3_2_6").delay(3000).animate({"opacity":"1"},100);
					$(".p3_3_6").delay(4000).animate({"opacity":"1"},100);
			break;
			case 6:
				$(".p3_1").delay(2000).animate({"opacity":"1"},100);
					$(".p3_2sev").delay(3000).animate({"opacity":"1"},100);
						$(".p3_3").delay(4000).animate({"opacity":"1"},100);
			break;
		}
	}
	function sound_player(sound_id, navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].multipleimagecontainer[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].multipleimagecontainer[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

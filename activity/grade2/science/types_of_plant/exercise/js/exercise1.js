var imgpath = $ref+"/exercise/images/";

var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_exe = new buzz.sound((soundAsset + "ex_ins.ogg"));
var sound_cor = new buzz.sound((soundAsset + "ex_cor.ogg"));


var content=[

	//ex1
	{
		contentblockadditionalclass: 'bg_main',
		extratextblock:[{
			textdata: data.string.questionone,
			textclass: 'topone'
		}],
		exerciseblock: [
			{
				quesblock: true,
				quesblockadditionalclass: 'come_up',
				textdata: data.string.exques1,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					}],
				imageblockadditionalclass: '',
				imageblock : [
					{
						imgclass:'black_img layer_down',
						imgsrc: imgpath + "blank.png",
					},
					{
						imgclass:'plant tree position_1',
						imgsrc: imgpath + "tree-1.png",
					},
					{
						imgclass:'plant shrub position_2',
						imgsrc: imgpath + "shrub-1.png",
					},
					{
						imgclass:'plant herb position_3',
						imgsrc: imgpath + "herb-1.png",
					},
					{
						imgclass:'plant tree position_4',
						imgsrc: imgpath + "tree-2.png",
					},
					{
						imgclass:'plant shrub position_5',
						imgsrc: imgpath + "shrub-2.png",
					},
					{
						imgclass:'plant herb position_6',
						imgsrc: imgpath + "herb-2.png",
					},
					{
						imgclass:'plant tree position_7',
						imgsrc: imgpath + "tree-3.png",
					},
					{
						imgclass:'plant shrub position_8',
						imgsrc: imgpath + "shrub-3.png",
					},
					{
						imgclass:'plant herb position_9',
						imgsrc: imgpath + "herb-3.png",
					},
					{
						imgclass:'plant tree position_10',
						imgsrc: imgpath + "tree-4.png",
					}
				]

			}
		]
	}
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn  = $('#activity-page-next-btn-enabled');
	var $prevBtn  = $('#activity-page-prev-btn-enabled');
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;
	/*var $total_page = content.length;*/

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	//eggTemplate.eggMove(countNext);
	testin.init(10);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);
		if(countNext==0){
			soundplayer(sound_exe);
		}
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }


		/*======= SCOREBOARD SECTION ==============*/

		testin.numberOfQuestions();


		/*random scoreboard eggs*/
		var ansClicked = false;
		var wrngClicked = false;
		var $currentPlant;
		var correct_answer;
		$('.plant').addClass('blink');

		$('.plant').click(function(){
			setTimeout(()=>soundplayer(sound_cor),500);
			$('.quesblock').show(0);
			$('.black_img').show(0);
			$('.plant').css('pointer-events', 'none');
			$(this).removeClass('zoom_out');
			$(this).addClass('zoom_in');
			$currentPlant = $(this);
		});

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
			if($currentPlant.hasClass('tree')){
				correct_answer = 'class1';
			} else if($currentPlant.hasClass('shrub')){
				correct_answer = 'class2';
			} else if($currentPlant.hasClass('herb')){
				correct_answer = 'class3';
			}
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass(correct_answer)){
						if(wrngClicked == false){
	 						testin.update(true);
							score++;
						}
						play_correct_incorrect_sound(1);
						$(this).addClass("correct_ans");
						$(this).siblings(".corctopt").show(0);
						$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						$(this).addClass("incorrect_ans");
						$(this).siblings(".wrngopt").show(0);
						wrngClicked = true;
						play_correct_incorrect_sound(0);
					}
				}
			});
		$nextBtn.click(function(){
			/*generate question no at the beginning of question*/
			testin.numberOfQuestions();
			countNext++;
			$nextBtn.hide(0);
			$('.plant').removeClass('blink');
			testin.gotoNext();
			ansClicked = false;
			wrngClicked = false;
			if (countNext == 10){

			}else {
				$currentPlant.removeClass('zoom_in');
				$currentPlant.removeClass('plant');
				$currentPlant.addClass('zoom_out');
				$currentPlant.css('pointer-events', 'none');
				$('.quesblock').removeClass('come_up');
				$('.quesblock').addClass('come_down');
				$('.black_img').removeClass('layer_down');
				$('.black_img').addClass('layer_up');
				setTimeout(function(){
					$('.plant').css('pointer-events', 'all');
					$('.buttonsel').removeClass("correct_ans");
					$('.buttonsel').removeClass("incorrect_ans");
					$('.buttonsel').siblings(".corctopt").hide(0);
					$('.buttonsel').siblings(".wrngopt").hide(0);
					$('.buttonsel').addClass('forhover forhoverimg');
					$('.quesblock').hide(0);
					$('.quesblock').removeClass('come_down');
					$('.quesblock').addClass('come_up');
					$('.black_img').hide(0);
					$('.black_img').removeClass('layer_up');
					$('.black_img').addClass('layer_down');
					$('#egg' + countNext).addClass('eggmove');
					$('.plant').addClass('blink');
					ansClicked = false;
				}, 2000);
			}

		});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

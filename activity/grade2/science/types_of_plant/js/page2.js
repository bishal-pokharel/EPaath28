var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound(soundAsset + "p2_s1.ogg");
var sound_2 = new buzz.sound(soundAsset + "congrats.ogg");

var content = [

//slide0

{
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "",

	// uppertextblock : [{
	// 	textdata : data.string.p1text2,
	// 	textclass : 'para_diy'
	// }],

	imageblock : [{
		imagestoshow : [
		{
			imgclass : "squirrel",
			imgsrc : imgpath + "lokharke.png",
		}],
		imagelabels: [{
			imagelabelclass : "font_diy",
			imagelabeldata : data.string.p1text1
		}]
	}]
},

//slide1
{
	contentblocknocenteradjust : true,
	uppertextblockadditionalclass : 'new_header',
	uppertextblock : [{
		textclass : "header_big",
		textdata : data.string.p9text1,
		datahighlightflag : true,
		datahighlightcustomclass : 'bold_green'
	}, {
		textclass : "header_instruction",
		textdata : data.string.p9text2,
		datahighlightflag : true,
		datahighlightcustomclass : 'bold_green'
	}],

	imageblockadditionalclass : '',
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "tree_wrong",
			imgsrc : imgpath + "wrong.png",
		},
		{
			imgclass : "shrub_wrong",
			imgsrc : imgpath + "wrong.png",
		},
		{
			imgclass : "herb_wrong",
			imgsrc : imgpath + "wrong.png",
		}],
		imagelabels : [,
		]
	}],
	draggableblock: [
		{
			draggablecard: 'drag_0 tree',
			imgclass : "card_draggable tree plant0",
			imgsrc : imgpath + "pine_tree.png",
			imagelabelclass : "tag_label horizontal_center plant_label_0",
			imagelabeldata : data.string.pttext1
		},
		{
			draggablecard: 'drag_1 tree',
			imgclass : "card_draggable tree plant1",
			imgsrc : imgpath + "peepal_tree.png",
			imagelabelclass : "tag_label horizontal_center plant_label_1",
			imagelabeldata : data.string.pttext2
		},
		{
			draggablecard: 'drag_2  tree',
			imgclass : "card_draggable tree plant2",
			imgsrc : imgpath + "mango_tree.png",
			imagelabelclass : "tag_label horizontal_center plant_label_2",
			imagelabeldata : data.string.pttext3
		},
		{
			draggablecard: 'drag_3 tree',
			imgclass : "card_draggable tree plant3",
			imgsrc : imgpath + "banyan_new.png",
			imagelabelclass : "tag_label horizontal_center plant_label_3",
			imagelabeldata : data.string.pttext4
		},
		{
			draggablecard: 'drag_4 shrub',
			imgclass : "card_draggable shrub plant4",
			imgsrc : imgpath + "rose.png",
			imagelabelclass : "tag_label horizontal_center plant_label_4",
			imagelabeldata : data.string.pstext1
		},
		{
			draggablecard: 'drag_5 shrub',
			imgclass : "card_draggable shrub plant5",
			imgsrc : imgpath + "coffee-plant01.png",
			imagelabelclass : "tag_label horizontal_center plant_label_5",
			imagelabeldata : data.string.pstext2
		},
		{
			draggablecard: 'drag_6 shrub',
			imgclass : "card_draggable shrub plant6",
			imgsrc : imgpath + "hibiscus.png",
			imagelabelclass : "tag_label horizontal_center plant_label_6",
			imagelabeldata : data.string.pstext3
		},
		{
			draggablecard: 'drag_7 shrub',
			imgclass : "card_draggable shrub plant7",
			imgsrc : imgpath + "timur-plant.png",
			imagelabelclass : "tag_label horizontal_center plant_label_7",
			imagelabeldata : data.string.pstext4
		},
		{
			draggablecard: 'drag_8 herb',
			imgclass : "card_draggable herb plant8",
			imgsrc : imgpath + "mint.png",
			imagelabelclass : "tag_label horizontal_center plant_label_8",
			imagelabeldata : data.string.phtext1
		},
		{
			draggablecard: 'drag_9 herb',
			imgclass : "card_draggable herb plant9",
			imgsrc : imgpath + "marigold.png",
			imagelabelclass : "tag_label horizontal_center plant_label_9",
			imagelabeldata : data.string.phtext2
		},
		{
			draggablecard: 'drag_10 herb',
			imgclass : "card_draggable herb plant10",
			imgsrc : imgpath + "coriander.png",
			imagelabelclass : "tag_label horizontal_center plant_label_10",
			imagelabeldata : data.string.phtext3
		},
		{
			draggablecard: 'drag_11 herb',
			imgclass : "card_draggable herb plant11",
			imgsrc : imgpath + "basil.png",
			imagelabelclass : "tag_label horizontal_center plant_label_11",
			imagelabeldata : data.string.phtext4
		}
	],

	droppableblockadditionalclass : '',
	droppableblock : [{
		imagestoshow : [{
			droppableclass:"shrub_droppable",
			imgclass : "glow",
			imgsrc : imgpath + "cart.png",

		}, {
			droppableclass:"herb_droppable",
			imgclass : "glow",
			imgsrc : imgpath + "cart.png",

		}, {
			droppableclass:"tree_droppable",
			imgclass : "glow",
			imgsrc : imgpath + "cart.png",

		}],
		imagelabels : [
		{
			imagelabelclass : "cart_label cart_label_left",
			imagelabeldata : data.string.psharedtext1
		},
		{
			imagelabelclass : "cart_label cart_label_center",
			imagelabeldata : data.string.psharedtext2
		},
		{
			imagelabelclass : "cart_label cart_label_right",
			imagelabeldata : data.string.psharedtext3
		}]
	}],

	extratextblock:[{
		textdata : data.string.p9text3,
		textclass : 'congrat_text_plant'
	},{
		textdata: '',
		textclass: 'header_yellow'
	}
	],


	lowertextblockadditionalclass : "green_bottom_line",
/*
	this block is empty but shows the green line at bottom
*/
	lowertextblock : [{

	}],
},

];

$(function() {

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;

	/* var to count the number of plant displayed */
	var plant_no = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/* store numbers from 0 to 11 in random in an array using the function*/
	var random_array = sandy_random_display(12);

	loadTimelineProgress($total_page, countNext + 1);

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if (countNext == 1 ) {
					$nextBtn.hide(0);
					$prevBtn.show(0);
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
			play_diy_audio();
			break;
		case 1:

				play_sound(sound_1);
				/* To display the first random plant */
				plant_count = random_array[plant_no] - 1;
	         	var temp_name = $('.drag_' + plant_count);
	         	temp_name.show(0);
	         	plant_no++;

				var hide_tree_tag = 0;
				var hide_herb_tag = 0;
				var hide_shrub_tag = 0;
				var is_last_plant = false;
				$( '.draggablecard' ).draggable({
                	containment: "body",
	         		cursor: "move",
	                revert: "invalid",
	                appendTo: "body",
	                helper: "clone",
			        start: function( event, ui ){
			        	$(this).css({
	                        "opacity": "0",
	                        "z-index": '500'
	                    });
	                    var $new_class_clone = $(this);
	                    ui.helper.css({
	           	 			// 'width': $new_class_clone.width() + 'px',
	           	 			// 'height': $new_class_clone.height() + 'px'
	           	 		});
	           	 		$('.tree_wrong').hide(0);
	           	 		$('.shrub_wrong').hide(0);
	           	 		$('.herb_wrong').hide(0);
	           	 		$('.plant_label_' + plant_count).hide(0);
						vocabcontroller.findwords(countNext);
	         		},
	         		stop: function( event, ui ){
	           	 		$(this).css({
	                        "opacity": "1",
	                    });
	           	 		$('.plant_label_' + plant_count).show(0);
	         		}
	      		});

	      		$( '.tree_droppable' ).droppable({
	      			over: function (event, ui) {
	               		$( this ).addClass( "ui_highlight" );
	               		$('.cart_label_right').addClass( "label_highlight" );
	               	},
	               	out: function (event, ui) {
	               		$( this ).removeClass( "ui_highlight" );
	               		$('.cart_label_right').removeClass( "label_highlight" );
	               	},
		            drop:function(event, ui) {
						if (ui.draggable.hasClass("tree")){
							sandydroppablefunction('.tree_droppable', ui.draggable, '.tree', 'right');
							plant_count = random_array[plant_no] -1;
							plant_no++;
					     	var temp_name = $('.drag_' + plant_count);
					     	temp_name.show(0);
					     	play_correct_incorrect_sound(1);
						}else{
							$( ".tree_wrong" ).show(0);
							$( this ).removeClass( "ui_highlight" );
							$('.cart_label_right').removeClass( "label_highlight" );
					     	play_correct_incorrect_sound(0);
						}
					}
	         	});

	         	$( '.shrub_droppable' ).droppable({
	      			over: function (event, ui) {
	               		$( this ).addClass( "ui_highlight" );
	               		$('.cart_label_center').addClass( "label_highlight" );
	               	},
	               	out: function (event, ui) {
	               		$( this ).removeClass( "ui_highlight" );
	               		$('.cart_label_center').removeClass( "label_highlight" );
	               	},
		            drop:function(event, ui) {
		            	if (ui.draggable.hasClass("shrub")){

							sandydroppablefunction('.shrub_droppable', ui.draggable, '.shrub', 'center');
							plant_count = random_array[plant_no] -1;
							plant_no++;
							var temp_name = $('.drag_' + plant_count);
							temp_name.show(0);
							play_correct_incorrect_sound(1);

					   }else{
					   		$( this ).removeClass( "ui_highlight" );
					   		$('.cart_label_center').removeClass( "label_highlight" );
					   		$( ".shrub_wrong" ).show(0);
								play_correct_incorrect_sound(0);

					   }
					}
	         	});

	         	$( '.herb_droppable' ).droppable({
	      			over: function (event, ui) {
	               		$( this ).addClass( "ui_highlight" );
	               		$('.cart_label_left').addClass( "label_highlight" );
	               	},
	               	out: function (event, ui) {
	               		$( this ).removeClass( "ui_highlight" );
	               		$('.cart_label_left').removeClass( "label_highlight" );
	               	},
		            drop:function(event, ui) {
		            	if (ui.draggable.hasClass("herb")){
							sandydroppablefunction('.herb_droppable', ui.draggable, '.herb', 'left' );
							plant_count = random_array[plant_no] - 1;
							plant_no++;
							var temp_name = $('.drag_' + plant_count);
							temp_name.show(0);
							play_correct_incorrect_sound(1);

					   }else{
					   		$( this ).removeClass( "ui_highlight" );
					   		$( ".herb_wrong" ).show(0);
					   		$('.cart_label_left').removeClass( "label_highlight" );
								play_correct_incorrect_sound(0);

					   }
					}

	         	});
			break;
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/

	}

	function play_sound(new_sound_data){
		var current_sound = new_sound_data;
		current_sound.stop();
		current_sound = new_sound_data;
		current_sound.play();
	}


	$nextBtn.on("click", function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

	/*
 	droppable_name is the cart in which it is dropped ( should have '.')
 	dropped is ui.draggable
 	dropped_item is type_of_plant and is class i.e. with '.'
 	location refers to cart location- left or right
	 */
	function sandydroppablefunction( droppable_name, dropped , dropped_item, location){
		dropped.draggable('disable');
		var my_location = '.cart_label_' + location;
		var cart_location = $(my_location);
		var droppedOn = $(droppable_name);

		/* Decide the position of dropped item in cart using the modulo operator
		   and no. of items currently in cart to calculate proper position */
		// var diff_h = -5+((droppedOn.find(dropped_item).length%2)+1)*5;
		var diff_l = ((droppedOn.find(dropped_item).length%4)+1)*20;
		droppedOn.removeClass( "ui_highlight" );
		cart_location.removeClass( "label_highlight" );
		$(dropped.children("img")).detach().css({
			"position" : "absolute",
			"left" : diff_l + "%",
			"top" :  "14%",
			"z-index" : "-10",
			"width": '30%',
			'transform' : 'translate(-50%, 0%)',
			'-moz-transform' : 'translate(-50%, 0%)',
			'-webkit-transform' : 'translate(-50%, 0%)',
		}).appendTo(droppedOn);
		dropped.hide(0);
		var plant_name = $('.plant'+plant_count);
		plant_name.removeClass('card_draggable');
		$('.plant_label_'+plant_count).hide(0);
		plant_name.css({
			'border': '0px',
			'z-index' : '-160',
    		'box-shadow' : '0px 0px 0px #888888'
		});
		/* if all plants are displayed move to next page */
		if( plant_no > 11){
			$('.congrat_text_plant').delay(2000).fadeIn(500);
			setTimeout(function(){
				ole.footerNotificationHandler.lessonEndSetNotification();
				play_sound(sound_2);
			},1200);
		}
	}

	/* function used to generate numbers from 1 to range in random order and store it in array */
	function sandy_random_display(range){
		var random_arr = [];
		var return_arr = [];
		var random_max = range;
		for(var i=0; i<range; i++){
			random_arr.push(i+1);
		}

		while(random_max)
		{
			var index = ole.getRandom(1, random_max-1, 0);
			return_arr.push(random_arr[index]);
			random_arr.splice((index),1);
			random_max--;
		}
		return return_arr;
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var current_sound = sound_1;


var content = [

//slide0

{
	hasheaderblock : false,
	contentblocknocenteradjust : true,

	contentblockadditionalclass : 'backgroundss',
	uppertextblock : [{
		textdata : data.lesson.chapter,
		textclass : 'lesson-title vertical-horizontal-center'
	}]
},

//slide1
{
	contentblockadditionalclass: 'main_bg',
	contentblocknocenteradjust : true,
	headerblockadditionalclass : 'header_transparent',
	headerblock : [{
		textclass : "header_normal header_laila fade_in",
		textdata : data.string.p2text1
	},
	{
		textclass : "header_normal fade_in its_hidden text_0",
		textdata : data.string.p2text2
	}],
	uppertextblockadditionalclass : "para_pg_1 front_div",

	uppertextblock : [{
		textdata : '<br> </br>',
		textclass : 'para_yellow' 
	},{
		textdata : data.string.p2text3,
		textclass : 'font_purple fade_in its_hidden text_1' 
	}, {
		textdata : data.string.p2text4,
		textclass : 'font_purple fade_in its_hidden text_2' 
	}, {
		textdata : data.string.p2text5,
		textclass : 'font_purple fade_in its_hidden text_3' 
	}]
},

//slide2
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark_fade',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "sidepara_1 front_div",

	uppertextblock : [{
		textdata : data.string.p3text1,
		textclass : 'para_yellow fade_in its_hidden text_2'
	}, {
		textdata : data.string.p3text2,
		textclass : 'para_yellow fade_in its_hidden text_3'
	}, {
		textdata : data.string.p3text3,
		textclass : 'para_yellow fade_in its_hidden text_4'
	}, {
		textdata : data.string.p3text4,
		textclass : 'para_yellow fade_in its_hidden text_5'
	}, {
		textdata : data.string.p3text5,
		textclass : 'para_yellow fade_in its_hidden text_6',
		datahighlightflag : true,
		datahighlightcustomclass : 'item_highlight'
	}],
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "oak_tree light_animation its_hidden text_0",
			imgsrc : imgpath + "oak-tree.png",
		}]
	}]
},


//slide3
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "",

	uppertextblock : [{
		textdata : data.string.p4text1,
		textclass : 'example_header zoomIn'
	}],
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "oak_tree",
			imgsrc : imgpath + "oak-tree.png",
		},
		{
			imgclass : "example_image_block example_image_0 fade_in its_hidden fade_0",
			imgsrc : imgpath + "mango_tree.png",
		},
		{
			imgclass : "example_image_block example_image_1 fade_in its_hidden fade_1",
			imgsrc : imgpath + "banyan_new.png",
		},
		{
			imgclass : "example_image_block example_image_2 fade_in its_hidden fade_2",
			imgsrc : imgpath + "pine_tree.png",
		},
		],
		imagelabels : [
		{
			imagelabelclass : "label_yellow example_label_0 fade_in its_hidden fade_0",
			imagelabeldata : data.string.p4text2
		},
		{
			imagelabelclass : "label_yellow example_label_1 fade_in its_hidden fade_1",
			imagelabeldata : data.string.p4text3
		},
		{
			imagelabelclass : "label_yellow example_label_2 fade_in its_hidden fade_2",
			imagelabeldata : data.string.p4text4
		}]
	}],
},


//slide4
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark_fade',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : data.string.p5text1,
		textclass : 'para_yellow fade_in its_hidden text_1'
	}, {
		textdata : data.string.p5text2,
		textclass : 'para_yellow fade_in its_hidden text_2'
	}, {
		textdata : data.string.p5text3,
		textclass : 'para_yellow fade_in its_hidden text_3',
		datahighlightflag : true,
		datahighlightcustomclass : 'item_highlight'
	}],
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "its_herb light_animation its_hidden text_0",
			imgsrc : imgpath + "grass_flower.png",
		}]
	}]
},


//slide5
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "",

	uppertextblock : [{
		textdata : data.string.p6text1,
		textclass : 'example_header zoomIn'
	}],
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "its_herb",
			imgsrc : imgpath + "grass_flower.png",
		},
		{
			imgclass : "example_image_block example_image_0 fade_in its_hidden fade_0",
			imgsrc : imgpath + "coriander.png",
		},
		{
			imgclass : "example_image_block example_image_1 fade_in its_hidden fade_1",
			imgsrc : imgpath + "wheat.png",
		},
		{
			imgclass : "example_image_block example_image_2 fade_in its_hidden fade_2",
			imgsrc : imgpath + "marigold.png",
		},
		],
		imagelabels : [
		{
			imagelabelclass : "label_yellow example_label_0 fade_in its_hidden fade_0",
			imagelabeldata : data.string.p6text2
		},
		{
			imagelabelclass : "label_yellow example_label_1 fade_in its_hidden fade_1",
			imagelabeldata : data.string.p6text3
		},
		{
			imagelabelclass : "label_yellow example_label_2 fade_in its_hidden fade_2",
			imagelabeldata : data.string.p6text4
		}]
	}],
},

//slide6
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark_fade',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "sidepara",

	uppertextblock : [{
		textdata : data.string.p7text1,
		textclass : 'para_yellow fade_in its_hidden text_1'
	}, {
		textdata : data.string.p7text2,
		textclass : 'para_yellow fade_in its_hidden text_2'
	}, {
		textdata : data.string.p7text3,
		textclass : 'para_yellow fade_in its_hidden text_3',
		datahighlightflag : true,
		datahighlightcustomclass : 'item_highlight'
	}],
	
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "its_shrub light_animation its_hidden text_0",
			imgsrc : imgpath + "flower-plant.png",
		}]
	}]
},


//slide7
{
	contentblockadditionalclass: 'main_bg',
	coverboardadditionalclass:'bg_dark',
	contentblocknocenteradjust : true,

	uppertextblockadditionalclass : "",

	uppertextblock : [{
		textdata : data.string.p8text1,
		textclass : 'example_header fade_in zoomIn'
	}],
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "its_shrub",
			imgsrc : imgpath + "flower-plant.png",
		},
		{
			imgclass : "example_image_block example_image_0 fade_in its_hidden fade_0",
			imgsrc : imgpath + "hibiscus.png",
		},
		{
			imgclass : "example_image_block example_image_1 fade_in its_hidden fade_1",
			imgsrc : imgpath + "rose.png",
		},
		{
			imgclass : "example_image_block example_image_2 fade_in its_hidden fade_2",
			imgsrc : imgpath + "lemon.png",
		},
		],
		imagelabels : [
		{
			imagelabelclass : "label_yellow example_label_0 fade_in its_hidden fade_0",
			imagelabeldata : data.string.p8text2
		},
		{
			imagelabelclass : "label_yellow example_label_1 fade_in its_hidden fade_1",
			imagelabeldata : data.string.p8text3
		},
		{
			imagelabelclass : "label_yellow example_label_2 fade_in its_hidden fade_2",
			imagelabeldata : data.string.p8text4
		}]
	}],
},

//slide8
{
	contentblockadditionalclass: 'bg_blue',
	contentblocknocenteradjust : true,
	
	headerblockadditionalclass : 'header_transparent',
	headerblock : [{
		textclass : "header_normal header_orange bold_header",
		textdata : data.string.p2text1
	}],
	uppertextblockadditionalclass : "",

	uppertextblock : [{
		textdata : data.string.psharedtext3,
		textclass : 'table_header table_0 its_hidden fade_1'
	},
	{
		textdata : data.string.psharedtext2,
		textclass : 'table_header table_1 its_hidden fade_4'
	},
	{
		textdata : data.string.psharedtext1,
		textclass : 'table_header table_2 its_hidden fade_8'
	}],
	newimageblock : [{
		imagestoshow : [
		{
			imgclass : "random_image_block random_image_0",
			imgsrc : imgpath + "pine_tree.png",
		},
		{
			imgclass : "random_image_block random_image_1",
			imgsrc : imgpath + "mango_tree.png",
		},
		{
			imgclass : "random_image_block random_image_2",
			imgsrc : imgpath + "banyan_new.png",
		},
		{
			imgclass : "random_image_block random_image_3",
			imgsrc : imgpath + "hibiscus.png",
		},
		{
			imgclass : "random_image_block random_image_4",
			imgsrc : imgpath + "rose.png",
		},
		{
			imgclass : "random_image_block random_image_5",
			imgsrc : imgpath + "lemon.png",
		},
		{
		
			imgclass : "random_image_block random_image_6",
			imgsrc : imgpath + "wheat.png",
		},
		{
			imgclass : "random_image_block random_image_7",
			imgsrc : imgpath + "marigold.png",
		},
		{
			imgclass : "random_image_block random_image_8",
			imgsrc : imgpath + "coriander.png",
		},],
	}],
},
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var timeoutvar = null;
	var timeoutvar2 = null;
		
	loadTimelineProgress($total_page, countNext + 1);
	
	
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if (countNext > 1) {
			$nextBtn.hide(0);
			$prevBtn.show(0);
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
        $nextBtn.hide(0);
        switch (countNext) {
		case 0:
			sound_player(sound_1);
			break;
		case 1:
			$nextBtn.hide(0);
			sound_player(sound_2);
			$('.text_0').delay(5500).show(0);
			$('.text_1').delay(11500).show(0);
			$('.text_2').delay(15000).show(0);
			$('.text_3').delay(18500).show(0);
			break;
		case 2:
			timeoutvar2 = setTimeout(function() {
				sound_player(sound_3);
			}, 2000);
			$('.text_0').show(0);
			$('.text_2').delay(2000).show(0);
			$('.text_3').delay(6000).show(0);
			$('.text_4').delay(10500).show(0);
			$('.text_5').delay(14500).show(0);
			$('.text_6').delay(18600).show(0);
			break;
		case 3:
			sound_player(sound_4);
			$('.fade_0').delay(3000).show(0);
			$('.fade_1').delay(5000).show(0);
			$('.fade_2').delay(7000).show(0);
			break;
		case 4:
			timeoutvar2 = setTimeout(function() {
				sound_player(sound_5);
			}, 2000);
			$('.text_0').show(0);
			$('.text_1').delay(2000).show(0);
			$('.text_2').delay(4800).show(0);
			$('.text_3').delay(10500).show(0);
			break;
		case 5:
			sound_player(sound_6);
			$('.fade_0').delay(2800).show(0);
			$('.fade_1').delay(4800).show(0);
			$('.fade_2').delay(6500).show(0);
			break;
		case 6:
			timeoutvar2 = setTimeout(function() {
				sound_player(sound_7);
			}, 2000);
			$('.text_0').show(0);
			$('.text_1').delay(2000).show(0);
			$('.text_2').delay(9000).show(0);
			$('.text_3').delay(13500).show(0);
			break;
		case 7:
			sound_player(sound_8);
			$('.fade_0').delay(3000).show(0);
			$('.fade_1').delay(4900).show(0);
			$('.fade_2').delay(6500).show(0);
			break;
		case 8:
			random_fade_caller('.fade_', 10);
			timeoutvar2 = setTimeout(function() {
				sound_player(sound_9);
			}, 12000);
			break;
		default:
			break;
		}
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
        current_sound.bind('ended', function() {
            countNext==8?ole.footerNotificationHandler.pageEndSetNotification():$nextBtn.show(0);
        });
	}

	$nextBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
	function sandy_fade_caller(fade_class, count){
		var time_delay = 0;
		for(var i = 0; i<count; i++){
			time_delay += 4000;
			var class_name = $(fade_class+i);
    		class_name.delay(time_delay).show(0);
		}
	}
	
	function random_fade_caller(fade_class, count){
		var time_delay = 0;
		for(var i = 0; i<count; i++){
			time_delay += 1000;
			var class_name = $(fade_class+i);
    		class_name.delay(time_delay).show(0);
		}
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

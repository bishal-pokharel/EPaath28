var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var sound_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p2_s10.ogg"));


var s_1 = new buzz.sound((soundAsset + "nails.ogg"));
var s_2 = new buzz.sound((soundAsset + "nose.ogg"));
var s_3 = new buzz.sound((soundAsset + "ear.ogg"));
var s_4 = new buzz.sound((soundAsset + "hands.ogg"));
var s_5 = new buzz.sound((soundAsset + "foot.ogg"));
var s_6 = new buzz.sound((soundAsset + "teeth.ogg"));
var s_7 = new buzz.sound((soundAsset + "hair.ogg"));
var s_8 = new buzz.sound((soundAsset + "eye.ogg"));

var sound_arr_var = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11];
var sound_arr_var2 = [s_1, s_2, s_3, s_4, s_5, s_6, s_7, s_8];


var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblockadditionalclass: 'center-text-2',
		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'my_font_ultra_big pangolin'
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text2,
				textclass : 'my_font_very_big top-title sniglet'
			}
		],
		
		imagetextblock: [{
			imagediv: 'side_description side_description_1',
			imgclass: '',
			imgsrc: imgpath+'soap.png',
			texts:[
					{
						textdata : data.string.p2text4,
						textclass : 'my_font_medium happymonkey show2'
					},
					{
						textdata : data.string.p2text5,
						textclass : 'my_font_medium happymonkey show3'
					}
				]
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_left_1',
					imgsrc: imgpath + "hair01.png",
				},
				{
					imgclass: 'image_left_2',
					imgsrc: imgpath + "hair02.png",
				}
			]
		}],
		
		lowertextblockadditionalclass: 'bottom_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text3,
				textclass : 'my_font_medium sniglet show1'
			}
		],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text6,
				textclass : 'my_font_very_big top-title sniglet'
			}
		],
		
		imagetextblock: [{
			imagediv: 'side_description side_description_2',
			imgclass: '',
			imgsrc: imgpath+'tooth.png',
			texts:[
					{
						textdata : data.string.p2text9,
						textclass : 'my_font_medium happymonkey show3'
					},
				]
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_left_1_1',
					imgsrc: imgpath + "teeth01.png",
				},
				{
					imgclass: 'image_left_2_1',
					imgsrc: imgpath + "teeth02.png",
				}
			]
		}],
		
		lowertextblockadditionalclass: 'bottom_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text7 ,
				textclass : 'my_font_medium sniglet show1'
			},
			{
				textdata : data.string.p2text8,
				textclass : 'my_font_medium sniglet show2'
			}
		],
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text10,
				textclass : 'my_font_very_big top-title sniglet'
			}
		],
		
		imagetextblock: [{
			imagediv: 'side_description side_description_2',
			imgclass: '',
			imgsrc: imgpath+'nailcutter.png',
			texts:[
					{
						textdata : data.string.p2text13,
						textclass : 'my_font_medium happymonkey show3'
					},
				]
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_center',
					imgsrc: imgpath + "nail.png",
				}
			]
		}],
		
		lowertextblockadditionalclass: 'bottom_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text11,
				textclass : 'my_font_medium sniglet show1'
			},
			{
				textdata : data.string.p2text12,
				textclass : 'my_font_medium sniglet show2'
			}
		],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text14,
				textclass : 'my_font_very_big top-title sniglet'
			},
			{
				textdata : '',
				textclass : 'wrong_div'
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_right_1',
					imgsrc: imgpath + "eye02.png",
				},
				{
					imgclass: 'image_right_2',
					imgsrc: imgpath + "eye01.png",
				},
                {
                    imgclass: 'wrongimg',
                    imgsrc: "images/wrong_01.png",
                }
			]
		}],
		
		lowertextblockadditionalclass: 'left_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text15,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text16,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text17,
				textclass : 'my_font_medium sniglet'
			}
		],
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text18,
				textclass : 'my_font_very_big top-title sniglet'
			},
			{
				textdata : '',
				textclass : 'wrong_div'
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_right_1_ear',
					imgsrc: imgpath + "ears02.png",
				},
				{
					imgclass: 'image_right_2',
					imgsrc: imgpath + "ears01.png",
				},
                {
                    imgclass: 'wrongimg',
                    imgsrc: "images/wrong_01.png",
                }
			]
		}],
		
		lowertextblockadditionalclass: 'left_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text19,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text20,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text21,
				textclass : 'my_font_medium sniglet'
			}
		],
	},
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text22,
				textclass : 'my_font_very_big top-title sniglet'
			},
			{
				textdata : '',
				textclass : 'wrong_div'
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_right_1_nose',
					imgsrc: imgpath + "nose02.png",
				},
				{
					imgclass: 'image_right_2',
					imgsrc: imgpath + "nose01.png",
				},
                {
                    imgclass: 'wrongimg',
                    imgsrc: "images/wrong_01.png",
                }
			]
		}],
		
		lowertextblockadditionalclass: 'left_text_block',
		lowertextblock : [
			{
				textdata : data.string.p2text23,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text24,
				textclass : 'my_font_medium sniglet'
			},
			{
				textdata : data.string.p2text25,
				textclass : 'my_font_medium sniglet'
			}
		],
	},
	
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text26,
				textclass : 'my_font_very_big top-title sniglet'
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_top_1',
					imgsrc: imgpath + "washing-hand.png",
				}
			]
		}],
		imagetextblock: [{
			imagediv: 'side_description_yellow side_description_yellow_1',
			imgclass: '',
			imgsrc: imgpath+'soap.png',
			texts:[
					{
						textdata : data.string.p2text29,
						textclass : 'my_font_medium happymonkey show3'
					}
				]
			}
		],
		
		lowertextblockadditionalclass: 'bottom_text_block_1',
		lowertextblock : [
			{
				textdata : data.string.p2text27,
				textclass : 'my_font_medium sniglet show1'
			},
			{
				textdata : data.string.p2text28,
				textclass : 'my_font_medium sniglet show2'
			}
		],
	},
	
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		extratextblock : [
			{
				textdata : data.string.p2text30,
				textclass : 'my_font_very_big top-title sniglet'
			}
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'image_top_1',
					imgsrc: imgpath + "washing-feet.png",
				}
			]
		}],
		imagetextblock: [{
			imagediv: 'side_description_yellow side_description_yellow_2',
			imgclass: '',
			imgsrc: imgpath+'soap-with-water.png',
			texts:[
					{
						textdata : data.string.p2text33,
						textclass : 'my_font_medium happymonkey show3'
					}
				]
			}
		],
		
		lowertextblockadditionalclass: 'bottom_text_block_1',
		lowertextblock : [
			{
				textdata : data.string.p2text31,
				textclass : 'my_font_medium sniglet show1'
			},
			{
				textdata : data.string.p2text32,
				textclass : 'my_font_medium sniglet show2'
			}
		],
	},
	
	
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		extratextblock : [{
			textdata : data.string.p2text34,
			textclass : 'my_font_big title-text happymonkey'
		}],
		
		dropblock : [{
			dropclass : 'drop-box box-1',
			arrowclass : 'arrow arrow-1',
			textdata : data.string.p1text3,
			textclass : 'item-body item-1 my_font_big'
		},{
			dropclass : 'drop-box box-2',
			arrowclass : 'arrow arrow-2',
			textdata : data.string.p1text4,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-3',
			arrowclass : 'arrow arrow-3',
			textdata : data.string.p1text5,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-4',
			arrowclass : 'arrow arrow-4',
			textdata : data.string.p1text6,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-5',
			arrowclass : 'arrow arrow-5',
			textdata : data.string.p1text7,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-6',
			arrowclass : 'arrow arrow-6',
			textdata : data.string.p1text8,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-7',
			arrowclass : 'arrow arrow-7',
			textdata : data.string.p1text9,
			textclass : 'item-body item-2 my_font_big'
		},{
			dropclass : 'drop-box box-8',
			arrowclass : 'arrow arrow-8',
			textdata : data.string.p1text10,
			textclass : 'item-body item-2 my_font_big'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'character-2',
					imgsrc: imgpath + "girl.png",
				}
			]
		}],
		popupblock:[{
			imgclass: '',
			imgsrc: imgpath + "hair01.png",
			headerclass: 'popup_header pangolin my_font_big',
			headerdata: '',
			texts:[]
			
		}]
	},
	
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		extratextblock : [
			{
				textdata : data.string.p2text44,
				textclass : 'my_font_very_big clean-center-text pangolin'
			}
		],
		imagetextblock: [
			{
				imagediv: 'thumbs thumb-7',
				imgclass: '',
				imgsrc: imgpath+'thumnail07.png',
				texts:[
						{
							textdata : data.string.p2text43,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-2',
				imgclass: '',
				imgsrc: imgpath+'thumnail02.png',
				texts:[
						{
							textdata : data.string.p2text42,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-3',
				imgclass: '',
				imgsrc: imgpath+'thumnail03.png',
				texts:[
						{
							textdata : data.string.p2text37,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-4',
				imgclass: '',
				imgsrc: imgpath+'thumnail04.png',
				texts:[
						{
							textdata : data.string.p2text41,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-8',
				imgclass: '',
				imgsrc: imgpath+'thumnail08.png',
				texts:[
						{
							textdata : data.string.p2text39,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-1',
				imgclass: '',
				imgsrc: imgpath+'thumnail01.png',
				texts:[
						{
							textdata : data.string.p2text36,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-6',
				imgclass: '',
				imgsrc: imgpath+'thumnail06.png',
				texts:[
						{
							textdata : data.string.p2text38,
							textclass : 'my_font_small happymonkey'
						}
					]
			},
			{
				imagediv: 'thumbs thumb-5',
				imgclass: '',
				imgsrc: imgpath+'thumnail05.png',
				texts:[
						{
							textdata : data.string.p2text40,
							textclass : 'my_font_small happymonkey'
						}
					]
			}
		],
			
	}
];

$(function() {

	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
    var current_sound = sound_1;
	var $total_page = content.length;
	var timeoutvar = null;
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
    function showtext(delay1,delay2,delay3){
        $(".show1,.show2,.show3").hide();
        $(".show1").delay(delay1).fadeIn();
        $(".show2").delay(delay2).fadeIn();
        $(".show3").delay(delay3).fadeIn();
	}
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);

		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 1:
                sound_nav(sound_arr_var[countNext]);
                showtext(1000,6000,11000);
                break;
			case 2:
                sound_nav(sound_arr_var[countNext]);
                $lang=='en'?showtext(1000,5000,9000):showtext(1000,6000,12000);
				break;
			case 3:
				$(".side_description_2 img").css({"width": "59%","top":"25%"})
                sound_nav(sound_arr_var[countNext]);
                $lang=='en'?showtext(1000,5000,14000):showtext(1000,7000,17000);
                break;
            case 7:
                sound_nav(sound_arr_var[countNext]);
                $lang=='en'?showtext(1000,6000,11000):showtext(1000,6000,13000);
                break;
            case 8:
                sound_nav(sound_arr_var[countNext]);
                $lang=='en'?showtext(1000,6000,9000):showtext(1000,13000,20000);
                break;
			case 9:
				$prevBtn.show(0);
				sound_nav(sound_arr_var[countNext]);
				var pop_obj =[
								[data.string.p1text3, [data.string.pdtext1], 'thumnail05'],
								[data.string.p1text4, [data.string.pdtext2], 'thumnail06'],
								[data.string.p1text5, [data.string.pdtext3], 'thumnail03'],
								[data.string.p1text6, [data.string.pdtext4], 'thumnail07'],
								[data.string.p1text7, [data.string.pdtext5], 'thumnail08'],
								[data.string.p1text8, [data.string.pdtext6], 'thumnail02'],
								[data.string.p1text9, [data.string.pdtext7], 'thumnail01'],
								[data.string.p1text10, [data.string.pdtext8], 'thumnail04'],
							];
				$( ".drop-box" ).each(function(index) {
				    $(this).on("click", function(event){
				    	$(this).addClass("checkall");
				    	if($(".checkall").length>=8){
                            nav_button_controls(200);
                        }
				    	$(this).css('background-color', 'rgb(146,107,169)');
				        popup_window(pop_obj[index][0], pop_obj[index][1], pop_obj[index][2], sound_arr_var2[index], $(this), event);
				    });
				});
				$('.wrong_button').click(function(){
					close_popup_window();
				});
				break;
			default:
				if(countNext!=0) $prevBtn.show(0);
				sound_nav(sound_arr_var[countNext]);
				break;
		}
	}
	
	var current_div_x;
	var current_div_y;
	var popup_is_close =  true;;
	
	function popup_window(header, text_arr, img_src, sounddata, click_class, event){
		if(popup_is_close){
			console.log('before started   '+ popup_is_close);
			popup_is_close = false;
			timeoutvar = setTimeout(function(){
				// console.log('started   '+ popup_is_close);
				$( ".drop-box" ).css('pointer-events', 'none');
				current_div_x = click_class.position().left + click_class.width()/2;
				current_div_y = click_class.position().top + click_class.height()/2;
				$('.popup_hygiene>.hyg_img').attr('src', imgpath+img_src+'.png');
				$('.popup_hygiene>.popup_header').html(header);
				for(i=0; i<text_arr.length; i++){
					$('.popup_hygiene>.popup_text_block').append('<p class="my_font_medium sniglet">'+ text_arr[i] + '</p>');
				}
				$('.popup_hygiene').css({'display': 'block', 'width': "0%", 'height': '0%', 'left':current_div_x, 'top': current_div_y});
				$('.popup_hygiene p, .popup_hygiene img, .wrong_button').hide(0);
				$('.popup_hygiene').animate({
					'width': "68%", 
					'height': '76%', 
					'left':'50%', 
					'top': '50%'
				}, 500, function(){
					sound_player(sounddata);
					$('.popup_hygiene p, .popup_hygiene img, .wrong_button').fadeIn(300);
				});
			}, 100);
		}
	}
	function close_popup_window(){
		current_sound.stop();
		// sound_nav(sound_arr_var[countNext]);
		$(".drop-box").css('background-color', 'rgb(120,121,196)');
		$('.popup_hygiene>.popup_text_block').html('');
		$('.popup_hygiene p, .popup_hygiene img, .wrong_button').fadeOut(300);
		$('.popup_hygiene').fadeOut(500, function(){
			popup_is_close = true;
			console.log('completed   ' + popup_is_close);
			$( ".drop-box" ).css('pointer-events', 'all');
		});
	}
	
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	
	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

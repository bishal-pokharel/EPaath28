var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var sound_1 = new buzz.sound((soundAsset + "ex.ogg"));

var content=[
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext2,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q1b.png',
                        labelname:data.string.q1textb
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q1a.png',
                        labelname:data.string.q1texta
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext3,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q2a.png',
                        labelname:data.string.q2texta
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q2b.png',
                        labelname:data.string.q2textb
                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext4,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q3b.png',
                        labelname:data.string.q3textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q3a.png',
                        labelname:data.string.q3texta
                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext5,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q4a.png',
                        labelname:data.string.q4textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q4b.png',
                        labelname:data.string.q4texta
                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext6,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q5a.png',
                        labelname:data.string.q5textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q5b.png',
                        labelname:data.string.q5texta
                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext7,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q6b.png',
                        labelname:data.string.q6texta
					},
					{
						option_class: "class2",
						option_image_src: imgpath+'q6a.png',
                        labelname:data.string.q6textb
					}],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext8,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q7a.png',
                        labelname:data.string.q7textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q7b.png',
                        labelname:data.string.q7texta

                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext9,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q8b.png',
                        labelname:data.string.q8textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q8a.png',
                        labelname:data.string.q8texta

                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext10,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q9a.png',
                        labelname:data.string.q9texta
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q9b.png',
                        labelname:data.string.q9textb
                    }],
			}
		]
	},
	//slide 0
	{
		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				instructiondata: data.string.etext1,
				questiondata: data.string.etext11,
				option: [
					{
						option_class: "class1",
						option_image_src: imgpath+'q10a.png',
                        labelname:data.string.q10textb
                    },
					{
						option_class: "class2",
						option_image_src: imgpath+'q10c.png',
                        labelname:data.string.q10texta
                    }],
			}
		]
	},

];
// content.shufflearray();

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();
   
	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $('#activity-page-next-btn-enabled');
	var $prevBtn		= $('#activity-page-prev-btn-enabled');
	var countNext		= 0;
	var testin			= new EggTemplate();
   
	var wrong_clicked 	= false;

	var total_page = content.length;
	var current_sound = sound_1;
    function sound_nav(sound_data,navigate){
        current_sound.stop();
        current_sound = sound_data;
        current_sound.play();
    }

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

         countNext==0?sound_nav(sound_1):"";

        /*for randomizing the options*/
		var option_position = [3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		// //top-left
		// $('.option-pos-1').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','scaleX(-1)');
		// }, function(){
		//
		// });
		//bottom-left
		$('.option-pos-3').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		// //top-right
		// $('.option-pos-2').hover(function(){
		// 	$('.center-sundar').attr('src', 'images/sundar/top-right.png');
		// 	$('.center-sundar').css('transform','none');
		// }, function(){
		//
		// });
		//bottom-right
		$('.option-pos-4').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){

		});


		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != total_page)
					$nextBtn.show(0);
			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-1.png');
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-2.png');
				} else {
					$('.center-sundar').attr('src', 'images/sundar/incorrect-3.png');
				}
				testin.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};


	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
	};

	this.create_exercise = function(){
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

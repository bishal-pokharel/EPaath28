var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques1,
                ans:data.string.p1ans2
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q1img1',
                    imgsrc: "",
                    ans:data.string.p1ans1,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans1
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "wrongimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q1img2',
                    imgsrc: "",
                    ans:data.string.p1ans2,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans2
                    }
                    ]
                }
            ]
        }],

    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques2,
                ans:data.string.p1ans1
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q1img1',
                    imgsrc: "",
                    ans:data.string.p1ans1,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans1
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "wrongimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q1img2',
                    imgsrc: "",
                    ans:data.string.p1ans2,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans2
                    }
                    ]
                }
            ]
        }],

    },
//    slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques3,
                ans:data.string.p1ans5
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg1",
                    imgclass: "relativecls img1",
                    imgid: 'q3img1',
                    imgsrc: "",
                    ans:data.string.p1ans5,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans5
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "wrongimg1",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q3img2',
                    imgsrc: "",
                    ans:data.string.p1ans6,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans6
                    }
                    ]
                }
            ]
        }],

    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques4,
                ans:data.string.p1ans7
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg1",
                    imgclass: "relativecls img1",
                    imgid: 'q3img2',
                    imgsrc: "",
                    ans:data.string.p1ans7,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans7
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "wrongimg1",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q3img1',
                    imgsrc: "",
                    ans:data.string.p1ans8,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans8
                    }
                    ]
                }
            ]
        }],

    },
//   slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques5,
                ans:data.string.p1ans9
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q5img1',
                    imgsrc: "",
                    ans:data.string.p1ans9,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans9
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q5img2',
                    imgsrc: "",
                    ans:data.string.p1ans10,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans10
                    }
                    ]
                }
            ]
        }],

    },
//    slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques6,
                ans:data.string.p1ans11
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q6img1',
                    imgsrc: "",
                    ans:data.string.p1ans11,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans11
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q6img2',
                    imgsrc: "",
                    ans:data.string.p1ans12,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans12
                    }
                    ]
                }
            ]
        }],

    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques7,
                ans:data.string.p1ans13
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q6img1',
                    imgsrc: "",
                    ans:data.string.p1ans13,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans13
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q6img2',
                    imgsrc: "",
                    ans:data.string.p1ans14,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans14
                    }
                    ]
                }
            ]
        }],

    },
//    slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques8,
                ans:data.string.p1ans12
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q6img2',
                    imgsrc: "",
                    ans:data.string.p1ans12,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans12
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q6img1',
                    imgsrc: "",
                    ans:data.string.p1ans11,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans11
                    }
                    ]
                }
            ]
        }],

    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques9,
                ans:data.string.p1ans17
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q8img1',
                    imgsrc: "",
                    ans:data.string.p1ans17,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans17
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q5img2',
                    imgsrc: "",
                    ans:data.string.p1ans18,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans18
                    }
                    ]
                }
            ]
        }],

    },
//    slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblockadditionalclass:"clicktitle",
        uppertextblock:[{
          textclass:"clicktext",
          textdata:data.string.click
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "quesn1 content centertext",
                textdata: data.string.p1ques10,
                ans:data.string.p1ans17
            }
        ],
        imageblock:[{
            imagestoshow: [
                {
                    divclass:"commondiv leftdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls img1",
                    imgid: 'q8img1',
                    imgsrc: "",
                    ans:data.string.p1ans17,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans17
                    }
                    ]
                },
                {
                    divclass:"commondiv rightdiv",
                    imgdiv: "correctimg",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'q5img2',
                    imgsrc: "",
                    ans:data.string.p1ans18,
                    textblock:[{
                        textdiv: "textdiv",
                        textclass: "centertext content1",
                        textdata: data.string.p1ans18
                    }
                    ]
                }
            ]
        }],

    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;

    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;

    var eggTemplate = new EggTemplate();
    eggTemplate.init($total_page);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "q1img1", src:imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q1img2", src:imgpath+"night_red_dot01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3img1", src:imgpath+"day.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q3img2", src:imgpath+"night.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q5img1", src:imgpath+"globe_rotation_final.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "q5img2", src:imgpath+"clock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q6img1", src:imgpath+"dayandnight01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "q6img2", src:imgpath+"dayandnight02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "q8img1", src:imgpath+"clock24.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
      			{id: "exer", src: soundAsset+"ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        eggTemplate.numberOfQuestions();

        if(countNext==0){
          sound_player("exer");
        }

        countNext<$total_page?put_image(content, countNext):"";
        shufflehint();
        checkans();
        $(".commondiv").hover(function(){
            $(this).addClass("hoverclass");
            $(this).find("div").eq(1).addClass("textdivhoverclass");
        },function() {
            $(this).removeClass("hoverclass");
            $(this).find("div").eq(1).removeClass("textdivhoverclass");
        });
    }
    function sound_player(sound_id, next){
  		createjs.Sound.stop();
  		current_sound = createjs.Sound.play(sound_id);
  		current_sound.play();
  	}


    function put_image(content, count) {
        if (content[count].hasOwnProperty('imageblock')) {
            var imageblock = content[count].imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                eggTemplate.gotoNext();
                break;
        }
    });


    function shufflehint() {
        var optiondiv = $(".coverboardfull");
        for (var i = 2; i >= 0; i--) {
            var random = Math.random() * i | 0;
            optiondiv.append(optiondiv.find(".commondiv").eq(random));
        }
        optiondiv.find(".commondiv").removeClass().addClass("current");
        var a = ["commondiv leftdiv","commondiv rightdiv"]
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find(".commondiv").removeClass("current");
    }
    function checkans(questiontext){
        $(".commondiv ").on("click",function () {
            if($(this).attr("data-answer").toString().trim()== $(".question").attr("data-answer").toString().trim()) {
                $(this).find("div").eq(1).addClass("correctans");
                $(".commondiv").addClass("avoid-clicks");
                $(this).find("div").eq(1).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                eggTemplate.update(true);
                navigationcontroller();
            }
            else{
                $(this).addClass("avoid-clicks");
                $(this).find("div").eq(1).addClass("wrongans");
                $(this).find("div").eq(1).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
                eggTemplate.update(false);
            }
        });
    }
});

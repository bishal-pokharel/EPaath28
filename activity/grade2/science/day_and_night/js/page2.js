var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide0
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: "starbg",
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "big_bg",
						imgid : 'big_bg',
						imgsrc: ""
				},{
					imgclass: "big_globe",
					imgid : 'big_globe',
					imgsrc: ""
			}]
			}]
		},

		// slide1
				{
					contentnocenteradjust: true,
					contentblockadditionalclass: "starbg",
					imageblock:[{
						imagestoshow:[
							{
								imgclass: "globe_later",
								imgid : 'globe_rotation_final',
								imgsrc: ""
						},{
							imgclass: "sun",
							imgid : 'sun',
							imgsrc: ""
					},{
						imgclass: "globe_half_covered",
						imgid : 'globe_half_covered',
						imgsrc: ""
				}]
			}],

				uppertextblock:[{
					textclass:'left_text_title',
					textdata:data.string.p1text10
				},{
					textclass:'right_text_title',
					textdata:data.string.p1text11
				},{
					textclass:'left_text_desc',
					textdata:data.string.p1text12
				},{
					textclass:'right_text_desc',
					textdata:data.string.p1text13
				}]
				},


				// slide2
						{
							contentnocenteradjust: true,
							contentblockadditionalclass: "starbg",
							imageblock:[{
								imagestoshow:[
									{
										imgclass: "globe",
										imgid : 'globe_rotation_final',
										imgsrc: ""
								},{
									imgclass: "sun",
									imgid : 'sun',
									imgsrc: ""
							},{
								imgclass: "globe_half_covered",
								imgid : 'globe_half_covered',
								imgsrc: ""
						},{
							imgclass: "spaceguy",
							imgid : 'spaceguy',
							imgsrc: ""
					}]
					}],
						uppertextblock:[{
							textclass:'left_text_title',
							textdata:data.string.p1text10
						},{
							textclass:'right_text_title',
							textdata:data.string.p1text11
						}],
						speechbox:[{
							speechbox:'dialogbox',
							imgid:'tb-1',
							textdata:data.string.p1text14,
							textclass:'insidetext'
						}]
					},

					// slide3
							{
								contentnocenteradjust: true,
								contentblockadditionalclass: "starbg",
								imageblock:[{
									imagestoshow:[
										{
											imgclass: "globe",
											imgid : 'globe_rotation_final',
											imgsrc: ""
									},{
										imgclass: "sun",
										imgid : 'sun',
										imgsrc: ""
								},{
									imgclass: "globe_half_covered",
									imgid : 'globe_half_covered',
									imgsrc: ""
							},{
								imgclass: "spaceguy",
								imgid : 'spaceguy',
								imgsrc: ""
						}]
						}],
						uppertextblock:[{
							textclass:'day_text',
							textdata:data.string.p1text1
						}],
							speechbox:[{
								speechbox:'dialogbox',
								imgid:'tb-1',
								textdata:data.string.p1text15,
								textclass:'insidetext'
							}]
						},

						// slide4
								{
									contentnocenteradjust: true,
									contentblockadditionalclass: "starbg",
									imageblock:[{
										imagestoshow:[
											{
												imgclass: "globe",
												imgid : 'globe_rotation_final',
												imgsrc: ""
										},{
											imgclass: "sun",
											imgid : 'sun',
											imgsrc: ""
									},{
										imgclass: "globe_half_covered",
										imgid : 'globe_half_covered',
										imgsrc: ""
								},{
									imgclass: "spaceguy",
									imgid : 'spaceguy',
									imgsrc: ""
							}]
							}],
							uppertextblock:[{
								textclass:'night_text',
								textdata:data.string.p1text2
							}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'tb-1',
									textdata:data.string.p1text16,
									textclass:'insidetext'
								}]
							},


							// slide5
									{
										contentnocenteradjust: true,
										contentblockadditionalclass: "starbg",
										imageblock:[{
											imagestoshow:[
												{
													imgclass: "globe",
													imgid : 'globe_rotation_final',
													imgsrc: ""
											},{
												imgclass: "sun",
												imgid : 'sun',
												imgsrc: ""
										},{
											imgclass: "globe_half_covered",
											imgid : 'globe_half_covered',
											imgsrc: ""
									},{
										imgclass: "spaceguy",
										imgid : 'spaceguy',
										imgsrc: ""
								},{
									imgclass: "arrow",
									imgid : 'arrow3',
									imgsrc: ""
								}]
								}],
								uppertextblock:[{
									textclass:'night_text',
									textdata:data.string.p1text2
								}],
									speechbox:[{
										speechbox:'dialogbox',
										imgid:'tb-1',
										textdata:data.string.p1text17,
										textclass:'insidetext'
									}]
								},


								// slide6
										{
											contentnocenteradjust: true,
											contentblockadditionalclass: "starbg",
											imageblock:[{
												imagestoshow:[
													{
														imgclass: "globe",
														imgid : 'globe_rotation_final',
														imgsrc: ""
												},{
													imgclass: "sun",
													imgid : 'sun',
													imgsrc: ""
											},{
												imgclass: "globe_half_covered",
												imgid : 'globe_half_covered',
												imgsrc: ""
										},{
											imgclass: "spaceguy",
											imgid : 'spaceguy',
											imgsrc: ""
									},{
										imgclass: "dotred",
										imgid : 'dot',
										imgsrc: ""
									}
									]
									}],
									uppertextblock:[{
										textclass:'night_text',
										textdata:data.string.p1text2
									},{
										textclass:'day_text',
										textdata:data.string.p1text1
									}],
										speechbox:[{
											speechbox:'dialogbox',
											imgid:'tb-1',
											textdata:data.string.p1text18,
											textclass:'insidetext'
										}]
									},

									// slide7
											{
												contentnocenteradjust: true,
												contentblockadditionalclass: "starbg",
												imageblock:[{
													imagestoshow:[
														{
															imgclass: "globe_animated",
															imgid : 'night_red_dot1',
															imgsrc: ""
													},{
														imgclass: "globe_animated2",
														imgid : 'day_red_dot1',
														imgsrc: ""
													},{
														imgclass: "sun",
														imgid : 'sun',
														imgsrc: ""
												},{
													imgclass: "globe_half_covered",
													imgid : 'globe_half_covered',
													imgsrc: ""
											},{
												imgclass: "spaceguy",
												imgid : 'spaceguy',
												imgsrc: ""
										},{
											imgclass: "dotred",
											imgid : 'dot',
											imgsrc: ""
										},{
											imgclass: "dotpink",
											imgid : 'pinkdot',
											imgsrc: ""
										}]
										}],
											speechbox:[{
												speechbox:'dialogbox',
												imgid:'tb-1',
												textdata:data.string.p1text19,
												textclass:'insidetext'
											}]
										},
										// slide8
												{
													contentnocenteradjust: true,
													contentblockadditionalclass: "starbg",
													imageblock:[{
														imagestoshow:[
															{
																imgclass: "globe_animated",
																imgid : 'night_red_dot1',
																imgsrc: ""
														},{
															imgclass: "globe_animated2",
															imgid : 'day_red_dot1',
															imgsrc: ""
														},{
															imgclass: "sun",
															imgid : 'sun',
															imgsrc: ""
													},{
														imgclass: "globe_half_covered",
														imgid : 'globe_half_covered',
														imgsrc: ""
												},{
													imgclass: "spaceguy",
													imgid : 'spaceguy',
													imgsrc: ""
												},{
															imgclass: "dotred1",
															imgid : 'dot',
															imgsrc: ""
														},{
															imgclass: "dotpink1",
															imgid : 'pinkdot',
															imgsrc: ""
											}]
											}],
												speechbox:[{
													speechbox:'dialogbox',
													imgid:'tb-1',
													textdata:data.string.p1text19a,
													textclass:'insidetext'
												}]
											},

											// slide9
													{
														contentnocenteradjust: true,
														contentblockadditionalclass: "starbg",
														imageblock:[{
															imagestoshow:[
																{
																	imgclass: "globe",
																	imgid : 'globe_rotation_final',
																	imgsrc: ""
															},{
																imgclass: "sun",
																imgid : 'sun',
																imgsrc: ""
														},{
															imgclass: "globe_half_covered",
															imgid : 'globe_half_covered',
															imgsrc: ""
													},{
														imgclass: "spaceguy",
														imgid : 'spaceguy',
														imgsrc: ""
												},{
													imgclass: "dotred",
													imgid : 'dot',
													imgsrc: ""
												},{
													imgclass: "dotpink1",
													imgid : 'pinkdot',
													imgsrc: ""
												}]
												}],
													speechbox:[{
														speechbox:'dialogbox',
														imgid:'tb-1',
														textdata:data.string.p1text20,
														textclass:'insidetext'
													}]
												},

												// slide10
														{
															contentnocenteradjust: true,
															contentblockadditionalclass: "starbg",
															imageblock:[{
																imagestoshow:[
																	{
																		imgclass: "globe",
																		imgid : 'globe_rotation_final',
																		imgsrc: ""
																},{
																	imgclass: "sun",
																	imgid : 'sun',
																	imgsrc: ""
															},{
																imgclass: "globe_half_covered",
																imgid : 'globe_half_covered',
																imgsrc: ""
														},{
															imgclass: "spaceguy",
															imgid : 'spaceguy',
															imgsrc: ""
														},{
															imgclass: "dotred1",
															imgid : 'dot',
															imgsrc: ""
														},{
															imgclass: "dotpink",
															imgid : 'pinkdot',
															imgsrc: ""
													}]
													}],
														speechbox:[{
															speechbox:'dialogbox',
															imgid:'tb-1',
															textdata:data.string.p1text21,
															textclass:'insidetext'
														}]
													},

													// slide11
															{
																contentnocenteradjust: true,
																contentblockadditionalclass: "starbg",
																imageblock:[{
																	imagestoshow:[
																		{
																			imgclass: "globe",
																			imgid : 'globe_rotation_final',
																			imgsrc: ""
																	},{
																		imgclass: "sun",
																		imgid : 'sun',
																		imgsrc: ""
																},{
																	imgclass: "globe_half_covered",
																	imgid : 'globe_half_covered',
																	imgsrc: ""
															},{
																imgclass: "spaceguy",
																imgid : 'spaceguy',
																imgsrc: ""
															},{
																imgclass: "dotred",
																imgid : 'dot',
																imgsrc: ""
															},{
																imgclass: "dotpink1",
																imgid : 'pinkdot',
																imgsrc: ""
														}]
														}],
															speechbox:[{
																speechbox:'dialogbox',
																imgid:'tb-1',
																textdata:data.string.p1text22,
																textclass:'insidetext'
															}]
														},

														// slide12
																{
																	contentnocenteradjust: true,
																	contentblockadditionalclass: "starbg",
																	imageblock:[{
																		imagestoshow:[
																			{
																				imgclass: "globe_animated",
																				imgid : 'night_red_dot',
																				imgsrc: ""
																		},{
																			imgclass: "globe_animated2",
																			imgid : 'day_red_dot',
																			imgsrc: ""
																	},{
																			imgclass: "sun",
																			imgid : 'sun',
																			imgsrc: ""
																	},{
																		imgclass: "globe_half_covered",
																		imgid : 'globe_half_covered',
																		imgsrc: ""
																},{
																	imgclass: "spaceguy2",
																	imgid : 'spaceguy',
																	imgsrc: ""
															},{
																imgclass: "day_scene",
																imgid : 'day_scene',
																imgsrc: ""
														}]
															}],
																speechbox:[{
																	speechbox:'dialogbox2',
																	imgid:'tb-1',
																	textdata:data.string.p1text25,
																	textclass:'insidetext'
																}]
															},

															// slide13
																	{
																		contentnocenteradjust: true,
																		contentblockadditionalclass: "starbg",
																		imageblock:[{
																			imagestoshow:[
																				{
																					imgclass: "globe_animated",
																					imgid : 'night_red_dot',
																					imgsrc: ""
																			},{
																				imgclass: "globe_animated2",
																				imgid : 'day_red_dot',
																				imgsrc: ""
																		},{
																				imgclass: "sun_animated",
																				imgid : 'sun',
																				imgsrc: ""
																		},{
																			imgclass: "globe_half_covered_animated",
																			imgid : 'globe_half_covered',
																			imgsrc: ""
																	},{
																		imgclass: "spaceguy2",
																		imgid : 'spaceguy',
																		imgsrc: ""
																},{
																	imgclass: "right_image_animated",
																	imgid : 'night_scene',
																	imgsrc: ""
															},{
																imgclass: "night_moon",
																imgid : 'night_moon',
																imgsrc: ""
														}]
																}],
																	speechbox:[{
																		speechbox:'dialogbox2',
																		imgid:'tb-1',
																		textdata:data.string.p1text26,
																		textclass:'insidetext'
																	}]
																},

																// slide14
																		{
																			contentnocenteradjust: true,
																			contentblockadditionalclass: "starbg",
																			imageblock:[{
																				imagestoshow:[
																					{
																						imgclass: "globe_animated",
																						imgid : 'globe_rotation_final',
																						imgsrc: ""
																				},{
																					imgclass: "sun_animated",
																					imgid : 'sun',
																					imgsrc: ""
																			},{
																				imgclass: "globe_half_covered_animated",
																				imgid : 'globe_half_covered',
																				imgsrc: ""
																		},{
																			imgclass: "spaceguy2",
																			imgid : 'spaceguy',
																			imgsrc: ""
																	},{
																		imgclass: "right_image_animated",
																		imgid : 'night_scene',
																		imgsrc: ""
																},{
																	imgclass: "night_moon",
																	imgid : 'night_moon',
																	imgsrc: ""
															}]
																	}],
																		speechbox:[{
																			speechbox:'dialogbox2',
																			imgid:'tb-1',
																			textdata:data.string.p1text27,
																			textclass:'insidetext'
																		}]
																	},

																	// slide15
																			{
																				contentnocenteradjust: true,
																				contentblockadditionalclass: "starbg",
																				imageblock:[{
																					imagestoshow:[
																						{
																							imgclass: "globe_animated",
																							imgid : 'globe_rotation_final',
																							imgsrc: ""
																					},{
																						imgclass: "sun_animated",
																						imgid : 'sun',
																						imgsrc: ""
																				},{
																					imgclass: "globe_half_covered_animated",
																					imgid : 'globe_half_covered',
																					imgsrc: ""
																			},{
																				imgclass: "spaceguy2",
																				imgid : 'spaceguy',
																				imgsrc: ""
																		},{
																			imgclass: "right_image_animated",
																			imgid : 'night_scene',
																			imgsrc: ""
																	},{
																		imgclass: "night_moon",
																		imgid : 'night_moon',
																		imgsrc: ""
																}]
																		}],
																			speechbox:[{
																				speechbox:'dialogbox2',
																				imgid:'tb-1',
																				textdata:data.string.p1text28,
																				textclass:'insidetext'
																			}]
																		},

																		// slide16
																				{
																					contentnocenteradjust: true,
																					contentblockadditionalclass: "starbg",
																					imageblock:[{
																						imagestoshow:[
																							{
																								imgclass: "globe_animated",
																								imgid : 'night_red_dot',
																								imgsrc: ""
																						},{
																							imgclass: "globe_animated2",
																							imgid : 'day_red_dot',
																							imgsrc: ""
																					},{
																							imgclass: "sun_animated",
																							imgid : 'sun',
																							imgsrc: ""
																					},{
																						imgclass: "globe_half_covered_animated",
																						imgid : 'globe_half_covered',
																						imgsrc: ""
																				},{
																					imgclass: "spaceguy2",
																					imgid : 'spaceguy',
																					imgsrc: ""
																			},{
																				imgclass: "right_image_animated",
																				imgid : 'daytonighttoday',
																				imgsrc: ""
																		},{
																				imgclass: "clock",
																				imgid : 'clock',
																				imgsrc: ""
																			}]
																			}],
																				speechbox:[{
																					speechbox:'dialogbox2',
																					imgid:'tb-1',
																					textdata:data.string.p1text29,
																					textclass:'insidetext'
																				}],
																				uppertextblock:[{
																					textclass:'left_increment',
																					textdata:data.string.p1text32,
																				},{
																					textclass:'right_increment',
																					textdata:data.string.p1text32,
																				}]
																			},



																			// slide17
																					{
																						contentnocenteradjust: true,
																						contentblockadditionalclass: "starbg",
																						imageblock:[{
																							imagestoshow:[
																								{
																									imgclass: "globe_animated",
																									imgid : 'globe_rotation_final',
																									imgsrc: ""
																							},{
																								imgclass: "sun_animated",
																								imgid : 'sun',
																								imgsrc: ""
																						},{
																							imgclass: "globe_half_covered_animated",
																							imgid : 'globe_half_covered',
																							imgsrc: ""
																						},{
																							imgclass: "spaceguy2",
																							imgid : 'spaceguy',
																							imgsrc: ""
																						},{
																							imgclass: "right_image_animated",
																							imgid : 'daytonighttoday',
																							imgsrc: ""
																						}]
																				}],
																					speechbox:[{
																						speechbox:'dialogbox2',
																						imgid:'tb-1',
																						textdata:data.string.p1text30,
																						textclass:'insidetext'
																					}]
																				},

																				// slide18
																						{
																							contentnocenteradjust: true,
																							contentblockadditionalclass: "starbg",
																							imageblock:[{
																								imagestoshow:[
																									{
																										imgclass: "globe_animated",
																										imgid : 'globe_rotation_final',
																										imgsrc: ""
																								},{
																									imgclass: "sun_animated",
																									imgid : 'sun',
																									imgsrc: ""
																							},{
																								imgclass: "globe_half_covered_animated",
																								imgid : 'globe_half_covered',
																								imgsrc: ""
																							},{
																								imgclass: "spaceguy2",
																								imgid : 'spaceguy',
																								imgsrc: ""
																							},{
																								imgclass: "right_image_animated",
																								imgid : 'daytonighttoday',
																								imgsrc: ""
																							},{
																									imgclass: "clock",
																									imgid : 'clock',
																									imgsrc: ""
																								}]
																					}],
																						speechbox:[{
																							speechbox:'dialogbox2',
																							imgid:'tb-1',
																							textdata:data.string.p1text30,
																							textclass:'insidetext'
																						}]
																					},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 16;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "girl_1", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "big_globe", src: imgpath+"big_globe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "big_bg", src: imgpath+"big_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "globe_rotation_final", src: imgpath+"globe_rotation_final.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "stars", src: imgpath+"stars.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "globe_half_covered", src: imgpath+"globe_half_covered.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spaceguy", src: imgpath+"spaceguy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "day_scene", src: imgpath+"day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_scene", src: imgpath+"night.png", type: createjs.AbstractLoader.IMAGE},
			{id: "day_to_night", src: imgpath+"dayandnight-min.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "night_to_day", src: imgpath+"nighttoday.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "night_moon", src: imgpath+"full_moon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "daytonighttoday", src: imgpath+"daytonighttoday.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "clock", src: imgpath+"clock-min.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "day_red_dot", src: imgpath+"day_red_dot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_red_dot", src: imgpath+"night_red_dot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dot", src: imgpath+"red_dot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow3", src: imgpath+"three_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "day_red_dot1", src: imgpath+"day_red_dot01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night_red_dot1", src: imgpath+"night_red_dot01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pinkdot", src: imgpath+"pink_dot.png", type: createjs.AbstractLoader.IMAGE},


			//textboxes
			{id: "tb-1", src: imgpath+"dialogue.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s2_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s2_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s2_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s2_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s2_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s2_p15.ogg"},
			{id: "sound_15", src: soundAsset+"s2_p16.ogg"},
			{id: "sound_16", src: soundAsset+"s2_p17.ogg"},
			{id: "sound_17", src: soundAsset+"s2_p18.ogg"},
			{id: "sound_18", src: soundAsset+"s2_p19.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch (countNext) {
			case 0:
			setTimeout(function(){
				nav_button_controls(1000);
				$('.big_globe').attr('src',preload.getResult('globe_rotation_final').src);
			},6000);
			break;

			case 1:
			$('.sun,.globe_half_covered,.left_text_title,.right_text_title,.left_text_desc,.right_text_desc').hide(0);
			setTimeout(function(){
				$('.sun,.globe_half_covered,.left_text_title,.right_text_title,.left_text_desc,.right_text_desc').fadeIn(500);
				sound_player1("sound_0");
			},1500);
			break;

			case 3:
				$('.dialogbox').css('height','20%');
				sound_player1("sound_"+(countNext));
				break;
			case 4:
			$('.dialogbox').css('height','20%');
			sound_player1("sound_"+(countNext));
			break;

			case 7:
			$('.globe_animated2,.dotred').fadeOut(2000);
			$('.dotpink').fadeIn(2000);
			$('.globe_animated2,.globe_animated').css('left','70%');
			$('.globe_half_covered').addClass('transformclass');
			sound_player1("sound_"+(countNext));
			break;

			case 8:
			$('.globe_animated,.dotpink1').css('z-index','28').fadeOut(2000);
			$('.dotred1').css('z-index','33').fadeIn(2000);
			$('.globe_animated2,.globe_animated').css('left','70%');
			$('.globe_half_covered').addClass('transformclass');
			sound_player1("sound_"+(countNext));
			break;
			case 10:
			$('.dotred1').fadeIn(1000);
			$('.dotpink').fadeIn(2000);
			sound_player1("sound_"+(countNext));
			break;

			case 12:
				$('.globe_animated2').fadeOut(10000);
				$('.sun').addClass('anim_sun');
				$('.globe_half_covered').addClass('anim_cover');
				$('.globe_half_covered').addClass('transformclass');
				$('.day_scene').addClass('image_slide');
				$('.globe').addClass('anim_earth');
				setTimeout(function(){
					$('.day_scene').attr('src',preload.getResult('day_to_night').src);
				},1000);
				sound_player("sound_"+(countNext));
				nav_button_controls(12500);
			break;
			case 13:
			sound_player("sound_"+(countNext));
			$('.globe_half_covered_animated').addClass('transformclass');
			$('.globe_animated').css('z-index','28').fadeOut(6000);;
			setTimeout(function(){
				$('.right_image_animated').attr('src',preload.getResult('night_to_day').src);
				$('.night_moon').hide(0);
				nav_button_controls(6000);
			},1000);
			break;

			case 16:
			$('.dialogbox2').hide(0);
			var count = 0;
			increasenumber();
			function increasenumber(){
				if(count<24){
					count++;
					$('.number_increment').html(count);
				}
				setTimeout(increasenumber, 365);
			}
			setTimeout(function(){
					sound_player1("sound_"+(countNext));
					$('.dialogbox2').show(500);
			},365*24);
			$('.globe_animated2').fadeOut(365*12);
			setTimeout(function(){
				$('.globe_animated2').fadeIn(365*12);
			},365*12);
			break;

			case 18:
			$('.right_image_animated').animate({'left':'100%'},1000);
			$('.clock').fadeOut(1000);
			$('.sun_animated').animate({'left':'5%'},1000);
			$('.globe_animated').animate({
			'width': '37%',
			'height': '54%',
			'left': '60%'
			},1000);

			$('.globe_half_covered_animated').animate({
			'width': '33.2%',
			'left': '62.2%',
			 'top': '26%',
			 'height': '52%'
			},1000);
			$('.spaceguy2').animate({
			'left': '71%',
			 'top': '19%'
			},1000);
			$('.dialogbox2').animate({
			'left': '33%',
			 'top': '20%'
		 },1000);
		 $('.insidetext').html(data.string.p1text31);
		 	sound_player1("sound_"+(countNext));
			break;
			default:
			$('.dialogbox,.spaceguy').fadeIn(500);
			sound_player1("sound_"+(countNext));
			break;
		}

	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nav_button_controls(0);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//console.log("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].multipleimagecontainer[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].multipleimagecontainer[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//coverpage
		{
			contentnocenteradjust: true,
			extratextblock:[{
				textclass:"center_text",
				textdata:data.lesson.chapter,
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:"coverpage",
					imgclass:'bg_full',
				}]
			}]
	},

	//slide1
		{
			contentnocenteradjust: true,
			imageblock:[{
				imagestoshow:[{
					imgid:"day",
					imgclass:'bg_full day',
				},{
					imgid:"night",
					imgclass:'bg_full night',
				},{
					imgid:"sun",
					imgclass:'sun',
				},{
					imgid:"full_moon",
					imgclass:'full_moon',
				}]
			}]
	},


		//slide2
			{
				contentnocenteradjust: true,
				imageblock:[{
					imagestoshow:[{
						imgid:"day01",
						imgclass:'left_image',
					},{
						imgid:"night01",
						imgclass:'right_image',
					}]
				}],
				extratextblock:[{
					textclass:"left_bot_center_text",
					textdata:data.string.p1text1,
				},{
					textclass:"right_bot_center_text",
					textdata:data.string.p1text2,
				}],
		},

		//slide3
			{
				contentnocenteradjust: true,
				imageblock:[{
					imagestoshow:[{
						imgid:"day",
						imgclass:'bg_full',
					},{
						imgid:"sagar",
						imgclass:'sagar',
					},{
						imgid:"sun",
						imgclass:'sun animate_up_down_infinite',
					}]
				}],
				speechbox:[{
					speechbox:'dialogbox',
					imgid:'speechBox',
					textdata:data.string.p1text3,
					textclass:'insidetext'
				}]
		},

				//slide4
					{
						contentnocenteradjust: true,
						imageblock:[{
							imagestoshow:[{
								imgid:"day",
								imgclass:'bg_full',
							},{
								imgid:"sagar",
								imgclass:'sagar',
							},{
								imgid:"sun",
								imgclass:'sun animate_up_down_infinite',
							}]
						}],
						speechbox:[{
							speechbox:'dialogbox',
							imgid:'speechBox',
							textdata:data.string.p1text4,
							textclass:'insidetext'
						}]
				},

						//slide5
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[{
										imgid:"day",
										imgclass:'bg_full',
									},{
										imgid:"sagar",
										imgclass:'sagar',
									},{
										imgid:"sun",
										imgclass:'sun  animate_up_down_infinite',
									},{
										imgid:"cloud",
										imgclass:'cloudan',
									},{
										imgid:"dayaftercloud",
										imgclass:'bg_full_cloud',
									}]
								}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'speechBox',
									textdata:data.string.p1text5,
									textclass:'insidetext'
								}]
						},

						//slide6
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[{
										imgid:"night",
										imgclass:'bg_full',
									},{
										imgid:"sagar",
										imgclass:'sagar',
									},{
										imgid:"full_moon",
										imgclass:'full_moon animate_up_down_infinite',
									}]
								}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'speechBox',
									textdata:data.string.p1text6,
									textclass:'insidetext'
								}]
						},

						//slide7
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[{
										imgid:"night",
										imgclass:'bg_full',
									},{
										imgid:"sagar",
										imgclass:'sagar',
									},{
										imgid:"fmoon",
										imgclass:'firstmoon',
									},{
										imgid:"smoon",
										imgclass:'secondmoon',
									},{
										imgid:"tmoon",
										imgclass:'thirdmoon',
									},{
										imgid:"ffmoon",
										imgclass:'fourmoon',
									},{
										imgid:"fffmoon",
										imgclass:'fivemoon',
									}]
								}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'speechBox',
									textdata:data.string.p1text7,
									textclass:'insidetext'
								}]
						},


						//slide8
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[{
										imgid:"night",
										imgclass:'bg_full',
									},{
										imgid:"sagar",
										imgclass:'sagar',
									}]
								}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'speechBox',
									textdata:data.string.p1text8,
									textclass:'insidetext'
								}]
						},

						//slide9
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[{
										imgid:"day",
										imgclass:'bg_full opa',
									},{
										imgid:"sagar",
										imgclass:'sagar',
									},{
										imgid:"sun",
										imgclass:'sun animate_up_down_infinite',
									}]
								}],
								speechbox:[{
									speechbox:'dialogbox',
									imgid:'speechBox',
									textdata:data.string.p1text9,
									textclass:'insidetext fontstyle'
								}]
						},

];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "day", src: imgpath+"day.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night", src: imgpath+"night.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "full_moon", src: imgpath+"full_moon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "day01", src: imgpath+"day01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "night01", src: imgpath+"night01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud", src: imgpath+"clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dayaftercloud", src: imgpath+"day02_after_clouds.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fmoon", src: imgpath+"step01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "smoon", src: imgpath+"step02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tmoon", src: imgpath+"step03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ffmoon", src: imgpath+"step04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fffmoon", src: imgpath+"step05.png", type: createjs.AbstractLoader.IMAGE},
		// speechbox
			{id: "speechBox", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		// put_image_sec(content, countNext);
		switch (countNext) {
			case 1:
			function day_to_night(){
				$('.day').css('opacity','.35').animate({'opacity':'1'},2000);
				setTimeout(function() {
					$('.full_moon').removeClass('animate_up_down');
					$('.day').animate({'opacity':'.35'},2000);
				},4000);
				$('.sun').addClass('animate_up_down');
				setTimeout(function() {
					$('.sun').removeClass('animate_up_down');
					$('.day').animate({'opacity':'0'},2000);
					$('.full_moon').addClass('animate_up_down');
				},6000);
				setTimeout(function() {
					$('.day').animate({'opacity':'.35'},2000);
				},10000);
				setTimeout(day_to_night, 12000);
				nav_button_controls(12000);
			}
			day_to_night();
			break;


			case 2:
			$('.left_bot_center_text,.right_bot_center_text').hide(0);
			$('.left_bot_center_text').fadeIn(1000,function(){
				$('.right_bot_center_text').fadeIn(1000);
			});
			sound_player("sound_2");
			break;
			case 4:
			sound_player1("sound_"+(countNext));
			nav_button_controls(6500);
			break;
			case 5:
			$(".bg_full_cloud").delay(4000).css("z-index","2").fadeIn(0);
			$(".bg_full_cloud").delay(1000).hide(0);
			sound_player("sound_"+(countNext));
			break;
			case 7:
			sound_player("sound_"+(countNext));
			$(".firstmoon").animate({
				opacity:0.5
				// $('.bg_full').css('opacity','1');
			},1000,function(){
				$(".secondmoon").animate({
					opacity:0.6
			},1000,function(){
				$(".thirdmoon").animate({
					opacity:0.7
			},1000,function(){
				$(".fourmoon").animate({
					opacity:0.8
				},1000,function(){
				$(".fivemoon").animate({
						opacity:0.9
						},1000);
					});
				});
			});
			});
				$(".bg_full").animate({
				opacity:1
				},1000,function(){
				$(".bg_full").animate({
					opacity:0.95
				},1000,function(){
				$(".bg_full").animate({
					opacity:0.9
				},1000,function(){
				$(".bg_full").animate({
					opacity:0.85
				},1000,function(){
				$(".bg_full").animate({
						opacity:0.8
				},1000);
				});
				});
				});
				});
				break
				case 9:
				sound_player1("sound_"+(countNext));
				nav_button_controls(6500);
				break;
			default:
			sound_player("sound_"+(countNext));
			break;

		}
		put_speechbox_image(content, countNext);
		function randomize(parent){
			var parent = $(parent);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}
		var count = 0, i, j;
		$(".pari, .sp-1").hide(0);
		function hideImages(){
				$(".grayout").fadeOut(3000, function(){ $(this).remove();});
				$(".pari").delay(4000).fadeIn();
				$(".sp-1").delay(4000).fadeIn();
				setTimeout(function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_0");
						current_sound.play();
						current_sound.on('complete',function(){
							$(".hide").addClass("fadeIn").removeClass("hide");
									createjs.Sound.stop();
									current_sound_1 = createjs.Sound.play("sound_0");
									current_sound_1.play();
									current_sound_1.on('complete',function(){
									$nextBtn.show(0);
									});
						});
				},5000);
		}

	}
// drag drop
	function handleDrop(event, ui, dragClass, dropClass){
		var dragAns = ui.draggable.parent().find("img").attr("answer");
		var dropAns = $(dropClass).parent().find("div").attr("answer");
		if(dragAns.toString().trim() == dropAns.toString().trim()){
		$(".draggable").draggable("disable");
		play_correct_incorrect_sound(1);
		$(dropClass).css({
			"background" : "#98C02E",
			"border" : "4px solid #EEFF41"
		});
		$(dropClass).siblings().css({
			"display" : "block"
		});
		ui.draggable.detach();
		switch (countNext) {
			case 2:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("bacon").src+"'/>")
			break;
			case 3:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("banana").src+"'/>")
			break;
			case 4:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("milk").src+"'/>")
			break;
			case 5:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("potato").src+"'/>")
			break;
			case 6:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("egg").src+"'/>")
			break;
			case 7:
				$(dropClass).append("<img class='dropdImg' src='"+preload.getResult("corn").src+"'/>")
			break;
			default:

		}
		navigationcontroller();
		}
		else{
		play_correct_incorrect_sound(0);
		ui.draggable.siblings().css({
			"display" : "block"
		});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls(0);
		});
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].multipleimagecontainer[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].multipleimagecontainer[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_image_third(content, count){
		if(content[count].dropdiv[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].dropdiv[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//console.log("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	// slide0
		{
			contentnocenteradjust: true,
			uppertextblock:[
			{
				textclass: "mid_text",
				textdata: data.string.p3text1
			}],
			imageblock:[{
				imagestoshow:[
					{
						imgclass: "bg_full",
						imgid : 'bg',
						imgsrc: ""
				}]
			}]
		},

		// slide1
			{
				contentnocenteradjust: true,
				imageblock:[{
					imagestoshow:[
						{
							imgclass: "bg_full",
							imgid : 'bg01',
							imgsrc: ""
					}]
				}],
				speechbox:[{
					speechbox:'dialogbox_left',
					imgid:'left_dialogue',
					textdata:data.string.p3text2,
					textclass:'insidetext'
				}]
			},

			// slide2
				{
					contentnocenteradjust: true,
					imageblock:[{
						imagestoshow:[
							{
								imgclass: "bg_full",
								imgid : 'bg01',
								imgsrc: ""
						}]
					}],
					speechbox:[{
						speechbox:'dialogbox_left',
						imgid:'left_dialogue',
						textdata:data.string.p3text3,
						textclass:'insidetext'
					}]
				},

				// slide3
					{
						contentnocenteradjust: true,
						imageblock:[{
							imagestoshow:[
								{
									imgclass: "bg_full",
									imgid : 'bg02',
									imgsrc: ""
							},{
								imgclass: "torch",
								imgid : 'torch',
								imgsrc: ""
						}]
						}],
						speechbox:[{
							speechbox:'dialogbox_left',
							imgid:'right_dialogue',
							textdata:data.string.p3text4,
							textclass:'insidetext'
						}]
					},

					// slide4
						{
							contentnocenteradjust: true,
							imageblock:[{
								imagestoshow:[
									{
										imgclass: "bg_full",
										imgid : 'bg01',
										imgsrc: ""
								},{
									imgclass: "torch",
									imgid : 'torch',
									imgsrc: ""
							},{
								imgclass: "globe",
								imgid : 'globe',
								imgsrc: ""
						}]
							}],
							speechbox:[{
								speechbox:'dialogbox_left',
								imgid:'left_dialogue',
								textdata:data.string.p3text5,
								textclass:'insidetext'
							}]
						},

						// slide5
							{
								contentnocenteradjust: true,
								imageblock:[{
									imagestoshow:[
										{
											imgclass: "bg_full",
											imgid : 'bg02',
											imgsrc: ""
									},{
										imgclass: "globe",
										imgid : 'globe',
										imgsrc: ""
								},{
									imgclass: "torch",
									imgid : 'torch',
									imgsrc: ""
							},{
									imgclass: "globe_rotation_final",
									imgid : 'globe_rotation_final',
									imgsrc: ""
							}]
								}],
								speechbox:[{
									speechbox:'dialogbox_left',
									imgid:'right_dialogue',
									textdata:data.string.p3text6,
									textclass:'insidetext'
								}]
							},

							// slide6
								{
									contentnocenteradjust: true,
									imageblock:[{
										imagestoshow:[
											{
												imgclass: "bg_full",
												imgid : 'bg02',
												imgsrc: ""
										},{
											imgclass: "globe",
											imgid : 'globe',
											imgsrc: ""
									},{
										imgclass: "torch",
										imgid : 'torch',
										imgsrc: ""
								},{
										imgclass: "globe_rotation_final",
										imgid : 'globe_rotation_final',
										imgsrc: ""
								}]
									}],
									speechbox:[{
										speechbox:'dialogbox_left',
										imgid:'right_dialogue',
										textdata:data.string.p3text8,
										textclass:'insidetext'
									}]
								},

								// slide7
									{
										contentnocenteradjust: true,
										imageblock:[{
											imagestoshow:[
												{
													imgclass: "bg_full bg_torch",
													imgid : 'bg_torch',
													imgsrc: ""
											},
												{
													imgclass: "bg_full bg_notorch",
													imgid : 'bg_notorch',
													imgsrc: ""
											},{
											imgclass: "globe_rotation_final2",
											imgid : 'globe_rotation_final',
											imgsrc: ""
									},{
											imgclass: "globe_half_covered",
											imgid : 'globe_half_covered',
											imgsrc: ""
									},{
											imgclass: "handicon",
											imgid : 'handicon',
											imgsrc: ""
									}]
										}],
										speechbox:[{
											speechbox:'dialogbox_left',
											imgid:'left_dialogue',
											textdata:data.string.p3text9,
											textclass:'insidetext'
										}]
									},

									// slide8
										{
											contentnocenteradjust: true,
											imageblock:[{
												imagestoshow:[
													{
														imgclass: "bg_full bg_torch",
														imgid : 'bg_torch',
														imgsrc: ""
												},
												{
												imgclass: "globe_rotation_final2",
												imgid : 'globe_rotation_final',
												imgsrc: ""
										},{
												imgclass: "globe_half_covered",
												imgid : 'globe_half_covered',
												imgsrc: ""
										}]
											}],
											speechbox:[{
												speechbox:'dialogbox_left',
												imgid:'right_dialogue',
												textdata:data.string.p3text11,
												textclass:'insidetext'
											}]
										},

										// slide9
											{
												contentnocenteradjust: true,
												imageblock:[{
													imagestoshow:[
														{
															imgclass: "bg_full bg_torch",
															imgid : 'bg_torch',
															imgsrc: ""
													},
													{
													imgclass: "globe_rotation_final2",
													imgid : 'globe_rotation_final',
													imgsrc: ""
											},{
													imgclass: "globe_half_covered",
													imgid : 'globe_half_covered',
													imgsrc: ""
											}]
												}],
												speechbox:[{
													speechbox:'dialogbox_left',
													imgid:'left_dialogue',
													textdata:data.string.p3text12,
													textclass:'insidetext'
												}]
											},

											// slide10
												{
													contentnocenteradjust: true,
													imageblock:[{
														imagestoshow:[
															{
																imgclass: "bg_full bg_torch",
																imgid : 'bg_torch',
																imgsrc: ""
														},
														{
														imgclass: "globe_rotation_final2",
														imgid : 'globe_rotation_final',
														imgsrc: ""
												},{
														imgclass: "globe_half_covered",
														imgid : 'globe_half_covered',
														imgsrc: ""
												}]
													}],
													speechbox:[{
														speechbox:'dialogbox_left',
														imgid:'right_dialogue',
														textdata:data.string.p3text13,
														textclass:'insidetext'
													}]
												},



];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var timeoutvar2 = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg03", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg04", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg05", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg06", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg06a", src: imgpath+"bg06a.png", type: createjs.AbstractLoader.IMAGE},
			{id: "globe", src: imgpath+"globe.png", type: createjs.AbstractLoader.IMAGE},
			{id: "torch", src: imgpath+"torch.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "globe_half_covered", src: imgpath+"globe_half_covered.png", type: createjs.AbstractLoader.IMAGE},
			{id: "globe_rotation_final", src: imgpath+"globe_rotation_final.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_notorch", src: imgpath+"1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_torch", src: imgpath+"2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: imgpath+"hand-icon-1.gif", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "left_dialogue", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "right_dialogue", src: 'images/textbox/white/tr-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5a", src: soundAsset+"s3_p6.ogg"},
			{id: "sound_5b", src: soundAsset+"s3_p6_1.ogg"},
			{id: "sound_6", src: soundAsset+"s3_p7.ogg"},
			{id: "sound_7a", src: soundAsset+"s3_p8.ogg"},
			{id: "sound_7b", src: soundAsset+"s3_p8_1.ogg"},
			{id: "sound_8", src: soundAsset+"s3_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s3_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s3_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch (countNext) {
			case 3:
			$('.torch').hide(0).fadeIn(500);
			sound_player1("sound_"+(countNext));
			break;

			case 4:
			$('.globe').hide(0).fadeIn(500);
			sound_player1("sound_"+(countNext));
			break;
			case 5:
			sound_player("sound_5a");
			$('.globe').addClass('anim_in_out');
			$('.globe_rotation_final').hide(0);
			$('.globe').click(function(){
				$('.insidetext').html(data.string.p3text7);
				$('.globe').removeClass('anim_in_out');
				$('.globe_rotation_final').fadeIn(200);
				sound_player1("sound_5b");
			});
			break;
			case 7:
			sound_player("sound_7a");
			$('.globe_half_covered').hide(0);
			$('.dialogbox_left').css({'top':'2%','left':'36%'});
			$('.handicon').click(function(){
				$(this).hide(0);
				$('.globe_half_covered').fadeIn(2000);
				$('.bg_notorch').fadeOut(2000);
				$('.insidetext').html(data.string.p3text10);
				sound_player1("sound_7b");
			});
			break;
			case 8:
			case 10:
			$('.dialogbox_left').css({'top':'7%','left':'36%'});
			sound_player1("sound_"+(countNext));
			break;
			case 9:
			$('.dialogbox_left').css('left','43%')
			sound_player1("sound_"+(countNext));
			break;
			default:
			sound_player1("sound_"+(countNext));
			break;

		}
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_player1(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
		nav_button_controls(0);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].multipleimagecontainer[0].hasOwnProperty('imageblock')){
			var imagblockVal = content[count].multipleimagecontainer[0];
			for(var j=0;j<imagblockVal.imageblock.length; j++){
							var imageblock = imagblockVal.imageblock[j];
							if(imageblock.hasOwnProperty('imagestoshow')){
								var imageClass = imageblock.imagestoshow;
								for(var i=0; i<imageClass.length; i++){
									var image_src = preload.getResult(imageClass[i].imgid).src;
									//get list of classes
									var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
									var selector = ('.'+classes_list[classes_list.length-1]);
									$(selector).attr('src', image_src);
								}
							}
  		}

		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

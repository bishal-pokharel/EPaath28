var imgpath = $ref + "/";
var testanimal = "";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

 var sound_c_1 = new buzz.sound((audioPath + "p3_s2.ogg"));
 var sound_c_2 = new buzz.sound((audioPath + "p3_s3_0.ogg"));
 var sound_c_3 = new buzz.sound((audioPath + "p3_s3_1.ogg"));
 var sound_c_click = new buzz.sound((audioPath + "p3_s0.ogg"));
 var animal_clk = new buzz.sound((audioPath + "s3_p4_1.ogg"));
 var domestic_sound = new buzz.sound((audioPath + "s3_p4_2_domestic.ogg"));
 var wild_sound = new buzz.sound((audioPath + "s3_p4_2_wild.ogg"));
var content=[
{
	uppertextblock:[
	{
		textclass : "headertext",
		textdata : data.string.p1text1,
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "selectanimal selectpig",
			imgid : "pig",
			imgsrc : imgpath + "images/colouring/pig.png"
		},
		{
			imgclass: "selectanimal selecthorse",
			imgid : "horse",
			imgsrc : imgpath + "images/colouring/horse.png"
		},
		{
			imgclass: "selectanimal selectelephant",
			imgid : "elephant",
			imgsrc : imgpath + "images/colouring/elephant.png"
		},
		{
			imgclass: "selectanimal selectdog",
			imgid : "dog",
			imgsrc : imgpath + "images/colouring/dog.png"
		},
		{
			imgclass: "selectanimal selectkangaroo",
			imgid : "kangaroo",
			imgsrc : imgpath + "images/colouring/kangaroo.png"
		},
		{
			imgclass: "selectanimal selectgoat",
			imgid : "goat",
			imgsrc : imgpath + "images/colouring/goat.png"
		},
		{
			imgclass: "selectanimal selectcow",
			imgid : "cow",
			imgsrc : imgpath + "images/colouring/cow.png"
		},
		{
			imgclass: "selectanimal selectgiraffee",
			imgid : "giraffe",
			imgsrc : imgpath + "images/colouring/giraffee.png"
		},
		{
			imgclass: "selectanimal selectdeer",
			imgid : "deer",
			imgsrc : imgpath + "images/colouring/deer.png"
		},
		{
			imgclass: "selectanimal selectlion",
			imgid : "lion",
			imgsrc : imgpath + "images/colouring/lion.png"
		},
		{
			imgclass: "selectanimal selectpanda",
			imgid : "panda",
			imgsrc : imgpath + "images/colouring/panda.png"
		},
		{
			imgclass: "selectanimal selectcamel",
			imgid : "camel",
			imgsrc : imgpath + "images/colouring/camel.png"
		},
		]
	}
	]
},

{
	canvasused: true,
	uppertextblock:[
	{
		textclass : "colorpicker",
		textdata : data.string.colors
	},
	{
		textclass : "sizepicker",
		textdata : data.string.size
	},
	{
		textclass : "eraserpicker",
		textdata : data.string.eraser
	}
	],
	extraimagearray: [
	{
		extraimage: "colorcon",
	},
	{
		extraimage: "brushcon",
	},
	{
		extraimage: "erasercon",
	},
	{
		extraimage: "images/fish",
	},
	{
		extraimage: "colors color1",
	},
	{
		extraimage: "colors color2",
	},
	{
		extraimage: "colors color3",
	},
	{
		extraimage: "colors color4",
	},
	{
		extraimage: "colors color5",
	},
	{
		extraimage: "colors color6",
	},
	{
		extraimage: "colors color7",
	},
	{
		extraimage: "colors color8",
	},
	{
		extraimage: "colors color9",
	},
	{
		extraimage: "colors color10",
	},
	{
		extraimage: "colors color11",
	},
	{
		extraimage: "colors color12",
	},
	{
		extraimage: "colors color13",
	},
	{
		extraimage: "colors color14",
	},
	{
		extraimage: "colors color15",
	},

	{
		extraimage: "normalsize",
	},
	{
		extraimage: "bigsize",
	},

	{
		extraimage: "whiteback",
	},

	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "animal",
			imgid : "pig",
			imgsrc : imgpath + "images/colouring/pig.png"
		},
		{
			imgclass: "animal",
			imgid : "horse",
			imgsrc : imgpath + "images/colouring/horse.png"
		},
		{
			imgclass: "animal",
			imgid : "elephant",
			imgsrc : imgpath + "images/colouring/elephant.png"
		},
		{
			imgclass: "animal",
			imgid : "dog",
			imgsrc : imgpath + "images/colouring/dog.png"
		},
		{
			imgclass: "animal",
			imgid : "kangaroo",
			imgsrc : imgpath + "images/colouring/kangaroo.png"
		},
		{
			imgclass: "animal",
			imgid : "goat",
			imgsrc : imgpath + "images/colouring/goat.png"
		},
		{
			imgclass: "animal",
			imgid : "cow",
			imgsrc : imgpath + "images/colouring/cow.png"
		},
		{
			imgclass: "animal",
			imgid : "giraffe",
			imgsrc : imgpath + "images/colouring/giraffee.png"
		},
		{
			imgclass: "animal",
			imgid : "deer",
			imgsrc : imgpath + "images/colouring/deer.png"
		},
		{
			imgclass: "animal",
			imgid : "lion",
			imgsrc : imgpath + "images/colouring/lion.png"
		},
		{
			imgclass: "animal",
			imgid : "panda",
			imgsrc : imgpath + "images/colouring/panda.png"
		},
		{
			imgclass: "animal",
			imgid : "camel",
			imgsrc : imgpath + "images/colouring/camel.png"
		},
		{
			imgclass: "smallsize",
			imgsrc : imgpath + "images/colouring/brush_4.png"
		},
		{
			imgclass: "normalsize",
			imgsrc : imgpath + "images/colouring/brush_3.png"
		},
		{
			imgclass: "bigsize",
			imgsrc : imgpath + "images/colouring/brush_2.png"
		},
		{
			imgclass: "largesize",
			imgsrc : imgpath + "images/colouring/brush_1.png"
		},
		{
			imgclass: "eraser",
			imgsrc : imgpath + "images/colouring/eraser.png"
		},
		]
	}
	]
},

{
	uppertextblock:[
	{
		textclass : "headertext2",
		textdata : data.string.p1text17,
	},
	{
		textclass : "animoptions animhover optpig",
		textid : "pig",
		textdata : data.string.p1text18,
	},
	{
		textclass : "animoptions animhover opthorse",
		textid : "horse",
		textdata : data.string.p1text9,
	},
	{
		textclass : "animoptions animhover optelephant",
		textid : "elephant",
		textdata : data.string.p1text12,
	},
	{
		textclass : "animoptions animhover optdog",
		textid : "dog",
		textdata : data.string.p1text8,
	},
	{
		textclass : "animoptions animhover optkangaroo",
		textid : "kangaroo",
		textdata : data.string.p1text19,
	},
	{
		textclass : "animoptions animhover optgoat",
		textid : "goat",
		textdata : data.string.p1text20,
	},
	{
		textclass : "animoptions animhover optcow",
		textid : "cow",
		textdata : data.string.p1text21,
	},
	{
		textclass : "animoptions animhover optgiraffee",
		textid : "giraffe",
		textdata : data.string.p1text13,
	},
	{
		textclass : "animoptions animhover optdeer",
		textid : "deer",
		textdata : data.string.p1text11,
	},
	{
		textclass : "animoptions animhover optlion",
		textid : "lion",
		textdata : data.string.p1text14,
	},
	{
		textclass : "animoptions animhover optpanda",
		textid : "panda",
		textdata : data.string.p1text22,
	},
	{
		textclass : "animoptions animhover optcamel",
		textid : "camel",
		textdata : data.string.p1text6,
	},
	],

	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "drawnimage",
			imgid : "drawnimage"
		},

		],
	}
	],
},

{
	uppertextblock:[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "goodfont",
		textclass : "page3left",
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "goodfont",
		textclass : "page3right",
	},
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "drawnimage2",
			imgid : "drawnimage"
		},
		{
			imgclass: "origimage",
		},
		],
	}
	],
}
];

$(function(){

	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;
	var animalimage;
	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program
	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	function generalTemplate(){

		var html = template(content[countNext]);

		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch (countNext) {
			case 0:
			play_audio(sound_c_click, true);
			$nextBtn.hide(0);

			$('.selectanimal').click(function(){
				testanimal = $(this).attr("id");
				$nextBtn.trigger("click");
			});

			break;

			case 1:
			/*canvas drawing*/
			var canvas = document.getElementById('canvasInAPerfectWorld');
			var outlineImage = document.getElementById(testanimal);
			context = document.getElementById('canvasInAPerfectWorld').getContext("2d");
			context.fillstyle=("#FFF");
			var canvasheight = Math.round($board.height() * 0.75);
			var canvaswidth = Math.round($board.width() * 0.7);
			$('#canvasInAPerfectWorld').addClass('normalbrush');

			canvas.height = canvasheight;
			canvas.width = canvaswidth;
			context.drawImage(outlineImage, 0, 0, canvaswidth, canvasheight);
			var color1 = "#FFBE54";
			var color2 = "#8D6731";
			var color3 = "#FF5C00";
			var color4 = "#F91D00";
			var color5 = "#841700";
			var color6 = "#00DEDE";
			var color7 = "#00B6D6";
			var color8 = "#007CB0";
			var color9 = "#782B96";
			var color10 = "#08682F";
			var color11 = "#CB8DA4";
			var color12 = "#6F6F6F";
			var color13 = "#3E3E3E";
			var color14 = "#000000";
			var color15 = "#09B54D";
			var colorEraser = "#FFF";

			var curColor = color1;

			var smallcur = 0.02;
			var normalcur = 0.04;
			var bigcur = 0.08;
			var largecur = 0.1;

			var radius = normalcur;

			var curSize = normalcur;

			$('.colors').click(function(){
				curColor = eval($(this).attr('class').split(' ')[1]);
				if(curSize == smallcur){
					$('#canvasInAPerfectWorld').removeClass();
					$('#canvasInAPerfectWorld').addClass('smallbrush');
				}
				else if(curSize == normalcur){
					$('#canvasInAPerfectWorld').removeClass();
					$('#canvasInAPerfectWorld').addClass('normalbrush');
				}
				else if(curSize == bigcur){
					$('#canvasInAPerfectWorld').removeClass();
					$('#canvasInAPerfectWorld').addClass('bigbrush');
				}
				else{
					$('#canvasInAPerfectWorld').removeClass();
					$('#canvasInAPerfectWorld').addClass('largebrush');
				}
			});

			$('.smallsize').click(function(){
				curSize = smallcur;
				$('#canvasInAPerfectWorld').removeClass();
				$('#canvasInAPerfectWorld').addClass('smallbrush');
			});

			$('.normalsize').click(function(){
				curSize = normalcur;
				$('#canvasInAPerfectWorld').removeClass();
				$('#canvasInAPerfectWorld').addClass('normalbrush');
			});

			$('.bigsize').click(function(){
				curSize = bigcur;
				$('#canvasInAPerfectWorld').removeClass();
				$('#canvasInAPerfectWorld').addClass('bigbrush');
			});

			$('.largesize').click(function(){
				curSize = largecur;
				$('#canvasInAPerfectWorld').removeClass();
				$('#canvasInAPerfectWorld').addClass('largebrush');
			});

			$('.eraser').click(function(){
				curColor = colorEraser;
				curSize = bigcur;
				$('#canvasInAPerfectWorld').removeClass();
				$('#canvasInAPerfectWorld').addClass('erasercur');
			});


		/* There are array to store X position, Y position, color and size of the points(basically lines) that are drawn
		 * Get Mouse position return the current mouse position,
		 * variable paint act as flag in redraw() function to draw a point or a line
		 * boolean variable dragging is pushed to clickDraw array which is used to draw entire canvas called during resize
		 * color is also stored in clickColor array
		 */
		 var clickColor = new Array();
		 var clickSize = new Array();

		 function getMousePos(canvas, evt) {
		 	var rect = canvas.getBoundingClientRect();
		 	return {
		 		x: evt.clientX - rect.left,
		 		y: evt.clientY - rect.top
		 	};
		 }

		 $('#canvasInAPerfectWorld').mousedown(function(e){
		 	paint++;
		 	var mousePos = getMousePos(canvas, e);
		 	addClick(mousePos.x, mousePos.y);
		 	redraw(e);
		 });

		 $('#canvasInAPerfectWorld').mousemove(function(e){

		 	if(paint){
		 		paint++;
		 		var mousePos = getMousePos(canvas, e);
		 		addClick(mousePos.x, mousePos.y, true);
		 		redraw(e);
		 	}
		 });

		 $('#canvasInAPerfectWorld').mouseup(function(e){
		 	paint = 0;
		 });

		 $('#canvasInAPerfectWorld').mouseleave(function(e){
		 	paint = 0;
		 });


		 var clickX = new Array();
		 var clickY = new Array();
		 var clickDrag = new Array();
		 var paint = 0;

		 function addClick(x, y, dragging)
		 {
		 	clickX.push(x);
		 	clickY.push(y);
		 	clickDrag.push(dragging);
		 	clickColor.push(curColor);

		 }

		 /* used to paint on canvas, canvas is not cleared but its state is saved everytime function is called */
		 function redraw(e){
		 	clickSize.push(curSize*canvasheight);
		  // context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas

		  context.strokeStyle = "#df4b26";
		  context.lineJoin = "round";
		  context.lineWidth = 5;
		  var mousePos = getMousePos(canvas, e);

		  context.beginPath();
		  if(paint>1){
		  		// 2*clickX[n-1] - clickX[n-2] is used to draw line of correct length while dragging
		  		context.moveTo(2*clickX[clickX.length-1]-clickX[clickX.length-2], 2*clickY[clickY.length-1]-clickY[clickY.length-2]);
		  	}else{
		  		//used to draw dot
		  		context.moveTo(clickX[clickX.length-1]-1, clickY[clickY.length-1]-1);
		  	}
		  	// line drawn to current mouse position
		  	context.lineTo(mousePos.x, mousePos.y);
		  	context.closePath();
		  	context.strokeStyle = curColor;
		  	context.lineWidth = curSize*canvasheight;
		  	context.stroke();
		  	context.drawImage(outlineImage, 0, 0, canvaswidth, canvasheight);
		  	context.save();
		  }
		  /*canvas drawing end*/

		  /* Resize clears the canvas and used all the array variables to draw all the lines */
		  $(window).resize(function() {
		  	canvasheight = Math.round($board.height() * 0.75);
		  	canvaswidth = Math.round($board.width() * 0.7);
		  	/* factor variable used to scale the size and location of paint into canvas when resized */
		  	var factorY = canvasheight/canvas.height;
		  	var factorX = canvaswidth/canvas.width;
		  	/* variable used to resize the width of dot or paint stroke */
		  	var bigfactor = (factorY>factorX)?factorY:factorX;
		  	canvas.height = canvasheight;
		  	canvas.width = canvaswidth;
		  	context.drawImage(outlineImage, 0, 0, canvaswidth, canvasheight);
		context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
		context.strokeStyle = "#df4b26";
		context.lineJoin = "round";
		context.lineWidth = 5;

		//loop through all elements and draw lines
		for(var i=0; i < clickX.length; i++) {
			clickX[i]*=factorX;
			clickY[i]*=factorY;
			clickSize[i]*= bigfactor;
			context.beginPath();
			if(clickDrag[i] && i){
				context.moveTo(clickX[i-1], clickY[i-1]);
			}else{
				context.moveTo((clickX[i]-1), (clickY[i]-1));
			}
			context.lineTo(clickX[i], clickY[i]);
			context.closePath();
			context.strokeStyle = clickColor[i];
			context.lineWidth = clickSize[i];
			context.stroke();
		}
		context.drawImage(outlineImage, 0, 0, canvaswidth, canvasheight);
		});

		  break;

		  case 2:
			play_audio(sound_c_1, true);
		  $nextBtn.hide(0);
		  $('#drawnimage').attr("src",animalimage);

		  setTimeout(function(){
              $('.animoptions').mouseenter(function(){
                  var audiofile = new buzz.sound((audioPath + "hover/"+$(this).attr('id')+".ogg"));
                  play_audio(audiofile, true);
              });
		  },5000);



		  $('.animoptions').click(function(){
		  	if(testanimal == $(this).attr("id")){
		  		$(this).css("background","#51BD6C");
		  		$('.animoptions').css('pointer-events', 'none');
		  		$nextBtn.show(0);
		  		var thiselem = $(this);
		  		var positionElem = thiselem.position();
		  		var newTop = positionElem.top/$('.contentblock').height() *100 + 11;
		  		var newLeft = positionElem.left/$('.contentblock').width() *100 + 5;
          play_correct_incorrect_sound(1);
		  		$(".contentblock").append(
		  			"<img style='width:5%; position:absolute; top:"+newTop+"%; left:"+newLeft+"%;' src='images/correct.png'/>"
		  			);
		  	}
		  	else{
		  		var thiselem = $(this);
		  		$(this).css('pointer-events', 'none');
		  		var positionElem = thiselem.position();
		  		var newTop = positionElem.top/$('.contentblock').height() *100 + 11;
		  		var newLeft = positionElem.left/$('.contentblock').width() *100 + 5;
		  		thiselem.css("background","rgb(242, 82, 85)");
		  		thiselem.removeClass("animhover");
          play_correct_incorrect_sound(0);
		  		$(".contentblock").append(
		  			"<img style='width:5%; position:absolute; top:"+newTop+"%; left:"+newLeft+"%;' src='images/wrong.png'/>"
		  			);
		  	}
		  });
		  break;

		  case 3:
  	 		$prevBtn.css('display', 'none');
  	 		$nextBtn.css('display', 'none');
			var aniname = new buzz.sound((audioPath + "hover/" + testanimal +".ogg"));
		  var seltext;
		  var domflag;
		  $('#drawnimage').attr("src",animalimage);
		  if (testanimal == "pig"){
		 	seltext = data.string.p1text18;
		 	domflag = true;
		  }
		  else if (testanimal == "horse"){
		 	seltext = data.string.p1text9;
		 	domflag = true;
		  }
		  else if (testanimal == "elephant"){
		 	seltext = data.string.p1text12;
		  }
		  else if (testanimal == "dog"){
		 	seltext = data.string.p1text8;
		 	domflag = true;
		  }
		  else if (testanimal == "kangaroo"){
		 	seltext = data.string.p1text19;
		  }
		  else if (testanimal == "goat"){
		 	seltext = data.string.p1text20;
		 	domflag = true;
		  }
		  else if (testanimal == "cow"){
		 	seltext = data.string.p1text21;
		 	domflag = true;
		  }
		  else if (testanimal == "giraffe"){
		 	seltext = data.string.p1text13;
		  }
		  else if (testanimal == "deer"){
		 	seltext = data.string.p1text11;
		  }
		  else if (testanimal == "lion"){
		 	seltext = data.string.p1text14;
		  }
		  else if (testanimal == "panda"){
		 	seltext = data.string.p1text22;
		  }
		  else if (testanimal == "camel"){
		 	seltext = data.string.p1text6;
		  }

		  $('.page3left').html(data.string.p1text23 + "<span style='color: #4770B9; font-size: 1.4em;''>" + seltext + '</span>');

		  if(domflag == true){
				 // play_audiochain(sound_c_2, aniname, sound_c_3);
				$('.page3right').html(data.string.p1text34);
			}
		else{
			// play_audiochain(sound_c_2, aniname, sound_c_3);
			$('.page3right').html(data.string.p1text33);
		}

    buzz.all().stop();
    animal_clk.play();
    animal_clk.bindOnce('ended', function(){
      buzz.all().stop();
      aniname.play();
      aniname.bindOnce('ended', function(){
        if(domflag == true){
          play_audio(domestic_sound, 0);
        }else{
          play_audio(wild_sound, 0)
        }
      });
    });
		  /*$('.page3left').html(data.string.p1text23 + "<span style='color: #4770B9; font-size: 1.4em;''>" + testanimal + '</span>');
		  if(testanimal == "elephant" || testanimal == "kangaroo" || testanimal == "giraffee" || testanimal == "deer" || testanimal == "lion" || testanimal == "camel" || testanimal == "panda" )
		  	$('.page3right').html(data.string.p1text33);
		  else
		  	$('.page3right').html(data.string.p1text34);*/
		  $('.origimage').attr("src",imgpath + "images/colouring/" + testanimal +"real.jpg");
              // navigationcontroller();

      break;
		  default:
		  break;
		}
	}
	function play_audio(audio, next){
		buzz.all().stop();
		audio.play();
		audio.bindOnce('ended', function(){
			if(!next)
				navigationcontroller();
				$("#audiorep").addClass("enableclick");
		});
	}

	function play_audiochain(audio, next, again){
		buzz.all().stop();
		audio.play();
		audio.bindOnce('ended', function(){
			next.play();
		});
		next.bindOnce('ended', function(){
			again.play();
		});
		again.bindOnce('ended', function(){
			navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}
	$nextBtn.on("click", function(){
		if(countNext == 1){
			var canvas = document.getElementById("canvasInAPerfectWorld");
			animalimage = canvas.toDataURL();

		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

 var sound_c_1 = new buzz.sound((audioPath + "p2_s0.ogg"));
 var sound_c_2 = new buzz.sound((audioPath + "p2_s0_1.ogg"));
 var sound_c_3 = new buzz.sound((audioPath + "p2_s1.ogg"));
 var sound_c_4 = new buzz.sound((audioPath + "p2_s1_1.ogg"));
 var camera = new buzz.sound((audioPath + "../camera.ogg"));

var content=[
	//1st slide
	{
		uppertextblock:[
		{
			textclass : "canyouidentify",
			textdata : data.string.p1text2,
		},
		{
			textclass : "alertbox",
			textdata : data.string.congrat,
		},
		{
			textclass : "cowans onetext",
			textdata : data.string.p1text21,
		},
		{
			textclass : "dogans twotext",
			textdata : data.string.p1text8,
		},
		{
			textclass : "goatans threetext",
			textdata : data.string.p1text20,
		},
		{
			textclass : "sheepans fourtext",
			textdata : data.string.p1text10,
		},
		{
			textclass : "horseans fivetext",
			textdata : data.string.p1text9,
		},
		{
			textclass : "pigans sixtext",
			textdata : data.string.p1text18,
		},
		],
		extradivarray: [
		{
			extradiv: "answercontainback",
		},
		{
			extradiv: "answercontainer",
		}
		],
		imageblock: [
		{
			imagetoshow: [

			{
				imgclass: "wilddomback",
				imgsrc : imgpath + "images/find/bg01.jpg"
			},
			{
				imgclass: "animal fox ",
				imgsrc : imgpath + "images/find/fox.png"
			},
			{
				imgclass: "animal cow ",
				imgsrc : imgpath + "images/find/cow.png"
			},
			{
				imgclass: "animaldone cowans",
				imgsrc : imgpath + "images/find/cow.png"
			},
			{
				imgclass: "animal deer ",
				imgsrc : imgpath + "images/find/deer.png"
			},
			{
				imgclass: "animal dog ",
				imgsrc : imgpath + "images/find/dog.png"
			},
			{
				imgclass: "animaldone dogans",
				imgsrc : imgpath + "images/find/dog.png"
			},
			{
				imgclass: "animal elephant ",
				imgsrc : imgpath + "images/find/elephant.png"
			},
			{
				imgclass: "animal giraffe ",
				imgsrc : imgpath + "images/find/giraffe.png"
			},
			{
				imgclass: "animal goat ",
				imgsrc : imgpath + "images/find/goat.png"
			},
			{
				imgclass: "animaldone goatans",
				imgsrc : imgpath + "images/find/goat.png"
			},
			{
				imgclass: "animal horse ",
				imgsrc : imgpath + "images/find/horse.png"
			},
			{
				imgclass: "animaldone horseans",
				imgsrc : imgpath + "images/find/horse.png"
			},
			{
				imgclass: "animal tiger ",
				imgsrc : imgpath + "images/find/tiger.png"
			},
			{
				imgclass: "animal pig ",
				imgsrc : imgpath + "images/find/pig.png"
			},
			{
				imgclass: "animaldone pigans",
				imgsrc : imgpath + "images/find/pig.png"
			},
			{
				imgclass: "animal rhino ",
				imgsrc : imgpath + "images/find/rhino.png"
			},
			{
				imgclass: "animal sheep ",
				imgsrc : imgpath + "images/find/ship.png"
			},
			{
				imgclass: "animaldone sheepans",
				imgsrc : imgpath + "images/find/ship.png"
			},
			{
				imgclass: "camera",
				imgsrc : imgpath + "images/camera.png"
			},

			],
		}
		]
	},

	//2nd slide
	{
		uppertextblock:[
		{
			textclass : "canyouidentify2",
			textdata : data.string.p1text3,
		},
		{
			textclass : "alertbox",
			textdata : data.string.congratwild,
		},
		{
			textclass : "foxans onetext",
			textdata : data.string.fox,
		},
		{
			textclass : "elephantans twotext",
			textdata : data.string.p1text12,
		},
		{
			textclass : "tigerans threetext",
			textdata : data.string.p1text16,
		},
		{
			textclass : "deerans fourtext",
			textdata : data.string.p1text11,
		},
		{
			textclass : "giraffeans fivetext",
			textdata : data.string.p1text13,
		},
		{
			textclass : "rhinoans sixtext",
			textdata : data.string.p1text15,
		},
		],
		extradivarray: [
		{
			extradiv: "answercontainback",
		},
		{
			extradiv: "answercontainer",
		}
		],
		imageblock: [
		{
			imagetoshow: [

			{
				imgclass: "wilddomback",
				imgsrc : imgpath + "images/find/bg01.jpg"
			},
			{
				imgclass: "animal fox ",
				imgsrc : imgpath + "images/find/fox.png"
			},
			{
				imgclass: "animal cow ",
				imgsrc : imgpath + "images/find/cow.png"
			},
			{
				imgclass: "animaldone foxans",
				imgsrc : imgpath + "images/find/fox.png"
			},
			{
				imgclass: "animal deer ",
				imgsrc : imgpath + "images/find/deer.png"
			},
			{
				imgclass: "animal dog ",
				imgsrc : imgpath + "images/find/dog.png"
			},
			{
				imgclass: "animaldone elephantans",
				imgsrc : imgpath + "images/find/elephant.png"
			},
			{
				imgclass: "animal elephant ",
				imgsrc : imgpath + "images/find/elephant.png"
			},
			{
				imgclass: "animal giraffe ",
				imgsrc : imgpath + "images/find/giraffe.png"
			},
			{
				imgclass: "animal goat ",
				imgsrc : imgpath + "images/find/goat.png"
			},
			{
				imgclass: "animaldone tigerans",
				imgsrc : imgpath + "images/find/tiger.png"
			},
			{
				imgclass: "animal horse ",
				imgsrc : imgpath + "images/find/horse.png"
			},
			{
				imgclass: "animaldone giraffeans",
				imgsrc : imgpath + "images/find/giraffe.png"
			},
			{
				imgclass: "animal tiger ",
				imgsrc : imgpath + "images/find/tiger.png"
			},
			{
				imgclass: "animal pig ",
				imgsrc : imgpath + "images/find/pig.png"
			},
			{
				imgclass: "animaldone rhinoans",
				imgsrc : imgpath + "images/find/rhino.png"
			},
			{
				imgclass: "animal rhino ",
				imgsrc : imgpath + "images/find/rhino.png"
			},
			{
				imgclass: "animal sheep ",
				imgsrc : imgpath + "images/find/ship.png"
			},
			{
				imgclass: "animaldone deerans",
				imgsrc : imgpath + "images/find/deer.png"
			},
			{
				imgclass: "camera",
				imgsrc : imgpath + "images/camera.png"
			},
			],
		}
		]
	},
	];

	$(function(){
		var $board = $(".board");
		
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
		var countNext = 0;
		var total_page = 0;
		var $total_page = content.length;

		var total_page = 0;
		loadTimelineProgress($total_page, countNext + 1);


	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		var anim_count = 0;

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		var fin;
		switch (countNext) {
			case 0:
			play_audio(sound_c_1, true);
			$(".cowans, .dogans, .goatans, .sheepans, .horseans, .pigans, .alertbox").hide(0);
			$nextBtn.hide(0);
			fin = 0;

			$(".cow").one("click",function(){
				play_audio(camera, true);
				$(this).attr("done","true");
				$(".cowans").show(0);
				cowflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".dog").one("click",function(){
				play_audio(camera, true);
				$(".dogans").show(0);
				dogflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".goat").one("click",function(){
				play_audio(camera, true);
				$(".goatans").show(0);
				goatflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".sheep").one("click",function(){
				play_audio(camera, true);
				$(".sheepans").show(0);
				sheepflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".horse").one("click",function(){
				play_audio(camera, true);
				$(".horseans").show(0);
				horseflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".pig").one("click",function(){
				play_audio(camera, true);
				$(".pigans").show(0);
				pigflag = true;
				fin++;
				activitycomplete(fin);
			});
			break;

			case 1:
			play_audio(sound_c_3, true);
			ole.footerNotificationHandler.hideNotification();
			$(".foxans, .elephantans, .tigerans, .deerans, .giraffeans, .rhinoans, .alertbox").hide(0);
			fin = 0;

			$(".fox").one("click",function(){
				play_audio(camera, true);
				$(".foxans").show(0);
				foxflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".elephant").one("click",function(){
				play_audio(camera, true);
				$(".elephantans").show(0);
				elephantflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".tiger").one("click",function(){
				play_audio(camera, true);
				$(".tigerans").show(0);
				tigerflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".deer").one("click",function(){
				play_audio(camera, true);
				$(".deerans").show(0);
				deerflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".giraffe").one("click",function(){
				play_audio(camera, true);
				$(".giraffeans").show(0);
				giraffeeflag = true;
				fin++;
				activitycomplete(fin);
			});

			$(".rhino").one("click",function(){
				play_audio(camera, true);
				$(".rhinoans").show(0);
				rhinoflag = true;
				fin++;
				activitycomplete(fin);
			});
			break;
		}
	}

	function play_audio(audio, next){
		audio.play();
		audio.bindOnce('ended', function(){
			if(!next)
				navigationcontroller();
				$("#audiorep").addClass("enableclick");
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	function activitycomplete(fin){
		$('.animal, .wilddomback').css("opacity","0");
		setTimeout( function(){
			$('.animal, .wilddomback').css("opacity","1");
		}  , 100 );
		if(fin == 6 && countNext == 0){
			$(".alertbox").show(0);
			play_audio(sound_c_2);
		}
		else if(fin == 6 && countNext == 1){
			$(".alertbox").show(0);
			play_audio(sound_c_4);
		}
	}


	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

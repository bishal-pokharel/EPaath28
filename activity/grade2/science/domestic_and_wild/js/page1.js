var imgpath = $ref + "/";
var audioPath;
if($lang == "en")
 audioPath = $ref + '/sound/eng/';
else if($lang == "np")
 audioPath = $ref + '/sound/nep/';

 var sound_c_1 = new buzz.sound((audioPath + "p1_s0.ogg"));
 var sound_c_2 = new buzz.sound((audioPath + "p1_s1.ogg"));
 var sound_c_3 = new buzz.sound((audioPath + "p1_s2.ogg"));
 var sound_c_4 = new buzz.sound((audioPath + "p1_s3.ogg"));
 var sound_c_5 = new buzz.sound((audioPath + "p1_s4.ogg"));
 var sound_c_6 = new buzz.sound((audioPath + "p1_s5.ogg"));
 var sound_c_7 = new buzz.sound((audioPath + "p1_s6.ogg"));
 var seone = new buzz.sound((audioPath + "p1_s1/buffalo.ogg"));
var content=[
	{
		//starting page
		additionalclasscontentblock : 'backgroundss',
		contentblocknocenteradjust : true,
		uppertextblock : [
		{
			textclass : "firsttitle",
			textdata : data.string.title
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
			imgclass : "cat",
			imgsrc : imgpath + "images/cat.gif"
			},
			{
			imgclass : "tiger",
			imgsrc : imgpath + "images/tiger.gif"
			}
			],
		}
		]
	},

	//1st slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text4,
		}
		],
		flipcontainer:[
		{
			sectioncontainer : "section seone",
			imgsrc : imgpath + "images/domestic/buffalo.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section setwo",
			imgsrc : imgpath + "images/domestic/camel.png",
			animalname : data.string.p1text6,
		},
		{
			sectioncontainer : "section sethree",
			imgsrc : imgpath + "images/domestic/cat6.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section sefour",
			imgsrc : imgpath + "images/domestic/dog.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section sefive",
			imgsrc : imgpath + "images/domestic/horse.png",
			animalname : data.string.p1text9,

		},
		{
			sectioncontainer : "section sesix",
			imgsrc : imgpath + "images/domestic/sheep02.png",
			animalname : data.string.p1text10,
		},
		],
	},

	//2nd slide
	{
		uppertextblock:[
		{
			textclass : "domesticheader",
			textdata : data.string.p1text25,
		},
		{
			textclass : "domestictext1",
			textdata : data.string.p1text27,
		},
		{
			textclass : "domestictext2",
			textdata : data.string.p1text28,
		},
		{
			textclass : "domestictext3",
			textdata : data.string.p1text29,
		},
		],
		flipcontainer:[
		{
			sectioncontainer : "section2 se2one",
			imgsrc : imgpath + "images/domestic/buffalo.png",
			animalname : data.string.p1text5,
		},
		{
			sectioncontainer : "section2 se2two",
			imgsrc : imgpath + "images/domestic/camel.png",
			animalname : data.string.p1text6,
		},
		{
			sectioncontainer : "section2 se2three",
			imgsrc : imgpath + "images/domestic/cat6.png",
			animalname : data.string.p1text7,
		},
		{
			sectioncontainer : "section2 se2four",
			imgsrc : imgpath + "images/domestic/dog.png",
			animalname : data.string.p1text8,
		},
		{
			sectioncontainer : "section2 se2five",
			imgsrc : imgpath + "images/domestic/horse.png",
			animalname : data.string.p1text9,

		},
		{
			sectioncontainer : "section2 se2six",
			imgsrc : imgpath + "images/domestic/sheep02.png",
			animalname : data.string.p1text10,
		},
		],
	},

	//3rd slide
	{
		uppertextblock:[
		{
			textclass : "headertext",
			textdata : data.string.p1text4,
		}
		],

		flipcontainer:[
		{
			sectioncontainer : "section seone",
			imgsrc : imgpath + "images/wild/deer.png",
			animalname : data.string.p1text11,
		},
		{
			sectioncontainer : "section setwo",
			imgsrc : imgpath + "images/wild/elephant.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section sethree",
			imgsrc : imgpath + "images/wild/giraffe.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section sefour",
			imgsrc : imgpath + "images/wild/lion.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section sefive",
			imgsrc : imgpath + "images/wild/rhinoceros.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section sesix",
			imgsrc : imgpath + "images/wild/tiger01.png",
			animalname : data.string.p1text16,
		},
		],
	},

	//4th slide
	{
		uppertextblock:[
		{
			textclass : "wildheader",
			textdata : data.string.p1text26,
		},
		{
			textclass : "domestictext1",
			textdata : data.string.p1text30,
		},
		{
			textclass : "domestictext2",
			textdata : data.string.p1text31,
		},
		{
			textclass : "domestictext3",
			textdata : data.string.p1text32,
		},
		],
		flipcontainer:[
		{
			sectioncontainer : "section2 se2one",
			imgsrc : imgpath + "images/wild/deer.png",
			animalname : data.string.p1text11,
		},
		{
			sectioncontainer : "section2 se2two",
			imgsrc : imgpath + "images/wild/elephant.png",
			animalname : data.string.p1text12,
		},
		{
			sectioncontainer : "section2 se2three",
			imgsrc : imgpath + "images/wild/giraffe.png",
			animalname : data.string.p1text13,
		},
		{
			sectioncontainer : "section2 se2four",
			imgsrc : imgpath + "images/wild/lion.png",
			animalname : data.string.p1text14,
		},
		{
			sectioncontainer : "section2 se2five",
			imgsrc : imgpath + "images/wild/rhinoceros.png",
			animalname : data.string.p1text15,
		},
		{
			sectioncontainer : "section2 se2six",
			imgsrc : imgpath + "images/wild/tiger01.png",
			animalname : data.string.p1text16,
		},
		],
	},
	];

	$(function(){
		var $board = $(".board");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;
		var total_page = 0;

		var $total_page = content.length;

		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();


		loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	//controls the navigational state of the program

	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var clickcount = 0;

		$board.html(html);

		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch (countNext) {
			case 0:
				play_audio(sound_c_1);
			break;
			case 1:
			play_audio(sound_c_2, true);
			$(".contentblock").css("background","#5FC5CE");
			$nextBtn.hide(0);
      $(".card").one("click",function(){
        sound_c_2.stop();
        var audiofile = new buzz.sound((audioPath + "p1_s1/"+$(this).parent().attr('class').split(' ')[1]+".ogg"));
        play_audio(audiofile, true);

        $(this).toggleClass("flipped");
        clickcount++;
        if(clickcount == 6)
          $nextBtn.show(0);
      });
			break;
			case 2:
      play_audio(sound_c_3);
 			$nextBtn.hide(0);
			break;
			case 3:
      play_audio(sound_c_2, true);
			$(".contentblock").css("background","#C8D0F5");
			$nextBtn.hide(0);
      $(".card").one("click",function(){
        sound_c_2.stop();
        var audiofile = new buzz.sound((audioPath + "p1_s3/"+$(this).parent().attr('class').split(' ')[1]+".ogg"));
        play_audio(audiofile, true);

        $(this).toggleClass("flipped");
        clickcount++;
        if(clickcount == 6)
          $nextBtn.show(0);
      });
			break;
			case 4:
      play_audio(sound_c_5);
      $(".front1").css('display','none');
			$(".contentblock").css("background","#C8D0F5");
			break;
		}
	}

	// Handlebars.registerHelper('listItem', function (from, to, context, options){
	    // var item = "";
	    // for (var i = from, j = to; i <= j; i++) {
	        // item = item + options.fn(context[i]);
	    // }
	    // return item;
	// });
	function play_audio(audio, next){
		audio.play();
		audio.bindOnce('ended', function(){
			if(!next)
				navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
    buzz.all().stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
    buzz.all().stop();
		templateCaller();
	});

	$prevBtn.on("click", function(){
    buzz.all().stop();
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	templateCaller();

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

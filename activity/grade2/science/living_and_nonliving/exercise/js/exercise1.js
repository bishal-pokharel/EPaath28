var imgpath = $ref + "/images/dragdrop/";
soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "ex.ogg"));

var no_of_draggable = 4; //no of draggable to display at a time1
Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content = [
    //slide 0
    {
        // contentblockadditionalclass: "simplebg",
        contentnocenteradjust: true,
        textblockadditionalclass: 'instruction',
        textblock: [{
            textdata: data.string.exercisehead,
            textclass: 'ques'
        }],
        // draggableblockadditionalclass: 'frac_ques',
        draggableblock: [{
            draggables: [{
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "dog.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "elephant.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "grass.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "tree.png"
            }, {
                draggableclass: "class_1 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "human.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "book.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "chair.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "cup.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "allimg",
                imgsrc: imgpath + "glass.png"
            }, {
                draggableclass: "class_2 sliding hidden",
                has_been_dropped : false,
                imgclass: "",
                imgsrc: imgpath + "tv.png"
            }]
        }],
        droppableblock: [{
            droppables: [{
                headerdata: data.string.e1,
                droppablecontainerclass: "",
                droppableclass: "drop_class_1",
                imgclass: "",

            }, {
                headerdata: data.string.e2,
                droppablecontainerclass: "",
                droppableclass: "drop_class_2",
                imgclass: "",

            }]
        }]
    }
];

var dummy_class = {
    draggableclass: "dummy_class hidden",
    imgclass: "",
    imgsrc: ""
};

/* Suffle content elements for draggable
 * add some dummy class so that flex behaves correctly
 * add position for first fixed no of draggables
 */
content[0].draggableblock[0].draggables.shufflearray();
for (var i = 1; i < no_of_draggable + 1; i++) {
    var asd = content[0].draggableblock[0].draggables[i - 1].draggableclass.split('"')[0].split('hidden');
    content[0].draggableblock[0].draggables[i - 1].draggableclass = asd[0] + 'position_' + i;
    content[0].draggableblock[0].draggables.push(dummy_class);
}


$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var no_egg = 0;
    var egg_count = false;

    var $total_page = 12;
    Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
    Handlebars.registerPartial("droppablecontent", $("#droppablecontent-partial").html());

    function navigationcontroller(islastpageflag) {
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
            alert("NavigationController : Hi Master, please provide a boolean parameter") :
            null;
    }

    var score = 0;
    /*random scoreboard eggs*/
    var wrngClicked = [false, false, false, false, false, false, false, false, false, false, false, false];

    var testin = new EggTemplate();
   
    testin.init(10);
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[0]);
        $board.html(html);

        $nextBtn.hide(0);
        $prevBtn.hide(0);
        countNext==0?sound_1.play():"";
        /*generate question no at the beginning of question*/
        switch (countNext) {
            default: $(".draggable").draggable({
                containment: "body",
                revert: "invalid",
                appendTo: "body",
                helper: "clone",
                zindex: 1000,
                start: function(event, ui) {
                    $(this).css({
                        "opacity": "0.5"
                    });
                    $(ui.helper).addClass("ui-draggable-helper");
                    $(ui.helper).removeClass("sliding");
                },
                stop: function(event, ui) {
                    $(this).css({
                        "opacity": "1"
                    });
                }
            });
            $('.drop_class_1').droppable({
                accept : ".draggable",
                hoverClass: "hovered",
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_1")) {
                        if (wrngClicked[countNext] == false) {
                            if(($(ui.draggable).data("dropped"))==false){
                                    testin.update(true);
                    		}
                        }
                        sound_1.stop();
                        play_correct_incorrect_sound(1);
                        handleCardDrop(event, ui, ".class_1", ".drop_class_1");
                    } else {
                        sound_1.stop();
                        play_correct_incorrect_sound(0);

                        if(($(ui.draggable).data("dropped"))==false){
	                        wrngClicked[countNext] = true;
	                        $("#egg" + countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
                                testin.update(false);
	                	}
                    }
                    if(($(ui.draggable).data("dropped"))==false){
                        countNext++;
                        $(ui.draggable).data("dropped",true);
                        if(countNext==10){
                            $nextBtn.show(0);
                            $nextBtn.click(function(){
                                testin.gotoNext();
                            });
                        }
                        else
                            testin.gotoNext();

                    }
                }
            });

            $('.drop_class_2').droppable({
                accept : ".draggable",
                hoverClass: "hovered",
                drop: function(event, ui) {
                    if (ui.draggable.hasClass("class_2")) {
                        sound_1.stop();
                        play_correct_incorrect_sound(1);

                        if (wrngClicked[countNext] == false) {
                        	if(($(ui.draggable).data("dropped"))==false){
                            testin.update(true);
		                    }
                        }
                        handleCardDrop(event, ui, ".class_2", ".drop_class_2");
                    } else {
                        sound_1.stop();
                        play_correct_incorrect_sound(0);

                        if(($(ui.draggable).data("dropped"))==false){
	                        wrngClicked[countNext] = true;
	                        testin.update(false);
	                	}
                    }
                    if(($(ui.draggable).data("dropped"))==false){
                        countNext++;
                        $(ui.draggable).data("dropped",true);
                        if(countNext==10){
                            $nextBtn.show(0);
                            $nextBtn.click(function(){
                                testin.gotoNext();
                            });
                        }
                        else
                            testin.gotoNext();
                    }
                }
            });

            function topLeftCalculator(count) {
                var top = count % 3;
                var factor = Math.floor(count / 3);
                var height = 0;
                var left = 0;
                switch (top) {
                    case 0:
                        height = 20;
                        left = (factor > 0) ? ((factor * 5) + 20) : 40;
                        break;
                    case 1:
                        height = 30;
                        left = (factor > 0) ? ((factor * 7) + 30) : 50;
                        break;
                    case 2:
                        height = 40;
                        left = (factor > 0) ? ((factor * 9) + 40) : 60;
                        break;
                }

                var returnNumber = [height, left];
                return returnNumber;

            }

            function handleCardDrop(event, ui, classname, droppedOn) {
                ui.draggable.draggable('disable');
                var dropped = ui.draggable;
                // var to count no. of divs in the droppable div
                var drop_index = $(droppedOn + ">div").length;
                var top_position = drop_index;
                // var lef_position = drop_index * 32;
                $(ui.draggable).removeClass("sliding");
                $(ui.draggable).detach().css({
                    "position": "relative",
                    "cursor": 'auto',
                    // "left" : lef_position + '%',
                    // "top" : top_position + '%',
                    "flex": "0 0 20%",
                    // "width": "30%",
                    "height": "auto",
                    "max-height": "35%"
                }).appendTo(droppedOn);
                var $newEntry = $(".draggableblock> .hidden").eq(0);

                var $draggable3;
                var $draggable2;
                var $draggable1;
                if (dropped.hasClass("position_4")) {
                    dropped.removeClass("position_4");
                    $draggable3 = $(".position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_3")) {
                    dropped.removeClass("position_3");
                    $draggable2 = $(".position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_2")) {
                    dropped.removeClass("position_2");
                    $draggable1 = $(".position_1");
                } else if (dropped.hasClass("position_1")) {
                    dropped.removeClass("position_1");
                }

                if ($draggable3 != null) {
                    $draggable3.removeClass("position_3").addClass("position_4");
                    $draggable3.removeClass('sliding');
                    setTimeout(function() {
                        $draggable3.addClass('sliding');
                    }, 1);
                }
                if ($draggable2 != null) {
                    $draggable2.removeClass("position_2").addClass("position_3");
                    $draggable2.removeClass('sliding');
                    setTimeout(function() {
                        $draggable2.addClass('sliding');
                    }, 1);
                }
                if ($draggable1 != null) {
                    $draggable1.removeClass("position_1").addClass("position_2");
                    $draggable1.removeClass('sliding');
                    setTimeout(function() {
                        $draggable1.addClass('sliding');
                    }, 1);
                }
                if ($newEntry != null) {
                    $newEntry.removeClass("hidden").addClass("position_1");
                }
                 if (no_egg == 12) {
                }
                egg_count = false;
            }
            break;
        }
    }


    function templateCaller() {
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // // call navigation controller
        // navigationcontroller();

        // call the template
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   
    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function() {
        countNext++;
        if (countNext == 1) {
            $('#score').html(score);
            $('[select=yes]').fadeTo(1000, 0).hide(0);
            $('.exefin').show(0);
            $('.contentblock').hide(0);
            $('.congratulation').show(0);
            $nextBtn.hide(0);
        }
    });

    // $refreshBtn.click(function(){
	// 	templateCaller();
	// });
	
	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        	previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

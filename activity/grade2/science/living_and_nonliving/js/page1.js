var imgpath = $ref + "/images/";
if($lang == "en"){
	soundAsset = $ref+"/sounds/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sounds/np/";
}
var sound_l_1 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p1_s9.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9];
var content=[
	{
	//slide 0
		additionalclasscontentblock: "ole-background-gradient-brick",
		uppertextblock:[
		{
			textclass : "centertext titletext",
			textdata : data.string.title,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "startcow",
				imgsrc : imgpath + "eating_grass.gif"
			},
			{
				imgclass: "starttable",
				imgsrc : imgpath + "table.png"
			},
			]
		}]
	},
	{
	//slide 1
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertextup cssfadein",
			textdata : data.string.p1text1,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "item bag forhover",
				imgsrc : imgpath + "bag.png"
			},
			{
				imgclass: "item bird forhover living",
				imgsrc : imgpath + "bird.png"
			},
			{
				imgclass: "item dog forhover living",
				imgsrc : imgpath + "dog.png"
			},
			{
				imgclass: "item fish forhover living",
				imgsrc : imgpath + "fish.png"
			},
			{
				imgclass: "item tv forhover",
				imgsrc : imgpath + "video.png"
			},
			{
				imgclass: "item football forhover",
				imgsrc : imgpath + "football.png"
			}
			]
		}]
	},
	{
	//slide 2
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertext cssfadein",
			textdata : data.string.p1text2,
		}
		],
	},
	{
	//slide 3
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertext cssfadein",
			textdata : data.string.p1text3,
		}
		],
	},
	{
	//slide 4
		additionalclasscontentblock: "ole-background-gradient-shore",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "lefttext cssfadein-notra",
			textdata : data.string.p1text4,
		},
		{
			textclass: "catspritecontainer spriteimage"
		}
		],
	},
	{
	//slide 5
		additionalclasscontentblock: "ole-background-gradient-shore",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "lefttext cssfadein-notra",
			textdata : data.string.p1text5,
		},
		{
			textclass: "plantspritecontainer spriteimage"
		}
		],
	},
	{
	//slide 6
		additionalclasscontentblock: "ole-background-gradient-shore",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "lefttext cssfadein-notra",
			textdata : data.string.p1text6,
		},
		{
			textclass: "elespritecontainer spriteimage"
		}
		],
	},
	{
	//slide 7
		additionalclasscontentblock: "ole-background-gradient-shore",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "lefttext cssfadein-notra",
			textdata : data.string.p1text7,
		},
		{
			textclass: "brespritecontainer spriteimage"
		}
		],
	},
	{
	//slide 8
		additionalclasscontentblock: "ole-background-gradient-shore",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "lefttext cssfadein-notra",
			textdata : data.string.p1text8,
		},
		{
			textclass: "repspritecontainer spriteimage"
		}
		],
	}
];

$(function(){
	var $board = $(".board");

	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
      		$nextBtn.delay(1000).show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);




		var div_ratio =$(".spriteimage").width()/$(".spriteimage").parent().width();
		$(".spriteimage").width(Math.round($(".spriteimage").parent().width()*div_ratio));

		switch(countNext){
			case 0:
			playaudio(sound_group_p1[0]);
			break;
			case 1:
			playaudio(sound_group_p1[1]);
			$nextBtn.hide(0);
			count = 0;
			$(".item").one("click",function(){

				$(this).removeClass("forhover");
				if($(this).hasClass("living")){
					play_correct_incorrect_sound(1);
					$(this).css({
						"border":"5px solid #7FEA4F",
						"border-radius":"15px",
					});
					count++;
				if(count == 3)
					$nextBtn.show(0);
				}
				else{
					play_correct_incorrect_sound(0);
					$(this).css({
						"border":"5px solid #B82C32",
						"border-radius":"15px",
					});
				}
			});
			break;
			case 2:
			playaudio(sound_group_p1[2]);
			break;
			case 3:
			playaudio(sound_group_p1[3]);
			break;
			case 4:
        $(".spriteimage").css("height","58%");
			playaudio(sound_group_p1[4]);
			break;
			case 5:
			playaudio(sound_group_p1[5]);
			break;
			case 6:
			playaudio(sound_group_p1[6]);
			break;
			case 7:
			playaudio(sound_group_p1[7]);
			break;
			case 8:
			playaudio(sound_group_p1[8]);
			break;
		}

	}

	function playaudio(sound_data){
		var playing = true;
			if(!playing){
				playaudio(sound_data);
			}
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext != 0)
				$prevBtn.show(0);
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					if(countNext != 1)
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

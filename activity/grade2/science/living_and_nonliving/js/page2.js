var imgpath = $ref + "/images/";
if($lang == "en"){
	soundAsset = $ref+"/sounds/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sounds/np/";
}
var sound_l_1 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_l_10 = new buzz.sound((soundAsset + "p2_s10.ogg"));
var sound_l_11 = new buzz.sound((soundAsset + "p2_s11.ogg"));
var yes1 = new buzz.sound((soundAsset + "yes1.ogg"));
var yes2 = new buzz.sound((soundAsset + "yes2.ogg"));
var yes3 = new buzz.sound((soundAsset + "yes3.ogg"));
var yes4 = new buzz.sound((soundAsset + "yes4.ogg"));
var yes5 = new buzz.sound((soundAsset + "yes5.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7, sound_l_8, sound_l_9, sound_l_10, sound_l_11];
var yes = [yes1, yes2, yes3, yes4, yes5];
var content=[
	{
	//slide 0
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertext cssfadein",
			textdata : data.string.p2text1,
		}
		],
	},
	{
	//slide 1
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			},
			]
		}]
	},
	{
	//slide 1
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			}
			]
		}]
	},
	{
	//slide 2
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},
		{
			textclass : "lefttext lt2",
			textdata : data.string.p2text3,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			}
			]
		}]
	},
	{
	//slide 3
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},
		{
			textclass : "lefttext lt2",
			textdata : data.string.p2text3,
		},
		{
			textclass : "lefttext lt3",
			textdata : data.string.p2text4,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			},
			]
		}]
	},
	{
	//slide 4
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},
		{
			textclass : "lefttext lt2",
			textdata : data.string.p2text3,
		},
		{
			textclass : "lefttext lt3",
			textdata : data.string.p2text4,
		},
		{
			textclass : "lefttext lt4",
			textdata : data.string.p2text5,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			},
			]
		}]
	},
	{
	//slide 5
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},
		{
			textclass : "lefttext lt2",
			textdata : data.string.p2text3,
		},
		{
			textclass : "lefttext lt3",
			textdata : data.string.p2text4,
		},
		{
			textclass : "lefttext lt4",
			textdata : data.string.p2text5,
		},
		{
			textclass : "lefttext lt5",
			textdata : data.string.p2text6,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			},
			]
		}]
	},
	{
	//slide 5
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			textclass : "animal",
			textdata : data.string.horse,
		},
		{
			textclass : "lefttext lt1",
			textdata : data.string.p2text2,
		},
		{
			textclass : "lefttext lt2",
			textdata : data.string.p2text3,
		},
		{
			textclass : "lefttext lt3",
			textdata : data.string.p2text4,
		},
		{
			textclass : "lefttext lt4",
			textdata : data.string.p2text5,
		},
		{
			textclass : "lefttext lt5",
			textdata : data.string.p2text6,
		},
		{
			textclass : "finaltext cssfadein",
			textdata : data.string.p2text11,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "horsecontainer",
				imgsrc : imgpath + "eating_grass.gif"
			},
			]
		}]
	},
	{
	//slide 0
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertextup cssfadein",
			textdata : data.string.p2text8,
		},
		{
			textclass : "text1",
			textdata : data.string.chick,
		},
		{
			textclass : "text2",
			textdata : data.string.dog,
		},
		{
			textclass : "text3",
			textdata : data.string.rose,
		},
		{
			textclass : "text4",
			textdata : data.string.fish,
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "image1",
				imgsrc : imgpath + "bird.png"
			},
			{
				imgclass: "image1",
				imgsrc : imgpath + "dog.png"
			},
			{
				imgclass: "image1",
				imgsrc : imgpath + "flower.png"
			},
			{
				imgclass: "image1",
				imgsrc : imgpath + "fish.png"
			},

			]
		}]
	},
	{
	//slide 0
		additionalclasscontentblock: "ole-background-gradient-biscuit",
		uppertextblock:[
		{
			textclass : "centertext cssfadein",
			textdata : data.string.dyk,
		}
		],
	},
	{
	//slide 5
		additionalclasscontentblock: "ole-background-gradient-mustard",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshighlight",
			textclass : "dyktext cssfadein-notra",
			textdata : data.string.p2text10,
		},

		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "plantcontainer cssfadein-notra",
				imgsrc : imgpath + "plant.png"
			}
			]
		}]
	},
];

$(function(){
	var $board = $(".board");
	
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;
	var total_page = 0;

	/*recalculateHeightWidth();*/

	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.delay(1000).show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch(countNext){
			case 0:
			playaudio(sound_group_p1[0]);
			break;
			case 1:
			playaudio(sound_group_p1[1]);
			break;
			case 2:
			playaudioyesno(sound_group_p1[2], yes[0]);
			$(".lt1").append('<span class="yestext"> '+data.string.yes1+'</span>');
			break;
			case 3:
			playaudioyesno(sound_group_p1[3], yes[1]);
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yestext"> '+data.string.yes2+'</span>');
			break;
			case 4:
			playaudioyesno(sound_group_p1[4], yes[2]);
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yesdone"> '+data.string.yes2+'</span>');
			$(".lt3").append('<span class="yestext"> '+data.string.yes3+'</span>');
			break;
			case 5:
			playaudioyesno(sound_group_p1[5], yes[3]);
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yesdone"> '+data.string.yes2+'</span>');
			$(".lt3").append('<span class="yesdone"> '+data.string.yes3+'</span>');
			$(".lt4").append('<span class="yestext"> '+data.string.yes4+'</span>');
			break;
			case 6:
			playaudioyesno(sound_group_p1[6], yes[4]);
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yesdone"> '+data.string.yes2+'</span>');
			$(".lt3").append('<span class="yesdone"> '+data.string.yes3+'</span>');
			$(".lt4").append('<span class="yesdone"> '+data.string.yes4+'</span>');
			$(".lt5").append('<span class="yestext"> '+data.string.yes5+'</span>');
			break;
			case 7:
			playaudio(sound_group_p1[7]);
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yesdone"> '+data.string.yes2+'</span>');
			$(".lt3").append('<span class="yesdone"> '+data.string.yes3+'</span>');
			$(".lt4").append('<span class="yesdone"> '+data.string.yes4+'</span>');
			$(".lt5").append('<span class="yesdone"> '+data.string.yes5+'</span>');
			$(".lt6").append('<span class="yestext"> '+data.string.yes6+'</span>');
			break;
			case 8:
			playaudio(sound_group_p1[8]);
			setTimeout(function(){
                $(".image1").eq(0).addClass("scaleup")
            },5000);
                setTimeout(function(){
                    $(".image1").eq(1).addClass("scaleup")
                },6000)
                setTimeout(function(){
                    $(".image1").eq(2).addClass("scaleup")
                },7000)
                setTimeout(function(){
                    $(".image1").eq(3).addClass("scaleup")
                },8000)
			$(".lt1").append('<span class="yesdone"> '+data.string.yes1+'</span>');
			$(".lt2").append('<span class="yesdone"> '+data.string.yes2+'</span>');
			$(".lt3").append('<span class="yesdone"> '+data.string.yes3+'</span>');
			$(".lt4").append('<span class="yesdone"> '+data.string.yes4+'</span>');
			$(".lt5").append('<span class="yesdone"> '+data.string.yes5+'</span>');
			$(".lt6").append('<span class="yesdone"> '+data.string.yes6+'</span>');
			break;
			case 9:
			playaudio(sound_group_p1[9]);
			break;
			case 10:
			playaudio(sound_group_p1[10]);
			break;
			case 11:
			playaudio(sound_group_p1[11]);
			break;
		}
	}
	function playaudio(sound_data){
		var playing = true;
			if(!playing){
				playaudio(sound_data);
			}
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext != 0)
				$prevBtn.show(0);
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}
	function playaudioyesno(sound_data, yesdata){
		var playing = true;
			if(!playing){
				playaudio(sound_data);
			}
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				yesdata.play();
			},500);
		});
		yesdata.bind('ended', function(){
			setTimeout(function(){
				if(countNext != 0)
				$prevBtn.show(0);
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
   

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

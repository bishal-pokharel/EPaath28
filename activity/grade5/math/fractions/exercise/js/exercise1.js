Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide 1
	{
		containscanvaselement: true,
		exerciseblock: [
		{
			textdata: data.string.ques1,
		}
		],
		questionblock:[
		{
			questionblockwrapperadditionalclass: 'current_selector',
			questionclass: "test",
			inputdata: '',
			fractioninput: 'fractioninput',
			inputclass_numerator: 'box_numerator digit_box_3 frac_1',
			inputclass_denominator: 'box_denominator digit_box_3 frac_2',
			buttondata: data.string.pctext1,
			buttonclass: "check_button",
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : ''
		}],
	},
	];

	$(function () {
		var $board    = $('.board');
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;

		/*for limiting the questions to 10*/
		var $total_page = 10;
		var preload;
		var timeoutvar = null;
		var current_sound;

		function init() {
			//specify type otherwise it will load assests as XHR
			manifest = [
				//images

				// sounds
				{id: "sound_0", src: soundAsset+"ex.ogg"},
			];
			preload = new createjs.LoadQueue(false);
			preload.installPlugin(createjs.Sound);//for registering sounds
			preload.on("progress", handleProgress);
			preload.on("complete", handleComplete);
			preload.on("fileload", handleFileLoad);
			preload.loadManifest(manifest, true);
		}
		function handleFileLoad(event) {
			// console.log(event.item);
		}
		function handleProgress(event) {
			$('#loading-text').html(parseInt(event.loaded*100)+'%');
		}
		function handleComplete(event) {
			$('#loading-wrapper').hide(0);
			//initialize varibales
			// call main function
			templateCaller();
		}
		//initialize
		init();

		function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var testin = new EggTemplate();

	 testin.init(10);

	 var arrayQues = [0,0,0,0,1,1,1,1,1,1];
	 var firstNo;
	 var firstNo2;
	 var secondNo;
	 var secondNo2;

	 	arrayQues.shufflearray();

	 	function generalTemplate() {
	 		var source = $("#general-template").html();
	 		var template = Handlebars.compile(source);
	 		var html = template(content[0]);
	 		$board.html(html);
	 		var testcount = 0;
	 		$(".hint1").html(data.string.hint);
            $nextBtn.hide(0);
            $prevBtn.hide(0);
			if(countNext==0){
				sound_player("sound_0");
			}

	 		if(arrayQues[countNext] == 0){
	 			$(".test").html(data.string.format1);
	 			Type1makeQuestion();
	 		}
	 		else{
	 			$(".test").html(data.string.format2);
	 			Type2makeQuestion();
	 		}

	 		splitintofractions($board);

	 		canvas = document.getElementById("canvas_fraction_1");
	 		canvas_2 = document.getElementById("canvas_fraction_2");

	 		ctx = canvas.getContext("2d");
	 		ctx_2 = canvas_2.getContext("2d");

	 		function init_canvas(){
	 			canvasheight = ($board.height() * 0.35);
	 			canvaswidth = ($board.width() * 0.30);

	 			canvas.height = canvasheight;
	 			canvas.width = canvaswidth;

	 			canvas_2.height = canvasheight;
	 			canvas_2.width = canvaswidth;

	 			ctx.clearRect(0, 0, canvaswidth, canvasheight);
	 			ctx_2.clearRect(0, 0, canvaswidth, canvasheight);
	 		}

	 		/* m, n for vertical grid, m2 and n2 for horizontal grid*/
	 		function draw_grids(canvasid, context, m , n, m2, n2, is_vertical, final_fill){
	 			context.clearRect(0, 0, canvasid.width, canvasid.height);
	 			context.beginPath();
	 			if(!final_fill){
	 				is_vertical?context.fillStyle = '#b44':context.fillStyle = '#44b';
	 				is_vertical?context.rect(0, 0, m*canvasid.width/n, canvasid.height):context.rect(0, 0, canvasid.width, m2*canvasid.height/n2);
	 				context.fill();
	 			} else {
	 				context.fillStyle = '#4b4';
	 				context.rect(0, 0, m*canvasid.width/n, m2*canvasid.height/n2);
	 				context.fill();
	 			}
	 			if( is_vertical){
	 				for (var i=0; i<n; i++){
	 					context.beginPath();
	 					context.strokeStyle = '#000';
	 					context.moveTo(i*canvasid.width/n, 0);
	 					context.lineTo(i*canvasid.width/n, canvasid.height);
	 					context.stroke();
	 				}
	 			} else {
	 				for (var i=0; i<n2; i++){
	 					context.beginPath();
	 					context.strokeStyle = '#000';
	 					context.moveTo(0, i*canvasid.height/n2);
	 					context.lineTo(canvasid.width, i*canvasid.height/n2);
	 					context.stroke();
	 				}
	 			}
	 		}

	 		function draw_grids_only(canvasid, context, m, n, m2, n2){
	 			for (var i=0; i<n; i++){
	 				context.beginPath();
	 				context.strokeStyle = '#000';
	 				context.moveTo(i*canvasid.width/n, 0);
	 				context.lineTo(i*canvasid.width/n, canvasid.height);
	 				context.stroke();
	 			}
	 			for (var i=0; i<n2; i++){
	 				context.beginPath();
	 				context.strokeStyle = '#000';
	 				context.moveTo(0, i*canvasid.height/n2);
	 				context.lineTo(canvasid.width, i*canvasid.height/n2);
	 				context.stroke();
	 			}
	 		}

	 		function save_variables( am1_c, an1_c, am2_c, an2_c, afull){
	 			m1_c = am1_c;
	 			n1_c = an1_c;
	 			m2_c = am2_c;
	 			n2_c = an2_c;
	 			full = afull;
	 		}

	 		$(window).resize(function() {
	 			init_canvas();
	 			draw_grids(canvas, ctx, m1_c , n1_c, m2_c, n2_c, true , full);
	 			draw_grids(canvas_2, ctx_2, m1_c , n1_c, m2_c, n2_c, false , full);
	 			draw_grids_only(canvas, ctx, m1_c , n1_c, m2_c, n2_c);
	 			draw_grids_only(canvas_2, ctx_2, m1_c , n1_c, m2_c, n2_c);
	 		});


	 		$('.congratulation').hide(0);
	 		$('.exefin').hide(0);
	 		$('.hint1').hide(0);

	 		$('#canvas_fraction_1').hide(0);
	 		$('#canvas_fraction_2').hide(0);

	 		$(".check_button").click(function(){
	 			if( (parseInt($('.frac_1').val())/ parseInt($('.frac_2').val()) ) == (firstNo*firstNo2) / (secondNo*secondNo2) ){
	 				$('.check_button').css("background", '#93C496');
	 				$('.digit_box_3').css("border", "2px solid #93C496");
	 				$('.digit_box_3').prop("disabled", true);
	 				testin.update(true);
	 				$nextBtn.show(0);
					play_correct_incorrect_sound(1);
	 			} else{
	 				$('.hint').removeClass('its_hidden');
	 				$('.check_button').css("background", '#E06666');
	 				testin.update(false);
					play_correct_incorrect_sound(0);
	 				if(arrayQues[countNext] == 0){
	 					$(".hint1").show(0);
	 				}
	 				else{
	 					$('#canvas_fraction_1').show(0);
	 					$('#canvas_fraction_2').show(0);
	 					init_canvas();
	 					save_variables(firstNo , secondNo, firstNo2, secondNo2, false);
	 					draw_grids(canvas, ctx, firstNo , secondNo, 1, 5, true , false);
	 					draw_grids(canvas_2, ctx_2, 2 , 3, firstNo2, secondNo2, false , false);
	 					go_next = 0;
	 					$('#canvas_fraction_1').css("left","5%");
	 					$('#canvas_fraction_2').css("left","60%");
	 					setTimeout(function(){
	 						$('#canvas_fraction_1').animate({
	 							'left':'32.5%'
	 						}, 2000);
	 						$('#canvas_fraction_2').animate({
	 							'left':'32.5%'
	 						}, 2000);
	 					}, 1000);
	 					setTimeout(function(){
	 						save_variables(firstNo , secondNo, firstNo2, secondNo2, true);
	 						draw_grids(canvas, ctx, firstNo , secondNo, firstNo2, secondNo2, true , true);
	 						draw_grids(canvas_2, ctx_2, firstNo , secondNo, firstNo2, secondNo2, false , true);
	 						draw_grids_only(canvas, ctx, firstNo , secondNo, firstNo2, secondNo2);
	 						draw_grids_only(canvas_2, ctx_2, firstNo , secondNo, firstNo2, secondNo2);
	 					}, 3000);

	 				}
	 			}
	 		});
	 	}

	 	function Type1makeQuestion(){
	 		firstNo = randomIntFromInterval(1, 10);
	 		secondNo = 1;

	 		firstNo2 = randomIntFromInterval(1, 10);
	 		secondNo2 = randomIntFromInterval(firstNo2, 13);

	 		$(".hint_a").html(firstNo);
	 		$(".hint_b").html(firstNo2);
	 		$(".hint_c").html(secondNo2);

	 		$(".ans_a").html(firstNo);
	 		$(".ans_b").html(firstNo2);
	 		$(".ans_c").html(secondNo2);
	 }

	 function Type2makeQuestion(){
	 	firstNo = randomIntFromInterval(1, 10);
	 	secondNo = randomIntFromInterval(firstNo, 13);

	 	firstNo2 = randomIntFromInterval(1, 10);
	 	secondNo2 = randomIntFromInterval(firstNo2, 13);

	 	$(".hint_a").html(firstNo);
	 	$(".hint_b").html(secondNo);
	 	$(".hint_c").html(firstNo2);
	 	$(".hint_d").html(secondNo2);
	 }

	 function randomIntFromInterval(min,max)
	 {
	 	return Math.floor(Math.random()*(max-min+1)+min);
	 }

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
	 function splitintofractions($splitinside){
	 	typeof $splitinside !== "object" ?
	 	alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
	 	null ;

	 	var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
	 	if($splitintofractions.length > 0){
	 		$.each($splitintofractions, function(index, value){
	 			$this = $(this);
	 			var tobesplitfraction = $this.html();
	 			if($this.hasClass('fraction')){
	 				tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
	 				tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	 			}else{
	 				tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
	 				tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	 			}

	 			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	 			$this.html(tobesplitfraction);
	 		});
	 	}
	 }
	 function sound_player(sound_id){
		 createjs.Sound.stop();
		 current_sound = createjs.Sound.play(sound_id);
		 current_sound.play();
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

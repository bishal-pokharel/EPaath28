var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext1,
			textclass: "title",
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1 correct",
				optaddclass:"",
				optdata:data.string.exetyp1opt1
			},{
					optcontainerextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.exetyp1opt2
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.exetyp1opt3
			},{
					optcontainerextra:"opti opti4 correct",
				optaddclass:"",
				optdata:data.string.exetyp1opt4
			},{
				optcontainerextra:"opti opti5 correct",
			optaddclass:"",
			optdata:data.string.exetyp1opt5
			},{
				optcontainerextra:"opti opti6",
			optaddclass:"",
			optdata:data.string.exetyp1opt6
			}]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',

		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext2,
			textclass: "title",
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1 correct",
				optaddclass:"",
				optdata:data.string.exetyp2opt1
			},{
					optcontainerextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.exetyp2opt2
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.exetyp2opt3
			},{
					optcontainerextra:"opti opti4 correct",
				optaddclass:"",
				optdata:data.string.exetyp2opt4
			},{
				optcontainerextra:"opti opti4 correct",
			optaddclass:"",
			optdata:data.string.exetyp2opt5
			},{
				optcontainerextra:"opti opti4",
			optaddclass:"",
			optdata:data.string.exetyp2opt6
			}]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"bold",
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"bold",
			textdata: data.string.diy_text,
			textclass: "quest-ion"
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"correct",
				optaddclass:"opti opti1 ",
				optdata:""
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" ",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"bold",
			textdata: data.string.diy_text,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" correct",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"bold",
			textdata: data.string.diy_text,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"correct",
				optaddclass:"opti opti2",

			},{
				optcntextra:" ",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 6a
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"clas",
			textdata: data.string.diytxt,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" ",
				optaddclass:"opti opti3",

			},{
					optcntextra:"correct",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 6b
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"clas",
			textdata: data.string.diy_txt,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" correct",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 7a
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"clas",
			textdata: data.string.diytxt,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" correct",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 7b
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"clas",
			textdata: data.string.diy_txt,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" correct",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 7c
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		extratextblock:[{
			textclass: "quest",
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"clas",
			textdata: data.string.diy_txt,
			textclass: "quest-ion",
		}],
		exetype2:[{
			optionsdivclss:"options-div",
			exeoption:[{
				optcntextra:"",
				optaddclass:"opti opti1 ",
			},{
					optcntextra:"",
				optaddclass:"opti opti2",

			},{
				optcntextra:" correct",
				optaddclass:"opti opti3",

			},{
					optcntextra:"",
				optaddclass:"opti opti4",

			}]
		}]
	},
	//slide 8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		cntblock:[{
		containtextblockadditionalclass: "mainImgContain1",
		containtext:[{
				textclass:"equa1",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class1",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain2",
		containtext:[{
				textclass:"equa2",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class2",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain3",
		containtext:[{
				textclass:"equa3",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class3",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain4",
		containtext:[{
				textclass:"equa4",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class4",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	}],
	extratextblock:[{
		textclass:"submit-button",
		textdata:data.string.submit
	}]
},
//slide 9
{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock:[{
			textdata: data.string.diytext_1,
			textclass: "title",
		}],
		cntblock:[{
		containtextblockadditionalclass: "mainImgContain1",
		containtext:[{
				textclass:"equa1",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class1",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain2",
		containtext:[{
				textclass:"equa2",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class2",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain3",
		containtext:[{
				textclass:"equa3",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class3",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	},{
		containtextblockadditionalclass: "mainImgContain4",
		containtext:[{
				textclass:"equa4",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class4",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	}],
	extratextblock:[{
		textclass:"submit-button",
		textdata:data.string.submit
	}]
},
//slide 10
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-2',
	uppertextblockadditionalclass: 'instruction',
	uppertextblock:[{
		textdata: data.string.diytext_1,
		textclass: "title",
	}],
	cntblock:[{
	containtextblockadditionalclass: "mainImgContain1",
	containtext:[{
			textclass:"equa1",
			// textdata:data.string.n1
	}],
		txtarea:"text-area",
		txtandintput:"lower-div",
		inputclass:"input-class1",
		txtdata: data.string.xequal,
		textclass:"lower-text-p"
},{
	containtextblockadditionalclass: "mainImgContain2",
	containtext:[{
			textclass:"equa2",
			// textdata:data.string.n1
	}],
		txtarea:"text-area",
		txtandintput:"lower-div",
		inputclass:"input-class2",
		txtdata: data.string.xequal,
		textclass:"lower-text-p"
},{
	containtextblockadditionalclass: "mainImgContain3",
	containtext:[{
			textclass:"equa3",
			// textdata:data.string.n1
	}],
		txtarea:"text-area",
		txtandintput:"lower-div",
		inputclass:"input-class3",
		txtdata: data.string.xequal,
		textclass:"lower-text-p"
},{
	containtextblockadditionalclass: "mainImgContain4",
	containtext:[{
			textclass:"equa4",
			// textdata:data.string.n1
	}],
		txtarea:"text-area",
		txtandintput:"lower-div",
		inputclass:"input-class4",
		txtdata: data.string.xequal,
		textclass:"lower-text-p"
}],
extratextblock:[{
	textclass:"submit-button",
	textdata:data.string.submit
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var score = 0;
	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			{id: "bg-1", src: imgpath+"bg_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pop-3", src: imgpath+"balloon.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init($total_page);
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

// exeTemp.init(0);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);


		$board.html(html);
		texthighlight($board);
		/*generate question no at the beginning of question*/
		// exeTemp.numberOfQuestions(10);
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;

		// random numberGenerator
		function randomGenerator(limit){
			var randNUm = Math.floor(Math.random() * (limit - 0 +1)) + 0;
			return randNUm;
		}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}
		switch(countNext) {
			case 0:
			case 1:
			var count=0;
			$(".optionscontainer").click(function(){
				if($(this).hasClass("correct")){
					$(this).children(".corctopt").show(0);
					play_correct_incorrect_sound(1);
					$(this).addClass('corrects');
					count++;

					if(count==3){
						if(wrngClicked==false){
								scoring.update(true);
						}
						$(" .optionscontainer").css("pointer-events","none");
						nav_button_controls(0);

					}
				}
				else{
					$(this).children(".wrngopt").show(0);
					play_correct_incorrect_sound(0);
					$(this).addClass('incorrect');
					$(this).css("pointer-events","none");
					wrngClicked=true;
				}
			});
			$(".instruction").prepend(countNext+1+". ");
			break;
			case 2:
			randomize(".options-div");
			var qno =  randomGenerator(4);

			var question_array = ['exe3ques1','exe3ques2','exe3ques3','exe3ques4','exe3ques5'];
			var option1_array = ['anss1','ans1','an1','a1','ansa'];
			var option2_array = ['anss2','ans2','an2','a2','ansb'];
			var option3_array = ['anss3','ans3','an3','a3','ansc'];
			var option4_array = ['anss4','ans4','an4','a4','ansd'];

			$('.quest').html(eval("data.string."+question_array[qno]));
			$('.opti1').html(eval("data.string."+option1_array[qno]));
			$('.opti2').html(eval("data.string."+option2_array[qno]));
			$('.opti3').html(eval("data.string."+option3_array[qno]));
			$('.opti4').html(eval("data.string."+option4_array[qno]));
					$(".instruction").prepend(countNext+1+". ");
			break;

			case 3:
 			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe4ques1','exe4ques2','exe4ques3','exe4ques4','exe4ques5'];
						var option1_array = ['opt1','opti1','optio1','op1','oa'];
						var option2_array = ['opt2','opti2','optio2','op2','ob'];
						var option3_array = ['opt3','opti3','optio3','op3','oc'];
						var option4_array = ['opt4','opti4','optio4','op4','od'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend(countNext+1+". ");
			break;

			case 4:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe5ques1','exe5ques2','exe5ques3','exe5ques4','exe5ques5'];
						var option1_array = ['o1','o_1','op_1','opt_1','o_a'];
						var option2_array = ['o2','o_2','op_2','opt_2','o_b'];
						var option3_array = ['o3','o_3','op_3','opt_3','o_c'];
						var option4_array = ['o4','o_4','op_4','opt_4','o_d'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend(countNext+1+". ");
			break;
			case 5:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe6ques1','exe6ques2','exe6ques3','exe6ques4','exe6ques5'];
						var option1_array = ['optn1','optn5','optn9','optn13','optn17'];
						var option2_array = ['optn2','optn6','optn10','optn14','optn18'];
						var option3_array = ['optn3','optn7','optn11','optn15','optn19'];
						var option4_array = ['optn4','optn8','optn12','optn16','optn20'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend("6("+"a"+")"+". ");
						$(".clas:eq(0)").addClass("underline");
						$(".clas:eq(1)").addClass("bold");
			break;
			case 6:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe6ques1','exe6ques2','exe6ques3','exe6ques4','exe6ques5'];
						var option1_array = ['optn_1','optn_5','optn_9','optn_13','optn_17'];
						var option2_array = ['optn_2','optn_6','optn_10','optn_14','optn_18'];
						var option3_array = ['optn_3','optn_7','optn_11','optn_15','optn_19'];
						var option4_array = ['optn_4','optn_8','optn_12','optn_16','optn_20'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend("6("+"b"+")"+". ");
						$(".clas:eq(0)").addClass("underline");
						$(".clas:eq(1)").addClass("bold");
			break;
			case 7:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe7ques1','exe7ques2','exe7ques3','exe7ques4','exe7ques5'];
						var option1_array = ['opti_1','opti_5','opti_9','opti_13','opti_17'];
						var option2_array = ['opti_2','opti_6','opti_10','opti_14','opti_18'];
						var option3_array = ['opti_3','opti_7','opti_11','opti_15','opti_19'];
						var option4_array = ['opti_4','opti_8','opti_12','opti_16','opti_20'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend("7("+"a"+")"+". ");
						$(".clas:eq(0)").addClass("underline");
						$(".clas:eq(1)").addClass("bold");
			break;
			case 8:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe7ques1','exe7ques2','exe7ques3','exe7ques4','exe7ques5'];
						var option1_array = ['optio_1','optio_5','optio_9','optio_13','optio_17'];
						var option2_array = ['optio_2','optio_6','optio_10','optio_14','optio_18'];
						var option3_array = ['optio_3','optio_7','optio_11','optio_15','optio_19'];
						var option4_array = ['optio_4','optio_8','optio_12','optio_16','optio_20'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend("7("+"b"+")"+". ");
						$(".clas:eq(0)").addClass("underline");
						$(".clas:eq(1)").addClass("bold");
			break;
			case 9:
			randomize(".options-div");
			var qno =  randomGenerator(4);

						var question_array = ['exe7ques1','exe7ques2','exe7ques3','exe7ques4','exe7ques5'];
						var option1_array = ['option_1','option_5','option_9','option_13','option_17'];
						var option2_array = ['option_2','option_6','option_10','option_14','option_18'];
						var option3_array = ['option_3','option_7','option_11','option_15','option_19'];
						var option4_array = ['option_4','option_8','option_12','option_16','option_20'];
						$('.quest').html(eval("data.string."+question_array[qno]));
						$('.opti1').html(eval("data.string."+option1_array[qno]));
						$('.opti2').html(eval("data.string."+option2_array[qno]));
						$('.opti3').html(eval("data.string."+option3_array[qno]));
						$('.opti4').html(eval("data.string."+option4_array[qno]));
						$(".instruction").prepend("7("+"c"+")"+". ");
						$(".clas:eq(0)").addClass("underline");
						$(".clas:eq(1)").addClass("bold");
			break;
			case 10:
			var qno =  randomGenerator(9);
			var qnArray = [
				[
					["3x=9",3,'x ='],
					["4=2y",2,'y ='],
					["z+2=5",3,'z ='],
					["a−4=−2",2,'a =']
				],
				[
					["4x=12",6,"x ="],
					["3x=9",3,'x ='],
					["8=2y",4,'y ='],
					["4z=28",7,'z ='],
				],
				[
					["8=2y",4,'y ='],
					["4z=28",7,'z ='],
					["3x=9",3,'x ='],
					["4=2y",2,'y ='],
				],
				[
					["z+3=5",2,'z ='],
					["r−6=1",7,'r ='],
					["4=2y",2,'y ='],
					["3x=9",3,'x ='],
				],
				[
					["a−2=1",3,'a ='],
					["3+s=12",9,'s ='],
					["2=12÷m",6,'m ='],
					["3=d−2",5,'d ='],
				],
				[
					["14=c+6",8,'c ='],
					["z+3=5",2,'z ='],
					["a−2=1",3,'a ='],
					["2=12÷m",6,'m ='],
				],
				[
					["3=d−2",5,'d ='],
					["a−2=1",3,'a ='],
					["z+3=5",2,'z ='],
					[" 21÷n=7",3,'n ='],
				],
				[
					["2=12÷m",6,'m ='],
					["3k=15",5,'k ='],
					["3+s=12",9,'s ='],
					["z+3=5",2,'z ='],
				],
				[
					["21÷n=7",3,'n ='],
					["3=d−2",5,'d ='],
					["14=c+6",8,'c ='],
					["a−2=1",3,'a ='],
				],
				[
					["14=2w",7,'w ='],
					["21÷n=7",3,'n ='],
					["2=12÷m",6,'m ='],
					["3=d−2",5,'d ='],
				]
			]

			for (var i = 1; i < 5; i++) {
					$(".equa"+i).html(qnArray[qno][i-1][0]);
					$(".mainImgContain"+i+">div:nth-child(3) p").html(qnArray[qno][i-1][2]);
					console.log("the answer of"+i+"is"+qnArray[qno][i-1][1]);
					$(".equa"+i).prepend(0+i+") ");

			}


					for (var k=1;k<5;k++){
				input_box('.input-class'+k, 3, null);
						$(".submit-button").css("pointer-events","none");
						// if($('.input-class'+k).val().length>0){
					$('.input-class'+k).keyup(function(event) {
							$(".submit-button").css("pointer-events","auto");
						$(".submit-button").attr('disabled','false');

					});

			}


					$('.submit-button').click(function(){
						var correct_answer=0;
						for (var j = 1; j < 5; j++) {
							if($('.input-class'+j).val()==qnArray[qno][j-1][1]){
								correct_answer++;

									$('.input-class'+j).attr('disabled', 'true');
										$('.input-class'+j).siblings(".corctopt").show(0);
										$('.input-class'+j).addClass('corrects');
							if(correct_answer==4){
							$nextBtn.show(0);
									$prevBtn.show(0);
									if(wrngClicked==false){
											scoring.update(true);
									}
							 play_correct_incorrect_sound(1);
						}
					}
						else{
							console.log(correct_answer);
							wrngClicked==true;
						}
					}
					});
							$(".instruction").prepend(8+". ");
			break;
			case 11:
			var qno =  randomGenerator(9);
			var qnArray = [
				[
					["6x − 10 =56",11,'x ='],
					["4y + 9 =25",4,'y ='],
					["3z − 6 =0",2,'z ='],
					["5a + 6 =56",10,'a =']
				],
				[
					["2y + 3 =9",3,"y ="],
					["6x − 10 =56",11,'x ='],
					["5a − 3 =27",6,'a ='],
					["3z − 6 =0",2,'z ='],
				],
				[
					["20 −2d =6",7,'d ='],
					["12 −2m =8",2,'m ='],
					["13= 3n + 4",3,'n ='],
					["6= 102 − 8w",12,'w ='],
				],
				[
					["2y + 3 =9",3,"y ="],
					["5a + 6 =56",10,'a ='],
					["6= 102 − 8w",12,'w ='],
					["3z − 6 =0",2,'z ='],
				],
				[
					["3 + 2b =9",3,'b ='],
					["3 + 2c =11",4,'c ='],
					["20 − 2d= 6",7,'d ='],
					["13= 3n + 4",3,'n ='],
				],
				[
					["6= 102 − 8w",12,'w ='],
					["3 + 2b =9",3,'b ='],
					["2y + 3 =9",3,"y ="],
					["3z − 6 =0",2,'z ='],
				],
				[
					["20 − 2d =6",7,'d ='],
					["5a − 3 =27",6,'a ='],
					["3z + 2 =14",4,'z ='],
					["13= 3n + 4",3,'n ='],
				],
				[
					["6= 102 − 8w",12,'w ='],
					["3z − 6 =0",2,'z ='],
					["2y + 3 =9",3,"y ="],
					["5a + 6 =56",10,'a =']
				],
				[
					["3z − 6 =0",2,'z ='],
					["20 − 2d =6",7,'d ='],
					["3 + 2b =9",3,'b ='],
					["5a − 2 =13",3,'a ='],
				],
				[
					["14 − 2 =2w",6,'w ='],
					["20 − 2d =6",3,'n ='],
					["3z − 6 =0",2,'z ='],
					["5a − 2 =13",3,'a ='],
				]
			]

									for (var i = 1; i < 5; i++) {
											$(".equa"+i).html(qnArray[qno][i-1][0]);
											$(".mainImgContain"+i+">div:nth-child(3) p").html(qnArray[qno][i-1][2]);
											console.log("the answer of"+i+"is"+qnArray[qno][i-1][1]);
											$(".equa"+i).prepend(0+i+") ");
									}


											for (var k=1;k<5;k++){
										input_box('.input-class'+k, 3, null);
												$(".submit-button").css("pointer-events","none");
												// if($('.input-class'+k).val().length>0){
											$('.input-class'+k).keyup(function(event) {
													$(".submit-button").css("pointer-events","auto");
												$(".submit-button").attr('disabled','false');

											});

									}


											$('.submit-button').click(function(){
												var correct_answer=0;
												for (var j = 1; j < 5; j++) {
													if($('.input-class'+j).val()==qnArray[qno][j-1][1]){
														correct_answer++;

															$('.input-class'+j).attr('disabled', 'true');
																$('.input-class'+j).siblings(".corctopt").show(0);
																$('.input-class'+j).addClass('corrects');
													if(correct_answer==4){
													$nextBtn.show(0);
													$prevBtn.show(0);
													if(wrngClicked==false){
															scoring.update(true);
													}
													 play_correct_incorrect_sound(1);
												}
											}
												else{
													console.log(correct_answer);
													wrngClicked==true;
												}
											}
											});
													$(".instruction").prepend(9+". ");
			break;
			case 12:
			var qno =  randomGenerator(9);
			var qnArray = [
				[
					["7x + 9 = 8x + 7",2,'x ='],
					["10y + 9 = 9y + 12",3,'y ='],
					["9z − 4 = 11z − 10 ",3,'z ='],
					["7a + 2 = 6a + 7",5,'a =']
				],
				[
					["4x + 2 = 3x + 4",2,"x ="],
					["3b + 6 = 5b + 0",3,'b ='],
					["3c + 7 = 2c + 14",7,'c ='],
					["3d + 3 = 2d + 6",3,'d ='],
				],
				[
					["2m + 2 = 4m − 6",4,'m ='],
					["6n − 2 = 2n + 34",9,'n ='],
					["4w − 2 = 3w + 0",2,'w ='],
					["4a + 2 = 3a + 7",5,'a ='],
				],
				[
					["3z + 3 = 2z + 7",4,"y ="],
					["7x + 9 = 8x + 7",2,'x ='],
					["3c + 7 = 2c + 14",7,'c ='],
					["3b + 6 = 5b + 0",3,'b ='],
				],
				[
					["10y + 9 = 9y + 12",3,'y ='],
					["7a + 2 = 6a + 7",5,'a ='],
					["2m + 2 = 4m − 6",4,'m ='],
					["4a + 2 = 3a + 7",5,'a ='],
				],
				[
					["m + 2 = 4m − 16",6,'m ='],
					["6n + 13 = −n + 34",3,'n ='],
					["4w − 2 = 3w + 7",9,'w ='],
					["10y + 4 = 9y + 12",8,'y ='],
				],
				[
					["4a + 2 = 3a + 7",5,'a ='],
					["10y + 9 = 9y + 12",3,'y ='],
					["u + 2 = 4u − 16",6,'u ='],
					["p + 2 = 4p − 19",7,'p ='],
				],
				[
					["4c + 4 = 3c + 17",13,'c ='],
					["y + 20 = 9y + 12",1,'y ='],
					["2g + 2 = 4g − 16",9,'g ='],
					["p + 2 = 4p − 19",7,'p ='],
				],
				[
					["x + 2 = 4x − 16",6,'x ='],
					["10a + 9 = 9a + 12",3,'a ='],
					["b + 2 = 4b − 19",7,'b ='],
					["4d + 2 = 3d + 7",5,'d ='],
				],
				[
					["3c + 7 = 2c + 14",7,'c ='],
					["7x + 9 = 8x + 7",2,'x ='],
					["3b + 6 = 5b + 0",3,'b ='],
					["3z + 3 = 2z + 7",4,"y ="],
				]
			]
			for (var i = 1; i < 5; i++) {
					$(".equa"+i).html(qnArray[qno][i-1][0]);
					$(".mainImgContain"+i+">div:nth-child(3) p").html(qnArray[qno][i-1][2]);
					console.log("the answer of"+i+"is"+qnArray[qno][i-1][1]);
					$(".equa"+i).prepend(0+i+") ");

			}


					for (var k=1;k<5;k++){
				input_box('.input-class'+k, 3, null);
						$(".submit-button").css("pointer-events","none");
						// if($('.input-class'+k).val().length>0){
					$('.input-class'+k).keyup(function(event) {
							$(".submit-button").css("pointer-events","auto");
						$(".submit-button").attr('disabled','false');

					});

			}


					$('.submit-button').click(function(){
						var correct_answer=0;
						for (var j = 1; j < 5; j++) {
							if($('.input-class'+j).val()==qnArray[qno][j-1][1]){
								correct_answer++;

									$('.input-class'+j).attr('disabled', 'true');
										$('.input-class'+j).siblings(".corctopt").show(0);
										$('.input-class'+j).addClass('corrects');
							if(correct_answer==4){
							$nextBtn.show(0);
								$prevBtn.show(0);
							 play_correct_incorrect_sound(1);
							 	if(wrngClicked==false){
										scoring.update(true);
								}
						}
					}
						else{
							console.log(correct_answer);
							wrngClicked=true;
					}
					}
				});
		$(".instruction").prepend(10+". ");
			break;
			default:
				nav_button_controls(0);
				break;
		}

		$(".options-container").click(function(){
			if($(this).hasClass("correct")){
				$(this).children(".corctopt").show(0);
				play_correct_incorrect_sound(1);
				$(this).addClass('corrects');
				$(" .options-container").css("pointer-events","none");
				nav_button_controls(0);
				if(wrngClicked==false){
							scoring.update(true);
				}

			}else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('incorrect');
				$(this).css("pointer-events","none");
				wrngClicked=true;
			}
		});


	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}







	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

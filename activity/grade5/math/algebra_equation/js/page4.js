var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass: 'text-box end-anim wobble ',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "txt",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'kite',
				imgclass:'kite',
			}]
		}]
	},{
		//slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
    imageblock:[{
      imagestoshow:[{
        imgid:'fairy',
        imgclass:'fairy',
      }]
    }],
		imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text2,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text3,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.twapl
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.twapl
							}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							}]
						}]
					}]
				}]
		}]
	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text4,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.twapl
							},{
								textclass:"swau-1",
								textdata:data.string.onapl
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.twapl
							},{
								textclass:"swau-2",
								textdata:data.string.onapl
							}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							}]
						}]
					}]
				},{
					rowclass:"tblrow rowfour",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'plus',
								imgclass:'plus1',
							},{
								imgid:'apple',
								imgclass:'ellpa ellpa1',
							}]
						}]
				},{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.blank
						}]

					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'plus',
								imgclass:'plus2',
							},{
								imgid:'apple',
								imgclass:'ellpa ellpa2',
							}]
						}]
					}]
				}]
		}]
	},{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text4,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							},{
								imgid:'apple',
								imgclass:'apple appl',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							},{
								imgid:'apple',
								imgclass:'apple appl',
							}]
						}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.threeapp
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.threeapp
							}]
					}]
				}]
		}],
		uppertextblockadditionalclass: "moreinfo",
		uppertextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"underline",
				textclass: "heading",
				textdata: data.string.p4text5
		},{
				textclass: "close_button",
				textdata: "&nbsp;"
			}],
		learn_more: true
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text6,
				textclass : 'text-2',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.twapl
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.twapl
							}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							}]
						}]
					}]
				}]
		}]
	},{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text7,
				textclass : 'text-2',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.twapl
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.twapl
							}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							}]
						}]
					}]
				}]
		}]
	},{
		//slide 7
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text8,
				textclass : 'text-2',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.twapl
							},{
								textclass:"swau-1",
								textdata:data.string.onapl
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.equal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.twapl
							},{
								textclass:"swau-2",
								textdata:data.string.twoap
							}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple1',
							},{
								imgid:'apple',
								imgclass:'apple apple2',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'equal',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple apple3',
							},{
								imgid:'apple',
								imgclass:'apple apple4',
							}]
						}]
					}]
				},{
					rowclass:"tblrow rowfor",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'plus',
								imgclass:'plus1',
							},{
								imgid:'apple',
								imgclass:'ellpa ellpa1',
							}]
						}]
				},{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.blank
						}]

					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'plus',
								imgclass:'plus2',
							},{
								imgid:'apple',
								imgclass:'ellpa ellpa2',
							},{
								imgid:'apple',
								imgclass:'ellpa ellpa2 ellpa3',
							}]
						}]
					}]
				}]
		}]
	},{
		//slide 8
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p4text9,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
		imgload:true,
			tblblk:[{
				tblclass:"tableCont",
				tablerow:[{
					rowclass:"tblrow",
					tabledata:[{
						tdtxt:[
							{
								textclass:"lhs",
								textdata:data.string.lhs
							}]
					},{
						tdtxt:[
							{
								textclass:"blank",
								textdata:data.string.blank
							}]
					},{
						tdtxt:[
							{
								textclass:"rhs",
								textdata:data.string.rhs
							}]
					}]
				},{
					rowclass:" tblrow rowsec",
					tabledata:[
				{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple-row apple-row1',
							},{
								imgid:'apple',
								imgclass:'apple-row apple-row2',
							},{
								imgid:'apple',
								imgclass:'apple-row apple-row3',
							}]
						}]
				},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'hash',
								imgclass:'equal',
							}]
						}]
					},{
						tdimg:true,
						imageblock:[{
							imagestoshow:[{
								imgid:'apple',
								imgclass:'apple-row apple-row1',
							},{
								imgid:'apple',
								imgclass:'apple-row apple-row2',
							},{
								imgid:'apple',
								imgclass:'apple-row apple-row3',
							},{
								imgid:'apple',
								imgclass:'apple-row apple-row4',
							}]
						}]
					}]
				},{
					rowclass:"tblrow rowthird",
					tabledata:[{
						tdtxt:[
							{
								textclass:"app-1",
								textdata:data.string.threeapp
							}]
					},{
						tdtxt:[
							{
								textclass:"equ",
								textdata:data.string.notequal
							}]
					},{
						tdtxt:[
							{
								textclass:"app-2",
								textdata:data.string.fourapp
							}]
					}]
				}]
		}]
	},{
	//slide 9
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'fairy',
		}]
	}],
	imagedivblock:[{
			imagedivclass:"mainImgCont",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text10,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
	imgload:true,
		tblblk:[{
			tblclass:"tableCont",
			tablerow:[{
				rowclass:"tblrow",
				tabledata:[{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.lhs
						}]
				},{
					tdtxt:[
						{
							textclass:"blank",
							textdata:data.string.blank
						}]
				},{
					tdtxt:[
						{
							textclass:"rhs",
							textdata:data.string.rhs
						}]
				}]
			},{
				rowclass:" tblrow rowsec",
				tabledata:[
			{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple-row apple-row1',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row2',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row3',
						}]
					}]
			},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'hash',
							imgclass:'equal',
						}]
					}]
				},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple-row apple-row1',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row2',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row3',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row4',
						}]
					}]
				}]
			},{
				rowclass:"tblrow rowthird",
				tabledata:[{
					tdtxt:[
						{
							textclass:"app-1",
							textdata:data.string.threeapp
						}]
				},{
					tdtxt:[
						{
							textclass:"equ",
							textdata:data.string.notequal
						}]
				},{
					tdtxt:[
						{
							textclass:"app-2",
							textdata:data.string.fourapp
						}]
				}]
			}]
	}]
},{
	//slide 10
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'fairy-1',
		}]
	}],
	imagedivblock:[{
			imagedivclass:"mainImgCont",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
		}],
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p4text12,
			textclass : 'txtbox',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p4text11,
			textclass : 'txtbox',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
	imgload:true,
		tblblk:[{
			tblclass:"tableCont",
			tablerow:[{
				rowclass:"tblrow",
				tabledata:[{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.lhs
						}]
				},{
					tdtxt:[
						{
							textclass:"blank",
							textdata:data.string.blank
						}]
				},{
					tdtxt:[
						{
							textclass:"rhs",
							textdata:data.string.rhs
						}]
				}]
			},{
				rowclass:" tblrow rowsec",
				tabledata:[
			{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple-row apple-row1',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row2',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row3',
						}]
					}]
			},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'hash',
							imgclass:'equal',
						}]
					}]
				},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple-row apple-row1',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row2',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row3',
						},{
							imgid:'apple',
							imgclass:'apple-row apple-row4',
						}]
					}]
				}]
			},{
				rowclass:"tblrow rowthird",
				tabledata:[{
					tdtxt:[
						{
							textclass:"app-1",
							textdata:data.string.threeapp
						}]
				},{
					tdtxt:[
						{
							textclass:"equ",
							textdata:data.string.notequal
						}]
				},{
					tdtxt:[
						{
							textclass:"app-2",
							textdata:data.string.fourapp
						}]
				}]
			}]
	}]
},{
	//slide 11
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'fairy',
		}]
	}],
	imagedivblock:[{
			imagedivclass:"mainImgCont",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text13,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
},{
	//slide 12
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'fairy',
		}]
	}],
	imagedivblock:[{
			imagedivclass:"mainImgCont",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text3,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
	imgload:true,
		tblblk:[{
			tblclass:"tableCont",
			tablerow:[{
				rowclass:"tblrow",
				tabledata:[{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.lhs
						}]
				},{
					tdtxt:[
						{
							textclass:"blank",
							textdata:data.string.blank
						}]
				},{
					tdtxt:[
						{
							textclass:"rhs",
							textdata:data.string.rhs
						}]
				}]
			},{
				rowclass:" tblrow rowsec",
				tabledata:[{
					tdtxt:[
						{
							textclass:"app-1",
							textdata:data.string.twapl
						}]
				},{
					tdtxt:[
						{
							textclass:"equ",
							textdata:data.string.equal
						}]
				},{
					tdtxt:[
						{
							textclass:"app-2",
							textdata:data.string.twapl
						}]
				}]
			},{
				rowclass:"tblrow rowthird",
				tabledata:[
			{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple apple1',
						},{
							imgid:'apple',
							imgclass:'apple apple2',
						}]
					}]
			},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'equal',
							imgclass:'equal',
						}]
					}]
				},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple apple3',
						},{
							imgid:'apple',
							imgclass:'apple apple4',
						}]
					}]
				}]
			}]
	}]
},{
//slide 13
contentnocenteradjust: true,
contentblockadditionalclass: 'bg-diy',
imageblock:[{
	imagestoshow:[{
		imgid:'fairy',
		imgclass:'fairy',
	}]
}],
imagedivblock:[{
		imagedivclass:"mainImgCont",
		imagestoshow:[{
			imgclass: 'chalkboard',
			imgid : 'chalkboard',
			imgsrc: '',
		}],
	}],
	speechbox:[{
		speechbox: 'sp-1',
		textdata : data.string.p4text14,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox',
		imgsrc: '',
	}],
imgload:true,
	tblblk:[{
		tblclass:"tableCont",
		tablerow:[{
			rowclass:"tblrow",
			tabledata:[{
				tdtxt:[
					{
						textclass:"lhs",
						textdata:data.string.lhs
					}]
			},{
				tdtxt:[
					{
						textclass:"blank",
						textdata:data.string.blank
					}]
			},{
				tdtxt:[
					{
						textclass:"rhs",
						textdata:data.string.rhs
					}]
			}]
		},{
			rowclass:" tblrow rowsec",
			tabledata:[{
				tdtxt:[
					{
						textclass:"app-1",
						textdata:data.string.twapl
					},{
						textclass:"swau-1",
						textdata:data.string.minusap
					}]
			},{
				tdtxt:[
					{
						textclass:"equ",
						textdata:data.string.equal
					}]
			},{
				tdtxt:[
					{
						textclass:"app-2",
						textdata:data.string.twapl
					},{
						textclass:"swau-2",
						textdata:data.string.minusap
					}]
			}]
		},{
			rowclass:"tblrow rowthird",
			tabledata:[
		{
				tdimg:true,
				imageblock:[{
					imagestoshow:[{
						imgid:'apple',
						imgclass:'apple apple1',
					},{
						imgid:'apple',
						imgclass:'apple apple2',
					}]
				}]
		},{
				tdimg:true,
				imageblock:[{
					imagestoshow:[{
						imgid:'equal',
						imgclass:'equal',
					}]
				}]
			},{
				tdimg:true,
				imageblock:[{
					imagestoshow:[{
						imgid:'apple',
						imgclass:'apple apple3',
					},{
						imgid:'apple',
						imgclass:'apple apple4',
					}]
				}]
			}]
		},{
			rowclass:"tblrow rowfour",
			tabledata:[
		{
				tdimg:true,
				imageblock:[{
					imagestoshow:[{
						imgid:'minus',
						imgclass:'minus1',
					},{
						imgid:'apple',
						imgclass:'ellpa ellpa1',
					}]
				}]
		},{
			tdtxt:[
				{
					textclass:"lhs",
					textdata:data.string.blank
				}]

			},{
				tdimg:true,
				imageblock:[{
					imagestoshow:[{
						imgid:'minus',
						imgclass:'minus2',
					},{
						imgid:'apple',
						imgclass:'ellpa ellpa2',
					}]
				}]
			}]
		}]
}]
},{
	//slide 14
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'fairy',
		}]
	}],
	imagedivblock:[{
			imagedivclass:"mainImgCont",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p4text14,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
	imgload:true,
		tblblk:[{
			tblclass:"tableCont",
			tablerow:[{
				rowclass:"tblrow",
				tabledata:[{
					tdtxt:[
						{
							textclass:"lhs",
							textdata:data.string.lhs
						}]
				},{
					tdtxt:[
						{
							textclass:"blank",
							textdata:data.string.blank
						}]
				},{
					tdtxt:[
						{
							textclass:"rhs",
							textdata:data.string.rhs
						}]
				}]
			},{
				rowclass:" tblrow rowsec",
				tabledata:[
			{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{

							imgid:'apple',
							imgclass:'apple apple2',
						}]
					}]
			},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'equal',
							imgclass:'equal',
						}]
					}]
				},{
					tdimg:true,
					imageblock:[{
						imagestoshow:[{
							imgid:'apple',
							imgclass:'apple apple4',
						}]
					}]
				}]
			},{
				rowclass:"tblrow rowthird",
				tabledata:[{
					tdtxt:[
						{
							textclass:"app-1",
							textdata:data.string.oneapp
						}]
				},{
					tdtxt:[
						{
							textclass:"equ",
							textdata:data.string.equal
						}]
				},{
					tdtxt:[
						{
							textclass:"app-2",
							textdata:data.string.oneapp
						}]
				}]
			}]
	}],
	uppertextblockadditionalclass: "moreinfo",
	uppertextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"underline",
			textclass: "heading",
			textdata: data.string.p4text15
	},{

			textclass: "close_button",
			textdata: "&nbsp;"
		}],
	learn_more: true
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"chibi_fairy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite", src: imgpath+"kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "apple", src: imgpath+"apple01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "equal", src: imgpath+"equal.png", type: createjs.AbstractLoader.IMAGE},
			{id: "plus", src: imgpath+"plus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "minus", src: imgpath+"minus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hash", src: imgpath+"hash.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s4_p10.ogg"},
			{id: "sound_10a", src: soundAsset+"s4_p11_1.ogg"},
			{id: "sound_10b", src: soundAsset+"s4_p11_2.ogg"},
			{id: "sound_11", src: soundAsset+"s4_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s4_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s4_p14.ogg"},
			{id: "sound_14", src: soundAsset+"s4_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		content[countNext].imgload?put_image3(content, countNext):"";
		$prevBtn.hide(0);
		$nextBtn.hide(0);
  	put_speechbox_image(content, countNext);
		$(".close_button").click(function(){
			createjs.Sound.stop();
			$(".moreinfo").hide(500);
			nav_button_controls(500);
		});

		switch(countNext) {
			case 2:
			case 12:
			sound_player("sound_"+(countNext));
			$(".lhs").delay(3000).show(0);
			$(".rhs").delay(4000).show(0);
			$(".app-1").delay(5000).show(0);
			$(".equ").delay(6000).show(0);
			$(".app-2").delay(7000).show(0);
			$(".apple1,.apple2").delay(8000).show(0);
			$(".equal").delay(9000).show(0);
			$(".apple3,.apple4").delay(10000).show(0);
			nav_button_controls(10500);
			break;
			case 3:
			$(".lhs,.rhs,.app-1,.equ,.app-2,.apple1,.apple2,.apple3, .apple4,.equal").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".swau-1").delay(500).show(0);
			$(".swau-2").delay(1500).show(0);
			$(".plus1").delay(2500).show(0);
			$(".ellpa1,.plus1").delay(3500).show(0);
			$(".plus2").delay(4500).show(0);
			$(".ellpa2,.plus2").delay(5500).show(0);
				nav_button_controls(6000);
			});
			break;
			case 4:
			case 14:
			$learn_more = $(".learn_more");
			$learn_more.click(function(){
				$(".moreinfo").show(500);
				setTimeout(function(){
					sound_player("sound_"+(countNext));
				},500);
			});
				$(".lhs,.rhs,.app-1,.equ,.app-2,.apple1,.apple2,.apple3, .apple4,.equal,.ellpa1,.ellpa2").show(0);
				$(".learn_more").delay(1000).show(0);

			break;
			case 5:
			case 6:
			$(".lhs,.rhs,.app-1,.equ,.app-2,.apple1,.apple2,.apple3, .apple4,.equal").show(0);
				sound_nav("sound_"+(countNext));
			break;
			case 7:
			$(".lhs,.rhs,.app-1,.equ,.app-2,.apple1,.apple2,.apple3, .apple4,.equal").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_7");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".swau-1").delay(500).show(0);
			$(".swau-2").delay(1500).show(0);
			$(".ellpa1,.plus1").delay(2500).show(0);
			$(".ellpa2,.plus2").delay(3500).show(0);
				nav_button_controls(4000);
			});
			break;
			case 8:
			case 9:
			$(".lhs,.rhs,.equ,.equal,.app-1,.app-2").show(0);
				sound_nav("sound_"+(countNext));
			break;
			case 10:
			$(".lhs,.rhs,.equ,.equal,.app-1,.app-2").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_10a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".sp-3").show(0);
			current_sound = createjs.Sound.play("sound_10b");
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		});
			break;
			case 13:
			$(".lhs,.rhs,.app-1,.equ,.app-2,.apple1,.apple2,.apple3, .apple4,.equal").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_13");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".swau-1").delay(500).show(0);
			$(".swau-2").delay(1500).show(0);
			$(".minus1").delay(2500).show(0);
			$(".ellpa1").delay(3500).show(0);
			$(".minus2").delay(4500).show(0);
			$(".ellpa2").delay(5500).show(0);
				nav_button_controls(6000);
			});
			break;

			default:
				sound_nav("sound_"+(countNext));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image3(content, count){
		if(content[count].hasOwnProperty('tblblk')){
			if(content[count].tblblk[0].hasOwnProperty('tablerow')){
			var tblimgblk = content[count].tblblk[0].tablerow;
				for(var j=0;j<tblimgblk.length;j++){
					if(tblimgblk[j].hasOwnProperty('tabledata')){
						// alert(tblimgblk.length);
						// alert("here");
						for(var k=0;k<tblimgblk[j].tabledata.length;k++){
							if(tblimgblk[j].tabledata[k].hasOwnProperty('imageblock')){
								var imageblock = tblimgblk[j].tabledata[k].imageblock[0];
									if(imageblock.hasOwnProperty('imagestoshow')){
										var imageClass = imageblock.imagestoshow;
										for(var i=0; i<imageClass.length; i++){
											var image_src = preload.getResult(imageClass[i].imgid).src;
											//get list of classes
											var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
											var selector = ('.'+classes_list[classes_list.length-1]);
											$(selector).attr('src', image_src);
										}
									}
							}
						}
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

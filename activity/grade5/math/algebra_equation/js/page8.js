var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-a',
			textdata : data.string.p8text1,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}]

	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p8text2,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p8text3,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p8text4,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p8text5
			},{
					optcontainerextra:"opti opti2 correct",
				optaddclass:"",
				optdata:data.string.p8text7
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p8text6
			},{
					optcontainerextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.p8text8
			}]
		}],
    table:[{
        firstrowdata	:[{
            textclass:"lhs",
            textdata:data.string.lhs
        },{
          textclass:"lhs",
          textdata:data.string.blank
        },
        {
            textclass:"lhs",
            textdata:data.string.rhs
        }],
        secondrowdata :[{
          textclass:"lhs-row",
          textdata:data.string.p8text9
      },{
        textclass:"lhs-row",
        textdata:data.string.equal
      },
      {
          textclass:"lhs-row",
          textdata:data.string.p8text10
        }],
        thirdrowdata:[{
          datahighlightflag:true,
              datahighlightcustomclass:"color-text",
          textclass:"lhs-row-1",
          textdata:data.string.minusp
      },{
        datahighlightflag:true,
  			datahighlightcustomclass:"color-text",
        textclass:"lhs-row-1",
        textdata:data.string.blank
      },
      {
        datahighlightflag:true,
        datahighlightcustomclass:"color-text",
          textclass:"lhs-row-1",
          textdata:data.string.minusp
        }]
    }]
  },
	//slide 3
	{
		contentnocenteradjust: true,
	  contentblockadditionalclass: 'diy-bg',
	  uppertextblockadditionalclass: 'uppertitle',
	  uppertextblock:[{
	    textdata: data.string.p8text2,
	    textclass: "title-text",
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgid:'fairy01',
	      imgclass:'chibi fairy-3',
	    }]
	  }],
	  speechbox:[{
	    speechbox: 'sp-3',
	    textdata : data.string.p8text4,
	    textclass : 'text-box',
	    imgclass: 'box',
	    imgid : 'spbox02',
	    imgsrc: '',
	  }],
	  imagedivblock:[{
	      imagedivclass:"mainImgContainer",
	      imagestoshow:[{
	        imgclass: 'chalkboard',
	        imgid : 'chalkboard',
	        imgsrc: '',
	      }],
	      imagelabels:true,
	      imagelabels:[{
	        imagelabelclass:"sbmt",
	        imagelabeldata:data.string.submit
	      },{
	        imagelabelclass:"tryagain-button",
	        imagelabeldata:data.string.tryagain
	      }]
	  }],

	  table:[{
	      firstrowdata	:[{
	          textclass:"lhs",
	          textdata:data.string.lhs
	      },{
	        textclass:"lhs",
	        textdata:data.string.blank
	      },
	      {
	          textclass:"lhs",
	          textdata:data.string.rhs
	      }],
	      secondrowdata :[{
	        textclass:"lhs-row",
	        textdata:data.string.p8text9
	    },{
	      textclass:"lhs-row",
	      textdata:data.string.equal
	    },
	    {
	        textclass:"lhs-row",
	        textdata:data.string.p8text10
	      }],
	      thirdrowdata:[{
	        datahighlightflag:true,
	            datahighlightcustomclass:"color-text",
	        textclass:"lhs-row-1",
	        textdata:data.string.minusp
	    },{

	      textclass:"lhs-row-1",
	      textdata:data.string.blank
	    },
	    {
	      datahighlightflag:true,
	      datahighlightcustomclass:"color-text",
	        textclass:"lhs-row-1",
	        textdata:data.string.minusp
	      }],
	              fourrowdata:[{
	                datahighlightflag:true,
	                    datahighlightcustomclass:"color-text",
	                textclass:"lhs-row-2",
	                textdata:data.string.p8text11
	            },{

	              textclass:"lhs-row-2",
	              textdata:data.string.equal
	            },
	            {
	              datahighlightflag:true,
	              datahighlightcustomclass:"color-text",
	                textclass:"lhs-row-2",
	                textdata:data.string.p8text12
	              }],
	              fiverowdata: [{
	                        textclass:"lhs-row-3",
	                        textdata:data.string.p8text15
	                },
	                {
	                    textclass:"lhs-row-3",
	                    textdata:data.string.equal
	                },
	                {

										textclass:"lhs-row-3",
										textdata:data.string.n9
	              }]
	          }]
  	},
	//slide 4
	{
    contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p8text2,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p8text3,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p8text24,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p8text5
			},{
					optcontainerextra:"opti opti2 correct",
				optaddclass:"",
				optdata:data.string.p8text7_1
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p8text6
			},{
					optcontainerextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.p8text8
			}]
		}],
    table:[{
        firstrowdata	:[{
            textclass:"lhs",
            textdata:data.string.lhs
        },{
          textclass:"lhs",
          textdata:data.string.blank
        },
        {
            textclass:"lhs",
            textdata:data.string.rhs
        }],
        secondrowdata :[{
          textclass:"lhs-row",
          textdata:data.string.p8text15
      },{
        textclass:"lhs-row",
        textdata:data.string.equal
      },
      {
          textclass:"lhs-row",
          textdata:data.string.n9
        }],
        thirdrowdata:[{
          datahighlightflag:true,
              datahighlightcustomclass:"color-text",
          textclass:"lhs-row-1",
          textdata:data.string.plu3
      },{

        textclass:"lhs-row-1",
        textdata:data.string.blank
      },
      {
        datahighlightflag:true,
        datahighlightcustomclass:"color-text",
          textclass:"lhs-row-1",
          textdata:data.string.plu3
        }],
        fourrowdata:[{
          datahighlightflag:true,
              datahighlightcustomclass:"color-text",
          textclass:"lhs-row-2",
          textdata:data.string.p8text16
      },{

        textclass:"lhs-row-2",
        textdata:data.string.equal
      },
      {
        datahighlightflag:true,
        datahighlightcustomclass:"color-text",
          textclass:"lhs-row-2",
          textdata:data.string.p8text17
        }]
    }]
},
//slide 5
{
  contentnocenteradjust: true,
  contentblockadditionalclass: 'diy-bg',
  uppertextblockadditionalclass: 'uppertitle',
  uppertextblock:[{
    textdata: data.string.p8text2,
    textclass: "title-text",
  }],
  imageblock:[{
    imagestoshow:[{
      imgid:'fairy01',
      imgclass:'chibi fairy-3',
    }]
  }],
  speechbox:[{
    speechbox: 'sp-3',
    textdata : data.string.p8text18,
    textclass : 'text-box',
    imgclass: 'box',
    imgid : 'spbox02',
    imgsrc: '',
  }],
  imagedivblock:[{
      imagedivclass:"mainImgContainer",
      imagestoshow:[{
        imgclass: 'chalkboard',
        imgid : 'chalkboard',
        imgsrc: '',
      }],
      imagelabels:true,
      imagelabels:[{
        imagelabelclass:"sbmt",
        imagelabeldata:data.string.submit
      },{
        imagelabelclass:"tryagain-button",
        imagelabeldata:data.string.tryagain
      }]
  }],

  table:[{
      firstrowdata	:[{
          textclass:"lhs",
          textdata:data.string.lhs
      },{
        textclass:"lhs",
        textdata:data.string.blank
      },
      {
          textclass:"lhs",
          textdata:data.string.rhs
      }],
      secondrowdata :[{
        textclass:"lhs-row",
        textdata:data.string.p8text15
    },{
      textclass:"lhs-row",
      textdata:data.string.equal
    },
    {
        textclass:"lhs-row",
        textdata:data.string.n9
      }],
      thirdrowdata:[{
        datahighlightflag:true,
            datahighlightcustomclass:"color-text",
        textclass:"lhs-row-1",
        textdata:data.string.plu3
    },{

      textclass:"lhs-row-1",
      textdata:data.string.blank
    },
    {
      datahighlightflag:true,
      datahighlightcustomclass:"color-text",
        textclass:"lhs-row-1",
        textdata:data.string.plu3
      }],
              fourrowdata:[{
                datahighlightflag:true,
                    datahighlightcustomclass:"color-text",
                textclass:"lhs-row-2",
                textdata:data.string.p8text16
            },{

              textclass:"lhs-row-2",
              textdata:data.string.equal
            },
            {
              datahighlightflag:true,
              datahighlightcustomclass:"color-text",
                textclass:"lhs-row-2",
                textdata:data.string.p8text17
              }],
              fiverowdata: [{
                        textclass:"lhs-row-3",
                        textdata:data.string.p
                },
                {
                    textclass:"lhs-row-3",
                    textdata:data.string.equal
                },
                {

                    inputdata:true,
                    inputclass:"num-box",
              }]
          }]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy01", src: imgpath+"chibi_fairy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},



			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s8_p2.ogg"},
			{id: "sound_2a", src: soundAsset+"s8_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s8_p3_2.ogg"},
			{id: "sound_2", src: soundAsset+"s8_p3_3.ogg"},
			{id: "sound_4", src: soundAsset+"s8_p5_3.ogg"},
			{id: "sound_5", src: soundAsset+"s8_p6.ogg"},
			{id: "s8_p3_last", src: soundAsset+"s8_p3_last.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		var wrdcount = 0;
  	put_speechbox_image(content, countNext);

		// random number generator
			function rand_generator(limit){
				var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
				return randNum;
			}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			sound_nav("sound_1");
			break;
			 case 2:
			 randomize(".optionsdiv");
			 $(".lhs,.lhs-row").delay(1000).show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_2a");
			 current_sound.play();
			 current_sound.on("complete", function(){
			 $(".sp-1,.fairy-1").show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_2b");
			 current_sound.play();
			 current_sound.on("complete", function(){
					 $(".optionsdiv").css("opacity","1");
			 });
		 });
			 break;
			 case 3:
			 $(".lhs").delay(1000).show(0);
			 $(".lhs-row").delay(2000).show(0);
			 $(".lhs-row-1").delay(3000).show(0);
			 $(".lhs-row-2").delay(4000).show(0);
			 $(".lhs-row-3").delay(5000).show(0);
			 $(".note").delay(6000).show(0);
			 $(".sp-3,.fairy-3").delay(7000).show(0);
			 nav_button_controls(8000);
			 break;
			 case 4:
			 randomize(".optionsdiv");
			 $(".lhs,.lhs-row").delay(1000).show(0);
			 $(".sp-1,.fairy-1").show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_2b");
			 current_sound.play();
			 current_sound.on("complete", function(){
				 $(".optionsdiv").css("opacity","1");
			 });
			 break;
			 case 5:
			 $('.correct-icon').attr('src', preload.getResult('correct').src);
			 $('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

			 $(".lhs,.lhs-row,.lhs-row-1,.lhs-row-2,.lhs-row-3,.num-box,.sbmt").show(0);
			 $('.sbmt').click(function(){
				 var inputVal = parseInt($('.num-box').val());
				 var answer = 12;
				 if( inputVal == answer){
				 play_correct_incorrect_sound(1);
				 $(this).addClass("notclk");
				 $('.tryagain-button').hide(0);
				 $('.num-box').attr('disabled', 'true');
				 $('.num-box').addClass('correct');
				 $('.num-box').parent().children('.correct-icon').show(0);
					$(".sp-3,.fairy-3").show(0);
					setTimeout(function(){
						sound_nav("sound_5");
					},1000);
			 } else{
				 play_correct_incorrect_sound(0);
				 $('.tryagain-button').show(0);
				 $('.num-box').addClass('incorrect');
				 $('.num-box').parent().children('.incorrect-icon').show(0);
				 $('.num-box').attr('disabled', 'true');
				 $(this).addClass("notclk");
			 }
		 });
			 $('.tryagain-button').click(function(){
				 templateCaller();
			 });
			 break;
			default:
				nav_button_controls(0);
				break;
		}

		$(".optionscontainer").click(function(){
			if($(this).hasClass("correct")){
				$(this).children(".corctopt").show(0);
				// play_correct_incorrect_sound(1);
				$(this).addClass('corrects');
				$(" .optionscontainer").css("pointer-events","none");
				$(".sp-2,.fairy-2").show(0);
				$(".lhs-row-1").delay(1000).show(0);
				$(".lhs-row-2").delay(2000).show(0);
				$(".sp-1,.fairy-1").hide(0);
				if (countNext == 2){
					sound_nav("s8_p3_last");					
				}else{
					nav_button_controls(3000);
					play_correct_incorrect_sound(1);
				}

			}else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('incorrect');
				$(this).css("pointer-events","none");
			}
		});
	}
	function showObo(count,wrdcount){
		$(".oboTxt:eq("+wrdcount+")").delay(1000).show(0);
		setTimeout(function(){
			if(wrdcount<=count){
				wrdcount+=1;
				showObo(count,wrdcount);
			}
		},1000);
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7text2,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p7text8,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				imagelabelclass:"toptext",
					datahighlightflag:true,
					datahighlightcustomclass:"oboTxt hidn",
				// imagelabeldata:data.string.p7text3
				imagelabeldata:data.string.testtxt
			}

		]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p7text
			},{
					optcontainerextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.p7text5
			},{
				optcontainerextra:"opti opti3 correct",
				optaddclass:"",
				optdata:data.string.p7text6
			},{
					optcontainerextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.p7text7
			}]
		}]
	},
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7text9,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p7text10,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				imagelabelclass:"toptext",
					datahighlightflag:true,
					datahighlightcustomclass:"oboTxt hidn",
				// imagelabeldata:data.string.p7text3
				imagelabeldata:data.string.testtxt
			}
		]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p7text11
			},{
					optcontainerextra:"opti opti2 correct",
				optaddclass:"",
				optdata:data.string.p7text12
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p7text13
			},{
					optcontainerextra:"opti opti4",
				optaddclass:"",
				optdata:data.string.p7text14
			}]
		}]
  },
	//slide 3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7text15,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p7text16,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				imagelabelclass:"tptxt",
					// datahighlightflag:true,
					// datahighlightcustomclass:"oboTxt hidn",
				splitintofractionsflag: true,
				imagelabeldata:data.string.p7text12
			},{
				imagelabelclass:"toptext",
					datahighlightflag:true,
					datahighlightcustomclass:"oboTxt hidn",
				splitintofractionsflag: true,
				imagelabeldata:data.string.p7text21
			}
		]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p7text17
			},{
					optcontainerextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.p7text18
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p7text19
			},{
					optcontainerextra:"opti opti4 correct",
				optaddclass:"",
				optdata:data.string.p7text20
			}]
		}]
	},
	//slide 4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.p7text1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'chibi fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'chibi fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p7text22,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-2',
			textdata : data.string.p7text23,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				imagelabelclass:"toptext",
					datahighlightflag:true,
					datahighlightcustomclass:"oboTxt hidn",
			splitintofractionsflag: true,
				imagelabeldata:data.string.p7text21
			}
		]
		}],
		exetype1:[{
			optionsdivclass:"optionsdiv",
			exeoptions:[{
				optcontainerextra:"opti opti1",
				optaddclass:"",
				optdata:data.string.p7text24
			},{
					optcontainerextra:"opti opti2",
				optaddclass:"",
				optdata:data.string.p7text25
			},{
				optcontainerextra:"opti opti3",
				optaddclass:"",
				optdata:data.string.p7text26
			},{
					optcontainerextra:"opti opti4 correct",
				optaddclass:"",
				optdata:data.string.p7text27
			}]
		}]
},
//slide 5
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'diy-bg',
	uppertextblockadditionalclass: 'uppertitle',
	uppertextblock:[{
		textdata: data.string.p7text1,
		textclass: "title-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy01',
			imgclass:'chibi fairy-1',
		}]
	}],
	speechbox:[{
		speechbox: 'sp-1',
		textdata : data.string.p7text28,
		textclass : 'text-box',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	}],
	imagedivblock:[{
			imagedivclass:"mainImgContain",
			imagestoshow:[{
				imgclass: 'chalkboard',
				imgid : 'chalkboard',
				imgsrc: '',
			}],
			imagelabels:true,
			imagelabels:[{
			imagelabelclass:" firsrow",
			imagelabeldata:data.string.p7text29
		},{
			imagelabelclass:"hide1 secrow",
			imagelabeldata:data.string.p7text27
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"color-text",
			imagelabelclass:"hide1 thdrow",
			imagelabeldata:data.string.p7text30
		},{
			imagelabelclass:"hide1 fourow",
			imagelabeldata:data.string.p7text31
		},{
			imagelabelclass:"hide1 fifrow",
			imagelabeldata:data.string.p7text32
		}]
	}],
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy01", src: imgpath+"chibi_fairy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1a", src: soundAsset+"s7_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s7_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s7_p2_3.ogg"},
			{id: "sound_2a", src: soundAsset+"s7_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s7_p3_2.ogg"},
			{id: "sound_3a", src: soundAsset+"s7_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s7_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s7_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s7_p5_2.ogg"},
			{id: "sound_5", src: soundAsset+"s7_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/
	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		var wrdcount = 0;
			splitintofractions($board);
  	put_speechbox_image(content, countNext);

		// random number generator
			function rand_generator(limit){
				var randNum = Math.floor(Math.random() * (limit - 1 +1)) + 1;
				return randNum;
			}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			 randomize(".optionsdiv");
			 // createjs.Sound.stop();
			 // current_sound = createjs.Sound.play("sound_1a");
			 // current_sound.play();
			 // current_sound.on("complete", function(){
			 $(".sp-1,.fairy-1").show(0);
			 $(".oboTxt:eq(0)").show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_1b");
			 current_sound.play();
			 current_sound.on("complete", function(){
			 $(".optionsdiv").css("opacity","1");
		 });
	 // });
			 break;
			 case 2:
			 randomize(".optionsdiv");
			 $(".sp-1,.fairy-1").show(0);
			 $(".oboTxt:eq(0)").delay(200).show(0);
			 $(".oboTxt:eq(1)").delay(800).show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_2a");
			 current_sound.play();
			 current_sound.on("complete", function(){
				 $(".optionsdiv").css("opacity","1");
			 });
			 break;
			 case 3:
			 randomize(".optionsdiv");
			 $(".sp-1,.fairy-1").show(0);
			 $(".tptxt").delay(1000).show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_3a");
			 current_sound.play();
			 current_sound.on("complete", function(){
				 $(".optionsdiv").css("opacity","1");
			 });
			 break;
			 case 4:
			 randomize(".optionsdiv");
			 $(".sp-1,.fairy-1").show(0);
			 $(".oboTxt:eq(0),.oboTxt:eq(1)").delay(1000).show(0);
			 createjs.Sound.stop();
			 current_sound = createjs.Sound.play("sound_4a");
			 current_sound.play();
			 current_sound.on("complete", function(){
				 $(".optionsdiv").css("opacity","1");
			 });
			 break;
			 case 5:
			  $(".sp-1,.fairy-1").show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5");
				current_sound.play();
				current_sound.on("complete", function(){
		  	$(".firsrow").delay(0).show(0);
			 	$(".secrow").delay(1000).show(0);
				$(".thdrow").delay(3000).show(0);
			 	$(".fourow").delay(4000).show(0);
				$(".fifrow").delay(5000).show(0);
				nav_button_controls(5500);
			});
				break;
			default:
				nav_button_controls(0);
				break;
		}

		$(".optionscontainer").click(function(){
			if($(this).hasClass("correct")){
				$(this).children(".corctopt").show(0);
				// play_correct_incorrect_sound(1);
				$(this).addClass('corrects');
				$(" .optionscontainer").css("pointer-events","none");
				$(".sp-2,.fairy-2").show(0);
				if(countNext==1){
					showObo(0,1);
					$(".oboTxt:eq(1)").addClass("hi");
					sound_nav("sound_1c");
				}
				else if(countNext==2){
					showObo(0,2);
					$(".oboTxt:eq(3)").delay(2000).show(0);
					sound_nav("sound_2b");
				}
				else if(countNext==3){
					showObo(0,1);
					$(".tptxt").hide(0);
					$(".oboTxt:eq(0)").show(0);
					$(".oboTxt:eq(1)").delay(2000).show(0);
					sound_nav("sound_3b");
				}
				else{
								showObo(0,2);
								sound_nav("sound_4b");
				}
				$(".sp-1,.fairy-1").hide(0);

			}else{
				$(this).children(".wrngopt").show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('incorrect');
				$(this).css("pointer-events","none");
			}
		});
	}
	function showObo(count,wrdcount){
		$(".oboTxt:eq("+wrdcount+")").delay(1000).show(0);
		setTimeout(function(){
			if(wrdcount<=count){
				wrdcount+=1;
				showObo(count,wrdcount);
			}
		},1000);
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

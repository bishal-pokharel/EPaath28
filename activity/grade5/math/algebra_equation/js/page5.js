var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}]
	},{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-chibi',
			}]
		}],

		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"sbmt",
					imagelabeldata:data.string.submit
				},{
					imagelabelclass:"tryagain-button",
					imagelabeldata:data.string.tryagain
				}]
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p5text2,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
			table:[
						{
							firstrowdata	:[
										{
											datahighlightflag : true,
											datahighlightcustomclass : 'symbol',
												textclass:"row-1",
												textdata:data.string.lhs
										},
										{
												textclass:"equal2",
												textdata:data.string.blank
										},
										{
												textclass:"row-1",
												textdata:data.string.rhs
										}
								],
								 secondrowdata:[
										{

												textclass:"row-2",
												textdata:data.string.n5
										},
										{
												textclass:"equal2",
												textdata:data.string.equal
										},
										{
												textclass:"row-2",
												textdata:data.string.n5
										}
								],
								thirdrowdata:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'highli',
												textclass:"row-3",
												textdata:data.string.mul
										},
										{
												textclass:"equal3",
												textdata:data.string.equal
										},
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'highli',
												textclass:"row-3",
												textdata:data.string.mul
										}
								],
								fourrowdata:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'symbol',
												inputclass:"num-box",
												inputdata:true,
												// textdata:data.string.isgreaterthan
										},
										{
												textclass:"content",
												// textdata:data.string.p1text9
										},
										{
												inputdata:true,
												inputclass:"box-num",
												// textdata:data.string.p1text19
										}
								],
								fifthrowdata: [
									{
											datahighlightflag : true,
											datahighlightcustomclass : 'symbol',
											textclass:"box-1",
											textdata:data.string.n15
									},
									{
											textclass:"equal4",
											textdata:data.string.equal
									},
									{
											textclass:"box-1",
											textdata:data.string.n15

								}]
						}
				]
	},{
		//slide2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-chibi',
			}]
		}],

		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"sbmt",
					imagelabeldata:data.string.submit
				},{
					imagelabelclass:"tryagain-button",
					imagelabeldata:data.string.tryagain
				}]
			}],
			speechbox:[{
				speechbox: 'sp-1',
				textdata : data.string.p5text2,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}],
			table:[
						{
							firstrowdata	:[
										{
											datahighlightflag : true,
											datahighlightcustomclass : 'symbol',
												textclass:"row-1",
												textdata:data.string.lhs
										},
										{
												textclass:"equal2",
												textdata:data.string.blank
										},
										{
												textclass:"row-1",
												textdata:data.string.rhs
										}
								],
								 secondrowdata:[
										{

												textclass:"row-2",
												textdata:data.string.n15
										},
										{
												textclass:"equal2",
												textdata:data.string.equal
										},
										{
												textclass:"row-2",
												textdata:data.string.n15
										}
								],
								thirdrowdata:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'highli',
												textclass:"row-3",
												textdata:data.string.div
										},
										{
												textclass:"equal3",
												textdata:data.string.equal
										},
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'highli',
												textclass:"row-3",
												textdata:data.string.div
										}
								],
								fourrowdata:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'symbol',
												inputclass:"num-box",
												inputdata:true,
												// textdata:data.string.isgreaterthan
										},
										{
												textclass:"content",
												// textdata:data.string.p1text9
										},
										{
												inputdata:true,
												inputclass:"box-num",
												// textdata:data.string.p1text19
										}
								],
								fifthrowdata: [
									{
											datahighlightflag : true,
											datahighlightcustomclass : 'symbol',
											textclass:"box-1",
											textdata:data.string.n5
									},
									{
											textclass:"equal4",
											textdata:data.string.equal
									},
									{
											textclass:"box-1",
											textdata:data.string.n5

								}]
						}
				]
	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-title',
		uppertextblock:[{
			textdata: data.string.p5text4,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5text5,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-3',
			textdata : data.string.p5text6,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},{
    //slide 4
    contentnocenteradjust: true,
    contentblockadditionalclass: 'diy-bg',
    uppertextblockadditionalclass: 'upper-title',
    uppertextblock:[{
      textdata: data.string.p5text4,
      textclass: "eqn-title",
    }],
    imageblock:[{
      imagestoshow:[{
        imgid:'fairy01',
        imgclass:'fairy-2',
      }]
    }],
    speechbox:[{
      speechbox: 'sp-2',
      textdata : data.string.p5text7,
      textclass : 'text-2',
      imgclass: 'box',
      imgid : 'spbox02',
      imgsrc: '',
    }]
  },{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text8,
			textclass: "title-text-add",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				// datahighlightflag:"true",
				// datahighlightcustomclass:"color-text",
				imagelabelclass:"top-text",
				imagelabeldata:data.string.a
			},{
				imagelabelclass:"middle-text1",
				imagelabeldata:data.string.p5text11
			},{
				imagelabelclass:"bottom-text",
				imagelabeldata:data.string.b
			},{
				imagelabelclass:"examp",
				imagelabeldata:data.string.example
			},{
				imagelabelclass:" top-text-ex",
				imagelabeldata:data.string.c
			},{
				datahighlightflag:"true",
			 	datahighlightcustomclass:"color-text",
				imagelabelclass:"middle-text-ex",
				imagelabeldata:data.string.d
			},{
				imagelabelclass:"bottom-text-ex",
				imagelabeldata:data.string.e
			}]
		}],
			speechbox:[{
				speechbox: 'sp-a',
				textdata : data.string.p5text9,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			},{
				speechbox: 'sp-b',
				textdata : data.string.p5text10,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			}]
	},{
    //slide 6
    contentnocenteradjust: true,
    contentblockadditionalclass: 'diy-bg',
    uppertextblockadditionalclass: 'upper-text',
    uppertextblock:[{
      textdata: data.string.p5text15,
      textclass: "title-text-add",
    }],
    imageblock:[{
      imagestoshow:[{
        imgid:'fairy01',
        imgclass:'fairy-1',
      },{
        imgid:'fairy01',
        imgclass:'fairy-anim',
      }]
    }],
    imagedivblock:[{
        imagedivclass:"mainImgContain",
        imagestoshow:[{
          imgclass: 'chalkboard-1',
          imgid : 'chalkboard',
          imgsrc: '',
        }],
      imagelabels:true,
      imagelabels:[{
        // datahighlightflag:"true",
        // datahighlightcustomclass:"color-text",
        imagelabelclass:"top-text",
        imagelabeldata:data.string.a
      },{
        imagelabelclass:"middle-text tex",
        imagelabeldata:data.string.p5text14
      },{
        imagelabelclass:"bottom-text",
        imagelabeldata:data.string.f
      },{
        imagelabelclass:"examp",
        imagelabeldata:data.string.example
      },{
        imagelabelclass:" top-text-ex",
        imagelabeldata:data.string.ten
      },{
        datahighlightflag:"true",
        datahighlightcustomclass:"color-text",
        imagelabelclass:"middle-text-ex",
        imagelabeldata:data.string.exa
      },{
        imagelabelclass:"bottom-text-ex",
        imagelabeldata:data.string.h
      }]
    }],
      speechbox:[{
        speechbox: 'sp-a',
        textdata : data.string.p5text12,
        textclass : 'text-1',
        imgclass: 'box',
        imgid : 'spbox04',
        imgsrc: '',
      },{
        speechbox: 'sp-b',
        textdata : data.string.p5text13,
        textclass : 'text-1',
        imgclass: 'box',
        imgid : 'spbox04',
        imgsrc: '',
      }]
  },{
		//slide 7
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text16,
			textclass: "title-text-add",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				// datahighlightflag:"true",
				// datahighlightcustomclass:"color-text",
				imagelabelclass:"top-text",
				imagelabeldata:data.string.a
			},{
				imagelabelclass:"middle-text2 tex",
				imagelabeldata:data.string.p5text19
			},{
				imagelabelclass:"bottom-text",
				imagelabeldata:data.string.i
			},{
				imagelabelclass:"examp",
				imagelabeldata:data.string.example
			},{
				imagelabelclass:" top-text-ex",
				imagelabeldata:data.string.j
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"color-text",
				imagelabelclass:"middle-text-ex",
				imagelabeldata:data.string.k
			},{
				imagelabelclass:"bottom-text-ex",
				imagelabeldata:data.string.l
			}]
		}],
			speechbox:[{
				speechbox: 'sp-a',
				textdata : data.string.p5text17,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			},{
				speechbox: 'sp-b',
				textdata : data.string.p5text18,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			}]
	},{
		//slide 8
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text23,
			textclass: "title-text-add",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
			imagelabels:true,
			imagelabels:[{
				// datahighlightflag:"true",
				// datahighlightcustomclass:"color-text",
				imagelabelclass:"top-text",
				imagelabeldata:data.string.a
			},{
				imagelabelclass:"middle-text2 tex",
				imagelabeldata:data.string.p5text22
			},{
				imagelabelclass:"bottom-text",
				imagelabeldata:data.string.m
			},{
				imagelabelclass:"examp",
				imagelabeldata:data.string.example
			},{
				imagelabelclass:" top-text-ex",
				imagelabeldata:data.string.l
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"color-text",
				imagelabelclass:"middle-text-ex",
				imagelabeldata:data.string.n
			},{
				imagelabelclass:"bottom-text-ex",
				imagelabeldata:data.string.o
			}]
		}],
			speechbox:[{
				speechbox: 'sp-a',
				textdata : data.string.p5text20,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			},{
				speechbox: 'sp-b',
				textdata : data.string.p5text21,
				textclass : 'text-1',
				imgclass: 'box',
				imgid : 'spbox04',
				imgsrc: '',
			}]
	},{
		//slide 9
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-text',
		uppertextblock:[{
			textdata: data.string.p5text24,
			textclass: "title-text-add",
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}]
			}],
			tab:[
						{
							firstrowdata	:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'font',
												textclass:"tab-text-1",
												textdata:data.string.p5text25
										},
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'font',
												textclass:"tab-text-2",
												textdata:data.string.p5text26
										}
								],
								 secondrowdata:[
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'font',
												textclass:"tab-text-3",
												textdata:data.string.p5text27
										},
										{
												datahighlightflag : true,
												datahighlightcustomclass : 'font',
												textclass:"tab-text-4",
												textdata:data.string.p5text28
										}]
									}]
	},{
		//slide 10
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-c',
			textdata : data.string.p5text29,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy01", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},



			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1a", src: soundAsset+"s5_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s5_p2_2.ogg"},
			{id: "sound_2a", src: soundAsset+"s5_p3_1.ogg"},
			{id: "sound_3a", src: soundAsset+"s5_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s5_p4_2.ogg"},
			{id: "sound_3c", src: soundAsset+"s5_p4_3.ogg"},
			{id: "sound_4", src: soundAsset+"s5_p5.ogg"},
			{id: "sound_5a", src: soundAsset+"s5_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s5_p6_2.ogg"},
			{id: "sound_5c", src: soundAsset+"s5_p6_3.ogg"},
			{id: "sound_6a", src: soundAsset+"s5_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s5_p7_2.ogg"},
			{id: "sound_6c", src: soundAsset+"s5_p7_3.ogg"},
			{id: "sound_7a", src: soundAsset+"s5_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s5_p8_2.ogg"},
			{id: "sound_7c", src: soundAsset+"s5_p8_3.ogg"},
			{id: "sound_8a", src: soundAsset+"s5_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s5_p9_2.ogg"},
			{id: "sound_8c", src: soundAsset+"s5_p9_3.ogg"},
			{id: "sound_9a", src: soundAsset+"s5_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s5_p10_2.ogg"},
			{id: "sound_9c", src: soundAsset+"s5_p10_3.ogg"},
			{id: "sound_9d", src: soundAsset+"s5_p10_4.ogg"},
			{id: "sound_10", src: soundAsset+"s5_p11.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
  	put_speechbox_image(content, countNext);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".row-1").delay(1000).show(0);
			$(".row-2,.equal2").delay(2000).show(0);
			$(".row-3,.equal3").delay(3000).show(0);
			$(".num-box,.box-num,.sbmt").delay(4000).show(0);
			$(".fairy-chibi,.sp-1").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1b");
			current_sound.play();
		});
			break;
			case 2:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".row-1").delay(1000).show(0);
			$(".row-2,.equal2").delay(2000).show(0);
			$(".row-3,.equal3").delay(3000).show(0);
			$(".num-box,.box-num,.sbmt").delay(4000).show(0);
			$(".fairy-chibi,.sp-1").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1b");
			current_sound.play();
		});
			break;
			case 3:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_3a");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".fairy-2,.sp-2").show(0);
				current_sound = createjs.Sound.play("sound_3b");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".sp-3").show(0);
				current_sound = createjs.Sound.play("sound_3c");
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
				});
			});
		});
	},1000);
			break;
			case 4:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_3a");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".fairy-2,.sp-2").show(0);
				current_sound = createjs.Sound.play("sound_4");
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
				});
		});
	},1000);
			break;
			case 5:
			case 6:
			case 7:
			case 8:
			sound_nav_player("sound_"+countNext+"a", "sound_"+countNext+"b", "sound_"+countNext+"c");
			break;
			case 9:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".tab-text-1").show(0);
			current_sound = createjs.Sound.play("sound_9a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".tab-text-2").show(0);
			current_sound = createjs.Sound.play("sound_9b");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".tab-text-3").show(0);
			current_sound = createjs.Sound.play("sound_9c");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".tab-text-4").show(0);
			current_sound = createjs.Sound.play("sound_9d");
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		});
	});
});
});
			break;
			case 10:
			sound_nav("sound_10");
			break;
			default:
				nav_button_controls(0);
				break;
		}
		$('.sbmt').click(function(){
			var inputVal = parseInt($('.num-box').val());
			var inputVal1 = parseInt($('.box-num').val());
			// var answer = 15;
			if(countNext==1 && inputVal ==  15 && inputVal1==15 || countNext==2 && inputVal ==  5 && inputVal1==5 ){
			play_correct_incorrect_sound(1);
			$('.sbmt').removeClass('active-submit');
			$('.tryagain-button').hide(0);
			$('.num-box,.box-num').attr('disabled', 'true');
			$('.num-box,.box-num').addClass('corrects');
			$('.num-box,.box-num').parent().children('.corctopt').show(0);
			$(".box-1,.equal4").show(0);
			nav_button_controls(0);
		} else{
			play_correct_incorrect_sound(0);
			$('.tryagain-button').show(0);
			$('.num-box,.box-num').addClass('incorrects');
			$('.num-box,.box-num').parent().children('.wrngopt').show(0);
			$('.num-box,.box-num').attr('disabled', 'true');
		}
	});
		$('.tryagain-button').click(function(){
			templateCaller();
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function sound_nav_player(sound_id1, sound_id2, sound_id3){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id1);
		current_sound.play();
		current_sound.on("complete", function(){
		createjs.Sound.stop();
		$(".fairy-1, .sp-a").show(0);
		current_sound = createjs.Sound.play(sound_id2);
		current_sound.play();
		current_sound.on("complete", function(){
		$(".top-text").delay(1000).show(0);
		$(".middle-text,.middle-text1,.middle-text2").delay(2000).show(0);
		$(".bottom-text").delay(3000).show(0);
		$(".examp").delay(4000).show(0);
		$(".top-text-ex").delay(5000).show(0);
		$(".middle-text-ex").delay(6000).show(0);
		$(".bottom-text-ex").delay(7000).show(0);
		$(".fairy-anim").delay(8000).show(0);
		setTimeout(function () {
		$(".sp-a,.fairy-1").hide(0);
		},8000);
		$(".sp-b").delay(11500).show(0);
		setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id3);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		},11500);
	});
});
	}

	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}


	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

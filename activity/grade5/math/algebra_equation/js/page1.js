var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"titletxt",
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "titletext",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'coverpage',
			}]
		}],
	},
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chibi-fairy',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p1text1,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},{
		//slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass: 'text-box end-anim wobble ',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "txt",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'kite',
				imgclass:'kite',
			}]
		}]
	},{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"small-box",
			textdata: data.string.p1text3,
			textclass: "middle-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chibi-fairy-1',
			},{
				imgid:'fairy04',
				imgclass:'chibi-fairy fairy',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p1text4,
			textclass : 'text-2',
			imgclass: 'box',
			imgid : 'spbox01',
			imgsrc: '',
		},{
			speechbox: 'sp-1',
			textdata : data.string.p1text6,
			textclass : 'text-3',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
		optioncontainer: 'bottom-option-container',
		textdata: data.string.p1text5,
		textclass: "box-text",
		optionblock:[{
			optionclass: 'bottom-option opt-1',
			textdivclass: 'bttexxt',
			textdata: data.string.n1,
		},{
			optionclass: 'bottom-option opt-2',
			textdivclass: 'bttexxt correct-ans',
			textdata: data.string.n2,
		},{
			optionclass: 'bottom-option opt-3',
			textdivclass: 'bttexxt',
			textdata: data.string.n3,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n6,
		}]
	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass: 'text-box end-anim wobble ',
		uppertextblock:[{
			textdata: data.string.p1text7,
			textclass: "txt",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'kite',
				imgclass:'kite',
			}]
		}]
	},{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"text-highlight",
			textdata: data.string.p1text8,
			textclass: "middle-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chibi-fairy fairy-1',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p1text9,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"text-highlight",
			textdata: data.string.p1text8,
			textclass: "middle-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chibi-fairy fairy-1',
			}		]
		}],
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p1text10,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			datahighlightflag:"true",
			datahighlightcustomclass:"text-highlight-1",
			speechbox: 'sp-4',
			textdata : data.string.p1text11,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'blackboard',
			imgsrc: '',
		}]
	},{
//slide 6
contentnocenteradjust: true,
contentblockadditionalclass: 'bg-diy',
extratextblock:[{
	datahighlightflag:"true",
	datahighlightcustomclass:"text-highlight",
	textdata: data.string.p1text8,
	textclass: "middle-text",
}],
imageblock:[{
	imagestoshow:[{
		imgid:'fairy',
		imgclass:'chibi-fairy fairy-1',
	}		]
}],
speechbox:[{
	speechbox: 'sp-3',
	textdata : data.string.p1text12,
	textclass : 'text-1',
	imgclass: 'box',
	imgid : 'spbox02',
	imgsrc: '',
},{
	datahighlightflag:"true",
	datahighlightcustomclass:"text-highlight-1",
	speechbox: 'sp-4',
	textdata : data.string.p1text11,
	textclass : 'text-1',
	imgclass: 'box',
	imgid : 'blackboard',
	imgsrc: '',
}]
},{
	//slide 7
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text13,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text14,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 8
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text15,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text14,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 9
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text16,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text17,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 10
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text18,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text17,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},
{
	//slide 13
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text19,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text20,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 14
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text21,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text20,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 15
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text22,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text23,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 16
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text24,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text23,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 17
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	extratextblock:[{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight",
		textdata: data.string.p1text8,
		textclass: "middle-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy',
			imgclass:'chibi-fairy fairy-1',
		}		]
	}],
	speechbox:[{
		speechbox: 'sp-3',
		textdata : data.string.p1text25,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{
		datahighlightflag:"true",
		datahighlightcustomclass:"text-highlight-1",
		speechbox: 'sp-4',
		textdata : data.string.p1text23,
		textclass : 'text-1',
		imgclass: 'box',
		imgid : 'blackboard',
		imgsrc: '',
	}]
},{
	//slide 18
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	uppertextblockadditionalclass: 'text-box end-anim wobble ',
	uppertextblock:[{
		textdata: data.string.p1text26,
		textclass: "text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'kite',
			imgclass:'kite',
		}]
	}]
},
//slide 19
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'bg-diy',
	cntblock:[{
		containtextblockadditionalclass:"mainContain1 heigt",
		txtclass:"top-text",
		textdata:data.string.p1text27,
		bxclass:"bigCont1",
		containtext:[{
			textclass:"quen",
			textdata:data.string.p1text29,
		},{
			textclass:"submit-button",
			textdata:data.string.submit
		},{
			textclass:"low-text",
			textdata:data.string.p1text31
		 }],
		txtandintput:"lower-div",
		inputclass:"input-class",
		txtdata: data.string.p1text30,
		textclass:"lower-text-p"
	},{
		containtextblockadditionalclass:"mainContain2 heigt",
		txtclass:"top-text",
		textdata:data.string.p1text28,
		bxclass:"bigCont2",
		containtext:[{
			textclass:"quen",
			textdata:data.string.p1text32,
		},{
			textclass:"submit-button1",
			textdata:data.string.submit
		}],
		txtandintput:"lower-div1",
		inputclass:"input-class1",
		txtdata: data.string.p1text33,
		textclass:"lower-text-p"
	}]
},
	//slide 20
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chi-fairy fai1',
			}	,{
				imgid:'fairy',
				imgclass:'chi-fairy fai2',
			}]
		}],
		cntblock:[{
			containtextblockadditionalclass:"mainContain1",
			txtclass:"top-text",
			textdata:data.string.p1text27,
			bxclass:"bigCont1",
			containtext:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"box1",
				textclass:"eqn1",
				textdata:data.string.p1text34,
			},{
				textclass:"eqn2",
				textdata:data.string.p1text35
			},{
				textclass:"eqn3",
				textdata:data.string.p1text36
			}],
			txtandintput:"lower-div",
			inputclass:"input-class",
		},{
			containtextblockadditionalclass:"mainContain2",
			txtclass:"top-text",
			textdata:data.string.p1text28,
			bxclass:"bigCont2",
			containtext:[{
				textclass:"eqn1",
				textdata:data.string.p1text37,
			},{
				textclass:"eqn2",
				textdata:data.string.p1text38
			},{
				textclass:"eqn3",
				textdata:data.string.p1text39
			}],
			txtandintput:"lower-div1",
			inputclass:"input-class1",
		}],
		speechbox:[{
			speechbox: 'sp-x',
			textdata : data.string.p1text40,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spboxss',
			imgsrc: '',
		}]

},{
	//slide 21

		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'chi-fairy fai1',
			}	,{
				imgid:'fairy',
				imgclass:'chi-fairy fai2',
			}]
		}],
		cntblock:[{
			containtextblockadditionalclass:"mainContain1",
			txtclass:"top-text",
			textdata:data.string.p1text27,
			bxclass:"bigCont1",
			containtext:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"box1",
				textclass:"eqn1",
				textdata:data.string.p1text34,
			},{
				textclass:"eqn2",
				textdata:data.string.p1text35
			},{
				textclass:"eqn3",
				textdata:data.string.p1text36
			}],
			txtandintput:"lower-div",
			inputclass:"input-class",
		},{
			containtextblockadditionalclass:"mainContain2",
			txtclass:"top-text",
			textdata:data.string.p1text28,
			bxclass:"bigCont2",
			containtext:[{
				textclass:"eqn1",
				textdata:data.string.p1text37,
			},{
				textclass:"eqn2",
				textdata:data.string.p1text38
			},{
				textclass:"eqn3",
				textdata:data.string.p1text39
			}],
			txtandintput:"lower-div1",
			inputclass:"input-class1",
		}],
		speechbox:[{
			speechbox: 'sp-x',
			textdata : data.string.p1text41,
			textclass : 'txt1',
			imgclass: 'box',
			imgid : 'spboxss',
			imgsrc: '',
		}]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"chibi_fairy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite", src: imgpath+"kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox01", src: imgpath+"text_box03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy04", src: imgpath+"chibi_fairy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blackboard", src: imgpath+"black_board01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spboxss", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3a", src: soundAsset+"s1_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s1_p4_2.ogg"},
			{id: "sound_3c", src: soundAsset+"s1_p4_3.ogg"},
			{id: "sound_3d", src: soundAsset+"s1_p4_4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5a", src: soundAsset+"s1_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s1_p6_2.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7_1.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s1_p14.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p16.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p17.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p18.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p19.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p20.ogg"},
			{id: "sound_18a", src: soundAsset+"s1_p21.ogg"},
			{id: "sound_18b", src: soundAsset+"s1_p21_1.ogg"},
			{id: "sound_19", src: soundAsset+"s1_p22.ogg"},
			{id: "sound_20", src: soundAsset+"s1_p23.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		switch(countNext) {
			case 0:
			sound_nav("sound_0");
			break;
			case 1:
			$(".sp-1").show(0);
			sound_nav("sound_1");
			break;
			case 2:
			sound_nav("sound_2");
			break;
			case 3:
			$(".bottom-option-container").hide(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".sp-2, .chibi-fairy-1").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3b");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			$(".bottom-option-container").show(0);
			current_sound = createjs.Sound.play("sound_3c");
			current_sound.play();
		});
	});
			//randomize options
			var parent = $(".bottom-option-container");
			var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
			$('.correct-icon').attr('src', preload.getResult('correct').src);
			$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
			$('.bttexxt').click(function(){
				if($(this).hasClass('correct-ans')){
					play_correct_incorrect_sound(1);
					$(this).parent().children('.correct-icon').show(0);
					$('.bttexxt').css('pointer-events', 'none');
					$(this).addClass('correct-answer');
					$(".small-box").html(2);
					$(".sp-1, .fairy").show(0);
					$(".bottom-option-container, .box-text,.sp-2,.chibi-fairy-1").hide(0);
					setTimeout(function(){
						sound_nav("sound_3d");
					},800);
				} else{
					play_correct_incorrect_sound(0);
					$(this).css('pointer-events', 'none');
					$(this).parent().children('.incorrect-icon').show(0);
					$(this).addClass('incorrect-answer');
				}
			});
			break;
			case 4:
			sound_nav("sound_"+countNext);
			break;
			case 5:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5a");
				current_sound.play();
				current_sound.on("complete", function(){
				animate1(".sp-3,.fairy-1");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_5b");
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
				});
			});
				break;
			case 6:
			case 8:
			case 10:
			case 12:
			case 14:
			case 16:
			sound_player("sound_"+countNext);
			$(".sp-3,.fairy-1").animate({
				opacity:1
			},2000,function(){
			$(".sp-4").animate({
				opacity:1,
			},3000,function(){
			$(".text-highlight-1:eq(0)").animate({
				opacity:1,
			},1000,function(){
				$(".text-highlight-1:eq(1)").animate({
					opacity:1
				},2000, function(){
					$(".text-highlight-1:eq(2)").animate({
						opacity:1
					},2000, function(){
						$(".hiddentext").show(0);
					});
				});
			});
		});
	});
		nav_button_controls(9000);
			break;
			case 7:
			case 9:
			case 11:
			case 13:
			case 15:
			case 17:
			$(".sp-4, .text-highlight-1").css("opacity", "1");
			animate1(".sp-3,.fairy-1");
			sound_nav("sound_"+countNext);
			break;
		case 18:
		sound_player("sound_18a");
		$('.submit-button').click(function(){
			createjs.Sound.stop();
			var inputVal = parseInt($('.input-class').val());
			if( inputVal == 7){
			play_correct_incorrect_sound(1);
			$('.input-class').attr('disabled', 'true');
			$('.input-class').addClass('correct-answer');
			$('.input-class').parent().children('.correct-icon').show(0);
			$(".fai1").hide(0);
			$(".fai2,.mainContain2").show(0);
			setTimeout(function(){
				sound_player("sound_18b");
				$('.submit-button1').click(function(){
					createjs.Sound.stop();
					var inputVal = parseInt($('.input-class1').val());
					if( inputVal == 6){
					play_correct_incorrect_sound(1);
					$('.input-class1').attr('disabled', 'true');
					$('.input-class1').addClass('correct-answer');
					$('.input-class1').parent().children('.correct-icon').show(0);
					nav_button_controls(0);
				} else{
					play_correct_incorrect_sound(0);
					$('.input-class1').addClass('incorrect-answer');
					$('.input-class1').parent().children('.incorrect-icon').show(0);
				}
			});
		},1000);
		} else{
			play_correct_incorrect_sound(0);
			$('.input-class').addClass('incorrect-answer');
			$('.input-class').parent().children('.incorrect-icon').show(0);

		}
	});
	break;
	case 19:
	case 20:
	$(".mainContain2").show(0);
	$(".lower-div1,.lower-div").hide(0);
	sound_nav("sound_"+countNext);
		break;

			default:

				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function animate1(txtclass){
		$(txtclass).animate({
			opacity:1
		},2000);
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

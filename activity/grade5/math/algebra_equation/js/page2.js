var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass: 'text-box end-anim wobble ',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "txt",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'kite',
				imgclass:'kite',
			}]
		}]
	},{
		//slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.p2text2,
			textclass: "middle-text",
		}],
		imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }]
			}]
	},{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				}]
			}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text4,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]

	},{
		//slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
    imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"high",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"animation",
				}]
			}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text5,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"lhs",
					textdata:data.string.blank
				},
				{
						textclass:"lhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					textclass:"lhs-row",
					textdata:data.string.p2text7
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"lhs-row",
					textdata:data.string.n7
				}]
		}],
		uppertextblockadditionalclass: "moreinfo",
		uppertextblock:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"color-pin",
				textclass: "heading",
				textdata: data.string.p2text9
		},{
				textclass: "close_button",
				textdata: "&nbsp;"
			}],
		learn_more: true

	},{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
    imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"anim",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text10,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"lhs",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"color",
					textclass:"lhs-row",
					textdata:data.string.p2text7_1
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"rhs-row",
					textdata:data.string.n7
			}],
			thirdrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row-eqn",
					textdata:data.string.minus2
			},{
				textclass:"lhs-row",
				textdata:data.string.blank
			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row-eqn",
					textdata:data.string.minus2
			}]
		}],
		uppertextblockadditionalclass: "moreinfo",
		uppertextblock:[{
				textclass: "heading",
				textdata: data.string.p2text14
		},{
				textclass: "close_button",
				textdata: "&nbsp;"
			}],
		learn_more: true
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
    imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"anim",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text13,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{

				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					textclass:"lhs-row",
					textdata:data.string.p2text7
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"rhs-row",
					textdata:data.string.n7
			}],
			thirdrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row-eqn",
					textdata:data.string.minus2
			},{
				textclass:"lhs-row-eqn",
				textdata:data.string.blank
			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row-eqn",
					textdata:data.string.minus2
			}],
			fourrowdata :[{
					textclass:"row-lhs",
					textdata:data.string.x
		},{
			textclass:"row-lhs",
			textdata:data.string.equal
		},
		{
					textclass:"row-lhs",
					textdata:data.string.p2text15
			}],
			fiverowdata : [{
					textclass:"row-lhs-eqn",
					textdata:data.string.x
		},{
			textclass:"row-lhs-eqn",
			textdata:data.string.equal
		},
		{
					textclass:"row-lhs-eqn",
					textdata:data.string.n5
			}]
		}]

	},{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
    imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					// imagelabelclass:"anim",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text16,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{

				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					textclass:"lhs-row",
					textdata:data.string.p2text7
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"rhs-row",
					textdata:data.string.p2text8
			}],
			thirdrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row-eqn",
					textdata:data.string.minus2
			},{

			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row-eqn",
					textdata:data.string.minus2
			}],
			fourrowdata :[{
					textclass:"row-lhs1",
					textdata:data.string.x
		},{
			textclass:"row-lhs1",
			textdata:data.string.equal
		},
		{
					textclass:"row-rhs1",
					textdata:data.string.p2text15
			}],
			fiverowdata : [{
					textclass:"row-lhs-eqn1",
					textdata:data.string.x
		},{
			textclass:"row-lhs-eqn1",
			textdata:data.string.equal
		},
		{
					textclass:"row-rhs-eqn1",
					textdata:data.string.n5
			}]
		}]
	},{
		//slide 7
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy-1',
			}]
		}],
    imagedivblock:[{
        imagedivclass:"mainImgCont",
        imagestoshow:[{
    			imgclass: 'chalkboard',
    			imgid : 'chalkboard',
    			imgsrc: '',
        }],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"firsrow",
					imagelabeldata:data.string.p2text17
				},{
					imagelabelclass:"secrow",
					imagelabeldata:data.string.p2text18
				},{
					imagelabelclass:"thdrow",
					imagelabeldata:data.string.p2text19
				},{
					datahighlightflag:"true",
					datahighlightcustomclass:"highli",
					imagelabelclass:"fourow",
					imagelabeldata:data.string.p2text20
				},{
					imagelabelclass:"fifrow",
					imagelabeldata:data.string.p2text21
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-2',
			textdata : data.string.p2text22,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},{
		//slide 8
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}]
			}],
			speechbox:[{
				datahighlightflag:"true",
				datahighlightcustomclass:"pulse-effect",
				speechbox: 'sp-1',
				textdata : data.string.p2text23,
				textclass : 'text-2',
				imgclass: 'box',
				imgid : 'spbox',
				imgsrc: '',
			}]
	},{
		//slide 9
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"animation",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text25,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"lhs",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					textclass:"lhs-row",
					textdata:data.string.p2text26
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"lhs-row",
					textdata:data.string.n14
				}]
		}]

	},{
		//slide 10
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"anim",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text28,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{

				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row",
					textdata:data.string.p2text26_1
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row",
					textdata:data.string.n14
			}],
			thirdrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row-eqn",
					textdata:data.string.plus7
			},{

			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row-eqn",
					textdata:data.string.plus7
			}]
		}]

	},{
		//slide 11
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"text-deco",
					imagelabelclass:"top-text",
					imagelabeldata:data.string.p2text6
				},{
					imagelabelclass:"anim",
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-1',
			textdata : data.string.p2text30,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}],
		table:[{
				firstrowdata	:[{
						textclass:"lhs",
						textdata:data.string.lhs
				},{},
				{
						textclass:"rhs",
						textdata:data.string.rhs
				}],
				secondrowdata :[{
					textclass:"lhs-row",
					textdata:data.string.p2text26
			},{
				textclass:"lhs-row",
				textdata:data.string.equal
			},
			{
					textclass:"rhs-row",
					textdata:data.string.n14
			}],
			thirdrowdata :[{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"lhs-row-eqn",
					textdata:data.string.plus7
			},{

			},
			{
					datahighlightflag:"true",
					datahighlightcustomclass:"eqn-highlight",
					textclass:"rhs-row-eqn",
					textdata:data.string.plus7
			}],
			fourrowdata :[{
					textclass:"row-lhs",
					textdata:data.string.xplusz
		},{
			textclass:"row-lhs",
			textdata:data.string.equal
		},
		{
					textclass:"row-lhs",
					textdata:data.string.p2text40
			}],
			fiverowdata : [{
					textclass:"row-lhs-eqn",
					textdata:data.string.x
		},{
			textclass:"row-lhs-eqn",
			textdata:data.string.equal
		},
		{
					textclass:"row-lhs-eqn",
					textdata:data.string.n21
			}]
		}]

	},{
		//slide 12
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text24,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy-1',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"firsrow",
					imagelabeldata:data.string.p2text17
				},{
					imagelabelclass:"secrow",
					imagelabeldata:data.string.p2text31
				},{
					imagelabelclass:"thdrow",
					imagelabeldata:data.string.p2text32
				},{
					datahighlightflag:"true",
					datahighlightcustomclass:"highli",
					imagelabelclass:"fourow",
					imagelabeldata:data.string.p2text33
				},{
					imagelabelclass:"fifrow",
					imagelabeldata:data.string.p2text34
				}]
			}],
		speechbox:[{
			datahighlightflag:"true",
			datahighlightcustomclass:"pulse-effect",
			speechbox: 'sp-2',
			textdata : data.string.p2text29,
			textclass : 'text-1',
			imgclass: 'box',
			imgid : 'spbox',
			imgsrc: '',
		}]
	},{
		//slide 13
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		// uppertextblockadditionalclass:"title",
		uppertextblock:[{
			textdata: data.string.p2text35,
			textclass: "summ-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgCont",
				imagestoshow:[{
					imgclass: 'chalkboard',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					datahighlightflag:"true",
					datahighlightcustomclass:"under-line",
					imagelabelclass:"step firs-row",
					imagelabeldata:data.string.p2text36
				},{
					datahighlightflag:"true",
					datahighlightcustomclass:"under-line",
					imagelabelclass:"step sec-row",
					imagelabeldata:data.string.p2text37
				},{
					datahighlightflag:"true",
					datahighlightcustomclass:"under-line",
					imagelabelclass:"step thd-row",
					imagelabeldata:data.string.p2text38
				},{
					datahighlightflag:"true",
					datahighlightcustomclass:"under-line",
					imagelabelclass:"step fou-row",
					imagelabeldata:data.string.p2text39
				}]
			}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
			{id: "kite", src: imgpath+"kite.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy", src: imgpath+"chibi_fairy02.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "info-icon", src: imgpath+"info_icon.png", type: createjs.AbstractLoader.IMAGE},


			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_3a", src: soundAsset+"s2_p4_1.ogg"},
			{id: "sound_3b", src: soundAsset+"s2_p4_2.ogg"},
			{id: "sound_4a", src: soundAsset+"s2_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s2_p5_2.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p7.ogg"},
			{id: "sound_7a", src: soundAsset+"s2_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s2_p8_2.ogg"},
			{id: "sound_8", src: soundAsset+"s2_p9.ogg"},
			{id: "sound_9a", src: soundAsset+"s2_p10_1.ogg"},
			{id: "sound_9b", src: soundAsset+"s2_p10_2.ogg"},
			{id: "sound_10", src: soundAsset+"s2_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s2_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s2_p13.ogg"},
			{id: "sound_13", src: soundAsset+"s2_p14_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		$(".close_button").click(function(){
			$(".moreinfo").hide(500);
			createjs.Sound.stop();
		});
		switch(countNext) {
			case 2:
			$(".top-text").delay(2500).show(0);
			setTimeout(function(){
			sound_nav("sound_"+(countNext));
		},1500);
			break;
			case 3:
			$learn_more = $(".learn_more");
			$learn_more.click(function(){
				$(".moreinfo").show(500);
				setTimeout(function(){
				sound_player("sound_3b");
			},500);
			});
			setTimeout(function(){
			createjs.Sound.stop();
			$(".top-text").show(0);
			current_sound = createjs.Sound.play("sound_3a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".animation").delay(3000).show(0);
			$(".lhs").delay(5000).show(0);
			$(".lhs-row").delay(6000).show(0);
			$(".learn_more").delay(7000).show(0);
			nav_button_controls(7000);
		});
		},1500);
			break;
			case 4:
			$(".anim").hide(0);
			$learn_more = $(".learn_more");
			$learn_more.click(function(){
				$(".moreinfo").show(500);
				setTimeout(function(){
				sound_player("sound_4b");
			},500);
			});
			setTimeout(function(){
			createjs.Sound.stop();
			$(".top-text,.anim,.lhs,.rhs,.lhs-row,.rhs-row").show(0);
			current_sound = createjs.Sound.play("sound_4a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".color").addClass("eqn-highlight");
			$(".lhs-row-eqn").delay(1000).show(0);
			$(".rhs-row-eqn").delay(2000).show(0);
			$(".learn_more").delay(3000).show(0);
			nav_button_controls(4000);
		});
	},1500);
			break;
			case 5:
			$(".anim").hide(0);
			setTimeout(function(){
			createjs.Sound.stop();
			$(".top-text,.anim,.lhs,.rhs,.lhs-row,.rhs-row, .lhs-row-eqn,.rhs-row-eqn").show(0);
			current_sound = createjs.Sound.play("sound_5");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".row-lhs").delay(500).show(0);
			$(".row-lhs-eqn").delay(1000).show(0);
			nav_button_controls(1500);
		});
	},1500);
			break;
			case 6:
			$(".top-text,.anim,.lhs,.rhs,.lhs-row,.rhs-row, .lhs-row-eqn,.rhs-row-eqn").show(0);
			$(".row-lhs,.row-rhs,.row-lhs-eqn,.row-rhs-eqn").show(0).css("tr:last-child","border:3px solid red");
			setTimeout(function(){
			sound_nav("sound_6");
		},1500);
			break;
			case 7:
			setTimeout(function(){
				createjs.Sound.stop();
				$(".firsrow").show(0);
				current_sound = createjs.Sound.play("sound_7a");
				current_sound.play();
				current_sound.on("complete", function(){
					$(".secrow").delay(500).show(0);
					$(".thdrow").delay(1500).show(0);
					$(".fourow").delay(2500).show(0);
					$(".fifrow").delay(3500).show(0);
					setTimeout(function(){
						$(".sp-2,.fairy-1").show(0);
						sound_nav("sound_7b");
					},4000);
				});
			},1000);

			break;
			case 8:
			setTimeout(function(){
			sound_nav("sound_"+(countNext));
		},1500);
		break;
			case 9:

			setTimeout(function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_9b");
			current_sound.play();
			current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".top-text").show(0);
				current_sound = createjs.Sound.play("sound_9a");
				current_sound.play();
				current_sound.on("complete", function(){
			$(".animation").delay(500).show(0);
			$(".lhs").delay(2500).show(0);
			$(".rhs").delay(3500).show(0);
			$(".lhs-row").delay(4500).show(0);
			$(".rhs-row").delay(5500).show(0);
			nav_button_controls(6000);
		});
		});
	},1500);
			break;
			case 10:
			$(".anim").hide(0);
			setTimeout(function(){
			createjs.Sound.stop();
			$(".top-text,.anim,.lhs,.rhs,.lhs-row,.rhs-row").show(0);
			current_sound = createjs.Sound.play("sound_10");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".color").addClass("eqn-highlight");
			$(".lhs-row-eqn").delay(1000).show(0);
			$(".rhs-row-eqn").delay(2000).show(0);
			$(".learn_more").delay(3000).show(0);
			nav_button_controls(4000);
		});
	},1500);
			break;
			case 11:
			$(".anim").hide(0);
			setTimeout(function(){
			createjs.Sound.stop();
			$(".top-text,.anim,.lhs,.rhs,.lhs-row,.rhs-row, .lhs-row-eqn,.rhs-row-eqn").show(0);
			current_sound = createjs.Sound.play("sound_11");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".row-lhs").delay(500).show(0);
			$(".row-lhs-eqn").delay(1000).show(0);
			nav_button_controls(1500);
		});
	},1500);
			break;
			case 12:
			setTimeout(function(){
				createjs.Sound.stop();
				$(".sp-2,.fairy-1").show(0);
				current_sound = createjs.Sound.play("sound_12");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".firsrow").show(0);
				current_sound = createjs.Sound.play("sound_7a");
				current_sound.play();
				current_sound.on("complete", function(){
					$(".secrow").delay(500).show(0);
					$(".thdrow").delay(1500).show(0);
					$(".fourow").delay(2500).show(0);
					$(".fifrow").delay(3500).show(0);
					nav_button_controls(4000);
				});
			});
			},1500);

			break;
			case 13:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_13");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".firs-row").delay(1000).show(0);
			$(".sec-row").delay(2000).show(0);
			$(".thd-row").delay(3000).show(0);
			$(".fou-row").delay(4000).show(0);
			nav_button_controls(5000);
		});
			break;
			default:
			sound_nav("sound_"+(countNext));
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

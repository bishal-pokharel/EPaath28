var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}]
	},{
		//slide1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "ques-text",
		},{
			textdata: data.string.p3text2,
			textclass: "eqn-text",
		},{
			textdata: data.string.p3text3,
			textclass: "optn-text",
		}],
    optioncontainer: 'bottom-option-container',
    optionblock:[{
      optionclass: 'bottom-option opt-1',
      textdivclass: 'bttexxt',
      textdata: data.string.n9,
    },{
      optionclass: 'bottom-option opt-2',
      textdivclass: 'bttexxt correct-ans',
      textdata: data.string.n2,
    },{
      optionclass: 'bottom-option opt-3',
      textdivclass: 'bttexxt',
      textdata: data.string.n5,
    },{
      optionclass: 'bottom-option opt-4',
      textdivclass: 'bttexxt',
      textdata: data.string.n10,
    },{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n0,
		}],
		lowertextblockadditionalclass:"lower-text",
		lowertextblock:[{
			textdata:data.string.p3text5,
			textclass:"lower-ans-text"
		},{
			textdata:data.string.p3text6,
			textclass:"lsh-rhs"
		},{
			datahighlightflag: true,
			datahighlightcustomclass: "text-highlight",
			textdata:data.string.p3text7,
			textclass:"eqn-ans"
		},{
			textdata:data.string.p3text8,
			textclass:"eqn-ans-1"
		},{
			textdata:data.string.p3text9,
			textclass:"eqn-ans-2"
		}],
		uppertextblockadditionalclass: "moreinfo",
		uppertextblock:[{
				textclass: "heading",
				textdata: data.string.tips
		},{
				textclass: "heading",
				textdata: data.string.tip1
			},{
					textclass: "heading",
					textdata: data.string.tip2
				},{
					textclass: "heading",
					textdata: data.string.tip3
				},{
				textclass: "close_button",
				textdata: "&nbsp;"
			}
		],
		learn_more: true
	},{
		//slide2

		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-diy',
		extratextblock:[{
			textdata: data.string.p3text10,
			textclass: "ques-text",
		},{
			textdata: data.string.p3text11,
			textclass: "eqn-text",
		},{
			textdata: data.string.p3text3,
			textclass: "optn-text",
		}],
		optioncontainer: 'bottom-option-container',
		optionblock:[{
			optionclass: 'bottom-option opt-1',
			textdivclass: 'bttexxt',
			textdata: data.string.n100,
		},{
			optionclass: 'bottom-option opt-2',
			textdivclass: 'bttexxt',
			textdata: data.string.n128,
		},{
			optionclass: 'bottom-option opt-3',
			textdivclass: 'bttexxt',
			textdata: data.string.n79,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n80,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt correct-ans',
			textdata: data.string.n49,
		},{
			optionclass: 'bottom-option opt-4',
			textdivclass: 'bttexxt',
			textdata: data.string.n1,
		}],
		lowertextblockadditionalclass:"lower-text",
		lowertextblock:[{
			textdata:data.string.p3text15,
			textclass:"lower-ans-text"
		},{
			textdata:data.string.p3text6,
			textclass:"lsh-rhs"
		},{
			 datahighlightflag: true,
			 datahighlightcustomclass: "text-highlight",
			textdata:data.string.p3text12,
			textclass:"eqn-ans"
		},{
			textdata:data.string.p3text13,
			textclass:"eqn-ans-1"
		},{
			textdata:data.string.p3text14,
			textclass:"eqn-ans-2"
		}],
		uppertextblockadditionalclass: "moreinfo",
		uppertextblock:[{
				textclass: "heading",
				textdata: data.string.tips
		},{
				textclass: "heading",
				textdata: data.string.tip4
			},{
					// datahighlightflag: true,
					// datahighlightcustomclass: "boldit",
					textclass: "heading",
					textdata: data.string.tip5
				},{
					textclass: "heading",
					textdata: data.string.tip6
				},{
					textclass: "heading",
					textdata: data.string.tip7
				},{
					textclass: "heading",
					textdata: data.string.tip8
				},{
				textclass: "close_button",
				textdata: "&nbsp;"
			}
		],
		learn_more: true
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"chibi_fairy04.png", type: createjs.AbstractLoader.IMAGE},
			// {id: "tip-icon", src: imgpath+"tip_icon.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1a", src: soundAsset+"s3_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s3_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s3_p2_3.ogg"},
			{id: "sound_1d", src: soundAsset+"s3_p2_4.ogg"},
			{id: "sound_2a", src: soundAsset+"s3_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s3_p3_2.ogg"},
			{id: "sound_2c", src: soundAsset+"s3_p3_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
    $(".close_button").click(function(){
      createjs.Sound.stop();
      $(".moreinfo").hide(500);
    });
		$(".bottom-option-container").hide(0);
		//randomize options
		var parent = $(".bottom-option-container");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			case 1:
      $learn_more = $(".learn_more");
      $learn_more.click(function(){
        $(".moreinfo").show(500);
					sound_player("sound_1d");
      });
			$(".ques-text").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".eqn-text").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1b");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".optn-text").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_1c");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".bottom-option-container").show(0);
			$learn_more.css("pointer-events","auto");
		});
	});
});
			break;
			case 2:
			$learn_more = $(".learn_more");
			$learn_more.click(function(){
				$(".moreinfo").show(500);
					sound_player("sound_2c");
			});
			$(".ques-text").show(0);
			$(".eqn-text").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".optn-text").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2b");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".bottom-option-container").show(0);
			$learn_more.css("pointer-events","auto");
	});
});
			break;
			default:
				nav_button_controls(0);
				break;
		}
		$('.bttexxt').click(function(){
			if($(this).hasClass('correct-ans')){
				play_correct_incorrect_sound(1);
				$(this).parent().children('.correct-icon').show(0);
				$('.bttexxt').css('pointer-events', 'none');
				$(this).addClass('correct-answer');
				$(".lower-text").show(0);
				nav_button_controls(0);
			} else{
				play_correct_incorrect_sound(0);
				$(this).css('pointer-events', 'none');
				$(this).parent().children('.incorrect-icon').show(0);
				$(this).addClass('incorrect-answer');
			}
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

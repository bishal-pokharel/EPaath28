var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		extratextblock:[{
			textdata: data.string.diytext,
			textclass: "text",

		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy',
				imgclass:'fairy',
			}]
		}]
	},
	//slide 1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.tit1,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-b',
			textdata : data.string.p9text7,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-c',
			textdata : data.string.level2,
			textclass : 'txt',
			imgclass: 'nextlevelbox',
			imgid : 'yellowbox',
			imgsrc: '',
		},{
			speechbox: 'sp-a',
			textdata : data.string.p9text1,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
			cntblock:[{
			containtextblockadditionalclass: "mainImgContain",
			containtext:[{
				textclass:"quen",
				textdata:data.string.p9text2,
				// ans:data.string.ans1
			},{
				textclass:"submit-button",
				textdata:data.string.submit
			},{
				textclass:"tryagain-button",
				textdata:data.string.tryagain
			},{
					textclass:"equa",
					// textdata:data.string.n1
			}],
				txtarea:"text-area",
				txtandintput:"lower-div",
				inputclass:"input-class",
				txtdata: data.string.xequal,
				textclass:"lower-text-p"
		}],
		extratextblock:[{
			textclass:"hint",
			textdata:data.string.hinttext
		}],
		lowertextblockadditionalclass:"low-box",
		lowertextblock:[{
			textclass:"low-text",
			textdata:data.string.p9text3
		},{
			textclass: "close_button",
			textdata: "&nbsp;"
		}]
  },
	//slide 2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'uppertitle',
		uppertextblock:[{
			textdata: data.string.tit2,
			textclass: "title-text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-2',
			}]
		}],
		speechbox:[{
			speechbox: 'sp-b',
			textdata : data.string.p9text8,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		},{
			speechbox: 'sp-c',
			textdata : data.string.level3,
			textclass : 'txt',
			imgclass: 'nextlevelbox',
			imgid : 'yellowbox',
			imgsrc: '',
		},{
			speechbox: 'sp-a',
			textdata : data.string.p9text1,
			textclass : 'text-box',
			imgclass: 'box',
			imgid : 'spbox02',
			imgsrc: '',
		}],
			cntblock:[{
			containtextblockadditionalclass: "mainImgContain",
			containtext:[{
				textclass:"quen",
				textdata:data.string.p9text2,
				// ans:data.string.ans1
			},{
				textclass:"submit-button",
				textdata:data.string.submit
			},{
				textclass:"tryagain-button",
				textdata:data.string.tryagain
			},{
					textclass:"equa",
					// textdata:data.string.n1
			}],
				txtarea:"text-area",
				txtandintput:"lower-div",
				inputclass:"input-class",
				txtdata: data.string.xequal,
				textclass:"lower-text-p"
		}],
		extratextblock:[{
			textclass:"hint",
			textdata:data.string.hinttext
		}],
		lowertextblockadditionalclass:"low-box",
		lowertextblock:[{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"low-text",
			textdata:data.string.p9text9
		},{
			datahighlightflag:true,
			datahighlightcustomclass:"underline",
			textclass:"low-text",
			textdata:data.string.p9text10
		},{

			textclass: "close_button",
			textdata: "&nbsp;"
		}]
},
//slide 3
{
	contentnocenteradjust: true,
	contentblockadditionalclass: 'diy-bg',
	uppertextblockadditionalclass: 'uppertitle',
	uppertextblock:[{
		textdata: data.string.tit3,
		textclass: "title-text",
	}],
	imageblock:[{
		imagestoshow:[{
			imgid:'fairy01',
			imgclass:'fairy-1',
		},{
			imgid:'fairy01',
			imgclass:'fairy-2',
		}]
	}],
	speechbox:[{
		speechbox: 'sp-b',
		textdata : data.string.p9text4,
		textclass : 'text-box',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	},{

		speechbox: 'sp-a',
		textdata : data.string.p9text1,
		textclass : 'text-box',
		imgclass: 'box',
		imgid : 'spbox02',
		imgsrc: '',
	}],
		cntblock:[{
		containtextblockadditionalclass: "mainImgContain",
		containtext:[{
			textclass:"quen",
			textdata:data.string.p9text2,
			// ans:data.string.ans1
		},{
			textclass:"submit-button",
			textdata:data.string.submit
		},{
			textclass:"tryagain-button",
			textdata:data.string.tryagain
		},{
				textclass:"equa",
				// textdata:data.string.n1
		}],
			txtarea:"text-area",
			txtandintput:"lower-div",
			inputclass:"input-class",
			txtdata: data.string.xequal,
			textclass:"lower-text-p"
	}],
	extratextblock:[{
		textclass:"hint",
		textdata:data.string.hinttext
	}],
	lowertextblockadditionalclass:"low-box",
	lowertextblock:[{
		datahighlightflag:true,
		datahighlightcustomclass:"underline",
		textclass:"low-text",
		textdata:data.string.p9text9
	},{
		datahighlightflag:true,
		datahighlightcustomclass:"underline",
		textclass:"low-text",
		textdata:data.string.p9text10
	},{

		textclass: "close_button",
		textdata: "&nbsp;"
	}]
}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy01", src: imgpath+"chibi_fairy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "yellowbox", src: imgpath+"yellow_arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},



			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1a", src: soundAsset+"s9_p2_1.ogg"},
			{id: "sound_1b", src: soundAsset+"s9_p2_2.ogg"},
			{id: "sound_1c", src: soundAsset+"s9_p2_3.ogg"},
			{id: "sound_2", src: soundAsset+"s9_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s9_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		var wrdcount = 0;
  	put_speechbox_image(content, countNext);

		// random numberGenerator
		function randomGenerator(limit){
			var randNUm = Math.floor(Math.random() * (limit - 0 +1)) + 0;
			return randNUm;
		}

			/*for randomizing the options*/
			function randomize(parent){
				var parent = $(parent);
				var divs = parent.children();
				while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
			}

			$(".close_button").click(function(){
				$(".low-box").hide(500);
			});

			$(".quen").click(function(){
				templateCaller();
				dropedImgCount = 0;
			});

		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
			break;
			 case 1:
			 sound_player("sound_1b");
			 var qno =  randomGenerator(6);
			 var qnArray=[[ "5x = 45",9,0],
									 ["4x=16",4,1],
									 ["2x=100",50,2],
									 ["10x=50",5,3],
									 ["7x=21",3,3],
									 ["3x=27",9,5],
									 ["14=2x",7,6]];
									 var fstNum = qnArray[qno][0];
									 var ans = qnArray[qno][1];
									 $(".equa").text(fstNum);
									 $(".equa").prepend(1+") ");

				break;
				case 2:
				var qno =  randomGenerator(7);
				var qnArray=[["7x - 2 =68",10,0],
										["4x-3 =13",4,1],
										["2x + 4 =14",5,2],
										["5x + 5 =30",5,3],
										["3x - 7 =11",6,4],
										["7x - 5 =23",4,5],
										["4x + 8 =40",8,6],
										["2x + 3=25",11,7]];
										var fstNum = qnArray[qno][0];
										var ans = qnArray[qno][1];
										$(".equa").text(fstNum);
										$(".equa").prepend(1+") ");
				 break;
				 case 3:
				 var qno =  randomGenerator(6);
				 var qnArray=[["4x + 7 =2x + 13",3,0],
										 ["7x + 7 = 13 + 6x",6,1],
										 ["-10x + 2 = -8x - 8",5,2],
										 ["-2x = 14-4x",7,3],
										 ["8-x=3x",2,4],
										 ["5x-6=3x-2",2,5],
										 ["4x+10=3x+15",8,6]];
										 var fstNum = qnArray[qno][0];
										 var ans = qnArray[qno][1];
										 $(".equa").text(fstNum);
										 $(".equa").prepend(1+") ");
			 break
			default:
				nav_button_controls(0);
				break;
		}
		$('.hint').click(function(){
		 $(".low-box").show(0);
		 // $(".hint").addClass("hov");
		});
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		$('.submit-button').click(function(){
			var inputVal = parseInt($('.input-class').val());
			// var answer = 12;
			if( inputVal == ans){
			play_correct_incorrect_sound(1);
			// $('.submit-button').removeClass('active-submit');
			$('.input-class').attr('disabled', 'true');
			$('.input-class').addClass('corrects');
			$('.input-class').siblings('.corctopt').show(0);
					$('.input-class').siblings('.wrngopt').hide(0);
			if(countNext==1){
			$(".p-textarea").html(eval("data.string.anser"+qnArray[qno][2])).css("z-index","1");
			setTimeout(function(){
				sound_nav("sound_1c");
			},1000);

		}else if(countNext==2){
			$(".p-textarea").html(eval("data.string.answ"+qnArray[qno][2])).css("z-index","1");
			setTimeout(function(){
				sound_nav("sound_2");
			},1000);
		}
		else{
			$(".p-textarea").html(eval("data.string.answe"+qnArray[qno][2])).css("z-index","1");
			setTimeout(function(){
				sound_nav("sound_3");
			},1000);
		}
			$(".hint,.sp-a,.fairy-1").hide(0);
			$(".sp-b,.fairy-2").show(0);
			// $(".sp-c").show(0);
			$(".quen").addClass("quen1");

		} else{
			play_correct_incorrect_sound(0);
			$('.input-class').addClass('incorrects');
			$('.input-class').siblings('.wrngopt').show(0);
				$(".hint").addClass("hint1");
		}
	});


}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			if (charCode === 13 && button_class != null) {
				$(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
			return true;
		});
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	//
	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

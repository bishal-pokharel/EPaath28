var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
		//slide0
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'upper-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text2,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text4,
					textclass : 'sec-text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
						firstrowdata	:[{
								textclass:"lhs",
								textdata:data.string.lhs
						},{
							textclass:"blnk",
							textdata:data.string.blank
						},
						{
								textclass:"rhs",
								textdata:data.string.rhs
						}],
						secondrowdata :[{
							textclass:"lhs-row",
							textdata:data.string.p6text3
					},{
						textclass:"eq-row",
						textdata:data.string.equal
					},
					{
							textclass:"rhs-row",
							textdata:data.string.n5
						}]
				}]
	},{
    //slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"animation",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-b',
					textdata : data.string.p6text5,
					textclass : 'sec-text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
						firstrowdata	:[{
								textclass:"lhs",
								textdata:data.string.lhs
						},{
							textclass:"lhs",
							textdata:data.string.blank
						},
						{
								textclass:"lhs",
								textdata:data.string.rhs
						}],
						secondrowdata :[{
							textclass:"lhs-row",
							textdata:data.string.p6text3
					},{
						textclass:"eq-row",
						textdata:data.string.equal
					},
					{
							textclass:"rhs-row",
							textdata:data.string.n5
						}]
				}]
  },{
		//slide 2
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text6,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text9,
					textclass : 'sec-text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
						firstrowdata	:[{
								textclass:"lhs",
								textdata:data.string.lhs
						},{
							textclass:"lhs",
							textdata:data.string.blank
						},
						{
								textclass:"lhs",
								textdata:data.string.rhs
						}],
						secondrowdata :[{
							textclass:"lhs-row",
							textdata:data.string.p6text3
					},{
						textclass:"eq-row",
						textdata:data.string.equal
					},
					{
							textclass:"rhs-row",
							textdata:data.string.n5
						}],
						thirdrowdata : [{
							textclass:"lhs-row-firs",
							textdata:data.string.minus3
					},{
						textclass:"blnk",
						textdata:data.string.blank
					},
					{
							textclass:"rhs-row-firs",
							textdata:data.string.minus3
						}]
				}],
				lowertextblockadditionalclass: "moreinfo",
				lowertextblock:[{
						textclass: "heading",
						textdata: data.string.p6text7
				},{
					textclass: "heading",
					textdata: data.string.p6text8
				},{
						textclass: "close_button",
						textdata: "&nbsp;"
					}],
				learn_more: true
	},{
    //slide 3
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text10,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
						firstrowdata	:[{
								textclass:"lhs",
								textdata:data.string.lhs
						},{
							textclass:"blnk",
							textdata:data.string.blank
						},
						{
								textclass:"rhs",
								textdata:data.string.rhs
						}],
						secondrowdata :[{
							textclass:"lhs-row",
							textdata:data.string.p6text3
					},{
						textclass:"eq-row",
						textdata:data.string.equal
					},
					{
							textclass:"rhs-row",
							textdata:data.string.n5
						}],
						thirdrowdata : [{
							textclass:"lhs-row-firs",
							textdata:data.string.minus3
					},{
						textclass:"blnk",
						textdata:data.string.blank
					},
					{
							textclass:"rhs-row-firs",
							textdata:data.string.minus3
						}],
						fourrowdata :[{
							datahighlightflag : true,
							datahighlightcustomclass : 'highlight-color',
							textclass:"lhs-row-sec",
							textdata:data.string.p6text12
					},{
						textclass:"eq-row-1",
						textdata:data.string.equal
					},
					{
							datahighlightflag : true,
							datahighlightcustomclass : 'highlight-color',
							textclass:"rhs-row-sec",
							textdata:data.string.p6text13
						}],
						fiverowdata :[{
							datahighlightflag : true,
							datahighlightcustomclass : 'highlight-color',
							textclass:"lhs-row-thd",
							textdata:data.string.p6text14
					},{
						textclass:"eq-row-2",
						textdata:data.string.equal
					},
					{
							textclass:"rhs-row-thd",
							textdata:data.string.n2
						}]
				}]
  },{
		//slide 4
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text15,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text17,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
					firstrowdata : [{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"blnk",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
					}],
						secondrowdata	:[{
								datahighlightflag : true,
								datahighlightcustomclass : 'color',
								textclass:"eqn-1",
								textdata:data.string.p6text16
						},{
							textclass:"eqn-3",
							textdata:data.string.equal
						},
						{
								textclass:"eqn-2",
								textdata:data.string.n2
						}]
					}]
	},{
		//slide 5
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text18,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text19,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
					firstrowdata : [{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"blnk",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
					}],
						secondrowdata	:[{
								datahighlightflag : true,
								datahighlightcustomclass : 'color',
								textclass:"eqn-1",
								textdata:data.string.p6text16
						},{
							textclass:"eqn-3",
							textdata:data.string.blank
						},
						{
								textclass:"eqn-2",
								textdata:data.string.n2
						}]
					}]
	},{
		//slide 6
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text20,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text21,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
					firstrowdata : [{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"blnk",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
					}],
						secondrowdata	:[{
								datahighlightflag : true,
								datahighlightcustomclass : 'col-text',
								textclass:"eqn-1 frac-text frac-1",
								textdata:data.string.f_1_2,
								splitintofractionsflag: true,
						},{
							textclass:"eqn-3",
							textdata:data.string.equal
						},
						{
								textclass:"eqn-2 frac-text frac-1",
								textdata:data.string.f_1_2,
									splitintofractionsflag: true,
						}]
					}],
					lowertextblockadditionalclass:"note-box",
					lowertextblock:[{
						datahighlightflag : true,
						datahighlightcustomclass : 'underline',
						textclass:"note-text",
						textdata:data.string.p6text28
					},{
						textclass:"note-text ",
						textdata:data.string.p6text29
					}]
	},{
		//slide 7
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairy-anim',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					imagelabelclass:"anim",
				}]
			}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text22,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text23,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],
				table:[{
					firstrowdata : [{
						textclass:"lhs",
						textdata:data.string.lhs
				},{
					textclass:"blnk",
					textdata:data.string.blank
				},
				{
						textclass:"rhs",
						textdata:data.string.rhs
					}],
						secondrowdata	:[{
								datahighlightflag : true,
								datahighlightcustomclass : 'col-text',
								textclass:"eqn-1 frac-text frac-1",
								textdata:data.string.f_1_2,
								splitintofractionsflag: true,
						},{
							textclass:"eqn-1",
							textdata:data.string.equal
						},
						{
								textclass:"eqn-1 frac-text frac-1",
								textdata:data.string.f_1_2,
									splitintofractionsflag: true,
						}],
						thirdrowdata : [{
							datahighlightflag : true,
							datahighlightcustomclass : 'coltxt',
							textclass:"div-eqn",
							textdata:data.string.p6text30
					},{
						textclass:"div-eqn",
						textdata:data.string.equal
					},
					{
						datahighlightflag : true,
						datahighlightcustomclass : 'coltxt',
							textclass:"div-eqn",
							textdata:data.string.p6text31
						}],
							fourrowdata	:[{

									textclass:"div-eqn-1",
									textdata:data.string.y

							},{
								textclass:"div-eqn-1",
								textdata:data.string.equal
							},
							{
									textclass:"div-eqn-1",
									textdata:data.string.n1,

							}],
					}],
	},{
		//slide 8
		contentnocenteradjust: true,
		contentblockadditionalclass: 'diy-bg',
		uppertextblockadditionalclass: 'que-title',
		uppertextblock:[{
			textdata: data.string.p6text1,
			textclass: "eqn-title",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fairy01',
				imgclass:'fairy-1',
			},{
				imgid:'fairy01',
				imgclass:'fairyanim',
			},{
				imgid:'fairy01',
				imgclass:'anim-fairy',
			},{
				imgid:'fairy01',
				imgclass:'fairy-2',
			}]
		}],
		imagedivblock:[{
				imagedivclass:"mainImgContain",
				imagestoshow:[{
					imgclass: 'chalkboard-1',
					imgid : 'chalkboard',
					imgsrc: '',
				}],
				imagelabels:true,
				imagelabels:[{
					// imagelabelclass:"anim",
				},{
				imagelabelclass:" firsrow",
				imagelabeldata:data.string.lhs
			},{
				imagelabelclass:"firsrow-1",
				imagelabeldata:data.string.rhs
			},
			{
				imagelabelclass:"hide1 secrow",
				imagelabeldata:data.string.p6text37
			},{
				imagelabelclass:"hide1 thdrow",
				imagelabeldata:data.string.p6text32
			},{
				datahighlightflag:"true",
				datahighlightcustomclass:"color-text",
				imagelabelclass:"hide1 fourow",
				imagelabeldata:data.string.p6text33
			},{
				imagelabelclass:"hide1 fifrow",
				imagelabeldata:data.string.p6text34
			},{
				imagelabelclass:"hide1 sixrow",
				imagelabeldata:data.string.p6text35
			}]
		}],
				speechbox:[{
					speechbox: 'sp-a',
					textdata : data.string.p6text24,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-c',
					textdata : data.string.p6text25,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				},{
					speechbox: 'sp-b',
					textdata : data.string.p6text26,
					textclass : 'text-box',
					imgclass: 'box',
					imgid : 'spbox04',
					imgsrc: '',
				}],

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "fairy", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fairy01", src: imgpath+"flying-chibi-fairy01.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "chalkboard", src: imgpath+"black_board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "spbox02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spbox04", src: imgpath+"text_box04.png", type: createjs.AbstractLoader.IMAGE},



			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_0a", src: soundAsset+"s6_p1_1.ogg"},
			{id: "sound_0b", src: soundAsset+"s6_p1_2.ogg"},
			{id: "sound_0c", src: soundAsset+"s6_p1_3.ogg"},
			{id: "sound_0d", src: soundAsset+"s6_p1_4.ogg"},
			{id: "sound_1", src: soundAsset+"s6_p2.ogg"},
			{id: "sound_2a", src: soundAsset+"s6_p3_1.ogg"},
			{id: "sound_2b", src: soundAsset+"s6_p3_2.ogg"},
			{id: "sound_2c", src: soundAsset+"s6_p3_3.ogg"},
			{id: "sound_3", src: soundAsset+"s6_p4.ogg"},
			{id: "sound_4a", src: soundAsset+"s6_p5_1.ogg"},
			{id: "sound_4b", src: soundAsset+"s6_p5_2.ogg"},
			{id: "sound_5a", src: soundAsset+"s6_p6_1.ogg"},
			{id: "sound_5b", src: soundAsset+"s6_p6_2.ogg"},
			{id: "sound_6a", src: soundAsset+"s6_p7_1.ogg"},
			{id: "sound_6b", src: soundAsset+"s6_p7_2.ogg"},
			{id: "sound_6c", src: soundAsset+"s6_p7_3.ogg"},
			{id: "sound_7a", src: soundAsset+"s6_p8_1.ogg"},
			{id: "sound_7b", src: soundAsset+"s6_p8_2.ogg"},
			{id: "sound_8a", src: soundAsset+"s6_p9_1.ogg"},
			{id: "sound_8b", src: soundAsset+"s6_p9_2.ogg"},
			{id: "sound_8c", src: soundAsset+"s6_p9_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/
	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside) {
		typeof $splitinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if ($splitintofractions.length > 0) {
			$.each($splitintofractions, function(index, value) {
				$this = $(this);
				var tobesplitfraction = $this.html();
				if ($this.hasClass('fraction')) {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				} else {
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}

				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/


	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		$prevBtn.hide(0);
		$nextBtn.hide(0);
  	put_speechbox_image(content, countNext);
		$(".close_button").click(function(){
			createjs.Sound.stop();
			$(".moreinfo").hide(500);
			nav_button_controls(500);
		});


		switch(countNext) {
			case 0:
			setTimeout(function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_0a");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".fairy-1, .sp-a").show(0);
				current_sound = createjs.Sound.play("sound_0b");
				current_sound.play();
				current_sound.on("complete", function(){
				createjs.Sound.stop();
				$(".lhs-row,.eq-row,.rhs-row").show(0);
				current_sound = createjs.Sound.play("sound_0c");
				current_sound.play();
				current_sound.on("complete", function(){
				$(".fairy-anim").show(0);
				$(".sp-b").delay(3000).show(0);
				$(".sp-a,.fairy-1").hide(0);
				setTimeout(function () {
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_0d");
				current_sound.play();
				current_sound.on("complete", function(){
					nav_button_controls(0);
				});
			},3000);
				});
			});
		});
			},1000);
			break;
			case 1:
			$(".lhs-row,.eq-row,.rhs-row").show(0);
			$(".animation").delay(1000).show(0);
			$(".lhs").delay(3000).show(0);
			$(".rhs").delay(4000).show(0);
			setTimeout(function(){
				$(".fairy").show(0);
				$(".sp-b").show(0);
				sound_nav("sound_1");
			},5000);
			break;
			case 2:
			$learn_more = $(".learn_more");
			$learn_more.click(function(){
				$(".moreinfo").show(500);
				sound_player("sound_2c");
			});
			$(".lhs-row,.eq-row,.rhs-row,.lhs,.rhs").show(0);
			$(".fairy-1, .sp-a").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".lhs-row-firs").delay(100).show(0);
			$(".rhs-row-firs").delay(900).show(0);
			$(".fairy-anim").show(0);
			$(".sp-a,.fairy-1").hide(0);
			$(".learn_more").delay(5000).css("pointer-events","auto").show(0);
			setTimeout(function () {
			$(".sp-b").show(0);
			sound_player("sound_2b");
		},3000);
	});
			break;
			case 3:
			$(".lhs-row,.eq-row,.rhs-row,.lhs,.rhs,.lhs-row-firs,.rhs-row-firs").show(0);
			$(".fairy-1, .sp-a").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".lhs-row-sec").show(0);
			$(".eq-row-1").show(0);
			$(".rhs-row-sec").show(0);
			$(".lhs-row-thd").delay(1000).show(0);
			$(".eq-row-2").delay(1000).show(0);
			$(".rhs-row-thd").delay(1000).show(0);
			nav_button_controls(1500);
		});
			break;
			case 4:
			$(".lhs,.rhs").show(0);
			$(".fairy-1, .sp-a").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_4a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".eqn-1").delay(200).show(0);
			$(".eqn-3").delay(200).show(0);
			$(".eqn-2").delay(200).show(0);
			$(".fairy-anim").show(0);
			$(".sp-a,.fairy-1").hide(0);
			setTimeout(function () {
				$(".sp-b").show(0);
				sound_nav("sound_4b");
		},3000);
	});
			break;
			case 5:
			$(".lhs,.rhs,.eqn-1,.eqn-2,.eqn-3").show(0);
			$(".fairy-1, .sp-a").show(0);
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_5a");
			current_sound.play();
			current_sound.on("complete", function(){
			$(".color").addClass("col-text");
			$(".fairy-anim").delay(200).show(0);
			$(".sp-a,.fairy-1").delay(200).hide(0);
			setTimeout(function () {
			$(".sp-b").show(0);
			sound_nav("sound_5b");
		},3000);
	});
			break;
			case 6:
					$(".lhs,.rhs,.eqn-1,.eqn-2,.eqn-3").show(0);
					$(".fairy-1, .sp-a").show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_6a");
					current_sound.play();
					current_sound.on("complete", function(){
					$(".note-box").show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_6c");
					current_sound.play();
					current_sound.on("complete", function(){
					$(".fairy-anim").show(0);
					$(".sp-a,.fairy-1").hide(0);
					setTimeout(function () {
					sound_nav("sound_6b");
					$(".sp-b").show(0);
				},3000);
			});
		});
			break;
			case 7:
				$(".lhs,.rhs,.eqn-1").show(0);
				$(".fairy-1, .sp-a").show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_7a");
				current_sound.play();
				current_sound.on("complete", function(){
				$(".div-eqn").delay(200).show(0);
				$(".div-eqn-1").delay(1200).show(0);
				$(".fairy-anim").delay(1500).show(0);
				$(".sp-a,.fairy-1").delay(1500).hide(0);
				setTimeout(function () {
				$(".sp-b").show(0);
				sound_nav("sound_7b");
			},4500);
		});
				break;
				case 8:
				$(".fairy-1, .sp-a").show(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_8a");
				current_sound.play();
				current_sound.on("complete", function(){
				$(".firsrow,.firsrow-1").show(0);
				$(".secrow").delay(1000).show(0);
				$(".thdrow").delay(2000).show(0);
				$(".fairyanim").delay(2000).show(0);
				$(".sp-a,.fairy-1").delay(2000).hide(0);
				$(".sp-c").delay(4000).show(0);
				setTimeout(function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_8b");
					current_sound.play();
					current_sound.on("complete", function(){
					$(".fourow").show(0);
					$(".fifrow").delay(1000).show(0);
					$(".sixrow").delay(2000).show(0);
					$(".anim-fairy").delay(2000).show(0);
					$(".sp-c,.fairyanim").delay(2000).hide(0);
					setTimeout(function () {
					$(".sp-b").show(0);
					sound_nav("sound_8c");
				},4000);
		});
},4000);
});

				break;

			default:
				nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('imagedivblock')){
			var imageblock = content[count].imagedivblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		for (var i = 0; i < content.length; i++) {
			slides(i);
			$($('.totalsequence')[i]).html(i);
			$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
				"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"
			});
		}
		function slides(i){
				$($('.totalsequence')[i]).click(function(){
					countNext = i;
					createjs.Sound.stop();
					templateCaller();
				});
			}
	*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

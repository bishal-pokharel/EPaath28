var imgpath = $ref+"/exercise/images/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'options class3',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext5,
			textclass : 'hint',
		}
		]
	},

	//slide 1
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext7,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'options class3',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext5,
			textclass : 'hint',
		}
		]
	},

	//slide 2
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext8,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext2,
			textclass : 'options class3 corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext5,
			textclass : 'hint',
		}
		]
	},

	//slide 3
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext9,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'options class3 corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext12,
			textclass : 'hint',
		}
		]
	},

	//slide 4
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext10,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'options class3 ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext6,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext12,
			textclass : 'hint',
		}
		]
	},

	//slide 5
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext11,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'options class3 ',
		},
		{
			textdata : data.string.ext6,
			textclass : 'class2 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext12,
			textclass : 'hint',
		}
		]
	},

	//slide 6
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext13,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'options class3 corans',
		},
		{
			textdata : data.string.ext6,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext15,
			textclass : 'hint',
		}
		]
	},

	//slide 7
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext14,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'options class3 ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class2 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext15,
			textclass : 'hint',
		}
		]
	},

	//slide 8
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext16,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'options class3 ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext18,
			textclass : 'hint',
		}
		]
	},

	//slide 9
	{
		contentblockadditionalclass:'ole-background-gradient-midnight',
		uppertextblock: [{
			textdata : data.string.ext17,
			textclass : 'instruction ',
		},],
		extratextblock : [
		{
			textdata : data.string.ext3,
			textclass : 'options class3 corans',
		},
		{
			textdata : data.string.ext6,
			textclass : 'class2 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext18,
			textclass : 'hint',
		}
		]
	},


];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();
	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		rhino.numberOfQuestions();
		$nextBtn.hide(0);
		$prevBtn.hide(0);


		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$('.hint').hide(0);
		$('.options').click(function(){
			if($(this).hasClass('corans'))
			{
				if(!wrong_clicked){
					rhino.update(true);
				}
				$('.text-5').animate({"opacity":"1"},400);
				$('.hint').fadeIn(200);
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(191%,-220%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background":"rgb(152,192,46)","border-radius":"1vmin","border-color":"rgb(197, 224, 123)"});
				$('.options').css({"pointer-events":"none"});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page){
							$nextBtn.show(0);
					}
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(191%,-220%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
				$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":"1vmin","border-color":"rgb(226, 129, 129)"});
				play_correct_incorrect_sound(0);
				if(!wrong_clicked){
					rhino.update(false);
				}
				wrong_clicked = true;
			}
		});
		// $(".options").click(function(){
		// 	if($(this).hasClass("corans")){
		// 		if(!wrong_clicked){
		// 			rhino.update(true);
		// 		}
		// 		$(".options").css('pointer-events', 'none');
		// 		$('.blank-space').html($(this).html().substring(4));
		// 		play_correct_incorrect_sound(1);
		// 		$(this).css({
		// 			'color': 'rgb(56,142,55)',
		// 		});
		// 		if(countNext != $total_page)
		// 			$nextBtn.show(0);
		// 	}
		// 	else{
		// 		if(!wrong_clicked){
		// 			rhino.update(false);
		// 		}
		// 		$(this).css({
		// 			'color': 'rgb(182,77,4)',
		// 			'text-decoration': 'line-through',
		// 			'pointer-events': 'none',
		// 		});
		// 		wrong_clicked = true;
		// 		play_correct_incorrect_sound(0);
		// 	}
		// });
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

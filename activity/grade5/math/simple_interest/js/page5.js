var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,

		extratextblock:[{
			textdata:  data.string.diytext,
			textclass: "top-text mid",
		}]
	},
	// slide1
	{
		extratextblock:[{
			textdata:  data.string.p5text1,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "dolma",
					imgsrc : '',
					imgid : 'ram'
				},
				{
					imgclass : "goat",
					imgsrc : '',
					imgid : 'goat'
				},
			]
		}],
	},

	// slide2
	{
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bank'
				},
				{
					imgclass : "dolma",
					imgsrc : '',
					imgid : 'ram'
				},
			]
		}],
	},

	// slide3
	{
		extratextblock:[{
			textdata:  data.string.p5text3,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bank1'
				},
			]
		}],
	},

	// slide3
	{
		extratextblock:[{
			textdata:  data.string.p5text4,
			textclass: "top-text ",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-2'
				},
			]
		}],
	},

	// slide2
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p5text4,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},
		{
			textdata:  data.string.p5text5,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p5text6,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.p5text7,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p5text8,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p5text12,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p5text10,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.p5text11,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p5text9,
			textclass: "bottom-text1 ",
		}
	],
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bank", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ram", src: imgpath+"ram01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "goat", src: imgpath+"goat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bank1", src: imgpath+"bankcounter01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"bg10.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p5_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		$('.optionspos-1, .optionspos-2, .optionspos-3, .optionspos-4,.optionspos7-1,.optionspos7-2,.optionspos7-3,.optionspos7-4,.optionspos7-5,.optionspos7-6,.optionspos7-7 ').click(function(){
			if($(this).hasClass('correct'))
			{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-18%,35%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$('.optionspos-1, .optionspos-2, .optionspos-3, .optionspos-4,.optionspos7-1,.optionspos7-2,.optionspos7-3,.optionspos7-4,.optionspos7-5,.optionspos7-6,.optionspos7-7 ').css({"pointer-events":"none"});
				$(this).css({"background":"rgb(190,214,47)","color":"white"});
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else {
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-18%,35%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
				$(this).css({"background":"rgb(168, 33, 36)","color":"white","pointer-events":"none"});
			}
		});
		switch(countNext) {
			case 0:
				$nextBtn.show(0);
				sound_player('sound_1');
				break;
			case 1:
			nav_button_controls(200);
				break;
			case 2:
			nav_button_controls(200);
				break;
			case 3:
			nav_button_controls(200);
				break;
		case 4:
		nav_button_controls(200);
			break;
			case 5:
			$('.bottom-text1').hide(0);
			$('.text-5').css({"opacity":"0"});
			$('.optionclass1').click(function(){
				if($(this).hasClass('correct'))
				{
					$('.text-5').animate({"opacity":"1"},400);
					$('.bottom-text1').fadeIn(200).delay(100).animate({"border-top-right-radius": "3vmin",
																														"border-bottom-left-radius": "3vmin"});
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(152%,-200%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
					$('.optionclass1').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					nav_button_controls(200);
				}
				else{
					$('.text-5').animate({"opacity":"1"},400);
					$('.text-5').fadeIn(200);
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(152%,-200%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
					$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
					play_correct_incorrect_sound(0);
				}
			});
				break;
			case 6:
				break;
			case 7:
				$('.long').css({"left":" 32%"});
				break;
			default:
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	function input_box(input_class, max_number, max) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
		});
		$(input_class).blur(function(event) {
			var curr_val = event.target.value;
			if(String(curr_val).length==1){
				$(input_class).val('0'+curr_val);
			}
			if (curr_val > max) {
				$(input_class).val(max);
			}
			if (curr_val == '') {
				$(input_class).val('00');
			} else{
				$(input_class).data('enter','1');
			}
			for(var i =1; i<6; i++){
				if(parseInt($('.hr'+i).data('enter')) == 0 || parseInt($('.min'+i).data('enter')) == 0){
					return false;
				}
			}
			ole.footerNotificationHandler.pageEndSetNotification();
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

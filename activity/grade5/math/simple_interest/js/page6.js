var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',

		extratextblock:[{
			textdata:  data.string.diytext,
			textclass: "top-text mid",
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p6text1,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},
		{
			textdata:  data.string.p6text2,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p6text3,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.p6text4,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p6text5,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p6text7,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p6text6,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.p6text8,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p6text9,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide2
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p6text10,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},
		{
			textdata:  data.string.p6text12,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p6text13,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.p6text14,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p6text15,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p6text16,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p6text17,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.p6text18,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p6text19,
			textclass: "bottom-text1 ",
		}
	],
	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "girl1", src: imgpath+"asha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl2", src: imgpath+"niti01.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			// {id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			// {id: "sound_2", src: soundAsset+"p4_s1_1.ogg"},
			// {id: "sound_3", src: soundAsset+"p4_s1.ogg"},
			// {id: "sound_4", src: soundAsset+"p4_s2.ogg"},
			// {id: "sound_5", src: soundAsset+"p4_s3.ogg"},
			// {id: "sound_6", src: soundAsset+"p4_s4.ogg"},
			// {id: "sound_7", src: soundAsset+"p4_s5.ogg"},
			// {id: "sound_8", src: soundAsset+"p4_s6.ogg"},
			// {id: "sound_9", src: soundAsset+"p4_s7_1.ogg"},
			// {id: "sound_10", src: soundAsset+"p4_s7.ogg"},
			// {id: "sound_11", src: soundAsset+"p4_s8.ogg"},
			// {id: "sound_12", src: soundAsset+"p4_s9.ogg"},
			// {id: "sound_13", src: soundAsset+"p4_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch (countNext) {
			case 0:
				// sound_play_click('sound_1');
				nav_button_controls(1000);
				break;
			case 1:
			case 2:
			var random = Math.floor(Math.random() * (12 - 8 + 1)) + 8;
			$('.interestnumber').html(random*1000);
			$('.interestnumber1').html((random*1000*1.1*7).toPrecision(5));
			$('.interestnumber2').html(random*1000*0.11*7);
			$('.interestnumber3').html(random*1000*0.1*8);
			$('.interestnumber4').html(random*1000*0.11);
			$('.bottom-text1').hide(0);
			$('.text-5').css({"opacity":"0"});
			$('.optionclass1').click(function(){
				if($(this).hasClass('correct'))
				{
					$('.text-5').animate({"opacity":"1"},400);
					$('.bottom-text1').fadeIn(200).delay(100).animate({"border-top-right-radius": "3vmin",
																														"border-bottom-left-radius": "3vmin"});
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'correct.png" />').insertAfter(this);
					$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
					$('.optionclass1').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					nav_button_controls(200);
				}
				else{
					$('.text-5').animate({"opacity":"1"},400);
					$('.text-5').fadeIn(200);
					var $this = $(this);
					var position = $this.position();
					var width = $this.width();
					var height = $this.height();
					var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
					var centerY = ((position.top + height)*100)/$board.height()+'%';
					$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
					$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
					play_correct_incorrect_sound(0);
				}
			});
			break;
			default:
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

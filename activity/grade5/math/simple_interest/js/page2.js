var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";


var content=[
	// slide0
	{
		extratextblock:[{
			textdata:  data.string.p2text1,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "book",
					imgsrc : '',
					imgid : 'bookcloud'
				},
			]
		}],
	},

	// slide1
	{
		extratextblock:[{
			textdata:  data.string.p2text2,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti-1",
					imgsrc : '',
					imgid : 'niti1'
				},
				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
				{
					imgclass : "rs500",
					imgsrc : '',
					imgid : 'rs100'
				},
			]
		}],
	},

	// slide2
	{
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text4,
			textclass: "principal",
		},
		{
			textdata:  data.string.p2text5,
			textclass: "interest",
		},
		{
			textdata:  data.string.p2text6,
			textclass: "rate",
		},
		{
			textdata:  data.string.p2text7,
			textclass: "borrowed",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "rs500-1",
					imgsrc : '',
					imgid : 'rs100'
				},
			]
		}],
	},

	// slide3
	{
		extratextblock:[{
			textdata:  data.string.p2text8,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text4,
			textclass: "principal",
		},
		{
			textdata:  data.string.p2text7,
			textclass: "borrowed",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "rs500-1",
					imgsrc : '',
					imgid : 'rs100'
				},
			]
		}],
	},

	// slide4
	{
		extratextblock:[{
			textdata:  data.string.p2text11,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text12,
			textclass: "text-2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti-1",
					imgsrc : '',
					imgid : 'niti1'
				},
				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
				{
					imgclass : "emptycloud",
					imgsrc : '',
					imgid : 'emptycloud'
				},
			]
		}],
	},

	// slide5
	{
		extratextblock:[{
			textdata:  data.string.p2text13,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text4,
			textclass: "principal",
		},
		{
			textdata:  data.string.p2text5,
			textclass: "interest",
		},
		{
			textdata:  data.string.p2text6,
			textclass: "rate",
		},
		{
			textdata:  data.string.p2text7,
			textclass: "borrowed",
		},
		{
			textdata:  data.string.p2text18,
			textclass: "reutrned borrowed",
		},
		{
			textdata:  data.string.p2text17,
			textclass: "addtext ",
			datahighlightflag:true,
			datahighlightcustomclass:'bigfont'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "rs500-1",
					imgsrc : '',
					imgid : 'rs100'
				},
				{
					imgclass : "rs500-2",
					imgsrc : '',
					imgid : 'rs100'
				},
			]
		}],
	},

	// slide6
	{
		extratextblock:[{
			textdata:  data.string.p2text20,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti1",
					imgsrc : '',
					imgid : 'niti02'
				},
			]
		}],
	},

	// slide7
	{
		extratextblock:[{
			textdata:  data.string.p2text21,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text12,
			textclass: "ten",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti1",
					imgsrc : '',
					imgid : 'niti02'
				},
				{
					imgclass : "cloud1",
					imgsrc : '',
					imgid : 'emptycloud'
				},
			]
		}],
	},

	// slide8
	{
		contentblockadditionalclass:'yellow',
		extratextblock:[{
			textdata:  data.string.p2text22,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text23,
			textclass: "board-text",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'bg-blackboard'
				},
			]
		}],
	},

	// slide9
	{
		contentblockadditionalclass:'yellow',
		extratextblock:[{
			textdata:  data.string.p2text25a,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text23,
			textclass: "board-text",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		},
		{
			textdata:  data.string.p2text24,
			textclass: "board-text midtext",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'bg-blackboard'
				},
			]
		}],
	},

	// slide10
	{
		contentblockadditionalclass:'yellow',
		extratextblock:[{
			textdata:  data.string.p2text26,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text23,
			textclass: "board-text",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		},
		{
			textdata:  data.string.p2text24,
			textclass: "board-text midtext",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		},
		{
			textdata:  data.string.p2text25,
			textclass: "board-text righttext",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'bg-blackboard'
				},
			]
		}],
	},

	// slide11
	{
		contentblockadditionalclass:'yellow',
		extratextblock:[{
			textdata:  data.string.p2text28a,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text23b,
			textclass: "board-text",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		},
		{
			textdata:  data.string.p2text24b,
			textclass: "board-text midtext",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		},
		{
			textdata:  data.string.p2text25b,
			textclass: "board-text righttext",
			datahighlightflag: true,
			datahighlightcustomclass: 'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'bg-blackboard'
				},
			]
		}],
	},

	// slide12
	{
		extratextblock:[{
			textdata:  data.string.p2text29,
			textclass: "text-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti-2",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
				{
					imgclass : "rs500-3",
					imgsrc : '',
					imgid : 'rs100'
				},
				{
					imgclass : "rs10",
					imgsrc : '',
					imgid : 'rs10'
				},
			]
		}],
	},

	// slide13
	{
		extratextblock:[{
			textdata:  data.string.p2text30,
			textclass: "text-1",
		},
		{
			textdata:  data.string.p2text4,
			textclass: "principal",
		},
		{
			textdata:  data.string.p2text5,
			textclass: "interest",
		},
		{
			textdata:  data.string.p2text6,
			textclass: "rate",
		},
		{
			textdata:  data.string.p2text7,
			textclass: "borrowed",
		},
		{
			textdata:  data.string.p2text18,
			textclass: "reutrned borrowed",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "rs500-1",
					imgsrc : '',
					imgid : 'rs100'
				},
				{
					imgclass : "rs500-2",
					imgsrc : '',
					imgid : 'rs100'
				},
				{
					imgclass : "rs10-a",
					imgsrc : '',
					imgid : 'rs10'
				},
			]
		}],
	},


		// slide14
		{
			extratextblock:[{
				textdata:  data.string.p2text30,
				textclass: "text-1",
			},
			{
				textdata:  data.string.p2text5,
				textclass: "interest",
			},
			{
				textdata:  data.string.p2text7,
				textclass: "borrowed",
			},
			{
				textdata:  data.string.p2text18,
				textclass: "reutrned borrowed",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg_full",
						imgsrc : '',
						imgid : 'bg-1'
					},
					{
						imgclass : "niti",
						imgsrc : '',
						imgid : 'niti'
					},
					{
						imgclass : "rs500-1",
						imgsrc : '',
						imgid : 'rs100'
					},
					{
						imgclass : "rs500-2",
						imgsrc : '',
						imgid : 'rs100'
					},
					{
						imgclass : "rs10-a",
						imgsrc : '',
						imgid : 'rs10'
					},
				]
			}],
		},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-blackboard", src: imgpath+"blackboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti", src: imgpath+"niti.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti1", src: imgpath+"niti03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha", src: imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bookcloud", src: imgpath+"bookcloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rs500", src: imgpath+"rs500.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rs50", src: imgpath+"rs50.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rs100", src: imgpath+"rs100.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rs10", src: imgpath+"rs10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "emptycloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti02", src: imgpath+"niti02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_poem_image(content, countNext);

		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				nav_button_controls(200);
				break;
			case 1:
				nav_button_controls(200);
					break;
				case 2:
				$('.text-1').css('bottom','77%');
					$('.principal,.interest,.rate').click(function(){
						if($(this).hasClass('principal'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.principal,.interest,.rate').css({"pointer-events":"none"});
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
						}
					});
					$nextBtn.hide(0);
					break;
				case 3:
					$('.principal').css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)","cursor":"auto"});
					$nextBtn.hide(0);
					nav_button_controls(200);
					break;
				case 4:
				nav_button_controls(200);
						break;
				case 5:
					$nextBtn.hide(0);
					$('.text-1').css('bottom','77%');
					$('.principal,.interest,.rate').click(function(){
						if($(this).hasClass('rate'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.principal,.interest,.rate').css({"pointer-events":"none"});
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
							play_correct_incorrect_sound(0);
						}
					});
					break;
					case 13:
						$nextBtn.hide(0);
						$('.text-1').css('bottom','77%');
						$('.principal,.interest,.rate').click(function(){
							if($(this).hasClass('interest'))
							{
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'correct.png" />').insertAfter(this);
								$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
								$('.principal,.interest,.rate').css({"pointer-events":"none"});
								play_correct_incorrect_sound(1);
								nav_button_controls(200);
							}
							else{
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-13%,-209%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
								$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								play_correct_incorrect_sound(0);
							}
						});
						break;
						case 14:
							$('.interest').css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)","cursor":"auto"});
							$nextBtn.hide(0);
							nav_button_controls(200);
							break;
					default:
					nav_button_controls(200);
					break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

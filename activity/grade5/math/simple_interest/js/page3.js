var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		extratextblock:[{
			textdata:  data.string.p3text1,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
			]
		}],
	},


	// slide1
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "top-text ",
		},
		{
			textdata:  data.string.p3text3,
			textclass: "text-1 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},
		{
			textdata:  data.string.p3text4,
			textclass: "text-2 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},
		{
			textdata:  data.string.p3text5,
			textclass: "text-3 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'blackboard'
				},
			]
		}],
	},

	// slide2
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p3text6,
			textclass: "top-text1",
		},
		{
			textdata:  data.string.p3text11,
			textclass: "mainquestion",
		},
		{
			textdata:  data.string.p3text12,
			textclass: "optionclass correct ",
		},
		{
			textdata:  data.string.p3text13,
			textclass: "optionclass ",
		},
		{
			textdata:  data.string.p3text14,
			textclass: "optionclass ",
		},
		{
			textdata:  data.string.p3text15,
			textclass: "optionclass ",
		},
		{
			textdata:  data.string.p3text16,
			textclass: "bottom-text ",
		}
	],
	},

	// slide3
	{
		extratextblock:[{
			textdata:  data.string.p3text17,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bg1'
				},
				{
					imgclass : "niti",
					imgsrc : '',
					imgid : 'niti'
				},
				{
					imgclass : "asha",
					imgsrc : '',
					imgid : 'asha'
				},
			]
		}],
	},

	// slide4
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p3text18,
			textclass: "top-text ",
		},
		{
			textdata:  data.string.p3text7,
			textclass: "text-1 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},
		{
			textdata:  data.string.p3text8,
			textclass: "text-2 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},
		{
			textdata:  data.string.p3text9,
			textclass: "text-3 ",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'blackboard'
				},
			]
		}],
	},

	// slide5
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p3text32,
			textclass: "top-text2 ",
		}]
	},

	// slide6
	{
		extratextblock:[{
			textdata:  data.string.p3text33,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bank'
				},
				{
					imgclass : "dolma",
					imgsrc : '',
					imgid : 'dolma'
				},
			]
		}],
	},

	// slide7
	{
		extratextblock:[{
			textdata:  data.string.p3text34,
			textclass: "top-text ",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'bank1'
				},
			]
		}],
	},

	// slide8
	{
		contentblockadditionalclass:'bg_yellow',
		extratextblock:[{
			textdata:  data.string.p3text35,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},
		{
			textdata:  data.string.p3text36,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text37,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.p3text38,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.p3text30,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p3text29,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.p3text31,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.p3text39,
			textclass: "bottom-text1 ",
		}
	],
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg1", src: imgpath+"bg09.png", type: createjs.AbstractLoader.IMAGE},
			{id: "niti", src: imgpath+"niti03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blackboard", src: imgpath+"blackboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bank", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dolma", src: imgpath+"dolma.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bank1", src: imgpath+"airport.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);

		$('.oneone').draggable({
								containment : ".generalTemplateblock",
								revert : true,
								cursor : "move",
								zIndex: 100000,
						});
			switch (countNext) {
				case 0:
						nav_button_controls(1000);
						break;
				case 1:
							nav_button_controls(200);
							break;
				case 2:
							var random = Math.floor(Math.random() * (49 - 26 + 1)) + 26;
							$('.interestnumber').html(random);
							$('.interestnumber1').html(random*10);
							$('.interestnumber2').html(random+5);
							$('.bottom-text').hide(0);
							$('.optionclass').click(function(){
								if($(this).hasClass('correct'))
								{
									$('.optionclass').css('pointer-events','none');
									$('.bottom-text').fadeIn(200).delay(100).animate({"border-top-right-radius": "3vmin",
							    																									"border-bottom-left-radius": "3vmin"});
									var $this = $(this);
									var position = $this.position();
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'correct.png" />').insertAfter(this);
									$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
									$('.principal,.interest,.rate').css({"pointer-events":"none"});
									play_correct_incorrect_sound(1);
									nav_button_controls(200);
								}
								else{
									var $this = $(this);
									var position = $this.position();
									var width = $this.width();
									var height = $this.height();
									var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
									var centerY = ((position.top + height)*100)/$board.height()+'%';
									$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
									$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
									play_correct_incorrect_sound(0);
								}
							});
							break;
				case 8:
								$('.bottom-text1').hide(0);
								$('.optionclass1').click(function(){
									if($(this).hasClass('correct'))
									{
										$('.optionclass').css('pointer-events','none');
										$('.bottom-text1').fadeIn(200).delay(100).animate({"border-top-right-radius": "3vmin",
																																			"border-bottom-left-radius": "3vmin"});
										var $this = $(this);
										var position = $this.position();
										var width = $this.width();
										var height = $this.height();
										var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
										var centerY = ((position.top + height)*100)/$board.height()+'%';
										$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'correct.png" />').insertAfter(this);
										$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
										$('.optionclass1').css({"pointer-events":"none"});
										play_correct_incorrect_sound(1);
										nav_button_controls(200);
									}
									else{
										var $this = $(this);
										var position = $this.position();
										var width = $this.width();
										var height = $this.height();
										var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
										var centerY = ((position.top + height)*100)/$board.height()+'%';
										$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(120%,-200%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
										$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
										play_correct_incorrect_sound(0);
									}
								});
								break;
				default:
							nav_button_controls(1000);
							break;
			}
		function dragges(correctdrop){
			$(".first1,.second1,.third1,.fourth1,.sixth1,.seventh1,.eighth1").droppable({
									accept: ".oneone",
									// activeClass: "borderclass",
									over: function(event, ui) {
												 $(this).addClass("borderclass");
								 		 },
										 out: function(event, ui) {
											 $(this).removeClass("borderclass");
							     },
									drop: function (event, ui){
										if($(this).hasClass(correctdrop))
										{
											$(this).removeClass("borderclass");
											var $this = $(this);
											var position = $this.position();
											var width = $this.width();
											var height = $this.height();
											var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
											var centerY = ((position.top + height)*100)/$board.height()+'%';
											$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(80%,-95%)" src="'+imgpath +'correct.png" />').insertAfter(this);
												nav_button_controls(1000);
												var texto=ui.draggable.text();
												ui.draggable.css({"display":"none"});
												$('.'+correctdrop).html(texto).css({"background":"rgb(190,214,47)"});
												play_correct_incorrect_sound(1);
												// ui.draggable({ revert: 'invalid' });
										}
										else{
												var $this = $(this);
												$(this).removeClass("borderclass");
												var position = $this.position();
												var width = $this.width();
												var height = $this.height();
												var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
												var centerY = ((position.top + height)*100)/$board.height()+'%';
												$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(80%,-95%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
												play_correct_incorrect_sound(0);
												$(this).css({"background":"rgb(168, 33, 36)"});
										}
									}
							});
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

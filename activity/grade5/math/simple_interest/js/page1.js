var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'cover'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text1,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text2,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text3,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text4,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-4'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text5,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-5'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text6,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-6'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text7,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-7'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg_blue',

		extratextblock:[{
			textdata: data.string.p1text8,
			textclass: "top-text",
		},{
			textdata: data.string.p1text12,
			textclass: "monkeylabel1",
		},{
			textdata: data.string.p1text13,
			textclass: "monkeylabel2",
		},{
			textdata: data.string.p1text14,
			textclass: "monkeylabel3",
		}],
			imageandlabel : [
				{
					imgboxclass:'imgbox1',
					imgclass : "img1 ",
					imgid : 'img-1',
					textdata:data.string.p1text9,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox2',
					imgclass : "img2 ",
					imgid : 'img-2',
					textdata:data.string.p1text10,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox3',
					imgclass : "img3 ",
					imgid : 'img-3',
					textdata:data.string.p1text11,
					textclass:'textinlabel'
				}
			]
	},

	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg_blue',

		extratextblock:[{
			textdata: data.string.p1text15,
			textclass: "top-text",
		},{
			textdata: data.string.p1text19,
			textclass: "monkeylabel1",
		},{
			textdata: data.string.p1text20,
			textclass: "monkeylabel2",
		},{
			textdata: data.string.p1text21,
			textclass: "monkeylabel3",
		}],
			imageandlabel : [
				{
					imgboxclass:'imgbox1',
					imgclass : "img1 ",
					imgid : 'img-4',
					textdata:data.string.p1text16,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox2',
					imgclass : "img2 ",
					imgid : 'img-5',
					textdata:data.string.p1text17,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox3',
					imgclass : "img3 ",
					imgid : 'img-6',
					textdata:data.string.p1text18,
					textclass:'textinlabel'
				}
			]
	},

	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg_blue',

		extratextblock:[{
			textdata: data.string.p1text22a,
			textclass: "top-text",
		},{
			textdata: data.string.p1text19a,
			textclass: "monkeylabel1",
			datahighlightflag:true,
			datahighlightcustomclass:'hideandshow'
		},{
			textdata: data.string.p1text20a,
			textclass: "monkeylabel2",
			datahighlightflag:true,
			datahighlightcustomclass:'hideandshow'
		},{
			textdata: data.string.p1text21a,
			textclass: "monkeylabel3",
			datahighlightflag:true,
			datahighlightcustomclass:'hideandshow'
		}],
			imageandlabel : [
				{
					imgboxclass:'imgbox1',
					imgclass : "img1 ",
					imgid : 'img-4',
					textdata:data.string.p1text16,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox2',
					imgclass : "img2 ",
					imgid : 'img-5',
					textdata:data.string.p1text17,
					textclass:'textinlabel'
				},
				{
					imgboxclass:'imgbox3',
					imgclass : "img3 ",
					imgid : 'img-6',
					textdata:data.string.p1text18,
					textclass:'textinlabel'
				}
			]
	},

	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg_blue',

		extratextblock:[{
			textdata: data.string.p1text23,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p1text19b,
			textclass: "interest_text a",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p1text20b,
			textclass: "interest_text b",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		},{
			textdata: data.string.p1text21b,
			textclass: "interest_text c",
			datahighlightflag:true,
			datahighlightcustomclass:'underlined'
		}]
	},

	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text24,
			textclass: "top-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-8'
				}
			]
		}]
	},

	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg_blue',

		extratextblock:[{
			textdata: data.string.p1text25,
			textclass: "texttop",
		},
		{
			textdata: data.string.p1text26,
			textclass: "principal",
		},
		{
			textdata: data.string.p1text27,
			textclass: "time",
		},
		{
			textdata: data.string.p1text28,
			textclass: "rate",
		},
		{
			textdata: data.string.p1text29,
			textclass: "monkey-instruction template-dialougebox2-top-white",
		},
		{
			textdata: data.string.p1text30,
			textclass: "p",
		},
		{
			textdata: data.string.p1text31,
			textclass: "t",
		},
		{
			textdata: data.string.p1text32,
			textclass: "r",
		},],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey",
					imgsrc : '',
					imgid : 'img-7'
				},
				{
					imgclass : "monkey1",
					imgsrc : '',
					imgid : 'wowmonkey'
				},
				{
					imgclass : "monkey2",
					imgsrc : '',
					imgid : 'wowmonkey'
				},
				{
					imgclass : "monkey3",
					imgsrc : '',
					imgid : 'wowmonkey'
				}
			]
		}]
	},
	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p1text33,
			textclass: "top-text",
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-1", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-6", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-7", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-8", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-1", src: imgpath+"eat.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2", src: imgpath+"shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3", src: imgpath+"travel.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4", src: imgpath+"startbusiness.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-5", src: imgpath+"bike.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-6", src: imgpath+"workabroad.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-7", src: imgpath+"monkeyexp.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wowmonkey", src: imgpath+"wowmonkey.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		switch(countNext) {
			case 0:
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 1:
			alert("herre");
				$('.lista,.listb').children().css({"opacity":"0"});
				for (var i = 0; i < 5; i++) {
					$('.lista').find('li:nth-child('+(i+1)+')').delay(i*500).animate({"opacity":"1","z-index":"99"},1000);
					$('.listb').find('li:nth-child('+(i+1)+')').delay(3000+i*500).animate({"opacity":"1","z-index":"99"},1000);
				}
				$('.girl1,.girla1,.boy1').animate({"opacity":"0"},1000);
				$('.s3-1,.s3-2,.s3-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				$prevBtn.show(0);
				nav_button_controls(7000);
				break;
			case 2:
				$('.midtext').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 3:
			sound_player("s1_p"+(countNext+1),1);
				break;
			case 4:
				$('.girl1,.girla1,.boy1').animate({"opacity":"0"},1000);
				$('.s3-1,.s3-2,.s3-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				sound_player("s1_p"+(countNext+1),1);
			break;
			case 5:
				$('.s3-1,.s3-2,.s3-3').animate({"opacity":"0"},1000);
				$('.s4-1,.s4-2,.s4-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 6:
				$('.s4-1,.s4-2,.s4-3').animate({"opacity":"0"},1000);
				$('.s5-1,.s5-2,.s5-3').css({"opacity":"0"}).animate({"opacity":"1"},1000);
				sound_player("s1_p"+(countNext+1),1);
				break;
			case 7:
			sound_player("s1_p"+(countNext+1),1);
				break;
			case 8:
			case 9:
				$('.imgbox1,.imgbox2,.imgbox3,.monkeylabel3,.monkeylabel2,.monkeylabel1').hide(0);
				$('.imgbox1,.monkeylabel1').fadeIn(500);
				$('.imgbox2,.monkeylabel2').delay(500).fadeIn(500);
				$('.imgbox3,.monkeylabel3').delay(1000).fadeIn(500);
				countNext==9?sound_player("s1_p"+(countNext+1),1):nav_button_controls(2000);
				break;
			case 13:
			$nextBtn.hide(0);
			var count = 0;
			sound_player("s1_p"+(countNext+1),0);
			$('.monkey1,.monkey2,.monkey3,.p,.t,.r').hide(0);
			$('.principal').click(function(){
				count=count + 1;
				$(this).css({"width": "25%",
										"left": "6.25%",
										"height": "50%",
										"z-index": "99",
										"pointer-events":"none",
										"cursor":"auto",
										"border-top-left-radius": "8vmin",
										"border-bottom-right-radius": "8vmin"});
				$('.monkey1,.p').fadeIn(100);
				$('.monkey').fadeOut(100);
				$('.monkey-instruction').fadeOut(100);
				if(count==3){
					nav_button_controls(100);
				}
			});
			$('.time').click(function(){
				count=count + 1;
				$(this).css({"width": "25%",
										"left": "37.5%",
										"height": "50%",
										"z-index": "99",
										"pointer-events":"none",
										"cursor":"auto",
										"border-top-left-radius": "8vmin",
										"border-bottom-right-radius": "8vmin"});
				$('.monkey2,.t').fadeIn(100);
				$('.monkey').fadeOut(100);
				$('.monkey-instruction').fadeOut(100);
				if(count==3){
					nav_button_controls(100);
				}
			});
			$('.rate').click(function(){
				count=count + 1;
				$(this).css({"width": "25%",
										"left": "68.75%",
										"height": "50%",
										"z-index": "99",
										"pointer-events":"none",
										"cursor":"auto",
										"border-top-left-radius": "8vmin",
										"border-bottom-right-radius": "8vmin"});
				$('.monkey3,.r').fadeIn(100);
				$('.monkey').fadeOut(100);
				$('.monkey-instruction').fadeOut(100);
				if(count==3){
					nav_button_controls(100);
				}
			});

			break;
			case 14:
			$('.top-text').css({"top": "43%"});
			nav_button_controls(1000);
			break;

			default:
				$prevBtn.show(0);
				sound_player("s1_p"+(countNext+1),1);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		alert(sound_id);
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

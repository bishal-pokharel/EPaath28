var imgpath = $ref + "/images/";
var soundAsset1 = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'graybg',
		extratextblock:[{
			textdata:  data.string.p2text1,
			textclass: "top_text",
		}],
		insideimage:'insideimage',
		pencils:data.string.p2text12,
		add:data.string.p2text13,
		multiply:data.string.p2text14,
		result:data.string.p2text15,
		add1:data.string.p2text3,
		add2:data.string.p2text4,
		add3:data.string.p2text5,
		add4:data.string.p2text5a,
		add5:data.string.p2text5b,
		multiply1:data.string.p2text6,
		multiply2:data.string.p2text7,
		multiply3:data.string.p2text8,
		multiply4:data.string.p2text8a,
		multiply5:data.string.p2text8b,
		result1:data.string.p2text9,
		result2:data.string.p2text10,
		result3:data.string.p2text11,
		result4:data.string.p2text11a,
		result5:data.string.p2text11b,
		table:[{}]
	},

	// slide1
	{
		contentblockadditionalclass:'graybg',
		extratextblock:[{
			textdata:  data.string.p2text16,
			textclass: "top_text",
		}],
		insideimage:'insideimage',
		pencils:data.string.p2text12,
		add:data.string.p2text13,
		multiply:data.string.p2text14,
		result:data.string.p2text15,
		add1:data.string.p2text3,
		add2:data.string.p2text4,
		add3:data.string.p2text5,
		add4:data.string.p2text5a,
		add5:data.string.p2text5b,
		multiply1:data.string.p2text6,
		multiply2:data.string.p2text7,
		multiply3:data.string.p2text8,
		multiply4:data.string.p2text8a,
		multiply5:data.string.p2text8b,
		result1:data.string.p2text9,
		result2:data.string.p2text10,
		result3:data.string.p2text11,
		result4:data.string.p2text11a,
		result5:data.string.p2text11b,
		table:[{}]
	},


		// slide2
		{
			contentblockadditionalclass:'graybg',
			extratextblock:[{
				textdata:  data.string.p2text17,
				textclass: "top_text",
			}],
			insideimage:'insideimage',
			pencils:data.string.p2text12,
			add:data.string.p2text13,
			multiply:data.string.p2text14,
			result:data.string.p2text15,
			add1:data.string.p2text3,
			add2:data.string.p2text4,
			add3:data.string.p2text5,
			add4:data.string.p2text5a,
			add5:data.string.p2text5b,
			multiply1:data.string.p2text6,
			multiply2:data.string.p2text7,
			multiply3:data.string.p2text8,
			multiply4:data.string.p2text8a,
			multiply5:data.string.p2text8b,
			result1:data.string.p2text9,
			result2:data.string.p2text10,
			result3:data.string.p2text11,
			result4:data.string.p2text11a,
			result5:data.string.p2text11b,
			table:[{}]
		},

		// slide3
		{
			contentblockadditionalclass:'graybg',
			extratextblock:[{
				textdata:  data.string.p2text18,
				textclass: "top_text",
			}],
			insideimage:'insideimage',
			pencils:data.string.p2text12,
			add:data.string.p2text13,
			multiply:data.string.p2text14,
			result:data.string.p2text15,
			add1:data.string.p2text3,
			add2:data.string.p2text4,
			add3:data.string.p2text5,
			add4:data.string.p2text5a,
			add5:data.string.p2text5b,
			multiply1:data.string.p2text6,
			multiply2:data.string.p2text7,
			multiply3:data.string.p2text8,
			multiply4:data.string.p2text8a,
			multiply5:data.string.p2text8b,
			result1:data.string.p2text9,
			result2:data.string.p2text10,
			result3:data.string.p2text11,
			result4:data.string.p2text11a,
			result5:data.string.p2text11b,
			table:[{}]
		},

		//slide4
		{
		contentblockadditionalclass:'graybg',
		extratextblock:[{
			textdata:  data.string.p2text19,
			textclass: "mid_text",
		}],
	},

	//slide5
	{
		contentblockadditionalclass:'graybg',
		extratextblock:[{
			textdata:  data.string.p2text20,
			textclass: "top_text",
		},
		{
			textdata:  data.string.p2text21,
			textclass: "bottom_left_text fadein",
		}
		],
	imageblock:[{
		imagestoshow:[{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		},{
			imgclass:'pencils',
			imgid:'pencil01'
		}]
	}]
},

//slide6
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text23,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text21,
		textclass: "bottom_left_text",
	},
	{
		textdata:  data.string.p2text22,
		textclass: "bottom_right_text fadein",
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},

//slide7
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text41,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text21,
		textclass: "bottom_left_text",
	},
	{
		textdata:  data.string.p2text22,
		textclass: "bottom_right_text scale_box",
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},

//slide8
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text24,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text25,
		textclass: "question",
	},
	{
		textdata:  data.string.p2text26,
		textclass: "option correct"
	},
	{
		textdata:  data.string.p2text27,
		textclass: "option",
	},
	{
		textdata:  data.string.p2text28,
		textclass: "option",
	},
	{
		textdata:  data.string.p2text29,
		textclass: "option",
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},

//slide9
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text30,
		textclass: "top_text",
	},

	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils scalepencil',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},


//slide10
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text24,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text31,
		textclass: "question",
	},
	{
		textdata:  data.string.p2text32,
		textclass: "option correct"
	},
	{
		textdata:  data.string.p2text33,
		textclass: "option",
	},
	{
		textdata:  data.string.p2text34,
		textclass: "option",
	},
	{
		textdata:  data.string.p2text35,
		textclass: "option",
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},

//slide11
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text36,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text37,
		textclass: "question",
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},

//slide12
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text36,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text38,
		textclass: "bottom_mid_text fadein",
		datahighlightflag:'true',
		datahighlightcustomclass:'blue'
	}
	],
imageblock:[{
	imagestoshow:[{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	},{
		imgclass:'pencils',
		imgid:'pencil01'
	}]
}]
},


//slide13
{
	contentblockadditionalclass:'graybg',
	extratextblock:[{
		textdata:  data.string.p2text39,
		textclass: "middle_text",
	},
	{
		textdata:  data.string.p2text40,
		textclass: "bottom_mid_text fadein",
		datahighlightflag:'true',
		datahighlightcustomclass:'purple'
	}
	]
},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "pencil01", src: imgpath+"rs-501.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes

			// sounds
			{id: "correct", src: soundAsset1+"correct.ogg"},
			{id: "incorrect", src: soundAsset1+"incorrect.ogg"},
            {id: "sound_1", src: soundAsset + "s2_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s2_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s2_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s2_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s2_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s2_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s2_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s2_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s2_p11.ogg"},
            {id: "sound_12", src: soundAsset + "s2_p12.ogg"},
            {id: "sound_13", src: soundAsset + "s2_p13.ogg"},
            {id: "sound_14", src: soundAsset + "s2_p14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		$('.insideimage').attr('src',preload.getResult('pencil01').src);

		vocabcontroller.findwords(countNext);
		var pos_array=['pos1','pos2','pos3','pos4'];
		pos_array.shufflearray();
		for (var i = 0; i < 4; i++) {
			$('.option').eq(i).addClass(pos_array[i]);
		}
        sound_player!=12?sound_player("sound_"+(countNext+1),true):"";

		switch (countNext) {
			case 0:
			$('.showlater,.showlaterh,.showlater2,.showlater3').css('opacity','0');
			$('.table').css('background','none');
			break;
			case 1:
				$('.showlater').css({'opacity':'0'});
				$('.table,.showlater2,.showlater3').css('background','none');
				$('.showlater,.showlater2,.showlater3,.showlaterh').css('opacity','0');
				$('.showlaterp,.showlaterh').css({'background':'white'});
				$('.showlater,.showlaterh').animate({'opacity':'1'},500);
					break;
				case 2:
				$('.showlater').css({'opacity':'1'});
				$('.table,.showlater2,.showlater3').css('background','none');
				$('.showlater2,.showlater3,.showlaterh').css('opacity','0');
				$('.showlaterp,.showlaterh').css({'background':'white','opacity':'1'});
				$('.showlater2,.showlater3').css({'background':'white','opacity':'0'});
				$('.showlater2').animate({'opacity':'1'},500);
					break;
				case 3:
				$('.showlater').css({'opacity':'1'});
				$('.table,.showlater2,.showlater3').css('background','none');
				$('.showlater2,.showlater3,.showlaterh').css('opacity','0');
				$('.showlaterp,.showlaterh,.showlater2').css({'background':'white','opacity':'1'});
				$('.showlater3').css({'background':'white','opacity':'0'});
				$('.showlater3').animate({'opacity':'1'},500);
					break;

					case 8:
					case 10:
					$('.option').click(function(){
						if($(this).hasClass('correct')){
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
							var centerY = ((position.top + height)*100)/$('.coverboardfull ').height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-8%,-246%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background": "#98C02E",
														"border-color": "#DEEF3C",
															"transition":'.1s'});
							$('.option').css('pointer-events','none');
							sound_player1('correct');
							nav_button_controls(100);
						}
						else{
							$(this).css({"background": "#FF0000",
														"border-color": "#980000",
															"transition":'.1s',
															"pointer-events":"none"});
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$('.coverboardfull ').width()+'%';
							var centerY = ((position.top + height)*100)/$('.coverboardfull ').height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-8%,-246%)" src="'+imgpath +'incorrect.png" />').insertAfter(this);
							sound_player1('incorrect');
						}
					});
					break;
			case 12:
                nav_button_controls(100);
                break;
            default:
					break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player1(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
        	if(countNext!=8 || countNext!=11) {
                (navigate && countNext < 13) ? nav_button_controls(500) : (navigate && countNext == 13) ? ole.footerNotificationHandler.pageEndSetNotification() : "";
            }
        });
    }
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

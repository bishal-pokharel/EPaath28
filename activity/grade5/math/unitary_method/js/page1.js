var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'cover_page'
				}
			]
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.string.p1text14,
			textclass: "top_text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'zoomedout_shop'
				},
				{
					imgclass : "left_char",
					imgsrc : '',
					imgid : 'asha03'
				},
				{
					imgclass : "right_char",
					imgsrc : '',
					imgid : 'bhim03'
				}
			]
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'zoomedout_shop'
				},
				{
					imgclass : "left_char",
					imgsrc : '',
					imgid : 'asha03'
				},
				{
					imgclass : "right_char",
					imgsrc : '',
					imgid : 'bhim03'
				}
			]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1 fadein_dialogue",
			dialogimageclass: "dialog_image1 ",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1text1
		}],
	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'zoomedout_shop'
				},
				{
					imgclass : "left_char",
					imgsrc : '',
					imgid : 'asha03'
				},
				{
					imgclass : "right_char width_change2",
					imgsrc : '',
					imgid : 'bhim04'
				}
			]
		}],
		containsdialog: [{
			dialogcontainer: "dialog2 fadein_dialogue",
			dialogimageclass: "dialog_image1 mirror_image_X",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1text2
		}],
	},

	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'zoomedout_shop'
				},
				{
					imgclass : "left_char width_change_aasha",
					imgsrc : '',
					imgid : 'asha02'
				},
				{
					imgclass : "right_char",
					imgsrc : '',
					imgid : 'bhim03'
				}
			]
		}],
		containsdialog: [{
			dialogcontainer: "dialog1 fadein_dialogue",
			dialogimageclass: "dialog_image1 ",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1text3
		}],
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'zoomedout_shop'
				},
				{
					imgclass : "left_char",
					imgsrc : '',
					imgid : 'asha03'
				},
				{
					imgclass : "right_char width_change2",
					imgsrc : '',
					imgid : 'bhim_hold_pencil'
				}
			]
		}],
		containsdialog: [{
			dialogcontainer: "dialog2 fadein_dialogue",
			dialogimageclass: "dialog_image1 mirror_image_X",
			dialogueimgid: "bubble2",
			dialogcontentclass: "textfill",
			dialogcontent: data.string.p1text4
		}],
	},


		// slide6
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text5,
				textclass: "top_text",
			},{
				textdata: data.string.p1text6,
				textclass: "one_pencil",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "hand",
						imgsrc : '',
						imgid : 'hand'
					},
					{
						imgclass : "pencil",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},


		// slide7
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text8,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},{
				textdata: data.string.p1text6,
				textclass: "one_pencil",
			},{
				textdata: data.string.p1text7,
				textclass: "right_sub_text",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "money_hand",
						imgsrc : '',
						imgid : 'hand'
					},
					{
						imgclass : "money5_still",
						imgsrc : '',
						imgid : 'rsfive'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},


		// slide8
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text9,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},{
				textdata: data.string.p1text6,
				textclass: "one_pencil",
			},{
				textdata: data.string.p1text7,
				textclass: "right_sub_text",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big scaleup",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5_still scaleup1",
						imgsrc : '',
						imgid : 'rsfive'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},

		// slide9
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text10,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},
			{
				textdata: data.string.p1text12,
				textclass: "two_pencil",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big_towards_right",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5_still",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "hand",
						imgsrc : '',
						imgid : 'hand'
					},
					{
						imgclass : "pencil",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1_towards_right'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},


		// slide10
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text11,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},
			{
				textdata: data.string.p1text12,
				textclass: "two_pencil",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big_towards_right",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5_still",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1_towards_right'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},

		// slide11
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text13,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},
			{
				textdata: data.string.p1text12,
				textclass: "two_pencil",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big_towards_right",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5_still",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money_hand",
						imgsrc : '',
						imgid : 'hand'
					},{
						imgclass : "money5",
						imgsrc : '',
						imgid : 'rsfive'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1_towards_right'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},

		// slide12
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: '',
			extratextblock:[{
				textdata: data.string.p1text15,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bg_blue'
			},
			{
				textdata: data.string.p1text12,
				textclass: "two_pencil",
			},
			{
				textdata: data.string.p1text12,
				textclass: "two_pencil",
			},
			{
				textdata: data.string.p1text17,
				textclass: "money_sub_text",
			},
			{
				textdata: data.string.p1text16,
				textclass: "plus",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "bg-full",
						imgsrc : '',
						imgid : 'table01'
					},
					{
						imgclass : "pencil_big_towards_right",
						imgsrc : '',
						imgid : 'pencil02'
					},
					{
						imgclass : "money5_still",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "money5_still_second",
						imgsrc : '',
						imgid : 'rsfive'
					},
					{
						imgclass : "pencil_big",
						imgsrc : '',
						imgid : 'pencil02'
					}
				],
				imagelabels:[{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1_towards_right'
				},
				{
					imagelabeldata: data.string.p1text7,
					imagelabelclass:'pencil_tag1'
				}]
			}]
		},


				// slide13
				{
					contentnocenteradjust: true,
					contentblockadditionalclass: '',
					extratextblock:[{
						textdata: data.string.p1text18,
						textclass: "top_text",
						datahighlightflag:true,
						datahighlightcustomclass:'bg_blue'
					},
					{
						textdata: data.string.p1text12,
						textclass: "two_pencil",
					},
					{
						textdata: data.string.p1text12,
						textclass: "two_pencil",
					},
					{
						textdata: data.string.p1text17,
						textclass: "money_sub_text",
					},{
						textdata: data.string.p1text19,
						textclass: "money_sub_text2",
					},
					{
						textdata: data.string.p1text16,
						textclass: "plus",
					}],
					imageblock:[{
						imagestoshow : [
							{
								imgclass : "bg-full",
								imgsrc : '',
								imgid : 'table01'
							},
							{
								imgclass : "pencil_big_towards_right",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "money5_still",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "money5_still_second",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "pencil_big",
								imgsrc : '',
								imgid : 'pencil02'
							}
						],
						imagelabels:[{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1_towards_right'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1'
						}]
					}]
				},

				// slide14
				{
					contentnocenteradjust: true,
					contentblockadditionalclass: '',
					extratextblock:[{
						textdata: data.string.p1text25,
						textclass: "top_text",
						datahighlightflag:true,
						datahighlightcustomclass:'bg_blue'
					},
					{
						textdata: data.string.p1text21,
						textclass: "three_pencils",
					},
					{
						textdata: data.string.p1text24,
						textclass: "money_sub_text small_text",
					}],
					imageblock:[{
						imagestoshow : [
							{
								imgclass : "bg-full",
								imgsrc : '',
								imgid : 'table01'
							},
							{
								imgclass : "pencil_big_towards_right",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "pencil_big_towards_right2",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "money5_1",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "money5_2",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "money5_3",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "pencil_big",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "pencil",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "hand",
								imgsrc : '',
								imgid : 'hand'
							},
							{
								imgclass : "money_hand",
								imgsrc : '',
								imgid : 'hand'
							},{
								imgclass : "money5",
								imgsrc : '',
								imgid : 'rsfive'
							}

						],
						imagelabels:[{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1_towards_right'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1_towards_right2'
						}]
					}]
				},

				// slide15
				{
					contentnocenteradjust: true,
					contentblockadditionalclass: '',
					extratextblock:[{
						textdata: data.string.p1text20,
						textclass: "top_text",
						datahighlightflag:true,
						datahighlightcustomclass:'bg_blue'
					},
					{
						textdata: data.string.p1text21,
						textclass: "three_pencils",
					},
					{
						textdata: data.string.p1text22,
						textclass: "money_sub_text small_text",
					},{
						textdata: data.string.p1text23,
						textclass: "money_sub_text2",
					}],
					imageblock:[{
						imagestoshow : [
							{
								imgclass : "bg-full",
								imgsrc : '',
								imgid : 'table01'
							},
							{
								imgclass : "pencil_big_towards_right",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "pencil_big_towards_right2",
								imgsrc : '',
								imgid : 'pencil02'
							},
							{
								imgclass : "money5_1",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "money5_2",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "money5_3",
								imgsrc : '',
								imgid : 'rsfive'
							},
							{
								imgclass : "pencil_big",
								imgsrc : '',
								imgid : 'pencil02'
							}
						],
						imagelabels:[{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1_towards_right'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1'
						},
						{
							imagelabeldata: data.string.p1text7,
							imagelabelclass:'pencil_tag1_towards_right2'
						}]
					}]
				},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "table01", src: imgpath+"table01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zoomedout_shop", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zoomedin_shop", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha03", src: imgpath+"asha03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "asha02", src: imgpath+"asha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bhim03", src: imgpath+"bhim03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bhim04", src: imgpath+"bhim04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bubble2", src: imgpath+"bubble2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil01", src: imgpath+"pencil01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencil02", src: imgpath+"pencil02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rsfive", src: imgpath+"5_rupee.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bhim_hold_pencil", src: imgpath+"bhim_hold_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_page", src: imgpath+"cover_page.jpg", type: createjs.AbstractLoader.IMAGE},

			// soundsa
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s1_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s1_p8.ogg"},
            {id: "sound_9", src: soundAsset + "s1_p9.ogg"},
            {id: "sound_10", src: soundAsset + "s1_p10.ogg"},
            {id: "sound_11", src: soundAsset + "s1_p11.ogg"},
            {id: "sound_12", src: soundAsset + "s1_p12.ogg"},
            {id: "sound_13", src: soundAsset + "s1_p13.ogg"},
            {id: "sound_14", src: soundAsset + "s1_p14.ogg"},
            {id: "sound_15", src: soundAsset + "s1_p15.ogg"},
            {id: "sound_16", src: soundAsset + "s1_p16.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		$nextBtn.hide(0);
		$('.plus_font').attr('style', 'font-family: sniglet !important');

		ole.footerNotificationHandler.hideNotification();
        sound_player("sound_"+(countNext+1),true);

        switch(countNext) {
			case 6:
			$('.pencil_big,.pencil_tag1,.one_pencil').css('opacity','0');
			setTimeout(function () {
				$('.pencil_big,.pencil_tag1,.one_pencil').animate({'opacity':'1'},1000);
				$('.pencil,.pencil_tag,.hand').animate({'opacity':'0'},1000);
			}, 2000);
			break;
			case 7:
			$('.money5_still,.right_sub_text').css('opacity','0');
			setTimeout(function () {
				$('.money5_still,.right_sub_text').animate({'opacity':'1'},1000);
				$('.money_hand,.money5').animate({'opacity':'0'},1000);
			}, 2000);
			break;
			case 9:
			$('.pencil_big,.pencil_tag1,.two_pencil').css('opacity','0');
			setTimeout(function () {
				$('.pencil_big,.pencil_tag1,.two_pencil').animate({'opacity':'1'},1000);
				$('.hand,.pencil_tag,.pencil').animate({'opacity':'0'},1000);
			}, 2000);
			break;
			case 12:
			$('.first,.second,.third,.plus,.bg_blue,.fourth').css('opacity','0');
			$('.money5_still').addClass('expanding_money');
			$('.first').animate({'opacity':'1'},1000);
			setTimeout(function(){
				$('.plus').animate({'opacity':'1'},1000);
				$('.second').animate({'opacity':'1'},1000);
			},1000);
			setTimeout(function(){
				$('.money5_still_second').addClass('expanding_money');
				$('.third').animate({'opacity':'1'},1000);
			},2000);
			setTimeout(function(){
				$('.bg_blue').animate({'opacity':'1'},1000);
				$('.fourth').animate({'opacity':'1'},1000);
			},3000);
			break;
			case 13:
			$('.first2,.second2,.third2,.bg_blue,.fourth2').css('opacity','0');
			$('.first2').animate({'opacity':'1'},1000);
			$('.money5_still').addClass('expanding_money');
			setTimeout(function(){
				$('.second2').animate({'opacity':'1'},1000);
			},1000);
			setTimeout(function(){
				$('.third2').animate({'opacity':'1'},1000);
				$('.pencil_big,.pencil_big_towards_right').addClass('expanding_money');
			},2000);
			setTimeout(function(){
				$('.fourth2,.bg_blue').animate({'opacity':'1'},1000);
			},3000);
			setTimeout(function(){
				$('.money_sub_text2').animate({'top':'77%'},1000);
				$('.money_sub_text').animate({'opacity':'0'},1000);
			},4000);

			break;
			case 14:
			$('.pencil_big,.pencil_tag1,.two_pencil').css('opacity','0');
			$('.money5_3,.money_sub_text').css('opacity','0');
			$('.money_hand,.money5').hide(0);
			setTimeout(function () {
				$('.pencil_big,.pencil_tag1,.two_pencil').animate({'opacity':'1'},1000);
				$('.hand,.pencil_tag,.pencil').animate({'opacity':'0'},1000);
			}, 2000);
			setTimeout(function () {
				$('.money_hand,.money5').show(0);
			}, 2500);
			setTimeout(function () {
				$('.money5_3,.money_sub_text').animate({'opacity':'1'},1000);
				$('.money_hand,.money5').animate({'opacity':'0'},1000);
			}, 4500);
			break;
			case 15:
			$('.first2,.second2,.third2,.bg_blue,.fourth2').css('opacity','0');
			$('.first2').animate({'opacity':'1'},1000);
			$('.money5_3').addClass('expanding_money');
			setTimeout(function(){
				$('.second2').animate({'opacity':'1'},1000);
			},1000);
			setTimeout(function(){
				$('.third2').animate({'opacity':'1'},1000);
				$('.pencil_big,.pencil_big_towards_right,.pencil_big_towards_right2').addClass('expanding_money');
			},2000);
			setTimeout(function(){
				$('.fourth2,.bg_blue').animate({'opacity':'1'},1000);
			},3000);
			setTimeout(function(){
				$('.money_sub_text2').animate({'top':'77%'},1000);
				$('.money_sub_text2').css('opacity','0');
				nav_button_controls(1000);
			},4000);

			break;
			default:
			if(countNext>0){
				$prevBtn.show(0);
			}
				// nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            (navigate && countNext<5)?nav_button_controls(500):(navigate && countNext==5)?ole.footerNotificationHandler.pageEndSetNotification():"";

        });
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset1 = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text1,
			textclass: "bottom_text",
		}],
		pencil_box_div:'pencil_box2',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			},{
				imgid:'pencils',
				imgclass:'pencil1 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil2 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil3 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil4 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil5 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil6 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil7 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil8 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil9 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil10 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil11 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil12 pencils'
			}],
			alonepencilbox:'pencil_box1',
			text_in_alonepencil:data.string.p5text3,
			pencilbox:[{
					imgid:'yellow_pencil',
					imgclass:'alone_pencil pencils'
			}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text5,
			textclass: "bottom_text",
		}],
		pencil_box_div:'pencil_box2',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			},{
				imgid:'pencils',
				imgclass:'pencil1 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil2 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil3 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil4 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil5 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil6 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil7 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil8 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil9 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil10 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil11 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil12 pencils'
			}],
			alonepencilbox:'pencil_box1',
			text_in_alonepencil:data.string.p5text3,
			pencil_text_class:'changetext',
			pencilbox:[{
					imgid:'yellow_pencil',
					imgclass:'alone_pencil pencils'
			}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text6,
			textclass: "bottom_text",
		}],
		pencil_box_div:'pencil_box1',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			},{
				imgid:'pencils',
				imgclass:'pencil1 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil2 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil3 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil4 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil5 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil6 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil7 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil8 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil9 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil10 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil11 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil12 pencils'
			}]
	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text7,
			textclass: "bottom_text",
		},{
			textdata:  data.string.p5text8,
			textclass: "top_text",
		}],
		pencil_box_div:'pencil_box1',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			},{
				imgid:'pencils',
				imgclass:'pencil1 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil2 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil3 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil4 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil5 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil6 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil7 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil8 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil9 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil10 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil11 pencils'
			},{
				imgid:'pencils',
				imgclass:'pencil12 pencils'
			}],
			imageblock:[{
				imagestoshow:[{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					}]
			}]
	},

	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text9,
			textclass: "bottom_text",
		},{
			textdata:  data.string.p5text10,
			textclass: "top_text",
		}],
		pencil_box_div:'pencil_box1',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			}],
			imageblock:[{
				imagestoshow:[{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					}]
			}]
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text11,
			textclass: "bottom_text",
		},{
			textdata:  data.string.p5text10,
			textclass: "top_text",
		}],
		pencil_box_div:'pencil_box1',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			}],
			imageblock:[{
				imagestoshow:[{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					}]
			}]
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text12,
			textclass: "bottom_text",
		},{
			textdata:  data.string.p5text10,
			textclass: "top_text",
		}],
		pencil_box_div:'pencil_box1',
		text_in_imagediv:data.string.p5text2,
		imagewithtext:[{
				imgid:'box_back',
				imgclass:'box_back'
			},{
				imgid:'box_front',
				imgclass:'box_front'
			}],
			imageblock:[{
				imagestoshow:[{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'pencils',
						imgclass:'pencil'
					},{
						imgid:'rs-501',
						imgclass:'pencilz'
					}]
			}]
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass:'greenbg',
		extratextblock:[{
			textdata:  data.string.p5text13,
			textclass: "top_mid_text",
		},{
			textdata:  data.string.p5text14,
			textclass: "bottom_bg_text",
			datahighlightflag:'true',
			datahighlightcustomclass:'blue'
		}]
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var settime;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "yellow_pencil", src: imgpath+"yellow_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pencils", src: imgpath+"yellow_pencil.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box_back", src: imgpath+"box_back.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box_front", src: imgpath+"box_front.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rs-501", src: imgpath+"rs-501.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "sound_1", src: soundAsset + "s5_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s5_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s5_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s5_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s5_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s5_p6.ogg"},
            {id: "sound_7", src: soundAsset + "s5_p7.ogg"},
            {id: "sound_8", src: soundAsset + "s5_p8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            (navigate && countNext < 7) ?nav_button_controls(100): (navigate && countNext == 7) ? ole.footerNotificationHandler.pageEndSetNotification() : "";
        });
    }

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		$('.pencils').attr('src',preload.getResult('yellow_pencil').src);
		$('.box_back').attr('src',preload.getResult('box_back').src);
		$('.box_front').attr('src',preload.getResult('box_front').src);
        sound_player("sound_"+(countNext+1),true);

        switch(countNext) {
			case 1:
				$('.pencil_box2').addClass('toleft');
				$('.pencil_box1').addClass('toright');
				setTimeout(function(){
					$('.changetext').html(data.string.p5text4);
				},500);
				break;

			case 3:
				clearTimeout(settime);
				for (var j = 1; j < 13; j++) {
					$('.show'+j).css('opacity','0');
				}
				$('.pencil').css('opacity','0');

				for (var i = 0; i < 13; i++) {
						show_hide(i);
				}
				function show_hide(i){
                    settime = setTimeout(function(){
						console.log("This is the value of i "+i)
						$('.pencils').eq(i).animate({'opacity':'0'},1000);
						$('.pencil').eq(i).animate({'opacity':'1'},1000);
						$('.show'+(i)).animate({'opacity':'1'},100);
					},i*1000);
				}
				// setTimeout(function(){
				// 	nav_button_controls(200);
				// },12000);
				break;

			case 6:
				clearTimeout(settime);
				$('.pencilz').css({'width':'11%'});
				break;
			case 7:
				$('.long').css({"left":" 32%"});
				break;
			default:
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}

	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	function input_box(input_class, max_number, max) {
		$(input_class).keydown(function(event) {
			var charCode = (event.which) ? event.which : event.keyCode;
			/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
			if (!condition) {
				return true;
			}
			//check if user inputs more than one '.'
			if ((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
				return false;
			}
			//check . and 0-9 separately after checking arrow and other keys
			if ((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110) {
				return false;
			}
			//check max no of allowed digits
			if (String(event.target.value).length >= max_number) {
				return false;
			}
		});
		$(input_class).blur(function(event) {
			var curr_val = event.target.value;
			if(String(curr_val).length==1){
				$(input_class).val('0'+curr_val);
			}
			if (curr_val > max) {
				$(input_class).val(max);
			}
			if (curr_val == '') {
				$(input_class).val('00');
			} else{
				$(input_class).data('enter','1');
			}
			for(var i =1; i<6; i++){
				if(parseInt($('.hr'+i).data('enter')) == 0 || parseInt($('.min'+i).data('enter')) == 0){
					return false;
				}
			}
			ole.footerNotificationHandler.pageEndSetNotification();
		});
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		clearTimeout(timeoutvar);
		clearTimeout(settime);
		countNext!=3?templateCaller():"";
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

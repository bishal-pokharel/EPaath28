var soundAsset1 = $ref+"/sounds/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/page7/";
var imgpath2 = $ref+"/images/page7/alone_pictures/";
var imgpath1 = $ref+"/images/";
var imgpath_shop1 = $ref+"/images/page7/shops/shop_1/";
var imgpath_shop2 = $ref+"/images/page7/shops/shop_2/";
var imgpath_shop3 = $ref+"/images/page7/shops/shop_3/";
var imgpath_shop4 = $ref+"/images/page7/shops/shop_4/";

var content=[
	// slide1
	{
		contentblockadditionalclass:'green_bg',
		extratextblock:[{
			textclass:'instuction',
			textdata:data.string.p7text1
		},{
			textclass:'top_instuction',
			textdata:data.string.p7text1_1
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'jomsommarket',
				imgclass:'jomsommarket'
			},{
				imgid:'lubhumarket',
				imgclass:'lubhumarket'
			},{
				imgid:'mahendranagarmarket',
				imgclass:'mahendranagarmarket'
			},{
				imgid:'hetaudamarket',
				imgclass:'hetaudamarket'
			}]
		}],
		other_image:[{
			imgclass:'shop-1'
		},{
			imgclass:'shop-2'
		},{
			imgclass:'shop-3'
		},{
			imgclass:'shop-4'
		},{
			imgclass:'bg_full'
		}]
	},

	//slide1
	{
		extratextblock:[{
			textclass:'shop_name_text',
		},{
			textclass:'item_price-1',
		},{
			textclass:'item_price-2',
		},{
			textclass:'item_price-3',
		},{
			textclass:'item_price-4',
		},{
			textclass:'item_price-5',
		},{
			textclass:'item_price-6',
		},{
			textclass:'instruction',
		}],
		other_image:[{
		imgclass:'item-1 items'
	},{
		imgclass:'item-2 items'
	},{
		imgclass:'item-3 items'
	},{
		imgclass:'item-4 items'
	},{
		imgclass:'item-5 items'
	},{
		imgclass:'item-6 items'
	},{
		imgclass:'bg_full'
	}],
	given_1:data.string.no_of_items_2,
	given_2:data.string.cost_if_item_2,
	question:data.string.question_2,
	question_div:[{}]
}



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var shop_clicked = '';
	var shop_number;
	var shop_type_number;

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "jomsommarket", src: imgpath+"jomsommarket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lubhumarket", src: imgpath+"lubhumarket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mahendranagarmarket", src: imgpath+"mahendranagarmarket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hetaudamarket", src: imgpath+"hetaudamarket.png", type: createjs.AbstractLoader.IMAGE},
			{id: "c_shop1", src: imgpath_shop1+"c_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "s_shop1", src: imgpath_shop1+"s_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_shop1", src: imgpath_shop1+"t_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "f_shop1", src: imgpath_shop1+"f_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg1", src: imgpath_shop1+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "c_shop2", src: imgpath_shop2+"c_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "s_shop2", src: imgpath_shop2+"s_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_shop2", src: imgpath_shop2+"t_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "f_shop2", src: imgpath_shop2+"f_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath_shop2+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "c_shop3", src: imgpath_shop3+"c_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "s_shop3", src: imgpath_shop3+"s_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_shop3", src: imgpath_shop3+"t_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "f_shop3", src: imgpath_shop3+"f_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath_shop3+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "c_shop4", src: imgpath_shop4+"c_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "s_shop4", src: imgpath_shop4+"s_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "t_shop4", src: imgpath_shop4+"t_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "f_shop4", src: imgpath_shop4+"f_shop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath_shop4+"bg.png", type: createjs.AbstractLoader.IMAGE},

			{id: "shop_item1-1", src: imgpath+"5flower.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item2-1", src: imgpath+"10flowers.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item3-1", src: imgpath+"24flowers.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item4-1", src: imgpath+"60flowers.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item5-1", src: imgpath+"3flowers.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item6-1", src: imgpath+"2flowers.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backgroundforshop-1", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "shop_item1-2", src: imgpath+"5toys.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item2-2", src: imgpath+"10toys.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item3-2", src: imgpath+"24toys.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item4-2", src: imgpath+"60toys.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item5-2", src: imgpath+"3_ball.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item6-2", src: imgpath+"2rings.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backgroundforshop-2", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "shop_item1-3", src: imgpath+"5bluedress.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item2-3", src: imgpath+"10greent_shirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item3-3", src: imgpath+"24frocks.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item4-3", src: imgpath+"60blackjeans.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item5-3", src: imgpath+"3bluet_shirt.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item6-3", src: imgpath+"2bluejeans.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backgroundforshop-3", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "shop_item1-4", src: imgpath+"5_notebook.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item2-4", src: imgpath+"10packets.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item3-4", src: imgpath+"24packets.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item4-4", src: imgpath+"60boxes.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item5-4", src: imgpath+"3packets.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shop_item6-4", src: imgpath+"glue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "backgroundforshop-4", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sampleimage1-1", src: imgpath+"rose.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage2-1", src: imgpath+"marigold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage3-1", src: imgpath+"lalupate.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage4-1", src: imgpath+"hibiscus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage5-1", src: imgpath+"jasmine.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage6-1", src: imgpath+"orchid.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sampleimage1-2", src: imgpath2+"toy01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage2-2", src: imgpath2+"toy02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage3-2", src: imgpath2+"toy04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage4-2", src: imgpath2+"toy05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage5-2", src: imgpath2+"toy07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage6-2", src: imgpath2+"toy08.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sampleimage1-3", src: imgpath2+"clothes01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage2-3", src: imgpath2+"clothes02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage3-3", src: imgpath2+"clothes03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage4-3", src: imgpath2+"clothes04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage5-3", src: imgpath2+"clothes05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage6-3", src: imgpath2+"clothes06.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sampleimage1-4", src: imgpath2+"stationary-item05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage2-4", src: imgpath2+"stationary-item06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage3-4", src: imgpath2+"stationary-item07.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage4-4", src: imgpath2+"stationary-item01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage5-4", src: imgpath2+"stationary-item08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sampleimage6-4", src: imgpath2+"stationary-item10.png", type: createjs.AbstractLoader.IMAGE},

			{id: "white_wrong", src: imgpath+"white_wrong.png", type: createjs.AbstractLoader.IMAGE},


			// soundsa
			{id: "correct", src: soundAsset1+"correct.ogg"},
			{id: "incorrect", src: soundAsset1+"incorrect.ogg"},
			{id: "sound_1", src: soundAsset + "s4_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s4_p1(after clicked).ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(500) :"";
        });
    }

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		$nextBtn.hide(0);
		ole.footerNotificationHandler.hideNotification();
		$('.option').click(function(){
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
				$('<img class="right_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-8%,-261%)" src="'+imgpath1 +'correct.png" />').insertAfter(this);
				$(this).addClass('right');
				$('.option').css('pointer-events','none');
				sound_player1('correct');
				$('.hint').fadeIn(1000);
				$('.cross_button').show(200);
				nav_button_controls(100);
			}
			else{
				$(this).css('pointer-events','none');
				$(this).addClass('wrong');
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.question_div ').width()+'%';
				var centerY = ((position.top + height)*100)/$('.question_div ').height()+'%';
				$('<img class="wrong_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(-8%,-261%)" src="'+imgpath1 +'incorrect.png" />').insertAfter(this);
				$('.hint').fadeIn(1000);
				sound_player1('incorrect');
			}
	});
		switch(countNext) {
			case 0:
                sound_player("sound_1",false);
                $('.top_instuction,.shop-1,.shop-2,.shop-3,.shop-4,.bg_full').css('display','none');
				$('.lubhumarket').click(function(){
						setTimeout(function(){
							sound_player("sound_2",false);
						},1000);
						shop_clicked='lubhumarket';
						shop_number=1;
						attr_image(1);
						console.log('The selected shop no. is '+shop_number+' which is '+ shop_clicked);
						$(this).addClass('fitimage').delay(2000).hide(0);
						$('.lubhumarket,.hetaudamarket,.jomsommarket,.mahendranagarmarket').css('pointer-events','none');
				});
				$('.hetaudamarket').click(function(){
						setTimeout(function(){
							sound_player("sound_2",false);
						},1000);
						shop_clicked='hetaudamarket';
						shop_number=2;
						attr_image(2);
						console.log('The selected shop no. is '+shop_number+' which is '+ shop_clicked);
						$(this).addClass('fitimage').delay(2000).hide(0);
						$('.lubhumarket,.hetaudamarket,.jomsommarket,.mahendranagarmarket').css('pointer-events','none');
				});
				$('.jomsommarket').click(function(){
						setTimeout(function(){
							sound_player("sound_2",false);
						},1000);
						shop_clicked='jomsommarket';
						shop_number=3;
						attr_image(3);
						console.log('The selected shop no. is '+shop_number+' which is '+ shop_clicked);
						$(this).addClass('fitimage').delay(2000).hide(0);
						$('.lubhumarket,.hetaudamarket,.jomsommarket,.mahendranagarmarket').css('pointer-events','none');
				});
				$('.mahendranagarmarket').click(function(){
						setTimeout(function(){
							sound_player("sound_2",false);
						},1000);
						shop_clicked='mahendranagarmarket';
						shop_number=4;
						attr_image(4);
						console.log('The selected shop no. is '+shop_number+' which is '+ shop_clicked);
						$(this).addClass('fitimage').delay(2000).hide(0);
						$('.lubhumarket,.hetaudamarket,.jomsommarket,.mahendranagarmarket').css('pointer-events','none');
				});
				function attr_image(shop_number){
					setTimeout(function(){
						$('.shop-1').attr('src',preload.getResult('f_shop'+shop_number).src);
						$('.shop-2').attr('src',preload.getResult('t_shop'+shop_number).src);
						$('.shop-3').attr('src',preload.getResult('c_shop'+shop_number).src);
						$('.shop-4').attr('src',preload.getResult('s_shop'+shop_number).src);
						$('.bg_full').attr('src',preload.getResult('bg'+shop_number).src);
						$('.lubhumarket,.hetaudamarket,.jomsommarket,.mahendranagarmarket,.instuction').css('display','none');
						$('.top_instuction,.shop-1,.shop-2,.shop-3,.shop-4,.bg_full').css('display','block');
					},2000);
					$('.shop-1,.shop-2,.shop-3,.shop-4').click(function(){
						$('.shop-1,.shop-2,.shop-3,.shop-4,.bg_full,.top_instuction').css('pointer-events','none');
						$('.shop-1,.shop-2,.shop-3,.shop-4,.bg_full').not(this).addClass('gray_out');
						$('.top_instuction').hide(0);
						if($(this).hasClass('shop-1')){
							shop_type_number=1;
							console.log('shope type '+shop_type_number+' selected');
						}
						if($(this).hasClass('shop-2')){
							shop_type_number=2;
							console.log('shope type '+shop_type_number+' selected');
						}
						if($(this).hasClass('shop-3')){
							shop_type_number=3;
							console.log('shope type '+shop_type_number+' selected');
						}
						if($(this).hasClass('shop-4')){
							shop_type_number=4;
							console.log('shope type '+shop_type_number+' selected');
						}
						nav_button_controls(200);
					});
				}
				break;
				case 1:
							$('.cross_button').hide(0);
							$('.instruction').html(eval('data.string.p7text2_'+shop_type_number));
							$('.shop_name_text').html(eval('data.string.title_part1_'+shop_number)+eval('data.string.title_part2_'+shop_type_number));
							$('.item_price-1').html(eval('data.string.price1_'+shop_type_number));
							$('.item_price-2').html(eval('data.string.price2_'+shop_type_number));
							$('.item_price-3').html(eval('data.string.price3_'+shop_type_number));
							$('.item_price-4').html(eval('data.string.price4_'+shop_type_number));
							$('.item_price-5').html(eval('data.string.price5_'+shop_type_number));
							$('.item_price-6').html(eval('data.string.price6_'+shop_type_number));

							$('.item-1').attr('src',preload.getResult('shop_item1-'+shop_type_number).src);
							$('.item-2').attr('src',preload.getResult('shop_item2-'+shop_type_number).src);
							$('.item-3').attr('src',preload.getResult('shop_item3-'+shop_type_number).src);
							$('.item-4').attr('src',preload.getResult('shop_item4-'+shop_type_number).src);
							$('.item-5').attr('src',preload.getResult('shop_item5-'+shop_type_number).src);
							$('.item-6').attr('src',preload.getResult('shop_item6-'+shop_type_number).src);
							$('.bg_full').attr('src',preload.getResult('backgroundforshop-'+shop_type_number).src);

							$('.cross_button').attr('src',preload.getResult('white_wrong').src);
							$('.cross_button').click(function(){
								$('.question_div').hide(200);
								$('.instruction').show(0);
								$(this).hide(200);
							});


							$('.question_div').hide(0);
							$('.items').click(function(){
								$('.instruction').hide(0);
								$('.question_div').show(500);
								if($(this).hasClass('item-1')){
									$('.sample_image').attr('src',preload.getResult('sampleimage1-'+shop_type_number).src);
									for_question_div(1,5);
								}
								if($(this).hasClass('item-2')){
									$('.sample_image').attr('src',preload.getResult('sampleimage2-'+shop_type_number).src);
									for_question_div(2,10);
								}
								if($(this).hasClass('item-3')){
									$('.sample_image').attr('src',preload.getResult('sampleimage3-'+shop_type_number).src);
									for_question_div(3,24);
								}
								if($(this).hasClass('item-4')){
									$('.sample_image').attr('src',preload.getResult('sampleimage4-'+shop_type_number).src);
									for_question_div(4,60);
								}
								if($(this).hasClass('item-5')){
									$('.sample_image').attr('src',preload.getResult('sampleimage5-'+shop_type_number).src);
									for_question_div(5,3);
								}
								if($(this).hasClass('item-6')){
									$('.sample_image').attr('src',preload.getResult('sampleimage6-'+shop_type_number).src);
									for_question_div(6,2);
								}
							});
							function for_question_div(item_number,number_of_items){
								$('.option').removeClass('wrong right').css('pointer-events','auto');
								$('.hint').html(data.string.hint).hide(0);
								$('.right_icon,.wrong_icon').hide(0);
								var costofitems = $('.item_price-'+item_number).text();
								$('.noofitem').html(number_of_items);
								$('.costofitem').html(costofitems);
								$('.option').eq(0).html(costofitems);
								$('.option').eq(1).html(data.string.thousand);
								var correct_ans = (parseInt(costofitems.replace("Rs. ","")))/number_of_items;
								if($lang=='np'){
									var correct_ans = (parseInt(costofitems.replace("रु. ","")))/number_of_items;
								}
								$('.option').eq(2).html(data.string.rs+correct_ans).addClass('correct');
								$('.option').eq(3).html(data.string.rs+number_of_items);
								$('.correct_answer').html(data.string.rs+correct_ans);
								var pos_array=['pos1','pos2','pos3','pos4'];
								pos_array.shufflearray();
								for (var i = 0; i < 4; i++) {
									$('.option').eq(i).removeClass('pos4 pos1 pos2 pos3').addClass(pos_array[i]);
								}

						}

				break;


			default:
			if(countNext>0){
				$prevBtn.show(0);
			}
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player1(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var content=[
    //slide 0
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq1,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q1a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q1a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q1a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q1a4,
                    }],
            }
        ]
    },
    //slide 1
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq2,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q2a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q2a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q2a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q2a4,
                    }],
            }
        ]
    },
    //slide 2
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq3,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q3a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q3a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q3a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q3a4,
                    }],
            }
        ]
    },
    //slide 3
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq4,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q4a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q4a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q4a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q4a4,
                    }],
            }
        ]
    },
    //slide 4
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq5,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q5a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q5a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q5a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q5a4,
                    }],
            }
        ]
    },
    //slide 5
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq6,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q6a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q6a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q6a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q6a4,
                    }],
            }
        ]
    },
    //slide 6
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq7,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q7a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q7a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q7a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q7a4,
                    }],
            }
        ]
    },
    //slide 7
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq8,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q8a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q8a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q8a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q8a4,
                    }],
            }
        ]
    },
    //slide 8
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq9,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q9a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q9a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q9a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q9a4,
                    }],
            }
        ]
    },
    //slide 10
    {
        contentblockadditionalclass:'',
        exerciseblock: [
            {
                instruction:data.string.p2text24,
                textclass : 'instruction my_font_big',
                questiondata: data.string.pq10,
                option: [
                    {
                        option_class: "class1",
                        optiondata: data.string.q10a1,
                    },
                    {
                        option_class: "class2",
                        optiondata: data.string.q10a2,
                    },
                    {
                        option_class: "class3",
                        optiondata: data.string.q10a3,
                    },
                    {
                        option_class: "class4",
                        optiondata: data.string.q10a4,
                    }],
            }
        ]
    },
];

/*remove this for non random questions*/
$(function () {
    var numbertemp = new NumberTemplate();

    var exercise = new template_exercise_mcq_monkey(content, numbertemp, true);
    exercise.create_exercise();
});

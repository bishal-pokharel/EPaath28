var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var shop_colors = [
					[		
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D', data.string.shopkeeper_1, 'signatures/s01.png' ],				
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D', data.string.shopkeeper_2, 'signatures/s02.png'  ],			
							['#E70052','#FABD15','#FFF4FC','#FF6F86','#BE7E54','#D39361', data.string.shopkeeper_3, 'signatures/s03.png' ],				
							['#90B84F','#BB84AA', '#FFFCED','#98C153','#8A5961','#EEDFDD', data.string.shopkeeper_4, 'signatures/s04.png' ]		
					],//lubhu
					[		
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D', data.string.shopkeeper_5, 'signatures/s05.png'  ],				
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D' , data.string.shopkeeper_6, 'signatures/s06.png' ],			
							['#21ADFF','#FABD15','#069A8F','#92C13F','#BE7E54','#E9A86E', data.string.shopkeeper_7, 'signatures/s07.png' ],				
							['#8E73A0','#E1D3ED','#FFF4FC','#E1D3ED','#8A627D','#EED9E8', data.string.shopkeeper_8, 'signatures/s08.png' ]		
					],//hetauda
					[		
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D', data.string.shopkeeper_9, 'signatures/s09.png'  ],				
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D', data.string.shopkeeper_10 , 'signatures/s10.png' ],		
							['#7B74D4','#FABD15','#1995AD','#716CA0','#BE7E54','#E9A86E', data.string.shopkeeper_11, 'signatures/s11.png' ],					
							['#FA6775','#FFF4FC','#FFFCED','#E1D3ED','#7F779B','#B0A5C4', data.string.shopkeeper_12, 'signatures/s12.png' ]		
					],//mahendranagar
					[		
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D' , data.string.shopkeeper_13, 'signatures/s13.png' ],				
							['#1CA77E','#E1D3ED','#FFFCED','#90B84F', '#BE7E54', '#EEBD8D' , data.string.shopkeeper_14, 'signatures/s14.png' ],			
							['#10A9A7','#FFF4FC','#E1D3ED','#10A9A7','#BE7E54','#D39361', data.string.shopkeeper_15, 'signatures/s15.png' ],				
							['#FF6779','#F2B187','#FDF6F6','#FEE1D8','#BE7E54', '#D39361', data.string.shopkeeper_16, 'signatures/s16.png' ]		
					]//jomsom
				  ];
				  
//cloth stationery toy flower
//title bg, title border, shop_bg, floor bg, shelf bg, shelf border


var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'ole-background-gradient-brick',
		uppertextblockadditionalclass : "options_budget my_font_big",
		uppertextblock : [{
			textclass : '',
			textdata : data.string.budget1
		},
		{
			textclass : '',
			textdata : data.string.budget2
		},
		{
			textclass : '',
			textdata : data.string.budget3
		},
		{
			textclass : '',
			textdata : data.string.budget4
		}],
		extratextblock:[{
			textdata : data.string.p2text1,
			textclass : 'my_font_ultra_big lets_shop'
		},
		{
			textdata : data.string.p2text9,
			textclass : 'my_font_big budget_ask'
		}]
		
	},
	//slide1
	{
		contentblockadditionalclass: 'green_bg',
		contentblocknocenteradjust : true,
		
		extratextblock:[{
			textdata : data.string.choose1,
			textclass : 'my_font_ultra_big choose-market-text'
		}],
		
		imagetextblock : [{
				imagediv : "split_1 splits",
				imgsrc : imgpath + "mall_1.png",
				textclass : "banner my_font_medium",
				textdata : data.string.market1,
			},
			{
				imagediv : "split_2 splits",
				imgsrc : imgpath + "mall_2.png",
				textclass : "banner my_font_medium",
				textdata : data.string.market2,
			},
			{
				imagediv : "split_3 splits",
				imgsrc : imgpath + "mall_3.png",
				textclass : "banner my_font_medium",
				textdata : data.string.market3,
			},
			{
				imagediv : "split_4 splits",
				imgsrc : imgpath + "mall_4.png",
				textclass : "banner my_font_medium",
				textdata : data.string.market4,
		}],
	},
	//slide2
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "block_cover",
		uppertextblock : [{
		}],
		
		extratextblock:[{
			textdata : data.string.choose2,
			textclass : 'my_font_ultra_big choose-shop-text'
		}],
		
		imagetextblock : [{
				imagediv : "shop_1 shop",
				imgsrc : imgpath + "shops/shop_1/c_shop.png",
				textclass : "my_font_medium",
				textdata : data.string.market2,
			},
			{
				imagediv : "shop_2 shop",
				imgsrc : imgpath + "shops/shop_1/s_shop.png",
				textclass : "my_font_medium",
				textdata : data.string.market2,
			},
			{
				imagediv : "shop_3 shop",
				imgsrc : imgpath + "shops/shop_1/t_shop.png",
				textclass : "my_font_medium",
				textdata : data.string.market2,
			},
			{
				imagediv : "shop_4 shop",
				imgsrc : imgpath + "shops/shop_1/f_shop.png",
				textclass : "my_font_medium",
				textdata : data.string.market2,
		}],
	},
	//slide3 clothes
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "shop_name_board my_font_big top_text",
		uppertextblock : [
		{
			textdata : data.string.shop5_name,
			textclass : 'shop_name_text'
		},
		{
			textdata : data.string.pxtext4,
			textclass : 'take_bill',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		],
		lowertextblockadditionalclass : "done_shopping my_font_big front_image",
		lowertextblock : [
		{
			textdata : data.string.done_shopping,
			textclass : '',
		}],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top_clothing shelf_top",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom_clothing shelf_bottom",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "black_image",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover clothing_shop',
		shoppingblock : [
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s4_item_1,
				imgsrc : imgpath + "clothes01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 2000'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s4_item_1,
				imgsrc : imgpath + "clothes01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 2000'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s4_item_2,
				imgsrc : imgpath + "clothes02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s4_item_2,
				imgsrc : imgpath + "clothes02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s4_item_2,
				imgsrc : imgpath + "clothes02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s4_item_3,
				imgsrc : imgpath + "clothes03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 800'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s4_item_3,
				imgsrc : imgpath + "clothes03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 800'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s4_item_3,
				imgsrc : imgpath + "clothes03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 800'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s4_item_4,
				imgsrc : imgpath + "clothes04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 900'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s4_item_4,
				imgsrc : imgpath + "clothes04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 900'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s4_item_5,
				imgsrc : imgpath + "clothes05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s4_item_5,
				imgsrc : imgpath + "clothes05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s4_item_5,
				imgsrc : imgpath + "clothes05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s4_item_6,
				imgsrc : imgpath + "clothes06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 1200'
			},
			{
				clothshopping: true,
				hangersrc: imgpath + "hanger.png",
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s4_item_6,
				imgsrc : imgpath + "clothes06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 1200'
			}
		],
		infolabel:[
			{
				inputclass: 'user_input my_font_medium input_name',
				textdata : data.string.pxtext2,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			},
			{
				inputclass: 'user_input my_font_medium input_address',
				textdata : data.string.pxtext3,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			}
		],
		bill_item:[{
			bill_container: 'bill_center slide_from_top bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
	//slide4 stationery
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "shop_name_board my_font_big top_text",
		uppertextblock : [
			{
				textdata : data.string.shop5_name,
				textclass : 'shop_name_text'
			},
		],
		definitionblockadditionalclass:'take_bill_div my_font_medium',
		definitionblock:[
			{
				textdata : data.string.pxtext4,
				textclass : 'take_bill',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_bill'
			}
		],
		lowertextblockadditionalclass : "done_shopping my_font_big front_image",
		lowertextblock : [
		{
			textdata : data.string.done_shopping,
			textclass : '',
		}],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "black_image",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_1,
				imgsrc : imgpath + "s_item_1.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_1,
				imgsrc : imgpath + "s_item_2.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_1,
				imgsrc : imgpath + "s_item_3.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s5_item_1,
				imgsrc : imgpath + "s_item_4.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 25'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s5_item_2,
				imgsrc : imgpath + "s_item_6.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 50'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s5_item_2,
				imgsrc : imgpath + "s_item_6.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 50'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s5_item_3,
				imgsrc : imgpath + "s_item_7.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 150'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s5_item_3,
				imgsrc : imgpath + "s_item_7.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 150'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s5_item_4,
				imgsrc : imgpath + "s_item_5.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 80'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s5_item_4,
				imgsrc : imgpath + "s_item_5.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 80'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s5_item_5,
				imgsrc : imgpath + "s_item_8.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 70'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s5_item_5,
				imgsrc : imgpath + "s_item_8.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 70'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s5_item_5,
				imgsrc : imgpath + "s_item_9.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 70'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s5_item_6,
				imgsrc : imgpath + "s_item_10.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s5_item_6,
				imgsrc : imgpath + "s_item_10.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 40'
			},
		],
		infolabel:[
			{
				inputclass: 'user_input my_font_medium input_name',
				textdata : data.string.pxtext2,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			},
			{
				inputclass: 'user_input my_font_medium input_address',
				textdata : data.string.pxtext3,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			}
		],
		bill_item:[{
			bill_container: 'bill_center slide_from_top bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
	//slide5 toy
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "shop_name_board my_font_big top_text",
		uppertextblock : [
			{
				textdata : data.string.shop5_name,
				textclass : 'shop_name_text'
			},
		],
		definitionblockadditionalclass:'take_bill_div my_font_medium',
		definitionblock:[
			{
				textdata : data.string.pxtext4,
				textclass : 'take_bill',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_bill'
			}
		],
		lowertextblockadditionalclass : "done_shopping my_font_big front_image",
		lowertextblock : [
		{
			textdata : data.string.done_shopping,
			textclass : '',
		}],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "black_image",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 200'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 200'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s2_item_1,
				imgsrc : imgpath + "toy01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 200'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_2,
				imgsrc : imgpath + "toy02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_2,
				imgsrc : imgpath + "toy02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 400'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_3,
				imgsrc : imgpath + "toy03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 300'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s2_item_3,
				imgsrc : imgpath + "toy03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 300'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_4,
				imgsrc : imgpath + "toy04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 450'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_4,
				imgsrc : imgpath + "toy04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 450'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_5,
				imgsrc : imgpath + "toy06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 150'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s2_item_5,
				imgsrc : imgpath + "toy06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 150'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 350'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 350'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s2_item_6,
				imgsrc : imgpath + "toy05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 350'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s2_item_7,
				imgsrc : imgpath + "toy07.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 350'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s2_item_7,
				imgsrc : imgpath + "toy07.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 350'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s2_item_8,
				imgsrc : imgpath + "toy08.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 250'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s2_item_8,
				imgsrc : imgpath + "toy08.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 250'
			},
		],
		infolabel:[
			{
				inputclass: 'user_input my_font_medium input_name',
				textdata : data.string.pxtext2,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			},
			{
				inputclass: 'user_input my_font_medium input_address',
				textdata : data.string.pxtext3,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			}
		],
		bill_item:[{
			bill_container: 'bill_center slide_from_top bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
	//slide6 flower
	{
		contentblockadditionalclass: '',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "shop_name_board my_font_big top_text",
		uppertextblock : [
			{
				textdata : data.string.shop5_name,
				textclass : 'shop_name_text'
			},
		],
		definitionblockadditionalclass:'take_bill_div my_font_medium',
		definitionblock:[
			{
				textdata : data.string.pxtext4,
				textclass : 'take_bill',
				datahighlightflag : true,
				datahighlightcustomclass : 'highlight_bill'
			}
		],
		
		lowertextblockadditionalclass : "done_shopping my_font_big front_image",
		lowertextblock : [
		{
			textdata : data.string.done_shopping,
			textclass : '',
		}],
		imageblockadditionalclass: 'block_cover',
		imageblock : [{
			imagelabels: [{
				imagelabelclass : "shelf_top ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "shelf_bottom ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "floor ",
				imagelabeldata : '',
			},
			{
				imagelabelclass : "black_image",
				imagelabeldata : '',
			}]
		}],
		basketblockadditionalclass: 'basket',
		basketblock:[{
			imgclass : "basket_front",
			imgsrc : imgpath + "basket_front.png",
		},
		{
			imgclass : "basket_back",
			imgsrc : imgpath + "basketback.png",
		}],
		shoppingblockadditionalclass: 'block_cover',
		shoppingblock : [
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s1_item_1,
				imgsrc : imgpath + "flower01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 700'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s1_item_1,
				imgsrc : imgpath + "flower01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 700'
			},
			{
				imgclass : "shop_items shop_item_1 top_shelf",
				item_name : data.string.s1_item_1,
				imgsrc : imgpath + "flower01.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 700'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s1_item_2,
				imgsrc : imgpath + "flower02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				imgclass : "shop_items shop_item_2 top_shelf",
				item_name : data.string.s1_item_2,
				imgsrc : imgpath + "flower02.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 500'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s1_item_3,
				imgsrc : imgpath + "flower03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 650'
			},
			{
				imgclass : "shop_items shop_item_3 top_shelf",
				item_name : data.string.s1_item_3,
				imgsrc : imgpath + "flower03.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 650'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s1_item_4,
				imgsrc : imgpath + "flower04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 550'
			},
			{
				imgclass : "shop_items shop_item_4 bottom_shelf",
				item_name : data.string.s1_item_4,
				imgsrc : imgpath + "flower04.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 550'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s1_item_5,
				imgsrc : imgpath + "flower05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 450'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s1_item_5,
				imgsrc : imgpath + "flower05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 450'
			},
			{
				imgclass : "shop_items shop_item_5 bottom_shelf",
				item_name : data.string.s1_item_5,
				imgsrc : imgpath + "flower05.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 450'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s1_item_6,
				imgsrc : imgpath + "flower06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 600'
			},
			{
				imgclass : "shop_items shop_item_6 bottom_shelf",
				item_name : data.string.s1_item_6,
				imgsrc : imgpath + "flower06.png",
				imagelabelclass : "price_tag my_font_small",
				imagelabeldata : 'Rs 600'
			},
		],
		infolabel:[
			{
				inputclass: 'user_input my_font_medium input_name',
				textdata : data.string.pxtext2,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			},
			{
				inputclass: 'user_input my_font_medium input_address',
				textdata : data.string.pxtext3,
				submit: data.string.sbmttext,
				textclass : '',
				infolabelclass: '',
				infolabeldata: '',
			}
		],
		bill_item:[{
			bill_container: 'bill_center slide_from_top bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
	//slide 7 questions
	{
		contentblockadditionalclass: 'ole-background-gradient-apple',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "reasonings",
		uppertextblock : [
		{
			textdata : data.string.p2text2,
			textclass : 'my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'my_budget'
		},
		{
			textdata : data.string.p2text3,
			textclass : 'my_font_big'
		},
		{
			textdata : data.string.p2text4,
			textclass : 'my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'special_highlight'
		}],	
		
		imagetextblock:[
			{
				imagediv : "par_div par_1",
				imgsrc : imgpath + "par_1.png",
				textclass : "feedback_rep my_font_very_big",
				textdata : data.string.p2text16,
			},
			{
				imagediv : "par_div par_2",
				imgsrc : imgpath + "par_2.png",
				textclass : "feedback_rep my_font_very_big",
				textdata : data.string.p2text17,
			},
			{
				imagediv : "par_div par_3",
				imgsrc : imgpath + "par_3.png",
				textclass : "feedback_rep my_font_very_big",
				textdata : data.string.p2text18,
			},
		],
		
		lowertextblockadditionalclass : "what_will_do my_font_medium",
		lowertextblock : [
		{
			textdata : data.string.p2text5,
			textclass : '',
		},
		{
			textdata : data.string.p2text6,
			textclass : 'opt_1 options',
		},
		{
			textdata : data.string.p2text7,
			textclass : 'opt_2 options',
		},
		{
			textdata : data.string.p2text8,
			textclass : 'opt_3 options',
		},
		],	
		
		bill_item:[{
			bill_container: 'bill_right slide_from_right',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
	//slide8 DIY
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
	
		contentblockadditionalclass: 'ole-background-gradient-apple',
        uppertextblockadditionalclass:"diytext",
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : ''
		}],
        imagetextblock:[
            {
                imgclass : "diyimage",
                imgsrc : "images/diy_bg/a_07.png",
            }
			]
	},
	//slide 9
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "questions_div my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p2text14,
			textclass : 'question',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "80",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "360",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "100",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "550",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center bill_left',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop1_name,
			shop_address: data.string.shop1_address,
			customer_name:'Ramesh Paudel',
			customer_address:'Traffic Chowk',
			customer_bill: '318',
			customer_date: '12/31',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop1_owner
		}],
	},
	//slide 10
	{
		contentblockadditionalclass: 'main_bg',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "questions_div my_font_medium",
		uppertextblock : [
		{
			textdata : data.string.p2text15,
			textclass : 'question',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "80",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "360",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "100",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		},
		{
			textdata : "550",
			textclass : 'answer',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bill'
		}],
		bill_item:[{
			bill_container: 'bill_center bill_left',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop1_name,
			shop_address: data.string.shop1_address,
			customer_name:'Ramesh Paudel',
			customer_address:'Traffic Chowk',
			customer_bill: '318',
			customer_date: '12/31',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop1_owner
		}],
	},
];



$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

		
	var go_to_next_slide = 0;
	var  global_save_val = '';
	var total_budget = 0;
	
	var shopped_item = new Map();
	var jump_to_shop = 0;
	
	var current_mall = 0;
	var current_shop = 0;
	
	var user_name = '';
	var shop_logo = '';
	var user_address = '';
	var shop_loaction = '';
	var shop_type = '';
	// var rand_bill_no = ole.getRandom(1, 9999, 1000)[0];
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s2_p1_1.ogg"},
            {id: "sound_2", src: soundAsset + "s2_p1_2.ogg"},
            {id: "sound_3", src: soundAsset + "s2_p1_3.ogg"},
            {id: "sound_name", src: soundAsset + "name.ogg"},
            {id: "sound_address", src: soundAsset + "addr.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
				sound_player("sound_1",false);

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				sound_player("sound_1",false);
				$('.options_budget>p').click(function(){
					$('.options_budget>p').removeClass('selected_budget');
					$(this).addClass('selected_budget');
					total_budget = parseInt($(this).html().split(' ')[1]);
					$nextBtn.show(0);
				});
				break;
			case 1:
                sound_player("sound_2",false);
                $('.splits').click(function(){
					$('.splits').css('pointer-events','none');
					if($(this).hasClass('split_1')){
						current_mall = 0;
						shop_loaction = data.string.mall_1_location;
					} else if($(this).hasClass('split_2')){
						current_mall = 1;
						shop_loaction = data.string.mall_2_location;
					}
					else if($(this).hasClass('split_3')){
						current_mall = 2;
						shop_loaction = data.string.mall_3_location;
					} else {
						current_mall = 3;
						shop_loaction = data.string.mall_4_location;
					}
					$(this).css('z-index','40');
					$('.contentblock').addClass('gray_out1');
					$('.banner').hide(0);
					$('.splits').addClass('gray_out');
					$(this).addClass('zoom_in_mid');
					$prevBtn.hide(0);
					setTimeout(function(){
						$nextBtn.trigger("click");
					}, 2000);
				});
				break;
			case 2:
				sound_player("sound_3",false);
				var new_idx_1 = current_mall+1;
				$('.block_cover').addClass('bg_place_'+new_idx_1);
				$('.shop_1>p').html(data.string.shopat+' '+shop_loaction+' '+data.string.shop_1);
				$('.shop_1>img').attr('src', 'activity/grade5/math/bill_budget/images/shops/shop_'+new_idx_1+'/c_shop.png');
				$('.shop_2>p').html(data.string.shopat+' '+shop_loaction+' '+data.string.shop_2);
				$('.shop_2>img').attr('src', 'activity/grade5/math/bill_budget/images/shops/shop_'+new_idx_1+'/s_shop.png');
				$('.shop_3>p').html(data.string.shopat+' '+shop_loaction+' '+data.string.shop_3);
				$('.shop_3>img').attr('src', 'activity/grade5/math/bill_budget/images/shops/shop_'+new_idx_1+'/t_shop.png');
				$('.shop_4>p').html(data.string.shopat+' '+shop_loaction+' '+data.string.shop_4);
				$('.shop_4>img').attr('src', 'activity/grade5/math/bill_budget/images/shops/shop_'+new_idx_1+'/f_shop.png');
				$prevBtn.show(0);
				$('.shop').click(function(){
					if($(this).hasClass('shop_1')){
						current_shop = 0;
						shop_type = data.string.shop_type_1;
						shop_logo = 'logo_cloth.png';
						jump_to_shop = 3;
					} else if($(this).hasClass('shop_2')){
						current_shop = 1;
						shop_type = data.string.shop_type_2;
						shop_logo = 'logo_station.png';
						jump_to_shop = 4;
					}
					else if($(this).hasClass('shop_3')){
						current_shop = 2;
						shop_type = data.string.shop_type_3;
						shop_logo = 'logo_toy.png';
						jump_to_shop = 5;
					} else {
						current_shop = 3;
						shop_type = data.string.shop_type_4;
						shop_logo = 'logo_flower.png';
						jump_to_shop = 6;
					}
					$('.shop').addClass('gray_out_2');
					$('.block_cover').addClass('gray_out_2');
					$(this).removeClass('gray_out_2');
					setTimeout(function(){
						$nextBtn.show(0);
						$prevBtn.show(0);
					}, 500);
				});
				break;
			case 3:
			case 4:
			case 5:
			case 6:
				items = [];
				shopped_item.clear();
				set_shop_color(current_mall, current_shop);
				go_to_next_slide = 0;
				var item_count = 0;
				arrage_items_in_shelf();
				$(".sbmtbtn").click(function(event) {
					$nextBtn.trigger("click");
				});
				$('.shop_items').click(function(){
					item_count++;setTimeout(function(){
						$('.done_shopping').show(0);
						$(".sbmtbtn").hide(0);
					}, 2000);
					var item_cost = parseInt($(this).find('label').text().split(" ")[1]);
					add_to_map($(this).data('name'), item_cost);
					$(this).css('pointer-events','none');
					var item_basket_y = 55-13*(Math.floor(item_count/5));
					var item_basket_x = 8+12*(item_count%5);
					$(this).fadeOut(500, function(){
						$(this).detach().css({
							'width': '20%',
							'z-index': '10',
							'top': item_basket_y+'%',
							'left': item_basket_x+'%',
							}).appendTo($('.basket'));
						$(this).fadeIn(500);
					});
				});
				$('.done_shopping').click(function(){
					$('.done_shopping').css({'pointer-events': 'none', 'opacity': '0'});
					$('.shop_items').css('pointer-events', 'none');
					$('.input_name').show(0);
					sound_player("sound_name",false);
					$('.black_image').fadeIn(500);
				});
				input_box('.input_name', $nextBtn);
				input_box('.input_address', $nextBtn);
				function add_to_map(key, price){
					var key_val_1 = 1;
					if(shopped_item.has(key)){
						key_val_1 +=  shopped_item.get(key)[0];
					}
					shopped_item.set(key, [key_val_1, price]);
				}
				$prevBtn.show(0);
				break;
			case 7:
				$prevBtn.show(0);
				var my_bill = new bill();
				var return_amt = 0;
				items = [];
				my_bill.reset_bill_items();
				my_bill.init(imgpath + "shop_bill/"+shop_logo, shop_loaction+' '+shop_type, shop_loaction, user_name, user_address, 'bill_container');
				shopped_item.forEach(arrayElements);
				my_bill.add_items(items);
				my_bill.set_shopkeeper_name(shop_colors[current_mall][current_shop][6]);
				my_bill.set_bill_sign(imgpath + shop_colors[current_mall][current_shop][7]);
				my_bill.update_bill('.bill_center');
				
				return_amt = total_budget - my_bill.get_total_amount();			
				$('.my_budget').html(total_budget);
				$('.special_highlight:first-child').html(total_budget);
				$('.special_highlight:nth-child(2)').html(my_bill.get_total_amount());
				$('.special_highlight:last-child').html(return_amt);
			  function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(0): "";
        });
    }

				$('.options').click(function(){
					if($(this).hasClass('opt_1')){
						$(this).addClass('right_selected');
						$('.options').css('pointer-events', 'none');
						$('.par_1').show(0);
						$nextBtn.show(0);
					} else if($(this).hasClass('opt_2')){
						$(this).addClass('right_selected');
						$('.options').css('pointer-events', 'none');
						$('.par_2').show(0);
						$nextBtn.show(0);
					} else {
						$(this).addClass('right_selected');
						$('.options').css('pointer-events', 'none');
						$('.par_3').show(0);
						$nextBtn.show(0);
					}
				});
				break;
			case 8:
				play_diy_audio();
                $nextBtn.show(0);
                break;
			case 9:
				var my_bill = new bill();
				my_bill.reset_bill_items();
				my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.shop4_name, data.string.shop4_address, data.string.ecustomer_name_3, data.string.customer_eaddress_3, 'bill_container');
				var new_items = [new bill_element(data.string.p2text10, 2, 700), new bill_element(data.string.p2text11, 3, 900), new bill_element(data.string.p2text12, 1, 400), new bill_element(data.string.p2text13, 1, 150)];
				my_bill.set_shopkeeper_name(data.string.shop4_owner);
				my_bill.add_items(new_items);
				my_bill.update_bill('.bill_center');
				$prevBtn.show(0);
				var my_ans_1 = Math.ceil(Math.random()*91)*50;
				var my_ans_2 = Math.ceil(Math.random()*91)*50;
				var my_ans_3 = Math.ceil(Math.random()*91)*50;
				while(my_ans_2==my_ans_1){
					my_ans_2 = Math.ceil(Math.random()*91)*50;
				}
				while(my_ans_2==my_ans_3 || my_ans_3==my_ans_1){
					my_ans_2 = Math.ceil(Math.random()*91)*50;
				}
				var answer_arr = [my_ans_1, my_ans_2, my_ans_3, 7000];
				answer_arr.shufflearray();
				for(var m=0; m<4; m++){
					if(answer_arr[m]==7000){
						$('.answer').eq(m).html(answer_arr[m]);
						$('.answer').eq(m).addClass('correct_ans');
					} else{
						$('.answer').eq(m).html(answer_arr[m]);
					}
				}
				$('.answer').click(function(){
					$(this).addClass('selected');
					if($(this).hasClass('correct_ans')){
						play_correct_incorrect_sound(1);
						$('.answer').css('pointer-events','none');
						$nextBtn.show(0);
						// ole.footerNotificationHandler.lessonEndSetNotification();
					} else{
                        play_correct_incorrect_sound(0);
                        $(this).css('pointer-events','none');
					}
				});
				break;
			case 10:
				var my_bill = new bill();
				my_bill.reset_bill_items();
				my_bill.init(imgpath + "shop_bill/logo_cloth.png", data.string.shop4_name, data.string.shop4_address, data.string.ecustomer_name_3, data.string.ecustomer_address_3, 'bill_container');
				var new_items = [new bill_element(data.string.p2text10, 2, 700), new bill_element(data.string.p2text11, 3, 900), new bill_element(data.string.p2text12, 1, 400), new bill_element(data.string.p2text13, 1, 150)];
				my_bill.set_shopkeeper_name(data.string.shop4_owner);
				my_bill.add_items(new_items);
				my_bill.update_bill('.bill_center');
				$prevBtn.show(0);
				var my_ans_1 = Math.ceil(Math.random()*139)*50;
				var my_ans_2 = Math.ceil(Math.random()*139)*50;
				var my_ans_3 = Math.ceil(Math.random()*139)*50;
				while(my_ans_1==87){
					my_ans_1 = Math.ceil(Math.random()*139)*50;
				}
				while(my_ans_2==my_ans_1 || my_ans_2==87){
					my_ans_2 = Math.ceil(Math.random()*139)*50;
				}
				while(my_ans_2==my_ans_3 || my_ans_3==my_ans_1 || my_ans_3==87){
					my_ans_3 = Math.ceil(Math.random()*139)*50;
				}
				var answer_arr = [my_ans_1, my_ans_2, my_ans_3, 4350];
				answer_arr.shufflearray();
				for(var m=0; m<4; m++){
					if(answer_arr[m]==4350){
						$('.answer').eq(m).html(answer_arr[m]);
						$('.answer').eq(m).addClass('correct_ans');
					} else{
						$('.answer').eq(m).html(answer_arr[m]);
					}
				}
				$('.answer').click(function(){
					$(this).addClass('selected');
					if($(this).hasClass('correct_ans')){
						$('.answer').css('pointer-events','none');
                        play_correct_incorrect_sound(1);
                        ole.footerNotificationHandler.lessonEndSetNotification();
					} else {
                        play_correct_incorrect_sound(0);
                        $(this).css('pointer-events','none');
					}
				});
				break;
			default:
				nav_button_controls(500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(0): "";
        });
    }


    function arrage_items_in_shelf(){
		for(var i=1; i<7; i++){
			var shelf_array = document.getElementsByClassName('shop_item_'+i);
			var total_in_array = shelf_array.length;
			for(var m=0; m<shelf_array.length; m++){
				var left_position = $('.shop_item_'+i).eq(0).position().left+m*$board.width()*0.06;
				$('.shop_item_'+i).eq(m).css({
					'left': left_position,
					'z-index':1 + (total_in_array-m)*1
				});
				// shelf_array[m].style.left = 1 + (total_in_array-m)*1;
			}
		}
	}
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
            // var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
            // //check if user inputs del, shift, caps , backspace or arrow keys
   			// if (!condition) {
    		// 	return true;
    		// }
    		// //check if user inputs more than one '.'
            // if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		// return false;
    		// }
    		// //check . and 0-9 separately after checking arrow and other keys
    		// if((charCode < 65 || charCode > 90)){
    		// 	return false;
    		// }
  			return true;
		});	
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});	
	}
	function set_shop_color(location, shop){
		$('.shop_name_board').css({
			'background-color': shop_colors[location][shop][0],
			'border-color': shop_colors[location][shop][1]
		});
		$('.shop_name_board').html(shop_loaction+' '+shop_type);
		$('.contentblock').css({
			'background-color': shop_colors[location][shop][2]
		});
		$('.floor').css({
			'background-color': shop_colors[location][shop][3]
		});
		$('.shelf_top, .shelf_bottom').css({
			'background-color': shop_colors[location][shop][4],
			'border-color': shop_colors[location][shop][5]
		});
	}
	$( window ).resize(function() {
		arrage_items_in_shelf();
	});

	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
        loadTimelineProgress($total_page, countNext + 1);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			case 2:
				countNext = jump_to_shop;
				templateCaller();
				break;
			case 3:
			case 4:
			case 5:
			case 6:
				if(go_to_next_slide==0){
					user_name = global_save_val;
					$('.input_name').hide(0);
					$(".sbmtbtn").hide(0);
					$('.input_address').show(0);
					sound_player("sound_address",false);
					$nextBtn.hide(0);
					go_to_next_slide++;
				} else if (go_to_next_slide==1){
					user_address = global_save_val;
					$nextBtn.hide(0);
					$('.input_address').hide(0);
					$('.take_bill_div').show(0);
					$('.shop_name_text').hide(0);
					var my_bill = new bill();
					my_bill.reset_bill_items();
					my_bill.init(imgpath + "shop_bill/"+shop_logo, shop_loaction+' '+shop_type, shop_loaction, user_name, user_address, 'bill_container');
					shopped_item.forEach(arrayElements);
					my_bill.add_items(items);
					my_bill.set_shopkeeper_name(shop_colors[current_mall][current_shop][6]);
					my_bill.set_bill_sign(imgpath + shop_colors[current_mall][current_shop][7]);
					my_bill.update_bill('.bill_center');
					$('.bill_hidden').show(0);
					go_to_next_slide++;
					$nextBtn.show(0);
				} else{
					countNext=7;
					templateCaller();
				}
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});
	var items = [];
	function arrayElements(value, key, map) {
		items.push(new bill_element(key, value[0], value[1]));
	}

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		switch(countNext){
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
				countNext=2;
				templateCaller();
				break;
			default:
				countNext--;
				templateCaller();
				break;
		}
	});

	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

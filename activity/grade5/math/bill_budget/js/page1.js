var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'main-bg',
	
		uppertextblockadditionalclass : "lesson-title vertical-horizontal-center",
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : ''
		}]
	},
	
	//slide1
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "definition_text",
		uppertextblock : [
		{
			textdata : data.string.pbudget,
			textclass : 'my_font_ultra_big title_font',
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big desc_font',
		}],	
	},
	//slide2
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "definition_text",
		uppertextblock : [
		{
			textdata : data.string.pbudget,
			textclass : 'my_font_ultra_big title_font',
		},
		{
			textdata : data.string.p1text1,
			textclass : 'my_font_big desc_font',
		}],	
		imagetextblock: [
			{
				imagediv: 'pencil_div img_tag',
				imgclass: '',
				imgsrc: imgpath + "pencil-p1-1.png",
				textclass : "my_font_medium",
				textdata : data.string.p1text2,
			},
			{
				imagediv: 'ruler_div img_tag',
				imgclass: '',
				imgsrc: imgpath + "ruler-p1.png",
				textclass : "my_font_medium",
				textdata : data.string.p1text3,
			},
			{
				imagediv: 'compass_div img_tag',
				imgclass: 'compass',
				imgsrc: imgpath + "compass-p1.png",
				textclass : "my_font_medium",
				textdata : data.string.p1text4,
			},
		],
		extratextblock:[{
			textdata : data.string.p1text5,
			textclass : 'bottom_text_1 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'highlight_bottom'
		}]
	},
	//slide3
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "definition_text_2",
		uppertextblock : [
		{
			textdata : data.string.pbudget,
			textclass : 'my_font_ultra_big title_font',
		},
		{
			textdata : data.string.p1text6,
			textclass : 'my_font_big desc_font_2',
		}],	
	},
	//slide4
	{
		contentblockadditionalclass: 'ole-background-gradient-turqiose',
		contentblocknocenteradjust : true,
		
		uppertextblockadditionalclass : "definition_text_2",
		uppertextblock : [
		{
			textdata : data.string.pbudget,
			textclass : 'my_font_ultra_big title_font',
		},
		{
			textdata : data.string.p1text6,
			textclass : 'my_font_big desc_font_2',
		}],	
		extratextblock:[{
			textdata : data.string.p1text7,
			textclass : 'question_text my_font_big fade_in',
		},
		{
			textdata : data.string.p1text8,
			textclass : 'fb_text my_font_medium',
			datahighlightflag : true,
			datahighlightcustomclass : 'return_amt'
		}
		],
		
		infolabel:[{
			inputclass: 'info_div fade_in',
			textdata : data.string.psubmit,
			textclass : 'submit_button my_font_very_big',
			infolabelclass : 'amount_enter'
		}],
		
		bill_item:[{
			bill_container: 'bill_right slide_from_right bill_hidden',
			shop_logo: imgpath + "shop_bill/logo_1.svg",
			shop_name: data.string.shop2_name,
			shop_address: data.string.shop2_address,
			customer_name:'Mark',
			customer_address:'USA',
			customer_bill: '300',
			customer_date: '12/12',
			signature: imgpath + "shop_bill/sign.png",
			shopkeeper_name: data.string.shop2_owner
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	
	loadTimelineProgress($total_page, countNext + 1);

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();


	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
		case 0:
            sound_player("sound_1",true);
			break;
			case 1:
                sound_player("sound_2",true);
			break;
            case 2:
                sound_player("sound_3",true);
                $('.img_tag').eq(0).hide().fadeIn(1000, function(){
				setTimeout(function(){
					$('.img_tag').eq(1).fadeIn(1000, function(){
						setTimeout(function(){
							$('.img_tag').eq(2).fadeIn(1000, function(){
								setTimeout(function(){
									$('.bottom_text_1').fadeIn(1000);
								},2000);
							});
						}, 2000);
					});
				}, 2000);
			});
			break;
			case 3:
                sound_player("sound_4",true);
                break;
            case 4:
            	setTimeout(function(){
                    sound_player("sound_5",true);
				},2000);
            var my_bill = new bill();
			var return_amt = 0;
			$prevBtn.show(0);
			input_box('.amount_enter', 3, '.submit_button');
			my_bill.reset_bill_items();
			my_bill.init(imgpath + "shop_bill/logo_book.png", data.string.s1text1, data.string.s1text2, data.string.s1text3, data.string.s1text4, 'bill_container');
			var items = [new bill_element(data.string.s1text5, 1, 5), new bill_element(data.string.s1text6, 1, 10), new bill_element(data.string.s1text7, 1, 20)];
			my_bill.add_items(items);
			my_bill.set_shopkeeper_name(data.string.s1text8);
			my_bill.update_bill('.bill_container');
			
			$('.submit_button').click(function(){
				if(parseInt($('.amount_enter').val())>=35){
					return_amt = parseInt($('.amount_enter').val())-35;
					$('.submit_button').css('background-color', '#93C47D');
					$('.amount_enter').prop( "disabled", true );
					$('.submit_button').addClass('correct');
					$('.fb_text').html(data.string.p1text9);
					texthighlight($board);
					$('.return_amt').html(return_amt);
					$('.fb_text').show(0);
					$nextBtn.show(0);
				} else {
					$('.submit_button').css('background-color', '#EA9999');
					$('.fb_text').show(0);
				}
			});
			break;
		default:
			nav_button_controls(500);
			break;
		}
	}
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? nav_button_controls(0): "";
        });
    }

    function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});	
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	$nextBtn.on("click", function() {
		switch(countNext){
			case 4:
				$nextBtn.hide(0);
				$('.coverboardfull>*:not(.definition_text_2)').fadeOut(1000, function(){
					setTimeout(function(){
						$('.desc_font_2').addClass('change_bg');
						ole.footerNotificationHandler.pageEndSetNotification();
					}, 500);
				});
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var imgpath = $ref + "/images/";

var content = [
  // slide17
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bgairport",
            imgsrc: "",
            imgid: "attheairport"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog11",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text22
      }
    ]
  },

  // slide18
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg2",
    extratextblock: [
      {
        textdata: data.string.p2text24,
        textclass: "time1",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog12",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text23
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "dilak",
            imgsrc: "",
            imgid: "dilak"
          },
          {
            imgclass: "clocknew",
            imgsrc: "",
            imgid: "clocknew"
          },
          {
            imgclass: "hour",
            imgsrc: "",
            imgid: "hour"
          },
          {
            imgclass: "minute",
            imgsrc: "",
            imgid: "minute"
          }
        ]
      }
    ]
  },

  // slide19
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg2",
    extratextblock: [
      {
        textdata: data.string.p2text24,
        textclass: "time1",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text26,
        textclass: "time2",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog12",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text25
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "dilak",
            imgsrc: "",
            imgid: "dilak"
          },
          {
            imgclass: "clocknew",
            imgsrc: "",
            imgid: "clocknew"
          },
          {
            imgclass: "hour",
            imgsrc: "",
            imgid: "hour"
          },
          {
            imgclass: "minute",
            imgsrc: "",
            imgid: "minute"
          },
          {
            imgclass: "clocknew1",
            imgsrc: "",
            imgid: "clocknew"
          },
          {
            imgclass: "hour1",
            imgsrc: "",
            imgid: "hour"
          },
          {
            imgclass: "minute1",
            imgsrc: "",
            imgid: "minute"
          }
        ]
      }
    ]
  },

  // slide20
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg2",
    extratextblock: [
      {
        textdata: data.string.p2text24,
        textclass: "time1",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text26,
        textclass: "time2",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog12",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text27
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "dilak",
            imgsrc: "",
            imgid: "dilak"
          },
          {
            imgclass: "clocknew",
            imgsrc: "",
            imgid: "clocknew"
          },
          {
            imgclass: "hour",
            imgsrc: "",
            imgid: "hour"
          },
          {
            imgclass: "minute",
            imgsrc: "",
            imgid: "minute"
          },
          {
            imgclass: "clocknew1",
            imgsrc: "",
            imgid: "clocknew"
          },
          {
            imgclass: "hour1",
            imgsrc: "",
            imgid: "hour"
          },
          {
            imgclass: "minute1",
            imgsrc: "",
            imgid: "minute"
          }
        ]
      }
    ]
  },

  // slide21
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog8",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox5",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text28
      }
    ]
  },

  // slide22
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text29,
        textclass: "information",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy3",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog9 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text30
      }
    ]
  },

  // slide23
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text31,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text32,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          },
          {
            imgclass: "fairy4",
            imgsrc: "",
            imgid: "fairy"
          },
         
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog10 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text33
      }
    ]
  },

  // slide24
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text31,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text32,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          },
          {
            imgclass: "fairy4",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog10 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text34
      }
    ]
  },

  // slide25
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text31,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text32,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          },
          {
            imgclass: "fairy4",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog10 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text35
      }
    ]
  },

  // slide26
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text36,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text37,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          },
          {
            imgclass: "fairy4",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog10 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text38
      }
    ]
  },

  // slide27
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text39,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text40,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text41,
        textclass: "step13",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          }
        ]
      }
    ]
  },

  // slide28
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text39,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text42,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text43,
        textclass: "step13",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          }
        ]
      }
    ]
  },

  // slide29
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg11",
    extratextblock: [
      {
        textdata: data.string.p2text39,
        textclass: "step11",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text44,
        textclass: "step12",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text45,
        textclass: "step13",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text46,
        textclass: "step14",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text47,
        textclass: "step15",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "clocknew3",
            imgsrc: "",
            imgid: "clock1"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images

      {
        id: "bg1",
        src: imgpath + "bg_roshan02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg2",
        src: imgpath + "bg_roshan03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg3",
        src: imgpath + "bg_roshan04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg4",
        src: imgpath + "bg_roshan05.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "fairy",
        src: imgpath + "flying-chibi01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan1",
        src: imgpath + "roshan03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan2",
        src: imgpath + "roshan04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan3",
        src: imgpath + "roshan02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan4",
        src: imgpath + "roshan05.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dad",
        src: imgpath + "dad.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mum",
        src: imgpath + "mum.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "twentyrs",
        src: imgpath + "twenty-rupees_note.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "fivers",
        src: imgpath + "five-rupees_note.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "oners",
        src: imgpath + "one_rupee_coin.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "twors",
        src: imgpath + "two_rupee_coin.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "attheairport",
        src: imgpath + "at_the_airport.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dilak",
        src: imgpath + "dilak.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clocknew",
        src: imgpath + "clock_new.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clock1",
        src: imgpath + "clock1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hour",
        src: imgpath + "hour.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "minute",
        src: imgpath + "minute.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "textbox1",
        src: imgpath + "text_box.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox2",
        src: imgpath + "text_box01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox3",
        src: imgpath + "text_box02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox4",
        src: imgpath + "text_box03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox5",
        src: imgpath + "text_box04.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "sound_1", src: soundAsset + "s3_p1.ogg" },
      { id: "sound_2", src: soundAsset + "s3_p2.ogg" },
      { id: "sound_3", src: soundAsset + "s3_p3.ogg" },
      { id: "sound_4", src: soundAsset + "s3_p4.ogg" },
      { id: "sound_5", src: soundAsset + "s3_p5.ogg" },
      { id: "sound_5_2", src: soundAsset + "s3_p5-2.ogg" },
      { id: "sound_6", src: soundAsset + "s3_p6.ogg" },
      { id: "sound_7", src: soundAsset + "s3_p7.ogg" },
      { id: "sound_7_2", src: soundAsset + "s3_p7(2).ogg" },
      { id: "sound_8", src: soundAsset + "s3_p8.ogg" },
      { id: "sound_9", src: soundAsset + "s3_p9.ogg" },
      { id: "sound_10", src: soundAsset + "s3_p10.ogg" },
      { id: "sound_10_2", src: soundAsset + "s3_p10-2.ogg" },
      { id: "sound_11", src: soundAsset + "s3_p11.ogg" },
      { id: "sound_12", src: soundAsset + "s3_p12.ogg" },
      { id: "sound_13", src: soundAsset + "s3_p13.ogg" },
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    put_label_image(content, countNext);
    put_card_image(content, countNext);
    vocabcontroller.findwords(countNext);
    $nextBtn.hide(0);
    $(".plus_font").attr("style", "font-family: sniglet !important");

    // ole.footerNotificationHandler.hideNotification();
    // sound_player("sound_"+(countNext+1),true);
    $(
      ".fairy1,.roshan1,.dialog,.dialog4,.step1,.step2,.step3,.step4,.step5,.bottom-option,.instruction,.question,.dialog2,.dialog3,.info,.info1,.info2,.basketfull,.suraj01,.nine_apple,.basket1,.basket2,.basket3,.basket4,.basket5"
    ).hide();
    switch (countNext) {
      case 0:
          sound_player('sound_'+(countNext+1),true);
        $(".textfill").css({ transform: "translate(-50%,9%)" });
        nav_button_controls(100);
        break;

      case 1:
      case 2:
      case 3:
          sound_player('sound_'+(countNext+1),true);
          $(".textfill").css({ transform: "translate(-50%,13%)" });
        nav_button_controls(100);
        break;
      case 4:
          createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_5');
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_5_2');
              current_sound1.play();
              current_sound1.on("complete", function() {
                nav_button_controls(500)
            });
        });
        $(".textfill").css({ left: "49%", transform: "translate(-50%,13%)" });
        nav_button_controls(100);
        break;
      case 5:
          sound_player('sound_'+(countNext+1),true);
          $(".textfill").css({
            transform: "translate(-50%,14%)",
            "font-family": "josfin sans"
          });
          break;
      case 6:
          createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_7');
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_7_2');
              current_sound1.play();
              current_sound1.on("complete", function() {
                nav_button_controls(500)
            });
        });
        $(".textfill").css({
          transform: "translate(-50%,14%)",
          "font-family": "josfin sans"
        });
        break;
      case 7:
      case 8:
          sound_player('sound_'+(countNext+1),true);
          $(".textfill").css({
            transform: "translate(-50%,14%)",
            "font-family": "josfin sans"
          });
          break;
      case 9:
          createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_10');
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_10_2');
              current_sound1.play();
              current_sound1.on("complete", function() {
                nav_button_controls(500)
            });
        });
          $(".textfill").css({
            transform: "translate(-50%,14%)",
            "font-family": "josfin sans"
          });
          break;
      case 10:
          sound_player('sound_'+(countNext+1),true);
          $(".textfill").css({
          transform: "translate(-50%,14%)",
          "font-family": "josfin sans"
        });
        break;
        case 11:
          sound_player('sound_'+(countNext+1),true);
          break;
      case 12:
          sound_player('sound_'+(countNext+1),true);
          ole.footerNotificationHandler.lessonEndSetNotification();
        break;
      default:
        nav_button_controls(1000);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id, navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    if(navigate){
      current_sound.on("complete", function() {
        nav_button_controls(500)
    });
    }
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
    if (content[count].hasOwnProperty("containsdialog")) {
      for (var j = 0; j < content[count].containsdialog.length; j++) {
        var containsdialog = content[count].containsdialog[j];
        console.log("imageblock", imageblock);
        if (containsdialog.hasOwnProperty("dialogueimgid")) {
          var image_src = preload.getResult(containsdialog.dialogueimgid).src;
          //get list of classes
          var classes_list =
            containsdialog.dialogimageclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_label_image(content, count) {
    if (content[count].hasOwnProperty("imageandlabel")) {
      var imageandlabel = content[count].imageandlabel;
      for (var i = 0; i < imageandlabel.length; i++) {
        var image_src = preload.getResult(imageandlabel[i].imgid).src;
        console.log(image_src);
        var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
          sound_player('sound_'+(countNext+1),true);
          var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }

  function put_card_image(content, count) {
    if (content[count].hasOwnProperty("card")) {
      var card = content[count].card;
      for (var i = 0; i < card.length; i++) {
        var image_src = preload.getResult(card[i].imgid).src;
        console.log(image_src);
        var classes_list = card[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }

  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    loadTimelineProgress($total_page, countNext + 1);
    generaltemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var imgpath = $ref + "/images/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    extratextblock: [
      {
        textdata: data.lesson.chapter,
        textclass: "lesson-title"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg1"
          },
          {
            imgclass: "apple_tree",
            imgsrc: "",
            imgid: "apple_tree"
          }
        ]
      }
    ]
  },

  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg2"
          },
          {
            imgclass: "left_char",
            imgsrc: "",
            imgid: "niti"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog1 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p1text1
      }
    ]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    extratextblock: [
      {
        textdata: data.string.p1text2,
        textclass: "info"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg1"
          },
          {
            imgclass: "kid1",
            imgsrc: "",
            imgid: "suraj"
          },
          {
            imgclass: "kid2",
            imgsrc: "",
            imgid: "asha"
          },
          {
            imgclass: "kid3",
            imgsrc: "",
            imgid: "prem"
          },
          {
            imgclass: "kid4",
            imgsrc: "",
            imgid: "rumi"
          },
          {
            imgclass: "kid5",
            imgsrc: "",
            imgid: "sagar"
          },
          {
            imgclass: "kid6",
            imgsrc: "",
            imgid: "suraj01"
          },
          {
            imgclass: "basket1",
            imgsrc: "",
            imgid: "basket_apple"
          },
          {
            imgclass: "basket2",
            imgsrc: "",
            imgid: "basket_apple"
          },
          {
            imgclass: "basket3",
            imgsrc: "",
            imgid: "basket_apple"
          },
          {
            imgclass: "basket4",
            imgsrc: "",
            imgid: "basket_apple"
          },
          {
            imgclass: "basket5",
            imgsrc: "",
            imgid: "basket_apple"
          },
          {
            imgclass: "treetop",
            imgsrc: "",
            imgid: "apple_tree_top"
          },
          {
            imgclass: "treetrunk",
            imgsrc: "",
            imgid: "apple_tree_trunk"
          },
          {
            imgclass: "apple1",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple2",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple3",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple4",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple5",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple6",
            imgsrc: "",
            imgid: "apple"
          }
        ]
      }
    ]
  },

  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    extratextblock: [
      {
        textdata: data.string.p1text3a,
        textclass: "info"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg1"
          },
          {
            imgclass: "kid1",
            imgsrc: "",
            imgid: "suraj"
          },
          {
            imgclass: "kid2",
            imgsrc: "",
            imgid: "asha"
          },
          {
            imgclass: "kid3",
            imgsrc: "",
            imgid: "prem"
          },
          {
            imgclass: "kid4",
            imgsrc: "",
            imgid: "rumi"
          },
          {
            imgclass: "kid5",
            imgsrc: "",
            imgid: "sagar"
          },
          {
            imgclass: "kid6",
            imgsrc: "",
            imgid: "suraj01"
          },
          {
            imgclass: "basket1",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "basket2",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "basket3",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "basket4",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "basket5",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "basket6",
            imgsrc: "",
            imgid: "five_apple"
          },
          {
            imgclass: "treetop",
            imgsrc: "",
            imgid: "apple_tree_top"
          },
          {
            imgclass: "treetrunk",
            imgsrc: "",
            imgid: "apple_tree_trunk"
          },
          {
            imgclass: "apple1",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple2",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple3",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple4",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple5",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple6",
            imgsrc: "",
            imgid: "apple"
          }
        ]
      }
    ]
  },

  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    extratextblock: [
      {
        textdata: data.string.p1text3,
        textclass: "info1"
      },
      {
        textdata: data.string.p1text4,
        textclass: "info2"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "suraj01",
            imgsrc: "",
            imgid: "suraj01"
          }
        ]
      }
    ],
    applebasket: [
      {
        aplebasketcontainer: "abContainer abc1",
        basketCont: "basketContainer bc1",
        apleCont: "apleContainer ac1"
      },
      {
        aplebasketcontainer: "abContainer abc2",
        basketCont: "basketContainer bc2",
        apleCont: "apleContainer ac2"
      }
    ]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",

    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg1"
          },
          {
            imgclass: "niti01",
            imgsrc: "",
            imgid: "niti01"
          },
          {
            imgclass: "basket",
            imgsrc: "",
            imgid: "basket"
          },
          {
            imgclass: "treetop",
            imgsrc: "",
            imgid: "apple_tree_top"
          },
          {
            imgclass: "treetrunk",
            imgsrc: "",
            imgid: "apple_tree_trunk"
          },
          {
            imgclass: "apple1",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple2",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple3",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple4",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple5",
            imgsrc: "",
            imgid: "apple"
          },
          {
            imgclass: "apple6",
            imgsrc: "",
            imgid: "apple"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog2 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox3",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p1text5
      }
    ]
  },

  // slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti02",
            imgsrc: "",
            imgid: "niti02"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog3 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox4",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p1text6
      }
    ]
  },

  // slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    optioncontainer: "bottom-option-container",
    optionblock: [
      {
        optionclass: "bottom-option opt-1",
        textdivclass: "bttexxt",
        textdata: data.string.p1text9
      },
      {
        optionclass: "bottom-option opt-2",
        textdivclass: "bttexxt",
        textdata: data.string.p1text10
      },
      {
        optionclass: "bottom-option opt-3",
        textdivclass: "bttexxt correct-ans",
        textdata: data.string.p1text11
      },
      {
        optionclass: "bottom-option opt-4",
        textdivclass: "bttexxt",
        textdata: data.string.p1text12
      }
    ],
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text7,
        textclass: "instruction",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text8,
        textclass: "question",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti02",
            imgsrc: "",
            imgid: "niti02"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog3 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox4",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.spbx_qn
      }
    ]
  },

  // slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    optioncontainer: "bottom-option-container",
    optionblock: [
      {
        optionclass: "bottom-option opt-1",
        textdivclass: "bttexxt",
        textdata: data.string.p1text14
      },
      {
        optionclass: "bottom-option opt-2",
        textdivclass: "bttexxt correct-ans",
        textdata: data.string.p1text15
      },
      {
        optionclass: "bottom-option opt-3",
        textdivclass: "bttexxt",
        textdata: data.string.p1text9
      },
      {
        optionclass: "bottom-option opt-4",
        textdivclass: "bttexxt",
        textdata: data.string.p1text10
      }
    ],
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text7,
        textclass: "instruction",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text13,
        textclass: "question",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti02",
            imgsrc: "",
            imgid: "niti02"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog3 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox4",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.spbx_qn
      }
    ]
  },

  // slide9
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    optioncontainer: "bottom-option-container",
    optionblock: [
      {
        optionclass: "bottom-option opt-1",
        textdivclass: "bttexxt",
        textdata: data.string.p1text16a,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-2",
        textdivclass: "bttexxt",
        textdata: data.string.p1text16b,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-3",
        textdivclass: "bttexxt correct-ans",
        textdata: data.string.p1text16c,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-4",
        textdivclass: "bttexxt",
        textdata: data.string.p1text16d,
        textclass: "opt1"
      }
    ],
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text7,
        textclass: "instruction",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text161,
        textclass: "question",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti02",
            imgsrc: "",
            imgid: "niti02"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog3 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox4",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.spbx_qn_1
      }
    ]
  },

  // slide10
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    optioncontainer: "bottom-option-container",
    optionblock: [
      {
        optionclass: "bottom-option opt-1",
        textdivclass: "bttexxt",
        textdata: data.string.s1_p12_op1,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-2",
        textdivclass: "bttexxt",
        textdata: data.string.s1_p12_op3,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-3",
        textdivclass: "bttexxt correct-ans",
        textdata: data.string.s1_p12_op2,
        textclass: "opt1"
      },
      {
        optionclass: "bottom-option opt-4",
        textdivclass: "bttexxt",
        textdata: data.string.s1_p12_op4,
        textclass: "opt1"
      }
    ],
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text7,
        textclass: "instruction",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p1text17,
        textclass: "question",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti02",
            imgsrc: "",
            imgid: "niti02"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog3 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox4",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.s1_p12_Qn
      }
    ]
  },
  // slide11
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textinDiv: true,
        textdivclass: "textContainer",
        textdata: data.string.s1_p13,
        textclass: "s13Txt",
        datahighlightflag: true,
        datahighlightcustomclass: "oneByOne"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti_13",
            imgsrc: "",
            imgid: "niti01"
          }
        ]
      }
    ]
  },
  // slide12
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textinDiv: true,
        textdivclass: "textContainer",
        textdata: data.string.s1_p14,
        textclass: "s13Txt",
        datahighlightflag: true,
        datahighlightcustomclass: "oneByOne"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti_13",
            imgsrc: "",
            imgid: "niti01"
          }
        ]
      }
    ]
  },
  // slide13
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textinDiv: true,
        textdivclass: "textContainer",
        textdata: data.string.s1_p15,
        textclass: "s13Txt",
        datahighlightflag: true,
        datahighlightcustomclass: "oneByOne"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti_13",
            imgsrc: "",
            imgid: "niti01"
          }
        ]
      }
    ]
  },
  // slide14
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textinDiv: true,
        textdivclass: "textContainer",
        textdata: data.string.s1_p16,
        textclass: "s13Txt",
        datahighlightflag: true,
        datahighlightcustomclass: "oneByOne"
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "niti_13",
            imgsrc: "",
            imgid: "niti01"
          }
        ]
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images
      
      {
        id: "bg1",
        src: imgpath + "bg.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "suraj01",
        src: imgpath + "suraj01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg2",
        src: imgpath + "bg1.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "apple",
        src: imgpath + "apple.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "apple_tree",
        src: imgpath + "apple_tree.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "apple_tree_top",
        src: imgpath + "appletree_top.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "apple_tree_trunk",
        src: imgpath + "appletree_trunk.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "coverpage",
        src: imgpath + "coverpage.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "asha",
        src: imgpath + "asha.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "five_apple",
        src: imgpath + "five_apple.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "basket",
        src: imgpath + "basket.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "basket_apple",
        src: imgpath + "basket_apple.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "basket_apple01",
        src: imgpath + "basket_apple01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "nine_apple",
        src: imgpath + "nine_apple.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti",
        src: imgpath + "niti.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti01",
        src: imgpath + "niti01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "niti02",
        src: imgpath + "niti02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "prem",
        src: imgpath + "prem.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "rumi",
        src: imgpath + "rumi.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "sagar",
        src: imgpath + "sagar.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "suraj",
        src: imgpath + "suraj.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "suraj01",
        src: imgpath + "suraj01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox1",
        src: imgpath + "text_box.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox2",
        src: imgpath + "text_box01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox3",
        src: imgpath + "text_box02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox4",
        src: imgpath + "text_box03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "correct",
        src: imgpath + "correct.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "wrong",
        src: imgpath + "wrong.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "sound_1", src: soundAsset + "s1_p1.ogg" },
      { id: "sound_2", src: soundAsset + "s1_p2.ogg" },
      { id: "sound_3", src: soundAsset + "s1_p3.ogg" },
      { id: "sound_4", src: soundAsset + "s1_p4.ogg" },
      { id: "sound_5", src: soundAsset + "s1_p5.ogg" },
      { id: "sound_6", src: soundAsset + "s1_p6.ogg" },
      { id: "sound_7", src: soundAsset + "s1_p7.ogg" },
      { id: "sound_8", src: soundAsset + "s1_p8(1).ogg" },
      { id: "sound_9", src: soundAsset + "s1_p8(2).ogg" },
      { id: "sound_10", src: soundAsset + "s1_p9(2).ogg" },
      { id: "sound_11", src: soundAsset + "s1_p9(3).ogg" },
      { id: "sound_12", src: soundAsset + "s1_p10(1).ogg" },
      // { id: "sound_13", src: soundAsset + "s1_p10(2).ogg" },
      { id: "sound_14", src: soundAsset + "s1_p10(3).ogg" },
      { id: "sound_15", src: soundAsset + "s1_p10(4).ogg" },
      { id: "sound_16", src: soundAsset + "s1_p11(1).ogg" },
      { id: "sound_17", src: soundAsset + "s1_p11(3)(incomplete).ogg" },
      { id: "sound_18", src: soundAsset + "s1_p12.ogg" },
      { id: "sound_19", src: soundAsset + "s1_p13.ogg" },
      { id: "sound_20", src: soundAsset + "s1_p14.ogg" },
      { id: "sound_21", src: soundAsset + "s1_p15.ogg" },
      { id: "sound_22", src: soundAsset + "s1_p11_2.ogg" },
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    put_label_image(content, countNext);
    put_card_image(content, countNext);
    vocabcontroller.findwords(countNext);
    $nextBtn.hide(0);
    $(".plus_font").attr("style", "font-family: sniglet !important");

    // ole.footerNotificationHandler.hideNotification();
    // sound_player("sound_"+(countNext+1),true);
    $(
      ".step1,.step2,.step3,.step4,.step5,.bottom-option,.instruction,.question,.dialog2,.dialog3,.info,.info1,.info2,.basketfull,.suraj01,.nine_apple,.basket1,.basket2,.basket3,.basket4,.basket5"
    ).hide();
    switch (countNext) {
      case 0:
      case 1:
        sound_player('sound_'+(countNext+1), true);
        break;
      case 2:
        sound_player('sound_'+(countNext+1), true);
        $(".info")
          .delay(800)
          .fadeIn(1000);
        setTimeout(function() {
          $(".treetop")
            .removeClass("zoom_in")
            .addClass("shaking");
        }, 2000);
        nav_button_controls(3500);
        break;
      case 3:
        sound_player('sound_'+(countNext+1), false);
        $(".info")
          .delay(800)
          .fadeIn(1000);
        setTimeout(function() {
          $(".treetop")
            .removeClass("zoom_in")
            .addClass("shaking");
        }, 2000);
        $(".basket1")
          .delay(2200)
          .fadeIn(1000);
        $(".basket2")
          .delay(3200)
          .fadeIn(1000);
        $(".basket3")
          .delay(4200)
          .fadeIn(1000);
        $(".basket4")
          .delay(5200)
          .fadeIn(1000);
        $(".basket5")
          .delay(6200)
          .fadeIn(1000);
        $(".basket6").hide(0)
        .delay(7200)
        .fadeIn(1000);

        nav_button_controls(8500);
        break;
      // case 4:
      //   $(".info1")
      //     .delay(800)
      //     .fadeIn(1000);
      //   $(".basketfull")
      //     .delay(1500)
      //     .fadeIn(1000);
      //   nav_button_controls(100);
      //   break;
      case 4:
        sound_player('sound_'+(countNext+1), false);
        $(".info1, .abc1")
          .delay(800)
          .fadeIn(1000);
        $(".info2")
          .delay(2500)
          .fadeIn(1000);
        $(".suraj01, .abc2")
          .delay(3500)
          .fadeIn(1000);
        let left = 1;
        let bottom = -5;
        $(".bc1, .bc2").append(
          "<img class='Aplbasket' src='" +
            preload.getResult("basket").src +
            "'>"
        );
        for (var i = 0; i < 15; i++) {
          $(".ac1").append(
            "<img class='aple_" +
              i +
              "' src='" +
              preload.getResult("apple").src +
              "'>"
          );
        }
        for (var i = 0; i < 10; i++) {
          $(".ac2").append(
            "<img class='ab2 aple_b2_" +
              i +
              "' src='" +
              preload.getResult("apple").src +
              "'>"
          );
        }
        $(".ab2").hide(0);

        for (var j = 0; j < 15; j++) {
          if (j === 6) {
            bottom += 41;
            left = 10;
          } else if (j === 11) {
            bottom += 41;
            left = 20;
          } else {
            bottom = bottom;
          }

          bottom = bottom;
          $(".aple_" + j + ", .aple_b2_" + j).css({
            position: "absolute",
            bottom: bottom + "%",
            left: left + "%",
            width: "17%"
          });
          left += 17;
        }
        // let ap1Num = 14;
        // let ap2Num=0;
        function showHideApple(ap1Num, ap2Num) {
          $(".aple_" + ap1Num).fadeOut(100);
          $(".aple_b2_" + ap2Num).fadeIn(100);
          if (ap2Num < 8) {
            timeoutvar = setTimeout(function() {
              showHideApple((ap1Num -= 1), (ap2Num += 1));
            }, 500);
          } else {
            nav_button_controls(100);
          }
        }

        setTimeout(function() {
          showHideApple(14, 0);
        }, 3800);

        break;

      case 5:
        sound_player('sound_'+(countNext+1),true);
        $(".dialog2")
          .delay(1000)
          .fadeIn(1000);
        setTimeout(function() {
          $(".treetop")
            .removeClass("zoom_in")
            .addClass("shaking");
        }, 2000);
        $(".textfill").css({ left: "50%", top: "28%" });
        break;

      case 6:
        sound_player('sound_'+(countNext+1),true);
        $(".dialog3")
          .delay(1000)
          .fadeIn(1000);
        $(".dialog3").css({
          "width": "62%",
          "left": "9%"
        });
        $(".textfill").css({ left: "52%", top: "25%", "text-align": "left" });
        break;

      case 7:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play('sound_8');
        current_sound.play();
          current_sound.on("complete", function() {
            var current_sound1 = createjs.Sound.play('sound_9');
            current_sound1.play();
            current_sound1.on("complete", function() {
              // nav_button_controls(100);
            });
        });
        $(".dialog3")
          .delay(800)
          .fadeIn(1000);
        $(".instruction")
          .delay(1300)
          .fadeIn(1000);
        $(".question")
          .delay(1800)
          .fadeIn(1000);
        $(".bottom-option")
          .delay(2300)
          .fadeIn(1000);
        $(".textfill").css({ left: "51%", top: "33%" });

        if (countNext > 0) $prevBtn.show(0);
        var parent = $(".bottom-option-container");
        var divs = parent.children();
        while (divs.length) {
          parent.append(
            divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
          );
        }
        $(".correct-icon").attr("src", preload.getResult("correct").src);
        $(".incorrect-icon").attr("src", preload.getResult("wrong").src);
        $(".bttexxt").click(function() {
          createjs.Sound.stop();
          if ($(this).hasClass("correct-ans")) {
            play_correct_incorrect_sound(1);
            $(this)
              .parent()
              .children(".correct-icon")
              .show(0);
            $(".bttexxt").css("pointer-events", "none");
            $(this).addClass("correct-answer");
            console.log("in correct ans");

            if (countNext < $total_page - 1) {
              nav_button_controls(100);
            } else {
              ole.footerNotificationHandler.pageEndSetNotification();
            }
          } else {
            console.log("in incorrect ans");
            play_correct_incorrect_sound(0);
            $(this).css("pointer-events", "none");
            $(this)
              .parent()
              .children(".incorrect-icon")
              .show(0);
            $(this).addClass("incorrect-answer");
          }
        });
        // nav_button_controls(100);
        break;
      case 8:
        $(".dialog3")
          .delay(800)
          .fadeIn(1000);
        $(".instruction")
          .delay(1300)
          .fadeIn(1000);
        $(".question")
          .delay(1800)
          .fadeIn(1000);
        $(".bottom-option")
          .delay(2300)
          .fadeIn(1000);
        $(".textfill").css({ left: "51%", top: "33%" });
        setTimeout(function(){
         sound_player('sound_10',false);
        },3300);
        if (countNext > 0) $prevBtn.show(0);
        var parent = $(".bottom-option-container");
        var divs = parent.children();
        while (divs.length) {
          parent.append(
            divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
          );
        }
        $(".correct-icon").attr("src", preload.getResult("correct").src);
        $(".incorrect-icon").attr("src", preload.getResult("wrong").src);
        $(".bttexxt").click(function() {
          createjs.Sound.stop();
          if ($(this).hasClass("correct-ans")) {
            play_correct_incorrect_sound(1);
            setTimeout(function(){
              sound_player('sound_11',true);
            },1500);
            $(this)
              .parent()
              .children(".correct-icon")
              .show(0);
            $(".bttexxt").css("pointer-events", "none");
            $(this).addClass("correct-answer");
            console.log("in correct ans");

            if (countNext < $total_page - 1) {
              $(".textfill").text(data.string.p1text16);
              $(".niti02").attr("src", preload.getResult("niti01").src);
              $(".niti02").css("width", "22%");
              // nav_button_controls(100);
            } else {
              ole.footerNotificationHandler.pageEndSetNotification();
            }
          } else {
            console.log("in incorrect ans");
            play_correct_incorrect_sound(0);
            $(this).css("pointer-events", "none");
            $(this)
              .parent()
              .children(".incorrect-icon")
              .show(0);
            $(this).addClass("incorrect-answer");
          }
        });
        // nav_button_controls(100);
        break;
      case 9:
         createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_12');
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_13');
              current_sound1.play();
              current_sound1.on("complete", function() {
                // nav_button_controls(100);
              });
          });
         $(".dialog3")
          .delay(800)
          .fadeIn(1000);
        $(".instruction")
          .delay(1300)
          .fadeIn(1000);
        $(".question")
          .delay(1800)
          .fadeIn(1000);
        $(".bottom-option")
          .delay(2300)
          .fadeIn(1000);
        $(".textfill").css({ left: "46%", top: "33%" });
        $(".bttexxt").css("width", "142%");

        if (countNext > 0) $prevBtn.show(0);
        var parent = $(".bottom-option-container");
        var divs = parent.children();
        while (divs.length) {
          parent.append(
            divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
          );
        }
        $(".correct-icon").attr("src", preload.getResult("correct").src);
        $(".incorrect-icon").attr("src", preload.getResult("wrong").src);
        $(".bttexxt").click(function() {
          createjs.Sound.stop();
          if ($(this).hasClass("correct-ans")) {
            play_correct_incorrect_sound(1);
            setTimeout(function(){
              sound_player('sound_15',true);
            },1500)
            $(this)
              .parent()
              .children(".correct-icon")
              .show(0);
            $(".bttexxt").css("pointer-events", "none");
            $(this).addClass("correct-answer");
            console.log("in correct ans");
            $(".textfill").html(data.string.spbx_ans1);
            if (countNext < $total_page - 1) {
              // nav_button_controls(100);
            } else {
              ole.footerNotificationHandler.pageEndSetNotification();
            }
          } else {
            console.log("in incorrect ans");
            play_correct_incorrect_sound(0);
            $(this).css("pointer-events", "none");
            $(this)
              .parent()
              .children(".incorrect-icon")
              .show(0);
            $(this).addClass("incorrect-answer");
          }
        });
        // nav_button_controls(100);
        break;
      case 10:
          createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_16');
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_17');
              current_sound1.play();
              current_sound1.on("complete", function() {
                // nav_button_controls(100);
              });
          });
        $(".dialog3")
          .delay(800)
          .fadeIn(1000);
        $(".instruction")
          .delay(1300)
          .fadeIn(1000);
        $(".question")
          .delay(1800)
          .fadeIn(1000);
        $(".bottom-option")
          .delay(2300)
          .fadeIn(1000);
        $(".textfill").css({ left: "50%", top: "33%" });
        $(".bttexxt").css("width", "142%");

        if (countNext > 0) $prevBtn.show(0);
        var parent = $(".bottom-option-container");
        var divs = parent.children();
        while (divs.length) {
          parent.append(
            divs.splice(Math.floor(Math.random() * divs.length), 1)[0]
          );
        }
        $(".correct-icon").attr("src", preload.getResult("correct").src);
        $(".incorrect-icon").attr("src", preload.getResult("wrong").src);
        $(".bttexxt").click(function() {
          createjs.Sound.stop();
          if ($(this).hasClass("correct-ans")) {
            play_correct_incorrect_sound(1);
            setTimeout(function(){
              sound_player('sound_22',true);
            },1500)
            $(".textfill").html(data.string.afterAns_12);
            $(this)
              .parent()
              .children(".correct-icon")
              .show(0);
            $(".bttexxt").css("pointer-events", "none");
            $(this).addClass("correct-answer");
            console.log("in correct ans");

            if (countNext < $total_page - 1) {
              // nav_button_controls(100);
            } else {
              ole.footerNotificationHandler.pageEndSetNotification();
            }
          } else {
            console.log("in incorrect ans");
            play_correct_incorrect_sound(0);
            $(this).css("pointer-events", "none");
            $(this)
              .parent()
              .children(".incorrect-icon")
              .show(0);
            $(this).addClass("incorrect-answer");
          }
        });
        // nav_button_controls(100);
        break;
      case 11:
        sound_player('sound_18',true);
        $(".oneByOne").hide(0);
        $(".oneByOne:eq(0)").fadeIn(100);
        $(".oneByOne:eq(1)")
          .delay(1000)
          .fadeIn(100);
        $(".oneByOne:eq(2)")
          .delay(2000)
          .fadeIn(100);
        break;
      case 12:
        sound_player('sound_19',true);
        $(".oneByOne").hide(0);
        $(".oneByOne:eq(0)").fadeIn(100);
        $(".oneByOne:eq(1)")
          .delay(1000)
          .fadeIn(100);
        break;
      case 13:
        sound_player('sound_20',true);
        texthighlight($board);
        $(".oneByOne:eq(2) .oneByOne:eq(3)").hide(0);
        // $(".oneByOne:eq(0)").fadeIn(100);
        // $(".oneByOne:eq(1)")
        //   .delay(1000)
        //   .fadeIn(100);
        $(".oneByOne:eq(2)")
          .delay(1000)
          .fadeIn(100);
        $(".oneByOne:eq(3)")
          .delay(2000)
          .fadeIn(100);
        break;
      case 14:
        sound_player('sound_21',true);
        texthighlight($board);
        $(".oneByOne:eq(3) .oneByOne:eq(4)").hide(0);
        // $(".oneByOne:eq(0)").fadeIn(100);
        // $(".oneByOne:eq(1)")
        //   .delay(1000)
        //   .fadeIn(100);
        // $(".oneByOne:eq(2)")
        //   .delay(2000)
        //   .fadeIn(100);
        $(".oneByOne:eq(3)")
          .delay(1000)
          .fadeIn(100);
        $(".oneByOne:eq(4)")
          .delay(2000)
          .fadeIn(100);
        break;
      default:
        // nav_button_controls(1000);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }
  function sound_player(sound_id, navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    if(navigate){
      current_sound.on("complete", function() {
        nav_button_controls(500)
    });
    }
   
  }
  

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
    if (content[count].hasOwnProperty("containsdialog")) {
      for (var j = 0; j < content[count].containsdialog.length; j++) {
        var containsdialog = content[count].containsdialog[j];
        console.log("imageblock", imageblock);
        if (containsdialog.hasOwnProperty("dialogueimgid")) {
          var image_src = preload.getResult(containsdialog.dialogueimgid).src;
          //get list of classes
          var classes_list =
            containsdialog.dialogimageclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }
  function put_label_image(content, count) {
    if (content[count].hasOwnProperty("imageandlabel")) {
      var imageandlabel = content[count].imageandlabel;
      for (var i = 0; i < imageandlabel.length; i++) {
        var image_src = preload.getResult(imageandlabel[i].imgid).src;
        console.log(image_src);
        var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }
  function put_card_image(content, count) {
    if (content[count].hasOwnProperty("card")) {
      var card = content[count].card;
      for (var i = 0; i < card.length; i++) {
        var image_src = preload.getResult(card[i].imgid).src;
        console.log(image_src);
        var classes_list = card[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }
  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    loadTimelineProgress($total_page, countNext + 1);
    generaltemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }
  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });
  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

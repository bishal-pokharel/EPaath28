var imgpath = $ref + "/images/";
var soundAsset = $ref + "/sounds/" + $lang + "/";

var imgpath = $ref + "/images/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    extratextblock: [
      {
        textdata: data.string.p2text1,
        textclass: "lesson-title"
      }
    ]
  },

  // slide1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog1 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text2
      }
    ]
  },

  // slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg2"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog1 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text3
      }
    ]
  },

  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg3"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog1 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text4
      }
    ]
  },

  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "bg-full",
            imgsrc: "",
            imgid: "bg4"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog1 fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text5
      }
    ]
  },

  // slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text6
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text7
      }
    ]
  },

  // slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text8,
        textclass: "text1",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9,
        textclass: "text2",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9a,
        textclass: "text3",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9a,
        textclass: "text4",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9b,
        textclass: "text5",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9c,
        textclass: "text6",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9d,
        textclass: "text7",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9c,
        textclass: "text8",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text9e,
        textclass: "text9",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "dad",
            imgsrc: "",
            imgid: "dad"
          },
          {
            imgclass: "mum",
            imgsrc: "",
            imgid: "mum"
          },
          {
            imgclass: "twentyrs",
            imgsrc: "",
            imgid: "twentyrs"
          },
          {
            imgclass: "twentyrs1",
            imgsrc: "",
            imgid: "twentyrs"
          },
          {
            imgclass: "fivers",
            imgsrc: "",
            imgid: "fivers"
          },
          {
            imgclass: "twors",
            imgsrc: "",
            imgid: "twors"
          },
          {
            imgclass: "twors1",
            imgsrc: "",
            imgid: "twors"
          },
          {
            imgclass: "oners",
            imgsrc: "",
            imgid: "oners"
          }
        ]
      }
    ]
  },

  // slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text6
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text10
      }
    ]
  },

  // slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text11
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text10
      }
    ]
  },

  // slide9
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text12
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text13
      }
    ]
  },

  // slide10
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      // {
      //   dialogcontainer: "dialog fadein_dialogue",
      //   dialogimageclass: "dialog_image1 ",
      //   dialogueimgid: "textbox2",
      //   dialogcontentclass: "textfill",
      //   dialogcontent: data.string.p2text12
      // },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text14
      }
    ]
  },

  // slide11
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text15
      },
      // {
      //   dialogcontainer: "dialog4 fadein_dialogue",
      //   dialogimageclass: "dialog_image2 ",
      //   dialogueimgid: "textbox1",
      //   dialogcontentclass: "textfill1",
      //   dialogcontent: data.string.p2text14
      // }
    ]
  },

  // slide12
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text16
      }
    ]
  },

  // slide13
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text17
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text18
      }
    ]
  },

  // slide14
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          },
          {
            imgclass: "roshan1",
            imgsrc: "",
            imgid: "roshan1"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text17
      },
      {
        dialogcontainer: "dialog4 fadein_dialogue",
        dialogimageclass: "dialog_image2 ",
        dialogueimgid: "textbox1",
        dialogcontentclass: "textfill1",
        dialogcontent: data.string.p2text19
      }
    ]
  },

  // slide15
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    extratextblock: [
      {
        textdata: "",
        textclass: "top_text",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      },
      {
        textdata: data.string.p2text14a,
        textclass: "toptext1",
        datahighlightflag: true,
        datahighlightcustomclass: ""
      }
    ],
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog fadein_dialogue",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox2",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text14c
      }
    ]
  },

  // slide16
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "contentbg",
    imageblock: [
      {
        imagestoshow: [
          {
            imgclass: "fairy1",
            imgsrc: "",
            imgid: "fairy"
          }
        ]
      }
    ],
    containsdialog: [
      {
        dialogcontainer: "dialog8",
        dialogimageclass: "dialog_image1 ",
        dialogueimgid: "textbox5",
        dialogcontentclass: "textfill",
        dialogcontent: data.string.p2text14d
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);
  var vocabcontroller = new Vocabulary();
  vocabcontroller.init();

  var preload;
  var timeoutvar = null;
  var current_sound;

  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      //images

      {
        id: "bg1",
        src: imgpath + "bg_roshan02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg2",
        src: imgpath + "bg_roshan03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg3",
        src: imgpath + "bg_roshan04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "bg4",
        src: imgpath + "bg_roshan05.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "fairy",
        src: imgpath + "flying-chibi01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan1",
        src: imgpath + "roshan03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan2",
        src: imgpath + "roshan04.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan3",
        src: imgpath + "roshan02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "roshan4",
        src: imgpath + "roshan05.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dad",
        src: imgpath + "dad.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "mum",
        src: imgpath + "mum.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "twentyrs",
        src: imgpath + "twenty-rupees_note.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "fivers",
        src: imgpath + "five-rupees_note.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "oners",
        src: imgpath + "one_rupee_coin.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "twors",
        src: imgpath + "two_rupee_coin.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "attheairport",
        src: imgpath + "at_the_airport.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "dilak",
        src: imgpath + "dilak.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "clocknew",
        src: imgpath + "clock_new.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "hour",
        src: imgpath + "hour.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "minute",
        src: imgpath + "minute.png",
        type: createjs.AbstractLoader.IMAGE
      },

      {
        id: "textbox1",
        src: imgpath + "text_box.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox2",
        src: imgpath + "text_box01.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox3",
        src: imgpath + "text_box02.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox4",
        src: imgpath + "text_box03.png",
        type: createjs.AbstractLoader.IMAGE
      },
      {
        id: "textbox5",
        src: imgpath + "text_box04.png",
        type: createjs.AbstractLoader.IMAGE
      },

      // sounds
      { id: "sound_1", src: soundAsset + "s2_p1.ogg" },
      { id: "sound_6", src: soundAsset + "s2_p6.ogg" },
      { id: "sound_7", src: soundAsset + "s2_p7.ogg" },
      { id: "sound_9", src: soundAsset + "s2_p9.ogg" },
      { id: "sound_10", src: soundAsset + "s2_p10.ogg" },
      { id: "sound_12", src: soundAsset + "s2_p12.ogg" },
      { id: "sound_13", src: soundAsset + "s2_p13.ogg" },
      { id: "sound_14", src: soundAsset + "s2_p14.ogg" },
      { id: "sound_16", src: soundAsset + "s2_p16.ogg" },
      { id: "sound_17", src: soundAsset + "s2_p17.ogg" },
      { id: "sound_18", src: soundAsset + "s2_p2.ogg" },
      { id: "sound_19", src: soundAsset + "s2_p3.ogg" },
      { id: "sound_20", src: soundAsset + "s2_p4.ogg" },
      { id: "sound_21", src: soundAsset + "s2_p5.ogg" },
      { id: "sound_22", src: soundAsset + "s2_p6(2).ogg" },
      { id: "sound_23", src: soundAsset + "s2_p8(2).ogg" },
      { id: "sound_24", src: soundAsset + "s2_p10(2).ogg" },
      { id: "sound_25", src: soundAsset + "s2_p11.ogg" },
      { id: "sound_26", src: soundAsset + "s2_p15(2).ogg" },
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound); //for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $("#loading-text").html(parseInt(event.loaded * 100) + "%");
  }
  function handleComplete(event) {
    $("#loading-wrapper").hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play("sound_1");
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();

  /*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  /*===============================================
	=            data highlight function            =
	===============================================*/
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object"
      ? alert(
          "Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted"
        )
      : null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
        $(this).attr(
          "data-highlightcustomclass"
        ) /*if there is data-highlightcustomclass defined it is true else it is not*/
          ? (stylerulename = $(this).attr("data-highlightcustomclass"))
          : (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
	 =            user notification function        =
	 ===============================================*/
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object"
      ? alert(
          "Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style."
        )
      : null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find(
      "*[data-usernotification='notifyuser']"
    );
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one("click", function() {
        /* Act on the event */
        $(this).attr("data-isclicked", "clicked");
        $(this).removeAttr("data-usernotification");
      });
    }
  }

  /*=====  End of user notification function  ======*/

  /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

  function navigationcontroller(islastpageflag) {
    typeof islastpageflag === "undefined"
      ? (islastpageflag = false)
      : typeof islastpageflag != "boolean"
      ? alert(
          "NavigationController : Hi Master, please provide a boolean parameter"
        )
      : null;

    // islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
  }

  /*=====  End of user navigation controller function  ======*/

  /*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

  function instructionblockcontroller() {
    var $instructionblock = $board.find("div.instructionblock");
    if ($instructionblock.length > 0) {
      var $contentblock = $board.find("div.contentblock");
      var $toggleinstructionblockbutton = $instructionblock.find(
        "div.toggleinstructionblock"
      );
      var instructionblockisvisibleflag;

      $contentblock.css("pointer-events", "none");

      $toggleinstructionblockbutton.on("click", function() {
        instructionblockisvisibleflag = $instructionblock.attr(
          "data-instructionblockshow"
        );
        if (instructionblockisvisibleflag == "true") {
          instructionblockisvisibleflag = "false";
          $contentblock.css("pointer-events", "auto");
        } else if (instructionblockisvisibleflag == "false") {
          instructionblockisvisibleflag = "true";
          $contentblock.css("pointer-events", "none");
        }

        $instructionblock.attr(
          "data-instructionblockshow",
          instructionblockisvisibleflag
        );
      });
    }
  }

  /*=====  End of InstructionBlockController  ======*/

  /*=================================================
	 =            general template function            =
	 =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    put_image(content, countNext);
    put_label_image(content, countNext);
    put_card_image(content, countNext);
    vocabcontroller.findwords(countNext);
    $nextBtn.hide(0);
    $(".plus_font").attr("style", "font-family: sniglet !important");

    // ole.footerNotificationHandler.hideNotification();
    // sound_player("sound_"+(countNext+1),true);
    $(
      ".fairy1,.roshan1,.dialog,.dialog4,.step1,.step2,.step3,.step4,.step5,.bottom-option,.instruction,.question,.dialog2,.dialog3,.info,.info1,.info2,.basketfull,.suraj01,.nine_apple,.basket1,.basket2,.basket3,.basket4,.basket5"
    ).hide();
    switch (countNext) {
      case 0:
          sound_player('sound_1',true);
          $(".textfill").css({ left: "46%" });
          break;
      case 1:
            sound_player('sound_18', true);
            $(".textfill").css({ left: "46%" });
            break;
      case 2:
          sound_player('sound_19', true);
          $(".textfill").css({ left: "46%" });
        break;
      case 3:
          sound_player('sound_20', true);
          $(".textfill").css({ left: "46%" });
        $(".dialog1").css({ height: "29%" });
        nav_button_controls(1000);
        break;
      case 4:
        $(".dialog1").css({ height: "50%", top: "29%" });
          sound_player('sound_21', true);
          $(".textfill").css({ left: "46%" });
        break;
      case 5:
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play('sound_6');
        current_sound.play();
          current_sound.on("complete", function() {
            var current_sound1 = createjs.Sound.play('sound_22');
            current_sound1.play();
            current_sound1.on("complete", function() {
              nav_button_controls(500);
          });
        });

        $(".dialog").css({ left: "20%", top: "6%" });
        $(".dialog4").css({ left: "36%", top: "47%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);

        break;
      case 6:
          // sound_player('sound_7', true);
          $(".dialog").css({ left: "22%", top: "6%" });
          nav_button_controls(500);
        break;
      case 7:
          $(".dialog").css({ left: "20%", top: "7%" });
        $(".dialog4").css({ left: "36%", top: "47%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);
        setTimeout(function(){
          sound_player('sound_23', true);
        },3000);
        break;
      case 10:
          $(".dialog").css({ left: "20%", top: "7%" });
        $(".dialog4").css({ left: "36%", top: "47%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);
          setTimeout(function(){
          sound_player('sound_25', true);
          },3000);
        break;
      case 9:
          createjs.Sound.stop();
          var current_sound = createjs.Sound.play('sound_'+(countNext+1));
          current_sound.play();
            current_sound.on("complete", function() {
              var current_sound1 = createjs.Sound.play('sound_24');
              current_sound1.play();
              current_sound1.on("complete", function() {
                nav_button_controls(500);
            });
          });
          $(".dialog").css({ left: "20%", top: "7%" });
          $(".dialog4").css({ left: "36%", top: "47%" });
          $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
          $(".textfill1").css({
            top: "31%",
            left: "45%",
            transform: "translate(-50%,9%)"
          });
          $(".fairy1")
            .delay(600)
            .fadeIn(1000);
          $(".dialog")
            .delay(1200)
            .fadeIn(1000);
          $(".roshan1")
            .delay(1800)
            .fadeIn(1000);
          $(".dialog4")
            .delay(2400)
            .fadeIn(1000);
  
          break;
      case 8:
          sound_player('sound_9', true);
          $(".dialog").css({ left: "20%", top: "19%" });
        $(".dialog4").css({ left: "36%", top: "47%" });
        $(".textfill").css({ top: "31%" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);

        nav_button_controls(100);
        break;
      case 11:
          sound_player('sound_12', true);
          $(".dialog").css({ left: "20%", top: "11%" });
        $(".dialog4").css({ left: "36%", top: "47%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);

        break;

      case 12:
          sound_player('sound_13', true);
          $(".dialog").css({ left: "20%", top: "6%", width: "62%" });
        $(".dialog4").css({ left: "36%", top: "44%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);

        break;

      case 13:
          sound_player('sound_14', true);
          $(".dialog").css({ left: "20%", top: "14%", width: "62%" });
        $(".dialog4").css({ left: "7%", top: "44%", width: "70%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);

        break;

      case 14:
          sound_player('sound_26', true);
          $(".dialog").css({ left: "20%", top: "13%", width: "62%" });
        $(".dialog4").css({ left: "36%", top: "44%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".textfill1").css({
          top: "31%",
          left: "45%",
          transform: "translate(-50%,9%)"
        });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);
        $(".roshan1")
          .delay(1800)
          .fadeIn(1000);
        $(".dialog4")
          .delay(2400)
          .fadeIn(1000);

        break;

      case 15:
          sound_player('sound_16', true);
          $(".dialog").css({ left: "20%", top: "54%", width: "62%" });
        $(".textfill").css({ top: "31%", transform: "translate(-50%,5%)" });
        $(".fairy1").css({ top: "58%" });
        $(".fairy1")
          .delay(600)
          .fadeIn(1000);
        $(".dialog")
          .delay(1200)
          .fadeIn(1000);

        break;

      case 16:
          sound_player('sound_17', true);
          $(".textfill").css({ left: "48%", top: "46%" });
        break;

      default:
        // nav_button_controls(1000);
        break;
    }
  }

  function nav_button_controls(delay_ms) {
    timeoutvar = setTimeout(function() {
      if (countNext == 0) {
        $nextBtn.show(0);
      } else if (countNext > 0 && countNext == $total_page - 1) {
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    }, delay_ms);
  }

  function sound_player(sound_id, navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    if(navigate){
      current_sound.on("complete", function() {
        nav_button_controls(500)
    });
    }
  }

  function put_image(content, count) {
    if (content[count].hasOwnProperty("imageblock")) {
      var imageblock = content[count].imageblock[0];
      if (imageblock.hasOwnProperty("imagestoshow")) {
        var imageClass = imageblock.imagestoshow;
        for (var i = 0; i < imageClass.length; i++) {
          var image_src = preload.getResult(imageClass[i].imgid).src;
          //get list of classes
          var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
    if (content[count].hasOwnProperty("containsdialog")) {
      for (var j = 0; j < content[count].containsdialog.length; j++) {
        var containsdialog = content[count].containsdialog[j];
        console.log("imageblock", imageblock);
        if (containsdialog.hasOwnProperty("dialogueimgid")) {
          var image_src = preload.getResult(containsdialog.dialogueimgid).src;
          //get list of classes
          var classes_list =
            containsdialog.dialogimageclass.match(/\S+/g) || [];
          var selector = "." + classes_list[classes_list.length - 1];
          $(selector).attr("src", image_src);
        }
      }
    }
  }

  function put_label_image(content, count) {
    if (content[count].hasOwnProperty("imageandlabel")) {
      var imageandlabel = content[count].imageandlabel;
      for (var i = 0; i < imageandlabel.length; i++) {
        var image_src = preload.getResult(imageandlabel[i].imgid).src;
        console.log(image_src);
        var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }

  function put_card_image(content, count) {
    if (content[count].hasOwnProperty("card")) {
      var card = content[count].card;
      for (var i = 0; i < card.length; i++) {
        var image_src = preload.getResult(card[i].imgid).src;
        console.log(image_src);
        var classes_list = card[i].imgclass.match(/\S+/g) || [];
        var selector = "." + classes_list[classes_list.length - 1];
        $(selector).attr("src", image_src);
      }
    }
  }

  function templateCaller() {
    $prevBtn.css("display", "none");
    $nextBtn.css("display", "none");

    loadTimelineProgress($total_page, countNext + 1);
    generaltemplate();
    /*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
  }

  $nextBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
        countNext++;
        templateCaller();
        break;
    }
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
  });
});

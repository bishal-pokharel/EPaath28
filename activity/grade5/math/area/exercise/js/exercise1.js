var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
Array.prototype.shufflearray = function(){
  	var i = this.length, j, temp;
    while(--i > 0){
        j = Math.floor(Math.random() * (i+1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
	return this;
};

var ctx;

var content=[
    //slide 0
	{
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        },{
          textclass: 'let_consider_text',
          textdata: data.string.letconsider
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: data.string.ex1_s1_0
        }, {
            textdata: ''
        }]
	},

    // Slide 1j
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: data.string.ex1_s1_0
        }, {
            textdata: ''
        }]
    },

    // Slide 2
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: data.string.ex1_s1_0
        }, {
            textdata: ''
        }]
    },

    // Slide 3
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: data.string.ex1_s1_0
        }, {
            textdata: ''
        }]
    },

    // Slide 4
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },

    // Slide 5
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },

    // Slide 6
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },

    // Slide 7
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },

    // Slide 8
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },

    // Slide 9
    {
        uppertextblock: [{
            textclass: 'question-text',
            textdata: data.string.ex1_s1
        }],
        canvasblock: true,
        answerblock: [{
            check: data.string.check
        }],
        hintblock: [{
            textdata: ''
        }, {
            textdata: ''
        }]
    },
];

function resizeCanvas(ctx, height, width) {
	ctx.canvas.height = height;
	ctx.canvas.width = width;
}

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var $contentBlock = $('.contentblock');
	var countNext = 0;

	var $total_page = 10;

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s1_p1", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

     /*random scoreboard eggs*/
	var score = 0;
    var testin = new EggTemplate();

    testin.init(10);
    // General template
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

        // canvas init
        var $canvasWrapper = $('.canvas-wrapper');
        var $canvasBase = $('#canvas-base');
        if ($canvasBase[0]) {
            ctx = $canvasBase[0].getContext('2d');
            console.log($canvasWrapper.height(), $canvasWrapper.width())
            resizeCanvas(ctx, $canvasWrapper.height(), $canvasWrapper.width());
        }

        // Canvas variables
        var unitSizes, endX, endY;
        var gridPoints = [];
        var randHeight = 0;     // Random relative height
        var randWidth = 0;      // Random relative width
        var startX = 0;
        var startY = 0;
        var answer = 0;
        var $hintTexts = $('.hintblock p');
        var $inputbox = $('.inputbox').eq(0);
        var $checkBtn = $('.check-btn');
        var tries = 0;

        if(countNext<4){
          $(".let_consider_text").show(0);
        }

		switch(countNext) {
            case 0:
                sound_player("s1_p1");
                generateExercise(1, 1, 3, 4, false, true);
                break;
            case 1:
                generateExercise(2, 2, 4, 4, true, true);
                break;
            case 2:
                generateExercise(3, 4, 8, 10, false, true);
                break;
            case 3:
                generateExercise(4, 4, 8, 8, true, true);
                break;
            case 4:
            case 5:
                generateExercise(2, 2, 7, 8, false, false);
                break;
            case 6:
                generateExercise(1, 1, 8, 8, true, false);
                break;
            case 7:
            case 8:
                generateExercise(7, 8, 14, 15, false, false);
                break;
            case 9:
                generateExercise(8, 8, 15, 15, true, false);
                break;
		}

        function generateExercise(minWidth, minHeight, maxWidth, maxHeight, isSquare, shouldGenerateLines) {
            var onceIncorrect = false; // flag to track if the answer is incorrect once

            // shouldGenerateLines is for second type of question
            if (shouldGenerateLines) {
                var length = 12;
                unitSizes = getUnitSizes($canvasBase.width(), $canvasBase.height(), length, length);
                gridPoints = genGridPoints(unitSizes[0], unitSizes[1], length, length);
            } else {
                var length = 17;
                unitSizes = getUnitSizes($canvasBase.width(), $canvasBase.height(), length, length);
                gridPoints = genGridPoints(unitSizes[0], unitSizes[1], length, length);
            }

            // Calculate end points
            endX = gridPoints[gridPoints.length - 1][0];
            endY = gridPoints[gridPoints.length - 1][1];

            // Validate inputbox
            integer_box('.inputbox', 5, '.check-btn');

            // Animate egg shaking
            $('#egg' + countNext).addClass('eggmove');

            // Hide hint texts
            $hintTexts.hide(0);

            // Hide nav buttons
            $nextBtn.hide(0);
            $prevBtn.hide(0);

            // Determines if the loop should be done
            // This only matters when isSquare is false
            // This is set to true if square is generated so that loop is done
            // to not generate square lengths
            // Otherwise it will always be false to not obstruct the program flow
            var shouldLoop = false;

            do {
                randIndex = ole.getRandom(1, gridPoints.length - 1, 0)[0];

                // Generate starting point
                startX = gridPoints[randIndex][0];
                startY = gridPoints[randIndex][1];

                // Generate random relative Width and Height
                if (isSquare) {
                    randHeight = randWidth = ole.getRandom(1, maxWidth, minWidth)[0];
                } else {
                    randWidth = ole.getRandom(1, maxWidth, minWidth)[0];
                    randHeight = ole.getRandom(1, maxHeight, minHeight)[0];
                }
                if (!isSquare) {
                    shouldLoop = randWidth === randHeight;
                }

            } while ((startX+randWidth*unitSizes[0] > endX || startY+randHeight*unitSizes[1] > endY)
                        || shouldLoop);

            // Calculate answer
            answer = randWidth * randHeight;

            // Listen to button click
            $checkBtn.on('click', function() {
                var userAnswer = $inputbox.val();

                // Check answer
                if (userAnswer == answer) {
                    if (!onceIncorrect) {
                        testin.update(true);
                        // play_correct_incorrect_sound(1);
                    }
                    colorifyCorrectBtn();
                    $nextBtn.show(0);
                    $checkBtn.off();
                } else {
                    testin.update(false);
                    // play_correct_incorrect_sound(0);
                    onceIncorrect = true;
    				$(this).addClass('incorrect_answer');
    				wrngClicked = true;
                    colorifyIncorrectBtn();

                    tries++
                    switch(tries) {
                        case 1:
                            $hintTexts.eq(0).fadeIn();
                            break;
                        case 2:
                            $hintTexts.eq(1).fadeIn();
                            break;
                    }
                }
            });

            // Set hint text for answer
            if (shouldGenerateLines) {
                $hintTexts.eq(1).html(data.string.ex1_s2 + answer + data.string.ex1_s3);
            } else {
                $hintTexts.eq(0).html(data.string.ex1_s4 + randWidth + data.string.ex1_s5 + randHeight + data.string.ex1_s6);
                $hintTexts.eq(1).html(data.string.ex1_s7 + randWidth + data.string.ex1_s8 + randHeight);
            }

            // Draw rectangle
            createRect(ctx, unitSizes[0], unitSizes[1], startX, startY, randWidth, randHeight);

            // Only generate lines in middle of rectangle for this flag
            // And write width and height texts when flag is false
            if (shouldGenerateLines) {
                // Draw lines joining points inside rectangle
                createLines(ctx, unitSizes[0], unitSizes[1], startX, startY, randWidth, randHeight);
            } else {
                // Style font
                ctx.font = '15px Verdana';
                ctx.fillStyle = '#000';

                // Rotate context for height text
                ctx.save();
                ctx.translate(startX, startY);
                ctx.rotate(-Math.PI/2);
                ctx.textAlign = "center";
                // Draw height text
                ctx.fillText('L = ' + randHeight + 'cm', -randHeight*unitSizes[1]/2, -5);
                ctx.restore();

                // Draw width text
                ctx.fillText('B = ' + randWidth + 'cm', (startX-25) + randWidth*unitSizes[0]/2, startY - 5)
            }

            // Uncomment the line below to draw grid points (Useful to visualize points)
            // createDots(ctx, gridPoints);
        }

        function colorifyCorrectBtn() {
            if ($checkBtn.hasClass('incorrect')) $checkBtn.removeClass('incorrect');
            if ($inputbox.hasClass('incorrect')) $inputbox.removeClass('incorrect');
            $checkBtn.addClass('correct');
          		createjs.Sound.stop();
            play_correct_incorrect_sound(1);
            $inputbox.addClass('correct');
        }

        function colorifyIncorrectBtn() {
            if ($checkBtn.hasClass('correct')) $checkBtn.removeClass('correct');
            if ($inputbox.hasClass('correct')) $inputbox.removeClass('correct');
            $checkBtn.addClass('incorrect');
          		createjs.Sound.stop();
            play_correct_incorrect_sound(0);
            $inputbox.addClass('incorrect');
        }

	}
  function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
         templateCaller();
         testin.gotoNext();

     });

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/**
 * Get unitX and unitY in array
 */
function getUnitSizes(canvasWidth, canvasHeight, relativeWidth, relativeHeight) {
    var unitX = canvasWidth / relativeWidth;
    var unitY = canvasHeight / relativeHeight;
    return [unitX, unitY];
}

/**
 * Generate an array of grid points
 */
function genGridPoints(unitX, unitY, maxX, maxY) {
    var points = [];

    for (var row = 1; row <= maxY; row++) {
        for (var col = 1; col <= maxX; col++) {
            points.push([col*unitX, row*unitY]);
        }
    }
    return points;
}

/**
 * Draw a reactangle
 */
function createRect(ctx, unitX, unitY, startX, startY, relativeWidth, relativeHeight) {
    ctx.beginPath();
    ctx.fillStyle = 'rgb(255,217,102)';
    ctx.lineWidth = 2;
    ctx.rect(startX, startY, relativeWidth*unitX, relativeHeight*unitY);
    ctx.fillRect(startX, startY, relativeWidth*unitX, relativeHeight*unitY);
    ctx.stroke();
}

/**
 * Plot dots on grid points
 */
function createDots(ctx, gridPoints) {
    gridPoints.forEach(function (point) {
        ctx.beginPath();
        ctx.arc(point[0], point[1], 4, 0, 2*Math.PI);
        ctx.fill();
    })
}

/**
 * Draw lines in middle of rectangle
 */
function createLines(ctx, unitX, unitY, startX, startY, randWidth, randHeight) {
    var x = startX + unitX;
    var y = startY + unitY;
    console.log(x, startY, startY + randHeight)

    // Draw vertical lines
    for (var i = 1; i < randWidth; i++) {
        ctx.beginPath();
        ctx.moveTo(x, startY);
        ctx.lineTo(x, startY + randHeight * unitY);
        ctx.stroke();
        x += unitX;
    }

    // Draw horizontal lines
    for (var j = 1; j < randHeight; j++) {
        ctx.beginPath();
        ctx.moveTo(startX, y);
        ctx.lineTo(startX + randWidth * unitX, y);
        ctx.stroke();
        y += unitY;
    }
}

/** function to check the key pressed is a valid number(1-9 and .) for the input box or not
 * event.key reurns the value of key pressed by user and it is converted to integer
 * event.target gets the element where event is occuring (usually a div)
 * conditions for backspace, del, arrow keys, decimal point and full stop are checked and enter is checked separately
 * input_class and button_classes should be something like '.class_name'
 * max_number must be number of digit allowed for 0-9 max_number = 1  and for 0-99 max_number = 2 and so on
 */
function integer_box(input_class, max_number, button_class) {
	$(input_class).keydown(function(event){
		var charCode = (event.which) ? event.which : event.keyCode;
		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys,, 46=> del */
		if(charCode === 13 && button_class!=null) {
	        $(button_class).trigger("click");
		}
		var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
		//check if user inputs del, backspace or arrow keys
			if (!condition) {
			return true;
		}
		//check . and 0-9 separately after checking arrow and other keys
		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105)){
			return false;
		}
		//check max no of allowed digits
		if (String(event.target.value).length >= max_number) {
			return false;
		}
			return true;
	});
}

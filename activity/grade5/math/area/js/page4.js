var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content = [{
	//starting page
	contentblockadditionalclass : "bluebg",
	contentblocknocenteradjust : true,
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p4_s1
	}]

}, {
	//page 1
	contentblockadditionalclass : "plainbg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction",
		textdata : data.string.p4_s2
	}],
	containscanvaselement: true,
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "correct01",
			imgsrc : "images/correct.png"
		},{
			imgclass : "incorrect01",
			imgsrc : "images/wrong.png"
		},{
			imgclass : "reload01",
			imgsrc: imgpath+"reload.png"
		}],
	}],
	inputfields:"inputfields",
	textclass: "description2",
	datahighlightflag: true,
	textdata: data.string.p4_s3,
	inputtypes:[{
		inputtype: "text",
		inputtypeclass: "areainput",
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass",
		inputspanvalue: data.string.areacm
	},{
		inputtype: "button",
		inputtypeclass: "areacheck",
		inputtypename: "areainputcheck",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "areahint",
		inputtypename: "areainputhint",
		inputtypevalue: data.string.hint_button
	}]
},{
	//page 2
	contentblockadditionalclass : "bluebg",
	contentblocknocenteradjust : true,
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p4_s4
	}]
},
{
	//page 3
	contentblockadditionalclass : "bluebg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction01",
		textdata : data.string.p4_s5
	}],
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "shape02.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s31
		},{
			imagelabelclass: "breadth slideinleft",
			imagelabeldata: data.string.p4_s31
		},{
			imagelabelclass: "totalarea",
			imagelabeldata: data.string.p4_s32
		}]
	}],
	inputfields:"inputfields",
	textclass: "description2",
	datahighlightflag: true,
	textdata: data.string.p4_s6,
	inputtypes:[{
		inputtype: "button",
		inputtypeclass: "yesbutton",
		inputtypename: "areainputcheck",
		inputtypevalue: data.string.yes_button
	},{
		inputtype: "button",
		inputtypeclass: "nobutton",
		inputtypename: "areainputhint",
		inputtypevalue: data.string.no_button
	}],
	lowertextblockadditionalclass : "lowertextblock",
	lowertextblock : [
	{
		textclass: "description05",
		datahighlightflag: true,
		textdata: data.string.p4_s33
	},
	{
		textclass: "description05",
		datahighlightflag: true,
		textdata: data.string.p4_s34
	}
	],
},
 {
 	//page 4
 	contentblockadditionalclass : "bluebg",
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "shape02.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s9
		},{
			imagelabelclass: "breadth slideinleft",
			imagelabeldata: data.string.p4_s8
		},{
			imagelabelclass: "totalarea",
			imagelabeldata: data.string.p4_s32
		}]
	}],
	lowertextblockadditionalclass : "lowertextblock",
	lowertextblock : [
	{
		textclass: "description3",
		datahighlightflag: true,
		textdata: data.string.p4_s11
	}
	],
}, {

	//page 5
	contentblockadditionalclass : "bluebg",
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "shape02.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s9
		},{
			imagelabelclass: "breadth slideinleft",
			imagelabeldata: data.string.p4_s8
		},{
			imagelabelclass: "totalarea",
			imagelabeldata: data.string.p4_s10
		}]
	}],
	lowertextblockadditionalclass : "lowertextblock",
	lowertextblock : [
	{
		textclass: "description3",
		datahighlightflag: true,
		textdata: data.string.p4_s12
	}
	],

}, {
	//page 6
	contentblockadditionalclass : "bluebg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction01 hide",
		textdata : data.string.p4_s16
	}],
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "rectangle.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s18
		},{
			imagelabelclass: "breadth slideinleft2",
			imagelabeldata: data.string.p4_s17
		}]
	}],
	inputfields:"inputfields2",
	textclass: "description4",
	datahighlightflag: true,
	textdata: data.string.p4_s13,
	inputtypes:[
	{
		inputtype: "text",
		inputtypeclass: "input1",
		preinputspanclass: "inputspanclass4",
		preinputspanvalue: data.string.p4_s14,
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass1",
		inputspanvalue: data.string.lengthcm
	},
	{
		inputtype: "text",
		inputtypeclass: "input2",
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass2",
		inputspanvalue: data.string.breadthcm
	},
	{
		inputtype: "text",
		inputtypeclass: "input3",
		preinputspanclass: "inputspanclass5",
		preinputspanvalue: data.string.p4_s15,
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass3",
		inputspanvalue: data.string.areacm
	},{
		inputtype: "button",
		inputtypeclass: "areacheck1",
		inputtypename: "areainputcheck",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "areahint1",
		inputtypename: "areainputhint",
		inputtypevalue: data.string.hint_button
	}]

},{
	//page 7
	contentblockadditionalclass : "bluebg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction01",
		textdata : data.string.p4_s16
	}],
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "rectangle.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s18
		},{
			imagelabelclass: "breadth slideinleft2",
			imagelabeldata: data.string.p4_s17
		}]
	}],
	inputfields:"inputfields2",
	textclass: "introduction01",
	datahighlightflag: true,
	textdata: data.string.p4_s19
}, {

	//page 8
	contentblockadditionalclass : "bluebg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction01 hide",
		textdata : data.string.p4_s20
	}],
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "rectangle.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s22
		},{
			imagelabelclass: "breadth slideinleft2",
			imagelabeldata: data.string.p4_s21
		}]
	}],
	inputfields:"inputfields2",
	textclass: "description4",
	datahighlightflag: true,
	textdata: data.string.p4_s13,
	inputtypes:[
	{
		inputtype: "text",
		inputtypeclass: "input1",
		preinputspanclass: "inputspanclass4",
		preinputspanvalue: data.string.p4_s14,
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass1",
		inputspanvalue: data.string.lengthcm
	},
	{
		inputtype: "text",
		inputtypeclass: "input2",
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass2",
		inputspanvalue: data.string.breadthcm
	},
	{
		inputtype: "text",
		inputtypeclass: "input3",
		preinputspanclass: "inputspanclass5",
		preinputspanvalue: data.string.p4_s15,
		inputtypename: "areainputname",
		inputspanclass: "inputspanclass3",
		inputspanvalue: data.string.areacm
	},{
		inputtype: "button",
		inputtypeclass: "areacheck1",
		inputtypename: "areainputcheck",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "areahint1",
		inputtypename: "areainputhint",
		inputtypevalue: data.string.hint_button
	}]

},{

	//page 9
	contentblockadditionalclass : "bluebg",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "lastpageinstroduction hide",
		textdata : data.string.p4_s25
	}],
	imageblockadditionalclass: "fullimageblock",
	imageblock : [{
		imagestoshow : [
		{
			imgclass : "square01",
			imgsrc : imgpath + "shape02.png"
		}],
		imagelabels : [{
			imagelabelclass: "length slideinbottom",
			imagelabeldata: data.string.p4_s23
		},{
			imagelabelclass: "breadth slideinleft",
			imagelabeldata: data.string.p4_s24
		}]
	}],
	inputfields:"inputfields2",
	textclass: "description4",
	datahighlightflag: true,
	textdata: data.string.p4_s13,
	inputtypes:[
	{
		inputtype: "text",
		inputtypeclass: "input1",
		inputtypename: "areainputname",
		preinputspanclass: "inputspanclass4",
		preinputspanvalue: data.string.p4_s29,
		inputspanclass: "inputspanclass1",
		inputspanvalue: data.string.breadthcm
	},
	{
		inputtype: "text",
		inputtypeclass: "input3",
		inputtypename: "areainputname",
		preinputspanclass: "inputspanclass5",
		preinputspanvalue: data.string.p4_s30,
		inputspanclass: "inputspanclass3",
		inputspanvalue: data.string.areacm
	},{
		inputtype: "button",
		inputtypeclass: "areacheck1",
		inputtypename: "areainputcheck",
		inputtypevalue: data.string.check_button
	},{
		inputtype: "button",
		inputtypeclass: "areahint1",
		inputtypename: "areainputhint",
		inputtypevalue: data.string.hint_button
	}]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s4_p1", src: soundAsset+"S4_P1.ogg"},
			{id: "s4_p2_1", src: soundAsset+"s4_p2.ogg"},
      {id: "s4_p2_2", src: soundAsset+"S4_P2_2.ogg"},
      {id: "s4_p3", src: soundAsset+"S4_P3.ogg"},
			{id: "s4_p4_1", src: soundAsset+"S4_P4_1.ogg"},
      {id: "s4_p4_2", src: soundAsset+"S4_P4_2.ogg"},
			{id: "s4_p4_no", src: soundAsset+"S4_P4_NO.ogg"},
			{id: "s4_p4_yes", src: soundAsset+"S4_P4_YES.ogg"},
			{id: "s4_p5", src: soundAsset+"S4_P5.ogg"},
			{id: "s4_p6", src: soundAsset+"S4_P6.ogg"},
			{id: "s4_p7", src: soundAsset+"S4_P7.ogg"},
			{id: "s4_p8_1", src: soundAsset+"S4_P8_1.ogg"},
			{id: "s4_p8_2", src: soundAsset+"S4_P8_2.ogg"},
      {id: "s4_p9", src: soundAsset+"S4_P9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();



	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.delay(500).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);

			$prevBtn.hide(0);
			$nextBtn.hide(0);
		switch(countNext) {
			case 1:
				sound_player("s4_p"+(countNext+1)+"_1");
				initaitedrawingoncanvas($board, $nextBtn);
				break;
			case 2:
			case 4:
			case 5:
				sound_player_nav("s4_p"+(countNext+1));
			break;
			case 7:
				// $nextBtn.delay(500).show(0);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s4_p8_1");
					current_sound.play();
					current_sound.on('complete', function(){
						sound_player_nav("s4_p"+(countNext+1)+"_2");
					});
			break;
			case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s4_p4_1");
				current_sound.play();
				current_sound.on('complete', function(){
					sound_player("s4_p"+(countNext+1)+"_2");
				});
				$nextBtn.hide(0);
				var reactmessageshown = false;
				$(".yesbutton").click(function(){
					if (reactmessageshown)
						return false;
					sound_player("s4_p"+(countNext+1)+"_yes");
					$(".description05:nth-child(1)").show(0);
					reactmessageshown = true;
					nav_button_controls(3000);
				});
				$(".nobutton").click(function(){
					if (reactmessageshown)
						return false;
					sound_player("s4_p"+(countNext+1)+"_no");
					$(".description05:nth-child(2)").show(0);
					reactmessageshown = true;
					nav_button_controls(5300);
				});
			break;
			case 6:
				sound_player("s4_p"+(countNext+1));
				$nextBtn.hide(0);
				var $lengthinput = $(".input1");
				var $widthinput = $(".input2");
				var $areainput = $(".input3");
				var $checkbtn = $(".areacheck1");
				var $hintbtn = $(".areahint1");


				takenumberinputonly($lengthinput);
				takenumberinputonly($widthinput);
				takenumberinputonly($areainput);

				$checkbtn.click(function(){
					var lengthinput = parseInt($lengthinput[0].value);
					var breadthinput = parseInt($widthinput[0].value);
					var areainput = parseInt($areainput[0].value);
					var allcorrect = true;
					if(lengthinput == 2){
						$lengthinput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$lengthinput.css("background-color", "#FD4F8C");
					}


					if(breadthinput == 1){
						$widthinput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$widthinput.css("background-color", "#FD4F8C");
					}

					if(areainput == 2){
						$areainput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$areainput.css("background-color", "#FD4F8C");
					}

					if (!allcorrect) {
						if (!$checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");

						if (!$hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						// $('.input1, input2, .input3').prop('disabled', true);
						play_correct_incorrect_sound(0);
					} else {
                        $('.input1, input2, .input3').prop('disabled', true);
                        $(".hide").toggleClass("hide");
							nav_button_controls(1500);
						if ($checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");
						if (!$checkbtn.hasClass("correct02"))
							$checkbtn.toggleClass("correct02");
						if ($hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						play_correct_incorrect_sound(1);
                        nav_button_controls(0);

                    }
				});

				$hintbtn.click(function(){
					if ($checkbtn.hasClass("correct02"))
						return false;
					$(".hide").toggleClass("hide");
					$lengthinput[0].value = 2;
					$lengthinput.css("background-color", "#fff");
					$widthinput[0].value = 1;
					$widthinput.css("background-color", "#fff");
				});

				break;
			case 8:
				sound_player("s4_p7");
				$nextBtn.hide(0);
				var $lengthinput = $(".input1");
				var $widthinput = $(".input2");
				var $areainput = $(".input3");
				var $checkbtn = $(".areacheck1");
				var $hintbtn = $(".areahint1");


				takenumberinputonly($lengthinput);
				takenumberinputonly($widthinput);
				takenumberinputonly($areainput);

				$checkbtn.click(function(){
					var lengthinput = parseInt($lengthinput[0].value);
					var breadthinput = parseInt($widthinput[0].value);
					var areainput = parseInt($areainput[0].value);
					var allcorrect = true;
					if(lengthinput == 5){
						$lengthinput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$lengthinput.css("background-color", "#FD4F8C");
					}


					if(breadthinput == 2){
						$widthinput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$widthinput.css("background-color", "#FD4F8C");
					}

					if(areainput == 10){
						$areainput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$areainput.css("background-color", "#FD4F8C");
					}

					if (!allcorrect) {
						if (!$checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");

						if (!$hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						play_correct_incorrect_sound(0);
					} else {
                        $('.input1, input2, .input3').prop('disabled', true);
                        $(".hide").toggleClass("hide");
							nav_button_controls(1500);
						if ($checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");
						if (!$checkbtn.hasClass("correct02"))
							$checkbtn.toggleClass("correct02");
						if ($hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						play_correct_incorrect_sound(1);
					}
				});

				$hintbtn.click(function(){
					if ($checkbtn.hasClass("correct02"))
						return false;
					$(".hide").toggleClass("hide");
					$lengthinput[0].value = 5;
					$lengthinput.css("background-color", "#fff");
					$widthinput[0].value = 2;
					$widthinput.css("background-color", "#fff");
				});

				break;
			case 9:
				sound_player("s4_p7");
				$nextBtn.hide(0);
				var $lengthinput = $(".input1");
				var $areainput = $(".input3");
				var $checkbtn = $(".areacheck1");
				var $hintbtn = $(".areahint1");


				takenumberinputonly($lengthinput);
				takenumberinputonly($areainput);

				$checkbtn.click(function(){
					var lengthinput = parseInt($lengthinput[0].value);
					var areainput = parseInt($areainput[0].value);
					var allcorrect = true;
					if(lengthinput == 4){
						$lengthinput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$lengthinput.css("background-color", "#FD4F8C");
					}

					if(areainput == 16){
						$areainput.css("background-color", "#5eff43");
					}else{
						allcorrect = false;
						$areainput.css("background-color", "#FD4F8C");
					}

					if (!allcorrect) {
						if (!$checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");

						if (!$hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						play_correct_incorrect_sound(0);
					} else {
						$(".hide").toggleClass("hide");
							nav_button_controls(1500);
						if ($checkbtn.hasClass("incorrect02"))
							$checkbtn.toggleClass("incorrect02");
						if (!$checkbtn.hasClass("correct02"))
							$checkbtn.toggleClass("correct02");
						if ($hintbtn.hasClass("vibrate")) {
							$hintbtn.toggleClass("vibrate");
						}
						play_correct_incorrect_sound(1);
					}
				});

				$hintbtn.click(function(){
					if ($checkbtn.hasClass("correct02"))
						return false;
					$lengthinput[0].value = 4;
					$lengthinput.css("background-color", "#fff");
					$(".hide").toggleClass("hide");
				});
				break;
			default:
				sound_player_nav("s4_p"+(countNext+1));
			break;
		}
	}


		function nav_button_controls(delay_ms){
			timeoutvar = setTimeout(function(){
				if(countNext==0){
					$nextBtn.show(0);
				} else if( countNext>0 && countNext == $total_page-1){
					$prevBtn.show(0);
					ole.footerNotificationHandler.lessonEndSetNotification();
				} else{
					$prevBtn.show(0);
					$nextBtn.show(0);
				}
			},delay_ms);
		}

		function sound_player(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
		}

		function sound_player_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on('complete', function(){
				nav_button_controls();
			});
		}
	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

function takenumberinputonly($inputfield){
	$inputfield.keydown(function (evt) {
				var charVal = parseInt(evt.key);
				var prevValue = parseInt(evt.target.value)*10 + charVal;
				var charCode = (evt.which) ? evt.which : evt.keyCode;
			   	if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)){
			    	// console.log("inside");
			   		// console.log(evt.target.value);
			        return false;
			    }

			    if(prevValue > 999){
			    	return false;
			    }
			    // if(isNaN(prevValue)){
			    	// prevValue = charVal;
			    // }else{
			    	// prevValue += charVal;
			    // }
			    // var returnvalue = prevValue+"cm&sup3;";
			    // evt.target.value = returnvalue;
			    // return evt.preventDefault();
			    return true;
			} );
}


function initaitedrawingoncanvas($board, $nextBtn) {
	var $customcanvasbase = $("#customcanvasbase");
	var $customcanvasgrid = $('#customcanvasgrid');

	var ctxbase = $customcanvasbase[0].getContext('2d');
	var ctxgrid = $customcanvasgrid[0].getContext('2d');

	var canvasheight = $board.height() * 0.65;
	var canvaswidth = $board.width() * 0.8;

	//the value of draw grid is true when hint is pressed
	var drawgrid = false;
	var $correctsign = $(".correct01");
	var $incorrectsign = $(".incorrect01");
	var $areacheck = $(".areacheck");
	var $areahint = $(".areahint");
	var $areainput = $(".areainput");
	var area = 0;
	var $inputfields = $(".inputfields");
	$inputfields.hide(0);

	//lockdown draw once the shape has been drawn and displayed the student
	//must answer before he can draw again
	var lockdowndraw = false;
	var $reload = $(".reload01");

	$correctsign.hide(0);
	$incorrectsign.hide(0);
	$areahint.click(function(){
		drawgrid = true;
		drawgrids(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
	});


	$areacheck.click(function(){
		if(area == 0)
			return false;
		var enteredans = parseInt($areainput[0].value);
		if(area != enteredans){
					$correctsign.hide(0);
					play_correct_incorrect_sound(0);

					$incorrectsign.show(0);
					if (!$areacheck.hasClass("incorrect02"))
						$areacheck.toggleClass("incorrect02");
					if (!$areahint.hasClass("vibrate")){
						$areahint.toggleClass("vibrate");
					}
		}else{
					$incorrectsign.hide(0);
					$correctsign.show(0);
					play_correct_incorrect_sound(1);

					$nextBtn.delay(1000).show(0);
					$reload.delay(500).show(0);
					$areacheck.attr('disabled','disabled');
					$areahint.attr('disabled','disabled');
					if($areacheck.hasClass("incorrect02"))
						$areacheck.toggleClass("incorrect02");
					if(!$areacheck.hasClass("correct02"))
						$areacheck.toggleClass("correct02");
					if ($areahint.hasClass("vibrate")){
						$areahint.toggleClass("vibrate");
					}
					drawgrids(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
		}
	});

	$reload.click(function(){
		$nextBtn.hide(0);
		lockdowndraw = false;
		ctxgrid.clearRect(0, 0, canvaswidth, canvasheight);
		$inputfields.hide(0);
		$correctsign.hide(0);
		$areacheck.removeAttr('disabled');
		$areahint.removeAttr('disabled');
		$(this).hide(0);
	});

	ctxbase.canvas.height = canvasheight;
	ctxbase.canvas.width = canvaswidth;

	ctxgrid.canvas.height = canvasheight;
	ctxgrid.canvas.width = canvaswidth;

	var startxycoordinate;
	var endxycoordinate;
	var oldendxycoordinate = {
		x : 0,
		y : 0
	};

	/*
	 *Ashish Gurung
	 * resize optimization begin
	 * This function resize end has been copied from stackover flow solution
	 *	@link: http://stackoverflow.com/questions/5489946/jquery-how-to-wait-for-the-end-of-resize-event-and-only-then-perform-an-ac
	 *	this is implemented because if the simple .resize() function is used then the canvas redraw function gets called too may times
	 *	which may hog the processor resources over exteded period of time
	 */
	var rtime;
	var timeout = false;
	var delta = 200;
	var resizefactoralongy;
	var resizefactoralongx;
	$(window).resize(function() {
	    rtime = new Date();
	    if (timeout === false) {
	        timeout = true;
	        setTimeout(resizeend, delta);
	    }
	});

	function redraw () {
			var oldcanvasheight = canvasheight;

		    canvasheight = $board.height() * 0.65;
			canvaswidth = $board.width() * 0.8;

			ctxbase.canvas.height = canvasheight;
			ctxbase.canvas.width = canvaswidth;

			ctxgrid.canvas.height = canvasheight;
			ctxgrid.canvas.width = canvaswidth;

			drawdots(ctxbase, canvasheight, canvaswidth);

			if(startxycoordinate != null || endxycoordinate != null){
				startxycoordinate.x *= resizefactoralongx;
				startxycoordinate.y *= resizefactoralongy;

				endxycoordinate.x *= resizefactoralongx;
				endxycoordinate.y *= resizefactoralongy;

				endxycoordinate = recalculatexycoordinates(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
				area = relocateendpoint(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
				if(drawgrid)
					drawgrids(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
			}
	}

	function resizeend() {
		resizefactoralongy = ($board.height() * 0.65)/ canvasheight;
		resizefactoralongx = ($board.width() * 0.8)/ canvaswidth;
	    if (new Date() - rtime < delta) {
	    	redraw();
	        setTimeout(resizeend, delta);
	    } else {
	        timeout = false;
	    	redraw();
	    }
	}

/*
 * Ashish Gurung
 * resize optimization end
 */

	drawdots(ctxbase, canvasheight, canvaswidth);

	var mousedown = false;
	$customcanvasgrid.mousedown(function(e) {
		if(lockdowndraw){
			return false;
		}

		$areainput[0].value = "";
		$correctsign.hide(0);
		$incorrectsign.hide(0);
		if($areacheck.hasClass("incorrect02"))
			$areacheck.toggleClass("incorrect02");
		if($areacheck.hasClass("correct02"))
			$areacheck.toggleClass("correct02");
		if($areahint.hasClass("vibrate")){
			$areahint.toggleClass("vibrate");
		}
		mousedown = true;
		startxycoordinate = getxycoordinates(ctxbase, e);
	});

	$customcanvasgrid.mouseup(function(e) {
		if(lockdowndraw){
			return false;
		}
		if(mousedown){
			mousedown = false;
			finishdrawing(e);
		}
	});

	$customcanvasgrid.mouseout(function(e){
		if(lockdowndraw){
			return false;
		}
		if(mousedown){
			mousedown = false;
			finishdrawing(e);
		}
	});

	function finishdrawing(e){
		endxycoordinate = getxycoordinates(ctxgrid, e);
		if (Math.abs(oldendxycoordinate.x - endxycoordinate.x) > 5 || Math.abs(oldendxycoordinate.y - endxycoordinate.y) > 5) {
			// drawdots(ctxbase, canvasheight, canvaswidth);
			drawrectangle(ctxgrid, startxycoordinate, endxycoordinate);
			oldendxycoordinate = endxycoordinate;
		}

		endxycoordinate = recalculatexycoordinates(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
		area = relocateendpoint(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);

		// area = drawgrids(ctxgrid, startxycoordinate, endxycoordinate, canvasheight, canvaswidth);
		if( area == 0 ){
			ctxgrid.clearRect(0, 0, canvaswidth, canvasheight);
		}else{
			$inputfields.show(0);
			lockdowndraw = true;
		}
		console.log(area+ " the area of the drawn rectangle/circle");


	}

	$customcanvasgrid.mousemove(function(e) {
		if (mousedown) {
			endxycoordinate = getxycoordinates(ctxgrid, e);
			if (Math.abs(oldendxycoordinate.x - endxycoordinate.x) > 5 || Math.abs(oldendxycoordinate.y - endxycoordinate.y) > 5) {
				// drawdots(ctxbase, canvasheight, canvaswidth);
				ctxgrid.clearRect(0, 0, canvaswidth, canvasheight);
				drawrectangle(ctxgrid, startxycoordinate, endxycoordinate);
				oldendxycoordinate = endxycoordinate;
			}
		}
	});

}



/*
 * Ashish Gurung
 * the functions below are for drawing on canvas
 */

function drawgrids(ctxgrid, startxy, endxy, height, width){
	var unitxaxis = Math.round(width / 20);
	var unityaxis = Math.round(height / 10);
	console.log("unit x-axis : "+ unitxaxis," unit y-axis : "+ unityaxis);
	var startgridalongxaxis;
	var endgridalongxaxis;
	var startgridalongyaxis;
	var endgridalongyaxis;

	var startcountx;
	var startcounty;
	if(startxy.x < endxy.x){
		startgridalongxaxis = startxy.x + unitxaxis;
		endgridalongxaxis = endxy.x;
		startcountx = startxy.x;

	}else{
		startgridalongxaxis = endxy.x + unitxaxis;
		endgridalongxaxis = startxy.x;
		startcountx = endxy.x;
	}
	if(startxy.y < endxy.y){
		startgridalongyaxis = startxy.y + unityaxis;
		endgridalongyaxis = endxy.y;
		startcounty = startxy.y;

	}else{
		startgridalongyaxis = endxy.y + unityaxis;
		endgridalongyaxis = startxy.y;
		startcounty = endxy.y;
	}

	var tempstartgridalongxaxis =  startgridalongxaxis - unitxaxis;
	var tempstartgridalongyaxis = startgridalongyaxis - unityaxis;
	if(tempstartgridalongxaxis == endgridalongxaxis || tempstartgridalongyaxis == endgridalongyaxis){
		ctxgrid.clearRect(0, 0, width, height);
		return;
	}

	ctxgrid.lineWidth = "1";
	ctxgrid.strokeStyle = "blue";
	var countx = 0;
	while(startgridalongxaxis < endgridalongxaxis && countx < 20){
		countx++;
		ctxgrid.beginPath();
        ctxgrid.moveTo(startgridalongxaxis, tempstartgridalongyaxis);
        ctxgrid.lineTo(startgridalongxaxis,endgridalongyaxis);
        ctxgrid.stroke();
		startgridalongxaxis += unitxaxis;
	}
	var county = 0;
	while(startgridalongyaxis < endgridalongyaxis && county < 10){
		county++;
		ctxgrid.beginPath();
        ctxgrid.moveTo(tempstartgridalongxaxis, startgridalongyaxis);
        ctxgrid.lineTo(endgridalongxaxis,startgridalongyaxis);
        ctxgrid.stroke();
		startgridalongyaxis += unityaxis;
	}

	var count = 1;
	var fontSize = unityaxis*0.5;
	ctxgrid.font = fontSize+"px serif";
	ctxgrid.textAlign = "center";

	for (var j = 0; j < (county + 1); j++) {
		for (var i = 0; i < (countx + 1); i++) {
			ctxgrid.fillText(count + "", startcountx + unitxaxis * (i + 0.5), startcounty + unityaxis * (0.65 + j));
			count++;
		}
	}


	return (++countx * ++county);
}

function relocateendpoint(ctxgrid, startxy, endxy, height, width){
	var unitxaxis = Math.round(width / 20);
	var unityaxis = Math.round(height / 10);
	var targetx = unitxaxis;
	var targety = unityaxis;
	var targetxset = false;
	var targetyset = false;

	var factor = 1;
	if(endxy.x > unitxaxis*19){
		targetx = unitxaxis*19;
	}else if(endxy.x < unitxaxis){
		targetx = unitxaxis;
	}else{
		factor = Math.round(endxy.x/unitxaxis);
		targetx = unitxaxis * factor;
	}

	if(endxy.y > unityaxis*9){
		targety = unityaxis*9;
	}else if(endxy.y < unityaxis){
		targety = unityaxis;
	}else{
		factor = Math.round(endxy.y/unityaxis);
		targety = unityaxis * factor;
	}

	targetx = Math.round(targetx);
	targety = Math.round(targety);
	var targetnotreached = true;
	while(targetnotreached){
		if(endxy.x == targetx && endxy.y == targety)
			targetnotreached = false;

		if(endxy.x > targetx){
			endxy.x -= 5;
			if(endxy.x < targetx ){
				endxy.x = targetx;
			}
		}else if(endxy.x < targetx){
			endxy.x += 5;
			if(endxy.x > targetx ){
				endxy.x += targetx;
			}
		}

		if(endxy.y > targety){
			endxy.y -= 5;

			if(endxy.y < targety ){
				endxy.y = targety;
			}
		}else if(endxy.y< targety){
			endxy.y += 5;

			if(endxy.y > targety ){
				endxy.y = targety;
			}
		}

		// drawdots(ctxbase, height, width);
		ctxgrid.clearRect(0, 0, width, height);
		drawrectangle(ctxgrid, startxy, endxy);


	}

	var rectanglelength = Math.abs(startxy.x - endxy.x) / unitxaxis;
	var rectanglebreadth = Math.abs(startxy.y - endxy.y) / unityaxis;
	return (rectanglelength * rectanglebreadth);
}

function recalculatexycoordinates(ctxgrid, startxy, endxy, height, width) {
	var unitxaxis = Math.round(width / 20);
	var unityaxis = Math.round(height / 10);
	var targetx = unitxaxis;
	var targety = unityaxis;
	var targetxset = false;
	var targetyset = false;

	if(startxy.x > unitxaxis*19 && endxy.x > startxy.x){
		targetx = unitxaxis *18;
		targetxset= true;
	}else if(startxy.x > unitxaxis*19 && endxy.x < startxy.x){
		targetx = unitxaxis *19;
		targetxset = true;
	}

	if(startxy.y > unityaxis*9 && endxy.y > startxy.y){
		targety = unityaxis *8;
		targetyset = true;
	}else if(startxy.y > unityaxis*9 && endxy.y < startxy.y){
		targety = unityaxis *9;
		targetyset = true;
	}

	var factor;
	if(!targetxset){
		factor = Math.round(startxy.x/unitxaxis);


		targetx = (factor > 1)? unitxaxis * factor: (startxy.x > endxy.x)? unitxaxis *2 : unitxaxis;
		targetxset = true;
	}


	if(!targetyset){
		factor = Math.round(startxy.y/unityaxis);
		targety = (factor > 1)? unityaxis * factor: (startxy.y > endxy.y)? unityaxis *2 : unityaxis;
		targetyset = true;
	}


	targetx = Math.round(targetx);
	targety = Math.round(targety);
	var targetnotreached = true;
	while(targetnotreached && targetxset && targetyset){
		if(startxy.x == targetx && startxy.y == targety)
			targetnotreached = false;

		if(startxy.x > targetx){
			startxy.x -= 5;

			if(startxy.x < targetx ){
				endxy.x -= (startxy.x -targetx + 5);
				startxy.x = targetx;
			}else{
				endxy.x -= 5;
			}
		}else if(startxy.x < targetx){
			startxy.x += 5;

			if(startxy.x > targetx ){
				endxy.x += (startxy.x -targetx + 5);
				startxy.x = targetx;
			}else{
				endxy.x += 5;
			}
		}

		if(startxy.y > targety){
			startxy.y -= 5;

			if(startxy.y < targety ){
				endxy.y -= (startxy.y -targety + 5);
				startxy.y = targety;
			}else{
				endxy.y -= 5;
			}
		}else if(startxy.y< targety){
			startxy.y += 5;

			if(startxy.y > targety ){
				endxy.y += (startxy.y -targety + 5);
				startxy.y = targety;
			}else{
				endxy.y += 5;
			}
		}

		// drawdots(ctxbase, height, width);
		ctxgrid.clearRect(0, 0, width, height);
		drawrectangle(ctxgrid, startxy, endxy);


	}
	return endxy;
}

function drawrectangle(ctxgrid, startxy, endxy) {
	ctxgrid.beginPath();
	var linewidth = "1";


	ctxgrid.lineWidth = linewidth;
	ctxgrid.strokeStyle = "red";
	var width = endxy.x - startxy.x;
	var height = endxy.y - startxy.y;
	ctxgrid.rect(startxy.x, startxy.y, width, height);
	ctxgrid.stroke();
}

function getxycoordinates(ctx, event) {
	var rect = ctx.canvas.getBoundingClientRect();

	var returnvalue = {
		x : Math.round(event.clientX - rect.left),
		y : Math.round(event.clientY - rect.top)
	};
	return returnvalue;
}

function drawdots(ctxbase, height, width) {
	var unitxaxis = Math.round(width / 20);
	var unityaxis = Math.round(height / 10);
	ctxbase.clearRect(0, 0, width, height);

	var circleradius = 1;

	if (width > 700) {
		circleradius = 5;
	} else if (width > 300 && width <= 700) {
		circleradius = 3;
	}
	for (var i = 1; i < 20; i++) {
		for (var j = 1; j < 10; j++) {
			ctxbase.beginPath();
			ctxbase.arc(unitxaxis * i, unityaxis * j, circleradius, 0, 2 * Math.PI, false);
			ctxbase.fillStyle = 'darkgrey';
			ctxbase.fill();
		}
	}
}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [{
	//starting page
	contentblockadditionalclass: "purplebg",
	contentblocknocenteradjust : true,
	uppertextblock : [{
		textclass : "firsttitle",
		textdata : data.string.p2_s1
	}]

}, {
	//page 1
	contentblockadditionalclass: "purplebg",
	uppertextblockadditionalclass: "description1",
	uppertextblock : [
	{
		textclass : "descriptiontext",
		datahighlightflag : true,
		textdata : data.string.p2_s2
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "square",
			imgsrc : imgpath + "hand01.png"
		}]
	}]
},
{
	//page 2
	contentblockadditionalclass: "purplebg",
	uppertextblockadditionalclass: "description1",
	uppertextblock : [
	{
		textclass : "descriptiontext",
		datahighlightflag : true,
		textdata : data.string.p2_s3
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "square mapleleaf",
			imgsrc : imgpath + "mapleleaf.png"
		},{
			imgclass : "square",
			imgsrc : imgpath + "hand01.png"
		},{
			imgclass : "square bananaleaf",
			imgsrc : imgpath + "bananaleaf.png"
		}]
	}]
},
 {
 	//page 3
 	contentblockadditionalclass: "purplebg",
	uppertextblockadditionalclass: "description1",
	uppertextblock : [
	{
		textclass : "descriptiontext",
		datahighlightflag : true,
		textdata : data.string.p2_s3
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "square mapleleaf",
			imgsrc : imgpath + "mapleleaf.png"
		},{
			imgclass : "square",
			imgsrc : imgpath + "hand01.png"
		},{
			imgclass : "square bananaleaf",
			imgsrc : imgpath + "bananaleaf.png"
		}]
	}],

	lowertextblockadditionalclass: "lowerdescription",
	lowertextblock:[
		{
			textclass : "descriptiontextlower",
			datahighlightflag : true,
			textdata : data.string.p2_s4
		}
	]
}, {

	//page 4
	contentblockadditionalclass: "purplebg",
	uppertextblockadditionalclass: "description1",
	uppertextblock : [
	{
		textclass : "descriptiontext",
		datahighlightflag : true,
		textdata : data.string.p2_s5
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "square mapleleaf",
			imgsrc : imgpath + "mapleleaf.png"
		},{
			imgclass : "square",
			imgsrc : imgpath + "hand01.png"
		}]
	}],

	lowertextblockadditionalclass: "lowerdescription",
	lowertextblock:[
		{
			textclass : "descriptiontext",
			datahighlightflag : true,
			textdata : data.string.p2_s6
		},{
			textclass : "descriptiontext",
			datahighlightflag : true,
			textdata : data.string.p2_s7
		}
	]

}, {
	//page 5
	contentblockadditionalclass: "bluebg",
	imageblock :[{
		imagestoshow : [{
			imgclass : "rectangle",
			imgsrc : imgpath + "rectangle.png"
		}],
		imagelabels: [{
			imagelabelclass: "length",
			imagelabeldata: data.string.p2_s9
		}, {
			imagelabelclass: "breadth",
			imagelabeldata: data.string.p2_s10
		}]
	}],
	lowertextblockadditionalclass: "lowerdescription02",
	lowertextblock:[
		{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s8
		}
	]

},{
	//page 6
	contentblockadditionalclass: "bluebg",
	imageblock :[{
		imagestoshow : [{
			imgclass : "rectangle",
			imgsrc : imgpath + "rectangle.png"
		}],
		imagelabels: [{
			imagelabelclass: "length",
			imagelabeldata: data.string.p2_s9
		}, {
			imagelabelclass: "breadth",
			imagelabeldata: data.string.p2_s10
		}]
	}],
	lowertextblockadditionalclass: "lowerdescription02",
	lowertextblock:[
		{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s11
		},{
			textclass : "descriptiontext1",
			datahighlightflag : true,
			textdata : data.string.p2_s12
		}
	]
}, {

	//page 7
	contentblockadditionalclass: "bluebg",
	uppertextblockadditionalclass: "description02",
	uppertextblock : [
	{
		textclass : "descriptiontext1",
		datahighlightflag : true,
		textdata : data.string.p2_s13
	},{
		textclass : "descriptiontextlower1",
		datahighlightflag : true,
		textdata : data.string.p2_s11
	},{
		textclass : "descriptiontext1 animate01",
		datahighlightflag : true,
		textdata : data.string.p2_s14
	}],
	imageblock :[{
		imagestoshow : [{
			imgclass : "rectangle00",
			imgsrc : imgpath + "book.png"
		},{
			imgclass : "rectangle00",
			imgsrc : imgpath + "footballground.png"
		},{
			imgclass : "rectangle00",
			imgsrc : imgpath + "carpet01.png"
		}],
	}],
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;


	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			// {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

			// soundsicon-orange
      {id: "s2_p1", src: soundAsset+"S2_P1.ogg"},
      {id: "s2_p2", src: soundAsset+"S2_P2.ogg"},
      {id: "s2_p3", src: soundAsset+"S2_P3.ogg"},
      {id: "s2_p4", src: soundAsset+"S2_P4.ogg"},
			{id: "s2_p5", src: soundAsset+"S2_P5.ogg"},
			{id: "s2_p6", src: soundAsset+"S2_P6.ogg"},
			{id: "s2_p7", src: soundAsset+"S2_P7.ogg"},
			{id: "s2_p8_1", src: soundAsset+"S2_P8_1.ogg"},
			{id: "s2_p8_2", src: soundAsset+"S2_P8_2.ogg"},
      {id: "s2_p8_3", src: soundAsset+"S2_P8_3.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();


	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.delay(500).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);

	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		//call notifyuser
		// notifyuser($anydiv);

			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
		switch(countNext) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
			// $nextBtn.delay(500).show(0);
			sound_player_nav("s2_p"+(countNext+1));
		break;
		case 7:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s2_p8_1");
			current_sound.play();
			current_sound.on('complete', function(){
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s2_p8_2");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p8_3");
					current_sound.play();
					current_sound.on('complete', function(){
							nav_button_controls(0);
					});
				});
			});
			// setTimeout(function() {
			// 	ole.footerNotificationHandler.pageEndSetNotification();
			// }, 4000);

		break;
		default:
			sound_player_nav("s2_p"+(countNext+1));
		break;
		}

		/*
		switch(countNext){
					case 2:
					case 6:
					case 8:

					var correctanswer;
					if(countNext == 2){
						correctanswer = 1;
					} else if(countNext == 6) {
						//TODO: change the image for page 6
						correctanswer = 2;
					} else{
						// TODO: change the image for page 7
						correctanswer = 9;
					}
					$nextBtn.hide(0);
					var $correctsign = $(".correct01");
					var $incorrectsign = $(".incorrect01");
					$correctsign.hide(0);
					$incorrectsign.hide(0);
					var $areainput = $(".areainput");
					var $areacheck = $(".areacheck");
					var $areahint = $(".areahint");

					$areainput.keydown(function (evt) {
						var charVal = parseInt(evt.key);
						var prevValue = parseInt(evt.target.value)*10 + charVal;
						var charCode = (evt.which) ? evt.which : evt.keyCode;
						   if ((charCode > 31 && (charCode < 48 || charCode > 57)) && isNaN(charVal)){
							// console.log("inside");
							   // console.log(evt.target.value);
							return false;
						}
												 if(prevValue > 999){
							return false;
						}
						// if(isNaN(prevValue)){
							// prevValue = charVal;
						// }else{
							// prevValue += charVal;
						// }
						// var returnvalue = prevValue+"cm&sup2;";
						// evt.target.value = returnvalue;
						// return evt.preventDefault();
						return true;
					} );

					$areacheck.click(function(){
						var input = parseInt($areainput[0].value);
						if(input != correctanswer){
							$correctsign.hide(0);
							$incorrectsign.show(0);
							if (!$areacheck.hasClass("incorrect02"))
								$areacheck.toggleClass("incorrect02");

							if (!$areahint.hasClass("vibrate")){
								$areahint.toggleClass("vibrate");
							}
						}else{
							$incorrectsign.hide(0);
							$correctsign.show(0);
							if(countNext != 8)
								$nextBtn.show(0);
							$areacheck.toggleClass("correct02");
							if ($areahint.hasClass("vibrate")){
								$areahint.toggleClass("vibrate");
							}

						}

					});

					$areahint.click(function() {
						if(countNext != 8){
							$nextBtn.trigger("click");
						}else{
							//TODO: code to chagne image goes here
						}

					});

						break;

					default:
						break;
				}*/


	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_player_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			nav_button_controls();
		});
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

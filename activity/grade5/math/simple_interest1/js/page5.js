var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'fullbg bgbrd',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "squirrel1",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-4',
			textdata : data.string.p5s0,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata: data.string.p5s1,
			textclass: "p1s2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "hasta s2",
					imgid : 'hasta',
					imgsrc : '',
				},
				{
					imgclass : "fullbg",
					imgid : 'bankbg',
					imgsrc : '',
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "zoombg",
					imgsrc : '',
					imgid : 'bankdai'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5s3,
			textclass : 'txtzm  lateshow',
			imgclass: 'box1  lateshow',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'bankerTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p5s4,
			textclass : 'txt1',
			imgclass: 'box2',
			imgid : 'box2',
			imgsrc: '',
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'hastaTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5s5,
			textclass : 'txt2',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'bankerTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p5s6,
			textclass : 'txt1',
			imgclass: 'box2',
			imgid : 'box2',
			imgsrc: '',
		}],
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'hastaTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5s7,
			textclass : 'txt2',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'bankerTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p5s8,
			textclass : 'txt1',
			imgclass: 'box2',
			imgid : 'box2',
			imgsrc: '',
		}],
	},
// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'fullbg bgbrd',
		speechbox:[{
			speechbox: 'sp-board',
			textdata : data.string.p5s9,
			textclass : 'brdl txtb',
			imgclass: 'box-brd',
			imgid : 'board',
			imgsrc: '',
		}]
		/*
		imageblock:[{
					imagestoshow : [
						{
							imgclass : "boardbg",
							imgsrc : '',
							imgid : 'board'
						}
					]
				}],
				extratextblock:[
					{
						textdata: data.string.p5s9,
						textclass: "boardtxt",
					}
				],*/

	},
	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'hastaTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5s10,
			textclass : 'txt2',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'hastaTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p5s11,
			textclass : 'txt2',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
		}],
	},
	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fullbg",
					imgsrc : '',
					imgid : 'bankerTlk'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p5s12,
			textclass : 'txt1',
			imgclass: 'box2',
			imgid : 'box2',
			imgsrc: '',
		}],
	},
// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'fullbg bgbrd',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "squirrel1",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}],
		speechbox:[{
			speechbox: 'sp-4',
			textdata : data.string.p5s13,
			textclass : 'txt1',
			imgclass: 'box1',
			imgid : 'box1',
			imgsrc: '',
            datahighlightflag:true,
            datahighlightcustomclass:"fadeInlater"
		}],
	},
// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'fullbg bgbrd',
		speechbox:[{
			speechbox: 'sp-board',
			textdata : data.string.p5s14,
			textclass : 'brd txtb',
			imgclass: 'box-brd',
			imgid : 'board',
			imgsrc: '',
            datahighlightflag:true,
            datahighlightcustomclass:"underline"
		}]
		/*
		imageblock:[{
					imagestoshow : [
						{
							imgclass : "boardbg",
							imgsrc : '',
							imgid : 'board'
						}
					]
				}],
				extratextblock:[
					{
						textdata: data.string.p5s14,
						textclass: "boardtxt-s14txt",
					}
				],*/

	},
// slide15
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'fullbg bgbrd',
		speechbox:[{
			speechbox: 'sp-board',
			textdata : data.string.p5s15,
			textclass : 'brd txtl',
			imgclass: 'box-brd',
			imgid : 'board',
			imgsrc: '',
		}]
/*
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boardbg",
					imgsrc : '',
					imgid : 'board'
				}
			]
		}],
		extratextblock:[
			{
				textdata: data.string.p5s15,
				textclass: "boardtxt-s15",
			}
		],*/

	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"cover_simpleinterest.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hasta", src: imgpath+"suraj hancy-14.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankbg", src: imgpath+"bank.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankdai", src: imgpath+"suraj-10.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bankerTlk", src: imgpath+"suraj-at-bank-2-12.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hastaTlk", src: imgpath+"suraj-at-bank-3-13.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box1", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			{id: "box2", src: imgpath+"textbox01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "board", src: imgpath+"board.png", type: createjs.AbstractLoader.IMAGE},
			{id: "squirrel", src: imgpath+"squirrel re-design-08.png", type: createjs.AbstractLoader.IMAGE},


			// soundsa
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p10", src: soundAsset+"s5_p10.ogg"},
			{id: "s5_p11", src: soundAsset+"s5_p11.ogg"},
			{id: "s5_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p15", src: soundAsset+"s5_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);
		switch(countNext) {
			case 2:
				setTimeout(function(){
					sound_player("s5_p"+(countNext+1),1);
				},3000);
			break;
			case 8:
				$prevBtn.show(0);
				$nextBtn.show(0);
				break;
			case 12:
				sound_player("s5_p"+(countNext+1),1);
        $(".fadeInlater").hide();
        setTimeout(function(){
            $(".fadeInlater").fadeIn();
        },2000);
			break;
			case 13:
				nav_button_controls(300);
			break;
			default:
				sound_player("s5_p"+(countNext+1),1);
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls(300):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//alert("imgsrc---"+image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var content = [{
        //starting page1
        contentblockadditionalclass: "firstpagebackground",
        contentblocknocenteradjust: true,
        uppertextblock: [{
            textclass: "firsttitle",
            textdata: data.string.firsttitle
        }],
        imageblock: [{
            imagestoshow: [{
                imgclass: "firstpageimage",
                imgsrc: imgpath + "elephant.gif"
            }]
        }],
    },
    //page2
    {
        contentblockadditionalclass: "mainTitle",
        imageblock: [{
            imagestoshow: [{
                imgclass: "angle1 animated",
                imgsrc: imgpath + "30.png"
            },
                {
                    imgclass: "angle1 animated fadeIn",
                    imgsrc: imgpath + "angle30_a.png"
                }
            ]
        }],
        uppertextblock: [{
            textclass: "angleIntro animated zoomIn",
            textdata: data.string.thisis,
        }]
    },

    {
        //page 3
        contentblockadditionalclass: "mainTitle2",
        uppertextblockadditionalclass: "questionBlock",
        imageblock: [{
            imagestoshow: [{
                imgclass: "angle1",
                imgsrc: imgpath + "30.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p1text1,
        inputbox: [{
            inputtypeclass: "firstinput",
            inputid: "valid",
            datahere: "ABC",
            keyupfunction: "allowAlpha();",
            inputtype: "text",
            inputspanvalue: "∠",
            maxlength: 3,
        }, {
            inputtype: "button",
            inputid: "check",
            inputtypevalue: data.string.check,
            inputtypeclass: "checkButton"
        }, {
            inputtype: "button",
            inputid: "hint",
            inputtypevalue: data.string.hintofans,
            inputtypeclass: "hintButton"
        }]
    },
    //page4
    {
        contentblockadditionalclass: "mainTitle2",
        imageblock: [{
            imagestoshow: [{
                imgclass: "angle1",
                imgsrc: imgpath + "30.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p1text3,
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            plcholdertxt: "ABC",
            inputspanvalue: "∠",
            maxlength: 3
        }],
        uppertextblock: [{
            textclass: "hintAns animated zoomIn",
            textdata: data.string.p1text2,
        }]
    },

    //5thpage
    {
        contentblockadditionalclass: "mainTitle2",
        imageblock: [{
            imagestoshow: [{
                imgclass: "angle1",
                imgsrc: imgpath + "125.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p1text1,
        inputbox: [{
            inputtypeclass: "firstinput",
            inputtype: "text",
            inputid: "valid",
            datahere: "ABC",
            keyupfunction: "allowAlpha();",
            inputspanvalue: "∠",
            maxlength: 3,

        }, {
            inputtype: "button",
            inputid: "check",
            inputtypevalue: data.string.check,
            inputtypeclass: "checkButton"
        }, {
            inputtype: "button",
            inputid: "hint",
            inputtypevalue: data.string.hintofans,
            inputtypeclass: "hintButton"
        }],
        uppertextblock: [{
            textclass: "hintAns animated zoomIn",
            textdata: data.string.p1text4,
        }]
    },
    //6thslide
    {
        contentblockadditionalclass: "mainTitle2",
        imageblock: [{
            imagestoshow: [{
                imgclass: "angle1",
                imgsrc: imgpath + "125.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p1text5,
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            plcholdertxt: "MNO",
            inputspanvalue: "∠",
            maxlength: 3
        }],

    },

];

$(function() {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    var $total_page = content.length;

    var boxquestionflag = false;
    var finalpageflag = false;
    var boxquestioncountnext = 0;
    loadTimelineProgress($total_page, countNext + 1);


    	var index = 0;
    	var preload;
    	var timeoutvar = null;
    	var current_sound;

    	function init() {
    		//specify type otherwise it will load assests as XHR
    		manifest = [
    			//images

    			// sounds
    			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
    			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
    			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
    		];
    		preload = new createjs.LoadQueue(false);
    		preload.installPlugin(createjs.Sound);//for registering sounds
    		preload.on("progress", handleProgress);
    		preload.on("complete", handleComplete);
    		preload.on("fileload", handleFileLoad);
    		preload.loadManifest(manifest, true);
    	}
    	function handleFileLoad(event) {
    		// console.log(event.item);
    	}
    	function handleProgress(event) {
    		$('#loading-text').html(parseInt(event.loaded*100)+'%');
    	}
    	function handleComplete(event) {
    		$('#loading-wrapper').hide(0);
    		//initialize varibales
    		// call main function
    		templateCaller();
    	}
    	//initialize
    	init();

    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    }

    /*=====  End of user navigation controller function  ======*/

    /*==================================================
    =            InstructionBlockController            =
    ==================================================*/
    /**
     How to:
     - Just call instructionblockcontroller() from the template
     */

    /**
     What it does:
     - It inserts and handles closing and opening of instruction block
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function instructionblockcontroller() {
        var $instructionblock = $board.find("div.instructionblock");
        if ($instructionblock.length > 0) {
            var $contentblock = $board.find("div.contentblock");
            var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
            var instructionblockisvisibleflag;

            $contentblock.css('pointer-events', 'none');

            $toggleinstructionblockbutton.on('click', function() {
                instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
                if (instructionblockisvisibleflag == 'true') {
                    instructionblockisvisibleflag = 'false';
                    $contentblock.css('pointer-events', 'auto');
                } else if (instructionblockisvisibleflag == 'false') {
                    instructionblockisvisibleflag = 'true';
                    $contentblock.css('pointer-events', 'none');
                }

                $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
            });
        }
    }

    /*=====  End of InstructionBlockController  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        if (countNext < 8) {
            var html = template(content[countNext]);
            $board.html(html);
        }
        vocabcontroller.findwords(countNext);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        //call instruction block controller
        instructionblockcontroller($board);

        //call notifyuser
        // notifyuser($anydiv);

        // find if there is linehorizontal div in the slide
        var $linehorizontal = $board.find("div.linehorizontal");
        if ($linehorizontal.length > 0) {
            $linehorizontal.attr('data-isdrawn', 'draw');
        }


        var canvasBlock = $('.canvasblock');
        var canvas = $('#canvas-first');

        // for last slide, canvas is not loaded so
        // validate canvas[0] object
        if (canvas[0]) {
            canvas[0].height = canvasBlock.height();
            canvas[0].width = canvasBlock.width();
            var ctx = canvas[0].getContext('2d');

            // For moving line
            var movingStart = {
                x: canvas.width() * 0.3,
                y: canvas.height() * 0.6
            }
            var movingEnd = {
                x: canvas.width() * 0.7,
                y: canvas.height() * 0.2
            }

            // For base line
            var baseStart = {
                x: canvas.width() * 0.3,
                y: canvas.height() * 0.8
            }
            var baseEnd = {
                x: canvas.width() * 0.8,
                y: canvas.height() * 0.8
            }

            var alpha = 0;
            var angle = 0;

            function drawBaseLine() {
                ctx.beginPath();
                ctx.moveTo(baseStart.x, baseStart.y);
                ctx.lineTo(baseEnd.x, baseEnd.y);
                ctx.lineWidth = 3;
                ctx.strokeStyle = "#000000";
                ctx.stroke();
            }

            function drawSecondLine() {
                ctx.beginPath();
                ctx.moveTo(movingStart.x, movingStart.y);
                ctx.lineTo(movingEnd.x, movingEnd.y);
                ctx.lineWidth = 3;
                ctx.strokeStyle = "#000000";
                ctx.stroke();
            }
        }



        switch (countNext) {
            case 0:
                sound_player("sound_0");
                $(".textblock").css({
                    "top": "0%"
                })
                break;
            case 1:
                setTimeout(function(){
                sound_player("sound_1");
              },2000);
                $(".textblock").css({
                    "top": "35%",
                    "width": "50%",
                    "left": "47%"
                })
                break;

            case 2:
              sound_player("sound_2");
                checkingAnswer("abc");
                allowAlpha();
                showHint();
                break;
            case 3:
            $prevBtn.show(0);
            case 5:
                $(".inputfields").css({
                    "background": "none"
                })
                $(".question").css({
                    "top": "12%"
                })
                if (countNext == 3) {
                    setTimeout(function() {
                        $nextBtn.show(0);
                    }, 2000);
                }
                if (countNext == 5) {
                    setTimeout(function() {
                      $prevBtn.show(0);
                        ole.footerNotificationHandler.pageEndSetNotification();
                    }, 1000);
                }
                break;
            case 4:
                checkingAnswer("mno");
                allowAlpha();
                showHint();
                break;
            default:
                break;
        }
    }
    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
        if(countNext==2 || countNext==4){
          $nextBtn.hide(0);
          $prevBtn.hide(0);
        }else{
          nav_button_controls(0);
        }
      });
    }

    function allowAlpha() {
        $("#valid").keyup(function(e) {
            // a-z => allow all lowercase alphabets
            // A-Z => allow all uppercase alphabets
            var regex = /^[a-zA-Z]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
            //alert if not true
            //alert("Invalid Input");
            // You can replace the invalid characters by:
                this.value = this.value.replace(/[^a-zA-Z]+/, '');
        });
    }

    function showHint() {
        $("#hint").click(function() {
            console.log("they clicked me");
            if (countNext <= 2) {
                $nextBtn.trigger("click");
                $('.hintAns').show(0);
            } else {

                $('.hintAns').show(0);
            }
        });
    }

    function checkingAnswer(ans1) {
        ans1 = ans1.toUpperCase();
        var rev = ans1.split("").reverse().join("");
        console.log(ans1, rev);
        $(".inputfields").on('click', "#check", function() {
            var ans = $("#valid").val().toUpperCase();
            console.log(ans1, rev, ans);
            play_correct_incorrect_sound(1);
            if (ans == ans1 || ans == rev) {
                console.log("correct")
                $(".wrongimg").css({
                    "display": "none"
                })
                $(".inputfields").append("<span class ='correctimg animated fadeIn'></span>").done(
                    setTimeout(function() {
                        $nextBtn.trigger("click");
                    }, 1200));

            } else {
              play_correct_incorrect_sound(0);
                console.log("incorrect")
                $(".inputfields").append("<span class ='wrongimg animated fadeIn'></span>");
            }
        })
    }
    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */
    function templateCaller() {
        if (boxquestionflag) {
            switch (boxquestioncountnext) {

                case 1:
                    $(".boxquestion1").hide(0);
                    $(".boxanswer1").show(0);
                    break;
                    /*
                    TODO: the homer dialog response in the mockup goes here
                    case 2:
                    					$(".boxquestion2").show(0);
                    					$(".boxanswer1").hide(0);
                    					 break;
                    */

                case 2:
                    $(".boxquestion2").show(0);
                    $(".boxanswer1").hide(0);
                    break;
                case 3:
                    $(".boxquestion2").hide(0);
                    $(".boxanswer2").show(0);
                    // break;
                    // case 4:
                    // $(".boxanswer2").hide(0);
                    // $(".boxanswer3").show(0);
                    boxquestionflag = false;
                    boxquestioncountnext = 0;

                    break;
                default:
                    boxquestioncountnext = 0;
                    boxquestionflag = false;
                    break;
            }
        } else {

            /*always hide next and previous navigation button unless
             explicitly called from inside a template*/
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // call navigation controller
            navigationcontroller();

            // call the template
            generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


            //call the slide indication bar handler for pink indicators
            loadTimelineProgress($total_page, countNext + 1);

            // just for development purpose to see total slide vs current slide number
            // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        if (boxquestionflag) {
            boxquestioncountnext++;
        } else {
            countNext++;
        }
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

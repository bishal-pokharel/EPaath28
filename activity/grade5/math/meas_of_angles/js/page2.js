var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var content = [
    //page1
    {
        contentblockadditionalclass: "mainTitle",
        uppertextblock: [{
            textclass: "guidetext animated zoomIn",
            textdata: data.string.p2text1
        }],
        imageblock: [{
            imagetoshow: [{
                    imgclass: "angle1 animated fadeIn",
                    imgsrc: imgpath + "30.png"
                },
                {
                    imgclass: "angle2 animated fadeIn",
                    imgsrc: imgpath + "125.png"
                }
            ]

        }]
    },
    //2nd
    {
        contentblockadditionalclass: "mainTitle",
        uppertextblock: [{
            textclass: "guidetext2 animated zoomIn",
            textdata: data.string.p2text2
        }],
        imageblock: [{
            imagetoshow: [{
                    imgclass: "angle1 animated fadeIn",
                    imgsrc: imgpath + "30.png"
                },
                {
                    imgclass: "angle2 animated fadeIn",
                    imgsrc: imgpath + "125.png"
                }
            ]
        }]
    },
    //3rd
    {
        contentblockadditionalclass: "mainTitle2",
        hascanvasblock: true,
        canvasid: "canvas-first",
        uppertextblock: [{
            textclass: "guidetext2 animated fadeIn",
            textdata: data.string.p2guess
        }],

        inputfields: true,
        textclass: "question",
        inputbox: [{
                inputtypeclass: "firstinput",
                inputid: "valid",
                keyupfunction: "allowNum();",
                inputtype: "text",
                inputspanvalue: "∠ABC = ",
                maxlength: 2,
            },

            {
                inputtype: "button",
                inputid: "check",
                inputspanvalue:"°",
                addspanclass:"thirdspan",
                inputtypevalue: data.string.submit,
                inputtypeclass: "checkButton"
            }
        ]
    },
    //4th page
    {
        contentblockadditionalclass: "mainTitle2",
        hascanvasblock: true,
        canvasid: "canvas-first",
        uppertextblock: [{
            textclass: "guidetext2 animated fadeIn",
            textdata: data.string.p2guess_ans
        }],
        inputfields: true,
        textclass: "question",
        inputbox: [{
            inputtypeclass: "firstinput",
            doweneedit: "disabled",
            plcholdertxt: "30°",
            inputid: "valid",
            keyupfunction: "allowNum();",
            inputtype: "text",
            inputspanvalue: "∠ABC = ",
            maxlength: 2,
        }]
    },

    //5th page
    {
        contentblockadditionalclass: "mainTitle2",
        uppertextblockadditionalclass: "titleblock",
        uppertextblock:[{
          textclass:"headertitle",
          textdata:data.string.headtext
        }],
        //protactorpart
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle-1",
                imgsrc: imgpath + "angle30_b.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p2_ques,
        inputbox: [{
                inputtypeclass: "firstinput",
                inputid: "valid",
                keyupfunction: "allowNum();",
                inputtype: "text",
                inputspanvalue: "∠ABC = ",
                maxlength: 2,
            },

            {
                inputtype: "button",
                inputid: "check",
                inputtypevalue: data.string.check,
                inputtypeclass: "checkButton"
            },

            {
                inputtype: "button",
                inputid: "hint",
                inputspanvalue:"°",
                addspanclass:"thirdspan",
                inputtypevalue: data.string.hintofans,
                inputtypeclass: "hintButton"
            }
        ]
    },

    //6th page
    {
        //protactorpart
        contentblockadditionalclass: "mainTitle2",
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle-1",
                imgsrc: imgpath + "angle30_b.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            inputspanvalue: "∠ABC = ",
            plcholdertxt: "30°",
            maxlength: 2
        }]
    },

    //7th page
    {
        contentblockadditionalclass: "mainTitle2",
        uppertextblockadditionalclass: "titleblock",
        uppertextblock:[{
          textclass:"headertitle",
          textdata:data.string.headtext
        }],
        //protactorpart
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "80.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p2_ques,
        inputbox: [{
                inputtypeclass: "firstinput",
                inputid: "valid",
                keyupfunction: "allowNum();",
                inputtype: "text",
                inputspanvalue: "∠ABC = ",
                maxlength: 2,
            },

            {
                inputtype: "button",
                inputid: "check",
                inputspanvalue:"°",
                addspanclass:"thirdspan",
                inputtypevalue: data.string.check,
                inputtypeclass: "checkButton"
            },

            {
                inputtype: "button",
                inputid: "hint",
                inputtypevalue: data.string.hintofans,
                inputtypeclass: "hintButton"
            }
        ]
    },

    //8th page
    {
        //protactorpart
        contentblockadditionalclass: "mainTitle2",
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "80.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            inputspanvalue: "∠ABC = ",
            plcholdertxt: "80°",
            maxlength: 2
        }]
    },

    //9th page
    {
        contentblockadditionalclass: "mainTitle2",
        uppertextblockadditionalclass: "titleblock",
        uppertextblock:[{
          textclass:"headertitle",
          textdata:data.string.headtext
        }],
        //protactorpart
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "40.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p2_ques,
        inputbox: [{
                inputtypeclass: "firstinput",
                inputid: "valid",
                keyupfunction: "allowNum();",
                inputtype: "text",
                inputspanvalue: "∠ABC = ",
                maxlength: 2,
            },

            {
                inputtype: "button",
                inputid: "check",
                inputspanvalue:"°",
                addspanclass:"thirdspan",
                inputtypevalue: data.string.check,
                inputtypeclass: "checkButton"
            },

            {
                inputtype: "button",
                inputid: "hint",
                inputtypevalue: data.string.hintofans,
                inputtypeclass: "hintButton"
            }
        ]
    },

    //10th page
    {
        //protactorpart
        contentblockadditionalclass: "mainTitle2",
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "40.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            inputspanvalue: "∠ABC = ",
            plcholdertxt: "40°",
            maxlength: 2
        }]
    },
    //11th page
    {
        contentblockadditionalclass: "mainTitle2",
        uppertextblockadditionalclass: "titleblock",
        uppertextblock:[{
          textclass:"headertitle",
          textdata:data.string.headtext
        }],
        //protactorpart
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "angle120_new.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        textdata: data.string.p2_ques,
        inputbox: [{
                inputtypeclass: "firstinput",
                inputid: "valid",
                keyupfunction: "allowNum();",
                inputtype: "text",
                inputspanvalue: "∠ABC = ",
                maxlength: 3,
            },

            {
                inputtype: "button",
                inputid: "check",
                inputspanvalue:"°",
                addspanclass:"thirdspan",
                inputtypevalue: data.string.check,
                inputtypeclass: "checkButton"
            },

            {
                inputtype: "button",
                inputid: "hint",
                inputtypevalue: data.string.hintofans,
                inputtypeclass: "hintButton"
            }
        ]
    },
    //12th page
    {
        //protactorpart
        contentblockadditionalclass: "mainTitle2",
        measureangle: true,
        instrumentclass: "draggable_wp",
        //end
        imageblock: [{
            imagetoshow: [{
                imgclass: "small-angle",
                imgsrc: imgpath + "120-str.png"
            }]
        }],
        inputfields: true,
        textclass: "question",
        inputbox: [{
            inputtype: "text",
            doweneedit: "disabled",
            inputtypeclass: "firstinput",
            inputspanvalue: "∠ABC = ",
            plcholdertxt: "120°",
            maxlength: 2
        }]
    }

];

$(function() {
    $(window).resize(function() {
        // recalculateHeightWidth();
        generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

    });
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;

    var boxquestionflag = false;
    var finalpageflag = false;
    var boxquestioncountnext = 0;

    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();

    loadTimelineProgress($total_page, countNext + 1);
  	var index = 0;
    var preload;
    var timeoutvar = null;
    var current_sound;

    function init() {
      //specify type otherwise it will load assests as XHR
      manifest = [
        //images

        // sounds
        {id: "sound_0", src: soundAsset+"s2_p1.ogg"},
        {id: "sound_1", src: soundAsset+"s2_p2.ogg"},
        {id: "sound_2", src: soundAsset+"s2_p3.ogg"},
        // {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
        {id: "sound_3", src: soundAsset+"s2_p5_1.ogg"},
        {id: "sound_4", src: soundAsset+"s2_p5_2.ogg"},
      ];
      preload = new createjs.LoadQueue(false);
      preload.installPlugin(createjs.Sound);//for registering sounds
      preload.on("progress", handleProgress);
      preload.on("complete", handleComplete);
      preload.on("fileload", handleFileLoad);
      preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
      // console.log(event.item);
    }
    function handleProgress(event) {
      $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
      $('#loading-wrapper').hide(0);
      //initialize varibales
      // call main function
      templateCaller();
    }
    //initialize
    init();


    /*==================================================
     =            Handlers and helpers Block            =
     ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    /*===============================================
    =            data highlight function            =
    ===============================================*/
    /**

     What it does:
     - send an element where the function has to see
     for data to highlight
     - this function searches for all nodes whose
     data-highlight element is set to true
     -searches for # character and gives a start tag
     ;span tag here, also for @ character and replaces with
     end tag of the respective
     - if provided with data-highlightcustomclass value for highlight it
     applies the custom class or else uses parsedstring class

     E.g: caller : texthighlight($board);
     */
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) : (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    /*=====  End of data highlight function  ======*/

    /*===============================================
    =            user notification function        =
    ===============================================*/
    /**
     How to:
     - First set any html element with
     "data-usernotification='notifyuser'" attribute,
     and "data-isclicked = ''".
     - Then call this function to give notification
     */

    /**
     What it does:
     - You send an element where the function has to see
     for data to notify user
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function notifyuser($notifyinside) {
        //check if $notifyinside is provided
        typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

        /*variable that will store the element(s) to remove notification from*/
        var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
        // if there are any notifications removal required add the event handler
        if ($allnotifications.length > 0) {
            $allnotifications.one('click', function() {
                /* Act on the event */
                $(this).attr('data-isclicked', 'clicked');
                $(this).removeAttr('data-usernotification');
            });
        }
    }

    /*=====  End of user notification function  ======*/

    /*======================================================
    =            Navigation Controller Function            =
    ======================================================*/
    /**
     How To:
     - Just call the navigation controller if it is to be called from except the
     last page of lesson
     - If called from last page set the islastpageflag to true such that
     footernotification is called for continue button to navigate to exercise
     */

    /**
     What it does:
     - If not explicitly overriden the method for navigation button
     controls, it shows the navigation buttons as required,
     according to the total count of pages and the countNext variable
     - If for a general use it can be called from the templateCaller
     function
     - Can be put anywhere in the template function as per the need, if
     so should be taken out from the templateCaller function
     - If the total page number is
     */

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

    }

    /*=====  End of user navigation controller function  ======*/

    /*==================================================
    =            InstructionBlockController            =
    ==================================================*/
    /**
     How to:
     - Just call instructionblockcontroller() from the template
     */

    /**
     What it does:
     - It inserts and handles closing and opening of instruction block
     - this function searches for all text nodes whose
     data-usernotification attribute is set to notifyuser
     - applies event handler for each of the html element which
     removes the notification style.
     */
    function instructionblockcontroller() {
        var $instructionblock = $board.find("div.instructionblock");
        if ($instructionblock.length > 0) {
            var $contentblock = $board.find("div.contentblock");
            var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
            var instructionblockisvisibleflag;

            $contentblock.css('pointer-events', 'none');

            $toggleinstructionblockbutton.on('click', function() {
                instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
                if (instructionblockisvisibleflag == 'true') {
                    instructionblockisvisibleflag = 'false';
                    $contentblock.css('pointer-events', 'auto');
                } else if (instructionblockisvisibleflag == 'false') {
                    instructionblockisvisibleflag = 'true';
                    $contentblock.css('pointer-events', 'none');
                }

                $instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
            });
        }
    }

    /*=====  End of InstructionBlockController  ======*/

    /*=====  End of Handlers and helpers Block  ======*/

    /*=======================================
     =            Templates Block            =
     =======================================*/
    /*=================================================
     =            general template function            =
     =================================================*/
    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        if (countNext < content.length) {
            var html = template(content[countNext]);
            $board.html(html);
        }
        vocabcontroller.findwords(countNext);


        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        //call instruction block controller
        instructionblockcontroller($board);

        //call notifyuser
        // notifyuser($anydiv);

        // find if there is linehorizontal div in the slide
        var $linehorizontal = $board.find("div.linehorizontal");
        if ($linehorizontal.length > 0) {
            $linehorizontal.attr('data-isdrawn', 'draw');
        }
        var canvasBlock = $('.canvasblock');
        var canvas = $('#canvas-first');

        // for last slide, canvas is not loaded so
        // validate canvas[0] object
        if (canvas[0]) {
            canvas[0].height = canvasBlock.height();
            canvas[0].width = canvasBlock.width();
            var ctx = canvas[0].getContext('2d');
            // ctx.style.border = "1px solid black";


            //For third line
            var thirdStart = {
                x: canvas.width() * 0.1,
                y: canvas.height() * 0.8
            }

            var thirdEnd = {
                x: canvas.width() * 0.8,
                y: canvas.height() * 0.2
            }

            // For second line
            var secondStart = {
                x: canvas.width() * 0.1,
                y: canvas.height() * 0.8
            }

            var secondEnd = {
                x: canvas.width() * 0.8,
                y: canvas.height() * 0.1
            }

            // For base line
            var baseStart = {
                x: canvas.width() * 0.1,
                y: canvas.height() * 0.8
            }
            var baseEnd = {
                x: canvas.width() * 0.8,
                y: canvas.height() * 0.8
            }

            var alpha = 0;
            var angle = 0;

            function drawBaseLine() {
                ctx.beginPath();
                ctx.moveTo(baseStart.x, baseStart.y);
                ctx.lineTo(baseEnd.x, baseEnd.y);
                ctx.lineWidth = 3;
                ctx.strokeStyle = "#000000";
                ctx.stroke();
            }

            function drawSecondLine() {

                ctx.save();
                ctx.beginPath();
                ctx.translate(baseStart.x, baseStart.y);
                ctx.rotate(-(30) * (Math.PI / 180));
                ctx.lineWidth = 3;
                ctx.moveTo(0, 0);
                ctx.lineTo(baseEnd.x - 12, 0);
                ctx.strokeStyle = "#000000";
                ctx.stroke();


                ctx.restore();

                // ctx.beginPath();
                // ctx.moveTo(baseStart.x, baseStart.y);
                // ctx.rotate(30 * Math.PI /180);
                // ctx.lineTo(baseEnd.x,0)
                // ctx.lineWidth=3;
                // ctx.strokeStyle="red";
                // ctx.stroke();
            }

            function drawThirdLine() {}
        }




        switch (countNext) {
            case 0:
            sound_player("sound_0");
            break;
            case 1:
            sound_player("sound_1");
            break;
            case 2:
            sound_player("sound_2");
                drawBaseLine();
                drawSecondLine();
                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })
                $(".checkButton").css({
                    "left": "41%"
                })
                allowNumbs();
                guessAngle(30);
                break;

            case 3:


                newAngle();

                function newAngle() {

                    drawBaseLine();
                    drawSecondLine();

                    ctx.save();
                    ctx.beginPath();
                    ctx.translate(baseStart.x, baseStart.y);
                    ctx.rotate(-(inputVal) * (Math.PI / 180));
                    ctx.lineWidth = 3;
                    ctx.moveTo(0, 0);
                    ctx.lineTo(baseEnd.x - 12, 0);
                    ctx.strokeStyle = "red";
                    ctx.stroke();


                    ctx.restore();




                }
                if (offVal == "correct") {
                    $(".guidetext2").text(data.string.correct);
                }
                else if(offVal=="offVal1"){
                  $(".guidetext2").text(data.string.smalltext + " " + offVal1 + "°" + data.string.fullstop1);
                }
                else
                {
                    $(".guidetext2").text(data.string.offtext + " " + offVal + "°" + data.string.fullstop);
                }


                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })
                $(".inputfields").css({
                    "background": "none"
                })

                setTimeout(function() {
                  nav_button_controls(0);
                }, 1200)
                break;

            case 4:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_3");
            current_sound.play();
            current_sound.on("complete", function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_4");
              current_sound.play();
            });
                rotateProtactor();
                $(".draggable_wp").css({
                    "transform": "rotate(-23deg)",
                    "left":"5.2%",
                    "top":'41.5%'
                })
                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })
                allowNumbs();
                showHint();
                checkingAnswer(30);

                break;

            case 5:
                rotateProtactor();
                $(".draggable_wp").css({
                    "transform": "rotate(0.222894deg)",
                    "left":"5.2%",
                    "top":'41.5%'

                })
                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                $(".inputfields").css({
                    "background": "none"
                })

                setTimeout(function() {
                  nav_button_controls(0);
                }, 1200)
                break;
            case 6:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_3");
            current_sound.play();
            current_sound.on("complete", function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_4");
              current_sound.play();
            });
                rotateProtactor();
                $(".draggable_wp").css({
                  "left": "5.2%",
                  "top": "49.1%",
                  "transform" : "rotate(7.4354deg)"
                })

                $(".small-angle").css({
                    "height": "64%"
                })

                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                allowNumbs();
                checkingAnswer(80);
                showHint(-383.102, -365.102);

                break;


            case 7:
                rotateProtactor();
                $(".draggable_wp").css({
                  "left": "5.2%",
                  "top": "49.1%",
                  "transform" : "rotate(-4.20742deg)"
                })

                $(".small-angle").css({
                    "height": "64%"
                })

                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                $(".inputfields").css({
                    "background": "none"
                })

                setTimeout(function() {
                  nav_button_controls(0);
                }, 1200)
                break;

            case 8:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_3");
            current_sound.play();
            current_sound.on("complete", function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_4");
              current_sound.play();
            });
                rotateProtactor();
                $(".draggable_wp").css({
                  "transform" : "rotate(-12.608deg)",
                  "left":" 5.2%",
                  "top": "49.2%"
                })

                $(".small-angle").css({
                    "height": "64%"
                })

                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                allowNumbs();
                checkingAnswer(40);
                showHint(-383.102, -364.102);
                break;

            case 9:
                rotateProtactor();
                $(".draggable_wp").css({
                    "transform": " rotate(-3.94628deg)",
                    "left":" 5.2%",
                    "top": "49.2%"
                })

                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })
                $(".small-angle").css({
                    "height": "64%"
                })

                $(".inputfields").css({
                    "background": "none"
                })

                setTimeout(function() {
                  nav_button_controls(0);
                }, 1200)
                break;

            case 10:
            createjs.Sound.stop();
            current_sound = createjs.Sound.play("sound_3");
            current_sound.play();
            current_sound.on("complete", function(){
              createjs.Sound.stop();
              current_sound = createjs.Sound.play("sound_4");
              current_sound.play();
            });
                rotateProtactor();
                $(".draggable_wp").css({
                    "transform": "rotate(-29.121deg)",
                    "left": "7.6%",
                    "top": "48.4%"
                })

                $(".small-angle").css({
                    "height": "62.9%",
                    "width": "73%",
                    "left": "16%"
                })

                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                allowNumbs();
                checkingAnswer(120);
                showHint(-29.121,0.879);
                break;
            case 11:
                rotateProtactor();
                $(".draggable_wp").css({
                  "transform":" rotate(0.25deg)",
                  "left": "7.6%",
                  "top": "48.4%"
                })

                $(".small-angle").css({
                    "height": "62.9%",
                    "width": "73%",
                    "left": "16%"
                })
                $(".imageblock").css({
                    "height": "60%",
                    "width": "49%",
                    "top": "20%"
                })

                $(".inputfields").css({
                    "background": "none"
                })

                setTimeout(function() {
                    nav_button_controls(0);
                }, 1200)


                break;
        }

    }

    function nav_button_controls(delay_ms){
  		timeoutvar = setTimeout(function(){
  			if(countNext==0){
  				$nextBtn.show(0);
  			} else if( countNext>0 && countNext == $total_page-1){
  				$prevBtn.show(0);
  				ole.footerNotificationHandler.pageEndSetNotification();
  			} else{
  				$prevBtn.show(0);
  				$nextBtn.show(0);
  			}
  		},delay_ms);
  	}
    function sound_player(sound_id){
      createjs.Sound.stop();
      current_sound = createjs.Sound.play(sound_id);
      current_sound.play();
      current_sound.on("complete", function(){
        if(countNext==2 || countNext==4){
          $nextBtn.hide(0);
          $prevBtn.hide(0);
        }else{
          nav_button_controls(0);
        }
      });
    }

    let inputVal;
    let offVal;

    function guessAngle(correctVal) {
        $("#check").click(function() {
            inputVal = $("#valid").val();

            console.log(inputVal)

            if (inputVal > correctVal) {
                offVal = Math.abs(inputVal - correctVal);
                console.log("You are off by " + offVal + "°")
                $(".inputfields").done(
                    setTimeout(function() {
                        $nextBtn.trigger("click");
                    }, 1200));
            } else if (inputVal < correctVal) {
                offVal1 = Math.abs(correctVal - inputVal);
                 offVal= "offVal1";
                console.log("You are less by " + offVal1 + "°")
                $(".inputfields").done(
                    setTimeout(function() {
                        $nextBtn.trigger("click");
                    }, 1200));
                  }else
            {
                offVal = "correct";
                  createjs.Sound.stop();
                play_correct_incorrect_sound(1);
                $(".inputfields").append("<span class ='correctimg animated fadeIn'></span>").done(
                    setTimeout(function() {
                        $nextBtn.trigger("click");
                    }, 1200));
            }
        })

    }



    function allowNumbs() {
        $("#valid").keyup(function(e) {
            var regex = /^[0-9]+$/;
            // This is will test the value against the regex
            // Will return True if regex satisfied
            if (regex.test(this.value) !== true)
            //alert if not true
            //alert("Invalid Input");
            // You can replace the invalid characters by:
                this.value = this.value.replace(/[^0-9]+/, '');
        });
    }

    function showHint(current, tomove) {
        if (countNext < 6) {
            $("#hint").click(function() {
                console.log("they clicked me");
                $nextBtn.trigger("click");
            });
        } else {
            $("#hint").mouseenter(function() {
                animateRotate(current, tomove);
            })
            $("#hint").mouseleave(function() {
                console.log("jell");
                backTo(tomove, current);
            })
        }
    }

    function animateRotate(current, tomove) {
        var elem = $(".draggable_wp");

        $({
            deg: current
        }).animate({
            deg: tomove
        }, {
            duration: 2000,
            step: function(now) {
                elem.css({
                    transform: "rotate(" + now + "deg)"
                })
            }
        })
    }

    function backTo(current, tomove) {
        var elem = $(".draggable_wp");
        var current1 = document.querySelector(".draggable_wp").style.transform;
        var found = current1.match(/[^a-z()]+/g);

        $({
            deg: found
        }).animate({
            deg: tomove
        }, {
            duration: 2000,
            step: function(now) {
                elem.css({
                    transform: "rotate(" + now + "deg)"
                })
            }
        })
    }

    function checkingAnswer(ans1) {
        $(".inputfields").on('click', "#check", function() {
            let ans = $("#valid").val();
            if (ans == ans1) {
                console.log("correct")
                $(".wrongimg").css({
                    "display": "none"
                });
                  createjs.Sound.stop();
                play_correct_incorrect_sound(1);
                $(".inputfields").append("<span class ='correctimg animated fadeIn'></span>").done(
                    setTimeout(function() {
                        $nextBtn.trigger("click");
                    }, 1200));

            } else {
              createjs.Sound.stop();
                play_correct_incorrect_sound(0);
                console.log("incorrect")
                $(".inputfields").append("<span class ='wrongimg animated fadeIn'></span>");
            }
        })
    }

    function rotateProtactor() {
        var prot = document.querySelector(".draggable_wp");
        var protRects = prot.getBoundingClientRect();
        var protX = protRects.left + protRects.width / 2;
        var protY = protRects.top + protRects.height / 2;

        var oldrotatevale = 0;
        var newrotatevalue = 0;
        var firstinstance = true;
        var initialvalue = -23;
        $('.draggable_wp').mousedown(function() {

            $('.draggable_wp').bind("mousemove", function(event) {
                newrotatevalue = Math.atan2(event.clientY - protY, event.clientX - protX) * (180 / Math.PI);
                if (firstinstance) {
                    firstinstance = false;
                    oldrotatevale = newrotatevalue;
                } else {
                    prot.style.transform = "rotate(" + (newrotatevalue - oldrotatevale + initialvalue) + "deg)";
                }
            });
        });

        $(document).mouseup(function(e) {
            firstinstance = true;
            initialvalue = newrotatevalue - oldrotatevale + initialvalue;
            $('.draggable_wp').unbind("mousemove");
        });
    }
    /*=====  End of Templates Block  ======*/

    /*==================================================
    =            Templates Controller Block            =
    ==================================================*/

    /*==================================================
    =            function to call templates            =
    ==================================================*/
    /**
     Motivation :
     - Make a single function call that handles all the
     template load easier

     How To:
     - Update the template caller with the required templates
     - Call template caller

     What it does:
     - According to value of the Global Variable countNext
     the slide templates are updated
     */

    function templateCaller() {
        if (boxquestionflag) {
            switch (boxquestioncountnext) {
                case 1:
                    $(".boxquestion1").hide(0);
                    $(".boxanswer1").show(0);
                    break;
                    /*
                    TODO: the homer dialog response in the mockup goes here
                    case 2:
                    					$(".boxquestion2").show(0);
                    					$(".boxanswer1").hide(0);
                    					 break;
                    */

                case 2:
                    $(".boxquestion2").show(0);
                    $(".boxanswer1").hide(0);
                    break;
                case 3:
                    $(".boxquestion2").hide(0);
                    $(".boxanswer2").show(0);
                    // break;
                    // case 4:
                    // $(".boxanswer2").hide(0);
                    // $(".boxanswer3").show(0);
                    boxquestionflag = false;
                    boxquestioncountnext = 0;

                    break;
                default:
                    boxquestioncountnext = 0;
                    boxquestionflag = false;
                    break;
            }
        } else {

            /*always hide next and previous navigation button unless
             explicitly called from inside a template*/
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // call navigation controller
            navigationcontroller();

            // call the template
            generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


            //call the slide indication bar handler for pink indicators
            loadTimelineProgress($total_page, countNext + 1);

            // just for development purpose to see total slide vs current slide number
            // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
        }
    }

    /*this countNext variable change here is solely for development phase and
    should be commented out for deployment*/
    // countNext+=1;

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on('click', function() {
        if (boxquestionflag) {
            boxquestioncountnext++;
        } else {
            countNext++;
        }
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
        countNext--;
        templateCaller();

        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    /*=====  End of Templates Controller Block  ======*/
});

Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
		//slide 0
		{
			hinttext: data.string.hint,
			imgexerciseblock: [
			{
				textdata: data.string.ques1,
				checkdata: data.string.checkbtn,
				hintdata: data.string.hintbtn,
			}
			]
		},
		];

		$(function () {
			var $board    = $('.board');
			var $nextBtn = $("#activity-page-next-btn-enabled");
			var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
			var countNext = 0;

			var $total_page = content.length;
		var preload;
  	var timeoutvar = null;
  	var current_sound;

  	function init() {
  		//specify type otherwise it will load assests as XHR
  		manifest = [
  			//images

  			// sounds
  			{id: "sound_0", src: soundAsset+"ex_instr.ogg"},
  		];
  		preload = new createjs.LoadQueue(false);
  		preload.installPlugin(createjs.Sound);//for registering sounds
  		preload.on("progress", handleProgress);
  		preload.on("complete", handleComplete);
  		preload.on("fileload", handleFileLoad);
  		preload.loadManifest(manifest, true);
  	}
  	function handleFileLoad(event) {
  		// console.log(event.item);
  	}
  	function handleProgress(event) {
  		$('#loading-text').html(parseInt(event.loaded*100)+'%');
  	}
  	function handleComplete(event) {
  		$('#loading-wrapper').hide(0);
  		//initialize varibales
  		// call main function
  		templateCaller();
  	}
  	//initialize
  	init();


			function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var score = 0;

	 /*creating array with radian degree for 10 to 90, 90 t0 190 and all*/
	 var array10to90 = [
	 [10, 0.174533],
	 [20, 0.349066],
	 [30, 0.523599],
	 [40, 0.698132],
	 [50, 0.872665],
	 [60, 1.0472],
	 [70, 1.22173],
	 [80, 1.39626],
	 [90, 1.5708]
	 ];

	 var array90to180 = [
	 [100, 1.74533],
	 [110, 1.91986],
	 [120, 2.0944],
	 [130, 2.26893],
	 [140, 2.44346],
	 [150, 2.61799],
	 [160, 2.79253],
	 [170, 2.96706],
	 [180, 3.14159],
	 ];

	 var array0to180 = [
	 [10, 0.174533],
	 [20, 0.349066],
	 [30, 0.523599],
	 [40, 0.698132],
	 [50, 0.872665],
	 [60, 1.0472],
	 [70, 1.22173],
	 [80, 1.39626],
	 [90, 1.5708],
	 [100, 1.74533],
	 [110, 1.91986],
	 [120, 2.0944],
	 [130, 2.26893],
	 [140, 2.44346],
	 [150, 2.61799],
	 [160, 2.79253],
	 [170, 2.96706],
	 [180, 3.14159],
	 ];

	 var testin = new LampTemplate();

 	 testin.init(10);
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[0]);
	 	$board.html(html);

	 	var canvasBlock = $('.canvasblock');
	 	var canvas = document.getElementById("canvasid");

	 	canvas.height = canvasBlock.height();
	 	canvas.width = canvasBlock.width();

	 	var ctx = canvas.getContext('2d');

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
	 	$('.congratulation').hide(0);
	 	$('.exefin').hide(0);
	 	$('.hintfield').hide(0);

	 	/*protractor rotation section*/
	 	var prot = document.querySelector(".draggable_wp");
	 	var protRects = prot.getBoundingClientRect();
	 	var protX = protRects.left + protRects.width / 2;
	 	var protY = protRects.top + protRects.height / 2;

	 	var oldrotatevale = 0;
	 	var newrotatevalue = 0;
	 	var firstinstance = true;
	 	var initialvalue = -23;
	 	$('.draggable_wp').mousedown(function() {
	 		/*for disabling transition if hint button is already pressed*/
	 		$(".draggable_wp").css({"transition-duration":"0s"});

	 		$('.draggable_wp').bind("mousemove", function(event) {
	 			newrotatevalue =  Math.atan2(event.clientY - protY, event.clientX - protX)*(180/Math.PI) ;
	 			if(firstinstance){
	 				firstinstance = false;
	 				oldrotatevale = newrotatevalue;
	 			}else{
	 				prot.style.transform = "rotate(" +(newrotatevalue-oldrotatevale+initialvalue)+ "deg)";
	 			}
	 		});
	 	});

	 	$(document).mouseup(function (e) {
	 		firstinstance = true;
	 		initialvalue = newrotatevalue-oldrotatevale+initialvalue;
	 		$('.draggable_wp').unbind("mousemove");
	 	});

	 	/*protractor rotation end*/
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	/*variables defined for different angles displayed*/
	 	var character = ["ABC","DEF","IJK","MNO","PQR","UVW","XYZ"];
	 	var randChar;

	 	function correctAns(){
			countNext==0?createjs.Sound.stop():'';
			play_correct_incorrect_sound(1);
	 		if(wrngClicked == false){
				testin.update(true);
	 		}
	 		ansClicked = true;

	 		if(countNext != 10)
	 			$nextBtn.show(0);
	 	}

	 	function incorrectAns(){
			countNext==0?createjs.Sound.stop():'';
			play_correct_incorrect_sound(0);
			testin.update(false);
	 		wrngClicked = true;
	 	}

	 	$(".draggable_wp").css({
	 		"transform": "rotate(-23deg)"
	 	});

	 	$(".degText").keypress(function (e) {
	 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
	 			return false;
	 		}
	 		if(e.which == 13) {
	 			$( ".Check" ).trigger( "click" );
	 		}
	 	});

	 	$(".angText").keypress(function (e) {
	 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 65 || e.which > 90) && (e.which < 97 || e.which > 122)) {
	 			return false;
	 		}
	 		if(e.which == 13) {
	 			$( ".Check" ).trigger( "click" );
	 		}
	 	});

	 	var angForChk;

	 	$(".Check").click(function(){
	 		var angStr = character[randChar];
	 		var revAngStr = angStr.split("").reverse().join("");
	 		var recText = $(".angText").val().toUpperCase();
	 		var recAng = $(".degText").val();

	 		if ((recText == angStr || recText == revAngStr) && (recAng == angForChk)){
	 			$(this).css("background","#6AA84F");
		   		$('.angText').css({"background": '#C5F295', 'box-shadow': 'none'});
		   		$('.degText').css({"background": '#C5F295', 'box-shadow': 'none'});
	 			correctAns();
	 		}
	 		else{
	 			if(!(recText == angStr || recText == revAngStr)){
	 				$(".angText").css("background-color","#FF7660");
	 				incorrectAns();
	 			}

	 			if (recAng != angForChk){
	 				$(".degText").css("background-color","#FF7660");
	 				incorrectAns();
	 			}
	 		}
	 	});

	 	var hintCount = 0;
	 	$(".Hint").click(function(){
	 		if(hintCount == 0){
	 			$(".hintfield").show(0);
	 			hintCount++;
	 		}
	 		else if(hintCount == 1){
	 			$(".draggable_wp").css({"transform": "rotate(0deg)", "transition-duration":"2s"});
	 			$(this).css({"background":"#001b23","cursor":"not-allowed"});
	 		}
	 	});

	 	switch(countNext){
	 		case 0:
			sound_player("sound_0");
	 		case 1:
	 		var rand;
	 		var wrngRandArr = [10, 20, 30, 40, 50, 60, 70, 80, 90];

	 		rand = Math.floor(Math.random() * array10to90.length);
	 		$('.class1').html(array10to90[rand][0]);

	 		var optForWrng = wrongRand(wrngRandArr, array10to90[rand][0]);
	 		var optForWrng2 = wrongRand2(wrngRandArr, array10to90[rand][0], optForWrng);

	 		$('.class2').html(optForWrng);
	 		$('.class3').html(optForWrng2);
	 		theta = -array10to90[rand][1];
	 		angForChk = array10to90[rand][0];

	 		caseone();
	 		array10to90.splice(rand,1);
	 		break;

	 		case 2:
	 		case 3:
	 		var rand;
	 		var wrngRandArr = [100, 110, 120, 130, 140, 150, 160, 170, 180];

	 		rand = Math.floor(Math.random() * array90to180.length);
	 		$('.class1').html(array90to180[rand][0]);

	 		var optForWrng = wrongRand(wrngRandArr, array90to180[rand][0]);
	 		var optForWrng2 = wrongRand2(wrngRandArr, array90to180[rand][0], optForWrng);

	 		$('.class2').html(optForWrng);
	 		$('.class3').html(optForWrng2);
	 		theta = -array90to180[rand][1];
	 		angForChk = array90to180[rand][0];

	 		casetwo();
	 		array90to180.splice(rand,1);
	 		break;

	 		case 4:
	 		case 5:
	 		case 6:
	 		case 7:
	 		case 8:
	 		case 9:
	 		var min = -45;
	 		var max = 45;
	 		var rotateRand = Math.floor(Math.random() * (max - min + 1)) + min;
	 		$('.canvasblock').css('transform','rotate('+ rotateRand +'deg)');
	 		var rand;
	 		var wrngRandArr = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180];

	 		rand = Math.floor(Math.random() * array0to180.length);
	 		theta = -array0to180[rand][1];
	 		angForChk = array0to180[rand][0];

	 		$('.class1').html(array0to180[rand][0]);

	 		var optForWrng = wrongRand(wrngRandArr, array0to180[rand][0]);
	 		var optForWrng2 = wrongRand2(wrngRandArr, array0to180[rand][0], optForWrng);

	 		$('.class2').html(optForWrng);
	 		$('.class3').html(optForWrng2);

	 		casethree();
	 		array0to180.splice(rand,1);
	 		break;
	 	}

	 	function drawLine(startx, starty, endx, endy, char, check) {
		//line
		ctx.beginPath();
		ctx.moveTo(startx, starty);
		ctx.lineTo(endx, endy);
		ctx.lineWidth = 3;
		ctx.strokeStyle = 'black';
		ctx.fillStyle = 'black';
		ctx.stroke();

		//circle
		ctx.beginPath();
		ctx.arc(startx, starty, 5, 0, 2 * Math.PI, false);
		ctx.fillStyle = 'black';
		ctx.fill();
		ctx.lineWidth = 5;
		ctx.stroke();

		ctx.font="20px Verdana";
		if(check){
			ctx.fillText(char.charAt(1),startx-30,starty+10);
			ctx.fillText(char.charAt(2),endx,endy);
		}
		else{
			ctx.fillText(char.charAt(0),endx,endy);
		}
	}

	function caseone(){
		x1 = canvas.width/2;
		y1 = canvas.height/1.2;
		r =  canvas.height/1.7;
		randChar = Math.floor(Math.random() * character.length);
		drawLine(x1, y1, x1+canvas.width/2.7, y1, character[randChar], true);
		drawLine(x1, y1, x1 + r * Math.cos(theta), y1 + r * Math.sin(theta), character[randChar], false);
	}

	function casetwo(){
		x1 = canvas.width/2;
		y1 = canvas.height/1.2;
		r =  canvas.height/1.7;
		randChar = Math.floor(Math.random() * character.length);

		drawLine(x1, y1, x1+canvas.width/2.7, y1, character[randChar], true);
		drawLine(x1, y1, x1 + r * Math.cos(theta), y1 + r * Math.sin(theta), character[randChar], false);
	}

	function casethree(){
		x1 = canvas.width/2;
		y1 = canvas.height/1.2;
		r =  canvas.height/1.7;
		randChar = Math.floor(Math.random() * character.length);

		drawLine(x1, y1, x1+canvas.width/2.7, y1, character[randChar], true);
		drawLine(x1, y1, x1 + r * Math.cos(theta), y1 + r * Math.sin(theta), character[randChar], false);
	}

	function wrongRand(array, right){
		var index = array.indexOf(right);
		(index != -1) ? array.splice(index, 1) : true;
		var wrongRandom = Math.floor(Math.random() * array.length);
		return(array[wrongRandom]);
	}

	function wrongRand2(array, right, right2){
		var index = array.indexOf(right);
		(index != -1) ? array.splice(index, 1) : true;
		var index2 = array.indexOf(right2);
		(index2 != -1) ? array.splice(index2, 1) : true;
		var wrongRandom = Math.floor(Math.random() * array.length);
		return(array[wrongRandom]);
	}

	$(window).resize(function() {
		canvas.height = canvasBlock.height();
		canvas.width = canvasBlock.width();
		switch(countNext){
			case 0:
			case 1:
			caseone();
			break;

			case 2:
			case 3:
			casetwo();
			break;

			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			casethree();
			break;
		}
	});

}
function sound_player(sound_id){
	createjs.Sound.stop();
	current_sound = createjs.Sound.play(sound_id);
	current_sound.play();
}
function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

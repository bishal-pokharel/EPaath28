
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide0
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata:data.string.diytext,
			textclass:'diytext'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'bg',
				imgclass:'bg_full'
			}]
		}]
	},

//slide1
{
	contentblockadditionalclass:'blue_bg',
	uppertextblock:[{
		textdata:  data.string.p2text1,
		textclass: "top_text",
	},
	{
		textdata:  data.string.p2text2,
		textclass: "top_bottom",
	},
	{
		textdata:  data.string.p2text3,
		textclass: "text-1",
		splitintofractionsflag: true

	},
	{
		textdata:  data.string.p2text4,
		textclass: "text-2",
		splitintofractionsflag: true

	},
	{
		textdata:  data.string.p2text6,
		textclass: "text-3",
		splitintofractionsflag: true

	},
	{
		textdata:  data.string.p2text7,
		textclass: "text-4",
		splitintofractionsflag: true

	},
	{
		textdata:  data.string.p2text8,
		textclass: "text-5",
		splitintofractionsflag: true

	},
	{
		textdata:  data.string.p2text5,
		textclass: "check-1",

	},
	{
		textdata:  data.string.p2text5,
		textclass: "check-2",

	},
	{
		textdata:  data.string.p2text5,
		textclass: "check-3",

	},
	{
		textdata:  data.string.p2text5,
		textclass: "check-4",

	}
	,
	{
		textdata:  data.string.p2text9,
		textclass: "tryagain-1",

	},
	{
		textdata:  data.string.p2text9,
		textclass: "tryagain-2",

	},
	{
		textdata:  data.string.p2text9,
		textclass: "tryagain-3",

	},
	{
		textdata:  data.string.p2text9,
		textclass: "tryagain-4",

	}
	],
	imageblock:[{
		imagestoshow : [
			{
				imgclass : "eql-1",
				imgsrc : '',
				imgid : 'equals'
			},
			{
				imgclass : "eql-2",
				imgsrc : '',
				imgid : 'equals'
			},
			{
				imgclass : "eql-3",
				imgsrc : '',
				imgid : 'equals'
			},
			{
				imgclass : "eql-4",
				imgsrc : '',
				imgid : 'equals'
			},
			{
				imgclass : "righticon-1",
				imgsrc : '',
				imgid : 'correct'
			},
			{
				imgclass : "righticon-2",
				imgsrc : '',
				imgid : 'correct'
			},
			{
				imgclass : "righticon-3",
				imgsrc : '',
				imgid : 'correct'
			},
			{
				imgclass : "righticon-4",
				imgsrc : '',
				imgid : 'correct'
			}
		]
	}]
},
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "equals", src: imgpath+"equals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"a_07.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		$(".first_input, .second_input, .third_input, .fourth_input, .fifth_input, .sixth_input, .seventh_input, .eight_input, .ninth_input").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		if ((e.keyCode==13)) {
			if(  $(".check-1").css('display') == 'block' ){
				$('.check-1').trigger("click");
			}
			if(  $(".check-2").css('display') == 'block' ){
				$('.check-2').trigger("click");
			}
			if(  $(".check-3").css('display') == 'block' ){
				$('.check-3').trigger("click");
			}
			if(  $(".check-4").css('display') == 'block' ){
				$('.check-4').trigger("click");
			}
		}

		});
		if(countNext==0){
			play_diy_audio();
			nav_button_controls(2000);
		}
		countNext==1?sound_play_click("s2_p2"):'';

		$('.check-2, .check-3, .check-4,.text-3,.text-4,.text-5,.eql-2,.eql-3,.eql-4,.tryagain-1, .tryagain-2, .tryagain-3, .tryagain-4,.righticon-1, .righticon-2, .righticon-3, .righticon-4').hide(0);

		// part 1
		$('.check-1').click(function(){
			createjs.Sound.stop();
			if($('.first_input').val()!=2){
				$('.first_input').addClass('wrong');
			}
			if($('.first_input').val()==2){
				$('.first_input').addClass('right');
			}
			if($('.second_input').val()!=3){
				$('.second_input').addClass('wrong');
			}
			if($('.second_input').val()==3){
				$('.second_input').addClass('right');
			}
			if($('.third_input').val()!=3){
				$('.third_input').addClass('wrong');
			}
			if($('.third_input').val()==3){
				$('.third_input').addClass('right');
			}
			if($('.first_input').hasClass('right') &&$('.second_input').hasClass('right') &&$('.third_input').hasClass('right') ){
				$('.check-1').hide(0);
				$('.righticon-1').show(0);
				$('.first_input,.second_input,.third_input').css({'background':'#98C02E','pointer-events':'none'});
				play_correct_incorrect_sound(1);
				setTimeout(function(){
					$('.text-3,.eql-2,.check-2').fadeIn(500);
				},100);
			}
			else{
				$('.check-1').hide(0);
				$('.tryagain-1').show(0);
			}
		});

		$('.tryagain-1').click(function(){
			$('.text-2').find('.wrong').removeClass('wrong');
			$('.check-1').show(0);
			$('.tryagain-1').hide(0);
		});

		// part 2
		$('.check-2').click(function(){
			if($('.fourth_input').val()!=12){
				$('.fourth_input').addClass('wrong');
			}
			if($('.fourth_input').val()==12){
				$('.fourth_input').addClass('right');
			}
			if($('.fifth_input').val()!=12){
				$('.fifth_input').addClass('wrong');
			}
			if($('.fifth_input').val()==12){
				$('.fifth_input').addClass('right');
			}
			if($('.sixth_input').val()!=12){
				$('.sixth_input').addClass('wrong');
			}
			if($('.sixth_input').val()==12){
				$('.sixth_input').addClass('right');
			}
			if($('.fourth_input').hasClass('right') &&$('.fifth_input').hasClass('right') &&$('.sixth_input').hasClass('right') ){
				$('.check-2').hide(0);
				$('.righticon-2').show(0);
				$('.fourth_input,.fifth_input,.sixth_input').css({'background':'#98C02E','pointer-events':'none'});
				play_correct_incorrect_sound(1);
				setTimeout(function(){
					$('.text-4,.eql-3,.check-3').fadeIn(500);
				},100);
			}
			else{
				$('.check-2').hide(0);
				$('.tryagain-2').show(0);
			}
		});

		$('.tryagain-2').click(function(){
			$('.text-3').find('.wrong').removeClass('wrong');
			$('.check-2').show(0);
			$('.tryagain-2').hide(0);
		});


		// part 3
		$('.check-3').click(function(){
			if($('.seventh_input').val()!=24){
				$('.seventh_input').addClass('wrong');
			}
			if($('.seventh_input').val()==24){
				$('.seventh_input').addClass('right');
			}
			if($('.eight_input').val()!=12){
				$('.eight_input').addClass('wrong');
			}
			if($('.eight_input').val()==12){
				$('.eight_input').addClass('right');
			}

			if($('.seventh_input').hasClass('right') &&$('.eight_input').hasClass('right') ){
				$('.check-3').hide(0);
				$('.righticon-3').show(0);
				$('.seventh_input,.eight_input').css({'background':'#98C02E','pointer-events':'none'});
				play_correct_incorrect_sound(1);
				setTimeout(function(){
					$('.text-5,.eql-4,.check-4').fadeIn(500);
				},100);
			}
			else{
				$('.check-3').hide(0);
				$('.tryagain-3').show(0);
			}
		});

		$('.tryagain-3').click(function(){
			$('.text-4').find('.wrong').removeClass('wrong');
			$('.check-3').show(0);
			$('.tryagain-3').hide(0);
		});


		// part 4
		$('.check-4').click(function(){
			if($('.ninth_input').val()!=2){
				$('.ninth_input').addClass('wrong');
			}
			if($('.ninth_input').val()==2){
				$('.ninth_input').addClass('right');
			}
			if($('.ninth_input').hasClass('right') ){
				$('.check-4').hide(0);
				$('.righticon-4').show(0);
				$('.seventh_input,.eight_input').css({'background':'#98C02E','pointer-events':'none'});
				play_correct_incorrect_sound(1);
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			else{
				$('.check-4').hide(0);
				$('.tryagain-4').show(0);
			}
		});

		$('.tryagain-4').click(function(){
			$('.text-5').find('.wrong').removeClass('wrong');
			$('.check-4').show(0);
			$('.tryagain-4').hide(0);
		});

	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

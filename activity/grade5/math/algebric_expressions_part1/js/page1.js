var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
	  contentnocenteradjust: true,
	  contentblockadditionalclass: 'pink_bg',
	  uppertextblock:[{
	    textdata : data.lesson.chapter,
	    textclass : 'lesson-title',
	  }],
	  imageblock:[{
	    imagestoshow : [
	      {
	        imgclass : "bg_full",
	        imgsrc : '',
	        imgid : 'cover_algebra'
	      }
	    ]
	  }]
	},

	// slide1
	{
	  contentnocenteradjust: true,
	  contentblockadditionalclass: 'pink_bg',
	  speechbox:[{
	    speechbox: 'sp-2 ',
	    textdata : data.string.p1text1,
	    textclass : 'text_inside fadesIn',
	    imgid : 'speechbubble',
			imgclass:'flipped',
	    imgsrc: '',
	    // audioicon: true,
	  }],
	  imageblock:[{
	    imagestoshow : [
	      {
	        imgclass : "fairy",
	        imgsrc : '',
	        imgid : 'fairygif'
	      }
	    ]
	  }]
	},
	// slide2
	{
	  contentnocenteradjust: true,
	  contentblockadditionalclass: 'pink_bg',
	  speechbox:[{
	    speechbox: 'sp-2 ',
	    textdata : data.string.p1text2,
	    textclass : 'text_inside fadesIn',
	    imgid : 'speechbubble',
			imgclass:'flipped',
	    imgsrc: '',
	    // audioicon: true,
	  }],
	  imageblock:[{
	    imagestoshow : [
	      {
	        imgclass : "fairy",
	        imgsrc : '',
	        imgid : 'fairygif'
	      }
	    ]
	  }]
	},

	// slide3
	{
	  contentnocenteradjust: true,
	  contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text3,
		 textclass : 'top_text fadesIn',
	 },{
		textdata : data.string.p1text4,
		textclass : 'center_text ',
		datahighlightflag: true,
		datahighlightcustomclass:'highlight_text'
	}]
	},

	// slide4
	{
	  contentnocenteradjust: true,
	  contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text5,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text4,
		textclass : 'center_text',
		datahighlightflag: true,
		datahighlightcustomclass:'highlight_text1',
		datahighlightflag2:true,
		datahighlightcustomclass2:'highlight_text2',
	},{
		 textdata : data.string.p1text6,
		 textclass : 'left-big',
	 },{
		textdata : data.string.p1text7,
		textclass : 'left-small',
	},{
	 textdata : data.string.p1text8,
	 textclass : 'right-big',
	},{
	 textdata : data.string.p1text9,
	 textclass : 'right-small',
 }],

 imageblock:[{
	 imagestoshow:[{
		 imgid:'arrow',
		 imgclass:'arrow_class1'
	 },
	 {
		 imgid:'arrow',
		 imgclass:'arrow_class2'
	 }]
 }]
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text10,
		 textclass : 'top_text',
	 }],
	 th1:data.string.p1text11,
	 th2: data.string.p1text12,
	 th3:data.string.p1text13,
	 t1d1:data.string.p1text14,
	 t1d2:data.string.p1text15,
	 t1d3:data.string.p1text16,
	 t2d1:data.string.p1text17,
	 t2d2:data.string.p1text19,
	 t2d3:data.string.p1text18,
	 data_table:[{}]
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text20,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text21,
		textclass : 'center_text',
		},{
		 textdata : data.string.p1text22,
		 textclass : 'bottom_text',
	 }]
	},

	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text23,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text24,
		textclass : 'center_text fadesIn',
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text26,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text21,
		textclass : 'top_mid_text',
		},{
 		textdata : data.string.p1text31,
 		textclass : 'bottext',
 		},{
 		textdata : data.string.p1text28,
 		textclass : 'center_instruction',
	},{
	textdata : data.string.p1text29,
	textclass : 'b_value',
	},{
	textdata : data.string.p1text30,
	textclass : 'submit_button',
	}],
	inputform:[{
		inputclass:'inputclass1',
		formclass:'form1',
	}]
	},

	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text32,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text33,
		textclass : 'center_text',
		},{
		 textdata : data.string.p1text34,
		 textclass : 'bottom_text',
	 }]
	},

	// slide10
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text35,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text36,
		textclass : 'top_below_text',
		},{
			textdata : data.string.p1text37,
			textclass : 'bot_center',
		},{
			 textdata : data.string.p1text39,
			 textclass : 'inputtext1',
		 },{
			textdata : data.string.p1text40,
			textclass : 'inputtext2',
		},{
		textdata : data.string.p1text30,
		textclass : 'submit_button changepos_button',
		}],
			inputform:[{
				inputclass:'inputclassa',
			},
			{
				inputclass:'inputclassb',
			}]
	},

	// slide11
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		 textdata : data.string.p1text38,
		 textclass : 'top_text',
	 },{
		textdata : data.string.p1text36,
		textclass : 'top_below_text',
	},{
			textdata : data.string.p1text52,
			textclass : 'output_text',
		},{
			 textdata : data.string.p1text39,
			 textclass : 'inputtext3',
		 },{
			textdata : data.string.p1text40,
			textclass : 'inputtext4',
		},{
		textdata : data.string.p1text30,
		textclass : 'submit_button changepos_button1',
		},{
		textdata : data.string.p1text51,
		textclass : 'tryagain',
		}],
			inputform:[{
				inputclass:'inputclassc',
			},
			{
				inputclass:'inputclassd',
			}],
			popup:[{
				popupdiv_class:'tips',
				textdata : data.string.p1text41,
				textclass : 'popup_text',
				imgsrc1:imgpath+'7.png',
				imgclass:'crossicon'
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'tipicon',
					imgclass:'tipicon'
				}]
			}]
	},

	// slide12
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
		textdata : data.string.p1text42,
		textclass : 'center_text',
		}]
	},

	// slide13
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
					textdata : data.string.p1text43,
					textclass : 'top_text',
				},
				{
					textdata : data.string.p1text44,
					textclass : 'main_question',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text45,
					textclass : 'option1',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text46,
					textclass : 'option2 correct',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text47,
					textclass : 'option3',
					splitintofractionsflag: true
				}]
	},

	// slide14
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',
		uppertextblock:[{
					textdata : data.string.p1text43,
					textclass : 'top_text',
				},
				{
					textdata : data.string.p1text44,
					textclass : 'main_question',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text45,
					textclass : 'option1',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text46,
					textclass : 'option2 correct',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text47,
					textclass : 'option3',
					splitintofractionsflag: true
				},
				{
					textdata : data.string.p1text48,
					textclass : 'info_1',
				},
				{
					textdata : data.string.p1text49,
					textclass : 'info_2',
				},
				{
					textdata : data.string.p1text50,
					textclass : 'info_3',
				}],
				imageblock:[{
					imagestoshow:[{
						imgid:'correct',
						imgclass:'correct_img'
					}]
				}]
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairygif", src: imgpath+"fairy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbubble", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"blue-arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tipicon", src: imgpath+"icontip.png", type: createjs.AbstractLoader.IMAGE},
			{id: "correct", src: imgpath+"correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cover_algebra", src: imgpath+"cover_algebra.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p6_2", src: soundAsset+"s1_p6_2.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p9_2", src: soundAsset+"s1_p9_2.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p10_2", src: soundAsset+"s1_p10_2.ogg"},
			{id: "s1_p11", src: soundAsset+"s1_p11.ogg"},
			{id: "s1_p12", src: soundAsset+"s1_p12.ogg"},
			{id: "s1_p12_1", src: soundAsset+"s1_p12_1(quick tip).ogg"},
			{id: "s1_p12_2", src: soundAsset+"s1_p12_2(after quick tip is clicked).ogg"},
			{id: "s1_p13", src: soundAsset+"s1_p13.ogg"},
			{id: "s1_p14", src: soundAsset+"s1_p14.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$('.option1,.option2,.option3').click(function(){
			createjs.Sound.stop();
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
				$(this).css({'border-color':'#EEFF41','background':'#98C02E','transition':'.2s'});
				$('.option1,.option2,.option3').css('pointer-events','none');
				$('<img style="left:'+centerX+';bottom:10%;position:absolute;height:5%;transform:translate(-8%,-1053%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
				$(this).css({'border-color':'#980000','background':'#FF0000','transition':'.2s'});
				$(this).css('pointer-events','none');
				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-8%,-1053%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
			}
		});


		$(".inputclass,.inputclassa,.inputclassb,.inputclassc,.inputclassd").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 48 && e.keyCode < 57)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +2+"%";
		$('.top_below_text').css({
			'top':top_val
		});
		switch(countNext) {
			case 4:
				sound_player("s1_p"+(countNext+1),1);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s1_p"+(countNext+1));
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(100);
						});
					});
			$('.arrow_class2,.arrow_class1,.highlight_text1,.highlight_text2,.left-big,.left-small,.right-big,.right-small').hide(0);
			$('.arrow_class1,.highlight_text1,.left-big,.left-small').fadeIn(1000,function(){
				$('.arrow_class2,.highlight_text2,.right-big,.right-small').fadeIn(1000);
			});
			// nav_button_controls(100);
			break;
			case 5:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				$('tr td:nth-child(1)').animate({'opacity':'1'},500);
				current_sound.on('complete', function(){
						$('tr td:nth-child(2)').animate({'opacity':'1'},500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$('tr td:nth-child(3)').animate({'opacity':'1'},500);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_2");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(100);
						});
					});
				});
			$('td').css('opacity','0');
			break;
			case 6:
				sound_player("s1_p"+(countNext+1),1);
				$('.bottom_text,.center_text').hide(0);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s1_p"+(countNext+1));
					current_sound.play();
					current_sound.on('complete', function(){
						$('.bottom_text,.center_text').fadeIn(300);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls(100);
						});
					});
			$('.left_image').css('background','white');
			break;
			case 8:
			$('.bottext,.submit_button,.inputclass1,.center_instruction,.top_mid_text,.b_value').hide(0);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					$('.top_mid_text').fadeIn(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.center_instruction').fadeIn(1000,function(){
							$('.submit_button,.inputclass1,.b_value').fadeIn(1000);
						});
					});
				});
			$('.submit_button').click(function(){
				  var enteredvalue = $('.inputclass1').val();
					$('.entered_value').html(enteredvalue);
					$(this).css({'pointer-events':'none','filter':'grayscale(100%)'});
					$('.inputclass1').css('pointer-events','none');
					$('.bottext').fadeIn(1000);
					nav_button_controls(100);
			});

			break;

			case 9:
				$('.bottom_text,.center_text').hide(0);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s1_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					$('.center_text').fadeIn(1000);
					$('.bottom_text').fadeIn(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(300);
					});
				});
			break;

			case 10:
				sound_player("s1_p"+(countNext+1),0);
			$('.submit_button').css({'pointer-events':'none','filter':'grayscale(1)'});
			$(".inputclassa,.inputclassb").keyup(function(e){
				var input_number1 = parseInt($('.inputclassa').val());
				var input_number2 = parseInt($('.inputclassb').val());
				// console.log(input_number);
				if(input_number1>=0 && input_number2>=0)
				{
					// $('input').not(this).css('pointer-events','none');
					$('.submit_button').css({'pointer-events':'auto','filter':'grayscale(0)'});
				}
			});

			$('.submit_button').click(function(){
				createjs.Sound.stop();
					p = $('.inputclassa').val();
				  q = $('.inputclassb').val();
					console.log(p);
					console.log(q);
					$(this).css({'pointer-events':'none','filter':'grayscale(100%)'});
					$('.inputclassa,.inputclassb').css('pointer-events','none');
					nav_button_controls(100);
			});
			break;
			case 11:
				sound_player("s1_p"+(countNext+1),0);
					$('.submit_button').css({'pointer-events':'none','filter':'grayscale(1)'});
					$(".inputclassc,.inputclassd").keyup(function(e){
						var input_number1 = parseInt($('.inputclassc').val());
						var input_number2 = parseInt($('.inputclassd').val());
						// console.log(input_number);
						if(input_number1>=0 && input_number2>=0)
						{
							// $('input').not(this).css('pointer-events','none');
							$('.submit_button').css({'pointer-events':'auto','filter':'grayscale(0)'});
						}
					});
						$('.tips').hide(0);
						console.log(p);
						console.log(q);
						$(".inputclassc").val(p);
						$(".inputclassd").val(q);
						$('.p').html(p);
						$('.q').html(q);
						$('.product1').html(5*p);
						$('.product2').html(4*q);
						$('.final_number').html(5*p-4*q);
						$('.submit_button').click(function(){
							createjs.Sound.stop();
								p = $('.inputclassc').val();
								q = $('.inputclassd').val();
								console.log(p);
								console.log(q);
								$(this).css({'pointer-events':'none','filter':'grayscale(100%)'});
								$('.inputclassa,.inputclassb').css('pointer-events','none');
								$(".inputclassc").val(p);
								$(".inputclassd").val(q);
								$('.p').html(p);
								$('.q').html(q);
								$('.product1').html(5*p);
								$('.product2').html(4*q);
								$('.final_number').html(5*p-4*q);
								nav_button_controls(100);
						});
						$('.tryagain').click(function(){
							$(".inputclassc").val("");
							$(".inputclassd").val("");
							$('.inputclassa,.inputclassb').css('pointer-events','auto');
							$('.submit_button').css({'pointer-events':'auto','filter':'grayscale(0%)'});
						});
						$('.tipicon').click(function(){
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_1");
							current_sound.play();
							current_sound.on('complete', function(){
								createjs.Sound.stop();
								var current_sound = createjs.Sound.play("s1_p"+(countNext+1)+"_2");
								current_sound.play();
							});
							$(this).css('pointer-events','none');
							$('.crossicon').css('pointer-events','auto');
							$('.tips').fadeIn(500);
						});
						$('.crossicon').click(function(){
							createjs.Sound.stop();
							$(this).css('pointer-events','none');
							$('.tipicon').css('pointer-events','auto');
							$('.tips').fadeOut(500);
						});
						break;
						case 13:
							sound_player("s1_p"+(countNext+1),1);
						break;
						case 14:
							sound_player("s1_p"+(countNext+1),1);
						    $(".correct_img").hide();
						$('.option1,.option2,.option3').css({'top':'29%','transform':'scale(.7)','pointer-events':'none'});
						$('.option2').css({'border-color':'#EEFF41','background':'#98C02E'});
						nav_button_controls(100);
						break;

						default:
							sound_player("s1_p"+(countNext+1),1);
						break;


		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
				next?nav_button_controls(0):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

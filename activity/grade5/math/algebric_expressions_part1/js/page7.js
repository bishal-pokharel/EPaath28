
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'purple_bg',
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.p5text18,
			textclass : 'inside_text',
			imgid: 'speechbubble'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'fairy',
				imgid:'fairygif'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text3,
			textclass:'mid_text'
		},
		{
			textdata:data.string.p7text4,
			textclass:'bot_text'
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text5,
			textclass:'left_text',
			datahighlightflag:true,
			datahighlightcustomclass:'text_decor'
		},
		{
			textdata:data.string.p7text7,
			textclass:'right_text',
			datahighlightflag:true,
			datahighlightcustomclass:'blue',
			datahighlightcustomclass2:'red',
		},
		{
			textdata:data.string.p7text8,
			textclass:'right_text_2'
		}],

		imageblock:[{
			imagestoshow:[{
				imgid:'arrow',
				imgclass:'arrow1'
			},{
				imgid:'arrow',
				imgclass:'arrow2'
			}]
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text6,
			textclass:'left_text',
			datahighlightflag:true,
			datahighlightcustomclass:'text_decor'
		},
		{
			textdata:data.string.p7text7,
			textclass:'right_text',
			datahighlightflag:true,
			datahighlightcustomclass:'blue',
			datahighlightcustomclass2:'red',
		},
		{
			textdata:data.string.p7text8,
			textclass:'right_text_2'
		}],
	},

	// slide4
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text10,
			textclass:'left_text'
		},
		{
			textdata:data.string.p7text7,
			textclass:'right_text',
			datahighlightflag:true,
			datahighlightcustomclass:'blue',
			datahighlightcustomclass2:'red',
		},
		{
			textdata:data.string.p7text8,
			textclass:'right_text_2'
		}],
	},


	// slide5
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text11,
			textclass:'mid_text'
		}],
	},

	// slide6
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text12,
			textclass:'left_text'
		},
		{
			textdata:data.string.p7text13,
			textclass:'right_text_vertical',
			splitintofractionsflag:true,
		},
		{
			textdata:data.string.p7text14,
			textclass:'right_text_2'
		}],
	},

	// slide7
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata:data.string.p7text2,
			textclass:'toptext',
			datahighlightflag:true,
			datahighlightcustomclass:'large'
		},
		{
			textdata:data.string.p7text15,
			textclass:'left_text'
		},
		{
			textdata:data.string.p7text13,
			textclass:'right_text_vertical',
			splitintofractionsflag:true,
		},
		{
			textdata:data.string.p7text14,
			textclass:'right_text_2'
		}],
	},

	//again

	// slide8
	{
	  contentblockadditionalclass:'purple_bg',
	  speechbox:[{
	    speechbox: 'speechbox1',
	    textdata : data.string.p5text18,
	    textclass : 'inside_text',
	    imgid: 'speechbubble'
	  }],
	  imageblock:[{
	    imagestoshow:[{
	      imgclass:'fairy',
	      imgid:'fairygif'
	    }]
	  }]
	},

	// slide9
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text20,
	    textclass:'mid_text',
	  }]
	},

	// slide10
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text5,
	    textclass:'left_text',
	    datahighlightflag:true,
	    datahighlightcustomclass:'text_decor'
	  },
	  {
	    textdata:data.string.p7text18,
	    textclass:'right_text',
	    datahighlightflag:true,
	    datahighlightcustomclass:'blue',
	    datahighlightcustomclass2:'red',
	  },
	  {
	    textdata:data.string.p7text8,
	    textclass:'right_text_2'
	  }],

	  imageblock:[{
	    imagestoshow:[{
	      imgid:'arrow',
	      imgclass:'arrow1'
	    },{
	      imgid:'arrow',
	      imgclass:'arrow2'
	    }]
	  }]
	},

	// slide11
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text6,
	    textclass:'left_text',
	    datahighlightflag:true,
	    datahighlightcustomclass:'text_decor'
	  },
	  {
	    textdata:data.string.p7text18,
	    textclass:'right_text',
	    datahighlightflag:true,
	    datahighlightcustomclass:'blue',
	    datahighlightcustomclass2:'red',
	  },
	  {
	    textdata:data.string.p7text8,
	    textclass:'right_text_2'
	  }],
	},

	// slide12
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text21,
	    textclass:'left_text'
	  },
	  {
	    textdata:data.string.p7text18,
	    textclass:'right_text',
	    datahighlightflag:true,
	    datahighlightcustomclass:'blue',
	    datahighlightcustomclass2:'red',
	  },
	  {
	    textdata:data.string.p7text8,
	    textclass:'right_text_2'
	  }],
	},


	// slide13
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text11,
	    textclass:'mid_text'
	  }],
	},

	// slide14
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text12,
	    textclass:'left_text'
	  },
	  {
	    textdata:data.string.p7text19,
	    textclass:'right_text_vertical',
	    splitintofractionsflag:true,
	  },
	  {
	    textdata:data.string.p7text14,
	    textclass:'right_text_2'
	  }],
	},

	// slide15
	{
	  contentblockadditionalclass:'purple_bg',
	  uppertextblock:[{
	    textdata:data.string.p7text16,
	    textclass:'toptext',
	    datahighlightflag:true,
	    datahighlightcustomclass:'large'
	  },
	  {
	    textdata:data.string.p7text22,
	    textclass:'left_text'
	  },
	  {
	    textdata:data.string.p7text19,
	    textclass:'right_text_vertical',
	    splitintofractionsflag:true,
	  },
	  {
	    textdata:data.string.p7text14,
	    textclass:'right_text_2'
	  }],
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairygif", src: imgpath+"fairy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbubble", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "let_us_try_another_example", src: soundAsset+"let_us_try_another_example.ogg"},
			{id: "s7_p2", src: soundAsset+"s7_p2.ogg"},
			{id: "s7_p3", src: soundAsset+"s7_p3.ogg"},
			{id: "s7_p3_1", src: soundAsset+"s7_p3_1.ogg"},
			{id: "s7_p3_2", src: soundAsset+"s7_p3_2.ogg"},
			{id: "s7_p4", src: soundAsset+"s7_p4.ogg"},
			{id: "s7_p4_1", src: soundAsset+"s7_p4_1.ogg"},
			{id: "s7_p5", src: soundAsset+"s7_p5.ogg"},
			{id: "s7_p5_1", src: soundAsset+"s7_p5_1.ogg"},
			{id: "s7_p6", src: soundAsset+"s7_p6.ogg"},
			{id: "s7_p7", src: soundAsset+"s7_p7.ogg"},
			{id: "s7_p7_1", src: soundAsset+"s7_p7_1.ogg"},
			{id: "s7_p7_2", src: soundAsset+"s7_p7_2.ogg"},
			{id: "s7_p8", src: soundAsset+"s7_p8.ogg"},
			{id: "s7_p10", src: soundAsset+"s7_p10.ogg"},
			{id: "s7_p11", src: soundAsset+"s7_p11.ogg"},
			{id: "s7_p11_1", src: soundAsset+"s7_p11_1.ogg"},
			{id: "s7_p11_2", src: soundAsset+"s7_p11_2.ogg"},
			{id: "s7_p12", src: soundAsset+"s7_p12.ogg"},
			{id: "s7_p12_1", src: soundAsset+"s7_p12_1.ogg"},
			{id: "s7_p13", src: soundAsset+"s7_p13.ogg"},
			{id: "s7_p13_1", src: soundAsset+"s7_p13_1.ogg"},
			{id: "s7_p13_2", src: soundAsset+"s7_p13_2.ogg"},
			{id: "s7_p14", src: soundAsset+"s7_p14.ogg"},
			{id: "s7_p15", src: soundAsset+"s7_p15.ogg"},
			{id: "s7_p15_1", src: soundAsset+"s7_p15_1.ogg"},
			{id: "s7_p15_2", src: soundAsset+"s7_p15_2.ogg"},
			{id: "s7_p16", src: soundAsset+"s7_p16.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		switch(countNext){
			case 0:
			case 8:
				sound_player("let_us_try_another_example",1);
			break;
			case 2:
			case 10:
				$('.show2_1').animate({'opacity':'1'},1500);
				$('.show3_1').animate({'opacity':'1'},1500);
				$('.arrow2').animate({'opacity':'1'},1500);
				$('.show_1').animate({'opacity':'1'},1500);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s7_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.show2_2').animate({'opacity':'1'},1500);
					$('.show_2').animate({'opacity':'1'},1500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s7_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						$('.show2_3').animate({'opacity':'1'},1500);
							$('.arrow1').animate({'opacity':'1'},1500);
							$('.show_3').animate({'opacity':'1'},1500);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s7_p"+(countNext+1)+"_2");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(300);
						});
					});
				});
			$('.right_text').css('width','34%');
			$('.right_text_2').css('left','82%');
			$('.arrow1,.arrow2,.show3_1,.show3_2,.show3_3,.show3_4,.show3_5,.show_1,.show_2,.show_3,.show_4,.show_5,.show_6,.show_7,.show_8,.show2_1,.show2_2,.show2_3').css('opacity','0');
			// $('.show2_1').animate({'opacity':'1'},1500);
			// ,function(){
						// ,function(){
							// $('.show2_2').animate({'opacity':'1'},1500);
							// ,function(){
								// ,function(){
								// 	,function(){,function(){
								// 			nav_button_controls(100);
								// 		});
								// });
							// });
						// });
					// });
			// });
			break;

			case 3:
			case 11:
				$('.show3_2').animate({'opacity':'1'},1500);
				$('.show_4').animate({'opacity':'1'},1500);
				$('.point_4').animate({'opacity':'1'},1500);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s7_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.show3_3').animate({'opacity':'1'},1500);
					$('.point_5').animate({'opacity':'1'},1500);
						$('.show_5').animate({'opacity':'1'},1500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s7_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			$('.right_text').css('width','34%');
			$('.right_text_2').css('left','82%');
			$('.point_4,.point_5,.show3_2,.show3_3,.show3_4,.show3_5,.show_4,.show_5,.show_6,.show_7,.show_8,.show2_1,.show2_2,.show2_3').css('opacity','0');

			break;


			case 4:
			case 12:
				$('.right_text').css('width','34%');
				$('.right_text_2').css('left','82%');
				$('.point_8,.point_6,.point_7,.show3_4,.show3_5,.show_6,.show_7,.show_8,.show2_1,.show2_2,.show2_3').css('opacity','0');
				$('.show3_4').animate({'opacity':'1'},1500);
				$('.point_7, .point_6').css({'opacity':'1'});
				$('.show_6').animate({'opacity':'1'},1500);
				$('.show3_5').animate({'opacity':'1'},1500);
				$('.point_7').animate({'opacity':'1'},1500);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s7_p13");
				current_sound.play();
				current_sound.on("complete", function(){
					$('.show_7').animate({'opacity':'1'},1500);
					$('.point_8').animate({'opacity':'1'},1500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s7_p13_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			break;

			case 6:
			case 14:
				$('.right_text').css('width','34%');
				$('.right_text_2').css('left','82%');
				$('.point_1,.point_2,.point_3,.number_1,.number_2,.number_3,.number_4,.mid_1,.mid_2,.mid_3,.mid_4').css('opacity','0');
				$('.number_1').animate({'opacity':'1'},1500);
				$('.point_1').animate({'opacity':'1'},1500);
				$('.mid_1').animate({'opacity':'1'},1500);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s7_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.number_2').animate({'opacity':'1'},1500);
					$('.point_2').animate({'opacity':'1'},1500);
					$('.mid_2').animate({'opacity':'1'},1500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s7_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						$('.number_3').animate({'opacity':'1'},1500);
						$('.point_3').animate({'opacity':'1'},1500);
						$('.mid_3').animate({'opacity':'1'},1500);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s7_p"+(countNext+1)+"_2");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(300);
						});
					});
				});
			// ,function(){
				// ,function(){
					// ,function(){
						// ,function(){
							// ,function(){
								// nav_button_controls(100);
							// });
						// });
					// });
				// });
			// });
			break;

			case 7:
			case 15:
			sound_player("s7_p"+(countNext+1),1);
			$('.number_4,.mid_4,.point_4').css('opacity','0');
			$('.number_4').animate({'opacity':'1'},1500);
			$('.point_4').animate({'opacity':'1'},1500,function(){
				$('.mid_4').animate({'opacity':'1'},1500);
			});
			break;

			default:
			sound_player("s7_p"+(countNext+1),1);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(300):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

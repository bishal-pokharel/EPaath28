var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
	  uppertextblock:[{
	    textdata : data.string.p5text1,
	    textclass : 'left_half',
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			datahighlightcustomclass2:'italic',
	  },{
	    textdata : data.string.p5text2,
	    textclass : 'right_half',
	  }]
	},

	// slide1
	{
	  uppertextblock:[{
	    textdata : data.string.p5text1,
	    textclass : 'left_half',
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			datahighlightcustomclass2:'italic',
	  },{
	    textdata : data.string.p5text2,
	    textclass : 'right_half',
	  },{
	    textdata : data.string.p5text3,
	    textclass : 'right_bottom_half',
			datahighlightflag:true,
			datahighlightcustomclass:'italic',
	  }]
	},

	// slide2
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text4,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text5,
			textclass : 'mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text6,
			textclass : 'bot_text',
			datahighlightflag:true,
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text7,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text5,
			textclass : 'mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text8,
			textclass : 'bot_text',
			datahighlightflag:true,
		}]
	},

	// slide4
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text10,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text5,
			textclass : 'left_text mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text11,
			textclass : 'right_text mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text12,
			textclass : 'bot_text',
			datahighlightflag:true,
		}]
	},

	// slide5
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text13,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text5,
			textclass : 'left_text mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text11,
			textclass : 'right_text mid2',
			datahighlightflag:true,
		}]
	},

	// slide6
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text13,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text11,
			textclass : 'mid_text mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text14,
			textclass : 'bot_text',
			datahighlightflag:true,
		}]
	},


	// slide7
	{
		contentblockadditionalclass:'blue_bg',
		uppertextblock:[{
			textdata : data.string.p5text15,
			textclass : 'top_text',
			datahighlightflag:true,
			datahighlightcustomclass:'italic'
		},{
			textdata : data.string.p5text2,
			textclass : 'mid1',
		},{
			textdata : data.string.p5text11,
			textclass : 'left_text mid2',
			datahighlightflag:true,
		},{
			textdata : data.string.p5text16,
			textclass : 'right_text mid2',
			splitintofractionsflag:true,
		},{
			textdata : data.string.p5text14,
			textclass : 'bot_text',
			datahighlightflag:true,
		}]
	},

	// slide8
	{
		contentblockadditionalclass:'blue_bg',
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.p5text18,
			textclass : 'inside_text',
			imgid: 'speechbubble'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'fairy',
				imgid:'fairygif'
			}]
		}]
	},

	// slide9
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textclass:'left_top_text',
			textdata:data.string.p5text19
		},{
			textclass:'right_top_text',
			textdata:data.string.p5text25
		},{
			textclass:'left_text_1',
			textdata:data.string.p5text20,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		},{
			textclass:'right_1',
			textdata:data.string.p5text21,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		},{
			textclass:'right_2',
			textdata:data.string.p5text23,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		}]
	},

	// slide10
	{
		contentblockadditionalclass:'blue_bg',
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.p5text18,
			textclass : 'inside_text',
			imgid: 'speechbubble'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'fairy',
				imgid:'fairygif'
			}]
		}]
	},

	// slide11
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textclass:'left_top_text',
			textdata:data.string.p5text19
		},{
			textclass:'right_top_text',
			textdata:data.string.p5text28
		},{
			textclass:'left_text_1',
			textdata:data.string.p5text26,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		},{
			textclass:'right_1',
			textdata:data.string.p5text27,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		},{
			textclass:'right_2',
			textdata:data.string.p5text23,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		}]
	},

	// slide12
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textclass:'top_mid_text',
			textdata:data.string.p5text28
		},{
			textclass:'bot_mid_text',
			textdata:data.string.p5text29
		},{
			textclass:'mid_top',
			textdata:data.string.p5text27,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		}]
	},

	// slide13
	{
		contentblockadditionalclass:'cream_bg',
		uppertextblock:[{
			textclass:'top_mid_text',
			textdata:data.string.p5text28
		},{
			textclass:'bot_mid_text',
			textdata:data.string.p5text31,
			datahighlightflag:true,
			datahighlightcustomclass:'first_show',
			datahighlightcustomclass2:'second_show',
			datahighlightcustomclass3:'third_show',
		},{
			textclass:'mid_top mid_textlast',
			textdata:data.string.p5text30,
			splitintofractionsflag:true
		}]
	},






];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairygif", src: imgpath+"fairy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbubble", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2", src: soundAsset+"s5_p2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p3_1", src: soundAsset+"s5_p3_1.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p4_1", src: soundAsset+"s5_p4_1.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p5_1", src: soundAsset+"s5_p5_1.ogg"},
			{id: "s5_p6", src: soundAsset+"s5_p6.ogg"},
			{id: "s5_p6_1", src: soundAsset+"s5_p6_1.ogg"},
			{id: "s5_p7", src: soundAsset+"s5_p7.ogg"},
			{id: "s5_p8", src: soundAsset+"s5_p8.ogg"},
			{id: "s5_p10", src: soundAsset+"s5_p10.ogg"},
			{id: "s5_p10_1", src: soundAsset+"s5_p10_1.ogg"},
			{id: "s5_p10_2", src: soundAsset+"s5_p10_2.ogg"},
			{id: "s5_p10_3", src: soundAsset+"s5_p10_3.ogg"},
			{id: "s5_p10_4", src: soundAsset+"s5_p10_4.ogg"},
			{id: "s5_p12", src: soundAsset+"s5_p12.ogg"},
			{id: "s5_p12_1", src: soundAsset+"s5_p12_1.ogg"},
			{id: "s5_p12_2", src: soundAsset+"s5_p12_2.ogg"},
			{id: "s5_p12_3", src: soundAsset+"s5_p12_3.ogg"},
			{id: "s5_p12_4", src: soundAsset+"s5_p12_4.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p14", src: soundAsset+"s5_p14.ogg"},
			{id: "s5_p14_1", src: soundAsset+"s5_p14_1.ogg"},
			{id: "let_us_try_another_example", src: soundAsset+"let_us_try_another_example.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$('.option1,.option2,.option3').click(function(){
			createjs.Sound.stop();
			if($(this).hasClass('correct')){
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
				$(this).css({'border-color':'#EEFF41','background':'#98C02E','transition':'.2s'});
				$('.option1,.option2,.option3').css('pointer-events','none');
				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-8%,-1053%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
				play_correct_incorrect_sound(1);
				nav_button_controls(100);
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
				var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
					$(this).css({'border-color':'#980000','background':'#FF0000','transition':'.2s'});
				$(this).css('pointer-events','none');
				$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(-8%,-1053%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
				play_correct_incorrect_sound(0);
			}
		});

		$(".inputclass,.inputclassa,.inputclassb,.inputclassc,.inputclassd").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +2+"%";
		$('.top_below_text').css({
			'top':top_val
		});
		switch(countNext) {
			case 2:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.mid2').fadeIn(1000);
					$('.bot_text').fadeIn(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			$('.mid2,.bot_text').hide(0);
			break;
			case 3:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bot_text').fadeIn(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			$('.bot_text').hide(0);
			break;
			case 4:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					$('.bot_text').fadeIn(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			$('.bot_text').hide(0);
			break;
			case 7:
			sound_player("s5_p"+(countNext+1),1);
			$('.mid2').css('width','20%');
			$('.bot_text').hide(1000);
			$('.right_text').css({'text-align':'center','left':'68%'});
			break;

			case 9:
			$('.first_show,.second_show,.third_show,.fourth_show').hide(0);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
          $(".first_show").show(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						$(".second_show").show(1000);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_2");
						current_sound.play();
						current_sound.on("complete", function(){
								$(".third_show").show(1000);
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_3");
							current_sound.play();
							current_sound.on("complete", function(){
									$(".fourth_show").show(1000);
								createjs.Sound.stop();
								var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_4");
								current_sound.play();
								current_sound.on("complete", function(){
									nav_button_controls(300);
								});
							});
						});
					});
				});
			// setTimeout(function(){
      //           setTimeout(function(){
      //               setTimeout(function(){
      //                   setTimeout(function(){
      //                       nav_button_controls(100);
      //                   },1500);
      //               },1500);
      //           },1500);
      //       },1500);
			break;
			case 8:
			case 10:
				sound_player('let_us_try_another_example',1);
			break;

			case 11:
			$('.right_1').css({'width': '32%',
			'left': '57%'});
			$('.right_2').css('left','87%');
			$('.first_show,.second_show,.third_show,.fourth_show').hide(0);
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
          $(".first_show").show(1000);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						$(".second_show").show(1000);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p10_2");
						current_sound.play();
						current_sound.on("complete", function(){
								$(".third_show").show(1000);
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s5_p10_3");
							current_sound.play();
							current_sound.on("complete", function(){
									$(".fourth_show").show(1000);
								createjs.Sound.stop();
								var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_4");
								current_sound.play();
								current_sound.on("complete", function(){
									nav_button_controls(300);
								});
							});
						});
					});
				});

			// $('.first_show').fadeIn(1500,function(){
			// 	$('.second_show').fadeIn(1500,function(){
			// 		$('.third_show').fadeIn(1500,function(){
      //                   $('.fourth_show').fadeIn(1500,function() {
      //                       nav_button_controls(100);
      //                   });
			// 		});
			// 	});
			// });
			break;

			case 12:
			sound_player("s5_p"+(countNext+1),1);
			$('.first_show,.second_show,.third_show').hide(0);
			$('.first_show').fadeIn(1500,function(){
				$('.second_show').fadeIn(1500,function(){
					$('.third_show').fadeIn(1500,function(){
					});
				});
			});
			break;


			case 13:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p"+(countNext+1));
				current_sound.play();
				current_sound.on("complete", function(){
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p"+(countNext+1)+"_1");
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(300);
					});
				});
			$('.first_number,.second_number,.third_number,.fourth_number,.fifth_number,.second_show,.third_show').css('opacity','0');
			$('.first_show').animate({'opacity':'1'},1500);
			$('.first_number').animate({'opacity':'1'},2000,function(){
				$('.second_number').animate({'opacity':'1'},1500,function(){
					$('.third_number').animate({'opacity':'1'},1500,function(){
						$('.second_show').animate({'opacity':'1'},1500);
						$('.fourth_number').animate({'opacity':'1'},1500,function(){
							$('.third_show').animate({'opacity':'1'},1500);
							$('.fifth_number').animate({'opacity':'1'});
							// nav_button_controls(100);
						});
					});
				});
			});
			break;


			default:
				sound_player("s5_p"+(countNext+1),1);
			break;


		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(300):'';
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

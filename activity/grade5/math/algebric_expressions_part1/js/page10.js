
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata : data.string.diytext,
			textclass : 'diytext',
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'bg_full',
				imgid:'diybg'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'purple_bg',
		uppertextblock:[{
			textdata : data.string.p10text1,
			textclass : 'top_text',
		},
		{
			textdata : data.string.p10text7,
			textclass : 'top_1',
		},
		{
			textdata :  data.string.p10text8,
			textclass : 'top_pencil',
		},
		{
			textdata : data.string.p10text6,
			textclass : 'mid_question',
		},
		{
			textdata : data.string.p10text2,
			textclass : 'bot_1',
		},
		{
			textdata : data.string.p9text20,
			textclass : 'check_class',
		},
		{
			textdata : data.string.p9text20,
			textclass : 'check_class1',
		},
		{
			textdata : data.string.p10text9,
			textclass : 'total_text',
		},
		{
			textdata : data.string.p10text3,
			textclass : 'top_2',
		}],
		inputform:[{
			inputclass:'input_m',
			// placeholder:' '
		}],
		inputform1:[
			{
				inputclass:'total_pencils',
				placeholder:'m'
			}
		]
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diybg", src: imgpath+"a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbubble", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			// {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			// {id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			// {id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			// {id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			// {id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			// {id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			// {id: "sound_6", src: soundAsset+"p2_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);


		switch(countNext){
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			$('.mid_question,.bot_1,.check_class,.total_pencils,.total_text,.top_pencil').hide(0);
			$('.total').html('?');
			$('.input_m,.total_pencils').addClass('focus');
			$('.input_m,.total_pencils').click(function(){
				$(this).removeClass('focus');
			});
			$('.check_class1').click(function(){
				if( $('.input_m').val()==24){
					play_correct_incorrect_sound(1);
					$('.check_class1').css({'background':'#98c02e','pointer-events':'none','border-color':'#eeff41'});
					$('.input_m').css({'background':'#98c02e','pointer-events':'none'});
					$('.mid_question,.bot_1,.check_class,.total_pencils,.total_text,.top_pencil').fadeIn(500);
				}
				else{
					$('.check_class1').css({'background':'#ff0000','border-color':'#980000'});
					$('.input_m').css({'background':'#ff0000'});
					play_correct_incorrect_sound(0);

				}
			});

			$('.check_class').click(function(){
				if( $('.total_pencils').val()==12){
					play_correct_incorrect_sound(1);
					$('.check_class,.total').css({'background':'#98c02e','pointer-events':'none','border-color':'#eeff41'});
					$('.total_pencils').css({'background':'#98c02e','pointer-events':'none'});
					$('.mid_question,.bot_1,.check_class,.total_pencils,.total_text,.top_pencil').fadeIn(500);
					$('.total').html(24*12);
					nav_button_controls(100);
				}
				else{
					$('.check_class').css({'background':'#ff0000','border-color':'#980000'});
					$('.total_pencils').css({'background':'#ff0000'});
					play_correct_incorrect_sound(0);

				}
			});
			break;

			default:
			nav_button_controls(100);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

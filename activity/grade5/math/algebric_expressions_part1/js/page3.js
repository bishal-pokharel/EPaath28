var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "top_text",
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[{
			textdata: data.string.p3text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:'fadesin center_image'
			}]
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[,
			{
				textdata: data.string.p3text3,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bold'
			},{
				textdata: data.string.p3text4,
				textclass: "submit_button"
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:'center_image'
			}]
		}],
		inputform:[{
			inputclass:'input-1'
		}]
	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[,
			{
				textdata: data.string.p3text5,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bold'
			},{
				textdata: data.string.p3text6,
				textclass: "instrunction_mid"
			},{
				textdata: data.string.p3text9,
				textclass: "option1"
			},{
				textdata: data.string.p3text10,
				textclass: "option2"
			},{
				textdata: data.string.p3text11,
				textclass: "option3 correct"
			},{
				textdata: '?',
				textclass: "answer_block "
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:'top_change center_image'
			}]
		}]
	},


	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[,
			{
				textdata: data.string.p3text7,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bold'
			},{
				textdata: data.string.p3text8,
				textclass: "instrunction_mid"
			},{
				textdata: data.string.p3text12,
				textclass: "option1"
			},{
				textdata: data.string.p3text13,
				textclass: "option2 correct"
			},{
				textdata: data.string.p3text14,
				textclass: "option3 "
			},{
				textdata: data.string.p3text11,
				textclass: "left_block "
			},{
				textdata: '?',
				textclass: "right_block answer_block "
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:' center_image left_change1'
			},
			{
				imgid:'laddu',
				imgclass:' center_image left_change2'
			}]
		}]
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[,
			{
				textdata: data.string.p3text15,
				textclass: "top_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bold'
			},{
				textdata: data.string.p3text16,
				textclass: "instrunction_mid"
			},{
				textdata: data.string.p3text17,
				textclass: "option1"
			},{
				textdata: data.string.p3text18,
				textclass: "option2 correct"
			},{
				textdata: data.string.p3text19,
				textclass: "option3 "
			},{
				textdata: data.string.p3text11,
				textclass: "left_block1 "
			},{
				textdata: data.string.p3text13,
				textclass: "mid_block1 "
			},{
				textdata: '?',
				textclass: "right_block1 answer_block"
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:'i1'
			},
			{
				imgid:'laddu',
				imgclass:'i2'
			},
			{
				imgid:'laddu',
				imgclass:'i3'
			}]
		}]
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'purple_bg',
		extratextblock:[,
			{
				textdata: data.string.p3text20,
				textclass: "bot_text",
				datahighlightflag:true,
				datahighlightcustomclass:'bold'
			},{
				textdata: data.string.p3text11,
				textclass: "left_block1 "
			},{
				textdata: data.string.p3text13,
				textclass: "mid_block1 "
			},{
				textdata: data.string.p3text19,
				textclass: "right_block1 answer_block"
			}],
		imageblock:[{
			imagestoshow:[{
				imgid:'laddu',
				imgclass:'i1'
			},
			{
				imgid:'laddu',
				imgclass:'i2'
			},
			{
				imgid:'laddu',
				imgclass:'i3'
			}]
		}]
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var inputtednumber=0;
	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "laddu", src: imgpath+"laddu.png", type: createjs.AbstractLoader.IMAGE},
			// soundsa
			// {id: "correct", src: soundAsset+"correct.ogg"},
			// {id: "incorrect", src: soundAsset+"incorrect.ogg"},
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_label_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		$nextBtn.hide(0);
		$(".input-1").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
		// if(countNext==0){
		// 	nav_button_controls(100);
		// }



				$('.option1,.option2,.option3').click(function(){
					createjs.Sound.stop();
					if($(this).hasClass('correct')){
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
						var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
						$(this).css({'border-color':'#EEFF41','background':'#98C02E','transition':'.2s'});
						$('.option1,.option2,.option3').css('pointer-events','none');
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(10%,-136%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter(this);
						play_correct_incorrect_sound(1);
						nav_button_controls(100);
						var correct_text = $('.correct').text();
						$('.answer_block').html(correct_text);
					}
					else{
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$('.coverboardfull').width()+'%';
						var centerY = (100-((position.top + height)*100)/$('.coverboardfull').height())+'%';
							$(this).css({'border-color':'#980000','background':'#FF0000','transition':'.2s'});
						$(this).css('pointer-events','none');
						$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(10%,-136%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter(this);
						play_correct_incorrect_sound(0);
					}
				});


		switch (countNext) {
			case 2:
				sound_player("s3_p"+(countNext+1),0);
				$('.submit_button').click(function(){
					createjs.Sound.stop();
					$(this).css({'filter':'grayscale(100%)','pointer-events':'none'});
					inputtednumber= $('.input-1').val();
					$('.input-1').css({'pointer-events':'none'});
					nav_button_controls(100);
					console.log(inputtednumber);
				});
				break;
				case 3:
				case 4:
				case 5:
				case 6:
					countNext==6?sound_player("s3_p"+(countNext+1),1):sound_player("s3_p"+(countNext+1),0);
				$('.inputtednumber_user').html(inputtednumber);
				// nav_button_controls(100);
				break;
			default:
				sound_player("s3_p"+(countNext+1),1);
				break;
		}

	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id,next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?nav_button_controls():"";
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
		if(content[count].hasOwnProperty("containsdialog")){
			for(var j = 0; j < content[count].containsdialog.length; j++){
				var containsdialog = content[count].containsdialog[j];
				console.log("imageblock", imageblock);
				if(containsdialog.hasOwnProperty('dialogueimgid')){
						var image_src = preload.getResult(containsdialog.dialogueimgid).src;
						//get list of classes
						var classes_list = containsdialog.dialogimageclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_label_image(content, count){
		if(content[count].hasOwnProperty('imageandlabel')){
			var imageandlabel = content[count].imageandlabel;
			for(var i=0; i<imageandlabel.length; i++){
				var image_src = preload.getResult(imageandlabel[i].imgid).src;
				console.log(image_src);
				var classes_list = imageandlabel[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

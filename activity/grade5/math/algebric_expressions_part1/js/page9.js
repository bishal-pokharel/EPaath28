
var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'bg_blue',
		speechbox:[{
			speechbox: 'speechbox1',
			textdata : data.string.p9text1,
			textclass : 'inside_text',
			imgid: 'speechbubble'
		}],
		imageblock:[{
			imagestoshow:[{
				imgclass:'fairy',
				imgid:'fairygif'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'bg_purple',
		uppertextblock:[{
			textclass: 'top_text',
			textdata : data.string.p9text2
		},
		{
			textclass: 'top_below_text',
			textdata : data.string.p9text3
		},
		{
			textclass: 'right_text',
			textdata : data.string.p9text29
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'apple01',
				imgclass:'apple01'
			},
			{
				imgid:'prem',
				imgclass:'prem'
			},
			{
				imgid:'rumi',
				imgclass:'rumi'
			},{
				imgid:'sagar',
				imgclass:'sagar'
			}]
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'bg_purple',
		uppertextblock:[{
			textclass: 'top_text',
			textdata : data.string.p9text3
		},
		{
			textclass: 'top_below_text',
			textdata : data.string.p9text4
		},
		{
			textclass: 'right_text2',
			textdata : data.string.p9text5
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'apple03',
				imgclass:'apple01'
			},
			{
				imgid:'rumi',
				imgclass:'rumi2'
			},{
				imgid:'sagar',
				imgclass:'sagar2'
			}]
		}]
	},

	// slide3
	{
		contentblockadditionalclass:'bg_purple',
		uppertextblock:[{
			textclass: 'top_text',
			textdata : data.string.p9text3
		},
		{
			textclass: 'top_below_text',
			textdata : data.string.p9text6
		},
		{
			textclass: 'right_text2',
			textdata : data.string.p9text7
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'apple02',
				imgclass:'apple01'
			},
			{
				imgid:'prem',
				imgclass:'prem'
			},
			{
				imgid:'rumi',
				imgclass:'rumi'
			},{
				imgid:'sagar',
				imgclass:'sagar'
			}]
		}]
	},

	{
		contentblockadditionalclass:'bg_purple',
		uppertextblock:[{
			textclass: 'top_text',
			textdata : data.string.p9text3
		},
		{
			textclass: 'top_below_text',
			textdata : data.string.p9text6
		},
		{
			textclass: 'checkclass',
			textdata : data.string.p9text10
		},
		{
			textclass: 'question',
			textdata : data.string.p9text9
		},
		{
			textclass: 'solution',
			textdata : data.string.p9text11
		},
		{
			textclass: 'horizontal',
			textdata : data.string.p6text9
		},
		{
			textclass: 'vertical',
			textdata : data.string.p6text11
		},
		{
			textclass: 'horizontal_content',
			textdata : data.string.p9text13
		},
		{
			textclass: 'yclass',
			textdata : data.string.p9text30
		},
		{
			textclass: 'vertical_content',
			textdata : data.string.p9text14,
			splitintofractionsflag:true
		},
		{
			textclass: 'equals',
			textdata : data.string.p9text28
		}],
		inputform:[{
			inputclass:'inputbax'
		}]
	},

	//slide4
		{
			contentblockadditionalclass:'bg_purple',
			uppertextblock:[{
				textclass: 'top_text',
				textdata : data.string.p9text3
			},
			{
				textclass: 'top_below_text',
				textdata : data.string.p9text15
			},
			{
				textclass: 'hor',
				textdata : data.string.p6text9
			},
			{
				textclass: 'ver',
				textdata : data.string.p6text11
			},
			{
				textclass: 'hor_cont',
				textdata : data.string.p9text13
			},
			{
				textclass: 'ver_cont',
				textdata : data.string.p9text14,
				splitintofractionsflag:true
			},
			{
				textclass: 'bot_text',
				textdata : data.string.p9text16
			}]
		},

		{
			contentblockadditionalclass:'bg_purple',
			uppertextblock:[{
				textclass: 'top_text',
				textdata : data.string.p9text17
			},
			{
				textclass: 'top_below_text',
				textdata : data.string.p9text18
			},
			{
				textclass: 'y_equal_class',
				textdata : data.string.p9text31
			},
			{
				textclass: 'hint_text',
				textdata : data.string.p9text27
			},
			{
				textclass: 'roll_number',
				textdata : '50'
			}],
			imageblock:[{
				imagestoshow:[{
					imgclass:'fairy',
					imgid:'fairygif'
				},
				{
					imgid:'triangle',
					imgclass:'increaser'
				},
				{
					imgid:'triangle',
					imgclass:'decreaser'
				}]
			}],
			speechbox:[{
		    speechbox: 'sp-2 ',
		    textdata : data.string.p9text19,
		    textclass : 'text_inside fadesIn',
		    imgid : 'speechbubble',
				imgclass:'flipped',
		    imgsrc: '',
		    // audioicon: true,
		  }]
		},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var p,q;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairygif", src: imgpath+"fairy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "speechbubble", src: imgpath+"speechbubble.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple01", src: imgpath+"apple01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple02", src: imgpath+"apple02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "apple03", src: imgpath+"apple03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem", src: imgpath+"prem.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rumi", src: imgpath+"rumi.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sagar", src: imgpath+"sagar.png", type: createjs.AbstractLoader.IMAGE},
			{id: "triangle", src: imgpath+"timer.png", type: createjs.AbstractLoader.IMAGE},

			{id: "sp-dialogue", src: imgpath+"thinking_cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sp-dialogue1", src: imgpath+"speechbubble-02.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s9_p1", src: soundAsset+"s9_p1.ogg"},
			{id: "s9_p2", src: soundAsset+"s9_p2.ogg"},
			{id: "s9_p2_1", src: soundAsset+"s9_p2_1.ogg"},
			{id: "s9_p3", src: soundAsset+"s9_p3.ogg"},
			{id: "s9_p4", src: soundAsset+"s9_p4.ogg"},
			{id: "s9_p6", src: soundAsset+"s9_p6.ogg"},
			{id: "s9_p7", src: soundAsset+"s9_p7.ogg"},
			{id: "s9_p7_1", src: soundAsset+"s9_p7_1.ogg"},
			{id: "s9_p7_4", src: soundAsset+"s9_p7_4.ogg"},
			{id: "let_us_try_another_example", src: soundAsset+"let_us_try_another_example.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightstarttag2;
			var texthighlightstarttag3;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

						$(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						(stylerulename2 = "parsedstring2") ;

						$(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						(stylerulename3 = "parsedstring3") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
	/*=====  End of data highlight function  ======*/
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


					tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);

		$(".inputbax").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});


		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +2+"%";
		$('.top_below_text').css({
			'top':top_val
		});

		switch(countNext){
			case 1:
					sound_player1("s9_p2","s9_p2_1");
                 break;
			case 4:
						$('.solution,.horizontal,.horizontal_content,.vertical,.vertical_content').hide(0);
						$('.inputbax').addClass('focus').click(function(){
							$('.inputbax').removeClass('focus');
						});
						$(".checkclass").click(function(){
							var input_value=$(".inputbax").val();
							if(input_value==14)
							{
								$('.inputbax,.checkclass').css({'background':'#98c02e','border-color':'#eeff41','pointer-events':'none'});
								$('.tryagainclass').css({'pointer-events':'none'});
								play_correct_incorrect_sound(1);
								nav_button_controls(100);
							}
							else{
								$('.solution,.horizontal,.horizontal_content,.vertical,.vertical_content').fadeIn(500);
								$('.inputbax,.checkclass').css({'background':'#ff0000','border-color':'#980000'});
								play_correct_incorrect_sound(0);
							}
						});
						break;
			case 6:
				// sound_player("s9_p"+(countNext+1),1);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s9_p7");
					current_sound.play();
					current_sound.on("complete", function(){
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s9_p7_1");
						current_sound.play();
						current_sound.on("complete", function(){
							$('.sp-2 ,.fairy').fadeIn(1000,function(){
									$('.y_equal_class, .roll_number, .increaser, .decreaser').fadeIn(1000,function(){
										$('.hint_text').fadeIn(1000);
									});
							});
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s9_p7_4");
							current_sound.play();
							current_sound.on("complete", function(){
								nav_button_controls(0);
								$('.increaser').click(function(){
									nav_button_controls(100);
									var initial_text = $('.roll_number').text();
									initial_text++;
									if(initial_text==99){
										$('.increaser').css({'opacity':'.5','pointer-events':'none'});
										$('.roll_number,.input_number').html(initial_text);
										$('.input_number_product').html(initial_text*14);
									}
									if(initial_text<99){
										$('.decreaser').css({'opacity':'1','pointer-events':'auto'});
										$('.roll_number,.input_number').html(initial_text);
										$('.input_number_product').html(initial_text*14);
									}

								});

								$('.decreaser').click(function(){
									var initial_text = $('.roll_number').text();
									initial_text--;
									if(initial_text==0){
										$('.decreaser').css({'opacity':'.5','pointer-events':'none'});
										$('.roll_number,.input_number').html(initial_text);
										$('.input_number_product').html(initial_text*14);
									}
									if(initial_text>0){
										$('.increaser').css({'opacity':'1','pointer-events':'auto'});
										$('.roll_number,.input_number').html(initial_text);
										$('.input_number_product').html(initial_text*14);
									}
								});
							});
						});
					});
			$('.sp-2 ,.fairy, .hint_text, .y_equal_class, .roll_number, .increaser, .decreaser').hide(0);
			var initial_text_begin = $('.roll_number').text();
			$('.input_number').html(initial_text_begin);
			$('.input_number_product').html(initial_text_begin*14);

			break;
			default:
				sound_player("s9_p"+(countNext+1),1);
			break;

		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
	    var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			next?nav_button_controls(0):'';
		});
	}
	function sound_player1(sound_id, sound_id2){
		createjs.Sound.stop();
	    var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play(sound_id2);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);	
			});
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

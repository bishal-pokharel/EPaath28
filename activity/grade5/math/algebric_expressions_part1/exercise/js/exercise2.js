var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var sound_l_1 = new buzz.sound((soundAsset + "ex2_1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "ex2_2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "ex2_3.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "ex2_5.ogg"));

var content=[
	//ex1
	{
	additionalclasscontentblock:'purple_bg',
	instructions:[{
		textclass:'instruction',
		textdata:data.string.instruction_2
	},],
  extratext:[
	{
		textclass:'question',
		textdata:data.string.q_1
	},
	{
		textclass:'horizontal',
		textdata:data.string.horizontal
	},
	{
		textclass:'vertical',
		textdata:data.string.vertical
	},
	{
		textclass:'horizontal_content',
		textdata:data.string.exetyp2opt1_o1
	},
	{
		textclass:'vertical_content',
		textdata:data.string.exetyp2opt2_o1,
		splitintofractionsflag:true
	},
	{
		textclass:'check_1',
		textdata:data.string.check
	},
	{
		textclass:'check_2',
		textdata:data.string.check
	},
	{
		textclass:'check_3',
		textdata:data.string.check
	}]
	},


	//ex2
	{
	additionalclasscontentblock:'purple_bg',
	instructions:[{
		textclass:'instruction',
		textdata:data.string.instruction_3
	},],
	extratext:[
	{
		textclass:'question',
		textdata:data.string.q_1
	},
	{
		textclass:'horizontal',
		textdata:data.string.horizontal
	},
	{
		textclass:'vertical',
		textdata:data.string.vertical
	},
	{
		textclass:'horizontal_content',
		textdata:data.string.exetyp2opt1_o1
	},
	{
		textclass:'vertical_content',
		textdata:data.string.exetyp2opt2_o1,
		splitintofractionsflag:true
	},
	{
		textclass:'check_1',
		textdata:data.string.check
	},
	{
		textclass:'check_2',
		textdata:data.string.check
	},
	{
		textclass:'check_3',
		textdata:data.string.check
	}]
	},

	//ex3
	{
	additionalclasscontentblock:'purple_bg',
	instructions:[{
		textclass:'instruction',
		textdata:data.string.instruction_4
	},],
	extratext:[
	{
		textclass:'question',
		textdata:data.string.q3_1
	},
	{
		textclass:'input_question',
		textdata:data.string.exetyp4opt1_o1
	},
	{
		textclass:'check',
		textdata:data.string.check
	}]
	},

	//ex4
	{
	additionalclasscontentblock:'purple_bg',
	instructions:[{
		textclass:'instruction',
		textdata:data.string.instruction_4
	},],
	extratext:[
	{
		textclass:'question',
		textdata:data.string.q4_1
	},
	{
		textclass:'input_question',
		textdata:data.string.exetyp5opt1_o1
	},
	{
		textclass:'check',
		textdata:data.string.check
	}]
	},

	//ex5
	{
	additionalclasscontentblock:'purple_bg',
	instructions:[{
		textclass:'instruction',
		textdata:data.string.instruction_5
	},],
	extratext:[
	{
		textclass:'question',
		textdata:data.string.q5_1
	},
	{
		textclass:'main_question',
		textdata:data.string.ver_ques_1
	},
	{
		textclass:'vertical_method',
		textdata:data.string.p6text11
	},
	{
		textclass:'check_a',
		textdata:data.string.check
	},
	{
		textclass:'check_b',
		textdata:data.string.check
	},
	{
		textclass:'sign_1',
		textdata:data.string.plus
	},
	{
		textclass:'sign_2',
		textdata:data.string.plus
	},
	{
		textclass:'sign_3',
		textdata:data.string.plus
	},
	{
		textclass:'sign_4',
		textdata:data.string.plus
	},
	{
		textclass:'sign_5',
		textdata:data.string.plus
	},
	{
		textclass:'input_1',
		textdata:data.string.q5ver_ques3_o1
	},
	{
		textclass:'input_2',
		textdata:data.string.q5ver_ques4_o1
	},
	{
		textclass:'input_3',
		textdata:data.string.q5ver_ques5_o1
	},
	{
		textclass:'only_line'
	}
],
	images:[{
		imgsrc:imgpath + 'timer.png',
		imgclass:'inc-1'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'inc-2'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'inc-3'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'inc-4'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'inc-5'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'dec-1'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'dec-2'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'dec-3'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'dec-4'
	},{
		imgsrc:imgpath + 'timer.png',
		imgclass:'dec-5'
	}]
		},


];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var currentSound = sound_l_1;
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("multiopscontent", $("#multiopscontent-partial").html());

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var exeTemp = new NumberTemplate();

 	exeTemp.init(5);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		exeTemp.numberOfQuestions();

		var ansClicked = false;
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;
    switch (countNext) {
      case 0:
			sound_l_1.play();
			var quesNo = rand_generator(5);
      $('.question').html(eval('data.string.q_'+quesNo));
			$('.horizontal_content').html(eval('data.string.exetyp7opt1_o'+quesNo));
			$('.vertical_content').html(eval('data.string.exetyp7opt2_o'+quesNo));
			var correct_answers= [[[5,3],8],[[3,27],30],[[10,18],28],[[13,8],21],[[16,17],33]];
			$('.hor_hide,.ver_hide,.check_2,.check_3').hide(0);
			var answertrue1 = false;
			var answertrue2 = false;
			var answertrue3 = false;
			var wrongclicked = false;
			var wrongclicked1 = false;
			var wrongclicked2 = false;
			$('.check_1').click(function(){
				currentSound.stop();
				if( parseInt($('.first_input').val()) + parseInt($('.second_input').val()) == correct_answers[quesNo - 1][0][0]+correct_answers[quesNo - 1][0][1]){
					play_correct_incorrect_sound(1);
					$('.hor_hide,.check_2').show(200);
					$('.first_input,.second_input,.check_1').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					if(!wrongclicked){
						answertrue1 = true;
					}

					correctactions('check_1');
				}
				else{
					incorrectactions('check_1');
					play_correct_incorrect_sound(0);
					$('.first_input,.second_input,.check_1').css({'background':'#ff0000','border-color':'#9b0000','color':'black'});

					wrongclicked=true;
				}
			});

			$('.check_2').click(function(){
				if(parseInt($('.third_input').val()) == correct_answers[quesNo - 1][1]){
					play_correct_incorrect_sound(1);
					$('.ver_hide,.check_3').show(200);
					$('.third_input,.check_2').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					correctactions('check_2');
					if(!wrongclicked1){
						answertrue2 = true;
					}
				}
				else{
					incorrectactions('check_2');
					play_correct_incorrect_sound(0);
					$('.third_input,.check_2').css({'background':'#ff0000','border-color':'#9b0000'});
					wrongclicked1=true;
				}
			});

			$('.check_3').click(function(){
				console.log (answertrue1 + '&' + answertrue2);
				if(parseInt($('.fourth_input').val()) == correct_answers[quesNo - 1][1]){
					play_correct_incorrect_sound(1);
					$nextBtn.show(100);
					correctactions('check_3');
					$('.fourth_input,.check_3').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					if(!wrongclicked2){
						answertrue3 = true;
					}
				}
				else{
					incorrectactions('check_3');
					play_correct_incorrect_sound(0);
					$('.fourth_input,.check_3').css({'background':'#ff0000','border-color':'#9b0000'});
					wrongclicked2=true;
				}
				if( answertrue1 && answertrue2 && answertrue3 ){
					exeTemp.update(true);
				}
				if( !(answertrue1 && answertrue2 && answertrue3) ){
					exeTemp.update(false);
				}
			});
			break;

			case 1:
			currentSound = sound_l_2;
			currentSound.play();
			var quesNo = rand_generator(5);
			$('.question').html(eval('data.string.q2_'+quesNo));
			$('.horizontal_content').html(eval('data.string.exetyp8opt1_o'+quesNo));
			$('.vertical_content').html(eval('data.string.exetyp8opt2_o'+quesNo));
			var correct_answers= [[[10,3],7],[[16,9],7],[[45,13],32],[[20,16],4],[[37,27],10]];
			$('.hor_hide,.ver_hide,.check_2,.check_3').hide(0);
			var answertrue1 = false;
			var answertrue2 = false;
			var answertrue3 = false;
			var wrongclicked = false;
			var wrongclicked1 = false;
			var wrongclicked2 = false;
			$('.check_1').click(function(){
				currentSound.stop();
				if( $('.first_input').val()==correct_answers[quesNo - 1][0][0]  &&  $('.second_input').val()==correct_answers[quesNo - 1][0][1]){
					play_correct_incorrect_sound(1);
					$('.hor_hide,.check_2').show(200);
					$('.first_input,.second_input,.check_1').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					if(!wrongclicked){
						answertrue1 = true;
					}

					correctactions('check_1');
				}
				else{
					incorrectactions('check_1');
					play_correct_incorrect_sound(0);
					$('.first_input,.second_input,.check_1').css({'background':'#ff0000','border-color':'#9b0000','color':'black'});

					wrongclicked=true;
				}
			});

			$('.check_2').click(function(){
				if(parseInt($('.third_input').val()) == correct_answers[quesNo - 1][1]){
					play_correct_incorrect_sound(1);
					$('.ver_hide,.check_3').show(200);
					$('.third_input,.check_2').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					correctactions('check_2');
					if(!wrongclicked1){
						answertrue2 = true;
					}
				}
				else{
					incorrectactions('check_2');
					play_correct_incorrect_sound(0);
					$('.third_input,.check_2').css({'background':'#ff0000','border-color':'#9b0000'});
					wrongclicked1=true;
				}
			});

			$('.check_3').click(function(){
				console.log (answertrue1 + '&' + answertrue2);
				if(parseInt($('.fourth_input').val()) == correct_answers[quesNo - 1][1]){
					play_correct_incorrect_sound(1);
					$nextBtn.show(100);
					correctactions('check_3');
					$('.fourth_input,.check_3').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					if(!wrongclicked2){
						answertrue3 = true;
					}
				}
				else{
					incorrectactions('check_3');
					play_correct_incorrect_sound(0);
					$('.fourth_input,.check_3').css({'background':'#ff0000','border-color':'#9b0000'});
					wrongclicked2=true;
				}
				if( answertrue1 && answertrue2 && answertrue3 ){
					exeTemp.update(true);
				}
				if( !(answertrue1 && answertrue2 && answertrue3) ){
					exeTemp.update(false);
				}
			});
			break;


			 case 2:
 			currentSound = sound_l_3;
 			currentSound.play();
			 var quesNo = rand_generator(5);
 			$('.question').html(eval('data.string.q3_'+quesNo));
 			$('.input_question').html(eval('data.string.exetyp9opt1_o'+quesNo));
 			var correct_answers= [[2,7],[1,1],[9,2],[8,1],[6,5]];
 			$('.hor_hide,.ver_hide,.check_2,.check_3').hide(0);
 			var answertrue1 = false;
 			var wrongclicked = false;
 			$('.check').click(function(){
				currentSound.stop();
 				console.log (answertrue1 + '&' + answertrue2);
 				if( $('.first_input').val() == correct_answers[quesNo - 1][0] && $('.second_input').val() == correct_answers[quesNo - 1][1]){
 					play_correct_incorrect_sound(1);
 					$nextBtn.show(100);
 					correctactions('check');
 					$('.first_input,.second_input,.check').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
 					if(!wrongclicked){
						exeTemp.update(true);
 					}
 				}
 				else{
					exeTemp.update(false);
 					incorrectactions('check');
 					play_correct_incorrect_sound(0);
 					$('.first_input,.second_input,.check').css({'background':'#ff0000','border-color':'#9b0000'});
 					wrongclicked=true;
 				}
 			});
 			break;


			case 3:

			var quesNo = rand_generator(5);
		 $('.question').html(eval('data.string.q4_'+quesNo));
		 $('.input_question').html(eval('data.string.exetyp10opt1_o'+quesNo));
		 var correct_answers= [[10,5],[0,5],[9,10],[2,12],[1,2]];
		 $('.hor_hide,.ver_hide,.check_2,.check_3').hide(0);
		 var answertrue1 = false;
		 var wrongclicked = false;
		 $('.check').click(function(){
			 console.log (answertrue1 + '&' + answertrue2);
			 if( $('.first_input').val() == correct_answers[quesNo - 1][0] && $('.second_input').val() == correct_answers[quesNo - 1][1]){
				 play_correct_incorrect_sound(1);
				 $nextBtn.show(100);
				 correctactions('check');
				 $('.first_input,.second_input,.check').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
				 if(!wrongclicked){
					 exeTemp.update(true);
				 }
			 }
			 else{
				 exeTemp.update(false);
				 incorrectactions('check');
				 play_correct_incorrect_sound(0);
				 $('.first_input,.second_input,.check').css({'background':'#ff0000','border-color':'#9b0000'});
				 wrongclicked=true;
			 }
		 });
		 break;

		 case 4:
		 currentSound = sound_l_5;
		 currentSound.play();
		 var quesNo = rand_generator(5);
		 $('.sign_4,.sign_5,.check_b,.input_1,.input_2,.input_3,.inc-4,.inc-5,.dec-4,.dec-5').hide(0);
		 $('.main_question').html(eval('data.string.ver_ques_'+quesNo));
		 $('.question').html(eval('data.string.q5_'+quesNo));
		 $('.input_1').html(eval('data.string.q5ver_ques3_o'+quesNo));
		 $('.input_2').html(eval('data.string.q5ver_ques4_o'+quesNo));
		 $('.input_3').html(eval('data.string.q5ver_ques5_o'+quesNo));
		 change_sign('inc-1','dec-1','sign_1');
		 change_sign('inc-2','dec-2','sign_2');
		 change_sign('inc-3','dec-3','sign_3');
		 change_sign('inc-4','dec-4','sign_4');
		 change_sign('inc-5','dec-5','sign_5');
		 var correct_answers= [['-','+','+',6,'+',8,'+',5],
												 	['-','-','+',1,'-',4,'-',0],
													['-','+','-',8,'+',24,'-',3],
													['-','-','-',0,'-',2,'-',8],
													['-','+','-',1,'+',6,'+',10]];

			var answertrue1 = false;
			var answertrue2 = false;
			var wrongclicked1 = false;
			var wrongclicked2 = false;

			$('.check_a').click(function(){
				currentSound.stop();
				if( $('.sign_1').text()==correct_answers[quesNo-1][0] && $('.sign_2').text()==correct_answers[quesNo-1][1] && $('.sign_3').text()==correct_answers[quesNo-1][2] ){
					correctactions('check_a');
					$('.sign_1,.sign_2,.sign_3,.check_a').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
					$('.inc-1,.inc-2,.inc-3,.dec-1,.dec-2,.dec-3').css({'pointer-events':'none','display':'none'});
					$('.sign_4,.sign_5,.check_b,.input_1,.input_2,.input_3,.inc-4,.inc-5,.dec-4,.dec-5').fadeIn(500);
					$nextBtn.show(0);
					if(!wrongclicked1){
						answertrue1 = true;
					}
				}
				else{
					incorrectactions('check_a');
					wrongclicked1=true;
				}
			});

			$('.check_b').click(function(){
				if(quesNo==2){
					if( parseInt($('.first_input1').val())==correct_answers[quesNo-1][3] &&
					$('.sign_4').text()==correct_answers[quesNo-1][4] &&
					parseInt($('.second_input1').val())==correct_answers[quesNo-1][5] &&
					($('.sign_5').text()==correct_answers[quesNo-1][6] || $('.sign_5').text()=='+') &&
					parseInt($('.third_input1').val())==correct_answers[quesNo-1][7]){
						correctactions('check_b');
						$('.sign_4,.sign_5,.check_b,.first_input1,.second_input1,.third_input1').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
						$('.inc-4,.inc-5,.dec-4,.dec-5').css({'pointer-events':'none','display':'none'});
						$nextBtn.show(0);
						if(!wrongclicked2){
							answertrue2 = true;
						}
					}
					else{
						incorrectactions('check_b');
						wrongclicked2=true;
					}
				}

				else{
					if( parseInt($('.first_input1').val())==correct_answers[quesNo-1][3] &&
					$('.sign_4').text()==correct_answers[quesNo-1][4] &&
					parseInt($('.second_input1').val())==correct_answers[quesNo-1][5] &&
					$('.sign_5').text()==correct_answers[quesNo-1][6] &&
					parseInt($('.third_input1').val())==correct_answers[quesNo-1][7]){
						correctactions('check_b');
						$('.sign_4,.sign_5,.check_b,.first_input1,.second_input1,.third_input1').css({'background':'#98C02E','pointer-events':'none','border-color':'#d5e93a'});
						$('.inc-4,.inc-5,.dec-4,.dec-5').css({'pointer-events':'none','display':'none'});
						if(!wrongclicked2){
							answertrue2 = true;
						}
					}
					else{
						incorrectactions('check_b');
						wrongclicked2=true;
					}
				}

				if( answertrue1 && answertrue2){
					exeTemp.update(true);
					console.log(answertrue1);
				}
				if( !(answertrue1 && answertrue2) ){
					exeTemp.update(false);
				}

			});


		 break;

    }
		texthighlight($board);
		splitintofractions($board);

		function rand_generator(limit){
			var randNum = Math.floor((Math.random() * limit) + 1);
			return randNum;
		}

		function change_sign(increaser_class,decreaser_class,sign_class){
			$('.'+increaser_class).click(function(){
				if($('.'+sign_class).text()=='+'){
					$('.'+sign_class).html('-');
				}
				else{
					$('.'+sign_class).html('+');
				}
			});
			$('.'+decreaser_class).click(function(){
				if($('.'+sign_class).text()=='+'){
					$('.'+sign_class).html('-');
				}
				else{
					$('.'+sign_class).html('+');
				}
			});
		}

		function correctactions(button_class){
			$('.incorrect').hide(0);
			var $this = $('.'+button_class);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.board').width()+'%';
			var centerY = (100-((position.top + height)*100)/$('.board').height())+'%';
			$('<img style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(220%,0%);z-index:2;" src="'+imgpath +'correct.png" />').insertAfter('.'+button_class);
		}

		function incorrectactions(button_class){
			var $this = $('.'+button_class);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$('.board').width()+'%';
			var centerY = (100-((position.top + height)*100)/$('.board').height())+'%';
			$('<img class="incorrect" style="left:'+centerX+';bottom:'+centerY+';position:absolute;height:5%;transform:translate(220%,0%);z-index:2;" src="'+imgpath +'incorrect.png" />').insertAfter('.'+button_class);
		}

		$(".first_input, .second_input, .third_input, .fourth_input").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 45 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
		/*for randomizing the options*/

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		// for exeTempg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		// exeTempg purpose code ends


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		currentSound.stop();
		exeTemp.gotoNext();
		templateCaller();

	});
	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
	 function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
				}
		}

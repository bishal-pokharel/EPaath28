var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_l_1 = new buzz.sound((soundAsset + "ex1_1.ogg"));
var content=[
	//ex1
	{
    instruction: data.string.exques1,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },
          {
          },
          {
          }
        ]
			}
		]
	},
	//ex2
	{
    instruction: data.string.exques1,
		exetype1: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "clickplace",
        exeoptions:[
          {
            optaddclass: "correct",
          },
          {
          },
          {
          },
          {
          }
        ]
			}
		]
	},
	//ex2
	{
		instruction: data.string.exques1,
		exetype1: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{
						optaddclass: "correct",
					},
					{
					},
					{
					},
					{
					}
				]
			}
		]
	},
	//ex2
	{
		instruction: data.string.exques1,
		exetype1: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{
						optaddclass: "correct",
					},
					{
					},
					{
					},
					{
					}
				]
			}
		]
	},
	//ex2
	{
		instruction: data.string.exques1,
		exetype1: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{
						optaddclass: "correct",
					},
					{
					},
					{
					},
					{
					}
				]
			}
		]
	},
	//ex2
	{
		instruction: data.string.exques1,
		exetype1: [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "clickplace",
				exeoptions:[
					{
						optaddclass: "correct",
					},
					{
					},
					{
					},
					{
					}
				]
			}
		]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var currentSound  = sound_l_1;

	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("multiopscontent", $("#multiopscontent-partial").html());

	/*for limiting the questions to 10*/
	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var exeTemp = new NumberTemplate();

 	exeTemp.init(6);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide();
		$prevBtn.hide();

		/*generate question no at the beginning of question*/
		exeTemp.numberOfQuestions();

		var ansClicked = false;
		var wrngClicked = false;
		var multiCorrCounter = 0;
		var multiClick = 0;

    switch (countNext) {
      default:
			countNext==0?sound_l_1.play():'';
			console.log(countNext+1);
			var quesNo = rand_generator(5);
      $('.question').html(eval('data.string.exetyp'+(countNext+1)+'_o'+quesNo));
      $(".buttonsel:eq(0)").html(eval('data.string.exetyp'+(countNext+1)+'opt1_o'+quesNo));
      $(".buttonsel:eq(1)").html(eval('data.string.exetyp'+(countNext+1)+'opt2_o'+quesNo));
			$(".buttonsel:eq(2)").html(eval('data.string.exetyp'+(countNext+1)+'opt3_o'+quesNo));
			$(".buttonsel:eq(3)").html(eval('data.string.exetyp'+(countNext+1)+'opt4_o'+quesNo));
			break;
    }
		texthighlight($board);

		function correct_action($curBox, fracfl){
			var addfactor = $curBox.parent().position();
			var posIt = $curBox.position();
			var icoLeft = posIt.left + addfactor.left + ($curBox.width()/2);
			var icoTop = posIt.top;
				$curBox.parent().append("<img class='imagestarttyp2' src= 'images/correct.png'>");
				if(fracfl){
					if($(".fraction2").find("input").parent(".top").length)
						$(".imagestarttyp2:eq(0)").addClass("corrfrac");
					else
						$(".imagestarttyp2:eq(0)").addClass("corrfrac2");
				}
				loopCounter++;
				$curBox.hide(0);
				$curBox.parent().find("input").prop('disabled', true).css({
					"background":"#98C02E",
					"border":"0.1em solid #DEEF3C",
					"color":"white"
				});
				if($(".eachloop:eq("+loopCounter+")").length){
					$(".eachloop:eq("+loopCounter+")").show(0);
					$(".eachloop:eq("+loopCounter+")").find("#input1").focus();
				}
				else{
					if(scoreFlagSet == true)
						exeTemp.update(true);
					if(countNext != $total_page)
					$nextBtn.show();
				}
				play_correct_incorrect_sound(true);
		}

		function incorrect_action($me){
			scoreFlagSet = false;
			play_correct_incorrect_sound(false);
			$me.css({
				"background":"#FF0000",
				"border":"0.1em solid #980000",
				"color":"white"
			});
		}

		$("input").keypress(function (e) {
			if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
				return false;
			}
			if(e.which == 13) {
				if($(this).siblings("input").length){
					var totinpNumbers = $(this).siblings("input").length;
					var curinpNumber = $(this).index();
					console.log(curinpNumber, totinpNumbers);
					if(curinpNumber == totinpNumbers)
						$(this).siblings(".chckbtn").trigger("click");
					else
					$(this).siblings("input:eq("+curinpNumber+")").focus();
				}
				else if($(this).siblings(".chckbtn").length)
					$(this).siblings(".chckbtn").trigger("click");
				else {
					$(this).parent().parent().siblings(".chckbtn").trigger("click");
				}
			}
		});
		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

    function rand_generator(limit){
      var randNum = Math.floor((Math.random() * limit) + 1);
      return randNum;
    }

		$(".buttonsel").click(function(){
			currentSound.stop();
      var $curBox = $(this);
      var addfactor = $(".optionsdiv").position();
      var posIt = $curBox.position();
      var icoLeft;
			if($(".buttonsel").length > 2)
				icoLeft = posIt.left + addfactor.left + $curBox.width()+50;
			else
				icoLeft = posIt.left + addfactor.left + $curBox.width()+ 50;

			$(this).removeClass('forhover');
				if(ansClicked == false){
					if($(this).hasClass("correct")){

						if(wrngClicked == false){
							exeTemp.update(true);
						}
						var corval = $(this).text();
						$(".clickplace").text(corval).css("color","#0B9620");
						play_correct_incorrect_sound(1);
						$(this).css("background","#bed62f");
						$(this).css("border","5px solid #deef3c");
            $(this).css("color","white");
						//$('.hint_image').show(0);
            $(this).siblings(".corctopt").show(0).css({
              "left": icoLeft
            });

						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;


						if(countNext != $total_page)
						$nextBtn.show();
					}
					else{
						exeTemp.update(false);
						play_correct_incorrect_sound(0);
						$(this).css("background","#FF0000");
						$(this).css("border","5px solid #980000");
						$(this).css("color","white");
            $(this).siblings(".wrngopt").show(0).css({
              "left": icoLeft
            });
						wrngClicked = true;
					}
				}
			});

			$(".multimcqiten").click(function(){
				var $curBox = $(this);
				if($curBox.hasClass("forhover")){
					var addfactor = $(".multiopsblock").position();
					var posIt = $curBox.position();
					var icoLeft;
					icoLeft = posIt.left + $curBox.width() + 15;
					$(this).removeClass('forhover');
							if($(this).hasClass("corritem")){
								multiClick++;
								if(wrngClicked == false){
									multiCorrCounter++;
								}
								if(multiCorrCounter == 3){
									exeTemp.update(true);
								}
								if(multiClick == 3){
									$(".multimcqiten").removeClass("forhover");
									$nextBtn.show();
								}
								play_correct_incorrect_sound(1);
								$(this).css("background","#bed62f");
								$(this).css("border","5px solid #deef3c");
								$(this).css("color","white");
								//$('.hint_image').show(0);
								$(this).siblings(".corctopt").show(0).css({
									"left": icoLeft
								});
							}
							else{
								exeTemp.update(false);
								play_correct_incorrect_sound(0);
								$(this).css("background","#FF0000");
								$(this).css("border","5px solid #980000");
								$(this).css("color","white");
								$(this).siblings(".wrngopt").show(0).css({
									"left": icoLeft
								});
								wrngClicked = true;
							}
				}

				});
		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		// for exeTempg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		// exeTempg purpose code ends


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		exeTemp.gotoNext();
		templateCaller();

	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

	 /*===== This function splits the string in data into convential fraction used in mathematics =====*/
	 function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
				}
		}

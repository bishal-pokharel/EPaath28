var imgpath = $ref+"/exercise/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext27,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext23,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext24,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext25,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext26,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide1
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19a,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext27,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext24,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext23,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext25,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext26,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide2
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19b,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20b,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21b,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22b,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext27,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext24b,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext23b,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext25b,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext26b,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide3
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19c,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20c,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21c,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22c,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext27,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext23c,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext24c,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext25c,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext26c,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide4
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19d,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20d,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21d,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22d,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext27,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext25d,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext24d,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext23d,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext26d,
			textclass: "bottom-text1 ",
		}
	],
	},

	// slide5
	{
		contentblockadditionalclass:'bg_yellow',
		uppertextblock :[{
			textdata:  data.string.ext19e,
			textclass: "top-text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},],
		extratextblock:[
		{
			textdata:  data.string.ext20e,
			textclass: "text-5",
		},
		{
			textdata:  data.string.ext21e,
			textclass: "text-5 ",
		},
		{
			textdata:  data.string.ext22e,
			textclass: "text-5",
		},
		{
			textdata:   data.string.ext22ea,
			textclass: "text-5",
		},
		{
			textdata:  data.string.p3text27,
			textclass: "instruction",
		},
		{
			textdata:  data.string.ext25e,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext24e,
			textclass: "optionclass1 ",
		},
		{
			textdata:  data.string.ext23e,
			textclass: "optionclass1 correct",
		},
		{
			textdata:  data.string.ext26e,
			textclass: "bottom-text1 ",
		}
	],
	},


		// slide6
		{
			contentblockadditionalclass:'bg_yellow',
			uppertextblock :[{
				textdata:  data.string.ext19f,
				textclass: "top-text",
				datahighlightflag:true,
				datahighlightcustomclass:'orange'
			},],
			extratextblock:[
			{
				textdata:  data.string.ext20f,
				textclass: "text-5",
			},
			{
				textdata:  data.string.ext21f,
				textclass: "text-5 ",
			},
			{
				textdata:  data.string.ext22f,
				textclass: "text-5",
			},
			{
				textdata:   data.string.ext22ef,
				textclass: "text-5",
			},
			{
				textdata:  data.string.p3text27,
				textclass: "instruction",
			},
			{
				textdata:  data.string.ext23f,
				textclass: "optionclass1 correct",
			},
			{
				textdata:  data.string.ext24f,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext25f,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext26f,
				textclass: "bottom-text1 ",
			}
		],
		},

		// slide7
		{
			contentblockadditionalclass:'bg_yellow',
			uppertextblock :[{
				textdata:  data.string.ext19g,
				textclass: "top-text",
				datahighlightflag:true,
				datahighlightcustomclass:'orange'
			},],
			extratextblock:[
			{
				textdata:  data.string.ext20g,
				textclass: "text-5",
			},
			{
				textdata:  data.string.ext21g,
				textclass: "text-5 ",
			},
			{
				textdata:  data.string.ext22g,
				textclass: "text-5",
			},
			{
				textdata:   data.string.ext22eg,
				textclass: "text-5",
			},
			{
				textdata:  data.string.p3text27,
				textclass: "instruction",
			},
			{
				textdata:  data.string.ext23g,
				textclass: "optionclass1 correct",
			},
			{
				textdata:  data.string.ext24g,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext25g,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext26g,
				textclass: "bottom-text1 ",
			}
		],
		},

		// slide8
		{
			contentblockadditionalclass:'bg_yellow',
			uppertextblock :[{
				textdata:  data.string.ext19h,
				textclass: "top-text",
				datahighlightflag:true,
				datahighlightcustomclass:'orange'
			},],
			extratextblock:[
			{
				textdata:  data.string.ext20h,
				textclass: "text-5",
			},
			{
				textdata:  data.string.ext21h,
				textclass: "text-5 ",
			},
			{
				textdata:  data.string.ext22h,
				textclass: "text-5",
			},
			{
				textdata:   data.string.ext22eh,
				textclass: "text-5",
			},
			{
				textdata:  data.string.p3text27,
				textclass: "instruction",
			},
			{
				textdata:  data.string.ext23h,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext24h,
				textclass: "optionclass1 correct",
			},
			{
				textdata:  data.string.ext25h,
				textclass: "optionclass1 ",
			},
			{
				textdata:  data.string.ext26h,
				textclass: "bottom-text1 ",
			}
		],
		},

				// slide9
				{
					contentblockadditionalclass:'bg_yellow',
					uppertextblock :[{
						textdata:  data.string.ext19i,
						textclass: "top-text",
						datahighlightflag:true,
						datahighlightcustomclass:'orange'
					},],
					extratextblock:[
					{
						textdata:  data.string.ext20i,
						textclass: "text-5",
					},
					{
						textdata:  data.string.ext21i,
						textclass: "text-5 ",
					},
					{
						textdata:  data.string.ext22i,
						textclass: "text-5",
					},
					{
						textdata:   data.string.ext22ei,
						textclass: "text-5",
					},
					{
						textdata:  data.string.p3text27,
						textclass: "instruction",
					},
					{
						textdata:  data.string.ext23i,
						textclass: "optionclass1 ",
					},
					{
						textdata:  data.string.ext24i,
						textclass: "optionclass1 ",
					},
					{
						textdata:  data.string.ext25i,
						textclass: "optionclass1 correct",
					},
					{
						textdata:  data.string.ext26i,
						textclass: "bottom-text1 ",
					}
				],
				},
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();
	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		rhino.numberOfQuestions();
		$nextBtn.hide(0);
		$prevBtn.hide(0);


		var random = Math.floor(Math.random() * (12 - 8 + 1)) + 8;
		switch (countNext) {
			case 0:
				$('.randomnumber1').html(random*1000*.1);
				$('.randomnumber2').html(random*1000*.01);
				break;
			case 1:
				$('.randomnumber1').html(random*1000*0.1);
				$('.randomnumber2').html(random*1000*.01);
				$('.randomnumber3').html(random*1000*1.5);
				break;
			case 2:
				$('.randomnumber1').html(random*1000*0.15);
				$('.randomnumber2').html(random*1000*.015);
				$('.randomnumber3').html(random*1000*1.5);
				break;
			case 3:
				$('.randomnumber1').html(random*1000*0.2);
				$('.randomnumber2').html(random*1000*.02);
				$('.randomnumber3').html(random*1000*2);
				break;
			case 4:
				$('.randomnumber1').html(random*1000*0.24);
				$('.randomnumber2').html(random*1000*.024);
				$('.randomnumber3').html(random*1000*2.4);
				break;
			case 5:
				$('.randomnumber1').html(random*1000*0.1*2);
				$('.randomnumber2').html(random*1000*.1);
				$('.randomnumber3').html(random*1000*1);
				break;

				case 6:
					$('.randomnumber1').html(random*1000*0.11*2);
					$('.randomnumber2').html(random*1000*.012);
					$('.randomnumber3').html(random*1000*.15);
					break;
				case 7:
					$('.randomnumber1').html(random*1000*.15*5);
					$('.randomnumber2').html(random*1000*.15);
					$('.randomnumber3').html(random*1000*1.5);
					break;
				case 8:
					$('.randomnumber1').html(random*1000*.06*7);
					$('.randomnumber2').html(random*1000*.06);
					$('.randomnumber3').html(random*1000*1);
					break;
					case 9:
						$('.randomnumber1').html(random*1000*.19*9);
						$('.randomnumber2').html(random*1000*.19);
						$('.randomnumber3').html(random*1000*1);
						break;

		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$('.randomnumber').html(random*1000);
			if($('.randomnumber').hasClass('needsmultiplication')){
				var thenumber =$('.randomnumber').text();
				$('.randomnumber').html(random*1000*thenumber);
			}

		$('.bottom-text1').hide(0).css({"width": "100%",
																	"left": "0",
																	"padding-right": "10%"});
		$('.text-5').css({"opacity":"0","top":"24%"});
		$('.optionclass1').click(function(){
			if($(this).hasClass('correct'))
			{
				if(!wrong_clicked){
					rhino.update(true);
				}
				$('.text-5').animate({"opacity":"1"},400);
				$('.bottom-text1').fadeIn(200);
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(140%,-200%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
				$('.optionclass1').css({"pointer-events":"none"});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page){
							$nextBtn.show(0);
					}
			}
			else{
				// $('.text-5').animate({"opacity":"1"},400);
				// $('.text-5').fadeIn(200);
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((position.top + height)*100)/$board.height()+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:4%;transform:translate(140%,-200%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
				$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
				play_correct_incorrect_sound(0);
				if(!wrong_clicked){
					rhino.update(false);
				}
				wrong_clicked = true;
			}
		});
		// $(".options").click(function(){
		// 	if($(this).hasClass("corans")){
		// 		if(!wrong_clicked){
		// 			rhino.update(true);
		// 		}
		// 		$(".options").css('pointer-events', 'none');
		// 		$('.blank-space').html($(this).html().substring(4));
		// 		play_correct_incorrect_sound(1);
		// 		$(this).css({
		// 			'color': 'rgb(56,142,55)',
		// 		});
		// 		if(countNext != $total_page)
		// 			$nextBtn.show(0);
		// 	}
		// 	else{
		// 		if(!wrong_clicked){
		// 			rhino.update(false);
		// 		}
		// 		$(this).css({
		// 			'color': 'rgb(182,77,4)',
		// 			'text-decoration': 'line-through',
		// 			'pointer-events': 'none',
		// 		});
		// 		wrong_clicked = true;
		// 		play_correct_incorrect_sound(0);
		// 	}
		// });
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

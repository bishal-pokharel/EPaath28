var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[


	// slide0
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext1,
			textclass: "top_text",
		},{
			textdata:  data.string.ext3,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  data.string.p6text1,
			textclass: "first_constant",
		},{
			textdata:  '12',
			textclass: "second_constant",
			splitintofractionsflag:true
		},{
			textdata:  data.string.ext2,
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text15,
			textclass: "clear_button",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "right_wrong_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext1,
			textclass: "top_text",
		},{
			textdata:  data.string.ext3,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "left_overlap",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  data.string.p6text1,
			textclass: "first_constant",
		},{
			textdata:  '12',
			textclass: "second_constant",
			splitintofractionsflag:true
		},{
			textdata:  data.string.ext2,
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "right_wrong_text",
		},{
			textclass: "input_text",
			textdata:data.string.ext5
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext4,
			textclass: "top_text",
		},{
			textdata:  data.string.ext6,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  data.string.p6text1,
			textclass: "first_constant",
		},{
			textclass: "second_constant",
		},{
			textclass: "third_constant",
			textdata: '4',
		},{
			textdata:  'y',
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text15,
			textclass: "clear_button",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "right_wrong_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},


	// slide3
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext4,
			textclass: "top_text",
		},{
			textdata:  data.string.ext6,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "left_overlap",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "third_constant",
			textdata: '4',
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  data.string.p6text1,
			textclass: "first_constant",
		},{
			textdata:  '12',
			textclass: "second_constant",
			splitintofractionsflag:true
		},{
			textdata:  data.string.ext2,
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "input_text",
			textdata:data.string.ext5
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},

	// slide4
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext8,
			textclass: "top_text",
		},{
			textdata:  data.string.ext9,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "third_constant",
			textdata: '4',
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  '5',
			textclass: "first_constant",
		},{
			textdata:  data.string.p4text15,
			textclass: "clear_button",
		},{
			textclass: "third_constant",
		},{
			textdata:  '10',
			textclass: "second_constant",
			splitintofractionsflag:true
		},{
			textdata:  'b',
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},


	// slide5
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.ext8,
			textclass: "top_text",
		},{
			textdata:  data.string.ext9,
			textclass: "below_top_text",
		},{
			textclass: "left_overlap",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "third_constant",
			textdata: '4',
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p6text7,
			textclass: "left_text",
		},{
			textdata:  '5',
			textclass: "first_constant",
		},{
			textclass: "third_constant",
		},{
			textdata:  '10',
			textclass: "second_constant",
			splitintofractionsflag:true
		},{
			textdata:  'b',
			textclass: "variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "input_text",
			textdata:data.string.ext5
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	var equation;
	var random_array = [0,1,2,3,4,5];
	random_array.shufflearray();
	var randomized_number = random_array[0];

	var numbertemplate = new NumberTemplate();
	numbertemplate.init($total_page);

	var equation2;
	var random_array2 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19];
	random_array2.shufflearray();
	var randomized_number2 = random_array2[9];
	console.log('2=' +randomized_number2);
	var equation3;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "coverpage", src: imgpath+"a_16.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		numbertemplate.numberOfQuestions($total_page);

		put_image(content, countNext);
		splitintofractions($board);
		put_speechbox_image(content, countNext);
		$(".input1").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});


function css_generator(){
	$('.top_mala').css({
		'height':(($('.top_text').height()/$('.coverboardfull').height() )* 100)+5.2+"%"
	});

	$('.below_top_text').css({
		'top':(($('.top_text').height()/$('.coverboardfull').height() )* 100)+4+"%"
	});
	$('.left_text,.right_mid_text').css({
		'top':(($('.top_text').height()/$('.coverboardfull').height() )* 100)+(($('.below_top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%"
	});
}




		$(".submit_button").append("<img class='corincor cor' src='"+imgpath+"correct.png'>");
		$(".submit_button").append("<img class='corincor wrong' src='"+imgpath+"wrong.png'>");
		// var array_number=
		switch (countNext) {

			case 0:
				// $(".p15Txt3").append("<img class='s15cor' src='"+imgpath +"correct.png'/>");
			css_generator();
			$('.top_text').css('font-size','3vmin');
			var wrongcount=0;
			var first_array_inputs=[2,3,5,2,3,5];
			var stiring_array1=[data.string.p6text12,data.string.p6text12,data.string.p6text12,data.string.p6text13,data.string.p6text13,data.string.p6text13];
			var stiring_array2=[data.string.p6text14,data.string.p6text15,data.string.p6text16,data.string.p6text14,data.string.p6text15,data.string.p6text16];
			var first_poss_add_equation =data.string.spacecharacter_one+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
			var second_poss_add_equation =data.string.spacecharacter_one+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
			var third_poss_add_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+'X'+data.string.spacecharacter ;
			var fourth_poss_add_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter ;
			var first_poss_sub_equation =data.string.spacecharacter_one+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'-'+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
			var second_poss_sub_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'-'+data.string.spacecharacter+'X'+data.string.spacecharacter;
			$('.first_constant').html(first_array_inputs[randomized_number]);
			$('.second_constant').html('12');
			$('.firstword_change').html(stiring_array1[randomized_number]);
			$('.secondword_change').html(stiring_array2[randomized_number]);
			$('.right_wrong_text').hide(0);
			$('.submit_button').css({'pointer-events':'none'});
			var click_count = 0;
			$('.first_constant,.second_constant,.variable,.add,.multiplication,.division,.minus,.equals').click(function(){
				click_count++;
				if(click_count==3){
					$('.submit_button').css({'pointer-events':'auto'});
				}
				if(click_count==6){
					$('.first_constant,.second_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'none'});
				}
				var clicked_character = $(this).text();
				$('.written_equation').append(clicked_character + data.string.spacecharacter);
			});
			$('.clear_button').click(function(){
				// $(".corincor").hide(0);
				click_count = 0;
				$('.first_constant,.second_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'auto'});
				$('.submit_button').css({'pointer-events':'none'});
				$('.right_wrong_text').hide(0);
				$('.submit_button').css({'background':'#93e7ff','border-color':'#4a93b2'});
				$('.written_equation').html(data.string.p4text19);
			});
			$('.submit_button').click(function(){
				var the_equation = $('.written_equation').text();
				console.log(the_equation);
				if(randomized_number>=3){
					if((the_equation == first_poss_sub_equation) || (the_equation == second_poss_sub_equation) )
					{
						console.log(wrongcount);
						equation=the_equation;
						$('.two,.fifteen,.y_variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
						$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
						$('.submit_button').children(".wrong").hide();
						$('.submit_button').children(".cor").show();
						play_correct_incorrect_sound(1);
						if(wrongcount==0){
							numbertemplate.update(true);
						}
						nav_button_controls(1000);
					}
					else{
						numbertemplate.update(false);
						wrongcount++;
						$('.submit_button').children(".wrong").show();
						play_correct_incorrect_sound(0);
						$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
						$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
					}
				}
				if(randomized_number<3){
					if((the_equation == first_poss_add_equation) || (the_equation == second_poss_add_equation) || (the_equation == third_poss_add_equation) || (the_equation == fourth_poss_add_equation))
					{
						equation=the_equation;
						$('.two,.fifteen,.y_variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
						$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
						play_correct_incorrect_sound(1);
						$('.submit_button').children(".wrong").hide();
						$('.submit_button').children(".cor").show();
						nav_button_controls(1000);
					}
					else{
						play_correct_incorrect_sound(0);
						$('.submit_button').children(".wrong").show();
						$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
						$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
					}
				}

			});
			break;

			case 1:
				css_generator();
				$(".input_text").append("<p class='eqtxt'> x = </p>");
				$('.top_text').css('font-size','3.5vmin');
				var wrongcount=0;
				var answer_array_first=[4,3,2,12,6,3];
				var first_array_inputs=[2,3,5,2,3,5];
				var stiring_array1=[data.string.p6text12,data.string.p6text12,data.string.p6text12,data.string.p6text13,data.string.p6text13,data.string.p6text13];
				var stiring_array2=[data.string.p6text14,data.string.p6text15,data.string.p6text16,data.string.p6text14,data.string.p6text15,data.string.p6text16];
				var first_poss_add_equation =data.string.spacecharacter_one+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
				var second_poss_add_equation =data.string.spacecharacter_one+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
				var third_poss_add_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+'X'+data.string.spacecharacter ;
				var fourth_poss_add_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+'X'+data.string.spacecharacter+'+'+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter ;
				var first_poss_sub_equation =data.string.spacecharacter_one+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'-'+data.string.spacecharacter+'X'+data.string.spacecharacter+'='+data.string.spacecharacter+'12'+data.string.spacecharacter ;
				var second_poss_sub_equation =data.string.spacecharacter_one+'12'+data.string.spacecharacter+'='+data.string.spacecharacter+first_array_inputs[randomized_number]+data.string.spacecharacter+'X'+data.string.spacecharacter+'-'+data.string.spacecharacter+'X'+data.string.spacecharacter;
				$('.first_constant').html(first_array_inputs[randomized_number]);
				$('.second_constant').html('12');
				$('.firstword_change').html(stiring_array1[randomized_number]);
				$('.secondword_change').html(stiring_array2[randomized_number]);
				$('.right_wrong_text').hide(0);
				$('.written_equation').html(equation);
				$('.input1').addClass('zoom_in_out');
				$('.submit_button').css('left','70%');
				$('.input1').click(function(){
					$('.input1').removeClass('zoom_in_out');
				});
				$('.submit_button').click(function(){
					if($('.input1').val()==answer_array_first[randomized_number])
					{
						if(wrongcount==0){
							numbertemplate.update(true);
						}
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c','pointer-events':'none'});
						$('.input1').css({'pointer-events':'none'});
						play_correct_incorrect_sound(1);
						$('.submit_button').children(".wrong").hide();
						$('.submit_button').children(".cor").show();
						nav_button_controls(1000);
					}
					else{
						wrongcount++;
							numbertemplate.update(false);
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
					}

				});
				break;

				case 2:
				css_generator();
				$(".input_text").append("<p class='eqtxt'> Y = </p>");
				$('.top_text').css('font-size','3.5vmin');
				var main_collection_array=[
																	 [data.string.p6text12,data.string.p6text14,4,2],
																	 [data.string.p6text12,data.string.p6text14,8,2],
																	 [data.string.p6text12,data.string.p6text14,12,2],
																	 [data.string.p6text12,data.string.p6text14,16,2],
																	 [data.string.p6text12,data.string.p6text14,20,2],
																	 [data.string.p6text12,data.string.p6text17,4,4],
																	 [data.string.p6text12,data.string.p6text17,8,4],
																	 [data.string.p6text12,data.string.p6text17,12,4],
																	 [data.string.p6text12,data.string.p6text17,16,4],
																	 [data.string.p6text12,data.string.p6text17,20,4],
																	 [data.string.p6text13,data.string.p6text14,4,2],
 																	 [data.string.p6text13,data.string.p6text14,8,2],
 																	 [data.string.p6text13,data.string.p6text14,12,2],
 																	 [data.string.p6text13,data.string.p6text14,16,2],
 																	 [data.string.p6text13,data.string.p6text14,20,2],
																	 [data.string.p6text13,data.string.p6text17,4,4],
																	 [data.string.p6text13,data.string.p6text17,8,4],
																	 [data.string.p6text13,data.string.p6text17,12,4],
																	 [data.string.p6text13,data.string.p6text17,16,4],
																	 [data.string.p6text13,data.string.p6text17,20,4]
																 		];

				var second_poss_add_equation2 =data.string.spacecharacter_one+4+data.string.spacecharacter+'+'+data.string.spacecharacter+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'='+data.string.spacecharacter+main_collection_array[randomized_number2][2]+data.string.spacecharacter ;
				var first_poss_add_equation2 =data.string.spacecharacter_one+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'+'+data.string.spacecharacter+4+data.string.spacecharacter+'='+data.string.spacecharacter+main_collection_array[randomized_number2][2]+data.string.spacecharacter ;
				var third_poss_add_equation2 =data.string.spacecharacter_one+main_collection_array[randomized_number2][2]+data.string.spacecharacter+'='+data.string.spacecharacter+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'+'+data.string.spacecharacter+4+data.string.spacecharacter;
				var fourth_poss_add_equation2 =data.string.spacecharacter_one+main_collection_array[randomized_number2][2]+data.string.spacecharacter+'='+data.string.spacecharacter+4+data.string.spacecharacter+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'+'+data.string.spacecharacter;
				var first_pos_sub_equation2 =data.string.spacecharacter_one+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'-'+data.string.spacecharacter+4+data.string.spacecharacter+'='+data.string.spacecharacter+main_collection_array[randomized_number2][2]+data.string.spacecharacter ;
				var second_pos_sub_equation2 =data.string.spacecharacter_one+main_collection_array[randomized_number2][2]+data.string.spacecharacter+'='+data.string.spacecharacter+main_collection_array[randomized_number2][3]+data.string.spacecharacter+'y'+data.string.spacecharacter+'-'+data.string.spacecharacter+4+data.string.spacecharacter ;
				console.log(second_pos_sub_equation2 + '&&&' + first_pos_sub_equation2);
				$('.first_constant').html(main_collection_array[randomized_number2][2]);
				$('.second_constant').html(main_collection_array[randomized_number2][3]);
				$('.firstword_change').html(main_collection_array[randomized_number2][0]);
				$('.secondword_change').html(main_collection_array[randomized_number2][1]);
				$('.thirdword_change').html(main_collection_array[randomized_number2][2]);

				if(main_collection_array[randomized_number2][2]==4){
					$('.first_constant').hide(0);
				}
				if(main_collection_array[randomized_number2][3]==4){
					$('.second_constant').hide(0);
				}
				$('.right_wrong_text').hide(0);
				$('.submit_button').css({'pointer-events':'none'});
				var click_count = 0;
				$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').click(function(){
					click_count++;
					if(click_count==3){
						$('.submit_button').css({'pointer-events':'auto'});
					}
					if(click_count==7){
						$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'none'});
					}
					var clicked_character = $(this).text();
					$('.written_equation').append(clicked_character + data.string.spacecharacter);
				});
				$('.clear_button').click(function(){
					// $(".corincor").hide(0);
					click_count = 0;
					$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'auto'});
					$('.submit_button').css({'pointer-events':'none'});
					$('.right_wrong_text').hide(0);
					$('.submit_button').css({'background':'#93e7ff','border-color':'#4a93b2'});
					$('.written_equation').html(data.string.p4text19);
				});
				var wrongcount = 0;
				$('.submit_button').click(function(){
					var the_equation2 = $('.written_equation').text();
					console.log(the_equation2);
					if(randomized_number2>9){
						if((the_equation2 == first_pos_sub_equation2) || (the_equation2 == second_pos_sub_equation2) )
						{
							if(wrongcount==0){
								numbertemplate.update(true);
							}
							equation2=the_equation2;
							$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
							$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
							$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
							play_correct_incorrect_sound(1);
							$('.submit_button').children(".wrong").hide();
							$('.submit_button').children(".cor").show();
							nav_button_controls(1000);
						}
						else{
							numbertemplate.update(false);
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
							$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
						}
					}
					if(randomized_number2<10){
						if((the_equation2 == first_poss_add_equation2) || (the_equation2 == second_poss_add_equation2) || (the_equation2 == third_poss_add_equation2) || (the_equation2 == fourth_poss_add_equation2))
						{
							equation2=the_equation2;
							$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
							$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
							$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
							play_correct_incorrect_sound(1);
							$('.submit_button').children(".wrong").hide();
							$('.submit_button').children(".cor").show();
							nav_button_controls(1000);
						}
						else{
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
							$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
						}
					}

				});
				break;

				case 3:

				css_generator();
				$(".input_text").append("<p class='eqtxt'> Y = </p>");
				$('.top_text').css('font-size','3.5vmin');
				$('.written_equation').html(equation2);

				$('.input1').addClass('zoom_in_out');

				$('.submit_button').css('left','70%');

				var answer_array_first2=[0,2,4,6,7,0,1,2,3,4,4,6,8,10,12,2,3,4,5,6];
				var main_collection_array=[
																	 [data.string.p6text12,data.string.p6text14,4,2],
																	 [data.string.p6text12,data.string.p6text14,8,2],
																	 [data.string.p6text12,data.string.p6text14,12,2],
																	 [data.string.p6text12,data.string.p6text14,16,2],
																	 [data.string.p6text12,data.string.p6text14,20,2],
																	 [data.string.p6text12,data.string.p6text17,4,4],
																	 [data.string.p6text12,data.string.p6text17,8,4],
																	 [data.string.p6text12,data.string.p6text17,12,4],
																	 [data.string.p6text12,data.string.p6text17,16,4],
																	 [data.string.p6text12,data.string.p6text17,20,4],
																	 [data.string.p6text13,data.string.p6text14,4,2],
 																	 [data.string.p6text13,data.string.p6text14,8,2],
 																	 [data.string.p6text13,data.string.p6text14,12,2],
 																	 [data.string.p6text13,data.string.p6text14,16,2],
 																	 [data.string.p6text13,data.string.p6text14,20,2],
																	 [data.string.p6text13,data.string.p6text17,4,4],
																	 [data.string.p6text13,data.string.p6text17,8,4],
																	 [data.string.p6text13,data.string.p6text17,12,4],
																	 [data.string.p6text13,data.string.p6text17,16,4],
																	 [data.string.p6text13,data.string.p6text17,20,4]
																 		];

				var answer2_collection = [];
				$('.first_constant').html(main_collection_array[randomized_number2][2]);
				$('.second_constant').html(main_collection_array[randomized_number2][3]);
				$('.firstword_change').html(main_collection_array[randomized_number2][0]);
				$('.secondword_change').html(main_collection_array[randomized_number2][1]);
				$('.thirdword_change').html(main_collection_array[randomized_number2][2]);
				if(main_collection_array[randomized_number2][2]==4){
					$('.first_constant').hide(0);
				}
				if(main_collection_array[randomized_number2][3]==4){
					$('.second_constant').hide(0);
				}
				$('.input1').click(function(){
					$('.input1').removeClass('zoom_in_out');
				});
				var wrongcount=0;
				$('.submit_button').click(function(){
					if($('.input1').val()==answer_array_first2[randomized_number2])
					{
						if(wrongcount==0){
							numbertemplate.update(true);
						}
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c','pointer-events':'none'});
						$('.input1').css({'pointer-events':'none'});
						play_correct_incorrect_sound(1);
						$('.submit_button').children(".wrong").hide();
						$('.submit_button').children(".cor").show();
						nav_button_controls(1000);
					}
					else{
						numbertemplate.update(false);
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
					}
				});
				break;

				case 4:


				css_generator();
				console.log(randomized_number);
				var array_collection_last=[['1/3',data.string.p6text18,data.string.p6text21],['1/4',data.string.p6text19,data.string.p6text21],['1/2',data.string.p6text20
				,data.string.p6text21],['1/3',data.string.p6text18,data.string.p6text22],['1/4',data.string.p6text19,data.string.p6text22],['1/2',data.string.p6text20
				,data.string.p6text22]];
				$('.third_constant').html(array_collection_last[randomized_number][0]);
				$('.firstword_change').html(array_collection_last[randomized_number][1]);
				$('.secondword_change').html(array_collection_last[randomized_number][2]);

				var first_poss_add_equation3=data.string.spacecharacter_one+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter+'='+data.string.spacecharacter+'10'+data.string.spacecharacter+'+'+data.string.spacecharacter+'5'+data.string.spacecharacter;
				var second_poss_add_equation3=data.string.spacecharacter_one+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter+'='+data.string.spacecharacter+'5'+data.string.spacecharacter+'+'+data.string.spacecharacter+'10'+data.string.spacecharacter;
				var third_poss_add_equation3=data.string.spacecharacter_one+'5'+data.string.spacecharacter+'+'+data.string.spacecharacter+'10'+data.string.spacecharacter+'='+data.string.spacecharacter+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter;
				var fourth_poss_add_equation3=data.string.spacecharacter_one+'10'+data.string.spacecharacter+'+'+data.string.spacecharacter+'5'+data.string.spacecharacter+'='+data.string.spacecharacter+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter;

				var first_poss_sub_equation3=data.string.spacecharacter_one+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter+'='+data.string.spacecharacter+'10'+data.string.spacecharacter+'-'+data.string.spacecharacter+'5'+data.string.spacecharacter;
				var second_poss_sub_equation3=data.string.spacecharacter_one+'10'+data.string.spacecharacter+'-'+data.string.spacecharacter+'5'+data.string.spacecharacter+'='+data.string.spacecharacter+array_collection_last[randomized_number][0]+data.string.spacecharacter+'b'+data.string.spacecharacter;

				$('.right_wrong_text').hide(0);
				$('.submit_button').css({'pointer-events':'none'});
				var click_count = 0;
				$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').click(function(){
					click_count++;
					if(click_count==3){
						$('.submit_button').css({'pointer-events':'auto'});
					}
					if(click_count==6){
						$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'none'});
					}
					var clicked_character = $(this).text();
					$('.written_equation').append(clicked_character + data.string.spacecharacter);
				});
				$('.clear_button').click(function(){
					// $(".corincor").hide(0);
					click_count = 0;
					$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'auto'});
					$('.submit_button').css({'pointer-events':'none'});
					$('.right_wrong_text').hide(0);
					$('.submit_button').css({'background':'#93e7ff','border-color':'#4a93b2'});
					$('.written_equation').html(data.string.p4text19);
				});
				var wrongcount=0;
				console.log('case 4 +'+first_poss_sub_equation3);
				$('.submit_button').click(function(){

					var the_equation3 = $('.written_equation').text();
					console.log(the_equation3);
					if(randomized_number>2){
						if((the_equation3 == first_poss_sub_equation3) || (the_equation3 == second_poss_sub_equation3) )
						{
							if(wrongcount==0){
								numbertemplate.update(true);
							}
							equation3=the_equation3;
							$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
							$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
							$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
							play_correct_incorrect_sound(1);
							$('.submit_button').children(".wrong").hide();
							$('.submit_button').children(".cor").show();
							nav_button_controls(1000);
						}
						else{
							numbertemplate.update(false);
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
							$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
						}
					}
					if(randomized_number<3){
						if((the_equation3 == first_poss_add_equation3) || (the_equation3 == second_poss_add_equation3) || (the_equation3 == third_poss_add_equation3) || (the_equation3 == fourth_poss_add_equation3))
						{
							equation3=the_equation3;
							$('.first_constant,.second_constant,.third_constant,.variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
							$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
							$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
							play_correct_incorrect_sound(1);
							$('.submit_button').children(".wrong").hide();
							$('.submit_button').children(".cor").show();
							nav_button_controls(1000);
						}
						else{
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
							$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
						}
					}

				});
				break;


				case 5:
				css_generator();
				$(".input_text").append("<p class='eqtxt'> b = </p>");
				var answer_3= [45,60,30,15,20,10];
				var array_collection_last=[['1/3',data.string.p6text18,data.string.p6text21],['1/4',data.string.p6text19,data.string.p6text21],['1/2',data.string.p6text20
				,data.string.p6text21],['1/3',data.string.p6text18,data.string.p6text22],['1/4',data.string.p6text19,data.string.p6text22],['1/2',data.string.p6text20
				,data.string.p6text22]];
				$('.third_constant').html(array_collection_last[randomized_number][0]);
				$('.firstword_change').html(array_collection_last[randomized_number][1]);
				$('.secondword_change').html(array_collection_last[randomized_number][2]);

				$('.written_equation').html(equation3);
				$('.input1').addClass('zoom_in_out');
				$('.submit_button').css('left','70%');

				$('.input1').click(function(){
					$('.input1').removeClass('zoom_in_out');
				});
				var wrongcount=0;
				$('.submit_button').click(function(){
					if($('.input1').val()==answer_3[randomized_number])
					{
						if(wrongcount==0){
							numbertemplate.update(true);
						}
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c','pointer-events':'none'});
						$('.input1').css({'pointer-events':'none'});
						play_correct_incorrect_sound(1);
						$('.submit_button').children(".wrong").hide();
						$('.submit_button').children(".cor").show();
						nav_button_controls(1000);
					}
					else{
							numbertemplate.update(false);
							play_correct_incorrect_sound(0);
							$('.submit_button').children(".wrong").show();
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
					}
				});
				break;
			default:
				css_generator();
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				$nextBtn.show(0);
				// ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
			if($splitintofractions.length > 0){
				$.each($splitintofractions, function(index, value){
					$this = $(this);
					var tobesplitfraction = $this.html();
					if($this.hasClass('fraction')){
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
					}else{
						tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
						tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
					}


			tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
					$this.html(tobesplitfraction);
				});
			}
	}
	/*===== split into fractions end =====*/
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
			countNext++;
			numbertemplate.gotoNext();
			if(countNext < 10){
				templateCaller();
			} else {
				$(".scoreboard").hide(0);
				$nextBtn.hide(0);
			}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

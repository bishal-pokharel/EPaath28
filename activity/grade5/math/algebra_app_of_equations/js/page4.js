var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.p4text1,
			textclass: "diytext",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'coverpage',
				imgclass:'bg_full'
			}]
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.p4text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p4text3,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p4text4,
			textclass: "left_text",
		},{
			textdata:  data.string.p4text5,
			textclass: "two",
		},{
			textdata:  data.string.p4text6,
			textclass: "fifteen",
		},{
			textdata:  data.string.p4text7,
			textclass: "y_variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text15,
			textclass: "clear_button",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "right_wrong_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		extratextblock:[{
			textdata:  data.string.p4text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p4text3,
			textclass: "below_top_text",
		},{
			textclass: "left",
		},{
			textclass: "right",
		},{
			textclass: "left_circle1",
		},{
			textclass: "left_circle2",
		},{
			textdata:  data.string.p4text4,
			textclass: "left_text",
		},{
			textdata:  data.string.p4text5,
			textclass: "two",
		},{
			textdata:  data.string.p4text6,
			textclass: "fifteen",
		},{
			textdata:  data.string.p4text7,
			textclass: "y_variable",
		},{
			textdata:  data.string.p4text8,
			textclass: "add",
		},{
			textdata:  data.string.p4text10,
			textclass: "multiplication",
		},{
			textdata:  data.string.p4text12,
			textclass: "minus",
		},{
			textdata:  data.string.p4text11,
			textclass: "division",
		},{
			textdata:  data.string.p4text9,
			textclass: "equals",
		},{
			textdata:  data.string.p4text13,
			textclass: "right_mid_text",
		},{
			textdata:  data.string.p4text16,
			textclass: "submit_button",
		},{
			textclass: "left_overlap",
		},{
			textclass: "written_equation",
			textdata:  data.string.p4text19,
		},{
			textclass: "input_text",
			textdata:data.string.ext5
		},{
			textclass: "exyeqtxt",
			textdata:data.string.exyeqtxt
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'mala',
				imgclass:'top_mala'
			}]
		}]
	},




];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var equation;
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "coverpage", src: imgpath+"a_16.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},


			// sounds
			{id: "s4_p2_1", src: soundAsset+"s4_p2_1.ogg"},
			{id: "s4_p2_2", src: soundAsset+"s4_p2_2.ogg"},
			{id: "s4_p2_3", src: soundAsset+"s4_p2_3.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		var height = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%";
		$('.top_mala').css({
			'height':height
		});

		var top_value = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%";
		$('.below_top_text').css({
			'top':top_value
		});
		var top_value_left_text = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+(($('.below_top_text').height()/$('.coverboardfull').height() )* 100)+8.5+"%";
		$('.left_text,.right_mid_text').css({
			'top':top_value_left_text
		})
		switch (countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(1500);
				break;
			case 1:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s4_p2_1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s4_p2_2");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s4_p2_3");
						current_sound.play();
					});
				});
				// $('.written_equation').html('');
				$('.right_wrong_text').hide(0);
				$('.submit_button').css({'pointer-events':'none'});
				var click_count = 0;
				$('.two,.fifteen,.y_variable,.add,.multiplication,.division,.minus,.equals').click(function(){
					click_count++;
					if(click_count==3){
						$('.submit_button').css({'pointer-events':'auto'});
					}
					if(click_count==6){
						$('.first_constant,.second_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'none'});
					}
					var clicked_character = $(this).text();
					$('.written_equation').append(clicked_character + data.string.spacecharacter);
				});
				$('.clear_button').click(function(){
					$('.first_constant,.second_constant,.variable,.add,.multiplication,.division,.minus,.equals').css({'pointer-events':'auto'});
					click_count = 0;
					$('.submit_button').css({'pointer-events':'none'});
					$('.right_wrong_text').hide(0);
					$('.submit_button').css({'background':'#93e7ff','border-color':'#4a93b2'});
					$('.written_equation').html(data.string.p4text19);
				});
				$('.submit_button').click(function(){
					var the_equation = $('.written_equation').text();
					console.log(the_equation);
					if((the_equation == data.string.p4text17) || (the_equation == data.string.p4text18) || (the_equation == data.string.p4text20) || (the_equation == data.string.p4text21))
					{
						equation = the_equation;
						$('.two,.fifteen,.y_variable,.add,.multiplication,.division,.minus,.equals,.submit_button,.clear_button').css('pointer-events','none');
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c'});
						$('.right_wrong_text').html(data.string.p4text23).fadeIn(1000);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						nav_button_controls(1000);
					}
					else{
						createjs.Sound.stop();
						play_correct_incorrect_sound(0);
						$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
						$('.right_wrong_text').html(data.string.p4text22).fadeIn(1000);
					}
				});
				break;

				case 2:
				$('.written_equation').html(equation);
				$('.input1').addClass('zoom_in_out');
				$('.submit_button').css('left','70%');
				$('.input1').click(function(){
					$('.input1').removeClass('zoom_in_out');
				});
				$('.submit_button').click(function(){
					if($('.input1').val()==5)
					{
						$('.submit_button').css({'background':'#98c02e','border-color':'#deef3c','pointer-events':'none'});
						$('.input1').css({'pointer-events':'none'});
						play_correct_incorrect_sound(1);
						nav_button_controls(1000);
					}
					else{
							play_correct_incorrect_sound(0);
							$('.submit_button').css({'background':'#ff0000','border-color':'#980000'});
					}

				});
				break;
			default:
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nxtBtnFlag?nav_button_controls():'';
		});
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		extratextblock:[{
			textdata:  data.string.p2text1,
			textclass: "center_title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'diy_cover'
				}
			]
		}],
	},


	// slide1
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "mid_rightalign_text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mala",
					imgsrc : '',
					imgid : 'mala'
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box02',
			textdata:data.string.p2text2,
			textclass:'inside_text',
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}]
	},

	// slide2
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "top_text",
		},{
			textdata:  data.string.p2text4,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p2text5,
			textclass: "option1 correct",
		},{
			textdata:  data.string.p2text6,
			textclass: "option2",
		},{
			textdata:  data.string.p2text7,
			textclass: "option3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide3
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "top_text",
		},{
			textdata:  data.string.p2text9,
			textclass: "after_mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'questionmark_class'
		},{
			textdata:  data.string.p2text4_1,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p2text11,
			textclass: "option1-a ",
		},{
			textdata:  data.string.p2text12,
			textclass: "option2-a",
		},{
			textdata:  data.string.p2text13,
			textclass: "option3-a",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide4
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "top_text",
		},{
			textdata:  data.string.p2text14,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p2text16,
			textclass: "option1-a ",
		},{
			textdata:  data.string.p2text15,
			textclass: "option2-a correct",
		},{
			textdata:  data.string.p2text17,
			textclass: "option3-a",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide5
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "top_text",
		},{
			textdata:  data.string.p2text18,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p2text19,
			textclass: "bottom_text ",
		},{
			textdata:  data.string.p2text20,
			textclass: "check_button",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},



];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var selected_variable='';

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diy_cover", src: imgpath+"a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "s2_p2_1", src: soundAsset+"s2_p2_1.ogg"},
			{id: "s2_p2_2", src: soundAsset+"s2_p2_2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		$(".input_text2").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});

		var height = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%";
		$('.top_mala').css({
			'height':height
		});

		switch (countNext) {
					case 0:
						play_diy_audio();
						nav_button_controls(1500);
					break;
					case 1:
					$('.sp-1,.mid_rightalign_text').hide(0);
					$('.sp-1').fadeIn(500);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s2_p2_1");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.mid_rightalign_text').fadeIn(500);
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s2_p2_2");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(2000);
							});
					});
					break;

					case 2:
					sound_play_click("s2_p3", 0);
					$('.option1,.option2,.option3').click(function(){
						if($(this).hasClass('correct'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.option1,.option2,.option3').css({"pointer-events":"none"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					});
					break;

					case 3:
					sound_play_click("s2_p4", 0);
					$('.option1-a,.option2-a,.option3-a').click(function(){
						$('.option1-a,.option2-a,.option3-a').css({'pointer-events':'none'});
						selected_variable=$(this).text();
						$(this).css({'background':'#98c02e','border-color':'#98c02e'});
						$('.questionmark_class').html(selected_variable);
						console.log(selected_variable);
						nav_button_controls(200);
					});
					break;

					case 4:
					$('.selectedvariable').html(selected_variable);
					$('.option1-a,.option2-a,.option3-a').css({'width':'19%'});
					$('.option1-a').css({'left':'14%'});
					$('.option2-a').css({'left':'42%'});
					$('.mid_text_question').css({'top':'42%','text-align':'left'});
					$('.option1-a,.option2-a,.option3-a').click(function(){
						if($(this).hasClass('correct'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-10%,78%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.option1-a,.option2-a,.option3-a').css({"pointer-events":"none"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(-10%,78%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					});
					break;

					case 5:
					$('.mid_text_question').css({'top':'42%','text-align':'left'});
					$('.selectedvariable').html(selected_variable);
					$('.input_text2').addClass('on_off_anim');
					$('.input_text2').click(function(){
						$(this).removeClass('on_off_anim');
					});
					$('.check_button').click(function(){
						if($('.input_text2').val()==20){
							$('.input_text2').css({'background':'#98c02e','color':'white','pointer-events':'none'});
							$('.check_button').hide(0);
							var $this = $('.bottom_text');
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('.wrong_icon').hide(0);
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(12%,27%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							nav_button_controls(200);
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
						}
						else{
							createjs.Sound.stop();
							play_correct_incorrect_sound(0);
							$('.check_button').css({'background':'#ff0000','border-color':'#980000','color':'white'});
							var $this = $('.bottom_text');
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img class="wrong_icon" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(180%,-96%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
						}
					});
					break;

					default:
					nav_button_controls(200);
					break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, nxtBtnflag){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nxtBtnflag?nav_button_controls:'';
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

		function put_speechbox_image(content, count){
			if(content[count].hasOwnProperty('speechbox')){
					var speechbox = content[count].speechbox;
					for(var i=0; i<speechbox.length; i++){
							var image_src = preload.getResult(speechbox[i].imgid).src;
							//get list of classes
							var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
							// console.log(selector);
							$(selector).attr('src', image_src);
					}
			}
		}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

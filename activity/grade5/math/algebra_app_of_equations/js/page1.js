var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson_title",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg_full",
					imgsrc : '',
					imgid : 'cover'
				}
			]
		}]
	},

	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'cream_bg',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mala_left",
					imgsrc : '',
					imgid : 'mala02'
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box02',
			textdata:data.string.p1text1,
			textclass:'inside_text',
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}]
	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'cream_bg',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mala_left",
					imgsrc : '',
					imgid : 'mala02'
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-2',
			imgid:'text_box01',
			textdata:data.string.p1text2,
			textclass:'inside_text',
			datahighlightflag:true,
			datahighlightcustomclass:'boldplusfont'
		}]
	},


	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'cream_bg',
			extratextblock : [
				{
					textdata:data.string.p1text3,
					textclass:'top_text',
				},{
					textdata:data.string.p1text4,
					textclass:'question_below_top',
					datahighlightflag:true,
					datahighlightcustomclass:'firsttext_ques',
					datahighlightcustomclass2:'secondtext_ques',
				},{
					textdata:data.string.p1text6,
					textclass:'bottom_middle_text',
					datahighlightflag:true,
					datahighlightcustomclass:'firsttext',
					datahighlightcustomclass2:'secondtext',
				}
			],
			// imageblock:[{
			// 	imagestoshow : [
			// 		{
			// 			imgclass : "below_top_image",
			// 			imgsrc : '',
			// 			imgid : 'bg01'
			// 		}
			// 	]
			// }],
	},

	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'cream_bg',
			extratextblock : [
				{
					textdata:data.string.p1text7,
					textclass:'top_text',
				},{
					textdata:data.string.p1text8,
					textclass:'mid_text',
					datahighlightflag:true,
					datahighlightcustomclass:'firsttext',
					datahighlightcustomclass2:'secondtext',
				}
			],
			// imageblock:[{
			// 	imagestoshow : [
			// 		{
			// 			imgclass : "below_top_image opacity_low",
			// 			imgsrc : '',
			// 			imgid : 'bg01'
			// 		}
			// 	]
			// }],
	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'cream_bg',
			extratextblock : [
				{
					textdata:data.string.p1text9,
					textclass:'top_text',
				},{
					textdata:data.string.p1text11,
					textclass:'check_button',
				},{
					textdata:data.string.p1text10,
					textclass:'mid_text',
					datahighlightflag:true,
					datahighlightcustomclass:'firsttext',
					datahighlightcustomclass2:'secondtext'
				}
			],
			// imageblock:[{
			// 	imagestoshow : [
			// 		{
			// 			imgclass : "below_top_image opacity_low",
			// 			imgsrc : '',
			// 			imgid : 'bg01'
			// 		}
			// 	]
			// }],
	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'blue_bg',
			extratextblock : [
				{
					textdata:data.string.p1text13,
					textclass:'top_text',
				},{
					textdata:data.string.p1text14,
					textclass:'top_below_text',
				},{
					textdata:data.string.p1text15,
					textclass:'mid_text1',
				},{
					textdata:data.string.p1text16,
					textclass:'mid_text2',
				},{
					textdata:data.string.p1text17,
					textclass:'mid_text3',
				}
			],
	},

	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'blue_bg',
			extratextblock : [
				{
					textdata:data.string.p1text13,
					textclass:'top_text',
				},{
					textdata:data.string.p1text14,
					textclass:'top_below_text',
				},{
					textdata:data.string.p1text18,
					textclass:'mid_text1',
					datahighlightflag:true,
					datahighlightcustomclass:'greenbg',
					datahighlightcustomclass2:'brownbg',
				},{
					textdata:data.string.p1text19,
					textclass:'mid_text2',
					datahighlightflag:true,
					datahighlightcustomclass:'greenbg',
					datahighlightcustomclass2:'brownbg',
				},{
					textdata:data.string.p1text20,
					textclass:'mid_text_small3',
					datahighlightflag:true,
					datahighlightcustomclass:'greenbg',
					datahighlightcustomclass2:'brownbg',
				}
			],
	},


		// slide8
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'blue_bg',
				extratextblock : [
					{
						textdata:data.string.p1text31,
						textclass:'top_text',
					},{
						textdata:data.string.p1text14,
						textclass:'top_below_text',
					},{
						textdata:data.string.p1text22,
						textclass:'mid_text1',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					},{
						textdata:data.string.p1text23,
						textclass:'mid_text2',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					},{
						textdata:data.string.p1text24,
						textclass:'mid_text_small3',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					}
				],
		},

		// slide9
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'blue_bg',
				extratextblock : [
					{
						textdata:data.string.p1text32,
						textclass:'top_text',
					},{
						textdata:data.string.p1text14,
						textclass:'top_below_text',
					},{
						textdata:data.string.p1text26,
						textclass:'mid_text1',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					},{
						textdata:data.string.p1text27,
						textclass:'mid_text2',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					}
				],
		},


		// slide10
		{
			contentnocenteradjust: true,
			contentblockadditionalclass: 'blue_bg',
				extratextblock : [
					{
						textdata:data.string.p1text31,
						textclass:'top_text',
					},{
						textdata:data.string.p1text14,
						textclass:'top_below_text',
					},{
						textdata:data.string.p1text28,
						textclass:'mid_text1',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					},{
						textdata:data.string.p1text29,
						textclass:'mid_text2',
						datahighlightflag:true,
						datahighlightcustomclass:'greenbg',
						datahighlightcustomclass2:'brownbg',
					}
				],
		},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover", src: imgpath+"coverpage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg01", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg02", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala01", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala02", src: imgpath+"mala02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala03", src: imgpath+"mala03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box01", src: imgpath+"text_box01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4_1", src: soundAsset+"s1_p4_1.ogg"},
			{id: "s1_p4_2", src: soundAsset+"s1_p4_2.ogg"},
			{id: "s1_p4_3", src: soundAsset+"s1_p4_3.ogg"},
			{id: "s1_p5_1", src: soundAsset+"s1_p5_1.ogg"},
			{id: "s1_p6_1", src: soundAsset+"s1_p6_1.ogg"},
			{id: "s1_p7_1", src: soundAsset+"s1_p7_1.ogg"},
			{id: "s1_p7_2", src: soundAsset+"s1_p7_2.ogg"},
			{id: "s1_p7_3", src: soundAsset+"s1_p7_3.ogg"},
			{id: "s1_p7_4", src: soundAsset+"s1_p7_4.ogg"},
			{id: "s1_p8_1", src: soundAsset+"s1_p8_1.ogg"},
			{id: "s1_p8_2", src: soundAsset+"s1_p8_2.ogg"},
			{id: "s1_p8_3", src: soundAsset+"s1_p8_3.ogg"},
			{id: "s1_p9_1", src: soundAsset+"s1_p9_1.ogg"},
			{id: "s1_p9_2", src: soundAsset+"s1_p9_2.ogg"},
			{id: "s1_p9_3", src: soundAsset+"s1_p9_3.ogg"},
			{id: "s1_p9_4", src: soundAsset+"s1_p9_4.ogg"},
			{id: "s1_p10_1", src: soundAsset+"s1_p10_1.ogg"},
			{id: "s1_p10_2", src: soundAsset+"s1_p10_2.ogg"},
			{id: "s1_p11_1", src: soundAsset+"s1_p11_1.ogg"},
			{id: "s1_p11_2", src: soundAsset+"s1_p11_2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
		 //check if $highlightinside is provided
		 typeof $highlightinside !== "object" ?
		 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		 null ;

		 var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		 var stylerulename;
		 var replaceinstring;
		 var texthighlightstarttag;
		 var texthighlightstarttag2;
		 var texthighlightstarttag3;
		 var texthighlightendtag   = "</span>";
		 if($alltextpara.length > 0){
			 $.each($alltextpara, function(index, val) {
				 /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
					 $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename = $(this).attr("data-highlightcustomclass")) :
					 (stylerulename = "parsedstring") ;

					 $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
					 (stylerulename2 = "parsedstring2") ;

					 $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
					 (stylerulename3 = "parsedstring3") ;

				 texthighlightstarttag = "<span class='"+stylerulename+"'>";
				 texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
				 texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
				 replaceinstring       = $(this).html();
				 replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				 replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				 replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				 $(this).html(replaceinstring);
			 });
		 }
	 }
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);

		$(".input_box").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 49 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});
		var top_val = (($('.top_text').height()/$('.coverboardfull').height() )* 100) +2+"%";
		$('.top_below_text').css({
			'top':top_val
		});
		var topadded = 35+(($('.top_text').height()/$('.coverboardfull').height() )* 100) +2 +(($('.top_below_text').height()/$('.coverboardfull').height() )* 100) +2+"%"
		console.log(topadded);
		$('.mid_text1').css({
			'top':topadded
		});
		switch(countNext) {
			case 0:
			case 1:
			case 2:
				sound_player("s1_p"+(countNext+1),1);
			break;
			case 3:
			$('.show_laters,.firsttext,.secondtext').hide(0);
			// $('.show_laters,.firsttext,.secondtext').css('opacity','0');
			$('.bottom_middle_text, .question_below_top').hide(0);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p4_1");
				current_sound.play();
				current_sound.on('complete', function(){
					$(".question_below_top, .show_laters").fadeIn();
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p4_2");
						current_sound.play();
						current_sound.on('complete',function(){
							$(".bottom_middle_text, .firsttext, .secondtext").fadeIn();
							sound_player("s1_p4_3",1);
								// createjs.Sound.stop();
								// current_sound = createjs.Sound.play("s1_p4_3");
								// current_sound.play();
						});
				});
			// $('.question_below_top').hide(0).fadeIn(1000,
			// function(){
			// 	$('.show_laters').animate({'opacity':'1'},1000,
			// 	function(){
			// 		$('.bottom_middle_text').fadeIn(1000,
			// 			function(){
			// 				$('.firsttext_ques').css('background','#adfece');
			// 				$('.firsttext').css('background','#adfece').animate({'opacity':'1'},2000,
			// 					function(){
			// 						$('.firsttext_ques,.firsttext').css('background','none');
			// 						$('.secondtext,.secondtext_ques').css('background','#adfece').animate({'opacity':'1'},500);
			// 						nav_button_controls(1000);
			// 					});
			// 			});
			// 	});
			// });
			break;

			case 4:
			sound_player("s1_p5_1",1);
			$('.mid_text').hide(0);
			$('.firsttext,.secondtext').css('opacity','0');
			$('.mid_text').hide(0).fadeIn(1000,
			function(){
				$('.firsttext').animate({'opacity':'1'},1000,
					function(){
						$('.secondtext').animate({'opacity':'1'},1000);
						// nav_button_controls(1000);
					});
			});
			break;

			case 5:
			sound_player("s1_p6_1",0);
			$('.check_button').click(function(){
				if($('.input_box').val()==6){
					createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					$('.check_button,.input_box').css({'background':'#77c912','border-color':'#71b115','pointer-events':'none'});
					nav_button_controls(500);
				}
				else{
					createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$('.check_button').css({'background':'#ff0000','border-color':'#980000'});
				}
			});
			break;

			case 6:
				$('ul,.mid_text1,.mid_text3').css('opacity','0');
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p7_1");
				current_sound.play();
				current_sound.on('complete',function(){
					$('.mid_text1').animate({'opacity':'1'},1000);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p7_2");
					current_sound.play();
					current_sound.on('complete',function(){
							$('ul').animate({'opacity':'1'},1000);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p7_3");
							current_sound.play();
							current_sound.on('complete', function(){
								$('.mid_text3').animate({'opacity':'1'},1000);
								createjs.Sound.stop();
								current_sound = createjs.Sound.play("s1_p7_4");
								current_sound.play();
								current_sound.on('complete', function(){
									nav_button_controls(1000);
								});
							});
					});
				});
			break;

			case 7:
			$('.mid_text2,.mid_text1,.mid_text_small3').css('opacity','0');
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p7_1");
			current_sound.play();
			current_sound.on('complete',function(){
				$('.mid_text1').animate({'opacity':'1'},1000);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p8_1");
				current_sound.play();
				current_sound.on('complete',function(){
						$('.mid_text2').animate({'opacity':'1'},1000);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p8_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.mid_text_small3').animate({'opacity':'1'},1000);
							createjs.Sound.stop();
							current_sound = createjs.Sound.play("s1_p8_3");
							current_sound.play();
							current_sound.on('complete', function(){
								nav_button_controls(1000);
							});
						});
				});
			});
			// $('.mid_text1').animate({'opacity':'1'},1000,
			// function(){
			// 	$('.mid_text2').animate({'opacity':'1'},1000,
			// 		function(){
			// 			$('.mid_text_small3').animate({'opacity':'1'},1000);
			// 			nav_button_controls(1000);
			// 		});
			// });
			break;


			case 8:
			$('.mid_text1').css('font-size','2.8vmin');
			$('.mid_text_small3').css('font-size','3.1vmin');
			$('.mid_text2').css({'font-size':'3.1vmin',"top":"56%"});
			$('.mid_text2,.mid_text1,.mid_text_small3').css('opacity','0');
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p9_1");
			current_sound.play();
				current_sound.on('complete',function(){
					$('.mid_text1').animate({'opacity':'1'},1000);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p9_2");
				current_sound.play();
					current_sound.on('complete',function(){
						$('.mid_text2').animate({'opacity':'1'},1000);
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s1_p9_3");
					current_sound.play();
						current_sound.on('complete',function(){
							$('.mid_text_small3').animate({'opacity':'1'},1000);
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("s1_p9_4");
						current_sound.play();
						current_sound.on('complete',function(){
							nav_button_controls(300);
						});
					});
				});
			});
			break;


			case 9:
			$('.mid_text2,.mid_text1').css('opacity','0');
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p10_1");
			current_sound.play();
			current_sound.on('complete',function(){
				$('.mid_text1, .mid_text2').animate({'opacity':'1'},1000);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p10_2");
				current_sound.play();
				current_sound.on('complete',function(){
					nav_button_controls(1000);
				});
			});
			// $('.mid_text1').animate({'opacity':'1'},2000,
			// function(){
			// 	$('.mid_text2').animate({'opacity':'1'},2000,
			// 		function(){
			// 			nav_button_controls(1000);
			// 		});
			// });
			break;

			case 10:
			$('.mid_text1').css('font-size','3.1vmin');
			$('.mid_text2').css({'font-size':'3.1vmin',"top":"70%"});
			$('.mid_text2,.mid_text1').css('opacity','0');
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("s1_p11_1");
			current_sound.play();
			current_sound.on('complete',function(){
				$('.mid_text1, .mid_text2').animate({'opacity':'1'},1000);
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s1_p11_2");
				current_sound.play();
				current_sound.on('complete',function(){
					nav_button_controls(1000);
				});
			});

			// $('.mid_text1').animate({'opacity':'1'},2000,
			// function(){
			// 	$('.mid_text2').animate({'opacity':'1'},2000,
			// 		function(){
			// 			nav_button_controls(1000);
			// 		});
			// });
			break;
			default:
				$prevBtn.show(0);
				nav_button_controls(1000);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nextBtnFlag){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete',function(){
			nextBtnFlag?nav_button_controls(1000):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

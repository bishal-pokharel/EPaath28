var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'light_bg',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mala",
					imgsrc : '',
					imgid : 'mala'
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box02',
			textdata:data.string.p5text1,
			textclass:'inside_text',
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text3,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text5,
			textclass: "option1 ",
		},{
			textdata:  data.string.p5text4,
			textclass: "option2 correct",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide2
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p5text6,
			textclass: "after_mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'questionmark_class'
		},{
			textdata:  data.string.p5text8,
			textclass: "option1-a ",
		},{
			textdata:  data.string.p5text9,
			textclass: "option2-a",
		},{
			textdata:  data.string.p5text10,
			textclass: "option3-a",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide3
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p5text11,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide4
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text31,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		},{
			textdata:  data.string.p5text13,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide5
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text31,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		},{
			textdata:  data.string.p5text14,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		},{
			textdata:  data.string.p5text16,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide6
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text18,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text22,
			textclass: "option1 ",
		},{
			textdata:  data.string.p5text20,
			textclass: "option2 correct",
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text21,
			textclass: "option3",
			splitintofractionsflag:true
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},
	// slide7
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		},{
			textdata:  data.string.p5text24,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass3:'yellow',
			datahighlightcustomclass:'orange'
		},{
			textdata:  data.string.p5text16,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide8
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'orange',
			datahighlightcustomclass2:'green'
		},{
			textdata:  data.string.p5text26,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass2:'green',
			datahighlightcustomclass:'orange',
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text16,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide9
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p5text27,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text32,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text33,
			textclass: "question_bot",
		},{
			textdata:  data.string.p5text29,
			textclass: "check_button",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide10
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
		},{
			textdata:  data.string.p5text34,
			textclass: "mid_text_question fades_in",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			splitintofractionsflag:true
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},
	// slide11
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg',
			datahighlightcustomclass2:'orange'
		},{
			textdata:  data.string.p5text36,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg',
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text35,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
		},{
			textdata:  data.string.p5text37,
			textclass: "left_caption",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		},{
			textdata:  data.string.p5text38,
			textclass: "right_caption",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				},{
						imgclass : "bracket1",
						imgsrc : '',
						imgid : 'brackets'
					},
					{
							imgclass : "bracket2",
							imgsrc : '',
							imgid : 'brackets'
						}
			]
		}],
	},

	// slide12
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text39,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text41,
			textclass: "option1 ",
		},{
			textdata:  data.string.p5text40,
			textclass: "option2 correct",
		},{
			textdata:  data.string.p5text42,
			textclass: "option3",
		},{
			textdata:  data.string.p5text43,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide13
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text44,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			splitintofractionsflag:true
		},{
			textdata:  data.string.p5text45,
			textclass: "option1 correct",
		},{
			textdata:  data.string.p5text46,
			textclass: "option2 ",
		},{
			textdata:  data.string.p5text47,
			textclass: "option3",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide13Sec
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},
		{
			textdata:  data.string.p5text44_sec,
			textclass: "p15Txt2",
		},
		{
			txtindiv:true,
			txtdivclass:"s15eqn",
			textdata:  data.string.p5text44_third,
			textclass: "p15Txt3",
			datahighlightflag:true,
			datahighlightcustomclass:'bold',
			splitintofractionsflag:true
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	//slide14
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text48,
			textclass: "mid_text_question",
		}]

	},

	//slide15
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text50,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text49,
			textclass: "bottom_boxed-text",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	//slide16
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text52,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text53,
			textclass: "bottom_boxed-text2",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	//slide17
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p5text54,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p5text55,
			textclass: "bottom_boxed-text3",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	//slide18
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text56,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p5text57,
			textclass: "bottom_boxed-text3",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	//slide19
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p5text58,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p5text59,
			textclass: "lists",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var selected_variable='';
	var selectedVarSound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diy_cover", src: imgpath+"a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "brackets", src: imgpath+"brackets.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "s5_p1", src: soundAsset+"s5_p1.ogg"},
			{id: "s5_p2_1", src: soundAsset+"s5_p2_1.ogg"},
			{id: "s5_p2_2", src: soundAsset+"s5_p2_2.ogg"},
			{id: "s5_p3", src: soundAsset+"s5_p3.ogg"},
			{id: "s5_p4", src: soundAsset+"s5_p4.ogg"},
			{id: "s5_p5", src: soundAsset+"s5_p5.ogg"},
			{id: "s5_p6_1", src: soundAsset+"s5_p6_1.ogg"},
			{id: "s5_p6_2", src: soundAsset+"s5_p6_2.ogg"},
			{id: "s5_p7_1", src: soundAsset+"s5_p7_1.ogg"},
			{id: "s5_p7_2", src: soundAsset+"s5_p7_2.ogg"},
			{id: "s5_p8_1", src: soundAsset+"s5_p8_1.ogg"},
			{id: "s5_p8_2", src: soundAsset+"s5_p8_2.ogg"},
			{id: "s5_p8_3", src: soundAsset+"s5_p8_3.ogg"},
			{id: "s5_p8_4", src: soundAsset+"s5_p8_4.ogg"},
			{id: "s5_p8_5", src: soundAsset+"s5_p8_5.ogg"},
			{id: "s5_p9", src: soundAsset+"s5_p9.ogg"},
			{id: "s5_p10", src: soundAsset+"s5_p10.ogg"},
			{id: "s5_p11", src: soundAsset+"s5_p11.ogg"},
			{id: "s5_p13", src: soundAsset+"s5_p13.ogg"},
			{id: "s5_p14_1", src: soundAsset+"s5_p14_1.ogg"},
			{id: "s5_p14_2", src: soundAsset+"s5_p14_2.ogg"},
			{id: "s5_p15", src: soundAsset+"s5_p15.ogg"},
			{id: "s5_p16", src: soundAsset+"s5_p16.ogg"},
			{id: "s5_p16_1", src: soundAsset+"s5_p16_1.ogg"},
			{id: "s5_p17", src: soundAsset+"s5_p17.ogg"},
			{id: "s5_p18", src: soundAsset+"s5_p18.ogg"},
			{id: "s5_p19", src: soundAsset+"s5_p19.ogg"},
			{id: "s5_p20_1", src: soundAsset+"s5_p20_1.ogg"},
			{id: "s5_p20_2", src: soundAsset+"s5_p20_2.ogg"},

			{id: "m", src: soundAsset+"m.ogg"},
			{id: "p", src: soundAsset+"p.ogg"},
			{id: "y", src: soundAsset+"y.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside){
		 //check if $highlightinside is provided
		 typeof $highlightinside !== "object" ?
		 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		 null ;

		 var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		 var stylerulename;
		 var replaceinstring;
		 var texthighlightstarttag;
		 var texthighlightstarttag2;
		 var texthighlightstarttag3;
		 var texthighlightendtag   = "</span>";
		 if($alltextpara.length > 0){
			 $.each($alltextpara, function(index, val) {
				 /*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
					 $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename = $(this).attr("data-highlightcustomclass")) :
					 (stylerulename = "parsedstring") ;

					 $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
					 (stylerulename2 = "parsedstring2") ;

					 $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					 (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
					 (stylerulename3 = "parsedstring3") ;

				 texthighlightstarttag = "<span class='"+stylerulename+"'>";
				 texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
				 texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
				 replaceinstring       = $(this).html();
				 replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				 replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				 replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
				 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				 $(this).html(replaceinstring);
			 });
		 }
	 }
	/*=====  End of data highlight function  ======*/


	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		splitintofractions($board);
		$(".input_text2,.input_text1").keydown(function (e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
				 // Allow: Ctrl+A, Command+A
				(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
				 // Allow: home, end, left, right, down, up
				(e.keyCode >= 35 && e.keyCode <= 40)) {
						 // let it happen, don't do anything
						 return;
		}
		if (e.keyCode == 13) {
			$('.submit_button').click();
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
		}
		});

		var height = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%";
		$('.top_mala').css({
			'height':height
		});

		switch (countNext) {
				case 0:
					sound_player("s5_p1",1);
				break;
				case 1:
					$('.option1,.option2').css("pointer-events","none");
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p2_1");
						current_sound.play();
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s5_p2_2");
							current_sound.play();
							current_sound.on('complete', function(){
								$('.option1,.option2').css("pointer-events","auto");
							});
						});
						$('.mid_text_question').css({'top':'40%'});
						$('.option1,.option2').click(function(){
							if($(this).hasClass('correct'))
							{
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
								$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
								$('.option1,.option2').css({"pointer-events":"none"});
									createjs.Sound.stop();
								play_correct_incorrect_sound(1);
								nav_button_controls(200);
							}
							else{
								var $this = $(this);
								var position = $this.position();
								var width = $this.width();
								var height = $this.height();
								var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
								var centerY = ((position.top + height)*100)/$board.height()+'%';
								$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
								$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
									createjs.Sound.stop();
								play_correct_incorrect_sound(0);
							}
						});
					break;

				case 2:
					sound_player("s5_p"+(countNext+1),1);
					$('.option1-a,.option2-a,.option3-a').click(function(){
						$('.option1-a,.option2-a,.option3-a').css({'pointer-events':'none'});
						selected_variable=$(this).text();
						$('.questionmark_class').html(selected_variable);
						$(this).css({'background':'#98c02e','border-color':'#98c02e'});
						console.log(selected_variable);
						nav_button_controls(200);
						selectedVarSound = selected_variable;
							sound_player(selectedVarSound,1);
					});
				break;

				case 3:
					$('.selectedvariable').html(selected_variable);
					nav_button_controls(300);
						// createjs.Sound.stop();
						// var current_sound = createjs.Sound.play("s5_p4");
						// current_sound.play();
						// current_sound.on('complete', function(){
						// });
				break;
				case 4:
					sound_player("s5_p5",1);
					// createjs.Sound.stop();
					// var current_sound = createjs.Sound.play("s5_p5");
					// current_sound.play();
					// current_sound.on('complete', function(){
					// 	createjs.Sound.stop();
					// 	var current_sound = createjs.Sound.play(selectedVarSound);
					// 	current_sound.play();
					// });
				break;

				case 5:
					$('.tips').hide(0);
					$('.selectedvariable').html(selected_variable);
					$('.mid_text_question').css({'top':'60%','left':'36%','width':'65%'});
					$('.tips').delay(1000).show(500);
						$('.selectedvariable').html(selected_variable);
						nav_button_controls(1000);
							// createjs.Sound.stop();
							// var current_sound = createjs.Sound.play("s5_p6_1");
							// current_sound.play();
							// current_sound.on('complete', function(){
							// 	createjs.Sound.stop();
							// 	var current_sound = createjs.Sound.play(selectedVarSound);
							// 	current_sound.play();
							// 	current_sound.on('complete',function(){
							// 		createjs.Sound.stop();
							// 		var current_sound = createjs.Sound.play("s5_p6_2");
							// 		current_sound.play();
							// 		current_sound.on('complete', function(){
							// 			nav_button_controls(2000);
							// 		});
							// 	});
							// });
					break;

				case 6:
					$('.selectedvariable').html(selected_variable);
					$('.mid_text_question').css({'text-align':'center','width':'100%'});
					$('.option1').css({'top':'55%'});
					$('.option2').css({'top':'70%'});
					$('.option3').css({'top':'90%'});
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p7_1");
						current_sound.play();
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s5_p7_2");
							current_sound.play();
						});
					$('.option1,.option2,.option3').click(function(){
						if($(this).hasClass('correct'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.option1,.option2,.option3').css({"pointer-events":"none"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					});
					break;

				case 7:
					$('.selectedvariable').html(selected_variable);
					nav_button_controls(100);
					$('.tips').hide(0);
					$('.mid_text_question').css({'top':'60%','left':'36%','width':'65%'});
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p8_1");
						current_sound.play();
						current_sound.on('complete', function(){
							createjs.Sound.stop();
							var current_sound = createjs.Sound.play("s5_p8_2");
							current_sound.play();
							current_sound.on('complete',function(){
								createjs.Sound.stop();
								var current_sound = createjs.Sound.play("s5_p8_3");
								current_sound.play();
								current_sound.on('complete', function(){
									createjs.Sound.stop();
									var current_sound = createjs.Sound.play("s5_p8_4");
									current_sound.play();
									current_sound.on('complete', function(){
											$('.tips').show(500);
											nav_button_controls(100);
											createjs.Sound.stop();
										var current_sound = createjs.Sound.play("s5_p5");
										current_sound.play();
										current_sound.on('complete', function(){
											$('.tips').show(500);
											nav_button_controls(100);
										});
									});
								});
							});
						});
					// nav_button_controls(1000);
				break;
				case 8:
					$('.selectedvariable').html(selected_variable);
					$('.tips').hide(0);
					$('.mid_text_question').css({'top':'60%','left':'36%','width':'65%','font-size':'3.2vmin'});
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p9");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.tips').show(500);
							nav_button_controls(1000);
						});
				break;

				case 9:
					$('.mid_text_question').css({'top':'45%','left':'36%','width':'65%'});
					$('.check_button').css({'left':'43%','top':'91%'});
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p10");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.tips').show(500);
							nav_button_controls(1000);
						});
					$('.check_button').click(function(){
						if($('.input_text1').val()==10 && $('.input_text2').val()==5){
							createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							$('.check_button,.input_text1,.input_text2').css({'background':'#77c912','border-color':'#71b115','pointer-events':'none'});
							nav_button_controls(500);
						}
						else{
							createjs.Sound.stop();
							play_correct_incorrect_sound(0);
							$('.check_button').css({'background':'#ff0000','border-color':'#980000'});
						}
					});
					break;

				case 10:
					$('.mid_text_question').css({'top':'57%'});
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p11");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.tips').show(500);
							nav_button_controls(1000);
						});
					// nav_button_controls(500);
					break;

				case 12:
					$('.tips').hide(0);
					$('.mid_text_question').css('width','60%');
					$('.option1,.option2,.option3').css("pointer-events","none");
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p13");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.mid_text_question,.option1,.option2,.option3').animate({'left':'35%'},1000,function(){
							$('.tips').show(500);
							$('.option1,.option2,.option3').css("pointer-events","auto");
						});
					});
					$('.option1,.option2,.option3').click(function(){
						if($(this).hasClass('correct'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.option1,.option2,.option3').css({"pointer-events":"none"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					});
					break;

				case 13:
					$(".parsedstring3").html("!");
					$('.mid_text_question').css({'font-size': '3vmin','text-align': 'center','top':'40%'});
					$('.option1').css('top','65%');
					$('.option2').css('top','77.5%');
					// $('.option1,.option2,.option3').css("pointer-events","none");
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p14_1");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p14_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.tips').show(500);
							$('.option1,.option2,.option3').css("pointer-events","auto");
						});
					});

					$('.option1,.option2,.option3').click(function(){
						if($(this).hasClass('correct'))
						{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
							$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
							$('.option1,.option2,.option3').css({"pointer-events":"none"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(1);
							nav_button_controls(200);
						}
						else{
							var $this = $(this);
							var position = $this.position();
							var width = $this.width();
							var height = $this.height();
							var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
							var centerY = ((position.top + height)*100)/$board.height()+'%';
							$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
							$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					});
					break;
				case 14:
						var $this = $(".p15Txt3");
						$(".p15Txt3").append("<img class='s15cor' src='"+imgpath +"correct.png'/>");
						nav_button_controls(200);
					break;
				case 15:
					sound_player("s5_p15",1);
				break;
				case 16:
					$('ul').css('top','61%');
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p16");
					current_sound.play();
					current_sound.on('complete', function(){
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s5_p16_1");
						current_sound.play();
						current_sound.on('complete', function(){
							nav_button_controls();
						});
					});
					break;
				case 17:
					sound_player("s5_p17",1);
				break;
				case 18:
					$('.mid_text_question').css('top','27%');
					sound_player("s5_p18",1);
					break;

				case 19:
					$('.bottom_boxed-text3').css('height','33%');
					sound_player("s5_p19",1);
				break;
				case 20:
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s5_p20_1");
				current_sound.play();
				current_sound.on('complete', function(){
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s5_p20_2");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls();
					});
				});
				break;

					default:
					nav_button_controls(200);
					break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nxtBtnFlag?nav_button_controls(0):'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

		function put_speechbox_image(content, count){
			if(content[count].hasOwnProperty('speechbox')){
					var speechbox = content[count].speechbox;
					for(var i=0; i<speechbox.length; i++){
							var image_src = preload.getResult(speechbox[i].imgid).src;
							//get list of classes
							var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
							var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
							// console.log(selector);
							$(selector).attr('src', image_src);
					}
			}
		}

		/*===== This function splits the string in data into convential fraction used in mathematics =====*/
		function splitintofractions($splitinside){
			typeof $splitinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
				if($splitintofractions.length > 0){
					$.each($splitintofractions, function(index, value){
						$this = $(this);
						var tobesplitfraction = $this.html();
						if($this.hasClass('fraction')){
							tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
							tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
						}else{
							tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
							tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
						}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
						$this.html(tobesplitfraction);
					});
				}
		}
		/*===== split into fractions end =====*/
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'light_bg',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "mala",
					imgsrc : '',
					imgid : 'mala'
				}
			]
		}],
		speechbox:[{
			speechbox:'sp-1',
			imgid:'text_box02',
			textdata:data.string.p3text1,
			textclass:'inside_text',
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}]
	},

	// slide1
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text3,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide3
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text2,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text5,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide4
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text6,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text5,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text7,
			textclass: "mid_below_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text8,
			textclass: "below_mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text40,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide5
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text9,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text10,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text11,
			textclass: "below_mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide6
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text13,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text14,
			textclass: "below_mid_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text41,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide7
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text15,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text17,
			textclass: "option1 ",
		},{
			textdata:  data.string.p3text18,
			textclass: "option2",
		},{
			textdata:  data.string.p3text19,
			textclass: "option3 correct",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},
	// slide8
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text21,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text22,
			textclass: "mid_text_box_text",
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		},{
			textdata:  data.string.p3text23,
			textclass: "bottom_text",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text24,
			textclass: "option1 ",
		},{
			textdata:  data.string.p3text26,
			textclass: "option2 correct",
		},{
			textdata:  data.string.p3text25,
			textclass: "option3 ",
		},{
			textdata:  data.string.p3text42,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide9
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text28,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text29,
			textclass: "mid_text_box_text",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		},{
			textdata:  data.string.p3text31,
			textclass: "bottom_text",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text42,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide10
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text12,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'none'
		},{
			textdata:  data.string.p3text32,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text33,
			textclass: "bottom_boxed-text",
			splitintofractionsflag: true,
			datahighlightflag:true,
			datahighlightcustomclass:'blue_text'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide11
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text6,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text34,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text43,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},
	// slide12
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text9,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text35,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text43,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},

	// slide13
	{
		contentblockadditionalclass:'light_bg',
		extratextblock:[{
			textdata:  data.string.p3text9,
			textclass: "top_text",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text37,
			textclass: "mid_text_question",
			datahighlightflag:true,
			datahighlightcustomclass:'green_bg'
		},{
			textdata:  data.string.p3text38,
			textclass: "mid_text_box_text",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text39,
			textclass: "bottom_text",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		},{
			textdata:  data.string.p3text44,
			textclass: "tips",
			datahighlightflag:true,
			datahighlightcustomclass:'bold'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "top_mala",
					imgsrc : '',
					imgid : 'mala04'
				}
			]
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var selected_variable='';

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "diy_cover", src: imgpath+"a_08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala", src: imgpath+"mala01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mala04", src: imgpath+"mala04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "text_box02", src: imgpath+"text_box02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p2_1", src: soundAsset+"s3_p2_1.ogg"},
			{id: "s3_p2_2", src: soundAsset+"s3_p2_2.ogg"},

			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
			{id: "s3_p5", src: soundAsset+"s3_p5.ogg"},
			{id: "s3_p6", src: soundAsset+"s3_p6.ogg"},
			{id: "s3_p7", src: soundAsset+"s3_p7.ogg"},

			{id: "s3_p10", src: soundAsset+"s3_p10.ogg"},
			{id: "s3_p11", src: soundAsset+"s3_p11.ogg"},
			{id: "s3_p12", src: soundAsset+"s3_p12.ogg"},

			{id: "s3_p8_1", src: soundAsset+"s3_p8_1.ogg"},
			{id: "s3_p8_2", src: soundAsset+"s3_p8_2.ogg"},
			{id: "s3_p9_1", src: soundAsset+"s3_p9_1.ogg"},
			{id: "s3_p9_2", src: soundAsset+"s3_p9_2.ogg"},
			{id: "s3_p13_1", src: soundAsset+"s3_p13_1.ogg"},
			{id: "s3_p13_2", src: soundAsset+"s3_p13_2.ogg"},
			{id: "s3_p13_3", src: soundAsset+"s3_p13_3.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/


	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		splitintofractions($board);
		$(".input_text2").keydown(function (e) {
			// Allow: backspace, delete, tab, escape, enter and .
			if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1 ||
			// Allow: Ctrl+A, Command+A
			(e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
			// Allow: home, end, left, right, down, up
			(e.keyCode >= 35 && e.keyCode <= 40)) {
				// let it happen, don't do anything
				return;
			}
			if (e.keyCode == 13) {
				$('.submit_button').click();
			}
			// Ensure that it is a number and stop the keypress
			if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
				e.preventDefault();
			}
		});

		var height = (($('.top_text').height()/$('.coverboardfull').height() )* 100)+6.5+"%";
		$('.top_mala').css({
			'height':height
		});

		switch (countNext) {
			case 0:
			sound_player("s3_p1",1);
			break;
			case 1:
			$('.mid_text_question').css({'top':'40%'});
			$('.mid_text_question').hide(0);
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("s3_p2_1");
			current_sound.play();
			current_sound.on('complete', function(){
				sound_player("s3_p2_2",1);
				$('.mid_text_question').fadeIn(100);
			});
			// nav_button_controls(200);
			break;
			case 2:
			case 3:
			case 4:
			case 5:
			sound_player("s3_p"+(countNext+1),1);
			break;

			case 6:
				$('.option1,.option2,.option3').css("pointer-events","none");
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s3_p7");
				current_sound.play();
				current_sound.on('complete',function(){
					$('.option1,.option2,.option3').css("pointer-events","auto");
				});
				$('.option1,.option2,.option3').click(function(){
					if($(this).hasClass('correct'))
					{
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
						var centerY = ((position.top + height)*100)/$board.height()+'%';
						$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
						$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
						$('.option1,.option2,.option3').css({"pointer-events":"none"});
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						nav_button_controls(200);
					}
					else{
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
						var centerY = ((position.top + height)*100)/$board.height()+'%';
						$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(670%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
						$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
			break;

			case 7:
				$('.tips').hide(0);
				$('.option1,.option2,.option3').css("pointer-events","none");
				// $('.mid_text_question,.mid_text_box_text,.bottom_text,.option1,.option2,.option3').animate({'left':'35%'},1000,function(){
				// 	$('.tips').show(500);
				// });
				createjs.Sound.stop();
				var current_sound = createjs.Sound.play("s3_p8_1");
				current_sound.play();
				current_sound.on('complete',function(){
					$('.mid_text_question,.mid_text_box_text,.bottom_text,.option1,.option2,.option3').animate({'left':'35%'},1000,function(){
						$('.tips').show(500);
						createjs.Sound.stop();
						var current_sound = createjs.Sound.play("s3_p8_2");
						current_sound.play();
						current_sound.on('complete', function(){
							$('.option1,.option2,.option3').css("pointer-events","auto");
						});
					});
				});
				$('.mid_text_question').css({'top':'25%','text-align':'center'});
				$('.option1').css('top','66%');
				$('.option2').css('top','78%');
				$('.option3').css('top','90%');
				$('.option1,.option2,.option3').click(function(){
					if($(this).hasClass('correct'))
					{
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
						var centerY = ((position.top + height)*100)/$board.height()+'%';
						$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(628%,-62%)" src="'+imgpath +'correct.png" />').insertAfter(this);
						$(this).css({"background":"rgb(152,192,46)","border-radius":".5em","border-color":"rgb(197, 224, 123)"});
						$('.option1,.option2,.option3').css({"pointer-events":"none"});
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						nav_button_controls(200);
					}
					else{
						var $this = $(this);
						var position = $this.position();
						var width = $this.width();
						var height = $this.height();
						var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
						var centerY = ((position.top + height)*100)/$board.height()+'%';
						$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(628%,-62%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
						$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":".5em","border-color":"rgb(226, 129, 129)"});
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
			break;

			case 8:

			$('.tips').hide(0);
			$('.mid_text_question').css({'top':'28%','text-align':'center'});
			$('.mid_text_box_text').css({'top':'57%','width':'50%'});
			$('.bottom_text').css('top','88%');

			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("s3_p9_1");
			current_sound.play();
			current_sound.on('complete',function(){
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s3_p9_2");
					current_sound.play();
					current_sound.on('complete', function(){
						$('.mid_text_question,.mid_text_box_text,.bottom_text,.option1,.option2,.option3').animate({'left':'35%'},1000,function(){
							$('.tips').show(500);
						$('.option1,.option2,.option3').css("pointer-events","auto");
						nav_button_controls(2000);
					});
				});
			});
			// $('.mid_text_question,.mid_text_box_text,.bottom_text').animate({'left':'35%'},1000,function(){
			// 	nav_button_controls(500);
			// 	$('.tips').show(500);
			// });
			break;
			case 9:
			case 10:
			case 11:
				sound_player("s3_p"+(countNext+1),1);
			break;

			case 12:
			$('.mid_text_box_text').css({'top':'60%','text-align':'left'});
			$('.bottom_text').css({'width':'51%','text-align':'left','top': '86%','left':'38%'});
			$('.bottom_text').hide(0);
			createjs.Sound.stop();
			var current_sound = createjs.Sound.play("s3_p13_1");
			current_sound.play();
			current_sound.on('complete',function(){
				$('.bottom_text').show(0);
					createjs.Sound.stop();
					var current_sound = createjs.Sound.play("s3_p13_2");
					current_sound.play();
					current_sound.on('complete', function(){
						nav_button_controls(2000);
					});
				});
			break;
			default:
			nav_button_controls(200);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, nxtBtnFlag){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nxtBtnFlag?nav_button_controls():'';
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}

	/*===== This function splits the string in data into convential fraction used in mathematics =====*/
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
				$this.html(tobesplitfraction);
			});
		}
	}
	/*===== split into fractions end =====*/
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q1opt3,
        questiondiv:"fracclass",
        questionblock: [
            {
                splitintofractionsflag: true,
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q1opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q1opt2
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "content2 centertext",
                textdata: data.string.q1opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q1opt4
            }

        ],
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q2opt1,
        questiondiv:"fracclass",
        questionblock: [
            {
                splitintofractionsflag: true,
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q2opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q2opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q2opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q2opt4
            }

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q3opt3,
        questiondiv:"fracclass",
        questionblock: [
            {
                splitintofractionsflag: true,
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q3opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q3opt2
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "content2 centertext",
                textdata: data.string.q3opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q3opt4
            }

        ],
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q4opt1,
        questiondiv:"fracclass",
        questionblock: [
            {
                splitintofractionsflag: true,
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q4
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q4opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q4opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q4opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q4opt4
            }

        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q5opt2,
        questionblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q5
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q5opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q5opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q5opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q5opt4
            }

        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q6opt2,
        questionblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q6
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q6opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q6opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q6opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q6opt4
            }

        ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q7opt2,
        questionblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q7
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q7opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q7opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q7opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q7opt4
            }

        ],
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q8opt2,
        questionblock: [
            {
                splitintofractionsflag: true,
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q8
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.q8opt1
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "content2 centertext",
                textdata: data.string.q8opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q8opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q8opt4
            }

        ],
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q9opt1,
        questionblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q9
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q9opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q9opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q9opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q9opt4
            }

        ],
    },
    //slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        ans:data.string.q10opt1,
        questionblock: [
            {
                datahighlightflag : true,
                datahighlightcustomclass : 'randomval',
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q10
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "content2 centertext",
                textdata: data.string.q10opt1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.q10opt2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.q10opt3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "content2 centertext",
                textdata: data.string.q10opt4
            }

        ],
    }
];
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var rhino = new RhinoTemplate();
    rhino.init(content.length);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [


            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        // current_sound = createjs.Sound.play('sound_1');
        // current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        /*generate question no at the beginning of question*/
        rhino.numberOfQuestions();
        texthighlight($board);
        splitintofractions($board);
        switch (countNext) {
            case 0:
                var randompercentage = generaterandomnum([1,2,3,4],[0]);
                $(".top").html(randompercentage);
                var correctans = (randompercentage * 100)/5;
                shufflehint();
                checkans(correctans+"%");
                break;
            case 1:
            case 2:
                var randompercentagesec = generaterandomnum([10,20,25,50],[0]);
                var array1 = [];
                for(var i=1;i<randompercentagesec;i++){
                    array1.push(i);
                }
                var randompercentagefirst = generaterandomnum(array1,[0]);
                $(".top").html(randompercentagefirst);
                $(".bottom").html(randompercentagesec);
                var correctans = (randompercentagefirst * 100)/randompercentagesec;
               setoption([10,20,30,40,50,60,70,80,90,100],correctans);

                shufflehint();
                checkans(correctans+"%");
                break;
            case 3:
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                var randompercentagefirst = generaterandomnum(mainarray,[0]);
                $(".top").html(randompercentagefirst);
                var correctans = (randompercentagefirst * 100)/100;
                setoption(mainarray,correctans);
                shufflehint();
                checkans(correctans+"%");
                break;
            case 4:
                var range = 100;
                var number = Math.floor( Math.random() * range / 2 ) * 2;
                $(".randomval").html(number);
                var correctans = (50 * number)/100;
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                setoption1(mainarray,correctans);
                shufflehint();
                checkans(correctans);
                break;
            case 5:
                var range = 100;
                var number = generaterandomnum([20,30,40,50,60,70,80,90],[0])
                $(".randomval").html(number);
                var correctans = (10 * number)/100;
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                setoption1(mainarray,correctans);
                shufflehint();
                checkans(correctans);
                break;
            case 6:
                var number = generaterandomnum([10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95],[60]);

                $(".randomval").html(number);
                var correctans = (60 * number)/100;
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                setoption1(mainarray,correctans);
                shufflehint();
                checkans(correctans);
                break;
            case 7:
                var range = 100;
                var number = generaterandomnum([25,50,200,300,500],[16]);
                $(".randomval").html(number);
                var correctans = (16 * number)/100;
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                setoption1(mainarray,correctans);
                shufflehint();
                checkans(correctans);
                break;
            case 8:
                var range = 100;
                var number = generaterandomnum([3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60,63,66,69,72,75],[0]);
                $(".randomval").html(number);
                var correctans = (number*100)/75;
                var mainarray = [];
                for(var i=1;i<100;i++){
                    mainarray.push(i);
                }
                setoption(mainarray,correctans);
                shufflehint();
                checkans(correctans+"%");
                break;
            case 9:
                var number = generaterandomnum([10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95],[0]);

                $(".randomval").html(number);
                var correctans = number - number/5;
                var mainarray = [];
                for(var i=1;i<=number;i++){
                    mainarray.push(i);
                }
                setoption1(mainarray,correctans);
                shufflehint();
                checkans(correctans);
                break;

            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
  		createjs.Sound.stop();
  		clearTimeout(timeoutvar);
  			countNext++;
  			rhino.gotoNext();
  			if(countNext < 10){
  				templateCaller();
  			} else {
  				$(".scoreboard").hide(0);
  				$nextBtn.hide(0);
  			}
    });

    // $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }
    function shufflehint() {
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });

    }
    function checkans(correctans){
        $(".commonbtn ").on("click",function () {
            if($(this).find("p").text()==correctans ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                rhino.update(true);
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
                rhino.update(false);
            }
        });
    }
    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function generaterandomnum(array,excludenum){
        for(var i = 0; i<excludenum.length;i++) {
            array.splice(array.indexOf(excludenum[i]), 1);
        }
        var random = array[Math.floor(Math.random() * array.length)];
        return random;
    }
    function setoption(array1,correctans){
        $(".option1 p").html(correctans+"%");
        var opt2 = generaterandomnum(array1,[correctans]);
        var opt3 = generaterandomnum(array1,[correctans,opt2]);
        var opt4 = generaterandomnum(array1,[correctans,opt2,opt3]);
        $(".option2 p").html(opt2+"%");
        $(".option3 p").html(opt3+"%");
        $(".option4 p").html(opt4+"%");
    }
    function setoption1(array1,correctans){
        $(".option1 p").html(correctans);
        var opt2 = generaterandomnum(array1,[correctans]);
        var opt3 = generaterandomnum(array1,[correctans,opt2]);
        var opt4 = generaterandomnum(array1,[correctans,opt2,opt3]);
        $(".option2 p").html(opt2);
        $(".option3 p").html(opt3);
        $(".option4 p").html(opt4);
    }

    function splitintofractions($splitinside){
        typeof $splitinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
        if($splitintofractions.length > 0){
            $.each($splitintofractions, function(index, value){
                $this = $(this);
                var tobesplitfraction = $this.html();
                if($this.hasClass('fraction')){
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
                }else{
                    tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
                    tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
                }


                tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
                $this.html(tobesplitfraction);
            });
        }
    }
});

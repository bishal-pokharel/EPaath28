var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
		//slide 0
		contentblockadditionalclass: "ole-background-gradient-apple",
		contentblockcenteradjust: true,
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textclass: "chapter fadein",
			textdata: data.string.p4t1
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblockadditionalclass: "leftupper",
		uppertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p4t2
		}

		]
	},
	{
		//slide 2
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblockadditionalclass: "leftupper",
		uppertextblock:[
		{
			textdata: data.string.p4t2
		}
		],
		lowertextblockadditionalclass: "rightlower",
		lowertextblock:[
		{
			textclass: "fadein",
			splitintofractionsflag: true,
			textdata: data.string.p4t3
		}
		]
	},
	{
		//slide3
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			textdata : data.string.p4t4,
			textclass : 'my_font_very_big title-2'
		},
		{
			textdata : data.string.p4t5,
			textclass : 'my_font_big chatrap'
		}],
		lowertextblock: [{
			textdata : data.string.hint1,
			textclass : 'hint-text hintf'
		},{
			textdata : data.string.hint2,
			splitintofractionsflag: true,
			textclass : 'hint-text hints'
		},{
			textdata : data.string.hint3,
			splitintofractionsflag: true,
			textclass : 'hint-text hintt'
		}],

		inputblock: [{
			inputdiv : 'ques_row check_div fade_in_1 div_0',
			textdata: data.string.p4t6,
			textdata2: "%",
			textclass: 'my_font_big example',
			textclass2: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.btn2,
					textclass: 'default_btn my_font_big check_btn',
				},
			]
		},

		{
			inputdiv : 'ques_row check_div div_1 fade_in_1',
			textdata: data.string.p4t7,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.btn2,
					textclass: 'default_btn my_font_big check_btn',
				}
			]
		},

		{
			inputdiv : 'ques_row check_div div_2 fade_in_1',
			textdata: data.string.p4t8,
			textclass: 'my_font_big example',
			inputclass: 'input_class my_font_big',
			inputdata: '',
			buttonblock: [
				{
					textdata: data.string.btn2,
					textclass: 'default_btn my_font_big check_btn',
				}
			]
		}
		],
	},
	{
		//slide4
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblockadditionalclass: 'question-text',
		uppertextblock : [{
			textdata : data.string.p4t9,
			textclass : 'my_font_very_big title-2'
		},
		],
		lowertextblock:[
		{
			textclass : 'maleno',
			textdata: data.string.p4t7,
		},
		{
			textclass : 'femaleno',
			textdata: data.string.p4t8,
		},
		{
			textclass : 'maleper',
			textdata: "<span class='maleval'>0</span>%",
		},
		{
			textclass : 'femaleper',
			textdata: "<span class='femaleval'>100</span>%",
		},
		{
			textclass : 'slihelp',
			textdata: data.string.p4t10,
		},
		{
			textclass: "ole-template-check-btn-default submitbtn",
			textdata: data.string.btn
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2",
			textdata: data.string.btn
		},
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1"
		},
		{
			inputclass: "ole-template-input-box-default input2"
		}
		],
		imageblock:[
			{
				imagetoshow:[
				{
					imgclass : "boy",
					imgsrc: imgpath + "rajan.png"
				},
				{
					imgclass : "girl",
					imgsrc: imgpath + "reeta.png"
				}]
			}
		]
	}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s4_p1", src: soundAsset+"s4_p1.ogg"},
			{id: "s4_p2", src: soundAsset+"s4_p2.ogg"},
			{id: "s4_p3", src: soundAsset+"s4_p3.ogg"},
			{id: "s4_p4_1", src: soundAsset+"s4_p4_1.ogg"},
			{id: "s4_p4_2", src: soundAsset+"s4_p4_2.ogg"},
			{id: "s4_p5", src: soundAsset+"s4_p5.ogg"},
			{id: "s4_p5_1", src: soundAsset+"s4_p5_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
	    splitintofractions($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
	    switch(countNext){
	    	case 3:
	    		sound_player("s4_p4_1",0);

			input_box('.div_0>.input_class', 5, '.div_0>.check_btn');
			input_box('.div_1>.input_class', 5, '.div_1>.check_btn');
			input_box('.div_2>.input_class', 5, '.div_2>.check_btn');
			$('.div_0>.check_btn').click(function(){
				createjs.Sound.stop();
	    		setTimeout(function(){sound_player("s4_p4_2",0);},2000);
				if( parseInt( $('.div_0>.input_class').val() ) == 75){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					$(".hintf").hide(0);
					$('.div_0>.input_class').prop('disabled', true);
					$(".title-2").append(data.string.p4t11);
					//$(".chatrap").fadeOut();

					$('.div_0>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_0>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_1').css('display', 'flex');
						$('.div_1').fadeIn(1000);
					});
				} else {
					$(this).addClass('incorrect');
						createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$(".hintf").animate({
						opacity: 1,
						bottom: "+=10"
					});
				}
			});

			$('.div_1>.check_btn').click(function(){
				if( parseInt( $('.div_1>.input_class').val() ) == 7){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					$(".hints").hide(0);
					$('.div_1>.input_class').prop('disabled', true);
					$('.div_1>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_1>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						$('.div_2').css('display', 'flex');
						$('.div_2').fadeIn(1000);
						$('.hint_2').removeClass('change_color_1');
					});
				} else {
					$(this).addClass('incorrect');
						createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$(".hints").animate({
						opacity: 1,
						bottom: "+=10"
					});
				}
			});
			$('.div_1>.hint_btn').click(function(){
				$('.hint_2').addClass('change_color_1');
			});


			$('.div_2>.check_btn').click(function(){
				if( parseInt( $('.div_2>.input_class').val() ) == 21){
					$(this).removeClass('incorrect');
					$(this).addClass('correct');
						createjs.Sound.stop();
					play_correct_incorrect_sound(1);
					$(".hintt").hide(0);
					$('.div_2>.input_class').prop('disabled', true);
					$('.div_2>.default_btn').delay(500).fadeOut(1000,function(){
						$('.div_2>.default_btn').css({'display': 'flex', 'visibility': 'hidden'});
						navigationcontroller();
					});
				} else {
					$(this).addClass('incorrect');
						createjs.Sound.stop();
					play_correct_incorrect_sound(0);
					$(".hintt").animate({
						opacity: 1,
						bottom: "+=10"
					});
				}
			});
			break;
	    	case 4:
	    		sound_player("s4_p"+(countNext+1),0);
	    	$("#slider").slider({
	    		value: 50,
	    		min: 0,
	    		max: 100
	    	});
	    	var step;

	    	$("#slider").on("slide", function(e, ui){
	    		step = ui.value;
	    		$(".maleval").text(step);
	    		$(".femaleval").text(100 - step);
	    	});
	    		$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn").trigger("click");
					}
			 	});

			 	$(".input2").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn").trigger("click");
					}
			 	});

			 	var access = true;
	    		$(".submitbtn").click(function(){
						sound_player("s4_p5_1", 0);
	    			if($(".input1").val() == '' || $(".input2").val() == ''){
						access = false;
	    				if($(".input1").val() == '')
							$(".input1").addClass("ole-template-input-box-default-incorrect");

	    				if($(".input2").val() == '')
							$(".input2").addClass("ole-template-input-box-default-incorrect");

	    			}
	    			else{
	    				access = true;
	    			}


	    			if(access){
	    				var stuPer;
	    				if(parseInt($(".input1").val()) < parseInt($(".input2").val())){
	    					stuPer = Math.round($(".input1").val()/(parseInt($(".input1").val()) + parseInt($(".input2").val())) * 100);
	    					console.log((parseInt($(".input1").val()) + parseInt($(".input2").val())));
	    				}
	    				else{
	    					stuPer = Math.round($(".input2").val()/(parseInt($(".input1").val()) + parseInt($(".input2").val())) * 100);
	    					stuPer = 100 - stuPer;
	    					console.log(stuPer);
	    				}


						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						$(".input2").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
	    				//alert(stuPer);
	    				$("#slider").show(0);
	    				$(".maleper").show(0);
	    				$(".femaleper").show(0);
	    				$(".boy").show(0);
	    				$(".girl").show(0);
	    				$(".slihelp").show(0);
	    				$(".submitbtn2").show(0);
	    			}

	    			$(".submitbtn2").click(function(){
	    				if(step == stuPer){
								createjs.Sound.stop();
								play_correct_incorrect_sound(1);
							$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
	    					navigationcontroller();
	    				}
	    				else{
								createjs.Sound.stop();
								play_correct_incorrect_sound(0);
							$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
	    				}
	    			});
	    		});
	    	break;
	    	default:
	    		sound_player("s4_p"+(countNext+1),1);
	    	break;
		}
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():"";
		});
	}
	function input_box(input_class, max_number, button_class) {
		$(input_class).keydown(function(event){
    		var charCode = (event.which) ? event.which : event.keyCode;
    		/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
    		if(charCode === 13 && button_class!=null) {
		        $(button_class).trigger("click");
			}
			var condition = charCode != 8 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, backspace or arrow keys
   			if (!condition) {
    			return true;
    		}
    		//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
        		return false;
    		}
    		//check . and 0-9 separately after checking arrow and other keys
    		if((charCode < 48 || charCode > 57) && (charCode < 96 || charCode > 105) && charCode != 190 && charCode != 110 ){
    			return false;
    		}
    		//check max no of allowed digits
    		if (String(event.target.value).length >= max_number) {
    			return false;
    		}
  			return true;
		});
	}

	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext < 3)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		createjs.Sound.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	// templateCaller();
});

/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/


    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
		//slide 0
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t2
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "bottle",
				imgsrc : imgpath + "empty-bottle.png",
			},
			{
				imgclass : "watertap",
				imgsrc : imgpath + "watertap.png",
			},
			{
				imgclass : "handicon",
				imgsrc : imgpath + "hand-icon.gif",
			}
			]
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t3
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "bottle",
				imgsrc : imgpath + "50-percentage.gif",
			},
			{
				imgclass : "watertap",
				imgsrc : imgpath + "watertap.png",
			},
			{
				imgclass : "handicon",
				imgsrc : imgpath + "hand-icon.gif",
			}
			]
		}
		],
	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t4
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "bottle",
				imgsrc : imgpath + "100percentage.gif",
			},
			{
				imgclass : "watertap",
				imgsrc : imgpath + "watertap.png",
			}
			]
		}
		],
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t5
		}
		]
	},
	{
		//slide 5
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t6
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "roti",
				imgsrc : imgpath + "rotione.png",
			}
			]
		}
		],
	},
	{
		//slide 6
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t7
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "roti",
				imgsrc : imgpath + "75per_roti.png",
			}
			]
		}
		],
	},
	{
		//slide 7
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t8
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "roti",
				imgsrc : imgpath + "half01.png",
			}
			]
		}
		],
	},
	{
		//slide 8
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p2t1
		}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
		{
			textclass: "fadein",
			textdata: data.string.p2t9
		},
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "roti",
				imgsrc : imgpath + "one_forth.png",
			}
			]
		}
		],
	},
	{
		//slide 9
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblock:[
		{
			textclass: "centertext fadein",
			textdata: data.string.p2t10
		}
		],
	},
	{
		//slide 10
		contentblockadditionalclass: "ole-background-gradient-apple fadein",
		uppertextblockadditionalclass: "ole_temp_uppertextblock dragthing",
		uppertextblock:[
		{
			textdata: data.string.p2t11
		},
		],
		lowertextblock:[
		{
			textclass: "dragitems-first",
			splitintofractionsflag: true,
			textdata: "::12_/_80;;"
		},
		{
			textclass: "dragitems-second",
			splitintofractionsflag: true,
			textdata: "::1_/_5;;"
		},
		{
			textclass: "drophere-one emtbtn",
		},
		{
			textclass: "drophere-two emtbtn",
		},
		{
			textclass: "drophere-three emtbtn",
		},
		{
			textclass: "drophere-four emtbtn"
		},
		{
			textclass: "hint-one",
			splitintofractionsflag: true,
			textdata: "::12_/_80;; &#215; 100%"
		},
		{
			textclass: "hint-two",
			splitintofractionsflag: true,
			textdata: "::1_/_5;; &#215; 100%"
		},
		{
			textclass: "hint-three",
			splitintofractionsflag: true,
			textdata: "::1_/_2;; &#215; 100%"
		},
		{
			textclass: "hint-four",
			splitintofractionsflag: true,
			textdata: "::3_/_4;; &#215; 100%"
		},
		{
			textclass: "lowertb",
			textdata: data.string.p2t12
		}
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "dragitems-third",
				imgsrc : imgpath + "50percentage.png",
			},
			{
				imgclass : "dragitems-fourth",
				imgsrc : imgpath + "75per_roti.png",
			}
			]
		}
		],
		buttonsblock:[
		{
			btnblockadditionalclass: "ole_translate thibtnset",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default btn1",
				textdata: "15%"
			},
			{
				textclass: "ole-template-check-btn-default btn2",
				textdata: "75%"
			},
			{
				textclass: "ole-template-check-btn-default btn3 correct",
				textdata: "50%"
			},
			{
				textclass: "ole-template-check-btn-default btn4",
				textdata: "20%"
			}
			]
		},
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s2_p1", src: soundAsset+"s2_p1.ogg"},
			{id: "s2_p2", src: soundAsset+"s2_p2.ogg"},
			{id: "s2_p3", src: soundAsset+"s2_p3.ogg"},
			{id: "s2_p4", src: soundAsset+"s2_p4.ogg"},
			{id: "s2_p5", src: soundAsset+"s2_p5.ogg"},
			{id: "s2_p6", src: soundAsset+"s2_p6.ogg"},
			{id: "s2_p7", src: soundAsset+"s2_p7.ogg"},
			{id: "s2_p8", src: soundAsset+"s2_p8.ogg"},
			{id: "s2_p9", src: soundAsset+"s2_p9.ogg"},
			{id: "s2_p10", src: soundAsset+"s2_p10.ogg"},
			{id: "s2_p11", src: soundAsset+"s2_p11.ogg"},
			{id: "s2_p11_1", src: soundAsset+"s2_p11_1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
	    splitintofractions($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch(countNext){
			case 1:
			case 2:
				sound_player("s2_p"+(countNext+1),0);
				$nextBtn.hide(0);
				$(".handicon").hide(0).delay(2000).show(0);
				$(".watertap,.handicon").click(function(){
					$nextBtn.trigger("click");
				});
			break;
			case 10:
				sound_player("s2_p"+(countNext+1),0);
				var finCount = 0;
				$(".lowertb").hide(0);
				$(".ole-template-check-btn-default").draggable({
					containment : ".generalTemplateblock",
					revert : "invalid",
					cursor : "move",
					zIndex: 100000,
				});

				$(".drophere-one").droppable({
				accept: ".btn1",
				drop: function (event, ui){
					$this = $(this);
					$(".hint-one").show(0);
					dropfunc(event, ui, $this);
				}
				});

				$(".drophere-two").droppable({
				accept: ".btn4",
				drop: function (event, ui){
					$this = $(this);
					$(".hint-two").show(0);
					dropfunc(event, ui, $this);
				}
				});

				$(".drophere-three").droppable({
				accept: ".btn3",
				drop: function (event, ui){
					$this = $(this);
					$(".hint-three").show(0);
					dropfunc(event, ui, $this);
				}
				});

				$(".drophere-four").droppable({
				accept: ".btn2",
				drop: function (event, ui){
					$this = $(this);
					$(".hint-four").show(0);
					dropfunc(event, ui, $this);
				}
				});

			function dropfunc(event, ui, $droppedOn){
				var classText = ui.draggable.text();
				ui.draggable.draggable('disable');
				ui.draggable.hide(0);
				$droppedOn.html(classText);
				$droppedOn.removeClass("emtbtn");
				$droppedOn.addClass('itsdropped');
				finCount++;
				if(finCount == 4){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("s2_p11_1");
					current_sound.play();
					current_sound.on('complete', function(){
						navigationcontroller();
					});
					$(".lowertb").fadeIn(1000);
				}
			}
			break;
			default:
				sound_player("s2_p"+(countNext+1),1);
			break;
		}
	}

	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():"";
		});
	}
	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 10)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		if(countNext == 3){
			$flexshifter = $(".flexcontainerblock100").children().clone();
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	// templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/
    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

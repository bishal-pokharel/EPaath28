var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[

	{
		//slide 0
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t1
		}
		],
		lowertextblock:[
		{
				textclass: "q1frac",
				splitintofractionsflag: true,
				textdata: data.string.p3_frac2
		},
		{
				textclass: "q1per",
				textdata: data.string.per
		},
		{
			textclass: "ole-template-check-btn-default submitbtn0",
			textdata: data.string.btn2
		},
		{
			textclass: "lowertb",
			textdata: data.string.p3t2
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input0"
		}],
	},
	{
		//slide 1
		contentblockadditionalclass: "ole-background-gradient-apple fadein",

		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t3
		}
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "dragitems-first",
				imgsrc : imgpath + "folded-paper_01.png",
			},
			{
				imgclass : "dragitems-second",
				imgsrc : imgpath + "folded-paper_01.png",
			},
			{
				imgclass : "dragitems-third",
				imgsrc : imgpath + "folded-paper_01.png",
			}
			],
			imagelabels:[
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-one",
				imagelabeldata: "::40_/_80;;"
			},
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-two",
				imagelabeldata: "::12_/_16;;"
			},
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-three",
				imagelabeldata: "::57_/_60;;"
			},
			{
				imagelabelclass: "test-one",
				imagelabeldata: data.string.ftest
			},
			{
				imagelabelclass: "test-two",
				imagelabeldata: data.string.stest
			},
			{
				imagelabelclass: "test-three",
				imagelabeldata: data.string.ttest
			}
			]
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole-background-gradient-apple",

		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			textdata: data.string.p3t4
		}
		],
		imageblock : [
		{
			imagetoshow: [
			{
				imgclass : "dragitems-first",
				imgsrc : imgpath + "folded-paper_01.png",
			},
			{
				imgclass : "dragitems-second secondset",
				imgsrc : imgpath + "folded-paper_01.png",
			},
			{
				imgclass : "dragitems-third thirdset",
				imgsrc : imgpath + "folded-paper_01.png",
			}
			],
			imagelabels:[
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-one",
				imagelabeldata: "::40_/_80;;"
			},
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-two secondset",
				imagelabeldata: "::12_/_16;;"
			},
			{
				splitintofractionsflag: true,
				imagelabelclass: "marks-three thirdset",
				imagelabeldata: "::57_/_60;;"
			},
			{
				imagelabelclass: "test-one",
				imagelabeldata: data.string.ftest
			},
			{
				imagelabelclass: "test-two secondset",
				imagelabeldata: data.string.stest
			},
			{
				imagelabelclass: "test-three thirdset",
				imagelabeldata: data.string.ttest
			},
			]
		}
		],
		lowertextblock:[
		{
			textclass: "ole-template-check-btn-default submitbtn1 fadein",
			textdata: data.string.btn
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2 secondset",
			textdata: data.string.btn
		},
		{
			textclass: "ole-template-check-btn-default submitbtn3 thirdset",
			textdata: data.string.btn
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1 fadein"
		},
		{
			inputclass: "ole-template-input-box-default input2 secondset"
		},
		{
			inputclass: "ole-template-input-box-default input3 thirdset"
		}
		],
	},
	{
		//slide 3
		contentblockadditionalclass: "ole-background-gradient-apple",
		uppertextblock:[
		{
			textclass: "centertext fadein",
			textdata: data.string.p3t5
		}
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s3_p1", src: soundAsset+"s3_p1.ogg"},
			{id: "s3_p1_1", src: soundAsset+"s3_p1_1.ogg"},
			{id: "s3_p2", src: soundAsset+"s3_p2.ogg"},
			{id: "s3_p3", src: soundAsset+"s3_p3.ogg"},
			{id: "s3_p4", src: soundAsset+"s3_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

   	Handlebars.registerPartial("fractioncontent", $("#fractioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ?
			// ole.footerNotificationHandler.lessonEndSetNotification() :
			// ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
	    splitintofractions($board);
	    $nextBtn.hide(0);
		$prevBtn.hide(0);
	    switch(countNext){
	    	case 0:
	    		sound_player("s3_p"+(countNext+1),0);
	    		$nextBtn.hide(0);
	    		$(".lowertb").hide(0);
	    		$(".input0").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn0").trigger("click");
					}
			 	});

			 	$(".submitbtn0").click(function(){
					if($(".input0").val() == 32){
						$(".input0").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn0").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".lowertb").fadeIn(1000);
						// navigationcontroller();
							createjs.Sound.stop();
						// play_correct_incorrect_sound(1);
			    		sound_player("s3_p1_1",1);
					}
					else{
						$(".input0").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn0").addClass("ole-template-check-btn-default-incorrect");
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
	    	break;
	    	case 2:
	    		sound_player("s3_p"+(countNext+1),0);
	    	$(".lowertb2").hide(0);
	    	$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn1").trigger("click");
					}
			 });

	    	$(".submitbtn1").click(function(){
					if($(".input1").val() == 50){
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn1").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						$(".secondset").fadeIn();
					}
					else{
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn1").addClass("ole-template-check-btn-default-incorrect");
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});

	    	$(".input2").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 });

	    	$(".submitbtn2").click(function(){
					if($(".input2").val() == 75){
						$(".input2").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						$(".thirdset").fadeIn();
					}
					else{
						$(".input2").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});

	    	$(".input3").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn3").trigger("click");
					}
			 });

	    	$(".submitbtn3").click(function(){
					if($(".input3").val() == 95){
						$(".input3").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn3").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
						navigationcontroller();
						//$(".ole_temp_uppertextblock").text(data.string.p3t5).hide(0).fadeIn(1000);
					}
					else{
						$(".input3").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn3").addClass("ole-template-check-btn-default-incorrect");
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
	    	break;
	    	case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("s3_p"+(countNext+1));
				current_sound.play();
				current_sound.on('complete', function(){
					ole.footerNotificationHandler.pageEndSetNotification()
				});
	    	break;
	    	default:
	    		sound_player("s3_p"+(countNext+1),1);
	    	break;
	    }
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():"";
		});
	}
	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 2)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	// templateCaller();
});
/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[
	{
		//slide 0
		contentblockadditionalclass: "ole-background-gradient-apple",
        lowertextblock:[
            {
                textclass: "chapter",
                textdata: data.string.chaptername
            }
        ],
        imageblock:[{
            imagetoshow: [
                {
                    imgclass: "coverpage",
                    imgsrc: imgpath+"cover_page.png",
                },
            ]
        }]
	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock fadein",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			textdata: data.string.p1t2
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100 fadein",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1",
			textdata: data.string.p1t2
		},
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t4
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2 fadein",
			textdata: data.string.btn
		}
		],
		inputbox:[
		{
			inputclass: "ole-template-input-box-default input1 fadein"
		}],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "hiddenmon sundarinc",
				imgsrc: "images/sundar/incorrect-2.png",
			},
			{
				imgclass: "hiddenmon sundarcor",
				imgsrc: "images/sundar/normal.png",
			}]
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1",
			textdata: data.string.p1t3
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 5
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1",
			textdata: data.string.p1t3
		},
		{
			textclass: "ole_translate sidetext2 fadein",
			textdata: data.string.p1t10
		},
		{
			textclass: "ole-template-check-btn-default submitbtn2 fadein",
			textdata: data.string.btn
		}
		],
		fractioninput: 'fractioninput fadein',
		inputclass_numerator: 'box_numerator digit_box_2',
		inputclass_denominator: 'box_denominator digit_box_2',
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			},
		]
	},
	{
		//slide 6
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			splitintofractionsflag: true,
			textdata: data.string.p1t5
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 7
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1",
			splitintofractionsflag: true,
			textdata: data.string.p1t5
		},
		{
			textclass: "ole_translate sidetext3 fadein",
			textdata: data.string.p1t6
		},
		],
		buttonsblock:[
		{
			btnblockadditionalclass: "ole_translate thibtnset fadein",
			indbtn: [
			{
				textclass: "ole-template-check-btn-default",
				textdata: "100"
			},
			{
				textclass: "ole-template-check-btn-default correct",
				textdata: "10"
			},
			{
				textclass: "ole-template-check-btn-default",
				textdata: "1"
			}
			]
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 8
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1 fadein",
			splitintofractionsflag: true,
			textdata: data.string.p1t7
		},
		{
			textclass: "ole_translate sidetext3 fadein",
			splitintofractionsflag: true,
			textdata: data.string.p1t8
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 9
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext1",
			splitintofractionsflag: true,
			textdata: data.string.p1t7
		},
		{
			textclass: "ole_translate sidetext3",
			splitintofractionsflag: true,
			textdata: data.string.p1t8
		},
		{
			textclass: "ole_translate sidetext4 fadein",
			splitintofractionsflag: true,
			textdata: data.string.p1t9
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 10
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext3 moveleft",
			splitintofractionsflag: true,
			textdata: data.string.p1t8
		}
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},
	{
		//slide 11
		contentblockadditionalclass: "ole_temp_bluecre_background1",
		headerblockadditionalclass: "ole_temp_bluecre_header",
		headerblock:[
		{
			textdata : data.lesson.chapter
		}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "letshigh",
			textdata: data.string.p1t1
		}
		],
		lowertextblock:[
		{
			textclass: "ole_translate sidetext3-1",
			splitintofractionsflag: true,
			textdata: data.string.p1t8
		},
		{
			textclass: "ole_translate sidetext4-1 fadein",
			splitintofractionsflag: true,
			textdata: data.string.p1t11
		},
		],
		flexblockcontainers: [
			{
				flexblockadditionalclass: "flexcontainerblock100",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			},
			{
				flexblockadditionalclass: "flexcontainerblock1002 fadein",
				flexblock:[
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						{flexboxrowclass :"rownormal selectthis",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
					{
						flexblockcolumn:[
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						{flexboxrowclass :"rownormal",},
						]
					},
				]
			}
		]
	},

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var $flexshifter;
	var ClickCount = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			// {id: "tb-3", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "s1_p1", src: soundAsset+"s1_p1.ogg"},
			{id: "s1_p2", src: soundAsset+"s1_p2.ogg"},
			{id: "s1_p3", src: soundAsset+"s1_p3.ogg"},
			{id: "s1_p4", src: soundAsset+"s1_p4.ogg"},
			{id: "s1_p5", src: soundAsset+"s1_p5.ogg"},
			{id: "s1_p6", src: soundAsset+"s1_p6.ogg"},
			{id: "s1_p7", src: soundAsset+"s1_p7.ogg"},
			{id: "s1_p8", src: soundAsset+"s1_p8.ogg"},
			{id: "s1_p9", src: soundAsset+"s1_p9.ogg"},
			{id: "s1_p10", src: soundAsset+"s1_p10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
	    splitintofractions($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		switch(countNext){
			case 3:
	    		sound_player("s1_p"+(countNext+1),0);
				ClickCount = 0;
				$(".hiddenmon").hide(0);
				$nextBtn.hide(0);
				$(".input1").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});

				$(".submitbtn2").click(function(){
					if($(".input1").val() == 10){
						$(".input1").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$(".sundarinc").hide(0);
						$(".sundarcor").show(0);
						$nextBtn.show(0);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
					}
					else{
						$(".input1").addClass("ole-template-input-box-default-incorrect");
						$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
						$(".sundarcor").hide(0);
						$(".sundarinc").show(0);
							createjs.Sound.stop();
						play_correct_incorrect_sound(0);
					}
				});
			break;
			case 5:
	    		sound_player("s1_p"+(countNext+1),0);
				$nextBtn.hide(0);
				$(".digit_box_2").keypress(function (e) {
			 		if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			 			return false;
			 		}
			 		if(e.which == 13) {
				        $(".submitbtn2").trigger("click");
					}
			 	});
			 	$(".submitbtn2").click(function(){
					if($(".box_numerator").val() == 1 && $(".box_denominator").val() == 10){
						$(".box_numerator").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".box_denominator").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct").blur();
						$(".submitbtn2").removeClass("ole-template-check-btn-default-incorrect").addClass("ole-template-check-btn-default-correct");
						$nextBtn.show(0);
							createjs.Sound.stop();
						play_correct_incorrect_sound(1);
					}
					else{
						if($(".box_numerator").val() == 1){
							$(".box_numerator").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						}
						else{
							$(".box_numerator").addClass("ole-template-input-box-default-incorrect");
							$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
						if($(".box_denominator").val() == 10){
							$(".box_denominator").removeClass("ole-template-input-box-default-incorrect").addClass("ole-template-input-box-default-correct");
						}
						else{
							$(".box_denominator").addClass("ole-template-input-box-default-incorrect");
							$(".submitbtn2").addClass("ole-template-check-btn-default-incorrect");
								createjs.Sound.stop();
							play_correct_incorrect_sound(0);
						}
					}
				});
			break;
			case 7:
	    		sound_player("s1_p"+(countNext+1),0);
			$nextBtn.hide(0);
			$(".btnblock > p").click(function(){
	    		if($(this).hasClass("correct")){
	    			$(this).addClass("ole-template-check-btn-default-correct");
	    			$(this).siblings().fadeOut();
					$nextBtn.show(0);
						createjs.Sound.stop();
					play_correct_incorrect_sound(1);
	    		}
	    		else{
	    			$(this).addClass("ole-template-check-btn-default-incorrect");
							createjs.Sound.stop();
					play_correct_incorrect_sound(0);
	    		}
	    	});
	    	break;
	    	case 10:
	    		sound_player("s1_p"+(countNext+1),1);
				$nextBtn.hide(0).delay(3000).show(0);
	    	break;
	    	default:
	    		sound_player("s1_p"+(countNext+1),1);
	    	break;
		}
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			next?navigationcontroller():"";
		});
	}
	function templateCaller(){
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		if(countNext == 3){
			$flexshifter = $(".flexcontainerblock100").children().clone();
		}
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
	total_page = content.length;
	// templateCaller();
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/
    /*===== This function splits the string in data into convential fraction used in mathematics =====*/
    function splitintofractions($splitinside){
   		typeof $splitinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
       	if($splitintofractions.length > 0){
       		$.each($splitintofractions, function(index, value){
	        	$this = $(this);
	        	var tobesplitfraction = $this.html();
	        	if($this.hasClass('fraction')){
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
	        	}else{
	        		tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
		        	tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
	        	}


				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');
	        	$this.html(tobesplitfraction);
	        });
       	}
   	}
   	/*===== split into fractions end =====*/

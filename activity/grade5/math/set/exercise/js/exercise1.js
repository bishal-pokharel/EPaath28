Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[

	{
	//slide 1
		uppertextblock:[
		{
			textclass : "centertitle",
			//textdata : data.string.ex1mainques1,
		},
		{
			textclass : "clicksug",
			textdata : data.string.cotca,
		}
		],
		quesHidAnsBlock:[
		{
			questioncontainer: "forhover",
			answercontainer: "forclick",

			chilrenadditionalclass1: "incorrect",
			//question1: data.string.ex1ques1_1,
			//answer1: data.string.ex1ans1_1,

			chilrenadditionalclass2: "incorrect",
			//question2: data.string.ex1ques1_2,
			//answer2: data.string.ex1ans1_2,

			chilrenadditionalclass3: "incorrect",
			//question3: data.string.ex1ques1_3,
			//answer3: data.string.ex1ans1_3,

			chilrenadditionalclass4: "correct",
			//question4: data.string.ex1ques1_4,
			//answer4: data.string.ex1ans1_4,
		}
		]

		},

];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
  var preload;
  var timeoutvar = null;
  var current_sound;


  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
      // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
      //   ,
      //images
      // {id: "bg01", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},

      // soundsicon-orange
      {id: "s1_p1", src: soundAsset+"exe.ogg"},

    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }
  function handleFileLoad(event) {
    // console.log(event.item);
  }
  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded*100)+'%');
  }
  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }
  //initialize
  init();
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	 var quesSet = [
	 	["ex1mainques1","ex1ques1_1","ex1ques1_2","ex1ques1_3","ex1ques1_4","ex1ans1_1","ex1ans1_2","ex1ans1_3","ex1ans1_4"],
	 	["ex1mainques2","ex1ques2_1","ex1ques2_2","ex1ques2_3","ex1ques2_4","ex1ans2_1","ex1ans2_2","ex1ans2_3","ex1ans2_4"],
	 	["ex1mainques3","ex1ques3_1","ex1ques3_2","ex1ques3_3","ex1ques3_4","ex1ans3_1","ex1ans3_2","ex1ans3_3","ex1ans3_4"],
	 	["ex1mainques4","ex1ques4_1","ex1ques4_2","ex1ques4_3","ex1ques4_4","ex1ans4_1","ex1ans4_2","ex1ans4_3","ex1ans4_4"],
	 	["ex1mainques5","ex1ques5_1","ex1ques5_2","ex1ques5_3","ex1ques5_4","ex1ans5_1","ex1ans5_2","ex1ans5_3","ex1ans5_4"],
	 	["ex1mainques6","ex1ques6_1","ex1ques6_2","ex1ques6_3","ex1ques6_4","ex1ans6_1","ex1ans6_2","ex1ans6_3","ex1ans6_4"],
	 	["ex1mainques7","ex1ques7_1","ex1ques7_2","ex1ques7_3","ex1ques7_4","ex1ans7_1","ex1ans7_2","ex1ans7_3","ex1ans7_4"],
	 	["ex1mainques8","ex1ques8_1","ex1ques8_2","ex1ques8_3","ex1ques8_4","ex1ans8_1","ex1ans8_2","ex1ans8_3","ex1ans8_4"],
	 	["ex1mainques9","ex1ques9_1","ex1ques9_2","ex1ques9_3","ex1ques9_4","ex1ans9_1","ex1ans9_2","ex1ans9_3","ex1ans9_4"],
	 	["ex1mainques10","ex1ques10_1","ex1ques10_2","ex1ques10_3","ex1ques10_4","ex1ans10_1","ex1ans10_2","ex1ans10_3","ex1ans10_4"],
	 ];

	 quesSet.shufflearray();
	 var quCounter = 0;

	var testin = new LampTemplate();

 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[0]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
    if(countNext==0){
      sound_player("s1_p1");
    }
		var ansClicked = false;
		var wrngClicked = false;

		if(quCounter<10){
			var qu = "data.string." + quesSet[quCounter][0];
			var qu1 = "data.string." + quesSet[quCounter][1];
			var qu2 = "data.string." + quesSet[quCounter][2];
			var qu3 = "data.string." + quesSet[quCounter][3];
			var qu4 = "data.string." + quesSet[quCounter][4];

			var ans1 = "data.string." + quesSet[quCounter][5];
			var ans2 = "data.string." + quesSet[quCounter][6];
			var ans3 = "data.string." + quesSet[quCounter][7];
			var ans4 = "data.string." + quesSet[quCounter][8];

			quCounter++;

			$(".centertitle").html(eval(qu));

			$(".children:nth-of-type(1) > .forhover").html(eval(qu1));
			$(".children:nth-of-type(2) > .forhover").html(eval(qu2));
			$(".children:nth-of-type(3) > .forhover").html(eval(qu3));
			$(".children:nth-of-type(4) > .forhover").html(eval(qu4));

			$(".children:nth-of-type(1) > .forclick").html(eval(ans1));
			$(".children:nth-of-type(2) > .forclick").html(eval(ans2));
			$(".children:nth-of-type(3) > .forclick").html(eval(ans3));
			$(".children:nth-of-type(4) > .forclick").html(eval(ans4));
		}


		/*new exe */
			var corFlag = false;
			var parent = $(".parentcontainer");
			var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}

			$(".children > p:nth-of-type(1)").one("click", function(){
				if(corFlag == false){
				$(this).parent().css("visibility","visible");
				$(this).siblings("p").hide(0).delay(500).fadeIn("slow");
				$(this).removeClass("forhover");

				if($(this).parent().hasClass("correct")){
	 				testin.update(true);
          countNext==0?createjs.Sound.stop():'';
					play_correct_incorrect_sound(1);
					$(this).addClass("childcorr");
					$(this).parent().addClass("parcorr");
					corFlag = true;
					setTimeout(function(){
							$(".incorrect").addClass("parinc").css("visibility","visible");
							$(".incorrect > .forhover").addClass("childinc").removeClass("forhover");
							navigationcontroller();
							$(".incorrect > .forclick").hide(0).delay(500).fadeIn("slow").removeClass("forclick");
							$nextBtn.show(0);
					}, 2000);
				}
				else{
	 					testin.update(false);
            countNext==0?createjs.Sound.stop():'';
						play_correct_incorrect_sound();
						$(this).addClass("childinc");
						$(this).parent().addClass("parinc");
						$(this).siblings().removeClass("forclick");
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}
  function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

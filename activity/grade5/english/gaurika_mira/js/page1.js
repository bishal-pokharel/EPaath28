var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgcover",
        uppertextblockadditionalclass:"chapterdiv",
        uppertextblock: [
            {
                textclass: "chapter centertext1",
                textdata: data.string.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls coverpageimg",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv1 zoomInEffect",
        uppertextblock: [
            {
                textclass: "content firsttext",
                textdata: data.string.p1text1
            },
            {
                textclass: "content3 secondtext",
                textdata: data.string.note
            }

        ]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv fadeInEffect",
        uppertextblock: [
            {
                textclass: "chapter textpadding",
                textdata: data.string.gaurika
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "gaurikaonly",
                    imgclass: "relativecls gaurikaonlyimg",
                    imgid: 'gaurikaonlyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv slideL",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p1text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls gaurikaimg",
                    imgid: 'gaurikaImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv1",
        uppertextblock: [
            {
                textclass: "content1 slideT centertext",
                textdata: data.string.p1text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv2",
                    imgclass: "relativecls gaurika1img",
                    imgid: 'gaurika1Img',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideR",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p1text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls olympicstadiumimg",
                    imgid: 'olympicstadiumImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideL",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p1text5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls olympicringsimg",
                    imgid: 'olympicringsImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideL",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p1text6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls gaurika3img",
                    imgid: 'gaurika3Img',
                    imgsrc: ""
                },

            ]
        }]
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "coverpage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gaurikaImg", src: imgpath + "Gaurika-Singh02.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "gaurika1Img", src: imgpath + "Gaurika-Singh01.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "olympicstadiumImg", src: imgpath + "olympicstadium.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "olympicringsImg", src: imgpath + "olympicrings.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "gaurika3Img", src: imgpath + "Gaurika-Singh03.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "gaurikamomImg", src: imgpath + "Gaurika-garima01.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "nepalearthquakeImg", src: imgpath + "nepalearthquake.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "gaurikaonlyImg", src: imgpath + "gaurika_singh.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "p1_s0.ogg"},
            {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                setTimeout(function(){
                    sound_player("sound_1",true);
                },1000);
                break;
            case 2:
                setTimeout(function(){
                  sound_player("sound_2",true);
                },1000);
                break;
            case 3:
                setTimeout(function(){
                  sound_player("sound_3",true);
                },1000);
                break;
            case 4:
                    setTimeout(function(){
                         sound_player("sound_4",true);
                    },1000);
                break;
            case 5:
                setTimeout(function(){
                  sound_player("sound_5",true);
                },1000);
                break;
            case 6:
                setTimeout(function(){
                   sound_player("sound_6",true);
                },1000);
                break;
            case 7:
                    setTimeout(function(){
                      sound_player("sound_7",true);
                    },1000);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});

var imgpath = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "title centertext1",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls pasangimg",
                    imgid: 'pasangImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.anuradha
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.rajesh
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.pasang
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.paras
            }

        ],
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls pasangimg",
                    imgid: 'pasangImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.fame2
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame4
            }

        ],
    },
// slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls pasangimg",
                    imgid: 'pasangImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls anuradhaimg",
                    imgid: 'anuradhaImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.pasang
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.mahabir
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.anuradha
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.jhamak
            }

        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls anuradhaimg",
                    imgid: 'anuradhaImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.fame4
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.fame5
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame6
            }

        ],
    },
// slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls anuradhaimg",
                    imgid: 'anuradhaImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mahabirimg",
                    imgid: 'mahabirImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.paras
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.pasang
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.mahabir
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.rajesh
            }

        ],
    },
    //slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mahabirimg",
                    imgid: 'mahabirImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame6
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.fame4
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame5
            }

        ],
    },
// slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mahabirImg",
                    imgid: 'mahabirImg',
                    imgsrc: ""
                },
            ]
        }],
    },

//slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls parasimg",
                    imgid: 'parasImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.rajesh
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "diyfont centertext",
                textdata: data.string.paras
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.jhamak
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.mahabir
            }

        ],
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls parasimg",
                    imgid: 'parasImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame2
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.fame5
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame6
            }

        ],
    },
// slide12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls parasimg",
                    imgid: 'parasImg',
                    imgsrc: ""
                },
            ]
        }],
    },

    //slide13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls rajeshimg",
                    imgid: 'rajeshImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1 correct",
                textclass: "diyfont centertext",
                textdata: data.string.rajesh
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.paras
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.pasang
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.mahabir
            }

        ],
    },
    //slide14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls rajeshimg",
                    imgid: 'rajeshImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.fame6
            },
            {
                textdiv:"commonbtn option2 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option3",
                textclass: "diyfont centertext",
                textdata: data.string.fame2
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame4
            }

        ],
    },
// slide15
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls rajeshimg",
                    imgid: 'rajeshImg',
                    imgsrc: ""
                },
            ]
        }],
    },


    //slide16
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls jhamakimg",
                    imgid: 'jhamakImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.pasang
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.rajesh
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.jhamak
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.anuradha
            }

        ],
    },
    //slide17
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls jhamakimg",
                    imgid: 'jhamakImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.question2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "diyfont centertext",
                textdata: data.string.fame3
            },
            {
                textdiv:"commonbtn option2",
                textclass: "diyfont centertext",
                textdata: data.string.fame4
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "diyfont centertext",
                textdata: data.string.fame5
            },
            {
                textdiv:"commonbtn option4",
                textclass: "diyfont centertext",
                textdata: data.string.fame6
            }

        ],
    },
// slide18
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"descdiv",
        upperextradiv:"centertext",
        uppertextblock: [
            {
                textclass: "title description1",
                textdata: data.string.correct
            },
            {
                textclass: "diyfont fadeInEffect",
                textdata: data.string.p2text6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls jhamakImg",
                    imgid: 'jhamakImg',
                    imgsrc: ""
                },
            ]
        }],
    },

];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath + "bg_diy.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pasangImg", src: imgpath + "pasanglhamusherpa.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "anuradhaImg", src: imgpath + "anuradhakoirala.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "jhamakImg", src: imgpath + "jhamakkumarighimire.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mahabirImg", src: imgpath + "mahabirpun.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "parasImg", src: imgpath + "paraskhadka01.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "rajeshImg", src: imgpath + "rajesh_hamal.jpg", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "p2_s0.ogg"},
            {id: "sound_pasang", src: soundAsset + "p2_Pasang.ogg"},
            {id: "sound_anuradha", src: soundAsset + "p2_Anuradha.ogg"},
            {id: "sound_mahabir", src: soundAsset + "p2_Mahabir.ogg"},
            {id: "sound_paras", src: soundAsset + "p2_Paras.ogg"},
            {id: "sound_rajesh", src: soundAsset + "p2_Rajesh.ogg"},
            {id: "sound_jhamak", src: soundAsset + "p2_jhamak.ogg"},
            {id: "sound_hfam", src: soundAsset + "he_is_famous.ogg"},
            {id: "sound_sfam", src: soundAsset + "She_is_famous.ogg"},
            {id: "sound_cyg", src: soundAsset + "Can_you_guess.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                sound_player("sound_cyg",false);
                shufflehint(data.string.pasang);
                checkans();
                break;
            case 2:
                sound_player("sound_sfam",false);
                shufflehint(data.string.fame1);
                checkans();
                break;
            case 3:
                setTimeout(function(){
                    sound_player("sound_pasang",true);
                });
                break;
            case 4:
                sound_player("sound_cyg",false);
                shufflehint(data.string.anuradha);
                checkans();
                break;
            case 5:
                sound_player("sound_sfam",false);
                shufflehint(data.string.fame4);
                checkans();
                break;
            case 6:
                setTimeout(function(){
                    sound_player("sound_anuradha",true);
                });
                break;
            case 7:
                sound_player("sound_cyg",false);
                shufflehint(data.string.mahabir);
                checkans();
                break;
            case 8:
                sound_player("sound_hfam",false);
                shufflehint(data.string.fame6);
                checkans();
                break;
            case 9:
                setTimeout(function(){
                    sound_player("sound_mahabir",true);
                });
                break;
            case 10:
                sound_player("sound_cyg",false);
                shufflehint(data.string.paras);
                checkans();
                break;
            case 11:
                sound_player("sound_hfam",false);
                shufflehint(data.string.fame2);
                checkans();
                break;
            case 12:
                setTimeout(function(){
                    sound_player("sound_paras",true);
                });
                break;
            case 13:
                sound_player("sound_cyg",false);
                shufflehint(data.string.rajesh);
                checkans();
                break;
            case 14:
                sound_player("sound_hfam",false);
                shufflehint(data.string.fame3);
                checkans();
                break;
            case 15:
                setTimeout(function(){
                    sound_player("sound_rajesh",true);
                });
                break;
            case 16:
                sound_player("sound_cyg",false);
                shufflehint(data.string.jhamak);
                checkans();
                break;
            case 17:
                sound_player("sound_sfam",false);
                shufflehint(data.string.fame5);
                checkans();
                break;
            case 18:
                setTimeout(function(){
                    sound_player("sound_jhamak",true);
                });
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg(){
        $(".div2,.imgdivright,.div4").hide(0);
        setTimeout(function(){
            $(".div2,.imgdivright,.div4").show(0);
        },2500);
    }
    function shufflehint(correctans) {
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans){
                $this.addClass("correct")
            }
        });

    }
    function checkans(){
        $(".commonbtn ").on("click",function () {
           createjs.Sound.stop();
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
       });
    }

});

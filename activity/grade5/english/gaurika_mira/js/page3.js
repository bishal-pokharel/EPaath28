var imgpath = $ref + "/images/mirarai/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass: "objdiv fadeInEffect",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.mira
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "miraonly",
                    imgclass: "relativecls miraonlyimg",
                    imgid: 'miraonlyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv slideL",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text1
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls miraraiimg",
                    imgid: 'miraraiImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv slideR",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text2
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mirarai01img",
                    imgid: 'mirarai01Img',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        textblock: [
            {
                textdiv:"div1 slideT1 borderright",
                textclass: "content centertext",
                textdata: data.string.p3text3
            },
            {
                textdiv:"div2 slideT1 borderleft",
                textclass: "content centertext",
                textdata: data.string.p3text4
            },
            {
                textdiv:"div3 slideL borderright",
                textclass: "content2 centertext1",
                textdata: data.string.p3text5
            },
            {
                textdiv:"div4 slideR borderleft",
                textclass: "content2 centertext1",
                textdata: data.string.p3text6
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdivleft borderright slideT1",
                    imgclass: "relativecls trailimg",
                    imgid: 'trailImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "imgdivright imgdivright1 borderleft slideT1",
                    imgclass: "relativecls skymarathonimg",
                    imgid: 'skymarathonImg',
                    imgsrc: ""
                },


            ]
        }]
    },
//slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv slideR",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text7
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mirarai02img",
                    imgid: 'mirarai02Img',
                    imgsrc: ""
                },

            ]
        }]
    },

//slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideL",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text8
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgd1",
                    imgclass: "logo_NGSimg",
                    imgid: 'logo_NGSImg',
                    imgsrc: ""
                },

            ]
        }]
    },
//slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideR",
        uppertextblock: [
            {
                textclass: "content2 centertext",
                textdata: data.string.p3text9
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgd2",
                    imgclass: "Gonelineimg",
                    imgid: 'GonelineImg',
                    imgsrc: ""
                },

            ]
        }]
    },
//slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv slideR",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p3text10
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mira_rai_newimg",
                    imgid: 'mira_rai_newImg',
                    imgsrc: ""
                },

            ]
        }]
    },
 //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideR",
        uppertextblock: [
            {
                textclass: "content1 centertext",
                textdata: data.string.p3text11
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mirarai04img",
                    imgid: 'mirarai04Img',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv1",
        uppertextblock: [
            {
                textclass: "content slideB centertext",
                textdata: data.string.p3text12
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdivright",
                    imgclass: "relativecls mirarai05img",
                    imgid: 'mirarai05Img',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideL",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text13
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mirarai06img",
                    imgid: 'mirarai06Img',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass:"contentdiv height slideR",
        uppertextblock: [
            {
                textclass: "content centertext",
                textdata: data.string.p3text14
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls mirarai07img",
                    imgid: 'mirarai07Img',
                    imgsrc: ""
                },

            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "coverpage.png", type: createjs.AbstractLoader.IMAGE},
            {id: "miraraiImg", src: imgpath + "mirarai.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai01Img", src: imgpath + "mirarai01.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai02Img", src: imgpath + "mirarai02.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai03Img", src: imgpath + "mirarai03.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai04Img", src: imgpath + "mirarai04.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai05Img", src: imgpath + "mirarai05.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai06Img", src: imgpath + "mirarai06.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai07Img", src: imgpath + "mirarai07.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mirarai08Img", src: imgpath + "mirarai08.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "skymarathonImg", src: imgpath + "skymarathon.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "trailImg", src: imgpath + "trail.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "logo_NGSImg", src: imgpath + "logo_NGS.png", type: createjs.AbstractLoader.IMAGE},
            {id: "GonelineImg", src: imgpath + "Goneline.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "mira_rai_newImg", src: imgpath + "mira_rai_new.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "miraonlyImg", src: imgpath + "mira_rai.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset + "p3_s0.ogg"},
            {id: "sound_1", src: soundAsset + "p3_s1.ogg"},
            {id: "sound_2", src: soundAsset + "p3_s2.ogg"},
            {id: "sound_3_0", src: soundAsset + "p3_s3_0.ogg"},
            {id: "sound_3_1", src: soundAsset + "p3_s3_1.ogg"},
            {id: "sound_4", src: soundAsset + "p3_s4.ogg"},
            {id: "sound_5", src: soundAsset + "p3_s5.ogg"},
            {id: "sound_6", src: soundAsset + "p3_s6.ogg"},
            {id: "sound_7", src: soundAsset + "p3_s7.ogg"},
            {id: "sound_8", src: soundAsset + "p3_s8.ogg"},
            {id: "sound_9", src: soundAsset + "p3_s9.ogg"},
            {id: "sound_10", src: soundAsset + "p3_s10.ogg"},
            {id: "sound_11", src: soundAsset + "p3_s11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                sound_player("sound_0",true);
                break;
            case 1:
                setTimeout(function(){
                    sound_player("sound_1",true);
                },1000);
                break;
            case 2:
                setTimeout(function(){
                    sound_player("sound_2",true);
                },1000);
                break;
            case 3:
                animateImg();
                navigationcontroller(countNext, $total_page);
                break;
            case 4:
                setTimeout(function(){
                    sound_player("sound_4",true);
                },1000);
                break;
            case 5:
                setTimeout(function(){
                    sound_player("sound_5",true);
                },1000);
                break;
            case 6:
                setTimeout(function(){
                    sound_player("sound_6",true);
                },1000);
                break;
            case 7:
                setTimeout(function(){
                    sound_player("sound_7",true);
                },1000);
                break;
            case 8:
                setTimeout(function(){
                    sound_player("sound_8",true);
                },1000);
                break;
            case 9:
                setTimeout(function(){
                    sound_player("sound_9",true);
                },1000);
                break;
            case 10:
                setTimeout(function(){
                    sound_player("sound_10",true);
                },1000);
                break;
            case 11:
                setTimeout(function(){
                    sound_player("sound_11",true);
                },1000);
                break;
            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg(){
        $(".div2,.imgdivright,.div4").hide(0);
        setTimeout(function(){
            $(".div2,.imgdivright,.div4").show(0);
            sound_player("sound_3_0",false);
            setTimeout(function(){
                sound_player("sound_3_1",true);
            },10000)
        },2500);
    }

});

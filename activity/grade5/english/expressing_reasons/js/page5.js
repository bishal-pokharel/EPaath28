var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var balloon_src = [
					"images/ex_balloon/balloon01.png",
					"images/ex_balloon/balloon02.png",
					"images/ex_balloon/balloon03.png",
					"images/ex_balloon/balloon04.png",
					"images/ex_balloon/balloon05.png",
					"images/ex_balloon/balloon06.png",
				];
var pop_balloon_src = "images/ex_balloon/pop.png";
var pop_star_src = "images/star_1.gif";
var pop_sound = new buzz.sound(('sounds/common/balloon-pop.ogg'));


var sound_1 = new buzz.sound((soundAsset + "p5_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p5_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p5_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p5_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p5_s6.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p5_s8.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p5_s10.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p5_s11.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 'sp-1 my_font_big',
			textdata : data.string.p5text1,
			textclass : 'my_font_very_big sniglet',
			imgsrc: 'images/textbox/white/tl-2.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "squirrel",
					imgsrc : imgpath + "squirrel.png",
				}
			],
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		extratextblock:[{
			textdata: data.string.p5text2,
			textclass: 'topic-story my_font_super_big comingsoon'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-topic-1",
					imgsrc : imgpath + "story/rat.png",
				}
			],
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text3,
			textclass: ''
		},{
			textdata: data.string.p5text4,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/queen.png",
				}
			],
		}]
	},
	//slide 3	
	{	
		contentblockadditionalclass: 'bg-lblue',
		exerciseblock: [
			{
				instructiondata : data.string.p5text20,
				instructionclass : 'my_font_medium luckiestguy',
				
				questionclass: 'my_font_big',
				questiondata: data.string.p5text5,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1 my_font_small shortstack",
						optiondata: data.string.p5text5a,
					},
					{
						mc_class: "class2 my_font_small shortstack",
						optiondata: data.string.p5text5b,
					},
					{
						mc_class: "class3 my_font_small shortstack",
						optiondata: data.string.p5text5c,
					},
					{
						mc_class: "class4 my_font_small shortstack",
						optiondata: data.string.p5text5d,
					}],
			} 
		] 
	},
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text6,
			textclass: ''
		},{
			textdata: data.string.p5text7,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/sun.png",
				}
			],
		}]
	},
	//slide 5
	{	
		contentblockadditionalclass: 'bg-lblue',
		exerciseblock: [
			{
				instructiondata : data.string.p5text20,
				instructionclass : 'my_font_medium luckiestguy',
				
				questionclass: 'my_font_big',
				questiondata: data.string.p5text8,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1 my_font_small shortstack",
						optiondata: data.string.p5text8a,
					},
					{
						mc_class: "class2 my_font_small shortstack",
						optiondata: data.string.p5text8b,
					},
					{
						mc_class: "class3 my_font_small shortstack",
						optiondata: data.string.p5text8c,
					},
					{
						mc_class: "class4 my_font_small shortstack",
						optiondata: data.string.p5text8d,
					}],
			} 
		] 
	},
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text9,
			textclass: ''
		},{
			textdata: data.string.p5text10,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/cloud.png",
				}
			],
		}]
	},
	//slide 7
	{	
		contentblockadditionalclass: 'bg-lblue',
		exerciseblock: [
			{
				instructiondata : data.string.p5text20,
				instructionclass : 'my_font_medium luckiestguy',
				
				questionclass: 'my_font_big',
				questiondata: data.string.p5text11,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1 my_font_small shortstack",
						optiondata: data.string.p5text11a,
					},
					{
						mc_class: "class2 my_font_small shortstack",
						optiondata: data.string.p5text11b,
					},
					{
						mc_class: "class3 my_font_small shortstack",
						optiondata: data.string.p5text11c,
					},
					{
						mc_class: "class4 my_font_small shortstack",
						optiondata: data.string.p5text11d,
					}],
			} 
		] 
	},
	
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text12,
			textclass: ''
		},{
			textdata: data.string.p5text13,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/wind.png",
				}
			],
		}]
	},
	//slide 9
	{	
		contentblockadditionalclass: 'bg-lblue',
		exerciseblock: [
			{
				instructiondata : data.string.p5text20,
				instructionclass : 'my_font_medium luckiestguy',
				
				questionclass: 'my_font_big',
				questiondata: data.string.p5text14,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1 my_font_small shortstack",
						optiondata: data.string.p5text14a,
					},
					{
						mc_class: "class2 my_font_small shortstack",
						optiondata: data.string.p5text14b,
					},
					{
						mc_class: "class3 my_font_small shortstack",
						optiondata: data.string.p5text14c,
					},
					{
						mc_class: "class4 my_font_small shortstack",
						optiondata: data.string.p5text14d,
					}],
			} 
		] 
	},
	
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text15,
			textclass: ''
		},{
			textdata: data.string.p5text16,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/grass.png",
				}
			],
		}]
	},
	
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-lblue',
		
		uppertextblockadditionalclass: 'text-story my_font_medum comingsoon',
		uppertextblock:[{
			textdata: data.string.p5text17,
			textclass: ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "img-story",
					imgsrc : imgpath + "story/rat-marry-with-rat.png",
				}
			],
		}]
	},
	//slide12
	{	
		contentblockadditionalclass: 'bg-lblue',
		exerciseblock: [
			{
				instructiondata : data.string.p5text20,
				instructionclass : 'my_font_medium luckiestguy',
				
				questionclass: 'my_font_big',
				questiondata: data.string.p5text18,
				datahighlightflag: true,
				datahighlightcustomclass: 'blank-space',
				
				option: [
					{
						mc_class: "class1 my_font_small shortstack",
						optiondata: data.string.p5text18a,
					},
					{
						mc_class: "class2 my_font_small shortstack",
						optiondata: data.string.p5text18b,
					},
					{
						mc_class: "class3 my_font_small shortstack",
						optiondata: data.string.p5text18c,
					},
					{
						mc_class: "class4 my_font_small shortstack",
						optiondata: data.string.p5text18d,
					}],
			} 
		] 
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var myTimeout1 =  null;
	var timeouts = [];
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				sound_nav(sound_1);
				break;
			case 1:
				$prevBtn.show(0);
				sound_nav(sound_2);
				break;
			case 2:
				$prevBtn.show(0);
				sound_nav(sound_3);
				break;
			case 4:
				$prevBtn.show(0);
				sound_nav(sound_4);
				break;
			case 6:
				$prevBtn.show(0);
				sound_nav(sound_5);
				break;
			case 8:
				$prevBtn.show(0);
				sound_nav(sound_6);
				break;
			case 10:
				$prevBtn.show(0);
				sound_nav(sound_7);
				break;
			case 11:
				$prevBtn.show(0);
				sound_nav(sound_8);
				break;
			case 3:
			case 5:
			case 7:
			case 9:
			case 12:
				$prevBtn.show(0);
				balloon_src.shufflearray();
				$('.class1>.balloon-icon').attr('src', balloon_src[0]);
				$('.class2>.balloon-icon').attr('src', balloon_src[1]);
				$('.class3>.balloon-icon').attr('src', balloon_src[2]);
				$('.class4>.balloon-icon').attr('src', balloon_src[3]);

				var option_position = [1,2,3,4];
				option_position.shufflearray();
				for(var op=0; op<4; op++){
					$('.main-container').eq(op).addClass('option_'+option_position[op]);
				}
				var wrong_clicked = false;
				
				$(".main-container").click(function(){
					if($(this).hasClass("class1")){
						var $textballoon =  $(this).children('p');
						var box_left = ( $('.question').position().left + $('.blank-space').position().left )*100/$board.width() +'%';
						var box_top = ($('.question').position().top + $('.blank-space').position().top + $('.blank-space').height()/2)*100/$board.height() +'%';
						
						var init_left = ($(this).position().left + $textballoon.position().left)*100/$board.width() +'%';
						var init_top = ($(this).position().top + $textballoon.position().top)*100/$board.height() +'%';
						
						$(this).children('p').detach().css({
							'left': init_left,
							'top': init_top,
							'z-index': '100',
							'width': '14%',
							'color': '#8E7CC3',
						}).appendTo('.contentblock');
						$textballoon.animate({
							'left': box_left,
							'top': box_top,
						}, 1000, function(){
							$textballoon.hide(0);
							$('.blank-space').css({
								'width': 'auto',
								'display': 'inline'
							});
							$('.blank-space').html($textballoon.html());
							play_correct_incorrect_sound(1);
							nav_button_controls(0);
						});
						
						$(".main-container").css('pointer-events', 'none');
						pop_sound.play();
						
						var $balloon = $(this);
						var dt = new Date();
						$(this).children('.balloon-icon').css({
							'z-index': 100,
							'height': '40%',
							'top': '20%',
							'width': '100%'}).attr('src', pop_star_src+'?' + dt.getTime());
						$(this).fadeOut(600, function(){
						});
					}
					else{
						$(this).css('transform', 'scale(0.8)');
						$(this).children('.incorrect-icon').show(0);
						play_correct_incorrect_sound(0);
					}
				}); 
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

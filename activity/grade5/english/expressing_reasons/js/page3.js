var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";


var sound_1 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p3_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p3_s8.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p3_s9.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p3_s10.ogg"));

var sound_c_1 = new buzz.sound((soundAsset + "healthy.ogg"));
var sound_c_2 = new buzz.sound((soundAsset + "proffesional.ogg"));
var sound_c_3 = new buzz.sound((soundAsset + "friends.ogg"));

var sound_choices = [sound_c_1, sound_c_2, sound_c_3];
var sound_arr_var = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11];

var image_choices = [imgpath+'asha-playing-football.png', imgpath+'asha-playing-football.png', imgpath+'asha-playing.png'];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		extratextblock: [{
			textclass: 'btext-1 my_font_very_big schoolbell',
			textdata: data.string.p3text1
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				}
			],
		}]
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		extratextblock: [{
			textclass: 'abt-1  schoolbell my_font_very_big',
			textdata: data.string.p3text2,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'abt-2 aligned-left  schoolbell my_font_very_big',
			textdata: data.string.p3text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "playing-image",
					imgsrc : imgpath + "asha-playing.png",
				}
			],
		}]
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		uppertextblockadditionalclass: 'boardtext-2 schoolbell my_font_very_big',
		uppertextblock: [{
			textclass: 'text-skyblue',
			textdata: data.string.p3text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'aligned-left margin-5-15',
			textdata: data.string.p3text5,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		},{
			textclass: 'bt-2 aligned-left',
			textdata: data.string.p3text6,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}], 
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				}
			],
		}]
	},
	
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		extratextblock:[{
			textclass: 'bt-answer my_font_big schoolbell',
			textdata: data.string.p3text10,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		uppertextblockadditionalclass: 'boardtext-2 schoolbell my_font_very_big',
		uppertextblock: [{
			textclass: 'text-skyblue',
			textdata: data.string.p3text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'aligned-left margin-5-15',
			textdata: data.string.p3text8,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}], 
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				}
			],
		}]
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		extratextblock:[{
			textclass: 'bt-answer my_font_big schoolbell',
			textdata: data.string.p3text10,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		uppertextblockadditionalclass: 'boardtext-2 schoolbell my_font_very_big',
		uppertextblock: [{
			textclass: 'text-skyblue',
			textdata: data.string.p3text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'aligned-left',
			textdata: data.string.p3text11,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}], 
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				}
			],
		}]
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		extratextblock:[{
			textclass: 'bt-answer my_font_big schoolbell',
			textdata: data.string.p3text10,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		uppertextblockadditionalclass: 'boardtext-2 schoolbell my_font_very_big',
		uppertextblock: [{
			textclass: 'text-skyblue',
			textdata: data.string.p3text4,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'aligned-left',
			textdata: data.string.p3text11,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				}
			],
		}],
		listblock : [{
			listblockadditionalclass: 'list-select my_font_big schoolbell',
			headerdata : data.string.p3text13,
			headerclass : 'ul happymonkey',
			textdata : data.string.p3text14,
			textclass : 'happymonkey',
			listitem: [
				{
					textclass: 'list-item t-1',
					textdata: data.string.p3text15
				},
				{
					textclass: 'list-item t-2',
					textdata: data.string.p3text16
				},
				{
					textclass: 'list-item t-3',
					textdata: data.string.p3text17
				},
			],
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		uppertextblockadditionalclass: 'boardtext-2 schoolbell my_font_very_big',
		uppertextblock: [{
			textclass: '',
			textdata: data.string.p3text18,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'text-skyblue',
			textdata: data.string.p3text19,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}], 
		extratextblock:[{
			textclass: 'bt-answer my_font_big schoolbell reason-text-fill',
			textdata: data.string.p3text20,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "reason-image",
					imgsrc : imgpath + "duck.png",
				}
			],
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		extratextblock: [{
			textclass: 'bottext-1 my_font_very_big schoolbell',
			textdata: data.string.p3text21,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'bottext-2 my_font_very_big schoolbell',
			textdata: data.string.p3text3,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "exercising",
					imgsrc : imgpath + "exercising.png",
				}
			],
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		extratextblock: [{
			textclass: 'bottext-1 my_font_very_big schoolbell',
			textdata: data.string.p3text21,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'bottext-2 my_font_very_big schoolbell',
			textdata: data.string.p3text22,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "exercising",
					imgsrc : imgpath + "exercising.png",
				}
			],
		}]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		extratextblock: [{
			textclass: 'bottext-1 my_font_very_big schoolbell',
			textdata: data.string.p3text21,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'bottext-2 my_font_very_big schoolbell',
			textdata: data.string.p3text23,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "exercising",
					imgsrc : imgpath + "exercising.png",
				}
			],
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-blue',
		
		extratextblock: [{
			textclass: 'bottext-1 my_font_very_big schoolbell',
			textdata: data.string.p3text21,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textclass: 'bottext-2 my_font_very_big schoolbell',
			textdata: data.string.p3text24,
			datahighlightflag : true,
			datahighlightcustomclass : 'ul-full'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "greenboard",
					imgsrc : imgpath + "board.svg",
				},{
					imgclass : "duckling",
					imgsrc : imgpath + "duck.png",
				},{
					imgclass : "exercising",
					imgsrc : imgpath + "exercising.png",
				}
			],
		}]
	},
	//slide12
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-blue',
// 		
		// extratextblock: [{
			// textclass: 'bottext-1 my_font_very_big schoolbell',
			// textdata: data.string.p3text25,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul'
		// },{
			// textclass: 'bottext-2 my_font_very_big schoolbell',
			// textdata: data.string.p3text3,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul-full'
		// }],
// 		
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "greenboard",
					// imgsrc : imgpath + "board.svg",
				// },{
					// imgclass : "duckling",
					// imgsrc : imgpath + "duck.png",
				// }
			// ],
		// }]
	// },
	// //slide13
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-blue',
// 		
		// extratextblock: [{
			// textclass: 'bottext-1 my_font_very_big schoolbell',
			// textdata: data.string.p3text25,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul'
		// },{
			// textclass: 'bottext-2 my_font_very_big schoolbell',
			// textdata: data.string.p3text26,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul-full'
		// }],
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "greenboard",
					// imgsrc : imgpath + "board.svg",
				// },{
					// imgclass : "duckling",
					// imgsrc : imgpath + "duck.png",
				// }
			// ],
		// }]
	// },
	// //slide14
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-blue',
// 		
		// extratextblock: [{
			// textclass: 'bottext-1 my_font_very_big schoolbell',
			// textdata: data.string.p3text25,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul'
		// },{
			// textclass: 'bottext-2 my_font_very_big schoolbell',
			// textdata: data.string.p3text27,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul-full'
		// }],
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "greenboard",
					// imgsrc : imgpath + "board.svg",
				// },{
					// imgclass : "duckling",
					// imgsrc : imgpath + "duck.png",
				// }
			// ],
		// }]
	// },
	// //slide15
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-blue',
// 		
		// extratextblock: [{
			// textclass: 'bottext-1 my_font_very_big schoolbell',
			// textdata: data.string.p3text25,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul'
		// },{
			// textclass: 'bottext-2 my_font_very_big schoolbell',
			// textdata: data.string.p3text28,
			// datahighlightflag : true,
			// datahighlightcustomclass : 'ul-full'
		// }],
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "greenboard",
					// imgsrc : imgpath + "board.svg",
				// },{
					// imgclass : "duckling",
					// imgsrc : imgpath + "duck.png",
				// }
			// ],
		// }]
	// },
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var sound_stop_choice = sound_c_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var reason_text = '';
	var reason_image = image_choices[0];
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 5:
				$prevBtn.show(0);
				timeoutvar = setTimeout(function() {
					sound_player(sound_6);
				}, 1500);
				$('.list-item').click(function(){
					if(!$(this).hasClass('selected-item')){
						$('.list-item').removeClass('selected-item');
						$(this).addClass('selected-item');
						reason_text = $(this).html();
						var classname = $(this).attr('class');
						classname = parseInt(classname.replace(/\D/g, ''));
						sound_stop_choice = sound_choices[classname-1];
						reason_image = image_choices[classname-1];
						$nextBtn.show(0);
					}
				});
				break;
			case 6:
				$prevBtn.show(0);
				$('.reason-text-fill').html($('.reason-text-fill').html()+ ' ' + reason_text);
				$('.reason-image').attr('src', reason_image);
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_7;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					current_sound = sound_stop_choice;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						nav_button_controls(0);
					});
				});
				break;
			default:
				if(countNext>0){
					$prevBtn.show(0);
				}
				sound_nav(sound_arr_var[countNext]);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

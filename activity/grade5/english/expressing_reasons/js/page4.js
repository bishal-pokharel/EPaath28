var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_2 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_3 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_4 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_5 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_6 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_7 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_8 = new buzz.sound((soundAsset + "beep.ogg"));
var sound_beep = new buzz.sound((soundAsset + "beep_loud.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-4',
	
		uppertextblockadditionalclass: 'diy-title',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : 'sniglet'
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text2
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text2d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "boy-feeling-hungry.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text2a,
			dragclass1: '',
			dragdata2: data.string.p4text2b,
			dragclass2: '',
			dragdata3: data.string.p4text2c,
			dragclass3: '',
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text3
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text3d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "dress.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text3a,
			dragclass1: '',
			dragdata2: data.string.p4text3b,
			dragclass2: '',
			dragdata3: data.string.p4text3c,
			dragclass3: '',
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text4
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text4d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "fail-in-text.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text4a,
			dragclass1: '',
			dragdata2: data.string.p4text4b,
			dragclass2: '',
			dragdata3: data.string.p4text4c,
			dragclass3: '',
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text8
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text8d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "backhome.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text8a,
			dragclass1: '',
			dragdata2: data.string.p4text8b,
			dragclass2: '',
			dragdata3: data.string.p4text8c,
			dragclass3: '',
		}]
	},
	//slide5
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg-weepy-sky',
		// extratextblock: [{
			// textclass: 't-header proximanova my_font_medium',
			// textdata: data.string.p4text1
		// },{
			// textclass: 'topic-p4 sniglet',
			// textdata: data.string.p4text5
		// },{
			// textclass: 'final-ans my_font_big sniglet',
			// textdata: data.string.p4text5d
		// }],
// 
		// imageblock : [{
			// imagestoshow : [
				// {
					// imgclass : "diy-image",
					// imgsrc : imgpath + "buying-water.png",
				// },
			// ],
		// }],
		// dropblock: true,
		// dragblock:[{
			// dragdata1: data.string.p4text5a,
			// dragclass1: '',
			// dragdata2: data.string.p4text5b,
			// dragclass2: '',
			// dragdata3: data.string.p4text5c,
			// dragclass3: '',
		// }]
	// },
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text6
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text6d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "late-in-class.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text6a,
			dragclass1: '',
			dragdata2: data.string.p4text6b,
			dragclass2: '',
			dragdata3: data.string.p4text6c,
			dragclass3: '',
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-weepy-sky',
		extratextblock: [{
			textclass: 't-header proximanova my_font_medium',
			textdata: data.string.p4text1
		},{
			textclass: 'topic-p4 sniglet',
			textdata: data.string.p4text7
		},{
			textclass: 'final-ans my_font_big sniglet',
			textdata: data.string.p4text7d
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-image",
					imgsrc : imgpath + "study-hard.png",
				},
			],
		}],
		dropblock: true,
		dragblock:[{
			dragdata1: data.string.p4text7a,
			dragclass1: '',
			dragdata2: data.string.p4text7b,
			dragclass2: '',
			dragdata3: data.string.p4text7c,
			dragclass3: '',
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var item_count = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$nextBtn.show(0);
				break;
			default:
				$prevBtn.show(0);
				item_count = 0;
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.drag-'+i).addClass('pos-'+positions[i-1]);
				}
				$('.dragblock').draggable({
					containment: ".board",
					cursor: "move",
					revert: true,
					appendTo: "body",
					zIndex: 50,
					start: function( event, ui ){
						// $(this).addClass('selected');
					},
					stop: function( event, ui ){
						// $(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-1');	
					}
				});
				$('.drop-2').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-2');	
					}
				});
				$('.drop-3').droppable({
					drop:function(event, ui) {
						dropper(ui.draggable, $(this), 'drag-3');	
					}
				});
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}
	function dropper(dropped, droppedOn, hasclass){
		if(dropped.hasClass(hasclass)){
			dropped.draggable('option', 'revert', false);
			item_count++;
			dropped.draggable('disable');
			dropped.detach().css({
				'top': '50%',
				'left': '0%',
				'width': '100%'
			}).appendTo(droppedOn);
			if(item_count>2){
				$('.dropblock').fadeOut(500, function(){
					$('.final-ans').fadeIn(500, function(){
						nav_button_controls(0);
					});
				});
			}
		} else{
			sound_player(sound_beep);
		}
	}

	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

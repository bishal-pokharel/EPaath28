var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p1_s7_1.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p1_s7_2.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p1_s7_3.ogg"));
var sound_12 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_13 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_14 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var sound_15 = new buzz.sound((soundAsset + "p1_s10_1.ogg"));
var sound_16 = new buzz.sound((soundAsset + "p1_s10_2.ogg"));
var sound_17 = new buzz.sound((soundAsset + "p1_s10_3.ogg"));
var sound_18 = new buzz.sound((soundAsset + "p1_s11.ogg"));
var sound_19 = new buzz.sound((soundAsset + "p1_s12.ogg"));
var sound_20 = new buzz.sound((soundAsset + "p1_s13.ogg"));
var sound_21 = new buzz.sound((soundAsset + "p1_s13_1.ogg"));
var sound_22 = new buzz.sound((soundAsset + "p1_s13_2.ogg"));
var sound_23 = new buzz.sound((soundAsset + "p1_s13_3.ogg"));
var sound_24 = new buzz.sound((soundAsset + "p1_s14.ogg"));
var sound_25 = new buzz.sound((soundAsset + "p1_s15.ogg"));

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
	
		uppertextblockadditionalclass: 'lesson-title',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'chelseamarket'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "frontballoon",
					imgsrc : imgpath + "balloon.png",
				}
			],
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'title-1 my_font_ultra_big pangolin',
			textdata: data.string.p1text1
		}],
		uppertextblockadditionalclass: 'p1-text my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : 'p1-texta',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text3,
			textclass : 'p1-textb my_font_very_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text4,
			textclass : 'p1-textc',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 'sp-1 my_font_very_big sniglet',
			textdata : data.string.p1text5,
			textclass : 'my_font_very_big sniglet',
			imgsrc: 'images/textbox/white/tl-2.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "squirrel",
					imgsrc : imgpath + "squirrel.png",
				}
			],
		}]
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p1text6,
			textclass : 'my_font_big sniglet',
			imgsrc: 'images/textbox/white/tr-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "animal monkey",
					imgsrc : imgpath + "monkey.png",
				},{
					imgclass : "animal rhino",
					imgsrc : imgpath + "rhino.png",
				},{
					imgclass : "animal leopard",
					imgsrc : imgpath + "snow-leopard.png",
				},{
					imgclass : "animal redpanda",
					imgsrc : imgpath + "redpanda.png",
				}
			],
		}]
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p1text7,
			textclass : 'my_font_big sniglet',
			imgsrc: 'images/textbox/white/tl-1.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "animal monkey",
					imgsrc : imgpath + "monkey.png",
				},{
					imgclass : "animal rhino",
					imgsrc : imgpath + "rhino.png",
				},{
					imgclass : "animal leopard",
					imgsrc : imgpath + "snow-leopard.png",
				},{
					imgclass : "animal redpanda",
					imgsrc : imgpath + "redpanda.png",
				}
			],
		}]
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'title-2 my_font_ultra_big pangolin',
			textdata: data.string.summary
		}],
		uppertextblockadditionalclass: 'textbox-1 tb-1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : 'p1-texta',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text9,
			textclass : 'p1-textb my_font_very_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		lowertextblockadditionalclass: 'textbox-1 tb-2',
		lowertextblock : [{
			textdata : data.string.whatr,
			textclass : 'p1-texta',
		},{
			textdata : data.string.p1text11,
			textclass : 'p1-textb my_font_very_big',
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		speechbox:[{
			speechbox: 'sp-1 my_font_big',
			textdata : data.string.p1text12,
			textclass : 'my_font_very_big sniglet',
			imgsrc: 'images/textbox/white/tl-2.png',
			// audioicon: true,
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "squirrel",
					imgsrc : imgpath + "squirrel.png",
				}
			],
		}]
	},
	//slide7 -girl
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow slide-left',
			textdata: ''
		},{
			textclass: 'side-yellow-2 slide-right',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet fade-in-1',
			textdata: data.string.p1text13
		},{
			textclass: 'bottom-text my_font_medium sniglet slide-left-1',
			textdata: data.string.p1text14,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-2 my_font_medium sniglet slide-up',
			textdata: data.string.pbecause,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-3 my_font_medium sniglet slide-right-1',
			textdata: data.string.p1text15,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "deepa slide-left-2",
					imgsrc : imgpath + "asha-sad.png",
				},{
					imgclass : "deepa-cycle slide-right-2",
					imgsrc : imgpath + "cycle.png",
				}
			],
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text13
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text15a,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text13,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "deepa-2",
					imgsrc : imgpath + "asha-sad.png",
				},{
					imgclass : "deepa-cycle-2",
					imgsrc : imgpath + "cycle.png",
				}
			],
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text13
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text15a,
			datahighlightflag : true,
			datahighlightcustomclass : ' reasontext '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text13,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		lowertextblockadditionalclass: 'textbox-1 reason1',
		lowertextblock : [{
			textdata : data.string.whatr,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text16,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "deepa-2",
					imgsrc : imgpath + "asha-sad.png",
				},{
					imgclass : "deepa-cycle-2",
					imgsrc : imgpath + "cycle.png",
				}
			],
		}]
	},
	//slide10 -cold
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow slide-left',
			textdata: ''
		},{
			textclass: 'side-yellow-2 slide-right',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet fade-in-1',
			textdata: data.string.p1text17
		},{
			textclass: 'bottom-text my_font_medium sniglet slide-left-1',
			textdata: data.string.p1text18,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-2 my_font_medium sniglet slide-up',
			textdata: data.string.pbecause,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-3 my_font_medium sniglet slide-right-1',
			textdata: data.string.p1text19,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "prem slide-left-2",
					imgsrc : imgpath + "cold01.png",
				},{
					imgclass : "prem-2 slide-right-2",
					imgsrc : imgpath + "cold02.png",
				}
			],
		}]
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text17
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text19a,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text17,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "prem",
					imgsrc : imgpath + "cold02.png",
				}
			],
		}]
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text17
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text19a,
			datahighlightflag : true,
			datahighlightcustomclass : ' reasontext '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text17,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		lowertextblockadditionalclass: 'textbox-1 reason1',
		lowertextblock : [{
			textdata : data.string.whatr,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text20,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "prem",
					imgsrc : imgpath + "cold01.png",
				}
			],
		}]
	},
	//slide13 -dog
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow slide-left',
			textdata: ''
		},{
			textclass: 'side-yellow-2 slide-right',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet fade-in-1',
			textdata: data.string.p1text21
		},{
			textclass: 'bottom-text my_font_medium sniglet slide-left-1',
			textdata: data.string.p1text22,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-2 my_font_medium sniglet slide-up',
			textdata: data.string.pbecause,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		},{
			textclass: 'bottom-text-3 my_font_medium sniglet slide-right-1',
			textdata: data.string.p1text23,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "dog slide-left-2",
					imgsrc : imgpath + "dog-barking.png",
				},{
					imgclass : "dog-2 slide-right-3",
					imgsrc : imgpath + "dog-barking01.png",
				}
			],
		}]
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text21
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text23a,
			datahighlightflag : true,
			datahighlightcustomclass : ' '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text21,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "dog-3",
					imgsrc : imgpath + "dog-barking01.png",
				}
			],
		}]
	},
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-1',
		
		extratextblock: [{
			textclass: 'side-yellow',
			textdata: ''
		},{
			textclass: 'title-3 my_font_very_big sniglet',
			textdata: data.string.p1text21
		},{
			textclass: 'bottom-text-full my_font_medium sniglet',
			textdata: data.string.p1text23a,
			datahighlightflag : true,
			datahighlightcustomclass : ' reasontext '
		}],
		uppertextblockadditionalclass: 'textbox-1 ques1',
		uppertextblock : [{
			textdata : data.string.whatq,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text21,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		lowertextblockadditionalclass: 'textbox-1 reason1',
		lowertextblock : [{
			textdata : data.string.whatr,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		},{
			textdata : data.string.p1text24,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'ul'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "dog-3",
					imgsrc : imgpath + "dog-barking01.png",
				}
			],
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_1;
	var myTimeout =  null;
	var timeoutvar =  null;
	var timeoutvar2 =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
		case 0:
			sound_nav(sound_1);
			nav_button_controls(1000);
			break;
		case 1:
			$prevBtn.show(0);
			sound_nav(sound_2);
			break;
		case 2:
			$prevBtn.show(0);
			mini_sound_and_nav(sound_3, click_dg, '.speechbox');
			break;
		case 3:
			$prevBtn.show(0);
			mini_sound_and_nav(sound_4, click_dg, '.speechbox');
			break;
		case 4:
			$prevBtn.show(0);
			mini_sound_and_nav(sound_5, click_dg, '.speechbox');
			break;
		case 5:
			$prevBtn.show(0);
			sound_nav(sound_6);
			break;
		case 6:
			$prevBtn.show(0);
			mini_sound_and_nav(sound_7, click_dg, '.speechbox');
			break;
		case 7:
		case 10:
		case 13:
			var sound_arr_var = [sound_8, sound_9, sound_10, sound_11];
			if(countNext==10){
				sound_arr_var = [sound_14, sound_15, sound_16, sound_17];
			} else if(countNext==13){
				sound_arr_var = [sound_20, sound_21, sound_22, sound_23];
			}
			$prevBtn.show(0);
			current_sound.stop();
			current_sound = sound_arr_var[0];
			timeoutvar=setTimeout(function(){
				current_sound.play(current_sound);
				current_sound.bindOnce('ended', function(){
					$('.slide-left-1').show(0);
					current_sound.stop();
					current_sound = sound_arr_var[1];
					current_sound.play(current_sound);
					current_sound.bindOnce('ended', function(){
						$('.slide-up').show(0);
						current_sound.stop();
						current_sound = sound_arr_var[2];
						current_sound.play(current_sound);
						current_sound.bindOnce('ended', function(){
							$('.slide-right').show(0);
							$('.slide-right-2').show(0);
							$('.slide-right-3').show(0);
							timeoutvar2 = setTimeout(function(){
								$('.slide-right-1').show(0);
								current_sound.stop();
								current_sound = sound_arr_var[3];
								current_sound.play(current_sound);
								current_sound.bindOnce('ended', function(){
									$nextBtn.show(0);
								});
							}, 1000);
						});
					});
				});
			}, 2000);
			break;
		case 8:
		case 11:
		case 14:
			var sound_new_var = sound_12;
			if(countNext==11){
				sound_new_var = sound_18;
			}else if(countNext==14){
				sound_new_var = sound_24;
			}
			$prevBtn.show(0);
			$('.title-3').addClass('encircle');
			sound_nav(sound_new_var);
			break;
		case 9:
		case 12:
		case 15:
			var sound_new_var = sound_13;
			if(countNext==12){
				sound_new_var = sound_19;
			}else if(countNext==15){
				sound_new_var = sound_25;
			}
			$prevBtn.show(0);
			$('.reasontext').addClass('hl-reason');
			sound_nav(sound_new_var);
			break;
		default:
			$prevBtn.show(0);
			nav_button_controls(100);
			break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}
	function conversation(class1, sound_data1, class2, sound_data2, change_src, has_imagination, imagination){
		$(class1).fadeIn(500, function(){
			current_sound.stop();
			current_sound = sound_data1;
			current_sound.play();
			current_sound.bindOnce('ended', function(){
				if(change_src==1){
					$('.boy').attr('src', imgpath+'pemba1.png');
					$('.girl').attr('src', imgpath+'maya2.png');
				} else if(change_src==2){
					$('.boy').attr('src', imgpath+'pemba2.png');
					$('.girl').attr('src', imgpath+'maya1.png');
				}
				$(class2).fadeIn(500, function(){
					current_sound.stop();
					current_sound = sound_data2;
					current_sound.play();
					current_sound.bindOnce('ended', function(){
						if(has_imagination){
							//adding imagination animation here
							$(imagination).show(0);
							nav_button_controls(1500);
						} else {
							nav_button_controls(0);
						}
					});
				});
			});
		});
	}
	//mini_sound_and_nav(sound_gr_1[popcount-2], click_dg, '.speechbox');
	function mini_sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if( countNext == $total_page-1 ){
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			current_sound.stop();
			current_sound = audio;
			current_sound.play();
			// current_sound.bindOnce('ended', function(){
			// });
		});
	}
	
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(timeoutvar2);
		current_sound.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

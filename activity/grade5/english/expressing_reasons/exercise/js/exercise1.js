var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var sound_1 = new buzz.sound((soundAsset + "ex.ogg"));

var content = [
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text1,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text1d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "1.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text1a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text1b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text1c,
			}
		],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text2,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text2d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "2.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text2a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text2b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text2c,
			}
		],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text3,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text3d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "3.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text3a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text3b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text3c,
			}
		],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text4,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text4d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "4.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text4a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text4b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text4c,
			}
		],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text5,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text5d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "5.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text5a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text5b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text5c,
			}
		],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text6,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text6d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "6.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text6a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text6b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text6c,
			}
		],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text7,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text7d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "7.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text7a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text7b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text7c,
			}
		],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text8,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text8d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "8.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text8a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text8b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text8c,
			}
		],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text9,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text9d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "9.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text9a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text9b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text9c,
			}
		],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		instructiondata: data.string.p4ins,
		questiondata: data.string.p4text10,
		
		extratextblock:[{
			textclass: 'bg-dark-overlay',
			textdata: ''
		},{
			textclass: 'final-text',
			textdata: data.string.p4text10d
		},{
			textclass: 'submit-btn',
			textdata: data.string.submit
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ex-image",
					imgsrc : imgpath + "10.png",
				},
			],
		}],
		exeoptions: [
			{
				optid: "opt-1",
				optdata: data.string.p4text10a,
			},
			{
				optid: "opt-2",
				optdata: data.string.p4text10b,
			},
			{
				optid: "opt-3",
				optdata: data.string.p4text10c,
			}
		],
	}
];
content.shufflearray();


$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
   var current_sound=sound_1;
	/*for limiting the questions to 10*/
	var $total_page = 10;
	
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	/*values in this array is same as the name of images of eggs in image folder*/

	var scoring = new NumberTemplate();
	scoring.init(10);
    function sound_nav(sound_data){
        current_sound.stop();
        current_sound = sound_data;
        current_sound.play();
    }
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var testcount = 0;
		
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		scoring.numberOfQuestions();
		
		var ansClicked = false;
		var wrngClicked = false;
		countNext==0?sound_nav(sound_1):"";

        /*for randomizing the options*/
		var ansorder = ['opt-1', 'opt-2', 'opt-3'];
		ansorder.shufflearray();
		$('.optionscontainer').appendTo('body');
		for(var i=0; i<3; i++){
			$('#'+ansorder[i]).appendTo('.optionsdiv');
		}
		var correctans = 0;

		
		wrngClicked = false;
		$(".optionsdiv").sortable({
			placeholder: "drop-highlight",
			cursor: 'grabbing',
			stop: function(event, ui) {
				ansorder = $(this).sortable('toArray');
				$(".submit-btn").show(0);
				// $(".instruction").html(productOrder);
			}
		});
		$(".submit-btn").click(function(){
			correctans = check_order(ansorder);
			if(ansClicked == false){
				ansClicked = true;
			}
		});
		function check_order(array){
			var correctones = 0;
			for(i=1; i<=array.length; i++){
				var current_number = array[i-1].replace(/\D/g,'');
				if(parseInt(current_number)==i){
					correctones++;
					$('#opt-'+i).removeClass('incorrect-opt').addClass('correct-opt');
					$('#opt-'+i+'>.wrngopt').hide(0);
					$('#opt-'+i+'>.corctopt').show(0);
				} else{
					$('#opt-'+i).removeClass('correct-opt').addClass('incorrect-opt');
					$('#opt-'+i+'>.corctopt').hide(0);
					$('#opt-'+i+'>.wrngopt').show(0);
				}
			}
			if(correctones==array.length){
				$( ".optionsdiv" ).sortable( "disable" );
				$('.optionscontainer').css('pointer-events', 'none');
				$(".submit-btn").css({
					'background-color': '#92AF3B',
					'border': '0.5vmin solid #D4EF34'
				});
				play_correct_incorrect_sound(1);
				$(".submit-btn").delay(500).fadeOut(500);
				$( ".optionsdiv" ).delay(500).fadeOut(500, function(){
					$('.final-text').fadeIn(500, function(){
						$nextBtn.show(0);
					});
				});
				if(!wrngClicked){
					scoring.update(true);
				}
			} else{
				if(!wrngClicked){
					scoring.update(false);
				}
				$(".submit-btn").css({
					'background-color': '#FF0000',
					'border': '0.5vmin solid #A62628'
				});
				wrngClicked = true;
				play_correct_incorrect_sound(0);
			}
			return correctones;
		}
		/*======= SCOREBOARD SECTION ==============*/
	}

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		// for scoringg purpose
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		// scoringg purpose code ends
	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		scoring.gotoNext();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
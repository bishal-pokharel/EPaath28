var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio/";
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");

var np_dialog0 = new buzz.sound(soundAsset+"np_1.ogg");
var np_dialog1 = new buzz.sound(soundAsset+"np_2.ogg");//
var sound1 = new buzz.sound(soundAsset+"p6_s0.ogg");//

var en_sound_group1 = [dialog0, dialog1];
var np_sound_group1 = [np_dialog0, np_dialog1];
var content=[
{
    //slide1
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text3
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text6a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text4
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text5
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text6
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},

{
    //slide2
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text7
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text10a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text8
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text9
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text10
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},
{
    //slide3
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text11
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text14a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text12
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text13
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text14
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},
{
    //slide4
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text15
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text18a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text16
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text17
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text18
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},
{
    //slide1
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text19
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text22a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text20
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text21
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text22
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},
{
    //slide1
    contentblockadditionalclass: "bg",
    uppertextblock : [
    {
        textclass : "instruction",
        textdata : data.string.p3text2
    },
    {
        textclass : "question",
        textdata : data.string.p3text23
    },
    {
        textclass : "afterclass",
        textdata : data.string.p3text26a
    }
    ],
    draggableblock: [
    {
        draggabletextstyle: "draggableitem class1",
        draggabledata: data.string.p3text24
    },
    {
        draggabletextstyle: "draggableitem class2",
        draggabledata: data.string.p3text25
    },
    {
        draggabletextstyle: "draggableitem class3",
        draggabledata: data.string.p3text26
    }
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "textbox",
            imgsrc : imgpath + "textbox.png"
        },
        {
            imgclass : "leftimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "rightimg",
            imgsrc : imgpath + "bird.png"
        },
        {
            imgclass : "cloud1",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud1text'
            }]
        },
        {
            imgclass : "cloud2",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud2text'
            }]
        },
        {
            imgclass : "cloud3",
            imgsrc : imgpath + "cloud.png",
            imagelabels:[{
                imagelabelclass:'cloud3text'
            }]
        }
        ],
    }
    ]
},
];
content.shufflearray();

$(function(){
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    if($lang == 'np'){
        sound_group1 = np_sound_group1;
    }else{
        sound_group1 = en_sound_group1;
    }

    var $total_page = content.length;
    var vocabcontroller =  new Vocabulary();
  	vocabcontroller.init();
    loadTimelineProgress($total_page, countNext + 1);

    /*
        inorder to use the handlebar partials we need to register them
        to their respective handlebar partial pointer first
        */
        Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
        Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
        Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
        Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());

    //controls the navigational state of the program



      /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
        islastpageflag = false :
        typeof islastpageflag != 'boolean'?
        alert("NavigationController : Hi Master, please provide a boolean parameter") :
        null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.pageEndSetNotification() :
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.pageEndSetNotification() :
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }


    function generalTemplate(){
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);
        vocabcontroller.findwords(countNext);


            var option_position = ["class1","class2","class3"];
            option_position.shufflearray();
            var first =option_position[0];
            var second =option_position[1];
            var third =option_position[2];

            $("."+ first ).addClass('a');
            $("."+ second ).addClass('b');
            $("."+ third ).addClass('c');

            sound_group1[0].play();
            var finCount = 0;
            $nextBtn.hide(0);
            $('.draggableitem').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                cursor : "move",
                zIndex: 100000,
            });

            $(".cloud1text").droppable({
                accept: ".class1",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".cloud2text").droppable({
                accept: ".class2",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".cloud3text").droppable({
                accept: ".class3",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            function dropfunc(event, ui, $droppedOn){
                var classText = ui.draggable.text();
                ui.draggable.draggable('disable');
                ui.draggable.hide(0);
                /*var droppedOn = $(this);*/
                $droppedOn.html(classText);
                $droppedOn.removeClass('labelbox');
                $droppedOn.addClass('labelboxdropped');
                finCount++;
                if(finCount == 3)
                {   play_correct_incorrect_sound(1);
                    $('.afterclass').animate({"opacity":"1"},500);
                    $nextBtn.show(0);
                }
            }
            countNext==0?sound1.play():'';
            switch(countNext){
                case 5:
                function dropfunc(event, ui, $droppedOn){
                        var classText = ui.draggable.text();
                        ui.draggable.draggable('disable');
                        ui.draggable.hide(0);
                        /*var droppedOn = $(this);*/
                        $droppedOn.html(classText);
                        $droppedOn.removeClass('labelbox');
                        $droppedOn.addClass('labelboxdropped');
                        finCount++;
                        if(finCount == 3)
                         if(finCount == 3)
                            {
                                $('.afterclass').animate({"opacity":"1"},500);
                                play_correct_incorrect_sound(1);

                                $nextBtn.hide(0);
                                ole.footerNotificationHandler.pageEndSetNotification() ;
                            }
                    }

                    break;
            }

    }


    function templateCaller(){
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        //navigationcontroller();

        generalTemplate();
        /*
          for (var i = 0; i < content.length; i++) {
            slides(i);
            $($('.totalsequence')[i]).html(i);
            $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
          "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
          }
          function slides(i){
              $($('.totalsequence')[i]).click(function(){
                countNext = i;
                templateCaller();
              });
            }
        */


    }

    function soundplayer(i){
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        sound_group1[i].play().bind("ended",function(){
            navigationcontroller();
        });
    }

    $nextBtn.on("click", function(){
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
// document.addEventListener("contentloaded", function(){
    total_page = content.length;
    templateCaller();
    // });

});



 /*===============================================
     =            data highlight function            =
     ===============================================*/
     function texthighlight($highlightinside){
            //check if $highlightinside is provided
            typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

            var $alltextpara = $highlightinside.find("*[data-highlight='true']");
            var stylerulename;
            var replaceinstring;
            var texthighlightstarttag;
            var texthighlightendtag   = "</span>";


            if($alltextpara.length > 0){
                $.each($alltextpara, function(index, val) {
                    /*if there is a data-highlightcustomclass attribute defined for the text element
                    use that or else use default 'parsedstring'*/
                    $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                    texthighlightstarttag = "<span>";


                    replaceinstring       = $(this).html();
                    replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                    replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                    $(this).html(replaceinstring);
                });
            }
        }
        /*=====  End of data highlight function  ======*/

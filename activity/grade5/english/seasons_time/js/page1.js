var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio/";
var sound_dg1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p1_s7.ogg"));

var content=[
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'a1',
			imgsrc: imgpath + 'autum.png'
		},
		{
			imgclass:'a1',
			imgsrc: imgpath + 'pastedImage0.png'
		},
		{
			imgclass:'a1 ta',
			imgsrc: imgpath + 'summer.png'
		},
		{
			imgclass:'a1',
			imgsrc: imgpath + 'winter.png'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		textclass: "bottomtext",
		textdata: data.string.p1text1
	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'titleimage',
			imgsrc: imgpath + 'pastedImage0.png'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text2

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'march_may.png'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text3

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'spring-flower-bloom.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text4

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'Sandakphu685.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text5

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'spring-bird-making-nest.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text6

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'spring-neither-hot-not-cold.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbg",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text7

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'spring-holi.jpg'
		}]
	}]
}

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
				playaudio(sound_dg1, $(".1d"));
				break;
			case 1:
				playaudio(sound_dg2, $(".1d"));
				$('.titleimage').addClass('fadein');
				break;
			case 2:
			playaudio(sound_dg3, $(".1d"));
				$('.descriptionimages').css({"width": "80%"});
				$('.descriptionimages').addClass('fadein');
			break;
			case 3:
			playaudio(sound_dg4, $(".1d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 4:
			playaudio(sound_dg5, $(".1d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 5:
			playaudio(sound_dg6, $(".1d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 6:
			playaudio(sound_dg7, $(".1d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 7:
			playaudio(sound_dg8, $(".1d"));
			$('.descriptionimages').addClass('fadein');
			break;
		}

	}
	function playaudio(sound_data, $dialog_container){
			buzz.all().stop();
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
					if(!playing){
							playaudio(sound_data, $dialog_container);
					}
					return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
					ole.footerNotificationHandler.hideNotification();
			}else{
					$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
					setTimeout(function(){
							$prevBtn.show(0);
							$dialog_container.addClass("playable");
							playing = false;
							sound_data.unbind('ended');
							if((countNext+1) == content.length){
									ole.footerNotificationHandler.pageEndSetNotification();
							}else{
									$nextBtn.show(0);
							}
					}, 1000);
			});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            // replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            // replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

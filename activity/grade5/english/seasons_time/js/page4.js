var imgpath = $ref + "/images/";
var soundAsset = $ref+"/audio/";
var sound_dg1 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var content=[
//Winter
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		textclass: "bottomtext",
		textdata: data.string.p1text23
	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'titleimage',
			imgsrc: imgpath + 'winter.png'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text24

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'dec_feb.png'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text25

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'winter-foggy.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text26

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'winter-wearing-warm.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text27

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'winter-bared-tree.jpg'
		}]
	}]
},
{
	contentblockadditionalclass: "normalbgwinter",
	uppertextblock:[
	{
		datahighlightflag:true,
		datahighlightcustomclass2:"pink",
		datahighlightcustomclass:"blue",
		textclass: "toptext",
		textdata: data.string.p1text28

	}
	],
	imageblock:[{
		imagetoshow:[{
			imgclass:'descriptionimages',
			imgsrc: imgpath + 'winter-tea.jpg'
		}]
	}]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){

			case 0:
			playaudio(sound_dg1, $(".7d"));
			$('.titleimage').css({"width": "40%","left":"59%"});
			$('.descriptionimages').addClass('fadein');
			break;
			case 1:
			playaudio(sound_dg2, $(".7d"));
			$('.descriptionimages').css({"width": "80%"});
			$('.descriptionimages').addClass('fadein');
			break;
			case 2:
			playaudio(sound_dg3, $(".7d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 3:
			playaudio(sound_dg4, $(".7d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 4:
			playaudio(sound_dg5, $(".7d"));
			$('.descriptionimages').addClass('fadein');
			break;
			case 5:
			playaudio(sound_dg6, $(".7d"));
			$('.descriptionimages').addClass('fadein');
			break;

		}

	}
	function playaudio(sound_data, $dialog_container){
			buzz.all().stop();
			var playing = true;
			$dialog_container.removeClass("playable");
			$dialog_container.click(function(){
					if(!playing){
							playaudio(sound_data, $dialog_container);
					}
					return false;
			});
			$prevBtn.hide(0);
			if((countNext+1) == content.length){
					ole.footerNotificationHandler.hideNotification();
			}else{
					$nextBtn.hide(0);
			}
			sound_data.play();
			sound_data.bind('ended', function(){
					setTimeout(function(){
							$prevBtn.show(0);
							$dialog_container.addClass("playable");
							playing = false;
							sound_data.unbind('ended');
							if((countNext+1) == content.length){
									ole.footerNotificationHandler.pageEndSetNotification();
							}else{
									$nextBtn.show(0);
							}
					}, 1000);
			});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            // replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            // replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio/";
var sound_1 = new buzz.sound((soundAsset + "s5_p1.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text1,

			}
		]
	},
	//slide 1
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text2,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text4,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text6,
					}],
			}
		]
	},
	//slide 2
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text7,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text4,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text6,
					}],
			}
		]
	},
	//slide 3
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text8,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text4,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text6,
					}],
			}
		]
	},
	//slide 4
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text9,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text4,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text6,
					}],
			}
		]
	},
	//slide 5
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text10,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text6,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text4,
					}],
			}
		]
	},
	//slide 6
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text11,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text6,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text4,
					}],
			}
		]
	},

//slide 5
	{
		contentblockadditionalclass:'bg',
		imageblock:[
		{
			imgclass:'leftimg',
			imgsrc: imgpath + 'bird.png'
		},
		{
			imgclass:'rightimg',
			imgsrc: imgpath + 'bird.png'
		}
		],

		exerciseblock: [
			{
				textclass : 'instruction my_font_big',
				questiondata: data.string.p2text12,
				option: [
					{
						option_class: "class1",
						optiondata: data.string.p2text3,
					},
					{
						option_class: "class2",
						optiondata: data.string.p2text5,
					},
					{
						option_class: "class3",
						optiondata: data.string.p2text6,
					},
					{
						option_class: "class4",
						optiondata: data.string.p2text4,
					}],
			}
		]
	}
];

/*remove this for non random questions*/
$(function () {
	var testin = new NumberTemplate();

	var exercise = new template_exercise_mcq_monkey(content, testin, true);
	exercise.create_exercise();
});

function template_exercise_mcq_monkey(content, scoring){
	var $board			= $('.board');
	var $nextBtn		= $("#activity-page-next-btn-enabled");
	var $prevBtn		= $("#activity-page-prev-btn-enabled");
	var countNext		= 0;
	var testin			= new EggTemplate();
	var wrong_clicked 	= false;

	var total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	function generaltemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		testin.numberOfQuestions();
		vocabcontroller.findwords(countNext);
		switch(countNext)
		{
			case 0:
			$('.center-sundar').css({"display":"none"});
			$nextBtn.delay(5000).show(0);
			soundplayer(sound_1);
			break;
		}

		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}
		//top-left
		$('.option-pos-1').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/top-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		//bottom-left
		$('.option-pos-3').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','scaleX(-1)');
		}, function(){

		});
		//top-right
		$('.option-pos-2').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/top-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){

		});
		//bottom-right
		$('.option-pos-4').hover(function(){
			$('.center-sundar').attr('src', 'images/sundar/bottom-right.png');
			$('.center-sundar').css('transform','none');
		}, function(){

		});


		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					testin.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$('.center-sundar').attr('src', 'images/sundar/'+correct_images[rand_img]);
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				total_page = content.length;
				if(countNext != total_page)
				{
					$nextBtn.show(0);
				}
				if(countNext == total_page-1)
				{
					$nextBtn.hide(0);
					ole.footerNotificationHandler.pageEndSetNotification() ;
				}

			}
			else{
				var classname_monkey = $(this).parent().attr('class').replace(/main-container/, '');
				classname_monkey = classname_monkey.replace(/ /g, '');
				$('.'+classname_monkey).off('mouseenter mouseleave');
				if(wrong_clicked==0){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-1.png');
				} else if(wrong_clicked == 1){
					$('.center-sundar').attr('src', 'images/sundar/incorrect-2.png');
				} else {
					$('.center-sundar').attr('src', 'images/sundar/incorrect-3.png');
				}
				testin.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	};

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		// call the template
		generaltemplate();

	};

	this.create_exercise = function(){
		// content.shufflearray();
		if(typeof scoring != 'undefined'){
			testin = scoring;
		}
	 	testin.init(total_page);

		templateCaller();
		$nextBtn.on("click", function(){
			countNext++;
			testin.gotoNext();
			templateCaller();
		});
		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			countNext--;
			templateCaller();
		});
	};
}

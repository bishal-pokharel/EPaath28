var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/audio/";
var sound_dg1 = new buzz.sound((soundAsset + "Click_on_the_correct_answer.ogg"));

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'bg1',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext5,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options corans',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 1
	{
		contentblockadditionalclass: 'bg2',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext7,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext8,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext9,
			textclass : 'class-2 options corans',
		},
		{
			textdata : data.string.ext10,
			textclass : 'class-3 options',
		},
		{
			textdata : data.string.ext11,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 2
	{
		contentblockadditionalclass: 'bg3',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext12,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options corans',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 3
	{
		contentblockadditionalclass: 'bg4',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext13,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 4
	{
		contentblockadditionalclass: 'bg1',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext18,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext14,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext15,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext16,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext17,
			textclass : 'class-4 options corans',
		},
		]
	},
	//slide 5
	{
		contentblockadditionalclass: 'bg2',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext19,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 6
	{
		contentblockadditionalclass: 'bg3',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext20,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 7
	{
		contentblockadditionalclass: 'bg4',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext21,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options corans',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options',
		},
		]
	},
	//slide 8
	{
		contentblockadditionalclass: 'bg2',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext22,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext14,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext15,
			textclass : 'class-2 options',
		},
		{
			textdata : data.string.ext16,
			textclass : 'class-3 options corans',
		},
		{
			textdata : data.string.ext17,
			textclass : 'class-4 options ',
		},
		]
	},
	//slide 9
	{
		contentblockadditionalclass: 'bg1',
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'instruction my_font_big',
		},
		{
			textdata : data.string.ext23,
			textclass : 'question my_font_big',
		},
		{
			textdata : data.string.ext1,
			textclass : 'class-1 options ',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class-2 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class-3 options ',
		},
		{
			textdata : data.string.ext4,
			textclass : 'class-4 options corans',
		},
		]
	},


];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var current_sound = sound_dg1;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		//randomize options
		var positions = [1,2,3,4];
		positions.shufflearray();
		for(var i=1; i<=4; i++){
			$('.class-'+i).addClass('pos-'+positions[i-1]);
		}
		countNext == 0?sound_dg1.play():'';
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$(".options").click(function(){
			current_sound.stop();
			if($(this).hasClass("corans")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".options").css('pointer-events', 'none');
				$('.blank-space').html($(this).html().substring(4));
				play_correct_incorrect_sound(1);
				var thistext = $(this).text();
				$(".ansfiller").text(thistext);
				$(this).css({
					'color': 'white',
					'font-weight':'bold',
					'background-color':'rgb(40,152,40)',
					'border':".5vmin solid white"
				});
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).css({
					'color': 'rgb(182,77,4)',
					'text-decoration': 'line-through',
					'pointer-events': 'none',
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

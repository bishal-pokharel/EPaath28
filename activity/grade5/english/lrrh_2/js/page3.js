var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p3_s0.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p3_s2.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p3_s6.ogg"));

var sound_correct = new buzz.sound((soundAsset + "correct.mp3"));
var sound_incorrect = new buzz.sound((soundAsset + "incorrect.mp3"));


var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7];


/*	Store the questions and answers in an array, the first array[0]->question_1[0] is the question
 * 	Second array[1] is the correct answer and the rest are incorrect answers
 *	Then store all the questions in another array-> questions so that they can be displayed randomly */
var question_1 = [data.string.p3ques1, data.string.p3ans1a, data.string.p3ans1b, data.string.p3ans1c];
var question_2 = [data.string.p3ques2, data.string.p3ans2a, data.string.p3ans2b, data.string.p3ans2c];
var question_3 = [data.string.p3ques3, data.string.p3ans3a, data.string.p3ans3b, data.string.p3ans3c];
var question_4 = [data.string.p3ques4, data.string.p3ans4a, data.string.p3ans4b, data.string.p3ans4c];

var questions = [question_4, question_3, question_2, question_1];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para my_font_medium",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_on",
			textdata : data.string.p3text1,
			datahighlightflag : true,
			datahighlightcustomclass : 'text_story',
		}],
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_on",
			textdata : data.string.p3text2,
		}]
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_off",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story my_font_medium text_l2 text_on",
			textdata : data.string.p3text3,
		}]
	},
	
	//slide 3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_off",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story my_font_medium text_l2 text_off",
			textdata : data.string.p3text3,
		},
		{
			textclass : "text_story my_font_medium text_l3 text_on",
			textdata : data.string.p3text4,
		}]
	},
	//slide 4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_off",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story my_font_medium text_l2 text_off",
			textdata : data.string.p3text3,
		},
		{
			textclass : "text_story my_font_medium text_l3 text_on",
			textdata : data.string.p3text4,
		},
		{
			textclass : "text_story my_font_medium text_l4 text_on",
			textdata : data.string.p3text5,
		}]
	},
	//slide 5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_off",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story my_font_medium text_l2 text_off",
			textdata : data.string.p3text3,
		},
		{
			textclass : "text_story my_font_medium text_l3 text_on",
			textdata : data.string.p3text4,
		},
		{
			textclass : "text_story my_font_medium text_l4 text_on",
			textdata : data.string.p3text5,
		},
		{
			textclass : "text_story my_font_medium text_l5 text_on",
			textdata : data.string.p3text6,
		}]
	},
	
	//slide 6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story my_font_medium text_l0 text_off",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story my_font_medium text_l1 text_off",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story my_font_medium text_l2 text_off",
			textdata : data.string.p3text3,
		},
		{
			textclass : "text_story my_font_medium text_l3 text_on",
			textdata : data.string.p3text4,
		},
		{
			textclass : "text_story my_font_medium text_l4 text_on",
			textdata : data.string.p3text5,
		},
		{
			textclass : "text_story my_font_medium text_l5 text_on",
			textdata : data.string.p3text6,
		},
		{
			textclass : "text_story my_font_medium text_l6 text_on",
			textdata : data.string.p3text7,
		}]
	},
	
	//slide 7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass: 'bg_story_3',
	
		storytextblockadditionalclass : "bottom_para",
	
		storytextblock : [
		{
			textclass : "text_story text_0 text_inactive my_font_medium",
			textdata : data.string.p3text1,
		},
		{
			textclass : "text_story text_1 text_inactive my_font_medium",
			textdata : data.string.p3text2,
		},
		{
			textclass : "text_story text_2 text_inactive my_font_medium",
			textdata : data.string.p3text3,
		},
		{
			textclass : "text_story text_3 text_inactive my_font_medium",
			textdata : data.string.p3text4,
		},
		{
			textclass : "text_story text_4 text_inactive my_font_medium",
			textdata : data.string.p3text5,
		},
		{
			textclass : "text_story text_5 text_inactive my_font_medium",
			textdata : data.string.p3text6,
		},
		{
			textclass : "text_story text_6 text_inactive my_font_medium",
			textdata : data.string.p3text7,
		}],
		lowertextblockadditionalclass: 'ques_div drop its_hidden',
		lowertextblock : [
		{
			textclass : "text_qna text_q0 text_ques my_font_big",
			textdata : '',
		},
		{
			textclass : "text_qna text_q1 ans_button_inactive my_font_big",
			textdata : '',
		},
		{
			textclass : "text_qna text_q2 ans_button_inactive my_font_big",
			textdata : '',
		},
		{
			textclass : "text_qna text_q3 ans_button_inactive my_font_big",
			textdata : '',
		},
		{
			textclass : "next_ques next_ques_inactive my_font_medium",
			textdata : data.string.p3next,
		},
		{
			textclass : "hide_btn my_font_small",
			textdata : data.string.p3hide,
		}],
		
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "hide_icon its_hidden",
				imgsrc : imgpath + "q_icon_green.png",
			}
			],
			imagelabels : [{
			imagelabelclass : "hint_para fade_in its_hidden",
			imagelabeldata : data.string.p3hint
			}]
		}]
	},
	
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $label = $(".label-box");
	var $total_page = content.length;
	var last_page = false;
	loadTimelineProgress($total_page, countNext + 1);
	var last_question=false;
	var toggle = 0;
	
	/* var to count the number of ques displayed */
	var ques_no = 0;
	var sound_data = sound_l_1;
	
	/* store numbers from 0 to 3 in random in an array using the function*/
	var random_array = sandy_random_display(questions.length);
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("speechboxcontent", $("#speechboxcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		loadTimelineProgress($total_page, countNext + 1);
		$board.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		vocabcontroller.findwords(countNext);
		switch (countNext) {
		case 0:
    		for_all_slides(1, 'text_l0', sound_group_p1[0], false);
			break;
		case 1:
    		for_all_slides(1, 'text_l1', sound_group_p1[1], false);
			break;
		case 2:
			for_all_slides(1, 'text_l2', sound_group_p1[2], false);
			break;
		case 3:
			for_all_slides(1, 'text_l3', sound_group_p1[3], false);
			break;
		case 4:
			for_all_slides(1, 'text_l4', sound_group_p1[4], false);
			break;
		case 5:
			for_all_slides(1, 'text_l5', sound_group_p1[5], false);
			break;
		case 6:
			for_all_slides(1, 'text_l6', sound_group_p1[6], false);
			break;
		case 7:
			$('.ques_div').show(0);
			$('.hide_icon').show(0);
			$('.hide_icon').click(function(){
				ole.footerNotificationHandler.pageEndSetNotification();
				if(toggle%2==0){
					toggle++;
					$('.ques_div').css({
						'top': '-70%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
				} else {
					toggle++;
					$('.ques_div').css({
						'top': '5%',
					});
					$('.hide_icon').attr('src', imgpath + "q_icon_green.png");
				}
			});
			$('.hide_btn').click(function(){
				toggle++;
				ole.footerNotificationHandler.pageEndSetNotification();
				$('.hint_para').removeClass('fade_away');	//adds fading effect
				$('.hint_para').show(0);
				$('.hint_para').delay(2000).addClass('fade_away');
				$('.hint_para').hide(0);
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			});
			question_caller('.text_q', questions);	
			$('.text_0').click(function() {
				click_text(this, sound_group_p1[0]);
			});
			$('.text_1').click(function() {
				click_text(this, sound_group_p1[1]);
			});
			$('.text_2').click(function() {
				click_text(this, sound_group_p1[2]);
			});
			$('.text_3').click(function() {
				click_text(this, sound_group_p1[3]);
			});
			$('.text_4').click(function() {
				click_text(this, sound_group_p1[4]);
			});
			$('.text_5').click(function() {
				click_text(this, sound_group_p1[5]);
			});
			$('.text_6').click(function() {
				click_text(this, sound_group_p1[6]);
			});
			$('.next_ques').click( function(){
				ques_no++;		// increase question number
				if(ques_no==questions.length-1){
					$('.next_ques').hide(0);	// hide the next question text
					last_question = true;
				}
				if(ques_no<questions.length){
					question_caller('.text_q', questions);		// call the next question
					ans_button_reset();			// reset all the answer buttons to unselected state
					$('.next_ques').removeClass('next_ques_active');
					$('.next_ques').addClass('next_ques_inactive');
				}
			});
			break;
		default:
			break;
		}
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		sound_data.stop();
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		sound_data.stop();
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
	function for_all_slides(slide_no, text_class, my_sound_data, last_page_flag){
		var $textblack = $("."+text_class);
		sound_data =  my_sound_data;
		var current_text = $textblack.html();
		// current_text.replace(/<.*>/, '');
		play_text($textblack, current_text);
		sound_data.bind('ended', function(){
			change_slides(last_page_flag);
		});
	}
	function change_slides(last_page_flag){
		var checking_interval = setInterval(function(){
			if(textanimatecomplete){
				if(!last_page_flag){
    				$nextBtn.show(0);
	    		} else{
	    			ole.footerNotificationHandler.pageEndSetNotification();
	    		}
	    		if(countNext!=0){
    				$prevBtn.show(0);
	    		}
    			clearInterval(checking_interval);
			} else{
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}
		},50);
	}

	/* --- function that make objects appear ---- 
	function audio_fade_caller(fade_class, audio_arr, audio_count){
		if(audio_count>audio_arr.length){
			return false;
		}
		var class_name = $(fade_class + audio_count);
		class_name.show(0);
		audio_arr[audio_count].play();
		audio_arr[audio_count].bind('ended', function(){
			audio_count++;
			audio_fade_caller(fade_class, audio_arr, audio_count);
		});
	}
	*/
	
/* For typing animation appends the text to an element specified by target class or id */
	function show_text($this,  $span_speec_text, message, interval) {
		var stags_counter = 1;
		var stags = message.split(/[<>]/);
		var gt_encounters=[];
	  	if (0 < message.length) {
		  	textanimatecomplete = false;
		  	var nextText = message.substring(0,1);
		  	var brText = message.substring(0,4);
		  	if(brText =='<br>'){
		  		$span_speec_text.append("<br>");
	  			message = message.substring(4, message.length);
		  	}
		  	else if(nextText == "<" ){
		  		gt_encounters.push($span_speec_text.html().length);
		  		// $span_speec_text.append('<'+stags[stags_counter]+'>');
		  		message = message.substring(stags[stags_counter].length+2, message.length);
		  		stags_counter+=2;
		  	}else{
		  		$span_speec_text.append(nextText);
		  		message = message.substring(1, message.length);
		  	}
		  	$this.html($span_speec_text);
		  	$this.append(message);
		    setTimeout(function () {
		    	show_text($this,  $span_speec_text, message, interval);
		  	}, interval);
		} else{
	  		textanimatecomplete = true;
	  	}
	}

	var intervalid;
	var soundplaycomplete = false;
	var textanimatecomplete = false;
	// uses the show text to add typing effect with sound and glowing animations
	function play_text($this, text/*, sound_data*/){
		$this.html("<span id='span_speec_text'></span>"+text);
		$prevBtn.hide(0);
		var $span_speec_text = $("#span_speec_text");
		// $this.css("background-color", "#faf");
		show_text($this, $span_speec_text,text, 65);	// 65 ms is the interval found out by hit and trial
		sound_data.play();
		sound_data.bind('ended', function(){
			// $this.removeClass('text_on');
			// $this.addClass('text_off');
			sound_data.unbind('ended');
			soundplaycomplete = true;
			ternimatesound_play_animate(text);
		});

		function ternimatesound_play_animate(text){
			 intervalid = setInterval(function () {
				if(textanimatecomplete && soundplaycomplete){
					$this.html($span_speec_text.html(text));
					$this.css("background-color", "transparent");
					clearInterval(intervalid);
					intervalid = null;
					animationinprogress = false;
       				vocabcontroller.findwords(countNext);
					// if((countNext+1) == content.length){
						// ole.footerNotificationHandler.pageEndSetNotification();
					// }else{
						// $nextBtn.show(0);
					// }
					// if(countNext>0){
						// $prevBtn.show(0);
					// }
				}
			}, 250);
		}
	}
	
	/* Typing animation ends */
	
	/*	Function to check if the answer is correct or incorrect as decided by answer_bool
	 *  Display button green or red accordingly with correct sound as well 	 */
	function click_answer(play_class, answer_bool){
		sound_data.stop();
		$('.text_story').removeClass('text_active');
		$('.text_story').addClass('text_inactive');
		$(play_class).removeClass('ans_button_inactive');
		if( answer_bool){
			$(play_class).addClass('ans_button_right');
			$('.next_ques').removeClass('next_ques_inactive');
			$('.next_ques').addClass('next_ques_active');
            play_correct_incorrect_sound(1)
			$('.next_ques').css('pointer-events','all');
			$('.text_qna').css({
				'pointer-events': 'none',
			});
			if(last_question){
				ole.footerNotificationHandler.pageEndSetNotification();
				toggle++;
				$('.ques_div').css({
					'top': '-70%',
				});
				$('.hide_icon').attr('src', imgpath + "q_icon_red.png");
			}
		} else {
			$(play_class).addClass('ans_button_wrong');
            play_correct_incorrect_sound(0)
		}
	}
	
	/*
	function click_text_caller(click_class, sound_arr) {
			for( var i=0; i<sound_arr.length; i++) {
				var class_name = $(click_class + i);
				class_name.click(function() {
					click_text(this, sound_arr[i]);
				});
			}
		}*/
	
	/* function to allow user to click the text and play sound of each sentence clicked
	 * then allow appropriate animation	*/
	function click_text(play_class, my_sound_data){
		sound_data.stop();
		$('.text_story').removeClass('text_active');
		$('.text_story').addClass('text_inactive');
		sound_data = my_sound_data;
		sound_data.play();
		$(play_class).removeClass('text_inactive');
		$(play_class).addClass('text_active');
		sound_data.bind('ended', function(){
			$(play_class).removeClass('text_active');
			$(play_class).addClass('text_inactive');
		});
	}
	
	
	// function to add sound event on click
	function sound_caller(sound_box_class, sound_var){
		$(sound_box_class).click(function(){
			sound_var.play();
		});
	}

	
	function next_btn_caller (next_display) {
		if(!next_display) {
			return false;
		} else {
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
	}
	
	/* This Function call the question based on global variables named random_array for random questions and ques_no
	 * to track the index of question in the questions array
	 * random number array is generated to shuffle the answer
	 * the correct answer being question[x][1] we get the text in the answerbox and compare it with the var question[x][1]
	 * click events are disabled accordingly
	 */
	function question_caller(text_name, questions_arr) {
		var text_class= $(text_name + 0);
		text_class.html(ques_no+1+'. '+questions_arr[random_array[ques_no]][0]);
		random_ans = sandy_random_display(3);
		for( var m = 0; m<3; m++) {
			var ans_no = 1 + random_ans[m];
			text_class= $(text_name + ans_no);
			text_class.html(questions_arr[random_array[ques_no]][m+1]);			
		}
		biggest_box_size(text_name, 1, 3);
		$('.text_q1').click(function() {
			click_answer(this, $(this).text()==questions[random_array[ques_no]][1]);
		});
		$('.text_q2').click(function() {
			click_answer(this, $(this).text()==questions[random_array[ques_no]][1]);
		});
		$('.text_q3').click(function() {
			click_answer(this, $(this).text()==questions[random_array[ques_no]][1]);
		});	
		$('.next_ques').css('pointer-events','none');
	}
	
	
	/* Function to reset the answer button */
	function ans_button_reset() {
		for(var m = 1; m<4; m++){
			$('.text_qna').css({
				'pointer-events': 'all',
			});
			var playclass = $('.text_q'+m);
			playclass.removeClass('ans_button_right');
			playclass.removeClass('ans_button_wrong');
			playclass.addClass('ans_button_inactive');
		}
	}
	
	/* This function takes the class name and gets the number of string characters in that class
	 * calculates the length of the string and set width of all the class accrodingly
	 * class_initial should be myclass_ and start_index and number_of_class should be integer */
	function biggest_box_size(class_initial, start_index, number_of_class){
		var l = 0;
		for( var m=start_index; m<(number_of_class + start_index); m++){
			var class_name = $(class_initial + m);
			var l_new = class_name.text().length;
			if( l_new > l){
				l = l_new;
			}
		}	
		for( var m=start_index; m<(number_of_class + start_index); m++){
			var class_name = $(class_initial + m);
			class_name.css('width', l*2.5+'%');			//1.5 comes from font-size, change parameter according with font size
		}
	}
	
	/*  function used to generate numbers from 0 to range-1 in random order and store it in array 
	 *	This function pushes all the numbers upto range -1 in an array, generates the random no upto range-1
	 *  moves the randomly generated index from that array to new array and repeats the process till every number < range is generated	*/
	function sandy_random_display(range){
		var random_arr = [];
		var return_arr = [];
		var random_max = range;
		for(var i=0; i<range; i++){
			random_arr.push(i);
		}
		
		while(random_max)
		{
			var index = ole.getRandom(1, random_max-1, 0);
			return_arr.push(random_arr[index]);
			random_arr.splice((index),1);
			random_max--;
		}
		return return_arr;
	}

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

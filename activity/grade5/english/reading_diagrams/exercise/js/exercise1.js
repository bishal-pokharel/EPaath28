var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content1=[
	//slide 0	
	{	
		contentblockadditionalclass: 'instruction_bg',
		extratextblock : [
		{
			textdata : data.string.eins,
			textclass : 'instruction-text',
		}],
	},
];
var content2 = [
	//slide1
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q1,
		ques_img: 'graph-1',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans1_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans1_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans1_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans1_d,
				}],
			} 
		] 
	},
	//slide2
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q2,
		ques_img: 'graph-1',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans2_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans2_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans2_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans2_d,
				}],
			} 
		] 
	},
	//slide3
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q3,
		ques_img: 'graph-1',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans3_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans3_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans3_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans3_d,
				}],
			} 
		] 
	},
	//slide4
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q4,
		ques_img: 'graph-1',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans4_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans4_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans4_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans4_d,
				}],
			} 
		] 
	},
	//slide5
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q5,
		ques_img: 'graph-1',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans5_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans5_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans5_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans5_d,
				}],
			} 
		] 
	},
];

var content3 = [
	//slide6
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q6,
		ques_img: 'graph-2',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans6_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans6_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans6_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans6_d,
				}],
			} 
		] 
	},
	//slide7
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q7,
		ques_img: 'graph-2',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans7_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans7_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans7_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans7_d,
				}],
			} 
		] 
	},
	//slide8
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q8,
		ques_img: 'graph-2',
		exerciseblock: [
			{					
				
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans8_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans8_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans8_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans8_d,
				}],
			} 
		] 
	},
	//slide9
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q9,
		ques_img: 'graph-2',
		exerciseblock: [
			{					
				
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans9_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans9_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans9_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans9_d,
				}],
			} 
		] 
	},
	//slide10
	{
		contentblockadditionalclass: 'default-bg',
		questiondata: data.string.e1_q10,
		ques_img: 'graph-2',
		exerciseblock: [
			{					
				option: [{
					option_class: "class1",
					optiondata: data.string.e1_ans10_a,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans10_b,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans10_c,
				},
				{
					option_class: "class2",
					optiondata: data.string.e1_ans10_d,
				}],
			} 
		] 
	}
];
content2.shufflearray();
content3.shufflearray();
var tempContent = content2.concat(content3);
var content = content1.concat(tempContent);

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = 10;
	loadTimelineProgress($total_page,countNext+1);
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	var scoring = new EggTemplate();
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//sundar
			{id: "graph-1", src: imgpath+'graph-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "graph-2", src: imgpath+'graph-2.png', type: createjs.AbstractLoader.IMAGE},
			
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},


			// sounds

            {id: "sound_1", src: soundAsset+"Ex_s1_p1.ogg"},

        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init(10);
		templateCaller();
	}
	//initialize
	init();


	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var ques_count = ['1. ', '2. ', '3. ', '4. ', '5. ', '6. ', '7. ', '8. ', '9. ', '10. '];

	function sound_player(sound_id){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on("complete", function(){
            $nextBtn.show(0);
        });
    }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		
		if(countNext==0){
            sound_player("sound_1")
		}
		else{
			var parent = $(".opt-container");
			var divs = parent.children();
			while (divs.length) {
				parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
			for(var i=0; i<4; i++){
				$('.option-label').eq(i).prepend(' '+(i+1)+'.\t');
			}
			$('#ques-no').html(ques_count[countNext-1]);
			$('.center-image').attr('src', preload.getResult(content[countNext].ques_img).src);
			$('.correct-icon').attr('src', preload.getResult('correct').src);
			$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
			
			var sendDataArr = null;
			mcqClickType1(".option-container", 'class1', scoring, 'correct-ans', 'incorrect-ans', $nextBtn, sendDataArr);
		}
		$prevBtn.hide(0);
	}
	
	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		navigationcontroller();
		generalTemplate();
	}

	$nextBtn.on('click', function() {
		countNext++;
		if(countNext>1) scoring.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});


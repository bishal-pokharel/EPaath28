var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text",
		}],
		
		uppertextblockadditionalclass: 'side-text',
		uppertextblock:[{
			textdata: data.string.p4text2,
			textclass: "fade-in-1",
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
		},{
			textdata: data.string.p4text3,
			textclass: "fade-in-1",
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
		},{
			textdata: data.string.p4text4,
			textclass: "fade-in-1",
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
		}],
		svgblock: [{
			svgblock: 'flowchart',
		}]
	},
	
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text-2",
		},{
			textdata: data.string.p4text5,
			textclass: "text-1",
		}],
		
		svgblock: [{
			svgblock: 'flowchart-1',
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text-2",
		},{
			textdata: data.string.p4text6,
			textclass: "text-1",
		}],
		
		svgblock: [{
			svgblock: 'flowchart-1',
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text",
		}],
		diydata: data.string.p4text7,
		quesdata: data.string.p4text10,
		quesdata2: data.string.p4text8,
		options: [{
			mc_class: "class1",
			optiondata: data.string.p4text10a,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text10b,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text10c,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text10d,
		}],
		svgblock: [{
			svgblock: 'flowchart',
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text",
		}],
		diydata: data.string.p4text7,
		quesdata: data.string.p4text11,
		quesdata2: data.string.p4text8,
		options: [{
			mc_class: "class1",
			optiondata: data.string.p4text11a,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text11b,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text11c,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text11d,
		}],
		svgblock: [{
			svgblock: 'flowchart',
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		extratextblock:[{
			textdata: data.string.p4text1,
			textclass: "flowchart-text",
		}],
		diydata: data.string.p4text7,
		quesdata: data.string.p4text12,
		quesdata2: data.string.p4text8,
		options: [{
			mc_class: "class1",
			optiondata: data.string.p4text12a,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text12b,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text12c,
		},
		{
			mc_class: "class2",
			optiondata: data.string.p4text12d,
		}],
		svgblock: [{
			svgblock: 'flowchart',
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-2',
		extratextblock:[{
			textdata: data.string.p4text13,
			textclass: "lasttext",
			datahighlightflag : true,
			datahighlightcustomclass : 'font-blue',
		},{
			textdata: data.string.p4text14,
			textclass: "text-know",
		},{
			textdata: data.string.p4text15,
			textclass: "tag tag-1 zoom-fade",
		},{
			textdata: data.string.p4text16,
			textclass: "tag tag-2 zoom-fade",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "zoom-spin piechart",
					imgsrc : '',
					imgid : 'piechart'
				},{
					imgclass : "zoom-fade linegraph",
					imgsrc : '',
					imgid : 'linegraph'
				},{
					imgclass : "squirrel",
					imgsrc : '',
					imgid : 'squirrel'
				}
			]
		}]
	}
];


$(function () { 
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "flowchart", src: imgpath+'flowchart.svg', type: createjs.AbstractLoader.IMAGE},
			
			{id: "squirrel", src: imgpath+'squirrel.png', type: createjs.AbstractLoader.IMAGE},
			{id: "piechart", src: imgpath+'piechart.png', type: createjs.AbstractLoader.IMAGE},
			{id: "linegraph", src: imgpath+'linegraph.png', type: createjs.AbstractLoader.IMAGE},
			
			{id: "correct", src: 'images/correct.png', type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: 'images/wrong.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_0a", src: soundAsset+"p4_s0_1.ogg"},
			{id: "sound_1", src: soundAsset+"p4_s1.ogg"},
			{id: "sound_1a", src: soundAsset+"p4_s1_1.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p4_s3.ogg"},
			// {id: "sound_3a", src: soundAsset+"p4_s3_1.ogg"},
			{id: "sound_4_1", src: soundAsset+"s4_p4_1.ogg"},
			{id: "sound_4_2", src: soundAsset+"s4_p4_2.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p6.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		
		switch(countNext) {
			case 0:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play('sound_0');
				current_sound.play();
				current_sound.on("complete", function(){
					sound_nav('sound_0a');
				});
				var s = Snap('#flowchart');
				var box=[], arrow=[];
				var svg = Snap.load(preload.getResult('flowchart').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					box.push(Snap.select('#box1'));
					box.push(Snap.select('#box2'));
					box.push(Snap.select('#box3a'));
					box.push(Snap.select('#box3b'));
					box.push(Snap.select('#box4a'));
					box.push(Snap.select('#box4b'));
					
					arrow.push(Snap.select('#a1'));
					arrow.push(Snap.select('#a2'));
					arrow.push(Snap.select('#a3'));
					arrow.push(Snap.select('#a4'));
					arrow.push(Snap.select('#a5'));
					
					timeoutvar=setTimeout(function(){
						hl_box();
					}, 8800);
					timeoutvar=setTimeout(function(){
						hl_arrow();
					}, 9800);
					timeoutvar=setTimeout(function(){
						hl_remove();
					}, 10800);
					
					function hl_box(){
						for(var i=0; i<6; i++){
							box[i].addClass('hl-bar');
						}
						for(var i=0; i<5; i++){
							arrow[i].removeClass('hl-bar');
						}
					}
					function hl_arrow(){
						for(var i=0; i<6; i++){
							box[i].removeClass('hl-bar');
						}
						for(var i=0; i<5; i++){
							arrow[i].addClass('hl-bar');
						}
					}
					$('.fc-box').hover(hl_box, hl_remove);
					$('.fc-arr').hover(hl_arrow, hl_remove);
					function hl_remove(){
						for(var i=0; i<6; i++){
							box[i].removeClass('hl-bar');
						}
						for(var i=0; i<5; i++){
							arrow[i].removeClass('hl-bar');
						}
					}
				} );
				break;
			case 1:
				$prevBtn.show(0);
				current_sound = createjs.Sound.play('sound_1');
				current_sound.play();
				current_sound.on("complete", function(){
					sound_nav('sound_1a');
					$('.step-1').css('animation-delay','9s');
					$('.step-2').css('animation-delay','10.9s');
					$('.step-3').css('animation-delay','13s');
					$('.step-4').css('animation-delay','16.5s');
					$('.step-5').css('animation-delay','20s');
					$('.step-6').css('animation-delay','23s');
				});
				var s = Snap('#flowchart-1');
				var steps=[], arrow = [];
				var svg = Snap.load(preload.getResult('flowchart').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					steps.push(Snap.select('#step1'));
					steps.push(Snap.select('#step2'));
					steps.push(Snap.select('#step3a'));
					steps.push(Snap.select('#step3b'));
					steps.push(Snap.select('#step4a'));
					steps.push(Snap.select('#step4b'));
					
					arrow.push(Snap.select('#a1'));
					arrow.push(Snap.select('#a2'));
					arrow.push(Snap.select('#a3'));
					arrow.push(Snap.select('#a4'));
					arrow.push(Snap.select('#a5'));
					for (var i=0; i<6; i++){
						steps[i].addClass('step-'+(i+1)+ ' fadeIn animflow');
					}
					for (var i=0; i<5; i++){
						arrow[i].addClass('step-'+(i+2)+ ' fadeIn animflow');
					}
				});
				break;
			case 2:
				$prevBtn.show(0);
				sound_nav('sound_2');
				var s = Snap('#flowchart-1');
				var svg = Snap.load(preload.getResult('flowchart').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				});
				break;
			case 3:
                play_sound_sequence(["sound_4_1","sound_4_2"],false)
                checkans();
                break;
			case 4:
                sound_player("sound_5");
                checkans();
                break;
			case 5:
                sound_player("sound_6");
                checkans();
				break;
			case 6:
				$prevBtn.show(0);
				sound_nav('sound_3');
				$('.linegraph, .tag-1').css('animation-delay', '7300ms');
				$('.zoom-spin, .tag-2').css('animation-delay', '9300ms');
				break;
			default:
				$prevBtn.show(0);
				$nextBtn.show(0);
				break;
		}
	}
	function checkans(){
        $prevBtn.show(0);
        $('.correct-icon').attr('src', preload.getResult('correct').src);
        $('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
        var s = Snap('#flowchart');
        var svg = Snap.load(preload.getResult('flowchart').src, function ( loadedFragment ) {
            s.append(loadedFragment);
        });
        var parent = $(".opt-container");
        var divs = parent.children();
        while (divs.length) {
            parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
        }
        for(var i=0; i<4; i++){
            $('.opt-no').eq(i).html((i+1)+'. ');
        }
        timeoutvar= setTimeout(function(){
            showHint(countNext);
        }, 5000);
        $(".main-container").click(function(){
            if($(this).hasClass("class1")){
                $(".li-item").css('pointer-events','none');
                play_correct_incorrect_sound(1);
                $(this).addClass('correct-ans');
                $(this).find('.correct-icon').show(0);
                nav_button_controls(0);
            }
            else{
                play_correct_incorrect_sound(0);
                $(this).addClass('incorrect-ans');
                // showHint(countNext);
                $(this).find('.incorrect-icon').show(0);
            }
        });
    }
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

    function play_sound_sequence(soundarray, navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                    play_sound_sequence(soundarray, navflag);
            }else{
                if(navflag)
                    navigationcontroller(countNext,$total_page);
            }

        });
    }
	
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	}); 

});

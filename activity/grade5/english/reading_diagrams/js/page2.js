var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p2text1,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-1',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girl-1",
					imgsrc : '',
					imgid : 'girl-1'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		
		speechbox:[{
			speechbox: 'sp-3new',
			textdata : data.string.p2text2,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
			]
		}],
		
		svgblock: [{
			svgblock: 'graph',
		},{
			svgblock: 'graph-1',
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text3,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
			]
		}],
		
		svgblock: [{
			svgblock: 'graph',
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text4,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},
			]
		}],
		svgblock: [{
			svgblock: 'graph',
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text5,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},{
					imgclass : "hand",
					imgsrc : '',
					imgid : 'hand'
				}
			]
		}],
		svgblock: [{
			svgblock: 'graph',
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text6,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				},{
					imgclass : "hand",
					imgsrc : '',
					imgid : 'hand'
				}
			]
		}],
		svgblock: [{
			svgblock: 'graph',
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text7,
			imgclass: '',
			textclass : '',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}],
		svgblock: [{
			svgblock: 'graph',
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-3',
			textdata : data.string.p2text8,
			imgclass: '',
			textclass : 'italics',
			imgid : 'tb-3',
			imgsrc: '',
			datahighlightflag : true,
			datahighlightcustomclass : 'bold',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'class'
				},{
					imgclass : "girl-2",
					imgsrc : '',
					imgid : 'girl-2'
				}
			]
		}],
		svgblock: [{
			svgblock: 'graph',
		}]
	},
];


$(function () { 
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	var preload;
	var timeoutvar = null;
	var current_sound;
	
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "class", src: imgpath+"class.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hand", src: imgpath+"hand.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-1", src: imgpath+"mala.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"mala2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-gif", src: imgpath+"mala-talk.gif", type: createjs.AbstractLoader.IMAGE},
			
			{id: "graph", src: imgpath+"graph.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "graph-1", src: imgpath+"graph-sketch.svg", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-1", src: 'images/textbox/white/tr-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: imgpath+"textbox.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	
	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	
	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		
		switch(countNext) {
			case 0:
				conversation('.speechbox', 'sound_0');
				break;
			case 1:
				$prevBtn.show(0);
				var s = Snap('#graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				} );
				var s1 = Snap('#graph-1');
				var paths=[];
				var svg = Snap.load(preload.getResult('graph-1').src, function ( loadedFragment ) {
					s1.append(loadedFragment);
					//to resive whole svg
					paths.push(Snap.select('#yellow'));
					paths.push(Snap.select('#orange'));
					paths.push(Snap.select('#red'));
					paths.push(Snap.select('#purple'));
					paths.push(Snap.select('#blue'));
					
					paths[0].addClass('scribble-1');
					paths[1].addClass('scribble-2');
					paths[2].addClass('scribble-3');
					paths[3].addClass('scribble-4');
					paths[4].addClass('scribble-5');
					timeoutvar = setTimeout(function(){
						$('#graph-1').addClass('fade-out-slow');
					}, 2300);
					timeoutvar = setTimeout(function(){
						conversation('.sp-3new', 'sound_1');
					}, 3200);
				} );
				break;
			case 2:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_2');
				var s = Snap('#graph');
				var num=[], bar=[];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					for(var i=1; i<6; i++){
						var bar_str = '#bar'+i;
						var class_str = '#num'+i;
						bar.push(Snap.select(bar_str));
						num.push(Snap.select(class_str));
					}
						num.push(Snap.select('#num6'));
					for(var i=0; i<5; i++){
						num[i].addClass('hl-bar');
						bar[i].addClass('hl-bar');
					}
					num[5].addClass('hl-bar');
				} );
				break;
			case 3:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_3');
				var s = Snap('#graph');
				var num=[], bar=[], classes=[];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				} );
				break;
			case 4:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_4');
				var s = Snap('#graph');
				var num=[], bar=[], classes=[];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				} );
				timeoutvar = setTimeout(function(){
					$('.hand').addClass('hand-move-2');
				}, 4500);
				timeoutvar = setTimeout(function(){
					$('.hand').removeClass('hand-move-2');
					$('.hand').addClass('hand-move-1');
				}, 9000);
				break;
			case 5:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_5');
				var s = Snap('#graph');
				var num=[], classes=[];
				var numstu;
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					numstu = Snap.select('#num');
					for(var i=1; i<6; i++){
						var num_str = '#num'+i;
						var class_str = '#class'+i;
						num.push(Snap.select(num_str));
						classes.push(Snap.select(class_str));
					}
					num.push(Snap.select('#num6'));
					timeoutvar=setTimeout(function(){
						hl_classes();
					}, 1000);
					timeoutvar=setTimeout(function(){
						hl_nums();
					}, 4500);
					timeoutvar=setTimeout(function(){
						hl_remove();
					}, 9000);
					function hl_nums(){
						for(var i=0; i<5; i++){
							num[i].addClass('hl-bar');
							classes[i].removeClass('hl-bar');
						}
						num[5].addClass('hl-bar');
						numstu.addClass('hl-bar');
					}
					function hl_classes(){
						for(var i=0; i<5; i++){
							num[i].removeClass('hl-bar');
							classes[i].addClass('hl-bar');
						}
						num[5].removeClass('hl-bar');
						numstu.removeClass('hl-bar');
					}
					function hl_remove(){
						for(var i=0; i<5; i++){
							num[i].removeClass('hl-bar');
							classes[i].removeClass('hl-bar');
						}
						num[5].removeClass('hl-bar');
						// numstu.removeClass('hl-bar');
					}
				} );
				break;
			case 6:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_6');
				var s = Snap('#graph');
				var num=[], bar=[], classes=[];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
				} );
				break;
			case 7:
				$prevBtn.show(0);
				conversation('.sp-3', 'sound_7');
				var s = Snap('#graph');
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					var bar = Snap.select('#bar1');
					var class1 = Snap.select('#class1');
					bar.addClass('hl-bar');
					class1.addClass('hl-bar');
				} );
				break;
			default:
				$prevBtn.show(0);
				conversation('.textboxsp', 'sound_'+countNext);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function conversation(speech_class, sound_data){
		$(speech_class).fadeIn(1000, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
				$(speech_class).click(function(){
					sound_player(sound_data);
				});
			});
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	}); 

});

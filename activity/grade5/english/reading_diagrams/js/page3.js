var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		extratextblock:[{
			textdata: data.string.p3text1,
			textclass: "diy-text",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'diy'
				},
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'graph',
		}],
		diydata: data.string.p3text2,
		quesdata: data.string.p3text3,
		options: [{
			option_class: "class1",
			optiondata: data.string.p3text3a,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text3b,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text3c,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text3d,
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'graph',
		}],
		diydata: data.string.p3text2,
		quesdata: data.string.p3text4,
		options: [{
			option_class: "class1",
			optiondata: data.string.p3text4a,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text4b,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text4c,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text4d,
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'graph',
		}],
		diydata: data.string.p3text2,
		quesdata: data.string.p3text5,
		options: [{
			option_class: "class1",
			optiondata: data.string.p3text5a,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text5b,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text5c,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text5d,
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'graph',
		}],
		diydata: data.string.p3text2,
		quesdata: data.string.p3text6,
		options: [{
			option_class: "class1",
			optiondata: data.string.p3text6a,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text6b,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text6c,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text6d,
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-1',
		svgblock: [{
			svgblock: 'graph',
		}],
		diydata: data.string.p3text2,
		quesdata: data.string.p3text7,
		options: [{
			option_class: "class1",
			optiondata: data.string.p3text7a,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text7b,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text7c,
		},
		{
				option_class: "class2",
				optiondata: data.string.p3text7d,
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "diy", src: imgpath+"diy.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "graph", src: imgpath+"graph.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s3_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		put_image(content, countNext);

		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
			break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				sound_player("sound_"+countNext)
				var s = Snap('#graph');
				var num=[], bar=[], classes=[];
				var svg = Snap.load(preload.getResult('graph').src, function ( loadedFragment ) {
					s.append(loadedFragment);
					//to resive whole svg
					num.push(Snap.select('#num'));
					num.push(Snap.select('#num1'));
					num.push(Snap.select('#num2'));
					num.push(Snap.select('#num3'));
					num.push(Snap.select('#num4'));
					num.push(Snap.select('#num5'));
					num.push(Snap.select('#num6'));
					for(var i=1; i<6; i++){
						var bar_str = '#bar'+i;
						var class_str = '#class'+i;
						bar.push(Snap.select(bar_str));
						classes.push(Snap.select(class_str));
					}
				} );
				function showHint(quesNo){
					switch(quesNo){
						case 1:
							num[6].removeClass('hint-blink');
							setTimeout(function(){
								num[6].addClass('hint-blink');
							},1);
							break;
						case 2:
							bar[2].removeClass('hint-blink');
							setTimeout(function(){
								bar[2].addClass('hint-blink');
							},1);
							break;
						case 3:
							bar[3].removeClass('hint-blink');
							setTimeout(function(){
								bar[3].addClass('hint-blink');
							},1);
							break;
						case 4:
							num[3].removeClass('hint-blink');
							setTimeout(function(){
								num[3].addClass('hint-blink');
							},1);
							break;
						case 5:
							num[4].removeClass('hint-blink');
							setTimeout(function(){
								num[4].addClass('hint-blink');
							},1);
							break;
						default:
							break;
					}
				}

				var parent = $(".opt-container");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				timeoutvar= setTimeout(function(){
					showHint(countNext);
				}, 5000);
				$('.correct-icon').attr('src', preload.getResult('correct').src);
				$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);
				$(".option-container").click(function(){
					if($(this).hasClass("class1")){
						$(".option-container").css('pointer-events','none');
			 			play_correct_incorrect_sound(1);
						$(this).addClass('correct-ans');
						$(this).parent().children('.correct-icon').show(0);
						wrong_clicked = 0;
						nav_button_controls(0);
					}
					else{
			 			play_correct_incorrect_sound(0);
						$(this).addClass('incorrect-ans');
						showHint(countNext);
						$(this).parent().children('.incorrect-icon').show(0);
					}
				});
				break;
			default:
				$prevBtn.show(0);
				sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_data){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_data);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

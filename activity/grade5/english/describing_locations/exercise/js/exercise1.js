var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";
var sound_1 = new buzz.sound((soundAsset + "ex_instr.ogg"));
var current_sound = sound_1;

var content=[
	//slide 0
	{
		contentblockadditionalclass: 'bg1',
		extratextblock : [
        {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
         },
		{
			textdata : data.string.etext1,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}
		],
		hintimageblock:[
		{
			imgsrc : imgpath + "1.png",
		}],
		optionsblockadditionalclass: 'img_options tb1',
		optionsblock : [{
			textdata : data.string.e1ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e1ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e1ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e1ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 1
	{
		contentblockadditionalclass: 'bg2',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext2,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "2.png",
		}],
		optionsblockadditionalclass: 'img_options tb2',
		optionsblock : [{
			textdata : data.string.e2ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e2ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e2ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e2ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 2
	{
		contentblockadditionalclass: 'bg3',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext3,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "3.png",
		}],
		optionsblockadditionalclass: 'img_options tb3',
		optionsblock : [{
			textdata : data.string.e3ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e3ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e3ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e3ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 3
	{
		contentblockadditionalclass: 'bg4',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext4,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "4.png",
		}],
		optionsblockadditionalclass: 'img_options tb4',
		optionsblock : [{
			textdata : data.string.e4ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e4ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e4ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e4ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 4
	{
		contentblockadditionalclass: 'bg5',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext5,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "5.png",
		}],
		optionsblockadditionalclass: 'img_options tb5',
		optionsblock : [{
			textdata : data.string.e5ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e5ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e5ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e5ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 5
	{
		contentblockadditionalclass: 'bg6',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext6,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "6.png",
		}],
		optionsblockadditionalclass: 'img_options tb6',
		optionsblock : [{
			textdata : data.string.e6ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e6ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e6ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e6ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 6
	{
		contentblockadditionalclass: 'bg7',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext7,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "7.png",
		}],
		optionsblockadditionalclass: 'img_options tb7',
		optionsblock : [{
			textdata : data.string.e7ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e7ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e7ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e7ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 7
	{
		contentblockadditionalclass: 'bg8',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext8,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "8.png",
		}],
		optionsblockadditionalclass: 'img_options tb8',
		optionsblock : [{
			textdata : data.string.e8ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e8ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e8ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e8ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 8
	{
		contentblockadditionalclass: 'bg9',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext9,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "9.png",
		}],
		optionsblockadditionalclass: 'img_options tb9 tb-large-2',
		optionsblock : [{
			textdata : data.string.e9ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e9ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e9ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e9ansd,
			textclass : 'class-4 options'
		}],
	},
	//slide 9
	{
		contentblockadditionalclass: 'bg10',
		extratextblock : [
            {
                textdata : data.string.clickmsg,
                textclass : 'clickmsg my_font_big',
            },
		{
			textdata : data.string.etext10,
			textclass : 'instruction my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'blank-space'
		}],
		hintimageblock:[
		{
			imgsrc : imgpath + "10.png",
		}],
		optionsblockadditionalclass: 'img_options tb10',
		optionsblock : [{
			textdata : data.string.e10ansa,
			textclass : 'class-1 options corans'
		},
		{
			textdata : data.string.e10ansb,
			textclass : 'class-2 options'
		},
		{
			textdata : data.string.e10ansc,
			textclass : 'class-3 options'
		},
		{
			textdata : data.string.e10ansd,
			textclass : 'class-4 options'
		}],
	}
];

// content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);

    function sound_player(sound_data,navigate){
        current_sound.stop();
        current_sound = sound_data;
        current_sound.play();
        current_sound.bind('ended', function () {
            navigate?nav_button_controls(500):"";
        });
    }

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		countNext == 0? sound_player(sound_1,false):"";
		//randomize options
		var positions = [1,2,3,4];
		positions.shufflearray();
		for(var i=1; i<=4; i++){
			$('.class-'+i).addClass('pos-'+positions[i-1]);
		}
		$('.pos-1').html('d)  ' + $('.pos-1').html());
		$('.pos-2').html('c)  ' + $('.pos-2').html());
		$('.pos-3').html('b)  ' + $('.pos-3').html());
		$('.pos-4').html('a)  ' + $('.pos-4').html());
		// positions.shufflearray();

		$('.instruction').html( parseInt(countNext+1)+'.'+$('.instruction').html());
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("corans")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(this).addClass("corClass");
				$(".options").css('pointer-events', 'none');
				$('.blank-space').html($(this).html().substring(4));
				play_correct_incorrect_sound(1);
				// $(this).css({
				// 	'color': '#CFF9B9',
				// });
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).addClass("incorClass");
				$(this).css({
					// 'color': '#FFAFBC',
					// 'text-decoration': 'line-through',
					'pointer-events': 'none',
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

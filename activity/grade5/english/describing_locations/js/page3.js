var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p3_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p3_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p3_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p3_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p3_s6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p3_s7.ogg"));

var sound_1a = new buzz.sound((soundAsset + "p3_s1_1.ogg"));
var sound_3a = new buzz.sound((soundAsset + "p3_s3_1.ogg"));
var sound_4a = new buzz.sound((soundAsset + "p3_s4_1.ogg"));
var sound_5a = new buzz.sound((soundAsset + "p3_s5_1.ogg"));
var sound_6a = new buzz.sound((soundAsset + "p3_s6_1.ogg"));
var sound_7a = new buzz.sound((soundAsset + "p3_s7_1.ogg"));

var sound_1b = new buzz.sound((soundAsset + "p3_s1_2.ogg"));
var sound_3b = new buzz.sound((soundAsset + "p3_s3_2.ogg"));
var sound_4b = new buzz.sound((soundAsset + "p3_s4_2.ogg"));
var sound_5b = new buzz.sound((soundAsset + "p3_s5_2.ogg"));
var sound_6b = new buzz.sound((soundAsset + "p3_s6_2.ogg"));
var sound_7b = new buzz.sound((soundAsset + "p3_s7_2.ogg"));

var sound_arr = [
					[sound_1, sound_1a, sound_1b],
					[sound_3, sound_3a, sound_3b],
					[sound_4, sound_4a, sound_4b],
					[sound_5, sound_5a, sound_5b],
					[sound_6, sound_6a, sound_6b],
					[sound_7, sound_7a, sound_7b],
				];

var current_sound = sound_1;

var helper = [
				[data.string.prep1, data.string.shop1, data.string.helper1],
				[data.string.prep2, data.string.shop2, data.string.helper2],
				[data.string.prep3, data.string.shop3, data.string.helper3],
				[data.string.prep4, data.string.shop4, data.string.helper4],
				[data.string.prep5, data.string.shop5, data.string.helper5],
				[data.string.prep6, data.string.shop6, data.string.helper6],
			];


var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-diy',
	
		extratextblock : [{
			textdata : data.string.diytext,
			textclass : 'diy-text chelseamarket'
		},{
			textdata : '',
			textclass : 'bg-diy-top-1'
		},{
			textdata : '',
			textclass : 'bg-diy-bottom-1'
		}]
	},
	
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text1,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj right',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj helper-class',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	},
	
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text4,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj right',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj helper-class',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	},
	
	
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text5,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj right',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj helper-class',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	},
	
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text6,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj helper-class',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj right',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj helper-class',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	},
	
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text7,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj helper-class',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj ',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj right',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	},
	
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-2',
		
		fairytextblockadditionalclass: 'my_font_medium',
		fairytextblock: [
			{
				textdata : data.string.p3text8,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "policestation.png",
				},
				{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "temple.png",
				},
				{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "cinema.png",
				},
				{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "toyshop.png",
				},
				{
					imgclass: 'house-5 map-obj helper-class',
					imgsrc: imgpath + "bookshop.png",
				},
				{
					imgclass: 'house-6 map-obj right',
					imgsrc: imgpath + "teashop.png",
				},
				{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "flowershop.png",
				},
				{
					imgclass: 'stop-1 map-obj',
					imgsrc: imgpath + "busstop.png",
				},
				{
					imgclass: 'park-1 map-obj',
					imgsrc: imgpath + "park.png",
				},
				{
					imgclass: 'hospital-1 map-obj',
					imgsrc: imgpath + "hospital.png",
				},
				{
					imgclass: 'car car-1 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-2 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-3 map-obj',
					imgsrc: imgpath + "car.png",
				},
				{
					imgclass: 'car car-4 map-obj',
					imgsrc: imgpath + "car.png",
				}
			]
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	
	var current_char = null;
	var $total_page = content.length;
	var wrong_counter = 0;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
                play_diy_audio();
				nav_button_controls(100);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				wrong_counter = 0;
				$prevBtn.show(0);
				sound_player(sound_arr[countNext-1][0]);
				mapclick(countNext-1);
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function mapclick(index){
		$('.map-obj:not(.right)').click(function(){
			// play_correct_incorrect_sound(0);
			sound_player(sound_arr[index][2]);
			$('.fairytextblock>p').html(data.string.p3text3);
			$('.shopname').html(helper[index][1]);
			$('.preposition').html(helper[index][0]);
			$('.helper').html(helper[index][2]);
			$(this).css('pointer-events', 'none');
			if(wrong_counter<2){
				$('.helper-class').addClass('hl-helper');
			} else if(wrong_counter==2){
				$('.right').addClass('hl-right');
			}
			wrong_counter++;
		});
		$('.right').click(function(){
			// play_correct_incorrect_sound(1);
			sound_player(sound_arr[index][1],true);
			$('.fairytextblock>p').html(data.string.p3text2);
			$('.shopname').html(helper[index][1]);
			$('.preposition').html(helper[index][0]);
			$('.helper').html(helper[index][2]);
			$('.map-obj').css('pointer-events', 'none');
			// nav_button_controls(0);
		});
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}
	function sound_player(sound_data,navigate){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
        current_sound.bindOnce('ended', function() {
        	navigate?nav_button_controls(100):"";
        });
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});
	
	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

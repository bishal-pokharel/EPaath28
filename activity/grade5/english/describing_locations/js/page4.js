var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p4_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p4_s0_1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p4_s1.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p4_s2.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p4_s3.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p4_s4.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p4_s5.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p4_s6.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p4_s7.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p4_s8.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p4_s9.ogg"));
var sound_12 = new buzz.sound((soundAsset + "p4_s10.ogg"));
var sound_13 = new buzz.sound((soundAsset + "p4_s11.ogg"));
var current_sound = sound_1;

var content = [
	
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		extratextblock : [{
			textdata : data.string.p4text1,
			textclass : 'template-dialougebox2-top-yellow dg-1'
		},
		{
			textdata : data.string.p4text2,
			textclass : 'template-dialougebox2-top-flipped-yellow dg-2'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'boy1',
					imgsrc: imgpath + "boy1.png",
				},
				{
					imgclass: 'boy2',
					imgsrc: imgpath + "boy2.png",
				}
			]
		}]
	},
	
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		extratextblock : [{
			textdata : data.string.p4text3,
			textclass : 'template-dialougebox2-top-yellow dg-1'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'boy1',
					imgsrc: imgpath + "boy1.png",
				}
			]
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text4,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text5,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-2 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text6,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-3 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text7,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-4 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj hl-map-object',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text8,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-5 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj hl-map-object',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text9,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-6 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text10,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-7 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj hl-map-object',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text11,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite pos-7 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj hl-map-object',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-3',
		
		uppertextblockadditionalclass: 'instructionblock happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p4text12,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		spriteblock : [{
			divclass : 'mansprite man-move-8 man-1',
			spriteclass : ''
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'house-1 map-obj',
					imgsrc: imgpath + "healthpost.png",
				},{
					imgclass: 'house-2 map-obj',
					imgsrc: imgpath + "bookshop01.png",
				},{
					imgclass: 'house-3 map-obj',
					imgsrc: imgpath + "fruitshop.png",
				},{
					imgclass: 'house-4 map-obj',
					imgsrc: imgpath + "policestation01.png",
				},{
					imgclass: 'house-5 map-obj',
					imgsrc: imgpath + "library.png",
				},{
					imgclass: 'house-6 map-obj',
					imgsrc: imgpath + "teashop01.png",
				},{
					imgclass: 'house-7 map-obj',
					imgsrc: imgpath + "school.png",
				},{
					imgclass: 'house-8 map-obj',
					imgsrc: imgpath + "house01.png",
				},{
					imgclass: 'house-9 map-obj',
					imgsrc: imgpath + "house02.png",
				},{
					imgclass: 'house-10 map-obj',
					imgsrc: imgpath + "house03.png",
				},{
					imgclass: 'house-11 map-obj',
					imgsrc: imgpath + "tailorshop.png",
				},{
					imgclass: 'cowshed map-obj',
					imgsrc: imgpath + "cowshed.png",
				},{
					imgclass: 'tree map-obj',
					imgsrc: imgpath + "pipaltree.png",
				},{
					imgclass: 'garden map-obj hl-map-object',
					imgsrc: imgpath + "garden03.png",
				}
			]
		}]
	},
	
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-green',
		
		extratextblock : [{
			textdata : data.string.p4text13,
			textclass : 'template-dialougebox2-top-flipped-yellow dg-3'
		}],
		
		imageblock : [{
			imagestoshow : [
                {
                    imgclass: 'boy1',
                    imgsrc: imgpath + "boy1.png",
                },
				{
					imgclass: 'boy2',
					imgsrc: imgpath + "boy2.png",
				}
			]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	
	var current_char = null;
	var $total_page = content.length;
	var timeoutvar = null;
	var timeoutvar2 = null;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_1;
				current_sound.play();
				current_sound.bindOnce('ended', function(){
					$('.dg-2').fadeIn(500, function(){
						$('.min-hand').addClass('hand-hl');
						current_sound = sound_2;
						current_sound.play();
						current_sound.bindOnce('ended', function(){
							click_dg('.dg-1', sound_1);
							click_dg('.dg-2', sound_2);
							nav_button_controls(100);
						});
					});
				});
				break;
			case 1:
				$prevBtn.show(0);
				sound_and_nav(sound_3, click_dg, '.dg-1');
				break;
			case 2:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_4;
				current_sound.play();
				stop_walk(0);
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-1');
					$('.spriteclass').addClass('walking-left');
					change_direction(1000, 'walking-up');
					stop_walk(2000,"walkingup-fixed");
					nav_button_controls(2000);
				});
				break;
			case 3:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_5;
				current_sound.play();
				stop_walk(0,"walkingup-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-21');
					$('.spriteclass').addClass('walking-up');
					$('.spriteclass').removeClass('walkingup-fixed');
					change_direction(4250, 'walking-right');
					stop_walk(5000,"walkingright-fixed");
					nav_button_controls(2000);
				});
				break;
			case 4:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_6;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-31');
					$('.spriteclass').addClass('walking-right');
                    stop_walk(2000);
					nav_button_controls(2000);
				});
				break;
			case 5:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_7;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-41');
					$('.spriteclass').addClass('walking-right');
                    stop_walk(1000);
					nav_button_controls(2000);
				});
				break;
			case 6:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_8;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-51');
					$('.spriteclass').addClass('walking-right');
					stop_walk(1000);
					nav_button_controls(2000);
				});
				break;
			case 7:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_9;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-61');
					$('.spriteclass').addClass('walking-right');
					$('.spriteclass').removeClass("walkingright-fixed");
					stop_walk(3000);
					change_direction(2400, 'walking-down');
					nav_button_controls(2000);
				});
				break;
			case 8:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_10;
				current_sound.play();
				stop_walk(0);
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-71');
					$('.spriteclass').addClass('walking-down');
					stop_walk(4000,"walkingright-fixed");
					nav_button_controls(2000);
				});
				break;
			case 9:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_11;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					stop_walk(4000);
					nav_button_controls(0);
				});
				break;
			case 10:
				$prevBtn.show(0);
				current_sound.stop();
				current_sound = sound_12;
				current_sound.play();
				stop_walk(0,"walkingright-fixed");
				current_sound.bindOnce('ended', function(){
					$('.spritediv').addClass('man-move-81');
					$('.spriteclass').addClass('walking-right');
					stop_walk(1500);
					nav_button_controls(2000);
				});
				break;
			case 11:
				$(".boy1").css("left","16%");
				$prevBtn.show(0);
				sound_and_nav(sound_13, click_dg, '.dg-3');
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(2000);
				break;
		}
	}
	
	function stop_walk(delay,stophere){
		clearTimeout(timeoutvar);
		timeoutvar = setTimeout(function(){
			$('.spriteclass').removeClass('walking-right');
			$('.spriteclass').removeClass('walking-up');
			$('.spriteclass').removeClass('walking-down');
			$('.spriteclass').removeClass('walking-left');
			stophere?$('.spriteclass').addClass(stophere):"";

		}, delay);
	}
	function change_direction(delay, class_walk){
		clearTimeout(timeoutvar2);
		timeoutvar2 = setTimeout(function(){
			$('.spriteclass').removeClass('walking-right');
			$('.spriteclass').removeClass('walking-up');
			$('.spriteclass').removeClass('walking-down');
			$('.spriteclass').removeClass('walking-left');
			$('.spriteclass').addClass(class_walk);
		}, delay);
	}

    function nav_button_controls(delay_ms){
        setTimeout(function(){
            if(countNext==0){
                $nextBtn.show(0);
            } else if( countNext>0 && countNext == $total_page-1){
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else{
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        },delay_ms);
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				clearTimeout(timeoutvar2);
				clearTimeout(timeoutvar);
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar2);
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
	});
	
	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

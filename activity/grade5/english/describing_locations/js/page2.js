var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var current_sound = sound_1;

var sound_arr = [sound_2, sound_3, sound_4, sound_5, sound_6, sound_7];

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint happymonkey',
		
		listblock : [{
			listblockadditionalclass: 'p2-list-1 my_font_big',
			headerdata : data.string.p2text1,
			headerclass : 'p2-header my_font_ultra_big happymonkey',
			textdata : data.string.p2text2,
			textclass : 'happymonkey its_hidden t-1',
			text2data : data.string.p2text9,
			text2class : 'happymonkey its_hidden t-8',
			listitem: [
				{
					textclass: 't-2 its_hidden',
					textdata: data.string.p2text3
				},
				{
					textclass: 't-3  its_hidden',
					textdata: data.string.p2text4
				},
				{
					textclass: 't-4 its_hidden',
					textdata: data.string.p2text5
				},
				{
					textclass: 't-5 its_hidden',
					textdata: data.string.p2text6
				},
				{
					textclass: 't-6 its_hidden',
					textdata: data.string.p2text7
				},
				{
					textclass: 't-7 its_hidden',
					textdata: data.string.p2text8
				}
			],
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 current-dir',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}]
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 ',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-2 current-dir',
			roaddiv: 'road-2',
			labeldata: data.string.p2text4,
			house:[
			{
				houseclass: 'dir2-1 h-1',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-2 h-2',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-3 h-3',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-4 h-4',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-5 h-5',
				housedata: data.string.texta
			}, {
				houseclass: 'center-total house-b h-6',
				housedata: data.string.textb
			}]
		}]
	},
	
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 ',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-2 ',
			roaddiv: 'road-2',
			labeldata: data.string.p2text4,
			house:[
			{
				houseclass: 'dir2-1 h-1',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-2 h-2',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-3 h-3',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-4 h-4',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-5 h-5',
				housedata: data.string.texta
			}, {
				houseclass: 'center-total house-b h-6',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-3 current-dir',
			roaddiv: 'road-3',
			labeldata: data.string.p2text8,
			house:[
			{
				houseclass: 'dir3-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir3-2 house-b h-2',
				housedata: data.string.textb
			}]
		},]
	},
	
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 ',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-2 ',
			roaddiv: 'road-2',
			labeldata: data.string.p2text4,
			house:[
			{
				houseclass: 'dir2-1 h-1',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-2 h-2',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-3 h-3',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-4 h-4',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-5 h-5',
				housedata: data.string.texta
			}, {
				houseclass: 'center-total house-b h-6',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-3',
			roaddiv: 'road-3',
			labeldata: data.string.p2text8,
			house:[
			{
				houseclass: 'dir3-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir3-2 house-b h-2',
				housedata: data.string.textb
			}]
		},{
			directioncontainer: 'dir-4 current-dir',
			roaddiv: 'road-1',
			labeldata: data.string.p2text7,
			house:[
			{
				houseclass: 'center-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-2 house-b h-2',
				housedata: data.string.textb
			}]
		}]
	},
	
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 ',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-2 ',
			roaddiv: 'road-2',
			labeldata: data.string.p2text4,
			house:[
			{
				houseclass: 'dir2-1 h-1',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-2 h-2',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-3 h-3',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-4 h-4',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-5 h-5',
				housedata: data.string.texta
			}, {
				houseclass: 'center-total house-b h-6',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-3',
			roaddiv: 'road-3',
			labeldata: data.string.p2text8,
			house:[
			{
				houseclass: 'dir3-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir3-2 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-4',
			roaddiv: 'road-1',
			labeldata: data.string.p2text7,
			house:[
			{
				houseclass: 'center-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-2 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-5 current-dir',
			roaddiv: 'road-5',
			labeldata: data.string.p2text5,
			house:[
			{
				houseclass: 'dir5-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir5-2 house-b h-2',
				housedata: data.string.textb
			}]
		}]
	},
	
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_mint',
		
		extratextblock : [{
			textdata : data.string.p2text1,
			textclass : 'p2-header-2 my_font_ultra_big'
		}],
		
		directionblock:[{
			directioncontainer: 'dir-1 ',
			roaddiv: 'road-1',
			labeldata: data.string.p2text3,
			house:[
			{
				houseclass: 'center-2 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-1 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-2 ',
			roaddiv: 'road-2',
			labeldata: data.string.p2text4,
			house:[
			{
				houseclass: 'dir2-1 h-1',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-2 h-2',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-3 h-3',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-4 h-4',
				housedata: data.string.texta
			},{
				houseclass: 'dir2-5 h-5',
				housedata: data.string.texta
			}, {
				houseclass: 'center-total house-b h-6',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-3',
			roaddiv: 'road-3',
			labeldata: data.string.p2text8,
			house:[
			{
				houseclass: 'dir3-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir3-2 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-4',
			roaddiv: 'road-1',
			labeldata: data.string.p2text7,
			house:[
			{
				houseclass: 'center-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'center-2 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-5',
			roaddiv: 'road-5',
			labeldata: data.string.p2text5,
			house:[
			{
				houseclass: 'dir5-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir5-2 house-b h-2',
				housedata: data.string.textb
			}]
		}, {
			directioncontainer: 'dir-6 current-dir',
			roaddiv: 'road-3',
			labeldata: data.string.p2text6,
			house:[
			{
				houseclass: 'dir6-1 h-1',
				housedata: data.string.texta
			}, {
				houseclass: 'dir6-2 h-2',
				housedata: data.string.texta
			}, {
				houseclass: 'dir6-3 house-b h-3',
				housedata: data.string.textb
			}]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var navtime = null;
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);

		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				sound_player(sound_1,true);
				for(var i=1; i<9; i++){
					if( i>1){
						$('.t-'+i).delay(i*2000+3500).fadeIn(1000);
					} else{
						$('.t-'+i).delay(i*2000).fadeIn(1000);
					}
				}
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
				$prevBtn.show(0);
				sound_player(sound_arr[countNext-1],true);
				var tot_len = $('.current-dir .houseclass').length;
				for(var i=1; i<=tot_len; i++){
					$('.h-'+i).delay((i-1)*1000).fadeIn(1000);
				}
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	
	function sound_player(sound_data,navigate){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
        current_sound.bind('ended', function () {
            navigate?nav_button_controls(500):"";
        });
	}
	
	function nav_button_controls(delay_ms){
		clearTimeout(navtime);
		navtime = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		current_sound.stop();
		switch(countNext){
			default:
				clearTimeout(navtime);
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		current_sound.stop();
		clearTimeout(navtime);
		countNext--;
		templateCaller();
	});
	
	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var current_sound = sound_1;
var sound_arr = [sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_opal',
	
		extratextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title happymonkey'
		}],
        imageblock : [{
            imagestoshow : [
                {
                    imgclass: 'coverpage',
                    imgsrc: imgpath + "cover_page.jpg",
                }
            ]
        }]
	},
	
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg_pani',
		
		extratextblock : [{
			textdata : data.string.p1text1,
			textclass : 'template-dialougebox2-top-yellow dg-1 happymonkey my_font_big'
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'durga',
					imgsrc: imgpath + "durga.png",
				}
			]
		}]
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text2,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple ',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum hl-map-object',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text3,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple ',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden hl-map-object',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text4,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple ',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain hl-map-object',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text5,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple hl-map-object',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4 hl-map-object',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text6,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother hl-map-object',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child hl-map-object',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text7,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1 hl-map-object',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2 hl-map-object',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3 hl-map-object',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	},
	
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'map-1',
		
		uppertextblockadditionalclass: 'bottom-dir my_font_medium happymonkey',
		uppertextblock: [
			{
				textdata : data.string.p1text8,
				textclass : '',
				datahighlightflag: true,
				datahighlightcustomclass: 'bold-ul my_font_big'
			}, 
		],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'temple',
					imgsrc: imgpath + "temple.png",
				},{
					imgclass: 'mother ',
					imgsrc: imgpath + "mother.png",
				},{
					imgclass: 'child ',
					imgsrc: imgpath + "child.png",
				},{
					imgclass: 'shop shop-1',
					imgsrc: imgpath + "bookshop.png",
				},{
					imgclass: 'shop shop-2 hl-map-object',
					imgsrc: imgpath + "toyshop.png",
				},{
					imgclass: 'shop shop-3',
					imgsrc: imgpath + "teashop.png",
				},{
					imgclass: 'shop shop-4',
					imgsrc: imgpath + "flowershop.png",
				},{
					imgclass: 'fountain',
					imgsrc: imgpath + "fountain.png",
				},{
					imgclass: 'garden',
					imgsrc: imgpath + "garden.png",
				},{
					imgclass: 'museum',
					imgsrc: imgpath + "museum.png",
				},{
					imgclass: 'bench bench-01',
					imgsrc: imgpath + "bench.png",
				},{
					imgclass: 'bench bench-02',
					imgsrc: imgpath + "bench.png",
				}
			]
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	
	var current_char = null;
	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				sound_player(sound_1);
				break;
			case 1:
				$prevBtn.show(0);
				sound_and_nav(sound_2, click_dg, '.dg-1');
				break;
			default:
				$prevBtn.show(0);
				sound_player(sound_arr[countNext-2]);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_and_nav(sound_data, clickfunction , a){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			if(typeof clickfunction != 'undefined'){
				clickfunction(a, sound_data);
			}
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		});
	}
	function click_dg(dg_class, audio){
		$(dg_class).click(function(){
			sound_player(audio);
		});
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
        current_sound.bind('ended', function () {
            nav_button_controls(500);
        });
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				current_sound.stop();
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		current_sound.stop();
		countNext--;
		templateCaller();
	});
	
	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques1,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q1ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q1ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q1ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q1ansd,
					}]

			}
		]
	},
	//ex2
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques2,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q2ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q2ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q2ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q2ansd,
					}]

			}
		]
	},
	//ex3
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques3,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q3ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q3ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q3ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q3ansd,
					}]

			}
		]
	},
	//ex4
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques4,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q4ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q4ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q4ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q4ansd,
					}]

			}
		]
	},
	//ex5
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques5,
				sentdata: data.string.sentques5,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q5ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q5ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q5ansc,
					},{
				    optclass:"",
						forshuffle: "class4",
						optdata: data.string.q5ansd,
					}]

			}
		]
	},
	//ex6
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques6,
        sentdata: data.string.sentques6,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q6ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q6ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q6ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q6ansd,
					}]

			}
		]
	},
	//ex7
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques7,
        sentdata: data.string.sentques7,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q7ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q7ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q7ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q7ansd,
					}]

			}
		]
	},
	//ex8
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques8,
        sentdata: data.string.sentques8,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q8ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q8ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q8ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q8ansd,
					}]

			}
		]
	},
	//ex9
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques9,
        sentdata: data.string.sentques9,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q9ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q9ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q9ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q9ansd,
					}]

			}
		]
	},
	//ex10
	{
		exerciseblock: [
			{
        datahighlightflag: true,
        datahighlightcustomclass: "hightext1",
				textdata: data.string.exques10,
        sentdata: data.string.sentques10,

				exeoptions: [
					{
            optclass:"correct",
						forshuffle: "class1",
						optdata: data.string.q10ansa,
					},
					{
            optclass:"",
						forshuffle: "class2",
						optdata: data.string.q10ansb,
					},
					{
            optclass:"",
						forshuffle: "class3",
						optdata: data.string.q10ansc,
					},
					{
            optclass:"",
						forshuffle: "class4",
						optdata: data.string.q10ansd,
					}]

			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new LampTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
    texthighlight($board);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
    $refreshBtn.css('pointer-events','none');
		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

    $(".optionscontainer").click(function(){
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("correct")){
						play_correct_incorrect_sound(1);
						if(wrngClicked == false){
							testin.update(true);
						}
						$(this).children(".corctopt").show(0);
          	$(this).addClass('corrects');
            $(" .optionscontainer").css("pointer-events","none");
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					  else{
						testin.update(false);
						play_correct_incorrect_sound(0);
            $(this).addClass('incorrect');
            $(this).css("pointer-events","none");
						$(this).children(".wrngopt").show(0);
						wrngClicked = true;
					}
				}
			});


		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

/*===============================================
  =            data highlight function            =
  ===============================================*/
  function texthighlight($highlightinside){
     //check if $highlightinside is provided
     typeof $highlightinside !== "object" ?
     alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
     null ;

     var $alltextpara = $highlightinside.find("*[data-highlight='true']");
     var stylerulename;
     var replaceinstring;
     var texthighlightstarttag;
     var texthighlightendtag   = "</span>";


     if($alltextpara.length > 0){
       $.each($alltextpara, function(index, val) {
         /*if there is a data-highlightcustomclass attribute defined for the text element
         use that or else use default 'parsedstring'*/
         $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
         (stylerulename = $(this).attr("data-highlightcustomclass")) :
         (stylerulename = "parsedstring") ;

         texthighlightstarttag = "<span class='"+stylerulename+"'>";
         replaceinstring       = $(this).html();
         replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
         replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


         $(this).html(replaceinstring);
       });
     }
   }
   /*=====  End of data highlight function  ======*/

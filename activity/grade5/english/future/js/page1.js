var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
{
	contentblockadditionalclass: "blackboard-1",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	}
	]
},
{
	contentblockadditionalclass: "blackboard-2",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text1,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-3",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text2,
		textclass : 'template-dialougebox2-top-flipped-white dg-2'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-4",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text3,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-5",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text4,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-1",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text5,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-2",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text6,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-3",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text7,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-4",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text8,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-6",
	uppertextblock:[
	{
		textdata : data.string.p1text9,
		textclass : 'template-dialougebox2-top-flipped-white dg-3'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-5",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text10,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-7",
	uppertextblock:[
	{
		textdata : data.string.p1text11,
		textclass : 'template-dialougebox2-top-white dg-3'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-8",
	uppertextblock:[
	{
		textdata : data.string.p1text12,
		textclass : 'template-dialougebox2-top-white dg-4'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-9",
	formblock:[
	{

	}
	],
	uppertextblock:[
	{
		textclass: "instruction",
		textdata: data.string.p1text15
	},
	{
		textdata : data.string.p1text13,
		textclass : 'template-dialougebox2-top-flipped-white personal'
	}
	]
},
{
	contentblockadditionalclass: "blackboard-4",
	uppertextblock:[
	{
		textclass: "thetitle",
		textdata: data.lesson.chapter
	},
	{
		textdata : data.string.p1text14,
		textclass : 'template-dialougebox2-top-flipped-white dg-1'
	}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p8.ogg"},
      {id: "sound_8", src: soundAsset+"s1_p9.ogg"},
      {id: "sound_9", src: soundAsset+"s1_p10.ogg"},
      {id: "sound_10", src: soundAsset+"s1_p11.ogg"},
      {id: "sound_11", src: soundAsset+"s1_p12.ogg"},
      {id: "sound_12", src: soundAsset+"s1_p13.ogg"},
      {id: "sound_13", src: soundAsset+"s1_p14.ogg"},
      {id: "sound_14", src: soundAsset+"s1_p15.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 13:
			sound_player("sound_13");
			$nextBtn.hide(0);
			var $shname = $("input[name='firstname']");
			var $shage = $("input[name='age']");
			var $shgrade = $("input[name='grade']");

			$("input[name='age'], input[name='grade']").keypress(function (e) {
				if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});

			$shname.keypress(function(event){
        var inputValue = event.which;
        // allow letters and whitespaces only.
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });

			$(".personal").hide(0);
			$(".submit_button").click(function(){
				$(".instruction").hide(0);
				if($shname.val() != "" && $shage.val() != "" && $shgrade.val() != ""){
					var name = $shname.val();
					var age = parseInt($shage.val()) + 1;
					var grade = parseInt($shgrade.val()) + 1;
					$("#per-name").html(name);
					$("#per-age").html(age);
					$("#per-grade").html(grade);
					var gender = $("input[name='gender']:checked").val();
					$(".formblock").hide(0);
					$(".contentblock").removeClass("blackboard-9").addClass("blackboard-10");
					nav_button_controls(1000);
					if(gender == "male"){
						$(".personal").addClass("dg-boy").show(0);
					}
					else{
						$(".personal").addClass("dg-girl").show(0);
					}
				}
				if($shname.val() == ""){
					$shname.css({
						"border-color":"red",
						"background":"#FF7660"
					});
				}
				if($shage.val() == ""){
					$shage.css({
						"border-color":"red",
						"background":"#FF7660"
					});
				}
				if($shgrade.val() == ""){
					$shgrade.css({
						"border-color":"red",
						"background":"#FF7660"
					});
				}
			});
			break;
			default:
			sound_nav("sound_"+(countNext));
			break;
		}
	}
	function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
  function sound_nav(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      nav_button_controls(0);
    });
  }
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

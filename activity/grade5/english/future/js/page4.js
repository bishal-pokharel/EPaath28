var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	{
		//slide 0
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblock:[
			{
				textdata : data.string.p4text2,
				textclass : 'template-dialougebox2-top-white dg-1'
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				textdata : data.string.p4text3,
				textclass : 'template-dialougebox2-top-white dg-2'
			}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "leftImg",
						imgsrc: imgpath + "sundari.png"
					},
					{
						imgclass: "rightImg",
						imgsrc: imgpath + "sundari.png"
					}
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text4
			}
		]

	},
	{
		//slide 1
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p4text5,
			}
		],
		extratextblock:[
			{
				textclass: "bigmiddletext",
				textdata : data.string.p4text6,
			}
		]

	},
	{
		//slide 2
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		tableblock : [
		{
			tableclass: "mytable",
			checkclass: "check",
			check: data.string.p2text22,
			colclass1: "column1",
			tableheading: [
			{
				h1: data.string.h1,
				h2: data.string.h2
			}],
			tablebody: [
				//1st row
				{
					c1: data.string.c1r1,
					c2: data.string.c2r1,
				},

				//2nd row
				{
					c1: data.string.c1r2,
					c2: data.string.c2r2,
				},

				//3rd row
				{
					c1: data.string.c1r3,
					c2: data.string.c2r3,
				},

				//4th row
				{
					c1: data.string.c1r4,
					c2: data.string.c2r4,
				},

				//4th row
				{
					c1: data.string.c1r5,
					c2: data.string.c2r5,
				},
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text10
			}
		]

	},
	{
		//slide 3
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text7,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text8
			}
		]
	},
	{
		//slide 4
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text9,
			}
		]
	},
	{
		//slide 5
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text9_1,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text11
			}
		]

	},
	{
		//slide 5
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text9_2,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text12
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text9_3,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text13
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text9_4,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text14
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p2text10,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hawa",
				datahighlightcustomclass2: "hightext4",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text15,
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text16
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblockadditionalclass: "ole_temp_uppertextblock",
		uppertextblock:[
			{
				textdata : data.string.p4text18,
			}
		],
		extratextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hawa",
				datahighlightcustomclass2: "hightext4",
				datahighlightcustomclass3: "hightext3",
				textclass: "bigmiddletext",
				textdata : data.string.p4text17,
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "middleImg",
						imgsrc: imgpath + "s02.png"
					}
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p4text19
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p4text1
			}
		],
		uppertextblock : [{
			textdata : data.string.p4diy1,
			textclass : 'diyques'
		}],
		lowertextblockadditionalclass: 'mcq_diy',
		lowertextblock:[
			{
				textclass: "mcq_p",
				textdata: data.string.p4diy2
			},{
				textclass: "mcq_p",
				textdata: data.string.p4diy3
			},{
				textclass: "mcq_p",
				textdata: data.string.p4diy4
			},{
				textclass: "mcq_p",
				textdata: data.string.p4diy5
			},{
				textclass: "mcq_p",
				textdata: data.string.p4diy6
			}
		],

	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			// sounds
			{id: "sound_a", src: soundAsset+"s4_p1.ogg"},
			{id: "sound_b", src: soundAsset+"s4_p1_a.ogg"},
			{id: "sound_c", src: soundAsset+"s4_p1_b.ogg"},
			{id: "sound_d", src: soundAsset+"s4_p1_2.ogg"},
			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s4_p3.ogg"},
			{id: "sound_3a", src: soundAsset+"s4_p4.ogg"},
			{id: "sound_3b", src: soundAsset+"s4_p4_1.ogg"},
			{id: "sound_4", src: soundAsset+"s4_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s4_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s4_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s4_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s4_p9.ogg"},
			{id: "sound_9a", src: soundAsset+"s4_p10.ogg"},
			{id: "sound_9b", src: soundAsset+"s4_p10_1.ogg"},
			{id: "sound_10", src: soundAsset+"s4_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s4_p12.ogg"},
			{id: "sound_12", src: soundAsset+"s4_p13.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_b");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_c");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_d");
			current_sound.play();
			current_sound.on("complete", function(){
			nav_button_controls(0);
					});
				});
			});
		});
			break;
			case 3:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_3b");
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		});
			break;
			case 9:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_9a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_9b");
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		});
			break;
			case 12:
			sound_player("sound_12");
			var count = 0;
			for(var i=1; i<6; i++){
				$('.mcq_p').eq(i-1).prepend(i+'. ');
			}

			$('.option>span').click(function(){
				if($(this).hasClass('correct')){
					count++;
					play_correct_incorrect_sound(1);
					$(this).addClass('mcqcor-option');
					$(this).parent().children('.incorrect').fadeOut(500);
					$(this).parent().css('pointer-events', 'none');
					if(count>=5){
						// nav_button_controls(0);
						$prevBtn.show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}
				} else{
					play_correct_incorrect_sound(0);
					$(this).addClass('mcqincor-option');
					$(this).css('pointer-events', 'none');
				}
			});
			break;
			default:
			sound_nav("sound_"+(countNext));
			break;
		}
	}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 12)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			 //check if $highlightinside is provided
			 typeof $highlightinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			 var stylerulename;
			 var replaceinstring;
			 var texthighlightstarttag;
			 var texthighlightstarttag2;
			 var texthighlightstarttag3;
			 var texthighlightendtag   = "</span>";
			 if($alltextpara.length > 0){
				 $.each($alltextpara, function(index, val) {
					 /*if there is a data-highlightcustomclass attribute defined for the text element
					 use that or else use default 'parsedstring'*/
						 $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename = $(this).attr("data-highlightcustomclass")) :
						 (stylerulename = "parsedstring") ;

						 $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						 (stylerulename2 = "parsedstring2") ;

						 $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						 (stylerulename3 = "parsedstring3") ;

					 texthighlightstarttag = "<span class='"+stylerulename+"'>";
					 texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					 texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					 replaceinstring       = $(this).html();
					 replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					 replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					 replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					 $(this).html(replaceinstring);
				 });
			 }
		 }
		/*=====  End of data highlight function  ======*/

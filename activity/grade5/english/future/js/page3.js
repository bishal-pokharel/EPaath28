var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p3text1
			}
		],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "void",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textdata : data.string.p3text2,
				textclass : 'template-dialougebox2-top-white dg-1'
			}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "middleImg",
						imgsrc: imgpath + "sagar-09.png"
					}
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p3text3
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p3text1
			}
		],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textdata : data.string.p3text2,
				textclass : 'template-dialougebox2-top-white dg-1'
			}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "middleImg",
						imgsrc: imgpath + "sagar-09.png"
					}
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p3text4
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p3text1
			}
		],
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textdata : data.string.p3text5,
				textclass : 'template-dialougebox2-top-white dg-1'
			}
		],
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "middleImg",
						imgsrc: imgpath + "sagar-09.png"
					}
				]
			}
		],
		lowertextblockadditionalclass: "ole_temp_lowertextblock",
		lowertextblock:[
			{
				textdata: data.string.p3text4
			}
		]

	},
	{
		contentblockadditionalclass: "ole_temp_subhead_background1",
		headerblockadditionalclass: "ole_temp_subhead_header",
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "hightext1",
				datahighlightcustomclass2: "hightext2",
				datahighlightcustomclass3: "hightext3",
				textdata: data.string.p2text1
			},
			{
				textdata: data.string.p3text1
			}
		],
		uppertextblock : [{
			textdata : data.string.p3text6,
			textclass : 'exehead'
		}],
		lowertextblockadditionalclass: 'exe-cont',
		lowertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "letspink",
				textclass: "mcq_p",
				textdata: data.string.p3text7
			},{
				datahighlightflag: true,
				datahighlightcustomclass: "letspink",
				textclass: "mcq_p",
				textdata: data.string.p3text8
			},{
				datahighlightflag: true,
				datahighlightcustomclass: "letspink",
				textclass: "mcq_p",
				textdata: data.string.p3text9
			},{
				datahighlightflag: true,
				datahighlightcustomclass: "letspink",
				textclass: "mcq_p",
				textdata: data.string.p3text10
			}
		],

	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			// sounds
			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
			{id: "sound_a", src: soundAsset+"s3_p1_1.ogg"},
			{id: "sound_b", src: soundAsset+"s3_p1_2.ogg"},
			{id: "sound_2", src: soundAsset+"s3_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s3_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s3_p4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_0");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_a");
			current_sound.play();
			current_sound.on("complete", function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_b");
			current_sound.play();
			current_sound.on("complete", function(){
			nav_button_controls(0);
					});
				});
			});
			break;
			case 1:
			sound_nav("sound_"+(countNext+1));
			break;
			case 2:
			sound_nav("sound_"+(countNext+1));
			break;
			case 3:
			sound_player("sound_4");
			var count = 0;
			for(var i=1; i<6; i++){
				$('.mcq_p').eq(i-1).prepend(i+'. ');
			}

			$('.option>span').click(function(){
				if($(this).hasClass('correct')){
					count++;
					play_correct_incorrect_sound(1);
					$(this).addClass('mcqcor-option');
					$(this).parent().children('.incorrect').fadeOut(500);
					$(this).parent().css('pointer-events', 'none');
					if(count>=5){
						$prevBtn.show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}
				} else{
					play_correct_incorrect_sound(0);
					$(this).addClass('mcqincor-option');
					$(this).css('pointer-events', 'none');
				}
			});
			nav_button_controls(4000);
			break;
		}
	}

	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		if(countNext != 14)
		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			 //check if $highlightinside is provided
			 typeof $highlightinside !== "object" ?
			 alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			 null ;

			 var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			 var stylerulename;
			 var replaceinstring;
			 var texthighlightstarttag;
			 var texthighlightstarttag2;
			 var texthighlightstarttag3;
			 var texthighlightendtag   = "</span>";
			 if($alltextpara.length > 0){
				 $.each($alltextpara, function(index, val) {
					 /*if there is a data-highlightcustomclass attribute defined for the text element
					 use that or else use default 'parsedstring'*/
						 $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename = $(this).attr("data-highlightcustomclass")) :
						 (stylerulename = "parsedstring") ;

						 $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
						 (stylerulename2 = "parsedstring2") ;

						 $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						 (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
						 (stylerulename3 = "parsedstring3") ;

					 texthighlightstarttag = "<span class='"+stylerulename+"'>";
					 texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
					 texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
					 replaceinstring       = $(this).html();
					 replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					 replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					 replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
					 replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					 $(this).html(replaceinstring);
				 });
			 }
		 }
		/*=====  End of data highlight function  ======*/

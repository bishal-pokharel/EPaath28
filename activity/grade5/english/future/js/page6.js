var imgpath = $ref + "/images/lastpage/";
var soundAsset = $ref+"/sounds/";

var trishalaStory=[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext1,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/pokhara.png"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext2,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/by-car.png"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext3,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/pokhara.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext4,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/peacepagoda.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext5,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/tallbarahi.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.tritext6,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/pokhara_airport.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "trishala.png"
					},
				]
			}
		],
	}
]

var premStory=[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pretext1,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/chitwan.png"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "prem.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pretext2,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/chitwan-national-park1.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "prem.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pretext3,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/chitwannational.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "prem.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pretext4,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/tharu.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "prem.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pretext5,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/jungle-safari_chitwan.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "prem.png"
					},
				]
			}
		],
	}
]

var praStory=[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pratext1,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/janakpur.png"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "pradeep.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pratext2,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/janakimandir.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "pradeep.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pratext3,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/janakpur-rail.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "pradeep.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.pratext4,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/ram-mandir_janakpur.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "pradeep.png"
					},
				]
			}
		],
	}
]

var aasStory=[
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.aastext1,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/lumbini.png"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "aasha.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.aastext2,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/lumbini.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "aasha.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.aastext3,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/mayadevitemple.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "aasha.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.aastext4,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/worldpeace.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "aasha.png"
					},
				]
			}
		],
	},
	{
		datahighlightflag: true,
		datahighlightcustomclass: "hightext1",
		storytext: "strytextclass",
		storydata: data.string.aastext5,
		imageblock:[
			{
				imagetoshow:[
					{
						imgclass: "storyimg",
						imgsrc: imgpath + "story/lumbiniwest.jpg"
					},
					{
						imgclass: "character",
						imgsrc: imgpath + "aasha.png"
					},
				]
			}
		],
	}
]

var content=[
{
	contentblockadditionalclass: "ole-background-gradient-turqiose",
	headerblock:[
		{
			textdata: data.string.p6text1
		}
	],
	imageblock:[
		{
			imagetoshow:[
				{
					imgclass: "middleImg",
					imgsrc: imgpath + "world-ture.png"
				},
			]
		}
	],
	imgandtext:[
		{
			imgtextadditionalclass: "firstblock forhover",
			imgsrc: imgpath + "trishala01.png",
			textdata: data.string.trisala
		},
		{
			imgtextadditionalclass: "secondblock forhover",
			imgsrc: imgpath + "prem01.png",
			textdata: data.string.prem
		},
		{
			imgtextadditionalclass: "thirdblock forhover",
			imgsrc: imgpath + "pradeep01.png",
			textdata: data.string.pradeep
		},
		{
			imgtextadditionalclass: "fourblock forhover",
			imgsrc: imgpath + "aasha01.png",
			textdata: data.string.aasha
		}
	]
}
];

$(function(){
	var $board = $(".board");
	var $board2 = $(".storydiv");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var storyNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var storydiv=0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images


			// sounds
			{id: "sound_0", src: soundAsset+"s6_p1(trisala).ogg"},
			{id: "sound_0_0", src: soundAsset+"s6_p1_1(trisala).ogg"},
			{id: "sound_0_1", src: soundAsset+"s6_p1_2(trisala).ogg"},
			{id: "sound_0_2", src: soundAsset+"s6_p1_3(trisala).ogg"},
			{id: "sound_0_3", src: soundAsset+"s6_p1_4(trisala).ogg"},
			{id: "sound_0_4", src: soundAsset+"s6_p1_5(trisala).ogg"},
			{id: "sound_1", src: soundAsset+"s6_p1(prem).ogg"},
			{id: "sound_1_0", src: soundAsset+"s6_p1_1(prem).ogg"},
			{id: "sound_1_1", src: soundAsset+"s6_p1_2(prem).ogg"},
			{id: "sound_1_2", src: soundAsset+"s6_p1_3(prem).ogg"},
			{id: "sound_1_3", src: soundAsset+"s6_p1_4(prem).ogg"},
			{id: "sound_1_4", src: soundAsset+"s6_p1_5(prem).ogg"},
			{id: "sound_2", src: soundAsset+"s6_p1(pradeep).ogg"},
			{id: "sound_2_0", src: soundAsset+"s6_p1_1(pradeep).ogg"},
			{id: "sound_2_1", src: soundAsset+"s6_p1_2(pradeep).ogg"},
			{id: "sound_2_2", src: soundAsset+"s6_p1_3(pradeep).ogg"},
			{id: "sound_2_3", src: soundAsset+"s6_p1_4(pradeep).ogg"},
			{id: "sound_2_4", src: soundAsset+"s6_p1_5(pradeep).ogg"},
			{id: "sound_3", src: soundAsset+"s6_p1(aasha).ogg"},
			{id: "sound_3_0", src: soundAsset+"s6_p1_1(aasha).ogg"},
			{id: "sound_3_1", src: soundAsset+"s6_p1_2(aasha).ogg"},
			{id: "sound_3_2", src: soundAsset+"s6_p1_3(aasha).ogg"},
			{id: "sound_3_3", src: soundAsset+"s6_p1_4(aasha).ogg"},
			{id: "sound_3_4", src: soundAsset+"s6_p1_5(aasha).ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


		function navigationcontroller(islastpageflag){
			 	typeof islastpageflag === "undefined" ?
			 	islastpageflag = false :
			 	typeof islastpageflag != 'boolean'?
			 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
			 	null;

		}




		var storyCounter = 0;
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".firstblock").click(function(){
			whichStory($(this));
			sound_player("sound_0");
		});

		$(".secondblock").click(function(){
			whichStory($(this));
			sound_player("sound_1");
		});

		$(".thirdblock").click(function(){
			whichStory($(this));
			sound_player("sound_2");

		});

		$(".fourblock").click(function(){
			whichStory($(this));
			sound_player("sound_3");

		});

	}

	// if(currentstory==trishalaStory || story==1 ){


	function whichStory($myStory){
		if($myStory.hasClass("forhover")){
			storyNext = 0;
			//$myStory.addClass("disabled").removeClass("forhover");

		if($myStory.hasClass("secondblock")){
			generalStoryTemplate(premStory);
		}
		else if($myStory.hasClass("firstblock")){
			generalStoryTemplate(trishalaStory);
		}
		else if($myStory.hasClass("thirdblock")){
			generalStoryTemplate(praStory);
		}
		else if($myStory.hasClass("fourblock")){
			generalStoryTemplate(aasStory);
		}
		}
	}

	function generalStoryTemplate(currentstory){
		$(".storydiv").show(0);
		var source2 = $("#story-template").html();
		var template2 = Handlebars.compile(source2);
		var html2 = template2(currentstory[storyNext]);
		//$board2.html(html2);

		$(".storydiv").html(html2);
		texthighlight($board);
		storynavigation();

		$(".closebtn").click(function(){
            storyCounter++;
            if(storyCounter == 4)
                ole.footerNotificationHandler.pageEndSetNotification();
            $(this).parent().fadeOut();
			current_sound.stop();
			if(currentstory == trishalaStory)
				$(".firstblock").addClass("disabled").removeClass("forhover");
				else if(currentstory == premStory)
						$(".secondblock").addClass("disabled").removeClass("forhover");
				else if(currentstory == praStory)
						$(".thirdblock").addClass("disabled").removeClass("forhover");
				else if(currentstory == aasStory)
						$(".fourblock").addClass("disabled").removeClass("forhover");


		});

		$(".storynxt").on("click", function(){
			 if(currentstory==trishalaStory){
			 	console.log("storynext nxtbtn"+storyNext);
				 sound_player("sound_0_"+(storyNext));
				 storyNext++;
				 generalStoryTemplate(currentstory);
			 }
			 else if(currentstory==premStory){
				 sound_player("sound_1_"+(storyNext));
				 storyNext++;
				 generalStoryTemplate(currentstory);
			 }
			 else if(currentstory==praStory){
				 sound_player("sound_2_"+(storyNext));
				 storyNext++;
				 generalStoryTemplate(currentstory);
			 }
			 else if(currentstory==aasStory){
				 sound_player("sound_3_"+(storyNext));
				 storyNext++;
				 generalStoryTemplate(currentstory);
			 }

		});

		$(".storyprev").on("click", function(){
			if(currentstory==trishalaStory){
                storyNext--;
                console.log("storynext prev"+storyNext);
                if(storyNext==0){
					sound_player("sound_0");
				}
				else
				{
                    sound_player("sound_0_"+(storyNext-1));
                }
				generalStoryTemplate(currentstory);
			}
            if(currentstory==premStory){
                storyNext--;
                console.log("storynext prev"+storyNext);
                if(storyNext==0){
                    sound_player("sound_1");
                }
                else
                {
                    sound_player("sound_1_"+(storyNext-1));
                }
                generalStoryTemplate(currentstory);
            }
            if(currentstory==praStory){
                storyNext--;
                console.log("storynext prev"+storyNext);
                if(storyNext==0){
                    sound_player("sound_2");
                }
                else
                {
                    sound_player("sound_2_"+(storyNext-1));
                }
                generalStoryTemplate(currentstory);
            }
            if(currentstory==aasStory){
                storyNext--;
                console.log("storynext prev"+storyNext);
                if(storyNext==0){
                    sound_player("sound_3");
                }
                else
                {
                    sound_player("sound_3_"+(storyNext-1));
                }
                generalStoryTemplate(currentstory);
            }
		});

		function storynavigation(){
				if(storyNext == 0){
					$(".storyprev").hide(0);
				}

				if(storyNext == currentstory.length - 1){
					$(".storynxt").hide(0);
					$(".closebtn").show(0);
				}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});



	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";
var sound_l_1 = new buzz.sound((soundAsset + "ex1_1.ogg"));

var content=[

	{
		// slide 1
		contentnocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblocknocenteradjust: true,
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s2
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s3
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s4
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s5
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s6
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-to-write-a-story.png"
			}]
		}]

	},
	{
		//slide 2
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s7
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s8
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s9
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s10
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s11
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-to-by-a-house.png"
			}]
		}]
	},
	{
		//slide 3
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s12
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s13
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s14
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s15
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s16
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-100rs.png"
			}]
		}]
	},
	{
		//slide 4
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s17
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s18
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s19
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s20
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s21
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"kids-want-to-playground.png"
			}]
		}]
	},
	{
		//slide 5
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s22
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s23
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s24
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s25
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s26
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-to-drink-milk.png"
			}]
		}]
	},
	{
		//slide 6
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s27
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s28
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s29
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s30
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s31
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-to-ride-a-cycle.png"
			}]
		}]
	},
	{
		//slide 7
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s32
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s33
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s34
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s35
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s36
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"want-to-cut-haircut.png"
			}]
		}]
	},
	{
		//slide 8
		contentnocenteradjust: true,
		uppertextblocknocenteradjust: true,
		contentblockadditionalclass: "additional_content_block",
		uppertextblock:[{
			textclass: "question",
			textdata: data.string.e1_s1
		},{
			textclass: "answer",
			textdata: data.string.e1_s37
		}],
		imageblock:[{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover yes",
				imagelabeldata: data.string.e1_s41
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"correct.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s39
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s40
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv",
			imagelabels:[{
				imagelabelclass: "options onhover no",
				imagelabeldata: data.string.e1_s38
			}],
			imagestoshow:[{
					imgclass: "image",
					imgsrc: imgpath+"incorrect.png"
			}]
		},{
			imageblockclass: "imagediv_example",
			imagestoshow:[{
					imgclass: "image_example",
					imgsrc: imgpath+"student-want-to-race.png"
			}]
		}]
	}
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

 	/*=====  End of data highlight function  ======*/


    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag){
  		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			// $prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			// $prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var eggTemplate = new EggTemplate();

  eggTemplate.init(8);
  function generalTemplate() {
	    var source = $("#general-template").html();
	    var template = Handlebars.compile(source);
	    var html = template(content[countNext]);
	    $board.html(html);

	    // highlight any text inside board div with datahighlightflag set true
	    texthighlight($board);
		// splitintofractions($(".fractionblock"));
	    var $imagediv = $(".imagediv");
	    var $imageblock = $(".imageblock");
	    var $imagediv_example = $(".imagediv_example");
	    $imageblock.html("");

	    var randomindex;
	    while($imagediv.length > 1){
	    	randomindex = Math.floor(Math.random() * ($imagediv.length + 1));
	    	$imageblock.append($($imagediv[randomindex]));
	    	$imagediv.splice(randomindex, 1);
	    }
			if(countNext==0){
				playaudio(sound_l_1, $(".dialog2"));
			}
	    // if(countNext != 0)
	    $nextBtn.hide(0);
	    $imageblock.append($($imagediv[0]));
	    $imageblock.append($imagediv_example);
	    var answered = false;
	    var incorrect = false;
	    $(".imagediv").click(function(){
	    	if(answered){
	    		return true;
	    	}
	    	var $this = $(this);
    		$this.find("img").show(0);
    		var $label = $this.find("label");
    		$label.removeClass("onhover");
	    	if($label.hasClass("yes")){
	    		$label.addClass("correct");
	    		answered = true;
	    		play_correct_incorrect_sound(1);
	    		if(!incorrect){
	    			eggTemplate.update(true);

	    		}

	    		if(countNext == ($total_page-1)){
					ole.footerNotificationHandler.pageEndSetNotification();
	    		}else{
		    		$nextBtn.show(0);
	    		}
	    		$(".imagediv> label").removeClass("onhover");
	    	} else {
	    		incorrect = true;
	    		eggTemplate.update(false);
	    		$label.addClass("incorrect");
	    		play_correct_incorrect_sound(0);
	    	}
	    });
  }
	function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		setTimeout(function(){
			sound_data.play();
		}, 1200);
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext==0){
					$prevBtn.hide(0);
				}else{
								$prevBtn.show(0);
				}

				playing = false;
				$dialog_container.css('cursor', 'pointer');
				$dialog_container.children('.audio_icon').show(0);
				sound_data.unbind('ended');
			}, 0);
		});
	}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

  function templateCaller(){
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template
    generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


    //call the slide indication bar handler for pink indicators

  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

	$nextBtn.on('click', function() {
			countNext++;
			eggTemplate.gotoNext();
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

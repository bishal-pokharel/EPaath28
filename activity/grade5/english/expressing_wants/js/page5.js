var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_l_1 = new buzz.sound((soundAsset + "p5_s1.ogg"));
var sound_l_2 = new buzz.sound((soundAsset + "p5_s2.ogg"));
var sound_l_3 = new buzz.sound((soundAsset + "p5_s3.ogg"));
var sound_l_4 = new buzz.sound((soundAsset + "p5_s4.ogg"));
var sound_l_5 = new buzz.sound((soundAsset + "p5_s5.ogg"));
var sound_l_6 = new buzz.sound((soundAsset + "p5_s6.ogg"));
var sound_l_7 = new buzz.sound((soundAsset + "p5_s7.ogg"));
var sound_l_8 = new buzz.sound((soundAsset + "s5_p1.ogg"));
var sound_l_9 = new buzz.sound((soundAsset + "s5_p17.ogg"));

var sound_group_p1 = [sound_l_1, sound_l_2, sound_l_3, sound_l_4, sound_l_5, sound_l_6, sound_l_7,sound_l_8,sound_l_9];

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_greenapple',

		uppertextblockadditionalclass: 'think-text',
		uppertextblock : [{
			textdata : data.string.p5text0,
			textclass : 'my_font_very_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-first'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'think-squirrel',
					imgsrc: "images/lokharke/squirrel_what_animated.svg",
				},
			]
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text1,
			textclass : 'title_chelsea_market my_font_very_big header hl-bold-ul',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-italics'
		},
		{
			textdata : data.string.p5text2,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text3,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text4,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text5,
			textclass : 'title_chelsea_market my_font_very_big header hl-bold-ul',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-italics'
		},
		{
			textdata : data.string.p5text6,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text7,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text8,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boy.png',
			labelclass: '',
			labeldata: data.string.p5text12,
			textdata2: data.string.p5text9,
			textclass2: '',
			textdata3: data.string.pwants,
			textclass3: '',
			textdata4: data.string.p5text11,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boys.png',
			labelclass: '',
			labeldata: data.string.p5text13,
			textdata2: data.string.p5text10,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text11,
			textclass4: '',
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'cat.png',
			labelclass: '',
			labeldata: data.string.p5text14,
			textdata2: data.string.pmissed1,
			textclass2: '',
			textdata3: data.string.pwants,
			textclass3: '',
			textdata4: data.string.pmilk,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'cats.png',
			labelclass: '',
			labeldata: data.string.p5text15,
			textdata2: data.string.p5text10,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.pmilk,
			textclass4: '',
		}]
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text16,
			textclass : 'title_chelsea_market my_font_very_big header hl-bold-ul',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-italics'
		},
		{
			textdata : data.string.p5text17,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text18,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text19,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text20,
			textclass : 'title_chelsea_market my_font_very_big header hl-bold-ul',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-italics'
		},
		{
			textdata : data.string.p5text21,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text22,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text23,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boy.png',
			labelclass: '',
			labeldata: data.string.p5text12,
			textdata2: data.string.p5text9,
			textclass2: '',
			textdata3: data.string.pdwants,
			textclass3: '',
			textdata4: data.string.p5text11,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boys.png',
			labelclass: '',
			labeldata: data.string.p5text13,
			textdata2: data.string.p5text10,
			textclass2: '',
			textdata3: data.string.pdwant,
			textclass3: '',
			textdata4: data.string.p5text11,
			textclass4: '',
		}]
	},

	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'cat.png',
			labelclass: '',
			labeldata: data.string.p5text14,
			textdata2: data.string.pmissed1,
			textclass2: '',
			textdata3: data.string.pdwants,
			textclass3: '',
			textdata4: data.string.pmilk,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'cats.png',
			labelclass: '',
			labeldata: data.string.p5text15,
			textdata2: data.string.p5text10,
			textclass2: '',
			textdata3: data.string.pdwant,
			textclass3: '',
			textdata4: data.string.pmilk,
			textclass4: '',
		}]
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text24,
			textclass : 'title_chelsea_market my_font_very_big header',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold-ul'
		},
		{
			textdata : data.string.p5text25,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text26,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text27,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text28,
			textclass : 'top-margin-1',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text29,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text30,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: 'img-only',
			imgsrc: imgpath + 'me.png',
			labelclass: '',
			labeldata: data.string.pi,
			textdata2: data.string.pi,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text31,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass shifted-process',
			divclass: '',
			imgdivclass: '',
			imgclass: 'img-only',
			imgsrc: imgpath + 'me.png',
			labelclass: '',
			labeldata: data.string.pi,
			textdata2: data.string.pi,
			textclass2: '',
			textdata3: data.string.pdwant,
			textclass3: '',
			textdata4: data.string.p5text32,
			textclass4: '',
		}]
	},

	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text33,
			textclass : 'title_chelsea_market my_font_very_big header hl-bold',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue'
		},
		{
			textdata : data.string.p5text34,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text35,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text36,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text37,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		}],
	},
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boy.png',
			labelclass: '',
			labeldata: data.string.p5text12,
			textdata1: data.string.pdoes,
			textclass1: '',
			textdata2: data.string.p5text9,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text38,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'dog.png',
			labelclass: '',
			labeldata: data.string.p5text40,
			textdata1: data.string.pdoes,
			textclass1: '',
			textdata2: data.string.pmissed1,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text39,
			textclass4: '',
		}]
	},


	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',

		uppertextblockadditionalclass: 'additional_utb description-text my_font_big proximanova',
		uppertextblock : [{
			textdata : data.string.p5text41,
			textclass : 'title_chelsea_market my_font_very_big header',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-blue'
		},
		{
			textdata : data.string.p5text42,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold'
		},
		{
			textdata : data.string.p5text43,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold2'
		},
		{
			textdata : data.string.p5text44,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold2'
		},
		{
			textdata : data.string.p5text45,
			textclass : 'black_text',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-bold2'
		}],
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'boys.png',
			labelclass: '',
			labeldata: data.string.p5text13,
			textdata1: data.string.pdo,
			textclass1: '',
			textdata2: data.string.p5text47,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text38,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'dogs.png',
			labelclass: '',
			labeldata: data.string.p5text48,
			textdata1: data.string.pdo,
			textclass1: '',
			textdata2: data.string.p5text47,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text39,
			textclass4: '',
		}]
	},
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_orange',
		coverboardadditionalclass: 'additional_utb',
		processblock:[{
			processdivclass: 'topclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'you.png',
			labelclass: '',
			labeldata: data.string.you,
			textdata1: data.string.pdo,
			textclass1: '',
			textdata2: data.string.you2,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text49,
			textclass4: '',
		},
		{
			processdivclass: 'bottomclass',
			divclass: '',
			imgdivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'me.png',
			labelclass: '',
			labeldata: data.string.pi,
			textdata1: data.string.pdo,
			textclass1: '',
			textdata2: data.string.pi,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
			textdata4: data.string.p5text50,
			textclass4: '',
		}]
	},
	//slide16
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'skybackground',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.string.psummary,
			textclass : 'my_font_super_big proximanova purple_text'
		}],
	},
	//slide 17
	{
		contentblockadditionalclass : 'skybackground',
		summaryblock: [
		{
			summarydivclass: 'summary-1',
			imgclass: '',
			imgsrc: imgpath + 'singular.png',
			textdata1: data.string.singular,
			textclass1: '',
			textdata2: data.string.positive,
			textclass2: '',
			textdata3: data.string.pwants,
			textclass3: '',
		},
		{
			summarydivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'singular.png',
			textdata1: data.string.singular,
			textclass1: '',
			textdata2: data.string.negative,
			textclass2: '',
			textdata3: data.string.pdwants,
			textclass3: '',
		},
		{
			summarydivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'plural.png',
			textdata1: data.string.plural,
			textclass1: '',
			textdata2: data.string.positive,
			textclass2: '',
			textdata3: data.string.pwant,
			textclass3: '',
		},
		{
			summarydivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'plural.png',
			textdata1: data.string.plural2,
			textclass1: '',
			textdata2: data.string.negative,
			textclass2: '',
			textdata3: data.string.pdwant,
			textclass3: '',
		},
		{
			summarydivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'singular.png',
			textdata1: data.string.singular,
			textclass1: '',
			textdata2: data.string.question,
			textclass2: '',
			textdata3: data.string.pdwants2,
			textclass3: '',
		},
		{
			summarydivclass: '',
			imgclass: '',
			imgsrc: imgpath + 'plural.png',
			textdata1: data.string.plural2,
			textclass1: '',
			textdata2: data.string.question,
			textclass2: '',
			textdata3: data.string.pdwant2,
			textclass3: '',
		}]
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var current_char = null;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch (countNext) {
			case 0:
				playaudio(sound_group_p1[7]);
				break;

			case 1:
				playaudio(sound_group_p1[0]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
			case 2:
				playaudio(sound_group_p1[1]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
	    	case 5:
	    		playaudio(sound_group_p1[2]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
	    	case 6:
	    		playaudio(sound_group_p1[3]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
			case 9:
				playaudio(sound_group_p1[4]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
			case 11:
				playaudio(sound_group_p1[5]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;
    		case 13:
    			playaudio(sound_group_p1[6]);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
	    		break;

			case 13:
				$prevBtn.show(0);
				fade_in_caller(0,  $('.description-text>p').length,'.description-text>p');
				break;
				case 16:
					playaudio(sound_group_p1[8]);
					break;
			default:
				$prevBtn.show(0);
				nav_button_controls(1000);
				break;
		}
	}

	var playing = false;
	function playaudio(sound_data){
		buzz.all().stop();
		$prevBtn.hide(0);
		playing = true;
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		setTimeout(function(){
			sound_data.play();
		}, 1200);
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext==0){
					$prevBtn.hide(0);
				}
				else{
					$prevBtn.show(0);
				}
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function fade_in_caller(i, length, classname){
		if(i<length){
			$(classname).eq(i).fadeIn(1000, function(){
				fade_in_caller(i+1, length, classname);
			});
		} else{
			if(!playing)
			nav_button_controls(100);
		}
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

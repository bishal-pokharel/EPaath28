var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";
var sound_l_1 = new buzz.sound((soundAsset + "s6_p1.ogg"));

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text ins-text-1',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques fade-after-1',
			textdata1: data.string.p6q1a,
			textdata2: data.string.p6q1b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q1c,
			textdata2: data.string.p6q1d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	},
	// //slide1
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'additional_content_blue',
//
		// uppertextblockadditionalclass: 'ins-text',
		// uppertextblock : [{
			// textdata : data.string.p6text1,
			// textclass : 'my_font_big shortstack',
			// datahighlightflag : true,
			// datahighlightcustomclass : 'hl-want-wants'
		// }],
		// questions:[{
			// quesclass: 'topques',
			// textdata1: data.string.p6q1a,
			// textdata2: data.string.p6q1b,
			// opt1class: 'class1',
			// opt1data: data.string.opt1,
			// opt2class: '',
			// opt2data: data.string.opt2,
		// },
		// {
			// quesclass: 'bottomques',
			// textdata1: data.string.p6q1c,
			// textdata2: data.string.p6q1d,
			// opt1class: 'class1',
			// opt1data: data.string.opt1,
			// opt2class: '',
			// opt2data: data.string.opt2,
		// }],
	// },

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q2a,
			textdata2: data.string.p6q2b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q2c,
			textdata2: data.string.p6q2d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q3a,
			textdata2: data.string.p6q3b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q3c,
			textdata2: data.string.p6q3d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q4a,
			textdata2: data.string.p6q4b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q4c,
			textdata2: data.string.p6q4d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	},

	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q5a,
			textdata2: data.string.p6q5b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q5c,
			textdata2: data.string.p6q5d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q6a,
			textdata2: data.string.p6q6b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q6c,
			textdata2: data.string.p6q6d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q7a,
			textdata2: data.string.p6q7b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q7c,
			textdata2: data.string.p6q7d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q8a,
			textdata2: data.string.p6q8b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q8c,
			textdata2: data.string.p6q8d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q9a,
			textdata2: data.string.p6q9b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q9c,
			textdata2: data.string.p6q9d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q10a,
			textdata2: data.string.p6q10b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q10c,
			textdata2: data.string.p6q10d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	},

	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q11a,
			textdata2: data.string.p6q11b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q11c,
			textdata2: data.string.p6q11d,
			opt1class: '',
			opt1data: data.string.opt1,
			opt2class: 'class1',
			opt2data: data.string.opt2,
		}],
	},

	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'additional_content_blue',

		uppertextblockadditionalclass: 'ins-text',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : 'my_font_big shortstack',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-want-wants'
		}],
		questions:[{
			quesclass: 'topques',
			textdata1: data.string.p6q12a,
			textdata2: data.string.p6q12b,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		},
		{
			quesclass: 'bottomques',
			textdata1: data.string.p6q12c,
			textdata2: data.string.p6q12d,
			opt1class: 'class1',
			opt1data: data.string.opt1,
			opt2class: '',
			opt2data: data.string.opt2,
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var current_char = null;
	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();


	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
			playaudio(sound_l_1);
				setTimeout(function(){
					$('.fade-after-1').css('display', 'flex');
				}, 1800);
				break;

			default:
				$prevBtn.show(0);
				break;
		}

		$('.topques .opt1').click(function(){
			if($(this).hasClass('class1')){
				$('.topques .opt1, .topques .opt2').css('pointer-events', 'none');
				$(this).addClass('to-center');
				setTimeout(function(){
					$('.topques .opt1').fadeOut(500, function(){
						$('.topques .blankdata').html($('.topques .opt1').html());
						$('.topques .correct').css('opacity', '1');
						$('.bottomques').css('display', 'flex');
					});
				}, 1000);
				play_correct_incorrect_sound(1);
			} else {
				$('.topques .incorrect-1').show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('selected');
				$(this).unbind('click');
			}
		});
		$('.topques .opt2').click(function(){
			if($(this).hasClass('class1')){
				$('.topques .opt1, .topques .opt2').css('pointer-events', 'none');
				$(this).addClass('to-center');
				setTimeout(function(){
					$('.topques .opt2').fadeOut(500, function(){
						$('.topques .blankdata').html($('.topques .opt2').html());
						$('.topques .correct').css('opacity', '1');
						$('.bottomques').css('display', 'flex');
					});
				}, 1000);
				play_correct_incorrect_sound(1);
			} else {
				$('.topques .incorrect-2').show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('selected');
				$(this).unbind('click');
			}
		});


		$('.bottomques .opt1').click(function(){
			if($(this).hasClass('class1')){
				$('.bottomques .opt1, .bottomques .opt2').css('pointer-events', 'none');
				$(this).addClass('to-center');
				setTimeout(function(){
					$('.bottomques .opt1').fadeOut(500, function(){
						$('.bottomques .blankdata').html($('.bottomques .opt1').html());
						$('.bottomques .correct').css('opacity', '1');
					});
					nav_button_controls(1000);
				}, 1000);
				play_correct_incorrect_sound(1);
			} else {
				$('.bottomques .incorrect-1').show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('selected');
				$(this).unbind('click');
			}
		});
		$('.bottomques .opt2').click(function(){
			if($(this).hasClass('class1')){
				$('.bottomques .opt1, .bottomques .opt2').css('pointer-events', 'none');
				$(this).addClass('to-center');
				setTimeout(function(){
					$('.bottomques .opt2').fadeOut(500, function(){
						$('.bottomques .blankdata').html($('.bottomques .opt2').html());
						$('.bottomques .correct').css('opacity', '1');
					});
					nav_button_controls(1000);
				}, 1000);
				play_correct_incorrect_sound(1);
			} else {
				$('.bottomques .incorrect-2').show(0);
				play_correct_incorrect_sound(0);
				$(this).addClass('selected');
				$(this).unbind('click');
			}
		});

	}
	var playing = false;
	function playaudio(sound_data){
		$prevBtn.hide(0);
		playing = true;
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		setTimeout(function(){
			sound_data.play();
		}, 1200);
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext==0){
					$prevBtn.hide(0);
				}
				else{
					$prevBtn.show(0);
				}
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					// $nextBtn.show(0);
				}
			}, 1000);
		});
	}

	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'bg_blue',

		uppertextblockadditionalclass:'sniglet',
		uppertextblock:[{
			textdata: data.string.p4text1,
			textclass: "toptext",
		},
		{
			textdata: data.string.p8text1,
			textclass: "title",
		}],
	},
	// slide1
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "boy",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "dog",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "dogfood",
					imgsrc : '',
					imgid : 'dogfood'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p8text2,
			imgclass: '',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide2
	{
		uppertextblock:[{
			textdata : data.string.p8text3,
			textclass : 'note',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "boy",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "dog",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "dogfood",
					imgsrc : '',
					imgid : 'dogfood'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-1',
			textdata : data.string.p8text4,
			imgclass: '',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide3
	{
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "fairy",
					imgsrc : '',
					imgid : 'fairy'
				},
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'bg'
				},
				{
					imgclass : "boy",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "dog",
					imgsrc : '',
					imgid : 'dog'
				},
				{
					imgclass : "dogfood",
					imgsrc : '',
					imgid : 'dogfood'
				},
			]
		}],
		speechbox:[{
			speechbox: 'sp-2',
			textdata : data.string.p8text5,
			imgclass: 'invert',
			textclass : 'arrangetext',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide4
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textclass:'btopic',
			textdata: data.lesson.chapter
		}],
		tableclass:'tableclass',
		row1headdata:data.string.p2text9,
		row2headdata:data.string.p2text10,
		row1data:data.string.p4text11,
		row2data:data.string.p4text12,
		row3data:data.string.p4text13,
		row4data:data.string.p4text13,
		row5data:data.string.p5text6,
		row6data:data.string.p5text7,
		row7data:data.string.p6text6,
		row8data:data.string.p6text7,
		row9data:data.string.p7text6,
		row10data:data.string.p7text7,
		row11data:data.string.p8text6,
		row12data:data.string.p8text7,
		table:[{

		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'blackboard'
				}
			]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'background',
		uppertextblock:[{
			textclass:'itstext',
			textdata: data.string.p8text9
		},
		{
			textclass:'toptext',
			textdata: data.string.p8text8
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "monkey",
					imgsrc : '',
					imgid : 'monkey'
				}
			]
		}],
		speechbox:[{
			speechbox: 'monkeydialogue',
			textdata : data.string.p8text10,
			imgclass: 'invert',
			textclass : 'arrangetext',
			imgid : 'tb-3',
			imgsrc: '',
			// audioicon: true,
		}],
	},
	// slide6
	{
		contentblockadditionalclass:'background',
		uppertextblock:[{
			textclass:'itstext',
			textdata: data.string.p8text11
		},
		{
			textclass:'centertext',
			textdata: data.string.p8text12
		},
		{
			textclass:'toptext',
			textdata: data.string.p8text8a
		}],

	},
	// slide7
	{
		contentblockadditionalclass:'background',
		uppertextblock:[{
			textclass:'itstext',
			textdata: data.string.p8text13
		},
		{
			datahighlightflag:true,
			datahighlightcustomclass:'red',
			textclass:'descriptiontext',
			textdata: data.string.p8text14
		},
		{
			textclass:'toptext',
			textdata: data.string.p8text8a
		}],

	},
	// slide8
	{
		uppertextblock:[{
			textclass:'questiontitle',
			textdata: data.string.p8text15
		},
		{
			textclass:'commonquestion',
			textdata: data.string.p8text16
		},
		{
			textclass:'question',
			textdata: data.string.p8text17
		},
		{
			textclass:'hint1',
			textdata: data.string.p8text18
		},
		{
			textclass:'hint2',
			textdata: data.string.p8text19
		},
		{
			textclass:'hintbutton',
			textdata: data.string.hinttext
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'practisebg'
				},
				{
					imgclass : "right",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong",
					imgsrc : '',
					imgid : 'wrong'
				},
			]
		}],

	},

	// slide9
	{
		uppertextblock:[{
			textclass:'questiontitle',
			textdata: data.string.p8text15
		},
		{
			textclass:'commonquestion',
			textdata: data.string.p8text16
		},
		{
			textclass:'question',
			textdata: data.string.p8text20
		},
		{
			textclass:'hint1',
			textdata: data.string.p8text21
		},
		{
			textclass:'hint2',
			textdata: data.string.p8text22
		},
		{
			textclass:'hintbutton',
			textdata: data.string.hinttext
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'practisebg'
				},
				{
					imgclass : "right",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong",
					imgsrc : '',
					imgid : 'wrong'
				},
			]
		}],

	},
	// slide10
	{
		uppertextblock:[{
			textclass:'questiontitle',
			textdata: data.string.p8text15
		},
		{
			textclass:'commonquestion',
			textdata: data.string.p8text16
		},
		{
			textclass:'question',
			textdata: data.string.p8text23
		},
		{
			textclass:'hint1',
			textdata: data.string.p8text24
		},
		{
			textclass:'hint2',
			textdata: data.string.p8text25
		},
		{
			textclass:'hintbutton',
			textdata: data.string.hinttext
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'practisebg'
				},
				{
					imgclass : "right",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong",
					imgsrc : '',
					imgid : 'wrong'
				},
			]
		}],

	},
	// slide11
	{
		uppertextblock:[{
			textclass:'questiontitle',
			textdata: data.string.p8text15
		},
		{
			textclass:'commonquestion',
			textdata: data.string.p8text16
		},
		{
			textclass:'question',
			textdata: data.string.p8text26
		},
		{
			textclass:'hint1',
			textdata: data.string.p8text27
		},
		{
			textclass:'hint2',
			textdata: data.string.p8text28
		},
		{
			textclass:'hintbutton',
			textdata: data.string.hinttext
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg",
					imgsrc : '',
					imgid : 'practisebg'
				},
				{
					imgclass : "right",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong",
					imgsrc : '',
					imgid : 'wrong'
				},
			]
		}],

	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "fairy", src: imgpath+"pari.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy-pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dogfood", src: imgpath+"dog-food.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blackboard", src: imgpath+"blackboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"monkeyy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "practisebg", src: imgpath+"bg_for_lets-practise.png", type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: imgpath+"right02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: imgpath+"wrong02.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s8_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s8_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s8_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s8_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s8_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s8_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s8_p7.ogg"},
			{id: "sound_7", src: soundAsset+"s8_p8.ogg"},
			{id: "sound_8", src: soundAsset+"s8_p9.ogg"},
			{id: "sound_9", src: soundAsset+"s8_p10.ogg"},
			{id: "sound_10", src: soundAsset+"s8_p11.ogg"},
			{id: "sound_11", src: soundAsset+"s8_p12.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$('.title').addClass('blurin');
				sound_nav("sound_0");
				break;
			case 1:
				$('.sp-1').addClass('fadein');
				sound_nav("sound_1");
				break;
			case 2:
				$('.note').addClass('fadein');
				sound_nav("sound_2");
				break;
			case 3:
				$('.sp-2').addClass('fadein');
				sound_nav("sound_3");
				break;
			case 4:
				sound_nav("sound_4");
				    $('.type13').addClass('typingclass');
				setTimeout(function() {
				    $('.type14').addClass('typingclass1');
					}, 2000);
				break;
			case 5:
				$('.monkeydialogue').addClass('fadein');
				sound_nav("sound_5");
				break;
			case 6:
				$('.centertext').addClass('fadein');
				sound_nav("sound_6");
				break;
			case 7:
				$('.descriptiontext').addClass('fadein');
				sound_nav("sound_7");
				break;

			case 8:
			sound_player("sound_8");
				$('.wrong').click(function(){
					$('.hint1').css({"opacity":"0"});
					$('.hint2').animate({"opacity":"1"},200);
					$('.a1').animate({'opacity':'1'},400);
					$('.hintbutton').css({"pointer-events":"none"});
					$(this).css({"background":"green","pointer-events":"none"});
					$('.right').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
				});
				$('.right').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				$('.hintbutton').click(function(){
					$('.hint1').animate({"opacity":"1"},200);
					});
				break;

			case 9:
				sound_player("sound_9");
				$('.right').click(function(){
					$('.hint1').css({"opacity":"0"});
					$('.hint2').animate({"opacity":"1"},200);
					$('.a1').animate({'opacity':'1'},400);
					$('.hintbutton').css({"pointer-events":"none"});
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
				});
				$('.wrong').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				$('.hintbutton').click(function(){
					$('.hint1').animate({"opacity":"1"},200);
					});
				break;
			case 10:
				sound_player("sound_10");
				$('.right').click(function(){
					$('.hint1').css({"opacity":"0"});
					$('.hint2').animate({"opacity":"1"},200);
					$('.a1').animate({'opacity':'1'},400);
					$('.hintbutton').css({"pointer-events":"none"});
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					nav_button_controls(1000);
				});
				$('.wrong').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				$('.hintbutton').click(function(){
					$('.hint1').animate({"opacity":"1"},200);
					});
				break;
			case 11:
				sound_player("sound_11");
				$('.wrong').click(function(){
					$('.hint1').css({"opacity":"0"});
					$('.hint2').animate({"opacity":"1"},200);
					$('.a1').animate({'opacity':'1'},400);
					$('.hintbutton').css({"pointer-events":"none"});
					$(this).css({"background":"green","pointer-events":"none"});
					$('.right').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
				nav_button_controls(1000);
				});
				$('.right').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				$('.hintbutton').click(function(){
					$('.hint1').animate({"opacity":"1"},200);
					});
				break;
			default:
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

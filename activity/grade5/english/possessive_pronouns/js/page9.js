var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'purplebg',

		uppertextblockadditionalclass:'sniglet',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "diy",
		},
		],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diycenter",
					imgsrc : '',
					imgid : 'diy'
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion',
		},
		{
			textdata : data.string.p9text3,
			textclass : 'question',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "one",
					imgsrc : '',
					imgid : '1'
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion',
		},
		{
			textdata : data.string.p9text4,
			textclass : 'question',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "one",
					imgsrc : '',
					imgid : '2'
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion',
		},
		{
			textdata : data.string.p9text5,
			textclass : 'question',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "one",
					imgsrc : '',
					imgid : '3'
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion colorblue',
		},
		{
			textdata : data.string.p9text6,
			textclass : 'question colorblue',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "two",
					imgsrc : '',
					imgid : '4'
				}
			]
		}]
	},
	// slide5
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion colorblue',
		},
		{
			textdata : data.string.p9text7,
			textclass : 'question colorblue',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "two",
					imgsrc : '',
					imgid : '5'
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: 'bluebg',
		uppertextblock:[{
			textdata : data.string.p9text1,
			textclass : 'commonquestion colorblue',
		},
		{
			textdata : data.string.p9text8,
			textclass : 'question colorblue',
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "two",
					imgsrc : '',
					imgid : '6'
				}
			]
		}]
	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "1", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "2", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "3", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "4", src: imgpath+"04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "5", src: imgpath+"05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "6", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "diy", src: imgpath+"do-it-yourself.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "t-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-3", src: 'images/textbox/white/l-1-b.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s9_p2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_option_image(content, countNext);
		put_speechbox_image(content, countNext);
		countNext!=3?vocabcontroller.findwords(countNext):"";
		switch(countNext) {
			case 0:
				play_diy_audio();
				nav_button_controls(2000);
				break;
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			if(countNext==1){
				sound_player("sound_1");
			}
			$('.coverboardfull').addClass('blurin');
			var count = 0;
				 $('.option').children().on('click',function(){
					if($(this).hasClass('correct')){

						count++;
						play_correct_incorrect_sound(1);
						$(this).addClass('mcqcor-option');
						$(this).parent().children('.incorrect').fadeOut(500);
						$(this).parent().css({'pointer-events':'none'});
						nav_button_controls(100);
						if(count>=2){
							setTimeout(function(){
								vocabcontroller.findwords(countNext);
							},1000);
							nav_button_controls(100);
						}
					} else{
						play_correct_incorrect_sound(0);
						$(this).addClass('mcqincor-option');
						$(this).css({'pointer-events':'none','color':'red'});
					}
				});
				//

				break;
			default:
				$prevBtn.show(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/

	 /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		CODE MODIFIED FOR THREE DIFFERENT HIGHTLIGHTS
	 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
		function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
              $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

              $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
              (stylerulename2 = "parsedstring2") ;

              $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
              (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
		/*=====  End of data highlight function  ======*/
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_option_image(content, count){
		if(content[count].hasOwnProperty('option')){
			var imageClass = content[count].option;
			console.log(imageClass);
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
			/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

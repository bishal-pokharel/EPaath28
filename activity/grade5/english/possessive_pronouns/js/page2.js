var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "text1",
		},{
			textdata: data.string.p2text2,
			textclass: "text2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bag1",
					imgsrc : '',
					imgid : 'bag'
				},
				{
					imgclass : "rita",
					imgsrc : '',
					imgid : 'girlwithoutbag'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textdata: data.string.p2text3,
			textclass: "ritatext1",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'highlighttext',
			textdata: data.string.p2text4,
			textclass: "ritatext2",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "ritawithbag",
					imgsrc : '',
					imgid : 'girlwithbag'
				}
			]
		}]
	},
	// slide2
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text5,
			textclass: "t1",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text5a,
			textclass: "t2",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text6,
			textclass: "t3",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text7,
			textclass: "t4",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "ritawithbag",
					imgsrc : '',
					imgid : 'girlwithbag'
				},
				{
					imgclass : "arrow1 arw1",
					imgsrc : '',
					imgid : 'arrow'
				},
				{
					imgclass : "arrow2 arw2",
					imgsrc : '',
					imgid : 'arrow'
				}
			]
		}]
	},
	// slide3
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text5,
			textclass: "t1",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text5a,
			textclass: "t2",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text6,
			textclass: "t3",
		},{
			datahighlightflag: true,
			datahighlightcustomclass: 'circle',
			textdata: data.string.p2text7,
			textclass: "t4",
		},{
			textdata: data.string.p2text8,
			textclass: "bottomtext",
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "ritawithbag",
					imgsrc : '',
					imgid : 'girlwithbag'
				},
				{
					imgclass : "arrow1",
					imgsrc : '',
					imgid : 'arrow'
				},
				{
					imgclass : "arrow2",
					imgsrc : '',
					imgid : 'arrow'
				}
			]
		}]
	},
	// slide4
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textclass:'toptext',
			textdata: data.lesson.chapter
		}],
		tableclass:'tableclass',
		row1headdata:data.string.p2text9,
		row2headdata:data.string.p2text10,
		row1data:data.string.p2text11,
		row2data:data.string.p2text12,

		table:[{

		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "blackboard",
					imgsrc : '',
					imgid : 'blackboard'
				}
			]
		}]
	},
	// slide5
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textclass:'upptext',
			textdata: data.string.p2text13
		},
		{
			textclass:'g1',
			textdata: data.string.p2text14
		},
		{
			textclass:'g2',
			textdata: data.string.p2text15
		},
		{
			textclass:'g3',
			textdata: data.string.p2text16
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "girlwithbag2",
					imgsrc : '',
					imgid : 'girlwithbag'
				},
				{
					imgclass : "girlwithbook2",
					imgsrc : '',
					imgid : 'girlwithbook'
				},
				{
					imgclass : "girlwithglasses2",
					imgsrc : '',
					imgid : 'girlwithglasses'
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass:'bluebg',
		uppertextblock:[{
			textclass:'upptext1',
			textdata: data.string.p2text17
		},
		{
			textclass:'ut1',
			textdata: data.string.p2text18
		},
		{
			textclass:'ut2',
			textdata: data.string.p2text19
		},
		{
			textclass:'ut3',
			textdata: data.string.p2text20
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "right01",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong01",
					imgsrc : '',
					imgid : 'wrong'
				}
			]
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bag", src: imgpath+"bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwithoutbag", src: imgpath+"school_girl-standing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwithbag", src: imgpath+"school_girl_with_bag.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwithbook", src: imgpath+"girl-holding-a-book.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girlwithglasses", src: imgpath+"girl-holding-a-eye-glasses.png", type: createjs.AbstractLoader.IMAGE},
			{id: "arrow", src: imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blackboard", src: imgpath+"blackboard.png", type: createjs.AbstractLoader.IMAGE},
			{id: "right", src: imgpath+"tick.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: imgpath+"cross.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s2_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s2_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s2_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s2_p6.ogg"},
			{id: "sound_6", src: soundAsset+"s2_p7.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$('.bag1').animate({"opacity":"1"},500);
				$('.rita').delay(1000).animate({"opacity":"1"},500);
				$('.text1').animate({"opacity":"1"},500);
				$('.text2').delay(1000).animate({"opacity":"1"},500);
				sound_nav("sound_0");
				break;
			case 1:
				$('.ritatext1').animate({"opacity":"1"},500);
				$('.ritatext2').delay(1000).animate({"opacity":"1"},500);
				sound_nav("sound_1");
				break;
			case 3:
				$('.textblock').css({"top":"-10%"});
				sound_nav("sound_3");
				break;
			case 4:
			    $('.type3').addClass('typingclass');
				setTimeout(function() {
				    $('.type4').addClass('typingclass');
					}, 1000);
					sound_nav("sound_4");
				break;
			case 5:
				sound_nav("sound_5");
				break;
			case 6:
				setTimeout(function() {
				    $('.wrong01').addClass('typingclass1');
					}, 100);
				setTimeout(function() {
				    $('.right01').addClass('typingclass1');
					}, 2000);
					sound_nav("sound_6");
				break;

			default:
			sound_nav("sound_"+(countNext));
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

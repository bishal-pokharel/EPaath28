var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
{
		contentblockadditionalclass: 'bg-blue',
		uppertextblock:[{
			textdata: data.string.p3text1,
			textclass: "DIY",
		}],
	},
	// slide0
	{
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "right1",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong1",
					imgsrc : '',
					imgid : 'wrong'
				},
				{
					imgclass : "right2",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong2",
					imgsrc : '',
					imgid : 'wrong'
				},
				{
					imgclass : "right3",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong3",
					imgsrc : '',
					imgid : 'wrong'
				},
				{
					imgclass : "right4",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong4",
					imgsrc : '',
					imgid : 'wrong'
				},
				{
					imgclass : "right5",
					imgsrc : '',
					imgid : 'right'
				},
				{
					imgclass : "wrong5",
					imgsrc : '',
					imgid : 'wrong'
				}

			]
		}],
		uppertextblock:[{
			textdata: data.string.p3text3,
			textclass: "q1",
		},
		{
			textdata: data.string.p3text3a,
			textclass: "q2",
		},
		{
			textdata: data.string.p3text4,
			textclass: "q3",
		},
		{
			textdata: data.string.p3text5,
			textclass: "q4",
		},
		{
			textdata: data.string.p3text6,
			textclass: "q5",
		},
		{
			textdata: data.string.p3text7,
			textclass: "a1",
		},
		{
			textdata: data.string.p3text7,
			textclass: "a2",
		},
		{
			textdata: data.string.p3text8,
			textclass: "a3",
		},
		{
			textdata: data.string.p3text9,
			textclass: "a4",
		},
		{
			textdata: data.string.p3text10,
			textclass: "a5",
		},
		{
			textdata: data.string.p3text2,
			textclass: "title",
		}],
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			{id: "data-xml", src: "data.xml", type: createjs.AbstractLoader.XML},
			//images
			{id: "right", src: imgpath+"right02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wrong", src: imgpath+"wrong02.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s3_p2.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
				break;
			case 1:
			sound_player("sound_1");
				var count=0;
				//question1
				$('.right1').click(function(){
					$('.a1').animate({'opacity':'1'},400);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong1').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					count++;
					if(count == 5){
					nav_button_controls(100);
				}
				});
				$('.wrong1').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				//question2
				$('.right2').click(function(){
					$('.a2').animate({'opacity':'1'},400);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.wrong2').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					count++;
					if(count == 5){
					nav_button_controls(100);
				}
				});
				$('.wrong2').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					$('.wrong2').css({"pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				//question3
				$('.wrong3').click(function(){
					$('.a3').animate({'opacity':'1'},400);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.right3').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					count++;
					if(count == 5){
					nav_button_controls(100);
				}
				});
				$('.right3').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				//question4
				$('.wrong4').click(function(){
					$('.a4').animate({'opacity':'1'},400);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.right4').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					count++;
					if(count == 5){
					nav_button_controls(100);
				}
				});
				$('.right4').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});
				//question5
				$('.wrong5').click(function(){
					$('.a5').animate({'opacity':'1'},400);
					$(this).css({"background":"green","pointer-events":"none"});
					$('.right5').css({"pointer-events":"none"});
					play_correct_incorrect_sound(1);
					count++;
					if(count == 5){
					nav_button_controls(100);
				}
				});
				$('.right5').click(function(){
					$(this).css({"background":"rgb(150,27,30)","pointer-events":"none"});
					play_correct_incorrect_sound(0);
				});

				break;

			default:
				$prevBtn.show(0);
				createjs.Sound.play('sound_3');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				//get list of classes
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				// console.log(selector);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

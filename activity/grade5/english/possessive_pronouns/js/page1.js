var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "coverpage",
					imgsrc : '',
					imgid : 'coverpage'
				}
			]
		}]
	},
	// slide1
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[
			{
				textdata: "",
				textclass: "backgrounddiv",
			},
			{
			textdata: data.string.p1text1,
			textclass: "text11",
		},{
			textdata: data.string.p1text2,
			textclass: "text21",
		},{
			textdata: data.string.p1text3,
			textclass: "text33",
		},],

	},
	// slide2
	{
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text4,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide3
	{
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text4,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'sp-2',
			textdata : data.string.p1text5,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "fairy",
					imgsrc : '',
					imgid : 'pari'
				}
			]
		}]
	},
	// slide4
	{
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text6,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl'
				}
			]
		}]
	},
	// slide5
	{
		speechbox:[{
			speechbox: 'sp-1',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text6,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		},
		{
			speechbox: 'sp-2',
			textdata : data.string.p1text7,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "fairy",
					imgsrc : '',
					imgid : 'pari'
				},
				{
					imgclass : "girl1",
					imgsrc : '',
					imgid : 'girl'
				}
			]
		}]
	},
	// slide6
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text8,
			textclass: "text1",
		}],
	},
	// slide7
	{
		speechbox:[{
			speechbox: 'sp-3',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text9,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [

				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-2'
				}
			]
		}]
	},
	//slide8
	{
		speechbox:[{
			speechbox: 'boydialogue',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text10,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy2'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				},
				{
					imgclass : "school1",
					imgsrc : '',
					imgid : 'school'
				},
				{
					imgclass : "studentboy",
					imgsrc : '',
					imgid : 'studentboy'
				},
				{
					imgclass : "studentgirl",
					imgsrc : '',
					imgid : 'studentgirl'
				}
			]
		}]
	},
	//slide9
	{
		speechbox:[{
			speechbox: 'boydialogue',
			datahighlightflag: true,
			datahighlightcustomclass: "purple_highlight",
			textdata : data.string.p1text11,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 'tb-2',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "boy1",
					imgsrc : '',
					imgid : 'boy2'
				},
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-3'
				},
				{
					imgclass : "dog1",
					imgsrc : '',
					imgid : 'dog'
				}
			]
		}]
	},
	// slide10
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "headerblock",
		},{
			textdata: data.string.p1text13,
			textclass: "text1",
		},{
			textdata: data.string.p1text14,
			textclass: "text3",
		},],

	},
	// slide11
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "headerblock",
		},{
			textdata: data.string.p1text15,
			textclass: "text4",
		}],

	},
	// slide12
	{
		contentblockadditionalclass: 'purplebg',
		uppertextblock:[{
			textdata: data.string.p1text12,
			textclass: "headerblock",
		},{
			textdata: data.string.p1text16,
			textclass: "text4",
		}],

	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	// readCSV();
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	//for preload
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "coverpage", src: imgpath+"cover_page.png", type: createjs.AbstractLoader.IMAGE},

			{id: "bg-1", src: imgpath+"bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy2", src: imgpath+"boy-pointing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pari", src: imgpath+"pari.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-2", src: imgpath+"bg_for_car.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg-3", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "studentgirl", src: imgpath+"reema01..png", type: createjs.AbstractLoader.IMAGE},
			{id: "studentboy", src: imgpath+"raj01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "school", src: imgpath+"school.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dog", src: imgpath+"dog-and-dog-house_01.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p2_1.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p2_2.ogg"},
			{id: "sound_4", src: soundAsset+"s1_p2_3.ogg"},
			{id: "sound_5", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_6", src: soundAsset+"s1_p5.ogg"},
			{id: "sound_7", src: soundAsset+"s1_p6_1.ogg"},
			{id: "sound_8", src: soundAsset+"s1_p6_2.ogg"},
			{id: "sound_9", src: soundAsset+"s1_p7.ogg"},
			{id: "sound_10", src: soundAsset+"s1_p8.ogg"},
			{id: "sound_11", src: soundAsset+"s1_p9.ogg"},
			// {id: "sound_12", src: soundAsset+"s1_p9_1.ogg"},
			{id: "sound_13", src: soundAsset+"s1_p10.ogg"},
			{id: "sound_14", src: soundAsset+"s1_p11_1.ogg"},
			{id: "sound_15", src: soundAsset+"s1_p11_2.ogg"},
			{id: "sound_16", src: soundAsset+"s1_p12.ogg"},
			{id: "sound_17", src: soundAsset+"s1_p13.ogg"},

		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_nav('sound_1');
				break;
			case 1:
			createjs.Sound.stop();
			current_sound = createjs.Sound.play("sound_2");
			current_sound.play();
				$('.text11').animate({"opacity":"1"},500);
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_3");
					current_sound.play();
				$('.text21').animate({"opacity":"1"},500);
				current_sound.on('complete',function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_4");
					current_sound.play();
				$('.text33').animate({"opacity":"1"},500);
				current_sound.on('complete',function(){
				nav_button_controls(0);
			});
		});
	});
				break;
				case 2:
				sound_nav('sound_6');
				break;
				case 3:
				sound_nav('sound_5');
				break;
				case 4:
				sound_nav('sound_7');
				break;
				case 5:
				sound_nav('sound_8');
				break;
			case 6:
				$('.text1').animate({"opacity":"1"},500);
				sound_nav('sound_9');
				break;
				case 7:
				sound_nav('sound_10');
				break;
				case 8:
				sound_nav('sound_11');
				break;
				case 9:
				sound_nav('sound_13');
				break;
			case 10:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_14");
				current_sound.play();
				$('.text1').animate({"opacity":"1"},3500);
				current_sound.on("complete", function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_15");
					current_sound.play();
					$('.text3').animate({"opacity":"1"},500);
					current_sound.on("complete", function(){
						nav_button_controls(0);
					});
				});
				break;
			case 11:
				$('.text4').animate({"opacity":"1"});
				sound_nav('sound_16');
				break;
			case 12:
				$('.text4').animate({"opacity":"1"});
						sound_nav('sound_17');
				break;
			default:
			nav_button_controls(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

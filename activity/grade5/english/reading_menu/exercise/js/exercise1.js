Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";

var content=[

	//ex1
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext1,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext1a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext1b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext1c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext1d,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "4.png",
					}
				]

			}
		]
	},
	//ex2
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext2,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext2b,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext2a,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext2c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext2d,
					}]

			}
		]
	},
	//ex3
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext3,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext3d,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext3a,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext3b,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext3c,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "chicken-chowmein.png",
					}
				]

			}
		]
	},
	//ex4
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext4,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext4b,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext4a,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext4c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext4d,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "mango-juice.png",
					}
				]

			}
		]
	},
	//ex5
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext5,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext5a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext5b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext5c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext5d,
					}]

			}
		]
	},
	//ex6
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext6,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext6c,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext6b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext6a,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext6d,
					}]

			}
		]
	},
	//ex7
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext7,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext7c,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext7a,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext7b,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext7d,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "bill.png",
					}
				]

			}
		]
	},
	//ex8
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext8,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext8a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext8b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext8c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext8d,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "veg.png",
					}
				]

			}
		]
	},
	//ex9
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext9,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext9a,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext9b,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext9c,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext9d,
					}],
				imageblockadditionalclass: 'display_image',
				imageblock : [
					{
						imgclass:'hint_image',
						imgsrc: imgpath + "veg.png",
					}
				]

			}
		]
	},
	//ex10
	{
    additionalclasscontentblock: "ole-background-gradient-pruplex",
		exerciseblock: [
			{
				textdata: data.string.ext10,

				exeoptions: [
					{
						forshuffle: "class1",
						optdata: data.string.ext10c,
					},
					{
						forshuffle: "class2",
						optdata: data.string.ext10a,
					},
					{
						forshuffle: "class3",
						optdata: data.string.ext10b,
					},
					{
						forshuffle: "class4",
						optdata: data.string.ext10d,
					}]

			}
		]
	},
];

/*remove this for non random questions*/
content.shufflearray();


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	/*for limiting the questions to 10*/
	var $total_page = 10;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

 	testin.init(10);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();

		/*for randomizing the options*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
			 while (divs.length) {
			        parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			  }

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
				if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							testin.update(true);
						}
						play_correct_incorrect_sound(1);
						$(this).siblings(".corctopt").show(0);
        	$(this).parent().addClass('corrects');
						$('.hint_image').show(0);
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
						$nextBtn.show(0);
					}
					else{
						testin.update(false);
						play_correct_incorrect_sound(0);

						$(this).siblings(".wrngopt").show(0);
          	$(this).css("pointer-events","none");
            	$(this).parent().addClass('incorrect');
						wrngClicked = true;
					}
				}
			});

		/*======= SCOREBOARD SECTION ==============*/
	}


	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg0 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_dg1 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p2_s8_0.ogg"));
var sound_dg8a = new buzz.sound((soundAsset + "p2_s8_1.ogg"));
var sound_dg8b = new buzz.sound((soundAsset + "p2_s8_2.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p2_s10_0.ogg"));
var sound_dg10a = new buzz.sound((soundAsset + "p2_s10_1.ogg"));
var sound_dg10b = new buzz.sound((soundAsset + "p2_s10_2.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "p2_s11.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "p2_s12.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "p2_s13.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "s2_p15.ogg"));
var sound_dg15 = new buzz.sound((soundAsset + "s2_p16.ogg"));


var content=[
{
	//slide0
	additionalclasscontentblock: 'contentwithbg ',
	uppertextblock : [
	{
		textclass : 'dialogueboxuncle1 template-dialougebox2-top-yellow fadein1 1d',
		textdata : data.string.p2text1
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "budaa",
				imgsrc : imgpath + "2.png"

			}
		]
	}]

},
{
	//slide1
	additionalclasscontentblock: 'contentwithbg1 ',
	uppertextblock : [
	{
		textclass : 'dialogueboxprem1 template-dialougebox2-top-flipped-yellow fadein1 2d ',
		textdata : data.string.p2text2
	},
	{
		textclass : 'dialogueboxuncle1 template-dialougebox2-top-yellow opacity1 3d',
		textdata : data.string.p2text1
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "budaa",
				imgsrc : imgpath + "3.png"

			}
		]
	}]
},

{
	//slide2
	additionalclasscontentblock: 'contentwithbg2',
	uppertextblock : [
	{
		textclass : 'dialogueboxprem1 template-dialougebox2-top-flipped-yellow opacity1 4d',
		textdata : data.string.p2text2
	},
	{
		textclass : 'dialogueboxuncle1 template-dialougebox2-top-yellow opacity1 5d',
		textdata : data.string.p2text1
	},
	{
		textclass : 'dialogueboxmaya1 template-dialougebox2-top-yellow fadein1 6d',
		textdata : data.string.p2text3
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "buda",
				imgsrc : imgpath + "4.png"

			}
		]
	}]
},
{
	//slide3
	additionalclasscontentblock: 'contentwithbg2',
	uppertextblock : [
	{
		textclass : 'dialogueboxbuda1 template-dialougebox2-top-flipped-yellow fadein1 7d',
		textdata : data.string.p2text4
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "buda",
				imgsrc : imgpath + "1.png"

			}
		]
	}]
},

{
	//slide4
	additionalclasscontentblock: 'ole-background-gradient-barley',
	uppertextblock : [
	{
		textclass : 'toptext fadein1',
		textdata : 'After 15 minutes...'
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "clock",
				imgsrc : imgpath + "clock01.png"

			},
			{
				imgclass : "hour",
				imgsrc : imgpath + "hour.png"

			},
			{
				imgclass : "mins",
				imgsrc : imgpath + "minute.png"

			},
			{
				imgclass : "dot",
				imgsrc : imgpath + "yellowdot.png"

			}
		]
	}]
},
{
	//slide5
	additionalclasscontentblock: 'contentwithbg3',
	uppertextblock : [
	{
		textclass : 'dialogueboxwaiter1 template-dialougebox2-top-flipped-yellow fadein1 8d',
		textdata : data.string.p2text5
	}
	]
},
{
	//slide6
	additionalclasscontentblock: 'contentwithbg4',
	uppertextblock : [
	{
		textclass : 'dialogueboxprem2 template-dialougebox2-top-yellow fadein1 9d',
		textdata : data.string.p2text6
	}
	]
},
{
	//slide7
	additionalclasscontentblock: 'contentwithbg5',
	uppertextblock : [
	{
		textclass : 'dialogueboxmaya3 template-dialougebox2-top-flipped-yellow fadein1 10d',
		textdata : data.string.p2text7
	}
	]
},
{
	//slide8
	additionalclasscontentblock: 'contentwithbg6',
	uppertextblock : [
	{
		textclass : 'dialogueboxprem3 template-dialougebox2-top-flipped-yellow fadein1 11d',
		textdata : data.string.p2text8
	},
	{
		textclass : 'dialogueboxmaya4 template-dialougebox2-top-yellow fadein2 12d',
		textdata : data.string.p2text8a
	},
	{
		textclass : 'dialogueboxuncle3 template-dialougebox2-top-yellow fadein3 13d',
		textdata : data.string.p2text9
	}
	]
},
{
	//slide9
	additionalclasscontentblock: 'contentwithbg7',
	uppertextblock : [
	{
		textclass : 'dialogueboxuncle4 template-dialougebox2-top-yellow fadein1 14d',
		textdata : data.string.p2text10
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "buda",
				imgsrc : imgpath + "1.png"

			}
		]
	}]
},

{
	//slide10
	additionalclasscontentblock: 'contentwithbg8',
	uppertextblock : [
	{
		textclass : 'dialogueboxbuda2 template-dialougebox2-top-flipped-yellow fadein1 15d',
		textdata : data.string.p2text11
	},
	{
		textclass : 'toptext',
		textdata : data.string.p2text12
	},
	{
		textclass : 'toptext1',
		textdata : data.string.p2text13
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "buda1",
				imgsrc : imgpath + "billbuda.png"

			},
			{
				imgclass : "billbuda",
				imgsrc : imgpath + "billllyes.png"

			},
			{
				imgclass : "mainbill",
				imgsrc : imgpath + "bill-10.png"

			}
		]
	}]
},
{
	//slide11
	additionalclasscontentblock: 'contentwithbg9',
	uppertextblock : [
	{
		textclass : 'dialogueboxuncle5 template-dialougebox2-top-yellow fadein1 16d ',
		textdata : data.string.p2text14
	}
	]
},
{
	//slide12
	additionalclasscontentblock: 'contentwithbg10',
	uppertextblock : [
	{
		textclass : 'dialogueboxbuda3 template-dialougebox2-top-flipped-yellow fadein1 17d',
		textdata : data.string.p2text15
	}
	]
},
{
	//slide13
	additionalclasscontentblock: 'contentwithbg3a',
	uppertextblock : [
	{
		textclass : 'dialogueboxboth template-dialougebox2-top-flipped-yellow fadein1 18d',
		textdata : data.string.p2text5a
	}
	]
},

{
	//slide14
	additionalclasscontentblock: 'contentwithbg3a',
	uppertextblock : [
	{
		textclass : 'dialogueboxboth1 template-dialougebox2-top-flipped-yellow fadein1 19d',
		textdata : data.string.p2text5b
	}
	]
},

{
	//slide15
	additionalclasscontentblock: 'contentwithbg3a',
	uppertextblock : [
	{
		textclass : 'dialogueboxboth2 template-dialougebox2-top-flipped-yellow fadein1 20d',
		textdata : data.string.p2text5c
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		switch (countNext) {
			case 0:
			playaudio(sound_dg0, $(".1d"));
			break;

			case 1:
			playaudio(sound_dg1, $(".2d"));
			$prevBtn.show(0);
			break;

			case 2:
			$prevBtn.show(0);
			playaudio(sound_dg2, $(".6d"));
			break;

			case 3:
			$prevBtn.show(0);
			playaudio(sound_dg3, $(".7d"));
			break;

			case 4:
			$prevBtn.show(0);
			playaudio(sound_dg4, $(".5d"));
			break;

			case 5:
			$prevBtn.show(0);
			playaudio(sound_dg5, $(".8d"));
			break;

			case 6:
			$prevBtn.show(0);
			playaudio(sound_dg6, $(".9d"));
			break;

			case 7:
			$prevBtn.show(0);
			playaudio(sound_dg7, $(".10d"));
			break;

			case 8:
			$prevBtn.show(0);
			playaudio(sound_dg8, $(".11d"));
			setTimeout(function(){
				playaudio(sound_dg8a, $(".12d"));
			},3000);
			setTimeout(function(){
				playaudio(sound_dg8b, $(".13d"));
			},6000);
			break;

			case 9:
			$prevBtn.show(0);
			playaudio(sound_dg9, $(".14d"));
			break;

			case 10:
			playaudio1(sound_dg10a, $(".15d"));
			setTimeout(function(){
				playaudio1(sound_dg10, $(""));
			},3000);
			$('.billbuda').click(function(){
				setTimeout(function(){
					playaudio(sound_dg10b, $(""));
				},1000);
				$('.menuu,.buda1,.toptext,.dialogueboxbuda2').addClass('fadeout');
				$(this).addClass('fadeout');
				$('.mainbill').css('width','60%').addClass('fadein1');
				$('.contentblock').addClass('backgroundanimation');
				$('.toptext1').addClass('fadein12');
				// $nextBtn.show(0);
			});
			break;
			case 11:
			$prevBtn.show(0);
			playaudio(sound_dg11, $(".16d"));
			break;
			case 12:
			$prevBtn.show(0);
			playaudio(sound_dg12, $(".17d"));
			break;
			case 13:
			$prevBtn.show(0);
			playaudio(sound_dg13, $(".18d"));
			break;
			case 14:
			$prevBtn.show(0);
			playaudio(sound_dg14, $(".19d"));
			break;
			case 15:
			$prevBtn.show(0);
			playaudio(sound_dg15, $(".20d"));
			break;
		}
	}

	var sound_control;

	function playaudio(sound_data, $dialog_container){
		sound_control = sound_data;
		var playing = true;

		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){

				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
					$prevBtn.show(0);
				}else if(countNext==0){
					$nextBtn.show(0);
					$prevBtn.hide(0);
				}else{
					switch (countNext) {
						case 8:
						setTimeout(function(){
							$nextBtn.show(0);
							$prevBtn.show(0);
						},6000);
							break;
						default:
						$nextBtn.show(0);
						$prevBtn.show(0);
					}
				}
			}, 1000);
		});
	}

	function playaudio1(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.click(function(){
			if(!playing){
				playaudio1(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		setTimeout(function(){
			sound_data.play();
		}, 1200);
		sound_data.bind('ended', function(){
			setTimeout(function(){
				if(countNext==10){
					$prevBtn.hide(0);
					$nextBtn.hide(0);
				}else{

				}

				playing = false;
				$dialog_container.css('cursor', 'pointer');
				$dialog_container.children('.audio_icon').show(0);
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{

				}
			}, 1000);
		});
	}



	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		sound_control.stop();
		templateCaller();
	});

	$refreshBtn.click(function(){
		sound_control.stop();
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		sound_control.stop();
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg0 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_dg1 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_dg_4 = new buzz.sound((soundAsset + "s1_p5.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var content=[
{
	//slide0
	additionalclasscontentblock: "ole-background-gradient-pruplex",
	uppertextblock : [
	{
		textclass : 'lesson-title',
		textdata : data.lesson.chapter
	}
	],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "mainback",
				imgsrc : imgpath + "cover_page.png"

			}
		]
	}],

},
{
	//slide1
	additionalclasscontentblock: "contentwithbg",
	uppertextblock:[
		{
			textclass: 'dialogueboxmaya1 template-dialougebox2-top-flipped-yellow fadein1 1d',
			textdata : data.string.p1text1
		}
	]
},

{
	//slide2
	additionalclasscontentblock: "contentwithbga",
	uppertextblock:[
		{
			textclass: 'dialogueboxprem1 template-dialougebox2-top-yellow 2d',
			textdata : data.string.p1text2
		},
		{
			textclass: 'dialogueboxmaya1 template-dialougebox2-top-flipped-yellow opacity1',
			textdata : data.string.p1text1
		}
	]
},
{
	//slide3
	additionalclasscontentblock: "contentwithbg1",
	uppertextblock:[
		{
			textclass: 'dialogueboxuncle1 template-dialougebox2-top-yellow fadein1 3d',
			textdata : data.string.p1text3
		}
	]
},
{
	//slide4
	additionalclasscontentblock: "contentwithbg2",
	uppertextblock:[
		{
			textclass: 'dialogueboxuncle2 template-dialougebox2-top-yellow fadein1 4d',
			textdata : data.string.p1text4
		}
	]
},
{		//slide5
	additionalclasscontentblock: 'ole-background-gradient-opal',
	uppertextblock:[

		{
			textclass: 'menudesc fadein1',
			textdata : data.string.p1text5
		}
	]
},
{	//slide6
	additionalclasscontentblock: "contentwithbg2a",
	uppertextblock:[
		{
			textclass: 'toptext fontplus',
			textdata : data.string.menuclick,
			datahighlightflag:true,
			datahighlightcustomclass:'menubold'
		}],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "buda",
				imgsrc : imgpath + "1.png"

			}
		]
	}],
},
{	//slide7
	uppertextblock:[
		{
			textclass: 'menudescription',
			textdata : data.string.p1text6,
		}],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu1",
				imgsrc : imgpath + "menufull.png"

			}
		]
	}],
},
{	//slide8
	uppertextblock:[
		{
			textclass: 'menudescription',
			textdata : data.string.p1text7,
		}],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu1",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "mark",
				imgsrc : imgpath + "black.png"

			}
		]
	}],
},
{	//slide9
	uppertextblock:[
		{
			textclass: 'menudescription',
			textdata : data.string.p1text8,
		}],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu1",
				imgsrc : imgpath + "menufull.png"

			},
			{
				imgclass : "mark1",
				imgsrc : imgpath + "black.png"

			}
		]
	}],
},
{	//slide10
	uppertextblock:[
		{
			textclass: 'menudescription',
			textdata : data.string.p1text9,
		}],
	imageblock: [
	{
		imagetoshow: [
			{
				imgclass : "menuu1",
				imgsrc : imgpath + "menufull.png"

			}
		]
	}],
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;

	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "cover-img", src: imgpath+"cover-page.png", type: createjs.AbstractLoader.IMAGE},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);
			$prevBtn.hide(0);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		$nextBtn.hide(0);
		switch (countNext) {
			case 0:

			playaudio(sound_dg0, $(".1d"));
				$prevBtn.hide(0);
				break;
			case 1:
				playaudio(sound_dg1, $(".1d"));
						$prevBtn.show(0);
				break;

			case 2:
				playaudio(sound_dg2, $(".2d"));
				$prevBtn.show(0);
				break;

			case 3:
				playaudio(sound_dg3, $(".3d"));
				$prevBtn.show(0);
				break;

			case 4:
				playaudio(sound_dg4, $(".4d"));
				$prevBtn.show(0);
				break;

			case 5:
			playaudio(sound_dg5, $(".4d"));
			$prevBtn.show(0);
				break;

			case 6:
				$prevBtn.show(0);
			playaudio(sound_dg6, $(".4d"));
				$('.menuu').click(function(){
					$(this).addClass("zoomin");
					setTimeout(function(){
					$nextBtn.show(0);
						$prevBtn.show(0);
					},3000);
				});
				break;

			case 7:
				$prevBtn.show(0);
			playaudio(sound_dg7, $(""));
			$('.menuu1').animate({
				height:"92%"
			})
			break;

			case 8:
				$prevBtn.show(0);
			playaudio(sound_dg8, $(""));
			$('.menuu1').animate({
				height:"92%"
			})
			break;

			case 9:
				$prevBtn.show(0);
			playaudio(sound_dg9, $(""));
			$('.menuu1').animate({
				height:"92%"
			})
			break;

			case 10:
				$prevBtn.show(0);
			playaudio(sound_dg10, $(""));
			$('.menuu1').animate({
				height:"92%"
			})
			break;

		}
	}
	var sound_control;

	function playaudio(sound_data, $dialog_container){
		sound_control = sound_data;
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				// $prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
						$prevBtn.show(0);
				}else 		if(countNext==0){
							$prevBtn.hide(0);
								$nextBtn.show(0);
						}else if(countNext==6){
							$nextBtn.hide(0);
							$prevBtn.hide(0);
						}else{

					$nextBtn.show(0);
					 $prevBtn.show(0);
				}
			}, 1000);
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		sound_control.stop();
		templateCaller();
	});

	$refreshBtn.click(function(){
		sound_control.stop();
		templateCaller();

	});

	$prevBtn.on("click", function(){
		countNext--;
		sound_control.stop();
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg0 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_dg1 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_dg8 = new buzz.sound((soundAsset + "p2_s8_0.ogg"));
var sound_dg8a = new buzz.sound((soundAsset + "p2_s8_1.ogg"));
var sound_dg8b = new buzz.sound((soundAsset + "p2_s8_2.ogg"));
var sound_dg9 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_dg10 = new buzz.sound((soundAsset + "p2_s10_0.ogg"));
var sound_dg10a = new buzz.sound((soundAsset + "p2_s10_1.ogg"));
var sound_dg10b = new buzz.sound((soundAsset + "p2_s10_2.ogg"));
var sound_dg11 = new buzz.sound((soundAsset + "p2_s11.ogg"));
var sound_dg12 = new buzz.sound((soundAsset + "p2_s12.ogg"));
var sound_dg13 = new buzz.sound((soundAsset + "p2_s13.ogg"));
var sound_dg14 = new buzz.sound((soundAsset + "p2_s14.ogg"));
var sound_dgsum = new buzz.sound((soundAsset + "sum.ogg"));
var sound_dg15 = new buzz.sound((soundAsset + "p2_s15.ogg"));
var sound_dg16 = new buzz.sound((soundAsset + "p2_s16.ogg"));
var sound_dg17 = new buzz.sound((soundAsset + "p2_s17.ogg"));
var sound_dg18 = new buzz.sound((soundAsset + "p2_s18.ogg"));


var content=[
  //slide 0
  {
    additionalclasscontentblock: 'backgrd',
    imageblock:[{
      imagetoshow: [
        {
          imgclass : "bgcov",
          imgsrc : imgpath + "bg_new.png"

        }
      ],
    }],
    uppertextblock:[

      {
        textclass: 'summry',
        textdata : data.string.page2slide1text20,
      }
      ]
  },

  {
  	//slide 1
  	additionalclasscontentblock: 'backgrd',
    imagedivblock:[{
      imagediv:"imgdiv1",
      imagestoshow:[{
        imgclass : "rest fadein1",
    			imgsrc : imgpath + "img1.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata:data.string.page2slide1text19
        }]
    }]
  },
  {
  	//slide 2
    additionalclasscontentblock: 'backgrd',
    imagedivblock:[{
      imagediv:"imgdiv1",
      imagestoshow:[{
        imgclass : "rest fadein1",
          imgsrc : imgpath + "img1.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
        	datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata:data.string.page2slide1text19
        }]
    },{
      imagediv:"imgdiv2",
      imagestoshow:[{
        imgclass : "rest1  fadein1",
    		imgsrc : imgpath + "img2.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text22,
        }]
    }]
  },

  {
  	//slide 3
    additionalclasscontentblock: 'backgrd',
    imagedivblock:[{
      imagediv:"imgdiv1",
      imagestoshow:[{
        imgclass : "rest fadein1",
          imgsrc : imgpath + "img1.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
        	datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata:data.string.page2slide1text19
        }]
    },{
      imagediv:"imgdiv2",
      imagestoshow:[{
        imgclass : "rest1  fadein1",
    		imgsrc : imgpath + "img2.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text22
        }]
    },{
      imagediv:"imgdiv3",
      imagestoshow:[{
        imgclass : "rest2 fadein1",
  		  imgsrc : imgpath + "img3.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text23
        }]
    }]
  },
  {
  	//slide 4
    additionalclasscontentblock: 'backgrd',
    imagedivblock:[{
      imagediv:"imgdiv1",
      imagestoshow:[{
        imgclass : "rest fadein1",
          imgsrc : imgpath + "img1.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata:data.string.page2slide1text19
        }]
    },{
      imagediv:"imgdiv2",
      imagestoshow:[{
        imgclass : "rest1  fadein1",
        imgsrc : imgpath + "img2.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text22
        }]
    },{
      imagediv:"imgdiv3",
      imagestoshow:[{
        imgclass : "rest2 fadein1",
        imgsrc : imgpath + "img3.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text23
        }]
    },{
      imagediv:"imgdiv4",
      imagestoshow:[{
        imgclass : "rest3 bil fadein1",
      	imgsrc : imgpath + "img4.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text24
        }]
    }]
  },
  {
  	//slide 5
    additionalclasscontentblock: 'backgrd',
    imagedivblock:[{
      imagediv:"imgdiv1",
      imagestoshow:[{
        imgclass : "rest fadein1",
          imgsrc : imgpath + "img1.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata:data.string.page2slide1text19
        }]
    },{
      imagediv:"imgdiv2",
      imagestoshow:[{
        imgclass : "rest1  fadein1",
        imgsrc : imgpath + "img2.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text22
        }]
    },{
      imagediv:"imgdiv3",
      imagestoshow:[{
        imgclass : "rest2 fadein1",
        imgsrc : imgpath + "img3.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text23
        }]
    },{
      imagediv:"imgdiv4",
      imagestoshow:[{
        imgclass : "rest3 bil fadein1",
      	imgsrc : imgpath + "img4.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text24
        }]
    },{
      imagediv:"imgdiv5",
      imagestoshow:[{
        imgclass : "rest4 bil fadein1",
       	imgsrc : imgpath + "img5.png"
      }],
      imagelabels:true,
        imagelabels:[{
          datahighlightflag:true,
          datahighlightcustomclass:'menubold',
          imagelabelclass:"bottomtext",
          imagelabeldata: data.string.page2slide1text25
        }]
    }]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$(".menuroll").one("click",function(){
			$(this).toggleClass("flipped");
			clickcount++;
			if(clickcount == 6)
				$nextBtn.show(0);
		});
		switch (countNext) {
			case 0:
			playaudio(sound_dgsum, $(".21d"));
			break;
			case 1:
      playaudio(sound_dg14, $(".21d"));
			break;
			case 2:
      playaudio(sound_dg15, $(".21d"));
			break;
      case 3:
      playaudio(sound_dg16, $(".21d"));
      break;
      case 4:
      playaudio(sound_dg17, $(".21d"));
      break;
      case 5:
      playaudio(sound_dg18, $(".21d"));

      break;
		}
	}
  var sound_control;
	function playaudio(sound_data, $dialog_container){
		sound_control = sound_data;
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){

				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
          	$prevBtn.show(0);
				}else if(countNext==0){
          	$nextBtn.show(0);
            $prevBtn.hide(0);
        }else{
					switch (countNext) {
						default:
						$nextBtn.show(0);
            $prevBtn.show(0);
					}
				}
			}, 1000);
		});
	}





	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		sound_control.stop();
    templateCaller();
	});

	$refreshBtn.click(function(){
		sound_control.stop();
    templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
    sound_control.stop();
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_dg0 = new buzz.sound((soundAsset + "s4_p2.ogg"));
var sound_dg1 = new buzz.sound((soundAsset + "s4_p3.ogg"));
var sound_dg2 = new buzz.sound((soundAsset + "s4_p4.ogg"));
var sound_dg3 = new buzz.sound((soundAsset + "s4_p5.ogg"));
var sound_dg4 = new buzz.sound((soundAsset + "s4_p6.ogg"));
var sound_dg5 = new buzz.sound((soundAsset + "s4_p8.ogg"));
var sound_dg6 = new buzz.sound((soundAsset + "s4_p9.ogg"));
var sound_dg7 = new buzz.sound((soundAsset + "s4_p10.ogg"));


var ordereditems = [
	[data.string.food1, imgpath + '',data.string.price1,false],
	[data.string.food2, imgpath + '',data.string.price2,false],
	[data.string.food3, imgpath + '',data.string.price3,false],
	[data.string.food4, imgpath + '',data.string.price4,false],
	[data.string.food5, imgpath + '',data.string.price5,false],
	[data.string.drink1, imgpath + '',data.string.price6,false],
	[data.string.drink2, imgpath + '',data.string.price7,false],
];
var content=[
	{
		//slide0
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'diystyle boldtoptext',
				textdata : data.string.page3text1
			},

		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "diybg",
						imgsrc : imgpath + "bg_diy.png"

					},
				]
			}
		]
	},
	{
		//slide1
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext boldtoptext fadein',
				textdata : data.string.page3text2
			},
			{
				textclass: 'incorrect option1 fadein1',
				textdata : data.string.page3text3xa
			},

			{
				textclass: 'incorrect option2 fadein1',
				textdata : data.string.page3text3x
			},

			{
				textclass: 'correct option3 fadein1',
				textdata : data.string.page3text5
			},

			{
				textclass: 'incorrect option4 fadein1',
				textdata : data.string.page3text3xb
			},
		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "sideboy",
						imgsrc : imgpath + "prem.png"

					},
				]
			}
		]
	},
	{
		//slide2
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext boldtoptext',
				textdata : data.string.page3text7
			}
		],
		imageblock: [
			{
				imagetoshow: [

					{
						imgclass : "restrauntclass fadein1",
						imgsrc : imgpath + "restrauntonly.png"

					},
					{
						imgclass : "sideboy1 fadein1",
						imgsrc : imgpath + "prem.png"

					},

				]
			}
		]
	},
	{
		//slide3
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext boldtoptext',
				textdata : data.string.page3text10
			},

			{
				textclass: 'incorrect option1 fadein1',
				textdata : data.string.page3text6
			},

			{
				textclass: 'incorrect option2 fadein1',
				textdata : data.string.page3text3a
			},

			{
				textclass: 'incorrect option3 fadein1',
				textdata : data.string.page3text3
			},

			{
				textclass: 'correct option4 fadein1',
				textdata : data.string.page3text11
			},
		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "sideboy",
						imgsrc : imgpath + "char1.png"

					},
				]
			}
		]
	},
	{
		//slide4
		additionalclasscontentblock: 'backgrd',
		additionaltextblockclass:'left',
		uppertextblock:[
			{
				textclass: 'toptext1',
				textdata : data.string.page3text12
			},
			{
				textclass: 'foodtitle ',
				textdata : data.string.food
			},
			{
				textclass: 'option1a food',
				textdata : data.string.food1
			},

			{
				textclass: 'option2a food',
				textdata : data.string.food2
			},

			{
				textclass: 'option3a food',
				textdata : data.string.food3
			},
			{
				textclass: 'option4a food',
				textdata : data.string.food4
			},

			{
				textclass: 'option5a food',
				textdata : data.string.food5
			},
			{
				textclass: 'price1 ',
				textdata : data.string.price1
			},

			{
				textclass: 'price2 ',
				textdata : data.string.price2
			},

			{
				textclass: 'price3 ',
				textdata : data.string.price3
			},
			{
				textclass: 'price4 ',
				textdata : data.string.price4
			},

			{
				textclass: 'price5 ',
				textdata : data.string.price5
			},
			{
				textclass: 'drinktitle ',
				textdata : data.string.drinks
			},
			{
				textclass: 'option6a food',
				textdata : data.string.drink1
			},
			{
				textclass: 'option7a food',
				textdata : data.string.drink2
			},
			{
				textclass: 'price6 ',
				textdata : data.string.price6
			},
			{
				textclass: 'price7 ',
				textdata : data.string.price7
			}
		],
		imageblock: [
			{
				imagetoshow: [

					{
						imgclass : "menubg",
						imgsrc : imgpath + "blankmenu.png"

					}

				]
			}
		]
	},
	{
		//slide5
		additionalclasscontentblock: 'backgrd',
		additionaltextblockclass:'righttext',
		uppertextblock:[
			{
				textclass: 'toptext2',
				textdata : data.string.page3text13
			},
			{
				textclass: 'foodtitle ',
				textdata : data.string.food
			},
			{
				textclass: 'option1a food',
				textdata : data.string.food1
			},

			{
				textclass: 'option2a food',
				textdata : data.string.food2
			},

			{
				textclass: 'option3a food',
				textdata : data.string.food3
			},
			{
				textclass: 'option4a food',
				textdata : data.string.food4
			},

			{
				textclass: 'option5a food',
				textdata : data.string.food5
			},
			{
				textclass: 'price1 ',
				textdata : data.string.price1
			},

			{
				textclass: 'price2 ',
				textdata : data.string.price2
			},

			{
				textclass: 'price3 ',
				textdata : data.string.price3
			},
			{
				textclass: 'price4 ',
				textdata : data.string.price4
			},

			{
				textclass: 'price5 ',
				textdata : data.string.price5
			},
			{
				textclass: 'drinktitle ',
				textdata : data.string.drinks
			},
			{
				textclass: 'option6a food',
				textdata : data.string.drink1
			},
			{
				textclass: 'option7a food',
				textdata : data.string.drink2
			},
			{
				textclass: 'price6 ',
				textdata : data.string.price6
			},
			{
				textclass: 'price7 ',
				textdata : data.string.price7
			},
			{
				textclass: 'button ',
				textdata : 'done'
			},
		],
		imageblock: [
			{
				imagetoshow: [

					{
						imgclass : "menubg1",
						imgsrc : imgpath + "blankmenu.png"

					}
				]
			}
		],
		orderlist:[{}]
	},//slide6
	{
		additionalclasscontentblock: 'backgrd',
		additionalclasscontentblock:'bgbg',
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "samosa",
						imgsrc : imgpath + "samosa.png"

					},
					{
						imgclass : "yogurt",
						imgsrc : imgpath + "yogurt.png"

					},
					{
						imgclass : "paratha",
						imgsrc : imgpath + "paratha.png"

					},
					{
						imgclass : "daalbhaat",
						imgsrc : imgpath + "daalbhaat.png"

					},
					{
						imgclass : "rotitarkari",
						imgsrc : imgpath + "rotitarkari.png"

					},
					{
						imgclass : "tea",
						imgsrc : imgpath + "tea.png"

					},
					{
						imgclass : "juice",
						imgsrc : imgpath + "juice.png"

					},
				]
			}
		]
	},
	//slide7
	{
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext boldtoptext',
				textdata : data.string.page3text14
			},

			{
				textclass: 'incorrect option1 fadein1',
				textdata : data.string.page3text3a
			},

			{
				textclass: 'incorrect option2 fadein1',
				textdata : data.string.page3text6
			},

			{
				textclass: 'incorrect option3 fadein1',
				textdata : data.string.page3text6a
			},
			{
				textclass: 'correct option4 fadein1',
				textdata : data.string.page3text15
			},
		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "sideboy2",
						imgsrc : imgpath + "char1.png"

					},
				]
			}
		]
	},
	//slide8
	{
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext2 boldtoptext',
				textdata : data.string.page3text16
			}
		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "billpos fadein1",
						imgsrc : imgpath + "emptybill.png"

					},
				]
			}
		],
		textblocklist:[
			{
				quantity:"1",
				itemclass:"i1",
				amountclass:"m1",
				quantityclass:"quantity"
			},
			{
				quantity:"2",
				itemclass:"i2",
				amountclass:"m2",
				quantityclass:"quantity"
			},
			{
				quantity:"3",
				itemclass:"i3",
				amountclass:"m3",
				quantityclass:"quantity"
			},
			{
				quantity:"4",
				itemclass:"i4",
				amountclass:"m4",
				quantityclass:"quantity"
			},
			{
				quantity:"5",
				itemclass:"i5",
				amountclass:"m5",
				quantityclass:"quantity"
			},
			{
				quantity:"6",
				itemclass:"i6",
				amountclass:"m6",
				quantityclass:"quantity"
			},
			{
				quantity:"7",
				itemclass:"i7",
				amountclass:"m7",
				quantityclass:"quantity"
			},
		],
		textblock:[
			{
				textdiv:"totalrs",
				textclass:"centertext totalprice",
				textdata:""
			}
		]
	},
	{
		additionalclasscontentblock: 'backgrd',
		uppertextblock:[

			{
				textclass: 'toptext3',
				textdata : data.string.page3text17
			}
		],
		imageblock: [
			{
				imagetoshow: [
					{
						imgclass : "monkey fadein1",
						imgsrc : imgpath + "welldone.png"

					},
				]
			}
		],
	},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
	var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	inorder to use the handlebar partials we need to register them
	to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	How To:
	- Just call the navigation controller if it is to be called from except the
	last page of lesson
	- If called from last page set the islastpageflag to true such that
	footernotification is called for continue button to navigate to exercise
	*/

	/**
	What it does:
	- If not explicitly overriden the method for navigation button
	controls, it shows the navigation buttons as required,
	according to the total count of pages and the countNext variable
	- If for a general use it can be called from the templateCaller
	function
	- Can be put anywhere in the template function as per the need, if
	so should be taken out from the templateCaller function
	- If the total page number is
	*/
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.hide(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(1);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$('.incorrect').click(function(){
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
			var centerY = ((position.top + height)*100)/$board.height()+'%';
			$('<img id="corr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(1350%,0%)" src="images/wrong.png" />').insertAfter(this);

			play_correct_incorrect_sound(0);
			// $('.imageblock').append('<img class="incorrimage" src="images/wrong.png" />');

			$(this).css('pointer-events', 'none');
			$(this).addClass('incorrect-answer');
		});
		$('.correct').click(function(){
			var $this = $(this);
			var position = $this.position();
			var width = $this.width();
			var height = $this.height();
			var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
			var centerY = ((position.top + height)*100)/$board.height()+'%';
			$('<img id="corr" style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(1350%,0%)" src="images/correct.png" />').insertAfter(this);
			play_correct_incorrect_sound(1);

			$nextBtn.show(0);
			$(this).addClass("correct-answer");
			$(".option1,.option2,.option3,.option4").css("pointer-events","none");
			// $('.incorrect').unbind("click").css("border","none");
		});

		switch(countNext) {
			case 0:
			play_diy_audio();
			break;
			case 1:
			soundplayer(sound_dg0,0);
			break;
			case 2:
			soundplayer(sound_dg1,1);
			break;
			case 3:
			soundplayer(sound_dg2,0);
			break;
			case 4:
			$('.textblock').css({"pointer-events" : "none"});
			soundplayer(sound_dg3,1);
			break;
			case 5:
			soundplayer(sound_dg4,0);
			$('.button').addClass("avoid-clicks");
			var total = 0;
			$('.option1a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option1a').hasClass('opt1')){

					$('.option1a').css("background-color", "rgb(255,154,89)" );
					$('.warning').html("");
					$('.option1a').removeClass('opt1');
					ordereditems[0][3] = false;
					total = total - 120;console.log(total);
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total+120==201 || total+120>201)
					{
						$('.warning').html("You don't have enough money to order Parattha." ).removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total+120<201)
						{
							total = total + 120;console.log(total);
							$('.warning').html("");
							ordereditems[0][3] = true;
							$('.option1a').css("background-color", "rgb(182, 97, 42)" );
							$('.option1a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}
				}

			});
			$('.option2a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option2a').hasClass('opt1')){
					$('.option2a').css("background-color", "rgb(255,154,89)" );
					$('.warning').html("");
					total = total - 25;
					$('.total1').html('Total  : Rs ' + total);
					ordereditems[1][3] = false;
					$('.option2a').removeClass('opt1');


				}
				else{
					if(total + 25==201 || total + 25>201)
					{
						$('.warning').html("You don't have enough money to order Samosa.").removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total + 25<201)
						{
							total = total + 25;
							$('.warning').html("");
							ordereditems[1][3] = true;
							$('.option2a').css("background-color", "rgb(182, 97, 42)" );
							$('.option2a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}
				}

			});
			$('.option3a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option3a').hasClass('opt1')){
					$('.warning').html("");
					ordereditems[2][3] = false;
					$('.option3a').removeClass('opt1');

					$('.option3a').css("background-color", "rgb(255,154,89)" );
					total = total -180;
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total + 180==201 || total + 180>201)
					{
						$('.warning').html("You don't have enough money to order Dal Bhat Tarkari.").removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total + 180<201)
						{
							total = total + 180;
							$('.option3a').css("background-color", "rgb(182, 97, 42)" );
							$('.warning').html("");
							ordereditems[2][3] = true;
							$('.option3a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}
				}

			});
			$('.option4a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option4a').hasClass('opt1')){

					$('.warning').html("");
					$('.option4a').css("background-color", "rgb(255,154,89)" );
					ordereditems[3][3] = false;
					$('.option4a').removeClass('opt1');

					total = total - 120;
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total+120==201 || total+120>201)
					{
						$('.warning').html("You don't have enough money to order Roti Tarkari.").removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total+120<201)
						{
							total = total + 120;
							$('.warning').html("");
							$('.option4a').css("background-color", "rgb(182, 97, 42)" );
							$('.total1').html('Total  : Rs ' + total);
							ordereditems[3][3] = true;
							$('.option4a').addClass('opt1');
						}
					}
				}


			});
			$('.option5a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option5a').hasClass('opt1')){

					$('.warning').html("");
					ordereditems[4][3] = false;
					$('.option5a').css("background-color", "rgb(255,154,89)" );
					$('.option5a').removeClass('opt1');

					total = total - 30;
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total+30==201 || total+30>201)
					{
						$('.warning').html("You don't have enough money to order Yogurt.").removeClass('blurin').addClass('blurin');
					}
					else{
						if(total+30<201)
						{
							total = total + 30;
							$('.warning').html("");
							$('.option5a').css("background-color", "rgb(182, 97, 42)" );
							ordereditems[4][3] = true;
							$('.option5a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}

				}

			});
			$('.option6a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option6a').hasClass('opt1')){

					$('.warning').html("");
					ordereditems[5][3] = false;
					$('.option6a').css("background-color", "rgb(255,154,89)" );
					$('.option6a').removeClass('opt1');

					total = total - 30;
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total+30==201 || total+30>201)
					{
						$('.warning').html("You don't have enough money to order Tea.").removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total+30<201)
						{
							total = total + 30;
							$('.option6a').css("background-color", "rgb(182, 97, 42)" );
							$('.warning').html("");
							ordereditems[5][3] = true;
							$('.option6a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}
				}

			});
			$('.option7a').click(function(){
				clickdone();
				var tx = $(this).html();
				if($('.option7a').hasClass('opt1')){

					$('.warning').html("");
					ordereditems[6][3] = false;
					$('.option7a').css("background-color", "rgb(255,154,89)" );
					$('.option7a').removeClass('opt1');

					total = total - 20;
					$('.total1').html('Total  : Rs ' + total);
				}
				else{
					if(total+20==201 || total+20>201)
					{
						$('.warning').html("You don't have enough money to order fruit juice.").removeClass('blurin').addClass('blurin');
					}
					else
					{
						if(total+20<201)
						{
							total = total + 20;
							$('.warning').html("");
							$('.option7a').css("background-color", "rgb(182, 97, 42)" );
							ordereditems[6][3] = true;
							$('.option7a').addClass('opt1');
							$('.total1').html('Total  : Rs ' + total);
						}
					}
				}

			});
			break;

			case 6:

			if(ordereditems[0][3] == true){
				$('.paratha').addClass('fadein1');
			}
			if(ordereditems[1][3] ==true){
				$('.samosa').addClass('fadein1');
			}
			if(ordereditems[2][3] == true){
				$('.daalbhaat').addClass('fadein1');
			}
			if(ordereditems[3][3] == true){
				$('.rotitarkari').addClass('fadein1');
			}
			if(ordereditems[4][3] == true){
				$('.yogurt').addClass('fadein1');
			}
			if(ordereditems[5][3] == true){
				$('.tea').addClass('fadein1');
			}
			if(ordereditems[6][3] == true){
				$('.juice').addClass('fadein1');
			}
			$nextBtn.show(0);
			$prevBtn.show(0);
			break;
			case 7:
			soundplayer(sound_dg5,0);
			break;
			case 8:
			soundplayer(sound_dg6,1);

			var total = 0 ;
			var countval = 0 ;
			$(".listdiv").find("span").hide();
			$(".listdiv div").eq(0).find(".quantity").html("");
			if(ordereditems[0][3] == true){
				countval++;
				console.log("quantity 1"+countval);
				$(".listdiv div").eq(0).addClass("listdiv"+countval).find("span").show();
				$('.i1').append('Paratha Set');
				$('.m1').append('Rs 120');
				$(".listdiv div").eq(0).find(".quantity").html(countval);
				total = total + 120;
				$(".totalrs").find("p").html('Rs ' +total);
			}
			if(ordereditems[1][3] ==true){
				countval++;
				console.log("quantity2"+countval);
				$(".listdiv div").eq(1).find(".quantity").html(countval);
				$(".listdiv div").eq(1).addClass("listdiv"+countval).find("span").show();
				$('.i2').append('Samosa');
				total = total + 25;
				$('.m2').append('Rs 25');
				$(".totalrs").find("p").html('Rs ' +total);

			}
			if(ordereditems[2][3] == true){
				countval++;
				console.log("quantity3"+countval);
				$(".listdiv div").eq(2).find(".quantity").html(countval);
				$(".listdiv div").eq(2).addClass("listdiv"+countval).find("span").show();
				$('.i3').append('Dal Bhat Tarkari');
				total = total + 180;
				$('.m3').append('Rs 180');
				$(".totalrs").find("p").html('Rs ' +total);

			}
			if(ordereditems[3][3] == true){
				countval++;
				console.log("quantity4"+countval);
				$(".listdiv div").eq(3).find(".quantity").html(countval);
				$(".listdiv div").eq(3).addClass("listdiv"+countval).find("span").show();
				$('.i4').append('Roti Tarkari');
				total = total + 120;
				$('.m4').append('Rs 120');
				$(".totalrs").find("p").html('Rs ' +total);

			}
			if(ordereditems[4][3] == true){
				countval++;
				console.log("quantity5"+countval);
				$(".listdiv div").eq(4).find(".quantity").html(countval);
				$(".listdiv div").eq(4).addClass("listdiv"+countval).find("span").show();
				total = total + 30;
				$('.i5').append('Yogurt');
				$('.m5').append('  Rs 30');
				$(".totalrs").find("p").html('Rs ' +total);

			}
			if(ordereditems[5][3] == true){
				countval++;
				console.log("quantity6"+countval);
				$(".listdiv div").eq(5).find(".quantity").html(countval);
				$(".listdiv div").eq(5).addClass("listdiv"+countval).find("span").show();
				$('.i6').append('Tea');
				total = total + 30;
				$('.m6').append('Rs 30');
				$(".totalrs").find("p").html('Rs ' +total);

			}
			if(ordereditems[6][3] == true){
				countval++;
				console.log("quantity7"+countval);
				$(".listdiv div").eq(6).find(".quantity").html(countval);
				$(".listdiv div").eq(6).addClass("listdiv"+countval).find("span").show();
				$('.i7').append('Fruit Juice');
				total = total + 20;
				$('.m7').append('Rs 20');
				$(".totalrs").find("p").html('Rs ' +total);

			}

			$('.total2').append('Rs ' + total);
			break;

			case 9:
			soundplayer(sound_dg7,0);
			setTimeout(function(){ole.footerNotificationHandler.pageEndSetNotification()},1000);
			break;
		}
	}

	function soundplayer(i,next){
		buzz.all().stop();
		i.play().bind("ended",function(){
			if(next==1) $nextBtn.show();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		navigationcontroller();

		generalTemplate();
		/*
		for (var i = 0; i < content.length; i++) {
		slides(i);
		$($('.totalsequence')[i]).html(i);
		$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
	}
	function slides(i){
	$($('.totalsequence')[i]).click(function(){
	countNext = i;
	templateCaller();
});
}
*/

}

$nextBtn.on("click", function(){
	countNext++;
	templateCaller();
});

$refreshBtn.click(function(){
	templateCaller();
});
var clickflag=0;
$prevBtn.on("click", function(){
	countNext--;
	templateCaller();
	/* if footerNotificationHandler pageEndSetNotification was called then on click of
	previous slide button hide the footernotification */
	countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
});
// document.addEventListener("contentloaded", function(){
total_page = content.length;
templateCaller();
// });
function clickdone(){
	clickflag=1;
	$('.button').removeClass("avoid-clicks");
	console.log(clickflag);

	$('.button').click(function(){
        $('.button,.food').addClass("avoid-clicks");
        if($(".opt1").length>-1)
		  $nextBtn.show(0);
		else
			alert("Select food item");
	});
}
function clickundone(){
	clickflag=0;
	console.log(clickflag);
	if(clickflag==0){
	$('.button').addClass("avoid-clicks");
}
}

});

/*===============================================
=            data highlight function            =
===============================================*/
function texthighlight($highlightinside){
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ?
	alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
	null ;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag   = "</span>";


	if($alltextpara.length > 0){
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			(stylerulename = $(this).attr("data-highlightcustomclass")) :
			(stylerulename = "parsedstring") ;

			texthighlightstarttag = "<span class='"+stylerulename+"'>";
			replaceinstring       = $(this).html();
			replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
			replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

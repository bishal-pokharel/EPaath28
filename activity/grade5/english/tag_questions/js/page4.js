var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var content=[
 {
  	//page 0
  	contentnocenteradjust: true,
  	greenboardnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p6_s1
  	}]
  },{
  	//page 1
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable correct",
  		textdata: data.string.p6_s3
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s4
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s5
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s6
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s7
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s8
  	}]
  },{
  	//page 2
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s9
  	},{
  		textclass: "title_option2 fade_in2 draggable correct",
  		textdata: data.string.p6_s10
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s11
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s12
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s7
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s13
  	}]
  },{
  	//page 3
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s14
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s15
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s16
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s17
  	},{
  		textclass: "title_option5 fade_in5 draggable correct",
  		textdata: data.string.p6_s18
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s19
  	}]
  },{
  	//page 4
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable correct",
  		textdata: data.string.p6_s20
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s21
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s22
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s23
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s24
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s25
  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s26
  	},{
  		textclass: "title_option2 fade_in2 draggable correct",
  		textdata: data.string.p6_s27
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s28
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s29
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s30
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s31
  	}]
  },{
  	//page 6
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s32
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s33
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s34
  	},{
  		textclass: "title_option4 fade_in4 draggable correct",
  		textdata: data.string.p6_s35
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s36
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s37
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s38
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s39
  	},{
  		textclass: "title_option3 fade_in3 draggable correct",
  		textdata: data.string.p6_s40
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s41
  	},{
  		textclass: "title_option5 fade_in5 draggable",
  		textdata: data.string.p6_s42
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s43
  	}]
  },{
  	//page 8
  	contentnocenteradjust: true,
  	contentblockadditionalclass : "additional_content_block1",
  	greenboard: true,
  	greenboardblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p6_s2
  	},{
  		textclass: "title_option1 fade_in1 draggable",
  		textdata: data.string.p6_s44
  	},{
  		textclass: "title_option2 fade_in2 draggable",
  		textdata: data.string.p6_s45
  	},{
  		textclass: "title_option3 fade_in3 draggable",
  		textdata: data.string.p6_s46
  	},{
  		textclass: "title_option4 fade_in4 draggable",
  		textdata: data.string.p6_s47
  	},{
  		textclass: "title_option5 fade_in5 draggable correct",
  		textdata: data.string.p6_s48
  	},{
  		textclass: "title_bottom fade_in6 draggable",
  		textdata: data.string.p6_s49
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			   //images

    			// sounds
    			{id: "sound_0", src: soundAsset+"s4_p1.ogg"},
    			{id: "sound_1", src: soundAsset+"s4_p2.ogg"},


    		];
    		preload = new createjs.LoadQueue(false);
    		preload.installPlugin(createjs.Sound);//for registering sounds
    		preload.on("progress", handleProgress);
    		preload.on("complete", handleComplete);
    		preload.on("fileload", handleFileLoad);
    		preload.loadManifest(manifest, true);
    	}
    	function handleFileLoad(event) {
    		// console.log(event.item);
    	}
    	function handleProgress(event) {
    		$('#loading-text').html(parseInt(event.loaded*100)+'%');
    	}
    	function handleComplete(event) {
    		$('#loading-wrapper').hide(0);
    		//initialize varibales
    		// call main function
    		templateCaller();
    	}
    	//initialize
    	init();


/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	switch(countNext){
    case 0:
      sound_nav("sound_0");
    break;
    case 1:
    if(countNext==1){
    sound_nav("sound_1");
    }
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
			$(".draggable").draggable({
					containment : "body",
					cursor : "grab",
					revert : "invalid",
					appendTo : "body",
					zindex : 1000,
				});
				$('.dropinto').droppable({
					accept : ".correct",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						handleCardDrop(event, ui);
					}
				});

				function handleCardDrop(event, ui) {
					ui.draggable.draggable('disable');
					var dropped = ui.draggable;
					var droppedOn = $('.dropinto');
					var count = 0;
					var top = 0;
          	play_correct_incorrect_sound(1);
						$(dropped).detach().css({
							"left" : "10%",
							"top" : "0%",
							"width": "auto",
							"height": "auto",
							"opacity": "1",
							"transform": "unset"
						}).appendTo(droppedOn);

					if(countNext == $total_page-1){
            $prevBtn.show(0);
						ole.footerNotificationHandler.pageEndSetNotification();
					}else{
	        nav_button_controls(0);
					}
				}
			break;
		default:
			break;
	}
  }

  function nav_button_controls(delay_ms){
    timeoutvar = setTimeout(function(){
      if(countNext==0){
        $nextBtn.show(0);
      } else if( countNext>0 && countNext == $total_page-1){
        $prevBtn.show(0);
        ole.footerNotificationHandler.pageEndSetNotification();
      } else{
        $prevBtn.show(0);
        $nextBtn.show(0);
      }
    },delay_ms);
  }
  function sound_player(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }
  function sound_nav(sound_id){
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on("complete", function(){
      if(countNext==1){
        $prevBtn.hide(0);
        $nextBtn.hide(0);
      }else{
        nav_button_controls(0);
      }
    });
  }

  /*=====
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
    createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

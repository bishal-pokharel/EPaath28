var imgpath = $ref+"/images/exerciseImage/";
var soundAsset = $ref+"/sounds/";

// Array.prototype.shufflearray = function() {
//     var i = this.length,
//         j, temp;
//     while (--i > 0) {
//         j = Math.floor(Math.random() * (i + 1));
//         temp = this[j];
//         this[j] = this[i];
//         this[i] = temp;
//     }
//     return this;
// };

var content=[
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques1,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques1_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques1_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques1_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques1_opt4
			}
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques2,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques2_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques2_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques2_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques2_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques3,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques3_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques3_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques3_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques3_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques4,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques4_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques4_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques4_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques4_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques5,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques5_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques5_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques5_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques5_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques6,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques6_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques6_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques6_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques6_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques7,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques7_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques7_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques7_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques7_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques8,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques8_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques8_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques8_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques8_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques9,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques9_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques9_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques9_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques9_opt4
			},
			]
		}
		]
	},
	{
		//slide 0
		uppertextblock:[{
			textclass: "instruction",
			textdata: data.string.ques_instruction
		}],
		exerciseblock: [
		{
			textdata: data.string.ques10,
			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				textdata: data.string.ques10_opt1
			},
			{
				forshuffle: "options_sign incorrectOne",
				textdata: data.string.ques10_opt2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques10_opt3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				textdata: data.string.ques10_opt4
			},
			]
		}
		]
	},
];

/*remove this for non random questions*/
// content.shufflearray();

$(function (){
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	  loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			   //images

    			// sounds
    			{id: "sound_0", src: soundAsset+"ex_instr.ogg"},



    		];
    		preload = new createjs.LoadQueue(false);
    		preload.installPlugin(createjs.Sound);//for registering sounds
    		preload.on("progress", handleProgress);
    		preload.on("complete", handleComplete);
    		preload.on("fileload", handleFileLoad);
    		preload.loadManifest(manifest, true);
    	}
    	function handleFileLoad(event) {
    		// console.log(event.item);
    	}
    	function handleProgress(event) {
    		$('#loading-text').html(parseInt(event.loaded*100)+'%');
    	}
    	function handleComplete(event) {
    		$('#loading-wrapper').hide(0);
    		//initialize varibales
    		// call main function
    		templateCaller();
    	}
    	//initialize
    	init();


	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new EggTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(10);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	/*random scoreboard eggs*/
	 	//var i = Math.floor(Math.random() * imageArray.length);
	 	//var randImg = imageArray[i];
	 	var ansClicked = false;
	 	var wrngClicked = false;
		if(countNext==0){
			sound_nav("sound_0");
		}
	 	$(".correctOne").click(function(){
			correct_btn(this);
	 		testin.update(true);
			play_correct_incorrect_sound(1);
	 		$nextBtn.show(0);
	 	});

	 	$(".incorrectOne").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	$(".incorrectTwo").click(function(){
			incorrect_btn(this);
	 		testin.update(false);
			play_correct_incorrect_sound(0);
	 	});

	 	function correct_btn(current_btn){
	 		$('.options_sign').addClass('disabled');
	 		$('.hidden_sign').html($(current_btn).html());
	 		$('.hidden_sign').addClass('fade_in');
	 		$('.optionscontainer').removeClass('forHover');
	 		$(current_btn).addClass('option_true');
	 	}

	 	function incorrect_btn(current_btn){
	 		$(current_btn).addClass('disabled');
	 		$(current_btn).addClass('option_false');
	 	}

	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	   function nav_button_controls(delay_ms){
	     timeoutvar = setTimeout(function(){
	       if(countNext==0){
	         $nextBtn.show(0);
	       } else if( countNext>0 && countNext == $total_page-1){
	         $prevBtn.show(0);
	         ole.footerNotificationHandler.pageEndSetNotification();
	       } else{
	         $prevBtn.show(0);
	         $nextBtn.show(0);
	       }
	     },delay_ms);
	   }
	   function sound_player(sound_id){
	     createjs.Sound.stop();
	     current_sound = createjs.Sound.play(sound_id);
	     current_sound.play();
	   }
	   function sound_nav(sound_id){
	     createjs.Sound.stop();
	     current_sound = createjs.Sound.play(sound_id);
	     current_sound.play();
	     current_sound.on("complete", function(){
	       if(countNext==0){
	         $prevBtn.hide(0);
	         $nextBtn.hide(0);
	       }else{
	         nav_button_controls(0);
	       }
	     });
	   }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/
		loadTimelineProgress($total_page, countNext + 1);

		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
		countNext++;
		templateCaller();
		testin.gotoNext();
}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

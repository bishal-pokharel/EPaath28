var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
			]
		}],
		poemimage:[{
			poemimgblockadditionalclass: 'init-1',
			imagestoshow : [
				{
					imgclass : "imggr-1 poem-img img-1",
					imgsrc : '',
					imgid : 'img-1'
				},
				{
					imgclass : "imggr-1 poem-img img-1a",
					imgsrc : '',
					imgid : 'img-1a'
				},
				{
					imgclass : "imggr-1 poem-img img-1b",
					imgsrc : '',
					imgid : 'img-1b'
				},


				{
					imgclass : "imggr-2 poem-img img-2a",
					imgsrc : '',
					imgid : 'img-2a'
				},
				{
					imgclass : "imggr-2 poem-img img-2",
					imgsrc : '',
					imgid : 'img-2'
				},
				{
					imgclass : "imggr-2 poem-img img-2b",
					imgsrc : '',
					imgid : 'img-2b'
				},
				{
					imgclass : "imggr-2 poem-img img-2c",
					imgsrc : '',
					imgid : 'img-2c'
				},


				{
					imgclass : "imggr-3 poem-img img-3b",
					imgsrc : '',
					imgid : 'img-3b'
				},{
					imgclass : "imggr-3 poem-img img-3a",
					imgsrc : '',
					imgid : 'img-3a'
				},
				{
					imgclass : "imggr-3 poem-img img-3c",
					imgsrc : '',
					imgid : 'img-3c'
				},
				{
					imgclass : "imggr-3 poem-img img-3",
					imgsrc : '',
					imgid : 'img-3'
				},
				{
					imgclass : "imggr-3 poem-img hand-d img-3d",
					imgsrc : '',
					imgid : 'img-3d'
				},
				{
					imgclass : "imggr-3 poem-img hand-e img-3e",
					imgsrc : '',
					imgid : 'img-3e'
				},


				{
					imgclass : "imggr-4 poem-img img-4",
					imgsrc : '',
					imgid : 'img-4'
				},
				{
					imgclass : "imggr-4 poem-img img-4a",
					imgsrc : '',
					imgid : 'img-4a'
				},
				{
					imgclass : "imggr-4 morning-ray",
					imgsrc : '',
					imgid : 'img-4d'
				},

			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},


		{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "poem-bg", src: imgpath+"inkpot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-1", src: imgpath+"poem/bedroom01.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "img-1a", src: imgpath+"poem/shadow.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "img-1b", src: imgpath+"poem/girl.svg", type: createjs.AbstractLoader.IMAGE},

			{id: "img-2", src: imgpath+"poem/eye.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2a", src: imgpath+"poem/head.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2b", src: imgpath+"poem/teeth.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-2c", src: imgpath+"poem/horn.png", type: createjs.AbstractLoader.IMAGE},

			{id: "img-3", src: imgpath+"poem/chair.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3a", src: imgpath+"poem/head_2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3b", src: imgpath+"poem/body.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3c", src: imgpath+"poem/spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3d", src: imgpath+"poem/lefthand_small.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-3e", src: imgpath+"poem/righthand_small.png", type: createjs.AbstractLoader.IMAGE},

			{id: "img-4", src: imgpath+"poem/eating.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4a", src: imgpath+"poem/girl01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4b", src: imgpath+"poem/girl02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4c", src: imgpath+"poem/girl03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img-4d", src: imgpath+"poem/blank.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "para-1", src: soundAsset+"poem/firstpara.ogg"},
			{id: "para-2", src: soundAsset+"poem/secondpara.ogg"},
			{id: "para-3", src: soundAsset+"poem/third.ogg"},
			{id: "thunder", src: soundAsset+"thunder.ogg"},
			{id: "sound_0", src: soundAsset+"s2_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		switch(countNext) {
			case 0:
				// replace this by audio bind
				// setTimeout(function(){
					// $('.imggr-1, .imggr-2').hide(0);
					// $('.imggr-3').hide(0);
				// }, 0);
				current_sound = createjs.Sound.play('sound_0');
					current_sound.on("complete", function(){
				$('.para-1').addClass('active-para');
						current_sound = createjs.Sound.play('para-1');
				current_sound.on("complete", function(){
					$('.imggr-2').removeClass('img-2, img-2a, img-2b, img-2c');
					$('.imggr-2').addClass('transition-1');
					$('.para-1').removeClass('active-para');
					setTimeout(function(){
						$('.poemimgblock').removeClass('init-1');
						$('.poemimgblock').addClass('init-2');
						$('.imggr-3').show(0);
						$('.para-2').addClass('active-para');
						current_sound = createjs.Sound.play('para-2');
						current_sound.on("complete", function(){
							$('.para-2').removeClass('active-para');
							$('.imggr-3').removeClass('img-3');
							$('.imggr-3').removeClass('img-3a');
							$('.imggr-3').removeClass('img-3b');
							$('.imggr-3').removeClass('img-3c');
							$('.imggr-3').removeClass('img-3d');
							$('.imggr-3').removeClass('img-3e');
							$('.imggr-3').addClass('transition-1');
							$('.poemimgblock').removeClass('init-2');
							$('.poemimgblock').addClass('init-3');
							$('.imggr-4').show(0);
							setTimeout(function(){
								createjs.Sound.play('thunder');
								createjs.Sound.play('para-3');
								$('.para-3').addClass('active-para');
								$('.poemimgblock').removeClass('init-3').addClass('flashit');
							}, 2000);
							setTimeout(function(){
								$('.img-4').removeClass('img-4').addClass('fadeout-1');
							}, 5000);
							setTimeout(function(){
								$('.img-4a').attr('src', preload.getResult('img-4b').src);
							}, 14000);
							setTimeout(function(){
								$('.img-4a').attr('src', preload.getResult('img-4c').src);
							}, 18000);
							setTimeout(function(){
								$('.para-3').removeClass('active-para');
								ole.footerNotificationHandler.pageEndSetNotification();
							}, 20000);
						});
					}, 2000);
				});
			});
				break;
			default:
				$prevBtn.show(0);
				// createjs.Sound.play('sound_3');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";
var ans_vals = [
					[data.string.p5text7a, data.string.p5text7b, data.string.p5text7c, data.string.p5text7d, data.string.p5text7e, data.string.p5text7f, 'ex_img-1', 4],
					[data.string.p5text8a, data.string.p5text8b, data.string.p5text8c, data.string.p5text8d, data.string.p5text8e, data.string.p5text8f, 'ex_img-2', 4],
					[data.string.p5text9a, data.string.p5text9b, data.string.p5text9c, data.string.p5text9d, data.string.p5text9e, data.string.p5text9f, 'ex_img-3', 3],
					[data.string.p5text10a, data.string.p5text10b, data.string.p5text10c, data.string.p5text10d, data.string.p5text10e, data.string.p5text10f, 'ex_img-4', 4],
					[data.string.p5text11a, data.string.p5text11b, data.string.p5text11c, data.string.p5text11d, data.string.p5text11e, data.string.p5text11f, 'ex_img-5', 3],
					[data.string.p5text12a, data.string.p5text12b, data.string.p5text12c, data.string.p5text12d, data.string.p5text12e, data.string.p5text12f, 'ex_img-6', 3]
			];

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',

		uppertextblockadditionalclass:'lesson-title shortstack',
		uppertextblock:[{
			textdata: data.string.diytext,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "diy-cover",
					imgsrc : '',
					imgid : 'diy-bg'
				}]
			}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'teller my_font_medium shortstack',
		uppertextblock:[{
			textdata:  data.string.p5text2,
			textclass: "",
		}],
		boatblock: true,
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				}
			]
		}],
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'teller my_font_medium shortstack',
		uppertextblock:[{
			textdata:  data.string.p5text3,
			textclass: "",
		}],
		boatblock: true,
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "shark-entry shark",
					imgsrc : '',
					imgid : 'shark-2'
				},
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				}
			]
		}],
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'teller my_font_medium shortstack teller-2 aligned-left',
		uppertextblock:[{
			textdata:  data.string.p5text4,
			textclass: "",
		}],

		boatblock: true,
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "shark-anim shark",
					imgsrc : '',
					imgid : 'shark-1'
				},
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				}
			]
		}],
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		uppertextblockadditionalclass: 'teller my_font_medium shortstack',
		uppertextblock:[{
			textdata:  data.string.p5text5,
			textclass: "",
		}],

		boatblock: true,
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "shark-anim shark",
					imgsrc : '',
					imgid : 'shark-1'
				},
				{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				}
			]
		}],
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-sky',

		boatblock: true,

		uppertextblockadditionalclass: 'teller my_font_medium shortstack',
		uppertextblock:[{
			textdata:  data.string.p5text0,
			textclass: "aa",
		},{
			textdata:  data.string.died,
			textclass: "you-died",
		}],

		lowertextblockadditionalclass: 'go-text',
		lowertextblock:[{
			textdata:  data.string.go,
			textclass: "go-btn",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "shark-anim shark",
					imgsrc : '',
					imgid : 'shark-1'
				},{
					imgclass : "wave-1-anim wave-1",
					imgsrc : '',
					imgid : 'wave-1'
				},{
					imgclass : "wave-2-anim wave-2",
					imgsrc : '',
					imgid : 'wave-2'
				},{
					imgclass : "wave-3-anim wave-3",
					imgsrc : '',
					imgid : 'wave-3'
				},{
					imgclass : "cloud-anim-1 cloud-1",
					imgsrc : '',
					imgid : 'cloud-1'
				},{
					imgclass : "cloud-anim-2 cloud-2",
					imgsrc : '',
					imgid : 'cloud-2'
				}
			]
		}],
		// lowertext2block:[{
		// 	textdata:  data.string.died,
		// 	textclass: "you-died",
		// }],
		extratextblock:[{
			textdata:  data.string.escape,
			textclass: "you-escaped",
		}
],

		qnablockclass:'',
		instructionclass: '',
		imgsrc: imgpath+"diy/1.png",
		instructiondata: data.string.p5text6,
		submitclass: '',
		submitdata: data.string.submit,
		option: [{
			textdata:  data.string.p5text7a,
			textclass: "a-1 correct",
		},{
			textdata:  data.string.p5text7b,
			textclass: "a-2 correct",
		},{
			textdata:  data.string.p5text7c,
			textclass: "a-3 correct",
		},{
			textdata:  data.string.p5text7d,
			textclass: "a-4 ",
		},{
			textdata:  data.string.p5text7e,
			textclass: "a-5",
		},{
			textdata:  data.string.p5text7f,
			textclass: "a-6",
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;
	var bg_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "diy-bg", src: imgpath+"cover_page_diy.png", type: createjs.AbstractLoader.IMAGE},

			{id: "cloud-1", src: imgpath+"diy/cloudhalf.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud-2", src: imgpath+"diy/cloudfull.png", type: createjs.AbstractLoader.IMAGE},

			{id: "wave-1", src: imgpath+"diy/wave01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-2", src: imgpath+"diy/wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-3", src: imgpath+"diy/wave03.png", type: createjs.AbstractLoader.IMAGE},

			{id: "shark-1", src: imgpath+"diy/shark01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "shark-2", src: imgpath+"diy/shark02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boat", src: imgpath+"diy/boat.png", type: createjs.AbstractLoader.IMAGE},

			{id: "man-1", src: imgpath+"diy/man01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man-2", src: imgpath+"diy/man02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "man-3", src: imgpath+"diy/man03.png", type: createjs.AbstractLoader.IMAGE},


			{id: "ex_img-1", src: imgpath+"diy/1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_img-2", src: imgpath+"diy/2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_img-3", src: imgpath+"diy/3.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_img-4", src: imgpath+"diy/4.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_img-5", src: imgpath+"diy/5.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ex_img-6", src: imgpath+"diy/6.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound-bg1", src: soundAsset+"game/battle.ogg"},
			{id: "sound-bg", src: soundAsset+"game/bg.ogg"},
			{id: "sound-lose", src: soundAsset+"game/lose2.ogg"},
			{id: "sound-win", src: soundAsset+"game/win.ogg"},
			{id: "sound-oh", src: soundAsset+"game/oh-oh.ogg"},
			{id: "sound-close", src: soundAsset+"game/shark_close.ogg"},
			{id: "sound-sigh", src: soundAsset+"game/sigh.ogg"},
			{id: "sound-yay", src: soundAsset+"game/yay.ogg"},

			{id: "sound_1", src: soundAsset+"s6_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s6_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s6_p4.ogg"},
			{id: "sound_4", src: soundAsset+"s6_p5.ogg"},
			{id: "sound_5", src: soundAsset+"s6_p6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		switch(countNext) {
			case 0:
			play_diy_audio();
			nav_button_controls(2000);
				break;
			case 1:
				sound_nav("sound_1");
				$('.boatbody').attr('src', preload.getResult('boat').src);
				$('.boatman').attr('src', preload.getResult('man-2').src);
				break;
			case 2:
				$('.boatbody').attr('src', preload.getResult('boat').src);
				$('.boatman').attr('src', preload.getResult('man-2').src);
				setTimeout(function(){
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound-close");
					current_sound.play();
					current_sound.on("complete", function(){
						createjs.Sound.stop();
						current_sound = createjs.Sound.play("sound_2");
						current_sound.play();
						current_sound.on("complete", function(){
							nav_button_controls(0);
						});
					});
				}, 1000);
				timeoutvar = setTimeout(function(){
					$('.shark').attr('src', preload.getResult('shark-1').src);
					$('.boatman').attr('src', preload.getResult('man-1').src);

				}, 4200);
				break;
			case 3:
						sound_nav("sound_3");
						$('.boatman').attr('src', preload.getResult('man-1').src);
						$('.boatbody').attr('src', preload.getResult('boat').src);
						break;
			case 4:
			sound_nav("sound_4");
				$('.boatman').attr('src', preload.getResult('man-1').src);
				$('.boatbody').attr('src', preload.getResult('boat').src);
				break;
			case 5:
			sound_nav("sound_5");

				var answer = true;

				var distance = 3;
				var boat_pos = 56;
				var shark_pos = 65;
				var qcount = 0;
				var questions = [0,1,2,3,4,5];
				questions.shufflearray();
				$('.boatman').attr('src', preload.getResult('man-1').src);
				$('.boatbody').attr('src', preload.getResult('boat').src);
				// bg_sound.volume = 0.5;
				// bg_sound.loop= -1;
				setTimeout(function(){
					$('.go-btn').click(function(){
						createjs.Sound.stop();
						$(this).css('pointer-events', 'none');
						qna_creater(questions[qcount]);
						$('.qnablock').fadeIn(1000);
						$('.qnablock>*').hide(0);
						$('.option').css('pointer-events', 'all');
						$('.submit').css('pointer-events', 'all');
						$('.qnablock').animate({
							'width': '100.5%',
							'height': '100.5%'
						}, 1000, function(){
							// bg_sound.volume = 0;
							$('.qnablock>*:not(#wrong-icon)').fadeIn(500);
						});
					});
				},8000);

				function reset_qna(){
					$('.option').removeClass('correct');
					$('.option').removeClass('selected-option');
					$('.option').removeClass('right-select');
					$('.option').removeClass('wrong-select');
					$('.option').removeClass('right-not-select');
					for(var op=1; op<7; op++){
						$('.option').removeClass('option-'+op);
					}
				}
				$('.option').click(function(){
					if(!$(this).hasClass('selected-option')){
						$(this).addClass('selected-option');
					} else{
						$(this).removeClass('selected-option');
					}
				});
				$('.submit').click(function(){
					answer = true;
					for(var i=0; i<6; i++){
						var $currentclass = $('.option').eq(i);
						if($currentclass.hasClass('correct')){
							if(!$currentclass.hasClass('selected-option')){
								answer = false;
							} else{
								play_correct_incorrect_sound(1);
								$currentclass.css('pointer-events', 'none');
								$currentclass.addClass('right-select');
								$currentclass.append(' &#x2714;');
							}
						} else{
							if($currentclass.hasClass('selected-option')){
								play_correct_incorrect_sound(0);
								$currentclass.addClass('wrong-select');
								answer = false;
								$currentclass.append(' &#x2718;');
							}
						}
					}
					if(answer==true){
						play_correct_incorrect_sound(1);
						distance++;
					} else{
						play_correct_incorrect_sound(0);
						distance--;
					}
					$('.correct:not(.right-select)').addClass('right-not-select');
					$('.option').css('pointer-events', 'none');
					$('.submit').css('pointer-events', 'none');
					$('#wrong-icon').show(0);
					qcount++;
				});
						sound_nav("sound_5");
				$('#wrong-icon').click(function(){
					$(".teller").hide(0);
					$('.qnablock').fadeOut(1000, function(){
						$('.qnablock').css({
							'width': '0%',
							'height': '0%'
						});
						if(answer==true){
							createjs.Sound.play('sound-yay');
							setTimeout(function(){
								$(".go-text").addClass("blinked");
							},2000);
							boat_pos += 1.5;
							shark_pos += 6.5;
						}
							else{
							createjs.Sound.play('sound-close');
							boat_pos -= 1.5;
							shark_pos -= 6.5;
						}
						$('.boatblock').animate({
							'left': boat_pos+'%'
						}, 1000);
						$('.shark').animate({
							'right': shark_pos+'%'
						}, 1000, function(){
							// bg_sound.volume = 0.5;
							if(distance<3){
								$('.shark').attr('src', preload.getResult('shark-1').src);
								$('.boatman').attr('src', preload.getResult('man-1').src);
							} else{
								$('.shark').attr('src', preload.getResult('shark-2').src);
								$('.boatman').attr('src', preload.getResult('man-2').src);
							}

							if(distance==0){
								createjs.Sound.stop();
								createjs.Sound.play('sound-lose');
								$('.shark').animate({
									'right': '40%'
								}, 1000);
								$('.you-died').fadeIn(1000);
								$('.teller').fadeIn(1000);
								$(".bg-sky").css("filter","grayscale(100%)");
								$(".aa").hide(0);
								$(".teller").addClass("tlt1");
							} else if(distance>5||qcount>5){
								sound_player('sound-sigh');
								$('.boatman').attr('src', preload.getResult('man-3').src);
								$('.go-text').fadeOut(1000);
								$('.boatblock').animate({
									'left': '40%'
								}, 1000);
								$('.shark').animate({
									'right': '120%'

								}, 1000, function(){
										sound_player('sound-win');
									$('.teller').hide(0);
									$('.you-escaped').fadeIn(1000);
									nav_button_controls(2000);
								});
							//escaped
							} else {
								$('.go-btn').css('pointer-events', 'all');
							}
						});
					});
				});
				function qna_creater(index){
					reset_qna();
					if(index==0 || index==1 || index==3){
						$('.instruction').html(data.string.p5text6);
					} else{
						$('.instruction').html(data.string.p5text6a);
					}
					$('.eximg').attr('src', preload.getResult(ans_vals[index][6]).src);
					for(var op=0; op<6; op++){
						$('.option').eq(op).html(ans_vals[index][op]);
						if(op<ans_vals[index][7]){
							$('.option').eq(op).addClass('correct');
						}
					}
					var option_position = [1,2,3,4,5,6];
					option_position.shufflearray();
					for(var op=0; op<6; op++){
						$('.option').eq(op).addClass('option-'+option_position[op]);
					}
				}
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){

			if(countNext==5){
				$prevBtn.hide(0);
				$nextBtn.hide(0);
			}
			else{
				nav_button_controls(0);
			}
		});
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		if(countNext==1){
			createjs.Sound.stop();
		}
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

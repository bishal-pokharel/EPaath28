var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-cover',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'coverpage'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "start-text my_font_very_big happymonkey",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		extratextblock:[{
			textdata:  data.string.p1text2,
			textclass: "start-text my_font_very_big happymonkey",
		}],

		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'bg-1'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-inside',

		extratextblock:[{
			textdata:  data.string.p1text3,
			textclass: "top-text my_font_medium happymonkey",
		}],

		card:[{
			carddiv: 'card-1',
			fronttextdata:  data.string.p1text4,
			fronttextclass: "my_font_big happymonkey",
			backtextdata:  data.string.p1text5,
			backtextclass: "my_font_big happymonkey",
			imgclass : "card-img c-1",
			imgsrc : '',
			imgid : 'card-1'
		},{
			carddiv: 'card-2',
			fronttextdata:  data.string.p1text6,
			fronttextclass: "my_font_big happymonkey",
			backtextdata:  data.string.p1text7,
			backtextclass: "my_font_big happymonkey",
			imgclass : "card-img c-2",
			imgsrc : '',
			imgid : 'card-2'
		},{
			carddiv: 'card-3',
			fronttextdata:  data.string.p1text8,
			fronttextclass: "my_font_big happymonkey",
			backtextdata:  data.string.p1text9,
			backtextclass: "my_font_big happymonkey",
			imgclass : "card-img c-3",
			imgsrc : '',
			imgid : 'card-3'
		},{
			carddiv: 'card-4',
			fronttextdata:  data.string.p1text10,
			fronttextclass: "my_font_big happymonkey",
			backtextdata:  data.string.p1text11,
			backtextclass: "my_font_big happymonkey",
			imgclass : "card-img c-4",
			imgsrc : '',
			imgid : 'card-4'
		}]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "cloud", src: imgpath+"cloud.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coverpage", src: imgpath+"describing_people_coverpage.png", type: createjs.AbstractLoader.IMAGE},

			{id: "card-1", src: imgpath+"boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "card-2", src: imgpath+"baby.png", type: createjs.AbstractLoader.IMAGE},
			{id: "card-3", src: imgpath+"singer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "card-4", src: imgpath+"play-with-dog.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"s1_p1.ogg"},
			{id: "sound_1", src: soundAsset+"s1_p2.ogg"},
			{id: "sound_2", src: soundAsset+"s1_p3.ogg"},
			{id: "sound_3", src: soundAsset+"s1_p4.ogg"},
			{id: "sound_1_1", src: soundAsset+"s1_p4_appearance.ogg"},
			{id: "sound_2_1", src: soundAsset+"s1_p4_appearance_.ogg"},
			{id: "sound_1_2", src: soundAsset+"s1_p4_habits.ogg"},
			{id: "sound_2_2", src: soundAsset+"s1_p4_habits_.ogg"},
			{id: "sound_1_3", src: soundAsset+"s1_p4_likes_dislikes.ogg"},
			{id: "sound_2_3", src: soundAsset+"s1_p4_likes_dislikes_.ogg"},
			{id: "sound_1_4", src: soundAsset+"s1_p4_possessions.ogg"},
			{id: "sound_2_4", src: soundAsset+"s1_p4_possessions_.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_card_image(content, countNext);
		switch(countNext) {
			case 0:
			sound_nav("sound_0");
				break;
			case 1:
			sound_nav("sound_1");
			break;
			case 2:
			sound_nav("sound_2");
			break;
			case 3:
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_3");
				current_sound.play();
				current_sound.on('complete',function(){
                    $(".card-1 .front-card").addClass("blinkEffect");
					setTimeout(function(){
					current_sound = createjs.Sound.play("sound_4");
				},1000);


});
				var clicked = [0,0,0,0];
				var crdClk_1,crdClk_2,crdClk_3,crdClk_4 = false;
				$('.card').click(function(){
					$(this).find(".front-card").removeClass("blinkEffect");
          var idx = $(this).attr('class');
					idx = parseInt(idx.replace(/\D/g,''))-1;
					var idx1 = idx+2;
					var idx2 = idx1-1;
					var $this = $(this);
					idx==0?crdClk_1=true:idx==1?crdClk_2=true:idx==2?crdClk_3=true:idx==3?crdClk_4=true:"";
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_1_"+(idx+1));
					current_sound.play();
					$(this).not().hasClass("flipped")?$(this).toggleClass('flipped'):"";
					current_sound.on('complete',function(){
						$this.toggleClass('flipped');
						if(($this).hasClass('flipped')){
							for(var i=0; i<4; i++){
								if(clicked[i]==0){
									clicked[idx] = 1;
									sound_player("sound_2_"+idx2);
                                    current_sound.on('complete',function(){
                                        $(".card-"+idx1+" .front-card").addClass("blinkEffect");
                                    });
									if(crdClk_1==true&&crdClk_2==true&&crdClk_3==true&&crdClk_4==true){
										nav_button_controls(2000);
									}
									return true;
								}else{
									clicked = [0,0,0,0];
								}
							}
						}
					});
				});
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
				{
					imgclass : "blind",
					imgsrc : '',
					imgid : 'blind'
				}
			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},

	{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],


		containerclass: 'drop-down',
		extratextblock:[{
			textdata: data.string.p2text14,
			textclass: "instruction my_font_medium shortstack",
		}],

		listtextblockadditionalclass:'',
		questionclass: 'question my_font_medium shortstack',
		questiondata: data.string.p2text15,
		listtextblock:[
		{
			textdata: data.string.p2text15a,
			textclass: "",
		},{
			textdata: data.string.p2text15b,
			textclass: "correct",
		},{
			textdata: data.string.p2text15c,
			textclass: "",
		},{
			textdata: data.string.p2text15d,
			textclass: "",
		},
		]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
				{
					imgclass : "blind",
					imgsrc : '',
					imgid : 'blind'
				}
			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},

	{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],


		containerclass: 'drop-down',
		extratextblock:[{
			textdata: data.string.p2text14,
			textclass: "instruction my_font_medium shortstack",
		}],

		listtextblockadditionalclass:'',
		questionclass: 'question my_font_medium shortstack',
		questiondata: data.string.p2text16,
		listtextblock:[
		{
			textdata: data.string.p2text16a,
			textclass: "",
		},{
			textdata: data.string.p2text16b,
			textclass: "correct",
		},{
			textdata: data.string.p2text16c,
			textclass: "",
		},{
			textdata: data.string.p2text16d,
			textclass: "",
		},
		]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
				{
					imgclass : "blind",
					imgsrc : '',
					imgid : 'blind'
				}
			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},

	{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],


		containerclass: 'drop-down',
		extratextblock:[{
			textdata: data.string.p2text14,
			textclass: "instruction my_font_medium shortstack",
		}],

		listtextblockadditionalclass:'',
		questionclass: 'question my_font_medium shortstack',
		questiondata: data.string.p2text17,
		listtextblock:[
		{
			textdata: data.string.p2text17a,
			textclass: "",
		},{
			textdata: data.string.p2text17b,
			textclass: "",
		},{
			textdata: data.string.p2text17c,
			textclass: "",
		},{
			textdata: data.string.p2text17d,
			textclass: "correct",
		},
		]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
				{
					imgclass : "blind",
					imgsrc : '',
					imgid : 'blind'
				}
			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},

	{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],


		containerclass: 'drop-down',
		extratextblock:[{
			textdata: data.string.p2text14,
			textclass: "instruction my_font_medium shortstack",
		}],

		listtextblockadditionalclass:'',
		questionclass: 'question my_font_medium shortstack',
		questiondata: data.string.p2text18,
		listtextblock:[
		{
			textdata: data.string.p2text18a,
			textclass: "",
		},{
			textdata: data.string.p2text18b,
			textclass: "",
		},{
			textdata: data.string.p2text18c,
			textclass: "correct",
		},{
			textdata: data.string.p2text18d,
			textclass: "",
		},
		]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'bg-blue',
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "poem-bg",
					imgsrc : '',
					imgid : 'poem-bg'
				},
				{
					imgclass : "blind",
					imgsrc : '',
					imgid : 'blind'
				}
			]
		}],
		uppertextblockadditionalclass:'poemblock',
		uppertextblock:[{
			textdata: data.string.p2text1,
			textclass: "poem-title",
		},

		{
			textdata: data.string.p2text2,
			textclass: "para-1",
		},{
			textdata: data.string.p2text3,
			textclass: "para-1",
		},{
			textdata: data.string.p2text4,
			textclass: "para-1",
		},{
			textdata: data.string.p2text5,
			textclass: "para-1 margin-bt-5",
		},

		{
			textdata: data.string.p2text6,
			textclass: "para-2",
		},{
			textdata: data.string.p2text7,
			textclass: "para-2",
		},{
			textdata: data.string.p2text8,
			textclass: "para-2",
		},{
			textdata: data.string.p2text9,
			textclass: "para-2 margin-bt-5",
		},

	{
			textdata: data.string.p2text10,
			textclass: "para-3",
		},{
			textdata: data.string.p2text11,
			textclass: "para-3",
		},{
			textdata: data.string.p2text12,
			textclass: "para-3",
		},{
			textdata: data.string.p2text13,
			textclass: "para-3",
		}],


		containerclass: 'drop-down',
		extratextblock:[{
			textdata: data.string.p2text14,
			textclass: "instruction my_font_medium shortstack",
		}],

		listtextblockadditionalclass:'',
		questionclass: 'question my_font_medium shortstack',
		questiondata: data.string.p2text19,
		listtextblock:[
		{
			textdata: data.string.p2text19a,
			textclass: "correct",
		},{
			textdata: data.string.p2text19b,
			textclass: "",
		},{
			textdata: data.string.p2text19c,
			textclass: "",
		},{
			textdata: data.string.p2text19d,
			textclass: "",
		},
		]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "poem-bg", src: imgpath+"inkpot.png", type: createjs.AbstractLoader.IMAGE},
			{id: "blind", src: imgpath+"poem/blind.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_curtain", src: soundAsset+"curtain.ogg"},
			{id: "sound_curtain_2", src: soundAsset+"curtain-2.ogg"},
			{id: "sound_drop", src: soundAsset+"drop.ogg"},

			{id: "para-1", src: soundAsset+"poem/firstpara.ogg"},
			{id: "para-2", src: soundAsset+"poem/secondpara.ogg"},
			{id: "para-3", src: soundAsset+"poem/third.ogg"},

			{id: "sound_0", src: soundAsset+"s3_p1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
				vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		switch(countNext) {
			default:
				if(countNext==0) {
										$('.qna-answers').css("pointer-events", "none");
                    current_sound = createjs.Sound.play('sound_curtain');
                    current_sound.on("complete", function () {
                        current_sound = createjs.Sound.play('sound_0');
												current_sound.on('complete', function(){
													$('.qna-answers').css("pointer-events", "auto");													
												});
                    });
                }
				if(countNext>0){
					$prevBtn.show(0);
				}
				var parent = $(".listtextblock>ol");
				var divs = parent.children();
				while (divs.length) {
					parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
				}
				$('.qna-answers').click(function(){
					if($(this).children('span').hasClass('correct')){
						countNext!=4?$(".blind").addClass("scaleup"):"";
						$(this).css({'color': '#7DC24B'});
						$(this).children('span').append(' &#10004;');
						play_correct_incorrect_sound(1);
						timeoutvar = setTimeout(function(){
							if( countNext == $total_page-1){
								ole.footerNotificationHandler.pageEndSetNotification();
							}	else{
								current_sound = createjs.Sound.play('sound_drop');
								$('.blind').animate({
									'bottom': '60%'
								}, 200, function(){
									$('.blind').css('pointer-events', 'all');
								});
							}
						}, 500);
						$('.qna-answers').css('pointer-events', 'none');
					} else{
						play_correct_incorrect_sound(0);
						$(this).children('span').append(' &#x2717;');
						$(this).css({'pointer-events': 'none','color': '#C70102'});
					}
				});
				$('.para-1').click(function(){
					$('.para-1, .para-2, .para-3').removeClass('active-para');
					createjs.Sound.stop();
					$('.para-1').addClass('active-para');
					var current_sound = createjs.Sound.play('para-1');
					current_sound.on("complete", function(){
						$('.para-1, .para-2, .para-3').removeClass('active-para');
					});
				});
				$('.para-2').click(function(){
					$('.para-1, .para-2, .para-3').removeClass('active-para');
					createjs.Sound.stop();
					$('.para-2').addClass('active-para');
					var current_sound = createjs.Sound.play('para-2');
					current_sound.on("complete", function(){
						$('.para-1, .para-2, .para-3').removeClass('active-para');
					});
				});
				$('.para-3').click(function(){
					$('.para-1, .para-2, .para-3').removeClass('active-para');
					createjs.Sound.stop();
					$('.para-3').addClass('active-para');
					var current_sound = createjs.Sound.play('para-3');
					current_sound.on("complete", function(){
						$('.para-1, .para-2, .para-3').removeClass('active-para');
					});
				});
				break;
		}
		$('.blind').click(function(){
			$prevBtn.hide(0);
			createjs.Sound.stop();
			clearTimeout(timeoutvar);
			$('blind').css('pointer-events', 'none');
			current_sound = createjs.Sound.play('sound_curtain_2');
			$(this).animate({
				'bottom': '45%'
			}, 200, function(){
				$(this).animate({
					'bottom': '100%'
				}, 300);
				$('.qna_container').removeClass('drop-down').addClass('go-up');
			});
			setTimeout(function(){
				countNext++;
				templateCaller();
			}, 900);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}


	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

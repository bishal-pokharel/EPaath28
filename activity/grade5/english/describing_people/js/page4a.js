var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-8',
			textdata : data.string.p4text18,
			imgclass: '',
			textclass : '',
			imgid : 't-4',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-5'
				},{
					imgclass : "girl-6",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-3",
					imgsrc : '',
					imgid : 'deepak-3'
				}
			]
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-9',
			textdata : data.string.p4text19,
			imgclass: '',
			textclass : '',
			imgid : 't-4',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-5'
				},{
					imgclass : "girl-6",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-3",
					imgsrc : '',
					imgid : 'deepak-3'
				}
			]
		}]
	},
	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-10',
			textdata : data.string.p4text20,
			imgclass: '',
			textclass : '',
			imgid : 't-5',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-5'
				},{
					imgclass : "girl-6",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-3",
					imgsrc : '',
					imgid : 'deepak-3'
				}
			]
		}]
	},
	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'teller teller-4 my_font_medium sniglet',
		uppertextblock:[{
			textdata:  data.string.p4text21,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-5'
				},{
					imgclass : "girl-7",
					imgsrc : '',
					imgid : 'girl-8'
				},{
					imgclass : "deepak-4",
					imgsrc : '',
					imgid : 'deepak-4'
				}
			]
		}]
	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		uppertextblockadditionalclass: 'teller teller-4 my_font_medium sniglet',
		uppertextblock:[{
			textdata:  data.string.p4text21,
			textclass: "",
		}],
		lowertextblockadditionalclass: 'teller teller-5 my_font_medium sniglet fade-in-3',
		lowertextblock:[{
			textdata:  data.string.p4text22,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-5'
				},{
					imgclass : "girl-7",
					imgsrc : '',
					imgid : 'girl-8'
				},{
					imgclass : "deepak-4",
					imgsrc : '',
					imgid : 'deepak-4'
				}
			]
		}]
	},
	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-11',
			textdata : data.string.p4text23,
			imgclass: '',
			textclass : '',
			imgid : 't-4',
			imgsrc: '',
			// audioicon: true,
		}],
		lowertextblockadditionalclass: 'teller teller-5 my_font_medium sniglet fade-out',
		lowertextblock:[{
			textdata:  data.string.p4text22,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-2'
				},
				{
					imgclass : "girl-8",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-5",
					imgsrc : '',
					imgid : 'deepak-3'
				},{
					imgclass : "prem-1",
					imgsrc : '',
					imgid : 'prem-1'
				}
			]
		}]
	},
	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-12',
			textdata : data.string.p4text24,
			imgclass: 'flipped-h',
			textclass : '',
			imgid : 't-4',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-2'
				},{
					imgclass : "girl-8",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-5",
					imgsrc : '',
					imgid : 'deepak-3'
				},{
					imgclass : "prem-1",
					imgsrc : '',
					imgid : 'prem-1'
				}
			]
		}]
	},
	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',
		speechbox:[{
			speechbox: 'sp-4',
			textdata : data.string.p4text25,
			imgclass: '',
			textclass : '',
			imgid : 't-3',
			imgsrc: '',
			// audioicon: true,
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-1'
				},{
					imgclass : "girl-4",
					imgsrc : '',
					imgid : 'girl-1'
				},{
					imgclass : "deepak-1",
					imgsrc : '',
					imgid : 'deepak-2'
				}
			]
		}]
	},
	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: '',

		uppertextblockadditionalclass: 'teller teller-7 my_font_medium sniglet',
		uppertextblock:[{
			textdata:  data.string.p4text26,
			textclass: "",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'station-3'
				},
			]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var intervalvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images

			{id: "station-1", src: imgpath+"temple/policestation_bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "station-2", src: imgpath+"temple/policestation_bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "station-3", src: imgpath+"temple/policestation_bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "station-4", src: imgpath+"temple/policestation_bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "station-5", src: imgpath+"temple/policestation_bg05.png", type: createjs.AbstractLoader.IMAGE},

			{id: "grandma", src: imgpath+"temple/grandmum.png", type: createjs.AbstractLoader.IMAGE},


			{id: "girl", src: imgpath+"temple/melisha.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-1", src: imgpath+"temple/melisha01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-2", src: imgpath+"temple/melisha02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-3", src: imgpath+"temple/melisha03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-4", src: imgpath+"temple/melisha04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-5", src: imgpath+"temple/melisha05.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-8", src: imgpath+"temple/melisha08.png", type: createjs.AbstractLoader.IMAGE},
			{id: "worry-1", src: imgpath+"temple/search01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "worry-2", src: imgpath+"temple/search02.png", type: createjs.AbstractLoader.IMAGE},

			{id: "deepak-1", src: imgpath+"temple/police_deepak01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deepak-2", src: imgpath+"temple/police_deepak02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deepak-3", src: imgpath+"temple/police_deepak03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deepak-4", src: imgpath+"temple/police_deepak04.png", type: createjs.AbstractLoader.IMAGE},

			{id: "prem-1", src: imgpath+"temple/police_prem01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "prem-2", src: imgpath+"temple/police_prem02.png", type: createjs.AbstractLoader.IMAGE},

			//textboxes
			{id: "t-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-2", src: 'images/textbox/white/tl-3.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-3", src: 'images/textbox/white/tr-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-4", src: 'images/textbox/white/lb-1-b.png', type: createjs.AbstractLoader.IMAGE},
			{id: "t-5", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},

			// sounds
			{id: "sound_1", src: soundAsset+"s5_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s5_p2.ogg"},
			{id: "sound_3", src: soundAsset+"s5_p3.ogg"},
			{id: "sound_4", src: soundAsset+"s5_p4.ogg"},
			{id: "sound_5", src: soundAsset+"s5_p5.ogg"},
			{id: "sound_6", src: soundAsset+"s5_p6.ogg"},
			{id: "sound_7", src: soundAsset+"s5_p7.ogg"},
			{id: "sound_8", src: soundAsset+"s5_p8.ogg"},
			{id: "sound_9", src: soundAsset+"s5_p9.ogg"},
			{id: "sound_10", src: soundAsset+"s5_p10.ogg"},
			{id: "sound_11", src: soundAsset+"s5_p11.ogg"},
			{id: "radio", src: soundAsset+"radio.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
				vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_card_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				sound_nav("sound_1");
				break;
			case 1:
				sound_nav("sound_2");
				break;
			case 2:
				sound_nav("sound_3");
				break;
			case 3:
				sound_nav("sound_4");
				break;
			case 4:
			setTimeout(function(){
					sound_nav("radio");
			},2000);
			setTimeout(function(){
				sound_nav("sound_5");
			},3500);
				$('.teller-4, .bg-full, .girl-7, .deepak-4').addClass('go-dark');
				break;
			case 5:
			setTimeout(function(){
				sound_nav("sound_6");
			},4000);
				$('.sp-11, .bg-full, .girl-8, .deepak-5, .prem-1').addClass('go-not-dark1');
				break;
				case 6:
				sound_nav("sound_7");
				break;
				case 7:
				sound_nav("sound_8");
				break;
				case 8:
				sound_nav("sound_9");
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}

		function sound_nav(sound_id){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_id);
			current_sound.play();
			current_sound.on("complete", function(){
				nav_button_controls(0);
			});
		}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
			case 4:
				clearInterval(intervalvar);
				countNext++;
				templateCaller();
				break;
			default:
				countNext++;
				templateCaller();
				break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		if(countNext==4){
			clearInterval(intervalvar);
		}
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

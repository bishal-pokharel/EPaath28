var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
	//slide 0
	{
		contentblockadditionalclass:'',
		extratextblock: [{
			textdata: data.string.exins,
			textclass: 'main-instruction-1'
		},{
			textdata: data.string.ex1,
			textclass: 'main-text-1'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-1'
				},
			]
		}]
	},
	//slide 1
	{
		contentblockadditionalclass:'bg-1',
		extratextblock: [{
			textdata: data.string.ex1q1,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-brown class1",
						optiondata: data.string.ex1q1a,
					},
					{
						option_class: "o-brown class2",
						optiondata: data.string.ex1q1b,
					},
					{
						option_class: "o-brown class3",
						optiondata: data.string.ex1q1c,
					},
					{
						option_class: "o-brown class4",
						optiondata: data.string.ex1q1d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-1',
			textdata: data.string.ex1,
			textclass: 'main-text-1 small-text'
		}]
	},
	//slide 2
	{
		contentblockadditionalclass:'bg-2',
		extratextblock: [{
			textdata: data.string.ex1q2,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-green class1",
						optiondata: data.string.ex1q2a,
					},
					{
						option_class: "o-green class2",
						optiondata: data.string.ex1q2b,
					},
					{
						option_class: "o-green class3",
						optiondata: data.string.ex1q2c,
					},
					{
						option_class: "o-green class4",
						optiondata: data.string.ex1q2d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-1',
			textdata: data.string.ex1,
			textclass: 'main-text-1 small-text'
		}]
	},
	//slide 3
	{
		contentblockadditionalclass:'bg-3',
		extratextblock: [{
			textdata: data.string.ex1q3,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-blue class1",
						optiondata: data.string.ex1q3a,
					},
					{
						option_class: "o-blue class2",
						optiondata: data.string.ex1q3b,
					},
					{
						option_class: "o-blue class3",
						optiondata: data.string.ex1q3c,
					},
					{
						option_class: "o-blue class4",
						optiondata: data.string.ex1q3d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-1',
			textdata: data.string.ex1,
			textclass: 'main-text-1 small-text'
		}]
	},




	//wife
	//slide 4
	{
		contentblockadditionalclass:'',
		extratextblock: [{
			textdata: data.string.exins,
			textclass: 'main-instruction-2'
		},{
			textdata: data.string.ex2,
			textclass: 'main-text-2'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-2'
				},
			]
		}]
	},
	//slide 5
	{
		contentblockadditionalclass:'bg-ex2a',
		extratextblock: [{
			textdata: data.string.ex2q1,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex2a class1",
						optiondata: data.string.ex2q1a,
					},
					{
						option_class: "o-ex2a class2",
						optiondata: data.string.ex2q1b,
					},
					{
						option_class: "o-ex2a class3",
						optiondata: data.string.ex2q1c,
					},
					{
						option_class: "o-ex2a class4",
						optiondata: data.string.ex2q1d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-2',
			textdata: data.string.ex2,
			textclass: 'main-text-2 small-text-2'
		}]
	},
	//slide 6
	{
		contentblockadditionalclass:'bg-ex2b',
		extratextblock: [{
			textdata: data.string.ex2q2,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex2b class1",
						optiondata: data.string.ex2q2a,
					},
					{
						option_class: "o-ex2b class2",
						optiondata: data.string.ex2q2b,
					},
					{
						option_class: "o-ex2b class3",
						optiondata: data.string.ex2q2c,
					},
					{
						option_class: "o-ex2b class4",
						optiondata: data.string.ex2q2d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-2',
			textdata: data.string.ex2,
			textclass: 'main-text-2 small-text-2'
		}]
	},
	//slide 7
	{
		contentblockadditionalclass:'bg-ex2c',
		extratextblock: [{
			textdata: data.string.ex2q3,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex2c class1",
						optiondata: data.string.ex2q3a,
					},
					{
						option_class: "o-ex2c class2",
						optiondata: data.string.ex2q3b,
					},
					{
						option_class: "o-ex2c class3",
						optiondata: data.string.ex2q3c,
					},
					{
						option_class: "o-ex2c class4",
						optiondata: data.string.ex2q3d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-2',
			textdata: data.string.ex2,
			textclass: 'main-text-2 small-text-2'
		}]
	},


	//children
	//slide 8
	{
		contentblockadditionalclass:'',
		extratextblock: [{
			textdata: data.string.exins,
			textclass: 'main-instruction-3'
		},{
			textdata: data.string.ex3,
			textclass: 'main-text-3'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "bg-full",
					imgsrc : '',
					imgid : 'im-3'
				},
			]
		}]
	},
	//slide 9
	{
		contentblockadditionalclass:'bg-ex3a',
		extratextblock: [{
			textdata: data.string.ex3q1,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex3a class1",
						optiondata: data.string.ex3q1a,
					},
					{
						option_class: "o-ex3a class2",
						optiondata: data.string.ex3q1b,
					},
					{
						option_class: "o-ex3a class3",
						optiondata: data.string.ex3q1c,
					},
					{
						option_class: "o-ex3a class4",
						optiondata: data.string.ex3q1d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-3',
			textdata: data.string.ex3,
			textclass: 'main-text-3 small-text'
		}]
	},
	//slide 10
	{
		contentblockadditionalclass:'bg-ex3b',
		extratextblock: [{
			textdata: data.string.ex3q2,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex3b class1",
						optiondata: data.string.ex3q2a,
					},
					{
						option_class: "o-ex3b class2",
						optiondata: data.string.ex3q2b,
					},
					{
						option_class: "o-ex3b class3",
						optiondata: data.string.ex3q2c,
					},
					{
						option_class: "o-ex3b class4",
						optiondata: data.string.ex3q2d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-3',
			textdata: data.string.ex3,
			textclass: 'main-text-3 small-text'
		}]
	},
	//slide 11
	{
		contentblockadditionalclass:'bg-ex3c',
		extratextblock: [{
			textdata: data.string.ex3q3,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex3c class1",
						optiondata: data.string.ex3q3a,
					},
					{
						option_class: "o-ex3c class2",
						optiondata: data.string.ex3q3b,
					},
					{
						option_class: "o-ex3c class3",
						optiondata: data.string.ex3q3c,
					},
					{
						option_class: "o-ex3c class4",
						optiondata: data.string.ex3q3d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-3',
			textdata: data.string.ex3,
			textclass: 'main-text-3 small-text'
		}]
	},
	//slide 12
	{
		contentblockadditionalclass:'bg-ex3d',
		extratextblock: [{
			textdata: data.string.ex3q4,
			textclass: 'instruction'
		},{
			textdata: data.string.desc,
			textclass: 'btn-text'
		}],
		exerciseblock: [
			{
				option: [
					{
						option_class: "o-ex3d class1",
						optiondata: data.string.ex3q4a,
					},
					{
						option_class: "o-ex3d class2",
						optiondata: data.string.ex3q4b,
					},
					{
						option_class: "o-ex3d class3",
						optiondata: data.string.ex3q4c,
					},
					{
						option_class: "o-ex3d class4",
						optiondata: data.string.ex3q4d,
					}],
			}
		],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "btn-ex-1",
					imgsrc : '',
					imgid : 'btn-1'
				},
				{
					imgclass : "btn-ex-1 btn-1",
					imgsrc : '',
					imgid : 'btn-2'
				},
			]
		}],
		popupblock:[{
			popupclass: '',
			pageclass: '',
			imgclass : "bg-full",
			imgid : 'im-3',
			textdata: data.string.ex3,
			textclass: 'main-text-3 small-text'
		}]
	}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;


	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var preload;
	var timeoutvar = null;
	var current_sound;

	var scoring = new NumberTemplate();

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "im-1", src: imgpath+"3.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "im-2", src: imgpath+"1.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "im-3", src: imgpath+"2.jpg", type: createjs.AbstractLoader.IMAGE},

			{id: "btn-1", src: imgpath+"btn_02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "btn-2", src: imgpath+"btn_01.png", type: createjs.AbstractLoader.IMAGE},

			{id: "correct", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrect", src: "images/wrong.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "exer", src: soundAsset+"ex.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		scoring.init(10);
		templateCaller();
	}
	//initialize
	init();


	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */


	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	var wrongclick = false;
	var total_count = 0;
	var popup_is_close =  true;

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);
		put_image(content, countNext);
		put_image2(content, countNext);
		$('.correct-icon').attr('src', preload.getResult('correct').src);
		$('.incorrect-icon').attr('src', preload.getResult('incorrect').src);

		scoring.numberOfQuestions();

		/*for randomizing the options*/
		var option_position = [1,2,3,4];
		option_position.shufflearray();
		for(var op=0; op<4; op++){
			$('.main-container').eq(op).addClass('option-pos-'+option_position[op]);
		}

		//instruction audio here
		if(countNext==0 ){
			sound_nav("exer");
		}
		if(countNext==4 || countNext==8){
			$nextBtn.show(0);
		}
		$('.btn-1, .btn-text').click(function(event){
			$('.btn-1').css('top', '62%');
			$('.btn-text').css('top', '64%');
			popup_window($(this), event);
		});
		$('.wrong_button').click(function(event){
			close_popup_window();
		});

		var wrong_clicked = 0;
		var correct_images = ['correct-1.png', 'correct-2.png', 'correct-3.png'];
		$(".option-container").click(function(){
			if($(this).hasClass("class1")){
				if(wrong_clicked<1){
					scoring.update(true);
				}
				var rand_img = Math.floor(Math.random()*correct_images.length);
				$('.option-pos-1, .option-pos-2, .option-pos-3, .option-pos-4').off('mouseenter mouseleave');
				$(".option-container").css('pointer-events','none');
	 			play_correct_incorrect_sound(1);
				$(this).addClass('correct-ans');
				$(this).parent().children('.correct-icon').show(0);
				wrong_clicked = 0;
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				scoring.update(false);
	 			play_correct_incorrect_sound(0);
				$(this).addClass('incorrect-ans');
				$(this).parent().children('.incorrect-icon').show(0);
				wrong_clicked++;
			}
		});
	}

	function sound_nav(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on("complete", function(){
			$nextBtn.show(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_image2(content, count){
		if(content[count].hasOwnProperty('popupblock')){
			var imageClass = content[count].popupblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function popup_window(click_class, event){
		if(popup_is_close){
			// console.log('before started   '+ popup_is_close);
			$( ".option-container" ).css('pointer-events', 'none');

			popup_is_close = false;
			setTimeout(function(){
				console.log('started   '+ popup_is_close);
				$('.popupdiv').css({'display': 'block', 'width': "0%", 'height': '0%', 'left':'50%', 'top': '50%'});
				$('.popuppage').hide(0);
				$('.popupdiv').animate({
					'width': "80%",
					'height': '80%',
					'left':'50%',
					'top': '50%'
				}, 500, function(){
					$('.popuppage').fadeIn(300);
				});
			}, 100);
			$('.wrong_button').show(0);
		}
	}
	function close_popup_window(){
		// current_sound.stop();
		$(".drop-box").css('background-color', 'rgb(120,121,196)');
		$('.popuppage, .wrong_button').fadeOut(300);
		$('.popupdiv').fadeOut(500, function(){
			popup_is_close = true;
			console.log('completed   ' + popup_is_close);
			$( ".option-container" ).css('pointer-events', 'all');
			$('.btn-1').css('top', '60%');
			$('.btn-text').css('top', '62%');
		});
	}

	function templateCaller(){
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		generalTemplate();
		// scoringg purpose code ends
		loadTimelineProgress($total_page,countNext+1);
	}

	$nextBtn.on('click', function() {
		countNext++;
		if(countNext==1 || countNext==5 || countNext==9){
		} else{
			scoring.gotoNext();
		}
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
});

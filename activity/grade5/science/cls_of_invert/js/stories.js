var ProtoStory=[
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.proto
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim01',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.proto
		},
		{
			textclass: "inner-mid-text",
			textdata: data.string.intyp1text1
		}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "microimg",
						imgid : 'microscope',
						imgsrc: "",
					},
					{
						imgclass: "microhand",
						imgid : 'handicon',
						imgsrc: "",
					},
					{
						imgclass: "microamoe",
						imgid : 'amoeba',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp1text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "microimg-move",
						imgid : 'microscope',
						imgsrc: "",
					},
					{
						imgclass: "microamoe-move",
						imgid : 'amoeba',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp1text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'amobreath',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp1text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'amorepro',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp1text5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim01',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp1text6
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp1text7
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp1text8
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp1text9
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'amojpg',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'paramecium',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'euglena',
						imgsrc: "",
					}
				]
			}
		]
	},
// quick test
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.proto
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp1text10
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp1opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp1opt2,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp1opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'amorepro',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp1text11
			}
		]
	}
]

var PorifStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.porif
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "pnktxt",
				textclass: "sidetext",
				textdata: data.string.intyp2text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp2text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "pnktxt",
				textclass: "sidetext",
				textdata: data.string.intyp2text3
			},
			{
				textclass: "buttonsel hoverdiy typ2yes",
				textdata: data.string.inttyp2o1
			},
			{
				textclass: "buttonsel correct hoverdiy typ2no",
				textdata: data.string.inttyp2o2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "pnktxt",
				textclass: "sidetext",
				textdata: data.string.intyp2text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		]
	},
	// {
	// 	singletext:[
	// 		{
	// 			textclass: "strytextclass",
	// 			textdata: data.string.porif
	// 		},
	// 		{
	// 			datahighlightflag: true,
	// 			datahighlightcustomclass: "pnktxt",
	// 			textclass: "sidetext",
	// 			textdata: data.string.intyp2text1
	// 		}
	// 	],
	// 	imageblock:[
	// 		{
	// 			imagestoshow:[
	// 				{
	// 					imgclass: "sideimg",
	// 					imgid : 'sycon1',
	// 					imgsrc: "",
	// 				}
	// 			]
	// 		}
	// 	]
	// },
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp2text7
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp2text8
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp2text9
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp2text10
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'sycon',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'spongilla',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'euplectella',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.porif
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp2text11
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp2opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp2opt2,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp2opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'sycon1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp2text12
			}
		]
	}
]

var ColenStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.coele
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'hydra1',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp3text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'hydra1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp3text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'hydragif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp3text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'hydragif3',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp3text5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'hydragif2',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp3text6
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp3text7
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp3text8
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp3text9
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'hydra',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'jellyfish',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'coral',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.coele
		},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.coele
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp3text10
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp3opt1,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp3opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp3opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'hydra1',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp3text11
			}
		]
	}
]

var PlatyStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.platy
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'tapeworm',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp4text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg ZoomIn",
						imgid : 'tapeworm',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp4text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimgZoomed zoomout",
						imgid : 'tapeworm',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp4text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'tapeworm',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp4text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'Cowoutline',
						imgsrc: "",
					},{
						imgclass: "sideimg tw zoomIn",
						imgid : 'tapeworm_in_cow',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp4text5
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp4text6
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp4text7
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp4text8
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'tapeworm',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'liverfluke',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'hammer_head_planaria',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.platy
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp4text9
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp4opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp4opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp4opt3,
				ans:"correct"
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'tapeworm_inside_cow',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp4text10
			}
		]
	}
]

var NematStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.nemat
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'ascaris',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp5text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'ascaris',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp5text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'ascaris',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp5text3
			},
			{
				textclass: "fadeIn2 malasc",
				textdata: data.string.malasc
			},
			{
				textclass: "fadeIn1 femalasc",
				textdata: data.string.femalasc
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'ascarisbg',
						imgsrc: "",
					},{
						imgclass: "sideimg fadeIn1",
						imgid : 'ascarisfemale',
						imgsrc: "",
					},{
						imgclass: "sideimg fadeIn2",
						imgid : 'ascarismale',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp5text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg boyBody",
						imgid : 'boy_body',
						imgsrc: "",
					},{
						imgclass: "worm zoomin",
						imgid : 'worm_boy',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp5text5
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp5text6
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp5text7
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp5text8
			},
			{
				textclass: "head",
				textdata: data.string.head
			},
			{
				textclass: "tail",
				textdata: data.string.tail
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'ascaris',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'hookworm_1',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'whipworm',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.nemat
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp5text9
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp5opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp5opt2,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp5opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'ascaris',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp5text10
			}
		]
	}
]

var AnnelStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.annel
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'leech_new',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp6text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim06',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext_sec",
				textdata: data.string.intyp6text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'leech_sucking_blood',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp6text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'leech_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp6text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp6text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp6text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp6text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'earthworm',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'anim06',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'polychaete',
						imgsrc: "",
					}
				]
			}
		]
	},
//  quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.annel
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp6text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp6opt1,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp6opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp6opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'earthworm',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp6text9
			}
		]
	}
]

var ArthrStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.arthr
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	//added
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp7text10
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp7text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp7text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp7text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp7text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim07',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'spider',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'crab',
						imgsrc: "",
					}
				]
			}
		]
	},
	// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.arthr
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp7text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp7opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp7opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp7opt3,
				ans:"correct"
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'anim07',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp7text9
			}
		]
	}
]

var MolluStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.mollu
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg zoomin",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text10
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp8text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim08',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp8text4
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp8text5
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp8text6
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp8text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim08',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'octopus',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'slug',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.mollu
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp8text8
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp8opt1
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp8opt2,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp8opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'slug_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp8text9
			}
		]
	}
]

var EchinStory = [
	{
		singletext:[
			{
			textclass: "strytextclass",
			textdata: data.string.echin
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "intypcenimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		],
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg zoominLarge",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'stargif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "sidetext",
				textdata: data.string.intyp9text4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimg",
						imgid : 'starFish_gif',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp9text5
			},
			{
				textclass: "threetext-1",
				textdata: data.string.intyp9text6
			},
			{
				textclass: "threetext-2",
				textdata: data.string.intyp9text7
			},
			{
				textclass: "threetext-3",
				textdata: data.string.intyp9text8
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimg-1",
						imgid : 'anim09',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-2",
						imgid : 'seaurchin',
						imgsrc: "",
					},
					{
						imgclass: "threeimg-3",
						imgid : 'seacucumber',
						imgsrc: "",
					}
				]
			}
		]
	},
// quicktest
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "qtStr",
				textdata: data.string.quicktest
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "bgQc",
						imgid : 'bg_quick_check',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "strytextclass",
				textdata: data.string.echin
			},
			{
				textclass: "inner-mid-text",
				textdata: data.string.intyp9text9
			},
			{
				textclass: "buttonsel hoverdiy diybtns-1",
				textdata: data.string.intyp9opt1,
				ans:"correct"
			},
			{
				textclass: "buttonsel hoverdiy diybtns-2",
				textdata: data.string.intyp9opt2
			},
			{
				textclass: "buttonsel hoverdiy diybtns-3",
				textdata: data.string.intyp9opt3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "diyimg",
						imgid : 'anim09',
						imgsrc: "",
					}
				]
			}
		]
	},
	{
		singletext:[
			{
				textclass: "summary",
				textdata: data.string.summary
			},
			{
				textclass: "pohead",
				textdata: data.string.intyp9text10
			}
		]
	}
]

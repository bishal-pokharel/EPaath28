var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		contentblockadditionalclass: "greenbg",
		singletext:[
			{
				textclass: "centertext",
				textdata: data.string.p2text1
			}
		]
	},
	// slide1
	{
		contentblockadditionalclass: "greenbg",
		headerblock:[
		{
			textdata: data.string.p2text2
		}
	],
	imgandtext:[
		{
			imgtextadditionalclass: "firstblock forhover",
			imgclass: "aganim-01",
			imgid : 'anim01',
			imgsrc: "",
			textdata: data.string.proto
		},
		{
			imgtextadditionalclass: "secondblock forhover",
			imgclass: "aganim-02",
			imgid : 'anim02',
			imgsrc: "",
			textdata: data.string.porif
		},
		{
			imgtextadditionalclass: "thirdblock forhover",
			imgclass: "aganim-03",
			imgid : 'anim03',
			imgsrc: "",
			textdata: data.string.coele
		},
		{
			imgtextadditionalclass: "fourblock forhover",
			imgclass: "aganim-04",
			imgid : 'anim04',
			imgsrc: "",
			textdata: data.string.platy
		},
		{
			imgtextadditionalclass: "fifthblock forhover",
			imgclass: "aganim-05",
			imgid : 'anim05',
			imgsrc: "",
			textdata: data.string.nemat
		},
		{
			imgtextadditionalclass: "sixthblock forhover",
			imgclass: "aganim-06",
			imgid : 'anim06',
			imgsrc: "",
			textdata: data.string.annel
		},
		{
			imgtextadditionalclass: "sevenblock forhover",
			imgclass: "aganim-07",
			imgid : 'anim07',
			imgsrc: "",
			textdata: data.string.arthr
		},
		{
			imgtextadditionalclass: "eightblock forhover",
			imgclass: "aganim-08",
			imgid : 'anim08',
			imgsrc: "",
			textdata: data.string.mollu
		},
		{
			imgtextadditionalclass: "nineblock forhover",
			imgclass: "aganim-09",
			imgid : 'anim09',
			imgsrc: "",
			textdata: data.string.echin
		}
	]
	}
];

$(function () {
	var $board = $('.board');
	var $board2 = $(".storydiv");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "corrimg", src: "images/correct.png", type: createjs.AbstractLoader.IMAGE},
			{id: "incorrimg", src: "images/wrongicon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim01", src: imgpath+"page01/amoeba.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim02", src: imgpath+"page01/sycon.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim03", src: imgpath+"page01/coeleterata.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim04", src: imgpath+"page01/tapeworm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim05", src: imgpath+"page01/hookworm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim06", src: imgpath+"page01/leech.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim07", src: imgpath+"page01/ant01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim08", src: imgpath+"page01/snail.png", type: createjs.AbstractLoader.IMAGE},
			{id: "anim09", src: imgpath+"page01/starfish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "starFish_gif", src: imgpath+"page01/star_fish.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "microscope", src: imgpath+"microscope.png", type: createjs.AbstractLoader.IMAGE},
			{id: "amoeba", src: imgpath+"page02/zoomin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "amobreath", src: imgpath+"page02/ameoba.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "amorepro", src: imgpath+"page02/amiba_reproduction.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "amojpg", src: imgpath+"page02/chaos-carolinens.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "paramecium", src: imgpath+"paramecium.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "euglena", src: imgpath+"euglena.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "handicon", src: "images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},

			{id: "sycon1", src: imgpath+"page02/porifera.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sycon", src: imgpath+"sycon_2.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spongilla", src: imgpath+"spongilla.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "euplectella", src: imgpath+"euplectella.png", type: createjs.AbstractLoader.IMAGE},

			{id: "hydra1", src: imgpath+"page02/hydra01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hydragif", src: imgpath+"page02/hydra_taking_food.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hydragif2", src: imgpath+"page02/hydra-reproduction.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hydragif3", src: imgpath+"page02/hydra movement.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hydra", src: imgpath+"hydra.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "jellyfish", src: imgpath+"jellyfish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "coral", src: imgpath+"coral.png", type: createjs.AbstractLoader.IMAGE},

			{id: "tapeworm", src: imgpath+"page02/tapeworm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tapeworm_in_cow", src: imgpath+"page02/tapeworm_in_cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "Cowoutline", src: imgpath+"page02/cow_outline.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tapeworm_inside_cow", src: imgpath+"tapeworm_inside_cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "liverfluke", src: imgpath+"liverfluke.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hammer_head_planaria", src: imgpath+"hammer_head_planaria.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earthworm", src: imgpath+"earthworm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "polychaete", src: imgpath+"polychaete.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ascarisbg", src: imgpath+"page02/ascaris_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ascarisfemale", src: imgpath+"page02/ascaris_female.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ascarismale", src: imgpath+"page02/ascaris_male.png", type: createjs.AbstractLoader.IMAGE},

			{id: "ascaris", src: imgpath+"ascaris.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ascaris_2", src: imgpath+"ascaris_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hookworm_inside_man", src: imgpath+"hookworm_inside_man.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hookworm_1", src: imgpath+"hookworm_1.png", type: createjs.AbstractLoader.IMAGE},
			{id: "whipworm", src: imgpath+"page02/whipworm.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_body", src: imgpath+"page02/boy-body.png", type: createjs.AbstractLoader.IMAGE},
			{id: "worm_boy", src: imgpath+"page02/worm_boy-body.png", type: createjs.AbstractLoader.IMAGE},

			{id: "leech_sucking_blood", src: imgpath+"leech_sucking_blood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "crab", src: imgpath+"crab.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sea_cucumber", src: imgpath+"sea_cucumber.png", type: createjs.AbstractLoader.IMAGE},
			{id: "octopus", src: imgpath+"octopus.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slug", src: imgpath+"slug.png", type: createjs.AbstractLoader.IMAGE},
			{id: "slug_gif", src: imgpath+"page02/slug-animation.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "leech_gif", src: imgpath+"page02/annelida_enhanced.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "leech_new", src: imgpath+"page02/leech_new.png", type: createjs.AbstractLoader.IMAGE},

			{id: "stargif", src: imgpath+"page02/star-fish.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "seacucumber", src: imgpath+"page02/sea-cucumber.png", type: createjs.AbstractLoader.IMAGE},
			{id: "seaurchin", src: imgpath+"page02/sea-urchin.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_quick_check", src: imgpath+"bg_quick_check.png", type: createjs.AbstractLoader.IMAGE},
			//textboxes
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
            {id: "qck", src: soundAsset+"quick_check.ogg"},
            {id: "sound_1", src: soundAsset+"s2_p1.ogg"},
			{id: "sound_2", src: soundAsset+"s2_p2_1.ogg"},
			{id: "pr_1", src: soundAsset+"s2_p2_protozoa_1.ogg"},
			{id: "pr_2", src: soundAsset+"s2_p2_protozoa_2.ogg"},
			{id: "pr_3", src: soundAsset+"s2_p2_protozoa_3.ogg"},
			{id: "pr_4", src: soundAsset+"s2_p2_protozoa_4.ogg"},
			{id: "pr_5", src: soundAsset+"s2_p2_protozoa_5.ogg"},
			{id: "pr_6", src: soundAsset+"s2_p2_protozoa_6.ogg"},
			{id: "pr_7", src: soundAsset+"s2_p2_protozoa_7.ogg"},
			{id: "pr_9", src: soundAsset+"s2_p2_protozoa_8.ogg"},
			{id: "pr_10", src: soundAsset+"s2_p2_protozoa_9.ogg"},

            {id: "por_1", src: soundAsset+"s2_p2_porifera_1.ogg"},
            {id: "por_2", src: soundAsset+"s2_p2_porifera_2.ogg"},
            {id: "por_3", src: soundAsset+"s2_p2_porifera_3.ogg"},
            {id: "por_4", src: soundAsset+"s2_p2_porifera_4.ogg"},
            {id: "por_5", src: soundAsset+"s2_p2_porifera_5.ogg"},
            {id: "por_6", src: soundAsset+"s2_p2_porifera_6.ogg"},
            {id: "por_8", src: soundAsset+"s2_p2_porifera_7.ogg"},
            {id: "por_9", src: soundAsset+"s2_p2_porifera_8.ogg"},

            {id: "col_1", src: soundAsset+"s2_p2_coelenterata_1.ogg"},
            {id: "col_2", src: soundAsset+"s2_p2_coelenterata_2.ogg"},
            {id: "col_3", src: soundAsset+"s2_p2_coelenterata_3.ogg"},
            {id: "col_4", src: soundAsset+"s2_p2_coelenterata_4.ogg"},
            {id: "col_5", src: soundAsset+"s2_p2_coelenterata_5.ogg"},
            {id: "col_6", src: soundAsset+"s2_p2_coelenterata_6.ogg"},
            {id: "col_8", src: soundAsset+"s2_p2_coelenterata_7.ogg"},
            {id: "col_9", src: soundAsset+"s2_p2_coelenterata_8.ogg"},


            {id: "plat_1", src: soundAsset+"s2_p2_platythelminthes_1.ogg"},
            {id: "plat_2", src: soundAsset+"s2_p2_platythelminthes_2.ogg"},
            {id: "plat_3", src: soundAsset+"s2_p2_platythelminthes_3.ogg"},
            {id: "plat_4", src: soundAsset+"s2_p2_platythelminthes_4.ogg"},
            {id: "plat_5", src: soundAsset+"s2_p2_platythelminthes_5.ogg"},
            {id: "plat_6", src: soundAsset+"s2_p2_platythelminthes_6.ogg"},
            {id: "plat_8", src: soundAsset+"s2_p2_platythelminthes_7.ogg"},
            {id: "plat_9", src: soundAsset+"s2_p2_platythelminthes_8.ogg"},

            {id: "nema_1", src: soundAsset+"s2_p2_nemathelminthes_1.ogg"},
            {id: "nema_2", src: soundAsset+"s2_p2_nemathelminthes_2.ogg"},
            {id: "nema_3", src: soundAsset+"s2_p2_nemathelminthes_3.ogg"},
            {id: "nema_4", src: soundAsset+"s2_p2_nemathelminthes_4.ogg"},
            {id: "nema_5", src: soundAsset+"s2_p2_nemathelminthes_5.ogg"},
            {id: "nema_6", src: soundAsset+"s2_p2_nemathelminthes_6.ogg"},
            {id: "nema_8", src: soundAsset+"s2_p2_nemathelminthes_7.ogg"},
            {id: "nema_9", src: soundAsset+"s2_p2_nemathelminthes_8.ogg"},

            {id: "ann_1", src: soundAsset+"s2_p2_annelida_1.ogg"},
            {id: "ann_2", src: soundAsset+"s2_p2_annelida_2.ogg"},
            {id: "ann_3", src: soundAsset+"s2_p2_annelida_3.ogg"},
            {id: "ann_4", src: soundAsset+"s2_p2_annelida_4.ogg"},
            {id: "ann_5", src: soundAsset+"s2_p2_annelida_5.ogg"},
            {id: "ann_7", src: soundAsset+"s2_p2_annelida_6.ogg"},
            {id: "ann_8", src: soundAsset+"s2_p2_annelida_7.ogg"},

            {id: "arth_1", src: soundAsset+"s2_p2_arthropoda_1.ogg"},
            {id: "arth_2", src: soundAsset+"s2_p2_arthropoda_2.ogg"},
            {id: "arth_3", src: soundAsset+"s2_p2_arthropoda_3.ogg"},
            {id: "arth_4", src: soundAsset+"s2_p2_arthropoda_4.ogg"},
            {id: "arth_5", src: soundAsset+"s2_p2_arthropoda_5.ogg"},
            {id: "arth_6", src: soundAsset+"s2_p2_arthropoda_6.ogg"},
            {id: "arth_8", src: soundAsset+"s2_p2_arthropoda_7.ogg"},
            {id: "arth_9", src: soundAsset+"s2_p2_arthropoda_8.ogg"},

            {id: "mol_1", src: soundAsset+"s2_p2_mollusca_1.ogg"},
            {id: "mol_2", src: soundAsset+"s2_p2_mollusca_2.ogg"},
            {id: "mol_3", src: soundAsset+"s2_p2_mollusca_3.ogg"},
            {id: "mol_4", src: soundAsset+"s2_p2_mollusca_4.ogg"},
            {id: "mol_5", src: soundAsset+"s2_p2_mollusca_5.ogg"},
            {id: "mol_6", src: soundAsset+"s2_p2_mollusca_6.ogg"},
            {id: "mol_8", src: soundAsset+"s2_p2_mollusca_7.ogg"},
            {id: "mol_9", src: soundAsset+"s2_p2_mollusca_8.ogg"},

            {id: "ech_1", src: soundAsset+"s2_p2_echinodermata_1.ogg"},
            {id: "ech_2", src: soundAsset+"s2_p2_echinodermata_2.ogg"},
            {id: "ech_3", src: soundAsset+"s2_p2_echinodermata_3.ogg"},
            {id: "ech_4", src: soundAsset+"s2_p2_echinodermata_4.ogg"},
            {id: "ech_5", src: soundAsset+"s2_p2_echinodermata_5.ogg"},
            {id: "ech_6", src: soundAsset+"s2_p2_echinodermata_6.ogg"},
            {id: "ech_8", src: soundAsset+"s2_p2_echinodermata_7.ogg"},
            {id: "ech_9", src: soundAsset+"s2_p2_echinodermata_8.ogg"},



        ];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templatecaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	var storyCounter = 0;
	function generaltemplate() {
		var myCurrClass;
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
        countNext==0?sound_player("sound_1",true):countNext==1?sound_player("sound_2",false):"";
		$(".imgtextcontainer").click(function(){
			$myStory = $(this);
			myCurrClass = $myStory.attr('class').split(' ')[1];
			if($myStory.hasClass("forhover")){
				storyNext = 0;
				//$myStory.addClass("disabled").removeClass("forhover");

			if($myStory.hasClass("firstblock")){
				generalStoryTemplate(ProtoStory);
			}
			else if($myStory.hasClass("secondblock")){
				generalStoryTemplate(PorifStory);
			}
			else if($myStory.hasClass("thirdblock")){
				generalStoryTemplate(ColenStory);
			}
			else if($myStory.hasClass("fourblock")){
				generalStoryTemplate(PlatyStory);
			}
			else if($myStory.hasClass("fifthblock")){
				generalStoryTemplate(NematStory);
			}
			else if($myStory.hasClass("sixthblock")){
				generalStoryTemplate(AnnelStory);
			}
			else if($myStory.hasClass("sevenblock")){
				generalStoryTemplate(ArthrStory);
			}
			else if($myStory.hasClass("eightblock")){
				generalStoryTemplate(MolluStory);
			}
			else{
				generalStoryTemplate(EchinStory);
			}
			}
		});

    // remove ;ater
		// $(".firstblock").trigger("click");

		function generalStoryTemplate(currentstory){
			$(".storydiv").show(0);
			var source2 = $("#story-template").html();
			var template2 = Handlebars.compile(source2);
			var html2 = template2(currentstory[storyNext]);
			//$board2.html(html2);

			$(".storydiv").html(html2);
			texthighlight($board);
			storynavigation();
			put_image3(currentstory, storyNext);
			console.log("storynext val"+storyNext);
			switch (currentstory) {
				case ProtoStory:
                    storyNext==7?sound_player("qck",false):sound_player("pr_"+(storyNext+1),false);
                    switch (storyNext) {
						case 1:
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
							$(".microhand").click(function(){
								$(".microimg").trigger("click");
							});
							$(".microimg").click(function(){
								$(".microhand").hide(0);
								$(".microamoe").fadeIn(1000);
								storynavigation();
							});
						break;
						case 8:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
				break;
				case PorifStory:
                    storyNext==6?sound_player("qck",false):sound_player("por_"+(storyNext+1),false);
                    switch (storyNext) {
						case 3:
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
				break;
				case ColenStory:
                    storyNext==6?sound_player("qck",false):sound_player("col_"+(storyNext+1),false);
                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
				break;
				case PlatyStory:
                    storyNext==6?sound_player("qck",false):sound_player("plat_"+(storyNext+1),false);

                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
					break;
				case NematStory:
                    storyNext==6?sound_player("qck",false):sound_player("nema_"+(storyNext+1),false);
                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
					break;
				case AnnelStory:
                    storyNext==5?sound_player("qck",false):sound_player("ann_"+(storyNext+1),false);
                    switch (storyNext) {
						case 6:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
					break;
				case ArthrStory:
                    storyNext==6?sound_player("qck",false):sound_player("arth_"+(storyNext+1),false);
                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
					break;
				case MolluStory:
                    storyNext==6?sound_player("qck",false):sound_player("mol_"+(storyNext+1),false);
                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
					break;
				case EchinStory:
                    storyNext==6?sound_player("qck",false):sound_player("ech_"+(storyNext+1),false);
                    switch (storyNext) {
						case 7:
							var optionclass = ["buttonsel hoverdiy diybtns-1","buttonsel hoverdiy diybtns-2","buttonsel hoverdiy diybtns-3"];
							shuffleOptions(optionclass);
							$(".storynxt").hide(0);
							$(".storyprev").hide(0);
						break;
						default:

					}
			break;
				default:

			}

			function shuffleOptions(optionclass){
	      var optdiv = $(".storydiv");
	      for (var i = optdiv.find(".buttonsel").length; i >= 0; i--) {
	          optdiv.append(optdiv.find(".buttonsel").eq(Math.random() * i | 0));
	      }
	      optdiv.find(".buttonsel").removeClass().addClass("current");
	      optdiv.find(".current").each(function (index) {
	          $(this).addClass(optionclass[index]);
	          $(this).addClass($(this).attr("data-answer"));
	      });
			}

			$(".buttonsel").click(function(){
				if($(this).hasClass("hoverdiy")){
					$(this).removeClass('hoverdiy');
						if($(this).hasClass("correct")){
							current_sound.stop();
							play_correct_incorrect_sound(1);
							$(this).css("background","#bed62f");
							$(this).css("border","5px solid #deef3c");
							$(this).css("color","white");
							appender($(this),'corrimg');
							$('.buttonsel').removeClass('hoverdiy forhoverimg');
							storynavigation();
						}
						else{
                            current_sound.stop();
							play_correct_incorrect_sound(0);
							appender($(this),'incorrimg');
							$(this).css("background","#FF0000");
							$(this).css("border","5px solid #980000");
							$(this).css("color","white");
						}
				}

					function appender($this, icon){
						// var addfactor = $this.parent().position();
						var posIt = $this.position();
						var centerX = ((posIt.left + ($this.outerWidth()/2)) / $(".storydiv").width())*100;
						var centerY = ((posIt.top + $this.outerHeight()) / $(".storydiv").height())*100;
						$(".storydiv").append("<img style='left:"+centerX+"%;top:"+(centerY+2)+"%;position:absolute;width:5%;transform:translateX(-50%);' src= '"+ preload.getResult(icon).src +"'>");
						}
				});

			$(".closebtn").click(function(){
				$("."+myCurrClass).addClass("disabled");
				if(($("."+myCurrClass)).hasClass("read")){
				storyCounter;
				}
				else{
					storyCounter++;
				}
				$("."+myCurrClass).addClass("read");
				if(storyCounter == 9)
					navigationcontroller();
				$(this).parent().fadeOut();
			});

			$(".storynxt").on("click", function(){
				storyNext++;
				generalStoryTemplate(currentstory);
			});

			$(".storyprev").on("click", function(){
				storyNext--;
				generalStoryTemplate(currentstory);
			});

			function storynavigation(){
					if(storyNext == 0){
						$(".storyprev").hide(0);
						$(".storynxt").show(0);
					}
					else if(storyNext == currentstory.length - 1){
						$(".storynxt").hide(0);
						$(".storyprev").show(0);
						$(".closebtn").show(0);
					}
					else{
						$(".storyprev").show(0);
						$(".storynxt").show(0);
					}
			}
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('imgandtext')){
			var imgandtext = content[count].imgandtext;
			for(var i=0; i<imgandtext.length; i++){
				var image_src = preload.getResult(imgandtext[i].imgid).src;
				//get list of classes
				var classes_list = imgandtext[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}

	function put_image3(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templatecaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');


		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templatecaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templatecaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templatecaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

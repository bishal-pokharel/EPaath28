var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/";

var content=[
// slide 1
{
	contentblockadditionalclass:"bluebg",
	extratextblock:[{
		textclass:"toptext",
		textdata: data.string.exc_pg1
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgid:'anj_scientist',
				imgclass:"btmImage btIm1",
				imgsrc:''
			},
			{
				imgid:'boy_scientist',
				imgclass:"btmImage btIm2",
				imgsrc:''
			},
			{
				imgid:'keti_scientist',
				imgclass:"btmImage btIm3",
				imgsrc:''
			}
		]
	}]

},
// slide 2
{
	contentblockadditionalclass:"bluebg",
	extratextblock:[{
		textclass:"toptext",
		textdata: data.string.exc_pg2
	}],
	imageblock:[{
		imagestoshow:[
			{
				imgid:'anj_scientist',
				imgclass:"btmImage btIm1",
				imgsrc:''
			},
			{
				imgid:'boy_scientist',
				imgclass:"btmImage btIm2",
				imgsrc:''
			},
			{
				imgid:'keti_scientist',
				imgclass:"btmImage btIm3",
				imgsrc:''
			}
		]
	}]

},
// slide 3
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg4
				}
			]
	}]
},
// slide 4--> blinking triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg5
				}
			]
	}
],
svgblock:[{
	svgblock:"numbers_svg"
}]
},
// slide 5
{
	contentblockadditionalclass:"blackbg",
	imageTextBlock:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk blk1',
					imgid:'microscope01',
					imgclass:"hlpImg pulse hi1",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk blk2',
					imgid:'microscope',
					imgclass:"hlpImg hidden",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideTextLong',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg6_1
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv',
		exerciseblockclass:"exercise1 ht80",
		ques_class:'questionClass fntsml',
		textdata:data.string.exc_pg6_2,
		exeoptions:[
			{
				optaddclass:'correct',
				optdata:data.string.ex1op1
			},
			{
				optaddclass:'',
				optdata:data.string.ex1op2
			},
			{
				optaddclass:'',
				optdata:data.string.ex1op3
			}
		]
	}
	]
},
// slide 6
{
	contentblockadditionalclass:"blackbg",
	imageTextBlock:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg7_1
				},
				{
					textblkclass:'sideHelpImgBlk whtFnt leftAlign',
					textclass:"smlSidetxt",
					textdata: data.string.ex2detail
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv',
		exerciseblockclass:"exercise2",
		ques_class:'questionClass',
		textdata:data.string.exc_pg7_2,
		exeoptions:[
			{
				optaddclass:'correct',
				optdata:data.string.ex2op1
			},
			{
				optaddclass:'',
				optdata:data.string.ex2op2
			},
			{
				optaddclass:'',
				optdata:data.string.ex2op3
			}
		]
	}
	]
},
// slide 7 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 8
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sckBoyBg',
					imgid:'sick_boy_bg',
					imgclass:"imgbg",
					imgsrc:''
				},
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg8_1
				},{
					textblkclass:'fulLineINstrn',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg8_2
				}
			]
	}
	],
	imageblock:[
	{
		imagestoshow:[

			{
				imgid:'sick_boy',
				imgclass:"sick_boy",
				imgsrc:''
			},
			{
				imgid:'mag_glass',
				imgclass:"mag_glass hidden",
				imgsrc:''
			}
		]
	}]
},
// slide 9
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'sideHelpImgBlk transparent',
					imgid:'ascaris',
					imgclass:"hlpImg hi1",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'topQn',
					textclass:"s9Qn",
					textdata: data.string.exc_pg9_1
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv',
		exerciseblockclass:"exercise1",
		exeoptions:[
			{
				optaddclass:'purplebg',
				optdata:data.string.ex3op1
			},
			{
				optaddclass:'purplebg correct',
				optdata:data.string.ex3op2
			},
			{
				optaddclass:'purplebg',
				optdata:data.string.ex3op3
			},
			{
				optaddclass:'purplebg',
				optdata:data.string.ex3op4
			}
		]
	}
	]
},
// slide 10
{
	contentblockadditionalclass:"blackbg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk blk1',
					imgid:'roundWorm',
					imgclass:"hlpImg top10",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideTextLong',
					textclass:"smallTxt",
					textdata: data.string.exc_pg10_1
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv shortOptDiv',
		exerciseblockclass:"exercise1 creambg ht80",
		ques_class:'questionClass',
		textdata:data.string.exc_pg10_2,
		exeoptions:[
			{
				optaddclass:'',
				optdata:data.string.ex4op1
			},
			{
				optaddclass:'correct',
				optdata:data.string.ex4op2
			},
			{
				optaddclass:'',
				optdata:data.string.ex4op3
			}
		]
	}
	],
	extratextblock:[{
		textclass:'btmHint',
		textdata: data.string.hinttxt
	}],
	hintblock:[{
		hintblockclass:"hintblock hidden",
		textclass:'hints',
		textdata: data.string.hintascaris,
		imgid:'cross',
		imgclass:"cross",
		imgsrc:''

	}]
},
// slide 11 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 12
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sckBoyBg',
					imgid:'bg_for_pig',
					imgclass:"background",
					imgsrc:''
				},
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg12_1
				},{
					textblkclass:'fulLineINstrn',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg12_2
				}
			]
	}
	],
	imageblock:[
	{
		imagestoshow:[

			{
				imgid:'pig',
				imgclass:"sick_boy pig",
				imgsrc:''
			},
			{
				imgid:'zoom02',
				imgclass:"mag_glass pigGlass hidden",
				imgsrc:''
			}
		]
	}]
},
// slide 13
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	imgoptn:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg13_1
				}
			]
	}
	],
	imgoptnblk:[
		{
			imgblkclass:'imgopn correct leftBlock',
			imgid:'tapeworm',
			imgclass:"blkimg bi1",
		},
		{
			imgblkclass:'imgopn rightBlock',
			imgid:'ascaris',
			imgclass:"blkimg bi2"
		}
	]
},
// slide 14
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg14_1
				},{
					textblkclass:'fulLineINstrn',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg14_2
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv_sec',
		exerciseblockclass:"exercise3",
		exeoptions:[
			{
				optcontainerextra:'optionscontainer_sec',
				optaddclass:'correct',
				optdata:data.string.ex4op1
			},
			{
				optcontainerextra:'optionscontainer_sec',
				optaddclass:'',
				optdata:data.string.ex4op2
			},
			{
				optcontainerextra:'optionscontainer_sec',
				optaddclass:'',
				optdata:data.string.ex4op3
			}
		]
	}
	],
	extratextblock:[{
		textclass:'btmHint',
		textdata: data.string.hinttxt
	}],
	hintblock:[{
		hintblockclass:"hintblock hidden",
		textclass:'hints',
		textdata: data.string.tapehint,
		imgid:'cross',
		imgclass:"cross",
		imgsrc:''

	}]
},
// slide 15 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 16
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
		{

			imgid:'bg_for_spider',
			imgclass:"bgImg",
			imgsrc:''
		},
		{

			imgid:'spider',
			imgclass:"spiderImg",
			imgsrc:''
		}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg15_1
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv_sec transparent',
		exerciseblockclass:"exercise2 ",
		exeoptions:[
			{
				optaddclass:'correct',
				optdata:data.string.ex7op1
			},
			{
				optaddclass:'',
				optdata:data.string.ex7op2
			},
			{
				optaddclass:'',
				optdata:data.string.ex7op3
			}
		]
	}
	],
},
// slide 17
{
	contentblockadditionalclass:"blackbg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk',
					imgid:'grasshooper',
					imgclass:"hlpImg hi1",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt smlFnt",
					textdata: data.string.exc_pg16_1
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv_sec top',
		exerciseblockclass:"exercise4",
		exeoptions:[
			{
				optaddclass:'algnLeft smlerFnt',
				optdata:data.string.ex8op1
			},
			{
				optaddclass:'algnLeft correct smlerFnt',
				optdata:data.string.ex8op2
			},
			{
				optaddclass:'algnLeft smlerFnt',
				optdata:data.string.ex8op3
			}
		]
	}
	],
	extratextblock:[{
		textclass:'btmHint',
		textdata: data.string.hinttxt
	}],
	hintblock:[{
		hintblockclass:"hintblock hidden",
		textclass:'hints',
		textdata: data.string.spiderhint,
		imgid:'cross',
		imgclass:"cross",
		imgsrc:''

	}]
},

// slide 18 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 19
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk small transparent',
					imgid:'earthworm',
					imgclass:"hlpImg hi1",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg19_1
				},{
					textblkclass:'fulLineINstrn longer',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg19_2
				}
			]
	}
	],
	dragdiv:[{
		dragclass:"topLong",
		dragdivoptns:[{
			textclass:"draggable d1",
			textdata: data.string.ex9op1,
			dataanswer:"Has long food tube"
		},{
			textclass:"draggable d2",
			textdata: data.string.ex9op2,
			dataanswer:"Segmented body"
		},{
			textclass:"draggable d3",
			textdata: data.string.ex9op3,
			dataanswer:""
		}]
	}],
	dropdiv:[{
		dropclass:"sidedrop",
		dropdivoptns:[,{
			textclass:"feature",
			textdata: data.string.roundwormfeature,
		},{
			dropplaceclass:"dropPlace",
			// textclass:"droppable dp1",
			// textdata: data.string.ex9op1,
			dataanswer:"Has long food tube"
		},{
			dropplaceclass:"dropPlace",
			// textclass:"droppable dp2",
			// textdata: data.string.ex9op2,
			dataanswer:"Segmented body"
		}]
	}]

},
// slide 20 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 21
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg20_1
				}
			]
	}],
	imageblock:[{
		imagestoshow:[
		{
			imgid:'ocean_bg',
			imgclass:"oceanBg",
			imgsrc:''
		},
		{
			imgid:'submarine',
			imgclass:"submarine",
			imgsrc:''
		}]
	}]
},
// slide 22
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imageblock:[{
		imagestoshow:[{
		imgid:'hydra_buttom',
		imgclass:"grayscaled hydra_buttom",
		imgsrc:''
		}]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'poriferaBlk pb1',
					imgid:'hydra_top',
					imgclass:"grayscaled hydra_top ",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg22_1
				},{
					textblkclass:'fulLineINstrn',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg22_2
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdivbox',
		exerciseblockclass:"exercise2",
		exeoptions:[
			{
				optcontainerextra:"bxRow",
				optaddclass:'correct btnslSec',
				optdata:data.string.ex10op1
			},
			{
				optcontainerextra:"bxRow",
				optaddclass:'correct1 btnslSec',
				optdata:data.string.ex10op2
			},
			{
				optcontainerextra:"bxRow",
				optaddclass:'btnslSec',
				optdata:data.string.ex10op3
			},
			{
				optcontainerextra:"bxRow",
				optaddclass:'btnslSec',
				optdata:data.string.ex10op4
			}
		]
	}
	]

},
// slide 23
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'poriferaBlk pbSec',
					imgid:'hydra_main',
					imgclass:"hydra",
					imgsrc:''
				}
			],
			texttoshow:[{
					textblkclass:'fulLineINstrn xtop',
					textclass:"smlSidetxt fntbg",
					textdata: data.string.ex13qn
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdivbox',
		exerciseblockclass:"exercise2",
		exeoptions:[
			{
				optcontainerextra:"bxRowSec",
				optaddclass:'btnslSec',
				optdata:data.string.ex13op1
			},
			{
				optcontainerextra:"bxRowSec",
				optaddclass:'correct btnslSec',
				optdata:data.string.ex13op2
			}
		]
	}
	]

},
// slide 24
{
	contentblockadditionalclass:"blackbg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk sideMngd transparent',
					imgid:'euplectella_new',
					imgclass:"hlpImg hi1",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg24_1
				},{
					textblkclass:'fulLineINstrn',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg24_2
				}
			]
	}
	],
	exetype1:[
	{
		optionsdivclass:'optionsdiv',
		exerciseblockclass:"exercise2 creambg",
		exeoptions:[
			{
				optaddclass:'correct',
				optdata:data.string.ex11op1
			},
			{
				optaddclass:'',
				optdata:data.string.ex11op2
			},
			{
				optaddclass:'',
				optdata:data.string.ex11op3
			}
		]
	}
	],
},
// slide 25 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	imgtxtblock:[
	{
		imagetextcontainerclass:'toptext',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg',
					imgid:'anj_scientist',
					imgclass:"smlImg",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg5
				}
			]
	}
	],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
// slide 26
{
	contentblockadditionalclass:"creambg",
	imageTextBlock:true,
	hint:true,
	imgtxtblock:[
	{
		imagetextcontainerclass:'background',
			imagestoshow:[
				{
					imgblockclass:'smlSideIMg_sec',
					imgid:'anj_scientist',
					imgclass:"smlImg_sec",
					imgsrc:''
				},
				{
					imgblockclass:'sideHelpImgBlk small transparent',
					imgid:'sea_urchin_new',
					imgclass:"hlpImg urchin",
					imgsrc:''
				}
			],
			texttoshow:[
				{
					textblkclass:'sideText_sec',
					textclass:"smlSidetxt",
					textdata: data.string.exc_pg25_1
				},{
					textblkclass:'fulLineINstrn longer',
					textclass:"smlSidetxt ",
					textdata: data.string.exc_pg25_2
				}
			]
	}
	],
	dragdiv:[{
		dragclass:"topLong",
		dragdivoptns:[{
			textclass:"draggable d1",
			textdata: data.string.ex12op1,
			dataanswer:"Spiny skinned"
		},{
			textclass:"draggable d3",
			textdata: data.string.ex12op2,
			dataanswer:"Segmented body"
		},{
			textclass:"draggable d3",
			textdata: data.string.ex12op3,
			dataanswer:"tentacles"
		}]
	}],
	dropdiv:[{
		dropclass:"sidedrop",
		dropdivoptns:[{
			textclass:"feature",
			textdata: data.string.urchinfeature,
		},{
			dropplaceclass:"dropPlace",
			// textclass:"droppable dp1",
			// textdata: data.string.ex12op1,
			dataanswer:"Spiny skinned"
		}]
	}]

},
// slide 27 blink triangles
{
	contentblockadditionalclass:"bluebg",
	imageTextBlock:true,
	imageblock:[{
		imagestoshow:[
			{
				imgid:'map',
				imgclass:"mapBg btmTwnty",
				imgsrc:''
			}
		]
	}],
	// imgtxtblock:[
	// {
	// 	imagetextcontainerclass:'toptext',
	// 		imagestoshow:[
	// 			{
	// 				imgblockclass:'smlSideIMg',
	// 				imgid:'anj_scientist',
	// 				imgclass:"smlImg",
	// 				imgsrc:''
	// 			}
	// 		],
	// 		// texttoshow:[
	// 		// 	{
	// 		// 		textblkclass:'sideText',
	// 		// 		textclass:"smlSidetxt",
	// 		// 		textdata: data.string.exc_pg5
	// 		// 	}
	// 		// ]
	// }
	// ],
	svgblock:[{
		svgblock:"numbers_svg"
	}]
},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var corrCount = 0;
	var corCountMap = {'1':0,
										 '2':0,
										 '3':0,
										 '4':0,
										 '5':0,
										 '6':0,
										 '7':0
									 };
	var checkClick = true;
	$(".scoreboard").hide(0);
	/*for limiting the questions to 10*/
	var $total_page = 27;
	loadTimelineProgress($total_page,countNext+1);
	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "anj_scientist", src: imgpath+"anjana-scientist.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy_scientist", src: imgpath+"boyscientist.png", type: createjs.AbstractLoader.IMAGE},
			{id: "keti_scientist", src: imgpath+"ketiscientist.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lab_professor", src: imgpath+"lab-professor-01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "map", src: imgpath+"map.png", type: createjs.AbstractLoader.IMAGE},
			{id: "microscope01", src: imgpath+"microscope01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "microscope", src: imgpath+"microscope.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sick_boy_bg", src: imgpath+"bg_for_sickboy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sick_boy", src: imgpath+"sick_boy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mag_glass", src: imgpath+"zoom01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ascaris", src: imgpath+"q01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "roundWorm", src: imgpath+"q03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cross", src: "images/white_wrong.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_pig", src: imgpath+"bg_for_pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "pig", src: imgpath+"pig.png", type: createjs.AbstractLoader.IMAGE},
			{id: "zoom02", src: imgpath+"zoom02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_spider", src: imgpath+"bg_for_spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "spider", src: imgpath+"spider.png", type: createjs.AbstractLoader.IMAGE},
			{id: "grasshooper", src: imgpath+"grasshooper.png", type: createjs.AbstractLoader.IMAGE},
			{id: "earthworm", src: imgpath+"earthworm03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "ocean_bg", src: imgpath+"ocean_bg.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "submarine", src: imgpath+"submarine.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "hydra_top", src: imgpath+"hydra_top02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hydra_buttom", src: imgpath+"hydra_buttom02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "euplectella_new", src: imgpath+"euplectella_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sea_urchin_new", src: imgpath+"sea_urchin_new.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tapeworm", src: imgpath+"q02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "gold", src: imgpath+"gold.png", type: createjs.AbstractLoader.IMAGE},
			{id: "traingle", src: imgpath+"traingle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "question", src: imgpath+"question.svg", type: createjs.AbstractLoader.IMAGE},
			{id: "hydra_main", src: imgpath+"hydra_main.png", type: createjs.AbstractLoader.IMAGE},

			// sounds
			// {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		//scoring.init(10);
		templateCaller();
	}
	//initialize
	init();

	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
 	}

	 /*values in this array is same as the name of images of eggs in image folder*/

	 //create eggs
	 var testin = new NumberTemplate();

	 	//eggTemplate.eggMove(countNext);
 		testin.init(27);

	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
		put_image(content, countNext);
		content[countNext].imageTextBlock?put_image_sec(content, countNext):"";
		content[countNext].hint?put_image_third(content, countNext):"";
		content[countNext].imgoptn?put_image_frth(content, countNext):"";
	 	var testcount = 0;

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	/*generate question no at the beginning of question*/
	 	testin.numberOfQuestions();

	 	/*for randomizing the options*/
		function randomize(parentDiv){
			var parent = $(parentDiv);
			var divs = parent.children();
			while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
			}
		}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	var ansClicked = false;
	 	var wrngClicked = false;
		var count = 0;
	 	switch(countNext){
	 		case 0:
	 		case 1:
			case 2:
				$nextBtn.show(0);
			break;
			case 3:
				var s = Snap('#numbers_svg');
				var coins = [];
				var svg = Snap.load(preload.getResult('question').src, function(loadedFragment){
					s.append(loadedFragment);
					for(var i=1; i<8; i++){
						Snap.select('#q'+i+'c').attr({'display':'none'});
					}
					for (var i=1; i<=8; i++){
						$("#q"+i).css('filter','grayscale(100%)');
					}
					var $elem = Snap.select('#q1');
					$elem.addClass('scale_eff');
					$elem.click(function(){
							countNext++;
							testin.gotoNext();
							templateCaller();
					});
				});
			case 4:
				btnslClk();
				randomize(".optionsdiv");
				$(".exercise1").css("right","-51%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
					$(".hi1").css("pointer-events", "auto");
					$(".hi1").on('click', function(){
		 				$(".hidden").removeClass("hidden");
		 				$(".hi1").addClass("hidden");
						$(".exercise1").animate({
							right: "0%"
						},2000);
					});
			break;
			case 5:
				btnslClk();
				randomize(".optionsdiv");
				$(".exercise2").css("right","-51%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
					$(".exercise2").animate({
						right:'0%'
					},2000);
				// });
			break;
			case 6:
				progressPage('#numbers_svg', 'question', 1);
			break;
	 		case 7:
				$(".fulLineINstrn").css("top","5%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
					$(".fulLineINstrn").animate({
						top: "20%"
					},500);
					$(".sick_boy").css("pointer-events", "auto");
			 			$(".sick_boy").on('click', function(){
			 				$(".mag_glass").removeClass("hidden").addClass("fadein");
			 			});
				// });
			$nextBtn.show(0);
			break;
			case 8:
				corrCount = 0;
				btnslClk();
				randomize(".optionsdiv");
			break;
			case 9:
				btnslClk();
				hintClick();
				randomize(".optionsdiv");
				$(".exercise1").css("right", "-50%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on('complete', function(){
					$(".exercise1").animate({
						right:"0%"
					},2000);
					$(".btmHint").animate({
						right: "10%"
					},2000);
				// });
			break;
			case 10:
				progressPage('#numbers_svg', 'question', 2);
			break;
			case 11:
					$(".fulLineINstrn").css("top","5%");
					createjs.Sound.stop();
					current_sound = createjs.Sound.play("sound_1");
					current_sound.play();
					// current_sound.on("complete",function(){
						$(".fulLineINstrn").animate({
							top: "20%"
						},500);
						$(".sick_boy").css("pointer-events", "auto");
						$(".pig").on('click', function(){
							$(".mag_glass").removeClass("hidden").addClass("fadein");
						});
					// });
			$nextBtn.show(0);
			break;
			case 12:
				corrCount = 0;
				imgopnClk();
			break;
			case 13:
				btnslClk();
				hintClick();
				randomize(".optionsdiv_sec");
				$(".fulLineINstrn").css("top","5%");
				$(".btmHint").css("right", "10%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
						$(".fulLineINstrn").animate({
							top: "20%"
						},500);
					// });

			break;
			case 14:
				progressPage('#numbers_svg', 'question', 3);
			break;
			case 15:
				corrCount = 0;
				randomize(".optionsdiv_sec");
				btnslClk();
			break;
	 		case 16:
				btnslClk();
				hintClick();
				randomize(".optionsdiv_sec");
				$(".exercise4").css("right","-51%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
					$(".exercise4").animate({
						right:'0%'
					},2000);
					$(".btmHint").animate({
						right: "10%"
					},2000);
				// });

			break;
			case 17:
				progressPage('#numbers_svg', 'question', 4);
			break;
			case 18:
				corrCount = 0;
				dragdrop();
				$(".fulLineINstrn").css("top","5%");
				$(".sidedrop").css("right","0%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
						$(".fulLineINstrn").animate({
							top: "20%"
						},500);
						createjs.Sound.stop();
						current_sound_1= createjs.Sound.play("sound_1");
						current_sound_1.play();
						current_sound_1.on('complete', function(){
							$(".sidedrop").animate({
								right:"0%"
							},1000);
						});
					// });
			break;
			case 19:
				progressPage('#numbers_svg', 'question', 5);
			break;
			case 20:
				$(".submarine").animate({
					"left" : "40%"
				},7000);
				$nextBtn.show(0);
			break;
			case 21:
				corrCount = 0;
				$(".btnslSec").removeClass("buttonsel");
				randomize(".optionsdivbox");
				btnslclkSec();
				break;
			case 22:
				$(".btnslSec").removeClass("buttonsel");
				randomize(".optionsdivbox");
				btnslclkSec();
			break;
			case 23:
				randomize(".optionsdiv");
				btnslClk();
				$(".fulLineINstrn").css("top","5%");
				$(".exercise2").css("right","0%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
						$(".fulLineINstrn").animate({
							top: "20%"
						},500);
						createjs.Sound.stop();
						current_sound_1 = createjs.Sound.play("sound_1");
						current_sound_1.play();
						current_sound_1.on("complete", function(){
							$(".exercise2").animate({
								right:'0%'
							},2000);
						});
					// });
			break;
			case 24:
				progressPage('#numbers_svg', 'question', 6);
	 		break;
			case 25:
				corrCount = 0;
				dragdrop();
				$(".fulLineINstrn").css("top","5%");
				$(".sidedrop").css("right","-58%");
				createjs.Sound.stop();
				current_sound = createjs.Sound.play("sound_1");
				current_sound.play();
				// current_sound.on("complete",function(){
						$(".fulLineINstrn").animate({
							top: "20%"
						},500);
						createjs.Sound.stop();
						current_sound_1= createjs.Sound.play("sound_1");
						current_sound_1.play();
						// current_sound_1.on('complete', function(){
							$(".sidedrop").animate({
								right:"0%"
							},1000);
						// });
					// });
			break;
			case 26:
					$('.btmTwnty').css({"top":"0","height":"100%"});
					progressPage('#numbers_svg', 'question', 7);
					$nextBtn.show(0);
			break;
	 	}
		function hintClick(){
			$(".btmHint").on('click', function(){
				$(".btmHint").addClass("hidden");
				$(".hintblock").removeClass("hidden");
			});
			$(".cross").on('click', function(){
				$(".btmHint").removeClass("hidden");
				$(".hintblock").addClass("hidden");
			});
		}
	function progressPage(svgId, picId, maxNumber){
			var next = maxNumber+1;
			var s = Snap(svgId);
			var coins = [];
			var svg = Snap.load(preload.getResult(picId).src, function(loadedFragment){
				s.append(loadedFragment);
				for(var i=1; i<=4; i++){
					Snap.select('#q'+i+'c1').attr({'display':'none'});
					Snap.select('#q'+i+'c2').attr({'display':'none'});
				}
				for(var i=1; i<=3; i++){
					Snap.select('#q6c'+i).attr({'display':'none'});;
				}
				Snap.select('#q5c1').attr({'display':'none'});
				Snap.select('#q7c1').attr({'display':'none'});

				for (var i=1; i<=8; i++){
					$("#q"+i).css('filter','grayscale(100%)');
				}
				for( var i = 1; i<= maxNumber; i++){
					$('#q'+i+'c').children().css("display","block");
					$('#q'+i+'c').children().children().children().css("filter","grayscale(100%)");
					for(var j = 1; j<=corCountMap[i]; j++){
						$('#q'+i+'c'+j).children().children().css("filter","grayscale(0%)");
					}
				}
				var $elem = Snap.select('#q'+next);
				$elem.addClass('scale_eff');
				$elem.click(function(){
					// alert("clicled");
						countNext++;
						testin.gotoNext();
						templateCaller();
				});
			});
		}
	function btnslClk(count){
			$(".buttonsel").on("click", function(){
				if($(this).hasClass("correct")){
					play_correct_incorrect_sound(1);
					$(this).siblings(".corctopt").show(0);
					$(this).css({
						"background" : "#99C743",
						"border" : "5px solid #DFEF82",
						"color" : "#fff"
					});
					$('.buttonsel').css("pointer-events", "none");
					$nextBtn.show(0);
					checkClick = true;
					corrCount+=1;
					var mapCount = countNext ==  8?mapCount = 2:countNext == 9?mapCount = 2:
												 countNext == 13?mapCount = 3:
												 countNext == 15?mapCount = 4:countNext == 16?mapCount = 4:
 												 countNext == 23?mapCount = 6:1;
					corCountMap[mapCount] = corrCount;
				}
				else{
					play_correct_incorrect_sound(0);
					checkClick == true?corrCount--:corrCount;
					checkClick = false;
					$(this).siblings(".wrngopt").show(0);
					$(this).css({
						"background" : "#FF0000",
						"border" : "5px solid #990000",
						"color" : "#fff"
					});
				}
			});
		}


	function imgopnClk(){
			$(".imgopn").on("click", function(){
				if($(this).hasClass("correct")){
					play_correct_incorrect_sound(1);
					$(this).children(".corctopt").show(0);
					$(".imgopn").css("pointer-events", "none");
					checkClick = true;
					corrCount++;
					corCountMap[3] = corrCount;
					$nextBtn.show(0);
				}
				else{
					play_correct_incorrect_sound(0);
					$(this).children(".wrngopt").show(0);
					$(this).css("pointer-events", "none");
					checkClick == true?corrCount--:corrCount;
					checkClick = false;
				}
			});
		}

	function dragdrop(){
			$(".draggable").draggable({
				cursor: "all-scroll",
				revert: true,
			});

			$(".dropPlace").droppable({
				accept : ".draggable",
				hoverClass: "hovered",
				drop: function(event, ui){
					handleDrop(event, ui, $(this), $(".draggable"))
				}
			});
		}
	var dragCorCount = 0;
	function handleDrop(event, ui, dropclass, draggable){
		var dragAns = ui.draggable.attr("data-answer").toString().trim();
		var dropAns = $(dropclass).children().attr("data-answer").toString().trim();
		var textToShow = ui.draggable.text();
		if(dragAns == dropAns || !$('.ui-draggable-dragging').hasClass('d3') ){
			dragCorCount+=1;
			$(dropclass).css({
					"background" : "#99C743",
					"border" : "5px solid #DFEF82",
					"color" : "#fff",
					"padding": "1%"
			});

			$(dropclass).children().html(textToShow);
			play_correct_incorrect_sound(1);
			ui.draggable.hide(0);
			switch (countNext) {
				case 18:
					if(dragCorCount == 2)
						{
							checkClick = true;
							corrCount+=1;
							corCountMap[5] = corrCount;
							$nextBtn.show(0);
						}
				break;
				case 25:
					checkClick = true;
					corrCount+=1;
					corCountMap[7] = corrCount;
					$nextBtn.show(0);
				break;
				default:

			}
		}
	else{
		play_correct_incorrect_sound(0);
		checkClick == true?corrCount--:corrCount;
		checkClick = false;
	}
	}

	var hydraCorCount = 0;
	function btnslclkSec(){
			$(".btnslSec").on("click", function(){
				if($(this).hasClass("correct") || $(this).hasClass("correct1")){
					if($(this).hasClass("correct")){
						$(".hydra_top").removeClass("grayscaled");
						$(this).siblings(".corctopt").show(0);
						hydraCorCount++;
					}
					else{
						$(".hydra_buttom").removeClass("grayscaled");
						$(this).siblings(".corctopt").show(0);
						hydraCorCount++;
					}
					$(this).css({
						"background" : "#99C743",
						"border" : "5px solid #DFEF82",
						"color" : "#fff",
						"pointer-events" : "none"
					});
					play_correct_incorrect_sound(1);
					switch (countNext) {
						case 21:
							if(hydraCorCount == 2){
								$nextBtn.show(0);
								$(".btnslSec").css("pointer-events", "none");
								testin.update(true);
								checkClick = true;
								corrCount+=1;
								corCountMap[6] = corrCount;
							}
						break;
						case 22:
							$nextBtn.show(0);
							$(".btnslSec").css("pointer-events", "none");
							testin.update(true);
							checkClick = true;
							corrCount+=1;
							corCountMap[6] = corrCount;
						break;
						default:

					}
				}
				else{
				$(this).siblings(".wrngopt").show(0);
					$(this).css({
						"background" : "#FF0000",
						"border" : "5px solid #990000",
						"color" : "#fff",
						"pointer-events" : "none"
					});
					play_correct_incorrect_sound(0);
					checkClick == true?corrCount--:corrCount;
					checkClick = false;
				}
			});
		}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	function put_image_sec(content, count){
		if(content[count].hasOwnProperty('imgtxtblock')){
			var imageblock = content[count].imgtxtblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					// console.log(selector);
					$(selector).attr('src',image_src);
				}
			}
		}
	}
	function put_image_third(content, count){
		if(content[count].hasOwnProperty('hintblock')){
			var imageClass = content[count].hintblock;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				// console.log(selector);
				$(selector).attr('src',image_src);
			}

		}
	}
	function put_image_frth(content, count){
		if(content[count].hasOwnProperty('imgoptnblk')){
			var imageClass = content[count].imgoptnblk;
			for(var i=0; i<imageClass.length; i++){
				var image_src = preload.getResult(imageClass[i].imgid).src;
				//get list of classes
				var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				// console.log(selector);
				$(selector).attr('src',image_src);
			}

		}
	}
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
		loadTimelineProgress($total_page,countNext+1);
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators


	}

	// first call to template caller
	// templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

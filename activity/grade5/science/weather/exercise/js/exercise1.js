var imgpath = $ref+"/exercise/images/";

var soundAsset = $ref + "/sounds/"+$lang+"/";

var sound1 = new buzz.sound(soundAsset + "click.ogg");
var content=[
	//slide 0
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et2,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'correct options a c',
			imgsrc:imgpath + 'q1a.jpg'
		},
		{
			imgclass:'incorrect options b d',
			imgsrc:imgpath + 'q1b.jpg'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 1
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et3,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'correct options a c',
			imgsrc:imgpath + 'q2a.jpg'
		},
		{
			imgclass:'incorrect options b d',
			imgsrc:imgpath + 'q2b.jpg'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 2
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et4,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'correct options a c',
			imgsrc:imgpath + 'snowy01.gif'
		},
		{
			imgclass:'incorrect options b d',
			imgsrc:imgpath + 'sunney_gif.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 3
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et5,
			textclass : 'changingtext',
		},{
			textclass:'correct options a text1 c',
			textdata:data.string.et12,
		},
		{
			textclass:'incorrect options b text2 d',
			textdata:data.string.et13,
		},
		],
		imageblock:[
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},


	//slide 4
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et6,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'incorrect options a c',
			imgsrc:imgpath + 'rainy.gif'
		},
		{
			imgclass:'correct options b d',
			imgsrc:imgpath + 'sunney_gif.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 5
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et7,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'incorrect options a c',
			imgsrc:imgpath + 'cloudy.gif'
		},
		{
			imgclass:'correct options b d',
			imgsrc:imgpath + 'fog.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 6
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et8,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'correct options a c',
			imgsrc:imgpath + 'wind_direction.png'
		},
		{
			imgclass:'incorrect options b d',
			imgsrc:imgpath + 'sunney_gif.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},

	//slide 7
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et9,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'incorrect options a c',
			imgsrc:imgpath + 'fog.gif'
		},
		{
			imgclass:'correct options b d',
			imgsrc:imgpath + 'rainy.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},


	//slide 8
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et10,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'incorrect options a c',
			imgsrc:imgpath + 'low.gif'
		},
		{
			imgclass:'correct options b d',
			imgsrc:imgpath + 'high.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},


	//slide 9
	{
		contentblockadditionalclass:'bg',
		extratextblock : [
		{
			textdata : data.string.et1,
			textclass : 'maininstruction',
		},
		{
			textdata : data.string.et11,
			textclass : 'changingtext',
		}
		],
		imageblock:[{
			imgclass:'incorrect options a c',
			imgsrc:imgpath + 'sunney_gif.gif'
		},
		{
			imgclass:'correct options b d',
			imgsrc:imgpath + 'fog.gif'
		},
		{
			imgclass:'righticon1 a',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon1 a',
			imgsrc:imgpath + 'wrong.png'
		},
		{
			imgclass:'righticon2 b',
			imgsrc:imgpath + 'correct.png'
		},
		{
			imgclass:'wrongicon2 b',
			imgsrc:imgpath + 'wrong.png'
		}]
	},
];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext==0){
			sound1.play();
		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$(".options").click(function(){
			if($(this).hasClass("correct")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				if($(this).hasClass('a')){
					$('.a.righticon1').css({'display':'block'});
				}
				if($(this).hasClass('b')){
					$('.b.righticon2').css({'display':'block'});
				}
				$(".options").css('pointer-events', 'none');
				play_correct_incorrect_sound(1);
				$(this).css({
					'border': '.4em solid rgb(56,142,55)',
				});
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				if($(this).hasClass('a')){
					$('.a.wrongicon1').css({'display':'block'});
				}
				if($(this).hasClass('b')){
					$('.b.wrongicon2').css({'display':'block'});
				}
				$(this).css({
					'border': '.4em solid rgb(182,77,4)',
					'pointer-events': 'none',
				});
				wrong_clicked = true;
				play_correct_incorrect_sound(0);
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata: data.lesson.chapter,
			textclass: "lesson-title",
		}]
	},
	// slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text2,
			textclass: "bottomtext",
		},
		{
			textdata:  data.string.p1text2a,
			textclass: "clickmetext",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg1",
					imgsrc : '',
					imgid : 'bg-1'
				},
				{
					imgclass : "sun",
					imgsrc : '',
					imgid : 'sun'
				}
			]
		}]

	},

	// slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text3,
			textclass: "bottomtext",
		},
		{
			textdata:  data.string.p1text2a,
			textclass: "clickmetext",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg1",
					imgsrc : '',
					imgid : 'cloudy'
				},
			]
		}]

	},

	// slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text4,
			textclass: "bottomtext",
		},{
			textdata:  '',
			textclass: "snowy",
		},
		{
			textdata:  data.string.p1text2a,
			textclass: "clickmetext",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg2",
					imgsrc : '',
					imgid : 'bg_for_snowy'
				}
			]
		}]

	},
	// slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text5,
			textclass: "bottomtext",
		},
		{
			textdata:  data.string.p1text2a,
			textclass: "clickmetext",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg1",
					imgsrc : '',
					imgid : 'rainy'
				},
			]
		}]

	},

	// slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text1,
			textclass: "toptext",
		},{
			textdata:  data.string.p1text5a,
			textclass: "bottomtext",
		},
		{
			textdata:  data.string.p1text2a,
			textclass: "clickmetext",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "bg1",
					imgsrc : '',
					imgid : 'fogy'
				},
			]
		}]

	},

	// slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[{
			textdata:  data.string.p1text6,
			textclass: "toptext-a",
		},{
			textdata:  data.string.p1text2,
			textclass: "text-1",
		},{
			textdata:  data.string.p1text3,
			textclass: "text-2",
		},{
			textdata:  data.string.p1text4,
			textclass: "text-3",
		},{
			textdata:  data.string.p1text5,
			textclass: "text-4",
		},{
			textdata:  '',
			textclass: "gif",
		}],
			imageblock:[{
			imagestoshow : [
				{
					imgclass : "sunny",
					imgsrc : '',
					imgid : 'sun'
				},
				{
					imgclass : "cloudy",
					imgsrc : '',
					imgid : 'cloudy'
				},
				{
					imgclass : "rainy",
					imgsrc : '',
					imgid : 'rainy'
				},
				{
					imgclass : "snowy-a",
					imgsrc : '',
					imgid : 'bg_for_snowy'
				},
				{
					imgclass : "sunny-bg",
					imgsrc : '',
					imgid : 'bg-1'
				},
			]
		}]

	},

	// slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[
			{
				textdata:  data.lesson.chapter,
				textclass: "text-5a",
			},{
			textdata:  data.string.p1text7,
			textclass: "text-5",
			datahighlightflag:true,
			datahighlightcustomclass:'purple'
		},{
			textdata:  data.string.p1text8,
			textclass: "text-6",
			datahighlightflag:true,
			datahighlightcustomclass:'purple'
		}]

	},


	// slide8
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[
			{
				textdata:  data.lesson.chapter,
				textclass: "text-5a",
			},{
			textdata:  data.string.p1text9,
			textclass: "text-5",
			datahighlightflag:true,
			datahighlightcustomclass:'purple'
		}]

	},

	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'green_bg',

		extratextblock:[
			{
				textdata:  data.string.p1text10,
				textclass: "text-5a",
			},{
				textdata:  data.string.p1text11,
				textclass: "text-5",
				datahighlightflag:true,
				datahighlightcustomclass:'purple'
			}]

	},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg-1", src: imgpath+"bg_for_sunney_gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sunney_gif.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cloudy", src: imgpath+"cloudy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "fogy", src: imgpath+"fogy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bg_for_snowy", src: imgpath+"bg_for_snowy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "snowy", src: imgpath+"snowy.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rainy", src: imgpath+"rainy.gif", type: createjs.AbstractLoader.IMAGE},

			// soundsa
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1_answer.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s2_answer.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s3_answer.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s4_answer.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s5_answer.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_11", src: soundAsset+"p1_s9.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_card_image(content, countNext);
		vocabcontroller.findwords(countNext);
		switch(countNext) {
			case 0:
				sound_player('sound_1');
				nav_button_controls(100);
				break;
			case 1:
				sound_player('sound_3');
				showanswer('sound_2');
				break;
			case 2:
				showanswer('sound_4');
				break;
			case 3:
				showanswer('sound_5');
				break;
			case 4:
				showanswer('sound_6');
			break;
			case 5:
				showanswer('sound_7');
					break;
			case 6:
				sound_player('sound_8');
				nav_button_controls(100);
				break;
			case 7:
				sound_player('sound_9');
				nav_button_controls(100);
				break;
			case 8:
				sound_player('sound_10');
				nav_button_controls(100);
				break;
			case 9:
				sound_player('sound_11');
				nav_button_controls(100);
				break;
			default:
				$prevBtn.show(0);
				// sound_player('sound_2');
				nav_button_controls(100);
				break;
		}
	}
	function showanswer(soundid){
		$('.clickmetext').click(function(){
			$(this).fadeOut(1000);
			sound_player(soundid);
			// $(this).addClass('fadeout');
			nav_button_controls(100);
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			// nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_card_image(content, count){
		if(content[count].hasOwnProperty('card')){
			var card = content[count].card;
			for(var i=0; i<card.length; i++){
				var image_src = preload.getResult(card[i].imgid).src;
				console.log(image_src);
				var classes_list = card[i].imgclass.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]);
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}
	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

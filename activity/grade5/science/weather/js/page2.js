var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";


var content=[
	// slide0
	{
		contentblockadditionalclass:'green_bg',
		extratextblock:[{
			textdata:  data.string.p2text1,
			textclass: "page2_text1",
		},{
			textdata:  data.string.p2text2,
			textclass: "page2_text2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "desc_image",
					imgsrc : '',
					imgid : 'windtree'
				},
			]
		}],
	},

	// slide1
	{
		contentblockadditionalclass:'green_bg',
		extratextblock:[{
			textdata:  data.string.p2text3,
			textclass: "page2_text1",
		},{
			textdata:  data.string.p2text4,
			textclass: "page2_text2",
		},{
			textdata:  data.string.p2text5,
			textclass: "page2_text3",
		},{
			textdata:  data.string.p2text6,
			textclass: "page2_text4",
		},{
			textdata:  data.string.p2text7,
			textclass: "page2_text5",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "desc_image1",
					imgsrc : '',
					imgid : 'low'
				},
				{
					imgclass : "desc_image2",
					imgsrc : '',
					imgid : 'medium'
				},
				{
					imgclass : "desc_image3",
					imgsrc : '',
					imgid : 'high'
				},
			]
		}],
	},

	// slide2
	{
		contentblockadditionalclass:'green_bg',
		extratextblock:[{
			textdata:  data.string.p2text8,
			textclass: "page2_text1",
		},{
			textdata:  data.string.p2text9,
			textclass: "page2_text2",
		},{
			textdata:  data.string.p2text10,
			textclass: "page2_text3",
		},{
			textdata:  data.string.p2text11,
			textclass: "page2_text4",
		},{
			textdata:  data.string.p2text12,
			textclass: "page2_text5",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "desc_image1",
					imgsrc : '',
					imgid : 'rain'
				},
				{
					imgclass : "desc_image2a",
					imgsrc : '',
					imgid : 'hailstones'
				},
				{
					imgclass : "desc_image3",
					imgsrc : '',
					imgid : 'snow'
				},
			]
		}],
	},


		// slide3
		{
			contentblockadditionalclass:'green_bg',
			extratextblock:[{
				textdata:  data.string.p2text13,
				textclass: "page2_text1",
			},{
				textdata:  data.string.p2text14,
				textclass: "page2_text2",
			},{
				textdata:  data.string.p2text15,
				textclass: "page2_text3",
			},{
				textdata:  data.string.p2text16,
				textclass: "page2_text4",
			},{
				textdata:  data.string.p2text17,
				textclass: "page2_text5",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "desc_image1b",
						imgsrc : '',
						imgid : '15digree'
					},
					{
						imgclass : "desc_image2b",
						imgsrc : '',
						imgid : '25digree'
					},
					{
						imgclass : "desc_image3b",
						imgsrc : '',
						imgid : '35digree'
					},
				]
			}],
		},

		// slide4
		{
			contentblockadditionalclass:'green_bg',
			extratextblock:[{
				textdata:  data.string.p2text18,
				textclass: "page2_text1",
			},{
				textdata:  data.string.p2text19,
				textclass: "page2_text2",
			},{
				textdata:  data.string.p2text20,
				textclass: "page2_text3a",
			},{
				textdata:  data.string.p2text21,
				textclass: "page2_text4a",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "desc_image4",
						imgsrc : '',
						imgid : 'morning'
					},
					{
						imgclass : "desc_image5",
						imgsrc : '',
						imgid : 'midday'
					}
				]
			}],
		},

		// slide5
		{
			contentblockadditionalclass:'green_bg',
			extratextblock:[{
				textdata:  data.string.p2text22,
				textclass: "page2_text1",
			},{
				textdata:  data.string.p2text23,
				textclass: "page2_text2",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "desc_image",
						imgsrc : '',
						imgid : 'fog'
					},
				]
			}],
		},

		// slide6
		{
			contentblockadditionalclass:'green_bg',
			extratextblock:[{
				textdata:  data.string.p2text24,
				textclass: "page2_text1",
			},{
				textdata:  data.string.p2text25,
				textclass: "page2_text2",
			}],
			imageblock:[{
				imagestoshow : [
					{
						imgclass : "desc_image",
						imgsrc : '',
						imgid : 'clouds'
					},
				]
			}],
		},

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "windtree", src: imgpath+"wind_direction.png", type: createjs.AbstractLoader.IMAGE},
			{id: "high", src: imgpath+"high.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "medium", src: imgpath+"medium.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "low", src: imgpath+"low.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "rain", src: imgpath+"rain.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "hailstones", src: imgpath+"hailstones.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "snow", src: imgpath+"snow.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "15digree", src: imgpath+"15digree.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "25digree", src: imgpath+"25digree.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "35digree", src: imgpath+"35digree.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "morning", src: imgpath+"morning.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "midday", src: imgpath+"midday.jpg", type: createjs.AbstractLoader.IMAGE},
			{id: "fog", src: imgpath+"fog.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "clouds", src: imgpath+"clouds.jpg", type: createjs.AbstractLoader.IMAGE},
			//textboxes

			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p2_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p2_s6.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		// call main function
		// createjs.Sound.play('para-1');
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_poem_image(content, countNext);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
					case 0:
						sound_player('sound_1');
						break;
					case 1:
						sound_player('sound_2');
						break;
					case 2:
						sound_player('sound_3');
						break;
					case 3:
						sound_player('sound_4');
						break;
					case 4:
						sound_player('sound_5');
						break;
					case 5:
						sound_player('sound_6');
						break;
					case 6:
						sound_player('sound_7');
						break;
					default:
					nav_button_controls(200);
					break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}


	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_poem_image(content, count){
		if(content[count].hasOwnProperty('poemimage')){
			var imageblock = content[count].poemimage[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

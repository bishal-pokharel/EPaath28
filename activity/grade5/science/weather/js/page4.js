var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";
var name;
var date;
var time;
var address;
var temp;
var weather;
var clouds;
var content=[
	// slide0
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.diytext,
			textclass: "title-1",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "weather",
					imgsrc : '',
					imgid : 'weathericons'
				}
			]
		}]
	},
	//slide1
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text1,
			textclass: "title-2",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}]
	},

	//slide2
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text2,
			textclass: "title-2",
		},
		{
			textdata:  data.string.button,
			textclass: "submitbutton",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		inputbox:[{
			inputclass:'name inputposition',
			textdata:data.string.nameclue
		}]
	},
	//slide3
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text3,
			textclass: "title-2",
		},
		{
			textdata:  data.string.button,
			textclass: "submitbutton",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		inputbox:[{
			inputclass:'date inputposition',
			textdata:data.string.dateclue
		}]
	},

	//slide4
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text4,
			textclass: "title-2",
		},
		{
			textdata:  data.string.button,
			textclass: "submitbutton",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		inputbox:[{
			inputclass:'time inputposition',
			textdata:data.string.timeclue
		}]
	},

	//slide5
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text5,
			textclass: "title-2",
		},
		{
			textdata:  data.string.button,
			textclass: "submitbutton",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		inputbox:[{
			inputclass:'address inputposition',
			textdata:data.string.addressclue
		}]
	},

	//slide6
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text6,
			textclass: "title-2",
		},
		{
			textdata:  data.string.button,
			textclass: "submitbutton",
		},
		{
			textdata:  data.string.degree,
			textclass: "degree",
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}],
		inputbox:[{
			inputclass:'temp inputposition',
			textdata:data.string.temperatureclue
		}]
	},

	//slide7
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text7,
			textclass: "toptext",
		},{
			textdata:  data.string.p4text8,
			textclass: "sunny",
		},{
			textdata:  data.string.p4text9,
			textclass: "cloudy",
		},{
			textdata:  data.string.p4text10,
			textclass: "foggy",
		},{
			textdata:  data.string.p4text11,
			textclass: "snowy",
		},{
			textdata:  data.string.p4text12,
			textclass: "rainy",
		},]
	},


	//slide8
	{
		contentblockadditionalclass:'pink_bg',
		extratextblock:[{
			textdata:  data.string.p4text13,
			textclass: "title",
		}],
		imagedivs:[{
			imagedivname:'div1',
			textdata: data.string.p3text2,
			textclass: 'imagetag',
			imgclass:'imageclass',
			imgsrc:imgpath+'cirrus-cloud.jpg'
		},
		{
			imagedivname:'div2',
			textdata: data.string.p3text3,
			textclass: 'imagetag',
			imgclass:'imageclass',
			imgsrc:imgpath+'cumulus-clouds.jpg'
		},
		{
			imagedivname:'div3',
			textdata: data.string.p3text4,
			textclass: 'imagetag',
			imgclass:'imageclass',
			imgsrc:imgpath+'nimbus-cloud.jpg'
		},
		{
			imagedivname:'div4',
			textdata: data.string.p3text5,
			textclass: 'imagetag',
			imgclass:'imageclass',
			imgsrc:imgpath+'stratus-cloud.jpg'
		}]
	},

	// slide9
	{
		contentnocenteradjust: true,
		contentblockadditionalclass: 'pink_bg',

		extratextblock:[{
			textdata:  data.string.p4text14,
			textclass: "answer",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		}],
		imageblock:[{
			imagestoshow : [
				{
					imgclass : "duck",
					imgsrc : '',
					imgid : 'duck'
				}
			]
		}]
	},


];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "weathericons", src: imgpath+"weather-icons.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duckling-11.png", type: createjs.AbstractLoader.IMAGE},


			// sounds
			{id: "sound_1", src: soundAsset+"p4_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p4_s1.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			case 0:
				sound_player('sound_1');
				$nextBtn.show(0);
				break;
			case 1:
				sound_player('sound_2');
				$nextBtn.show(0);
				break;
			case 2:
				$nextBtn.hide(0);
				$('.submitbutton').hide(0);
				$('.submitbutton').click(function(){
						$(this).css('pointer-events','none');
						$nextBtn.show(0);
						name = $('.name').val();
						console.log(name);
						$('.name').css("pointer-events","none");
				});
				input_box1('.name','.submitbutton',3);
				break;
			case 3:
				$nextBtn.hide(0);
				$('.submitbutton').hide(0);
				$('.submitbutton').click(function(){
					$(this).css('pointer-events','none');
						$nextBtn.show(0);
						date = $('.date').val();
						console.log(date);
						$('.date').css("pointer-events","none");
				});
				input_box('.date','.submitbutton',3);
				break;
			case 4:
					$nextBtn.hide(0);
					$('.submitbutton').hide(0);
					$('.submitbutton').click(function(){
						$(this).css('pointer-events','none');
							$nextBtn.show(0);
							time = $('.time').val();
							console.log(time);
							$('.time').css("pointer-events","none");
					});
					input_box('.time','.submitbutton',2);
					break;
			case 5:
						$nextBtn.hide(0);
						$('.submitbutton').hide(0);
						$('.submitbutton').click(function(){
							$(this).css('pointer-events','none');
								$nextBtn.show(0);
								address = $('.address').val();
								console.log(address);
								$('.address').css("pointer-events","none");
						});
						input_box('.address','.submitbutton',3);
						break;
			case 6:
					$nextBtn.hide(0);
					$('.submitbutton').hide(0);
					$('.submitbutton').click(function(){
						$(this).css('pointer-events','none');
							$nextBtn.show(0);
							temp = $('.temp').val();
							console.log(temp);
							$('.temp').css("pointer-events","none");
					});
					input_box('.temp','.submitbutton',1);
				break;
			case 7:
				$nextBtn.hide(0);
				$('.sunny,.cloudy,.foggy,.snowy,.rainy').click(function(){
						weather=$(this).text();
						$('.sunny,.cloudy,.foggy,.snowy,.rainy').css({"pointer-events":"none"});
						$nextBtn.show(0);
				});
				break;
			case 8:
			$nextBtn.hide(0);
			$('.div1, .div2, .div3, .div4').click(function(){
					clouds=$(this).children().text();
					console.log(clouds);
					$('.div1, .div2, .div3, .div4').css({"pointer-events":"none"});
					$nextBtn.show(0);
			});
			break;
			case 9:
				$('.duck').css({"top":" 68%","width": "20%"});
				$('.name').html(name);
				$('.weather1').html(weather);
				$('.temp').html(temp);
				$('.clouds').html(clouds);
				$('.address').html(address);
				$('.time').html(time);
				$('.date').html(date);
				if(clouds=="Cirrus cloud"){
						$('.cloudtext').html(data.string.p4text17);
				}
				if(clouds=="Cumulus cloud"){
					$('.cloudtext').html(data.string.p4text15);
				}
				if(clouds=="Stratus cloud"){
					$('.cloudtext').html(data.string.p4text16);
				}
				if(clouds=="Nimbus cloud"){
					$('.cloudtext').html(data.string.p4text18);
				}
				if(clouds=="सिरस बादल"){
						$('.cloudtext').html(data.string.p4text17);
				}
				if(clouds=="क्युमुलस बादल"){
					$('.cloudtext').html(data.string.p4text15);
				}
				if(clouds=="स्ट्राटस बादल"){
					$('.cloudtext').html(data.string.p4text16);
				}
				if(clouds=="निम्बस बादल"){
					$('.cloudtext').html(data.string.p4text18);
				}
				nav_button_controls(1000);
				break;
			default:
				nav_button_controls(1000);
				break;
		}
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
	}
	// for entering the value in input box
		function input_box(input_class, button_class, input_limit) {
			$(input_class).keydown(function(event){
					var charCode = (event.which) ? event.which : event.keyCode;
					if(charCode === 13 && button_class!=null) {
							return false;
						}
			});
			$(input_class).keyup(function(event){
					if( $(this).val().length < input_limit ) {
							$(button_class).css('display','none');
					 }
	    		if (String(event.target.value).length >= input_limit) {
	    			$(button_class).show(0);
	    			global_save_val = String(event.target.value);
	    		}
	    		else{
	    		}
	  			return true;
			});
		}
		function input_box1(input_class, button_class, input_limit) {
			$(input_class).keydown(function(event){
					var charCode = (event.which) ? event.which : event.keyCode;
					if(charCode === 13 && button_class!=null) {
							return false;
						}
					if (this.selectionStart == 0 && event.keyCode >= 65 && event.keyCode <= 90 && !(event.shiftKey) && !(event.ctrlKey) && !(event.metaKey) && !(event.altKey)) {
				            var $t = $(this);
				            event.preventDefault();
				            var char = String.fromCharCode(event.keyCode);
				            $t.val(char + $t.val().slice(this.selectionEnd));
				            this.setSelectionRange(1,1);
				         }
			});
			$(input_class).keyup(function(event){
					if( $(this).val().length < input_limit ) {
							$(button_class).css('display','none');
					 }
	    		if (String(event.target.value).length >= input_limit) {
	    			$(button_class).show(0);
	    			global_save_val = String(event.target.value);
	    		}
	    		else{
	    		}
	  			return true;
			});
		}


	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	//change_src-> put 1 for girl speak first else 2 for boy-speak first, 3 for boy-3
	function conversation(class1, sound_data1, class2, sound_data2, change_src){
		$(class1).fadeIn(500, function(){
			createjs.Sound.stop();
			current_sound = createjs.Sound.play(sound_data1);
			current_sound.play();
			current_sound.on("complete", function(){
				if(change_src==1){
					$('.boy').attr('src', preload.getResult('boy-1').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				} else if(change_src==2){
					$('.boy').attr('src', preload.getResult('boy-2').src);
					$('.girl').attr('src', preload.getResult('girl-2').src);
				} else if(change_src==3){
					$('.boy').attr('src', preload.getResult('boy-3').src);
					$('.girl').attr('src', preload.getResult('girl-1').src);
				}
				$(class2).fadeIn(500, function(){
					current_sound = createjs.Sound.play(sound_data2);
					current_sound.play();
					current_sound.on("complete", function(){
						nav_button_controls(0);
						$(class1).click(function(){
							sound_player(sound_data1);
						});
						$(class2).click(function(){
							sound_player(sound_data2);
						});
					});
				});
			});
		});
	}
	function sound_player(sound_id){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
				var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
			for (var i = 0; i < content.length; i++) {
				slides(i);
				$($('.totalsequence')[i]).html(i);
				$($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
			"width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
			}
			function slides(i){
					$($('.totalsequence')[i]).click(function(){
						countNext = i;
						templateCaller();
					});
				}
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

	var content=[
		//slide 1
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e1text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "soap.png",
				containerclass : 'class1 options',
				textdata : data.string.e1ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class2 options',
				textdata : data.string.e1ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e1ansc,
				textclass : 'option-text',
			}],
		},
		//slide 2
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e2text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "nailcutter.png",
				containerclass : 'class1 options',
				textdata : data.string.e2ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hankey.png",
				containerclass : 'class2 options',
				textdata : data.string.e2ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e2ansc,
				textclass : 'option-text',
			}],
		},
		//slide 3
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e3text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "earbud.png",
				containerclass : 'class1 options',
				textdata : data.string.e3ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothpaste.png",
				containerclass : 'class2 options',
				textdata : data.string.e3ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toiletroll.png",
				containerclass : 'class3 options',
				textdata : data.string.e3ansc,
				textclass : 'option-text',
			}],
		},
		//slide 4
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e4text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class1 options',
				textdata : data.string.e4ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothpaste.png",
				containerclass : 'class2 options',
				textdata : data.string.e4ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e4ansc,
				textclass : 'option-text',
			}],
		},
		//slide 5
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e5text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class1 options',
				textdata : data.string.e5ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "shampoo.png",
				containerclass : 'class2 options',
				textdata : data.string.e5ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "nailcutter.png",
				containerclass : 'class3 options',
				textdata : data.string.e5ansc,
				textclass : 'option-text',
			}],
		},
		//slide 6
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e6text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "shoesbrush.png",
				containerclass : 'class1 options',
				textdata : data.string.e6ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class2 options',
				textdata : data.string.e6ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e6ansc,
				textclass : 'option-text',
			}],
		},
		//slide 7
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e7text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "hankey.png",
				containerclass : 'class1 options',
				textdata : data.string.e7ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "earbud.png",
				containerclass : 'class2 options',
				textdata : data.string.e7ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e7ansc,
				textclass : 'option-text',
			}],
		},
		//slide 8
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e8text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "soap.png",
				containerclass : 'class1 options',
				textdata : data.string.e8ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class2 options',
				textdata : data.string.e8ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e8ansc,
				textclass : 'option-text',
			}],
		},
		//slide 9
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e9text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "towel.png",
				containerclass : 'class1 options',
				textdata : data.string.e9ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "toothbrush.png",
				containerclass : 'class2 options',
				textdata : data.string.e9ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e9ansc,
				textclass : 'option-text',
			}],
		},
		//slide 10
		{
			contentblockadditionalclass: 'main_bg',
			extratextblock : [{
				textdata : data.string.einstruction,
				textclass : 'instruction'
			},
			{
				textdata : data.string.e10text,
				textclass : 'command_question',
			}],

			optionsblockadditionalclass: 'img_options',
			optionsblock : [{
				imgsrc : imgpath + "water.png",
				containerclass : 'class1 options',
				textdata : data.string.e10ansa,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "nailcutter.png",
				containerclass : 'class2 options',
				textdata : data.string.e10ansb,
				textclass : 'option-text',
			},
			{
				imgsrc : imgpath + "hairbrush.png",
				containerclass : 'class3 options',
				textdata : data.string.e10ansc,
				textclass : 'option-text',
			}],
		}
	];

	content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;
	var score = 0;
	var current_sound;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            // sounds
            {id: "sound_1", src: soundAsset + "ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();
    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }
	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	//add bg to rhino
	// add_bg(['bg_1.png','bg_2.png','bg_3.png']);
	// add_bg(['bg01.png','bg02.png','bg03.png']);
	// add_bg(['city_1.png','city_2.png','city_3.png']);
	var rhino = new RhinoTemplate();

	rhino.init(10);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);
        countNext==0?sound_player("sound_1"):"";
        //randomize options
		var parent = $(".optionsblock");
		var divs = parent.children();
		while (divs.length) {
	    	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		var wrong_clicked = false;
		$(".option_container").click(function(){
			if($(this).hasClass("class1")){
				if(!wrong_clicked){
					rhino.update(true);
				}
				$(".option_container").css('pointer-events','none');
				current_sound.stop();
	 			play_correct_incorrect_sound(1);
				$(this).css("background","#6AA84F");
				$(this).css("border","5px solid #B6D7A8");
				$(this).children('.cor').show(0);
				if(countNext != $total_page)
					$nextBtn.show(0);
			}
			else{
				if(!wrong_clicked){
					rhino.update(false);
				}
				$(this).children('.incor').show(0);
				$(this).css("background","#EA9999");
				$(this).css("border","5px solid #efb3b3");
				$(this).css("color","black");
                current_sound.stop();
	 			play_correct_incorrect_sound(0);
				wrong_clicked = true;
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		if(countNext < 11){
			templateCaller();
			rhino.gotoNext();
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

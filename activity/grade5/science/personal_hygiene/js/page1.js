var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p1_s2.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p1_s3.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p1_s8.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p1_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p1_s6.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p1_s7.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p1_s4.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p1_s9.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p1_s10.ogg"));
var sound_12 = new buzz.sound((soundAsset + "p1_s11.ogg"));
var sound_13 = new buzz.sound((soundAsset + "p1_s12.ogg"));
var current_sound = sound_1;

var sound_arr_var = [sound_1,sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11, sound_12];


var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'center-text',
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title'
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'bg_full',
					imgsrc: imgpath + "class.png",
				}
			]
		}]
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'green_bg',

		extratextblock : [{
			textdata : data.string.p1text1,
			textclass : 'my_font_very_big title-text proximanova'
		},
		{
			textdata : data.string.p1text2,
			textclass : 'my_font_very_big lets-look proximanova'
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'center-image-large',
					imgsrc: imgpath + "class.png",
				}
			]
		}]
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "bath.png",
				}
			]
		}]
	},

	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "brush.png",
				}
			]
		}]
	},

	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "hand.png",
				}
			]
		}]
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "nail.png",
				}
			]
		}]
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "hair.png",
				}
			]
		}]
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			},
			{
				textdata : data.string.rule6,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image clothes-image',
					imgsrc: imgpath + "clothes.png",
				}
			]
		}]
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			},
			{
				textdata : data.string.rule6,
				textclass : ''
			},
			{
				textdata : data.string.rule7,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "ear.png",
				}
			]
		}]
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			},
			{
				textdata : data.string.rule6,
				textclass : ''
			},
			{
				textdata : data.string.rule7,
				textclass : ''
			},
			{
				textdata : data.string.rule8,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image garbage-image',
					imgsrc: imgpath + "garbage.png",
				}
			]
		}]
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			},
			{
				textdata : data.string.rule6,
				textclass : ''
			},
			{
				textdata : data.string.rule7,
				textclass : ''
			},
			{
				textdata : data.string.rule8,
				textclass : ''
			},
			{
				textdata : data.string.rule9,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "hand.png",
				}
			]
		}]
	},

	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'blackboard',

		extratextblock : [{
			textdata : data.string.p1text3,
			textclass : 'my_font_very_big rules-title proximanova'
		}],

		listblockadditionalclass: 'rules-list my_font_medium proximanova',
		listitem:[
			{
				textdata : data.string.rule1,
				textclass : ''
			},
			{
				textdata : data.string.rule2,
				textclass : ''
			},
			{
				textdata : data.string.rule3,
				textclass : ''
			},
			{
				textdata : data.string.rule4,
				textclass : ''
			},
			{
				textdata : data.string.rule5,
				textclass : ''
			},
			{
				textdata : data.string.rule6,
				textclass : ''
			},
			{
				textdata : data.string.rule7,
				textclass : ''
			},
			{
				textdata : data.string.rule8,
				textclass : ''
			},
			{
				textdata : data.string.rule9,
				textclass : ''
			},
			{
				textdata : data.string.rule10,
				textclass : ''
			}
		],

		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'rule-image',
					imgsrc: imgpath + "leg.png",
				}
			]
		}]
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var timeoutvar = null;
	var current_char = null;
	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
		case 0:
			sound_nav(sound_arr_var[countNext]);
			break;
		case 1:
			$prevBtn.show(0);
			sound_nav(sound_arr_var[countNext]);
			break;
		case 2:
			$('.listblock').hide(0).delay(3000).fadeIn(500);
			sound_nav(sound_arr_var[countNext]);
			break;
		default:
			$prevBtn.show(0);
			sound_nav(sound_arr_var[countNext]);
			break;
		}
	}

	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		current_sound.stop();
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

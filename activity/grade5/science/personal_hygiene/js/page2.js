var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var sound_1 = new buzz.sound((soundAsset + "p2_s0.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p2_s1.ogg"));
var sound_2_1 = new buzz.sound((soundAsset + "p2_s1_1.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p2_s2.ogg"));
var sound_3_1 = new buzz.sound((soundAsset + "p2_s2_1.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p2_s3.ogg"));
var sound_4_1 = new buzz.sound((soundAsset + "p2_s3_1.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p2_s4.ogg"));
var sound_5_1 = new buzz.sound((soundAsset + "p2_s4_1.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p2_s5.ogg"));
var sound_6_1 = new buzz.sound((soundAsset + "p2_s5_1.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p2_s6.ogg"));
var sound_7_1 = new buzz.sound((soundAsset + "p2_s6_1.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p2_s7.ogg"));
var sound_8_1 = new buzz.sound((soundAsset + "p2_s7_1.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p2_s8.ogg"));
var sound_9_1 = new buzz.sound((soundAsset + "p2_s8_1.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p2_s9.ogg"));
var sound_11 = new buzz.sound((soundAsset + "positive.ogg"));
var sound_12 = new buzz.sound((soundAsset + "negative.ogg"));
var current_sound = sound_1;

var sound_arr_var = [sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10];
var sound_arr_var2 = [sound_1, sound_2_1, sound_3_1, sound_4_1, sound_5_1, sound_6_1, sound_7_1, sound_8_1, sound_9_1, sound_10];


var content = [

	//slide0
	// {
		// hasheaderblock : false,
		// contentblocknocenteradjust : true,
		// contentblockadditionalclass : '',
//
		// uppertextblockadditionalclass: 'header-text-2',
		// uppertextblock : [{
			// textdata : data.string.p2text1,
			// textclass : 'my_font_very_big proximanova'
		// }],
//
		// extratextblock : [{
			// isoption : true,
			// textdata : data.string.pgood,
			// textclass : 'my_font_very_big option happymonkey opt-1'
		// },
		// {
			// isoption : true,
			// textdata : data.string.pbad,
			// textclass : 'my_font_very_big option happymonkey opt-2'
		// }],
	// },
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'center-text-2',
		uppertextblock : [{
			textdata : data.string.p2text2,
			textclass : 'my_font_big proximanova'
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text3,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pno,
			textclass : 'my_font_very_big option happymonkey opt-2'
		},
		{
			textdata : data.string.p2text4,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text5,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes10,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pno10,
			textclass : 'my_font_very_big option happymonkey opt-2'
		},
		{
			textdata : data.string.p2text6,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text7,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes1,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno1,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		},
		{
			textdata : data.string.p2text8,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text9,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes2,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno2,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		},
		{
			textdata : data.string.p2text10,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text11,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes11,
			textclass : 'my_font_very_big option happymonkey opt-1'
		},
		{
			isoption : true,
			textdata : data.string.pno11,
			textclass : 'my_font_very_big option happymonkey opt-2'
		},
		{
			textdata : data.string.p2text12,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text13,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes3,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno3,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		},
		{
			textdata : data.string.p2text14,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text15,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes3,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno3,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		},
		{
			textdata : data.string.p2text16,
			textclass : 'feedback slide-in my_font_big'
		}],
	},
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'question-text pangolin',
		uppertextblock : [{
			textdata : data.string.p2text17,
			textclass : 'my_font_very_big'
		}],

		extratextblock : [{
			isoption : true,
			textdata : data.string.pyes4,
			textclass : 'my_font_very_big option happymonkey opt-1 option-l0'
		},
		{
			isoption : true,
			textdata : data.string.pno4,
			textclass : 'my_font_very_big option happymonkey opt-2 option-l0'
		},
		{
			textdata : data.string.p2text18,
			textclass : 'feedback slide-in my_font_big'
		}],
	},

	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',

		uppertextblockadditionalclass: 'center-text-2',
		uppertextblock : [{
			textdata : data.string.p2text19,
			textclass : 'my_font_big proximanova'
		}],
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		imageblock : [{
			imagestoshow : [
				{
					imgclass: 'squirrel',
					imgsrc: imgpath + "squirrel.png",
				}
			]
		}],
		listblock : [{
			listblockadditionalclass: 'positive-list slide-down',
			textdata : data.string.p2text20,
			textclass : 'my_font_big proximanova',
			listitem: [

			],
		},
		{
			listblockadditionalclass: 'negative-list slide-down',
			textdata : data.string.p2text21,
			textclass : 'my_font_big proximanova',
			listitem: [

			],
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;

	var timeoutvar = null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var count_arr = [-1,-1,-1,-1,-1,-1,-1,-1];
	var good_arr = [
					data.string.good1,
					data.string.good2,
					data.string.good3,
					data.string.good4,
					data.string.good5,
					data.string.good6,
					data.string.good7,
					data.string.good8,
					];
	var bad_arr = [
					data.string.bad1,
					data.string.bad2,
					data.string.bad3,
					data.string.bad4,
					data.string.bad5,
					data.string.bad6,
					data.string.bad7,
					data.string.bad8,
					];

	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);

		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch (countNext) {
			// case 0:
				// $('.option').click(function(){
					// $('.option').removeClass('selected-option');
					// $(this).addClass('selected-option');
					// $nextBtn.show(0);
				// });
				// break;
			case 0:
				sound_nav(sound_1);
				break;
			case 9:
				$prevBtn.show(0);
				sound_nav(sound_10);
				break;
			case 10:
				var pos_count = 0;
				sound_player(sound_11);
				// count_arr = [0,0,0,0,1,1,1,1];
				nav_button_controls(15000);
				for( var i =0; i<count_arr.length; i++){
					if(count_arr[i]==1){
						$('.positive-list>ol').html($('.positive-list>ol').html()+'<li>'+good_arr[i]+'</li>');
						pos_count++;
					} else if(count_arr[i]==0){
						$('.negative-list>ol').html($('.negative-list>ol').html()+'<li>'+bad_arr[i]+'</li>');
					}
				}

				$('.positive-list').show(0);
				var m = 0;
				while(m <= count_arr.length ){
					if(m<pos_count){
						$('.positive-list li').eq(m).delay(1500+m*1500).fadeIn(1000);
					} else if(m==pos_count){
						$('.negative-list').delay(4000+m*1500).show(0, function(){
							sound_player(sound_12);
							$('.squirrel').css('transform', 'scaleX(-1)');
						});
					} else {
						$('.negative-list li').eq(m-(pos_count+1)).delay(1500+m*1500).fadeIn(1000);
					}
					m++;
				}
				break;
				
			default:
				$prevBtn.show(0);
				sound_player(sound_arr_var[countNext]);
				if(count_arr[countNext-1]==1){
					$('.opt-1').addClass('selected-option');
					$('.feedback').show(0);
					$nextBtn.show(0);
				} else if(count_arr[countNext-1]==0){
					$('.opt-2').addClass('selected-option');
					$('.feedback').show(0);
					$nextBtn.show(0);
				}
				$('.option').click(function(){
					$('.option').removeClass('selected-option');
					$(this).addClass('selected-option');
					if( $(this).hasClass('opt-1')){
						count_arr[countNext-1] = 1;
					} else{
						count_arr[countNext-1] = 0;
					}
					sound_nav(sound_arr_var2[countNext]);
					$('.feedback').show(0);
				});
				break;
		}
	}



	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	function sound_nav(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
		current_sound.bindOnce('ended', function(){
			nav_button_controls(0);
		});
	}

	function templateCaller() {
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;
	templateCaller();

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

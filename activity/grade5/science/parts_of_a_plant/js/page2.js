var imgpath = $ref + "/images/";
var soundAsset;
if($lang == "en"){
	soundAsset = $ref+"/sound/en/p2/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/p2/";
}
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");
var dialog5 = new buzz.sound(soundAsset+"6.ogg");
var dialog6 = new buzz.sound(soundAsset+"7.ogg");
var dialog7 = new buzz.sound(soundAsset+"8.ogg");
var dialog8 = new buzz.sound(soundAsset+"9.ogg");
var dialog9 = new buzz.sound(soundAsset+"10.ogg");
var dialog10 = new buzz.sound(soundAsset+"11.ogg");
var dialog11 = new buzz.sound(soundAsset+"12.ogg");
var dialog12 = new buzz.sound(soundAsset+"13.ogg");
var dialog13 = new buzz.sound(soundAsset+"14.ogg");
var dialog14 = new buzz.sound(soundAsset+"15.ogg");
var dialog15 = new buzz.sound(soundAsset+"16.ogg");
var dialog16 = new buzz.sound(soundAsset+"17.ogg");
var dialog17 = new buzz.sound(soundAsset+"18.ogg");
var dialog18 = new buzz.sound(soundAsset+"19.ogg");
var dialog19 = new buzz.sound(soundAsset+"s2_p1_title.ogg");
var dialog20 = new buzz.sound(soundAsset+"s2_p5_title.ogg");
var dialog21 = new buzz.sound(soundAsset+"s2_p8_title.ogg");
var dialog22 = new buzz.sound(soundAsset+"s2_p13_title.ogg");
var dialog23 = new buzz.sound(soundAsset+"s2_p16_title.ogg");

var sound_group1 = [dialog0, dialog1, dialog2, dialog3, dialog4, dialog5, dialog6, dialog7, dialog8, dialog9, dialog10, dialog11, dialog12, dialog13, dialog14, dialog15, dialog16, dialog17, dialog18,dialog19,dialog20,dialog21,dialog22,dialog23];

var content=[
/*flower*/
{
	//page 1
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partflower.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.flower,
		expinfo : [
		{
			infoclass : "cssfadein",
			infodata : data.string.floexp1
		},
		],
	}
	]
},
{
	//page 2
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partflower.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.flower,
		expinfo : [
		{
			infodata : data.string.floexp1
		},
		{
			infoclass : "cssfadein",
			infodata : data.string.floexp2
		},
		],
		imagetoshow : [
		{
			imgclass : "expflo1 cssfadein",
			imgsrc : imgpath + "flowers/flower.png"
		},
		{
			imgclass : "expflo2 cssfadein",
			imgsrc : imgpath + "flowers/flower02.png"
		},
		{
			imgclass : "expflo3 cssfadein",
			imgsrc : imgpath + "flowers/flower03.png"
		},
		],
	}
	]
},
{
	//page 3
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partflower.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.flower,
		expinfo : [
		{
			infoclass : "cssfadein",
			infodata : data.string.floexp3
		},
		],
		imagetoshow : [
		{
			imgclass : "expflo1 cssfadein",
			imgsrc : imgpath + "seed.png"
		},
		{
			imgclass : "expflo2 cssfadein",
			imgsrc : imgpath + "flower.png"
		},
		],
		spritetoshow : [
		{
			spritecss : "seedsgrow"
		}
		]
	}
	]
},
{
	//page 4
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partflower.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.flower,
		expinfo : [
		{
			infoclass : "cssfadein",
			infodata : data.string.floexp4
		},
		],
		imagetoshow : [
		{
			imgclass : "beeimg cssfadein",
			imgsrc : imgpath + "flowers/bee.gif"
		},
		{
			imgclass : "butterimg cssfadein",
			imgsrc : imgpath + "flowers/butterfly2.gif"
		},
		],
	}
	]
},
/*leaves*/
{
	//page 5
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partleaf.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.leaf,
		expinfo : [
		{
			infodata : data.string.leafexp1
		},
		],
		imagetoshow : [
		{
			imgclass : "expbig1",
			imgsrc : imgpath + "flowers/leaf01.png"
		},
		{
			imgclass : "expbig2",
			imgsrc : imgpath + "flowers/leaf02.png"
		},
		{
			imgclass : "expbig3",
			imgsrc : imgpath + "flowers/leaf03.png"
		},
		],

	}
	],

},
{
	//page 6
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partleaf.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.leaf,
		expinfo : [
		{
			infodata : data.string.leafexp2
		},
		{
			infoclass : "lightcaption",
			infodata : data.string.light
		},
		],
		imagetoshow : [
		{
			imgclass : "leafmain",
			imgsrc : imgpath + "flowers/plant.png"
		},
		{
			imgclass : "arrowsun",
			imgsrc : imgpath + "flowers/arrofromsun.png"
		}
		],

	}
	],

},
{
	//page 7
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partleaf.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.leaf,
		expinfo : [
		{
			infodata : data.string.leafexp3
		},
		{
			infoclass : "lightcaption",
			infodata : data.string.light
		},
		{
			infoclass : "carboncaption",
			infodata : data.string.carbon
		},
		{
			infoclass : "watercaption",
			infodata : data.string.water
		},
		{
			infoclass : "oxygencaption",
			infodata : data.string.oxygen
		},
		],
		imagetoshow : [
		{
			imgclass : "leafmain",
			imgsrc : imgpath + "flowers/plant.png"
		},
		{
			imgclass : "arrowsun",
			imgsrc : imgpath + "flowers/arrofromsun.png"
		},
		{
			imgclass : "arrowfood",
			imgsrc : imgpath + "flowers/arroforfood.png"
		},
		{
			imgclass : "arrowwater",
			imgsrc : imgpath + "flowers/arroforwater.png"
		},
		{
			imgclass : "arrowleaf",
			imgsrc : imgpath + "flowers/arrowfromleaf.png"
		}
		],

	}
	],

},
{
	//page 8
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partstem.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.stem,
		expinfo : [
		{
			infodata : data.string.stemexp1
					},/*
					{
						infodata : data.string.floexp8
					},*/
					],
					imagetoshow : [
					{
						imgclass : "stemmain",
						imgsrc : imgpath + "parts/steam.png"
					},
					],

				}
				],
			},
			{
	//page 9
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partstem.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.stem,
		expinfo : [
		{
			infodata : data.string.stemexp1
		},
		{
			infodata : data.string.stemexp2
		},
		],
		imagetoshow : [
		{
			imgclass : "stemmain",
			imgsrc : imgpath + "parts/steam.png"
		},
		],

	}
	],
},

{
	//page 10
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partstem.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.stem,
		expinfo : [
		{
			infodata : data.string.stemexp3
		},
		],
		imagetoshow : [
		{
			imgclass : "suntrunk",
			imgsrc : imgpath + "sunflowertrunk.jpg"
		},
		{
			imgclass : "oaktrunk",
			imgsrc : imgpath + "oak-tree-trunk.png"
		},
		],

	}
	],
},
{
	//page 11
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partstem.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.stem,
		expinfo : [
		{
			infodata : data.string.stemexp4
		},
		],
		imagetoshow : [
		{
			imgclass : "treewater",
			imgsrc : imgpath + "tree02.gif"
		},
		],

	}
	],
},
{
	//page 12
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partstem.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.stem,
		expinfo : [
		{
			infodata : data.string.stemexp5
		},
		],
		imagetoshow : [
		{
			imgclass : "treewater",
			imgsrc : imgpath + "tree01.gif"
		},
		],

	}
	],
},
{
	//page 13
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partfruits.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.fruit,
		expinfo : [
		{
			infodata : data.string.fruexp1
		},
		],
		imagetoshow : [
		{
			imgclass : "fruit1",
			imgsrc : imgpath + "fruits/01.png"
		},
		{
			imgclass : "fruit2",
			imgsrc : imgpath + "fruits/02.png"
		},
		{
			imgclass : "fruit3",
			imgsrc : imgpath + "fruits/03.png"
		},
		{
			imgclass : "fruit4",
			imgsrc : imgpath + "fruits/04.png"
		},
		{
			imgclass : "fruit5",
			imgsrc : imgpath + "fruits/05.png"
		},
		{
			imgclass : "fruit6",
			imgsrc : imgpath + "fruits/06.png"
		},
		{
			imgclass : "fruit7",
			imgsrc : imgpath + "fruits/07.png"
		},
		{
			imgclass : "fruit8",
			imgsrc : imgpath + "fruits/08.png"
		},
		{
			imgclass : "fruit9",
			imgsrc : imgpath + "fruits/09.png"
		},
		{
			imgclass : "fruit10",
			imgsrc : imgpath + "fruits/10.png"
		},
		],

	}
	],
},
{
	//page 14
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partfruits.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.fruit,
		expinfo : [
		{
			infodata : data.string.fruexp2
		},
		],
		imagetoshow : [
		{
			imgclass : "expflo1",
			imgsrc : imgpath + "fruits/apple.png"
		},
		{
			imgclass : "expflo2",
			imgsrc : imgpath + "fruits/kiwi.png"
		},
		{
			imgclass : "expflo3",
			imgsrc : imgpath + "fruits/watermekon.png"
		},
		],

	}
	],
},
{
	//page 15
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partfruits.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.fruit,
		expinfo : [
		{
			infodata : data.string.fruexp3
		},
		],
		imagetoshow : [
		{
			imgclass : "exproot1",
			imgsrc : imgpath + "fruits/crow.png"
		},
		{
			imgclass : "exproot2",
			imgsrc : imgpath + "fruits/eating-watermelon.png"
		},
		{
			imgclass : "exproot3",
			imgsrc : imgpath + "fruits/monkey01.png"
		},
		],

	}
	],
},





/*root*/
{
	//page 16
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partroot.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.root,
		expinfo : [
		{
			infodata : data.string.rootexp3
		},
		],
		imagetoshow : [
		{
			imgclass : "rootpic",
			imgsrc : imgpath + "roots-holds-plants-strongly-in-the-soil.png"
		},
		],

	}
	],
},
{
	//page 17
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partroot.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.root,
		expinfo : [
		{
			infodata : data.string.rootexp2
		},
		],
		imagetoshow : [
		{
			imgclass : "treewater",
			imgsrc : imgpath + "tree02.gif"
		},
		],

	}
	],
},

{
	//page 18
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partroot.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.root,
		expinfo : [
		{
			infodata : data.string.rootexp1
		},
		],
		spritetoshow : [
		{
			spritecss : "windy"
		}
		]

	}
	],
},
{
	//page 19
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantmoved",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "planthighlight",
			imgsrc : imgpath + "parts/partroot.png"
		},
		]
	}
	],
	explanationblock: [
	{
		exptitle: data.string.root,
		expinfo : [
		{
			infodata : data.string.rootexp4
		},
		],
		imagetoshow : [
		{
			imgclass : "rootpic",
			imgsrc : imgpath + "flowers/different-types-of-root.png"
		},
		],

	}
	],
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch(countNext) {
			case 0:
				soundplayer1(19);
				setTimeout(function(){
					soundplayer(0);
				},1500);
			break;
			case 1:
				soundplayer(1);
			break;
			case 2:
				soundplayer(2);
			break;
			case 3:
				soundplayer(3);
			break;
			case 4:
				soundplayer1(20);
				setTimeout(function(){
					soundplayer(4);
				},1500);
			break;
			case 5:
				soundplayer(5);
			break;
			case 6:
				soundplayer(6);
			break;
			case 7:
			soundplayer1(21);
			setTimeout(function(){
				soundplayer(7);
			},1500);
			break;
			case 8:
				soundplayer(8);
			break;
			case 9:
				soundplayer(9);
			break;
			case 10:
				soundplayer(10);
			break;
			case 11:
				soundplayer(11);
			break;
			case 12:
			soundplayer1(22);
			setTimeout(function(){
				soundplayer(12);
			},1500);
			break;
			case 13:
				soundplayer(13);
			break;
			case 14:
				soundplayer(14);
			break;
			case 15:
			soundplayer1(23);
			setTimeout(function(){
				soundplayer(15);
			},1500);
			break;
			case 16:
				soundplayer(16);
			break;
			case 17:
				var div_ratio =$(".windy").width()/$(".windy").parent().width();
				$(".windy").width(Math.round($(".windy").parent().width()*div_ratio));
				soundplayer(17);
			break;
			case 18:
				soundplayer(18);
			break;
			case 19:
				soundplayer(19);
			break;


		}
	}

	function soundplayer(i, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
			if(!hidebtn)
				navigationcontroller();
		});
	}
	function soundplayer1(i, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
			$nextBtn.hide(0);
			$prevBtn.hide(0);
		});
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

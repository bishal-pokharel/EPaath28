var imgpath = $ref + "/images/";

var soundAsset;
if($lang == "en"){
	soundAsset = $ref+"/sound/en/p1/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/p1/";
}
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");

var sound_group1 = [dialog0, dialog1, dialog2, dialog3, dialog4];

var content=[
{
	//starting page
	contentblockadditionalclass : "contentwithbg",
	contentblocknocenteradjust : true,
	uppertextblock : [
	{
		textclass : "firsttitle",
		textdata : data.string.title
	}
	]
},
{
	//page 1
	contentblockadditionalclass : "contentwithbg2",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction cssfadein",
		textdata : data.string.p1text1
	},
	{
		textclass : "introduction cssfadein2",
		textdata : data.string.p1text2
	},
	{
		textclass : "introduction cssfadein3",
		textdata : data.string.p1text3
	}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "girl",
			imgsrc : imgpath + "migma.png"
		},
		{
			imgclass : "plant",
			imgsrc : imgpath + "plant.png"
		},
		],
	}
	]
},
{
	//page 2
	contentblockadditionalclass : "contentwithbg2",
	uppertextblockadditionalclass: "uppertextblock",
	uppertextblock : [
	{
		textclass : "introduction cssfadein",
		textdata : data.string.p1text4
	},
	{
		textclass : "introduction cssfadein2",
		textdata : data.string.p1text5
	}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "eye cssfadein",
			imgsrc : imgpath + "eye.png"
		},
		{
			imgclass : "ear cssfadein",
			imgsrc : imgpath + "ear.png"
		},
		{
			imgclass : "foot cssfadein",
			imgsrc : imgpath + "foot.png"
		},
		{
			imgclass : "leaf cssfadein2",
			imgsrc : imgpath + "leaf.png"
		},
		{
			imgclass : "flower cssfadein2",
			imgsrc : imgpath + "flowercartoon.png"
		},
		{
			imgclass : "root cssfadein2",
			imgsrc : imgpath + "root.png"
		},
		],
	}
	]
},
{
	//page 3
	contentblockadditionalclass : "contentwithbg2",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.p1text6
	},
	],
	uppertextblock : [
	{
		textclass : "flowertext labelbox cssfadein",
		textdata : data.string.flower
	},
	{
		textclass : "fruittext labelbox cssfadein",
		textdata : data.string.fruit
	},
	{
		textclass : "leaftext labelbox cssfadein",
		textdata : data.string.leaf
	},
	{
		textclass : "stemtext labelbox cssfadein",
		textdata : data.string.stem
	},
	{
		textclass : "roottext labelbox cssfadein",
		textdata : data.string.root
	}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "plantwhole cssfadein",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "arrow1 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow2 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow3 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow4 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow5 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		]
	}
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	
	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		
	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
      */

  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
      */  
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}
	
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	
		switch(countNext) {
			case 0:
				soundplayer(0);
			break;
			case 1:
				soundplayer(1);
			break;
			case 2:
				soundplayer(2);
			break;
			case 3:
				soundplayer(3);
			break;
		}
	}

	function soundplayer(i, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
			if(!hidebtn)
				navigationcontroller();
		});
	}
	
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		
		loadTimelineProgress($total_page, countNext + 1);
		
		//navigationcontroller();
		
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
		
	}
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});
	
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });
	
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			
			
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;
					
					texthighlightstarttag = "<span>";
					

					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					
					
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


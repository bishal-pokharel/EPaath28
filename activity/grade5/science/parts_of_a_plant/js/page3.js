var imgpath = $ref + "/images/";
var soundAsset;
if($lang == "en"){
	soundAsset = $ref+"/sound/en/p3/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/p3/";
}
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");

var sound_group1 = [dialog0, dialog1, dialog2];
var content=[
{
	//starting page
	contentblockadditionalclass : "contentwithbg",
	contentblocknocenteradjust : true,
	uppertextblock : [
	{
		textclass : "firsttitle",
		textdata : data.string.diy
	}
],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "diycover",
			imgsrc : imgpath + "diy_cover.png"
		}]
	}]
},
{
	//FIRST DIY
	contentblockadditionalclass : "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.diyheader
	},
	],
	uppertextblock : [
	{
		textclass : "droppable testdrop",
		ans:"dragflower"
	},
	{
		textclass : "droppable fruitdrop",
        ans:"dragfruit"
	},
	{
		textclass : "droppable leafdrop1",
        ans:"dragleaf"

    },
	{
		textclass : "droppable leafdrop2",
        ans:"dragleaf"

    },
	{
		textclass : "droppable stemdrop",
        ans:"dragstem"

    },
	{
		textclass : "droppable rootdrop",
        ans:"dragroot"

    }
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "draggable dragflower",
			imgsrc : imgpath + "parts/flower03.png"
		},
		{
			imgclass : "draggable dragleaf",
			imgsrc : imgpath + "parts/leaf03.png"
		},
		{
			imgclass : "draggable dragstem",
			imgsrc : imgpath + "parts/steam.png"
		},
		{
			imgclass : "draggable dragfruit",
			imgsrc : imgpath + "parts/fruit01.png"
		},
		{
			imgclass : "draggable dragroot",
			imgsrc : imgpath + "parts/root.png"
		},
		/*droppable image displays*/
		{
			imgclass : "draggable planthidden",
			imgsrc : imgpath + "parts/greymustardplant.png"
		},
		{
			imgclass : "planthidden dragflowershow",
			imgsrc : imgpath + "parts/partflower.png"
		},
		{
			imgclass : "planthidden dragleafshow",
			imgsrc : imgpath + "parts/partleaf.png"
		},
		{
			imgclass : "planthidden dragstemshow",
			imgsrc : imgpath + "parts/partstem.png"
		},
		{
			imgclass : "planthidden dragfruitshow",
			imgsrc : imgpath + "parts/partfruits.png"
		},
		{
			imgclass : "planthidden dragrootshow",
			imgsrc : imgpath + "parts/partroot.png"
		},
		]
	}
	]
},
{
	//second diy
	contentblockadditionalclass : "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.diyheader2
	},
	],
	uppertextblock : [
	{
		textclass : "droppable flowertext labelbox cssfadein",
		ans:"floweropt"
	},
	{
		textclass : "droppable fruittext labelbox cssfadein",
        ans:"fruitopt"
	},
	{
		textclass : "droppable leaftext labelbox cssfadein",
        ans:"leafopt"
	},
	{
		textclass : "droppable stemtext labelbox cssfadein",
        ans:"stemopt"
	},
	{
		textclass : "droppable roottext labelbox cssfadein",
        ans:"rootopt"
	}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "planthidden",
			imgsrc : imgpath + "parts/plant.png"
		},
		{
			imgclass : "arrow1 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow2 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow3 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow4 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "arrow5 cssfadein",
			imgsrc : imgpath + "line.png"
		},
		]
	}
	],
	explanationblock: [
	{
		expinfo : [
		{
			infoclass : "draggable labelbox fruitopt",
			infodata : data.string.fruit
		},
		{
			infoclass : "draggable labelbox stemopt",
			infodata : data.string.stem
		},
		{
			infoclass : "draggable labelbox rootopt",
			infodata : data.string.root
		},
		{
			infoclass : "draggable labelbox leafopt",
			infodata : data.string.leaf
		},
		{
			infoclass : "draggable labelbox floweropt",
			infodata : data.string.flower
		},
		],
	}
	]
}

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		switch(countNext) {
			case 0:
			play_diy_audio();
			break;
			case 1:
				soundplayer(1, true);
			var finCount = 0;
			$nextBtn.hide(0);
			$('.dragflowershow, .dragfruitshow, .dragleafshow, .dragstemshow, .dragrootshow').hide(0);
			$('.draggable').draggable({revert : true});

                $('.droppable').droppable({
                    	accept: ".draggable",
                    	drop: function(event, ui){
                    		dropfunc(event, ui,$(this));
                    	}
                    });


			function dropfunc(event, ui,drop){
				if(ui.draggable.attr('class').split(' ')[1]==drop.attr("data-answer")) {
                    var className = ui.draggable.attr('class').split(' ')[1] + "show";
                    ui.draggable.draggable('disable');
                    ui.draggable.hide(0);
                    $('.' + className).show(0);
                    finCount++;
                    play_correct_incorrect_sound(1);
                    if (finCount == 5) {
                        $nextBtn.show(0);
                        $prevBtn.show(0);
                    }
                }
                else{
                    play_correct_incorrect_sound(0);
                }

			}
			break;
			case 2:
				soundplayer(2, true);
				var finCount = 0;

				$(".draggable").draggable({revert : true,zIndex : 1000});

                $('.droppable').droppable({
                    accept: ".draggable",
                    drop: function(event, ui){
                        dropfunc2(event, ui,$(this));
                    }
                });

				// $('.flowertext').droppable({
				// accept: ".floweropt",
				// drop: dropfunc2
				// });
                //
				// $('.fruittext').droppable({
				// 	accept: ".fruitopt",
				// 	drop: dropfunc2
				// });
                //
				// $('.leaftext').droppable({
				// 	accept: ".leafopt",
				// 	drop: dropfunc2
				// });
                //
				// $('.stemtext').droppable({
				// 	accept: ".stemopt",
				// 	drop: dropfunc2
				// });
                //
				// $('.roottext').droppable({
				// 	accept: ".rootopt",
				// 	drop: dropfunc2
				// });

				function dropfunc2(event, ui,drop){
                    if(ui.draggable.attr('class').split(' ')[2]==drop.attr("data-answer")) {
                        {
                            var classText = ui.draggable.text();
                            drop.html(classText);
                            console.log("This is class text "+classText);
                            ui.draggable.draggable('disable');
                            ui.draggable.hide(0);
                            finCount++;
                            play_correct_incorrect_sound(1);
                            if (finCount == 5) {
                                $prevBtn.show(0);
                                ole.footerNotificationHandler.lessonEndSetNotification();
                            }

                        }
                    }
                    else{
                    play_correct_incorrect_sound(0);
                    }
                }
			break;
		}
	}
	function soundplayer(i, hidebtn){
	$nextBtn.hide(0);
	$prevBtn.hide(0);
	sound_group1[i].play().bind("ended",function(){
		if(!hidebtn)
			navigationcontroller();
	});
}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});




 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

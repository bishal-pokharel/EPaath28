Array.prototype.shufflearray = function(){
  var i = this.length, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset;
if($lang == "en"){
	soundAsset = $ref+"/sound/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/";
}
var dialog0 = new buzz.sound(soundAsset+"ex_inst_2.ogg");
var sound_group1 = [dialog0];


var content=[
	{
		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q1,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans4,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q2,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans3,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q3,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans2,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q4,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans1,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q5,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans5,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q6,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans4,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q7,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans2,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q8,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans2,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q9,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans1,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},

	{

		levelblock: [{
			textdata: data.string.lvl2,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl2_a,
			textclass: "header"
		}],

		textblock: [{
			queblock: [{
				quedata: data.string.lvl2_q10,
				queclass: "question"
			}],

			ansblock: [{
				ansdata: data.string.ans5,
				ansclass: "answer"
			}]
		}],

		svgblock: [{
			svgname: "svgcontainer",
		}]
	},
];

/*remove this for non random questions*/
/*content.shufflearray();*/

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;
	var testin = new EggTemplate();

    testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		var countAll = 0;

		$nextBtn.hide(0);
		$prevBtn.hide(0);

		/*======= SCOREBOARD SECTION ==============*/

		var ansClicked = false;
		var wrngClicked = false;

		$(".buttonsel").click(function(){
			$(this).removeClass('forhover');
			if(ansClicked == false){

				/*class 1 is always for the right answer. updates scoreboard and disables other click if
				right answer is clicked*/
				if($(this).hasClass("class1")){

					if(wrngClicked == false){
						$("#egg"+countNext).attr("src", "images/eggs/" + randImg +".png").removeClass('eggmove');
						score++;
					}
					$(this).css("border","5px solid #B6D7A8");
					$(this).siblings(".corctopt").css("visibility","visible");
					$('.buttonsel').removeClass('forhover forhoverimg');
					ansClicked = true;

					if(countNext != $total_page)
					$nextBtn.show(0);
				}
				else{
					$("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
					$(this).css("border","5px solid #efb3b3");
					$(this).css("color","#F66E20");
					$(this).siblings(".wrngopt").css("visibility","visible");
					wrngClicked = true;
				}
			}
		});

		function incorrect(){
			if(ansClicked == false){
                testin.update(false);
				play_correct_incorrect_sound(0);
			}
		}
		function correct(cName){
			play_correct_incorrect_sound(1);
			if(ansClicked == false){
             	testin.update(true);
			}
			$(".answerblock").html(cName);
			$(".answerblock").show(0);
			$nextBtn.show(0);
		}

		var s = Snap(".svgcontainer");
		var className;
		Snap.load(imgpath+"mustard_plant.svg", function (f) {
		s.append(f);
			switch(countNext){
				case 0:
        soundplayer(0);
					className = "flower";
					$('.flower').click(function(){correct(data.string.ans4);});
					$('.leaf, .stem, .fruit, .root').click(function(){incorrect();});
				break;
				case 1:
					className = "leaf";
					$('.leaf').click(function(){correct(data.string.ans3);});
					$('.flower, .stem, .fruit, .root').click(function(){incorrect();});
				break;
				case 2:
					className = "stem";
					$('.stem').click(function(){correct(data.string.ans2);});
					$('.leaf, .flower, .fruit, .root').click(function(){incorrect();});
				break;
				case 3:
					className = "fruit";
					$('.fruit').click(function(){correct(data.string.ans1);});
					$('.leaf, .stem, .flower, .root').click(function(){incorrect();});
				break;
				case 4:
					className = "root";
					$('.root').click(function(){correct(data.string.ans5);});
					$('.leaf, .stem, .fruit, .flower').click(function(){incorrect();});
				break;
				case 5:
					className = "flower";
					$('.flower').click(function(){correct(data.string.ans4);});
					$('.leaf, .stem, .fruit, .root').click(function(){incorrect();});
				break;
				case 6:
					className = "stem";
					$('.stem').click(function(){correct(data.string.ans2);});
					$('.leaf, .flower, .fruit, .root').click(function(){incorrect();});
				break;
				case 7:
					className = "stem";
					$('.stem').click(function(){correct(data.string.ans2);});
					$('.leaf, .flower, .fruit, .root').click(function(){incorrect();});
				break;
				case 8:
					className = "fruit";
					$('.fruit').click(function(){correct(data.string.ans1);});
					$('.leaf, .stem, .flower, .root').click(function(){incorrect();});
				break;
				case 9:
					className = "root";
					$('.root').click(function(){correct(data.string.ans5);});
					$('.leaf, .stem, .fruit, .flower').click(function(){incorrect();});
				break;
			}
		});
		/*======= SCOREBOARD SECTION ==============*/
	}
  function soundplayer(i, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
			if(!hidebtn)
				navigationcontroller();
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
     	testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

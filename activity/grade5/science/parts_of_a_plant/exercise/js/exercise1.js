// Array.prototype.shufflearray = function(){
//   var i = this.length, j, temp;
// 	    while(--i > 0){
// 	        j = Math.floor(Math.random() * (i+1));
// 	        temp = this[j];
// 	        this[j] = this[i];
// 	        this[i] = temp;
// 	    }
// 	    return this;
// }

var imgpath = $ref+"/exercise/images/";
var soundAsset;
if($lang == "en"){
	soundAsset = $ref+"/sound/en/";
}
else if($lang == "np")
{
	soundAsset = $ref+"/sound/np/";
}
var dialog0 = new buzz.sound(soundAsset+"ex_inst_1.ogg");
var sound_group1 = [dialog0];

var content=[
	{
		levelblock: [{
			textdata: data.string.lvl1,
			textclass: "level"
		}],

		uppertextblock: [{
			textdata: data.string.lvl1_a,
			textclass: "header"
		}],

		numberblock: [{
			numdata: data.string.no1,
			numclass: "no no1"
		},{
			numdata: data.string.no2,
			numclass: "no no2"
		},{
			numdata: data.string.no3,
			numclass: "no no3"
		},{
			numdata: data.string.no4,
			numclass: "no no4"
		},{
			numdata: data.string.no5,
			numclass: "no no5"
		}],

		imageblock: [{
			plantblock:[{
				imgsrc: imgpath + "partoftheplants.jpg",
				imgclass: "plant"
			}],

			arrowblock:[{
				imgsrc: imgpath + "arrow.png",
				imgclass: "arrow arrow1"
			},{
				imgsrc: imgpath + "arrow.png",
				imgclass: "arrow arrow2"
			},{
				imgsrc: imgpath + "arrow.png",
				imgclass: "arrow arrow3"
			},{
				imgsrc: imgpath + "arrow.png",
				imgclass: "arrow arrow4"
			},{
				imgsrc: imgpath + "arrow.png",
				imgclass: "arrow arrow5"
			}],

			emptyboxblock: [{
				boxclass: "box box1 dropable1"
			},{
				boxclass: "box box2 dropable2"
			},{
				boxclass: "box box3 dropable3"
			},{
				boxclass: "box box4 dropable4"
			},{
				boxclass: "box box5 dropable5"
			}]
		}],

		optionblock: [{
			textdata: data.string.ans1,
			textclass: "option option1 draggable draggable1"
		},{
			textdata: data.string.ans2,
			textclass: "option option2 draggable draggable2"
		},{
			textdata: data.string.ans3,
			textclass: "option option3 draggable draggable3"
		},{
			textdata: data.string.ans4,
			textclass: "option option4 draggable draggable4"
		},{
			textdata: data.string.ans5,
			textclass: "option option5 draggable draggable5"
		},]
	},
];

/*remove this for non random questions*/
/*content.shufflearray();*/

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	 }

	var score = 0;

 	//create eggs
 	var testin = new EggTemplate();

 	//eggTemplate.eggMove(countNext);
	testin.init(5);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
	 	var testcount = 0;

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		$('.congratulation').hide(0);
		$('.exefin').hide(0);
		// if(countNext==0){
		//
		// }
		var hasDropped_1 = false;
		var hasDropped_2 = false;
		var hasDropped_3 = false;
		var hasDropped_4 = false;
		switch(countNext){
			case 0:
				soundplayer(0);
				$(".no1").delay(1000).fadeIn(500);
				$('.draggable').draggable({
					containment : "body",
					cursor : "all-scroll",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
					zindex : 10000,
				});

				$('.dropable1').droppable({
					hoverClass : "hovered",
					drop : handleCardDrop1
				});

				$('.dropable2').droppable({
					hoverClass : "hovered",
					drop : handleCardDrop2
				});

				$('.dropable3').droppable({
					hoverClass : "hovered",
					drop : handleCardDrop3
				});

				$('.dropable4').droppable({
					hoverClass : "hovered",
					drop : handleCardDrop4
				});

				$('.dropable5').droppable({
					hoverClass : "hovered",
					drop : handleCardDrop5
				});
				$( ".dropable2, .dropable3, .dropable4, .dropable5" ).droppable( "option", "disabled", true );

				function handleCardDrop1(event, ui) {
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					$( ".dropable2, .dropable3, .dropable4, .dropable5" ).droppable( "option", "disabled", true );
					if(dropped.hasClass("draggable1")) {
						ui.draggable.draggable('disable');

						$(dropped.context).css({
							"width": "100%",
							"height": "auto",
							"max-height": "100%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "0%",
							"right" : "0%",
							"margin" : "0 auto",
							"top" : "12%",
							"opacity" : "1",
							"cursor" : "all-scroll",
							"background":"transparent",
							"pointer-events":"none"

						}).appendTo(droppedOn);
						hasDropped_1 = true;
						$( ".dropable2" ).droppable("enable");
						$(".no2").fadeIn(500);
		    		$(".no1").css("animation", "none");
						play_correct_incorrect_sound(1);
	 					testin.update(true);
						testin.gotoNext();

					} else {
						hasDropped_1?$( ".dropable2" ).droppable("enable"):'';
						play_correct_incorrect_sound(0);
	 					testin.update(false);
					}
				}

				function handleCardDrop2(event, ui) {
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					$( ".dropable1, .dropable3, .dropable4, .dropable5" ).droppable( "option", "disabled", true );
					if(dropped.hasClass("draggable2")) {
						ui.draggable.draggable('disable');

						$(dropped.context).css({
							"width": "100%",
							"height": "auto",
							"max-height": "100%",
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "0%",
							"right" : "0%",
							"margin" : "0 auto",
							"top" : "12%",
							"opacity" : "1",
							"cursor" : "all-scroll"
						}).appendTo(droppedOn);
						hasDropped_2 = true;
						$( ".dropable3" ).droppable("enable");
						$(".no3").fadeIn(500);
		    		$(".no2").css("animation", "none");
						play_correct_incorrect_sound(1);
	 					testin.update(true);
						testin.gotoNext();

					} else {
						hasDropped_2?$( ".dropable3" ).droppable("enable"):'';
						play_correct_incorrect_sound(0);
	 					testin.update(false);
					}
				}

				function handleCardDrop3(event, ui) {
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					$( ".dropable1, .dropable2, .dropable4, .dropable5" ).droppable( "option", "disabled", true );
					if(dropped.hasClass("draggable3")) {
						ui.draggable.draggable('disable');

						$(dropped.context).css({
							"width": "100%",
							"height": "auto",
							"max-height": "100%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "0%",
							"right" : "0%",
							"margin" : "0 auto",
							"top" : "12%",
							"opacity" : "1",
							"cursor" : "all-scroll"
						}).appendTo(droppedOn);
						hasDropped_3 = true;
						$( ".dropable4" ).droppable("enable");
						$(".no4").fadeIn(500);
		    		$(".no3").css("animation", "none");
						play_correct_incorrect_sound(1);
	 					testin.update(true);
						testin.gotoNext();

					} else {
						hasDropped_3?$( ".dropable4" ).droppable("enable"):'';
						play_correct_incorrect_sound(0);
	 					testin.update(false);
					}
				}

				function handleCardDrop4(event, ui) {
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					$( ".dropable1, .dropable2, .dropable3, .dropable5" ).droppable( "option", "disabled", true );
					if(dropped.hasClass("draggable4")) {
						ui.draggable.draggable('disable');

						$(dropped.context).css({
							"width": "100%",
							"height": "auto",
							"max-height": "100%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "0%",
							"right" : "0%",
							"margin" : "0 auto",
							"top" : "12%",
							"opacity" : "1",
							"cursor" :'all-scroll'
						}).appendTo(droppedOn);
						hasDropped_4 = true;
						$( ".dropable5" ).droppable("enable");
						$(".no5").fadeIn(500);
		    		$(".no4").css("animation", "none");
						play_correct_incorrect_sound(1);
	 					testin.update(true);
						testin.gotoNext();

					} else {
						hasDropped_4?$( ".dropable5" ).droppable("enable"):'';
						play_correct_incorrect_sound(0);
	 					testin.update(false);
					}
				}

				function handleCardDrop5(event, ui) {
					var dropped = ui.draggable;
					var droppedOn = $(this);
					var count = 0;
					$( ".dropable1, .dropable2, .dropable3, .dropable4" ).droppable( "option", "disabled", true );
					if(dropped.hasClass("draggable5")) {
						ui.draggable.draggable('disable');

						$(dropped.context).css({
							"width": "100%",
							"height": "auto",
							"max-height": "100%"
						});

						$(dropped).detach().css({
							"position" : "absolute",
							"left" : "0%",
							"right" : "0%",
							"margin" : "0 auto",
							"top" : "12%",
							"opacity" : "1",
							"cursor" : "all-scroll"
						}).appendTo(droppedOn);
						$( ".dropable1, .dropable2, .dropable3, .dropable4, .dropable5" ).droppable("enable");
						play_correct_incorrect_sound(1);
	 					testin.update(true);
						testin.gotoNext();

					} else {
						play_correct_incorrect_sound(0);
	 					testin.update(false);
					}
				}

			break;
		}
		/*======= SCOREBOARD SECTION ==============*/
	}

	function soundplayer(i, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
			if(!hidebtn)
				navigationcontroller();
		});
	}
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


		//call the slide indication bar handler for pink indicators
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

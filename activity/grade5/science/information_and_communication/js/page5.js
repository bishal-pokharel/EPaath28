var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content=[

	// slide0
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[
		{
			textdata: data.string.p5text1,
			textclass: "text-top",
		},
		{
			textdata: data.string.p5text2,
			textclass: "text-2",
		},
		{
			textdata: data.string.p5text3,
			textclass: "text-3",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'img1',
				imgclass:'right-image'
			}]
		}]

	},

	// slide1
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[
		{
			textdata: data.string.cpu,
			textclass: "text-title",
		},
		{
			textdata: data.string.p5text4,
			textclass: "text-desc",
			datahighlightflag:true,
			datahighlightcustomclass:'orangebold'
		},],
		imageblock:[{
			imagestoshow:[{
				imgid:'img2',
				imgclass:'right-image1'
			}]
		}]

	},

	// slide2
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[
		{
			textdata: data.string.monitor,
			textclass: "text-title",
		},
		{
			textdata: data.string.p5text5,
			textclass: "text-desc",
			datahighlightflag:true,
			datahighlightcustomclass:'orangebold'
		},],
		imageblock:[{
			imagestoshow:[{
				imgid:'img3',
				imgclass:'right-image1'
			}]
		}]

	},

	// slide3
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[
		{
			textdata: data.string.keyboard,
			textclass: "text-title",
		},
		{
			textdata: data.string.p5text6,
			textclass: "text-desc",
			datahighlightflag:true,
			datahighlightcustomclass:'orangebold'
		},],
		imageblock:[{
			imagestoshow:[{
				imgid:'img4',
				imgclass:'right-image1 keyboard'
			}]
		}]

	},

	// slide4
	{
		contentblockadditionalclass:'green_bg',
		uppertextblock:[
		{
			textdata: data.string.mouse,
			textclass: "text-title",
		},
		{
			textdata: data.string.p5text7,
			textclass: "text-desc",
			datahighlightflag:true,
			datahighlightcustomclass:'orangebold'
		},],
		imageblock:[{
			imagestoshow:[{
				imgid:'img5',
				imgclass:'right-image1'
			}]
		}]

	},

	// slide5
	{
		uppertextblock:[{
			textdata: data.string.p5text8,
			textclass: "template-dialougebox2-left-white d1",
		},
		{
			textdata: data.string.p1text1,
			textclass: "big_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'teacher2',
				imgclass:'bg-full'
			}]
		}]
	},

	// slide6
	{
		uppertextblock:[
		{
			textdata: data.string.p5text9,
			textclass: "template-dialougebox2-top-white d3",
			datahighlightflag:true,
			datahighlightcustomclass:'orange'
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'students',
				imgclass:'bg-full'
			}]
		}]
	},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "img1", src: imgpath+"desktop.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img2", src: imgpath+"cpu.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img3", src: imgpath+"moniter.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img4", src: imgpath+"keyboard_01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "img5", src: imgpath+"mouse.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teacher2", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "students", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p5_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p5_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p5_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p5_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p5_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p5_s6.ogg"},
			// {id: "sound_7", src: soundAsset+"p5_s7.ogg"},
			// {id: "sound_8", src: soundAsset+"p5_s8.ogg"},
			// {id: "sound_9", src: soundAsset+"p5_s9.ogg"},
			// {id: "sound_10", src: soundAsset+"p5_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
					sound_play_click('sound_0','doctor_dialogue1');
					// nav_button_controls(1500);
					break;
			case 1:
					sound_play_click('sound_1','doctor_dialogue1');
					// nav_button_controls(1500);
					break;
			case 2:
					sound_play_click('sound_2','doctor_dialogue1');
					// nav_button_controls(1500);
					break;
			case 3:
						$prevBtn.show(0);
						sound_play_click('sound_3');
						break;
			case 4:
						$prevBtn.show(0);
						sound_play_click('sound_4');
						// nav_button_controls(2000);
						break;
			case 5:
						$('.d1').hide(0).fadeIn(500);
						sound_play_click('sound_5');
						// nav_button_controls(2000);
						break;

			case 6:
						$('.d3').hide(0).fadeIn(500);
						sound_play_click('sound_6');
						// nav_button_controls(2000);
						break;

			default:
				$prevBtn.show(0);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext==6){
				$prevBtn.show(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$('.'+click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(1000);
		});
	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

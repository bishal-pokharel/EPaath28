var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var imgpath = $ref+"/images/";

var content=[
	// slide0
	{
		contentblockadditionalclass: 'blue_bg',
		uppertextblock:[{
			textdata: data.lesson.chapter,
			textclass: "chaptertitle",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'kids',
				imgclass:'img-center'
			}]
		}]
	},

	// slide1
	{
		contentblockadditionalclass: 'blue_bg',
		uppertextblock:[{
			textdata: data.string.p1text2,
			textclass: "template-dialougebox2-top-flipped-white d1",
		},
		{
			textdata: data.string.p1text1,
			textclass: "text-center",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'teacher1',
				imgclass:'bg-full'
			}]
		}]
	},

	// slide2
	{
		contentblockadditionalclass: 'blue_bg',
		uppertextblock:[{
			textdata: data.string.p1text3,
			textclass: "template-dialougebox2-top-flipped-white d2",
		},
		{
			textdata: data.string.p1text1,
			textclass: "text-center big_text",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'teacher2',
				imgclass:'bg-full'
			}]
		}]
	},

	// slide3
	{
		uppertextblock:[
		{
			textdata: data.string.p1text4,
			textclass: "text-top",
		},
		{
			textdata: data.string.p1text5,
			textclass: "text-bottom",
		}],
		imageblock:[{
			imagestoshow:[{
				imgid:'fire',
				imgclass:'bg-full'
			},
			{
				imgid:'firegif',
				imgclass:'fire'
			}]
		}]
	},


		// slide4
		{
			uppertextblock:[
			{
				textdata: data.string.p1text6,
				textclass: "text-top",
			},
			{
				textdata: data.string.p1text7,
				textclass: "text-bottom",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'bell',
					imgclass:'bg-full'
				},
				{
					imgid:'bellgif',
					imgclass:'bell'
				}]
			}]
		},

		// slide5
		{
			contentblockadditionalclass: 'green_bg',
			uppertextblock:[
			{
				textdata: data.string.p1text8,
				textclass: "text-top",
			},
			{
				textdata: data.string.p1text9,
				textclass: "text-bottom",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'dove',
					imgclass:'center_img'
				}]
			}]
		},

		// slide6
		{
			contentblockadditionalclass: 'green_bg',
			uppertextblock:[
			{
				textdata: data.string.p1text10,
				textclass: "text-top",
			},
			{
				textdata: data.string.p1text11,
				textclass: "text-bottom",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'postman',
					imgclass:'center_img'
				}]
			}]
		},

		// slide7
		{
			contentblockadditionalclass: 'green_bg',
			uppertextblock:[
			{
				textdata: data.string.p1text12,
				textclass: "text-top",
			},
			{
				textdata: data.string.p1text13,
				textclass: "text-bottom",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'drum',
					imgclass:'center_img'
				}]
			}]
		},

		// slide8
		{
			contentblockadditionalclass: 'green_bg',
			uppertextblock:[
			{
				textdata: data.string.p1text14,
				textclass: "text-top",
			},
			{
				textdata: data.string.p1text15,
				textclass: "text-bottom",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'beats',
					imgclass:'center_img'
				}]
			}]
		},

		// slide9
		{
			uppertextblock:[
			{
				textdata: data.string.p1text16,
				textclass: "template-dialougebox2-top-white d3",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'students',
					imgclass:'bg-full'
				}]
			}]
		},

		// slide10
		{
			contentblockadditionalclass: 'blue_bg',
			uppertextblock:[{
				textdata: data.string.p1text17,
				textclass: "template-dialougebox2-top-flipped-white d4",
			},
			{
				textdata: data.string.p1text1,
				textclass: "text-center big_text",
			}],
			imageblock:[{
				imagestoshow:[{
					imgid:'teacher2',
					imgclass:'bg-full'
				}]
			}]
		},
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "kids", src: imgpath+"01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teacher1", src: imgpath+"02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "teacher2", src: imgpath+"03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire", src: imgpath+"bg_for_fire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bell", src: imgpath+"bg_for_bell.png", type: createjs.AbstractLoader.IMAGE},
			{id: "dove", src: imgpath+"birdflying.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "postman", src: imgpath+"postman.png", type: createjs.AbstractLoader.IMAGE},
			{id: "students", src: imgpath+"06.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drum", src: imgpath+"beating_drum.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "beats", src: imgpath+"beating-cymbals.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "firegif", src: imgpath+"fire.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "bellgif", src: imgpath+"temple_bell.gif", type: createjs.AbstractLoader.IMAGE},
			{id: "cymbols", src: imgpath+"temple_bell.gif", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p1_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p1_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p1_s8.ogg"},
			{id: "sound_9", src: soundAsset+"p1_s9.ogg"},
			{id: "sound_10", src: soundAsset+"p1_s10.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_speechbox_image(content, countNext);
		switch(countNext) {
			case 0:
				$prevBtn.hide(0);
				sound_play_click('sound_0');
				break;
			case 1:
				$('.template-dialougebox2-top-flipped-white').hide(0).fadeIn(500);
				sound_play_click('sound_1');
				break;
			case 2:
				$('.template-dialougebox2-top-flipped-white').hide(0).fadeIn(500);
				sound_play_click('sound_2');
				break;
			case 3:
				sound_play_click('sound_3');
				break;
			case 4:
				sound_play_click('sound_4');
				$('.bottomtext').css({"bottom":"-30%"}).animate({"bottom":"0%"},500);
				break;
			case 5:
				sound_play_click('sound_5');
				break;
			case 6:
			$('.center_img').css({'width':'25%','left':'37.5%'});
				sound_play_click('sound_6');
				break;
			case 7:
				sound_play_click('sound_7');
				break;
			case 8:
				sound_play_click('sound_8');
				break;
			case 9:
				$('.template-dialougebox2-top-white').hide(0).fadeIn(500);
				sound_play_click('sound_9');
				break;
			case 10:
				$('.template-dialougebox2-top-flipped-white').hide(0).fadeIn(500);
				sound_play_click('sound_10');
				break;

			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_play_click(sound_id, click_class){
		createjs.Sound.stop();
		var current_sound = createjs.Sound.play(sound_id);
		current_sound.on("complete", function(){
			if(typeof click_class != 'undefined'){
				$(click_class).click(function(){
					current_sound.play();
				});
			}
			nav_button_controls(0);
		});
	}
	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
			var speechbox = content[count].speechbox;
			for(var i=0; i<speechbox.length; i++){
				var image_src = preload.getResult(speechbox[i].imgid).src;
				console.log(image_src);
				var selector = ('.'+speechbox[i].speechbox+'>.speechbg');
				$(selector).attr('src', image_src);
			}
		}
	}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();

		loadTimelineProgress($total_page, countNext + 1);
		generaltemplate();
		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var sound_1 = new buzz.sound(soundAsset + "ex1.ogg");

var content=[
	//slide 0
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext4,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext2,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + '12.png'
		}]
	},

	//slide 1
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext5,
			textclass : 'template-dialougebox2-top-white d d1',
		},
		{
			textdata : data.string.ext9,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext3,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + '11.png'
		}]
	},

	//slide 2
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext6,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext10,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext11,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage',
			imgsrc:imgpath + '12.png'
		}]
	},

	//slide 3
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext7,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext12,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext13,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'books.png'
		}]
	},

	//slide 4
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext8,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext15,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext14,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'cost.png'
		}]
	},

	//slide 5
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext16,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext18,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext17,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'threepeople.png'
		}]
	},

	//slide 6
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext19,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext20,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext21,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'talking.png'
		}]
	},

	//slide 7
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext22,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext24,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext23,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'moniter.png'
		}]
	},

	//slide 8
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext25,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext26,
			textclass : 'class1 options corans',
		},
		{
			textdata : data.string.ext27,
			textclass : 'class2 options ',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'keyboard_01.png'
		}]
	},

	//slide 9
	{
		contentblockadditionalclass:'bg_green',
		uppertextblock: [{
			textdata : data.string.ext1,
			textclass : 'instruction',
		},],
		extratextblock : [
		{
			textdata : data.string.ext28,
			textclass : 'template-dialougebox2-left-white d',
		},
		{
			textdata : data.string.ext30,
			textclass : 'class1 options ',
		},
		{
			textdata : data.string.ext29,
			textclass : 'class2 options corans',
		}
		],
		imageblock:[{
			imgclass:'centerimage1',
			imgsrc:imgpath + 'mouse.png'
		}]
	},



];

content.shufflearray();


$(function ()
{
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var score = 0;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}

	function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }

	var rhino = new NumberTemplate();

	rhino.init($total_page);



	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		rhino.numberOfQuestions();

		texthighlight($board);
		$nextBtn.hide(0);
		$prevBtn.hide(0);

		if(countNext==0){
			buzz.all().stop();
			sound_1.play();
		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		$('.hint').hide(0);
		$('.options').click(function(){
			if($(this).hasClass('corans'))
			{
				if(!wrong_clicked){
					rhino.update(true);
				}
				var text1=$(this).text();
				$('.blank').html(text1).css({"color":"#F63477"});
				$('.text-5').animate({"opacity":"1"},400);
				$('.hint').fadeIn(200);
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				console.log("position top"+position.top)
				console.log("height"+height)
				console.log("board height"+$board.height())
				var centerY = ((((position.top + height)*100)/$board.height())- 3)+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(301%,-190%)" src="'+imgpath +'correct.png" />').insertAfter(this);
				$(this).css({"background":"rgb(152,192,46)","border-radius":"1vmin","border-color":"rgb(197, 224, 123)"});
				$('.options').css({"pointer-events":"none"});
				play_correct_incorrect_sound(1);
				if(countNext != $total_page){
							$nextBtn.show(0);
					}
			}
			else{
				var $this = $(this);
				var position = $this.position();
				var width = $this.width();
				var height = $this.height();
				var centerX = ((position.left + width / 2)*100)/$board.width()+'%';
				var centerY = ((((position.top + height)*100)/$board.height())- 3)+'%';
				$('<img style="left:'+centerX+';top:'+centerY+';position:absolute;width:5%;transform:translate(301%,-190%)" src="'+imgpath +'wrong.png" />').insertAfter(this);
				$(this).css({"pointer-events":"none","background":"rgb(191, 13, 13)","border-radius":"1vmin","border-color":"rgb(226, 129, 129)"});
				play_correct_incorrect_sound(0);
				if(!wrong_clicked){
					rhino.update(false);
				}
				wrong_clicked = true;
			}
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		rhino.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

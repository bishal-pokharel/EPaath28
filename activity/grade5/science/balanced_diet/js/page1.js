var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
//slide0
{
	contentblockadditionalclass:'ole-background-gradient-shore',
	uppertextblock:[{
		textdata: data.lesson.chapter,
		textclass: 'titleclass'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "balanceddietimage",
							imgsrc : imgpath + "balanced-diet.png"
						}
				]
		}
	]
},
//slide1
{
	contentblockadditionalclass:'bg1',
	uppertextblock:[{
		textdata: data.string.p1text1,
		textclass: 'bottom_middle_text'
	}]
},

//slide2
{
	contentblockadditionalclass:'ole-background-gradient-shore',
	uppertextblock:[{
		textdata: data.string.p1text2,
		textclass: 'top_heading_text'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "yogagirl",
							imgsrc : imgpath + "yoga.png"
						}
				]
		}
	]

},

//slide3
{
	contentblockadditionalclass:'ole-background-gradient-shore',
	uppertextblock:[{
		textdata: data.string.p1text2,
		textclass: 'top_heading_text'
	},
	{
		textdata: data.string.p1text3,
		textclass: 'description_text1 fadein'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "yogagirl",
							imgsrc : imgpath + "yoga.png"
						}
				]
		}
	]

},

//slide4
{
	contentblockadditionalclass:'ole-background-gradient-shore',
	uppertextblock:[{
		textdata: data.string.p1text2,
		textclass: 'top_heading_text'
	},
	{
		textdata: data.string.p1text3,
		textclass: 'description_text1 opacity1'
	},
	{
		textdata: data.string.p1text4,
		textclass: 'description_text2 fadein'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "yogagirl",
							imgsrc : imgpath + "yoga.png"
						}
				]
		}
	]

},

//slide5
{
	contentblockadditionalclass:'ole-background-gradient-morningsky',
	uppertextblock:[
	{
		textdata: data.string.p1text5,
		textclass: 'top_heading_text bot'
	},
	{
		textdata: data.string.p1text6,
		textclass:'carbs fadein1'
	},
	{
		textdata: data.string.p1text7,
		textclass:'vits fadein1'
	},
	{
		textdata: data.string.p1text8,
		textclass:'fat fadein1'
	},
	{
		textdata: data.string.p1text9,
		textclass:'pro fadein1'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "allnutrients",
							imgsrc : imgpath + "foodplatewithwater.png"
						}
				]
		}
	]

},

//slide6
{
	contentblockadditionalclass:'ole-background-gradient-morningsky',
	uppertextblock:[
	{
		textdata: data.string.p1text10,
		textclass: 'top_heading_text bot'
	},
	{
		textdata: data.string.p1text6,
		textclass:'carbs fadein1'
	},
	{
		textdata: data.string.p1text7,
		textclass:'vits fadein1'
	},
	{
		textdata: data.string.p1text8,
		textclass:'fat fadein1'
	},
	{
		textdata: data.string.p1text9,
		textclass:'pro fadein1'
	},
	{
		textdata: data.string.p1text11,
		textclass:'carb_text carb_pop'
	},
	{
		textdata: data.string.p1text12,
		textclass:'pro_text pro_pop'
	},
	{
		textdata: data.string.p1text13,
		textclass:'vits_text vits_pop'
	},
	{
		textdata: data.string.p1text14,
		textclass:'fat_text fat_pop'
	},
	{
		textdata: data.string.p1backtxt,
		textclass:'back_text'
	},
        {
            textdata: data.string.p1text6,
            textclass:'carbs_t bdtopic'
        },
        {
            textdata: data.string.p1text7,
            textclass:'vits_t bdtopic'
        },
        {
            textdata: data.string.p1text8,
            textclass:'fat_t bdtopic'
        },
        {
            textdata: data.string.p1text9,
            textclass:'pro_t bdtopic'
        },



	],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "carb_image",
							imgsrc : imgpath + "foodplate_orange.png"
						},
						{
							imgclass: "vits_image",
							imgsrc : imgpath + "foodplate_green'.png"
						},
						{
							imgclass: "fat_image",
							imgsrc : imgpath + "foodplate_blue.png"
						},
						{
							imgclass: "pro_image",
							imgsrc : imgpath + "foodplate_yellow.png"
						}
				]
		}
	],
	popups:[
		{
			popupclass:"carb_pop",
			partimgclass:"center_pop_bg",
			backimage:imgpath+"orange.png",
			imagedivclass:"imagecontainer",
			imagetoshow: [
					{
						imgclass: "sclOnHvr rice",
						imgsrc : imgpath + "rice_racks.png",
						labeltext : data.string.p1text17
					},
					{
						imgclass: "sclOnHvr corn",
						imgsrc : imgpath + "corn.png",
						labeltext : data.string.p1text18
					},
					{
						imgclass: "sclOnHvr barley",
						imgsrc : imgpath + "barli.png",
						labeltext : data.string.p1text19
					},
					{
						imgclass: "sclOnHvr oats",
						imgsrc : imgpath + "oats.png",
						labeltext : data.string.p1text22
					},
					{
						imgclass: "sclOnHvr sweetpotato",
						imgsrc : imgpath + "sweetpotato.png",
						labeltext : data.string.p1text21
					},
					{
						imgclass: "sclOnHvr wheat",
						imgsrc : imgpath + "wheat.png",
						labeltext : data.string.p1text20
					},
					{
						imgclass: "sclOnHvr potato",
						imgsrc : imgpath + "potato.png",
						labeltext : data.string.p1text23
					}
			]
		},
		{
			popupclass:"vits_pop",
			partimgclass:"center_pop_bg1",
			backimage:imgpath+"green.png",
			imagedivclass:"imagecontainer1",
			imagetoshow: [
					{
						imgclass: "sclOnHvr hotchilly",
						imgsrc : imgpath + "hotchilly.png",
						labeltext : data.string.p1text25
					},
					{
						imgclass: "sclOnHvr onion",
						imgsrc : imgpath + "onion.png",
						labeltext : data.string.p1text26
					},
					{
						imgclass: "sclOnHvr mango",
						imgsrc : imgpath + "mango.png",
						labeltext : data.string.p1text27
					},
					{
						imgclass: "sclOnHvr greensaag",
						imgsrc : imgpath + "greensaag.png",
						labeltext : data.string.p1text28
					},
					{
						imgclass: "sclOnHvr apple",
						imgsrc : imgpath + "apple.png",
						labeltext : data.string.p1text29
					},
					{
						imgclass: "sclOnHvr cauliflwoer",
						imgsrc : imgpath + "cauliflwoer.png",
						labeltext : data.string.p1text30
					},
					{
						imgclass: "sclOnHvr watermelon",
						imgsrc : imgpath + "watermelon.png",
						labeltext : data.string.p1text31
					},
					{
						imgclass: "sclOnHvr peach",
						imgsrc : imgpath + "naspati.png",
						labeltext : data.string.p1text32
					},
					{
						imgclass: "sclOnHvr pomogranate",
						imgsrc : imgpath + "pomogranate.png",
						labeltext : data.string.p1text33
					},
					{
						imgclass: "sclOnHvr strawberry",
						imgsrc : imgpath + "straberry.png",
						labeltext : data.string.p1text34
					},
					{
						imgclass: "sclOnHvr carrot",
						imgsrc : imgpath + "carrot.png",
						labeltext : data.string.p1text35
					},
					{
						imgclass: "sclOnHvr banana",
						imgsrc : imgpath + "banana01.png",
						labeltext : data.string.p1text36
					},
					{
						imgclass: "sclOnHvr mushroom",
						imgsrc : imgpath + "musroom.png",
						labeltext : data.string.p1text37
					},
					{
						imgclass: "sclOnHvr brocauli",
						imgsrc : imgpath + "brocauli.png",
						labeltext : data.string.p1text38
					}
			]
		},
			{
				popupclass:"pro_pop",
				partimgclass:"center_pop_bg",
				backimage:imgpath+"yellow.png",
				imagedivclass:"imagecontainer",
				imagetoshow: [
						{
							imgclass: "sclOnHvr peas",
							imgsrc : imgpath + "pears.png",
							labeltext : data.string.p1text39
						},
						{
							imgclass: "sclOnHvr meat",
							imgsrc : imgpath + "meat.png",
							labeltext : data.string.p1text40
						},
						{
							imgclass: "sclOnHvr chicken",
							imgsrc : imgpath + "chicken.png",
							labeltext : data.string.p1text42
						},
						{
							imgclass: "sclOnHvr egg",
							imgsrc : imgpath + "hard-boiled-egg.png",
							labeltext : data.string.p1text43
						},
						{
							imgclass: "sclOnHvr bean",
							imgsrc : imgpath + "beans-3.png",
							labeltext : data.string.p1text41
						},
						{
							imgclass: "sclOnHvr fish",
							imgsrc : imgpath + "fishinplate.png",
							labeltext : data.string.p1text44
						},
						{
							imgclass: "sclOnHvr peanuts",
							imgsrc : imgpath + "peanuit.png",
							labeltext : data.string.p1text45
						}
				]
			},
				{
					popupclass:"fat_pop",
					partimgclass:"center_pop_bg2",
					backimage:imgpath+"blue.png",
					imagedivclass:"imagecontainer2",
					imagetoshow: [
							{
								imgclass: "sclOnHvr curd",
								imgsrc : imgpath + "curd.png",
								labeltext : data.string.p1text46
							},
							{
								imgclass: "sclOnHvr milk",
								imgsrc : imgpath + "milk.png",
								labeltext : data.string.p1text47
							},
							{
								imgclass: "sclOnHvr butter",
								imgsrc : imgpath + "butter.png",
								labeltext : data.string.p1text49
							},
							{
								imgclass: "sclOnHvr ghee",
								imgsrc : imgpath + "ghee.png",
								labeltext : data.string.p1text51
							},
							{
								imgclass: "sclOnHvr cheese",
								imgsrc : imgpath + "chese01.png",
								labeltext : data.string.p1text48
							},
							{
								imgclass: "sclOnHvr oil",
								imgsrc : imgpath + "oil.png",
								labeltext : data.string.p1text50
							}
					]
				},
	]

},
//slide7
{
	contentblockadditionalclass:'ole-background-gradient-morningsky',
	uppertextblock:[
	{
		textdata: data.string.p1text15,
		textclass: 'top_heading_text bot'
	},
	{
		textdata: data.string.p1text6,
		textclass:'carbs fadein1'
	},
	{
		textdata: data.string.p1text7,
		textclass:'vits fadein1'
	},
	{
		textdata: data.string.p1text8,
		textclass:'fat fadein1'
	},
	{
		textdata: data.string.p1text9,
		textclass:'pro fadein1'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "allnutrients",
							imgsrc : imgpath + "foodplatewithwater.png"
						}
				]
		}
	]

},
//slide8
{
	contentblockadditionalclass:'ole-background-gradient-morningsky',
	uppertextblock:[
	{
		textdata: data.string.p1text6,
		textclass:'carbs fadein1'
	},
	{
		textdata: data.string.p1text7,
		textclass:'vits fadein1'
	},
	{
		textdata: data.string.p1text8,
		textclass:'fat fadein1'
	},
	{
		textdata: data.string.p1text9,
		textclass:'pro fadein1'
	},
	{
		textdata: data.string.p1text16,
		textclass:'fadein1 makingsure'
	}],
	imageblock: [
		{
				imagetoshow: [
						{
							imgclass: "allnutrients last",
							imgsrc : imgpath + "foodplatewithwater.png"
						}
				]
		}
	]

},

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
    var current_sound;


	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images
            {id: "coverpageImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "bgmain", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hen", src: imgpath+"chicken.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pond", src: imgpath+"pond02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shed", src: imgpath+"goth.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree2", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flower", src: imgpath+"flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "man", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
            //textboxes
            {id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
            {id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
            // // sounds
            {id: "sound_1", src: soundAsset+"s1_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s1_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s1_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s1_p4.ogg"},
            {id: "sound_5", src: soundAsset+"s1_p5.ogg"},
            {id: "sound_6", src: soundAsset+"s1_p6.ogg"},
            {id: "sound_7", src: soundAsset+"s1_p7.ogg"},
            {id: "sound_7_1", src: soundAsset+"s1_p7_1.ogg"},
            {id: "sound_7_2", src: soundAsset+"s1_p7_2.ogg"},
            {id: "sound_7_3", src: soundAsset+"s1_p7_3.ogg"},
            {id: "sound_7_4", src: soundAsset+"s1_p7_4.ogg"},
            {id: "sound_8", src: soundAsset+"s1_p8.ogg"},
            {id: "sound_9", src: soundAsset+"s1_p9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		//tooltipdesign
		(function () {
     var ID = "tooltip", CLS_ON = "tooltip_ON", FOLLOW = true,
     DATA = "_tooltip", OFFSET_X = 20, OFFSET_Y = 10,
     showAt = function (e) {
         var ntop = e.pageY + OFFSET_Y, nleft = e.pageX + OFFSET_X;
         $("#" + ID).html($(e.target).data(DATA)).css({
             position: "absolute","font-size":"2.8vmin", "text-shadow": "-1px 0 white, 0 1px black, 1px 0 white, 0 -1px white", top: ntop, left: nleft, "z-index": "99"
         }).show();
     };
     $(document).on("mouseenter", "*[title]", function (e) {
         $(this).data(DATA, $(this).attr("title"));
         $(this).removeAttr("title").addClass(CLS_ON);
         $("<div id='" + ID + "' />").appendTo("body");
         showAt(e);
     });
     $(document).on("mouseleave", "." + CLS_ON, function (e) {
         $(this).attr("title", $(this).data(DATA)).removeClass(CLS_ON);
         $("#" + ID).remove();
     });
     if (FOLLOW) { $(document).on("mousemove", "." + CLS_ON, showAt); }
 }());

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
        switch(countNext){

			case 2:
	        sound_player("sound_"+(countNext+1),true);
			$('.top_heading_text').css('top','4%');
					$('.yogagirl').css({width:'57%'});
						break;
			case 3:
	        sound_player("sound_"+(countNext+1),true);
			$('.top_heading_text').css('top','4%');
						$('.yogagirl').css({width:'57%'});
						break;
			case 4:
	        sound_player("sound_"+(countNext+1),true);
			$('.top_heading_text').css('top','4%');
						$('.yogagirl').animate({width:'49%'},800);
						break;
			case 5:
	        sound_player("sound_"+(countNext+1),true);
						$('.top_heading_text').css('top','4%');
						$('.allnutrients').animate({width:'50%'},800);
						break;
			case 6:
	        sound_player("sound_"+(countNext+1),false);
						$('.top_heading_text.bot').addClass('zoom_inout');
						$nextBtn.hide(0);
						$('.back_text').hide(0);
						var clickcount=0;
						$(".bdtopic").css('display', 'none');
						popinout('carb_image','carb_pop',"sound_7_1", ".carb_text","carbs_t");
						popinout('vits_image','vits_pop',"sound_7_2", ".vits_text","vits_t");
						popinout('pro_image','pro_pop',"sound_7_3", ".pro_text","pro_t");
						popinout('fat_image','fat_pop',"sound_7_4", ".fat_text","fat_t");

						// $( "img" ).tooltip({ show: { effect: "", duration: 0 } });

						break;
			case 7:
	        sound_player("sound_"+(countNext+1),true);
						$('.allnutrients').animate({width:'50%'},800);
						break;
			case 8:
	        sound_player("sound_"+(countNext+1),true);
						$('.allnutrients').animate({width:'30%'},800);
						$('.carbs').css({left: "26.5%",
															top: "30%"
															});
						$('.vits').css({left: "77%",
															top: "28%"
															});
						$('.pro').css({left: "29.5%",
															top: "58%"
															});
						$('.fat').css({left: "69%",
															top: "59%"
															});
						$nextBtn.hide(0);
						break;
			default:
	        sound_player("sound_"+(countNext+1),true);
					break;
		}
	}
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
    }

	var clickcount=0;
function popinout(image_class,pop_class,soundid, textClass,showname){
	// $('.top_heading_text').removeClass('zoom_inout');
	$('.'+image_class).click(function(){
		console.log("+showname"+showname);
        $("."+showname).css('display', 'block');
        $(".carb_image, .vits_image, .fat_image,.pro_image").hide(0);
        sound_player(soundid,0);
        $('.back_text').show(0);
		$('.back_text').addClass('shakeup');

		clickcount = clickcount + 1;

		$('.'+pop_class).animate({opacity:1},0);
        $(textClass).css("z-index","101").hide().delay(6000).fadeIn(1000);
        $('.imageblock,.carbs,.fat,.pro,.vits').removeClass('fadein1').animate({opacity:0},0);
		$('.top_heading_text').html(data.string.p1text24);
		console.log(clickcount);
		if(clickcount==4)
		{
			$nextBtn.show(0);
		}
	});
	$('.back_text').click(function(e) {
        $(".bdtopic").css('display', 'none');
        $('.back_text').hide(0);
			 $('.'+pop_class).animate({opacity:0},0);
			 $('.imageblock,.carbs,.fat,.pro,.vits').animate({opacity:1},0);
			 $('.top_heading_text').html(data.string.p1text10);
			 $(".carb_image, .vits_image, .fat_image,.pro_image").show(0);
			 $(textClass).css("z-index",0);

	});

}
function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/DIY/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var foods = [
					[false,imgpath + "rice02.png","strength_foods",data.string.rice],
					[false,imgpath + "sugercane.png","strength_foods",data.string.sugarcane],
					[false,imgpath + "wheat.png","strength_foods",data.string.wheat],
					[false,imgpath + "corn.png","strength_foods",data.string.corn],
					[false,imgpath + "potato.png","strength_foods",data.string.potato],
					[false,imgpath + "banana.png","protective_foods",data.string.banana],
    			[false,imgpath + "brocauli.png","protective_foods",data.string.broccauli],
					[false,imgpath + "carrot.png","protective_foods",data.string.carrot],
					[false,imgpath + "green-saag.png","protective_foods",data.string.greenvegetable],
					[false,imgpath + "peas.png","protective_foods",data.string.peas],
					[false,imgpath + "eggban.png","body_building_foods",data.string.egg],
					[false,imgpath + "milk.png","body_building_foods",data.string.milk],
					[false,imgpath + "fish.png","body_building_foods",data.string.fish],
					[false,imgpath + "chese.png","body_building_foods",data.string.cheese],
					[false,imgpath + "curd.png","body_building_foods",data.string.curd],
					];
var dinner_foods = [
					[false,imgpath + "rice02.png","strength_foods",data.string.rice],
					[false,imgpath + "sugercane.png","strength_foods",data.string.sugarcane],
					[false,imgpath + "wheat.png","strength_foods",data.string.wheat],
					[false,imgpath + "corn.png","strength_foods",data.string.corn],
					[false,imgpath + "roti.png","strength_foods",data.string.roti],
					[false,imgpath + "banana.png","protective_foods",data.string.banana],
    			[false,imgpath + "brinjal.png","protective_foods",data.string.brinjal],
					[false,imgpath + "apple.png","protective_foods",data.string.apple],
					[false,imgpath + "green-saag.png","protective_foods",data.string.greenvegetable],
					[false,imgpath + "cauliflwoer.png","protective_foods",data.string.cauliflower],
					[false,imgpath + "meat01.png","body_building_foods",data.string.chicken],
					[false,imgpath + "milk.png","body_building_foods",data.string.milk],
					[false,imgpath + "fish.png","body_building_foods",data.string.fish],
					[false,imgpath + "chana.png","body_building_foods",data.string.chana],
					[false,imgpath + "curd.png","body_building_foods",data.string.curd],
					];
var array_to_display = [];
var body_building_foods_names = [];
var protective_foods_names=[];
var strength_foods_names=[];
var body_building_foods_names_dinner = [];
var protective_foods_names_dinner=[];
var strength_foods_names_dinner=[];
var bodyfood = false;
var protectivefood = false;
var strengthfood = false;
var bodyfood_dinner = false;
var protectivefood_dinner = false;
var strengthfood_dinner = false;

var content=[
	// slide0
{
	uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : 'lesson-title'
		}],
		imageblock: [{
			imagetoshow: [
			{
				imgclass: "bottomimage",
				imgsrc : imgpath + "diy1.png"
			}]
		}]
},

// slide1

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	uppertextblock : [
		{
				textdata : data.string.p3text2,
				textclass : 'buttonsubmit'
			}],
	textinputbox:[{
		textclass:"ipbxtxt",
		textdata:data.string.ipnmtxt,
		plchldrtxt:data.string.plchldrtxt
	}],
	imageblock: [
	{
		imagetoshow: [


		{
			imgclass: "bottomimage",
			imgsrc : imgpath + "holdingboard.png"
		}

		]
	}]
},

// slide2

{
		contentblockadditionalclass:'ole-background-gradient-blunatic',
		uppertextblock : [
			{
					textdata : data.string.p3text3,
					textclass : 'hitext'
				}],
			imageblock: [
			{
				imagetoshow: [
				{
					imgclass: "plateholder",
					imgsrc : imgpath + "holdingplate.png"
				}

				]
			}]

},
// slide3

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	imageblockadditionalclass:'changetopimage',
					imageblock: [
					{
						containerdiv:'div1a',
						imagetoshow: [
						{
							imgclass: "image1a block1 s1",
							imgsrc : imgpath + "rice02.png"
						},
						{
							imgclass: "image2a block1 s2",
							imgsrc : imgpath + "sugercane.png"
						},
						{
							imgclass: "image3a block1 s3",
							imgsrc : imgpath + "wheat.png"
						},
						{
							imgclass: "image4a block1 s4",
							imgsrc : imgpath + "corn.png"
						},
						{
							imgclass: "image5a block1 s5",
							imgsrc : imgpath + "potato.png"
						}
						]
					},
					{
						containerdiv:'div1a',
						imagetoshow: [
						{
							imgclass: "image1a block2 r1",
							imgsrc : imgpath + "banana.png"
						},
						{
							imgclass: "image2a block2 r2",
							imgsrc : imgpath + "brocauli.png"
						},
						{
							imgclass: "image3a block2 r3",
							imgsrc : imgpath + "carrot.png"
						},
						{
							imgclass: "image4a block2 r4",
							imgsrc : imgpath + "green-saag.png"
						},
						{
							imgclass: "image5a block2 r5",
							imgsrc : imgpath + "peas.png"
						}
						]
					},
					{
						containerdiv:'div1a',
						imagetoshow: [
						{
							imgclass: "image1a block2 t1",
							imgsrc : imgpath + "eggban.png"
						},
						{
							imgclass: "image2a block2 t2",
							imgsrc : imgpath + "milk.png"
						},
						{
							imgclass: "image3a block2 t3",
							imgsrc : imgpath + "fish.png"
						},
						{
							imgclass: "image4a block2 t4",
							imgsrc : imgpath + "chese.png"
						},
						{
							imgclass: "image5a block2 t5",
							imgsrc : imgpath + "curd.png"
						}
						]
					},],
					uppertextblock : [
						{
								textdata : data.string.p3text8b,
								textclass : 'hitext'
							}],
},
// slide4

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
					imageblock: [
					{
						imagetoshow: [
						{
							imgclass: " plate",
							imgsrc : imgpath + "empty-plate.png"
						},
						{
							imgclass: " food1",
						},
						{
							imgclass: "food2",
						},
						{
							imgclass: " food3",
						},
						{
							imgclass: " food4",
						},
						{
							imgclass: " food5",
						}
						]
					}],
					uppertextblock : [
						{
								textdata : data.string.p3text4,
								textclass : 'hitext'
							}],
},

// slide5

{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
					imageblock: [
					{
						imagetoshow: [
						{
							imgclass: " plate",
							imgsrc : imgpath + "empty-plate.png"
						},
						{
							imgclass: " food1",
						},
						{
							imgclass: "food2",
						},
						{
							imgclass: " food3",
						},
						{
							imgclass: " food4",
						},
						{
							imgclass: " food5",
						}
						]
					}],
					uppertextblock : [
						{
								textdata : data.string.p3text6,
								textclass : 'hitext opacity1'
							}],
},
//slide6
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
	foodwithdescription:[
		{
			foodivname: 'fooddiv1',
			foodimageclass1:'foodsrc1',
			foodimageclass2:'foodsrc2',
			foodimageclass3:'foodsrc3',
			foodimageclass4:'foodsrc4',
			foodimageclass5:'foodsrc5',
			desciptionclass:'description1',
		},
		{
			foodivname: 'fooddiv2',
			foodimageclass1:'foodsrca1',
			foodimageclass2:'foodsrca2',
			foodimageclass3:'foodsrca3',
			foodimageclass4:'foodsrca4',
			foodimageclass5:'foodsrca5',
			desciptionclass:'description2',
			descriptiontext:data.string.p3text8
		},
		{
			foodivname: 'fooddiv3',
			foodimageclass1:'foodsrcb1',
			foodimageclass2:'foodsrcb2',
			foodimageclass3:'foodsrcb3',
			foodimageclass4:'foodsrcb4',
			foodimageclass5:'foodsrcb5',
			desciptionclass:'description3',
			descriptiontext:data.string.p3text8a
		}],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "lokharkey",
				imgsrc: imgpath + "lokharke02.png"
			}
			]
		}],
		uppertextblock : [
			{
					textclass : 'suggestion template-dialougebox2-right-yellow'
				}],
},
//slide7
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
					imageblock: [
					{
						imagetoshow: [
						{
							imgclass: " plate",
							imgsrc : imgpath + "empty-plate.png"
						},
						{
							imgclass: " food1",
						},
						{
							imgclass: "food2",
						},
						{
							imgclass: " food3",
						},
						{
							imgclass: " food4",
						},
						{
							imgclass: " food5",
						}
						]
					},
					{
						containerdiv:'div1a div11',
						imagetoshow: [
						{
							imgclass: "image1a block1 s1",
							imgsrc : imgpath + "rice02.png"
						},
						{
							imgclass: "image2a block1 s2",
							imgsrc : imgpath + "sugercane.png"
						},
						{
							imgclass: "image3a block1 s3",
							imgsrc : imgpath + "wheat.png"
						},
						{
							imgclass: "image4a block1 s4",
							imgsrc : imgpath + "corn.png"
						},
						{
							imgclass: "image5a block1 s5",
							imgsrc : imgpath + "potato.png"
						}
						]
					},
					{
						containerdiv:'div1a div22',
						imagetoshow: [
						{
							imgclass: "image1a block2 r1",
							imgsrc : imgpath + "banana.png"
						},
						{
							imgclass: "image2a block2 r2",
							imgsrc : imgpath + "brocauli.png"
						},
						{
							imgclass: "image3a block2 r3",
							imgsrc : imgpath + "carrot.png"
						},
						{
							imgclass: "image4a block2 r4",
							imgsrc : imgpath + "green-saag.png"
						},
						{
							imgclass: "image5a block2 r5",
							imgsrc : imgpath + "peas.png"
						}
						]
					},
					{
						containerdiv:'div1a div33',
						imagetoshow: [
						{
							imgclass: "image1a block2 t1",
							imgsrc : imgpath + "eggban.png"
						},
						{
							imgclass: "image2a block2 t2",
							imgsrc : imgpath + "milk.png"
						},
						{
							imgclass: "image3a block2 t3",
							imgsrc : imgpath + "fish.png"
						},
						{
							imgclass: "image4a block2 t4",
							imgsrc : imgpath + "chese.png"
						},
						{
							imgclass: "image5a block2 t5",
							imgsrc : imgpath + "curd.png"
						}
						]
					}],
					uppertextblock:[{
						textclass:'directiontext',
					}]
},
//slide8
{
	contentblockadditionalclass:'ole-background-gradient-blunatic',
					imageblock: [
					{
						imagetoshow: [
						{
							imgclass: " plate",
							imgsrc : imgpath + "empty-plate.png"
						},
						{
							imgclass: " food1",
						},
						{
							imgclass: "food2",
						},
						{
							imgclass: " food3",
						},
						{
							imgclass: " food4",
						},
						{
							imgclass: " food5",
						},
						{
							imgclass: " food6",
						},
						{
							imgclass: " food7",
						}
						]
					}],
					uppertextblock:[{
						textclass:'descriptiontext',
						textdata:data.string.p3text30
					},
					{
					textdata : data.string.p3text4,
					textclass : 'hitext'
					}]
				},

					// slide9

					{
							contentblockadditionalclass:'ole-background-gradient-mustard',
							uppertextblock : [
								{
										textdata : data.string.p3text3a,
										textclass : 'hitext'
									}],
								imageblock: [
								{
									imagetoshow: [
									{
										imgclass: "plateholder",
										imgsrc : imgpath + "holdingplate.png"
									}

									]
								}]

					},
					// slide10

					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
						imageblockadditionalclass:'changetopimage',
										imageblock: [
										{
											containerdiv:'div1a',
											imagetoshow: [
											{
												imgclass: "image1a block1 x1",
												imgsrc : imgpath + "rice02.png"
											},
											{
												imgclass: "image2a block1 x2",
												imgsrc : imgpath + "sugercane.png"
											},
											{
												imgclass: "image3a block1 x3",
												imgsrc : imgpath + "wheat.png"
											},
											{
												imgclass: "image4a block1 x4",
												imgsrc : imgpath + "roti.png"
											},
											{
												imgclass: "image5a block1 x5",
												imgsrc : imgpath + "potato.png"
											}
											]
										},
										{
											containerdiv:'div1a',
											imagetoshow: [
											{
												imgclass: "image1a block2 y1",
												imgsrc : imgpath + "banana.png"
											},
											{
												imgclass: "image2a block2 y2",
												imgsrc : imgpath + "brinjal.png"
											},
											{
												imgclass: "image3a block2 y3",
												imgsrc : imgpath + "apple.png"
											},
											{
												imgclass: "image4a block2 y4",
												imgsrc : imgpath + "green-saag.png"
											},
											{
												imgclass: "image5a block2 y5",
												imgsrc : imgpath + "cauliflwoer.png"
											}
											]
										},
										{
											containerdiv:'div1a',
											imagetoshow: [
											{
												imgclass: "image1a block2 z1",
												imgsrc : imgpath + "meat01.png"
											},
											{
												imgclass: "image2a block2 z2",
												imgsrc : imgpath + "milk.png"
											},
											{
												imgclass: "image3a block2 z3",
												imgsrc : imgpath + "fish.png"
											},
											{
												imgclass: "image4a block2 z4",
												imgsrc : imgpath + "chana.png"
											},
											{
												imgclass: "image5a block2 z5",
												imgsrc : imgpath + "curd.png"
											}
											]
										},],
										uppertextblock : [
											{
													textdata : data.string.p3text8b,
													textclass : 'hitext'
												}],
					},
					// slide11

					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
										imageblock: [
										{
											imagetoshow: [
											{
												imgclass: " plate",
												imgsrc : imgpath + "empty-plate.png"
											},
											{
												imgclass: " foodx1",
											},
											{
												imgclass: "foodx2",
											},
											{
												imgclass: " foodx3",
											},
											{
												imgclass: " foodx4",
											},
											{
												imgclass: " foodx5",
											}
											]
										}],
										uppertextblock : [
											{
													textdata : data.string.p3text4a,
													textclass : 'hitext'
												}],
					},

					// slide12

					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
										imageblock: [
										{
											imagetoshow: [
											{
												imgclass: " plate",
												imgsrc : imgpath + "empty-plate.png"
											},
											{
												imgclass: " foodx1",
											},
											{
												imgclass: "foodx2",
											},
											{
												imgclass: " foodx3",
											},
											{
												imgclass: " foodx4",
											},
											{
												imgclass: " foodx5",
											}
											]
										}],
										uppertextblock : [
											{
													textdata : data.string.p3text6,
													textclass : 'hitext opacity1'
												}],
					},
					//slide13
					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
						foodwithdescription:[
							{
								foodivname: 'fooddiv1',
								foodimageclass1:'foodsrcx1',
								foodimageclass2:'foodsrcx2',
								foodimageclass3:'foodsrcx3',
								foodimageclass4:'foodsrcx4',
								foodimageclass5:'foodsrcx5',
								desciptionclass:'description1',
							},
							{
								foodivname: 'fooddiv2',
								foodimageclass1:'foodsrcy1',
								foodimageclass2:'foodsrcy2',
								foodimageclass3:'foodsrcy3',
								foodimageclass4:'foodsrcy4',
								foodimageclass5:'foodsrcy5',
								desciptionclass:'description2',
								descriptiontext:data.string.p3text8
							},
							{
								foodivname: 'fooddiv3',
								foodimageclass1:'foodsrcz1',
								foodimageclass2:'foodsrcz2',
								foodimageclass3:'foodsrcz3',
								foodimageclass4:'foodsrcz4',
								foodimageclass5:'foodsrcz5',
								desciptionclass:'description3',
								descriptiontext:data.string.p3text8a
							}],
							imageblock: [
							{
								imagetoshow: [
								{
									imgclass: "lokharkey",
									imgsrc: imgpath + "lokharke02.png"
								}
								]
							}],
							uppertextblock : [
								{
										textclass : 'suggestion template-dialougebox2-right-yellow'
									}],
					},
					//slide14
					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
										imageblock: [
										{
											imagetoshow: [
											{
												imgclass: " plate",
												imgsrc : imgpath + "empty-plate.png"
											},
											{
												imgclass: "foodx1",
											},
											{
												imgclass: "foodx2",
											},
											{
												imgclass: "foodx3",
											},
											{
												imgclass: "foodx4",
											},
											{
												imgclass: "foodx5",
											}
											]
										},
										{
											containerdiv:'div1a div11',
											imagetoshow: [
											{
												imgclass: "image1a block1 x1",
												imgsrc : imgpath + "rice02.png"
											},
											{
												imgclass: "image2a block1 x2",
												imgsrc : imgpath + "sugercane.png"
											},
											{
												imgclass: "image3a block1 x3",
												imgsrc : imgpath + "wheat.png"
											},
											{
												imgclass: "image4a block1 x4",
												imgsrc : imgpath + "corn.png"
											},
											{
												imgclass: "image5a block1 x5",
												imgsrc : imgpath + "potato.png"
											}
											]
										},
										{
											containerdiv:'div1a div22',
											imagetoshow: [
											{
												imgclass: "image1a block2 y1",
												imgsrc : imgpath + "banana.png"
											},
											{
												imgclass: "image2a block2 y2",
												imgsrc : imgpath + "brocauli.png"
											},
											{
												imgclass: "image3a block2 y3",
												imgsrc : imgpath + "carrot.png"
											},
											{
												imgclass: "image4a block2 y4",
												imgsrc : imgpath + "green-saag.png"
											},
											{
												imgclass: "image5a block2 y5",
												imgsrc : imgpath + "peas.png"
											}
											]
										},
										{
											containerdiv:'div1a div33',
											imagetoshow: [
											{
												imgclass: "image1a block2 z1",
												imgsrc : imgpath + "eggban.png"
											},
											{
												imgclass: "image2a block2 z2",
												imgsrc : imgpath + "milk.png"
											},
											{
												imgclass: "image3a block2 z3",
												imgsrc : imgpath + "fish.png"
											},
											{
												imgclass: "image4a block2 z4",
												imgsrc : imgpath + "chese.png"
											},
											{
												imgclass: "image5a block2 z5",
												imgsrc : imgpath + "curd.png"
											}
											]
										}],
										uppertextblock:[{
											textclass:'directiontext',
										}]
					},
					//slide15
					{
						contentblockadditionalclass:'ole-background-gradient-mustard',
										imageblock: [
										{
											imagetoshow: [
											{
												imgclass: " plate",
												imgsrc : imgpath + "empty-plate.png"
											},
											{
												imgclass: " foodx1",
											},
											{
												imgclass: "foodx2",
											},
											{
												imgclass: " foodx3",
											},
											{
												imgclass: " foodx4",
											},
											{
												imgclass: " foodx5",
											},
											{
												imgclass: " foodx6",
											},
											{
												imgclass: " foodx7",
											}
											]
										}],
										uppertextblock:[{
											textclass:'descriptiontext',
											textdata:data.string.p3text30
										},
										{
										textdata : data.string.p3text4a,
										textclass : 'hitext'
										}]
									},
									//slide16
									{
										contentblockadditionalclass:'ole-background-gradient-mustard',
														imageblock: [
														{
															imagetoshow: [
															{
																imgclass: " plate p1",
																imgsrc : imgpath + "empty-plate.png"
															},
															{
																imgclass: " foodx1 f1",
															},
															{
																imgclass: "foodx2 f2",
															},
															{
																imgclass: " foodx3 f3 ",
															},
															{
																imgclass: " foodx4 f4",
															},
															{
																imgclass: " foodx5 f5",
															},
															{
																imgclass: " foodx6 f6",
															},
															{
																imgclass: " foodx7 f7",
															},
															{
																imgclass: " plate p2",
																imgsrc : imgpath + "empty-plate.png"
															},
															{
																imgclass: " food1 f8",
															},
															{
																imgclass: "food2 f9",
															},
															{
																imgclass: " food3 f10",
															},
															{
																imgclass: " food4 f11",
															},
															{
																imgclass: " food5 f12",
															},
															{
																imgclass: " food6 f13",
															},
															{
																imgclass: " food7 f14",
															}
															]
														}],
														uppertextblock:[
														{
														textdata : data.string.p3text4a,
														textclass : 'hitext  h1'
													},
													{
													textdata : data.string.p3text4,
													textclass : 'hitext h2'
													}]
												}

];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var clicks = 0;
    var current_sound;
	var  inputname = '';


	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images
            {id: "coverpageImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "bgmain", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hen", src: imgpath+"chicken.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pond", src: imgpath+"pond02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shed", src: imgpath+"goth.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree2", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flower", src: imgpath+"flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "man", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
            //textboxes
            {id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
            {id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
            // // sounds
            {id: "sound_4", src: soundAsset+"s3_p4.ogg"},
            {id: "sound_11", src: soundAsset+"s3_p11.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
    }
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$prevBtn.hide(0);
		switch(countNext){
			case 0:
				play_diy_audio();
				navigationcontroller();
			break;

			case 1:
						$nextBtn.hide(0);
						$('.buttonsubmit').click(function(){
								$nextBtn.show(0);
								inputname = $('#inputtext').val();
								$('#inputtext').css("pointer-events","none");
                            $nextBtn.click();
                        });
						input_box('#inputtext','.buttonsubmit');
						break;

			case 2:
						$('.hitext').addClass('fadein');
						$('.nametext').html(inputname);
						navigationcontroller();
						break;
			case 3:
				        sound_player("sound_4",false);
						$('.hitext').addClass('fadein');
						$nextBtn.hide(0);
						clicks = 0;
						$('.image1a').animate({left:'5%'},1000);
						$('.image2a').delay(1000).animate({left:'24%'},800);
						$('.image3a').delay(1800).animate({left:'43%'},600);
						$('.image4a').delay(2400).animate({left:'62%'},500);
						$('.image5a').delay(2900).animate({left:'81%'},400);

						selector('.s1',0,foods);
						selector('.s2',1,foods);
						selector('.s3',2,foods);
						selector('.s4',3,foods);
						selector('.s5',4,foods);

						selector('.r1',5,foods);
						selector('.r2',6,foods);
						selector('.r3',7,foods);
						selector('.r4',8,foods);
						selector('.r5',9,foods);

						selector('.t1',10,foods);
						selector('.t2',11,foods);
						selector('.t3',12,foods);
						selector('.t4',13,foods);
						selector('.t5',14,foods);


						$prevBtn.hide(0);
						break;
			case 4:
				        current_sound.stop();
						$('.nametext').html(inputname);
						$('.hitext').addClass('fadein');
						find_selected(foods);
						$prevBtn.hide(0);
						$nextBtn.show(0);

						break;
			case 5:
				        navigationcontroller();
						find_selected1(foods);
						break;
			case 6:
			 			find1a(foods);
						find1b(foods);
						find1c(foods);
						deciding_text();
						navigationcontroller();
						break;
			case 7:
						$nextBtn.hide(0);
						$('.image1a').animate({left:'5%'},1000);
						$('.image2a').delay(1000).animate({left:'24%'},800);
						$('.image3a').delay(1800).animate({left:'43%'},600);
						$('.image4a').delay(2400).animate({left:'62%'},500);
						$('.image5a').delay(2900).animate({left:'81%'},400);
						selectorax('.s1',0,foods);
						selectorax('.s2',1,foods);
						selectorax('.s3',2,foods);
						selectorax('.s4',3,foods);
						selectorax('.s5',4,foods);

						selectorax('.r1',5,foods);
						selectorax('.r2',6,foods);
						selectorax('.r3',7,foods);
						selectorax('.r4',8,foods);
						selectorax('.r5',9,foods);

						selectorax('.t1',10,foods);
						selectorax('.t2',11,foods);
						selectorax('.t3',12,foods);
						selectorax('.t4',13,foods);
						selectorax('.t5',14,foods);
						$('.plate,.food1,.food2,.food3,.food4,.food5').css({opacity:".1"});
						find_selected1a(foods);
						show_options();
						break;
			case 8:
				        navigationcontroller();
						find_selected1b(foods);
						$('.hitext').addClass('fadein');
						$('.nametext').html(inputname);
						break;
			case 9:
						$('.hitext').addClass('fadein');
						$('.nametext').html(inputname);
						$prevBtn.hide(0);
						$nextBtn.show(0);
						break;
			case 10:
                sound_player("sound_11",false);
                $('.hitext').addClass('fadein');
						$nextBtn.hide(0);
						clicks = 0;
						$('.image1a').animate({left:'5%'},1000);
						$('.image2a').delay(1000).animate({left:'24%'},800);
						$('.image3a').delay(1800).animate({left:'43%'},600);
						$('.image4a').delay(2400).animate({left:'62%'},500);
						$('.image5a').delay(2900).animate({left:'81%'},400);

						selectorb('.x1',0,dinner_foods);
						selectorb('.x2',1,dinner_foods);
						selectorb('.x3',2,dinner_foods);
						selectorb('.x4',3,dinner_foods);
						selectorb('.x5',4,dinner_foods);

						selectorb('.y1',5,dinner_foods);
						selectorb('.y2',6,dinner_foods);
						selectorb('.y3',7,dinner_foods);
						selectorb('.y4',8,dinner_foods);
						selectorb('.y5',9,dinner_foods);

						selectorb('.z1',10,dinner_foods);
						selectorb('.z2',11,dinner_foods);
						selectorb('.z3',12,dinner_foods);
						selectorb('.z4',13,dinner_foods);
						selectorb('.z5',14,dinner_foods);


						$prevBtn.hide(0);
						break;
			case 11:
                        current_sound.stop();
						$('.nametext').html(inputname);
						$('.hitext').addClass('fadein');
						find_selectedx1(dinner_foods);
						navigationcontroller();
						break;
			case 12:
						find_selectedy1(dinner_foods);
						navigationcontroller();
						break;
			case 13:
			 			find1x(dinner_foods);
						find1y(dinner_foods);
						find1z(dinner_foods);
						deciding_text_dinner();
						navigationcontroller();
						break;
			case 14:
						$nextBtn.hide(0);
						clicks = 0;
						$('.image1a').animate({left:'5%'},1000);
						$('.image2a').delay(1000).animate({left:'24%'},800);
						$('.image3a').delay(1800).animate({left:'43%'},600);
						$('.image4a').delay(2400).animate({left:'62%'},500);
						$('.image5a').delay(2900).animate({left:'81%'},400);
						selectorc('.x1',0,dinner_foods);
						selectorc('.x2',1,dinner_foods);
						selectorc('.x3',2,dinner_foods);
						selectorc('.x4',3,dinner_foods);
						selectorc('.x5',4,dinner_foods);

						selectorc('.y1',5,dinner_foods);
						selectorc('.y2',6,dinner_foods);
						selectorc('.y3',7,dinner_foods);
						selectorc('.y4',8,dinner_foods);
						selectorc('.y5',9,dinner_foods);

						selectorc('.z1',10,dinner_foods);
						selectorc('.z2',11,dinner_foods);
						selectorc('.z3',12,dinner_foods);
						selectorc('.z4',13,dinner_foods);
						selectorc('.z5',14,dinner_foods);
						$('.plate,.foodx1,.foodx2,.foodx3,.foodx4,.foodx5').css({opacity:".1"});
						find_selectedx1(dinner_foods);
						show_options_dinner();
						break;
			case 15:
						find_selectedyy1(dinner_foods);
						$('.hitext').addClass('fadein');
						$('.nametext').html(inputname);
						navigationcontroller();
						break;
			case 16:
						find_selected1bx(foods);
						$('.hitext').addClass('fadein');
						find_selectedyy1x(dinner_foods);
						$('.nametext').html(inputname);
				        navigationcontroller();
						break;
					}
	}

// array update function for selected foods
function selector(selectclass,item_array_number,array_name)
{
	$(selectclass).click(function(){
			if($(selectclass).hasClass('opt1'))
			{
					$(selectclass).removeClass("glow opt1");
					array_name[item_array_number][0] = false;
					clicks = clicks - 1;
				}
				else{
					if(clicks==4)
					{
						$nextBtn.show(0);
					}
					if(clicks<=4)
						{
									$(selectclass).addClass("glow");
									$(selectclass).addClass("opt1");
									clicks = clicks + 1;
									array_name[item_array_number][0] = true;
						}
					}
	});
}

// array update function for selected foods
function selectorax(selectclass,item_array_number,array_name)
{
	clicks = 0;
	$(selectclass).click(function(){
			if($(selectclass).hasClass('opt1'))
			{
					$(selectclass).removeClass("glow opt1");
					array_name[item_array_number][0] = false;
					clicks = clicks - 1;
				}
				else{
					if(clicks==1)
					{
						$nextBtn.show(0);
					}
					if(clicks<=1)
						{
									$(selectclass).addClass("glow");
									$(selectclass).addClass("opt1");
									clicks = clicks + 1;
									array_name[item_array_number][0] = true;
						}
					}
	});

	// if((strengthfood == true  && protectivefood == false && bodyfood == true){
	// 	if(item_array_number<5)
	// 	{
	//
	// 	}
	// }
}

// array update function for selected foods
function selectorb(selectclass,item_array_number,array_name)
{
	$(selectclass).click(function(){
			if($(selectclass).hasClass('opt1'))
			{
					$(selectclass).removeClass("glow opt1");
					array_name[item_array_number][0] = false;
					clicks = clicks - 1;
				}
				else{
					if(clicks==4)
					{
						$nextBtn.show(0);
					}
					if(clicks<=4)
						{
									$(selectclass).addClass("glow");
									$(selectclass).addClass("opt1");
									clicks = clicks + 1;
									array_name[item_array_number][0] = true;
						}
					}
	});
}

// array update function for selected foods
function selectorc(selectclass,item_array_number,array_name)
{
	$(selectclass).click(function(){
			if($(selectclass).hasClass('opt1'))
			{
					$(selectclass).removeClass("glow opt1");
					array_name[item_array_number][0] = false;
					clicks = clicks - 1;
				}
				else{
					if(clicks==1)
					{
						$nextBtn.show(0);
					}
					if(clicks<=1)
						{
									$(selectclass).addClass("glow");
									$(selectclass).addClass("opt1");
									clicks = clicks + 1;
									array_name[item_array_number][0] = true;
						}
					}
	});
}

// to find selected foods
var k=1;
function find_selected(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true )
			{
					$('.food'+(k)).attr('src',array_name[i][1]).css("opacity","1");
					k=k+1;
			}
		}
}
var l=1;
function find_selected1(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(l)).attr('src',array_name[i][1]).css("opacity","1");
					l=l+1;
			}
		}
}

var lx=1;
function find_selectedx1(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.foodx'+(lx)).attr('src',array_name[i][1]).css("opacity","1");
					lx=lx+1;
			}
		}
}

var lxx=1;
function find_selectedy1(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.foodx'+(lxx)).attr('src',array_name[i][1]).css("opacity","1");
					lxx=lxx+1;
			}
		}
}

var lxxx=1;
function find_selectedyy1(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.foodx'+(lxxx)).attr('src',array_name[i][1]).css("opacity","1");
					lxxx=lxxx+1;
			}
		}
}

var lxxxx=1;
function find_selectedyy1x(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.foodx'+(lxxxx)).attr('src',array_name[i][1]).css("opacity","1");
					lxxxx=lxxxx+1;
			}
		}
}


var la=1;
function find_selected1a(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(la)).attr('src',array_name[i][1]).css("opacity","1");
					la=la+1;
			}
		}
}

var laa=1;
function find_selected1aa(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(laa)).attr('src',array_name[i][1]).css("opacity","1");
					laa=laa+1;
			}
		}
}

var laaa=1;
function find_selected1aaa(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(laaa)).attr('src',array_name[i][1]).css("opacity","1");
					laaa=laaa+1;
			}
		}
}

var aa=1;
function find_selected1b(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(aa)).attr('src',array_name[i][1]).css("opacity","1");
					aa=aa+1;
			}
		}
}

var aax=1;
function find_selected1bx(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true)
			{
					$('.food'+(aax)).attr('src',array_name[i][1]).css("opacity","1");
					aax=aax+1;
			}
		}
}

function capitalizeFirstLetter(string) {
	console.log(string);
	string[1]=string[1].toUpperCase();
	return string;
}
var p=1;
var u = 0;
function find1a(array_name)
{

		for (var j = 0; j < array_name.length; j++) {
			if(array_name[j][0]==true && array_name[j][2]=='body_building_foods')
			{
					bodyfood = true;
					$('.fooddiv1').css({display:"block"});
					$('.foodsrc'+(p)).attr('src',array_name[j][1]).css("opacity","1");
					body_building_foods_names.push(' '+array_name[j][3]);
					p=p+1;
					u=u+1;

			}
		}
		data.string.p3text7= body_building_foods_names + ' ' + data.string.p3text7;
		var foodNames = $lang=="en"?data.string.p3text7.split(""):data.string.p3text7;
		var name = capitalizeFirstLetter(foodNames);
		$('.description1').html(name);
}

var m=1;
function find1b(array_name)
{
		for (var x = 0; x < array_name.length; x++) {
			if(array_name[x][0]==true && array_name[x][2]=='protective_foods')
			{
				protectivefood = true;
				$('.fooddiv2').css({display:"block"});
					$('.foodsrca'+(m)).attr('src',array_name[x][1]).css("opacity","1");
					protective_foods_names.push(' '+array_name[x][3]);
					m=m+1;
			}
		}
		data.string.p3text8= protective_foods_names + ' ' + data.string.p3text8;
		var foodNames = $lang=="en"?data.string.p3text8.split(""):data.string.p3text8;
		var name = capitalizeFirstLetter(foodNames);
		$('.description2').html(name);
}

var n=1;
function find1c(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true && array_name[i][2]=='strength_foods')
			{
					strengthfood=true;
					$('.fooddiv3').css({display:"block"});
					$('.foodsrcb'+(n)).attr('src',array_name[i][1]).css("opacity","1");
					strength_foods_names.push(' '+array_name[i][3]);
					n=n+1;
			}
		}
		data.string.p3text8a= strength_foods_names + ' ' + data.string.p3text8a;
		// var foodNames = data.string.p3text8a.split("");
		var foodNames = $lang=="en"?data.string.p3text8a.split(""):data.string.p3text8a;
		var name = capitalizeFirstLetter(foodNames);
		$('.description3').html(name);
}

var px=1;
var ux = 0;
function find1x(array_name)
{

		for (var j = 0; j < array_name.length; j++) {
			if(array_name[j][0]==true && array_name[j][2]=='strength_foods')
			{
					 strengthfood_dinner= true;
					$('.fooddiv1').css({display:"block"});
					$('.foodsrcx'+(px)).attr('src',array_name[j][1]).css("opacity","1");
					body_building_foods_names_dinner.push(' '+array_name[j][3]);
					px=px+1;
					ux=ux+1;

			}
		}
		data.string.p3text8ax= body_building_foods_names_dinner + ' ' + data.string.p3text8ax;
		// var foodNames = data.string.p3text8ax.split("");
		var foodNames = $lang=="en"?data.string.p3text8ax.split(""):data.string.p3text8ax;
		var name = capitalizeFirstLetter(foodNames);
		$('.description1').html(name);
		// $('.description1').html(data.string.p3text8ax);
}

var mx=1;
function find1y(array_name)
{
		for (var x = 0; x < array_name.length; x++) {
			if(array_name[x][0]==true && array_name[x][2]=='protective_foods')
			{
				protectivefood_dinner = true;
				$('.fooddiv2').css({display:"block"});
					$('.foodsrcy'+(mx)).attr('src',array_name[x][1]).css("opacity","1");;
					protective_foods_names_dinner.push(' '+array_name[x][3]);
					mx=mx+1;
			}
		}
		data.string.p3text8x= protective_foods_names_dinner + ' ' + data.string.p3text8x;
		// var foodNames = data.string.p3text8x.split("");
		var foodNames = $lang=="en"?data.string.p3text8x.split(""):data.string.p3text8x;
		var name = capitalizeFirstLetter(foodNames);
		$('.description2').html(name);
		// $('.description2').html(data.string.p3text8x);
}

var nx=1;
function find1z(array_name)
{
		for (var i = 0; i < array_name.length; i++) {
			if(array_name[i][0]==true && array_name[i][2]=='body_building_foods')
			{
					bodyfood_dinner=true;
					$('.fooddiv3').css({display:"block"});
					$('.foodsrcz'+(nx)).attr('src',array_name[i][1]).css("opacity","1");;
					strength_foods_names_dinner.push(' '+array_name[i][3]);
					nx=nx+1;
			}
		}
		data.string.p3text7x= strength_foods_names_dinner + ' ' + data.string.p3text7x;
		// var foodNames = data.string.p3text7x.split("");
		var foodNames = $lang=="en"?data.string.p3text7x.split(""):data.string.p3text7x;
		var name = capitalizeFirstLetter(foodNames);
		$('.description3').html(name);
}


function deciding_text(){
	if(bodyfood==true  && strengthfood==false && protectivefood==false)
	{
		$('.suggestion').html(data.string.p3text9);
	}
	else if(bodyfood==false  && protectivefood==true && strengthfood == false )
	{
		$('.suggestion').html(data.string.p3text11);
	}
	else if(bodyfood == false  && protectivefood == false && strengthfood == true )
	{
		$('.suggestion').html(data.string.p3text13);
	}
	else if(bodyfood == true  && protectivefood == true && strengthfood == false )
	{
		$('.suggestion').html(data.string.p3text15);
	}
	else if(bodyfood == false  && protectivefood == true && strengthfood == true )
	{
		$('.suggestion').html(data.string.p3text19);
	}
	else if(bodyfood == true  && protectivefood == false && strengthfood == true )
	{
		$('.suggestion').html(data.string.p3text17);
	}
	else if(bodyfood == true  && protectivefood == true && strengthfood == true )
	{
		$('.suggestion').html(data.string.p3text21);
	}
}

function deciding_text_dinner(){
	if(bodyfood_dinner==true  && strengthfood_dinner==false && protectivefood_dinner==false)
	{
		$('.suggestion').html(data.string.p3text9);
	}
	else if(bodyfood_dinner==false  && protectivefood_dinner==true && strengthfood_dinner == false )
	{
		$('.suggestion').html(data.string.p3text11);
	}
	else if(bodyfood_dinner == false  && protectivefood_dinner == false && strengthfood_dinner == true )
	{
		$('.suggestion').html(data.string.p3text13);
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == true && strengthfood_dinner == false )
	{
		$('.suggestion').html(data.string.p3text15);
	}
	else if(bodyfood_dinner == false  && protectivefood_dinner == true && strengthfood_dinner == true )
	{
		$('.suggestion').html(data.string.p3text19);
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == false && strengthfood_dinner == true )
	{
		$('.suggestion').html(data.string.p3text17);
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == true && strengthfood_dinner == true )
	{
		$('.suggestion').html(data.string.p3text21);
	}
}

function show_options(){
	if(bodyfood==true  && strengthfood==false && protectivefood==false)
	{
		$('.directiontext').html(data.string.p3text29);
		$('.div22').css({display:"block"});
		$('.div11').css({display:"block"});
	}
	else if(bodyfood==false  && protectivefood==true && strengthfood == false )
	{
		$('.directiontext').html(data.string.p3text28);
		$('.div11').css({display:"block"});
		$('.div33').css({display:"block"});
	}
	else if(bodyfood == false  && protectivefood == false && strengthfood == true )
	{
		$('.directiontext').html(data.string.p3text27);
		$('.div22').css({display:"block"});
		$('.div33').css({display:"block"});
	}
	else if(bodyfood == true  && protectivefood == true && strengthfood == false )
	{
		$('.directiontext').html(data.string.p3text24);
		$('.div11').css({display:"block"});
	}
	else if(bodyfood == false  && protectivefood == true && strengthfood == true )
	{
		$('.directiontext').html(data.string.p3text26);
		$('.div33').css({display:"block"});
	}
	else if(bodyfood == true  && protectivefood == false && strengthfood == true )
	{
		$('.directiontext').html(data.string.p3text25);
		$('.div22').css({display:"block"});
	}
	else if(bodyfood == true  && protectivefood == true && strengthfood == true )
	{
		countNext++;
		templateCaller();
	}
}

function show_options_dinner(){
	if(bodyfood_dinner==true  && strengthfood_dinner==false && protectivefood_dinner==false)
	{
		$('.directiontext').html(data.string.p3text29);
		$('.div22').css({display:"block"});
		$('.div11').css({display:"block"});
	}
	else if(bodyfood_dinner==false  && protectivefood_dinner==true && strengthfood_dinner == false )
	{
		$('.directiontext').html(data.string.p3text28);
		$('.div11').css({display:"block"});
		$('.div33').css({display:"block"});
	}
	else if(bodyfood_dinner == false  && protectivefood_dinner == false && strengthfood_dinner == true )
	{
		$('.directiontext').html(data.string.p3text27);
		$('.div22').css({display:"block"});
		$('.div33').css({display:"block"});
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == true && strengthfood_dinner == false )
	{
		$('.directiontext').html(data.string.p3text24);
		$('.div11').css({display:"block"});
	}
	else if(bodyfood_dinner == false  && protectivefood_dinner == true && strengthfood_dinner == true )
	{
		$('.directiontext').html(data.string.p3text26);
		$('.div33').css({display:"block"});
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == false && strengthfood_dinner == true )
	{
		$('.directiontext').html(data.string.p3text25);
		$('.div22').css({display:"block"});
	}
	else if(bodyfood_dinner == true  && protectivefood_dinner == true && strengthfood_dinner == true )
	{
		countNext++;
		templateCaller();
	}
}


// for entering the value in input box
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if(charCode === 13 && button_class!=null) {
						$(button_class).trigger("click");
			}
			var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, shift, caps , backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
						return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if((charCode < 65 || charCode > 90)){
					return false;
				}
				return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});
	}

function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	// $refreshBtn.click(function(){
	// 	templateCaller();
	// });

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// templateCaller();
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	//slide0
	{
		contentblockadditionalclass:'color_background',
		uppertextblock:[
		{
			textdata: data.string.p5text1,
			textclass: 'question'
		}]

	},
	//slide1
	{
		contentblockadditionalclass:'color_background',
		uppertextblock:[
		{
			textdata: data.string.p5text1,
			textclass: 'question'
		},
		{
			textdata: data.string.p5text2,
			textclass: 'desc1'
		}]

	},
	//slide2
	{
		contentblockadditionalclass:'color_background',
		uppertextblock:[
		{
			textdata: data.string.p5text1,
			textclass: 'question'
		},
		{
			textdata: data.string.p5text2,
			textclass: 'desc1'
		},
		{
			textdata: data.string.p5text3,
			textclass: 'desc2'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "undereat",
								imgsrc : imgpath + "malnutritioned.png"
							},
							{
								imgclass: "overeat",
								imgsrc : imgpath + "overeat.png"
							},


					]
			}
		]

	},
	//slide3
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text4,
			textclass: 'diseases_text'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "hand",
								imgsrc : imgpath + "diseases.png"
							}


					]
			}
		]

	},
	//slide4
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text5,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "vitamin_a.png"
							}


					]
			}
		]

	},
	//slide5
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text6,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "malnutritioned.png"
							}


					]
			}
		]

	},
	//slide6
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text7,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "overeat.png"
							}


					]
			}
		]

	},
	//slide7
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text8,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "anemia-and-normal-blood.png"
							}


					]
			}
		]

	},
	//slide8
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text9,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "tooth-ache01.png"
							}


					]
			}
		]

	},
	//slide9
	{
		contentblockadditionalclass:'color_background2',
		uppertextblock:[
		{
			textdata: data.string.p5text10,
			textclass: 'desc_text',
			datahighlightflag: true,
			datahighlightcustomclass:'orange'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "desc_img",
								imgsrc : imgpath + "shokuyoku_woman.gif"
							}


					]
			}
		]

	}


];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	var clicks = 0;

	var  inputname = '';


	var $total_page = content.length;

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images
            {id: "coverpageImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "bgmain", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hen", src: imgpath+"chicken.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pond", src: imgpath+"pond02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shed", src: imgpath+"goth.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree2", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flower", src: imgpath+"flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "man", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
            //textboxes
            {id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
            {id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
            // // sounds
            {id: "sound_1", src: soundAsset+"s5_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s5_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s5_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s5_p4.ogg"},
            {id: "sound_5", src: soundAsset+"s5_p5.ogg"},
            {id: "sound_6", src: soundAsset+"s5_p6.ogg"},
            {id: "sound_7", src: soundAsset+"s5_p7.ogg"},
            {id: "sound_8", src: soundAsset+"s5_p8.ogg"},
            {id: "sound_9", src: soundAsset+"s5_p9.ogg"},
            {id: "sound_10", src: soundAsset+"s5_p10.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


      function navigationcontroller(islastpageflag){
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
    }
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		$prevBtn.hide(0);
        sound_player("sound_"+(countNext+1),true);
        switch(countNext){

			case 1:
						$('.question').animate({top: "7%"},500);
						$('.desc1').delay(500).animate({opacity: 1},500);
						break;

			case 2:
						$('.question').css({top: "7%"},500);
						$('.desc1').css({opacity: 1},500);
						$('.desc1,.desc2,.overeat,.undereat').animate({opacity: 1},500);
						break;
			case 3:
			$('.hand').animate({width:"43%"},700);
			$('.diseases_text').delay(700).animate({opacity:1},500);
						break;
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
						$('.desc_text').delay(700).animate({opacity: 1},800);
						$('.desc_img').animate({width:"90%"},700);
						break;

					}
	}


// for entering the value in input box
	function input_box(input_class, button_class) {
		$(input_class).keydown(function(event){
				var charCode = (event.which) ? event.which : event.keyCode;
				/* charCodes 8 => backspace, 48 - 57 => 0 -9, 97-105 => num(1-9), 37-40 => arrowkeys, 110=>., 190=>., 46=> del */
				if(charCode === 13 && button_class!=null) {
						$(button_class).trigger("click");
			}
			var condition = charCode != 8 && charCode != 16 && charCode != 20 && (charCode < 37 || charCode > 40) && charCode != 46;
			//check if user inputs del, shift, caps , backspace or arrow keys
				if (!condition) {
					return true;
				}
				//check if user inputs more than one '.'
			if((charCode == 190 || charCode == 110) && event.target.value.split('.').length >= 2) {
						return false;
				}
				//check . and 0-9 separately after checking arrow and other keys
				if((charCode < 65 || charCode > 90)){
					return false;
				}
				return true;
		});
		$(input_class).keyup(function(event){
    		if (String(event.target.value).length >= 1) {
    			$(".sbmtbtn").show(0);
    			$(button_class).show(0);
    			global_save_val = String(event.target.value);
    		}
    		else{
    			$(".sbmtbtn").hide(0);
    			$(button_class).hide(0);
    		}
  			return true;
		});
	}

function playaudio(sound_data, $dialog_container){
		var playing = true;
		$dialog_container.removeClass("playable");
		$dialog_container.click(function(){
			if(!playing){
				playaudio(sound_data, $dialog_container);
			}
			return false;
		});
		$prevBtn.hide(0);
		if((countNext+1) == content.length){
			ole.footerNotificationHandler.hideNotification();
		}else{
			$nextBtn.hide(0);
		}
		sound_data.play();
		sound_data.bind('ended', function(){
			setTimeout(function(){
				$prevBtn.show(0);
				$dialog_container.addClass("playable");
				playing = false;
				sound_data.unbind('ended');
				if((countNext+1) == content.length){
					ole.footerNotificationHandler.pageEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			}, 1000);
		});
	}
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	// });

});


 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

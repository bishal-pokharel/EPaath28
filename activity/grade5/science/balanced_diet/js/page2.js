var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
	//slide0
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'kitchen',

		uppertextblock : [{
			textdata : data.string.p2text1,
			textclass : 'top_text'
		}],
	},

	//slide1
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text2,
			textclass: 'boytext'
		},
		{
			textdata: data.string.p1text6,
			textclass:'carbs_text fadein1'
		},
		{
			textdata: data.string.p1text7,
			textclass:'vits_text fadein1'
		},
		{
			textdata: data.string.p1text8,
			textclass:'fat_text fadein1'
		},
		{
			textdata: data.string.p1text9,
			textclass:'pro_text fadein1'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "carbs",
								imgsrc : imgpath + "Carbohydrates.png"
							},
							{
								imgclass: "vits",
								imgsrc : imgpath + "Vitamins-and-Minerals.png"
							},
							{
								imgclass: "fats",
								imgsrc : imgpath + "Fats.png"
							},
							{
								imgclass: "pros",
								imgsrc : imgpath + "Proteins.png"
							},
							{
								imgclass: "boy",
								imgsrc : imgpath + "boy2.png"
							}
					]
			}
		]

	},
	//slide2
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text2,
			textclass: 'boytext'
		},
		{
			textdata: data.string.p1text6,
			textclass:'carbs_text fadein1'
		},
		{
			textdata: data.string.p1text7,
			textclass:'vits_text fadein1'
		},
		{
			textdata: data.string.p1text8,
			textclass:'fat_text fadein1'
		},
		{
			textdata: data.string.p1text9,
			textclass:'pro_text fadein1'
		},
		{
			textdata: data.string.p2text3,
			textclass:'boytext2 fadein'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "carbs",
								imgsrc : imgpath + "Carbohydrates.png"
							},
							{
								imgclass: "vits",
								imgsrc : imgpath + "Vitamins-and-Minerals.png"
							},
							{
								imgclass: "fats",
								imgsrc : imgpath + "Fats.png"
							},
							{
								imgclass: "pros",
								imgsrc : imgpath + "Proteins.png"
							},
							{
								imgclass: "boy",
								imgsrc : imgpath + "boy2.png"
							}
					]
			}
		]

	},
	//slide3
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text4,
			textclass: 'top_text'
		},
		{
			textdata: data.string.p2text5,
			textclass:'dialogue1 template-dialougebox2-top-white'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "boywithapple",
								imgsrc : imgpath + "boy3.png"
							},
							{
								imgclass: "food",
								imgsrc : imgpath + "page23.png"
							}
					]
				}]
	},
	//slide4
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text5a,
			textclass: 'top_text big_font'
		},
		{
			textdata: data.string.p2text6,
			textclass:'about1'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "balancedietimage",
								imgsrc : imgpath + "balanced-diet.png"
							}
					]
				}]
	},
	//slide5
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text5a,
			textclass: 'top_text big_font'
		},
		{
			textdata: data.string.p2text6,
			textclass:'about1'
		},
		{
			textdata: data.string.p2text7,
			textclass:'about2'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "balancedietimage",
								imgsrc : imgpath + "balanced-diet.png"
							}
					]
				}]
	},
	//slide6
	{
		contentblockadditionalclass:'ole-background-gradient-morningsky',
		uppertextblock:[
		{
			textdata: data.string.p2text5b,
			textclass: 'top_text big_font'
		},
		{
			textdata: data.string.p2text8,
			textclass:'about1'
		}],
		imageblock: [
			{
					imagetoshow: [
							{
								imgclass: "balancedietimage",
								imgsrc : imgpath + "junk-food.png"
							}
					]
				}]
	}

];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
    var current_sound;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            // {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
            // {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
            //   ,
            //images
            {id: "coverpageImg", src: imgpath+"coverpage.jpg", type: createjs.AbstractLoader.IMAGE},
            {id: "bgmain", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hen", src: imgpath+"chicken.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pond", src: imgpath+"pond02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "house", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
            {id: "shed", src: imgpath+"goth.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "tree2", src: imgpath+"tree03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flower", src: imgpath+"flower.png", type: createjs.AbstractLoader.IMAGE},
            {id: "man", src: imgpath+"man.png", type: createjs.AbstractLoader.IMAGE},
            {id: "stone", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
            //textboxes
            {id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
            {id: "tb-1", src: 'images/textbox/white/lb-1.png', type: createjs.AbstractLoader.IMAGE},
            // // sounds
            {id: "sound_1", src: soundAsset+"s2_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s2_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s2_p3.ogg"},
            {id: "sound_4", src: soundAsset+"s2_p4.ogg"},
            {id: "sound_4_1", src: soundAsset+"s2_p4_1.ogg"},
            {id: "sound_5", src: soundAsset+"s2_p5.ogg"},
            {id: "sound_6", src: soundAsset+"s2_p6.ogg"},
            {id: "sound_7", src: soundAsset+"s2_p7.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }
    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded*100)+'%');
    }
    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }
    //initialize
    init();

	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
    function sound_player(sound_id,navigate){
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function(){
            navigate?navigationcontroller():"";
        });
    }
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);
		loadTimelineProgress($total_page, countNext + 1);

		texthighlight($board);
        countNext!==3?sound_player("sound_"+(countNext+1),true):"";

		switch (countNext) {
			case 1:
				$('.imageblock').css({left: "15%",top: "-5%"});
				$('.boytext').animate({opacity:1},700);
				break;
			case 2:
				$('.imageblock').css({left: "15%",top: "-5%"});
				$('.boytext').css({opacity:1});
				$('.boytext2').animate({opacity:1},700);
				break;
			case 3:
                sound_player("sound_4",false);
                current_sound.on('complete', function(){
                    sound_player("sound_4_1",true);
                });
				$('.food').animate({width:"40%"});
				break;
			case 4:
				$('.about1').animate({opacity:1},700);
				break;
			case 5:
				$('.about1').css({opacity:1});
				$('.about2').animate({opacity:1},700);
				break;
				case 6:
				$('.about1').animate({opacity:1},700);
					break;
		}
	}


    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandlergen.lessonEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
                ole.footerNotificationHandler.lessonEndSetNotification() :
                ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }
	function nav_button_controls(delay_ms){
		setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}

	function templateCaller() {
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}


	$nextBtn.on("click", function() {
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}

	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		countNext--;
		templateCaller();
	    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});


	total_page = content.length;

});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

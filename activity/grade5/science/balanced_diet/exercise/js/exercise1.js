var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
    //slide 0
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'corn.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'brocauli.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'ghee.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'chana.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 1
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'potato.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'brinjal.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'curd.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'chicken.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 2
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'wheat.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'green-saag.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'chese.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'beans.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 3
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'rice.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'mango.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'oil.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'peas.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 4
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'barli.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'cauliflwoer.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'butter.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'peanuit.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 5
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'sweetpotato.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'cauliflwoer.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'milk.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'beans.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 6
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'rice_racks.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'carrot.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'chese.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'fish.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 7
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'rice_racks.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'banana01.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'oil.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'hard-boiled-egg.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },

    //slide 8
    {
        contentblockadditionalclass:'ole-background-gradient-weepysky',
        extratextblock : [
            {
                textdata : data.string.et5,
                textclass : 'instruction',
            },
            {
                textdata : data.string.et6,
                textclass : 'congratstext',
            }
        ],
        optionsblock:[{
            optionsdivclass:'carb_div',
            imgclass:'optionimg',
            imgsrc:imgpath+'pie_04.png',
            textclass:'carbPsn optiontext',
            textdata:data.string.et1
        },
            {
                optionsdivclass:'vits_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_01.png',
                textclass:'vtmnPsn optiontext',
                textdata:data.string.et2
            },
            {
                optionsdivclass:'prots_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_03.png',
                textclass:'prtnPsn optiontext',
                textdata:data.string.et3
            },
            {
                optionsdivclass:'fats_div',
                imgclass:'optionimg',
                imgsrc:imgpath+'pie_02.png',
                textclass:'fatPsn optiontext',
                textdata:data.string.et4
            }],
        imageblock:[{
            imgclass:'plate',
            imgsrc:imgpath+'empty-plate.png'
        },
            {
                imgclass:'carbs class1',
                imgsrc:imgpath+'rice.png'
            },
            {
                imgclass:'vits class2',
                imgsrc:imgpath+'turnip.png'
            },
            {
                imgclass:'fats class3',
                imgsrc:imgpath+'butter.png'
            },
            {
                imgclass:'prots class4',
                imgsrc:imgpath+'peas.png'
            },
            {
                imgclass:'congratsmonkey',
                imgsrc:imgpath+'welldone02.png'
            }]

    },


];

content.shufflearray();


$(function ()
{
    var $board    = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var current_sound;

    var $total_page = content.length;
    var score = 0;
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            //    sounds
            {id: "sound_1", src: soundAsset + "ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }
    function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;
    }

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";
                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    var rhino = new NumberTemplate();

    rhino.init($total_page);



    function generalTemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        texthighlight($board);
        countNext==0?sound_player("sound_1"):"";
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        var classcount=[1,2,3,4];
        classcount.shufflearray();
        $('.class'+classcount[0]).css({"top": "34%","left": "14%"});
        $('.class'+classcount[1]).css({"top": "34%","left": "25%"});
        $('.class'+classcount[2]).css({"top": "55%","left": "14%"});
        $('.class'+classcount[3]).css({"top": "55%","left": "25%"});

        switch(countNext){
            default:
                var wrong_dropped = false;
                var correct_count=0;
                var counterfornext=0;
                $('.carbs,.vits,.fats,.prots').draggable({
                    containment : ".generalTemplateblock",
                    revert : true,
                    cursor : "move",
                    zIndex: 100000,
                });
                $(".carb_div").droppable({
                    accept: ".carbs,.fats,.prots,.vits",
                    drop: function (event, ui){
                        if(ui.draggable.hasClass('carbs'))
                        {
                            counterfornext++;
                            play_correct_incorrect_sound(1);
                            dropfunc(event, ui, $this);
                            ui.draggable.css({"left": "55%","top": "33%","pointer-events":"none"});
                            ui.draggable({ revert: 'invalid' });
                            $('.options').css({"pointer-events": "none"});
                            $this = $(this);
                            ui.draggable.removeClass('carbs');
                        }
                        else{
                            if(!wrong_dropped){
                                wrong_dropped = true;
                                rhino.update(false);
                            }
                            play_correct_incorrect_sound(0);
                        }
                    }
                });
                $(".vits_div").droppable({
                    accept: ".carbs,.fats,.prots,.vits",
                    drop: function (event, ui){
                        if(ui.draggable.hasClass('vits'))
                        {
                            counterfornext++;
                            play_correct_incorrect_sound(1);
                            dropfunc(event, ui, $this);
                            ui.draggable.css({"left": "78%","top": "33%","pointer-events":"none"});
                            ui.draggable({ revert: 'invalid' });
                            $('.options').css({"pointer-events": "none"});
                            $this = $(this);
                        }
                        else{
                            if(!wrong_dropped){
                                wrong_dropped = true;
                                rhino.update(false);
                            }
                            play_correct_incorrect_sound(0);
                        }
                    }
                });
                $(".fats_div").droppable({
                    accept: ".carbs,.fats,.prots,.vits",
                    drop: function (event, ui){
                        if(ui.draggable.hasClass('fats'))
                        {
                            counterfornext++;
                            play_correct_incorrect_sound(1);
                            dropfunc(event, ui, $this);
                            ui.draggable.css({"left": "78%","top": "63%","pointer-events":"none"});
                            ui.draggable({ revert: 'invalid' });
                            $('.options').css({"pointer-events": "none"});
                            $this = $(this);
                        }
                        else{
                            if(!wrong_dropped){
                                wrong_dropped = true;
                                rhino.update(false);
                            }
                            play_correct_incorrect_sound(0);
                        }
                    }
                });
                $(".prots_div").droppable({
                    accept: ".carbs,.fats,.prots,.vits",
                    drop: function (event, ui){
                        if(ui.draggable.hasClass('prots'))
                        {
                            counterfornext++;
                            $this = $(this);
                            play_correct_incorrect_sound(1);
                            dropfunc(event, ui, $this);
                            ui.draggable.css({"left": "60%","top": "63%","pointer-events":"none"});
                            $('.options').css({"pointer-events": "none"});
                            ui.draggable({ revert: 'invalid' });
                        }
                        else{
                            if(!wrong_dropped){
                                wrong_dropped = true;
                                rhino.update(false);
                            }
                            play_correct_incorrect_sound(0);
                        }
                    }
                });

            function dropfunc(event, ui, $droppedOn){
                if(counterfornext==4){
                    setTimeout(function(){
                        $nextBtn.show(0);
                    },700);
                }
                if(!wrong_dropped){
                    correct_count++;
                    console.log(correct_count);
                }
                if(correct_count==4)
                {
                    rhino.update(true);
                    $('.plate').animate({"opacity":"0"},500);
                    $('.congratsmonkey,.congratstext').animate({"opacity":"1"},500);
                    // nav_button_controls(500);
                    setTimeout(function(){
                        $nextBtn.show(0);
                    },700);
                }
            }
        }

    }

    // function nav_button_controls(delay_ms){
    // 	timeoutvar = setTimeout(function(){
    // 		if(countNext==0){
    // 			$nextBtn.show(0);
    // 		} else if( countNext>0 && countNext == $total_page-1){
    // 			$prevBtn.show(0);
    // 			ole.footerNotificationHandler.lessonEndSetNotification();
    // 		} else{
    // 			$prevBtn.show(0);
    // 			$nextBtn.show(0);
    // 		}
    // 	},delay_ms);
    // }
    function templateCaller(){
        /*always hide next and previous navigation button unless
        explicitly called from inside a template*/
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');

        // call navigation controller
        navigationcontroller();

        // call the template
        generalTemplate();
        /*
        for (var i = 0; i < content.length; i++) {
          slides(i);
          $($('.totalsequence')[i]).html(i);
          $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
        "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
        }
        function slides(i){
            $($('.totalsequence')[i]).click(function(){
              countNext = i;
              templateCaller();
            });
          }
      */


    }

    // first call to template caller
    templateCaller();

    /* navigation buttons event handlers */

    $nextBtn.on("click", function(){
        countNext++;
        rhino.gotoNext();
        templateCaller();
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click',function () {
        countNext--;
        templateCaller();
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

});
var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/audio_"+$lang+"/";
var sound_0 = new buzz.sound((soundAsset + "ex.ogg"));

var content1=[
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			imgsrc: imgpath + '04.png',
			imgclass: 'center-image',
		}],
		option_class: '',
		option: [{
			textclass: 'option-text',
			textdata: data.string.e_opt1
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt2
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt3
		},
		{
			optiondivclass: 'class1',
			textclass: 'option-text',
			textdata: data.string.e_opt4
		}]
	},
	{
		//slide 1	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			imgsrc: imgpath + '01.png',
			imgclass: 'center-image',
		}],
		option_class: '',
		option: [{
			optiondivclass: 'class1',
			textclass: 'option-text',
			textdata: data.string.e_opt1
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt2
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt3
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt4
		}]
	},
	{
		//slide 3
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			imgsrc: imgpath + '02.png',
			imgclass: 'center-image',
		}],
		option_class: '',
		option: [{
			textclass: 'option-text',
			textdata: data.string.e_opt1
		},
		{
			optiondivclass: 'class1',
			textclass: 'option-text',
			textdata: data.string.e_opt2
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt3
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt4
		}]
	},
	{
		//slide 3	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			imgsrc: imgpath + '03.png',
			imgclass: 'center-image',
		}],
		option_class: '',
		option: [{
			textclass: 'option-text',
			textdata: data.string.e_opt1
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt2
		},
		{
			optiondivclass: 'class1',
			textclass: 'option-text',
			textdata: data.string.e_opt3
		},
		{
			textclass: 'option-text',
			textdata: data.string.e_opt4
		}]
	},
];


var content2=[
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			textclass: 'ques-text',
			textdata: data.string.e_opt1
		}],
		option_class: '',
		option: [{
			optiondivclass: 'class1',
			imgsrc: imgpath + '01.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '02.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '03.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '04.png',
			imgclass: 'option-image',
		}]
	},
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			textclass: 'ques-text',
			textdata: data.string.e_opt2
		}],
		option_class: '',
		option: [{
			imgsrc: imgpath + '01.png',
			imgclass: 'option-image',
		},
		{
			optiondivclass: 'class1',
			imgsrc: imgpath + '02.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '03.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '04.png',
			imgclass: 'option-image',
		}]
	},
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			textclass: 'ques-text',
			textdata: data.string.e_opt3
		}],
		option_class: '',
		option: [{
			imgsrc: imgpath + '01.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '02.png',
			imgclass: 'option-image',
		},
		{
			optiondivclass: 'class1',
			imgsrc: imgpath + '03.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '04.png',
			imgclass: 'option-image',
		}]
	},
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		ques_class: '',
		question: [{
			textclass: 'ques-text',
			textdata: data.string.e_opt4
		}],
		option_class: '',
		option: [{
			imgsrc: imgpath + '01.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '02.png',
			imgclass: 'option-image',
		},
		{
			imgsrc: imgpath + '03.png',
			imgclass: 'option-image',
		},
		{
			optiondivclass: 'class1',
			imgsrc: imgpath + '04.png',
			imgclass: 'option-image',
		}]
	}
];



var content3=[
	{
		//slide 0	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		lifecycle:true,
		
		topclass: 'hidden-class',
		
		
		topdata: data.string.e_opt4,
		leftdata: data.string.e_opt3,
		bottomdata: data.string.e_opt2,
		rightdata: data.string.e_opt1,
		
		option_class: '',
		option: [{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt1,
			imgsrc: imgpath + '01.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt2,
			imgsrc: imgpath + '02.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt3,
			imgsrc: imgpath + '03.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div class1',
			textclass: 'option-text',
			textdata: data.string.e_opt4,
			imgsrc: imgpath + '04.png',
			imgclass: '',
		}]
	},
	{
		//slide 1	
		contentblockadditionalclass: 'main-bg',
		uppertextblockadditionalclass: 'instruction',
		uppertextblock : [
			{
				textclass : "upper",
				textdata : data.string.e_ins
			}
		],
		lifecycle:true,
		
		leftclass: 'hidden-class',
		
		
		topdata: data.string.e_opt4,
		leftdata: data.string.e_opt3,
		bottomdata: data.string.e_opt2,
		rightdata: data.string.e_opt1,
		
		option_class: '',
		option: [{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt1,
			imgsrc: imgpath + '01.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt2,
			imgsrc: imgpath + '02.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div class1',
			textclass: 'option-text',
			textdata: data.string.e_opt3,
			imgsrc: imgpath + '03.png',
			imgclass: '',
		},
		{
			optiondivclass: 'mixed-div',
			textclass: 'option-text',
			textdata: data.string.e_opt4,
			imgsrc: imgpath + '04.png',
			imgclass: '',
		}]
	},
];

/*remove this for non random questions*/
content1.shufflearray();
content2.shufflearray();
content3.shufflearray();

var content = content1.concat(content2).concat(content3);

$(function (){	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

  var playing_audio;
	var $total_page = content.length;

	function navigationcontroller(islastpageflag){
		// check if the parameter is defined and if a boolean,
		// update islastpageflag accordingly
		typeof islastpageflag === "undefined" ? 
		islastpageflag = false : 
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;
	}
	
	var score = 0;

	/*values in this array is same as the name of images of eggs in image folder*/
	var testin = new RhinoTemplate();
   
 	testin.init(10);
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		/*generate question no at the beginning of question*/
		testin.numberOfQuestions();
		//randomize options
		var parent = $(".option-container");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}
		$('.question_number').html('Question: '+ (countNext+1));
		var wrong_clicked = false;
		switch(countNext){
			case 0:
                playing_audio = sound_0;
                $nextBtn.hide(0);
                playing_audio.play();
			case 1:
			case 2: 
			case 3:
			case 4:
			case 5:
			case 6: 
			case 7:
				$(".option-div").click(function(){
					if($(this).hasClass("class1")){
						if(!wrong_clicked){
							testin.update(true);
						}
						$(".option-div").css('pointer-events', 'none');
						$(this).addClass('correct-option');
						play_correct_incorrect_sound(1);
						$nextBtn.show(0);
					}
					else{
						if(!wrong_clicked){
							testin.update(false);
						}
						$(this).addClass('incorrect-option');
						wrong_clicked = true;
						play_correct_incorrect_sound(0);
					}
				}); 
				break;
			case 8: 
			case 9:
				$(".option-div").click(function(){
					if($(this).hasClass("class1")){
						if(!wrong_clicked){
							testin.update(true);
						}
						$(".option-div").css('pointer-events', 'none');
						$(this).addClass('correct-option');
						$('.hidden-class>*').fadeIn(1000);
						play_correct_incorrect_sound(1);
						$nextBtn.show(0);
					}
					else{
						if(!wrong_clicked){
							testin.update(false);
						}
						$(this).addClass('incorrect-option');
						wrong_clicked = true;
						play_correct_incorrect_sound(0);
					}
				}); 
				break;
			default:
				break;
		}
		
	}
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		testin.gotoNext();
		countNext++;
		templateCaller();	
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
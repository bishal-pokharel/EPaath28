var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/page2/";

var sound_0 = new buzz.sound((soundAsset + "recording1.ogg"));
var sound_1 = new buzz.sound((soundAsset + "recording2.ogg"));
var sound_2 = new buzz.sound((soundAsset + "recording3.ogg"));
var sound_3 = new buzz.sound((soundAsset + "recording4.ogg"));
var sound_4 = new buzz.sound((soundAsset + "recording5.ogg"));
var sound_5 = new buzz.sound((soundAsset + "recording6.ogg"));
var sound_6 = new buzz.sound((soundAsset + "recording7.ogg"));
var sound_7 = new buzz.sound((soundAsset + "recording8.ogg"));
var sound_8 = new buzz.sound((soundAsset + "recording9.ogg"));
var sound_9 = new buzz.sound((soundAsset + "recording10.ogg"));
var sound_10 = new buzz.sound((soundAsset + "recording11.ogg"));
var sound_11 = new buzz.sound((soundAsset + "recording12.ogg"));
var sound_12 = new buzz.sound((soundAsset + "recording13.ogg"));
var sound_13 = new buzz.sound((soundAsset + "recording14.ogg"));
var sound_14 = new buzz.sound((soundAsset + "recording15.ogg"));
var sound_15 = new buzz.sound((soundAsset + "recording16.ogg"));
var sound_16 = new buzz.sound((soundAsset + "recording17.ogg"));
var sound_17 = new buzz.sound((soundAsset + "recording18.ogg"));
var sound_18 = new buzz.sound((soundAsset + "recording19.ogg"));
var sound_19 = new buzz.sound((soundAsset + "recording20.ogg"));
var sound_20 = new buzz.sound((soundAsset + "recording21.ogg"));
var sound_21 = new buzz.sound((soundAsset + "recording22.ogg"));
var sound_22 = new buzz.sound((soundAsset + "recording23.ogg"));
var sound_23 = new buzz.sound((soundAsset + "recording24.ogg"));
var sound_24 = new buzz.sound((soundAsset + "recording25.ogg"));
var sound_25 = new buzz.sound((soundAsset + "recording26.ogg"));

var sound_list = [sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11, sound_12, sound_13, sound_14, sound_15, sound_16, sound_17, sound_18, sound_19, sound_20, sound_21, sound_22, sound_23, sound_24, sound_25];

var content=[
 {
  	//page 0
  	contentblockadditionalclass: "bluebg",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description",
  		textdata: data.string.p2_s1
  	},{
  		textclass: "description_bottom fadein",
  		textdata: data.string.p2_s2
  	}],
  	imageblock:[{
  		imageblockclass: "imagediv1",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"egg.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p2_s3
  		}]
  	},{
  		imageblockclass: "imagediv2",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"larva.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p2_s4
  		}]
  	},{
  		imageblockclass: "imagediv3",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"pupa.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p2_s5
  		}]
  	},{
  		imageblockclass: "imagediv4",
  		imagestoshow:[{
  			imgclass: "image",
  			imgsrc: imgpath+"butterfly.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "imagelabel",
  			imagelabeldata: data.string.p2_s6
  		}]
  	}]
  },{
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 orangebg",
  		textdata: data.string.p2_s7
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"butterfly-egger.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s8
  	}]
  },{
  	//page 2
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 orangebg",
  		textdata: data.string.p2_s7
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"leaf-egg.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s8
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s9
  	}]
  },{
  	//page 3
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 orangebg",
  		textdata: data.string.p2_s7
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"egg-cracking.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s8
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s9
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s10
  	}]
  },{
  	//page 4
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 orangebg",
  		textdata: data.string.p2_s7
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"larva1.jpg"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s8
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s9
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s10
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s11
  	},{
  		textclass: "description3",
  		datahighlightflag: true,
  		datahighlightcustomclass: "pink_highlight",
  		textdata: data.string.p2_s12
  	}]
  },{
  	//page 5
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"egg-chewing.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s14
  	}]
  },{
  	//page 6
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft2_top",
			imgsrc: imgpath+"egg-chewing.gif"
		},{
			imgclass: "imageleft2_bottom",
			imgsrc: imgpath+"chomping1.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s14
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s15
  	}]
  },{
  	//page 7
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft2_top",
			imgsrc: imgpath+"egg-chewing.gif"
		},{
			imgclass: "imageleft2_bottom",
			imgsrc: imgpath+"chomping1.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s16
  	}]
  },{
  	//page 8
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"shedding-and-walking.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s17
  	}]
  },{
  	//page 9
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"chewing.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s18
  	}]
  },{
  	//page 10
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 greenbg",
  		textdata: data.string.p2_s13
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imagecenter",
			imgsrc: imgpath+"chrysalis-formation.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb2",
  	lowertextblock: [{
  		textclass: "description4",
  		textdata: data.string.p2_s19
  	}]
  },{
  	//page 11
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 lightbluebg",
  		textdata: data.string.p2_s24
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "image3_1",
			imgsrc: imgpath+"chrysalis-formation.gif"
		},{
			imgclass: "image3_2",
			imgsrc: imgpath+"chrysalis-formation-2.gif"
		},{
			imgclass: "image3_3",
			imgsrc: imgpath+"chrysalis-formation-3.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb2",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s20
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s21
  	},{
  		textclass: "description3",
  		datahighlightflag: true,
  		datahighlightcustomclass: "black_highlight",
  		textdata: data.string.p2_s22
  	},{
  		textclass: "description5",
  		textdata: data.string.p2_s23
  	}]
  },{
  	//page 12
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 lightbluebg",
  		textdata: data.string.p2_s24
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imagecenter",
            imgsrc: imgpath+"chrysalis-formation-2.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb2",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s25
  	}]
  },{
  	//page 13
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 pinkbg",
  		textdata: data.string.p2_s26
  	},{
  		textclass: "description3_bottom",
  		textdata: data.string.p2_s29
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imagecenter",
			imgsrc: imgpath+"butterfly-ready-to-fly.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb2",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s27
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s28
  	}]
  },{
  	//page 14
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 pinkbg",
  		textdata: data.string.p2_s26
  	}],
  	
	imageblock: [{
		imagestoshow: [{
			imgclass: "imageleft1",
			imgsrc: imgpath+"content-butterfly.gif"
		}]
	}],
	  
  	lowertextblockadditionalclass: "additional_ltb",
  	lowertextblock: [{
  		textclass: "description3",
  		textdata: data.string.p2_s30
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s31
  	},{
  		textclass: "description3",
  		textdata: data.string.p2_s32
  	}]
  },{
  	//page 15
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description6 fadein5",
  		textdata: data.string.p2_s33
  	}],
	imageblock:[{
		imageblockclass : "egg fadein1",
		imagestoshow : [{
			imgclass : "image",
			imgsrc : imgpath + "leaf-egg.gif"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel",
			imagelabeldata : data.string.p2_s3
		}]
	},{
		imageblockclass: "larva fadein2",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"larva1.jpg"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s4
		}]
	},{
		imageblockclass: "pupa fadein3",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"chrysalis-formation-3.gif"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s5
		}]
	},{
		imageblockclass: "butterfly fadein4",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"content-butterfly.gif"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s6
		}]
	},{
		imagestoshow: [{
			imgclass: "arrow1 fadein1_5",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow2 fadein2_5",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow3 fadein3_5",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow4 fadein4_5",
			imgsrc: imgpath+"arrow2.png"
		}]
	}]
  },{
  	//page 16
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description6 fadein1",
  		datahighlightflag: true,
  		datahighlightcustomclass: "purple_highight",
  		textdata: data.string.p2_s34
  	}],
	imageblock:[{
		imageblockclass : "egg",
		imagestoshow : [{
			imgclass : "image",
			imgsrc : imgpath + "leaf-egg.gif"
		}],
		imagelabels : [{
			imagelabelclass : "imagelabel",
			imagelabeldata : data.string.p2_s3
		}]
	},{
		imageblockclass: "larva",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"larva1.jpg"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s4
		}]
	},{
		imageblockclass: "pupa",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"chrysalis-formation-3.gif"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s5
		}]
	},{
		imageblockclass: "butterfly",
		imagestoshow: [{
			imgclass: "image",
			imgsrc: imgpath+"content-butterfly.gif"
		}],
		imagelabels:[{
			imagelabelclass: "imagelabel",
			imagelabeldata: data.string.p2_s6
		}]
	},{
		imagestoshow: [{
			imgclass: "arrow1",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow2",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow3",
			imgsrc: imgpath+"arrow2.png"
		},{
			imgclass: "arrow4",
			imgsrc: imgpath+"arrow2.png"
		}]
	}]
  }
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/
   
   /*
       containts flash to html converted code --- start----
   */
	 // function checkforflashtohtml($highlightinside){
        // //check if $highlightinside is provided
        // typeof $highlightinside !== "object" ?
        // alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        // null ;
//           
        // var $alltextpara = $highlightinside.find("*[data-flashtohtml='true']");
        // if($alltextpara.length > 0){
          // $.each($alltextpara, function(index, val) { 
            // /*if there is a data-highlightcustomclass attribute defined for the text element
            // use that or else use default 'parsedstring'*/       
           // if( $(this).attr("data-htmlfilename")){ /*if there is data-highlightcustomclass defined it is true else it is not*/
	            // var stylerulename = $(this).attr("data-htmlfilename");
	            // $(this).load( stylerulename+".html", function() {
					// console.log( "The html page is loaded : ", stylerulename+".html");
				// });
          // }
          // });
        // }
      // }

	/*
	       containts flash to html converted code ---end----
	*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag){
    typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.delay(1200).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if(countNext == $total_page - 1){
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
				}
			},1800);
		}
   }
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/   
  var playing_audio;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
        
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    switch(countNext){
    	case 0:
    		playing_audio = sound_list[countNext];
    		$nextBtn.hide(0);
    		playing_audio.play();
    		playing_audio.bind("ended", function(){
    				playing_audio = sound_list[countNext+2];
		    		playing_audio.play();
		    		playing_audio.bind("ended", function(){
		    			playing_audio = sound_list[countNext+1];
		    			playing_audio.play();
		    			playing_audio.bind("ended", function(){
							$nextBtn.show(0);
						});
		    		});
    		});
    		var delaytime = 3000;
    		  setTimeout(function(){
    		  $(".imagediv1").addClass("scaleup");
    		  setTimeout(function(){
                  		  $(".imagediv2").addClass("scaleup");
                  		  setTimeout(function(){
                              		  $(".imagediv3").addClass("scaleup");
                              		  setTimeout(function(){
                                          		  $(".imagediv4").addClass("scaleup");
                                          		  },1000);
                              		  },1000);
                  		  },1000);
    		  },8000);
    		break;
		case 1:
			playing_audio = sound_list[countNext+2];
    		playing_audio.play();
    		playing_audio.bind("ended", function(){
				playing_audio = sound_list[countNext+3];
    			playing_audio.play();
    			playing_audio.bind("ended", function(){
					$nextBtn.show(0);
				});
			});
			break;
		case 2:
		case 3:
			playing_audio = sound_list[countNext+3];
    		playing_audio.play();
    		playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
		case 4:
			playing_audio = sound_list[countNext+3];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
		case 5:
            playing_audio = sound_list[13];
            playing_audio.play();
            playing_audio.bind("ended", function(){
                playing_audio.stop();
                playing_audio = sound_list[countNext+3];
                playing_audio.play();
                playing_audio.bind("ended", function(){
                    $nextBtn.show(0);
                });
            });
            break;
		case 6:
			playing_audio = sound_list[countNext+3];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
		case 7:
			playing_audio = sound_list[countNext+3];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
		case 8:
		case 9:
			playing_audio = sound_list[countNext+3];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
        case 10:
            playing_audio = sound_list[countNext+4];
            playing_audio.play();
            playing_audio.bind("ended", function(){
                $nextBtn.show(0);
            });
            break;
		case 11:
			playing_audio = sound_list[countNext+4];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
    				playing_audio = sound_list[countNext+5];
		    		playing_audio.play();
		    		playing_audio.bind("ended", function(){
		    			playing_audio = sound_list[countNext+6];
		    			playing_audio.play();
		    			playing_audio.bind("ended", function(){
							$nextBtn.show(0);
						});
		    		});
    		});
			break;
		case 12:
			playing_audio = sound_list[countNext+6];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				$nextBtn.show(0);
			});
			break;
		case 13:
			playing_audio = sound_list[countNext+6];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				playing_audio = sound_list[countNext+7];
				playing_audio.play();
	    		playing_audio.bind("ended", function(){
						$nextBtn.show(0);
	    		});
    		});
			break;
		case 14:
			playing_audio = sound_list[countNext+7];
    		playing_audio.play();
			playing_audio.bind("ended", function(){
				playing_audio = sound_list[countNext+8];
				playing_audio.play();
	    		playing_audio.bind("ended", function(){
						$nextBtn.show(0);
	    		});
			});
			break;
		case 15:
			playing_audio = sound_list[countNext+8];
			setTimeout(function(){
	    		playing_audio.play();
				playing_audio.bind("ended", function(){
					playing_audio = sound_list[countNext+9];
	    			playing_audio.play();
	    			playing_audio.bind("ended", function(){
						$nextBtn.show(0);
					});
				});
			}, 1500);
			break;
		case 16:
			playing_audio = sound_list[countNext+9];
    		playing_audio.play();
			break;
		default:
			break;
    }

    
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
  function templateCaller(){
   			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
	
			// call navigation controller
			navigationcontroller();
	
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	
			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);
	
			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();
	
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});
var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/page1/";

var sound_0 = new buzz.sound((soundAsset + "p1_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p1_s1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "recording3.ogg"));
var sound_3 = new buzz.sound((soundAsset + "recording4.ogg"));
var sound_4 = new buzz.sound((soundAsset + "recording5.ogg"));
var sound_5 = new buzz.sound((soundAsset + "recording6.ogg"));
var sound_6 = new buzz.sound((soundAsset + "recording7.ogg"));
var sound_7 = new buzz.sound((soundAsset + "recording8.ogg"));
var sound_8 = new buzz.sound((soundAsset + "recording9.ogg"));
var sound_9 = new buzz.sound((soundAsset + "recording10.ogg"));
var sound_10 = new buzz.sound((soundAsset + "recording11.ogg"));

var sound_list = [sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10];

var content=[
 {
  	// slide 0
  	contentblockadditionalclass: "ole-background-gradient-blunatic",
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textdata: data.string.p0_heading
  	}]
    ,
    imageblock:[{
        imagestoshow:[{
          imgclass: "butterfly",
          imgsrc: imgpath+"cover_page.jpg"
        }]
    }]
  },{
  	// slide 1
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition",
  	uppertextblock: [{
  		textclass: "description",
  		textdata: data.string.p1_s1
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image fadein1",
  			imgsrc: imgpath + "intro02.jpg"
  		},{
  			imgclass: "image fadein2",
  			imgsrc: imgpath + "intro03.jpg"
  		},{
  			imgclass: "image fadein3",
  			imgsrc: imgpath + "intro04.jpg"
  		},{
  			imgclass: "image fadein4",
  			imgsrc: imgpath + "intro05.jpg"
  		},{
  			imgclass: "image fadein5",
  			imgsrc: imgpath + "intro06.jpg"
  		},{
  			imgclass: "image fadein6",
  			imgsrc: imgpath + "intro07.jpg"
  		}]
  	}]
  },{
  	// slide 2
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition purplebg fadein",
  	utbnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s2
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image6",
  			imgsrc: imgpath + "intro02.jpg"
  		},{
  			imgclass: "image5",
  			imgsrc: imgpath + "intro03.jpg"
  		},{
  			imgclass: "image4",
  			imgsrc: imgpath + "intro04.jpg"
  		},{
  			imgclass: "image3",
  			imgsrc: imgpath + "intro05.jpg"
  		},{
  			imgclass: "image2",
  			imgsrc: imgpath + "intro06.jpg"
  		},{
  			imgclass: "image1",
  			imgsrc: imgpath + "intro07.jpg"
  		}]
  	}]
  },{
  	// slide 3
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition pinkbg",
  	utbnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description2 fadein",
  		textdata: data.string.p1_s3
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "image6",
  			imgsrc: imgpath + "intro02.jpg"
  		},{
  			imgclass: "image5",
  			imgsrc: imgpath + "intro03.jpg"
  		},{
  			imgclass: "image4",
  			imgsrc: imgpath + "intro04.jpg"
  		},{
  			imgclass: "image3",
  			imgsrc: imgpath + "intro05.jpg"
  		},{
  			imgclass: "image2",
  			imgsrc: imgpath + "intro06.jpg"
  		},{
  			imgclass: "image1",
  			imgsrc: imgpath + "intro07.jpg"
  		}]
  	}]
  },{
  	// slide 4
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition pinkbg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s4
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage",
  			imgsrc: imgpath + "caterpillar.jpg"
  		}]
  	}]
  },{
  	// slide 5
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description3 fadein",
  		textdata: data.string.p1_s5
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "tow_images1",
  			imgsrc: imgpath + "caterpillar02.jpg"
  		},{
  			imgclass: "tow_images2",
  			imgsrc: imgpath + "butterfly10.jpg"
  		}]
  	}]
  },{
  	// slide 6
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s6
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage",
  			imgsrc: imgpath + "butterfly10.jpg"
  		}]
  	}]
  },{
  	// slide 7
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s7
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage",
  			imgsrc: imgpath + "egg01.jpg"
  		}]
  	}]
  },{
  	// slide 8
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s8
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage",
  			imgsrc: imgpath + "caterpillar02.jpg"
  		}]
  	}]
  },{
  	// slide 9
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s9
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage",
  			imgsrc: imgpath + "pupa1.jpg"
  		}]
  	}]
  },{
  	// slide 10
  	contentnocenteradjust: true,
  	uppertextblockadditionalclass: "utbaddition bluebg",
  	uppertextblock: [{
  		textclass: "description2",
  		textdata: data.string.p1_s10
  	}],
  	imageblockadditionalclass: "fadein",
  	imageblock:[{
  		imagestoshow:[{
  			imgclass: "singleimage_left fadein",
  			imgsrc: imgpath + "egg03.jpg"
  		},{
  			imgclass: "singleimage_right fadein_2",
  			imgsrc: imgpath + "butterfly10.jpg"
  		},{
  			imgclass: "arrow",
  			imgsrc: imgpath + "arrow.png"
  		}]
  	}]
  }
];


$(function () { 
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var playing_sound;

  var $total_page = content.length;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
   
     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true 
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class
        
        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;
          
        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) { 
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/       
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**   
      How to:
      - First set any html element with 
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification    
     */
    
    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/ 
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
    */
  
  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
     */  
   
  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			// $nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				if (countNext == $total_page - 1){
					islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
				}
			}, 2300);
		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**   
      How to:
      - Just call instructionblockcontroller() from the template    
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which 
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);          
        });
      }
    } 
  /*=====  End of InstructionBlockController  ======*/
  
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/  
  var playing_audio;
  function generalTemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
        
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    vocabcontroller.findwords(countNext);

    switch(countNext){
    	case 0: 
    		playing_audio = sound_list[countNext];
    		$nextBtn.hide(0);
    		playing_audio.play();
    		playing_audio.bind("ended", function(){
    			$nextBtn.show(0);
    		});
    		break;
    	case 1:
    		playing_audio = sound_list[countNext];
    		playing_audio.play();
    		$nextBtn.delay(12000).show(0);
    		break;
    	case 2:
    	case 3:
    	case 4:
    	case 6:
    	case 7:
    	case 8:
    	case 9:
    		playing_audio = sound_list[countNext];
    		playing_audio.play();
            playing_audio.bind("ended", function(){
                $nextBtn.show(0);
            });    		break;
    	case 5:
    		playing_audio = sound_list[countNext];
    		playing_audio.play();
    		$nextBtn.delay(3700).show(0);
    		break;
		case 10:
			playing_audio = sound_list[countNext];
    		playing_audio.play();
			break;
    	default:
    		break;
    }
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the 
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated 
   */
  
 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
	
			// call navigation controller
			navigationcontroller();
	
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	
			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);
	
			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}
	
  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	}); 

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
  
/*=====  End of Templates Controller Block  ======*/
});
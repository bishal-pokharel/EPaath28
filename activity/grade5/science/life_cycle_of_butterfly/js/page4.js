var imgpath = $ref+"/images/";
var soundAsset = $ref+"/audio_"+$lang+"/page4/";

var sound_0 = new buzz.sound((soundAsset + "recording1.ogg"));
var sound_1 = new buzz.sound((soundAsset + "recording2.ogg"));

var sound_list = [sound_0, sound_1];

var content=[
  {
  	//page 0
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description_title",
  		textdata: data.string.p4_s1
  	}],
      imageblock: [{
          imagestoshow: [
              {
                  imgclass: 'diyimage',
                  imgid: 'diyimage',
                  imgsrc: "images/diy_bg/a_07.png"
              }
          ]
      }],
  },{
  	//page 1
  	contentblockadditionalclass: "simplebg",
  	uppertextblockadditionalclass: "add_utb",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "description lightbluebg",
  		textdata: data.string.p4_s2
  	}],
  	imageblock:[{
  		imagestoshow:[{
  			imgclass:"egg",
  			imgsrc: imgpath+"leaf-egg.gif"
  		},{
  			imgclass:"larva",
  			imgsrc: imgpath+"egg-chewing.gif"
  		},{
  			imgclass:"pupa",
  			imgsrc: imgpath+"chrysalis-formation-3.gif"
  		},{
  			imgclass:"adult",
  			imgsrc: imgpath+"content-butterfly.gif"
  		}]
  	},{
		imageblockclass: "cycle_container",
		imagestoshow:[{
			imgclass:"image1",
			imgsrc: imgpath+"circle01.png"
		},{
			imgclass:"image2",
			imgsrc: imgpath+"circle02.png"
		},{
			imgclass:"image3",
			imgsrc: imgpath+"circle03.png"
		},{
			imgclass:"image4",
			imgsrc: imgpath+"circle04.png"
		},{
			imgclass:"image5",
			imgsrc: imgpath+"circle04.png"
		},{
			imgclass:"image6",
			imgsrc: imgpath+"circle04.png"
		},{
			imgclass:"image7",
			imgsrc: imgpath+"circle04.png"
		}]
  	},{
  		imageblockclass: "egg_drop",
  		imagestoshow:[{
			imgclass:"image",
			imgsrc: imgpath+"leaf_egg.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "egg_label",
  			imagelabeldata: data.string.p2_s3
  		}]
  	},{
  		imageblockclass: "larva_drop",
  		imagestoshow:[{
			imgclass:"image",
			imgsrc: imgpath+"egg-chewing.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "adult_larva_label",
  			imagelabeldata: data.string.p2_s4
  		}]
  	},{
  		imageblockclass: "pupa_drop",
  		imagestoshow:[{
			imgclass:"image",
			imgsrc: imgpath+"chrysalis-formation.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "pupa_label",
  			imagelabeldata: data.string.p2_s5
  		}]
  	},{
  		imageblockclass: "adult_drop",
  		imagestoshow:[{
			imgclass:"image",
			imgsrc: imgpath+"content-butterfly.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "adult_larva_label",
  			imagelabeldata: data.string.p2_s6
  		}]
  	},{
  		imageblockclass: "example_div examplediv_updown",
  		imagestoshow:[{
			imgclass:"imagepresentation",
			imgsrc: imgpath+"butterfly-egger.gif"
		},{
			imgclass:"pointing_arrow rotate0_arrow",
			imgsrc: imgpath+"uparrow.png"
		}],
		imagelabels:[{
			imagelabelclass: "question_text",
			imagelabeldata: data.string.p4_s3
		}]
  	}]
  }
];


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	
	loadTimelineProgress($total_page, countNext + 1);

	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templateCaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templateCaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var playing_audio;
	function generalTemplate() {
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		//call notifyuser
		// notifyuser($anydiv);
		switch(countNext){
			case 0:
				playing_audio = sound_list[countNext];
	    		playing_audio.play();
				playing_audio.bind("ended", function(){
					$nextBtn.show(0);
				});
				break;
			case 1:
				playing_audio = sound_list[countNext];
	    		playing_audio.play();
				var $egg_drop = $(".egg_drop");
				var $larva_drop = $(".larva_drop");
				var $pupa_drop = $(".pupa_drop");
				var $adult_drop = $(".adult_drop");
				var $imagepresentation = $(".imagepresentation");
				var $yellowcircle = $(".image2");
				
				var $pointing_arrow = $(".pointing_arrow");
				var $question_text = $(".question_text");
				var $examplediv = $(".example_div");
				
				$egg_drop.show(0);
				$(".egg, .larva, .pupa, .adult").draggable({
					containment : "body",
					cursor : "grab",
					revert : "invalid",
					appendTo : "body",
					helper : "clone",
				});
				
				$(".egg_drop").droppable({
					accept : ".egg",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						ui.draggable.draggable('disable');
						$(ui.draggable).css("filter", "grayscale(100%)");
						$(".egg_drop > img").show(0);
						$(".egg_drop > label").show(0);
						$imagepresentation.show(0);
						$pointing_arrow.removeClass("rotate0_arrow");
						$pointing_arrow.hide(0);
						$question_text.hide(0);
						$examplediv.removeClass("examplediv_updown");
						
						setTimeout(function(){
							$(".image4").show(0);
							$yellowcircle.addClass("rotate90");
							setTimeout(function(){
								$imagepresentation.hide(0);
								$imagepresentation.attr('src', imgpath+'chewing.gif');
								$larva_drop.show(0);
								
								$pointing_arrow.show(0);
								$question_text.show(0);
								$examplediv.addClass("examplediv_leftright");
								$pointing_arrow.addClass("rotate90_arrow");
								$question_text.html(data.string.p4_s4);
							}, 1000);
						}, 3500);
					}
				});
				
				$(".larva_drop").droppable({
					accept : ".larva",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						ui.draggable.draggable('disable');
						$(ui.draggable).css("filter", "grayscale(100%)");
						$(".larva_drop > img").show(0);
						$(".larva_drop > label").show(0);
						$imagepresentation.show(0);
						$pointing_arrow.removeClass("rotate90_arrow");
						$pointing_arrow.hide(0);
						$question_text.hide(0);
						$examplediv.removeClass("examplediv_leftright");
						setTimeout(function(){
							$(".image5").show(0);
							$yellowcircle.addClass("rotate180");
							setTimeout(function(){
								$examplediv.addClass("examplediv_updown");
								$imagepresentation.hide(0);
								$imagepresentation.attr('src', imgpath+'chrysalis-formation-2.gif');
								$pupa_drop.show(0);
								$pointing_arrow.show(0);
								$question_text.show(0);
								$pointing_arrow.addClass("rotate180_arrow");
								$question_text.html(data.string.p4_s4);
							});
						}, 3500);
					}
				});
				
				$(".pupa_drop").droppable({
					accept : ".pupa",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						ui.draggable.draggable('disable');
						$(ui.draggable).css("filter", "grayscale(100%)");
						$(".pupa_drop > img").show(0);
						$(".pupa_drop > label").show(0);
						$imagepresentation.show(0);
						$pointing_arrow.removeClass("rotate180_arrow");
						$pointing_arrow.hide(0);
						$question_text.hide(0);
						$examplediv.removeClass("examplediv_updown");
						setTimeout(function(){
							$(".image6").show(0);
							$yellowcircle.addClass("rotate270");
							setTimeout(function(){
								$imagepresentation.hide(0);
								$examplediv.addClass("examplediv_leftright");
								$(".example_div").css("background-color", "#A4D4FF");
								$imagepresentation.attr('src', imgpath+'content-butterfly.gif');
								$adult_drop.show(0);
								$pointing_arrow.show(0);
								$question_text.show(0);
								$pointing_arrow.addClass("rotate270_arrow");
								$question_text.html(data.string.p4_s5);
							});
						}, 6500);
					}
				});
				
				$(".adult_drop").droppable({
					accept : ".adult",
					hoverClass : "hovered",
					drop : function upondrop(event, ui){
						ui.draggable.draggable('disable');
						$(ui.draggable).css("filter", "grayscale(100%)");
						$(".adult_drop > img").show(0);
						$(".adult_drop > label").show(0);
						$imagepresentation.show(0);
						$examplediv.removeClass("examplediv_leftright");
						$pointing_arrow.hide(0);
						$question_text.hide(0);
						setTimeout(function(){
							$(".image7").show(0);
							$imagepresentation.hide(0);
							$(".example_div").css("background-color", "");
							$yellowcircle.hide(0);
							ole.footerNotificationHandler.lessonEndSetNotification();
						}, 3500);
					}
				});
				
				break;
			default: 
				break;
		}
		
		
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
	
			// call navigation controller
			navigationcontroller();
	
			// call the template
			generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
	
			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);
	
			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click', function() {
			countNext++;
			templateCaller();
	}); 

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});
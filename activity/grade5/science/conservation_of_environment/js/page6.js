var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.envconservation,
            }
        ],
        imageblock: [{
            textblock:false,
            imagediv:"imageClass",
            textclass:"image",
            imagestoshow: [
                {
                    divimgcls:"talkingladydiv",
                    imgclass: "giff",
                    imgid: 'talkinglady',
                    imgsrc: "",
                },{
                  divimgcls:"talkingladydiv",
                  imgclass: "imgg",
                  imgid: 'hi',
                  imgsrc: "",
                }

            ]
        }],
        popupdiv:[
            {
                textClass:"popupdiv",
                txtdiv:"txtdiv",
                textdata:data.string.p6text1,
                button:[{
                    btndiv:"btn1div",
                    btnclass:"reason btncls clickhere",
                    btndata:data.string.how,
                }],
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdiv:"handimg",
                            imgclass: "clickhere handiconImg",
                            imgid: "handicon",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic9",
                            imgclass: "degdustbin",
                            imgid: "pic9",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic10",
                            imgclass: "nondegdustbin",
                            imgid: "pic10",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic1",
                            imgclass: "redcane",
                            imgid: "pic1",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic2",
                            imgclass: "bottle",
                            imgid: "pic2",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic3",
                            imgclass: "yellowcane",
                            imgid: "pic3",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic4",
                            imgclass: "cane",
                            imgid: "pic4",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic5",
                            imgclass: "poop",
                            imgid: "pic5",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic6",
                            imgclass: "peels",
                            imgid: "pic6",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic7",
                            imgclass: "bananapeels",
                            imgid: "pic7",
                            imgsrc: "",
                        },
                        {
                            imgdiv:"pic8",
                            imgclass: "fruitswaste",
                            imgid: "pic8",
                            imgsrc: "",
                        }

                    ]
                }]

            }
        ]
    },
    // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p6text2,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    divimgcls:"bgdiv",
                    imagediv:"image10_bg_holder manputtingmool",
                    imgclass: "blink bgsolution",
                    imgid: 'imge2',
                    imgsrc: "",
                }

            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p6text3,
            }
        ],
        imageblock: [{
            extradivclass:"bgcolor bgsolution1",
            imagestoshow: [
                {
                    divimgcls:"div1",
                    imgclass: "btlimg",
                    imgid: 'bottle',
                    imgsrc: "",
                },
                {
                    divimgcls:"div2",
                    imgclass: "penholderimg",
                    imgid: 'pencilholder',
                    imgsrc: "",
                },
                {
                    divimgcls:"div3",
                    imgclass: "bottlepotimg",
                    imgid: 'bottlepot',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1 tp",
                textdata: data.string.p6text4,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    divimgcls:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'imge4',
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "page5-css", src: $ref + "css/page5.css", type: createjs.AbstractLoader.CSS},
            // // images
            {id: "talkinglady", src: imgpath+"talking_lady.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "imge2", src: imgpath+"putting-mool_page29.png", type: createjs.AbstractLoader.IMAGE},
            {id: "imge4", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "land", src: imgpath+"putting-mool_page29.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bottle", src: imgpath+"page43/bottle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pencilholder", src: imgpath+"page43/pencilholder.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bottlepot", src: imgpath+"page43/bottlepot.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic1", src: imgpath+"page_38/b01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic2", src: imgpath+"page_38/b02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic3", src: imgpath+"page_38/b03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic4", src: imgpath+"page_38/b04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic5", src: imgpath+"page_38/b05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic6", src: imgpath+"page_38/b06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic7", src: imgpath+"page_38/b08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic8", src: imgpath+"page_38/b09.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic9", src: imgpath+"page_38/Dustbin_02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic10", src: imgpath+"page_38/Dustbin_03.png", type: createjs.AbstractLoader.IMAGE},            // {id: "sound_1", src: soundAsset+"p1_s0.ogg"},
            {id: "hi", src: imgpath+"talking-lady.png", type: createjs.AbstractLoader.IMAGE},


            //sounds
            {id: "sound_0", src: soundAsset+"p6_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p6_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p6_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p6_s3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if((countNext == 0)&& $total_page!=1){
            $nextBtn.css('display', 'none');
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                put_image2(content, countNext);
                $(".handiconImg").hide(0);
                $(".clickhere").attr("disabled","disabled");
                sound_player_only("sound_0","handiconImg","talkinglady",imgpath+"talking-lady.png");
                animateWaste();
                $board.on('click', '.clickhere', function () {
                    countNext=1;
                    templateCaller();
                });
                setTimeout(function(){
                  $(".imgg").css("z-index","2");
                },2000);
                break;
            case 1:
                sound_player("sound_1");
                break;
            case 2:
                sound_player_only("sound_2","nothing");
                animateImg();
                break;
            case 3:
                sound_player("sound_3");
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }

    function sound_player_only(sound_id,showElem,imgclss,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        if(showElem!="nothing") {
            current_sound.on('complete', function () {
                $("."+imgclss).attr("src",changeImg);
                $("." + showElem).show(0);
                $(".clickhere").removeAttr("disabled");
                $('.div1').removeClass("grayout").removeClass("nonclickable");
            });
        }
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }

    function put_image2(content, count) {
        if (content[count].hasOwnProperty('popupdiv')) {
            var contentCount=content[count].popupdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg() {
        $(".penholderimg,.bottlepotimg").hide(0)
            setTimeout(function(){
                $(".btlimg").clone().addClass("slideL btlimg2 tobefade").appendTo(".div2");
                    setTimeout(function(){
                        $(".tobefade").addClass("fadingout");
                            setTimeout(function(){
                                $(".tobefade").hide(0);
                                $(".penholderimg").show(0);
                                    setTimeout(function(){
                                        $(".btlimg").clone().addClass("slideL btlimg2 tobefade1").appendTo(".div3");
                                            setTimeout(function(){
                                                $(".tobefade1").addClass("rotatebottle");
                                                    setTimeout(function () {
                                                                $(".tobefade1").hide(0);
                                                                $(".bottlepotimg").show(0);
                                                        navigationcontroller();
                                                    },3000);
                                            },2000);
                                    },2000);
                            },1000);
                    },2000);
            },2000);
        }

        function animateWaste(){
        setTimeout(function(){
            $(".redcane").addClass("translatepic1");
            $(".bottle").addClass("translatepic2");
            $(".yellowcane").addClass("translatepic3");
            $(".cane").addClass("translatepic4");
            $(".poop").addClass("translatepic5");
            $(".peels").addClass("translatepic6");
            $(".bananapeels").addClass("translatepic7");
            $(".fruitswaste").addClass("translatepic8");
          },1000);
        }
});

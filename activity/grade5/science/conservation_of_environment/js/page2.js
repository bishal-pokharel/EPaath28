var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.envconservation,
            }
          ],
        imageblock: [{
            textblock:false,
            imagediv:"imageClass",
            textclass:"image",
            imagestoshow: [
                {
                    imagediv:"talkingladydiv",
                    imgclass: "giff",
                    imgid: 'talkinglady',
                    imgsrc: "",
                },{
                  imagediv:"talkingladydiv",
                  imgclass: "imgg",
                  imgid: 'hi',
                  imgsrc: "",
                }

            ]
        }],
        popupdiv:[
            {
                textClass:"popupdiv",
                txtdiv:"txtdiv",
                textdata:data.string.p2text2,
                button:[{
                    btndiv:"btn1div",
                    btnclass:"reason btncls clickhere",
                    btndata:data.string.how,
                }],
                imageblock: [{
                    imagestoshow: [
                        {
                            animation:true,
                            divclass:"hand1img",
                            imgclass: "clickhere handiconImg hand",
                            imgid: "handicon",
                            imgsrc: "",
                        },
                        {
                            animation:true,
                            divclass:"horizontally firstsmoke",
                            imgclass: "smoke1",
                            imgid: 'smk1',
                            imgsrc: "",
                        },
                        {
                            animation:true,
                            divclass:"horizontally secondsmoke",
                            imgclass: "smoke2",
                            imgid: 'smk2',
                            imgsrc: "",
                        }
                    ]
                }]

            }
        ]
    },
    // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        uppertextblock: [
            {
                textclass: "topic1",
                textdata: data.string.p2text3,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    firstImgCls:"first",
                    anchorClass:"adiv1",
                    popupdisplay:true,
                    imagediv:"div1",
                    imgclass: "img1",
                    imgid: "img1", // show img4
                    imgsrc: ""
                },
                {
                    firstImgCls:"second",
                    anchorClass:"adiv2",
                    popupdisplay:true,
                    imagediv:"div2",
                    imgclass: "img2",
                    imgid: "img2", // show img5
                    imgsrc: ""
                },
                {
                    firstImgCls:"third",
                    anchorClass:"adiv3",
                    popupdisplay:true,
                    imagediv:"div3",
                    imgclass: "img3",
                    imgid: "img3",// show img6
                    imgsrc: ""
                },
                {
                    imagediv:"generaldiv handiconImage",
                    imgclass: "handImage",
                    imgid: "handicon",
                    imgsrc: ""
                }
            ]
      } ],

        dialogbox:[
            {

                    divclass: "firstimage",
                    imageblock: [{
                        imagestoshow: [
                            {
                                imgdata:data.string.p2text4,
                                imgclass: "commonImg img4",
                                imgid: "img4",// bind to img1
                                imgsrc: ""
                            }
                        ]
                    }]

            },
            {
                divclass: "secondimage",
                imageblock: [{
                    imagestoshow: [
                        {
                            imgdata:data.string.p2text5,
                            imgclass: "commonImg img5",
                            imgid: "img5", // bind to img2
                            imgsrc: ""
                        },
                        {
                            imgclass: "hideclass img6",
                            imgid: "img6", // bind to img2
                            imgsrc: ""
                        }
                    ]
                }]
            },
            {
                    divclass:"thirdimage",
                    imageblock: [{
                        imagestoshow: [
                            {
                                imgdata:data.string.p2text6,
                                imgclass: "commonImg img7",
                                imgid: "img7", // bind to img3
                                imgsrc: "",
                            },
                            {
                                imgclass: "img8",
                                imgid: "img8", // bind to img3
                                imgsrc: "",
                            }
                        ]
                    }]
            }
        ]

 },

    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p2text7,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'bg4',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p2text8,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'bg5',
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "page2-css", src: $ref + "css/page2.css", type: createjs.AbstractLoader.CSS},
            // // images
            {id: "talkinglady", src: imgpath+"talking_lady.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "img1", src: imgpath+"page11/local-chulo.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img2", src: imgpath+"page11/gobar-gas.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img3", src: imgpath+"page11/solar.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img4", src: imgpath+"page11/chulo-gif.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "img5", src: imgpath+"page11/Biogas/process.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "img6", src: imgpath+"page11/Biogas/zoom.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img7", src: imgpath+"page11/Solar/house.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "img8", src: imgpath+"page11/Solar/sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg4", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg5", src: imgpath+"bg05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smk1", src: imgpath+"page11/smoke_cloud-10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "smk2", src: imgpath+"page11/smoke_cloud-2-11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hi", src: imgpath+"talking-lady.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p2_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p2_s1.ogg"},
            {id: "sound_1_1", src: soundAsset+"p2_s1_1.ogg"},
            {id: "sound_1_2", src: soundAsset+"p2_s1_2.ogg"},
            {id: "sound_1_3", src: soundAsset+"p2_s1_3.ogg"},
            {id: "sound_2", src: soundAsset+"p2_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p2_s3.ogg"},

                    {id: "sound_a", src: soundAsset+"p1_s0.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if((countNext == 0)&& $total_page!=1){
            $nextBtn.css('display', 'none');
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
                ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                put_image2(content, countNext);
                $(".handiconImg").hide(0);
                $(".clickhere").attr("disabled","disabled");
                 sound_player_only("sound_0","handiconImg","talkinglady",imgpath+"talking-lady.png");
                 $board.on('click', '.clickhere', function () {
                    countNext=1;
                    templateCaller();

              });
              $(".imgg").show(0);
              setTimeout(function(){
                $(".giff").css("z-index","2");
                setTimeout(function(){
                  $(".imgg").css("z-index","3");
                },4500);
              },3800);

                break;
            case 1:
                $('.whitecontent,.generaldiv').hide(0);
                $('.div1,.div2,.div3').addClass("grayout").addClass("nonclickable");
                sound_player_only("sound_1","generaldiv");
                particulardivClick();
                $prevBtn.css('display', 'none');
                break;
            case 2:
                sound_player("sound_2");

                break;
            case 3:
                sound_player("sound_3");

                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player_only(sound_id,showElem,imgclss,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            $("."+imgclss).attr("src",changeImg);
            $("."+showElem).show(0);
            $(".clickhere").removeAttr("disabled");
            $('.div1').removeClass("grayout").removeClass("nonclickable");
        });
    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }


    function put_image2(content, count) {
        if (content[count].hasOwnProperty('popupdiv')) {
            var contentCount=content[count].popupdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function put_image3(content, count, imgCount) {
        if (content[count].hasOwnProperty('dialogbox')) {
            var contentCount=content[count].dialogbox[imgCount];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            loadimage(imageblockcontent,contentCount,imgCount);
        }
    }


    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function loadimage(imageblockcontent,contentCount,imgCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                emptyimages(imageClass);
                for (var i = 0; i < imageClass.length; i++) {

                    if(imgCount==1 && i==1) {
                        setTimeout(function () {
                            var image_src = preload.getResult(imageClass[1].imgid).src;
                            //get list of classes
                            var classes_list = imageClass[1].imgclass.match(/\S+/g) || [];
                            var selector = ('.' + classes_list[classes_list.length - 1]);
                            $(selector).removeClass("hideclass");
                            $(selector).attr('src', image_src);
                            $(selector).addClass("animatefadeup");
                        }, 5000);
                    }

                        else {
                            var image_src = preload.getResult(imageClass[i].imgid).src;
                            //get list of classes
                            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                            var selector = ('.' + classes_list[classes_list.length - 1]);
                            $(selector).attr('src', image_src);
                        }
                    }
                }

            }
        }
    // empty all src of images
    function emptyimages(imageClass){
        for (var i = 0; i < imageClass.length; i++) {
            //get list of classes
            var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
            var selector = ('.' + classes_list[classes_list.length - 1]);
            $(selector).attr('src', '')
        }
    }




    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

  function particulardivClick(){
      $board.on('click','.div1,.div2, .div3',function(){
          $(".crossbtn").attr("src",imgpath+"crossbtn.png");
          var imgCount=0;
          var divContent = $(this);
          if(divContent.hasClass("div1")){
              put_image3(content,countNext,0);
              $(".crossbtn").hide(0);
              $('.firstimage').show(0).addClass("zoomin");
              sound_player_only("sound_1_1","crossbtn");
              $('.black_overlay').show(0);
              $('.div2').removeClass("grayout").removeClass("nonclickable");
              $('.div1,.div3').addClass("grayout").addClass("nonclickable");
              $board.on('click',".closecontent",function() {
                  $('.firstimage').hide(0);
                  $('.black_overlay').hide(0);
                  $(".firstimage img").attr("src","");
                  $(".handiconImage").removeClass("generaldiv").addClass("generaldiv1");
              });

          }
          else if(divContent.hasClass("div2")){
              put_image3(content,countNext,1);
              $(".crossbtn").hide(0);
              $('.secondimage').show(0).addClass("zoomin");
              sound_player_only("sound_1_2","crossbtn");
              $('.black_overlay').show(0);
              $('.div3').removeClass("grayout").removeClass("nonclickable");
              $('.div1,.div2').addClass("grayout").addClass("nonclickable");
              $board.on('click',".closecontent",function() {
                  $('.secondimage').hide(0);
                  $('.black_overlay').hide(0);
                  $(".secondimage img").attr("src","");
                  $(".handiconImage").removeClass("generaldiv1").addClass("generaldiv2");
              });

          }
          else if(divContent.hasClass("div3")){
              put_image3(content,countNext,2);
              $(".crossbtn").hide(0);
              $('.thirdimage').show(0).addClass("zoomin");
              sound_player_only("sound_1_3","crossbtn");
              $('.black_overlay').show(0);
              $('.div1').removeClass("grayout").removeClass("nonclickable");
              $('.div2,.div3').addClass("grayout").addClass("nonclickable");
              $board.on('click',".closecontent",function() {
                  $('.thirdimage').hide(0);
                  $('.black_overlay').hide(0);
                  $(".thirdimage img").attr("src","");
                  $(".handiconImage").removeClass("generaldiv2").addClass("generaldiv");
                  navigationcontroller();
              });

          }
      });


  }


});

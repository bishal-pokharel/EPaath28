var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.envconservation,
            }
        ],
        imageblock: [{
            textblock:false,
            imagediv:"imageClass",
            textclass:"image",
            imagestoshow: [
                {
                    imagediv:"talkingladydiv",
                    imgclass: "giff",
                    imgid: 'talkinglady',
                    imgsrc: "",
                },{
                  imagediv:"talkingladydiv",
                  imgclass: "imgg",
                  imgid: 'hi',
                  imgsrc: "",
                }

            ]
        }],
        popupdiv:[
            {
                textClass:"popupdiv",
                txtdiv:"txtdiv",
                textdata:data.string.p3text1,
                button:[{
                    btndiv:"btn1div",
                    btnclass:"reason btncls clickhere",
                    btndata:data.string.how,
                }],
                imageblock: [{
                    imagestoshow: [
                        {
                            animation:true,
                            divclass:"handimg",
                            imgclass: "clickhere handiconImg",
                            imgid: "handicon",
                            imgsrc: "",
                        },
                        {
                            imgclass: "grassland",
                            imgid: "grassland1",
                            imgsrc: "",

                        },
                        {
                            imgclass: "bush2",
                            imgid: "bush2",
                            imgsrc: "",
                        },
                        {
                            imgclass: "bush",
                            imgid: "bush1",
                            imgsrc: "",

                        }

                    ]
                }]

            }
        ]
    },
    // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p3text2,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'image2',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p3text3,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'image3',
                    imgsrc: "",
                },
                {
                    imagediv:"smallplant",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv:"smallplant1",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv:"smallplant2",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant3",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant4",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant5",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant6",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant7",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant8",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                },
                {
                    imagediv: "smallplant9",
                    imgclass: "generalplant",
                    imgid: 'image7',
                    imgsrc: "",
                }

            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p3text4,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution bgimg",
                    imgid: 'image4',
                    imgsrc: "",
                },
                {
                    imagediv:"slideRslow fencediv",
                    imgclass: "bgsolution fenceimg",
                    imgid: 'fence',
                    imgsrc: "",
                },
                {
                    imagediv:"animaldiv",
                    imgclass: "bgsolution animalimg",
                    imgid: 'animal',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p3text5,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'image5',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p3text6,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'image6',
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "page3-css", src: $ref + "css/page3.css", type: createjs.AbstractLoader.CSS},
            // // images
            {id: "talkinglady", src: imgpath+"talking_lady.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "image2", src: imgpath+"page11/deforestation.png", type: createjs.AbstractLoader.IMAGE},
            {id: "image3", src: imgpath+"MALI.png", type: createjs.AbstractLoader.IMAGE},
            {id: "image4", src: imgpath+"page03_bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "image5", src: imgpath+"bg10.png", type: createjs.AbstractLoader.IMAGE},
            {id: "image6", src: imgpath+"bg12_jungle_fire.png", type: createjs.AbstractLoader.IMAGE},
            {id: "image7", src: imgpath+"PLANT-11.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bush2", src: imgpath+"page28/busse01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bush1", src: imgpath+"page28/busses.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grassland1", src: imgpath+"page19/b03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fence", src: imgpath+"fench.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hi", src: imgpath+"talking-lady.png", type: createjs.AbstractLoader.IMAGE},

            {id: "animal", src: imgpath+"animals.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"p3_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p3_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p3_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p3_s3.ogg"},
            {id: "sound_4", src: soundAsset+"p3_s4.ogg"},
            {id: "sound_5", src: soundAsset+"p3_s5.ogg"}

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if((countNext == 0)&& $total_page!=1){
            $nextBtn.css('display', 'none');
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                put_image2(content, countNext);
                $(".handiconImg").hide(0);
                $(".clickhere").attr("disabled","disabled");
                sound_player_only("sound_0","handiconImg","talkinglady",imgpath+"talking-lady.png");
                $board.on('click', '.clickhere', function () {
                    countNext=1;
                    templateCaller();
                });
                animateImg();
                  setTimeout(function(){
                    $(".imgg").css("z-index","2");
                  },3800);
                break;
            case 1:
                sound_player("sound_1");
               break;
            case 2:
                sound_player_only("sound_2","nothing");
                animatePlantation();
                break;
            case 3:
                sound_player("sound_3");
                break;
            case 4:
                sound_player("sound_4");
                break;
             case 5:
                 sound_player("sound_5");
                 break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }

    function sound_player_only(sound_id,showElem,imgclss,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        if(showElem!="nothing") {
            current_sound.on('complete', function () {
                $("."+imgclss).attr("src",changeImg);
                $("." + showElem).show(0);
                $(".clickhere").removeAttr("disabled");
                $('.div1').removeClass("grayout").removeClass("nonclickable");
            });
        }
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }
    function put_image2(content, count) {
        if (content[count].hasOwnProperty('popupdiv')) {
            var contentCount=content[count].popupdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

   function animateImg() {
       $(".bush").hide(0);
       $(".bush2").hide(0);
       setTimeout(function () {
           $(".bush").show(0);
           $(".bush").addClass("slideL");
           setTimeout(function () {
               $(".bush").show(0);
               $(".bush").addClass("bounceItem");
                   setTimeout(function () {
                       $(".bush2").show(0);
                       $(".bush2").addClass("slideR");
                       setTimeout(function () {
                           $(".bush2").show(0);
                           $(".bush2").addClass("bounceItem");
                       }, 1000);
                   }, 1000);
           }, 1000);
       }, 1000);

   }

   function animatePlantation(){
       $(".smallplant,.smallplant1,.smallplant2,.smallplant3,.smallplant4,.smallplant5,.smallplant6,.smallplant7,.smallplant8,.smallplant9").hide(0);
       setTimeout(function(){
           $(".smallplant").show(0).addClass("zoominPlant");
           setTimeout(function(){
               $(".smallplant4").show(0).addClass("zoominPlant");
               setTimeout(function(){
                   $(".smallplant3").show(0).addClass("zoominPlant");
                   setTimeout(function(){
                       $(".smallplant5").show(0).addClass("zoominPlant");
                       setTimeout(function(){
                           $(".smallplant6").show(0).addClass("zoominPlant");
                           setTimeout(function(){
                               $(".smallplant8").show(0).addClass("zoominPlant");
                               setTimeout(function(){
                                   $(".smallplant1").show(0).addClass("zoominPlant");
                                   setTimeout(function(){
                                       $(".smallplant2").show(0).addClass("zoominPlant");
                                       setTimeout(function(){
                                           $(".smallplant7").show(0).addClass("zoominPlant");
                                           setTimeout(function(){
                                               $(".smallplant9").show(0).addClass("zoominPlant");
                                                navigationcontroller();
                                           },500);
                                       },500);
                                   },500);
                               },500);
                           },500);
                       },500);
                   },500);
               },500);
           },500);
       },1000) ;
   }
});

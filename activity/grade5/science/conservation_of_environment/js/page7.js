var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.p7text1
            },
            {
                textclass: "subtitle",
                textdata: data.string.p7text2
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "pic",
                    imgid: 'pic',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs1",
        uppertextblock: [
            {
                textclass: "text1",
                textdata: data.string.p7text3
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "imgcls1",
                    imgid: 'pic1',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "onepagepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs3",
        uppertextblock: [
            {
                textclass: "text1 padd",
                textdata: data.string.p7text4
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "imgcls2",
                    imgid: 'pic2',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "hidecls onepagepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "onepagepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs4",
        uppertextblock: [
            {
                textclass: "text1 padd",
                textdata: data.string.p7text5
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "imgcls3",
                    imgid: 'pic3',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "hidecls onepagepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                },
                {
                    imgclass: "onepagepic3",
                    imgid: 'pic3',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs5",
        uppertextblock: [
            {
                textclass: "text1 padd",
                textdata: data.string.p7text6
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "imgcls4",
                    imgid: 'pic4',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "hidecls onepagepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic3",
                    imgid: 'pic3',
                    imgsrc: "",
                },
                {
                    imgclass: "onepagepic4",
                    imgid: 'pic4',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs6",
        uppertextblock: [
            {
                textclass: "text1 padd",
                textdata: data.string.p7text7
            }
        ],

        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "imgcls5",
                    imgid: 'pic5',
                    imgsrc: "",
                }
            ]
        }]
    },
    // slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "hidecls onepagepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic3",
                    imgid: 'pic3',
                    imgsrc: "",
                },
                {
                    imgclass: "hidecls onepagepic4",
                    imgid: 'pic4',
                    imgsrc: "",
                },
                {
                    imgclass: "onepagepic5",
                    imgid: 'pic5',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        uppertextblock: [
            {
                textclass: "text1 padd",
                textdata: data.string.p7text8
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "onepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic3",
                    imgid: 'pic3',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic4",
                    imgid: 'pic4',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic5",
                    imgid: 'pic5',
                    imgsrc: "",
                },
                {
                    imgclass: "talkinggirl",
                    imgid: 'talkinglady',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bgs2",
        uppertextblock: [
            {
                textclass: "text2",
                textdata: data.string.p7text9
            },
            {
                textclass: "text3",
                textdata: data.string.p7text10
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgclass: "onepic1",
                    imgid: 'pic1',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic2",
                    imgid: 'pic2',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic3",
                    imgid: 'pic3',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic4",
                    imgid: 'pic4',
                    imgsrc: "",
                },
                {
                    imgclass: "onepic5",
                    imgid: 'pic5',
                    imgsrc: "",
                },
                {
                    imgclass: "talkingwomen",
                    imgid: 'talkinglady',
                    imgsrc: "",
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "page7-css", src: $ref + "css/page7.css", type: createjs.AbstractLoader.CSS},
            // // images
            {id: "talkinglady", src: imgpath+"talking_lady.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "pic", src: imgpath+"page43/thinking-girl.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic1", src: imgpath+"page43/girl-walking.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic2", src: imgpath+"page43/manwatering.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic3", src: imgpath+"page43/girl-cycling.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic4", src: imgpath+"page43/swipping.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pic5", src: imgpath+"page43/boy-using-dustbin.png", type: createjs.AbstractLoader.IMAGE},
            // sounds
            {id: "sound_0", src: soundAsset+"p7_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p7_s1.ogg"},
            {id: "sound_3", src: soundAsset+"p7_s3.ogg"},
            {id: "sound_5", src: soundAsset+"p7_s5.ogg"},
            {id: "sound_7", src: soundAsset+"p7_s7.ogg"},
            {id: "sound_9", src: soundAsset+"p7_s9.ogg"},
            {id: "sound_11", src: soundAsset+"p7_s11.ogg"},
            {id: "sound_12", src: soundAsset+"p7_s12.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag){
        console.log("count next >>>>>>>>>>>>>>>>>>>>>>>>"+countNext);
        console.log("count next >>>>>>>>>>>>>>>>>>>>>>>>"+$total_page);
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification() ;
            ole.footerNotificationHandler.lessonEndSetNotification();
        }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                sound_player("sound_0");
                break;
            case 1:
                sound_player("sound_1");
                break;
            case 2:
                animateImage("imgcls1","onepagepic1","bgs1","bgs2",false);
                break;
            case 3:
                sound_player("sound_3");
                break;
            case 4:
                animateImage("imgcls2","onepagepic2","bgs3","bgs2",false);
                break;
            case 5:
                sound_player("sound_5");
                break;
            case 6:
                animateImage("imgcls3","onepagepic3","bgs4","bgs2",true);
                break;
            case 7:
                sound_player("sound_7");
                break;
            case 8:
                animateImage("imgcls4","onepagepic4","bgs7","bgs2",false);
                break;
            case 9:
                sound_player("sound_9");
                break;
            case 10:
                animateImage("imgcls5","onepagepic5","bgs6","bgs2",false);
                break;
            case 11:
                sound_player("sound_11",'talkinggirl',imgpath+'talking-lady.png');
                break;
            case 12:
                sound_player("sound_12",'talkingwomen',imgpath+'talking-lady.png');
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id,imgclss,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            $("."+imgclss).attr("src",changeImg);
            navigationcontroller();
        });
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }

    function put_image2(content, count) {
        if (content[count].hasOwnProperty('popupdiv')) {
            var contentCount=content[count].popupdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

  function animateImage(prevImg,currImg,prevbg,currbg,slideCount){
      $(".hidecls").hide(0);
      $(".contentblock ").removeClass(currbg).addClass(prevbg);
      $("."+currImg).removeClass(currImg).addClass(prevImg);
      setTimeout(function(){
          if(slideCount)
              $("."+prevImg).show(0).addClass("translateR");
          else
              $("."+prevImg).show(0).addClass("translateL");
          setTimeout(function(){
              if(slideCount)
                  $("."+prevImg).removeClass("translateR").removeClass(prevImg).addClass(currImg);
              else
                  $("."+prevImg).removeClass("translateL").removeClass(prevImg).addClass(currImg);
              $(".contentblock ").removeClass(prevbg).addClass(currbg);
              $(".hidecls").show(0);
              $("."+currImg).addClass("zoomin");
              navigationcontroller();
          },1000);
      },500);
    }
});

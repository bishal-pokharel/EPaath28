var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblock: [
            {
                textclass: "title",
                textdata: data.string.envconservation,
            }
        ],
        imageblock: [{
            textblock:false,
            imagediv:"imageClass",
            textclass:"image",
            imagestoshow: [
                {
                    imagediv:"talkingladydiv",
                    imgclass: "giff",
                    imgid: 'talkinglady',
                    imgsrc: "",
                },{
                  imagediv:"talkingladydiv",
                  imgclass: "imgg",
                  imgid: 'hi',
                  imgsrc: "",
                }

            ]
        }],
        popupdiv:[
            {
                textClass:"popupdiv",
                txtdiv:"txtdiv",
                textdata:data.string.p4text1,
                button:[{
                    btndiv:"btndiv",
                    btnclass:"reason clickhere",
                    btndata:data.string.how,
                }],
                imageblock: [{
                    imagestoshow: [
                        {
                            animation:true,
                            divclass:"handimg",
                            imgclass: "clickhere handiconImg",
                            imgid: "handicon",
                            imgsrc: "",
                        },
                        {
                            imgclass: "frontimg1",
                            imgid: "frontimg1",
                            imgsrc: "",
                        },
                        {
                            imgclass: "frontimg2",
                            imgid: "frontimg2",
                            imgsrc: "",
                        }
                    ]
                }]

            }
        ]
    },
    // slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p4text2,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'imag2',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p4text3,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'imag3',
                    imgsrc: "",
                }
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg2",
        uppertextblock: [
            {
                textclass: "title1",
                textdata: data.string.p4text4,
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imagediv:"bgdiv",
                    imgclass: "bgsolution",
                    imgid: 'imag4',
                    imgsrc: "",
                },
                {
                    imagediv:"imgcls5",
                    imgclass: "img5",
                    imgid: 'imag5',
                    imgsrc: "",
                },
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "page4-css", src: $ref + "css/page4.css", type: createjs.AbstractLoader.CSS},
            // // images
            {id: "talkinglady", src: imgpath+"talking_lady.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "handicon", src: imgpath+"hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "imag2", src: imgpath+"bg07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "imag3", src: imgpath+"bg06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "imag4", src: imgpath+"mali-bgbg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "imag5", src: imgpath+"WATERING-CAN-GIF.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "frontimg1", src: imgpath+"Updatedpage28/bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "frontimg2", src: imgpath+"page28/wall.png", type: createjs.AbstractLoader.IMAGE},
            {id: "hi", src: imgpath+"talking-lady.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_0", src: soundAsset+"p4_s0.ogg"},
            {id: "sound_1", src: soundAsset+"p4_s1.ogg"},
            {id: "sound_2", src: soundAsset+"p4_s2.ogg"},
            {id: "sound_3", src: soundAsset+"p4_s3.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag){
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean'?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if((countNext == 0)&& $total_page!=1){
            $nextBtn.css('display', 'none');
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification() ;
        }
    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);


        // highlight any text inside board div with datahighlightflag set true
        // texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext);
        switch(countNext){
            case 0:
                put_image2(content, countNext);
                $(".handiconImg").hide(0);
                $(".clickhere").attr("disabled","disabled");
                sound_player_only("sound_0","handiconImg","talkinglady",imgpath+"talking-lady.png");
                animateImg();
                $board.on('click', '.clickhere', function () {
                    countNext=1;
                    templateCaller();
                });
                setTimeout(function(){
                  $(".imgg").css("z-index","2");
                },3200);
                break;
            case 1:
                sound_player("sound_1");
                break;
            case 2:
                sound_player("sound_2");
                break;
            case 3:
                sound_player("sound_3");
                break;
            default:
                navigationcontroller();
                break;
        }
    }

    function nav_button_controls(delay_ms) {
        timeoutvar = setTimeout(function () {
            if (countNext == 0) {
                $nextBtn.show(0);
            } else if (countNext > 0 && countNext == $total_page - 1) {
                $prevBtn.show(0);
                ole.footerNotificationHandler.pageEndSetNotification();
            } else {
                $prevBtn.show(0);
                $nextBtn.show(0);
            }
        }, delay_ms);
    }

    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller();
        });
    }

    function sound_player_only(sound_id,showElem,imgclss,changeImg) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        if(showElem!="nothing") {
            current_sound.on('complete', function () {
                $("."+imgclss).attr("src",changeImg);
                $("." + showElem).show(0);
                $(".clickhere").removeAttr("disabled");
                $('.div1').removeClass("grayout").removeClass("nonclickable");
            });
        }
    }


    function put_image(content, count) {
        var contentCount=content[count];
        var imageblockcontent=contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent,contentCount)
    }

    function put_image2(content, count) {
        if (content[count].hasOwnProperty('popupdiv')) {
            var contentCount=content[count].popupdiv[0];
            var imageblockcontent=contentCount.hasOwnProperty('imageblock');
            dynamicimageload(imageblockcontent,contentCount);
        }
    }

    function dynamicimageload(imageblockcontent,contentCount){
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function animateImg() {
        $(".frontimg1,.frontimg2").hide(0);
        setTimeout(function () {
            $(".frontimg1").show(0).addClass("slideL");
                    setTimeout(function () {
                        $(".frontimg2").show(0).addClass("slideR");
                    }, 1000);
        }, 1000);
    }
});

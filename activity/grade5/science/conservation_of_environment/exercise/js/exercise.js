var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background1",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques1
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans1
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans2
            }
        ]

    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background2",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques2
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans3
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans4
            }
        ]

    },
    // slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background3",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques3
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans5
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans6
            }
        ]

    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background4",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques4
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans7
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans8
            }
        ]

    },
    // slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background5",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques5
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans9
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans10
            }
        ]

    },
    // slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background6",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques6
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans11
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans12
            }
        ]

    },
    // slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background7",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques7
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans13
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans14
            }
        ]

    },
    // slide7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background8",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques8
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans15
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans16
            }
        ]

    },
    // slide8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background9",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques9
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans17
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans18
            }
        ]

    },
    // slide9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background10",
        uppertextblock: [
            {
                textclass: "quesn1",
                textdata: data.string.p1ques10
            }
        ],
        uprtxtblkadditionalclass:"tittxt",
        uprtxtblk:[{
          textclass: "titleques",
          textdata: data.string.ques
        }],
        button:[
            {
                btnclass:"right",
                btntext:data.string.p1ans19
            },
            {
                btnclass:"wrong",
                btntext:data.string.p1ans20
            }
        ]
    },
    // slide10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "background10",
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;

    var $total_page = content.length;
    // loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;

    var lampTemplate = new LampTemplate();
    lampTemplate.init($total_page-1);


    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "ex", src: $ref + "css/exercisetemplate.css", type: createjs.AbstractLoader.CSS},
            {id: "ex1", src: $ref + "css/exercise1.css", type: createjs.AbstractLoader.CSS},
            //sound
            {id: "exer", src: soundAsset+"ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());



    /*======================================================
     =            Navigation Controller Function            =
     ======================================================*/

    function navigationcontroller(islastpageflag) {
        typeof islastpageflag === "undefined" ?
            islastpageflag = false :
            typeof islastpageflag != 'boolean' ?
                alert("NavigationController : Hi Master, please provide a boolean parameter") :
                null;

        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page - 1) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    /*=====  End of user navigation controller function  ======*/


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);

        put_image(content, countNext);
        shufflebutton();
        checkans();
        if(countNext==0){
          sound_player("exer");
        }
    }


    function sound_player(sound_id) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();

    }


    function put_image(content, count) {
        if (content[count].hasOwnProperty('imageblock')) {
            var imageblock = content[count].imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }



    function templateCaller() {
        $nextBtn.css('display', 'none');
        generaltemplate();
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                lampTemplate.gotoNext();
                templateCaller();
                break;
        }
    });


    function shufflebutton(){
        var btndiv = document.querySelector('#btn');
        for (var i = btndiv.children.length; i >= 0; i--) {
            btndiv.appendChild(btndiv.children[Math.random() * i | 0]);
        }
        $("#btn :first-child").addClass("btn1");
        $("#btn :first-child").next().addClass("btn2");
    }

    function checkans(){
        $(".right").removeClass("rightans").removeAttr("disabled").addClass("bgclr");
        $(".wrong").removeClass("wrongans").removeAttr("disabled").addClass("bgclr");
        $(".right").click(function(){
            play_correct_incorrect_sound(1);
            $(this).removeClass("bgclr").addClass("rightans");
            $("this").attr("disabled","disabled");
            $(".wrong").attr("disabled","disabled");
            lampTemplate.update(true);
            navigationcontroller();
        });
        $(".wrong").click(function(){
            play_correct_incorrect_sound(0);
            $(this).removeClass("bgclr").addClass("wrongans");
            lampTemplate.update(false);

        });

    }
});

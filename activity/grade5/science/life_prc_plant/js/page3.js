var imgpath = $ref + "/images/img_diy/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "content2 centertext",
                textdata: data.string.dragdropmsg
            },
            {
                textdiv:"draggable drag1",
                textclass: "content2 centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"draggable drag2",
                textclass: "content2 centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"draggable drag3",
                textclass: "content2 centertext",
                textdata: data.string.reproduction
            },
            {
                textdiv:"draggable drag4",
                textclass: "content2 centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"droppable drop1",
                textclass: "content2 centertext",
                ans: data.string.inttrans
            },
            {
                textdiv:"droppable drop2",
                textclass: "content2 centertext",
                ans: data.string.excretion
            },
            {
                textdiv:"droppable drop3",
                textclass: "content2 centertext",
                ans: data.string.reproduction
            },
            {
                textdiv:"droppable drop4",
                textclass: "content2 centertext",
                ans: data.string.respiration
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "transportation",
                    imgclass: "relativecls img1",
                    imgid: 'transImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "excretion",
                    imgclass: "relativecls img2",
                    imgid: 'excreImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "reproduction",
                    imgclass: "relativecls img3",
                    imgid: 'reproImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "respiration",
                    imgclass: "relativecls img4",
                    imgid: 'respImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "monkey",
                    imgclass: "relativecls img5",
                    imgid: 'welldoneImg',
                    imgsrc: ""
                },
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "transImg", src: imgpath + "transportation.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "excreImg", src: imgpath + "excretion.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "reproImg", src: imgpath + "reproduction.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "respImg", src: imgpath + "respiration.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "welldoneImg", src: imgpath + "welldone02.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s3_p1.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            default:
                sound_player("sound_"+(countNext+1),false);
                var a = ["draggable drag1","draggable drag2","draggable drag3","draggable drag4"];
                shufflehint("draggable",a);
                dragdrop();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint(optionName,a){
        var optiondiv = $(".contentblock");
        for (var i = 4; i >= 0; i--) {
            optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
        }
        optiondiv.find("."+optionName).removeClass().addClass("current");
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find("."+optionName).removeClass("current");
    }

    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if(ui.draggable.text().toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("p").html(ui.draggable.text());
                    $(this).addClass("correctans");
                    texthighlight($board);
                    count++;
                    if(count>3){
                        $(".monkey").css("opacity","1");
                        navigationcontroller(countNext,$total_page);
                    }
                }
                else {
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }
});

var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/arrows/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"chtitle",
                textclass: "chapter centertext",
                textdata: data.string.chaptertitle
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "coverpage",
                    imgclass: "relativecls img1",
                    imgid: 'coverpageImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "thinkingbox",
                    imgclass: "relativecls img2",
                    imgid: 'boxImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle appear",
                textclass: "title centertext",
                textdata: data.string.p1text0
            },
            {
                textdiv:"subtitle appear",
                textclass: "content1 centertext",
                textdata: data.string.p1text1
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p1text0
            },
            {
                textdiv:"subtitle appear",
                textclass: "content1 centertext",
                textdata: data.string.p1text2
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img1",
                    imgid: 'sqImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p1text3
                        }
                    ]
                }
            ]
        }],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel1",
                    imgclass: "relativecls img2",
                    imgid: 'sq1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"purpleclr",
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p1text4
                        }
                    ]
                }
            ]
        }],
    },
    // slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"options nutrition",
                textclass: "content1 centertext",
                textdata: data.string.nutrition
            },
            {
                textdiv:"options respiration",
                textclass: "content1 centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"options internaltransport",
                textclass: "content1 centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"options excretion",
                textclass: "content1 centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"options reproduction",
                textclass: "content1 centertext",
                textdata: data.string.reproduction
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel2",
                    imgclass: "relativecls img1",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial2",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p1text5
                        }
                    ]
                }
            ]
        }],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "coverpageImg", src: imgpath + "cover_page.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath + "thinking_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sqImg", src: imgpath + "squirrel02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sq1Img", src: imgpath + "squirrel01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sq2Img", src: imgpath + "squirrel03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubble01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s1_p1.ogg"},
            {id: "sound_2", src: soundAsset + "s1_p2.ogg"},
            {id: "sound_3", src: soundAsset + "s1_p3.ogg"},
            {id: "sound_4", src: soundAsset + "s1_p4.ogg"},
            {id: "sound_5", src: soundAsset + "s1_p5.ogg"},
            {id: "sound_6", src: soundAsset + "s1_p6.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        appearoneafteranother(0);
        switch (countNext) {
            default:
                sound_player("sound_"+(countNext+1),true);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }

    function appearoneafteranother(delaytime){
        $(".appear").hide();
        clearInterval(setTime);
        setTime = setInterval(function(index){
            $(".appear").each(function(){
                $(this).delay(delaytime).fadeIn(1000);
                 delaytime = delaytime +1500;
            });
        });
    }
});

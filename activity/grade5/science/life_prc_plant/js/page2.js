var imgpath = $ref + "/images/";
var imgpath1 = $ref + "/images/Reproduction_of_plant/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var maincontent = [
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1",
        textblock: [
            {
                textdiv:"options nutrition",
                textclass: "content1 centertext",
                textdata: data.string.nutrition
            },
            {
                textdiv:"options respiration",
                textclass: "content1 centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"options internaltransport",
                textclass: "content1 centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"options excretion",
                textclass: "content1 centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"options reproduction",
                textclass: "content1 centertext",
                textdata: data.string.reproduction
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img1",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p2text1
                        }
                    ]
                }
            ]
        }],
    },
];

var contentnutri = [
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 nutr",
        textblock: [
            {
                textdiv:"opttitle",
                textclass: "chapter centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            },
            {
                textdiv:"maintitle",
                textclass: "title centertext",
                textdata: data.string.p2text2
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"slidetext",
                textclass: "content2 centertext",
                textdata: data.string.p2text4
            },
         ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "dial2",
                    imgclass: "relativecls img1",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2",
                            imgtxt:data.string.p2text3
                        }
                    ]
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img2",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "squirrel",
                    imgclass: "relativecls img1",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p2text5
                        }
                    ]
                }
            ]
        }],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "watering",
                    imgclass: "relativecls img2",
                    imgid: 'wateringplantImg',
                    imgsrc: ""
                },

                {
                    imgdiv: "dial3",
                    imgclass: "relativecls img3",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"textclr",
                            imgcls:"content3 appear ",
                            imgtxt:data.string.p2text6
                        }
                    ]
                },
                {
                    imgdiv: "sun",
                    imgclass: "relativecls img4",
                    imgid: 'sunImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"appear",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p2text7
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower2",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial4",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"textclr",
                            imgcls:"content2 ",
                            imgtxt:data.string.p2text8
                        }
                    ]
                },
            ]
        }],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p2text9
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"nutricod",
                textclass: "content2 centertext",
                textdata: data.string.cod
            },
            {
                textdiv:"nutriwater",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"nutrimineral",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "photoflower1",
                    imgclass: "relativecls img1",
                    imgid: 'photosynthesisimg',
                    imgsrc: ""
                },
                {
                    imgdiv: "watering",
                    imgclass: "relativecls img2",
                    imgid: 'wateringplantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sungif",
                    imgclass: "relativecls img4",
                    imgid: 'sungifImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p2text10
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"nutricod",
                textclass: "content2 centertext",
                textdata: data.string.cod
            },
            {
                textdiv:"nutriwater",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"nutrimineral",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"nutrioxy",
                textclass: "content2 centertext",
                textdata: data.string.oxy   
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "photoflower1",
                    imgclass: "relativecls img1",
                    imgid: 'photosynthesisimg',
                    imgsrc: ""
                },
                {
                    imgdiv: "watering",
                    imgclass: "relativecls img2",
                    imgid: 'wateringplantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sungif",
                    imgclass: "relativecls img4",
                    imgid: 'sungifImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p2text11
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"nutricod",
                textclass: "content2 centertext",
                textdata: data.string.cod
            },
            {
                textdiv:"nutriwater",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"nutrimineral",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"nutrioxy",
                textclass: "content2 centertext",
                textdata: data.string.oxy   
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "photoflower1",
                    imgclass: "relativecls img1",
                    imgid: 'photosynthesisimg',
                    imgsrc: ""
                },
                {
                    imgdiv: "watering",
                    imgclass: "relativecls img2",
                    imgid: 'wateringplantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "sungif",
                    imgclass: "relativecls img4",
                    imgid: 'sungifImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial1",
                    imgclass: "relativecls img5",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"textclr",
                            imgcls:"content2 ",
                            imgtxt:data.string.p2text12
                        }
                    ]
                },
            ]
        }],
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg nutr",
        textblock: [
            {
                textdiv:"t1",
                textclass: "title centertext",
                textdata: data.string.dyk
            },
            {
                textdiv:"t2",
                textclass: "content2 centertext",
                textdata: data.string.p2text13
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "venus",
                    imgclass: "relativecls img1",
                    imgid: 'venusImg',
                    imgsrc: ""
                }
            ]
        }],
    },
]

var contentresp = [
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 res",
        textblock: [
            {
                textdiv:"opttitle",
                textclass: "chapter centertext",
                textdata: data.string.respiration
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"emptydiv1"
            },
            {
                textdiv:"question",
                textclass: "content2 centertext",
                textdata: data.string.p3text1,
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"option opt1",
                textclass: "content2 centertext",
                textdata: data.string.co,
                ans:"correct"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"option opt2",
                textclass: "content2 centertext",
                textdata: data.string.oxygen
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower2",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "rdial ",
                    imgclass: "relativecls img2",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p3text1_1
                        }
                    ]
                },
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg1 res",
        textblock: [
            {
                textdiv:"question1",
                textclass: "content2 centertext",
                textdata: data.string.p3text2,
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"option1 opt1",
                textclass: "content2 centertext",
                textdata: data.string.co
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"option1 opt2",
                textclass: "content2 centertext",
                textdata: data.string.oxygen,
                ans:"correct"
            },
        ],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"emptydiv2"
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "respsquirrel show1",
                    imgclass: "relativecls img2",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "rdial1 show2",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content4 appear ",
                            imgtxt:data.string.p3text3
                        }
                    ]
                },
            ]
        }]
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            }
            ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "rdial1",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"subscript",
                            imgcls:"diyfont appear paddingcls ",
                            imgtxt:data.string.p3text3_1
                        }
                    ]
                },
            ]
        }]
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p3text4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
    // slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen o1",
                textclass: "content centertext",
                textdata: data.string.oxy
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen o2",
                textclass: "content centertext",
                textdata: data.string.oxy
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "rdial ",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p3text5
                        }
                    ]
                },
            ]
        }],
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"respt1 show1",
                textclass: "content4 centertext",
                textdata: data.string.p3text6
            },
            {
                textdiv:"water show1",
                textclass: "content2 centertext",
                textdata: data.string.water

            },
            {
                textdiv:"energy show4",
                textclass: "content2 centertext",
                textdata: data.string.energy

            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen oxy1 show2",
                textclass: "content centertext",
                textdata: data.string.oxy
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen co1 show3",
                textclass: "content centertext",
                textdata: data.string.cod
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leaf",
                    imgclass: "relativecls img1",
                    imgid: 'leafImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1 show1",
                    imgclass: "relativecls img2",
                    imgid: 'bluearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow2 show4",
                    imgclass: "relativecls img3",
                    imgid: 'yellowarImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"respt1 show1 zoomInEffect",
                textclass: "content4 centertext",
                textdata: data.string.p3text6_1
            },
            {
                textdiv:"water show1",
                textclass: "content2 centertext",
                textdata: data.string.water

            },
            {
                textdiv:"energy show4",
                textclass: "content2 centertext",
                textdata: data.string.energy

            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen oxy1 show2",
                textclass: "content centertext",
                textdata: data.string.oxy
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"oxygen co1 show3",
                textclass: "content centertext",
                textdata: data.string.cod
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leaf1",
                    imgclass: "relativecls img1",
                    imgid: 'leafImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1 show1",
                    imgclass: "relativecls img2",
                    imgid: 'bluearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow2 show4",
                    imgclass: "relativecls img3",
                    imgid: 'yellowarImg',
                    imgsrc: ""
                }
            ]
        }],
    },
    //slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg3 res",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"question2",
                textclass: "content2 centertext",
                textdata: data.string.p3text7
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"draggable drag1",
                textclass: "content2 centertext",
                textdata: data.string.p3text8,
                ans:"ans1"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"draggable drag2",
                textclass: "content2 centertext",
                textdata: data.string.p3text9,
                ans:"ans2"
            },
            {
                textdiv:"answer1",
                textclass: "content centertext",
                textdata: data.string.p3text10

            },
            {
                textdiv:"answer2",
                textclass: "content centertext",
                textdata: data.string.p3text11
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"droppable drop1",
                textclass: "content2 centertext",
                textdata: data.string.p3text9,
                ans:"ans2"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"droppable drop2",
                textclass: "content2 centertext",
                textdata: data.string.p3text8,
                ans:"ans1"
            }
        ]
    }
  ];

var contentinttrans=[
    //slide1
       {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 inttrans",
        textblock: [{
                textdiv:"opttitle",
                textclass: "content centertext",
                textdata: data.string.inttrans
        },
        {
                textdiv:"opttitle",
                textclass: "content centertext",
                textdata: data.string.inttrans
        }],

        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }]
      },

    //slide 2
        {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 inttrans",
            textblock: [
                {
                    textdiv:"topictitleleft",
                    textclass: "diyfont centertext",
                    textdata: data.string.inttrans
                }
                ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "diallatest",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"subscript",
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p4text1
                        }
                    ]
                },
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 inttrans",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.inttrans
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "diallatest",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"subscript",
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p4text2
                        }
                    ]
                },
            ]
        }]
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg inttrans",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p3text4
            },
            {
                textdiv:"rootdiv"
            },
            {
                textdiv:"w1 show3",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"m1 show3",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "transdial show1",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content2 appear ",
                            imgtxt:data.string.p3text4_1
                        }
                    ]
                },
                {
                    imgdiv: "planttakingminerals show2",
                    imgclass: "relativecls img3",
                    imgid: 'rootwtrminImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "waterarrow show3",
                    imgclass: "relativecls img4",
                    imgid: 'waterarrImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mineralarrow show3",
                    imgclass: "relativecls img5",
                    imgid: 'mineralarrImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg inttrans",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p4text3
            },
            {
                textdiv:"w1 show3",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"m1 show3",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "planttakingminerals show2",
                    imgclass: "relativecls img3",
                    imgid: 'rootwtrminImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "waterarrow show3",
                    imgclass: "relativecls img4",
                    imgid: 'waterarrImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mineralarrow show3",
                    imgclass: "relativecls img5",
                    imgid: 'mineralarrImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg inttrans",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p4text4
            },
            {
                textdiv:"w1 show3",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                textdiv:"m1 show3",
                textclass: "content2 centertext",
                textdata: data.string.mineral
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "planttakingminerals show2",
                    imgclass: "relativecls img3",
                    imgid: 'rootwtrminImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "waterarrow show3",
                    imgclass: "relativecls img4",
                    imgid: 'waterarrImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mineralarrow show3",
                    imgclass: "relativecls img5",
                    imgid: 'mineralarrImg',
                    imgsrc: ""
                },
            ]
        }],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg inttrans",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"inttext1 fadeInEffect",
                textclass: "content1 centertext",
                textdata: data.string.p4text5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "tree",
                    imgclass: "relativecls img1",
                    imgid: 'treeImg',
                    imgsrc: ""
                },
            ]
        }],
    },

];


var contentexcre=[
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 excre",
        textblock: [
            {
                textdiv:"opttitle",
                textclass: "chapter centertext",
                textdata: data.string.excretion
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"subscript",
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p5text1
                        }
                    ]
                },
            ]
        }]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial1",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"subscript",
                            imgcls:"content4 appear ",
                            imgtxt:data.string.p5text2
                        }
                    ]
                },
            ]
        }]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p5text3
            },
            {
                textdiv:"exwater show1",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"exoxygen show1",
                textclass: "content2 centertext",
                textdata: data.string.oxy
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"emptydiv"
            },
            {
                textdiv:"clickleafdiv ldiv1"
            },
            {
                textdiv:"clickleafdiv ldiv2"
            },
            {
                textdiv:"exwater2 show1",
                textclass: "content2 centertext",
                textdata: data.string.water
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"exoxygen2 show1",
                textclass: "content2 centertext",
                textdata: data.string.oxy
            },
            {
                textdiv:"pore",
                textclass: "content2 centertext",
                textdata: data.string.pore
            },

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial2",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont1 appear ",
                            imgtxt:data.string.p5text4
                        }
                    ]
                },
                {
                    imgdiv: "oxygencoming",
                    imgclass: "relativecls img3",
                    imgid: 'leafoxyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial3",
                    imgclass: "relativecls img4",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content4 appear ",
                            imgtxt:data.string.p5text3_1
                        }
                    ]
                },
                {
                    imgdiv: "arrblck",
                    imgclass: "relativecls img5",
                    imgid: 'blckarr1Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"emptydiv"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p5text5
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"exoxygen show1",
                textclass: "content2 centertext",
                textdata: data.string.cod
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respflower1",
                    imgclass: "relativecls img1",
                    imgid: 'flower3Img',
                    imgsrc: ""
                },
            ]
        }],
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg excre",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.excretion
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"boxtext",
                textclass: "content2 centertext",
                textdata: data.string.p5text6
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "plant",
                    imgclass: "relativecls img1",
                    imgid: 'plantImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "gum s show2",
                    imgclass: "relativecls img2",
                    imgid: 'gumImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "latex s show4",
                    imgclass: "relativecls img3",
                    imgid: 'latexImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "resins s show6",
                    imgclass: "relativecls img4",
                    imgid: 'resinsImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arr exa1 show1",
                    imgclass: "relativecls img5",
                    imgid: 'blckarrImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arr exa2 show3",
                    imgclass: "relativecls img6",
                    imgid: 'blckarrImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arr exa3 show5",
                    imgclass: "relativecls img7",
                    imgid: 'blckarrImg',
                    imgsrc: ""
                },
            ]
        }],
    }
];
var contentrepro=[
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg1 repro",
        textblock: [
            {
                textdiv:"opttitle",
                textclass: "chapter centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "background",
                    imgclass: "relativecls img1",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
            ]
        }]
    },
    //    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
            ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial1",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p6text1
                        }
                    ]
                },
            ]
        }]
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p6text2
                        }
                    ]
                },
            ]
        }]
    },
//    slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"righttext",
                textclass: "content2 centertext",
                textdata: data.string.p6text3
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "beebg",
                    imgclass: "relativecls img1",
                    imgid: 'bgbeeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "honeybee",
                    imgclass: "relativecls img2",
                    imgid: 'honeybeeImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"righttext",
                textclass: "content2 centertext",
                textdata: data.string.p6text4
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "beebg",
                    imgclass: "relativecls img1",
                    imgid: 'bgbeeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "honeybee",
                    imgclass: "relativecls img2",
                    imgid: 'honeybeeImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "reprobg repro",
        textblock: [
            {
                textdiv:"reprotext1",
                textclass: "content2 centertext",
                textdata: data.string.p6text5
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "life lcyc1",
                    imgclass: "relativecls img1",
                    imgid: 'img1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc2",
                    imgclass: "relativecls img2",
                    imgid: 'img2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc3",
                    imgclass: "relativecls img3",
                    imgid: 'img3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc4",
                    imgclass: "relativecls img4",
                    imgid: 'img4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc5",
                    imgclass: "relativecls img5",
                    imgid: 'img5Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc6",
                    imgclass: "relativecls img6",
                    imgid: 'img6Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "life lcyc7",
                    imgclass: "relativecls img7",
                    imgid: 'img7Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar1",
                    imgclass: "relativecls img8",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar2",
                    imgclass: "relativecls img9",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar3",
                    imgclass: "relativecls img10",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar4",
                    imgclass: "relativecls img11",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar5",
                    imgclass: "relativecls img12",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar6",
                    imgclass: "relativecls img13",
                    imgid: 'garImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "larrow gar7",
                    imgclass: "relativecls img14",
                    imgid: 'garImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "respsquirrel",
                    imgclass: "relativecls img2",
                    imgid: 'sq3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "exdial1",
                    imgclass: "relativecls img3",
                    imgid: 'dial1Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p6text6
                        }
                    ]
                },
            ]
        }]
    },


    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "rbg2 repro",
        textblock: [
            {
                textdiv:"ferntext fern",
                textclass: "content2 centertext",
                textdata: data.string.fern
            },
            {
                textdiv:"mossestext mosses",
                textclass: "content2 centertext",
                textdata: data.string.mosses
            },
            {
                textdiv:"topictitleleft",
                textclass: "diyfont centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "reprosquirrel",
                    imgclass: "relativecls img1",
                    imgid: 'sq2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "reprodial",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            datahighlightflag:true,
                            datahighlightcustomclass:"yellowbg",
                            imgcls:"diyfont appear ",
                            imgtxt:data.string.p6text7
                        }
                    ]
                },
                {
                    imgdiv: "fern fernimg",
                    imgclass: "relativecls img3",
                    imgid: 'fernImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "mosses mossesimg",
                    imgclass: "relativecls img4",
                    imgid: 'mossesImg',
                    imgsrc: ""
                },
            ]
        }]
    }
];
var visitedcontent =
    {"nutrition":false,
        "respiration":false,
        "inttrans":false,
        "excretion":false,
        "reproduction":false};

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = maincontent.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();
    var mergecontent = [];
    // var visitedcontent =
    //     {"nutrition":false,
    //     "respiration":false,
    //     "inttrans":false,
    //     "excretion":false,
    //     "reproduction":false};

    var nutritionContent = $.merge(mergecontent,maincontent);
        nutritionContent = $.merge(mergecontent,contentnutri);
        mergecontent = [];
    var respirationContent = $.merge(mergecontent,maincontent)
        respirationContent = $.merge(mergecontent,contentresp);
        mergecontent = [];
    var inttransContent = $.merge(mergecontent,maincontent)
        inttransContent = $.merge(mergecontent,contentinttrans);
        mergecontent = [];
    var excretionContnent = $.merge(mergecontent,maincontent)
        excretionContnent = $.merge(mergecontent,contentexcre);
        mergecontent = [];
    var reproductionContent = $.merge(mergecontent,maincontent)
        reproductionContent = $.merge(mergecontent,contentrepro);

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "sq2Img", src: imgpath + "squirrel03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sq3Img", src: imgpath + "squirrel02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubble01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dial1Img", src: imgpath + "bubble02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bgImg", src: imgpath + "bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flowerImg", src: imgpath + "talking_sun01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "flowerImg1", src: imgpath + "talking_sun01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flower1Img", src: imgpath + "talking_sun02.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "flower3Img", src: imgpath + "talking_sun02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wateringplantImg", src: imgpath + "watering_plant.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "sunImg", src: imgpath + "sun.png", type: createjs.AbstractLoader.IMAGE},
            {id: "movingleafImg", src: imgpath + "moving_leaf.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "venusImg", src: imgpath + "venus2.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "leafImg", src: imgpath + "leaf.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellowarImg", src: imgpath + "yellow_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bluearImg", src: imgpath + "blue_arrow03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "leafoxyImg", src: imgpath + "oxygen-coming-out.png", type: createjs.AbstractLoader.IMAGE},
            {id: "plantImg", src: imgpath + "plant_latex_gum_resins.png", type: createjs.AbstractLoader.IMAGE},
            {id: "gumImg", src: imgpath + "gum.png", type: createjs.AbstractLoader.IMAGE},
            {id: "latexImg", src: imgpath + "latex.png", type: createjs.AbstractLoader.IMAGE},
            {id: "resinsImg", src: imgpath + "resins.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blckarrImg", src: imgpath + "black_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "blckarr1Img", src: imgpath + "black_arrow_01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "rootwtrminImg", src: imgpath + "talking_water_menirals01.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "treeImg", src: imgpath + "tree_taking_minerals.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "bgbeeImg", src: imgpath + "bg_bee01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "honeybeeImg", src: imgpath + "honey_bee.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "waterarrImg", src: imgpath + "blue_arrow02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mineralarrImg", src: imgpath + "arrow_mineral.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img1Img", src: imgpath1 + "img01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img2Img", src: imgpath1 + "img02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img3Img", src: imgpath1 + "img03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img4Img", src: imgpath1 + "img04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img5Img", src: imgpath1 + "img05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img6Img", src: imgpath1 + "img06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "img7Img", src: imgpath1 + "img07.png", type: createjs.AbstractLoader.IMAGE},
            {id: "garImg", src: imgpath1 + "arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fernImg", src: imgpath + "fern.png", type: createjs.AbstractLoader.IMAGE},
            {id: "mossesImg", src: imgpath + "mosses.png", type: createjs.AbstractLoader.IMAGE},
            {id: "photosynthesisimg", src: imgpath + "plant_taking_food.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "sungifImg", src: imgpath + "sun.gif", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s2_p1.ogg"},

            {id: "sound_nutri_2", src: soundAsset + "s2_p1_nutrition_2.ogg"},
            {id: "sound_nutri_3", src: soundAsset + "s2_p1_nutrition_3.ogg"},
            {id: "sound_nutri_3_1", src: soundAsset + "s2_p1_nutrition_3_1.ogg"},
            {id: "sound_nutri_4", src: soundAsset + "s2_p1_nutrition_4.ogg"},
            {id: "sound_nutri_5", src: soundAsset + "s2_p1_nutrition_5.ogg"},
            {id: "sound_nutri_6", src: soundAsset + "s2_p1_nutrition_6.ogg"},
            {id: "sound_nutri_6_1", src: soundAsset + "s2_p1_nutrition_6_1.ogg"},
            {id: "sound_nutri_7", src: soundAsset + "s2_p1_nutrition_7.ogg"},
            {id: "sound_nutri_8", src: soundAsset + "s2_p1_nutrition_8.ogg"},
            {id: "sound_nutri_9", src: soundAsset + "s2_p1_nutrition_9.ogg"},
            {id: "sound_nutri_9_1", src: soundAsset + "s2_p1_nutrition_9_1.ogg"},
            {id: "sound_nutri_10", src: soundAsset + "s2_p1_nutrition_10.ogg"},

            {id: "sound_resp_2", src: soundAsset + "s2_p1_respiration_2.ogg"},
            {id: "sound_resp_3", src: soundAsset + "s2_p1_respiration_3.ogg"},
            {id: "sound_resp_4", src: soundAsset + "s2_p1_respiration_4.ogg"},
            {id: "sound_resp_5", src: soundAsset + "s2_p1_respiration_5.ogg"},
            {id: "sound_resp_6", src: soundAsset + "s2_p1_respiration_6.ogg"},
            {id: "sound_resp_7", src: soundAsset + "s2_p1_respiration_7.ogg"},
            {id: "sound_resp_8", src: soundAsset + "s2_p1_respiration_8.ogg"},
            {id: "sound_resp_9", src: soundAsset + "s2_p1_respiration_9.ogg"},
            {id: "sound_resp_10", src: soundAsset + "s2_p1_respiration_10.ogg"},
            {id: "sound_resp_11", src: soundAsset + "s2_p1_respiration_11.ogg"},

            {id: "sound_int_2", src: soundAsset + "s2_p1_internaltransport_2.ogg"},
            {id: "sound_int_3", src: soundAsset + "s2_p1_internaltransport_3.ogg"},
            {id: "sound_int_4", src: soundAsset + "s2_p1_internaltransport_4.ogg"},
            {id: "sound_int_5", src: soundAsset + "s2_p1_internaltransport_5.ogg"},
            {id: "sound_int_5_1", src: soundAsset + "s2_p1_internaltransport_5_1.ogg"},
            {id: "sound_int_6", src: soundAsset + "s2_p1_internaltransport_6.ogg"},
            {id: "sound_int_7", src: soundAsset + "s2_p1_internaltransport_7.ogg"},
            {id: "sound_int_8", src: soundAsset + "s2_p1_internaltransport_8.ogg"},

            {id: "sound_ex_2", src: soundAsset + "s2_p1_excretion_2.ogg"},
            {id: "sound_ex_3", src: soundAsset + "s2_p1_excretion_3.ogg"},
            {id: "sound_ex_4", src: soundAsset + "s2_p1_excretion_4.ogg"},
            {id: "sound_ex_5", src: soundAsset + "s2_p1_excretion_5.ogg"},
            {id: "sound_ex_6", src: soundAsset + "s2_p1_excretion_6.ogg"},
            {id: "sound_ex_6_1", src: soundAsset + "s2_p1_excretion_6_1.ogg"},
            {id: "sound_ex_7", src: soundAsset + "s2_p1_excretion_7.ogg"},
            {id: "sound_ex_8", src: soundAsset + "s2_p1_excretion_8.ogg"},

            {id: "sound_repro_2", src: soundAsset + "s2_p1_reproduction_2.ogg"},
            {id: "sound_repro_3", src: soundAsset + "s2_p1_reproduction_3.ogg"},
            {id: "sound_repro_4", src: soundAsset + "s2_p1_reproduction_4.ogg"},
            {id: "sound_repro_5", src: soundAsset + "s2_p1_reproduction_5.ogg"},
            {id: "sound_repro_6", src: soundAsset + "s2_p1_reproduction_6.ogg"},
            {id: "sound_repro_7", src: soundAsset + "s2_p1_reproduction_7.ogg"},
            {id: "sound_repro_8", src: soundAsset + "s2_p1_reproduction_8.ogg"},
            {id: "sound_repro_9", src: soundAsset + "s2_p1_reproduction_9.ogg"},



        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*=
    =            Handlers and helpers Block            =
    =*/
    /*===  register the handlebar partials first  ===*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

    function sound_player_seq(soundarray,navflag){
        createjs.Sound.stop();
        var current_sound = createjs.Sound.play(soundarray[0]);
        soundarray.splice( 0, 1);
        current_sound.on("complete", function(){
            if(soundarray.length > 0){
                sound_player_seq(soundarray, navflag);
            }else{
                if(navflag)
                    navigationcontroller(countNext,$total_page);
            }

        });
    }
    /*
     =            general template function            =
     */
    function generaltemplate() {
        countNext==0?sound_player("sound_1",false):"";
        loadpage(maincontent);
        $(".options").click(function () {
            countNext = 1;
            if($(this).hasClass("nutrition")){
                visitedcontent["nutrition"]=true;
                maincontent = nutritionContent;
                nutritiongeneraltemp();
            }
            else if($(this).hasClass("respiration")){
                visitedcontent["respiration"]=true;
                maincontent = respirationContent;
                respirationgeneraltemp();
            }
            else if($(this).hasClass("internaltransport")){
                visitedcontent["inttrans"]=true;
                maincontent = inttransContent;
                internaltransportgeneraltemp();
            }
            else if($(this).hasClass("excretion")){
                visitedcontent["excretion"]=true;
                maincontent = excretionContnent;
                excretiongeneraltemp();
            }
            else if($(this).hasClass("reproduction")){
                visitedcontent["reproduction"]=true;
                maincontent = reproductionContent;
                reproductiongeneraltemp();
            }
        });
    }
    function loadpage(contenttobeload){
        $total_page = contenttobeload.length;
        loadTimelineProgress($total_page, countNext + 1);
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(contenttobeload[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(contenttobeload, countNext, preload);
    }
    function nutritiongeneraltemp(){
        current_sound.stop();
        loadpage(maincontent);
        console.log("I am in nutrition tab"+countNext);
        switch(countNext){
            case 1:
                sound_player("sound_nutri_2",true);
                $prevBtn.css("display","none");
                break;
            case 2:
                setTimeout(function(){
                    $(".flower img").attr("src",preload.getResult("flowerImg1").src)
                },4000);
                sound_player_seq(["sound_nutri_3","sound_nutri_3_1"],true);
                appearoneafteranother(0);
                break;
            case 3:
                sound_player("sound_nutri_4",true);
                $(".squirrel").css({"width":"38%","left":"8%"});
                $(".dial1").css("left","33%");
                break;
            case 4:
                setTimeout(function(){
                    $(".flower1 img").attr("src",preload.getResult("flower3Img").src)
                },11000);
                sound_player("sound_nutri_5",true);
                $(".dial3").find("p").css("padding","6%");
                $(".dial3").css("top","13%");
                $(".textclr").eq(0).addClass("orangeclr");
                $(".textclr").eq(1).addClass("blueclr");
                $(".textclr").eq(2).addClass("drkorangeclr");
                $(".textclr").eq(3).addClass("greenclr");
                break;
            case 5:
                $(".dial4").hide().css("top","13%");;
                setTimeout(function(){
                    sound_player("sound_nutri_6",false);
                    current_sound.on('complete', function () {


                    $(".flower2").find("img").attr("src",preload.getResult("movingleafImg").src)
                   setTimeout(function(){
                       $(".flower2").find("img").attr("src",preload.getResult("flower3Img").src)
                       $(".flower2").addClass("leaveszoom");
                       setTimeout(function(){
                           $(".flower2").css({
                               'position': 'absolute',
                               'width': '63%',
                               'top': '-100%',
                               'left': '2%'
                           });
                           $(".flower2").addClass("leaveszoomout");
                           setTimeout(function(){
                               sound_player("sound_nutri_6_1",true);
                               $(".dial4").fadeIn(100);
                               $(".flower2").find("img").attr("src",preload.getResult("flower1Img").src)
                           },4000);
                       },2000)
                   },1000);
                });
                },2000);
                $(".appear").hide().delay(4500).fadeIn(2000);
                $(".appear").children().addClass("greenbgclr");
                break;
            case 6:
                setTimeout(function(){
                    sound_player("sound_nutri_7",true);
                },2000);
                $(".flower1").css({"left":"14%"});
                $(".watering").css({"right":"51%"});
                break;
            case 7:
                setTimeout(function(){
                    sound_player("sound_nutri_8",true);
                },2000);
                $(".flower1").css({"left":"14%"});
                $(".watering").css({"right":"51%"});
            break;
            case 8:
                $(".dial1").hide().animate({"left":"32%","top":"4%"},100);
                setTimeout(function(){
                    sound_player("sound_nutri_9",false);
                    current_sound.on('complete', function () {
                        $(".dial1").fadeIn(1000);
                        sound_player("sound_nutri_9_1",true);
                    });
                },2000);
                $(".flower1").css({"left":"14%"});
                $(".watering").css({"right":"51%"});
                navigationcontroller(countNext,$total_page);
                break;
            case 9:
                sound_player("sound_nutri_10",false);
                navigationcontroller(countNext,$total_page,false,checkforlastpage("nutrition"));
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;

        }

    }


    function respirationgeneraltemp(){
        loadpage(maincontent);
        console.log("I am in respiration tab"+countNext);
        switch(countNext){
            case 1:
                sound_player("sound_resp_2",true);
                $prevBtn.css("display","none");
                break;
            case 2:
                sound_player("sound_resp_3",false);
                $(".rdial").hide();
                var a = ["option opt1","option opt2"];
                shufflehint("option",a);
                checkans();
                break;
            case 3:
                sound_player("sound_resp_4",false);
                var a = ["option1 opt1","option1 opt2"];
                shufflehint("option1",a);
                checkans();
                break;
            case 4:
                setTimeout(function() {
                    sound_player("sound_resp_5", true);
                },2000);
                $(".show1,.show2").hide();
                $(".rdial1 p").css("padding","7%");
                $(".show1").delay(1000).fadeIn(1000);
                $(".show2").delay(2000).fadeIn(1000);
                break;
            case 5:
                sound_player("sound_resp_6",true);
                $(".respsquirrel").css("right","33%");
                $(".rdial1").css({"right":"36%","width":"44%"});
                break;
            case 6:
                setTimeout(function() {
                    sound_player("sound_resp_7", true);
                },2000);
                break;
            case 7:
                sound_player("sound_resp_8",true);
                current_sound.on('complete', function () {
                    $(".respflower1 img").attr("src",preload.getResult("flower3Img").src)
                });
                $(".rdial").css({"right":"18%","top":"2%"});
                break;
            case 8:
                setTimeout(function() {
                    sound_player("sound_resp_9", true);
                },2000);
                $(".show1,.show2,.show3,.show4").hide();
                $(".show1").delay(2200).fadeIn(100);
                $(".show2").delay(4200).fadeIn(100);
                $(".show3").delay(7000).fadeIn(100);
                $(".show4").delay(9000).fadeIn(100);
                break;
            case 9:
                setTimeout(function() {
                    sound_player("sound_resp_10", true);
                },2000);
                break;
            case 10:
                sound_player("sound_resp_11",false);
                $(".droppable").find("p").hide();
                count = 0;
                var a = ["draggable drag1","draggable drag2"];
                shufflehint("draggable",a);
                dragdrop();
                break;
        }
    }

    function internaltransportgeneraltemp(){
        loadpage(maincontent);
        console.log("I am in internal transport tab"+countNext);
        switch(countNext){
            case 1:
                sound_player("sound_int_2",true);
                $prevBtn.css("display","none");
                break;
            case 2:
                sound_player("sound_int_3",true);
                $(".respsquirrel").css("right","33%");
                $(".dial1").css({"right":"36%","width":"44%"});
                break;
            case 3:
                sound_player("sound_int_4",true);
                $(".respsquirrel").css("right","33%");
                break;
            case 4:
                setTimeout(function () {
                    sound_player("sound_int_5",false);
                    current_sound.on('complete', function () {
                        $(".transdial").fadeIn(1000);
                        sound_player("sound_int_5_1",false);
                    });
                },2000);
                $(".show2,.transdial,.show3").hide();
                $(".rootdiv").click(function(){
                    $(".show2,.show3").delay(2000).fadeIn(100);
                    $(".respflower1").addClass("zoomPlant");
                    $(".emptydiv").delay(1000).animate({"height":"45%"},1000);
                    $(".show1").hide();
                    navigationcontroller(countNext,$total_page);
                });
                break;
            case 5:
            case 6:
                setTimeout(function(){
                    sound_player("sound_int_"+(countNext+1),true);
                },2000);
                break;
            case 7:
                sound_player("sound_int_"+(countNext+1),false);
                navigationcontroller(countNext,$total_page,false,checkforlastpage("excretion"));
                break;
            default:
                navigationcontroller(countNext,$total_page);
        }
    }

    function excretiongeneraltemp(){
        loadpage(maincontent);
        console.log("I am in excretion tab"+countNext);
        switch(countNext){
            case 1:
                sound_player("sound_ex_2",true);
                $prevBtn.css("display","none");
                break;
            case 2:
                sound_player("sound_ex_3",true);
                $(".respsquirrel").css("right","33%");
                $(".exdial").find("p").css("padding","7%");
                break;
            case 3:
                sound_player("sound_ex_4",true);
                $(".respsquirrel").css("right","33%");
                $(".exdial1").find("p").css("padding","7%");
                break;
            case 4:
                setTimeout(function () {
                    sound_player("sound_ex_5",true);
                },2000);
                $(".show1").hide().delay(4000).fadeIn(100);
                break;
            case 5:
                sound_player("sound_ex_6",false);
                $(".exdial3").hide();
                $(".oxygencoming").hide();
                $(".clickleafdiv").click(function(){
                    sound_player("sound_ex_6_1",true);
                    $(".exwater2").addClass("animationWater2");
                    $(".exoxygen2").addClass("animationOxygen2");
                    $(".exdial2").fadeOut(500);
                    $(".exdial3").fadeIn(1000);
                    $(".oxygencoming").show();
                    $(".pore,.arrblck").delay(3000).animate({"opacity":"1"},500);
                });
                break;
            case 6:
                setTimeout(function () {
                    sound_player("sound_ex_7", true);
                },2000);
                $(".show1").hide().delay(4000).fadeIn(100);
                break;
            case 7:
                setTimeout(function () {
                    sound_player("sound_ex_8", false);
                },2000);
                $(".s,.arr").hide();
                var delayTime = 4000;
                for(var i=1;i<7;i++){
                    $(".show"+i).delay(delayTime).fadeIn(1000);
                    delayTime = delayTime+1000;
                    if(i==6){
                        navigationcontroller(countNext,$total_page,false,checkforlastpage("excretion"));
                    }
                }
                break;
            default:
                navigationcontroller(countNext,$total_page);
        }
    }

    function reproductiongeneraltemp(){
        loadpage(maincontent);
        console.log("I am in reproduction tab");
        countNext!=6?sound_player("sound_repro_"+(countNext+1),true):sound_player("sound_repro_"+(countNext+1),false);
        switch(countNext){
            case 1:
                $prevBtn.css("display","none");
                break;
            case 2:
                $(".exdial1").css("width","49%");
                $(".respsquirrel").css("right","33%");
            case 3:
                $(".exdial").css("width","45%");
                $(".respsquirrel").css("right","33%");
                break;
            case 6:
                $(".life,.larrow").hide();
                var delayTime=1000;
                for(var i = 1;i<8;i++){
                    $(".lcyc"+i).delay(delayTime).fadeIn(100);
                    $(".gar"+i).delay(delayTime+1000).fadeIn(100);
                    delayTime = delayTime+1500;
                }
                setTimeout(function(){
                    navigationcontroller(countNext,$total_page);

                },11000);
                break;
            case 7:
                $(".exdial1").css("width","49%");
                $(".exdial1").find("p").css("padding","9%");
                $(".respsquirrel").css("right","33%");
                break;
            case 8:
                $(".exdial").css("width","45%");
                $(".respsquirrel").css("right","33%");
                $(".fern").hide().delay(1000).fadeIn(1000);
                $(".mosses").hide().delay(2000).fadeIn(1000);
                navigationcontroller(countNext,$total_page,false,checkforlastpage("reproduction"));
                break;
        }
    }

    function  checkforlastpage(updatekey){
        var test = 0;
        visitedcontent[updatekey] = true;
        if( visitedcontent["nutrition"] == true &&
            visitedcontent["respiration"] == true &&
            visitedcontent["inttrans"] == true &&
            visitedcontent["excretion"] == true &&
            visitedcontent["reproduction"] == true
        )
            return false;
        else
            return true;
    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on("click", function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        if (countNext == $total_page - 1) {
            countNext = 0;
            templateCaller();
        }
        else {
            countNext++;
            switchtemplate();
        }
    });

    function switchtemplate(){
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        $(".contentblock").hasClass("nutr")?nutritiongeneraltemp():
            $(".contentblock").hasClass("res")?respirationgeneraltemp():
                $(".contentblock").hasClass("inttrans")?internaltransportgeneraltemp():
                    $(".contentblock").hasClass("excre")?excretiongeneraltemp():
                        $(".contentblock").hasClass("repro")?reproductiongeneraltemp():generaltemplate();
    }

    $refreshBtn.click(function(){
        countNext==0?templateCaller():switchtemplate();
    });


    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        countNext==0?templateCaller():switchtemplate();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function appearoneafteranother(delaytime){
        $(".appear").hide();
        clearInterval(setTime);
        setTime = setInterval(function(index){
            $(".appear").each(function(){
                $(this).delay(delaytime).fadeIn(1000);
                delaytime = delaytime +1500;
            });
        });
    }



    function shufflehint(optionName,a){
        console.log("I am in shufflehint")
        var optiondiv = $(".contentblock");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
        }
        optiondiv.find("."+optionName).removeClass().addClass("current");
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find("."+optionName).removeClass("current");
    }

    function checkans(){
        $(".option,.option1 ").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").trim()=="correct") {
                $(this).addClass("correctans");
                $(".option,.option1").addClass("avoid-clicks");
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                $(".rdial").fadeIn(100);
                $(".respflower").find("img").attr("src",preload.getResult("flower1Img").src)
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }
    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            },
            stop:function(event,ui){
                $(".draggable").css("top","");

            }
        });
        $('.droppable').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if(ui.draggable.attr("data-answer").toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    current_sound.stop();
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("p").show();
                    $(this).addClass("correctans");
                    texthighlight($board);
                    count++;
                    if(count>1){
                        navigationcontroller(countNext,$total_page,false,checkforlastpage("respiration"));
                    }
                }
                else {
                    current_sound.stop();
                    play_correct_incorrect_sound(0);
                }
            }
        });
    }
});

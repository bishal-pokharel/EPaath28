var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"popuptext",
                textclass: "content centertext",
                textdata: data.string.nutrition
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.nutrition
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text1
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text1_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text1_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text1_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.nutrition
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text2
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text2_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text2_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text2_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"popuptext",
                textclass: "content centertext",
                textdata: data.string.respiration
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text3
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text3_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text3_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text3_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.respiration
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text4
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"draggable opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text4_2,
                ans:data.string.p7text4_2
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"draggable opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text4_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv:"droppable drop1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text4_2,
                ans:data.string.p7text4_2
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img1",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont1 ",
                            imgtxt:data.string.p7text4_1
                        }
                    ]
                },
                {
                    imgdiv: "arrow1",
                    imgclass: "relativecls img3",
                    imgid: 'arrow1Img',
                    imgsrc: ""
                }
            ]
        }]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"popuptext",
                textclass: "content centertext",
                textdata: data.string.inttrans
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text5
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text5_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text5_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text5_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.inttrans
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text6
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text6_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text6_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text6_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv"
            },
            {
                textdiv: "emptydiv1"
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"popuptext",
                textclass: "content centertext",
                textdata: data.string.excretion
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 10
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text7
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text7_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text7_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text7_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 11
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.excretion
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text8
            },
            {
                textdiv:"option opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text8_2,
                ans:"correct"
            },
            {
                textdiv:"option opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text8_3
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"blank",
                textdiv:"fillques",
                textclass: "optionfont centertext",
                textdata: data.string.p7text8_1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
            ]
        }]
    },
//    slide 12
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv: "emptydiv1"
            },
            {
                textdiv:"clicktext blinkEffect",
                textclass: "content1 centertext",
                textdata: data.string.click
            },
            {
                textdiv:"popuptext",
                textclass: "content centertext",
                textdata: data.string.reproduction
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "piechart",
                    imgclass: "relativecls img1",
                    imgid: 'piechart1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow",
                    imgclass: "relativecls img2",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img3",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 13
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.reproduction
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text9
            },
            {
                textdiv:"option1 opt11 mainopt",
                textclass: "optionfont1 centertext",
                textdata: data.string.p7text9_2,
                ans:"correct"
            },
            {
                textdiv:"option1 opt22 mainopt1",
                textclass: "optionfont1 centertext",
                textdata: data.string.p7text9_3
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img1",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"optionfont1 ",
                            imgtxt:data.string.clickques
                        }
                    ]
                },
                {
                    imgdiv: "optionimg o1 mainopt",
                    imgclass: "relativecls img3",
                    imgid: 'option1Img',
                    imgsrc: "",
                    ans:"correct"
                },
                {
                    imgdiv: "optionimg o2 mainopt1",
                    imgclass: "relativecls img4",
                    imgid: 'option2Img',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide 14
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv:"emptydiv",
            },
            {
                textdiv:"emptydiv1",
            },
            {
                textdiv:"maintitle",
                textclass: "content1 centertext",
                textdata: data.string.reproduction
            },
            {
                textdiv:"querytitle",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10
            },
            {
                textdiv:"droppable1 drop11",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_2,
                ans:data.string.p7text10_2
            },
            {
                textdiv:"droppable1 drop22",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_3,
                ans:data.string.p7text10_3
            },
            {
                textdiv:"draggable opt1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_2,
                ans:data.string.p7text10_2
            },
            {
                textdiv:"draggable opt2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_3,
                ans:data.string.p7text10_3
            },
            {
                textdiv:"dropquery1",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_4
            },
            {
                textdiv:"dropquery2",
                textclass: "optionfont centertext",
                textdata: data.string.p7text10_5
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "flower",
                    imgclass: "relativecls img1",
                    imgid: 'flowerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dial fadeInEffect",
                    imgclass: "relativecls img2",
                    imgid: 'dialImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"content3 ",
                            imgtxt:data.string.p7text10_1
                        }
                    ]
                }
            ]
        }]
    }
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn = $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;
    var setTime;
    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);
    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "piechart1Img", src: imgpath + "pie_chart01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src: imgpath + "orange_down_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "flowerImg", src: imgpath + "talking_sun02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dialImg", src: imgpath + "bubble02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "arrow1Img", src: imgpath + "blue_arrow03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "option1Img", src: imgpath + "new_plant_grow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "option2Img", src: imgpath + "no_plant_grow.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_nutrition", src: soundAsset + "s4_p1_1.ogg"},
            {id: "sound_respiration", src: soundAsset + "s4_p4_1.ogg"},
            {id: "sound_internaltransport", src: soundAsset + "s4_p7_1.ogg"},
            {id: "sound_excretion", src: soundAsset + "s4_p10_1.ogg"},
            {id: "sound_reproduction", src: soundAsset + "s4_p13_1.ogg"},
            {id: "sound_click", src: soundAsset + "s4_p2.ogg"},
            {id: "sound_drag", src: soundAsset + "s4_p6.ogg"},
            {id: "sound_drag1", src: soundAsset + "s4_p15.ogg"},
            {id: "spinning_wheel", src: soundAsset + "spinning_wheel.ogg"},
            // {id: "sound_1", src: soundAsset + "p1_s1.ogg"},
            // {id: "sound_2", src: soundAsset + "p1_s2.ogg"},
            // {id: "sound_3", src: soundAsset + "p1_s3.ogg"},
            // {id: "sound_4", src: soundAsset + "p1_s4.ogg"},
            // {id: "sound_5", src: soundAsset + "p1_s5.ogg"},
            // {id: "sound_6", src: soundAsset + "p1_s6.ogg"},
            // {id: "sound_7", src: soundAsset + "p1_s7.ogg"},
            // {id: "sound_8", src: soundAsset + "p1_s8.ogg"},
            // {id: "sound_9", src: soundAsset + "p1_s9.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }
    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        countNext!=1?vocabcontroller.findwords(countNext):"";
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                rotatethewheel("animationrotaten","sound_nutrition", "spinning_wheel");
                break;
            case 3:
                rotatethewheel("animationrotater","sound_respiration", "spinning_wheel");
                break;
            case 6:
                rotatethewheel("animationrotateit","sound_internaltransport", "spinning_wheel");
                break;
            case 9:
                rotatethewheel("animationrotateex","sound_excretion", "spinning_wheel");
                break;
            case 12:
                rotatethewheel("animationrotaterp","sound_reproduction", "spinning_wheel");
                break;
            case 1:
            case 2:
            case 4:
            case 7:
            case 8:
            case 10:
            case 11:
                $(".querytitle,.fillques,.option").hide();
                sound_player("sound_click",false,true);
                shufflehint("option",["option opt1","option opt2"]);
                checkans();
                break;
            case 13:
                $(".querytitle,.optionimg,.option1").hide();
                sound_player("sound_click",false,true);
                $(".mainopt").hover(function(){
                    $(".mainopt").addClass("hoveringcss")
                },function(){
                    $(".mainopt").removeClass("hoveringcss");
                });
                $(".mainopt1").hover(function(){
                    $(".mainopt1").addClass("hoveringcss")
                },function(){
                    $(".mainopt1").removeClass("hoveringcss");
                });
                checkans1();
                break;
            case 14:
                $(".droppable1,.dropquery1,.dropquery2,.querytitle,.draggable").hide();
                sound_player("sound_drag1",false,true);
                $(".droppable1 p").hide();
                count = 0;
                shufflehint("draggable",["draggable opt1","draggable opt2"]);
                $(".draggable").css("bottom","51%");
                dragdrop();
                break;
            case 5:
                $(".querytitle,.draggable").hide();
                sound_player("sound_drag",false,true);
                count = 0;
                $(".droppable p").hide();
                shufflehint("draggable",["draggable opt1","draggable opt2"]);
                dragdrop();
                break;
            default:
                break;
        }
    }

    function rotatethewheel(animationclass,soundid, wheel_sound){
        $(".rotatingtext1").hide();
        $(".piechart,.clicktext").click(function(){
            createjs.Sound.stop();
            current_sound = createjs.Sound.play(wheel_sound);
            current_sound.play();
            current_sound.on('complete', function () {
                  sound_player(soundid,true);
                  $(".popuptext").animate({"opacity":"1","z-index":"3"},50);
            });
            $(".clicktext").removeClass("blinkEffect").addClass("avoid-clicks");
            $(".rotatingtext").hide();
            $(".piechart").addClass(animationclass);
            $(".rotatingtext1").delay(500).fadeIn(100);
        });
        // $(".rotatingtext1").hide();
        // $(".piechart,.clicktext").click(function(){
        //     $(".clicktext").removeClass("blinkEffect").addClass("avoid-clicks");
        //     $(".rotatingtext").hide();
        //     $(".piechart").addClass(animationclass);
        //     $(".rotatingtext1").delay(500).fadeIn(100);
        //     setTimeout(function(){
        //         sound_player(soundid,true);
        //         $(".popuptext").animate({"opacity":"1","z-index":"3"},50);
        //     },1000);
        // });
    }

    function sound_player(sound_id, navigate,showtext) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
            showtext?$(".droppable1,.dropquery1,.dropquery2,.querytitle,.fillques,.option,.draggable,.option1,.optionimg").fadeIn(500):"";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function () {
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });


    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";
        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                 use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    ( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";
                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
                $(this).html(replaceinstring);
            });
        }
    }
    function shufflehint(optionName,a){
        var optiondiv = $(".contentblock");
        for (var i = 2; i >= 0; i--) {
            optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
        }
        optiondiv.find("."+optionName).removeClass().addClass("current");
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find("."+optionName).removeClass("current");
    }

    function checkans(){
        $(".option,.optionImg").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").trim()=="correct") {
                $(this).addClass("correctans");
                $(".option,.optionImg").addClass("avoid-clicks");
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
               countNext!=10? $(".blank").html($(this).find("p").text().toString().toLowerCase()).css("color","#0838ee"):$(".blank").html($(this).find("p").text()).css("color","#0838ee");
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }
    function checkans1(){
        $(".mainopt,.mainopt1").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").trim()=="correct") {
                if($(this).hasClass("mainopt")) {
                    $(".mainopt").addClass("correctans");
                    $(".mainopt").eq(1).append("<img class='correctwrongimg' src='images/right.png'/>")

                }
                else{
                    $(".mainopt1").addClass("correctans");
                    $(".mainopt1").eq(1).append("<img class='correctwrongimg' src='images/right.png'/>")

                }
                $(".mainopt,.mainopt1").addClass("avoid-clicks");
                play_correct_incorrect_sound(1);
                $(".blank").html($(this).find("p").text().toString().toLowerCase()).css("color","#0838ee");
                navigationcontroller(countNext,$total_page);
            }
            else{
                if($(this).hasClass("mainopt")) {
                    $(".mainopt").addClass("wrongans avoid-clicks");
                    $(".mainopt").eq(1).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                }
                else {
                    $(".mainopt1").addClass("wrongans avoid-clicks");
                    $(".mainopt1").eq(1).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                }
                play_correct_incorrect_sound(0);
            }
        });
    }

    function dragdrop(){
        $(".draggable").draggable({
            containment: "body",
            revert: true,
            appendTo: "body",
            zindex: 10,
            start:function(event, ui){
                $(".wrongImg").remove();
                $(".draggable,.droppable").removeClass("wrongcss");
            }
        });
        $('.droppable,.droppable1').droppable({
            accept : ".draggable",
            hoverClass: "hovered",
            drop: function(event, ui) {
                if(ui.draggable.attr("data-answer").toString().trim() == ($(this).attr("data-answer").toString().trim())) {
                    play_correct_incorrect_sound(1);
                    ui.draggable.hide(0);
                    $(this).find("p").show();
                    $(this).addClass("correctans");
                    texthighlight($board);
                    count++;
                    if(countNext==14 && count>1){
                        navigationcontroller(countNext,$total_page,true,false);
                    }
                    else if(countNext<14){
                        navigationcontroller(countNext,$total_page);
                    }
                }
                else {
                    play_correct_incorrect_sound(0);
                    $(ui.draggable).addClass("wrongans");

                }
            }
        });
    }
});

var imgpath = $ref + "/images/images_for_exercise/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var content = [
    //slide 0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "emptydiv",
            },
            {
                textdiv: "question",
                textclass: "box centertext",
                textdata: data.string.query1,
                quesno:true
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv: "option opt1",
                textclass: "box centertext",
                textdata: data.string.q1_1,
                ans:"correct"
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv: "option opt2",
                textclass: "box centertext",
                textdata: data.string.q1_2
            },
            {
                textdiv: "option opt3",
                textclass: "box centertext",
                textdata: data.string.q1_3
            },
            {
                textdiv: "option opt4",
                textclass: "box centertext",
                textdata: data.string.q1_4
            },
            {
                textdiv: "sunlight",
                textclass: "box centertext",
                textdata: data.string.sunlight
            },
            {
                textdiv: "water",
                textclass: "box centertext",
                textdata: data.string.water
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass:"subscript",
                textdiv: "co",
                textclass: "box centertext",
                textdata: data.string.cod
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftimage",
                    imgclass: "relativecls img1",
                    imgid: 'q01Img',
                    imgsrc: ""
                },
            ]
        }]
    },
    //slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "emptydiv",
            },
            {
                textdiv: "question",
                textclass: "box centertext",
                textdata: data.string.query2,
                quesno:true
            },
            {
                textdiv: "option opt1",
                textclass: "box centertext",
                textdata: data.string.q2_1,
                ans:"correct"
            },
            {
                textdiv: "option opt2",
                textclass: "box centertext",
                textdata: data.string.q2_2
            },
            {
                textdiv: "option opt3",
                textclass: "box centertext",
                textdata: data.string.q2_3
            },
            {
                textdiv: "option opt4",
                textclass: "box centertext",
                textdata: data.string.q2_4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftimage",
                    imgclass: "relativecls img1",
                    imgid: 'q02Img',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "emptydiv",
            },
            {
                textdiv: "question",
                textclass: "box centertext",
                textdata: data.string.query3,
                quesno:true
            },
            {
                textdiv: "option opt1",
                textclass: "box centertext",
                textdata: data.string.q3_1,
                ans:"correct"
            },
            {
                textdiv: "option opt2",
                textclass: "box centertext",
                textdata: data.string.q3_2
            },
            {
                textdiv: "option opt3",
                textclass: "box centertext",
                textdata: data.string.q3_3
            },
            {
                textdiv: "option opt4",
                textclass: "box centertext",
                textdata: data.string.q3_4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftimage",
                    imgclass: "relativecls img1",
                    imgid: 'q03Img',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "question1",
                textclass: "box centertext",
                textdata: data.string.query4,
                ans:data.string.root,
                quesno:true
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgoption imgo1",
                    imgclass: "relativecls img1",
                    imgid: 'q04Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.root
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo2",
                    imgclass: "relativecls img2",
                    imgid: 'q05Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.leaf
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo3",
                    imgclass: "relativecls img3",
                    imgid: 'q06Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.flower
                        }
                    ]
                },
            ]
        }]
    },
//    slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "question1",
                textclass: "box centertext",
                textdata: data.string.query5,
                ans:data.string.stem,
                quesno:true
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgoption imgo1",
                    imgclass: "relativecls img1",
                    imgid: 'q04Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.root
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo2",
                    imgclass: "relativecls img2",
                    imgid: 'truckImg',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.stem
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo3",
                    imgclass: "relativecls img3",
                    imgid: 'q06Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.flower
                        }
                    ]
                },
            ]
        }]
    },
//   slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "question1",
                textclass: "box centertext",
                textdata: data.string.query6,
                ans:data.string.leaf,
                quesno:true
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgoption imgo1",
                    imgclass: "relativecls img1",
                    imgid: 'q04Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.root
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo2",
                    imgclass: "relativecls img2",
                    imgid: 'q05Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.leaf
                        }
                    ]
                },
                {
                    imgdiv: "imgoption imgo3",
                    imgclass: "relativecls img3",
                    imgid: 'q06Img',
                    imgsrc: "",
                    textblock:[
                        {
                            imgcls:"box ",
                            imgtxt:data.string.flower
                        }
                    ]
                },
            ]
        }]
    },
//    slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "emptydiv",
            },
            {
                textdiv: "question",
                textclass: "box centertext",
                textdata: data.string.query7,
                quesno:true
            },
            {
                textdiv: "option opt1",
                textclass: "box centertext",
                textdata: data.string.q7_1,
                ans:"correct"
            },
            {
                textdiv: "option opt2",
                textclass: "box centertext",
                textdata: data.string.q7_2
            },
            {
                textdiv: "option opt3",
                textclass: "box centertext",
                textdata: data.string.q7_3
            },
            {
                textdiv: "option opt4",
                textclass: "box centertext",
                textdata: data.string.q7_4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftimage",
                    imgclass: "relativecls img1",
                    imgid: 'q08Img',
                    imgsrc: ""
                },
            ]
        }]
    },
//    slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        textblock: [
            {
                textdiv: "questiontitle",
                textclass: "content2 centertext",
                textdata: data.string.extitle,
                quesno:false
            },
            {
                textdiv: "emptydiv",
            },
            {
                textdiv: "question",
                textclass: "box centertext",
                textdata: data.string.query8,
                quesno:true
            },
            {
                textdiv: "option opt1",
                textclass: "box centertext",
                textdata: data.string.q8_1,
                ans:"correct"
            },
            {
                textdiv: "option opt2",
                textclass: "box centertext",
                textdata: data.string.q8_2
            },
            {
                textdiv: "option opt3",
                textclass: "box centertext",
                textdata: data.string.q8_3
            },
            {
                textdiv: "option opt4",
                textclass: "box centertext",
                textdata: data.string.q8_4
            },
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "leftimage",
                    imgclass: "relativecls img1",
                    imgid: 'q09Img',
                    imgsrc: ""
                },
            ]
        }]
    },
];
// content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn = $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var score = new LampTemplate();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "q01Img", src: imgpath + "q01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q02Img", src: imgpath + "q02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q03Img", src: imgpath + "q03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q04Img", src: imgpath + "q04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q05Img", src: imgpath + "q05.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q06Img", src: imgpath + "q06.png", type: createjs.AbstractLoader.IMAGE},
            {id: "truckImg", src: imgpath + "trunk.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q08Img", src: imgpath + "q08.png", type: createjs.AbstractLoader.IMAGE},
            {id: "q09Img", src: imgpath + "q09.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "ex.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        score.init(content.length);
        templateCaller();
    }

    //initialize
    init();


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        put_image(content, countNext, preload);
        score.numberOfQuestions();
        countNext==0?sound_player("sound_1"):"";
        switch (countNext) {
            case 3:
            case 4:
            case 5:
                var a = ["imgoption imgo1","imgoption imgo2","imgoption imgo3"]
                shufflehint("imgoption",a);
                checkans1();
                break;
            default:
                $(".co p").hide();
                var a = ["option opt1","option opt2","option opt3","option opt4"]
                shufflehint("option",a);
                checkans();
                break;
        }
    }

    function put_image(content, count, preload) {
        var contentCount = content[count];
        var imageblockcontent = contentCount.hasOwnProperty('imageblock');
        dynamicimageload(imageblockcontent, contentCount, preload)
    }

    function dynamicimageload(imageblockcontent, contentCount, preload) {
        if (imageblockcontent) {
            var imageblock = contentCount.imageblock[0];
            if (imageblock.hasOwnProperty('imagestoshow')) {
                var imageClass = imageblock.imagestoshow;
                for (var i = 0; i < imageClass.length; i++) {
                    var image_src = preload.getResult(imageClass[i].imgid).src;
                    //get list of classes
                    var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
                    var selector = ('.' + classes_list[classes_list.length - 1]);
                    $(selector).attr('src', image_src);
                }
            }
        }
    }

    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }

    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside) {
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag = "</span>";


        if ($alltextpara.length > 0) {
            $.each($alltextpara, function (index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring");

                texthighlightstarttag = "<span class='" + stylerulename + "'>";


                replaceinstring = $(this).html();
                replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
                replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function shufflehint(optionName,a){
        console.log("I am in shufflehint")
        var optiondiv = $(".contentblock");
        for (var i = $("."+optionName).length; i >= 0; i--) {
            optiondiv.append(optiondiv.find("."+optionName).eq(Math.random() * i | 0));
        }
        optiondiv.find("."+optionName).removeClass().addClass("current");
        optiondiv.find(".current").each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });
        optiondiv.find("."+optionName).removeClass("current");
    }

    function checkans(){
        var scoreupdate = true;
        $(".option").on("click",function () {
            createjs.Sound.stop();
            if($(this).attr("data-answer").trim()=="correct") {
                $(this).addClass("correctans");
                $(".option").addClass("avoid-clicks");
                $(this).append("<img class='correctwrongimg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                $(".co p").show();
                scoreupdate==true? score.update(true):"";
                navigationcontroller(countNext,$total_page);
            }
            else{
                scoreupdate = false;
                score.update(false);
                $(this).addClass("wrongans avoid-clicks");
                $(this).append("<img class='correctwrongimg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }
    function checkans1(){
        var scoreupdate = true;
        $(".imgoption").on("click",function () {
            createjs.Sound.stop();
            if($(this).find("p").text().trim()==$(".question1").attr("data-answer")) {
                $(this).find("p").addClass("correctans");
                $(".imgoption").addClass("avoid-clicks");
                $(this).find("p").append("<img class='correctwrongimg1' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                scoreupdate==true? score.update(true):"";
                navigationcontroller(countNext,$total_page);
            }
            else{
                scoreupdate = false;
                score.update(false);
                $(this).find("p").addClass("wrongans");
                $(this).addClass("avoid-clicks");
                $(this).find("p").append("<img class='correctwrongimg1' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

});

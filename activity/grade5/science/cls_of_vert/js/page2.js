var imgpath = $ref + "/images/Page01/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"coverpagetext",
    uppertextblock: [
      {
        textclass: "chapter centertext",
        textdata: data.string.pisces1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "seabg",
          imgclass: "relativecls seabgimg",
          imgid: 'seabgImg',
          imgsrc: ""
        },
        {
          imgdiv: "fish imgdiv imgclick zoomInEffect",
          imgclass: "relativecls fishimg",
          imgid: 'fishImg',
          imgsrc: ""
        },
        {
          imgdiv: "seahorse imgdiv imgclick zoomInEffect",
          imgclass: "relativecls seahorseimg",
          imgid: 'seahorseImg',
          imgsrc: ""
        },
        {
          imgdiv: "shark imgdiv  imgclick zoomInEffect",
          imgclass: "relativecls sharkimg",
          imgid: 'sharkImg',
          imgsrc: ""
        },

      ]
    }]
  },
  //slide 1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"topic",
    uppertextblock: [
      {
        textclass: "content centertext",
        textdata: data.string.p2text1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "seabg",
          imgclass: "relativecls seabgimg",
          imgid: 'seabgImg',
          imgsrc: ""
        },
        {
          imgdiv: "fish imgdiv imgclick zoomInEffect",
          imgclass: "relativecls fishimg",
          imgid: 'fishImg',
          imgsrc: ""
        },
        {
          imgdiv: "seahorse imgdiv imgclick zoomInEffect",
          imgclass: "relativecls seahorseimg",
          imgid: 'seahorseImg',
          imgsrc: ""
        },
        {
          imgdiv: "shark imgdiv  imgclick zoomInEffect",
          imgclass: "relativecls sharkimg",
          imgid: 'sharkImg',
          imgsrc: ""
        },
        {
          imgdiv: "fishbox",
          imgclass: "relativecls fishboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.fish
        },
        {
          imgdiv: "seahorsebox",
          imgclass: "relativecls fishboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.seahorse
        },
        {
          imgdiv: "sharkbox",
          imgclass: "relativecls fishboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.shark
        },
        {
          imgdiv: "hand hand1 imgclick zoomInEffect",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        }

      ]
    }]
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"topic",
    uppertextblock: [
      {
        textclass: "content centertext",
        textdata: data.string.p2text2
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "seabg",
          imgclass: "relativecls seabgimg",
          imgid: 'seabgImg',
          imgsrc: ""
        },
        {
          imgdiv: "fishdiv",
          imgclass: "relativecls fishimg",
          imgid: 'fishImg',
          imgsrc: ""
        },
        {
          imgdiv: "fishscale",
          imgclass: "relativecls fishscaleimg",
          imgid: 'fishscaleImg',
          imgsrc: ""
        },

      ]
    }],
    textblock:[
      {
        textdiv:"scalediv",
        textclass:"content centertext",
        textdata:data.string.scale
      }
    ]
  },
  // slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"topic",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p2text3
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "seabg",
          imgclass: "relativecls seabgimg",
          imgid: 'seabgImg',
          imgsrc: ""
        },
        {
          imgdiv: "fish fish2",
          imgclass: "relativecls fishimg",
          imgid: 'fishImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow1",
          imgclass: "relativecls arrow1img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow2",
          imgclass: "relativecls arrow2img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow3",
          imgclass: "relativecls arrow3img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "finsbox zoomInEffect",
          imgclass: "relativecls finsboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.fins
        },
        {
          imgdiv: "gillsbox zoomInEffect",
          imgclass: "relativecls gillsboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.gills
        },
      ]
    }]
  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"topic1",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p2text4
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv:"svgdiv",
          imgclass:"svgimg",
          imgid: 'fisRepoImg',
          imgsrc: "",
        }
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"topic2",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p2text5
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "seabg",
          imgclass: "relativecls img1",
          imgid: 'seabgImg',
          imgsrc: ""
        },
        {
          imgdiv: "sharkbg",
          imgclass: "relativecls img2",
          imgid: 'shrakbgImg',
          imgsrc: ""
        }

      ]
    }]
  },
];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "seabgImg", src: imgpath+"sea_bg.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fishbgImg", src: imgpath+"fishbg.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fishImg", src: imgpath+"fish_01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "seahorseImg", src: imgpath+"seahorse.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sharkImg", src: imgpath+"shark.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boxImg", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fishscaleImg", src: imgpath+"fish-scales.png", type: createjs.AbstractLoader.IMAGE},
      {id: "arrowImg", src: imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "fisRepoImg", src: imgpath+"fishreproduction_new.svg", type: createjs.AbstractLoader.IMAGE},
      {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "shrakbgImg", src:imgpath+"fish_eating_fish01.png", type:createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_1", src: soundAsset+"s2_p1.ogg"},
      {id: "sound_2", src: soundAsset+"s2_p2.ogg"},
      {id: "sound_3", src: soundAsset+"s2_p2_1_shark.ogg"},
      {id: "sound_4", src: soundAsset+"s2_p2_2_rohufish.ogg"},
      {id: "sound_5", src: soundAsset+"s2_p2_3_seahorse.ogg"},
      {id: "sound_6", src: soundAsset+"s2_p3.ogg"},
      {id: "sound_7", src: soundAsset+"s2_p3_scale.ogg"},
      {id: "sound_8", src: soundAsset+"s2_p4.ogg"},
      {id: "sound_9", src: soundAsset+"s2_p4_fins gills.ogg"},
      {id: "sound_10", src: soundAsset+"s2_p5.ogg"},
      {id: "sound_11", src: soundAsset+"s2_p6.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);

    switch(countNext){
      case 0:
      sound_player('sound_1');
      break;
      case 1:
      sound_player('sound_2');
      showName();
      break;
      case 2:
      sound_player('sound_6');
      $(".scalediv").delay(2000).animate({"opacity":"1"},1000);
      break;
      case 3:
      sound_player('sound_8');
      animateNames();
      break;
      case 4:
      sound_player_nonav('sound_10');
      $prevBtn.delay(18000).show(0);
      $nextBtn.delay(18000).show(0);
      break;
      case 5:
      sound_player('sound_11');
      break;
      default:
      navigationcontroller(countNext,$total_page);
      break;
    }
  }



  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigationcontroller(countNext,$total_page);
    });
  }

  function sound_player_nonav(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }

  function showName(){
    $(".fishbox,.sharkbox,.seahorsebox").hide();
    $(".imgclick").click(function(){
      if($(this).hasClass("fish")||$(this).hasClass("hand2")){
        $(".fishbox").show();
        $(this).addClass("avoid-clicks");
        $(".hand").removeClass("hand2").addClass("hand3");
        sound_player_nonav('sound_4');
      }
      if($(this).hasClass("shark")||$(this).hasClass("hand1")){
        $(".sharkbox").show();
        $(this).addClass("avoid-clicks");
        sound_player_nonav('sound_3');
        $(".hand").removeClass("hand1").addClass("hand2");
      }
      if($(this).hasClass("seahorse")||$(this).hasClass("hand3")){
        $(".seahorsebox").show();
        navigationcontroller(countNext,$total_page);
        sound_player_nonav('sound_5');
        $(this).addClass("avoid-clicks");
        $(".hand").remove();
      }
    });
  }

  function animateNames(){
    $(".arrow1,.arrow2,.arrow3,.finsbox,.gillsbox").hide();
    setTimeout(function(){
      $(".finsbox").show();
      setTimeout(function(){
        $(".arrow1,.arrow2,.gillsbox").show();
        setTimeout(function(){
          $(".arrow3").show();
          // navigationcontroller(countNext,$total_page);
        },1000);
      },1000);
    },2000);
  }
});

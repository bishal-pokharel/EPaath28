var imgpath = $ref + "/images/Page02/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"coverpagetext",
    uppertextblock: [
      {
        textclass: "chapter centertext",
        textdata: data.string.amphibian1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },

      ]
    }]
  },
  //slide 1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p3text1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "salamander imgdiv imgclick zoomInEffect",
          imgclass: "relativecls salamanderimg",
          imgid: 'salamanderImg',
          imgsrc: ""
        },
        {
          imgdiv: "frog imgdiv imgclick zoomInEffect",
          imgclass: "relativecls frogimg",
          imgid: 'frogImg',
          imgsrc: ""
        },
        {
          imgdiv: "newt imgdiv  imgclick zoomInEffect",
          imgclass: "relativecls newtimg",
          imgid: 'newtImg',
          imgsrc: ""
        },
        {
          imgdiv: "salamanderbox",
          imgclass: "relativecls salamanderboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.salamander
        },
        {
          imgdiv: "frogbox",
          imgclass: "relativecls frogboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.frog
        },
        {
          imgdiv: "newtbox",
          imgclass: "relativecls newtboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.newt
        },
        {
          imgdiv: "hand hand1 imgclick zoomInEffect",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        }

      ]
    }]
  },
  // //slide 2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic ",
    uppertextblock: [
      {

        textclass: "subtopic centertext",
        textdata: data.string.p3text1_1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "salamander imgdiv imgclick zoomInEffect",
          imgclass: "relativecls salamanderimg",
          imgid: 'salamanderImg',
          imgsrc: ""
        },
        {
          imgdiv: "frog imgdiv imgclick zoomInEffect",
          imgclass: "relativecls frogimg",
          imgid: 'frogImg',
          imgsrc: ""
        },
        {
          imgdiv: "newt imgdiv  imgclick zoomInEffect",
          imgclass: "relativecls newtimg",
          imgid: 'newtImg',
          imgsrc: ""
        },
        {
          imgdiv: "salamanderbox",
          imgclass: "relativecls salamanderboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.salamander
        },
        {
          imgdiv: "frogbox",
          imgclass: "relativecls frogboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.frog
        },
        {
          imgdiv: "newtbox",
          imgclass: "relativecls newtboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.newt
        }

      ]
    }]
  },
  //slide3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic heightcls",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p3text2
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg1",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "frogdiv zoomInEffect",
          imgclass: "relativecls frogimg",
          imgid: 'frogImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow1",
          imgclass: "relativecls arrow1img",
          imgid: 'arrow1Img',
          imgsrc: ""
        },
        {
          imgdiv: "arrow2",
          imgclass: "relativecls arrow2img",
          imgid: 'arrow2Img',
          imgsrc: ""
        },

      ]
    }],
    textblock:[
      {
        textdiv:"div1 zoomInEffect",
        textclass:"box centertext",
        textdata:data.string.p3text3
      },
      {
        textdiv:"div2 zoomInEffect",
        textclass:"box centertext",
        textdata:data.string.p3text4
      }
    ]
  },
  // slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic maintopic1",
    uppertextblock: [
      {
        textclass: "newfont centertext",
        textdata: data.string.p3text5
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg1",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "frogcycle1",
          imgclass: "relativecls frogcycle1img",
          imgid: 'frogcycle1Img',
          imgsrc: ""
        },
        {
          imgdiv: "frogcycle2",
          imgclass: "relativecls frogcycle2img",
          imgid: 'frogcycle2Img',
          imgsrc: ""
        },
        {
          imgdiv: "frogcycle3",
          imgclass: "relativecls frogcycle3img",
          imgid: 'frogcycle3Img',
          imgsrc: ""
        },
        {
          imgdiv: "frogcycle4",
          imgclass: "relativecls frogcycle4img",
          imgid: 'frogcycle4Img',
          imgsrc: ""
        },
        {
          imgdiv: "yellarw1",
          imgclass: "relativecls yellarw1img",
          imgid: 'yellarwImg',
          imgsrc: ""
        },
        {
          imgdiv: "yellarw2",
          imgclass: "relativecls yellarw2img",
          imgid: 'yellarwImg',
          imgsrc: ""
        },
        {
          imgdiv: "yellarw3",
          imgclass: "relativecls yellarw3img",
          imgid: 'yellarwImg',
          imgsrc: ""
        },
        {
          imgdiv: "yellarw4",
          imgclass: "relativecls yellarw4img",
          imgid: 'yellarwImg',
          imgsrc: ""
        },
      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic maintopic1",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p3text6
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "frogswim imgclick",
          imgclass: "relativecls frogswimimg",
          imgid: 'frogswimImg',
          imgsrc: ""
        },
        {
          imgdiv: "frogweb zoomInEffect",
          imgclass: "relativecls frogwebimg",
          imgid: 'frogwebImg',
          imgsrc: ""
        },
        {
          imgdiv: "handpulse imgclick",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        }
      ]
    }],
    extradiv:"true"
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic maintopic1",
    uppertextblock: [
      {

        textclass: "subtopic centertext",
        textdata: data.string.p3text7
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "newt1 ",
          imgclass: "relativecls newtimg1",
          imgid: 'newtImg',
          imgsrc: ""
        },
        {
          imgdiv: "salamander1",
          imgclass: "relativecls salamanderimg1",
          imgid: 'salamanderImg',
          imgsrc: ""
        }
      ]
    }],
  },

];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "bgImg", src: imgpath+"page2_bg.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bgImg1", src: imgpath+"page2_bg_new.png", type: createjs.AbstractLoader.IMAGE},
      {id: "salamanderImg", src: imgpath+"salamander.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogImg", src: imgpath+"frog_01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "newtImg", src: imgpath+"newt.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boxImg", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "arrow1Img", src:imgpath+"orange_arrow01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "arrow2Img", src:imgpath+"orange_arrow02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogcycle1Img", src:imgpath+"froglifecycle01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogcycle2Img", src:imgpath+"froglifecycle02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogcycle3Img", src:imgpath+"froglifecycle03.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogcycle4Img", src:imgpath+"froglifecycle04.png", type: createjs.AbstractLoader.IMAGE},
      {id: "yellarwImg", src:imgpath+"arrow.png", type: createjs.AbstractLoader.IMAGE},
      {id: "frogswimImg", src:imgpath+"frog_swimming_02.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "frogwebImg", src:imgpath+"frog01.png", type: createjs.AbstractLoader.IMAGE},


      // sounds
      {id: "sound_1", src: soundAsset+"s3_p1.ogg"},
      {id: "sound_2", src: soundAsset+"s3_p2.ogg"},
      {id: "sound_3", src: soundAsset+"s3_p2_1_salamander.ogg"},
      {id: "sound_4", src: soundAsset+"s3_p2_2_frog.ogg"},
      {id: "sound_5", src: soundAsset+"s3_p2_3_newt.ogg"},
      {id: "sound_6", src: soundAsset+"s3_p3.ogg"},
      {id: "sound_7", src: soundAsset+"s3_p4.ogg"},
      {id: "sound_8", src: soundAsset+"s3_p4_body head.ogg"},
      {id: "sound_9", src: soundAsset+"s3_p5.ogg"},
      {id: "sound_10", src: soundAsset+"s3_p6.ogg"},
      {id: "sound_11", src: soundAsset+"s3_p7.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    switch(countNext){
      case 0:
      sound_player('sound_1');
      break;
      case 1:
      sound_player_nonav('sound_2');
      showName();
      break;
      case 2:
      sound_player('sound_6');
      break;
      case 3:
      sound_player('sound_7');
      break;
      case 4:
      sound_player('sound_9');
      showlifecycle();
      break;
      case 5:
      sound_player_nonav('sound_10');
      $(".frogweb,.handpulse").hide();
      setTimeout(function(){
        $(".handpulse").show();
        $(".frogswimimg").attr("src",imgpath+"frog_swimming_02.png");
        $(".imgclick").click(function(){
          navigationcontroller(countNext,$total_page);
          $(".frogweb").show();
          $(".handpulse").hide();
          $(".pondbg").css("opacity",0.5);
        });
      },3000);
      break;
      case 6:
      sound_player('sound_11');
      break;
      default:
      navigationcontroller(countNext,$total_page);
      break;
    }
  }



  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigationcontroller(countNext,$total_page);
    });
  }

  function sound_player_nonav(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }


  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }

  function showName(){
    $(".salamanderbox,.frogbox,.newtbox").hide();
    $(".imgclick").click(function(){
      if($(this).hasClass("salamander")||$(this).hasClass("hand1")){
        $(".salamanderbox").show();
        $(this).addClass("avoid-clicks");
        sound_player_nonav('sound_3');
        $(".hand").removeClass("hand1").addClass("hand2");
      }
      else if($(this).hasClass("frog")||$(this).hasClass("hand2")){
        $(".frogbox").show();
        $(this).addClass("avoid-clicks");
        sound_player_nonav('sound_4');
        $(".hand").removeClass("hand2").addClass("hand3");
      }
      else if($(this).hasClass("newt")||$(this).hasClass("hand3")){
        $(".newtbox").show();
        $(this).addClass("avoid-clicks");
        $(".hand").remove();
        sound_player_nonav('sound_5');
        navigationcontroller(countNext,$total_page);
      }
    });
  }

  function showlifecycle(){
    $('.frogcycle1').animate({opacity:1},500);
    $('.yellarw1').delay(500).animate({opacity:1},500);
    $('.frogcycle2').delay(1000).animate({opacity:1},500);
    $('.yellarw2').delay(1500).animate({opacity:1},500);
    $('.frogcycle3').delay(2000).animate({opacity:1},500);
    $('.yellarw3').delay(2500).animate({opacity:1},500);
    $('.frogcycle4').delay(3000).animate({opacity:1},500);
    $('.yellarw4').delay(3500).animate({opacity:1},500);

  }
});

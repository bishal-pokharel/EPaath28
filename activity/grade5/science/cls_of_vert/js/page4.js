var imgpath = $ref + "/images/Page03/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.string.reptiles1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pondbg",
                    imgclass: "relativecls bgimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintopic maintopic1",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p4text1
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pondbg",
                    imgclass: "relativecls bgimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crocodilediv",
                    imgclass: "relativecls crocodileimg",
                    imgid: 'crocodileImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "wallLizarddiv",
                    imgclass: "relativecls wallLizardimg",
                    imgid: 'wallLizardImg',
                    imgsrc: ""
                },


            ]
        }],
        textblock:[
            {
                textdiv:"div1",
                textclass:"box centertext",
                textdata:data.string.crocodile
            },
            {
                textdiv:"div2",
                textclass:"box centertext",
                textdata:data.string.wallLizard
            }
        ]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintopic",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p4text4
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pondbg",
                    imgclass: "relativecls bgimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crocodile imgdiv imgclick zoomInEffect",
                    imgclass: "relativecls crocodileimg",
                    imgid: 'crocodile1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "snake imgdiv imgclick zoomInEffect",
                    imgclass: "relativecls snakeimg",
                    imgid: 'snakeImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "turtle imgdiv  imgclick zoomInEffect",
                    imgclass: "relativecls turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crocodilebox",
                    imgclass: "relativecls crocodileboximg",
                    imgid: 'boxImg',
                    imgsrc: "",
                    text:true,
                    textclass:"imgtext box",
                    textdata:data.string.crocodile
                },
                {
                    imgdiv: "snakebox",
                    imgclass: "relativecls snakeboximg",
                    imgid: 'boxImg',
                    imgsrc: "",
                    text:true,
                    textclass:"imgtext box",
                    textdata:data.string.snake
                },
                {
                    imgdiv: "turtlebox",
                    imgclass: "relativecls turtleboximg",
                    imgid: 'boxImg',
                    imgsrc: "",
                    text:true,
                    textclass:"imgtext box",
                    textdata:data.string.turtle
                },
                {
                    imgdiv: "hand hand1 imgclick zoomInEffect",
                    imgclass: "relativecls handimg",
                    imgid: 'handImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintopic",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p4text5
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pondbg1",
                    imgclass: "relativecls bgimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crockdiv zoomInEffect",
                    imgclass: "relativecls crockimg",
                    imgid: 'crocodile1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow1",
                    imgclass: "relativecls arrow1img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow2",
                    imgclass: "relativecls arrow2img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow3",
                    imgclass: "relativecls arrow3img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "arrow4",
                    imgclass: "relativecls arrow4img",
                    imgid: 'arrowImg',
                    imgsrc: ""
                },

            ]
        }],
        textblock:[
            {
                textdiv:"d1 namebox",
                textclass:"box centertext",
                textdata:data.string.head
            },
            {
                textdiv:"d2 namebox",
                textclass:"box centertext",
                textdata:data.string.tail
            },
            {
                textdiv:"d3 namebox",
                textclass:"box centertext",
                textdata:data.string.neck
            },
            {
                textdiv:"d4 namebox",
                textclass:"box centertext",
                textdata:data.string.body
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"maintopic",
        uppertextblock: [
            {
                textclass: "subtopic centertext",
                textdata: data.string.p4text6
            }
        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "pondbg1",
                    imgclass: "relativecls bgimg",
                    imgid: 'bgImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "crockcycle1",
                    imgclass: "relativecls crockcycle1img",
                    imgid: 'crockcycle1Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "crockcycle2",
                    imgclass: "relativecls crockcycle2img",
                    imgid: 'crockcycle2Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "crockcycle3",
                    imgclass: "relativecls crockcycle3img",
                    imgid: 'crockcycle3Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "crockcycle4",
                    imgclass: "relativecls crockcycle4img",
                    imgid: 'crockcycle4Img',
                    imgsrc: ""
                },
                {
                    imgdiv: "yellarw1",
                    imgclass: "relativecls yellarw1img",
                    imgid: 'yellarwImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "yellarw2",
                    imgclass: "relativecls yellarw2img",
                    imgid: 'yellarwImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "yellarw3",
                    imgclass: "relativecls yellarw3img",
                    imgid: 'yellarwImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "yellarw4",
                    imgclass: "relativecls yellarw4img",
                    imgid: 'yellarwImg',
                    imgsrc: ""
                },
            ]
        }]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "bgImg", src: imgpath+"sea_bg.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crocodileImg", src: imgpath+"crocodile.png", type: createjs.AbstractLoader.IMAGE},
            {id: "wallLizardImg", src: imgpath+"wall_lizard.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crocodile1Img", src: imgpath+"crock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snakeImg", src: imgpath+"snake.png", type: createjs.AbstractLoader.IMAGE},
            {id: "turtleImg", src: imgpath+"turtle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "boxImg", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
            {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
            {id: "arrowImg", src:imgpath+"green_arrow.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crockcycle1Img", src:imgpath+"croc01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crockcycle2Img", src:imgpath+"crock02.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crockcycle3Img", src:imgpath+"crock03.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crockcycle4Img", src:imgpath+"crock04.png", type: createjs.AbstractLoader.IMAGE},
            {id: "yellarwImg", src:imgpath+"arrow01.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"s4_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s4_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s4_p2_crocodile.ogg"},
            {id: "sound_4", src: soundAsset+"s4_p2_lizard.ogg"},
            {id: "sound_5", src: soundAsset+"s4_p3.ogg"},
            {id: "sound_6", src: soundAsset+"s4_p3_1_crocodile.ogg"},
            {id: "sound_7", src: soundAsset+"s4_p3_2_snake.ogg"},
            {id: "sound_8", src: soundAsset+"s4_p3_3_tortoise.ogg"},
            {id: "sound_9", src: soundAsset+"s4_p4.ogg"},
            {id: "sound_10", src: soundAsset+"s4_p5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
          case 0:
          sound_player('sound_1');
          break;
            case 1:
              sound_player('sound_2');
                showImage();
                break;
            case 2:
            sound_player_nonav('sound_5');
                showName();
                break;
            case 3:
            sound_player('sound_9');
                showlabel();
                break;
            case 4:
            sound_player('sound_10');
                showlifecycle();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }


    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext,$total_page);
        });
    }

    function sound_player_nonav(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }

    function showName(){
        $(".crocodilebox,.snakebox,.turtlebox").hide();
        $(".imgclick").click(function(){
            if($(this).hasClass("crocodile")||$(this).hasClass("hand1")){
                $(".crocodilebox").show();
                $(this).addClass("avoid-clicks");
                $(".hand").removeClass("hand1").addClass("hand2");
                sound_player_nonav('sound_6');
            }
            else if($(this).hasClass("snake")||$(this).hasClass("hand2")){
                $(".snakebox").show();
                $(this).addClass("avoid-clicks");
                $(".hand").removeClass("hand2").addClass("hand3");
                sound_player_nonav('sound_7');
            }
            else if($(this).hasClass("turtle")||$(this).hasClass("hand3")){
                $(".turtlebox").show();
                $(this).addClass("avoid-clicks");
                $(".hand").remove();
                sound_player_nonav('sound_8');
                navigationcontroller(countNext,$total_page);
            }
        });
    }

    function showlifecycle(){
        $('.crockcycle1').animate({opacity:1},500);
        $('.yellarw1').delay(500).animate({opacity:1},500);
        $('.crockcycle2').delay(1000).animate({opacity:1},500);
        $('.yellarw2').delay(1500).animate({opacity:1},500);
        $('.crockcycle3').delay(2000).animate({opacity:1},500);
        $('.yellarw3').delay(2500).animate({opacity:1},500);
        $('.crockcycle4').delay(3000).animate({opacity:1},500);
        $('.yellarw4').delay(3500).animate({opacity:1},500);
        // setTimeout(function(){
        //     navigationcontroller(countNext,$total_page);
        // },4000);
    }
    function showImage(){
        $(".crocodilediv").animate({opacity:1},1000);
        $(".div1").delay(1500).animate({opacity:1},1000);
        $(".wallLizarddiv").delay(2500).animate({opacity:1},1000);
        $(".div2").delay(3500).animate({opacity:1},1000);
    }
    function showlabel(){
        $(".d1").delay(2000).animate({opacity:1},1000);
        $(".arrow1").delay(3000).animate({opacity:1},500);
        $(".d2").delay(3500).animate({opacity:1},1000);
        $(".arrow2").delay(4500).animate({opacity:1},500);
        $(".d3").delay(5000).animate({opacity:1},1000);
        $(".arrow3").delay(6000).animate({opacity:1},500);
        $(".d4").delay(6500).animate({opacity:1},1000);
        $(".arrow4").delay(7500).animate({opacity:1},500);
        // setTimeout(function(){
        //     navigationcontroller(countNext,$total_page);
        // },8000);
    }
});

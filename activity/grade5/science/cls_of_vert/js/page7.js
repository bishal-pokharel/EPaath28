var imgpath = $ref + "/images/diy1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "content centertext1",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls subimg crocodileimg",
                    imgid: 'crocodileImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "subtopic centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "subtopic centertext",
                textdata: data.string.amphibian1
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "subtopic centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option4",
                textclass: "subtopic centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option5",
                textclass: "subtopic centertext",
                textdata: data.string.mammals1
            }

        ],
    },
    //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls sparrowimg",
                    imgid: 'sparrowImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "subtopic centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "subtopic centertext",
                textdata: data.string.amphibian1
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "subtopic centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option4",
                textclass: "subtopic centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option5",
                textclass: "subtopic centertext",
                textdata: data.string.mammals1
            }

        ],
    },

//slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls deerimg",
                    imgid: 'deerImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "subtopic centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "subtopic centertext",
                textdata: data.string.amphibian1
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "subtopic centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option4",
                textclass: "subtopic centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option5",
                textclass: "subtopic centertext",
                textdata: data.string.mammals1
            }

        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls frogimg",
                    imgid: 'frogImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "subtopic centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "subtopic centertext",
                textdata: data.string.amphibian1
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "subtopic centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option4",
                textclass: "subtopic centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option5",
                textclass: "subtopic centertext",
                textdata: data.string.mammals1
            }

        ],
    },
    //slide6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv1",
                    imgclass: "relativecls sharkimg",
                    imgid: 'sharkImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "subtopic centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "subtopic centertext",
                textdata: data.string.amphibian1
            },
            {
                textdiv:"commonbtn option3 correct",
                textclass: "subtopic centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option4",
                textclass: "subtopic centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option5",
                textclass: "subtopic centertext",
                textdata: data.string.mammals1
            }

        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath + "bg_diy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crocodileImg", src: imgpath + "crock_big.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sparrowImg", src: imgpath + "bird.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath + "deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "frogImg", src: imgpath + "frog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sharkImg", src: imgpath + "shark.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"s7_p2_instruction.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
                  play_diy_audio();
                  navigationcontroller(countNext,$total_page);
                  break;
            case 1:
            sound_player_nonav('sound_1');
                shufflehint(data.string.reptiles1);
                checkans();
                break;
            case 2:
                shufflehint(data.string.aves1);
                checkans();
                break;
            case 3:
                shufflehint(data.string.mammals1);
                checkans();
                break;
            case 4:
                shufflehint(data.string.amphibian1);
                checkans();
                break;
            case 5:
                shufflehint(data.string.pisces1);
                checkans();
                break;
            default:
                navigationcontroller(countNext,$total_page);
                break;
        }
    }

    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext,$total_page);
        });
    }

    function sound_player_nonav(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg(){
        $(".div2,.imgdivright,.div4").hide(0);
        setTimeout(function(){
            $(".div2,.imgdivright,.div4").show(0);
        },2500);
    }
    function shufflehint(correctans) {
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4","commonbtn option5"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().trim()==correctans.toString().trim()){
                $this.addClass("correct")
            }
        });

    }
    function checkans(){
        $(".commonbtn ").on("click",function () {
            createjs.Sound.stop();
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                navigationcontroller(countNext,$total_page);
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
        });
    }

});

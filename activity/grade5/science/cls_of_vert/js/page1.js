var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    // slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"coverpagetext",
        uppertextblock: [
            {
                textclass: "chapter centertext",
                textdata: data.lesson.chapter
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "fish",
                    imgclass: "relativecls fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "merle",
                    imgclass: "relativecls merleimg",
                    imgid: 'merleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dog",
                    imgclass: "relativecls dogimg",
                    imgid: 'dogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "newt",
                    imgclass: "relativecls newtimg",
                    imgid: 'newtImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "turtle",
                    imgclass: "relativecls turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                }

            ]
        }]
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic",
        uppertextblock: [
            {
                textclass: "content centertext hide2",
                textdata: data.string.p1text1
            },
             {
               datahighlightflag: true,
               datahighlightcustomclass: "colortext",
                  textclass: "content centertext hide1",
                  textdata: data.string.p1text2
              }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "frogim animal vertebrates",
                    imgclass: "relativecls zoomInEffect frogimg",
                    imgid: 'frogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "turtleim animal vertebrates",
                    imgclass: "relativecls zoomInEffect turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "dogim animal vertebrates",
                    imgclass: "relativecls zoomInEffect dogimg",
                    imgid: 'dogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "grasshopperim animal",
                    imgclass: "relativecls  zoomInEffect grasshopperimg",
                    imgid: 'grasshopperImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "butterflyim animal",
                    imgclass: "relativecls zoomInEffect butterflyimg",
                    imgid: 'butterflyImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "fishim animal vertebrates",
                    imgclass: "relativecls zoomInEffect fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "insectim animal",
                    imgclass: "relativecls  zoomInEffect insectimg",
                    imgid: 'insectImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "henim animal vertebrates",
                    imgclass: "relativecls zoomInEffect henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    //slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic1 slideL",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textcolor",
                textclass: "content centertext",
                textdata: data.string.p1text3
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls relativeimg1 fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2",
                    imgclass: "relativecls relativeimg1 frogimg",
                    imgid: 'frogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div3",
                    imgclass: "relativecls relativeimg1 turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div4",
                    imgclass: "relativecls relativeimg1 henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div5",
                    imgclass: "relativecls relativeimg1 dogimg",
                    imgid: 'dogImg',
                    imgsrc: ""
                }
            ]
        }]
    },
    // slide3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic1 slideL",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textcolor",
                textclass: "subtopic centertext",
                textdata: data.string.p1text4
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1 gdiv",
                    imgclass: "relativecls relativeimg1 fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2 gdiv",
                    imgclass: "relativecls relativeimg1 frogimg",
                    imgid: 'frogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div3 gdiv",
                    imgclass: "relativecls relativeimg1 turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div4 gdiv",
                    imgclass: "relativecls relativeimg1 henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div5 gdiv",
                    imgclass: "relativecls relativeimg1 dogimg",
                    imgid: 'dogImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"piscesdiv",
                textclass:"box centertext",
                textdata:data.string.pisces
            },
            {
                textdiv:"amphibiandiv",
                textclass:"box centertext",
                textdata:data.string.amphibian
            },
            {
                textdiv:"reptilesdiv ",
                textclass:"box centertext",
                textdata:data.string.reptiles
            },
            {
                textdiv:"avesdiv",
                textclass:"box centertext",
                textdata:data.string.aves
            },
            {
                textdiv:"mammalsdiv",
                textclass:"box centertext",
                textdata:data.string.mammals
            }
        ]
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        uppertextblockadditionalclass:"topic1 slideR",
        uppertextblock: [
            {
                datahighlightflag: true,
                datahighlightcustomclass: "textcolor",
                textclass: "subtopic centertext",
                textdata: data.string.p1text5
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "div1",
                    imgclass: "relativecls relativeimg1 fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div2",
                    imgclass: "relativecls relativeimg1 frogimg",
                    imgid: 'frogImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div3",
                    imgclass: "relativecls relativeimg1 turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div4",
                    imgclass: "relativecls relativeimg1 henimg",
                    imgid: 'henImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "div5",
                    imgclass: "relativecls relativeimg1 dogimg",
                    imgid: 'dogImg',
                    imgsrc: ""
                }
            ]
        }],
        textblock:[
            {
                textdiv:"piscesdiv",
                textclass:"box centertext",
                textdata:data.string.pisces
            },
            {
                textdiv:"amphibiandiv",
                textclass:"box centertext",
                textdata:data.string.amphibian
            },
            {
                textdiv:"reptilesdiv",
                textclass:"box centertext",
                textdata:data.string.reptiles
            },
            {
                textdiv:"avesdiv",
                textclass:"box centertext",
                textdata:data.string.aves
            },
            {
                textdiv:"mammalsdiv",
                textclass:"box centertext",
                textdata:data.string.mammals
            }
        ]
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count=0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller =  new Vocabulary();
    vocabcontroller.init();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [
            {id: "fishImg", src: imgpath+"fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "merleImg", src: imgpath+"merle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dogImg", src: imgpath+"dog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "newtImg", src: imgpath+"newt.png", type: createjs.AbstractLoader.IMAGE},
            {id: "turtleImg", src: imgpath+"turtle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "grasshopperImg", src: imgpath+"grasshopper.png", type: createjs.AbstractLoader.IMAGE},
            {id: "butterflyImg", src: imgpath+"butterfly.png", type: createjs.AbstractLoader.IMAGE},
            {id: "henImg", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
            {id: "frogImg", src: imgpath+"frog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "insectImg", src: imgpath+"insect.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset+"s1_p1.ogg"},
            {id: "sound_2", src: soundAsset+"s1_p2.ogg"},
            {id: "sound_3", src: soundAsset+"s1_p2_1.ogg"},
            {id: "sound_4", src: soundAsset+"s1_p3.ogg"},
            {id: "sound_5", src: soundAsset+"s1_p4.ogg"},
            {id: "sound_6", src: soundAsset+"s1_p4_1_pisces.ogg"},
            {id: "sound_7", src: soundAsset+"s1_p4_2_amphibians.ogg"},
            {id: "sound_8", src: soundAsset+"s1_p4_3_reptiles.ogg"},
            {id: "sound_9", src: soundAsset+"s1_p4_4_aves.ogg"},
            {id: "sound_10", src: soundAsset+"s1_p4_5_mammals.ogg"},
            {id: "sound_11", src: soundAsset+"s1_p5.ogg"},
        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        texthighlight($board);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext,preload);
        switch(countNext){
            case 0:
              sound_player('sound_'+(countNext+1));
              break;
            case 1:
            sound_player_nonav('sound_'+(countNext+1));
            $(".hide1").hide();
                showAnimalsOneByOne();
                break;
            case 2:
            sound_player('sound_4');
            break;
            case 3:
                sound_player_nonav('sound_5');
                showVertebratesGroup();
                break;
            case 4:
            sound_player('sound_11');
            break;
        }
    }



    function sound_player(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigationcontroller(countNext,$total_page);
        });
    }

    function sound_player_nonav(sound_id,navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }

    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";


        if($alltextpara.length > 0){
            $.each($alltextpara, function(index, val) {
                /*if there is a data-highlightcustomclass attribute defined for the text element
                use that or else use default 'parsedstring'*/
                $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                texthighlightstarttag = "<span class='"+stylerulename+"'>";


                replaceinstring       = $(this).html();
                replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                $(this).html(replaceinstring);
            });
        }
    }
  function showAnimalsOneByOne(){
      $(".animal").find("img").hide();
      setTimeout(function(){
         $(".frogimg").show();
          setTimeout(function(){
              $(".turtleimg").show();
              setTimeout(function(){
                  $(".dogimg").show();
                  setTimeout(function(){
                      $(".grasshopperimg").show();
                      setTimeout(function(){
                          $(".butterflyimg").show();
                          setTimeout(function(){
                              $(".fishimg").show();
                              setTimeout(function(){
                                  $(".insectimg").show();
                                  setTimeout(function(){
                                      $(".henimg").show();
                                      checkVertebrates();
                                  },1000);
                              },1000);
                          },1000);
                      },1000);
                  },1000);
              },1000);
          },1000);
      },1000);
  }
    function checkVertebrates(){
        var count=0;
        $(".animal").click(function(){
            if($(this).hasClass("vertebrates")){
                play_correct_incorrect_sound(1);
                $(this).prepend("<img class='correctImg' src='images/right.png'/>");
                $(this).addClass("avoid-clicks");
                count++;

            }
            else{
                $(this).prepend("<img class='correctImg' src='images/wrong.png'/>");
                play_correct_incorrect_sound(0);
                $(this).addClass("avoid-clicks");
            }
            if(count==5){
                $(".hide2").hide();
                $(".hide1").show();
                sound_player('sound_3');
                // $(".topic p").text(data.string.p1text2)
                $(".topic").css("height","20%");
                positionVertebrates();
            }
        })
    }
    function positionVertebrates(){
        $(".correctImg,.butterflyim,.insectim,.grasshopperim").remove();
        $(".fishim").addClass("fishdiv");
        $(".frogim").addClass("frogdiv");
        $(".turtleim").addClass("turtlediv");
        $(".henim").addClass("hendiv");
        $(".dogim").addClass("dogdiv");
        $(".fishImg,.frogImg,.turtleImg,.henImg,.dogImg").addClass("relativeimg");
    }

    function showVertebratesGroup(){
        $(".piscesdiv,.amphibiandiv,.reptilesdiv,.avesdiv,.mammalsdiv").hide();
        var count=0;
        $(".gdiv").click(function(){
            if($(this).hasClass("div1")){
                $(".piscesdiv").show();
                $(this).addClass("avoid-clicks");
                sound_player_nonav('sound_6');
                count++;
            }
            if($(this).hasClass("div2")){
                $(".amphibiandiv").show();
                sound_player_nonav('sound_7');
                $(this).addClass("avoid-clicks");
                count++;
            }
            if($(this).hasClass("div3")){
                $(".reptilesdiv").show();
                sound_player_nonav('sound_8');
                $(this).addClass("avoid-clicks");
                count++;
            }
            if($(this).hasClass("div4")){
                $(".avesdiv").show();
                sound_player_nonav('sound_9');
                $(this).addClass("avoid-clicks");
                count++;
            }
            if($(this).hasClass("div5")){
                $(".mammalsdiv").show();
                sound_player_nonav('sound_10');
                $(this).addClass("avoid-clicks");
                count++;
            }
            if(count==5){
              navigationcontroller(countNext,$total_page);
            }
        });
    }
});

var imgpath = $ref + "/images/Page05/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"coverpagetext",
    uppertextblock: [
      {
        textclass: "chapter centertext title",
        textdata: data.string.mammals1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },

      ]
    }]
  },
  //slide 1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic slideR",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p6text7
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "dogdiv slideL",
          imgclass: "relativecls cowimg",
          imgid: 'cowImg',
          imgsrc: ""
        }
      ]
    }]
  },

  //slide 2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic slideR",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p6text8
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "dogdiv slideL",
          imgclass: "relativecls cowimg",
          imgid: 'batImg',
          imgsrc: ""
        }
      ]
    }]
  },
  //slide 3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic slideR",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p6text9
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "dogdiv slideL",
          imgclass: "relativecls cowimg",
          imgid: 'dolphinImg',
          imgsrc: ""
        }
      ]
    }]
  },

  //slide 4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p6text10
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "fishes",
          imgclass: "relativecls monkeyimg",
          imgid: 'threeImg',
          imgsrc: ""
        }

      ]
    }]
  },

  //slide5

  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p6text11
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "dogdiv1 zoomInEffect",
          imgclass: "relativecls dogimg",
          imgid: 'dogImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow1",
          imgclass: "relativecls arrow1img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow2",
          imgclass: "relativecls arrow2img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow3",
          imgclass: "relativecls arrow3img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow4",
          imgclass: "relativecls arrow4img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow5",
          imgclass: "relativecls arrow5img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow6",
          imgclass: "relativecls arrow6img",
          imgid: 'arrowImg',
          imgsrc: ""
        }
      ]
    }],
    textblock:[
      {
        textdiv:"d1 namebox",
        textclass:"box centertext",
        textdata:data.string.head
      },
      {
        textdiv:"d2 namebox",
        textclass:"box centertext",
        textdata:data.string.body
      },
      {
        textdiv:"d3 namebox",
        textclass:"box centertext",
        textdata:data.string.tail
      },
      {
        textdiv:"d4 namebox",
        textclass:"box centertext",
        textdata:data.string.legs
      },
      {
        textdiv:"d5 namebox",
        textclass:"box centertext",
        textdata:data.string.neck
      }
    ]
  },
  //slide 6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p6text1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg',
          imgsrc: ""
        },
        {
          imgdiv: "monkey imgdiv imgclick zoomInEffect",
          imgclass: "relativecls monkeyimg",
          imgid: 'monkeyImg',
          imgsrc: ""
        },
        {
          imgdiv: "bear imgdiv imgclick zoomInEffect",
          imgclass: "relativecls bearimg",
          imgid: 'bearImg',
          imgsrc: ""
        },
        {
          imgdiv: "deer imgdiv  imgclick zoomInEffect",
          imgclass: "relativecls deerimg",
          imgid: 'deerImg',
          imgsrc: ""
        },
        {
          imgdiv: "monkeybox",
          imgclass: "relativecls monkeyboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.monkey
        },
        {
          imgdiv: "bearbox",
          imgclass: "relativecls bearboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.bear
        },
        {
          imgdiv: "deerbox",
          imgclass: "relativecls deerboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.deer
        },
        {
          imgdiv: "hand hand1 imgclick zoomInEffect",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        }

      ]
    }]
  },

  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p6text3
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "dolphindiv zoomInEffect",
          imgclass: "relativecls dolphinimg",
          imgid: 'dolphinImg',
          imgsrc: ""
        },
        {
          imgdiv: "farrow1",
          imgclass: "relativecls arrow1img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
      ]
    }],
    textblock:[
      {
        textdiv:"fd1 namebox",
        textclass:"box centertext",
        textdata:data.string.fins
      },
    ]
  },

  //slide8
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "main_center_text",
        textdata: data.string.p6text14
      }
    ],
      imageblock: [{
          imagestoshow: [
              {
                  imgdiv: "humanimg",
                  imgclass: "relativecls bgimg",
                  imgid: 'humanImg',
                  imgsrc: ""
              }
          ]
      }]
  }

];

$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "bgImg", src: imgpath+"jungle.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bgImg1", src: imgpath+"sea.png", type: createjs.AbstractLoader.IMAGE},
      {id: "monkeyImg", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bearImg", src: imgpath+"bear.png", type: createjs.AbstractLoader.IMAGE},
      {id: "deerImg", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boxImg", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "arrowImg", src:imgpath+"pink_arrow.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dogImg", src:imgpath+"dog01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "dolphinImg", src:imgpath+"Dolphin.png", type: createjs.AbstractLoader.IMAGE},
      {id: "catImg", src:imgpath+"cat.png", type: createjs.AbstractLoader.IMAGE},
      {id: "cowImg", src:imgpath+"cow-and-calf_new.png", type: createjs.AbstractLoader.IMAGE},
      {id: "batImg", src:imgpath+"bat.png", type: createjs.AbstractLoader.IMAGE},
      {id: "threeImg", src:imgpath+"three_fish.png", type: createjs.AbstractLoader.IMAGE},
      {id: "humanImg", src:imgpath+"human.png", type: createjs.AbstractLoader.IMAGE},
      // sounds
      {id: "sound_1", src: soundAsset+"s6_p1.ogg"},
      {id: "sound_2", src: soundAsset+"s6_p2.ogg"},
      {id: "sound_3", src: soundAsset+"s6_p3.ogg"},
      {id: "sound_4", src: soundAsset+"s6_p4.ogg"},
      {id: "sound_5", src: soundAsset+"s6_p5.ogg"},
      {id: "sound_6", src: soundAsset+"s6_p6.ogg"},
      {id: "sound_7", src: soundAsset+"s6_p7.ogg"},
      {id: "sound_8", src: soundAsset+"s6_p7_1_monkey.ogg"},
      {id: "sound_9", src: soundAsset+"s6_p7_2_bear.ogg"},
      {id: "sound_10", src: soundAsset+"s6_p7_3_deer.ogg"},
      {id: "sound_11", src: soundAsset+"s6_p8.ogg"},
      {id: "sound_12", src: soundAsset+"s6_p8_1_fins.ogg"},
      {id: "sound_13", src: soundAsset+"s6_p9.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    if(countNext<6){
      sound_player('sound_'+(countNext+1));
    }
    switch(countNext){
      case 0:
      break;
      case 6:
      sound_player_nonav('sound_7');
      showName();
      break;
      case 5:
      showlabel();
      break;
      case 7:
      sound_player('sound_11');
      $(".fd1").delay(1000).animate({opacity:1},1000);
      $(".farrow1").delay(1500).animate({opacity:1},500);
      break;
      case 4:
      sound_player('sound_'+(countNext+1));
      $(".cd1").delay(1000).animate({opacity:1},1000);
      $(".carrow1").delay(1500).animate({opacity:1},500);
      $(".cd2").delay(2000).animate({opacity:1},1000);
      $(".carrow2").delay(2500).animate({opacity:1},500);
      break;
      case 8:
      sound_player('sound_13');
      break;


    }
  }



  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      navigationcontroller(countNext,$total_page);
    });
  }

  function sound_player_nonav(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }


  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }

  function showName(){
    $(".monkeybox,.bearbox,.deerbox").hide();
    $(".imgclick").click(function(){
      if($(this).hasClass("monkey")||$(this).hasClass("hand1")){
        $(".monkeybox").show();
        $(".hand").removeClass("hand1").addClass("hand2");
        sound_player_nonav('sound_8');
      }
      else if($(this).hasClass("bear")||$(this).hasClass("hand2")){
        $(".bearbox").show();
        $(".hand").removeClass("hand2").addClass("hand3");
        sound_player_nonav('sound_9');
      }
      else if($(this).hasClass("deer")||$(this).hasClass("hand3")){
        $(".deerbox").show();
        $(".hand").remove();
        sound_player_nonav('sound_10');
        navigationcontroller(countNext,$total_page);
      }
    });
  }

  function showlifecycle(){
    $('.crockcycle1').animate({opacity:1},500);
    $('.yellarw1').delay(500).animate({opacity:1},500);
    $('.crockcycle2').delay(1000).animate({opacity:1},500);
    $('.yellarw2').delay(1500).animate({opacity:1},500);
    $('.crockcycle3').delay(2000).animate({opacity:1},500);
    $('.yellarw3').delay(2500).animate({opacity:1},500);
    $('.crockcycle4').delay(3000).animate({opacity:1},500);
    $('.yellarw4').delay(3500).animate({opacity:1},500);
    setTimeout(function(){
      navigationcontroller(countNext,$total_page);
    },4000);
  }
  function showImage(){
    $(".crocodilediv").animate({opacity:1},1000);
    $(".div1").delay(1500).animate({opacity:1},1000);
    $(".wallLizarddiv").delay(2500).animate({opacity:1},1000);
    $(".div2").delay(3500).animate({opacity:1},1000);
    setTimeout(function(){
      navigationcontroller(countNext,$total_page);
    },4000);
  }
  function showlabel(){
    $(".d1").delay(1000).animate({opacity:1},1000);
    $(".arrow1").delay(2000).animate({opacity:1},500);
    $(".d2").delay(2500).animate({opacity:1},1000);
    $(".arrow2").delay(3500).animate({opacity:1},500);
    $(".d3").delay(4000).animate({opacity:1},1000);
    $(".arrow3").delay(5000).animate({opacity:1},500);
    $(".d4").delay(5500).animate({opacity:1},1000);
    $(".arrow4,.arrow5").delay(6500).animate({opacity:1},500);
    $(".d5").delay(7500).animate({opacity:1},1000);
    $(".arrow6").delay(8000).animate({opacity:1},500);

  }
});

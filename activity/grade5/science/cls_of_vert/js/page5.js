var imgpath = $ref + "/images/Page04/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
  // slide0
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg_blue",
    uppertextblockadditionalclass:"coverpagetext",
    uppertextblock: [
      {
        textclass: "chapter centertext title",
        textdata: data.string.aves1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "peacockdiv ",
          imgclass: "relativecls peacockimg",
          imgid: 'peacockImg',
          imgsrc: ""
        },
        {
          imgdiv: "sparrowdiv ",
          imgclass: "relativecls sparrowimg",
          imgid: 'sparrowImg',
          imgsrc: ""
        },

      ]
    }]
  },
  //slide 1
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p5text1
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "sparrowdiv ",
          imgclass: "relativecls sparrowimg",
          imgid: 'sparrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "peacockdiv ",
          imgclass: "relativecls peacockimg",
          imgid: 'peacockImg',
          imgsrc: ""
        },
        {
          imgdiv: "sparrowbox",
          imgclass: "relativecls sparrowboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.sparrow
        },
        {
          imgdiv: "peacockbox",
          imgclass: "relativecls peacockboximg",
          imgid: 'boxImg',
          imgsrc: "",
          text:true,
          textclass:"imgtext box",
          textdata:data.string.peacock
        },
        {
          imgdiv: "hand hand1 imgclick zoomInEffect",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        }

      ]
    }],
  },
  //slide2
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "subtopic centertext",
        textdata: data.string.p5text2
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "sparrowdiv zoomInEffect",
          imgclass: "relativecls sparrowimg",
          imgid: 'sparrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "peacockdiv zoomInEffect",
          imgclass: "relativecls peacockimg",
          imgid: 'peacockImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow1",
          imgclass: "relativecls arrow1img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "arrow2",
          imgclass: "relativecls arrow2img",
          imgid: 'arrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "handtxt imgclick zoomInEffect",
          imgclass: "relativecls handimg",
          imgid: 'handImg',
          imgsrc: ""
        },
        {
          imgdiv: "birdsrespdiv zoomInEffect",
          imgclass: "relativecls birdsrespimg",
          imgid: 'birdsrespImg',
          imgsrc: "",
          closebutton:[{}]
        },
      ]
    }],
    textblock:[
      {
        textdiv:"d1 imgclick namebox",
        textclass:"box centertext",
        textdata:data.string.lungs
      },
      {
        textdiv:"d2 namebox",
        textclass:"box centertext",
        textdata:data.string.beak
      },
      {
        textdiv: "respiration",
        textclass:"resp",
        textdata:data.string.respiration
      }
    ]
  },
  //slide 3
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg",
    uppertextblockadditionalclass:"maintopic slideL",
    uppertextblock: [
      {
        textclass: "subtopic centertext",
        textdata: data.string.p5text3
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "pondbg",
          imgclass: "relativecls bgimg",
          imgid: 'bgImg1',
          imgsrc: ""
        },
        {
          imgdiv: "sparrowdiv zoomInEffect",
          imgclass: "relativecls sparrowimg",
          imgid: 'sparrowImg',
          imgsrc: ""
        },
        {
          imgdiv: "peacockdiv zoomInEffect",
          imgclass: "relativecls peacockimg",
          imgid: 'peacockImg',
          imgsrc: ""
        },

      ]
    }]

  },
  //slide4
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        datahighlightflag: true,
        datahighlightcustomclass: "colortext",
        textclass: "box centertext",
        textdata: data.string.p5text4
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "treebranch",
          imgclass: "relativecls treebranchimg",
          imgid: 'treebranchImg',
          imgsrc: ""
        },
        {
          imgdiv: "hawk",
          imgclass: "relativecls hawkimg",
          imgid: 'hawkImg',
          imgsrc: ""
        },

      ]
    }]
  },
  //slide5
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "box centertext",
        textdata: data.string.p5text5
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "ostrichdiv fadeInEffect",
          imgclass: "relativecls ostrichimg",
          imgid: 'ostrichImg',
          imgsrc: ""
        },
        {
          imgdiv: "penguindiv fadeInEffect",
          imgclass: "relativecls penguinimg",
          imgid: 'penguinImg',
          imgsrc: ""
        },

      ]
    }],
    textblock:[
      {
        textdiv:"div1",
        textclass:"box1 centertext",
        textdata:data.string.ostrich
      },
      {
        textdiv:"div2",
        textclass:"box2 centertext",
        textdata:data.string.penguin
      }
    ]
  },
  //slide6
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "box centertext",
        textdata: data.string.p5text5
      }
    ],
    imageblock: [{
      imagestoshow: [
        {
          imgdiv: "ostrichdiv fadeInEffect",
          imgclass: "relativecls ostrichimg",
          imgid: 'ostrichImg',
          imgsrc: ""
        },

        {
          imgdiv: "ostrichbox fadeInEffect",
          imgclass: "relativecls ostrichingimg",
          imgid: 'squarebox',
          imgsrc: "",
          text:true,
          textclass:"box relativecls imgtext1",
          textdata:data.string.ostrich1
        },


      ]
    }],
    textblock:[
      {
        textdiv:"div1",
        textclass:"box1 centertext",
        textdata:data.string.ostrich
      }

    ]
  },
  //slide7
  {
    contentnocenteradjust: true,
    contentblockadditionalclass: "bg1",
    uppertextblockadditionalclass:"maintopic",
    uppertextblock: [
      {
        textclass: "box centertext",
        textdata: data.string.p5text5
      }
    ],
    imageblock: [{
      imagestoshow: [

        {
          imgdiv: "penguindiv fadeInEffect",
          imgclass: "relativecls penguinimg",
          imgid: 'penguinImg',
          imgsrc: ""
        },

        {
          imgdiv: "penguinbox fadeInEffect",
          imgclass: "relativecls penguinboximg",
          imgid: 'squarebox',
          imgsrc: "",
          text:true,
          textclass:" box relativecls imgtext2",
          textdata:data.string.penguin1
        },
      ]
    }],
    textblock:[

      {
        textdiv:"div2",
        textclass:"box2 centertext",
        textdata:data.string.penguin
      }
    ]
  },

];
$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var count=0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  var preload;
  var timeoutvar = null;
  var current_sound;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();
  function init() {
    //specify type otherwise it will load assests as XHR
    manifest = [
      {id: "bgImg", src: imgpath+"bg_for_bird.png", type: createjs.AbstractLoader.IMAGE},
      {id: "bgImg1", src: imgpath+"bg_bird02.png", type: createjs.AbstractLoader.IMAGE},
      {id: "sparrowImg", src: imgpath+"bird.png", type: createjs.AbstractLoader.IMAGE},
      {id: "peacockImg", src: imgpath+"peacock.png", type: createjs.AbstractLoader.IMAGE},
      {id: "boxImg", src: imgpath+"text_box.png", type: createjs.AbstractLoader.IMAGE},
      {id: "handImg", src:"images/hand-icon.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "arrowImg", src:imgpath+"pink_arrow_01.png", type: createjs.AbstractLoader.IMAGE},
      {id: "hawkImg", src:imgpath+"hawk-eagle-gif.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "treebranchImg", src:imgpath+"tree_branch.png", type: createjs.AbstractLoader.IMAGE},
      {id: "ostrichImg", src:imgpath+"ostrich.png", type: createjs.AbstractLoader.IMAGE},
      {id: "penguinImg", src:imgpath+"penguin.png", type: createjs.AbstractLoader.IMAGE},
      {id: "birdsrespImg", src:imgpath+"respiration-of-birds_new.gif", type: createjs.AbstractLoader.IMAGE},
      {id: "squarebox", src:imgpath+"notice_board.png", type: createjs.AbstractLoader.IMAGE},
      {id: "close", src:"images/white_wrong.png", type: createjs.AbstractLoader.IMAGE},

      // sounds
      {id: "sound_1", src: soundAsset+"s5_p1.ogg"},
      {id: "sound_2", src: soundAsset+"s5_p2.ogg"},
      {id: "sound_3", src: soundAsset+"s5_p2_1_sparrow.ogg"},
      {id: "sound_4", src: soundAsset+"s5_p2_2_peacock.ogg"},
      {id: "sound_5", src: soundAsset+"s5_p3.ogg"},
      {id: "sound_6", src: soundAsset+"s5_p3_beak.ogg"},
      {id: "sound_7", src: soundAsset+"s5_p3_lungs.ogg"},
      {id: "sound_8", src: soundAsset+"s5_p3_respiration.ogg"},
      {id: "sound_9", src: soundAsset+"s5_p4.ogg"},
      {id: "sound_10", src: soundAsset+"s5_p5.ogg"},
      {id: "sound_11", src: soundAsset+"s5_p6.ogg"},
      {id: "sound_12", src: soundAsset+"s5_p6_ostrich_penguin.ogg"},
      {id: "sound_13", src: soundAsset+"s5_p7.ogg"},
      {id: "sound_14", src: soundAsset+"s5_p8.ogg"},
    ];
    preload = new createjs.LoadQueue(false);
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.installPlugin(createjs.Sound);//for registering sounds
    preload.on("progress", handleProgress);
    preload.on("complete", handleComplete);
    preload.on("fileload", handleFileLoad);
    preload.loadManifest(manifest, true);
  }

  function handleFileLoad(event) {
    // console.log(event.item);
  }

  function handleProgress(event) {
    $('#loading-text').html(parseInt(event.loaded * 100) + '%');
  }

  function handleComplete(event) {
    $('#loading-wrapper').hide(0);
    //initialize varibales
    current_sound = createjs.Sound.play('sound_1');
    current_sound.stop();
    // call main function
    templateCaller();
  }

  //initialize
  init();

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    texthighlight($board);
    vocabcontroller.findwords(countNext);
    put_image(content, countNext,preload);
    switch(countNext){
      case 0:
      sound_player('sound_1');
      break;
      case 1:
      sound_player_nonav('sound_2');
      showName();
      break;
      case 2:
      showlabel();
      sound_player('sound_5',1);
      $('.closebutton').attr('src',preload.getResult('close').src);
      $('.closebutton').click(function(){
        $('.birdsrespdiv').hide(300);
        $('.resp').hide(300);
      });
      break;
      case 3:
      sound_player('sound_9');
      break;
      case 4:
      sound_player('sound_10');
      break;
      case 5:
      sound_player('sound_11');
      showImage();
      break;
      case 6:
      sound_player('sound_13');
      break;
      case 7:
      sound_player('sound_14');
      break;
      default:
      navigationcontroller(countNext,$total_page);
      break;
    }
  }



  function sound_player(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
    current_sound.on('complete', function () {
      if(!navigate) navigationcontroller(countNext,$total_page);
    });
  }

  function sound_player_nonav(sound_id,navigate) {
    createjs.Sound.stop();
    current_sound = createjs.Sound.play(sound_id);
    current_sound.play();
  }

  function templateCaller() {
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');
    generaltemplate();
    loadTimelineProgress($total_page, countNext + 1);
  }

  $nextBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    switch (countNext) {
      default:
      countNext++;
      templateCaller();
      break;
    }
  });

  $refreshBtn.click(function(){
    templateCaller();
  });

  $prevBtn.on('click', function () {
    createjs.Sound.stop();
    clearTimeout(timeoutvar);
    countNext--;
    templateCaller();
    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  function texthighlight($highlightinside){
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
    null ;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag   = "</span>";


    if($alltextpara.length > 0){
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
        (stylerulename = $(this).attr("data-highlightcustomclass")) :
        (stylerulename = "parsedstring") ;

        texthighlightstarttag = "<span class='"+stylerulename+"'>";


        replaceinstring       = $(this).html();
        replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
        replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


        $(this).html(replaceinstring);
      });
    }
  }

  function showName(){
    $(".sparrowbox,.peacockbox").hide();
    $(".imgclick").click(function(){
      if($(this).hasClass("sparrowdiv")||$(this).hasClass("hand1")){


        $(".sparrowbox").show();
        // $(this).addClass("avoid-clicks");
        $(".hand").removeClass("hand1").addClass("hand2");
        sound_player_nonav('sound_3');
      }
      else if($(this).hasClass("peacockdiv")||$(this).hasClass("hand2")){


        $(".peacockbox").show();
        // $(this).addClass("avoid-clicks");
        $(".hand").remove();
        // $(".maintopic p").text(data.string.p5text5_1);
        navigationcontroller(countNext,$total_page);
        sound_player_nonav('sound_4');
      }
    });
  }

  function showlabel(){
    $(".birdsrespdiv,.handtxt,.respiration").hide();

    $(".d1").delay(2000).animate({opacity:1},1000);
    $(".arrow1").delay(3000).animate({opacity:1},500);
    $(".d2").delay(3500).animate({opacity:1},1000);
    $(".arrow2").delay(4500).animate({opacity:1},500);
    setTimeout(function(){
      $(".handtxt").show();
      $(".imgclick").click(function(){
        $(".birdsrespdiv,.respiration").show();
        navigationcontroller(countNext,$total_page);
        $(".handtxt").remove();
      });
    },5000);

  }
  function showImage(){
    $(".ostrichdiv").animate({opacity:1});
    $(".div1").animate({opacity:1});
    $(".penguindiv").animate({opacity:1});
    $(".div2").animate({opacity:1});
    // setTimeout(function(){
    //   navigationcontroller(countNext,$total_page);
    // });
  }



});

var imgpath = $ref + "/images/diy2/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        uppertextblockadditionalclass: "objdiv",
        uppertextblock: [
            {
                textclass: "content centertext1",
                textdata: data.string.diy
            }

        ],
        imageblock: [{
            imagestoshow: [
                {
                    imgdiv: "imgdiv",
                    imgclass: "relativecls diyimg",
                    imgid: 'diyImg',
                    imgsrc: ""
                },

            ]
        }]
    },
    // slide1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            option:true,
            imagestoshow: [
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls crocodileimg",
                    imgid: 'crocodileImg',
                    imgsrc: "",
                    ans:"correct1",
                },
                {
                    imgdiv: "commonimg img2",
                    imgclass: "relativecls frogimg",
                    imgid: 'frogImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img3",
                    imgclass: "relativecls deerimg",
                    imgid: 'deerImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "commonimg img4",
                    imgclass: "relativecls newtimg",
                    imgid: 'newtImg',
                    imgsrc: "",
                    ans:"correct2",
                },
                {
                    imgdiv: "commonimg img5",
                    imgclass: "relativecls fishimg",
                    imgid: 'fishImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq2
            },
        ],
    },
  //slide 2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            option:true,
            imagestoshow: [
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: "",
                    ans:"correct1",
                },
                {
                    imgdiv: "commonimg img2",
                    imgclass: "relativecls frogimg",
                    imgid: 'frogImg',
                    imgsrc: "",
                    ans:"correct2",
                },
                {
                    imgdiv: "commonimg img3",
                    imgclass: "relativecls bearimg",
                    imgid: 'bearImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "commonimg img4",
                    imgclass: "relativecls dolphinimg",
                    imgid: 'dolphinImg',
                    imgsrc: ""
                },
                {
                    imgdiv: "commonimg img5",
                    imgclass: "relativecls quidimg",
                    imgid: 'batImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq3
            },
        ],
    },
  //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            option:true,
            imagestoshow: [
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls monkeyimg",
                    imgid: 'monkeyImg',
                    imgsrc: "",
                    ans:"correct1",
                },
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls turtleimg",
                    imgid: 'turtleImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img3",
                    imgclass: "relativecls bearimg",
                    imgid: 'bearImg',
                    imgsrc: "",
                    ans:"correct2",
                },
                {
                    imgdiv: "commonimg img4",
                    imgclass: "relativecls seahorseimg",
                    imgid: 'seahorseImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img5",
                    imgclass: "relativecls salamanderimg",
                    imgid: 'salamanderImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq4
            },
        ],
    },
    //slide4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            option:true,
            imagestoshow: [
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls sharkimg",
                    imgid: 'sharkImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls snakeimg",
                    imgid: 'snakeImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img3",
                    imgclass: "relativecls peagonimg",
                    imgid: 'peagonImg',
                    imgsrc: "",
                    ans:"correct2",
                },
                {
                    imgdiv: "commonimg img4",
                    imgclass: "relativecls lionimg",
                    imgid: 'lionImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img5",
                    imgclass: "relativecls merleimg",
                    imgid: 'merleImg',
                    imgsrc: "",
                    ans:"correct1",
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq5
            },
        ],
    },
    //slide5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "bg",
        imageblock: [{
            option:true,
            imagestoshow: [
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls sharkimg",
                    imgid: 'sharkImg',
                    imgsrc: "",
                    ans:"correct1",
                },
                {
                    imgdiv: "commonimg img1",
                    imgclass: "relativecls snakeimg",
                    imgid: 'snakeImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img3",
                    imgclass: "relativecls peagonimg",
                    imgid: 'peagonImg',
                    imgsrc: "",
                },
                {
                    imgdiv: "commonimg img4",
                    imgclass: "relativecls fishimg",
                    imgid: 'fishImg',
                    imgsrc: "",
                    ans:"correct2",
                },
                {
                    imgdiv: "commonimg img5",
                    imgclass: "relativecls quidimg",
                    imgid: 'batImg',
                    imgsrc: ""
                },

            ]
        }],
        questionblock: [
            {
                textdiv:"question",
                textclass: "subtopic centertext",
                textdata: data.string.diyq6
            },
        ],
    },
];

$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var vocabcontroller = new Vocabulary();
    vocabcontroller.init();

    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

            {id: "diyImg", src: imgpath + "bg_diy01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "crocodileImg", src: imgpath + "crock.png", type: createjs.AbstractLoader.IMAGE},
            {id: "frogImg", src: imgpath + "frog.png", type: createjs.AbstractLoader.IMAGE},
            {id: "deerImg", src: imgpath + "deer.png", type: createjs.AbstractLoader.IMAGE},
            {id: "fishImg", src: imgpath + "fish.png", type: createjs.AbstractLoader.IMAGE},
            {id: "newtImg", src: imgpath + "newt.png", type: createjs.AbstractLoader.IMAGE},
            {id: "turtleImg", src: imgpath + "turtle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bearImg", src: imgpath + "bear.png", type: createjs.AbstractLoader.IMAGE},
            {id: "dolphinImg", src: imgpath + "Dolphin.png", type: createjs.AbstractLoader.IMAGE},
            {id: "quidImg", src: imgpath + "quid.png", type: createjs.AbstractLoader.IMAGE},
            {id: "monkeyImg", src: imgpath + "monkey.png", type: createjs.AbstractLoader.IMAGE},
            {id: "seahorseImg", src: imgpath + "seahorse.png", type: createjs.AbstractLoader.IMAGE},
            {id: "salamanderImg", src: imgpath + "salamander.png", type: createjs.AbstractLoader.IMAGE},
            {id: "sharkImg", src: imgpath + "shark.png", type: createjs.AbstractLoader.IMAGE},
            {id: "snakeImg", src: imgpath + "snake.png", type: createjs.AbstractLoader.IMAGE},
            {id: "peagonImg", src: imgpath + "birds01.png", type: createjs.AbstractLoader.IMAGE},
            {id: "lionImg", src: imgpath + "lion.png", type: createjs.AbstractLoader.IMAGE},
            {id: "merleImg", src: imgpath + "merle.png", type: createjs.AbstractLoader.IMAGE},
            {id: "batImg", src: imgpath + "bat.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            {id: "sound_1", src: soundAsset + "s8_p2.ogg"},
            {id: "sound_2", src: soundAsset + "s8_p3.ogg"},
            {id: "sound_3", src: soundAsset + "s8_p4.ogg"},
            {id: "sound_4", src: soundAsset + "s8_p5.ogg"},
            {id: "sound_5", src: soundAsset + "s8_p6.ogg"},
          ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        current_sound = createjs.Sound.play('sound_1');
        current_sound.stop();
        // call main function
        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        vocabcontroller.findwords(countNext);
        put_image(content, countNext, preload);
        switch (countNext) {
            case 0:
            play_diy_audio();
                navigationcontroller(countNext,$total_page);
                break;
            default:
                sound_player('sound_'+countNext);
                shufflehint(data.string.pisces1);
                checkans();
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            default:
                countNext++;
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });

    function animateImg(){
        $(".div2,.imgdivright,.div4").hide(0);
        setTimeout(function(){
            $(".div2,.imgdivright,.div4").show(0);
        },2500);
    }
    function shufflehint() {
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonimg img1","commonimg img2","commonimg img3","commonimg img4","commonimg img5"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
        });

    }
    function checkans(){
        var count =0;
        $(".commonimg ").on("click",function () {
            createjs.Sound.stop();
            var clickobj =$(this).find("img").attr('data-answer');
            if(clickobj== "correct1" || clickobj == "correct2" ) {
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                count++;
            }
            else{
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
            }
            if(count>1){
                $(".commonimg").addClass("avoid-clicks");
                navigationcontroller(countNext,$total_page,(countNext==$total_page-1)?true:false);
            }
        });
    }

});

var imgpath = $ref + "/images/imagesfordiy/";
var soundAsset = $ref+"/sounds/";

var content = [
    //slide0
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q1
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.aves1
            },

        ],
    },
    //slide 1
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q2
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
//slide2
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q3
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 3
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q4
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.reptiles1
            },

        ],
    },
    //slide 4
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q5
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 5
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q6
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 6
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q7
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 7
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q8
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.reptiles1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 8
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q9
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.pisces1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.amphibian1
            },

        ],
    },
    //slide 9
    {
        contentnocenteradjust: true,
        contentblockadditionalclass: "",
        questionblock: [
            {
                textdiv:"div1 relativecls",
                textclass: "content centertext",
                textdata: data.string.q10
            },
        ],
        optionblock:[
            {
                textdiv:"commonbtn option1",
                textclass: "content2 centertext",
                textdata: data.string.mammals1
            },
            {
                textdiv:"commonbtn option2",
                textclass: "content2 centertext",
                textdata: data.string.aves1
            },
            {
                textdiv:"commonbtn option3",
                textclass: "content2 centertext",
                textdata: data.string.reptiles1
            },

        ],
    }
];
// content.shufflearray();
$(function () {
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var count = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    var preload;
    var timeoutvar = null;
    var current_sound;
    var score = new scoreBoardTemplate();
    function init() {
        //specify type otherwise it will load assests as XHR
        manifest = [

          {id: "green_flag_png", src:"images/scoreboard/green_flag.png", type: createjs.AbstractLoader.IMAGE},
          {id: "red_flag_png", src:"images/scoreboard/red_flag.png", type: createjs.AbstractLoader.IMAGE},

            // sounds
            // {id: "sound_1", src: soundAsset + "p1_s0.ogg"},

        ];
        preload = new createjs.LoadQueue(false);
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.installPlugin(createjs.Sound);//for registering sounds
        preload.on("progress", handleProgress);
        preload.on("complete", handleComplete);
        preload.on("fileload", handleFileLoad);
        preload.loadManifest(manifest, true);
    }

    function handleFileLoad(event) {
        // console.log(event.item);
    }

    function handleProgress(event) {
        $('#loading-text').html(parseInt(event.loaded * 100) + '%');
    }

    function handleComplete(event) {
        $('#loading-wrapper').hide(0);
        //initialize varibales
        // call main function
        score.init(content.length);

        templateCaller();
    }

    //initialize
    init();

    /*==================================================
    =            Handlers and helpers Block            =
    ==================================================*/
    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


    /*=================================================
     =            general template function            =
     =================================================*/
    function generaltemplate() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        /*generate question no at the beginning of question*/
        score.numberOfQuestions();
        $('.redfrog').attr('src','images/scoreboard/red_flag.png');
        $('.greenfrog').attr('src','images/scoreboard/green_flag.png');
        switch (countNext) {
            case 0:
                shufflehint(data.string.mammals1);
                checkans();
                break;
            case 1:
                shufflehint(data.string.pisces1);
                checkans();
                break;
            case 2:
                shufflehint(data.string.mammals1);
                checkans();
                break;
            case 3:
                shufflehint(data.string.mammals1);
                checkans();
                break;
            case 4:
                shufflehint(data.string.amphibian1);
                checkans();
                break;
            case 5:
                shufflehint(data.string.amphibian1);
                checkans();
                break;
            case 6:
                shufflehint(data.string.aves1);
                checkans();
                break;
            case 7:
                shufflehint(data.string.reptiles1);
                checkans();
                break;
            case 8:
                shufflehint(data.string.mammals1);
                checkans();
                break;
            case 9:
                shufflehint(data.string.aves1);
                checkans();
                break;

            default:
                navigationcontroller(countNext, $total_page);
                break;
        }
    }


    function sound_player(sound_id, navigate) {
        createjs.Sound.stop();
        current_sound = createjs.Sound.play(sound_id);
        current_sound.play();
        current_sound.on('complete', function () {
            navigate ? navigationcontroller(countNext, $total_page) : "";
        });
    }


    function templateCaller() {
        $prevBtn.css('display', 'none');
        $nextBtn.css('display', 'none');
        generaltemplate();
        loadTimelineProgress($total_page, countNext + 1);
    }

    $nextBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        switch (countNext) {
            case 9:
            countNext++;
            score.gotoNext();
            templateCaller();
            $('#activity-page-current-slide').html('10');
            break;
            default:
                countNext++;
                score.gotoNext();
                templateCaller();
                break;
        }
    });

    $refreshBtn.click(function(){
        templateCaller();
    });

    $prevBtn.on('click', function () {
        createjs.Sound.stop();
        clearTimeout(timeoutvar);
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
         previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
    function navigationcontroller(islastpageflag) {
        if (countNext == 0 && $total_page != 1) {
            $nextBtn.show(0);
        }
        else if ($total_page == 1) {
            $nextBtn.css('display', 'none');

            ole.footerNotificationHandler.lessonEndSetNotification();
        }
        else if (countNext > 0 && countNext < $total_page) {

            $nextBtn.show(0);
        }
        else if (countNext == $total_page - 2) {

            $nextBtn.css('display', 'none');
            // if lastpageflag is true
            // ole.footerNotificationHandler.pageEndSetNotification();
        }

    }
    function shufflehint(correctans) {
        // var correctans = $(".question").attr("data-answer");
        var optiondiv = $(".option");
        for (var i = optiondiv.children().length; i >= 0; i--) {
            optiondiv.append(optiondiv.children().eq(Math.random() * i | 0));
        }
        optiondiv.children().removeClass();
        var a = ["commonbtn option1","commonbtn option2","commonbtn option3","commonbtn option4"]
        optiondiv.children().each(function (index) {
            var $this = $(this)
            $this.addClass(a[index]);
            if($this.find('p').text().toString().trim()==correctans.toString().trim()){
                $this.addClass("correct")
            }
        });

    }
    function checkans(){
       var wrongclick = false;
        $(".commonbtn ").on("click",function () {
            if($(this).hasClass("correct") ) {
                $(this).addClass("correctans");
                $(".commonbtn").addClass("avoid-clicks");
                $(this).prepend("<img class='correctWrongImg' src='images/right.png'/>")
                play_correct_incorrect_sound(1);
                !wrongclick?score.update(true):'';
                navigationcontroller();
            }
            else{
                $(this).addClass("wrongans");
                $(this).prepend("<img class='correctWrongImg' src='images/wrong.png'/>")
                play_correct_incorrect_sound(0);
                score.update(false);
                wrongclick = true;
            }
        });
    }

});

var imgpath = $ref + "/images/page2/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "text-type-1",
			textdata: data.string.p2text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	uppertextblock:[
	{
		textclass: "text-type-2",
		textdata: data.string.p2text2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	uppertextblock:[
		{
			textclass: "text-type-6",
			textdata: data.string.p2text9
		},
		{
			textclass: "spriteimage girl-sprite",
		},
		{
			textclass: "spriteimage industry-sprite",
		},
		{
			textclass: "spriteimage fire-sprite",
		},
		{
			textclass: "spriteimage car-sprite",
		},
		{
			textclass: "spriteimage waste-sprite",
		}
],
lowertextblockadditionalclass: "ltbasexdiv",
lowertextblock:[
	{
		textclass: "thetext text-type-2",
	},
	{
		textclass: "spriteimage industry-sprite-second",
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	uppertextblock:[
	{
		textclass: "text-type-4",
		textdata: data.string.p2text7
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "circle",
			imgid : 'circle',
			imgsrc: ""
		},
		{
			imgclass: "cough",
			imgid : 'girl',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	uppertextblock:[
		{
			datahighlightflag: "true",
			datahighlightcustomclass: "blackte",
			textclass: "text-type-5",
			textdata: data.string.p2text8
		},
		{
			textclass: "spriteimage girl-sprite",
		},
		{
			textclass: "spriteimage industry-sprite",
		},
		{
			textclass: "spriteimage fire-sprite",
		},
		{
			textclass: "spriteimage car-sprite",
		},
		{
			textclass: "spriteimage waste-sprite",
		}
],
lowertextblockadditionalclass: "ltbasexdiv",
lowertextblock:[
	{
		textclass: "thetext text-type-2",
	},
	{
		textclass: "spriteimage industry-sprite-second",
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg1", src: imgpath+"air.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"bg01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"bg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"bg04.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"bg03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl", src: imgpath+"coughing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle", src: imgpath+"circle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girl-sprite", src: imgpath+"girl-walking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "industry-sprite", src: imgpath+"industry_smoke.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car-sprite", src: imgpath+"car_smoke.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waste-sprite", src: imgpath+"wastage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire-sprite", src: imgpath+"fire_smoke.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tb-2", src: 'images/textbox/white/tl-1.png', type: createjs.AbstractLoader.IMAGE},
			{id: "tb-1", src: 'images/textbox/white/tr-2.png', type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p2_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p2_s1.ogg"},
			{id: "sound_3-1", src: soundAsset+"p2_s2_0.ogg"},
			{id: "sound_3-2", src: soundAsset+"p2_s2_1.ogg"},
			{id: "sound_3-3", src: soundAsset+"p2_s2_2.ogg"},
			{id: "sound_3-4", src: soundAsset+"p2_s2_3.ogg"},
			{id: "sound_4", src: soundAsset+"p2_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p2_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p2_s2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	var $refreshBtn= $("#activity-page-refresh-btn");
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$(".forextra").hide(0);
		switch(countNext){
			case 0:
			sound_player("sound_1", 1);
			break;
			case 1:
			sound_player("sound_2", 1);
			break;
			case 2:
			sound_player("sound_6", 0);
			//sound_player("sound_1");
			$(".girl-sprite").css({
     		'background-image': 'url("'+ preload.getResult('girl-sprite').src +'")',
   		}).addClass("girl-added-sprite girlwalking-1");
			$( ".girl-sprite" ).animate({
				"left": "4%"
			}, 3000, function(){
				$(this).removeClass("girlwalking-1");
				$(".industry-sprite").addClass("highlightthis");
				// $(".spriteimage").addClass("bnwimg");
				// $(".cover").addClass("bnwimg");
			} );
			$(".industry-sprite").css({
     		'background-image': 'url("'+ preload.getResult('industry-sprite').src +'")',
   		});
			$(".fire-sprite").css({
     		'background-image': 'url("'+ preload.getResult('fire-sprite').src +'")',
   		});
			$(".car-sprite").css({
     		'background-image': 'url("'+ preload.getResult('car-sprite').src +'")',
   		});
			$(".waste-sprite").css({
     		'background-image': 'url("'+ preload.getResult('waste-sprite').src +'")',
   		});

			$(".spriteimage").each(function(){
					var div_ratio =$(this).width()/$(this).parent().width();
					$(this).width(Math.round($(this).parent().width()*div_ratio));
			});
 			 // $(".girl-added-sprite").css({"width":"116px"});
 			 $(".car-sprite").css({"width":"589px"});
			function sprclicked(){
				$(".forextra").show(0);
				$(".forextra > .closeico").attr("src", imgpath + "close.png");
				var div_ratio =$("#imgsprite").width()/$("#imgsprite").parent().width();
				$("#imgsprite").width(Math.round($("#imgsprite").parent().width()*div_ratio));
			}

			$(".industry-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
					sound_player("sound_3-1", 0);
					$(".forextra > .thetext").text(data.string.p2text4).addClass("text-type-3");
					$(".forextra > #imgsprite").removeClass().addClass("industry-sprite-second").css({
						'background-image': 'url("'+ preload.getResult('industry-sprite').src +'")',
					});
					$(".forextra").css({
						'background-image': 'url("'+ preload.getResult('bg4').src +'")',
						'background-size': '100% 100%'
					});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".fire-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
					sound_player("sound_3-2", 0);
					$(".forextra > .thetext").text(data.string.p2text3).addClass("text-type-3");
					$(".forextra > #imgsprite").removeClass().addClass("industry-sprite-second").css({
		     		'background-image': 'url("'+ preload.getResult('fire-sprite').src +'")',
					// 'width':'35%'
		   		});
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg5').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".car-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
					sound_player("sound_3-3", 0);
					$(".forextra > .thetext").text(data.string.p2text5).removeClass("text-type-3").addClass("text-type-10");
					$(".forextra > #imgsprite").removeClass().addClass("car-sprite-second").css({
		     		'background-image': 'url("'+ preload.getResult('car-sprite').src +'")',
					// 'width':"54%"
		   		});
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg6').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".waste-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
					sound_player("sound_3-4", 0);
					$(".forextra > .thetext").text(data.string.p2text6).addClass("text-type-3");
					$(".forextra > #imgsprite").removeClass().addClass("waste-sprite-second").css({
		     		'background-image': 'url("'+ preload.getResult('waste-sprite').src +'")',
		   		});
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg5').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});
			var closeCount = 1;
			$(".closeico").click(function(){
				createjs.Sound.stop();
				$(this).parent().fadeOut();
				switch (closeCount) {
					case 0:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "24%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".industry-sprite").addClass("highlightthis");
						} );
					break;
					case 1:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "24%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".fire-sprite").addClass("highlightthis");
						} );
					break;
					case 2:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "48%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".car-sprite").addClass("highlightthis");
						} );
					break;
					case 3:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "74%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".waste-sprite").addClass("highlightthis");
						} );
					break;
					case 4:
					navigationcontroller();
					break;
					default:

				}
				closeCount++;
			});

			break;
			case 3:
			sound_player("sound_4", 1);
			break;
			case 4:
			sound_player("sound_5", 1);

			$(".industry-sprite").css({
     		'background-image': 'url("'+ preload.getResult('industry-sprite').src +'")',
   		});
			$(".fire-sprite").css({
     		'background-image': 'url("'+ preload.getResult('fire-sprite').src +'")',
   		});
			$(".car-sprite").css({
     		'background-image': 'url("'+ preload.getResult('car-sprite').src +'")',
   		});
			$(".waste-sprite").css({
     		'background-image': 'url("'+ preload.getResult('waste-sprite').src +'")',
   		});
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

var imgpath = $ref + "/images/page3/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "text-type-1",
			textdata: data.string.p3text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "sea",
	uppertextblock:[
		{
			textclass: "text-type-2",
			textdata: data.string.p3text2
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass : "wave-1-anim wave-1",
				imgsrc : '',
				imgid : 'wave-1'
			},{
				imgclass : "wave-2-anim wave-2",
				imgsrc : '',
				imgid : 'wave-2'
			},{
				imgclass : "wave-3-anim wave-3",
				imgsrc : '',
				imgid : 'wave-3'
			}
		]
	}]
},
// slide2
{
	contentnocenteradjust: true,
	contentblockadditionalclass: "sea",
	uppertextblock:[
		{
			textclass: "text-type-2",
			textdata: data.string.p3text3
		},
		{
			textclass: "textbub-1",
			textdata: data.string.p3text4
		},
		{
			textclass: "textbub-2",
			textdata: data.string.p3text5
		},
		{
			textclass: "textbub-3",
			textdata: data.string.p3text6
		},
		{
			textclass: "textbub-4",
			textdata: data.string.p3text7
		},
		{
			textclass: "textbub-5",
			textdata: data.string.p3text8
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass : "wave-1-anim wave-1",
				imgsrc : '',
				imgid : 'wave-1'
			},{
				imgclass : "wave-2-anim wave-2",
				imgsrc : '',
				imgid : 'wave-2'
			},{
				imgclass : "wave-3-anim wave-3",
				imgsrc : '',
				imgid : 'wave-3'
			},
			{
				imgclass: "bubble-1",
				imgid : 'drinking',
				imgsrc: ""
			},
			{
				imgclass: "bubble-3",
				imgid : 'cooking',
				imgsrc: ""
			},
			{
				imgclass: "bubble-2",
				imgid : 'bathing',
				imgsrc: ""
			},
			{
				imgclass: "bubble-5",
				imgid : 'farming',
				imgsrc: ""
			},
			{
				imgclass: "bubble-4",
				imgid : 'washing',
				imgsrc: ""
			}
		]
	}]
},
// slide3
{
	uppertextblock:[
		{
			textclass: "text-type-7",
			textdata: data.string.p2text9
		},
		{
			textclass: "spriteimage girl-sprite",
		},
		{
			textclass: "spriteimage industry-sprite",
		},
		{
			textclass: "spriteimage fire-sprite",
		},
		{
			textclass: "spriteimage car-sprite",
		},


		{
			textclass: "spriteimage waste-sprite",
		},
        {
            textclass: "spriteimage chemical-sprite "
        }

],
lowertextblockadditionalclass: "ltbasexdiv",
lowertextblock:[
	{
		textclass: "thetext text-type-2",
	},
	{
		textclass: "spriteimage industry-sprite-second",
	},
	{
		textclass: "spriteimage car-sprite-second"

	},
	{
	    textclass: "spriteimage chemical-sprite-second"
	},
    {
        textclass: "spriteimage chemical-sprite-third"
    },

],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	uppertextblock:[
	{
		textclass: "text-type-4",
		textdata: data.string.p3text15
	},
	{
		textclass: "spriteimage pesticides-sprite",
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg8',
			imgsrc: ""
		}
	]
}]
},
{
	uppertextblock:[
	{
		textclass: "text-type-6",
		textdata: data.string.p3text16
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "circle",
			imgid : 'circle',
			imgsrc: ""
		},
		{
			imgclass: "cough",
			imgid : 'boy',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	uppertextblock:[
		{
			datahighlightflag: "true",
			datahighlightcustomclass: "blackte",
			textclass: "text-type-5",
			textdata: data.string.p3text17
		},
		{
			textclass: "spriteimage girl-sprite",
		},
		{
			textclass: "spriteimage industry-sprite",
		},
		{
			textclass: "spriteimage fire-sprite",
		},
		{
			textclass: "spriteimage car-sprite",
		},
		{
			textclass: "spriteimage waste-sprite",
		}
],
lowertextblockadditionalclass: "ltbasexdiv",
lowertextblock:[
	{
		textclass: "thetext text-type-2",
	},
	{
		textclass: "spriteimage industry-sprite-second",
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg1", src: imgpath+"waterbg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bathing", src: imgpath+"bathing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "cooking", src: imgpath+"cooking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "drinking", src: imgpath+"drinkwater.png", type: createjs.AbstractLoader.IMAGE},
			{id: "farming", src: imgpath+"farming.png", type: createjs.AbstractLoader.IMAGE},
			{id: "washing", src: imgpath+"washingdish.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-1", src: imgpath+"wave01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-2", src: imgpath+"wave02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wave-3", src: imgpath+"wave03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "circle", src: imgpath+"circle.png", type: createjs.AbstractLoader.IMAGE},
			{id: "boy", src: imgpath+"sick-boy.png", type: createjs.AbstractLoader.IMAGE},


			{id: "girl-sprite", src: imgpath+"girl-walking.png", type: createjs.AbstractLoader.IMAGE},
			{id: "industry-sprite", src: imgpath+"industrywastage.png", type: createjs.AbstractLoader.IMAGE},
			{id: "car-sprite", src: imgpath+"humanwaste.png", type: createjs.AbstractLoader.IMAGE},
			{id: "waste-sprite", src: imgpath+"swim_animation.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire-sprite", src: imgpath+"cow_animation.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chemical-sprite", src: imgpath+"chemical_flow_new.png", type: createjs.AbstractLoader.IMAGE},
            {id: "chemical", src: imgpath+"chemicalflowing.png", type: createjs.AbstractLoader.IMAGE},
			{id: "raining", src: imgpath+"raining.png", type: createjs.AbstractLoader.IMAGE},
            {id: "pesticides-sprite", src: imgpath+"putting_pesticides.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg_diy.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"waterbg0.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"waterbg02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"cow.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"toilet_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"takingbath.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"watering-plant.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg9", src: imgpath+"bg_rain.png", type: createjs.AbstractLoader.IMAGE},
            {id: "bg10", src: imgpath+"toilet_bg.png", type: createjs.AbstractLoader.IMAGE},


            // sounds
			{id: "sound_1", src: soundAsset+"p3_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p3_s1.ogg"},
			{id: "sound_3-0", src: soundAsset+"p3_s2_0.ogg"},
			{id: "sound_3-1", src: soundAsset+"p3_s2_1.ogg"},
			{id: "sound_3-2", src: soundAsset+"p3_s2_2.ogg"},
			{id: "sound_3-3", src: soundAsset+"p3_s2_3.ogg"},
			{id: "sound_3-4", src: soundAsset+"p3_s2_4.ogg"},
			{id: "sound_3-5", src: soundAsset+"p3_s2_5.ogg"},
			{id: "sound_4-0", src: soundAsset+"p3_s3_0.ogg"},
			{id: "sound_4-1", src: soundAsset+"p3_s3_1.ogg"},
			{id: "sound_4-2", src: soundAsset+"p3_s3_2.ogg"},
			{id: "sound_4-3", src: soundAsset+"p3_s3_3.ogg"},
			{id: "sound_4-4", src: soundAsset+"p3_s3_4.ogg"},
			{id: "sound_5", src: soundAsset+"p3_s4.ogg"},
			{id: "sound_6", src: soundAsset+"p3_s5.ogg"},
			{id: "sound_7", src: soundAsset+"p3_s6.ogg"},
			{id: "sound_8", src: soundAsset+"p2_s2.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});55
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {bg7
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		//var div_ratio =$(".spriteimage").width()/$(".spriteimage").parent().width();
		//$(".spriteimage").width(Math.round($(".spriteimage").parent().width()*div_ratio));

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);
		$(".forextra").hide(0);
		switch(countNext){
			case 0:
			sound_player("sound_1", 1);
			break;
			case 1:
			sound_player("sound_2", 1);
			break;
			case 2:
			var audioChain = ["sound_3-0", "sound_3-1", "sound_3-2", "sound_3-3", "sound_3-4", "sound_3-5"];
			chain_player(audioChain, 1);

			break;

			case 3:
      sound_player("sound_8", 0);
			$(".girl-sprite").css({
     		'background-image': 'url("'+ preload.getResult('girl-sprite').src +'")',
   		}).addClass("girl-added-sprite girlwalking-1");
			$( ".girl-sprite" ).animate({
				"left": "2%"
			}, 3000, function(){
				$(this).removeClass("girlwalking-1");
				$(".industry-sprite").addClass("highlightthis");
			} );
			$(".industry-sprite").css({
     		'background-image': 'url("'+ preload.getResult('industry-sprite').src +'")',
   		});
			$(".fire-sprite").css({
     		'background-image': 'url("'+ preload.getResult('fire-sprite').src +'")',
				"width":"343px"
   		});
			$(".car-sprite").css({
     		'background-image': 'url("'+ preload.getResult('car-sprite').src +'")',
   		});
			$(".waste-sprite").css({
     		'background-image': 'url("'+ preload.getResult('waste-sprite').src +'")',
   		});
			$(".chemical-sprite").css({
                'background-image': 'url("'+ preload.getResult('chemical-sprite').src +'")',
            });

			// $(".chemical").css({
             //        'background-image': 'url("'+ preload.getResult('chemical').src +'")',
             //    });
			//


			function sprclicked(){
				$(".forextra").show(0);

				$(".forextra > .closeico").attr("src", imgpath + "close.png");
				var div_ratio =$("#imgsprite").width()/$("#imgsprite").parent().width();
				$("#imgsprite").width(Math.round($("#imgsprite").parent().width()*div_ratio));
			}



			$(".industry-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
                  //  $('.industry-sprite-third').show(0);
                    $('.chemical-sprite-third').hide(0);
                    $('.thetext2').show(0);
                    sound_player("sound_4-0", 0);
					$(".forextra > .thetext").text(data.string.p3text10).addClass("text-type-3");
                    $(".forextra > .thetext2").text(data.string.p3text18).addClass("text-type-11");
					$(".forextra > #imgsprite").removeClass().addClass("industry-sprite-second").css({
						'background-image': 'url("'+ preload.getResult('industry-sprite').src +'")',
					});

                   // $('.text-type-11').hide(0).delay(3000).fadeIn(500);

					$(".forextra").css({
						'background-image': 'url("'+ preload.getResult('bg4').src +'")',
						'background-size': '100% 100%'
					});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".fire-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
                    $('.chemical-sprite-third').hide(0);
                    $('.thetext2').hide(0);
					sound_player("sound_4-1", 0);

					$(".forextra > .thetext").text(data.string.p3text13).addClass("text-type-4");
					$(".forextra > #imgsprite").removeClass().addClass("fire-sprite-second").css({
                        'background-image': 'url("'+ preload.getResult('fire-sprite').src +'")',
						// 'width': '33%'
		   		});
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg5').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".car-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
                    $('.chemical-sprite-third').hide(0);
                    $('.thetext2').hide(0);
					sound_player("sound_4-2", 0);
					$(".forextra > .thetext").text(data.string.p3text12).removeClass("text-type-4").addClass("text-type-3");
					 $(".forextra > #imgsprite").removeClass().addClass("car-sprite-second").css({
		     	 	'background-image': 'url("'+ preload.getResult('car-sprite').src +'")',
                     // 'width': '45%',
		   		 });
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg10').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

			$(".waste-sprite").click(function(){
				if($(this).hasClass("highlightthis")){
                    $('.chemical-sprite-third').hide(0);
                    $('.thetext2').hide(0);
					sound_player("sound_4-3", 0);
					$(".forextra > .thetext").text(data.string.p3text11).removeClass("text-type-4");
					$(".forextra > #imgsprite").removeClass().addClass("waste-sprite-second").css({
		     		'background-image': 'none',
		   		});
					$(".forextra").css({
		     		'background-image': 'url("'+ preload.getResult('bg7').src +'")',
						'background-size': '100% 100%'
		   		});
					$(this).removeClass("highlightthis");
					sprclicked();
				}
			});

               // $('.chemical-sprite-third').show(0);

                $(".chemical-sprite").click(function(){
                    if($(this).hasClass("highlightthis")){

                        $('.chemical-sprite-third').show(0);
                        $('.thetext2').hide(0);
                        sound_player("sound_4-4", 0);
                        $(".forextra > .thetext").text(data.string.p3text14).addClass("text-type-4");
                        $(".forextra > #imgsprite").removeClass().addClass("chemical-sprite-second").css({
                            'background-image': 'url("'+ preload.getResult('chemical').src +'")',
							'width': '25%'
                          //  'background-image': 'url("'+ preload.getResult('raining').src +'")',
                        });
                        $(".forextra > #imgsprite2").removeClass().addClass("chemical-sprite-third").css({
                            'background-image': 'url("'+ preload.getResult('raining').src +'")',
							'width': '25%'
                        });


                        $(".forextra").css({
                            'background-image': 'url("'+ preload.getResult('bg9').src +'")',
                            'background-size': '100% 100%'
                        });
                        $(this).removeClass("highlightthis");
                        sprclicked();
                    }
                });

			var closeCount = 1;
			$(".closeico").click(function(){
				createjs.Sound.stop();
                $('.industry-sprite-third').hide(0);
                $(this).parent().fadeOut();
				switch (closeCount) {
				    //industry
					case 0:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "24%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".industry-sprite").addClass("highlightthis");
						} );
					break;
					// chemical
					case 1:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "8%",
							"top": "48%"

                        }, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".chemical-sprite").addClass("highlightthis");
						} );
					break;
					//cattle
					case 2:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "8%",
							"top": "58%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".fire-sprite").addClass("highlightthis");
						} );
					break;
					//waste
					case 3:
						$( ".girl-sprite" ).addClass("girlwalking-1").animate({
							"left": "47%",
							"top": "65%"
						}, 3000, function(){
							$(this).removeClass("girlwalking-1");
							$(".car-sprite").addClass("highlightthis");
						} );
					break;
                  //bath
                    case 4:
                        $( ".girl-sprite" ).addClass("girlwalking-1").animate({
                            "left": "62%",
                            "top": "72%"
                        }, 3000, function(){
                            $(this).removeClass("girlwalking-1");
                            $(".waste-sprite").addClass("highlightthis");
                        } );
                        break;
					case 5:
					navigationcontroller();
					break;

					default:

				}
				closeCount++;
			});
			break;
			case 4:
			sound_player("sound_5", 1);
			$(".pesticides-sprite").css({
     		'background-image': 'url("'+ preload.getResult('pesticides-sprite').src +'")',
   		});
			break;
			case 5:
			sound_player("sound_6", 1);
			break;
			case 6:
			sound_player("sound_7", 1);
			break;
		}

		$(".spriteimage").each(function(){
				var div_ratio =$(this).width()/$(this).parent().width();
				$(this).width(Math.round($(this).parent().width()*div_ratio));
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	var audioCounter = 0;
	function chain_player(chainaudio, next){
		$(".bubble-"+(audioCounter)).show(0);
		$(".textbub-"+(audioCounter)).show(0);
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(chainaudio[audioCounter]);
		current_sound.play();
		audioCounter++;
		current_sound.on('complete', function(){
			if(audioCounter != 6)
				chain_player(chainaudio, 1);
			else{
				if(next)
				navigationcontroller();
			}
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

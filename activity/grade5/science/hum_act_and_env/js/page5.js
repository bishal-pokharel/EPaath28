var imgpath = $ref + "/images/page5/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "text-type-1",
			textdata: data.string.p5text1
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	uppertextblock:[
	{
		textclass: "text-type-2",
		textdata: data.string.p5text2
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "blck-orange",
			imgid : 'orange',
			imgsrc: ""
		},
		{
			imgclass: "blck-wood",
			imgid : 'wood',
			imgsrc: ""
		},
		{
			imgclass: "blck-fire",
			imgid : 'fire',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	uppertextblock:[
	{
		textclass: "text-type-2",
		textdata: data.string.p5text3
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg9',
			imgsrc: ""
		},
		{
			imgclass: "animal-1",
			imgid : 'deer',
			imgsrc: ""
		},
		{
			imgclass: "animal-2",
			imgid : 'lion',
			imgsrc: ""
		},
		{
			imgclass: "animal-3",
			imgid : 'monkey',
			imgsrc: ""
		},
		{
			imgclass: "animal-4",
			imgid : 'rabit',
			imgsrc: ""
		},
		{
			imgclass: "animal-5",
			imgid : 'turtle',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	uppertextblock:[
	{
		textclass: "text-type-2",
		textdata: data.string.p5text4
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg4',
			imgsrc: ""
		},
		{
			imgclass: "cover loseit2",
			imgid : 'bg5',
			imgsrc: ""
		},
		{
			imgclass: "cover loseit1",
			imgid : 'bg6',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	uppertextblock:[
	{
		textclass: "text-type-3",
		textdata: data.string.p5text5
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg7',
			imgsrc: ""
		}
	]
}]
},
// slide5
{
	uppertextblock:[
	{
		textclass: "text-type-3",
		textdata: data.string.p5text6
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg8',
			imgsrc: ""
		}
	]
}]
},
// slide6
{
	uppertextblock:[
	{
		textclass: "text-type-3",
		textdata: data.string.p5text7
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg10',
			imgsrc: ""
		}
	]
}]
},
// slide7
{
	uppertextblock:[
	{
		datahighlightflag: "true",
		datahighlightcustomclass: "greentxt",
		textclass: "text-type-4",
		textdata: data.string.p5text8
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		},
		{
			imgclass: "animal-6",
			imgid : 'rabit',
			imgsrc: ""
		},
		{
			imgclass: "animal-7",
			imgid : 'turtle',
			imgsrc: ""
		}
	]
}]
},
// slide8
{
	uppertextblock:[
	{
		datahighlightflag: "true",
		datahighlightcustomclass: "blackte",
		textclass: "text-type-10",
		textdata: data.string.p5text9
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "cover",
			imgid : 'bg3',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			//images
			{id: "bg1", src: imgpath+"forest03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"fruits.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg3", src: imgpath+"animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg4", src: imgpath+"forest01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg5", src: imgpath+"forest02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg6", src: imgpath+"forest03.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg7", src: imgpath+"cuttingwood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg8", src: imgpath+"cows.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg9", src: imgpath+"page60_bg.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg10", src: imgpath+"cutting_tree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "orange", src: imgpath+"orange.png", type: createjs.AbstractLoader.IMAGE},
			{id: "wood", src: imgpath+"wood.png", type: createjs.AbstractLoader.IMAGE},
			{id: "fire", src: imgpath+"fire.png", type: createjs.AbstractLoader.IMAGE},
			{id: "deer", src: imgpath+"deer.png", type: createjs.AbstractLoader.IMAGE},
			{id: "lion", src: imgpath+"lion.png", type: createjs.AbstractLoader.IMAGE},
			{id: "monkey", src: imgpath+"monkey.png", type: createjs.AbstractLoader.IMAGE},
			{id: "rabit", src: imgpath+"rabit.png", type: createjs.AbstractLoader.IMAGE},
			{id: "turtle", src: imgpath+"turtle.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_0", src: soundAsset+"p5_s0.ogg"},
			{id: "sound_1", src: soundAsset+"p5_s1.ogg"},
			{id: "sound_2", src: soundAsset+"p5_s2.ogg"},
			{id: "sound_3", src: soundAsset+"p5_s3.ogg"},
			{id: "sound_4", src: soundAsset+"p5_s4.ogg"},
			{id: "sound_5", src: soundAsset+"p5_s5.ogg"},
			{id: "sound_6", src: soundAsset+"p5_s6.ogg"},
			{id: "sound_7", src: soundAsset+"p5_s7.ogg"},
			{id: "sound_8", src: soundAsset+"p5_s8.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);

		switch(countNext){
			case 0:
			sound_player("sound_0", 1);
			break;
			case 1:
			sound_player("sound_1", 1);
			break;
			case 2:
			sound_player("sound_2", 1);
			break;
			case 3:
			sound_player("sound_3", 1);
			break;
			case 4:
			sound_player("sound_4", 1);
			break;
			case 5:
			sound_player("sound_5", 1);
			break;
			case 6:
			sound_player("sound_6", 1);
			break;
			case 7:
			sound_player("sound_7", 1);
			break;
			case 8:
			sound_player("sound_8", 1);
			break;
		}

		$(".spriteimage").each(function(){
				var div_ratio =$(this).width()/$(this).parent().width();
				$(this).width(Math.round($(this).parent().width()*div_ratio));
		});
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

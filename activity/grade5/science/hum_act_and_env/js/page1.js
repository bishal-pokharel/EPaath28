var imgpath = $ref + "/images/page1/";
var soundAsset = $ref+"/sounds/"+$lang+"/";

var content=[
	// slide0
	{
		uppertextblock:[
		{
			textclass: "middletext",
			textdata: data.lesson.chapter
		}
	],
	imageblock:[{
		imagestoshow:[
			{
				imgclass: "cover",
				imgid : 'bg1',
				imgsrc: ""
			}
		]
	}]
},
// slide1
{
	contentblockadditionalclass: "bluebg",
	uppertextblock:[
	{
		textclass: "text-type-1",
		textdata: data.string.p1text1
	}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "sun",
			imgid : 'sun',
			imgsrc: ""
		},
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "tree2",
			imgid : 'tree2',
			imgsrc: ""
		},
		{
			imgclass: "tree3",
			imgid : 'tree3',
			imgsrc: ""
		},
		{
			imgclass: "tree4",
			imgid : 'tree1',
			imgsrc: ""
		},
		{
			imgclass: "stone1",
			imgid : 'stone1',
			imgsrc: ""
		},
		{
			imgclass: "stone2",
			imgid : 'stone2',
			imgsrc: ""
		},
		{
			imgclass: "flower1",
			imgid : 'flower1',
			imgsrc: ""
		},
		{
			imgclass: "flower2",
			imgid : 'flower2',
			imgsrc: ""
		},
		{
			imgclass: "bush1",
			imgid : 'bush1',
			imgsrc: ""
		},
		{
			imgclass: "house1",
			imgid : 'house1',
			imgsrc: ""
		},
		{
			imgclass: "house2",
			imgid : 'house2',
			imgsrc: ""
		},
		{
			imgclass: "house3",
			imgid : 'house3',
			imgsrc: ""
		},
		{
			imgclass: "animals",
			imgid : 'animals',
			imgsrc: ""
		},
		{
			imgclass: "hen",
			imgid : 'hen',
			imgsrc: ""
		},
		{
			imgclass: "duck",
			imgid : 'duck',
			imgsrc: ""
		}
	]
}]
},
// slide2
{
	contentblockadditionalclass: "bluebg",
	uppertextblock:[
		{
			textclass: "text-type-2",
			textdata: data.string.p1text2
		},
		{
			textclass: "netit",
		},
		{
			textclass: "sweep",
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "sun-fixed",
			imgid : 'sun',
			imgsrc: ""
		},
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree2",
			imgid : 'tree2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree3",
			imgid : 'tree3',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree4",
			imgid : 'tree1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone1",
			imgid : 'stone1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone2",
			imgid : 'stone2',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower1",
			imgid : 'flower1',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower2",
			imgid : 'flower2',
			imgsrc: ""
		},
		{
			imgclass: "fixed bush1",
			imgid : 'bush1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house1",
			imgid : 'house1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house2",
			imgid : 'house2',
			imgsrc: ""
		},
		{
			imgclass: "fixed house3",
			imgid : 'house3',
			imgsrc: ""
		},
		{
			imgclass: "fixed animals",
			imgid : 'animals',
			imgsrc: ""
		},
		{
			imgclass: "fixed hen",
			imgid : 'hen',
			imgsrc: ""
		},
		{
			imgclass: "mangirl",
			imgid : 'mangirl',
			imgsrc: ""
		},
		{
			imgclass: "girldog",
			imgid : 'girldog',
			imgsrc: ""
		}
	]
}]
},
// slide3
{
	contentblockadditionalclass: "bluebg bgmove",
	uppertextblock:[
		{
			textclass: "text-type-3",
			textdata: data.string.p1text3
		},
		{
			textclass: "netit",
		},
		{
			textclass: "sweep",
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "sun-fixed",
			imgid : 'sun',
			imgsrc: ""
		},
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree2",
			imgid : 'tree2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree3",
			imgid : 'tree3',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree4",
			imgid : 'tree1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone1",
			imgid : 'stone1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone2",
			imgid : 'stone2',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower1",
			imgid : 'flower1',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower2",
			imgid : 'flower2',
			imgsrc: ""
		},
		{
			imgclass: "fixed bush1",
			imgid : 'bush1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house1",
			imgid : 'house1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house2",
			imgid : 'house2',
			imgsrc: ""
		},
		{
			imgclass: "fixed house3",
			imgid : 'house3',
			imgsrc: ""
		},
		{
			imgclass: "fixed animals",
			imgid : 'animals',
			imgsrc: ""
		},
		{
			imgclass: "fixed hen",
			imgid : 'hen',
			imgsrc: ""
		},
		{
			imgclass: "fixed mangirl",
			imgid : 'mangirl',
			imgsrc: ""
		},
		{
			imgclass: "fixed girldog",
			imgid : 'girldog',
			imgsrc: ""
		}
	]
}]
},
// slide4
{
	contentblockadditionalclass: "bluebg bgmovealready",
	uppertextblock:[
		{
			textclass: "text-type-4",
			textdata: data.string.p1text4
		},
		{
			textclass: "netit",
		},
		{
			textclass: "sweep",
		}
],
imageblock:[{
	imagestoshow:[
		{
			imgclass: "sun-fixed",
			imgid : 'sun',
			imgsrc: ""
		},
		{
			imgclass: "cover",
			imgid : 'bg2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree2",
			imgid : 'tree2',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree3",
			imgid : 'tree3',
			imgsrc: ""
		},
		{
			imgclass: "fixed tree4",
			imgid : 'tree1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone1",
			imgid : 'stone1',
			imgsrc: ""
		},
		{
			imgclass: "fixed stone2",
			imgid : 'stone2',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower1",
			imgid : 'flower1',
			imgsrc: ""
		},
		{
			imgclass: "fixed flower2",
			imgid : 'flower2',
			imgsrc: ""
		},
		{
			imgclass: "fixed bush1",
			imgid : 'bush1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house1",
			imgid : 'house1',
			imgsrc: ""
		},
		{
			imgclass: "fixed house2",
			imgid : 'house2',
			imgsrc: ""
		},
		{
			imgclass: "fixed house3",
			imgid : 'house3',
			imgsrc: ""
		},
		{
			imgclass: "fixed animals",
			imgid : 'animals',
			imgsrc: ""
		},
		{
			imgclass: "fixed hen",
			imgid : 'hen',
			imgsrc: ""
		},
		{
			imgclass: "fixed mangirl",
			imgid : 'mangirl',
			imgsrc: ""
		},
		{
			imgclass: "fixed girldog",
			imgid : 'girldog',
			imgsrc: ""
		}
	]
}]
}
];

$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
    var $refreshBtn= $("#activity-page-refresh-btn");

    var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();

	var preload;
	var timeoutvar = null;
	var current_sound;

	function init() {
		//specify type otherwise it will load assests as XHR
		manifest = [
			// {id: "common-css", src: $ref+"css/common.css", type: createjs.AbstractLoader.CSS},
			// {id: "page1-css", src: $ref+"css/page1.css", type: createjs.AbstractLoader.CSS},
			//   ,
			//images
			{id: "bg1", src: imgpath+"cover-page.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bg2", src: imgpath+"bg_main.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sun", src: imgpath+"sun.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree1", src: imgpath+"tree01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree2", src: imgpath+"twotree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "tree3", src: imgpath+"threefour.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone1", src: imgpath+"stone.png", type: createjs.AbstractLoader.IMAGE},
			{id: "stone2", src: imgpath+"bush.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower1", src: imgpath+"flowerthree.png", type: createjs.AbstractLoader.IMAGE},
			{id: "flower2", src: imgpath+"flowertwo.png", type: createjs.AbstractLoader.IMAGE},
			{id: "bush1", src: imgpath+"bush02.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house1", src: imgpath+"hosue.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house2", src: imgpath+"hosue01.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house3", src: imgpath+"animals-shade.png", type: createjs.AbstractLoader.IMAGE},
			{id: "house4", src: imgpath+"animal-grass.png", type: createjs.AbstractLoader.IMAGE},
			{id: "animals", src: imgpath+"three-animals.png", type: createjs.AbstractLoader.IMAGE},
			{id: "hen", src: imgpath+"hen.png", type: createjs.AbstractLoader.IMAGE},
			{id: "duck", src: imgpath+"duck.png", type: createjs.AbstractLoader.IMAGE},
			{id: "mangirl", src: imgpath+"man-and-girl.png", type: createjs.AbstractLoader.IMAGE},
			{id: "girldog", src: imgpath+"girlwithdog.png", type: createjs.AbstractLoader.IMAGE},
			{id: "sweep-sprite", src: imgpath+"sweeping.png", type: createjs.AbstractLoader.IMAGE},
			{id: "netit-sprite", src: imgpath+"manpicking-garbage.png", type: createjs.AbstractLoader.IMAGE},
			// sounds
			{id: "sound_1", src: soundAsset+"p1_s0.ogg"},
			{id: "sound_2", src: soundAsset+"p1_s1.ogg"},
			{id: "sound_3", src: soundAsset+"p1_s2.ogg"},
			{id: "sound_4", src: soundAsset+"p1_s3.ogg"},
			{id: "sound_5", src: soundAsset+"p1_s4.ogg"},
		];
		preload = new createjs.LoadQueue(false);
		preload.installPlugin(createjs.Sound);//for registering sounds
		preload.on("progress", handleProgress);
		preload.on("complete", handleComplete);
		preload.on("fileload", handleFileLoad);
		preload.loadManifest(manifest, true);
	}
	function handleFileLoad(event) {
		// console.log(event.item);
	}
	function handleProgress(event) {
		$('#loading-text').html(parseInt(event.loaded*100)+'%');
	}
	function handleComplete(event) {
		$('#loading-wrapper').hide(0);
		//initialize varibales
		current_sound = createjs.Sound.play('sound_1');
		current_sound.stop();
		// call main function
		templateCaller();
	}
	//initialize
	init();

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/

function navigationcontroller(islastpageflag){
 typeof islastpageflag === "undefined" ?
 islastpageflag = false :
 typeof islastpageflag != 'boolean'?
 alert("NavigationController : Hi Master, please provide a boolean parameter") :
 null;

 if(countNext == 0 && $total_page!=1){
 	$nextBtn.show(0);
 	$prevBtn.css('display', 'none');
 }
 else if($total_page == 1){
 	$prevBtn.css('display', 'none');
 	$nextBtn.css('display', 'none');

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.lessonEndSetNotification() ;
 }
 else if(countNext > 0 && countNext < $total_page-1){
 	$nextBtn.show(0);
 	$prevBtn.show(0);
 }
 else if(countNext == $total_page-1){
 	$nextBtn.css('display', 'none');
 	$prevBtn.show(0);

 	// if lastpageflag is true
 	islastpageflag ?
 	ole.footerNotificationHandler.lessonEndSetNotification() :
 	ole.footerNotificationHandler.pageEndSetNotification() ;
 }
 }

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	 =            InstructionBlockController            =
	 ==================================================*/

	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}

	/*=====  End of InstructionBlockController  ======*/

	/*=================================================
	 =            general template function            =
	 =================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		put_image(content, countNext);
		put_image2(content, countNext);
		put_speechbox_image(content, countNext);


		$(".sweep").css({
			'background-image': 'url("'+ preload.getResult('sweep-sprite').src +'")',
		});
		$(".netit").css({
			'background-image': 'url("'+ preload.getResult('netit-sprite').src +'")',
		});
		switch(countNext){
			case 0:
			sound_player("sound_1");
			break;
			case 1:
			setTimeout(function(){
				sound_player("sound_2");
			}, 7000);
			break;
			case 2:
			sound_player("sound_3");
			break;
			case 3:
			setTimeout(function(){
				sound_player("sound_4");
			}, 2000);
			break;
			case 4:
			sound_player("sound_5");
			break;
		}
	}
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function sound_player(sound_id, next){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		current_sound.play();
		current_sound.on('complete', function(){
			if(next == null)
			navigationcontroller();
		});
	}

	function sound_player_duo(sound_id, sound_id_2){
		createjs.Sound.stop();
		current_sound = createjs.Sound.play(sound_id);
		//current_sound_2 = createjs.Sound.play(sound_id_2);
		current_sound.play();
		current_sound.on('complete', function(){
			$(".dotext").show(0);
			sound_player(sound_id_2);
		});

	}

	function put_image(content, count){
		if(content[count].hasOwnProperty('imageblock')){
			var imageblock = content[count].imageblock[0];
			if(imageblock.hasOwnProperty('imagestoshow')){
				var imageClass = imageblock.imagestoshow;
				for(var i=0; i<imageClass.length; i++){
					var image_src = preload.getResult(imageClass[i].imgid).src;
					//get list of classes
					var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
					var selector = ('.'+classes_list[classes_list.length-1]);
					$(selector).attr('src', image_src);
				}
			}
		}
	}

	function put_image2(content, count){
		if(content[count].hasOwnProperty('livinnonlivin')){
			var lncontent = content[count].livinnonlivin[0];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
			var lncontent = content[count].livinnonlivin[1];
			if(lncontent.hasOwnProperty('imageblock')){
				var imageblock = lncontent.imageblock[0];
				if(imageblock.hasOwnProperty('imagestoshow')){
					var imageClass = imageblock.imagestoshow;
					for(var i=0; i<imageClass.length; i++){
						var image_src = preload.getResult(imageClass[i].imgid).src;
						//get list of classes
						var classes_list = imageClass[i].imgclass.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]);
						$(selector).attr('src', image_src);
					}
				}
			}
		}
	}
	function put_speechbox_image(content, count){
		if(content[count].hasOwnProperty('speechbox')){
				var speechbox = content[count].speechbox;
				for(var i=0; i<speechbox.length; i++){
						var image_src = preload.getResult(speechbox[i].imgid).src;
						//get list of classes
						var classes_list = speechbox[i].speechbox.match(/\S+/g) || [];
						var selector = ('.'+classes_list[classes_list.length-1]+'>.speechbg');
						// console.log(selector);
						$(selector).attr('src', image_src);
				}
		}
}
	function templateCaller() {
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		if(countNext == 0)
		navigationcontroller();

		generaltemplate();
		loadTimelineProgress($total_page, countNext + 1);
	}

	$nextBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		switch(countNext) {
		default:
			countNext++;
			templateCaller();
			break;
		}
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		createjs.Sound.stop();
		clearTimeout(timeoutvar);
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

});

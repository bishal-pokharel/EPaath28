Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
}

var imgpath = $ref+"/exercise/images/";
var soundAsset = $ref+"/sound/ex/";
var dialog0 = new buzz.sound(soundAsset+"ex.ogg");
var np_dialog0 = new buzz.sound(soundAsset+"np_ex.ogg");

var en_sound_group1 = [dialog0];
var np_sound_group1 = [np_dialog0];

var content=[
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q1,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q2,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q3,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q4,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q5,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q6,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q7,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q8,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q9,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},
{
	//slide 1
	exerciseblock: [
	{
		textdata: data.string.mainq,
		q2data: data.string.q10,

		svgblock: [
		{
			svgname: "svgcontainer",
		},
		]
	}
	]
},

];

/*remove this for non random questions*/
/*content.shufflearray();*/

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	if($lang == 'np'){
		sound_group1 = np_sound_group1;
	}else{
		sound_group1 = en_sound_group1;
	}

	var $total_page = content.length;

	Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var score = 0;
	 var testin = new EggTemplate();

	 testin.init(10);
	 function generalTemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);

	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);

	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".buttonsel").click(function(){
	 		$(this).removeClass('forhover');
	 		// if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){

						if(wrngClicked == false){
							$("#egg"+countNext).attr("src", "images/eggs/" + randImg +".png").removeClass('eggmove');
							score++;
						}
						$(this).css("border","5px solid #B6D7A8");
						$(".buttonsel").css('pointer-events','none');
						$(this).siblings(".corctopt").css("visibility","visible");
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;

						if(countNext != $total_page)
							$nextBtn.show(0);
					}
					else{
						$("#egg"+countNext).attr("src", "images/eggs/egg_wrong.png").removeClass('eggmove');
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#F66E20");
						$(this).siblings(".wrngopt").css("visibility","visible");

						wrngClicked = true;
					}
				// }
			});

	 	var $svgwrapper = $board.children('div.svgwrapper');
	 	var $svgcontainer = $board.children('div.svgcontainer');

	 	var s = Snap(".svgcontainer");
	 	var className;
	 	Snap.load(imgpath+"flower_new.svg", function (f) {
	 		s.append(f);
	 		switch(countNext){
	 			case 0:
				soundplayer(0 ,false);
	 			className = "stigma";
	 			$('.stigma').click(function(){correct(data.string.stigma);});
	 			$('.svgbtn:not(.stigma)').click(function(){incorrect();});
	 			break;

	 			case 1:
	 			className = "corolla";
	 			$('.corolla').click(function(){correct(data.string.corolla);});
	 			$('.svgbtn:not(.corolla)').click(function(){incorrect();});
	 			break;

	 			case 2:
	 			className = "stem";
	 			$('.stem').click(function(){correct(data.string.stem);});
	 			$('.svgbtn:not(.stem)').click(function(){incorrect();});
	 			break;

	 			case 3:
	 			className = "filament";
	 			$('.filament').click(function(){correct(data.string.fila);});
	 			$('.svgbtn:not(.filament)').click(function(){incorrect();});
	 			break;

	 			case 4:
	 			className = "style";
	 			$('.style').click(function(){correct(data.string.style);});
	 			$('.svgbtn:not(.style)').click(function(){incorrect();});
	 			break;

	 			case 5:
	 			className = "calyx";
	 			$('.calyx').click(function(){correct(data.string.calyx);});
	 			$('.svgbtn:not(.calyx)').click(function(){incorrect();});
	 			break;

	 			case 6:
	 			className = "anther";
	 			$('.anther').click(function(){correct(data.string.anther);});
	 			$('.svgbtn:not(.anther)').click(function(){incorrect();});
	 			break;

	 			case 7:
	 			className = "stigma";
	 			$('.stigma').click(function(){correct(data.string.stigma);});
	 			$('.svgbtn:not(.stigma)').click(function(){incorrect();});
	 			break;

	 			case 8:
	 			className = "ovary";
	 			$('.ovary').click(function(){correct(data.string.ovules);});
	 			$('.svgbtn:not(.ovary)').click(function(){incorrect();});
	 			break;

	 			case 9:
	 			className = "ovary";
	 			$('.ovary').click(function(){correct(data.string.ovary);});
	 			$('.svgbtn:not(.ovary)').click(function(){incorrect();});
	 			break;

	 			function incorrect(){
					$(this).css("pointer-events","none");
	 				if(ansClicked == false){
	 					testin.update(false);

	 					play_correct_incorrect_sound(0);
	 				}
	 			}
	 			function correct(cName){
							$(this).css("pointer-events","none");
	 				play_correct_incorrect_sound(1);
                    $('.svgcontainer').css("pointer-events","none");
	 				if(ansClicked == false){
	 					testin.update(true);
	 				}
	 				$(".answerblock").html(cName);
	 				$(".answerblock").show(0);
	 				$nextBtn.show(0);
	 			}

	 		}
	 	});
	 	/*======= SCOREBOARD SECTION ==============*/
	 }

	 function soundplayer(audio, hidebtn){
		 $nextBtn.hide(0);
		 $prevBtn.hide(0);
		 sound_group1[audio].play().bind("ended",function(){
				 if(!hidebtn)
				 navigationcontroller();
		 });
	 }

	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
     	testin.gotoNext();
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});

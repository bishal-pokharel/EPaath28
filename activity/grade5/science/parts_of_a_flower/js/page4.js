var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sound/p4/";
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");

var np_dialog0 = new buzz.sound(soundAsset+"np_1.ogg");
var np_dialog1 = new buzz.sound(soundAsset+"np_2.ogg");
var np_dialog2 = new buzz.sound(soundAsset+"np_3.ogg");

var en_sound_group1 = [dialog0, dialog1, dialog2];
var np_sound_group1 = [np_dialog0, np_dialog1, np_dialog2];
var content=[
{
    //page 1
    contentblockadditionalclass: "contentwithbg2",
    uppertextblock : [
    {
        textclass : "diy",
        textdata : data.string.diy
    },
    ],
},
{
    //page 2
    contentblockadditionalclass: "contentwithbg",
    draggableblock: [
    {
        draggabletextstyle: "draggableitem calyx",
        draggabledata: data.string.calyx
    },
    {
        draggabletextstyle: "draggableitem corolla",
        draggabledata: data.string.corolla
    },
    {
        draggabletextstyle: "draggableitem andro",
        draggabledata: data.string.andro
    },
    {
        draggabletextstyle: "draggableitem gyno",
        draggabledata: data.string.gyno
    },
    {
        draggabletextstyle: "draggableitem fila",
        draggabledata: data.string.fila
    },
    {
        draggabletextstyle: "draggableitem anther",
        draggabledata: data.string.anther
    },
    {
        draggabletextstyle: "draggableitem stigma",
        draggabledata: data.string.stigma
    },
    {
        draggabletextstyle: "draggableitem style",
        draggabledata: data.string.style
    },
    {
        draggabletextstyle: "draggableitem ovary",
        draggabledata: data.string.ovary
    },
    {
        draggabletextstyle: "draggableitem ovules",
        draggabledata: data.string.ovules
    },
    ],
    uppertextblock : [
    {
        textclass : "introductiontop1",
        textdata : data.string.p4text1
    },
    {
        textclass : "stigmatext labelbox",
    },
    {
        textclass : "styletext labelbox",
    },
    {
        textclass : "ovarytext labelbox",
    },
    {
        textclass : "anthertext labelbox",
    },
    {
        textclass : "filamenttext labelbox",
    },
    {
        textclass : "gynotext labelbox",
    },
    {
        textclass : "androtext labelbox",
    },
    {
        textclass : "ovuletext labelbox",
    },
    {
        textclass : "cortext labelbox",
    },
    {
        textclass : "caytext labelbox",
    },
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "diyimg",
            imgsrc : imgpath + "parts/main.png"
        },
        {
            imgclass : "lineone",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "linetwo",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "linethree",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "linefour",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "linefive",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "linesix",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "lineseven",
            imgsrc : imgpath + "line.png"
        },
        {
            imgclass : "arrowone",
            imgsrc : imgpath + "arrowtwo.png"
        },
        {
            imgclass : "arrowtwo",
            imgsrc : imgpath + "arrowtwo.png"
        },
        {
            imgclass : "arrowthree",
            imgsrc : imgpath + "arrowtwo.png"
        },
        ],
    }
    ]
},
{
    //page 3
    contentblockadditionalclass: "contentwithbg",
    uppertextblock : [
    {
        textclass : "diy",
        textdata : data.string.ttt
    },
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "ttt",
            imgsrc :  "images/lokharke/squirrel_what_animated.svg"
        },
        ]
    }
    ]
},
{
    //page 1
    contentblockadditionalclass: "contentwithbg",
    uppertextblock : [
    {
        textclass : "introductiontop1",
        textdata : data.string.p4text2
    },
    ],
    imageblock : [
    {
        imagetoshow : [
        {
            imgclass : "fimg fourimg1",
            imgsrc : imgpath + "page55/flower01.jpg"
        },
        {
            imgclass : "fimg fourimg2",
            imgsrc : imgpath + "page55/mustard01.jpg"
        },
        {
            imgclass : "fimg fourimg3",
            imgsrc : imgpath + "page55/new01.jpg"
        },
        {
            imgclass : "fimg fourimg4",
            imgsrc : imgpath + "page55/new02.jpg"
        },
        {
            imgclass : "fimg fourimg5",
            imgsrc : imgpath + "page55/new03.jpg"
        },
        {
            imgclass : "fimg fourimg6",
            imgsrc : imgpath + "page55/new04.jpg"
        },
        ]
    }
    ]
},
];

$(function(){
    var $board = $(".board");
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;
    var total_page = 0;

    if($lang == 'np'){
        sound_group1 = np_sound_group1;
    }else{
        sound_group1 = en_sound_group1;
    }

    var $total_page = content.length;
    loadTimelineProgress($total_page, countNext + 1);

    /*
        inorder to use the handlebar partials we need to register them
        to their respective handlebar partial pointer first
        */
        Handlebars.registerPartial("draggablecontent", $("#draggablecontent-partial").html());
        Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
        Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
        Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());

    //controls the navigational state of the program



      /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
        // check if the parameter is defined and if a boolean,
        // update islastpageflag accordingly
        typeof islastpageflag === "undefined" ?
        islastpageflag = false :
        typeof islastpageflag != 'boolean'?
        alert("NavigationController : Hi Master, please provide a boolean parameter") :
        null;

        if(countNext == 0 && $total_page!=1){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if($total_page == 1){
            $prevBtn.css('display', 'none');
            $nextBtn.css('display', 'none');

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.lessonEndSetNotification() :
            ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);

            // if lastpageflag is true
            islastpageflag ?
            ole.footerNotificationHandler.lessonEndSetNotification() :
            ole.footerNotificationHandler.lessonEndSetNotification() ;
        }
    }


    function generalTemplate(){
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);

        $board.html(html);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        switch(countNext) {
        case 0:
          play_diy_audio();
          $nextBtn.delay(2000).show(0);
        break;
        case 1:
            sound_group1[0].play();
            var finCount = 0;
            $nextBtn.hide(0);
            $('.draggableitem').draggable({
                containment : ".generalTemplateblock",
                revert : "invalid",
                cursor : "move",
                zIndex: 100000,
            });

            $(".stigmatext").droppable({
                accept: ".stigma",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".caytext").droppable({
                accept: ".calyx",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".cortext").droppable({
                accept: ".corolla",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".androtext").droppable({
                accept: ".andro",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".gynotext").droppable({
                accept: ".gyno",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".filamenttext").droppable({
                accept: ".fila",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".anthertext").droppable({
                accept: ".anther",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".styletext").droppable({
                accept: ".style",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".ovarytext").droppable({
                accept: ".ovary",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            $(".ovuletext").droppable({
                accept: ".ovules",
                drop: function (event, ui){
                    $this = $(this);
                    dropfunc(event, ui, $this);
                }
            });

            function dropfunc(event, ui, $droppedOn){
                var classText = ui.draggable.text();
                ui.draggable.draggable('disable');
                ui.draggable.hide(0);
                /*var droppedOn = $(this);*/
                $droppedOn.html(classText);
                $droppedOn.removeClass('labelbox');
                play_correct_incorrect_sound(1);
                $droppedOn.addClass('labelboxdropped');
                finCount++;
                if(finCount == 10)
                    $nextBtn.show(0);
                    $prevBtn.show(0);
            }
        break;
        case 2:
            sound_group1[2].play();
            $nextBtn.show(0);
            $prevBtn.show(0);
        break;
        case 3:
        soundplayer(1);
        break;
        }
    }


    function templateCaller(){
        //convention is to always hide the prev and next button and show them based
        //on the convention or page index
        $prevBtn.hide(0);
        $nextBtn.hide(0);

        loadTimelineProgress($total_page, countNext + 1);

        //navigationcontroller();

        generalTemplate();
        /*
          for (var i = 0; i < content.length; i++) {
            slides(i);
            $($('.totalsequence')[i]).html(i);
            $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
          "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
          }
          function slides(i){
              $($('.totalsequence')[i]).click(function(){
                countNext = i;
                templateCaller();
              });
            }
        */


    }

    function soundplayer(i){
        $nextBtn.hide(0);
        $prevBtn.hide(0);
        sound_group1[i].play().bind("ended",function(){
            navigationcontroller();
        });
    }

    $nextBtn.on("click", function(){
        countNext++;
        templateCaller();
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
        countNext--;
        templateCaller();
        /* if footerNotificationHandler pageEndSetNotification was called then on click of
        previous slide button hide the footernotification */
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
    });
// document.addEventListener("contentloaded", function(){
    total_page = content.length;
    templateCaller();
    // });

});



 /*===============================================
     =            data highlight function            =
     ===============================================*/
     function texthighlight($highlightinside){
            //check if $highlightinside is provided
            typeof $highlightinside !== "object" ?
            alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
            null ;

            var $alltextpara = $highlightinside.find("*[data-highlight='true']");
            var stylerulename;
            var replaceinstring;
            var texthighlightstarttag;
            var texthighlightendtag   = "</span>";


            if($alltextpara.length > 0){
                $.each($alltextpara, function(index, val) {
                    /*if there is a data-highlightcustomclass attribute defined for the text element
                    use that or else use default 'parsedstring'*/
                    $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
                    (stylerulename = $(this).attr("data-highlightcustomclass")) :
                    (stylerulename = "parsedstring") ;

                    texthighlightstarttag = "<span>";


                    replaceinstring       = $(this).html();
                    replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
                    replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


                    $(this).html(replaceinstring);
                });
            }
        }
        /*=====  End of data highlight function  ======*/

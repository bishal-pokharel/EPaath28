var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sound/p1/";
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");
var dialog5 = new buzz.sound(soundAsset+"6.ogg");
var dialog6 = new buzz.sound(soundAsset+"7.ogg");
var dialog7 = new buzz.sound(soundAsset+"8.ogg");

var np_dialog0 = new buzz.sound(soundAsset+"np_1.ogg");
var np_dialog1 = new buzz.sound(soundAsset+"np_2.ogg");
var np_dialog2 = new buzz.sound(soundAsset+"np_3.ogg");
var np_dialog3 = new buzz.sound(soundAsset+"np_4.ogg");
var np_dialog4 = new buzz.sound(soundAsset+"np_5.ogg");
var np_dialog5 = new buzz.sound(soundAsset+"np_6.ogg");
var np_dialog6 = new buzz.sound(soundAsset+"np_7.ogg");
var np_dialog7 = new buzz.sound(soundAsset+"np_8.ogg");

var en_sound_group1 = [dialog0, dialog1, dialog2, dialog3, dialog4, dialog5, dialog6, dialog7];
var np_sound_group1 = [np_dialog0, np_dialog1, np_dialog2, np_dialog3, np_dialog4, np_dialog5, np_dialog6, np_dialog7];

var content=[
{
	//starting page
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "firsttitle",
		textdata : data.string.title
	}
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "bee",
			imgsrc : imgpath + "bee.gif"
		},
		{
			imgclass : "flo_back",
			imgsrc : imgpath + "flower01.png"
		}
		],
	}
	]
},
{
	//page 1
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop1 cssfadein",
		textdata : data.string.p1text1
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "flo-img",
			imgsrc : imgpath + "flower04.gif"
		},
		],
	}
	]
},
{
	//page 2
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introduction2",
		textdata : data.string.p1text2
	},
	{
		textclass : "introduction3",
		textdata : data.string.p1text3
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "smallflo flo1",
			imgsrc : imgpath + "flower02.gif"
		},
		{
			imgclass : "smallflo flo2",
			imgsrc : imgpath + "flower03.gif"
		},
		{
			imgclass : "smallflo flo3",
			imgsrc : imgpath + "flower05.gif"
		},
		{
			imgclass : "smallflo flo4",
			imgsrc : imgpath + "flower06.gif"
		},
		{
			imgclass : "smallflo flo5",
			imgsrc : imgpath + "flower07.gif"
		},
		{
			imgclass : "smallflo flo6",
			imgsrc : imgpath + "flower08.gif"
		},
		],
	}
	]
},
{
	//page 3
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop1",
		textdata : data.string.p1text4
	},
	{
		textclass : "introductiontop2",
		textdata : data.string.p1text5
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "floleft",
			imgsrc : imgpath + "male-flower.jpg"
		},
		{
			imgclass : "floright",
			imgsrc : imgpath + "jatropintegerherb.jpg"
		},
		],
	}
	]
},
{
	//page 4
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop2",
		textdata : data.string.p1text6
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "floleft",
			imgsrc : imgpath + "pumkin-female-flower-stigma.jpg"
		},
		{
			imgclass : "floright",
			imgsrc : imgpath + "female_flowers.jpg"
		},
		],
	}
	]
},
{
	//page 5
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p1text7
	},
	{
		textclass : "introductiontop2",
		textdata : data.string.p1text8
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "bor-img fourimg1",
			imgsrc : imgpath + "male-flower.jpg"
		},
		{
			imgclass : "bor-img fourimg2",
			imgsrc : imgpath + "jatropintegerherb.jpg"
		},
		{
			imgclass : "bor-img fourimg4",
			imgsrc : imgpath + "pumkin-female-flower-stigma.jpg"
		},
		{
			imgclass : "bor-img fourimg3",
			imgsrc : imgpath + "female_flowers.jpg"
		},
		],
	}
	]
},
{
	//page 6
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p1text9
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "bor-img threeimg1",
			imgsrc : imgpath + "asian-star-lily.jpg"
		},
		{
			imgclass : "bor-img threeimg2",
			imgsrc : imgpath + "hibiscus.jpg"
		},
		{
			imgclass : "bor-img threeimg3",
			imgsrc : imgpath + "tulipa_flower.jpg"
		},
		],
	}
	]
},
{
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "centertext cssfadein",
		textdata : data.string.p1text10
	},
	]
}
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	if($lang == 'np'){
		sound_group1 = np_sound_group1;
	}else{
		sound_group1 = en_sound_group1;
	}

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag) {
      	typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

      	if (countNext == 0 && $total_page != 1) {
      		$nextBtn.delay(800).show(0);
      		$prevBtn.css('display', 'none');
      	} else if ($total_page == 1) {
      		$prevBtn.css('display', 'none');
      		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		vocabcontroller.findwords(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		switch(countNext) {
			case 0:
				soundplayer(0);
			break;
			case 1:
				soundplayer(1);
			break;
			case 2:
				$nextBtn.hide(0);
				setTimeout(function() {   //calls click event after a certain time
					soundplayer(2);
				}, 6000);
			break;
			case 3:
				soundplayer(3);
			break;
			case 4:
				soundplayer(4);
			break;
			case 5:
				soundplayer(5);
			break;
			case 6:
				soundplayer(6);
			break;
			case 7:
				soundplayer(7);
			break;
		}
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	function soundplayer(i){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[i].play().bind("ended",function(){
				navigationcontroller();
		});
	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });
});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

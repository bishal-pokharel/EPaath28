var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sound/p2/";

var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");
var dialog5 = new buzz.sound(soundAsset+"6.ogg");
var dialog6 = new buzz.sound(soundAsset+"7.ogg");
var dialog7 = new buzz.sound(soundAsset+"8.ogg");
var dialog8 = new buzz.sound(soundAsset+"9.ogg");
var dialog9 = new buzz.sound(soundAsset+"10.ogg");
var dialog10 = new buzz.sound(soundAsset+"11.ogg");
var dialog11 = new buzz.sound(soundAsset+"12.ogg");
var dialog12 = new buzz.sound(soundAsset+"13.ogg");
var dialog13 = new buzz.sound(soundAsset+"14.ogg");
var dialog14 = new buzz.sound(soundAsset+"15.ogg");
var dialog15 = new buzz.sound(soundAsset+"16.ogg");
var dialog16 = new buzz.sound(soundAsset+"17.ogg");
var dialog17 = new buzz.sound(soundAsset+"18.ogg");
var dialog18 = new buzz.sound(soundAsset+"19.ogg");
var dialog19 = new buzz.sound(soundAsset+"20.ogg");
var dialog20 = new buzz.sound(soundAsset+"21.ogg");
var dialog21 = new buzz.sound(soundAsset+"22.ogg");
var dialog22 = new buzz.sound(soundAsset+"23.ogg");
var dialog23 = new buzz.sound(soundAsset+"24.ogg");
var dialog24 = new buzz.sound(soundAsset+"25.ogg");
var dialog25 = new buzz.sound(soundAsset+"26.ogg");
var dialog26 = new buzz.sound(soundAsset+"27.ogg");
var dialog27 = new buzz.sound(soundAsset+"28.ogg");
var dialog28 = new buzz.sound(soundAsset+"29.ogg");

var np_dialog0 = new buzz.sound(soundAsset+"np_1.ogg");
var np_dialog1 = new buzz.sound(soundAsset+"np_2.ogg");
var np_dialog2 = new buzz.sound(soundAsset+"np_3.ogg");
var np_dialog3 = new buzz.sound(soundAsset+"np_4.ogg");
var np_dialog4 = new buzz.sound(soundAsset+"np_5.ogg");
var np_dialog5 = new buzz.sound(soundAsset+"np_6.ogg");
var np_dialog6 = new buzz.sound(soundAsset+"np_7.ogg");
var np_dialog7 = new buzz.sound(soundAsset+"np_8.ogg");
var np_dialog8 = new buzz.sound(soundAsset+"np_9.ogg");
var np_dialog9 = new buzz.sound(soundAsset+"np_10.ogg");
var np_dialog10 = new buzz.sound(soundAsset+"np_11.ogg");
var np_dialog11 = new buzz.sound(soundAsset+"np_12.ogg");
var np_dialog12 = new buzz.sound(soundAsset+"np_13.ogg");
var np_dialog13 = new buzz.sound(soundAsset+"np_14.ogg");
var np_dialog14 = new buzz.sound(soundAsset+"np_15.ogg");
var np_dialog15 = new buzz.sound(soundAsset+"np_16.ogg");
var np_dialog16 = new buzz.sound(soundAsset+"np_17.ogg");
var np_dialog17 = new buzz.sound(soundAsset+"np_18.ogg");
var np_dialog18 = new buzz.sound(soundAsset+"np_19.ogg");
var np_dialog19 = new buzz.sound(soundAsset+"np_20.ogg");
var np_dialog20 = new buzz.sound(soundAsset+"np_21.ogg");
var np_dialog21 = new buzz.sound(soundAsset+"np_22.ogg");
var np_dialog22 = new buzz.sound(soundAsset+"np_23.ogg");
var np_dialog23 = new buzz.sound(soundAsset+"np_24.ogg");
var np_dialog24 = new buzz.sound(soundAsset+"np_25.ogg");
var np_dialog25 = new buzz.sound(soundAsset+"np_26.ogg");
var np_dialog26 = new buzz.sound(soundAsset+"np_27.ogg");
var np_dialog27 = new buzz.sound(soundAsset+"np_28.ogg");
var np_dialog28 = new buzz.sound(soundAsset+"np_29.ogg");

var en_sound_group1 = [dialog0, dialog1, dialog2, dialog3, dialog4, dialog5, dialog6, dialog7, dialog8, dialog9, dialog10, dialog11, dialog12, dialog13, dialog14, dialog15, dialog16, dialog17, dialog18, dialog19, dialog20, dialog21, dialog22, dialog23, dialog24, dialog25, dialog26, dialog27, dialog28];
var np_sound_group1 = [np_dialog0, np_dialog1, np_dialog2, np_dialog3, np_dialog4, np_dialog5, np_dialog6, np_dialog7, np_dialog8, np_dialog9, np_dialog10, np_dialog11, np_dialog12, np_dialog13, np_dialog14, np_dialog15, np_dialog16, np_dialog17, np_dialog18, np_dialog19, np_dialog20, np_dialog21, np_dialog22, np_dialog23, np_dialog24, np_dialog25, np_dialog26, np_dialog27, np_dialog28];

var content=[
{
	//page 1
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "centertext cssfadein",
		textdata : data.string.p2text1
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "fourpart1",
			imgsrc : imgpath + "parts/flowerandroecium.png"
		},
		{
			imgclass : "fourpart2",
			imgsrc : imgpath + "parts/flowercalyx.png"
		},
		{
			imgclass : "fourpart3",
			imgsrc : imgpath + "parts/flowercorolla.png"
		},
		{
			imgclass : "fourpart4",
			imgsrc : imgpath + "parts/flowerstem.png"
		},
		],
	}
	]
},
{
	//page 2
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop1",
		textdata : data.string.p2text1
	},
	],
	imageblock : [
	{
		imagetoshow : [

		{
			imgclass : "fourpart3 combineanim",
			imgsrc : imgpath + "parts/flowercorolla.png"
		},
		{
			imgclass : "fourpart1 combineanim",
			imgsrc : imgpath + "parts/flowerandroecium.png"
		},
		{
			imgclass : "fourpart4 combineanim",
			imgsrc : imgpath + "parts/flowerstem.png"
		},
		{
			imgclass : "fourpart2 combineanim",
			imgsrc : imgpath + "parts/flowercalyx.png"
		},
		],
	}
	]
},
{
	//page 3
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop1",
		textdata : data.string.p2text2
	},
	{
		textclass : "label1",
		textdata : data.string.calyx
	},
	{
		textclass : "label2",
		textdata : data.string.corolla
	},
	{
		textclass : "label3",
		textdata : data.string.andro
	},
	{
		textclass : "label4",
		textdata : data.string.gyno
	},
	],
	imageblock : [
	{
		imagetoshow : [

		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowercorolla.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowerandroecium.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowerstem.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowercalyx.png"
		},
		{
			imgclass : "lineone",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linetwo",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linethree",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linefour",
			imgsrc : imgpath + "line.png"
		},
		],
	}
	]
},
{
	//page 4
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		textclass : "introductiontop1",
		textdata : data.string.p2text3
	},
	{
		textclass : "label1",
		textdata : data.string.calyx
	},
	{
		textclass : "label2",
		textdata : data.string.corolla
	},
	{
		textclass : "label3",
		textdata : data.string.andro
	},
	{
		textclass : "label4",
		textdata : data.string.gyno
	},
	],
	imageblock : [
	{
		imagetoshow : [

		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowercorolla.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowerandroecium.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowerstem.png"
		},
		{
			imgclass : "fourpartmov",
			imgsrc : imgpath + "parts/flowercalyx.png"
		},
		{
			imgclass : "lineone",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linetwo",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linethree",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linefour",
			imgsrc : imgpath + "line.png"
		},
		],
	}
	]
},
{
	//page 5
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.calyx
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text5
	},
	{
		textclass : "labelall calyxlabel",
		textdata : data.string.calyx
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linecalyx",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/calyx.png"
		},
		],
	}
	]
},
{
	//page 6
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.corolla
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text6
	},
	{
		textclass : "labelall corollalabel",
		textdata : data.string.corolla
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linecorolla",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/corolla.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/androecium.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 7
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text7
	},
	{
		textclass : "labelall androlabel",
		textdata : data.string.andro
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineandro",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/androecium.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 8
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		textclass : "intro",
		textdata : data.string.p2text8
	},
	{
		textclass : "simplelabel1 cssfadein3",
		textdata : data.string.fila
	},
	{
		textclass : "simplelabel2 cssfadein3",
		textdata : data.string.anther
	},
	{
		textclass : "simplelabel3 cssfadein3",
		textdata : data.string.pollen
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "fila",
			imgsrc : imgpath + "parts/filament.png"
		},
		{
			imgclass : "anther",
			imgsrc : imgpath + "parts/anther.png"
		},
		{
			imgclass : "pollen",
			imgsrc : imgpath + "parts/pollengrain.png"
		},
		],
	}
	]
},
{
	//page 9
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text9
	},
	{
		textclass : "labelall androlabel",
		textdata : data.string.fila
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineandro",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/filament.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 10
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text10
	},
	{
		textclass : "labelall anthlabel",
		textdata : data.string.anther
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineanth",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/anther.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 11
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text11
	},
	{
		textclass : "labelall anthlabel",
		textdata : data.string.pollen
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineanth",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/pollengrain.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 12
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.andro
	},
	],
	uppertextblock : [
	{
		textclass : "intro",
		textdata : data.string.p2text12
	},
	{
		textclass : "simplelabel2_2 cssfadein2",
		textdata : data.string.andro
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "fila",
			imgsrc : imgpath + "parts/filament.png"
		},
		{
			imgclass : "anther2",
			imgsrc : imgpath + "parts/anther.png"
		},
		{
			imgclass : "pollen2",
			imgsrc : imgpath + "parts/pollengrain.png"
		},
		],
	}
	]
},
{
	//page 13
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text13
	},
	{
		textclass : "labelall gynolabel",
		textdata : data.string.gyno
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linegyno",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 14
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		textclass : "intro",
		textdata : data.string.p2text19
	},
	{
		textclass : "labstig",
		textdata : data.string.stigma
	},
	{
		textclass : "labsty",
		textdata : data.string.style
	},
	{
		textclass : "labova",
		textdata : data.string.ovary
	},
	{
		textclass : "labgyn",
		textdata : data.string.gyno
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linestigma",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "lineovary",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "linestyle",
			imgsrc : imgpath + "arrowtwo.png"
		},
		{
			imgclass : "linegy",
			imgsrc : imgpath + "arrowtwo.png"
		},
		{
			imgclass : "ovary",
			imgsrc : imgpath + "parts/ovary.png"
		},
		],
	}
	]
},
{
	//page 15
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text14
	},
	{
		textclass : "labelall stiglabel",
		textdata : data.string.stigma
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linestig",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/top.png"
		},
		],
	}
	]
},
{
	//page 16
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text15
	},
	{
		textclass : "labelall stylabel",
		textdata : data.string.style
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "linesty",
			imgsrc : imgpath + "arrowtwo.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/neck.png"
		},
		],
	}
	]
},
{
	//page 17
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text16
	},
	{
		textclass : "labelall ovalabel",
		textdata : data.string.ovary
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineova",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/tomy.png"
		},
		],
	}
	]
},
{
	//page 18
	contentblockadditionalclass: "contentwithbg",
	headerblock : [
	{
		textclass : "header",
		textdata : data.string.gyno
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "explanation",
		textdata : data.string.p2text17
	},
	{
		textclass : "labelall ovulabel",
		textdata : data.string.ovules
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "lineovu",
			imgsrc : imgpath + "line.png"
		},
		{
			imgclass : "flotwobg",
			imgsrc : imgpath + "parts/main.png"
		},
		{
			imgclass : "flotwohigh",
			imgsrc : imgpath + "parts/inside.png"
		},
		],
	}
	]
},
{
	//page 19
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "centertext cssfadein",
		textdata : data.string.p2text18
	},
	],
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	if($lang == 'np'){
		sound_group1 = np_sound_group1;
	}else{
		sound_group1 = en_sound_group1;
	}

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
	*/
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
     */
 	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$nextBtn.hide(0);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		function soundonclick(sound){
			$('.labelall').css('background','none');
			$('.labelall').css('animation','none');
			$('.explanation').addClass('cssfadein');
			soundplayer(sound);
		}
		switch(countNext) {

			case 0:
				soundplayer(0 ,false);
			break;
			case 1:
			$prevBtn.delay(3000).show(0);
				$nextBtn.delay(3000).show(0);
			break;
			case 2:
				soundplayer(1 ,false);
			break;
			case 3:
				soundplayer(2 ,false);
			break;
			case 4:
				soundplayer(3 ,true);
				$(".linecalyx,.calyxlabel").show(2000);
				$('.labelall').click(function(){
					soundonclick(4);
				});
			break;
			case 5:
				soundplayer(5 ,true);
				$('.labelall').click(function(){
					soundonclick(6);
				});
			break;
			case 6:
				soundplayer(7 ,true);
				$('.labelall').click(function(){
					soundonclick(8);
				});
			break;
			case 7:
				soundplayer(9 ,false);
			break;
			case 8:
				soundplayer(10 ,true);
				$('.labelall').click(function(){
					soundonclick(11);
				});
			break;
			case 9:
				soundplayer(12 ,true);
				$('.labelall').click(function(){
					soundonclick(13);
				});
			break;
			case 10:
				soundplayer(14 ,true);
				$('.labelall').click(function(){
					soundonclick(15);
				});
			break;
			case 11:
				soundplayer(16 ,false);
			break;
			case 12:
				soundplayer(17 ,true);
				$('.labelall').click(function(){
					soundonclick(18);
				});
			break;
			case 13:
				soundplayer(19 ,false);
			break;
			case 14:
				soundplayer(20 ,true);
				$('.labelall').click(function(){
					soundonclick(21);
				});
			break;
			case 15:
				soundplayer(22 ,true);
				$('.labelall').click(function(){
					soundonclick(23);
				});
			break;
			case 16:
				soundplayer(24 ,true);
				$('.labelall').click(function(){
					soundonclick(25);
				});
			break;
			case 17:
				soundplayer(26 ,true);
				$('.labelall').click(function(){
					soundonclick(27);
				});
			break;
			case 18:
				soundplayer(28 ,false);
			break;

		}
	}


	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

	loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	function soundplayer(audio, hidebtn){
		$nextBtn.hide(0);
		$prevBtn.hide(0);
		sound_group1[audio].play().bind("ended",function(){
				if(!hidebtn)
				navigationcontroller();
		});
	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
		total_page = content.length;
		templateCaller();
	// });

});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";


			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span>";


					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

var imgpath = $ref + "/images/page3/";
var soundAsset = $ref+"/sound/p3/";
var dialog0 = new buzz.sound(soundAsset+"1.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3 = new buzz.sound(soundAsset+"4.ogg");
var dialog4 = new buzz.sound(soundAsset+"5.ogg");

var np_dialog0 = new buzz.sound(soundAsset+"np_1.ogg");
var np_dialog1 = new buzz.sound(soundAsset+"np_2.ogg");
var np_dialog2 = new buzz.sound(soundAsset+"np_3.ogg");
var np_dialog3 = new buzz.sound(soundAsset+"np_4.ogg");
var np_dialog4 = new buzz.sound(soundAsset+"np_5.ogg");

var en_sound_group1 = [dialog0, dialog1, dialog2, dialog3, dialog4];
var np_sound_group1 = [np_dialog0, np_dialog1, np_dialog2, np_dialog3, np_dialog4];

var content=[
{
	//page 1
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p3text1
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "bee bee_move",
			imgsrc : imgpath + "beewithoutseed.gif"
		},
		{
			imgclass : "flower1",
			imgsrc : imgpath + "flower02.png"
		},
		{
			imgclass : "flower2",
			imgsrc : imgpath + "flower03.png"
		},
		{
			imgclass : "seed1",
			imgsrc : imgpath + "seed.png"
		},
		{
			imgclass : "seed2",
			imgsrc : imgpath + "seed.png"
		},
		],
	}
	]
},
{
	//page 2
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p3text2
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "bee2 bee_move2",
			imgsrc : imgpath + "beewithseed.gif"
		},
		{
			imgclass : "ova",
			imgsrc : imgpath + "ovaryzoom.png"
		},
		{
			imgclass : "seed3",
			imgsrc : imgpath + "seed.png"
		},
		],
	}
	]
},
{
	//page 3
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p3text3
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "svg1",
			imgsrc : imgpath + "reproduction_plants.svg"
		},
		],
	}
	]
},
{
	//page 4
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p3text4
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "svg1",
			imgsrc : imgpath + "reproduction_plants01.svg"
		},
		],
	}
	]
},
{
	//page 5
	contentblockadditionalclass: "contentwithbg",
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : "introductiontop1",
		textdata : data.string.p3text5
	},
	],
	imageblock : [
	{
		imagetoshow : [
		{
			imgclass : "svg1",
			imgsrc : imgpath + "reproduction_plants02.svg"
		},
		],
	}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;
	
	if($lang == 'np'){
		sound_group1 = np_sound_group1;
	}else{
		sound_group1 = en_sound_group1;
	}

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
		inorder to use the handlebar partials we need to register them 
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		Handlebars.registerPartial("explanationcontent", $("#explanationcontent-partial").html());
		
	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/    
   /**   
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that 
      footernotification is called for continue button to navigate to exercise
      */

  /**   
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if 
        so should be taken out from the templateCaller function
      - If the total page number is 
      */  
 	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.delay(800).show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

		}
	}
	
	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		
		$board.html(html);
		vocabcontroller.findwords(countNext);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	

		switch(countNext) {
			case 0:
				sound_group1[0].play();
				$('.seed1, .seed2').hide(0);
				$nextBtn.hide(0);
				setTimeout(function(){
					$('.bee').attr("src",imgpath + "beewithseed.gif");
				}, 8000);
				setTimeout(function(){
					$('.seed1, .seed2').show(0);
					$nextBtn.delay(5000).show(0);
				}, 15000);
			break;
			case 1:
				sound_group1[1].play();
				$('.seed3').hide(0);
				$nextBtn.hide(0);
				setTimeout(function(){
					$('.seed3').show(0);
					$nextBtn.delay(5000).show(0);
				}, 5000);
			break;
			case 2:
				sound_group1[2].play();
				$nextBtn.hide(0);
				$nextBtn.delay(8000).show(0);
			break;
			case 3:
				sound_group1[3].play();
				$nextBtn.hide(0);
				$nextBtn.delay(8000).show(0);
			break;
			case 4:
				sound_group1[4].play();
				setTimeout(function(){
					ole.footerNotificationHandler.pageEndSetNotification();
				}, 25000);
			break;
		}
	}
	
	function templateCaller(){
		//convention is to always hide the prev and next button and show them based 
		//on the convention or page index		
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		
		loadTimelineProgress($total_page, countNext + 1);
		
		navigationcontroller();
		
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/
   
		
	}
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});
	
	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });
	
});



 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			
			
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;
					
					texthighlightstarttag = "<span>";
					

					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					
					
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


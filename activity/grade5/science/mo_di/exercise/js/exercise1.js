var imgpath = $ref+"/images/";

var content=[

	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.opt1,
						imgsrc: imgpath + 'grass.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.q5o3,
						imgsrc: imgpath + 'diy/pumpkinleaf.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q3o1,
						imgsrc: imgpath + 'mustard-flower.png'
					},
					{
						option_class: "class3",
						optiondata: data.string.q5o2,
						imgsrc: imgpath + 'diy/ladyfingerleaf.jpg'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.onion,
						imgsrc: imgpath + 'diy/onionroot.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt2,
						imgsrc: imgpath + 'eggplants.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q5o4,
						imgsrc: imgpath + 'diy/cabbage.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.mustard,
						imgsrc: imgpath + 'mustard-flower.png'
					}
					],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.rice,
						imgsrc: imgpath + 'rice.png'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt3,
						imgsrc: imgpath + 'diy/radishroot.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.opt2,
						imgsrc: imgpath + 'eggplants.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.opt4,
						imgsrc: imgpath + 'string beans.jpg'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.q6o4,
						imgsrc: imgpath + 'diy/garlicroot.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.q2o2,
						imgsrc: imgpath + 'diy/cauliflower.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q5o2,
						imgsrc: imgpath + 'diy/ladyfingerleaf.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.mustard,
						imgsrc: imgpath + 'mustard-flower.png'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.q1o2,
						imgsrc: imgpath + 'wheat-farming.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt3,
						imgsrc: imgpath + 'diy/radishroot.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q2o2,
						imgsrc: imgpath + 'diy/cauliflower.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q4o3,
						imgsrc: imgpath + 'diy/brockman-turnip2.jpg'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.wheat,
						imgsrc: imgpath + 'wheat-farming.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt4,
						imgsrc: imgpath + 'string beans.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q2o2,
						imgsrc: imgpath + 'diy/cauliflower.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q3o1,
						imgsrc: imgpath + 'mustard-flower.png'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq1,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.rice,
						imgsrc: imgpath + 'rice.png'
					},
					{
						option_class: "class2",
						optiondata: data.string.q2o2,
						imgsrc: imgpath + 'diy/cauliflower.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.mustard,
						imgsrc: imgpath + 'diy/mustardleave.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.q5o3,
						imgsrc: imgpath + 'diy/pumpkinleaf.jpg'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq2,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.q5o2,
						imgsrc: imgpath + 'diy/ladyfingerleaf.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.rice,
						imgsrc: imgpath + 'rice.png'
					},
					{
						option_class: "class3",
						optiondata: data.string.onion,
						imgsrc: imgpath + 'diy/onionroot.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.wheat,
						imgsrc: imgpath + 'wheat-farming.jpg'
					}],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq2,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.q2o2,
						imgsrc: imgpath + 'diy/cauliflower.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.rice,
						imgsrc: imgpath + 'rice.png'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt1,
						imgsrc: imgpath + 'grass.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.onion,
						imgsrc: imgpath + 'diy/onionroot.jpg'
					},],
			}
		]
	},
	//ex1
	{
		exerciseblock: [
			{
				questiondata: data.string.exeq2,

				option: [
					{
						option_class: "class1",
						optiondata: data.string.q4o3,
						imgsrc: imgpath + 'diy/brockman-turnip2.jpg'
					},
					{
						option_class: "class2",
						optiondata: data.string.opt1,
						imgsrc: imgpath + 'grass.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.mustard,
						imgsrc: imgpath + 'diy/millet.jpg'
					},
					{
						option_class: "class3",
						optiondata: data.string.rice,
						imgsrc: imgpath + 'rice.png'
					}],
			}
		]
	},
];

/*remove this for non random questions*/
$(function () {
	var testin = new RhinoTemplate();

	var exercise = new template_exercise_mcq_picture_choices_1(content, testin);
	exercise.create_exercise();
});

var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var dialog0 = new buzz.sound(soundAsset+"p3_s0.ogg");
var content=[
	{
		//slide 0
		uppertextblock : [
		{
			textclass : 'title_diy',
			textdata : data.string.diy
		}
		],
		imageblock: [
		{
			imagetoshow: [
			{
				imgclass: "bg_full",
				imgsrc : imgpath + "a_04.png"
			}

			]
		}]

	},
{
	//slide 1
	uppertextblock : [
	{
		textclass : 'middletext',
		textdata : data.string.p3text1
	},
	{
		textclass : 'congtext',
		textdata : data.string.congmono
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "quesimg",
			imgsrc : imgpath + "germination-beans.png"
		},
		{
			imgclass: "monkey",
			imgsrc : imgpath + "monkey.png"
		}

		]
	}
	],
	quesline:[
	{
		queslineadditionalclass: "ques1",
		questext: data.string.leaf + ":",
		firstopt: data.string.p3o1,
		secondopt: data.string.p3o2,
		secondoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques2",
		questext: data.string.seeds + ":",
		firstopt: data.string.p3o3,
		secondopt: data.string.p3o4,
		secondoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques3",
		questext: data.string.roots + ":",
		firstopt: data.string.p3o5,
		secondopt: data.string.p3o6,
		firstoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques4",
		questext: data.string.plantt + ":",
		firstopt: data.string.p3o7,
		secondopt: data.string.p3o8,
		secondoptclass: "correct"
	}
	]
},
{
	//slide 0
	uppertextblock : [
	{
		textclass : 'middletext',
		textdata : data.string.p3text1
	},
	{
		textclass : 'congtext',
		textdata : data.string.congdico
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "quesimg",
			imgsrc : imgpath + "germination-corn.png"
		},
		{
			imgclass: "monkey",
			imgsrc : imgpath + "monkey.png"
		}

		]
	}
	],
	quesline:[
	{
		queslineadditionalclass: "ques1",
		questext: data.string.leaf + ":",
		firstopt: data.string.p3o1,
		secondopt: data.string.p3o2,
		firstoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques2",
		questext: data.string.seeds + ":",
		firstopt: data.string.p3o3,
		secondopt: data.string.p3o4,
		firstoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques3",
		questext: data.string.roots + ":",
		firstopt: data.string.p3o5,
		secondopt: data.string.p3o6,
		secondoptclass: "correct"
	},
	{
		queslineadditionalclass: "ques4",
		questext: data.string.plantt + ":",
		firstopt: data.string.p3o7,
		secondopt: data.string.p3o8,
		firstoptclass: "correct"
	}
	]
}
];

	$(function(){
		var $board = $(".board");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  	var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;
		var total_page = 0;

		var $total_page = content.length;
		var vocabcontroller =  new Vocabulary();
		vocabcontroller.init();
		loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		$(".quesline").hide(0);
		$(".congtext").hide(0);

		var quesCount = 1;
		questionTrigger();
		if(countNext==0){
	 	 play_diy_audio();
		 navigationcontroller();
	  }
		function questionTrigger(){
			$(".ques"+quesCount).fadeIn(300);
			quesCount++;
		}
		if(countNext==1){
			dialog0.play();
		}
		$(".forhover").click(function(){
				if($(".ques"+quesCount).length == 0){
					setTimeout(function(){
						navigationcontroller();
						$(".congtext").show(0);
					}, 2000);
				}
				$(this).unbind("click");
				$(this).siblings().unbind("click");

				if($(this).hasClass("correct")){
					$(this).addClass("corcolor").removeClass("forhover");
					$(this).parent().addClass("quescomp");
					$(this).siblings(".otext").fadeOut(1500);
					$(this).siblings(".corbtn").fadeIn(1500);
					play_correct_incorrect_sound(1);
				}
				else{
					$(this).addClass("inccolor").removeClass("forhover");
					$(this).parent().addClass("quescomp");
					$(this).siblings(".incbtn").fadeIn().delay(500).fadeOut();
					$(this).siblings(".corbtn").delay(1700).fadeIn();
					$(this).siblings(".correct").addClass("corcolor").removeClass("forhover");
					$(this).delay(1500).fadeOut(1500);
					play_correct_incorrect_sound(0);
				}
				setTimeout(function(){questionTrigger();}, 2000);
			});
		}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
 ===============================================*/
 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

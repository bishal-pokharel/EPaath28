var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var dialog0 = new buzz.sound(soundAsset+"p2_s0.ogg");
var dialog1 = new buzz.sound(soundAsset+"p2_s1.ogg");
var dialog2 = new buzz.sound(soundAsset+"p2_s2.ogg");
var dialog3 = new buzz.sound(soundAsset+"p2_s3.ogg");
var dialog4 = new buzz.sound(soundAsset+"p2_s4.ogg");
var dialog5 = new buzz.sound(soundAsset+"p2_s5.ogg");
var content=[
{
	//slide 0
	leftblockadditionalclass: "s1card1",
	leftsidediv:[{
		textclass: "slideslide",
		textdata: data.string.mono
	}],
	rightblockadditionalclass: "s1card2",
	rightsidediv:[{
		textclass: "slideslide",
		textdata: data.string.dico
	}]
},
{
	//slide 1
	mainleft:[
	{
			maincaption: data.string.monotext,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "cornseed.png",
				lowercaption: data.string.maize
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "wheat-seed.png",
				lowercaption: data.string.wheat
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "rice-seed.png",
				lowercaption: data.string.rice
			}
			]
		}
	],
	mainright:[
	{
			maincaption: data.string.dicotext,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "pumkin-seed.png",
				lowercaption: data.string.pumpkin_seed
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "peanutsseed.png",
				lowercaption: data.string.peanut
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "gram-seed.png",
				lowercaption: data.string.gram
			}
			]
		}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
	uppertextblock : [
	{
		textclass : 'middleheader',
		textdata : data.string.seeds
	}
	],
},
{
	//slide 2
	mainleft:[
	{
			maincaption: data.string.germi1,
			lowerimg:[
			{
				insideimgcontainer: "forone",
				insideimgimg: "onlyone",
				lowerimgsrc: imgpath + "germination-corn.png",
			}
			]
		}
	],
	mainright:[
	{
			maincaption: data.string.germi2,
			lowerimg:[
			{
				insideimgcontainer: "forone",
				insideimgimg: "onlyone",
				lowerimgsrc: imgpath + "germination-beans.png",
			}
			]
		}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
	uppertextblock : [
	{
		textclass : 'middleheader',
		textdata : data.string.germination
	}
	],
},
{
	//slide 3
	mainleft:[
	{
			mainimgsrc: imgpath + "monoleaf.png",
			maincaption: data.string.leaf1,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "rice.png",
				lowercaption: data.string.riceb
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "cornplant.png",
				lowercaption: data.string.maizeb
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "flower.png",
				lowercaption: data.string.orchidb
			}
			]
		}
	],
	mainright:[
	{
			mainimgsrc: imgpath + "dicot-leaf.png",
			maincaption: data.string.leaf2,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "pepal-leaf.png",
				lowercaption: data.string.pipalb
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "beans-leaf.png",
				lowercaption: data.string.beanb
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "mint-leaf.png",
				lowercaption: data.string.mintb
			}
			]
		}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
	uppertextblock : [
	{
		textclass : 'middleheader',
		textdata : data.string.leaf
	}
	],
},
{
	//slide 3
	mainleft:[
	{
			mainimgsrc: imgpath + "monocotplantsroot01.png",
			maincaption: data.string.root1,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "maize-root.png",
				lowercaption: data.string.maize
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "onion.png",
				lowercaption: data.string.onion
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "orchid-root.png",
				lowercaption: data.string.orchidb
			}
			]
		}
	],
	mainright:[
	{
			mainimgsrc: imgpath + "dicoplantroot01.png",
			maincaption: data.string.root2,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "tomato-plant.png",
				lowercaption: data.string.tomato
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "mustard-plant.png",
				lowercaption: data.string.mustard
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "carrot-root.png",
				lowercaption: data.string.carrot
			}
			]
		}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
	uppertextblock : [
	{
		textclass : 'middleheader',
		textdata : data.string.roots
	}
	],
},
{
	//slide 1
	mainleft:[
	{
			maincaption: data.string.flower1,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "white-lily.png",
				lowercaption: data.string.wlily
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "red-lily.png",
				lowercaption: data.string.rlily
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "gardenia.png",
				lowercaption: data.string.gardenia
			}
			]
		}
	],
	mainright:[
	{
			maincaption: data.string.flower2,
			lowerimg:[
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "mustard-flower.png",
				lowercaption: data.string.mustardf
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "hibiscus.png",
				lowercaption: data.string.hibinscus
			},
			{
				insideimgcontainer: "forthree",
				insideimgimg: "threeimages",
				lowerimgsrc: imgpath + "dahlia.png",
				lowercaption: data.string.dahlia
			}
			]
		}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
	uppertextblock : [
	{
		textclass : 'middleheader',
		textdata : data.string.flower
	}
	],
},
{
	//slide 1
	singleline:[
	{
		singlelineadditionalclass: "slfirst",
		lefttext: data.string.rule11,
		righttext: data.string.rule12,
		maintext: data.string.seeds,
		leftimage: imgpath + "corn.png",
		rightimage: imgpath + "beansseed.png",
	},
	{
		lefttext: data.string.rule21,
		righttext: data.string.rule22,
		maintext: data.string.germination,
		leftimage: imgpath + "germination-corn.png",
		rightimage: imgpath + "germination-beans.png",
	},
	{
		lefttext: data.string.rule31,
		righttext: data.string.rule32,
		maintext: data.string.leaf,
		leftimage: imgpath + "monocot-leaf.png",
		rightimage: imgpath + "dicot-leaf.png",
	},
	{
		lefttext: data.string.rule41,
		righttext: data.string.rule42,
		maintext: data.string.roots,
		leftimage: imgpath + "monocotplantsroot01.png",
		rightimage: imgpath + "dicoplantroot01.png",
	},
	{
		lefttext: data.string.rule51,
		righttext: data.string.rule52,
		maintext: data.string.flower,
		leftimage: imgpath + "red-lily.png",
		rightimage: imgpath + "hibiscus.png",
	}
	],
	leftblockadditionalclass: "s2card1",
	leftsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.mono
	}
	],
	rightblockadditionalclass: "s2card2",
	rightsidediv:[
	{
		textclass: "slideslide",
		textdata: data.string.dico
	}
	],
},
];

	$(function(){
		var $board = $(".board");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;
		var total_page = 0;

		var $total_page = content.length;
		var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
		loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);
		switch(countNext){
			case 0:
				soundplayer(dialog0);
			break;
			case 1:
			setTimeout(function(){
				soundplayer(dialog1);
			},2000);
			break;
			case 2:
			setTimeout(function(){
				soundplayer(dialog2);
			},2000);
			break;
			case 3:
			setTimeout(function(){
				soundplayer(dialog3);
			},2000);
			break;
			case 4:
			setTimeout(function(){
				soundplayer(dialog4);
			},2000);
			break;
			case 5:
			setTimeout(function(){
				soundplayer(dialog5);
			},2000);
			case 6:
                navigationcontroller();
			break;
		}
	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		// setTimeout(function(){ navigationcontroller(); }, 3000);


		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
 ===============================================*/
 function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";


		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

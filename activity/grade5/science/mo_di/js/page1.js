var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var dialog0 = new buzz.sound(soundAsset+"p1_s0.ogg");
var dialog1 = new buzz.sound(soundAsset+"p1_s1.ogg");
var dialog2 = new buzz.sound(soundAsset+"p1_s2.ogg");
var dialog3 = new buzz.sound(soundAsset+"p1_s3.ogg");
var dialog4 = new buzz.sound(soundAsset+"p1_s4.ogg");
var dialog6 = new buzz.sound(soundAsset+"p1_s6.ogg");
var dialog7 = new buzz.sound(soundAsset+"p1_s7.ogg");
var content=[
{
	//slide0
	additionalclasscontentblock:'bg_blue',
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "letshighlight",
		textclass : 'lesson-title',
		textdata : data.string.chapname
	}
	],
},
{
	//slide1
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "highlight1",
		datahighlightcustomclass2: "highlight2",
		textclass : 'middletext',
		textdata : data.string.p1text1
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "flowering",
			imgsrc : imgpath + "floweringplant.png"
		},
		{
			imgclass: "nonflowering",
			imgsrc : imgpath + "nonfloweringplant.png"
		}

		]
	}
	]
},
{
	//slide1
	uppertextblock : [
	{
		datahighlightflag: true,
		textclass : 'middletext alreadyhigh1',
		textdata : data.string.p1text2
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "groupimage image1",
			imgsrc : imgpath + "beansplant.png"
		},
		{
			imgclass: "groupimage image2",
			imgsrc : imgpath + "cornplant.png"
		},
		{
			imgclass: "groupimage image3",
			imgsrc : imgpath + "flower.png"
		},
		{
			imgclass: "groupimage image4",
			imgsrc : imgpath + "floweringplant.png"
		},
		{
			imgclass: "groupimage image5",
			imgsrc : imgpath + "floweringplant01.png"
		},
		{
			imgclass: "groupimage image6",
			imgsrc : imgpath + "flowerringplant.png"
		},
		{
			imgclass: "groupimage image7",
			imgsrc : imgpath + "mustard-plant.png"
		},
		{
			imgclass: "groupimage image8",
			imgsrc : imgpath + "onion.png"
		},
		]
	}
	]
},
{
	//slide1
	uppertextblock : [
	{
		textclass : 'middletext alreadyhigh1',
		textdata : data.string.p1text3
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "groupimage image1",
			imgsrc : imgpath + "peas_in_pod.png"
		},
		{
			imgclass: "groupimage image2",
			imgsrc : imgpath + "pumkin.png"
		},
		{
			imgclass: "groupimage image3",
			imgsrc : imgpath + "avocado.png"
		},
		{
			imgclass: "groupimage image4",
			imgsrc : imgpath + "pomegranate.png"
		},
		{
			imgclass: "groupimage image5",
			imgsrc : imgpath + "almond.png"
		},
		{
			imgclass: "groupimage image6",
			imgsrc : imgpath + "foodgrain.png"
		},
		{
			imgclass: "groupimage image7",
			imgsrc : imgpath + "cherry.png"
		},
		{
			imgclass: "groupimage image8",
			imgsrc : imgpath + "soybean-pod.png"
		},
		]
	}
	]
},
{
	//slide1
	uppertextblock : [
		{
			textclass : 'middletext1 alreadyhigh1',
			textdata : data.string.p1text4
		},
	{
		textclass : 'lefttext alreadyhigh1 makeitfade',
		textdata : data.string.p1text5
	},
	{
		textclass : 'righttext alreadyhigh1 makeitfade',
		textdata : data.string.p1text6
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "leftimage makeitfade",
			imgsrc : imgpath + "cornseed.png"
		},
		{
			imgclass: "rightimage makeitfade",
			imgsrc : imgpath + "beansseed.png"
		}
		]
	}
	]
},
{
	//slide1
	uppertextblock : [
	{
		textclass : 'lefttext alreadyhigh1',
		textdata : data.string.p1text5
	},
	{
		textclass : 'righttext alreadyhigh1',
		textdata : data.string.p1text6
	},
	{
		datahighlightflag: true,
		textclass : 'middletext alreadyhigh1',
		textclass : 'lefttext2 makeitfade',
		textdata : data.string.p1text7
	},
	{
		datahighlightflag: true,
		textclass : 'middletext alreadyhigh1',
		textclass : 'righttext2 makeitfade',
		textdata : data.string.p1text8
	}
	],
	imageblock: [
	{
		imagetoshow: [
		{
			imgclass: "leftimage",
			imgsrc : imgpath + "monocotyledons.png"
		},
		{
			imgclass: "rightimage",
			imgsrc : imgpath + "dicotyledons.png"
		}
		]
	}
	]
},
{
	//slide1
	additionalclasscontentblock:'bg_blue',
	uppertextblock : [
	{
		textclass : 'middletext',
		textdata : data.string.p1text9
	}
	]
},
];

$(function(){
	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var total_page = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	loadTimelineProgress($total_page, countNext + 1);

	/*
		inorder to use the handlebar partials we need to register them
		to their respective handlebar partial pointer first
		*/
		Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
		Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	//controls the navigational state of the program



	  /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
      */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templateCaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templateCaller function
      - If the total page number is
      */
      function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	}

	function generalTemplate(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);

		$board.html(html);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		vocabcontroller.findwords(countNext);

		switch(countNext){
			case 0:
				soundplayer(dialog0);
			break;
			case 1:
			soundplayer(dialog1);
			break;
			case 2:
			soundplayer(dialog2);
			break;
			case 3:
			soundplayer(dialog3);
			break;
			case 4:
			soundplayer(dialog4);
			break;
			case 5:
			soundplayer(dialog6);
			break;
			case 6:
			soundplayer(dialog7);
			break;
		}

	}

	function soundplayer(i){
		buzz.all().stop();
		i.play().bind("ended",function(){
				navigationcontroller();
		});
	}

	function templateCaller(){
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);

		loadTimelineProgress($total_page, countNext + 1);

		//navigationcontroller();

		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/

	}

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on("click", function(){
		countNext--;
		templateCaller();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
// document.addEventListener("contentloaded", function(){
	total_page = content.length;
	templateCaller();
	// });

});

 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	 function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightstarttag2;
        var texthighlightstarttag3;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
        	$.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename = $(this).attr("data-highlightcustomclass")) :
            (stylerulename = "parsedstring") ;

            $(this).attr("data-highlightcustomclass2") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename2 = $(this).attr("data-highlightcustomclass2")) :
            (stylerulename2 = "parsedstring2") ;

            $(this).attr("data-highlightcustomclass3") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
            (stylerulename3 = $(this).attr("data-highlightcustomclass3")) :
            (stylerulename3 = "parsedstring3") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            texthighlightstarttag2 = "<span class='"+stylerulename2+"'>";
            texthighlightstarttag3 = "<span class='"+stylerulename3+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/%/g,texthighlightstarttag2);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);


            replaceinstring       = replaceinstring.replace(/!/g,texthighlightstarttag3);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
        });
        }
    }
    /*=====  End of data highlight function  ======*/

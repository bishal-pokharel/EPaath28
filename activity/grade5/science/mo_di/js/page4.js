var imgpath = $ref+"/images/diy/";
var imgpath1 = $ref+"/images/";
var soundAsset = $ref+"/sounds/"+$lang+"/";
var dialog0 = new buzz.sound(soundAsset+"s4_p1.ogg");

Array.prototype.shufflearray = function() {
    var i = this.length,
        j, temp;
    while (--i > 0) {
        j = Math.floor(Math.random() * (i + 1));
        temp = this[j];
        this[j] = this[i];
        this[i] = temp;
    }
    return this;
};

var content=[
	{
		//slide 0
		uppertextblock:[
		{
			textclass: "ole_temp_diytext",
			textdata: data.string.diy
		}
		],
		imageblock:[{
			imagetoshow:[
			{
				imgclass: "bg_full",
				imgsrc: imgpath1 +"a_09.png",
			}
			]
		}
		],
	},
	{
	//slide 1
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q1,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "peanut-seed.png",
				labeltext: data.string.q1o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "wheat-seed.png",
				labeltext: data.string.q1o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cornseed.png",
				labeltext: data.string.q1o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "rice-seed.png",
				labeltext: data.string.q1o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q2,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "sugercane.jpg",
				labeltext: data.string.q2o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cauliflower.jpg",
				labeltext: data.string.q2o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "ladyfingerleaf.jpg",
				labeltext: data.string.q2o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "pumpkinleaf.jpg",
				labeltext: data.string.q2o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q3,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "mustardleave.jpg",
				labeltext: data.string.q3o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "cornleaf.jpg",
				labeltext: data.string.q3o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "bamboo-leaf.jpg",
				labeltext: data.string.q3o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "milletter.jpg",
				labeltext: data.string.q3o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q4,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "cornleaf.jpg",
				labeltext: data.string.q4o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "mustardleave.jpg",
				labeltext: data.string.q4o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "brockman-turnip2.jpg",
				labeltext: data.string.q4o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "youngcarrot.jpg",
				labeltext: data.string.q4o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q5,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "sugercane.jpg",
				labeltext: data.string.q5o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "ladyfingerleaf.jpg",
				labeltext: data.string.q5o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "pumpkinleaf.jpg",
				labeltext: data.string.q5o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cabbage.jpg",
				labeltext: data.string.q5o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q6,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "brockman-turnip2.jpg",
				labeltext: data.string.q6o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "onionroot.jpg",
				labeltext: data.string.q6o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cornroot.jpg",
				labeltext: data.string.q6o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "garlicroot.jpg",
				labeltext: data.string.q6o4

			},
			]
		}
		]
	},
	{
	//slide 0
		uppertextblock:[
		{
			textclass: "mainques",
			textdata: data.string.p4q
		}
		],
		exerciseblock: [
		{
			textdata: data.string.p4q7,

			imageoptions: [
			{
				forshuffle: "options_sign correctOne",
				imgsrc: imgpath + "youngcarrot.jpg",
				labeltext: data.string.q7o1
			},
			{
				forshuffle: "options_sign incorrectOne",
				imgsrc: imgpath + "sugercane.jpg",
				labeltext: data.string.q7o2
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "cornroot.jpg",
				labeltext: data.string.q7o3
			},
			{
				forshuffle: "options_sign incorrectTwo",
				imgsrc: imgpath + "bamboo-leaf.jpg",
				labeltext: data.string.q7o4

			},
			]
		}
		]
	},
	];

/*remove this for non random questions*/
//content.shufflearray();

$(function (){
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	loadTimelineProgress($total_page, countNext + 1);

	function navigationcontroller(islastpageflag){
		typeof islastpageflag === "undefined" ?
		islastpageflag = false :
		typeof islastpageflag != 'boolean'?
		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		null;

		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
			ole.footerNotificationHandler.lessonEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}

	var score = 0;
	 	//eggTemplate.eggMove(countNext);
	/*values in this array is same as the name of images of eggs in image folder*/
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		vocabcontroller.findwords(countNext);

		/*generate question no at the beginning of question*/
		var parent = $(".optionsdiv");
		var divs = parent.children();
		while (divs.length) {
			parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
		}

		$('#egg' + countNext).addClass('eggmove');
		 if(countNext==0){
       play_diy_audio();
     }
     if(countNext==1){
       dialog0.play();
     }
		switch(countNext) {
			default:
				var frac_1_n = 0;
				var frac_1_d = 0;
				var frac_2_n = 0;
				var frac_2_d = 0;
				var new_question1 = 0;
				var new_question2 = 0;
				var rand_multiplier_1 = 1;
				var rand_multiplier_2 = 1;
				var incorrect = false;

				function correct_btn(current_btn){
					$('.options_sign').addClass('disabled');
					$('.hidden_sign').html($(current_btn).html());
					$('.hidden_sign').addClass('fade_in');
					$('.optionscontainer').removeClass('forHover');
					$(current_btn).addClass('option_true');
					navigationcontroller();
                    play_correct_incorrect_sound(true);
				}
				function incorrect_btn(current_btn){
					$(current_btn).addClass('disabled');
					$(current_btn).addClass('option_false');
					incorrect = true;
                    play_correct_incorrect_sound(false);
				}
				break;
		}

		$('.correctOne').click(function(){
			correct_btn(this);
		});

		$('.incorrectOne, .incorrectTwo').click(function(){
			incorrect_btn(this);
		});
	}

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		loadTimelineProgress($total_page, countNext + 1);
		// call navigation controller
		if(countNext == 0)
		navigationcontroller();

		// call the template
		generalTemplate();
  		/*
		  for (var i = 0; i < content.length; i++) {
		    slides(i);
		    $($('.totalsequence')[i]).html(i);
		    $($('.totalsequence')[i]).css({'color':'red',"height": "4.3vmin",
		  "width": "4.3vmin" , "cursor" : "pointer","text-align":"center"});
		  }
		  function slides(i){
		      $($('.totalsequence')[i]).click(function(){
		        countNext = i;
		        templateCaller();
		      });
		    }
		*/


	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();

	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});
/*=====  End of Templates Controller Block  ======*/
});

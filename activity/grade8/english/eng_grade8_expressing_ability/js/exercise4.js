(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var content = [
	{
		question : data.string.q24,
		answers: [
		{ans : data.string.name71},
		{ans : data.string.name72,
		correct : "correct"},
		{ans : data.string.name73},
		]	
	},
	{
		question : data.string.q25,
		answers: [
		{ans : data.string.name74,
			correct : "correct"},
		{ans : data.string.name75},
		{ans : data.string.name76},
		]
	},
	{
		question : data.string.q26,
		answers: [
		{ans : data.string.name77,correct : "correct"},
		{ans : data.string.name78},
		{ans : data.string.name79},
		]
		
	},
	{
		question : data.string.q27,
		answers: [
		{ans : data.string.name80},
		{ans : data.string.name81,
			correct : "correct"},
		{ans : data.string.name82},
		]
	},
	{
		question : data.string.q28,
		answers: [
		{ans : data.string.name83},
		{ans : data.string.name84,correct : "correct"},
		{ans : data.string.name85},
		]
	},
	
	{
		question : data.string.q29,
		answers: [
		{ans : data.string.name86},
		{ans : data.string.name87},
		{ans : data.string.name88,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q30,
		answers: [
		{ans : data.string.name89},
		{ans : data.string.name90,
			correct : "correct"},
		{ans : data.string.name91},
		]
	},
	
	{
		question : data.string.q31,
		answers: [
		{ans : data.string.name92},
		{ans : data.string.name93},
		{ans : data.string.name94,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q32,
		answers: [
		{ans : data.string.name95},
		{ans : data.string.name96,
			correct : "correct"},
		{ans : data.string.name97},
		]
	}
	
	]


		$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').text("Congratulation on finishing your exercise").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

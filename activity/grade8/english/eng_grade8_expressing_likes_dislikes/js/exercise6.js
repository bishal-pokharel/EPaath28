(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q21,
		answers: [
		{ans : data.string.name01,correct : "correct"},
		{ans : data.string.name02},
		{ans : data.string.name03},
		]	
	},
	{
		question : data.string.q22,
		answers: [
		{ans : data.string.name04},
		{ans : data.string.name05},
		{ans : data.string.name06,correct : "correct"},
		]
	},
	{
		question : data.string.q23,
		answers: [
		{ans : data.string.name07,correct : "correct"},
		{ans : data.string.name08},
		{ans : data.string.name09},
		]
		
	},
	{
		question : data.string.q24,
		answers: [
		{ans : data.string.name010},
		{ans : data.string.name011},
		{ans : data.string.name012,correct : "correct"},
		]
	},
	{
		question : data.string.q25,
		answers: [
		{ans : data.string.name013},
		{ans : data.string.name014,correct : "correct"},
		{ans : data.string.name015},
		]
	},
	
	{
		question : data.string.q26,
		answers: [
		{ans : data.string.name016},
		{ans : data.string.name017,correct : "correct"},
		{ans : data.string.name018},
		]
	},
	
	{
		question : data.string.q27,
		answers: [
		{ans : data.string.name019,correct : "correct"},
		{ans : data.string.name020},
		{ans : data.string.name021},
		]
	},
	
	{
		question : data.string.q28,
		answers: [
		{ans : data.string.name022},
		{ans : data.string.name023},
		{ans : data.string.name024,correct : "correct"},
		]
	},
	
	{
		question : data.string.q29,
		answers: [
		{ans : data.string.name025},
		{ans : data.string.name026},
		{ans : data.string.name027,correct : "correct"},
		]
	},

	{
		question : data.string.q30,
		answers: [
		{ans : data.string.name028},
		{ans : data.string.name029,correct : "correct"},
		{ans : data.string.name030},
		]
	},

	{
		question : data.string.q31,
		answers: [
		{ans : data.string.name031},
		{ans : data.string.name032,correct : "correct"},
		{ans : data.string.name033},
		]
	},

	{
		question : data.string.q32,
		answers: [
		{ans : data.string.name034,correct : "correct"},
		{ans : data.string.name035},
		{ans : data.string.name036},
		]
	},

	{
		question : data.string.q33,
		answers: [
		{ans : data.string.name037},
		{ans : data.string.name038},
		{ans : data.string.name039,correct : "correct"},
		]
	},

	{
		question : data.string.q34,
		answers: [
		{ans : data.string.name040},
		{ans : data.string.name041,correct : "correct"},
		{ans : data.string.name042},
		]
	},

	{
		question : data.string.q35,
		answers: [
		{ans : data.string.name043},
		{ans : data.string.name044},
		{ans : data.string.name045,correct : "correct"},
		]
	},

	{
		question : data.string.q36,
		answers: [
		{ans : data.string.name046,correct : "correct"},
		{ans : data.string.name047},
		{ans : data.string.name048},
		]
	},

	{
		question : data.string.q37,
		answers: [
		{ans : data.string.name049},
		{ans : data.string.name050},
		{ans : data.string.name051,correct : "correct"},
		]
	},

	{
		question : data.string.q38,
		answers: [
		{ans : data.string.name052,correct : "correct"},
		{ans : data.string.name053},
		{ans : data.string.name054},
		]
	},
	
	{
		question : data.string.q39,
		answers: [
		{ans : data.string.name055},
		{ans : data.string.name056,correct : "correct"},
		{ans : data.string.name057},
		]
	},

	{
		question : data.string.q40,
		answers: [
		{ans : data.string.name058},
		{ans : data.string.name059},
		{ans : data.string.name060,correct : "correct"},
		]
	},

	{
		question : data.string.q41,
		answers: [
		{ans : data.string.name061,correct : "correct"},
		{ans : data.string.name062},
		{ans : data.string.name063},
		]
	},

	{
		question : data.string.q42,
		answers: [
		{ans : data.string.name064},
		{ans : data.string.name065,correct : "correct"},
		{ans : data.string.name066},
		]
	},

	{
		question : data.string.q43,
		answers: [
		{ans : data.string.name067},
		{ans : data.string.name068},
		{ans : data.string.name069,correct : "correct"},
		]
	},

	{
		question : data.string.q44,
		answers: [
		{ans : data.string.name070,correct : "correct"},
		{ans : data.string.name071},
		{ans : data.string.name072},
		]
	},

	{
		question : data.string.q45,
		answers: [
		{ans : data.string.name073},
		{ans : data.string.name074,correct : "correct"},
		{ans : data.string.name075},
		]
	}
	];


$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<25){
			qA();
		}
		else if (questionCount==25){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			//$('.title').text("Congratulation on finishing your exercise").css({
					// "position": "absolute",
					// "top": "48%",
					// "transform": "translateY(-50%)"
			// });
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
	
	
	$(".closebtn").click(function(){
		ole.activityComplete.finishingcall();
		
	});
	

})(jQuery);
	
	

imageAsset = $ref+"/slide_images/";
soundAsset = $ref+"/sounds/page3/";

var dialog1part1 = new buzz.sound(soundAsset+"1_1.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog1part3 = new buzz.sound(soundAsset+"1_3.ogg");
var dialog1 = [dialog1part1,dialog1part2,dialog1part3];

var dialog2part1 = new buzz.sound(soundAsset+"2_1.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"2_2.ogg");
var dialog2part3 = new buzz.sound(soundAsset+"2_3.ogg");
var dialog2 = [dialog2part1,dialog2part2,dialog2part3];

var dialog3part1 = new buzz.sound(soundAsset+"3_1.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"3_2.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"3_3.ogg");
var dialog3 = [dialog3part1,dialog3part2,dialog3part3];

var dialog4part1 = new buzz.sound(soundAsset+"4_1.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"4_2.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"4_3.ogg");
var dialog4 = [dialog4part1,dialog4part2,dialog4part3];

var dialog5part1 = new buzz.sound(soundAsset+"5_1.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"5_2.ogg");
var dialog5part3 = new buzz.sound(soundAsset+"5_3.ogg");
var dialog5 = [dialog5part1,dialog5part2,dialog5part3];

var dialog6part1 = new buzz.sound(soundAsset+"6_1.ogg");
var dialog6part2 = new buzz.sound(soundAsset+"6_2.ogg");
var dialog6part3 = new buzz.sound(soundAsset+"6_3.ogg");
var dialog6 = [dialog6part1,dialog6part2,dialog6part3];

var dialog7part1 = new buzz.sound(soundAsset+"7_1.ogg");
// var dialog7part2 = new buzz.sound(soundAsset+"7_2.ogg");
var dialog7part3 = new buzz.sound(soundAsset+"7_3.ogg");
var dialog7 = [dialog7part1,dialog7part3];

var dialog8part1 = new buzz.sound(soundAsset+"8_1.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"8_2.ogg");
var dialog8part3 = new buzz.sound(soundAsset+"8_3.ogg");
var dialog8 = [dialog8part1,dialog8part2,dialog8part3];

var dialog9part1 = new buzz.sound(soundAsset+"9_1.ogg");
var dialog9part2 = new buzz.sound(soundAsset+"9_2.ogg");
// var dialog9part3 = new buzz.sound(soundAsset+"9_3.ogg");
var dialog9 = [dialog9part1,dialog9part2];

// var dialog10part1 = new buzz.sound(soundAsset+"10_1.ogg");
// var dialog10part1 = new buzz.sound(soundAsset+"10_1.ogg");
var dialog10part1 = new buzz.sound(soundAsset+"10_2.ogg");
var dialog10 = [dialog10part1];

var dialog11part1 = new buzz.sound(soundAsset+"11_1.ogg");
var dialog11part2 = new buzz.sound(soundAsset+"11_2.ogg");
var dialog11 = [dialog11part1,dialog11part2];

var dialog12part1 = new buzz.sound(soundAsset+"12_1.ogg");
var dialog12part2 = new buzz.sound(soundAsset+"12_2.ogg");
var dialog12 = [dialog12part1,dialog12part2];

var soundcontent = [dialog1, dialog2, dialog3, dialog4, dialog5, dialog6,dialog7,dialog8,dialog9,dialog10,dialog11,dialog12];

var content=[
	{
		bgImgSrc : imageAsset+"4a.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digslide3_1part1,
							data.string.digslide3_1part2,
							data.string.digslide3_1part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"4b.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_2part1,
							data.string.digslide3_2part2,
							data.string.digslide3_2part3
		                   ],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"4c.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
			lineCountDialog : [data.string.digslide3_3part1,
							data.string.digslide3_3part2,
							data.string.digslide3_3part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"4d.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_4part1,
							data.string.digslide3_4part2,
							data.string.digslide3_4part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4e.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_5part1,
							data.string.digslide3_5part2,
							data.string.digslide3_5part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4f.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.digslide3_6part1,
							data.string.digslide3_6part2,
							data.string.digslide3_6part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"4g.jpg",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_7part1,
							// data.string.digslide3_7part2,
							data.string.digslide3_7part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4h.jpg",
		forwhichdialog : "dialog8",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_8part1,
							data.string.digslide3_8part2,
							data.string.digslide3_8part3],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4i.jpg",
		forwhichdialog : "dialog9",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_9part1,
							data.string.digslide3_9part2 ],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4j.jpg",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_10part2,],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"4k.jpg",
		forwhichdialog : "dialog11",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_11part1,
							data.string.digslide3_11part2],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"4a.jpg",
		forwhichdialog : "dialog12",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"5.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"2.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""//hightlight characters according to them speaking
			},
			{
				talkHeadImgSrc :  imageAsset+"4.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.digslide3_12part1,
							data.string.digslide3_12part2],
		speakerImgSrc : $ref+"/slide_images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		questionend: true,
		questiondata: data.string.s3_q1,
		answerdata: data.string.s3_q1_a1
	}
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		
		if(countNext == ($total_page-1)){
			$(".click_for_ref").click(function(){
				$(".answer").show(0);
				$(this).addClass("clicked");
				$prevBtn.show(0);
	     		ole.footerNotificationHandler.pageEndSetNotification();
			});
		} else {
				var $slide = $board.children('div');
				var $dialogcontainer = $slide.children('div.dialogcontainer');
				var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
				var $paralines = $dialogcontainer.children('p').children('span');
				var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
					
					if($.isArray(soundcontent[countNext])){
						playThisDialog = soundcontent[countNext];
						playThisDialog[0].play();
					}
					else if(!$.isArray(soundcontent[countNext])){
						playThisDialog = [soundcontent[countNext]];
						playThisDialog[0].play();
					}
					
		
				$listenAgainButton.on('click',  function() {
					/* Act on the event */
					$paralinestohideonListenAgain.css('display', 'none');
					playThisDialog[0].play();
					$nextBtn.hide(0);
			     	$prevBtn.hide(0);
					$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
				});
		
				/*this function binds appropriate events handlers to the 
				audio as required*/
				function playerbinder(){
						$.each(playThisDialog, function( index, entry ) { 
							if(index < playThisDialog.length-1){
							 	entry.bind('ended', function(){
							 		// alert(index +"sound ended");
							 		$paralines.eq(index+1).fadeIn(400);
							 		playThisDialog[index+1].play();
							 	});
							}
		
							if(index == playThisDialog.length-1){
							 	entry.bind("ended", function() {	
								$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');
		
						     	if(countNext > 0 && countNext < $total_page-1){
						     		$nextBtn.show(0);
						     		$prevBtn.show(0);
						     	}
		
						     	else if(countNext < 1){
						     		$nextBtn.show(0);
						     	}
		
						     	else if(countNext >= $total_page-1){
						     		$prevBtn.show(0);
						     		ole.footerNotificationHandler.pageEndSetNotification();
						     	}
							});
							}
						});
					}
		
					playerbinder();
			}			
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
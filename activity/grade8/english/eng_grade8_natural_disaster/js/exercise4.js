(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2,
		correct : "correct"},
		{ans : data.string.name3},
		]	
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name4,
			correct : "correct"},
		{ans : data.string.name5},
		{ans : data.string.name6},
		]
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name7},
		{ans : data.string.name8,correct : "correct"},
		{ans : data.string.name9},
		]
		
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name10},
		{ans : data.string.name11},
		{ans : data.string.name12,correct : "correct"},
		]
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name13},
		{ans : data.string.name14,correct : "correct"},
		{ans : data.string.name15
			},
		]
	},
	
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name16,correct : "correct"},
		{ans : data.string.name17},
		{ans : data.string.name18},
		]
	},
	
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name19,correct : "correct"},
		{ans : data.string.name20
			},
		{ans : data.string.name21},
		]
	},
	
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name22},
		{ans : data.string.name23},
		{ans : data.string.name24,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name25},
		{ans : data.string.name26},
		{ans : data.string.name27,correct : "correct"},
		]
	},

	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name28},
		{ans : data.string.name29,
			correct : "correct"},
		{ans : data.string.name30},
		]
	},

	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name31},
		{ans : data.string.name32},
		{ans : data.string.name33,correct : "correct"},
		]
	},

	{
		question : data.string.q12,
		answers: [
		{ans : data.string.name34},
		{ans : data.string.name35,
			correct : "correct"},
		{ans : data.string.name36},
		]
	},

	{
		question : data.string.q13,
		answers: [
		{ans : data.string.name37,correct : "correct"},
		{ans : data.string.name38},
		{ans : data.string.name39},
		]
	},

	{
		question : data.string.q14,
		answers: [
		{ans : data.string.name40,correct : "correct"},
		{ans : data.string.name41},
		{ans : data.string.name42},
		]
	},

	{
		question : data.string.q15,
		answers: [
		{ans : data.string.name43},
		{ans : data.string.name44
			},
		{ans : data.string.name45,correct : "correct"},
		]
	},

	{
		question : data.string.q16,
		answers: [
		{ans : data.string.name46,correct : "correct"},
		{ans : data.string.name47
			},
		{ans : data.string.name48},
		]
	}
	
	];


	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<16){
			qA();
		}
		else if (questionCount==16){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// $('.title').text("Congratulation on finishing your exercise").css({
					// "position": "absolute",
					// "top": "48%",
					// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
		}
	});

$(".closebtn").on("click",function(){
		ole.activityComplete.finishingcall();
	});

})(jQuery);

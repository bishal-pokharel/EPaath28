imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page1/";
background_image = $ref+"/images/"

var dialog0 = new buzz.sound(soundAsset+"naturaldisaster.ogg");
var dialog1 = new buzz.sound(soundAsset+"1.1.ogg");
var dialog2 = new buzz.sound(soundAsset+"2.ogg");
var dialog3 = new buzz.sound(soundAsset+"3.ogg");
var dialog4 = new buzz.sound(soundAsset+"4.ogg");
var dialog5 = new buzz.sound(soundAsset+"5.ogg");
var dialog6 = new buzz.sound(soundAsset+"6.ogg");
var dialog7 = new buzz.sound(soundAsset+"7.ogg");
var dialog1main = [dialog1, dialog2, dialog3, dialog4, dialog5, dialog6, dialog7 ];

var dialog2 = new buzz.sound(soundAsset+"8.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"10.ogg");
var dialog2part3 = new buzz.sound(soundAsset+"11.ogg");
var dialog2main = [dialog2, dialog2part1, dialog2part2, dialog2part3];

var dialog3 = new buzz.sound(soundAsset+"12.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"13.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"14.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"15.ogg");
var dialog3part4 = new buzz.sound(soundAsset+"16.ogg");
var dialog3part5 = new buzz.sound(soundAsset+"17.ogg");
var dialog3main = [dialog3, dialog3part1, dialog3part2, dialog3part3, dialog3part4, dialog3part5];

var dialog4 = new buzz.sound(soundAsset+"18.ogg");
var dialog4part1 = new buzz.sound(soundAsset+"19.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"20.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"21.ogg");
var dialog4part4 = new buzz.sound(soundAsset+"22.ogg");
var dialog4part5 = new buzz.sound(soundAsset+"23.ogg");
var dialog4main = [dialog4, dialog4part1, dialog4part2, dialog4part3, dialog4part4, dialog4part5];

var dialog5 = new buzz.sound(soundAsset+"24.ogg");
var dialog5part1 = new buzz.sound(soundAsset+"25.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"26.ogg");
var dialog5part3 = new buzz.sound(soundAsset+"27.ogg");
var dialog5main = [dialog5, dialog5part1, dialog5part2, dialog5part3];

var dialog6 = new buzz.sound(soundAsset+"28.ogg");
var dialog6part1 = new buzz.sound(soundAsset+"29.ogg");
var dialog6part2 = new buzz.sound(soundAsset+"30.ogg");
var dialog6part3 = new buzz.sound(soundAsset+"31.ogg");
var dialog6main = [dialog6, dialog6part1, dialog6part2, dialog6part3];


var dialog7 = new buzz.sound(soundAsset+"32.ogg");
var dialog7part1 = new buzz.sound(soundAsset+"33.ogg");
var dialog7part2 = new buzz.sound(soundAsset+"34.ogg");
var dialog7part3 = new buzz.sound(soundAsset+"35.ogg");
var dialog7main = [dialog7, dialog7part1, dialog7part2, dialog7part3 ];

var dialog8 = new buzz.sound(soundAsset+"36.ogg");
var dialog8part1 = new buzz.sound(soundAsset+"37.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"38.ogg");
var dialog8part3 = new buzz.sound(soundAsset+"39.ogg");
var dialog8part4 = new buzz.sound(soundAsset+"40.ogg");
var dialog8main = [dialog8, dialog8part1, dialog8part2, dialog8part3, dialog8part4];

var dialog9 = new buzz.sound(soundAsset+"41.ogg");
// var dialog9part1 = new buzz.sound(soundAsset+"42.ogg");
var dialog9part2 = new buzz.sound(soundAsset+"43.1.ogg");
var dialog9main = [dialog9, dialog9part2];





var soundcontent = [dialog0,dialog1main, dialog2main, dialog3main, dialog4main, dialog5main, dialog6main, dialog7main, dialog8main, dialog9main];


var content=[
    {
        bgImgSrc : imageAsset+"natural_disaster.png",
        imageid : "bg_image1",
        dialog_id : "dialog_1main",
        forwhichdialog : "dialog1main",
        lineCountDialog : [data.string.p1dialog0
        ],
        speakerImgSrc : $ref+"/images/speakers.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"reeta.png",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		forwhichdialog : "dialog1main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p1dialog1,
							data.string.p1dialog2,
							data.string.p1dialog3,
							data.string.p1dialog4,
							data.string.p1dialog5,
							data.string.p1dialog6,
							data.string.p1dialog7,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"ls.png",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		forwhichdialog : "dialog2main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p2dialog2,
							data.string.p2dialog2part1,
							data.string.p2dialog2part2,
							data.string.p2dialog2part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
		
		
	},
	
	{
		bgImgSrc : imageAsset+"cry.png",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p3dialog3,
							data.string.p3dialog3part1,
							data.string.p3dialog3part2,
							data.string.p3dialog3part3,
							data.string.p3dialog3part4,
							data.string.p3dialog3part5,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"rescuee.png",
		imageid : "bg_image4",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog4main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p4dialog4,
							data.string.p4dialog4part1,
							data.string.p4dialog4part2,
							data.string.p4dialog4part3,
							data.string.p4dialog4part4,
							data.string.p4dialog4part5,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"volunteers.png",
		imageid : "bg_image5",
		dialog_id : "dialog_5main",
		forwhichdialog : "dialog5main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p5dialog5,
							data.string.p5dialog5part1,
							data.string.p5dialog5part2,
							data.string.p5dialog5part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"schools.png",
		imageid : "bg_image6",
		dialog_id : "dialog_6main",
		forwhichdialog : "dialog6main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog6,
							data.string.p6dialog6part1,
							data.string.p6dialog6part2,
							data.string.p6dialog6part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
		
	},
	
	{
		bgImgSrc : imageAsset+"deff.png",
		imageid : "bg_image7",
		dialog_id : "dialog_7main",
		forwhichdialog : "dialog7main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p7dialog7,
							data.string.p7dialog7part1,
							data.string.p7dialog7part2,
							data.string.p7dialog7part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"eq.png",
		imageid : "bg_image8",
		dialog_id : "dialog_8main",
		forwhichdialog : "dialog8main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],

		lineCountDialog : [data.string.p8dialog8,
							data.string.p8dialog8part1,
							data.string.p8dialog8part2,
							data.string.p8dialog8part3,
							data.string.p8dialog8part4,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"lastt.png",
		imageid : "bg_image9",
		dialog_id : "dialog_9main",
		forwhichdialog : "dialog9main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p9dialog9,
							// data.string.p9dialog9part1,
							data.string.p9dialog9part2,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	

	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 10;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==4){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==5){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page1/";
background_image = $ref+"/images/"



var dialog10 = new buzz.sound(soundAsset+"44.ogg");
var dialog10part1 = new buzz.sound(soundAsset+"45.ogg");
var dialog10part2 = new buzz.sound(soundAsset+"46.ogg");
var dialog10part3 = new buzz.sound(soundAsset+"47.ogg");
var dialog10main = [dialog10, dialog10part1, dialog10part2, dialog10part3];

var dialog11 = new buzz.sound(soundAsset+"48.ogg");
var dialog11part1 = new buzz.sound(soundAsset+"49.ogg");
var dialog11part2 = new buzz.sound(soundAsset+"50.ogg");
var dialog11part3 = new buzz.sound(soundAsset+"51.ogg");
var dialog11part4 = new buzz.sound(soundAsset+"52.ogg");
var dialog11part5 = new buzz.sound(soundAsset+"53.ogg");
var dialog11main = [dialog11, dialog11part1, dialog11part2, dialog11part3, dialog11part4, dialog11part5];

var dialog12 = new buzz.sound(soundAsset+"54.ogg");
var dialog12part1 = new buzz.sound(soundAsset+"55.ogg");
var dialog12part2 = new buzz.sound(soundAsset+"56.ogg");
var dialog12part3 = new buzz.sound(soundAsset+"57.ogg");
var dialog12part4 = new buzz.sound(soundAsset+"58.ogg");
var dialog12main = [dialog12, dialog12part1, dialog12part2, dialog12part3, dialog12part4];

var dialog13 = new buzz.sound(soundAsset+"59.ogg");
var dialog13part1 = new buzz.sound(soundAsset+"60.ogg");
var dialog13part2 = new buzz.sound(soundAsset+"61.ogg");
var dialog13part3 = new buzz.sound(soundAsset+"62.ogg");
var dialog13main = [dialog13, dialog13part1, dialog13part2, dialog13part3];

var dialog14 = new buzz.sound(soundAsset+"63.ogg");
var dialog14part1 = new buzz.sound(soundAsset+"64.ogg");
var dialog14part2 = new buzz.sound(soundAsset+"65.ogg");
var dialog14main = [dialog14, dialog14part1, dialog14part2];

var dialog15 = new buzz.sound(soundAsset+"66.ogg");
var dialog15part1 = new buzz.sound(soundAsset+"67.ogg");
var dialog15part2 = new buzz.sound(soundAsset+"68.ogg");
var dialog15part3 = new buzz.sound(soundAsset+"69.ogg");
var dialog15part4 = new buzz.sound(soundAsset+"70.ogg");
var dialog15part5 = new buzz.sound(soundAsset+"71.ogg");
var dialog15part6 = new buzz.sound(soundAsset+"72.ogg");
var dialog15main = [dialog15, dialog15part1, dialog15part2, dialog15part3, dialog15part4, dialog15part5, dialog15part6];



var soundcontent = [dialog10main, dialog11main, dialog12main, dialog13main, dialog14main, dialog15main];


var content=[
	
	
	{
		bgImgSrc : imageAsset+"gudiyya.png",
		imageid : "bg_image10",
		dialog_id : "dialog_10main",
		forwhichdialog : "dialog10main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p10dialog10,
							data.string.p10dialog10part1,
							data.string.p10dialog10part2,
							data.string.p10dialog10part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	{
		bgImgSrc : imageAsset+"floods.png",
		imageid : "bg_image11",
		dialog_id : "dialog_11main",
		forwhichdialog : "dialog11main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p11dialog11,
							data.string.p11dialog11part1,
							data.string.p11dialog11part2,
							data.string.p11dialog11part3,
							data.string.p11dialog11part4,
							data.string.p11dialog11part5,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	{
		bgImgSrc : imageAsset+"victim.png",
		imageid : "bg_image12",
		dialog_id : "dialog_12main",
		forwhichdialog : "dialog12main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p12dialog12,
							data.string.p12dialog12part1,
							data.string.p12dialog12part2,
							data.string.p12dialog12part3,
							data.string.p12dialog12part4,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	{
		bgImgSrc : imageAsset+"child.png",
		imageid : "bg_image13",
		dialog_id : "dialog_13main",
		forwhichdialog : "dialog13main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p13dialog13,
							data.string.p13dialog13part1,
							data.string.p13dialog13part2,
							data.string.p13dialog13part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"sisters.png",
		imageid : "bg_image14",
		dialog_id : "dialog_14main",
		forwhichdialog : "dialog14main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p14dialog14,
							data.string.p14dialog14part1,
							data.string.p14dialog14part2,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"house.png",
		imageid : "bg_image15",
		dialog_id : "dialog_15main",
		forwhichdialog : "dialog15main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p15dialog15,
							data.string.p15dialog15part1,
							data.string.p15dialog15part2,
							data.string.p15dialog15part3,
							data.string.p15dialog15part4,
							data.string.p15dialog15part5,
							data.string.p15dialog15part6,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==3){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==4){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




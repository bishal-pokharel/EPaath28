var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "p5_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p5_s1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p5_s2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p5_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p5_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p5_s5.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p5_s6.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p5_s7.ogg"));
var sound_8 = new buzz.sound((soundAsset + "p5_s8.ogg"));
var sound_9 = new buzz.sound((soundAsset + "p5_s9.ogg"));
var sound_10 = new buzz.sound((soundAsset + "p5_s10.ogg"));
var sound_11 = new buzz.sound((soundAsset + "p5_s11.ogg"));
var sound_12 = new buzz.sound((soundAsset + "p5_s12.ogg"));
var sound_13 = new buzz.sound((soundAsset + "p5_s13.ogg"));
var sound_14 = new buzz.sound((soundAsset + "p5_s14.ogg"));
var sound_15 = new buzz.sound((soundAsset + "p5_s15.ogg"));
var sound_16 = new buzz.sound((soundAsset + "p5_s16.ogg"));
var sound_17 = new buzz.sound((soundAsset + "p5_s17.ogg"));

var sounds=[ sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7, sound_8, sound_9, sound_10, sound_11, sound_12, sound_13, sound_14, sound_15, sound_16, sound_17];


var content=[
 {
  	//page 0
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03-bag.png"
  		},{
			imgclass: "copy_table_1",
			imgsrc : imgpath+"notebook-06.png"
  		},{
			imgclass: "one_girl_table",
			imgsrc : imgpath+"Niti-15.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s1
  		}]
  	}]
  },{
  	//page 1
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-16-bag.png"
  		},{
			imgclass: "copy_table_1",
			imgsrc : imgpath+"notebook-06.png"
  		},{
			imgclass: "one_girl_table",
			imgsrc : imgpath+"Niti-03.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s2
  		}]
  	}]
  },{
  	//page 2
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03-bag.png"
  		},{
			imgclass: "copy_table_1",
			imgsrc : imgpath+"notebook-06.png"
  		},{
			imgclass: "one_girl_table",
			imgsrc : imgpath+"Niti-15.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s3
  		}]
  	}]
  },{
  	//page 3
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_1",
  			imgsrc : imgpath+"sagar-14.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s4
  		}]
  	}]
  },{
  	//page 4
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "orange_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-04.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s5
  		}]
  	}]
  },{
  	//page 5
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "purple_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_1",
  			imgsrc : imgpath+"sagar-14.png"
  		},{
  			imgclass: "fourkids_table_2",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer3_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s6
  		}]
  	}]
  },{
  	//page 6
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "purple_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-14.png"
  		},{
  			imgclass: "fourkids_table_3",
  			imgsrc : imgpath+"Niti-03.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		},{
  			imgclass: "bag",
  			imgsrc : imgpath+"bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer4_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s7
  		}]
  	}]
  },{
  	//page 7
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "purple_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "sagar1 mirror_rotate",
  			imgsrc : imgpath+"sagar-03-bag.png"
  		},{
  			imgclass: "fourkids_table_3",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s8
  		}]
  	}]
  },{
  	//page 8
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s9
  		}]
  	}]
  },{
  	//page 9
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-50-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-04.png"
  		},{
  			imgclass: "teacher_boy_girl_2 ",
  			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer4",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s10
  		}]
  	}]
  },{
  	//page 10
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"rumi-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2",
  			imgsrc : imgpath+"sagar-14-bag.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer5_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s11
  		}]
  	}]
  },{
  	//page 11
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4",
  			imgsrc : imgpath+"prem-13.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer6_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s12
  		}]
  	}]
  },{
  	//page 12
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "fourkids_table_4",
  			imgsrc : imgpath+"prem-13.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer7_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s13
  		}]
  	}]
  },{
  	//page 13
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "table",
  			imgsrc : imgpath+"table-05.png"
  		},{
  			imgclass: "fourkids_table_3 mirror_rotate",
  			imgsrc : imgpath+"Niti-03.png"
  		},{
  			imgclass: "fourkids_table_4 mirror_rotate",
  			imgsrc : imgpath+"prem-15.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer5",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s14
  		}]
  	}]
  },{
  	//page 14
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"Niti-03.png"
  		},{
  			imgclass: "teacher_boy_girl_2 mirror_rotate",
  			imgsrc : imgpath+"prem-04.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer5_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s15
  		}]
  	}]
  },{
  	//page 15
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-50-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"Niti-15.png"
  		},{
  			imgclass: "teacher_boy_girl_2 mirror_rotate",
  			imgsrc : imgpath+"prem-04.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer4",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s16
  		}]
  	}]
  },{
  	//page 16
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "teacher",
  			imgsrc : imgpath+"miss-09-01.png"
  		},{
  			imgclass: "teacher_boy_girl_1",
  			imgsrc : imgpath+"Niti-03.png"
  		},{
  			imgclass: "teacher_boy_girl_2 mirror_rotate",
  			imgsrc : imgpath+"prem-04.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer8_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			imagelabeldata: data.string.p4_s17
  		}]
  	}]
  },{
  	//page 17
  	contentnocenteradjust: true,
  	contentblockadditionalclass: "yellow_bg",
  	imageblock:[{
		imagestoshow:[{
  			imgclass: "niti1",
  			imgsrc : imgpath+"Niti-03.png"
  		},{
  			imgclass: "teacher_boy_girl_2 mirror_rotate",
  			imgsrc : imgpath+"prem-04.png"
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer6",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			imagelabeldata: data.string.p4_s18
  		}]
  	}]
  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 4;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// setTimeout(function(){
				// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			// }, 8000);

		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var playing_audio;
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	$nextBtn.hide(0);
	switch(countNext){
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				$nextBtn.delay(200).show(0);
			});
			break;
		case 17:
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				ole.footerNotificationHandler.pageEndSetNotification();
			});
			break;
		default:
			break;
	}
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generaltemplate();

			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
    playing_audio.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();
		sound_l_1.stop();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

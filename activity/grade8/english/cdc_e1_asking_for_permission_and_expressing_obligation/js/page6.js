var imgpath = $ref+"/images/";
var soundAsset = $ref+"/sounds/";

var sound_0 = new buzz.sound((soundAsset + "p6_s0.ogg"));
var sound_1 = new buzz.sound((soundAsset + "p6_s1.ogg"));
var sound_2 = new buzz.sound((soundAsset + "p6_s2.ogg"));
var sound_3 = new buzz.sound((soundAsset + "p6_s3.ogg"));
var sound_4 = new buzz.sound((soundAsset + "p6_s4.ogg"));
var sound_5 = new buzz.sound((soundAsset + "p6_s4_2.ogg"));
var sound_6 = new buzz.sound((soundAsset + "p6_s5.ogg"));
var sound_7 = new buzz.sound((soundAsset + "p6_s5_2.ogg"));

var sounds=[ sound_0, sound_1, sound_2, sound_3, sound_4, sound_5, sound_6, sound_7];


var content=[
  {
  	//page 0
  	contentblockadditionalclass: "dottedbackground",
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textclass: "maintitle2",
  		textdata: data.string.p5_s1
  	}]
  }, {
  	//page 1
  	contentblockadditionalclass: "dottedbackground",
  	uppertextblockadditionalclass: "firsttitle",
  	uppertextblock: [{
  		textclass: "maintitle",
  		textdata: data.string.p5_s2
  	}]
  }, {
  	//page 2
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle3",
  		textdata: data.string.p5_s3,
  	}],
  	imageblockadditionalclass: "additional_imgblock",
  	imageblock:[{
  		imgcontainerdiv: "dialogcontainer1",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s5
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s6
  		}]
  	},{
  		imagestoshow:[{
  			imgclass: "talkleft",
  			imgsrc: imgpath+"woman_head.png"
  		},{
  			imgclass: "talkright",
  			imgsrc: imgpath+"woman_head.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "instruction",
  			imagelabeldata: data.string.p5_s4
  		}]
  	}]

  }, {
  	//page 3
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle3",
  		textdata: data.string.p5_s3,
  	}],
  	imageblockadditionalclass: "additional_imgblock",
  	imageblock:[{
  		imgcontainerdiv: "dialogcontainer1",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s8
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer1_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s9
  		}]
  	},{
  		imagestoshow:[{
  			imgclass: "talkleft",
  			imgsrc: imgpath+"man_head.png"
  		},{
  			imgclass: "talkright",
  			imgsrc: imgpath+"man_head.png"
  		}],
  		imagelabels:[{
  			imagelabelclass: "instruction",
  			imagelabeldata: data.string.p5_s7
  		}]
  	}]

  }, {
  	//page 4
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle3",
  		textdata: data.string.p5_s10,
  	}],
  	imageblockadditionalclass: "additional_imgblock",
  	imageblock:[{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s12
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s14
  		}]
  	},{
  		imagelabels:[{
  			imagelabelclass: "instruction",
  			imagelabeldata: data.string.p5_s11
  		},{
  			imagelabelclass: "instruction2",
  			imagelabeldata: data.string.p5_s13
  		}]
  	}]

  }, {
  	//page 5
  	contentblockadditionalclass: "dottedbackground",
  	contentnocenteradjust: true,
  	uppertextblock: [{
  		textclass: "maintitle3",
  		textdata: data.string.p5_s10,
  	}],
  	imageblockadditionalclass: "additional_imgblock",
  	imageblock:[{
  		imgcontainerdiv: "dialogcontainer2",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s16
  		}]
  	},{
  		imgcontainerdiv: "dialogcontainer2_b mirror_rotate",
  		imagelabels: [{
  			imagelabelclass: "dialogcontent2",
  			datahighlightflag: true,
  			datahighlightcustomclass: "red_highlight_color",
  			imagelabeldata: data.string.p5_s18
  		}]
  	},{
  		imagelabels:[{
  			imagelabelclass: "instruction",
  			imagelabeldata: data.string.p5_s15
  		},{
  			imagelabelclass: "instruction2",
  			imagelabeldata: data.string.p5_s17
  		}]
  	}]

  }
];


$(function () {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
  var countNext = 0;
  var vocabcontroller =  new Vocabulary();
  vocabcontroller.init();

  var $total_page = content.length;
  loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

     /*===============================================
     =            data highlight function            =
     ===============================================*/
      /**

        What it does:
        - send an element where the function has to see
        for data to highlight
        - this function searches for all nodes whose
        data-highlight element is set to true
        -searches for # character and gives a start tag
        ;span tag here, also for @ character and replaces with
        end tag of the respective
        - if provided with data-highlightcustomclass value for highlight it
          applies the custom class or else uses parsedstring class

        E.g: caller : texthighlight($board);
       */
      function texthighlight($highlightinside){
        //check if $highlightinside is provided
        typeof $highlightinside !== "object" ?
        alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
        null ;

        var $alltextpara = $highlightinside.find("*[data-highlight='true']");
        var stylerulename;
        var replaceinstring;
        var texthighlightstarttag;
        var texthighlightendtag   = "</span>";
        if($alltextpara.length > 0){
          $.each($alltextpara, function(index, val) {
            /*if there is a data-highlightcustomclass attribute defined for the text element
            use that or else use default 'parsedstring'*/
            $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
              (stylerulename = $(this).attr("data-highlightcustomclass")) :
              (stylerulename = "parsedstring") ;

            texthighlightstarttag = "<span class='"+stylerulename+"'>";
            replaceinstring       = $(this).html();
            replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
            replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
            $(this).html(replaceinstring);
          });
        }
      }
    /*=====  End of data highlight function  ======*/

    /*===============================================
     =            user notification function        =
     ===============================================*/
    /**
      How to:
      - First set any html element with
        "data-usernotification='notifyuser'" attribute,
      and "data-isclicked = ''".
      - Then call this function to give notification
     */

    /**
      What it does:
      - You send an element where the function has to see
      for data to notify user
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
     */
    function notifyuser($notifyinside){
      //check if $notifyinside is provided
      typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null ;

      /*variable that will store the element(s) to remove notification from*/
      var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
       // if there are any notifications removal required add the event handler
      if($allnotifications.length > 0){
        $allnotifications.one('click', function() {
          /* Act on the event */
          $(this).attr('data-isclicked', 'clicked');
          $(this).removeAttr('data-usernotification');
        });
      }
    }
    /*=====  End of user notification function  ======*/

   /*======================================================
   =            Navigation Controller Function            =
   ======================================================*/
   /**
    How To:
    - Just call the navigation controller if it is to be called from except the
      last page of lesson
    - If called from last page set the islastpageflag to true such that
      footernotification is called for continue button to navigate to exercise
    */

  /**
      What it does:
      - If not explicitly overriden the method for navigation button
        controls, it shows the navigation buttons as required,
        according to the total count of pages and the countNext variable
      - If for a general use it can be called from the templatecaller
        function
      - Can be put anywhere in the template function as per the need, if
        so should be taken out from the templatecaller function
      - If the total page number is
     */

  function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
			// $nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			setTimeout(function(){
				islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
			}, 1000);

		}
	}
   /*=====  End of user navigation controller function  ======*/

   /*==================================================
  =            InstructionBlockController            =
  ==================================================*/
    /**
      How to:
      - Just call instructionblockcontroller() from the template
     */

    /**
      What it does:
      - It inserts and handles closing and opening of instruction block
      - this function searches for all text nodes whose
      data-usernotification attribute is set to notifyuser
      - applies event handler for each of the html element which
       removes the notification style.
    */
    function instructionblockcontroller(){
      var $instructionblock = $board.find("div.instructionblock");
      if($instructionblock.length > 0){
        var $contentblock = $board.find("div.contentblock");
        var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
        var instructionblockisvisibleflag;

        $contentblock.css('pointer-events', 'none');

        $toggleinstructionblockbutton.on('click', function() {
          instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
          if(instructionblockisvisibleflag == 'true'){
            instructionblockisvisibleflag = 'false';
            $contentblock.css('pointer-events', 'auto');
          }
          else if(instructionblockisvisibleflag == 'false'){
            instructionblockisvisibleflag = 'true';
            $contentblock.css('pointer-events', 'none');
          }

          $instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
        });
      }
    }
  /*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  var playing_audio;
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
	vocabcontroller.findwords(countNext);
	$nextBtn.hide(0);
	switch(countNext){
		case 0:
		case 1:
		case 2:
		case 3:
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				$nextBtn.delay(200).show(0);
			});
			break;
		case 4:
			playing_audio = sounds[countNext];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				playing_audio = sounds[countNext+1];
				playing_audio.play();
				playing_audio.bind('ended', function(){
					$nextBtn.delay(200).show(0);
				});
			});
			break;
		case 5:
			playing_audio = sounds[countNext+1];
			playing_audio.play();
			playing_audio.bind('ended', function(){
				playing_audio = sounds[countNext+2];
				playing_audio.play();
				playing_audio.bind('ended', function(){
					ole.footerNotificationHandler.pageEndSetNotification();
				});
			});
			break;
		default:
			break;
	}
  }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
    Motivation :
    - Make a single function call that handles all the
      template load easier

    How To:
    - Update the template caller with the required templates
    - Call template caller

    What it does:
    - According to value of the Global Variable countNext
      the slide templates are updated
   */

 	function templateCaller() {
			/*always hide next and previous navigation button unless
			 explicitly called from inside a template*/
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// call navigation controller
			navigationcontroller();

			// call the template
			generaltemplate();

			//call the slide indication bar handler for pink indicators
			loadTimelineProgress($total_page, countNext + 1);

			// just for development purpose to see total slide vs current slide number
			// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */
 	$nextBtn.on('click', function() {
    playing_audio.stop();
			countNext++;
			templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click', function() {
		playing_audio.stop();
		countNext--;
		templateCaller();
		sound_l_1.stop();
		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

Array.prototype.shufflearray = function(){
	var i = this.length, j, temp;
	while(--i > 0){
		j = Math.floor(Math.random() * (i+1));
		temp = this[j];
		this[j] = this[i];
		this[i] = temp;
	}
	return this;
};

var imgpath = $ref+"/exercise/images/";

var content=[
		//slide 1	
		{

			imgexerciseblock: [

			{	
				ques_img: imgpath + "water.png",
				textdata: data.string.ques1,
				questiontext: data.string.question1,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q1background1" ,
					optionstext: data.string.fish,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q1background2" ,
					optionstext: data.string.leo,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q1background3" ,
					optionstext: data.string.ele,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q1background4" ,
					optionstext: data.string.bee,
				},
				]
			}
			]

		},

		//slide 2
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "water.png",
				textdata: data.string.ques1,
				questiontext: data.string.question3,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q2background2" ,
					optionstext: data.string.frog,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q2background1" ,
					optionstext: data.string.man,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q2background3" ,
					optionstext: data.string.horse,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q2background4" ,
					optionstext: data.string.rabb,
				},
				]
			}
			]

		},

		//slide 3
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "landandwater.png",
				textdata: data.string.ques1,
				questiontext: data.string.question3,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q3background1" ,
					optionstext: data.string.croc,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q3background2" ,
					optionstext: data.string.sheep,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q3background3" ,
					optionstext: data.string.lion,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q3background4" ,
					optionstext: data.string.pig,
				},
				]
			}
			]

		},

		//slide 4
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "rabithome.png",
				textdata: data.string.ques1,
				questiontext: data.string.question4,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q4background1" ,
					optionstext: data.string.rabb,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q4background2" ,
					optionstext: data.string.buff,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q4background3" ,
					optionstext: data.string.cow,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q4background4" ,
					optionstext: data.string.fish,
				},
				]
			}
			]

		},

		//slide 5
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "stable.png",
				textdata: data.string.ques1,
				questiontext: data.string.question5,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q5background1" ,
					optionstext: data.string.horse,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q5background2" ,
					optionstext: data.string.crow,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q5background3" ,
					optionstext: data.string.snake,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q5background4" ,
					optionstext: data.string.rabb,
				},
				]
			}
			]

		},

		//slide 6
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "jungle.png",
				textdata: data.string.ques1,
				questiontext: data.string.question6,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q6background1" ,
					optionstext: data.string.tiger,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q6background2" ,
					optionstext: data.string.cow,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q6background3" ,
					optionstext: data.string.duck,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q6background4" ,
					optionstext: data.string.sheep,
				},
				]
			}
			]

		},

		//slide 7
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "shed.png",
				textdata: data.string.ques1,
				questiontext: data.string.question7,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q7background1" ,
					optionstext: data.string.cow,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q7background2" ,
					optionstext: data.string.croc,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q7background3" ,
					optionstext: data.string.frog,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q7background4" ,
					optionstext: data.string.lion,
				},
				]
			}
			]

		},

		//slide 8
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "tree.png",
				textdata: data.string.ques1,
				questiontext: data.string.question8,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q8background1" ,
					optionstext: data.string.crow,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q8background2" ,
					optionstext: data.string.goat,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q8background3" ,
					optionstext: data.string.cow,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q8background4" ,
					optionstext: data.string.fish,
				},
				]
			}
			]

		},

		//slide 9
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "pen.png",
				textdata: data.string.ques1,
				questiontext: data.string.question9,
				
				exeoptions: [

				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q9background1" ,
					optionstext: data.string.goat,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q9background2" ,
					optionstext: data.string.snake,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q9background3" ,
					optionstext: data.string.leo,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q9background4" ,
					optionstext: data.string.crow,
				},
				]
			}
			]

		},

		//slide 10
		{
			imgexerciseblock: [
			{	
				ques_img: imgpath + "hive.png",
				textdata: data.string.ques1,
				questiontext: data.string.question10,
				
				exeoptions: [
				{
					forshuffle: "class1",
					backgroundopt: "backgroundtrue q10background1" ,
					optionstext: data.string.bee,
				},
				{
					forshuffle: "class2",
					backgroundopt: "backgroundtrue q10background2" ,
					optionstext: data.string.crow,
				},
				{
					forshuffle: "class3",
					backgroundopt: "backgroundtrue q10background3" ,
					optionstext: data.string.ele,
				},
				{
					forshuffle: "class4",
					backgroundopt: "backgroundtrue q10background4" ,
					optionstext: data.string.pig,
				},
				]
			}
			]

		},

		];


		/*remove this for non random questions*/
		content.shufflearray();


$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;
	 }

	 var score = 0;

	 /*values in this array is same as the name of images of eggs in image folder*/
	var testin = new EggTemplate();
	 	//eggTemplate.eggMove(countNext);
 	testin.init(10);
	 function generaltemplate() {
	 	var source = $("#general-template").html();
	 	var template = Handlebars.compile(source);
	 	var html = template(content[countNext]);
	 	$board.html(html);
	 	
	 	$nextBtn.hide(0);
	 	$prevBtn.hide(0);
	 	$("#num_ques").text(countNext + 1 + ". ");
	 	/*for randomizing the options*/
	 	var parent = $(".optionsdiv_img");
	 	var divs = parent.children();
	 	while (divs.length) {
	 		parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
	 	}


	 	/*======= SCOREBOARD SECTION ==============*/
	 	var ansClicked = false;
	 	var wrngClicked = false;

	 	$(".buttonsel").click(function(){
	 		$(this).removeClass('forhover');
	 		if(ansClicked == false){

					/*class 1 is always for the right answer. updates scoreboard and disables other click if
					right answer is clicked*/
					if($(this).hasClass("class1")){
						if(wrngClicked == false){
							testin.update(true);
						}
						$(this).css("background","#6AA84F");
						$(this).siblings().css("color","#6AA84F");
						$(this).css("border","5px solid #B6D7A8");
						$('.buttonsel').removeClass('forhover forhoverimg');
						ansClicked = true;
						play_correct_incorrect_sound(true);
						if(countNext != $total_page)
							$nextBtn.show(0);
					}
					else{
						testin.update(false);
						$(this).css("background","#EA9999");
						$(this).siblings().css("color","#EA9999");
						$(this).css("border","5px solid #efb3b3");
						$(this).css("color","#F66E20");
						wrngClicked = true;
						play_correct_incorrect_sound(false);
					}
				}
			}); 
	 	
	 	/*======= SCOREBOARD SECTION ==============*/
	 }


	 function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators

	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on("click", function(){
		countNext++;
		templateCaller();
		testin.gotoNext();
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q15,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2,correct : "correct"},
		]	
	},
	{
		question : data.string.q16,
		answers: [
		{ans : data.string.name3,correct : "correct"},
		{ans : data.string.name4},
		]
	},
	{
		question : data.string.q17,
		answers: [
		{ans : data.string.name5,correct : "correct"},
		{ans : data.string.name6},
		]
		
	},
	{
		question : data.string.q18,
		answers: [
		{ans : data.string.name7},
		{ans : data.string.name8,correct : "correct"},
		]
	},
	{
		question : data.string.q19,
		answers: [
		{ans : data.string.name9},
		{ans : data.string.name10,correct : "correct"},
		]
	},
	
	{
		question : data.string.q20,
		answers: [
		{ans : data.string.name11,correct : "correct"},
		{ans : data.string.name12},
		]
	},
	
	{
		question : data.string.q21,
		answers: [
		{ans : data.string.name15},
		{ans : data.string.name16,correct : "correct"},
		]
	},
	
	{
		question : data.string.q22,
		answers: [
		{ans : data.string.name17,correct : "correct"},
		{ans : data.string.name18},
		]
	},
	
	{
		question : data.string.q23,
		answers: [
		{ans : data.string.name19},
		{ans : data.string.name20,correct : "correct"},
		]
	},

	{
		question : data.string.q24,
		answers: [
		{ans : data.string.name21},
		{ans : data.string.name22,correct : "correct"},
		]
	},

	{
		question : data.string.q25,
		answers: [
		{ans : data.string.name23},
		{ans : data.string.name24,correct : "correct"},
		]
	},

	{
		question : data.string.q26,
		answers: [
		{ans : data.string.name25,correct : "correct"},
		{ans : data.string.name26},
		]
	},

	{
		question : data.string.q27,
		answers: [
		{ans : data.string.name27},
		{ans : data.string.name28,correct : "correct"},
		]
	},

	{
		question : data.string.q28,
		answers: [
		{ans : data.string.name29,correct : "correct"},
		{ans : data.string.name30},
		]
	},

	{
		question : data.string.q29,
		answers: [
		{ans : data.string.name31},
		{ans : data.string.name32,correct : "correct"},
		]
	},

	{
		question : data.string.q30,
		answers: [
		{ans : data.string.name33,correct : "correct"},
		{ans : data.string.name34},
		]
	}
	
	];

$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;


	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<16){
			qA();
		}
		else if (questionCount==16){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
	
	

imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/";

var dialog0 = new buzz.sound(soundAsset+"ecotourisminnepal.ogg");
var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"2.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"3.ogg");
var dialog1main = [dialog1,dialog1part1,dialog1part2];

var dialog2 = new buzz.sound(soundAsset+"4.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"5.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"6.ogg");
var dialog2main = [dialog2,dialog2part1,dialog2part2];


var dialog3 = new buzz.sound(soundAsset+"7.1.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"8.ogg");
var dialog3part1_1 = new buzz.sound(soundAsset+"9.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"10.1.ogg");
var dialog3main = [dialog3,dialog3part1,dialog3part1_1,dialog3part2];



var nat1 = new buzz.sound(soundAsset+"11.ogg");
var nat2 = new buzz.sound(soundAsset+"12.ogg");
var nat3 = new buzz.sound(soundAsset+"13.ogg");
var nat4 = new buzz.sound(soundAsset+"14.ogg");
var nat5 = new buzz.sound(soundAsset+"15.ogg");
var nat6 = new buzz.sound(soundAsset+"16.ogg");
var nat7 = new buzz.sound(soundAsset+"17.ogg");
var nat8 = new buzz.sound(soundAsset+"18.ogg");
var nat9 = new buzz.sound(soundAsset+"19.ogg");
var nat10 = new buzz.sound(soundAsset+"20.ogg");
var nat11 = new buzz.sound(soundAsset+"21.ogg");
var natmain = [nat1,nat2,nat3,nat4,nat5,nat6,nat7,nat8,nat9,nat10,nat11];


var wildtitle1 = new buzz.sound(soundAsset+"22.ogg");
var wildtitle = new buzz.sound(soundAsset+"23.ogg");
var wild1 = new buzz.sound(soundAsset+"24.ogg");
var wild2 = new buzz.sound(soundAsset+"25.ogg");
var wild3 = new buzz.sound(soundAsset+"26.ogg");
var wildmain = [wildtitle1,wildtitle,wild1,wild2,wild3];


var cn = new buzz.sound(soundAsset+"27.ogg");
var cntitle = new buzz.sound(soundAsset+"28.ogg");
var cn1 = new buzz.sound(soundAsset+"29.ogg");
var cn2 = new buzz.sound(soundAsset+"30.ogg");
var cn3 = new buzz.sound(soundAsset+"31.ogg");
var cn4 = new buzz.sound(soundAsset+"32.ogg");
var cn5 = new buzz.sound(soundAsset+"33.ogg");
var cn6 = new buzz.sound(soundAsset+"34.ogg");
var cnmain = [cn,cntitle,cn1,cn2,cn3,cn4,cn5,cn6];


var bufferzone1 = new buzz.sound(soundAsset+"35.ogg");
var bufferzone2 = new buzz.sound(soundAsset+"36.ogg");
var bufferzone2part2 = new buzz.sound(soundAsset+"37.ogg");
var bufferzone2part3 = new buzz.sound(soundAsset+"38.ogg");
var bufferzone2part4 = new buzz.sound(soundAsset+"39.ogg");
var bufferzone2part5 = new buzz.sound(soundAsset+"40.ogg");
var bufferzone2part6 = new buzz.sound(soundAsset+"41.ogg");
var bufferzone3 = new buzz.sound(soundAsset+"42.ogg");
var bufferzonemain = [bufferzone1,bufferzone2,bufferzone2part2,bufferzone2part3,bufferzone2part4,bufferzone2part5,bufferzone2part6,bufferzone3];

var whhtitle = new buzz.sound(soundAsset+"43.ogg");
var whh1 = new buzz.sound(soundAsset+"44.ogg");
var whh2 = new buzz.sound(soundAsset+"45.ogg");
var whh3 = new buzz.sound(soundAsset+"46.ogg");
var whh4 = new buzz.sound(soundAsset+"47.ogg");
var whh5 = new buzz.sound(soundAsset+"48.ogg");
var whh6 = new buzz.sound(soundAsset+"49.ogg");
var whh7 = new buzz.sound(soundAsset+"50.ogg");
var whh8 = new buzz.sound(soundAsset+"51.ogg");
var whh9 = new buzz.sound(soundAsset+"52.ogg");
var whh10 = new buzz.sound(soundAsset+"53.ogg");
var whmain = [whhtitle,whh1,whh2,whh3,whh4,whh5,whh6,whh7,whh8,whh9,whh10];

var slide9_1 = new buzz.sound(soundAsset+"54.1.ogg");
var slide9_2 = new buzz.sound(soundAsset+"55.ogg");
var slidemain = [slide9_1,slide9_2];

var eco0 = new buzz.sound(soundAsset+"56.ogg");
var eco1 = new buzz.sound(soundAsset+"57.ogg");
var eco2 = new buzz.sound(soundAsset+"58.1.ogg");
var eco3 = new buzz.sound(soundAsset+"59.ogg");
var eco4 = new buzz.sound(soundAsset+"60.ogg");
var eco5 = new buzz.sound(soundAsset+"61.ogg");
var eco6 = new buzz.sound(soundAsset+"62.ogg");
var eco7 = new buzz.sound(soundAsset+"63.ogg");
var eco7_1 = new buzz.sound(soundAsset+"64.ogg");
var ecomain = [eco0,eco1,eco2,eco3,eco4,eco5,eco6,eco7,eco7_1];




var eco8 = new buzz.sound(soundAsset+"65.ogg");
var eco9 = new buzz.sound(soundAsset+"66.ogg");
var eco10 = new buzz.sound(soundAsset+"67.ogg");
var eco11 = new buzz.sound(soundAsset+"68.ogg");
var eco12 = new buzz.sound(soundAsset+"69.ogg");
var eco13 = new buzz.sound(soundAsset+"70.ogg");
var eco14 = new buzz.sound(soundAsset+"71.ogg");
var ecomain1 = [eco8,eco9,eco10,eco11,eco12,eco13,eco14];



var eco15 = new buzz.sound(soundAsset+"72.ogg");
var eco16 = new buzz.sound(soundAsset+"73.ogg");
var eco17 = new buzz.sound(soundAsset+"74.ogg");
var eco18 = new buzz.sound(soundAsset+"75.ogg");
var ecomain2 = [eco15,eco16,eco17,eco18];


var sd110 = new buzz.sound(soundAsset+"76.ogg");
var sd111 = new buzz.sound(soundAsset+"77.ogg");
var sd112 = new buzz.sound(soundAsset+"78.ogg");
var sd113 = new buzz.sound(soundAsset+"79.ogg");
var sd114 = new buzz.sound(soundAsset+"80.ogg");
var sd115 = new buzz.sound(soundAsset+"81.ogg");
var sd116 = new buzz.sound(soundAsset+"82.ogg");
var sd117 = new buzz.sound(soundAsset+"83.ogg");
var sd118 = new buzz.sound(soundAsset+"84.ogg");
var sd119 = new buzz.sound(soundAsset+"85.ogg");
var sd1110 = new buzz.sound(soundAsset+"86.ogg");
var sd1111 = new buzz.sound(soundAsset+"87.ogg");
var sd11main = [sd110,sd111,sd112,sd113,sd114,sd115,sd116,sd117,sd118,sd119,sd1110,sd1111];

var soundcontent = [dialog0,dialog1main,dialog2main,dialog3main,natmain,wildmain,cnmain,bufferzonemain,whmain,slidemain,ecomain,ecomain1,ecomain2,sd11main];

var content=[
    {
        bgImgSrc : imageAsset+"ecotourism_in_nepal.png",
        imageid : "bg_image1",
        dialog_id : "dialog_1main",
        forwhichdialog : "dialog0",
        lineCountDialog : [
            {
                span_id:"span1",
                span_data: data.string.p1dialog0,
            }
        ],
        speakerImgSrc : $ref+"/images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"one.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"span1",
							span_data: data.string.p1dialog1,	
						},
						{
							span_id:"span2",
							span_data: data.string.p1dialog1part1,	
						},
						{
							span_id:"span3",
							span_data: data.string.p1dialog1part2,	
						}
					],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"eco.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		span_id : "span2",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p2dialog2,	
						},
						{
							span_id:"span2",
							span_data: data.string.p2dialog2part1,	
						},
						{
							span_id:"span3",
							span_data: data.string.p2dialog2part2,	
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"landlocked.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		span_id : "span3",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1_slide2",
							span_data: data.string.p3dialog3,	
						},
						{
							span_id:"span2_slide3",
							span_data: data.string.p3dialog3part1,	
						},
						{
							span_id:"span2_slide3_1",
							span_data: data.string.p3dialog3part1_1,	
						},
						{
							span_id:"span3_slide4",
							span_data: data.string.p3dialog3part2,	
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"nt.jpg",
		imageid : "bg_image4",
		dialog_id : "natmain",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"span1_nat",
							span_data: data.string.nt1,	
						},
						{
							span_id:"span2_nat",
							span_data: data.string.nt2,	
						},
						{
							span_id:"span3_nat",
							span_data: data.string.nt3,	
						},
						{
							span_id:"span4_nat",
							span_data: data.string.nt4,	
						},

						{
							span_id:"span5_nat",
							span_data: data.string.nt5,	
						},

						{
							span_id:"span6_nat",
							span_data: data.string.nt6,	
						},

						{
							span_id:"span7_nat",
							span_data: data.string.nt7,	
						},

						{
							span_id:"span8_nat",
							span_data: data.string.nt8,	
						},

						{
							span_id:"span9_nat",
							span_data: data.string.nt9,	
						},

						{
							span_id:"span10_nat",
							span_data: data.string.nt10,	
						},

						{
							span_id:"span11_nat",
							span_data: data.string.nt11,	
						}

						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"wild.jpg",
		imageid : "bg_image5",
		dialog_id : "wildmain",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"title",
							span_data: data.string.title,	
						},
						{
							span_id:"wildtitle",
							span_data: data.string.titlewildlife,	
						},
						{
							span_id:"wd2_nat",
							span_data: data.string.wd1,	
						},
						{
							span_id:"wd3_nat",
							span_data: data.string.wd2,	
						},
						{
							span_id:"wd4_nat",
							span_data: data.string.wd3,	
						},
						{
							span_id:"clicktest1",
							span_class:"clicktest",
							span_data: data.string.titlewildlife1,	
						},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"cn.jpg",
		imageid : "bg_image6",
		dialog_id : "cnmain",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"cn",
							span_data: data.string.title1,	
						},
						{
							span_id:"cntitle",
							span_data: data.string.titleconservation,	
						},
						{
							span_id:"cn2_nat",
							span_data: data.string.cn1,	
						},
						{
							span_id:"cn3_nat",
							span_data: data.string.cn2,	
						},
						{
							span_id:"cn4_nat",
							span_data: data.string.cn3,	
						},
						{
							span_id:"cn5_nat",
							span_data: data.string.cn4,	
						},
						{
							span_id:"cn6_nat",
							span_data: data.string.cn5,	
						},
						{
							span_id:"cn7_nat",
							span_data: data.string.cn6,	
						},
						{
							span_id:"clicktest1",
							span_class:"clicktest",
							span_data: data.string.titlewildlife1,	
						},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"bufferzonenepal.jpg",
		imageid : "bg_image7",
		dialog_id : "bufferzonemain",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"bf1",
							span_data: data.string.bf1,	
						},
						{
							span_id:"bf2",
							span_data: data.string.bf2,	
						},
						{
							span_id:"bf3",
							span_data: data.string.bf2_part2,	
						},
						{
							span_id:"bf4",
							span_data: data.string.bf2_part3,	
						},
						{
							span_id:"bf5",
							span_data: data.string.bf2_part4,	
						},
						{
							span_id:"bf6",
							span_data: data.string.bf2_part5,	
						},
						{
							span_id:"bf7",
							span_data: data.string.bf2_part6,	
						},
						{
							span_id:"bf8",
							span_data: data.string.bf3,	
						},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"wd.jpg",
		imageid : "bg_image8",
		dialog_id : "whmain",
		forwhichdialog : "dialog8",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"whtitle",
							span_data: data.string.whtitle,	
						},
						{
							span_id:"wh1",
							span_data: data.string.wh1,	
						},
						{
							span_id:"wh2",
							span_data: data.string.wh2,	
						},
						{
							span_id:"wh3",
							span_data: data.string.wh3,	
						},
						{
							span_id:"wh4",
							span_data: data.string.wh4,	
						},
						{
							span_id:"wh5",
							span_data: data.string.wh5,	
						},
						{
							span_id:"wh6",
							span_data: data.string.wh6,	
						},
						{
							span_id:"wh7",
							span_data: data.string.wh7,	
						},
						{
							span_id:"wh8",
							span_data: data.string.wh8,	
						},
						{
							span_id:"wh9",
							span_data: data.string.wh9,	
						},
						{
							span_id:"wh10",
							span_data: data.string.wh10,	
						},
						{
							span_id:"clicktest1",
							span_class:"clicktest",
							span_data: data.string.titlewildlife1,	
						},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"slide9.jpg",//image
		imageid : "bg_image9",
		dialog_id : "slidemain",
		forwhichdialog : "dialog9",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [
							{
								span_id:"sd1_slide",
								span_data: data.string.sd1,	
							},
							{
								span_id:"sd2_slide",
								span_data: data.string.sd2,	
							},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},

	{
		bgImgSrc : imageAsset+"slide10.jpg",//image
		imageid : "bg_image10",
		dialog_id : "ecomain",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [{
								span_id:"ec0_slide",
								span_data: data.string.sd100,	
							},
							{
								span_id:"ec1_slide",
								span_data: data.string.sd101,	
							},
							{
								span_id:"ec2_slide",
								span_data: data.string.sd102,	
							},
							{
								span_id:"ec3_slide",
								span_data: data.string.sd103,	
							},
							{
								span_id:"ec4_slide",
								span_data: data.string.sd104,	
							},
							{
								span_id:"ec5_slide",
								span_data: data.string.sd105,	
							},
							{
								span_id:"ec6_slide",
								span_data: data.string.sd106,	
							},
							{
								span_id:"ec7_slide",
								span_data: data.string.sd107,	
							},
							 {
							 	span_id:"ec7_1slide",
								span_data: data.string.sd107_1,	
							},
							// {
							// 	span_id:"ec9_slide",
							// 	span_data: data.string.sd109,	
							// },
							// {
							// 	span_id:"ec10_slide",
							// 	span_data: data.string.sd1010,	
							// },
							// {
							// 	span_id:"ec11_slide",
							// 	span_data: data.string.sd1011,	
							// },
							// {
							// 	span_id:"ec12_slide",
							// 	span_data: data.string.sd1012,	
							// },
							// {
							// 	span_id:"ec13_slide",
							// 	span_data: data.string.sd1013,	
							// },
							// {
							// 	span_id:"ec14_slide",
							// 	span_data: data.string.sd1014,	
							// },
							// {
							// 	span_id:"ec15_slide",
							// 	span_data: data.string.sd1015,	
							// },
							// {
							// 	span_id:"ec16_slide",
							// 	span_data: data.string.sd1016,	
							// },
							// {
							// 	span_id:"ec17_slide",
							// 	span_data: data.string.sd1017,	
							// },
							// {
							// 	span_id:"ec18_slide",
							// 	span_data: data.string.sd1018,	
							// },
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	// part2

	{
		bgImgSrc : imageAsset+"slide10.jpg",//image
		imageid : "bg_image10_1",
		dialog_id : "ecomain1",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [
							
							{
								span_id:"ec8_slide",
								span_data: data.string.sd108,	
							},
							{
								span_id:"ec9_slide",
								span_data: data.string.sd109,	
							},
							{
								span_id:"ec10_slide",
								span_data: data.string.sd1010,	
							},
							{
								span_id:"ec11_slide",
								span_data: data.string.sd1011,	
							},
							{
								span_id:"ec12_slide",
								span_data: data.string.sd1012,	
							},
							{
								span_id:"ec13_slide",
								span_data: data.string.sd1013,	
							},
							{
								span_id:"ec14_slide",
								span_data: data.string.sd1014,	
							},
							// {
							// 	span_id:"ec15_slide",
							// 	span_data: data.string.sd1015,	
							// },
							// {
							// 	span_id:"ec16_slide",
							// 	span_data: data.string.sd1016,	
							// },
							// {
							// 	span_id:"ec17_slide",
							// 	span_data: data.string.sd1017,	
							// },
							// {
							// 	span_id:"ec18_slide",
							// 	span_data: data.string.sd1018,	
							// },
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"slide10.jpg",//image
		imageid : "bg_image10_2",
		dialog_id : "ecomain2",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [
							
							{
								span_id:"ec15_slide",
								span_data: data.string.sd1015,	
							},
							{
								span_id:"ec16_slide",
								span_data: data.string.sd1016,	
							},
							{
								span_id:"ec17_slide",
								span_data: data.string.sd1017,	
							},
							{
								span_id:"ec18_slide",
								span_data: data.string.sd1018,	
							},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},



	{
		bgImgSrc : imageAsset+"lastslide.jpg",//image
		imageid : "bg_image10",
		dialog_id : "sd11main",
		forwhichdialog : "dialog10",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [{
								span_id:"sdd0_slide",
								span_data: data.string.sd110,	
							},
							{
								span_id:"sdd1_slide",
								span_data: data.string.sd111,	
							},
							{
								span_id:"sdd2_slide",
								span_data: data.string.sd112,	
							},
							{
								span_id:"sdd3_slide",
								span_data: data.string.sd113,	
							},
							
							{
								span_id:"sdd4_slide",
								span_data: data.string.sd114,	
							},
							{
								span_id:"sdd5_slide",
								span_data: data.string.sd115,	
							},
							{
								span_id:"sdd6_slide",
								span_data: data.string.sd116,	
							},
							{
								span_id:"sdd7_slide",
								span_data: data.string.sd117,	
							},
							{
								span_id:"sdd8_slide",
								span_data: data.string.sd118,	
							},
							{
								span_id:"sdd9_slide",
								span_data: data.string.sd119,	
							},
							{
								span_id:"sdd10_slide",
								span_data: data.string.sd1110,	
							},
							{
								span_id:"sdd11_slide",
								span_data: data.string.sd1111,	
							},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		questionend: true,
		questiondata: data.string.s1_q1,
		answerdata: data.string.s1_q1_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q2,
		answerdata: data.string.s1_q2_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q3,
		answerdata: data.string.s1_q3_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q4,
		answerdata: data.string.s1_q4_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q5,
		answerdata: data.string.s1_q5_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q6,
		answerdata: data.string.s1_q6_a1
	},
	{
		questionend: true,
		questiondata: data.string.s1_q7,
		answerdata: data.string.s1_q7_a1
	}
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	
	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		/*The following code is all manual process on clicks--> Not a perfect solution thought, but
		a better solution for english*/	
			
		/*This is national park section*/
		if(countNext > 13){
			$(".click_for_ref").click(function(){
				$(".answer").show(0);
				$(this).addClass("clicked");
				$prevBtn.show(0);
				if(countNext == ($total_page-1)){
		     		ole.footerNotificationHandler.lessonEndSetNotification();
				}else{
					$nextBtn.show(0);
				}
			});
		} else {
				var test =  $("#natmain").find("span");
				$(".close_imageblock").click(function(){
						$("#imageblock").fadeOut(500);
						$("#natmain").removeClass("opacity_image");
		
				});
		
				test.on("click", function(){ 
					var id = $(this).attr("id");
					var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/chitwan.jpg')";
					var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sagarmatha.jpg')";
					var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/makalu.jpg')";
					var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/langtang.jpg')";
					var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/rara.jpg')";
					var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/khaptad.jpg')";
					var bg7 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pho.jpg')";
					var bg8 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bardiya.jpg')";
					var bg9 = "url('activity/grade8/english/eng_grade8_ecotourism/images/shiva.jpg')";
					var bg10 = "url('activity/grade8/english/eng_grade8_ecotourism/images/banke.jpg')";
		
					if (id == "span0_nat"){
						$(".title").show(0);
					}
					
					if (id == "span2_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg1 ;
						$("#imageblock").addClass("imageblock_chitwan");
						$("#natmain").addClass("opacity_image");
		
					}
		
					if (id == "span3_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg2 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span4_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg3 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span5_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg4 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span6_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg5 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span7_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg6 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span8_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg7 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span9_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg8 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span10_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg9 ;
						$("#natmain").addClass("opacity_image");
					}
		
					if (id == "span11_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg10 ;
						$("#natmain").addClass("opacity_image");
					}
				});
				
				/*This is wild life  park section*/
				
				var con =  $("#cnmain").find("span");
				$(".close_imageblock").click(function(){
						$("#imageblock").fadeOut(500);
						
				});
				con.on("click", function(){ 
					var id = $(this).attr("id");
					var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/anna.jpg')";
					var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/kan.jpg')";
					var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/man.jpg')";
					var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bla.jpg')";
					var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/ami.jpg')";
					var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/gauri.jpg')";
		
					
					if (id == "cn2_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg1 ;
						$("#imageblock").addClass("imageblock_chitwan");
						
		
					}
		
					if (id == "cn3_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg2 ;
						
					}
		
					if (id == "cn4_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg3 ;
						
					}
						if (id == "cn5_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg4 ;
						
					}
		
					if (id == "cn6_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg5 ;
						
					}
		
					if (id == "cn7_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg6 ;
						
					}
		
		
		
				});
		
				/*This is conservation section*/
				var wildlife =  $("#wildmain").find("span");
				$(".close_imageblock").click(function(){
						$("#imageblock").fadeOut(500);
						
				});
				wildlife.on("click", function(){ 
					var id = $(this).attr("id");
					var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/parsa.jpg')";
					var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/koshi.jpg')";
					var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sukla.jpg')";
					
					if (id == "wd2_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg1 ;
						$("#imageblock").addClass("imageblock_chitwan");
						
		
					}
		
					if (id == "wd3_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg2 ;
						
					}
		
					if (id == "wd4_nat"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg3 ;
						
					}
		
				});
		
				/*World heritage*/
					var world =  $("#whmain").find("span");
						$(".close_imageblock").click(function(){
								$("#imageblock").fadeOut(500);
								
						});
				world.on("click", function(){ 
					var id = $(this).attr("id");
					var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/hun.jpeg')";
					var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pt.jpg')";
					var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bt.jpg')";
					var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pa.jpg')";
					var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/lu.jpg')";
					var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sw.jpg')";
					var bg7 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bd.jpg')";
					var bg8 = "url('activity/grade8/english/eng_grade8_ecotourism/images/cna.jpg')";
					var bg9 = "url('activity/grade8/english/eng_grade8_ecotourism/images/chi.jpg')";
					var bg10 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sagar.jpg')";
					
					if (id == "wh1"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg1 ;
						
						
		
					}
		
					if (id == "wh2"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg2 ;
						
					}
		
					if (id == "wh3"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg3 ;
					}
		
					if (id == "wh4"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg4 ;
					}
		
		
					if (id == "wh5"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg5 ;
					}
		
		
					if (id == "wh6"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg6 ;
					}
		
		
					if (id == "wh7"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg7 ;
					}
		
		
					if (id == "wh8"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg8 ;
					}
		
		
					if (id == "wh9"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg9 ;
					}
		
		
					if (id == "wh10"){
						$("#imageblock").fadeIn(1000);
						document.getElementById("imageblock").style.backgroundImage = bg10 ;
					}
		
				});
					
				/*End Rashik Maharjan*/
		
		
				var $slide = $board.children('div');
				var $dialogcontainer = $slide.children('div.dialogcontainer');
				var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
				var $paralines = $dialogcontainer.children('p').children('span');
				var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
				console.log("I am in sound"+countNext)

					if($.isArray(soundcontent[countNext])){
						console.log("I am in sound"+countNext)
						playThisDialog = soundcontent[countNext];
						playThisDialog[0].play();
					}
					else if(!$.isArray(soundcontent[countNext])){
						playThisDialog = [soundcontent[countNext]];
						playThisDialog[0].play();
					}
					
		
				$listenAgainButton.on('click',  function() {
					/* Act on the event */
					$paralinestohideonListenAgain.css('display', 'none');
					playThisDialog[0].play();
					$nextBtn.hide(0);
			     	$prevBtn.hide(0);
					$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
				});
		
				function playerbinder(){
						$.each(playThisDialog, function( index, entry ) { 
							if(index < playThisDialog.length-1){
							 	entry.bind('ended', function(){
							 		// alert(index +"sound ended");
							 		$paralines.eq(index+1).fadeIn(400);
							 		playThisDialog[index+1].play();
							 	});
							}
		
							if(index == playThisDialog.length-1){
							 	entry.bind("ended", function() {	
								$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');
		
						     	if(countNext > 0 && countNext < $total_page-1){
						     		$nextBtn.show(0);
									 $prevBtn.show(0);
									 console.log("I am in next button");
									 $(".clicktest").css("display","inline");
						     	}
		
						     	else if(countNext < 1){
						     		$nextBtn.show(0);
						     	}
		
						     	else if(countNext >= $total_page-1){
						     		$prevBtn.show(0);
						     		ole.footerNotificationHandler.lessonEndSetNotification();
						     	}
							});
							}
						});
					}
		
					playerbinder();
			}
						
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

});




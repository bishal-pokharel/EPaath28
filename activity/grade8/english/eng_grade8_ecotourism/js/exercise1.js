$('.title').text(data.string.title);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/exercise_image/cs.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/exercise_image/mass.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/exercise_image/res.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/exercise_image/sus.png",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/exercise_image/deg.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/exercise_image/topo.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/exercise_image/tro.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/exercise_image/tun.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/exercise_image/buf.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/exercise_image/des.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/exercise_image/cul.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/exercise_image/mot.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/exercise_image/co.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/exercise_image/safe.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/exercise_image/upr.jpg",
			},//
			{
				words : data.string.word16,
				description : data.string.des16,
				img : $ref + "/exercise_image/inc.jpg",
			},//
			
		
			];
			console.log(content);
			displaycontent();
			ole.footerNotificationHandler.pageEndSetNotification();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		$(this).toggleClass('active');
	}); 
});
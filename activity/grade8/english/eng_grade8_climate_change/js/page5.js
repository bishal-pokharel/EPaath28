imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page4/";
background_image = $ref+"/images/"


var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog2 = new buzz.sound(soundAsset+"3.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"4.ogg");

var dialog2main = [dialog2, dialog2part1, ];

var dialog3 = new buzz.sound(soundAsset+"climate.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"6.ogg");
var dialog3main = [dialog3, dialog3part1];

var dialog4 = new buzz.sound(soundAsset+"7.ogg");
var dialog4part1 = new buzz.sound(soundAsset+"8.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"9.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"10.ogg");
var dialog4main = [dialog4, dialog4part1, dialog4part2, dialog4part3];

//This will have all the sound 
var soundcontent = [dialog1, dialog2main, dialog3main, dialog4main]; 


var content=[
	{
		bgImgSrc : imageAsset+"wv.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
		],
		lineCountDialog : [data.string.p4dialog1],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"gg.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2",
		forwhichdialog : "dialog2main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p4dialog2,
							data.string.p4dialog2part1, 
							
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	{
		bgImgSrc : imageAsset+"bt.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p4dialog3,
							data.string.p4dialog3part1 
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	{
		bgImgSrc : imageAsset+"greenhousegases.svg",
		imageid : "bg_image4",
		dialog_id : "dialog_4",
		forwhichdialog : "dialog4main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p4dialog4,
							data.string.p4dialog4part1, 
							data.string.p4dialog4part2,
							data.string.p4dialog4part3,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);

		
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);




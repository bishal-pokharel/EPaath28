$('.title').text(data.string.title);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/images/exercise1/combustion.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/images/exercise1/deforestation.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/images/exercise1/urbanization.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/images/exercise1/urban.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/images/exercise1/industrialization.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/images/exercise1/glacier.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/images/exercise1/drought.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/images/exercise1/desertification.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/images/exercise1/environmentalist.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/images/exercise1/phenomenon.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/images/exercise1/emission.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/images/exercise1/adverse.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/images/exercise1/consequence.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/images/exercise1/organic.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/images/exercise1/fertilizer.jpg",
			},//
			];
			
			//console.log(content);
			displaycontent();
			ole.footerNotificationHandler.pageEndSetNotification();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		$(this).toggleClass('active');
	}); 
});
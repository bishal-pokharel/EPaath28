var imgpath = $ref+"/images/";

var content=[
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"question1",
				textdata: data.string.ex1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"solar.jpg"
					}
				]
			}
		],

	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q2",
				textdata: data.string.ex2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"agriculture.jpg"
					}
				]
			}
		]
	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q3",
				textdata: data.string.ex3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"melt.jpg"
					}
				]
			}
		]
	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q4",
				textdata: data.string.ex4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"fertile.jpg"
					}
				]
			}
		]
	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q5",
				textdata: data.string.ex5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"roof.jpg"
					}
				]
			}
		]
	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q6",
				textdata: data.string.ex6
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"green.jpg"
					}
				]
			}
		]
	},
	{	
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Click on the correct answer"
			},
			{
				textclass:"q7",
				textdata: data.string.ex7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"subwrapper",
						imgsrc: imgpath+"kir.jpg"
					}
				]
			}
		]
	},
	{
		contentnocenteradjust: true,
		uppertextblock:[
			{
				textclass:"title",
				textdata: "Congratulations on finishing the exercise"
			}
		]
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	console.log(countNext);
/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;		
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	
	
	
	var answered = false;
	var attemptcount = 0;
		
	var totalq = content.length;
	var correctlyanswered = 0;
	
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		//call instruction block controller
		// instructionblockcontroller($board);
		var answered = false;
		$(".yes").on("click", function(){ 
			if(answered){
				return answered;
			}
			attemptcount++;
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$(this).addClass("correct");
			$nextBtn.show(0);
			play_correct_incorrect_sound(true); 
		});

		$(".no").on("click", function(){ 
			if(answered){
				return answered;
			}
			attemptcount++;
			$(this).addClass("incorrect");
			play_correct_incorrect_sound(false); 
		});
		
		if(countNext == ($total_page-1)){
			$(".title").html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
		}

	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/
		/*switch(countNext){
			case 0 : sometemplate()); break;
			.
			.
			.
			case 5 : someothertemplate(); break;
			default : break;
		}*/

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		answered = false;
		attemptcount = 0;
		countNext++;		
		templateCaller();
		console.log(countNext);
		
		
			
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */
		/*countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;*/			
		
		
	});

/*=====  End of Templates Controller Block  ======*/
});
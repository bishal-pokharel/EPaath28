(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2},
		{ans : data.string.name3,correct : "correct"},
		]	
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name4},
		{ans : data.string.name5,correct : "correct"},
		{ans : data.string.name6},
		]
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name7},
		{ans : data.string.name8},
		{ans : data.string.name9,correct : "correct"},
		]
		
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name10},
		{ans : data.string.name11},
		{ans : data.string.name12,correct : "correct"},
		]
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name13,correct : "correct"},
		{ans : data.string.name14},
		{ans : data.string.name15},
		]
	},
	
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name16},
		{ans : data.string.name17,correct : "correct"},
		{ans : data.string.name18},
		]
	},
	
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name19,correct : "correct"},
		{ans : data.string.name20},
		{ans : data.string.name21},
		]
	},
	
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name22},
		{ans : data.string.name23},
		{ans : data.string.name24,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name25,correct : "correct"},
		{ans : data.string.name26},
		{ans : data.string.name27},
		]
	},

	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name28},
		{ans : data.string.name29,
			correct : "correct"},
		{ans : data.string.name30},
		]
	},

	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name31,correct : "correct"},
		{ans : data.string.name32},
		{ans : data.string.name33},
		]
	},

	{
		question : data.string.q12,
		answers: [
		{ans : data.string.name34},
		{ans : data.string.name35},
		{ans : data.string.name36,correct : "correct"},
		]
	},

	{
		question : data.string.q13,
		answers: [
		{ans : data.string.name37,correct : "correct"},
		{ans : data.string.name38},
		{ans : data.string.name39},
		]
	},
	
	]


	$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<13){
			qA();
		}
		else if (questionCount==13){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

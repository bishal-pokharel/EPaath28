imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page6/";
background_image = $ref+"/images/"

var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog2 = new buzz.sound(soundAsset+"2.ogg");
var dialog3 = new buzz.sound(soundAsset+"3.ogg");
var dialog4 = new buzz.sound(soundAsset+"4.ogg");
var dialog5 = new buzz.sound(soundAsset+"5.ogg");
var dialog6 = new buzz.sound(soundAsset+"6.ogg");
var dialog7 = new buzz.sound(soundAsset+"7.ogg");
var dialog8 = new buzz.sound(soundAsset+"8.ogg");



var soundcontent = [dialog1,dialog2,dialog3,dialog4,dialog5,dialog6,dialog7,dialog8];


var content=[
	{
		bgImgSrc : imageAsset+"pro.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		forwhichdialog : "dialog1main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog1
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"tea.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		forwhichdialog : "dialog2main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog2],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
		
		
	},
	
	{
		bgImgSrc : imageAsset+"fund.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog3],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"bg.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog4],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"ch.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_5main",
		forwhichdialog : "dialog4main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog5],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"tr.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog5main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog6],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"named.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog6main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog7],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"lai.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog7main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog8],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},

];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 8;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




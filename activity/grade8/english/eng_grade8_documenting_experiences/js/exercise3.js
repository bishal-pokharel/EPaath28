(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q01,
		answers: [
		{ans : data.string.name01},
		{ans : data.string.name02,correct : "correct"},
		{ans : data.string.name03},
		]	
	},
	{
		question : data.string.q02,
		answers: [
		{ans : data.string.name04},
		{ans : data.string.name05},
		{ans : data.string.name06,correct : "correct"},
		]
	},
	{
		question : data.string.q03,
		answers: [
		{ans : data.string.name07,correct : "correct"},
		{ans : data.string.name08},
		{ans : data.string.name09},
		]
		
	},
	{
		question : data.string.q04,
		answers: [
		{ans : data.string.name010,correct : "correct"},
		{ans : data.string.name011},
		{ans : data.string.name012},
		]
	},
	{
		question : data.string.q05,
		answers: [
		{ans : data.string.name013},
		{ans : data.string.name014,correct : "correct"},
		{ans : data.string.name015},
		]
	},
	
	{
		question : data.string.q06,
		answers: [
		{ans : data.string.name016},
		{ans : data.string.name017},
		{ans : data.string.name018,correct : "correct"},
		]
	},
	
	{
		question : data.string.q07,
		answers: [
		{ans : data.string.name019,correct : "correct"},
		{ans : data.string.name020},
		{ans : data.string.name021},
		]
	},
	
	{
		question : data.string.q08,
		answers: [
		{ans : data.string.name022,correct : "correct"},
		{ans : data.string.name023},
		{ans : data.string.name024},
		]
	},
	
	{
		question : data.string.q09,
		answers: [
		{ans : data.string.name025},
		{ans : data.string.name026},
		{ans : data.string.name027,correct : "correct"},
		]
	},

	{
		question : data.string.q010,
		answers: [
		{ans : data.string.name028},
		{ans : data.string.name029,correct : "correct"},
		{ans : data.string.name030},
		]
	},

	{
		question : data.string.q011,
		answers: [
		{ans : data.string.name031},
		{ans : data.string.name032,correct : "correct"},
		{ans : data.string.name033},
		]
	},

	{
		question : data.string.q012,
		answers: [
		{ans : data.string.name034},
		{ans : data.string.name035},
		{ans : data.string.name036,correct : "correct"},
		]
	},

	{
		question : data.string.q013,
		answers: [
		{ans : data.string.name037},
		{ans : data.string.name038},
		{ans : data.string.name039,correct : "correct"},
		]
	},

	{
		question : data.string.q014,
		answers: [
		{ans : data.string.name040},
		{ans : data.string.name041,correct : "correct"},
		{ans : data.string.name042},
		]
	}
	];


	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<14){
			qA();
		}
		else if (questionCount==14){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			
			// $(".finishtxt").show(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			// $('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				// "position": "absolute",
				// "top": "48%",
				// "transform": "translateY(-50%)"
			// });
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
	
	$(".closebtn").click(function(){
		ole.activityComplete.finishingcall();
		
	});
	

	
})(jQuery);

(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q001,
		answers: [
		{ans : data.string.name001},
		{ans : data.string.name002,correct : "correct"},
		{ans : data.string.name003},
		]	
	},
	{
		question : data.string.q002,
		answers: [
		{ans : data.string.name004,correct : "correct"},
		{ans : data.string.name005},
		{ans : data.string.name006},
		]
	},
	{
		question : data.string.q003,
		answers: [
		{ans : data.string.name007},
		{ans : data.string.name008},
		{ans : data.string.name009,correct : "correct"},
		]
		
	},
	{
		question : data.string.q004,
		answers: [
		{ans : data.string.name0010},
		{ans : data.string.name0011},
		{ans : data.string.name0012,correct : "correct"},
		]
	},
	{
		question : data.string.q005,
		answers: [
		{ans : data.string.name0013},
		{ans : data.string.name0014},
		{ans : data.string.name0015,correct : "correct"},
		]
	},
	
	{
		question : data.string.q06,
		answers: [
		{ans : data.string.name0016,correct : "correct"},
		{ans : data.string.name0017},
		{ans : data.string.name0018},
		]
	},
	
	{
		question : data.string.q007,
		answers: [
		{ans : data.string.name0019},
		{ans : data.string.name0020,correct : "correct"},
		{ans : data.string.name0021},
		]
	},

	{
		question : data.string.q008,
		answers: [
		{ans : data.string.name0022,correct : "correct"},
		{ans : data.string.name0023},
		{ans : data.string.name0024},
		]
	},

	{
		question : data.string.q009,
		answers: [
		{ans : data.string.name0025},
		{ans : data.string.name0026,correct : "correct"},
		{ans : data.string.name0027},
		]
	},

	{
		question : data.string.q0010,
		answers: [
		{ans : data.string.name0028},
		{ans : data.string.name0029,correct : "correct"},
		{ans : data.string.name0030},
		]
	},

	{
		question : data.string.q0011,
		answers: [
		{ans : data.string.name0031},
		{ans : data.string.name0032},
		{ans : data.string.name0033,correct : "correct"},
		]
	},

	{
		question : data.string.q0012,
		answers: [
		{ans : data.string.name0034},
		{ans : data.string.name0035,correct : "correct"},
		{ans : data.string.name0036},
		]
	},

	{
		question : data.string.q0013,
		answers: [
		{ans : data.string.name0037},
		{ans : data.string.name0038,correct : "correct"},
		{ans : data.string.name0039},
		]
	},

	{
		question : data.string.q0014,
		answers: [
		{ans : data.string.name0040,correct : "correct"},
		{ans : data.string.name0041},
		{ans : data.string.name0042},
		]
	},

	{
		question : data.string.q0015,
		answers: [
		{ans : data.string.name0043},
		{ans : data.string.name0044},
		{ans : data.string.name0045,correct : "correct"},
		]
	},

	{
		question : data.string.q0016,
		answers: [
		{ans : data.string.name0046},
		{ans : data.string.name0047},
		{ans : data.string.name0048,correct : "correct"},
		]
	},

	{
		question : data.string.q0017,
		answers: [
		{ans : data.string.name0049},
		{ans : data.string.name0050,correct : "correct"},
		{ans : data.string.name0051},
		]
	},

	{
		question : data.string.q0018,
		answers: [
		{ans : data.string.name0052,correct : "correct"},
		{ans : data.string.name0053},
		{ans : data.string.name0054},
		]
	},

	{
		question : data.string.q0019,
		answers: [
		{ans : data.string.name0055},
		{ans : data.string.name0056},
		{ans : data.string.name0057,correct : "correct"},
		]
	},
	]


	$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<19){
			qA();
		}
		else if (questionCount==19){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').text("Congratulation on finishing your exercise").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

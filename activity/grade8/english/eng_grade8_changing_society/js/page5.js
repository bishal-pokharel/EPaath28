imageAsset = $ref+"/images/page1/";
soundAsset = $ref+"/sounds/page1/";

var dialog1part1 = new buzz.sound(soundAsset+"man1.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"man1.ogg");
var dialog1part3 = new buzz.sound(soundAsset+"man1.ogg");
var dialog1part4 = new buzz.sound(soundAsset+"man1.ogg");
var dialog1 = [dialog1part1,dialog1part2,dialog1part3,dialog1part4];

var dialog2 = new buzz.sound(soundAsset+"man1.ogg");
var soundcontent = [dialog1, dialog2];

var content=[
	{
		bgImgSrc : imageAsset+"1.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p5dialog1part1,
							data.string.p5dialog1part2,
							data.string.p5dialog1part3,
							data.string.p5dialog1part4],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"2.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "",
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p5dialogpart1,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
imageAsset = $ref+"/images/page1/";
soundAsset = $ref+"/sounds/page2/";

var dialogpart1 = new buzz.sound(soundAsset+"1_1.ogg");
var dialogpart2 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog1 = [dialogpart1,dialogpart2];

var dialogpart3 = new buzz.sound(soundAsset+"2_1.ogg");
var dialogpart4 = new buzz.sound(soundAsset+"2_2.ogg");
var dialog2 = [dialogpart3,dialogpart4];

var dialogpart5 = new buzz.sound(soundAsset+"3_1.ogg");
var dialogpart6 = new buzz.sound(soundAsset+"3_2.ogg");
var dialog3 = [dialogpart5,dialogpart6];

var dialogpart7 = new buzz.sound(soundAsset+"4_1.ogg");
var dialogpart8 = new buzz.sound(soundAsset+"4_2.ogg");
var dialog4 = [dialogpart7,dialogpart8];

var dialogpart9 = new buzz.sound(soundAsset+"5_1.ogg");
var dialogpart10 = new buzz.sound(soundAsset+"5_2.ogg");
var dialog5 = [dialogpart9,dialogpart10];

var dialogpart11 = new buzz.sound(soundAsset+"6_1.ogg");
var dialogpart12 = new buzz.sound(soundAsset+"6_2.ogg");
var dialog6 = [dialogpart11,dialogpart12];

var soundcontent = [dialog1, dialog2, dialog3, dialog4,
					dialog5,dialog6];


var content=[
	{
		bgImgSrc : imageAsset+"5.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p2dialogpart1,
							data.string.p2dialogpart2],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"4.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "",
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p2dialogpart3,
						   data.string.p2dialogpart4,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"3.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p2dialogpart5,
							data.string.p2dialogpart6,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"20.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p2dialogpart7,
						   data.string.p2dialogpart8,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"21.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p2dialogpart9,
						   data.string.p2dialogpart10,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"11.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [data.string.p2dialogpart11,
						   data.string.p2dialogpart12,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		questionend: true,
		questiondata: data.string.s2_q1,
		answerdata: data.string.s2_q1_a1
	}
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		if(countNext == ($total_page-1)){
			$(".click_for_ref").click(function(){
				$(".answer").show(0);
				$(this).addClass("clicked");
				$prevBtn.show(0);
	     		ole.footerNotificationHandler.pageEndSetNotification();
			});
		} else {
			var $slide = $board.children('div');
			var $dialogcontainer = $slide.children('div.dialogcontainer');
			var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
			var $paralines = $dialogcontainer.children('p').children('span');
			var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
				
				if($.isArray(soundcontent[countNext])){
					playThisDialog = soundcontent[countNext];
					playThisDialog[0].play();
				}
				else if(!$.isArray(soundcontent[countNext])){
					playThisDialog = [soundcontent[countNext]];
					playThisDialog[0].play();
				}
				
	
			$listenAgainButton.on('click',  function() {
				/* Act on the event */
				$paralinestohideonListenAgain.css('display', 'none');
				playThisDialog[0].play();
				$nextBtn.hide(0);
		     	$prevBtn.hide(0);
				$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
			});
	
			/*this function binds appropriate events handlers to the 
			audio as required*/
			function playerbinder(){
					$.each(playThisDialog, function( index, entry ) { 
						if(index < playThisDialog.length-1){
						 	entry.bind('ended', function(){
						 		// alert(index +"sound ended");
						 		$paralines.eq(index+1).fadeIn(400);
						 		playThisDialog[index+1].play();
						 	});
						}
	
						if(index == playThisDialog.length-1){
						 	entry.bind("ended", function() {	
							$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');
	
					     	if(countNext > 0 && countNext < $total_page-1){
					     		$nextBtn.show(0);
					     		$prevBtn.show(0);
					     	}
	
					     	else if(countNext < 1){
					     		$nextBtn.show(0);
					     	}
	
					     	else if(countNext >= $total_page-1){
					     		$prevBtn.show(0);
					     		ole.footerNotificationHandler.pageEndSetNotification();
					     	}
						});
						}
					});
				}
	
				playerbinder();
		}
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
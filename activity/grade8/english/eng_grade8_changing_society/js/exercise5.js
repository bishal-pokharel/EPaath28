(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q01,
		answers: [
		{ans : data.string.name01},
		{ans : data.string.name02},
		{ans : data.string.name03,correct : "correct"},
		]	
	},
	{
		question : data.string.q02,
		answers: [
		{ans : data.string.name04,correct : "correct"},
		{ans : data.string.name05},
		{ans : data.string.name06},
		]
	},
	{
		question : data.string.q03,
		answers: [
		{ans : data.string.name07},
		{ans : data.string.name08},
		{ans : data.string.name09,correct : "correct"},
		]
		
	},
	{
		question : data.string.q04,
		answers: [
		{ans : data.string.name010},
		{ans : data.string.name011,correct : "correct"},
		{ans : data.string.name012},
		]
	},
	{
		question : data.string.q05,
		answers: [
		{ans : data.string.name013},
		{ans : data.string.name014,correct : "correct"},
		{ans : data.string.name015},
		]
	},
	
	{
		question : data.string.q06,
		answers: [
		{ans : data.string.name016},
		{ans : data.string.name017},
		{ans : data.string.name018,correct : "correct"},
		]
	},
	
	{
		question : data.string.q07,
		answers: [
		{ans : data.string.name019,correct : "correct"},
		{ans : data.string.name020},
		{ans : data.string.name021},
		]
	},
	]


	$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<7){
			qA();
		}
		else if (questionCount==7){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').text("Congratulation on finishing your exercise").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

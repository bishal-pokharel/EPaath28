(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q001,
		answers: [
		{ans : data.string.name001,correct : "correct"},
		{ans : data.string.name002},
		{ans : data.string.name003},
		]	
	},
	{
		question : data.string.q002,
		answers: [
		{ans : data.string.name004},
		{ans : data.string.name005},
		{ans : data.string.name006,correct : "correct"},
		]
	},
	{
		question : data.string.q003,
		answers: [
		{ans : data.string.name007},
		{ans : data.string.name008},
		{ans : data.string.name009,correct : "correct"},
		]
		
	},
	{
		question : data.string.q004,
		answers: [
		{ans : data.string.name0010},
		{ans : data.string.name0011,correct : "correct"},
		{ans : data.string.name0012},
		]
	},
	{
		question : data.string.q005,
		answers: [
		{ans : data.string.name0013,correct : "correct"},
		{ans : data.string.name0014},
		{ans : data.string.name0015},
		]
	},
	
	{
		question : data.string.q06,
		answers: [
		{ans : data.string.name0016},
		{ans : data.string.name0017,correct : "correct"},
		{ans : data.string.name0018},
		]
	},
	
	{
		question : data.string.q007,
		answers: [
		{ans : data.string.name0019,correct : "correct"},
		{ans : data.string.name0020},
		{ans : data.string.name0021},
		]
	},

	{
		question : data.string.q008,
		answers: [
		{ans : data.string.name0022},
		{ans : data.string.name0023},
		{ans : data.string.name0024,correct : "correct"},
		]
	},

	{
		question : data.string.q009,
		answers: [
		{ans : data.string.name0025},
		{ans : data.string.name0026},
		{ans : data.string.name0027,correct : "correct"},
		]
	},
	]


	$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').text("Congratulation on finishing your exercise").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q19,
		answers: [
		{ans : data.string.name55},
		{ans : data.string.name56},
		{ans : data.string.name57,correct : "correct"},
		]	
	},
	{
		question : data.string.q20,
		answers: [
		{ans : data.string.name58},
		{ans : data.string.name59,correct : "correct"},
		{ans : data.string.name60},
		]
	},
	{
		question : data.string.q21,
		answers: [
		{ans : data.string.name61},
		{ans : data.string.name62},
		{ans : data.string.name63,correct : "correct"},
		]
		
	},
	{
		question : data.string.q22,
		answers: [
		{ans : data.string.name64},
		{ans : data.string.name65},
		{ans : data.string.name66,correct : "correct"},
		]
	},
	{
		question : data.string.q23,
		answers: [
		{ans : data.string.name67},
		{ans : data.string.name68,correct : "correct"},
		{ans : data.string.name69},
		]
	},
	
	{
		question : data.string.q24,
		answers: [
		{ans : data.string.name70,correct : "correct"},
		{ans : data.string.name71},
		{ans : data.string.name72},
		]
	},
	
	{
		question : data.string.q25,
		answers: [
		{ans : data.string.name73},
		{ans : data.string.name74},
		{ans : data.string.name75,correct : "correct"},
		]
	},
	
	{
		question : data.string.q26,
		answers: [
		{ans : data.string.name76},
		{ans : data.string.name77},
		{ans : data.string.name78,correct : "correct"},
		]
	},
	
	{
		question : data.string.q27,
		answers: [
		{ans : data.string.name79,correct : "correct"},
		{ans : data.string.name80},
		{ans : data.string.name81},
		]
	}
	
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
	
	

(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name28},
		{ans : data.string.name29},
		{ans : data.string.name30,correct : "correct"},
		]	
	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name31},
		{ans : data.string.name32,correct : "correct"},
		{ans : data.string.name33},
		]
	},
	{
		question : data.string.q12,
		answers: [
		{ans : data.string.name34},
		{ans : data.string.name35},
		{ans : data.string.name36,correct : "correct"},
		]
		
	},
	{
		question : data.string.q13,
		answers: [
		{ans : data.string.name37,correct : "correct"},
		{ans : data.string.name38},
		{ans : data.string.name39},
		]
	},
	{
		question : data.string.q14,
		answers: [
		{ans : data.string.name40},
		{ans : data.string.name41},
		{ans : data.string.name42,correct : "correct"},
		]
	},
	
	{
		question : data.string.q15,
		answers: [
		{ans : data.string.name43},
		{ans : data.string.name44,correct : "correct"},
		{ans : data.string.name45},
		]
	},
	
	{
		question : data.string.q16,
		answers: [
		{ans : data.string.name46},
		{ans : data.string.name47,correct : "correct"},
		{ans : data.string.name48},
		]
	},
	
	{
		question : data.string.q17,
		answers: [
		{ans : data.string.name49,correct : "correct"},
		{ans : data.string.name50},
		{ans : data.string.name51},
		]
	},
	
	{
		question : data.string.q18,
		answers: [
		{ans : data.string.name52},
		{ans : data.string.name53},
		{ans : data.string.name54,correct : "correct"},
		]
	}
	
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
	
	

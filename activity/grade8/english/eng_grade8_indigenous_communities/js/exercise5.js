(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q28,
		answers: [
		{ans : data.string.name82},
		{ans : data.string.name83},
		{ans : data.string.name84,correct : "correct"},
		]	
	},
	{
		question : data.string.q29,
		answers: [
		{ans : data.string.name85},
		{ans : data.string.name86},
		{ans : data.string.name87,correct : "correct"},
		]
	},
	{
		question : data.string.q30,
		answers: [
		{ans : data.string.name88},
		{ans : data.string.name89,correct : "correct"},
		{ans : data.string.name90},
		]
		
	},
	{
		question : data.string.q31,
		answers: [
		{ans : data.string.name91},
		{ans : data.string.name92},
		{ans : data.string.name93,correct : "correct"},
		]
	},
	{
		question : data.string.q32,
		answers: [
		{ans : data.string.name94,correct : "correct"},
		{ans : data.string.name95},
		{ans : data.string.name96},
		]
	},
	
	{
		question : data.string.q33,
		answers: [
		{ans : data.string.name97,correct : "correct"},
		{ans : data.string.name98},
		{ans : data.string.name99},
		]
	},
	
	{
		question : data.string.q34,
		answers: [
		{ans : data.string.name100},
		{ans : data.string.name101},
		{ans : data.string.name102,correct : "correct"},
		]
	},
	
	{
		question : data.string.q35,
		answers: [
		{ans : data.string.name103},
		{ans : data.string.name104,correct : "correct"},
		{ans : data.string.name105},
		]
	},
	
	{
		question : data.string.q36,
		answers: [
		{ans : data.string.name106,correct : "correct"},
		{ans : data.string.name107},
		{ans : data.string.name108},
		]
	}
	
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
	
	

(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q37,
		answers: [
		{ans : data.string.name109},
		{ans : data.string.name110},
		{ans : data.string.name111,correct : "correct"},
		]	
	},
	{
		question : data.string.q38,
		answers: [
		{ans : data.string.name112},
		{ans : data.string.name113},
		{ans : data.string.name114,correct : "correct"},
		]
	},
	{
		question : data.string.q39,
		answers: [
		{ans : data.string.name115},
		{ans : data.string.name116,correct : "correct"},
		{ans : data.string.name117},
		]
		
	},
	{
		question : data.string.q40,
		answers: [
		{ans : data.string.name118,correct : "correct"},
		{ans : data.string.name119},
		{ans : data.string.name120},
		]
	},
	{
		question : data.string.q41,
		answers: [
		{ans : data.string.name121},
		{ans : data.string.name123,correct : "correct"},
		{ans : data.string.name124},
		]
	},
	
	{
		question : data.string.q42,
		answers: [
		{ans : data.string.name125,correct : "correct"},
		{ans : data.string.name126},
		{ans : data.string.name127},
		]
	},
	
	{
		question : data.string.q43,
		answers: [
		{ans : data.string.name128},
		{ans : data.string.name129},
		{ans : data.string.name130,correct : "correct"},
		]
	},
	
	{
		question : data.string.q44,
		answers: [
		{ans : data.string.name131,correct : "correct"},
		{ans : data.string.name132},
		{ans : data.string.name133},
		]
	}
		
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".finishtxt").show(0);
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			//$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			$('.closebtn').click(function(){
				ole.activityComplete.finishingcall();
			});
			//$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.");
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
})(jQuery);
	
	

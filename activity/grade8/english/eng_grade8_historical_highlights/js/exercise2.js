(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.name19},
		{ans : data.string.name20,correct : "correct"},
		]	
	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.name21},
		{ans : data.string.name22,correct : "correct"},
		
		]
	},
	{
		question : data.string.q12,
		answers: [
		{ans : data.string.name23},
		{ans : data.string.name24,correct : "correct"},
		]
		
	},
	{
		question : data.string.q13,
		answers: [
		{ans : data.string.name25},
		{ans : data.string.name26,correct : "correct"},
		]
	},
	{
		question : data.string.q14,
		answers: [
		{ans : data.string.name27},
		{ans : data.string.name28,correct : "correct"},
		
		]
	},
	{
		question : data.string.q15,
		answers: [
		{ans : data.string.name29,correct : "correct"},
		{ans : data.string.name30},
		
		]
	},
	{
		question : data.string.q16,
		answers: [
		{ans : data.string.name31},
		{ans : data.string.name32,correct : "correct"},
		
		]
	},
	{
		question : data.string.q17,
		answers: [
		{ans : data.string.name33,correct : "correct"},
		{ans : data.string.name34},
		]
	},
	{
		question : data.string.q18,
		answers: [
		{ans : data.string.name35},
		{ans : data.string.name36,correct : "correct"},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// $('.title').text("Congratulation on finishing your exercise").css({
					// "position": "absolute",
					// "top": "48%",
					// "transform": "translateY(-50%)"
			// });
			$('.finishtxt').append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);

		}
	});

	$('.closebtn').click(function(){
	  ole.activityComplete.finishingcall();
	});
})(jQuery);
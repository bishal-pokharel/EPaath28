(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q1,
		answers: [
		{ans : data.string.name1},
		{ans : data.string.name2,correct : "correct"},
		]	
	},
	{
		question : data.string.q2,
		answers: [
		{ans : data.string.name3,correct : "correct"},
		{ans : data.string.name4},
		
		]
	},
	{
		question : data.string.q3,
		answers: [
		{ans : data.string.name5},
		{ans : data.string.name6,correct : "correct"},
		]
		
	},
	{
		question : data.string.q4,
		answers: [
		{ans : data.string.name7,correct : "correct"},
		{ans : data.string.name8},
		]
	},
	{
		question : data.string.q5,
		answers: [
		{ans : data.string.name9,correct : "correct"},
		{ans : data.string.name10},
		
		]
	},
	{
		question : data.string.q6,
		answers: [
		{ans : data.string.name11,correct : "correct"},
		{ans : data.string.name12},
		
		]
	},
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.name13},
		{ans : data.string.name14,correct : "correct"},
		
		]
	},
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.name15,correct : "correct"},
		{ans : data.string.name16},
		]
	},
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.name17,correct : "correct"},
		{ans : data.string.name18},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
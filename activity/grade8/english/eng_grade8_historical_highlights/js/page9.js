imageAsset = $ref+"/images/page2/";
soundAsset = $ref+"/sounds/page2/";

var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog2 = new buzz.sound(soundAsset+"2.ogg");
var dialog3 = new buzz.sound(soundAsset+"3.ogg");


var soundcontent = [dialog1, dialog2, dialog3];

var content=[
	{
		bgImgSrc : imageAsset+"page2a.jpg",
		forwhichdialog : "dialog1",
		dialog_id : "first_diag",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [{
								datahighlightflag: true,
								span_id:"par1_slide",
								span_data: data.string.p9dialog1,	
							}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page2b.jpg",
		forwhichdialog : "dialog2",
		dialog_id : "second_diag",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "",
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
								
								span_id:"par2_slide",
								span_data: data.string.p9dialog2,	
							}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page2c.jpg",
		forwhichdialog : "dialog3",
		dialog_id : "third_diag",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "",
			},
		],
		lineCountDialog : [{
								
								span_id:"par3_slide",
								span_data: data.string.p9dialog3,	
							}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);

	$(".dict").click(function(){
		$(".information_box").show(0);
	});

	$(".close_info_box").click(function(){
		$(".information_box").hide(0);
		$(".dict").show(0);
	});

	 function texthighlight($highlightinside){
	 		var spanid=0;
		    //check if $highlightinside is provided
		    typeof $highlightinside !== "object" ?
		    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		    null ;
		     
		    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		    var stylerulename;
		    var replaceinstring;
		    var texthighlightstarttag;
		    var texthighlightendtag   = "</span>";
		    if($alltextpara.length > 0){
		     $.each($alltextpara, function(index, val) { 
		     	spanid++;
		      /*if there is a data-highlightcustomclass attribute defined for the text element
		      use that or else use default 'parsedstring'*/    
		      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
		       (stylerulename = $(this).attr("data-highlightcustomclass")) :
		       (stylerulename = "wordmeaning") ;
		      texthighlightstarttag = "<span class='"+stylerulename+"' >";
		      replaceinstring       = $(this).html();
		      replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
		      replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
		      $(this).html(replaceinstring);
		     });
		}
   }

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		texthighlight($board);

		$(".wordmeaning").click(function(){
				$(".information_box").show(0);
				$(".dict").hide(0);
			});

		$(".firstlist").click(function(){
				$(".imgone").fadeIn(1500);
				$(".meaningone").fadeIn(1500);

		});

		//the code will run from here


		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
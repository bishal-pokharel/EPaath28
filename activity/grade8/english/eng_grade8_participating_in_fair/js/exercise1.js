
	var testsTexts = [ 
	{	
		test : data.string.d_5_3,
	},
	{
		test : data.string.d_5_4,
	},
	{
		test : data.string.d_5_5,
	},
	{
		test : data.string.d_5_6, 
	},
	{
		test : data.string.d_5_7,
	},
	{
		test : data.string.d_5_8,
	},
	{
		test : data.string.d_5_9, 
	},
	{
		test : data.string.d_5_10,
	},
	{
		test : data.string.d_5_11,
	},
	{
		test : data.string.d_5_11_1, 
	},
];
	
	var resultsTexts = [ 
	{	
		result : data.string.d_5_13,
	},
	{
		result : data.string.d_5_14,
	},
	{
		result : data.string.d_5_15,
	},
	{
		result : data.string.d_5_16,
	},
	{
		result : data.string.d_5_17,
	},
	{
		result : data.string.d_5_18,
	},
	{
		result : data.string.d_5_19,
	},
	{
		result : data.string.d_5_20,
	},
	{
		result : data.string.d_5_21,
	},
	{
		result : data.string.d_5_22,
	},
];

$(function(){
	$(".title").text(data.string.d_5_1);
	var testId =0;
	var resultId =0;
	
	testContents();
	function testContents(){
		var test1 =$.each(testsTexts,function(key,values){
		testId++;
		contents = values.test;
		console.log(contents);
		appendContents = '<p id="match' + testId + '" class="hoverclass">';
		appendContents += contents;
		appendContents +='</p>';
		$("#paragraph_contents").append(appendContents);
		});
	}

	resultContents();
	function resultContents(){
		var test2 =$.each(resultsTexts,function(key,values){
		resultId++;
		contents = values.result;
		console.log(contents);
		  
		appendContents = '<p id="match' + resultId + '" class="hoverclass">';
		appendContents += contents;
		appendContents +='</p>';
		$("#answer_contents").append(appendContents);
	});
		
		/*shuffle the answers*/
		var parent = $("#answer_contents");
    	var divs = parent.children();
    	while (divs.length) {
        	parent.append(divs.splice(Math.floor(Math.random() * divs.length), 1)[0]);
    	}
	}

	var test1 =  $("#paragraph_contents").find("p");
	var test2 =  $("#answer_contents").find("p");
	var result1;
	var result2;
	var rightCounter = 0;

	//Sony Tandukar
	var id1;
	test1.on("click", function(){ 
		if(!$(this).hasClass("hoverclass")){
			return true;
		}
		id1 = $(this).attr("id");
		result1 = id1;
		$('#paragraph_contents .hoverclass').removeClass('change_color_click');
		$(this).addClass('change_color_click');
		$("#answer1").text(result1);   
	});

	 test2.on("click", function(){ 
	 	if(result1 == ""){
	 		return true;
	 	}
		var id3 = $(this).attr("id");
		result2 = id3;
	
		if(result1 == result2){
			rightCounter++;
			$(this).addClass('correct').removeClass('hoverclass');
			$('#'+id1).addClass('correct').removeClass('hoverclass');
			play_correct_incorrect_sound(true); 
			result1 = "";
			if(rightCounter==10)
			{
				ole.footerNotificationHandler.pageEndSetNotification();
			}
		} else {
			play_correct_incorrect_sound(false);
		}
		$("#answer2").text(result2);
	});
});	
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q0000001,
		answers: [
		{ans : data.string.name0000001},
		{ans : data.string.name0000002,correct : "correct"},
		{ans : data.string.name0000003},
		]	
	},


	{
		question : data.string.q0000002,
		answers: [
		{ans : data.string.name0000004},
		{ans : data.string.name0000005,correct : "correct"},
		{ans : data.string.name0000006},
		]
	},
	{
		question : data.string.q0000003,
		answers: [
		{ans : data.string.name0000007,correct : "correct"},
		{ans : data.string.name0000008},
		{ans : data.string.name0000009},
		]
		
	},
	{
		question : data.string.q0000004,
		answers: [
		{ans : data.string.name00000010},
		{ans : data.string.name00000011},
		{ans : data.string.name00000012,correct : "correct"},
		]
	},
	{
		question : data.string.q0000005,
		answers: [
		{ans : data.string.name00000013,correct : "correct"},
		{ans : data.string.name00000014},
		{ans : data.string.name00000015},
		]
	},
		{
		question : data.string.q0000006,
		answers: [
		{ans : data.string.name00000016},
		{ans : data.string.name00000017,correct : "correct"},
		{ans : data.string.name00000018},
		]
	},	{
		question : data.string.q0000007,
		answers: [
		{ans : data.string.name00000019,correct : "correct"},
		{ans : data.string.name00000020},
		{ans : data.string.name00000021},
		]
	},
		{
		question : data.string.q0000008,
		answers: [
		{ans : data.string.name00000022},
		{ans : data.string.name00000023},
		{ans : data.string.name00000024,correct : "correct"},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();
	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
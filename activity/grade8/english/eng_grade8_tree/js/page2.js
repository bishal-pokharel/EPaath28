imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/";

var dialog0 = new buzz.sound(soundAsset+"Poem.ogg");
var dialog1 = new buzz.sound(soundAsset+"2.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"3.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"4.ogg");
var dialog1main = [dialog1,dialog1part1,dialog1part2];

var dialog2 = new buzz.sound(soundAsset+"5.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"6.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"7.ogg");
var dialog2main = [dialog2,dialog2part1,dialog2part2];


var dialog3 = new buzz.sound(soundAsset+"8.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog3main = [dialog3,dialog3part1];

var dialog4 = new buzz.sound(soundAsset+"10.ogg");
var dialog4part1 = new buzz.sound(soundAsset+"11.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"12.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"13.ogg");
var dialog4main = [dialog4,dialog4part1,dialog4part2,dialog4part3];

var nat1 = new buzz.sound(soundAsset+"2.ogg");
var nat2 = new buzz.sound(soundAsset+"3.ogg");
var nat3 = new buzz.sound(soundAsset+"4.ogg");
var nat4 = new buzz.sound(soundAsset+"5.ogg");
var nat5 = new buzz.sound(soundAsset+"6.ogg");
var nat6 = new buzz.sound(soundAsset+"7.ogg");
var nat7 = new buzz.sound(soundAsset+"8.ogg");
var nat8 = new buzz.sound(soundAsset+"9.ogg");
var nat9 = new buzz.sound(soundAsset+"10.ogg");
var nat10 = new buzz.sound(soundAsset+"11.ogg");
var nat11 = new buzz.sound(soundAsset+"12.ogg");
var nat12 = new buzz.sound(soundAsset+"13.ogg");
var natmain = [nat1,nat2,nat3,nat4,nat5,nat6,nat7,nat8,nat9,nat10,nat11,nat12];




var soundcontent = [dialog0,dialog1main,dialog2main,dialog3main,dialog4main,natmain];

var content=[
    {
        bgImgSrc : imageAsset+"poem.png",
        imageid : "bg_image1",
        dialog_id : "dialog_1main",

        forwhichdialog : "dialog0",
        lineCountDialog : [
            {
                span_id:"span2",
                span_data: data.string.p1dialog0,
            }
        ],
        speakerImgSrc : $ref+"/images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"spring.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"span1",
							span_data: data.string.p1dialog1,	
						},
						{
							span_id:"span2",
							span_data: data.string.p1dialog1part1,	
						},
						{
							span_id:"span3",
							span_data: data.string.p1dialog1part2,	
						}
					],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"aut.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		span_id : "span2",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p2dialog2,	
						},
						{
							span_id:"span2",
							span_data: data.string.p2dialog2part1,	
						},
						{
							span_id:"span3",
							span_data: data.string.p2dialog2part2,	
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"snow.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		span_id : "span3",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1_slide2",
							span_data: data.string.p3dialog3,	
						},
						{
							span_id:"span2_slide3",
							span_data: data.string.p3dialog3part1,	
						},
						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"sum.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		span_id : "span3",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1_slide2",
							span_data: data.string.p4dialog,	
						},
						{
							span_id:"span2_slide3",
							span_data: data.string.p4dialog4part1,	
						},
						{
							span_id:"span2_slide3_1",
							span_data: data.string.p4dialog4part2,	
						},
						{
							span_id:"span3_slide4",
							span_data: data.string.p4dialog4part3,	
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"fi.jpg",
		imageid : "bg_image4",
		dialog_id : "natmain",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"span1_nat",
							span_data: data.string.p1dialog1,	
						},
						{
							span_id:"span2_nat",
							span_data: data.string.p1dialog1part1,	
						},
						{
							span_id:"span3_nat",
							span_data: data.string.p1dialog1part2,	
						},
						{
							span_id:"span4_nat",
							span_data: data.string.p2dialog2,	
						},

						{
							span_id:"span5_nat",
							span_data: data.string.p2dialog2part1,	
						},

						{
							span_id:"span6_nat",
							span_data: data.string.p2dialog2part2,	
						},

						{
							span_id:"span7_nat",
							span_data: data.string.p3dialog3,	
						},

						{
							span_id:"span8_nat",
							span_data: data.string.p3dialog3part1,	
						},

						{
							span_id:"span9_nat",
							span_data: data.string.p4dialog,	
						},

						{
							span_id:"span10_nat",
							span_data: data.string.p4dialog4part1,	
						},

						{
							span_id:"span11_nat",
							span_data: data.string.p4dialog4part2,	
						},

						{
							span_id:"span12_nat",
							span_data: data.string.p4dialog4part3,	
						}

						],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);

	
	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
          if( countNext==0){
          	$(".poet").show();
          	$("#span2").css("color","brown");
          }
          else{
              $(".poet").hide();
              $("#span2").css("color","white");
		  }

		/*The following code is all manual process on clicks--> Not a perfect solution thought, but
		a better solution for english*/	
			
		/*Rashik Maharjan*/
		/*This is national park section*/

		var test =  $("#natmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);
				$("#natmain").removeClass("opacity_image");

		});

		// test.on("click", function(){ 
		// 	var id = $(this).attr("id");
		// 	var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/chitwan.jpg')";
		// 	var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sagarmatha.jpg')";
		// 	var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/makalu.jpg')";
		// 	var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/langtang.jpg')";
		// 	var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/rara.jpg')";
		// 	var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/khaptad.jpg')";
		// 	var bg7 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pho.jpg')";
		// 	var bg8 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bardiya.jpg')";
		// 	var bg9 = "url('activity/grade8/english/eng_grade8_ecotourism/images/shiva.jpg')";
		// 	var bg10 = "url('activity/grade8/english/eng_grade8_ecotourism/images/banke.jpg')";

		// 	if (id == "span0_nat"){
		// 		$(".title").show(0);
		// 	}
			
		// 	if (id == "span2_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg1 ;
		// 		$("#imageblock").addClass("imageblock_chitwan");
		// 		$("#natmain").addClass("opacity_image");

		// 	}

		// 	if (id == "span3_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg2 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span4_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg3 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span5_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg4 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span6_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg5 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span7_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg6 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span8_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg7 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span9_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg8 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span10_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg9 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}

		// 	if (id == "span11_nat"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 		$("#natmain").addClass("opacity_image");
		// 	}
		// });
		
		/*This is wild life  park section*/
		
		var con =  $("#cnmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);
				
		});
		con.on("click", function(){ 
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/anna.jpg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/kan.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/man.jpg')";
			var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bla.jpg')";
			var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/ami.jpg')";
			var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/gauri.jpg')";

			
			if (id == "cn2_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				$("#imageblock").addClass("imageblock_chitwan");
				

			}

			if (id == "cn3_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;
				
			}

			if (id == "cn4_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;
				
			}
				if (id == "cn5_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg4 ;
				
			}

			if (id == "cn6_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg5 ;
				
			}

			if (id == "cn7_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg6 ;
				
			}



		});

		/*This is conservation section*/
		var wildlife =  $("#wildmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);
				
		});
		wildlife.on("click", function(){ 
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/parsa.jpg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/koshi.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sukla.jpg')";
			
			if (id == "wd2_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				$("#imageblock").addClass("imageblock_chitwan");
				

			}

			if (id == "wd3_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;
				
			}

			if (id == "wd4_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;
				
			}

		});

		/*World heritage*/
			var world =  $("#whmain").find("span");
				$(".close_imageblock").click(function(){
						$("#imageblock").fadeOut(500);
						
				});
		world.on("click", function(){ 
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_ecotourism/images/hun.jpeg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pt.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bt.jpg')";
			var bg4 = "url('activity/grade8/english/eng_grade8_ecotourism/images/pa.jpg')";
			var bg5 = "url('activity/grade8/english/eng_grade8_ecotourism/images/lu.jpg')";
			var bg6 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sw.jpg')";
			var bg7 = "url('activity/grade8/english/eng_grade8_ecotourism/images/bd.jpg')";
			var bg8 = "url('activity/grade8/english/eng_grade8_ecotourism/images/cna.jpg')";
			var bg9 = "url('activity/grade8/english/eng_grade8_ecotourism/images/chi.jpg')";
			var bg10 = "url('activity/grade8/english/eng_grade8_ecotourism/images/sagar.jpg')";
			
			if (id == "wh1"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				
				

			}

			if (id == "wh2"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;
				
			}

			if (id == "wh3"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;
			}

			if (id == "wh4"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg4 ;
			}


			if (id == "wh5"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg5 ;
			}


			if (id == "wh6"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg6 ;
			}


			if (id == "wh7"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg7 ;
			}


			if (id == "wh8"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg8 ;
			}


			if (id == "wh9"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg9 ;
			}


			if (id == "wh10"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg10 ;
			}

		});
			

		// 	var ecotouri =  $("#ecotou").find("span");
		// 		$(".close_imageblock").click(function(){
		// 				$("#imageblock").fadeOut(500);
						
		// 		});
		// ecotouri.on("click", function(){ 
		// 	var id = $(this).attr("id");
		// 	var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/hun.jpeg')";
		// 	var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/pt.jpg')";
		// 	var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/bt.jpg')";
		// 	var bg4 = "url('activity/grade8/english/eng_grade8_lesson8/images/pa.jpg')";
		// 	var bg5 = "url('activity/grade8/english/eng_grade8_lesson8/images/lu.jpg')";
		// 	var bg6 = "url('activity/grade8/english/eng_grade8_lesson8/images/sw.jpg')";
		// 	var bg7 = "url('activity/grade8/english/eng_grade8_lesson8/images/bd.jpg')";
		// 	var bg8 = "url('activity/grade8/english/eng_grade8_lesson8/images/cna.jpg')";
		// 	var bg9 = "url('activity/grade8/english/eng_grade8_lesson8/images/chi.jpg')";
		// 	var bg10 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg11 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg12 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg13 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg14 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg15 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg16 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg17 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg18 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
			
		// 	if (id == "ec1_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg1 ;
				
				

		// 	}

		// 	if (id == "ec2_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg2 ;
				
		// 	}

		// 	if (id == "ec3_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg3 ;
		// 	}

		// 	if (id == "ec4_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg4 ;
		// 	}


		// 	if (id == "ec5_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg5 ;
		// 	}


		// 	if (id == "ec6_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg6 ;
		// 	}


		// 	if (id == "ec7_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg7 ;
		// 	}


		// 	if (id == "ec8_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg8 ;
		// 	}


		// 	if (id == "ec9_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg9 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}

		// });

		/*End Rashik Maharjan*/


		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		console.log(countNext);	
		if(countNext == 1){
		    $(".leav").show(0);
		    $(".leav2").show(0);
			$(".butter").hide(0);
			// $(".butter1").hide(0);
			$(".bird").hide(0);

		}	

		if(countNext == 2){
		    $(".leav").hide(0);
		    $(".leav2").hide(0);
			$(".butter").hide(0);
			$(".butter1").hide(0);
			$(".bird").hide(0);
			$(".snow-container").show(0);
		}	

		if(countNext == 3){
		    $(".leav").hide(0);
		    $(".leav2").hide(0);
			$(".butter").show(0);
			$(".butter1").show(0);
			$(".bird").show(0);
			$(".snow-container").hide(0);
		}		
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);
		console.log(countNext);	
		if(countNext == 3)
		{
			$(".butter").hide(0);
			$(".butter1").hide(0);
			$(".bird").hide(0);
			$(".snow-container").show(0);
		}

		if(countNext == 2)
		{
			$(".butte1").show(0);
			$(".butter").hide(0);
			$(".bird").hide(0);
			$(".leav").show(0);
		    $(".leav2").show(0);
			$(".snow-container").hide(0);
		}

		if(countNext == 1)
		{
			$(".butter").show(0);
			$(".butter1").show(0);
			$(".bird").show(0);
			$(".leav").hide(0);
		    $(".leav2").hide(0);
			$(".snow-container").hide(0);
		}
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

});




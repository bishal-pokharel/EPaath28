(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q00000001,
		answers: [
		{ans : data.string.name00000001,correct : "correct"},
		{ans : data.string.name00000002},
		{ans : data.string.name00000003},
		]	
	},


	{
		question : data.string.q00000002,
		answers: [
		{ans : data.string.name00000004,correct : "correct"},
		{ans : data.string.name00000005},
		{ans : data.string.name00000006},
		]
	},
	{
		question : data.string.q00000003,
		answers: [
		{ans : data.string.name00000007},
		{ans : data.string.name00000008},
		{ans : data.string.name00000009,correct : "correct"},
		]
		
	},
	{
		question : data.string.q00000004,
		answers: [
		{ans : data.string.name000000010},
		{ans : data.string.name000000011,correct : "correct"},
		{ans : data.string.name000000012},
		]
	},
	{
		question : data.string.q00000005,
		answers: [
		{ans : data.string.name000000013,correct : "correct"},
		{ans : data.string.name000000014},
		{ans : data.string.name000000015},
		]
	},
		{
		question : data.string.q00000006,
		answers: [
		{ans : data.string.name000000016},
		{ans : data.string.name000000017,correct : "correct"},
		{ans : data.string.name000000018},
		]
	},	{
		question : data.string.q00000007,
		answers: [
		{ans : data.string.name000000019},
		{ans : data.string.name000000020},
		{ans : data.string.name000000021,correct : "correct"},
		]
	},
		{
		question : data.string.q00000008,
		answers: [
		{ans : data.string.name000000022,correct : "correct"},
		{ans : data.string.name000000023},
		{ans : data.string.name000000024},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		// new code 
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			//new code
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});
	


	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			$('.title').hide(0);
			//$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.");
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
	
	
	$(".closebtn").click(function(){
		
		ole.activityComplete.finishingcall();
		
	});
	
	

})(jQuery);


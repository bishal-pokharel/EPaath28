(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q000001,
		answers: [
		{ans : data.string.name000001,correct : "correct"},
		{ans : data.string.name000002},
		{ans : data.string.name000003},
		]	
	},


	{
		question : data.string.q000002,
		answers: [
		{ans : data.string.name000004,correct : "correct"},
		{ans : data.string.name000005},
		{ans : data.string.name000006},
		]
	},
	{
		question : data.string.q000003,
		answers: [
		{ans : data.string.name000007},
		{ans : data.string.name000008},
		{ans : data.string.name000009,correct : "correct"},
		]
		
	},
	{
		question : data.string.q000004,
		answers: [
		{ans : data.string.name0000010},
		{ans : data.string.name0000011,correct : "correct"},
		{ans : data.string.name0000012},
		]
	},
	{
		question : data.string.q000005,
		answers: [
		{ans : data.string.name0000013},
		{ans : data.string.name0000014,correct : "correct"},
		{ans : data.string.name0000015},
		]
	},
		{
		question : data.string.q000006,
		answers: [
		{ans : data.string.name0000016},
		{ans : data.string.name0000017},
		{ans : data.string.name0000018,correct : "correct"},
		]
	},	{
		question : data.string.q000007,
		answers: [
		{ans : data.string.name0000019,correct : "correct"},
		{ans : data.string.name0000020},
		{ans : data.string.name0000021},
		]
	},
		{
		question : data.string.q000008,
		answers: [
		{ans : data.string.name0000022},
		{ans : data.string.name0000023,correct : "correct"},
		{ans : data.string.name0000024},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q31,
		answers: [
		{ans : data.string.name37,correct : "correct"},
		{ans : data.string.name38},
		]	
	},
	{
		question : data.string.q32,
		answers: [
		{ans : data.string.name39},
		{ans : data.string.name40,correct : "correct"},
		]
	},
	{
		question : data.string.q33,
		answers: [
		{ans : data.string.name41,correct : "correct"},
		{ans : data.string.name42},
		]
		
	},
	{
		question : data.string.q34,
		answers: [
		{ans : data.string.name43},
		{ans : data.string.name44,correct : "correct"},
		]
	},
	{
		question : data.string.q35,
		answers: [
		{ans : data.string.name45},
		{ans : data.string.name46,correct : "correct"},
		]
	},
	
	{
		question : data.string.q36,
		answers: [
		{ans : data.string.name47},
		{ans : data.string.name48,correct : "correct"},
		]
	},
	
	{
		question : data.string.q37,
		answers: [
		{ans : data.string.name49,correct : "correct"},
		{ans : data.string.name50},
		]
	},
	
	{
		question : data.string.q38,
		answers: [
		{ans : data.string.name51},
		{ans : data.string.name52,correct : "correct"},
		]
	},
	
	{
		question : data.string.q39,
		answers: [
		{ans : data.string.name53},
		{ans : data.string.name54,correct : "correct"},
		]
	},

	{
		question : data.string.q40,
		answers: [
		{ans : data.string.name55},
		{ans : data.string.name56,correct : "correct"},
		]
	}
	
	];

$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		// new code 
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<10){
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').text("Congratulation on finishing your exercise").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);
	
	

(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q00001,
		answers: [
		{ans : data.string.name00001,correct : "correct"},
		{ans : data.string.name00002},
		{ans : data.string.name00003},
		]	
	},


	{
		question : data.string.q00002,
		answers: [
		{ans : data.string.name00004},
		{ans : data.string.name00005},
		{ans : data.string.name00006,correct : "correct"},
		]
	},
	{
		question : data.string.q00003,
		answers: [
		{ans : data.string.name00007,correct : "correct"},
		{ans : data.string.name00008},
		{ans : data.string.name00009},
		]
		
	},
	{
		question : data.string.q00004,
		answers: [
		{ans : data.string.name000010},
		{ans : data.string.name000011,correct : "correct"},
		{ans : data.string.name000012},
		]
	},
	{
		question : data.string.q00005,
		answers: [
		{ans : data.string.name000013},
		{ans : data.string.name000014,correct : "correct"},
		{ans : data.string.name000015},
		]
	},
		{
		question : data.string.q00006,
		answers: [
		{ans : data.string.name000016},
		{ans : data.string.name000017},
		{ans : data.string.name000018,correct : "correct"},
		]
	},	{
		question : data.string.q00007,
		answers: [
		{ans : data.string.name000019},
		{ans : data.string.name000020,correct : "correct"},
		{ans : data.string.name000021},
		]
	},
		{
		question : data.string.q00008,
		answers: [
		{ans : data.string.name000022,correct : "correct"},
		{ans : data.string.name000023},
		{ans : data.string.name000024},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
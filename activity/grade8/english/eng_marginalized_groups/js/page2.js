imageAsset = $ref+"/images/page1/";
soundAsset = $ref+"/sounds/page2/";

var dialog1part1 = new buzz.sound(soundAsset+"p2slide1a.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"p2slide1b.ogg");
var dialog1part3 = new buzz.sound(soundAsset+"p2slide1c.ogg");
var dialog1 = [dialog1part1,dialog1part2,dialog1part3];

var dialog2part1 = new buzz.sound(soundAsset+"p2slide2a.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"p2slide2b.ogg");
var dialog2part3 = new buzz.sound(soundAsset+"p2slide2c.ogg");
var dialog2part4 = new buzz.sound(soundAsset+"p2slide2d.ogg");
var dialog2part5 = new buzz.sound(soundAsset+"p2slide2e.ogg");
var dialog2part6 = new buzz.sound(soundAsset+"p2slide2f.ogg");
var dialog2part7 = new buzz.sound(soundAsset+"p2slide2g.ogg");

var dialog2 = [dialog2part1,dialog2part2,dialog2part3,dialog2part4,dialog2part5,dialog2part6,dialog2part7];

var dialog3part1 = new buzz.sound(soundAsset+"p2slide3a.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"p2slide3b.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"p2slide3c.ogg");
var dialog3 = [dialog3part1,dialog3part2,dialog3part3];



var dialog4part1 = new buzz.sound(soundAsset+"p2slide4a.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"p2slide4b.ogg");

var dialog4 = [dialog4part1,dialog4part2];



var soundcontent = [dialog1, dialog2, dialog3, dialog4];

var content=[
	{
		bgImgSrc : imageAsset+"page2.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p2dialog1part1,
							data.string.p2dialog1part2,
							data.string.p2dialog1part3],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page2.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p2dialog2part1,
							data.string.p2dialog2part2,
							data.string.p2dialog2part3,
							data.string.p2dialog2part4,
							data.string.p2dialog2part5,
							data.string.p2dialog2part6,
							data.string.p2dialog2part7,
							data.string.p2dialog2part8,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"page2.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p2dialog3part1,
							data.string.p2dialog3part2,
							data.string.p2dialog3part3,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


	{
		bgImgSrc : imageAsset+"page2.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p2dialog4part1,
							 data.string.p2dialog4part2,
							 ],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);
	
	$(".dict").click(function(){
		$(".information_box").show(0);
	});

	$(".close_info_box").click(function(){
		$(".information_box").hide(0);
		$(".dict").show(0);
	});
	
	
	 function texthighlight($highlightinside){
	 		var spanid=0;
		    //check if $highlightinside is provided
		    typeof $highlightinside !== "object" ?
		    alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		    null ;
		     
		    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		    var stylerulename;
		    var replaceinstring;
		    var texthighlightstarttag;
		    var texthighlightendtag   = "</span>";
		    if($alltextpara.length > 0){
		     $.each($alltextpara, function(index, val) { 
		     	spanid++;
		      /*if there is a data-highlightcustomclass attribute defined for the text element
		      use that or else use default 'parsedstring'*/    
		      $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
		       (stylerulename = $(this).attr("data-highlightcustomclass")) :
		       (stylerulename = "wordmeaning") ;
		      texthighlightstarttag = "<span class='"+stylerulename+"' >";
		      replaceinstring       = $(this).html();
		      replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
		      replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
		      $(this).html(replaceinstring);
		     });
		}
   }
	

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		
		
		$(".wordmeaning").click(function(){
				$(".information_box").show(0);
				$(".dict").hide(0);
			});

		$(".firstlist").click(function(){
				$(".imgone").fadeIn(1500);
				$(".meaningone").fadeIn(1500);
				$(".imgtwo").hide(0);
				$(".meaningtwo").hide(0);
			});
			
		$(".secondlist").click(function(){
				$(".imgtwo").fadeIn(1500);
				$(".meaningtwo").fadeIn(1500);
				$(".imgone").hide(0);
				$(".meaningone").hide(0);
			});
		
		
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
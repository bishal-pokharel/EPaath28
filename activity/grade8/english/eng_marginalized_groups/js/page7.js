imageAsset = $ref+"/images/page1/";
soundAsset = $ref+"/sounds/page7/";

var dialog1 = new buzz.sound(soundAsset+"p7slide1a.ogg");

var dialog2 = new buzz.sound(soundAsset+"p7slide2a.ogg");

var dialog3part1 = new buzz.sound(soundAsset+"p7slide3a.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"p7slide3b.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"p7slide3c.ogg");
var dialog3 = [dialog3part1,dialog3part2,dialog3part3];

var dialog4part1 = new buzz.sound(soundAsset+"p7slide4a.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"p7slide4b.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"p7slide4c.ogg");
var dialog4part4 = new buzz.sound(soundAsset+"p7slide4d.ogg");
var dialog4 = [dialog4part1,dialog4part2,dialog4part3,dialog4part4];


var dialog5part1 = new buzz.sound(soundAsset+"p7slide5a.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"p7slide5b.ogg");
var dialog5part3 = new buzz.sound(soundAsset+"p7slide5c.ogg");
var dialog5 = [dialog5part1,dialog5part2,dialog5part3];

var dialog6part1 = new buzz.sound(soundAsset+"p7slide6a.ogg");
var dialog6part2 = new buzz.sound(soundAsset+"p7slide6b.ogg");
var dialog6part3 = new buzz.sound(soundAsset+"p7slide6c.ogg");
var dialog6part4 = new buzz.sound(soundAsset+"p7slide6d.ogg");
var dialog6 = [dialog6part1,dialog6part2,dialog6part3,dialog6part4];

var soundcontent = [dialog1, dialog2, dialog3, dialog4, dialog5, dialog6];

var content=[
	{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog1,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog3part1,
							data.string.p7dialog3part2,
							data.string.p7dialog3part3,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog4part1,
							data.string.p7dialog4part2,
							data.string.p7dialog4part3,
							data.string.p7dialog4part4,
								],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

		{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog5part1,
							data.string.p7dialog5part2,
							data.string.p7dialog5part3,
							
								],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

		{
		bgImgSrc : imageAsset+"page7.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			
		],
		lineCountDialog : [data.string.p7dialog6part1,
							data.string.p7dialog6part2,
							data.string.p7dialog6part3,
							data.string.p7dialog6part4,
								],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
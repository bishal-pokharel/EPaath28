imageAsset = $ref+"/images/page1/";
soundAsset = $ref+"/sounds/page1/";

var dialog0 = new buzz.sound(soundAsset+"marginalizedgroups.ogg");
var dialog1 = new buzz.sound(soundAsset+"p1slide1.ogg");
var dialog2 = new buzz.sound(soundAsset+"p1slide2.ogg");
var dialog3 = new buzz.sound(soundAsset+"p1slide3.ogg");
var dialog4 = new buzz.sound(soundAsset+"p1slide4.ogg");
var dialog5 = new buzz.sound(soundAsset+"p1slide5.ogg");
var dialog6 = new buzz.sound(soundAsset+"p1slide6.ogg");
var dialog7 = new buzz.sound(soundAsset+"p1slide7.ogg");

var dialog8part1 = new buzz.sound(soundAsset+"p1slide8a.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"p1slide8b.ogg");
var dialog8part3 = new buzz.sound(soundAsset+"p1slide8c.ogg");
var dialog8part4 = new buzz.sound(soundAsset+"p1slide8d.ogg");
var dialog8 = [dialog8part1,dialog8part2,dialog8part3,dialog8part4];


var soundcontent = [dialog0,dialog1, dialog2, dialog3, dialog4,
					dialog5,dialog6,dialog7,dialog8];

var content=[
	{
		bgImgSrc : imageAsset+"marginalized_groups.png",
		forwhichdialog : "dialog0",
		lineCountDialog : [data.string.p1dialog0],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog1],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog2,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog3,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog4,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog5,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog6,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog7",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog7,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page1.jpg",
		forwhichdialog : "dialog8",
		talkHeadImages : [

		],
		lineCountDialog : [data.string.p1dialog8part1,
							data.string.p1dialog8part2,
							data.string.p1dialog8part3,
							data.string.p1dialog8part4,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 9;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');

			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}


		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');
		});

		/*this function binds appropriate events handlers to the
		audio as required*/
		function playerbinder(){
			if(countNext >= $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			else{
				$nextBtn.show(0);
			}
				$.each(playThisDialog, function( index, entry ) {
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();

	}

	slide1(countNext+=0);

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);
		ole.footerNotificationHandler.hideNotification();
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);

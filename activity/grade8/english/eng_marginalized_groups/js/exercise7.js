(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	
	//q-a
	{
		question : data.string.q0001,
		answers: [
		{ans : data.string.name0001},
		{ans : data.string.name0002,correct : "correct"},
		{ans : data.string.name0003},
		]	
	},
	//q-b
	{
		question : data.string.q0002,
		answers: [
		{ans : data.string.name0004,correct : "correct"},
		{ans : data.string.name0005},
		{ans : data.string.name0006},
		]
	},
	//q-c
	{
		question : data.string.q0003,
		answers: [
		{ans : data.string.name0007},
		{ans : data.string.name0008},
		{ans : data.string.name0009,correct : "correct"},
		]
		
	},
	//q-d
	{
		question : data.string.q0004,
		answers: [
		{ans : data.string.name00010},
		{ans : data.string.name00011},
		{ans : data.string.name00012,correct : "correct"},
		]
	},
	//q-e
	{
		question : data.string.q0005,
		answers: [
		{ans : data.string.name00013},
		{ans : data.string.name00014},
		{ans : data.string.name00015,correct : "correct"},
		]
	},
	
	//q-f
	{
		question : data.string.q0006,
		answers: [
		{ans : data.string.name00016,correct : "correct"},
		{ans : data.string.name00017},
		{ans : data.string.name00018},
		]
	},
	
	//q-g
	{
		question : data.string.q0007,
		answers: [
		{ans : data.string.name00019},
		{ans : data.string.name00020,correct : "correct"},
		{ans : data.string.name00021},
		]
	},
	
	//q-h
	{
		question : data.string.q0008,
		answers: [
		{ans : data.string.name00022,correct : "correct"},
		{ans : data.string.name00023},
		{ans : data.string.name00024},
		]
	},
	
	//q-i

	{
		question : data.string.q0009,
		answers: [
		{ans : data.string.name00025},
		{ans : data.string.name00026,correct : "correct"},
		{ans : data.string.name00027},
		]
	},
	
	//q-j
	{
		question : data.string.q00010,
		answers: [
		{ans : data.string.name00028},
		{ans : data.string.name00029,correct : "correct"},
		{ans : data.string.name00030},
		]
	},
	
	//q-k
	{
		question : data.string.q00011,
		answers: [
		{ans : data.string.name00031},
		{ans : data.string.name00032},
		{ans : data.string.name00033,correct : "correct"},
		]
	},
	
	//q-l
	{
		question : data.string.q00012,
		answers: [
		{ans : data.string.name00034},
		{ans : data.string.name00035,correct : "correct"},
		{ans : data.string.name00036},
		]
	},
	
	//q-m
	{
		question : data.string.q00013,
		answers: [
		{ans : data.string.name00037},
		{ans : data.string.name00038,correct : "correct"},
		{ans : data.string.name00039},
		]
	},
	
	//q-n
	{
		question : data.string.q00014,
		answers: [
		{ans : data.string.name00040,correct : "correct"},
		{ans : data.string.name00041},
		{ans : data.string.name00042},
		]
	},
	
	//q-o
	{
		question : data.string.q00015,
		answers: [
		{ans : data.string.name00043},
		{ans : data.string.name00044},
		{ans : data.string.name00045,correct : "correct"},
		]
	},
	
	//q-p
	{
		question : data.string.q00016,
		answers: [
		{ans : data.string.name00046},
		{ans : data.string.name00047},
		{ans : data.string.name00048,correct : "correct"},
		]
	},
	
	//q-q
	{
		question : data.string.q00017,
		answers: [
		{ans : data.string.name00049},
		{ans : data.string.name00050,correct : "correct"},
		{ans : data.string.name00051},
		]
	},
	
	//q-r
	{
		question : data.string.q00018,
		answers: [
		{ans : data.string.name00052,correct : "correct"},
		{ans : data.string.name00053},
		{ans : data.string.name00054},
		]
	},

	{
		question : data.string.q00019,
		answers: [
		{ans : data.string.name00055},
		{ans : data.string.name00056},
		{ans : data.string.name00057,correct : "correct"},
		]
	}
	];


	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

				// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<19){
			qA();
		}
		else if (questionCount==19){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

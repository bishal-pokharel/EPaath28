imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page4/";
background_image = $ref+"/images/"



var dialog7 = new buzz.sound(soundAsset+"1_1.ogg");
var dialog7part1 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog7part2 = new buzz.sound(soundAsset+"1_3.ogg");
var dialog7part3 = new buzz.sound(soundAsset+"1_4.ogg");
var dialog7main = [dialog7, dialog7part1, dialog7part2, dialog7part3];


var dialog7part4 = new buzz.sound(soundAsset+"1_5.ogg");
var dialog7part5 = new buzz.sound(soundAsset+"1_6.ogg");
var dialog7part6 = new buzz.sound(soundAsset+"1_7.ogg");
var dialog7main1 = [dialog7part4, dialog7part5, dialog7part6];

var dialog8 = new buzz.sound(soundAsset+"2_1.ogg");
var dialog8part1 = new buzz.sound(soundAsset+"2_2.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"2_3.ogg");
var dialog8main = [dialog8, dialog8part1, dialog8part2];


var dialog8part3 = new buzz.sound(soundAsset+"2_4.ogg");
var dialog8part4 = new buzz.sound(soundAsset+"2_5.ogg");
var dialog8part5 = new buzz.sound(soundAsset+"2_6.ogg");
var dialog8main1 = [dialog8part3, dialog8part4, dialog8part5];



var soundcontent = [dialog7main, dialog7main1, dialog8main, dialog8main1];


var content=[
	
	
	{
		bgImgSrc : imageAsset+"dpk1.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_7main",
		forwhichdialog : "dialog7main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p7dialog7,
							data.string.p7dialog7part1,
							data.string.p7dialog7part2,
							data.string.p7dialog7part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	

		{
		bgImgSrc : imageAsset+"rolpa2.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_7main1",
		forwhichdialog : "dialog7main1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [
							data.string.p7dialog7part4,
							data.string.p7dialog7part5,
							data.string.p7dialog7part6,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	{
		bgImgSrc : imageAsset+"dpk2.jpg",
		imageid : "bg_image11",
		dialog_id : "dialog_8main",
		forwhichdialog : "dialog8main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p8dialog8,
							data.string.p8dialog8part1,
							data.string.p8dialog8part2,
						
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	

		{
		bgImgSrc : imageAsset+"rolpa4.jpg",
		imageid : "bg_image11",
		dialog_id : "dialog_8main",
		forwhichdialog : "dialog8main1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [
						
							data.string.p8dialog8part3,
							data.string.p8dialog8part4,
							data.string.p8dialog8part5
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==3){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==4){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page1/";
background_image = $ref+"/images/"

var dialog0 = new buzz.sound(soundAsset+"ourgrandparents.ogg");
var dialog1 = new buzz.sound(soundAsset+"1_1.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"1_3.ogg");
var dialog1part3 = new buzz.sound(soundAsset+"1_4.ogg");
var dialog1main = [dialog1, dialog1part1, dialog1part2, dialog1part3];

var dialog2 = new buzz.sound(soundAsset+"2_1.ogg");
var dialog2part1= new buzz.sound(soundAsset+"2_2.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"2_3.ogg");
var dialog2main = [dialog2, dialog2part1, dialog2part2];


var dialog2part3 = new buzz.sound(soundAsset+"2_4.ogg");
var dialog2part4 = new buzz.sound(soundAsset+"2_5.ogg");
var dialog3main = [dialog2part3, dialog2part4];

var soundcontent = [dialog0,dialog1main, dialog2main, dialog3main];


var content=[
//	slide 0
    {
        bgImgSrc : imageAsset+"our_grandparents.png",
        imageid : "bg_image1",
        dialog_id : "dialog_1main",
        forwhichdialog : "dialog0",
        lineCountDialog : [data.string.p1dialog0
        ],
        speakerImgSrc : $ref+"/images/speakers.png",
        listenAgainText : data.string.listenAgainData,
    },
//slide 1
	{
		bgImgSrc : imageAsset+"chepang1.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		forwhichdialog : "dialog1main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p1dialog1,
							data.string.p1dialog1part1,
							data.string.p1dialog1part2,
							data.string.p1dialog1part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	//slide 2
	{
		bgImgSrc : imageAsset+"chepang2.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		forwhichdialog : "dialog2main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p2dialog2,
							data.string.p2dialog2part1,
							data.string.p2dialog2part2,
							
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
		
		
	},
	//slide 3
		{
		bgImgSrc : imageAsset+"chepang3.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [
							
							data.string.p2dialog2part3,
							data.string.p2dialog2part4,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
		
		
	},
	
	
	
	

	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 4;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==4){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==5){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




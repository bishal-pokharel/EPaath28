imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page2/";
background_image = $ref+"/images/"



var dialog3 = new buzz.sound(soundAsset+"1_1.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"1_3.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"1_4.ogg");
var dialog3main = [dialog3, dialog3part1, dialog3part2, dialog3part3];

var dialog4 = new buzz.sound(soundAsset+"2_1.ogg");
var dialog4part1 = new buzz.sound(soundAsset+"2_2.ogg");
var dialog4main = [dialog4, dialog4part1];


var dialog4part2 = new buzz.sound(soundAsset+"2_3.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"2_4.ogg");
var dialog4part4 = new buzz.sound(soundAsset+"2_5.ogg");
var dialog5main = [dialog4part2, dialog4part3, dialog4part4];

var soundcontent = [dialog3main, dialog4main, dialog5main];


var content=[
	// slide 1
	
	{
		bgImgSrc : imageAsset+"mus1.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_3main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p3dialog3,
							data.string.p3dialog3part1,
							data.string.p3dialog3part2,
							data.string.p3dialog3part3,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	// slide 2
	
	{
		bgImgSrc : imageAsset+"mus2.jpg",
		imageid : "bg_image11",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog4main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p4dialog4,
							data.string.p4dialog4part1,
							
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	// slide 3
		
	{
		bgImgSrc : imageAsset+"muslim3.jpg",
		imageid : "bg_image11",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog5main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [
							data.string.p4dialog4part2,
							data.string.p4dialog4part3,
							data.string.p4dialog4part4,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==3){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==4){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page5/";
background_image = $ref+"/images/"


var dialog9 = new buzz.sound(soundAsset+"1_1.ogg");
var dialog9part1 = new buzz.sound(soundAsset+"1_2.ogg");
var dialog9part2 = new buzz.sound(soundAsset+"1_3.ogg");
var dialog9part3 = new buzz.sound(soundAsset+"1_4.ogg");
var dialog9part4 = new buzz.sound(soundAsset+"1_5.ogg");
var dialog9part5 = new buzz.sound(soundAsset+"1_6.ogg");
var dialog9part6 = new buzz.sound(soundAsset+"1_7.ogg");
var dialog9part7 = new buzz.sound(soundAsset+"1_8.ogg");
var dialog9main = [dialog9, dialog9part1, dialog9part2, dialog9part3, dialog9part4, dialog9part5, dialog9part6, dialog9part7 ];

var dialog10 = new buzz.sound(soundAsset+"2_1.ogg");
var dialog10part1 = new buzz.sound(soundAsset+"2_2.ogg");
var dialog10part2 = new buzz.sound(soundAsset+"2_3.ogg");
var dialog10main = [dialog10, dialog10part1, dialog10part2];

var dialog11 = new buzz.sound(soundAsset+"3_1.ogg");
var dialog11part1 = new buzz.sound(soundAsset+"3_2.ogg");
var dialog11part2 = new buzz.sound(soundAsset+"3_3.ogg");
var dialog11part3 = new buzz.sound(soundAsset+"3_4.ogg");
var dialog11part4 = new buzz.sound(soundAsset+"3_5.ogg");
var dialog11part5 = new buzz.sound(soundAsset+"3_6.ogg");
var dialog11part6 = new buzz.sound(soundAsset+"3_7.ogg");
var dialog11part7 = new buzz.sound(soundAsset+"3_8.ogg");
var dialog11main = [dialog11, dialog11part1, dialog11part2, dialog11part3, dialog11part4, dialog11part5, dialog11part6, dialog11part7];



var soundcontent = [dialog9main, dialog10main, dialog11main];


var content=[
	
	
	{
		bgImgSrc : imageAsset+"badi1.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_9main",
		forwhichdialog : "dialog9main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p9dialog9,
							data.string.p9dialog9part1,
							data.string.p9dialog9part2,
							data.string.p9dialog9part3,
							data.string.p9dialog9part4,
							data.string.p9dialog9part5,
							data.string.p9dialog9part6,
							data.string.p9dialog9part7,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	{
		bgImgSrc : imageAsset+"badi2.jpg",
		imageid : "bg_image11",
		dialog_id : "dialog_10main",
		forwhichdialog : "dialog10main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p10dialog10,
							data.string.p10dialog10part1,
							data.string.p10dialog10part2,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	
	{
		bgImgSrc : imageAsset+"badi3.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_11main",
		forwhichdialog : "dialog11main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"three.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p11dialog11,
							data.string.p11dialog11part1,
							data.string.p11dialog11part2,
							data.string.p11dialog11part3,
							data.string.p11dialog11part4,
							data.string.p11dialog11part5,
							data.string.p11dialog11part6,
							data.string.p11dialog11part7,
							],
		speakerImgSrc : $ref+"/images/speakers.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==3){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==4){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

})(jQuery);




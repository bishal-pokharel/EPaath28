(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var content = [
    {
      question: data.string.q24,
      answers: [
        { ans: data.string.name71 },
        { ans: data.string.name72, correct: "correct" },
        { ans: data.string.name73 }
      ]
    },
    {
      question: data.string.q25,
      answers: [
        { ans: data.string.name74, correct: "correct" },
        { ans: data.string.name75 },
        { ans: data.string.name76 }
      ]
    },
    {
      question: data.string.q26,
      answers: [
        { ans: data.string.name77, correct: "correct" },
        { ans: data.string.name78 },
        { ans: data.string.name79 }
      ]
    },
    {
      question: data.string.q27,
      answers: [
        { ans: data.string.name80 },
        { ans: data.string.name81, correct: "correct" },
        { ans: data.string.name82 }
      ]
    },
    {
      question: data.string.q28,
      answers: [
        { ans: data.string.name83 },
        { ans: data.string.name84, correct: "correct" },
        { ans: data.string.name85 }
      ]
    },

    {
      question: data.string.q29,
      answers: [
        { ans: data.string.name86 },
        { ans: data.string.name87 },
        { ans: data.string.name88, correct: "correct" }
      ]
    },

    {
      question: data.string.q30,
      answers: [
        { ans: data.string.name89 },
        { ans: data.string.name90, correct: "correct" },
        { ans: data.string.name91 }
      ]
    },

    {
      question: data.string.q31,
      answers: [
        { ans: data.string.name92 },
        { ans: data.string.name93 },
        { ans: data.string.name94, correct: "correct" }
      ]
    },

    {
      question: data.string.q32,
      answers: [
        { ans: data.string.name95 },
        { ans: data.string.name96, correct: "correct" },
        { ans: data.string.name97 }
      ]
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();

  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    if (answered) {
      return answered;
    }
    attemptcount++;
    var element_li = $(this).text();
    console.log(element_li);
    // console.log("what");
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();

      // new code
      var string = document.getElementsByClassName("question")[0].innerHTML;
      var replacedString = string.replace("......", element_li);
      console.log(replacedString);
      document.getElementsByClassName("question")[0].innerHTML = replacedString;
      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 9) {
      qA();
    } else if (questionCount == 9) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

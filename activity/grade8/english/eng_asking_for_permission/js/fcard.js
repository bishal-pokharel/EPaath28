$(".title").text(data.string.p4title);
$(document).ready(function() {
  $(".title").text(data.string.exerciseTitle2);
  var content = [
    {
      words: data.string.p4p1,
      description: data.string.p4des1,
      img: $ref + "/images/exercise7/vim.jpg"
    }, //
    {
      words: data.string.p4p2,
      description: data.string.p4des2,
      img: $ref + "/images/exercise7/jk.jpg"
    }, //

    {
      words: data.string.p4p3,
      description: data.string.p4des3,
      img: $ref + "/images/exercise7/main.jpg"
    }, //

    {
      words: data.string.p4p4,
      description: data.string.p4des4,
      img: $ref + "/images/exercise7/pur.png"
    }, //
    {
      words: data.string.p4p5,
      description: data.string.p4des5,
      img: $ref + "/images/exercise7/bio.png"
    } //
  ];
  console.log(content);
  displaycontent();
  function displaycontent() {
    var words = $.each(content, function(key, value) {
      words = value.words;
      description = value.description;
      image = value.img;
      // console.log(description);
      appendTab = "<li>";
      appendTab +=
        '<a href="#' + words + '" data-toggle="tab"  >' + words + "</a>";
      appendTab += "</li>";
      $("#myTab").append(appendTab);

      appendcontent = '<div id="' + words + '" class="tab-pane ">';
      appendcontent += '<div class="f1_container">';
      appendcontent += '<div class="shadow f1_card">';
      appendcontent += '<div class="front face">';
      appendcontent += "<p>" + words + "</p>";
      appendcontent += "</div>";
      appendcontent += '<div class="back face center">';
      appendcontent +=
        '<img src="' + image + '" class="tab_image img-responsive"/>';
      appendcontent += '<p class="textroll">';
      appendcontent += description;
      appendcontent += "</p>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      appendcontent += "</div>";
      $("#myTabContent").append(appendcontent);
      ole.footerNotificationHandler.pageEndSetNotification();
    });
  }
  /*
   * function qA() { var source = $('#qA-templete').html(); var
   * template = Handlebars.compile(source); var html =
   * template(content); //$board.html(html); // console.log(html); }
   *
   * qA();
   */
});
$(document).ready(function() {
  loadTimelineProgress(1, 1);

  $(".f1_container").click(function() {
    //alert("ok");
    loadTimelineProgress(1, 1);

    $(this).toggleClass("active");
  });
});

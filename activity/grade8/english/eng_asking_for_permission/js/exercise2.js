(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  var content = [
    {
      question: data.string.q6,
      answers: [
        { ans: data.string.name16 },
        { ans: data.string.name17, correct: "correct" },
        { ans: data.string.name18 }
      ]
    },
    {
      question: data.string.q7,
      answers: [
        { ans: data.string.name19, correct: "correct" },
        { ans: data.string.name20 },
        { ans: data.string.name21 }
      ]
    },
    {
      question: data.string.q8,
      answers: [
        { ans: data.string.name22 },
        { ans: data.string.name23, correct: "correct" },
        { ans: data.string.name24 }
      ]
    },
    {
      question: data.string.q9,
      answers: [
        { ans: data.string.name25 },
        { ans: data.string.name26, correct: "correct" },
        { ans: data.string.name27 }
      ]
    },
    {
      question: data.string.q10,
      answers: [
        { ans: data.string.name28, correct: "correct" },
        { ans: data.string.name29 },
        { ans: data.string.name30 }
      ]
    },

    {
      question: data.string.q11,
      answers: [
        { ans: data.string.name31, correct: "correct" },
        { ans: data.string.name32 },
        { ans: data.string.name33 }
      ]
    },

    {
      question: data.string.q12,
      answers: [
        { ans: data.string.name34 },
        { ans: data.string.name35 },
        { ans: data.string.name36, correct: "correct" }
      ]
    },

    {
      question: data.string.q13,
      answers: [
        { ans: data.string.name37 },
        { ans: data.string.name38, correct: "correct" },
        { ans: data.string.name39 }
      ]
    },

    {
      question: data.string.q14,
      answers: [
        { ans: data.string.name40 },
        { ans: data.string.name41, correct: "correct" },
        { ans: data.string.name42 }
      ]
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();

  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;
  $board.on("click", ".neutral", function() {
    // new code
    if (answered) {
      return answered;
    }
    attemptcount++;
    var element_li = $(this).text();
    console.log(element_li);

    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();

      // new code
      var string = document.getElementsByClassName("question")[0].innerHTML;
      var replacedString = string.replace("......", element_li);
      console.log(replacedString);
      document.getElementsByClassName("question")[0].innerHTML = replacedString;

      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 9) {
      qA();
    } else if (questionCount == 9) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

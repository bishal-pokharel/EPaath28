(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  var content = [
    {
      question: data.string.q33,
      answers: [
        { ans: data.string.name98 },
        { ans: data.string.name99, correct: "correct" },
        { ans: data.string.name100 }
      ]
    },
    {
      question: data.string.q34,
      answers: [
        { ans: data.string.name101 },
        { ans: data.string.name102, correct: "correct" },
        { ans: data.string.name103 }
      ]
    },
    {
      question: data.string.q35,
      answers: [
        { ans: data.string.name104 },
        { ans: data.string.name105 },
        { ans: data.string.name106, correct: "correct" }
      ]
    },
    {
      question: data.string.q36,
      answers: [
        { ans: data.string.name107, correct: "correct" },
        { ans: data.string.name108 },
        { ans: data.string.name109 }
      ]
    },
    {
      question: data.string.q37,
      answers: [
        { ans: data.string.name110 },
        { ans: data.string.name111 },
        { ans: data.string.name112, correct: "correct" }
      ]
    },

    {
      question: data.string.q38,
      answers: [
        { ans: data.string.name113 },
        { ans: data.string.name114 },
        { ans: data.string.name115, correct: "correct" }
      ]
    },

    {
      question: data.string.q39,
      answers: [
        { ans: data.string.name116 },
        { ans: data.string.name117 },
        { ans: data.string.name118, correct: "correct" }
      ]
    },

    {
      question: data.string.q40,
      answers: [
        { ans: data.string.name119 },
        { ans: data.string.name120, correct: "correct" },
        { ans: data.string.name121 }
      ]
    },

    {
      question: data.string.q41,
      answers: [
        { ans: data.string.name122, correct: "correct" },
        { ans: data.string.name123 },
        { ans: data.string.name124 }
      ]
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();

  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;

  $board.on("click", ".neutral", function() {
    if (answered) {
      return answered;
    }
    attemptcount++;
    var element_li = $(this).text();
    console.log(element_li);

    // console.log("what");
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();

      // new code
      var string = document.getElementsByClassName("question")[0].innerHTML;
      var replacedString = string.replace("......", element_li);
      console.log(replacedString);
      document.getElementsByClassName("question")[0].innerHTML = replacedString;
      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 9) {
      qA();
    } else if (questionCount == 9) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title")
        .html(
          "Congratulations on finishing your exercise <br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .css({
          position: "absolute",
          top: "48%",
          transform: "translateY(-50%)"
        });
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
})(jQuery);

imageAsset = $ref+"/images/page3/";
soundAsset = $ref+"/sounds/page3/";


var dialog1 = new buzz.sound(soundAsset+"8.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"10.ogg");
var dialog2 = [dialog2part1, dialog2part2];

var dialog3 = new buzz.sound(soundAsset+"11.ogg");

var dialog4part1 = new buzz.sound(soundAsset+"12.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"13.ogg");
var dialog4 = [dialog4part1,dialog4part2];



var dialog5 = new buzz.sound(soundAsset+"14.ogg");
var dialog6 = new buzz.sound(soundAsset+"15.ogg");

/*
var dialog5part1 = new buzz.sound(soundAsset+"12.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"13.ogg");
var dialog5part3 = new buzz.sound(soundAsset+"14.ogg");
var dialog5part4 = new buzz.sound(soundAsset+"15.ogg");
var dialog5 = [dialog5part1, dialog5part2, dialog5part3, dialog5part4];
*/


var soundcontent = [dialog1, dialog2, dialog3, dialog4,
					dialog5,dialog6];

var content=[
	{
		bgImgSrc : imageAsset+"page3a.jpg",
		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight",
			},
		],
		lineCountDialog : [data.string.p3dialog1,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"pageb.jpg",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight",
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : ""
			},
		],
		lineCountDialog : [data.string.p3dialog2,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page3c.jpg",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight",
			},
		],
		lineCountDialog : [data.string.p3dialog3
		             ],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page3d.jpg",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight",
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : ""
			},
		],
		lineCountDialog : [data.string.p3dialog4part1,
							data.string.p3dialog4part2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	{
		bgImgSrc : imageAsset+"page3e.jpg",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight",
			},
		],
		lineCountDialog : [data.string.p3dialog5],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
	
	
	{
		bgImgSrc : imageAsset+"page3e.jpg",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"bhuwan.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"nidhi.png",
				nameofwhosehead : "nidhi",
				highlightFlag : ""
			},
			{
				talkHeadImgSrc :  imageAsset+"boothoperator.png",
				nameofwhosehead : "boothoperator",
				highlightFlag : "highlight",
			},
		],
		lineCountDialog : [data.string.p3dialog6],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 6;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		/*this function binds appropriate events handlers to the 
		audio as required*/
		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);
(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  var content = [
    {
      question: data.string.q42,
      answers: [
        { ans: data.string.name127 },
        { ans: data.string.name128, correct: "correct" },
        { ans: data.string.name129 },
        { ans: data.string.name130 }
      ]
    },
    {
      question: data.string.q43,
      answers: [
        { ans: data.string.name131 },
        { ans: data.string.name132, correct: "correct" },
        { ans: data.string.name133 },
        { ans: data.string.name134 }
      ]
    },
    {
      question: data.string.q44,
      answers: [
        { ans: data.string.name135 },
        { ans: data.string.name136 },
        { ans: data.string.name137 },
        { ans: data.string.name138, correct: "correct" }
      ]
    },
    {
      question: data.string.q45,
      answers: [
        { ans: data.string.name139 },
        { ans: data.string.name140 },
        { ans: data.string.name141, correct: "correct" },
        { ans: data.string.name142 }
      ]
    },
    {
      question: data.string.q46,
      answers: [
        { ans: data.string.name143 },
        { ans: data.string.name144, correct: "correct" },
        { ans: data.string.name145 },
        { ans: data.string.name146 }
      ]
    },

    {
      question: data.string.q47,
      answers: [
        { ans: data.string.name147, correct: "correct" },
        { ans: data.string.name148 },
        { ans: data.string.name149 },
        { ans: data.string.name150 }
      ]
    },

    {
      question: data.string.q48,
      answers: [
        { ans: data.string.name151 },
        { ans: data.string.name152 },
        { ans: data.string.name153 },
        { ans: data.string.name154, correct: "correct" }
      ]
    },

    {
      question: data.string.q49,
      answers: [
        { ans: data.string.name156 },
        { ans: data.string.name157, correct: "correct" },
        { ans: data.string.name158 },
        { ans: data.string.name159 }
      ]
    }
  ];

  $nextBtn.hide(0);

  // console.log(content);

  var questionCount = 0;
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");

  console.log(content);
  function qA() {
    var source = $("#qA-templete").html();
    var template = Handlebars.compile(source);
    var html = template(content[questionCount]);
    $board.html(html);
    answered = false;
    attemptcount = 0;
    // console.log(html);
  }
  qA();

  var answered = false;
  var attemptcount = 0;

  var totalq = content.length;
  loadTimelineProgress(totalq + 1, attemptcount + 1);
  var correctlyanswered = 0;

  $board.on("click", ".neutral", function() {
    if (answered) {
      return answered;
    }
    attemptcount++;
    var element_li = $(this).text();
    console.log(element_li);
    // console.log("what");
    var $this = $(this);
    var isCorrect = $(this).data("correct");
    if (isCorrect === "correct") {
      if (attemptcount == 1) {
        correctlyanswered++;
      }
      answered = true;
      $this.addClass("right").removeClass("neutral");
      $nextBtn.fadeIn();

      // new code
      var string = document.getElementsByClassName("question")[0].innerHTML;
      var replacedString = string.replace("......", element_li);
      console.log(replacedString);
      document.getElementsByClassName("question")[0].innerHTML = replacedString;
      play_correct_incorrect_sound(true);
    } else {
      play_correct_incorrect_sound(false);
      $this.addClass("wrong").removeClass("neutral");
    }
  });

  $nextBtn.on("click", function() {
    $nextBtn.hide(0);
    questionCount++;
    loadTimelineProgress(totalq + 1, questionCount + 1);
    if (questionCount < 8) {
      qA();
    } else if (questionCount == 8) {
      $(".mainholder").hide(0);
      $(".imgholder").hide(0);
      $(".answers").hide(0);
      $(".result").hide(0);
      $(".question").hide(0);
      $(".title").hide(0);
      //$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
      // "position": "absolute",
      // "top": "48%",
      // "transform": "translateY(-50%)"
      // });
      $(".finishtxt")
        .append(
          "<br> You have correctly answered " +
            correctlyanswered +
            " out of " +
            totalq +
            " questions."
        )
        .show(0);
      //ole.footerNotificationHandler.pageEndSetNotification();
    }
  });
  $(".closebtn").on("click", function() {
    ole.activityComplete.finishingcall();
  });
})(jQuery);

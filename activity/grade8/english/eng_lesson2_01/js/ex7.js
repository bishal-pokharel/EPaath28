//$(document).ready(function(){
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle7);
	var content = [
	{
		question : data.string.q17,
		answers: [
		{ans : data.string.serial1+" "+data.string.name11,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name7},
		{ans : data.string.serial3+" "+data.string.name5}
		],


	},
	{
		question : data.string.q18,
		answers: [
		{ans : data.string.serial1+" "+data.string.name12},
		{ans : data.string.serial2+" "+data.string.name1},
		{ans : data.string.serial3+" "+data.string.name7,
			correct : "correct"}
		],

	},
	{
		question : data.string.q19,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name6},
  		{ans : data.string.serial2+" "+data.string.name5,
  			correct : "correct"},
  		{ans : data.string.serial3+" "+data.string.name8}
  		],

	},
	{
		question : data.string.q20,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name9,
  			correct : "correct"},
  		{ans : data.string.serial2+" "+data.string.name10},
  		{ans : data.string.serial3+" "+data.string.name1}
  		],

	},
	{
		question : data.string.q21,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name7},
  		{ans : data.string.serial2+" "+data.string.name8,
  			correct : "correct"},
  		{ans : data.string.serial3+" "+data.string.name5}
  		],
	},
	{
		question : data.string.q22,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name6},
  		{ans : data.string.serial2+" "+data.string.name13,correct : "correct"},
  		{ans : data.string.serial3+" "+data.string.name10}
  		],

	},
	{
		question : data.string.q23,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name6},
  		{ans : data.string.serial2+" "+data.string.name3},
  		{ans : data.string.serial3+" "+data.string.name13,
  			correct : "correct"}
  		],

	},
	{
		question : data.string.q24,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name9},
  		{ans : data.string.serial2+" "+data.string.name1,
  			correct : "correct"},
  		{ans : data.string.serial3+" "+data.string.name3}
  		],

	},
	{
		question : data.string.q25,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name14},
  		{ans : data.string.serial2+" "+data.string.name15,
  			correct : "correct"},
  		{ans : data.string.serial3+" "+data.string.name12}
  		],

	},
	{
		question : data.string.q26,
		answers: [
  		{ans : data.string.serial1+" "+data.string.name3,
  			correct : "correct"},
  		{ans : data.string.serial2+" "+data.string.name1},
  		{ans : data.string.serial3+" "+data.string.name14}
  		],

	}

	];

	$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	loadTimelineProgress(content.length, questionCount+1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("____________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString;
			play_correct_incorrect_sound(true);
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<10){
			loadTimelineProgress(content.length, questionCount+1);
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// .text("Congratulation on finishing your exercise").css({
					// "position": "absolute",
					// "top": "48%",
					// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
		}
	});
	$(".closebtn").on("click",function(){
		ole.activityComplete.finishingcall();
	});

})(jQuery);

//$(document).ready(function(){
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle6);
	var content = [
	{
		question : data.string.q7,
		answers: [
		{ans : data.string.serial1+" "+data.string.name5},
		{ans : data.string.serial2+" "+data.string.name6,
			correct : "correct"}
		],


	},
	{
		question : data.string.q8,
		answers: [
		{ans : data.string.serial1+" "+data.string.name7},
		{ans : data.string.serial2+" "+data.string.name8,
			correct : "correct"}
		],

	},
	{
		question : data.string.q9,
		answers: [
		{ans : data.string.serial1+" "+data.string.name5,
		correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name6}
		],

	},
	{
		question : data.string.q10,
		answers: [
		{ans : data.string.serial1+" "+data.string.name7,
		correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name8}
		],

	},
	{
		question : data.string.q11,
		answers: [
		{ans : data.string.serial1+" "+data.string.name9,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name10}
		]
	},
	{
		question : data.string.q12,
		answers: [
		{ans : data.string.serial1+" "+data.string.name7,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name8}
		],

	},
	{
		question : data.string.q13,
		answers: [
		{ans : data.string.serial1+" "+data.string.name5,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name6},
		],

	},
	{
		question : data.string.q14,
		answers: [
		{ans : data.string.serial1+" "+data.string.name7},
		{ans : data.string.serial2+" "+data.string.name8,
		correct : "correct"}
		],

	},
	{
		question : data.string.q15,
		answers: [
		{ans : data.string.serial1+" "+data.string.name9,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name10}
		],

	},
	{
		question : data.string.q16,
		answers: [
		{ans : data.string.serial1+" "+data.string.name5,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name6},
		],

	}

	];

	$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
		loadTimelineProgress(content.length, questionCount+1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();
	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("____________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString;
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true);
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<10){
			loadTimelineProgress(content.length, questionCount+1);
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
					"position": "absolute",
					"top": "48%",
					"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

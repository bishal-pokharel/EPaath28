$('.title').text(data.string.exerciseTitle);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/images/exercise_image/land.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/images/exercise_image/rr.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/images/exercise_image/de.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/images/exercise_image/mg.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/images/exercise_image/ta.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/images/exercise_image/dt.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/images/exercise_image/cp.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/images/exercise_image/mess.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/images/exercise_image/fall.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/images/exercise_image/cel.png",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/images/exercise_image/mo.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/images/exercise_image/ca.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/images/exercise_image/pi.jpg",
			}
			];
			console.log(content);
			displaycontent();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
				loadTimelineProgress(1,  1);
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);
					ole.footerNotificationHandler.pageEndSetNotification();
				});
			}
			/*
			 * function qA() { var source = $('#qA-templete').html(); var
			 * template = Handlebars.compile(source); var html =
			 * template(content); //$board.html(html); // console.log(html); }
			 *
			 * qA();
			 */

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	});
});

//$(document).ready(function(){
var imgpath = $ref+"/images/exercise5/";
(function () {

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.exerciseTitle4);
	var content = [
	{
		question : data.string.q27,
		answers: [
		{ans : data.string.serial1+" "+data.string.name16},
		{ans : data.string.serial2+" "+data.string.name17},
		{ans : data.string.serial3+" "+data.string.name18},
		{ans : data.string.serial4+" "+data.string.name19,
			correct : "correct"},
		{ans : data.string.serial5+" "+data.string.name20}
		],

	},
	{
		question : data.string.q28,
		answers: [
		{ans : data.string.serial1+" "+data.string.name16,
			correct : "correct"},
		{ans : data.string.serial2+" "+data.string.name17},
		{ans : data.string.serial3+" "+data.string.name18},
		{ans : data.string.serial4+" "+data.string.name19},
		{ans : data.string.serial5+" "+data.string.name20}
		],

	},
	{
		question : data.string.q29,
		answers: [
		{ans : data.string.serial1+" "+data.string.name16},
		{ans : data.string.serial2+" "+data.string.name17},
		{ans : data.string.serial3+" "+data.string.name18},
		{ans : data.string.serial4+" "+data.string.name19},
		{ans : data.string.serial5+" "+data.string.name20,
			correct : "correct"}
		],

	},
	{
		question : data.string.q30,
		answers: [
		{ans : data.string.serial1+" "+data.string.name16},
		{ans : data.string.serial2+" "+data.string.name17},
		{ans : data.string.serial3+" "+data.string.name18,
			correct : "correct"},
		{ans : data.string.serial4+" "+data.string.name19},
		{ans : data.string.serial5+" "+data.string.name20}
		],

	},
	{
		question : data.string.q31,
		answers: [
		{ans : data.string.serial1+" "+data.string.name16},
		{ans : data.string.serial2+" "+data.string.name17,
			correct : "correct"},
		{ans : data.string.serial3+" "+data.string.name18},
		{ans : data.string.serial4+" "+data.string.name19},
		{ans : data.string.serial5+" "+data.string.name20}
		],

	}
	];


	$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	loadTimelineProgress(content.length, questionCount+1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;
	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

					// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("____________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString;
			play_correct_incorrect_sound(true);
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<5){
			loadTimelineProgress(content.length, questionCount+1);
			qA();
		}
		else if (questionCount==5){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

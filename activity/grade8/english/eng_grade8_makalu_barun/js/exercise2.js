var content=[
	{
		diyImageSource :$ref + "exercise_images/1.png",
		diyTextData : data.string.d_1,
		text_1_1: data.string.d_3,

		arrBack:[
			{txt:1,whatarrs:data.string.d_4_1,
				opt1:data.string.d_4_1_op1,
				opt2:data.string.d_4_1_op2,
				opt3:data.string.d_4_1_op3,
				opt4:data.string.d_4_1_op4,
				opt5:data.string.d_4_1_op5,
				opt6:data.string.d_4_1_op6,
				opt7:data.string.d_4_1_op7,
				opt8:data.string.d_4_1_op8,
				corr1:1,corr2:0,corr3:0,corr4:0,corr5:0,corr6:0,corr7:0,corr8:0,
				remain:data.string.d_4_1_1
			},

			{txt:2,whatarrs:data.string.d_4_2,
				 opt1:data.string.d_4_2_op1,
				opt2:data.string.d_4_2_op2,
				opt3:data.string.d_4_2_op3,
				opt4:data.string.d_4_2_op4,
				opt5:data.string.d_4_2_op5,
				opt6:data.string.d_4_2_op6,
				opt7:data.string.d_4_2_op7,
				opt8:data.string.d_4_2_op8,
				corr1:0,corr2:1,corr3:0,corr4:0,corr5:0,corr6:0,corr7:0,corr8:0,
				remain:data.string.d_4_2_1
			},

			{txt:3,whatarrs:data.string.d_4_3,
				opt1:data.string.d_4_3_op1,
				opt2:data.string.d_4_3_op2,
				opt3:data.string.d_4_3_op3,
				opt4:data.string.d_4_3_op4,
				opt5:data.string.d_4_3_op5,
				opt6:data.string.d_4_3_op6,
				opt7:data.string.d_4_3_op7,
				opt8:data.string.d_4_3_op8,
				corr1:0,corr2:0,corr3:1,corr4:0,corr5:0,corr6:0,corr7:0,corr8:0,
				remain:data.string.d_4_3_1
			},

			{txt:4,whatarrs:data.string.d_4_4,
				 opt1:data.string.d_4_4_op1,
				 opt2:data.string.d_4_4_op2,
				 opt3:data.string.d_4_4_op3,
				opt4:data.string.d_4_4_op4,
				opt5:data.string.d_4_4_op5,
				opt6:data.string.d_4_4_op6,
				opt7:data.string.d_4_4_op7,
				opt8:data.string.d_4_4_op8,
				corr1:0,corr2:0,corr3:0,corr4:1,corr5:0,corr6:0,corr7:0,corr8:0,
				remain:data.string.d_4_4_1
			},

			{txt:5,whatarrs:data.string.d_4_5,
				 opt1:data.string.d_4_5_op1,
				 opt2:data.string.d_4_5_op2,
				 opt3:data.string.d_4_5_op3,
				 opt4:data.string.d_4_5_op4,
				opt5:data.string.d_4_5_op5,
				opt6:data.string.d_4_5_op6,
				opt7:data.string.d_4_5_op7,
				opt8:data.string.d_4_5_op8,
				corr1:0,corr2:0,corr3:0,corr4:0,corr5:1,corr6:0,corr7:0,corr8:0,
				remain:data.string.d_4_5_1
			},

			{txt:6,whatarrs:data.string.d_4_6,
				 opt1:data.string.d_4_6_op1,
				 opt2:data.string.d_4_6_op2,
				 opt3:data.string.d_4_6_op3,
				 opt4:data.string.d_4_6_op4,
				 opt5:data.string.d_4_6_op5,
				opt6:data.string.d_4_6_op6,
				opt7:data.string.d_4_6_op7,
				opt8:data.string.d_4_6_op8,
				corr1:0,corr2:0,corr3:0,corr4:0,corr5:0,corr6:1,corr7:0,corr8:0,
				remain:data.string.d_4_6_1
			},

			{txt:7,whatarrs:data.string.d_4_7,
				 opt1:data.string.d_4_7_op1,
				 opt2:data.string.d_4_7_op2,
				 opt3:data.string.d_4_7_op3,
				 opt4:data.string.d_4_7_op4,
				 opt5:data.string.d_4_7_op5,
				 opt6:data.string.d_4_7_op6,
				opt7:data.string.d_4_7_op7,
				opt8:data.string.d_4_7_op8,
				corr1:0,corr2:0,corr3:0,corr4:0,corr5:0,corr6:0,corr7:1,corr8:0,
				remain:data.string.d_4_7_1
			},

			{txt:8,whatarrs:data.string.d_4_8,
				 opt1:data.string.d_4_7_op1,
				 opt2:data.string.d_4_7_op2,
				 opt3:data.string.d_4_7_op3,
				 opt4:data.string.d_4_7_op4,
				 opt5:data.string.d_4_7_op5,
				 opt6:data.string.d_4_7_op6,
				opt7:data.string.d_4_7_op7,
				opt8:data.string.d_4_8_op8,
				corr1:0,corr2:0,corr3:0,corr4:0,corr5:0,corr6:0,corr7:0,corr8:1,
				remain:data.string.d_4_8_1
			},
		],
		whatImgvalme:$ref+"/exercise_images/img/1.png",

	}

];

$(function () {
		
		var $board = $('.board');
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var countNext = 0;
		var $value = 1;

	/*
	*
	* second
	*/
		function first () {
			var source = $("#first-template").html();
			var template = Handlebars.compile(source);
			var html = template(content[countNext]);
			$board.html(html);
			// $prevBtn.hide(0);
			// $nextBtn.hide(0);
			// alert("hello")
		

		$(".nonactive").on("click",function(){
			var $whatIdvalstoo=$("#whatId_"+$value);
			// alert ($whatIdvalstoo);
			var datvals=$(this).attr('dataval');
			var valHtml=$(this).html();			
			var clickedme=$(this);
			
			if(datvals==1)
			{
				// alert("inside if loop");
				$whatIdvalstoo.find(".whatactive[dataval='1']").addClass('actives');
				$whatIdvalstoo.find(".whatactive").removeClass('nonactive');
				
				$whatIdvalstoo.find(".bothvals").delay(500).fadeOut(500,function(){
				$whatIdvalstoo.find(".dashedLine").html(valHtml).addClass('dsLine');
					
					if($value==1)
					// alert("inside if loop");

						$(".whatImgvalme").delay(200).fadeIn(50);
					else
					{
						// alert($value);
						$(".whatImgvalme img:nth-of-type(1)").attr("src",$ref+"/exercise_images/img/"+$value+".png");
					}
					$value++;

					
					// alert($value);
					console.log($value);
					if($value<=8)
					{					
						$("#whatId_"+$value).delay(2000).fadeIn(500);
					}
					else
					{
						setTimeout(function(){
							// $("#bac_anim_2").show(0);
							// $("#bac_anim_2").css("right","400%");
							// $(".whatImgvalme img:nth-of-type(1)").hide(0);
							$nextBtn.show(0);
							// ole.footerNotificationHandler.pageEndSetNotification();
						},1500);
						$nextBtn.delay(6000).show(0);
					}

					if($value ==9)
					{
						ole.footerNotificationHandler.pageEndSetNotification();
					}
					
				});
				play_correct_incorrect_sound(true); 
			} else {
				play_correct_incorrect_sound(false);		
				$whatIdvalstoo.find(clickedme).addClass('wronged');
			}
		});

	}
		first();
});
imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/page1/";
background_image = $ref+"/images/"

var dialog0 = new buzz.sound(soundAsset+"avisittoanationalpark.ogg");
var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog2 = new buzz.sound(soundAsset+"2.ogg");
var dialog3 = new buzz.sound(soundAsset+"3.ogg");
var dialog4 = new buzz.sound(soundAsset+"4.ogg");
var dialog5 = new buzz.sound(soundAsset+"5.ogg");

var dialog1main = [dialog1, dialog2, dialog3, dialog4, dialog5 ];

var dialog2 = new buzz.sound(soundAsset+"6.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"7.ogg");
var dialog2main = [dialog2, dialog2part1];

var dialog3 = new buzz.sound(soundAsset+"8.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"10.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"11.ogg");
var dialog3main = [dialog3, dialog3part1, dialog3part2, dialog3part3];

var dialog4 = new buzz.sound(soundAsset+"12.ogg");
var dialog4part1 = new buzz.sound(soundAsset+"13.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"14.ogg");
var dialog4main = [dialog4, dialog4part1, dialog4part2];

var dialog5 = new buzz.sound(soundAsset+"15.ogg");
var dialog5part1 = new buzz.sound(soundAsset+"16.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"17.ogg");
var dialog5main = [dialog5, dialog5part1, dialog5part2];

var dialog6 = new buzz.sound(soundAsset+"18.ogg");
var dialog6part1 = new buzz.sound(soundAsset+"19.ogg");
var dialog6main = [dialog6, dialog6part1];

/*page71*/
var dialog7 = new buzz.sound(soundAsset+"20.ogg");
var dialog7part1 = new buzz.sound(soundAsset+"21.ogg");
var dialog7part2 = new buzz.sound(soundAsset+"22.ogg");

var dialog7main = [dialog7, dialog7part1, dialog7part2];

var dialog8 = new buzz.sound(soundAsset+"23.ogg");
var dialog8part1 = new buzz.sound(soundAsset+"24.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"25.ogg");
var dialog8main = [dialog8, dialog8part1, dialog8part2];

var dialog9 = new buzz.sound(soundAsset+"26.ogg");
var dialog9part1 = new buzz.sound(soundAsset+"27.ogg");
var dialog9part2 = new buzz.sound(soundAsset+"28.ogg");
var dialog9part3 = new buzz.sound(soundAsset+"29.ogg");
var dialog9main = [dialog9, dialog9part1, dialog9part2, dialog9part3];

var dialog10 = new buzz.sound(soundAsset+"30.ogg");
var dialog10part1 = new buzz.sound(soundAsset+"31.ogg");
var dialog10part2 = new buzz.sound(soundAsset+"32.1.ogg");
var dialog10main = [dialog10, dialog10part1, dialog10part2];

var soundcontent = [dialog0,dialog1main, dialog2main, dialog3main, dialog4main, dialog5main, dialog6main, dialog7main, dialog8main, dialog9main, dialog10main];


var content=[
    {
        bgImgSrc : imageAsset+"anita01.jpg",
        imageid : "bg_image1",
        dialog_id : "dialog_0",
        forwhichdialog : "dialog1main",
        lineCountDialog : [data.string.p1dialog0
        ],
        speakerImgSrc : $ref+"/images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : imageAsset+"anita01.jpg",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",
		forwhichdialog : "dialog1main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p1dialog1,
							data.string.p1dialog2,
							data.string.p1dialog3,
							data.string.p1dialog4,
							data.string.p1dialog5,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"mm.jpg",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		forwhichdialog : "dialog2main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p2dialog2,
							data.string.p2dialog2part1,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
		
		
	},
	
	{
		bgImgSrc : imageAsset+"solukhumbu01.jpg",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		forwhichdialog : "dialog3main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p3dialog3,
							data.string.p3dialog3part1,
							data.string.p3dialog3part2,
							data.string.p3dialog3part3,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"mountain.jpg",
		imageid : "bg_image4",
		dialog_id : "dialog_4main",
		forwhichdialog : "dialog4main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p4dialog4,
							data.string.p4dialog4part1,
							data.string.p4dialog4part2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"forest.jpg",
		imageid : "bg_image5",
		dialog_id : "dialog_5main",
		forwhichdialog : "dialog5main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p5dialog5,
							data.string.p5dialog5part1,
							data.string.p5dialog5part2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"flower.jpg",
		imageid : "bg_image6",
		dialog_id : "dialog_6main",
		forwhichdialog : "dialog6main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p6dialog6,
							data.string.p6dialog6part1,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
		
	},
	
	{
		bgImgSrc : imageAsset+"bg.jpg",
		imageid : "bg_image7",
		dialog_id : "dialog_7main",
		forwhichdialog : "dialog7main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p7dialog7,
							data.string.p7dialog7part1,
							data.string.p7dialog7part2,
							data.string.p7dialog7part3,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"birds.jpg",
		imageid : "bg_image8",
		dialog_id : "dialog_8main",
		forwhichdialog : "dialog8main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],

		lineCountDialog : [data.string.p8dialog8,
							data.string.p8dialog8part1,
							data.string.p8dialog8part2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"agriculture.jpg",
		imageid : "bg_image9",
		dialog_id : "dialog_9main",
		forwhichdialog : "dialog9main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [data.string.p9dialog9,
							data.string.p9dialog9part1,
							data.string.p9dialog9part2,
							data.string.p9dialog9part3,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : imageAsset+"tourism.jpg",
		imageid : "bg_image10",
		dialog_id : "dialog_10main",
		forwhichdialog : "dialog10main",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
			
		],
		lineCountDialog : [data.string.p10dialog10,
							data.string.p10dialog10part1,
							data.string.p10dialog10part2,
							],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	
];

// array that stores array of current audio to be played
var playThisDialog;


$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 11;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();
						
	}

	slide1();
	

	
	

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		
		if(countNext==4){
			$(".text01").show(0);
			$(".text02").show(0);
			$(".text03").show(0);
			$(".text04").show(0);
			}
		
		if(countNext==5){
			$(".text01").hide(0);
			$(".text02").hide(0);
			$(".text03").hide(0);
			$(".text04").hide(0);
			}
		
		if(countNext==6){
			$("#rhododendron1").show(0);
			$("#primrose1").show(0);
			$("#orchid1").show(0);
			$("#bamboo1").show(0);
			$("#oak1").show(0);
			$("#fodder1").show(0);
			$("#herb1").show(0);
			$(".text4").show(0);
			$(".text5").show(0);
			$(".text6").show(0);
			$(".text7").show(0);
			$(".text8").show(0);
			$(".text9").show(0);
			$(".text10").show(0);
			$(".text").hide(0);
			$(".text1").hide(0);
			$(".text2").hide(0);
			$(".text3").hide(0);
			
			
		}	

		if(countNext==7){
			$("#butterflies1").show(0);
			$("#reptiles1").show(0);
			$("#amphibians1").show(0);
			$("#fishes1").show(0);
			$(".text").show(0);
			$(".text1").show(0);
			$(".text2").show(0);
			$(".text3").show(0);
			$(".text4").hide(0);
			$(".text5").hide(0);
			$(".text6").hide(0);
			$(".text7").hide(0);
			$(".text8").hide(0);
			$(".text9").hide(0);
			$(".text10").hide(0);
			$("#rhododendron1").hide(0);
			$("#primrose1").hide(0);
			$("#orchid1").hide(0);
			$("#bamboo1").hide(0);
			$("#oak1").hide(0);
			$("#fodder1").hide(0);
			$("#herb1").hide(0);
		}	
		
		if(countNext==8){
			$("#butterflies1").hide(0);
			$("#reptiles1").hide(0);
			$("#amphibians1").hide(0);
			$("#fishes1").hide(0);
			$(".text").hide(0);
			$(".text1").hide(0);
			$(".text2").hide(0);
			$(".text3").hide(0);
			$(".text11").show(0);
			$(".text12").show(0);
			$(".text13").show(0);
			$(".text14").show(0);
			$(".text15").show(0);
			$(".text16").show(0);
			$(".text17").show(0);
			$(".text18").show(0);
			
		}
		
		if(countNext==9){
			$(".text11").hide(0);
			$(".text12").hide(0);
			$(".text13").hide(0);
			$(".text14").hide(0);
			$(".text15").hide(0);
			$(".text16").hide(0);
			$(".text17").hide(0);
			$(".text18").hide(0);
			$(".sherpa").show(0);
			$(".rai").show(0);
			$(".magar").show(0);
			$(".gurung").show(0);
			$(".tamang").show(0);
			$(".brahmin").show(0);
			$(".newar").show(0);
			$(".chhetri").show(0);

		}
		
		if(countNext==10){
			$(".sherpa").hide(0);
			$(".rai").hide(0);
			$(".magar").hide(0);
			$(".gurung").hide(0);
			$(".tamang").hide(0);
			$(".brahmin").hide(0);
			$(".newar").hide(0);
			$(".chhetri").hide(0);
		}
		
		
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});
	

});




//$(document).ready(function(){
var imgpath = $ref+"/images/exercise4/";
(function () {

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	$('.title').text(data.string.e5title);

	var content = [
	{
		question : data.string.e4q1,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
				correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"apple.png"
	},

	{
		question : data.string.e4q2,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"elephant01.png"
	},
	{
		question : data.string.e4q3,
		answers: [
		   {ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
		img: imgpath+"van.jpg"
	},
	{
		question : data.string.e4q4,
		answers: [
		    {ans : data.string.eans4},
			{ans : data.string.eans5,
		    correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"icecream.png"
	},
	{
		question : data.string.e4q5,
		answers: [
		    {ans : data.string.eans4,
		      	correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"parrot.png"
	},
	{
		question : data.string.e4q6,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
			correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"pencil.png"
	},

	{
		question : data.string.e4q7,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
				correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"pencil.png"
	},
	{
		question : data.string.e4q8,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
			correct : "correct"}
		],
		img: imgpath+"books.png"
	},
	{
		question : data.string.e4q9,
		answers: [
			{ans : data.string.eans4,
			correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"college-girl.png"
	},
	{
		question : data.string.e4q10,
		answers: [
			{ans : data.string.eans4,
			correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"deligent.png"
	},
	{
		question : data.string.e4q11,
		answers: [
			{ans : data.string.eans4,
			correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"unicorn.png"
	},
	{
		question : data.string.e4q12,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
			correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"umbrella.png"
	},
	{
		question : data.string.e4q13,
		answers: [
		{ans : data.string.eans4},
		{ans : data.string.eans5},
		{ans : data.string.eans6,
		correct : "correct"},
		],
		img: imgpath+"luggage.jpg"
	}

];

$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	loadTimelineProgress(content.length,1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();

	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			answered = true;
			if(attemptcount == 1){
				correctlyanswered++;
			}
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString;
			play_correct_incorrect_sound(true);
		}
		else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<13){
			loadTimelineProgress(content.length,questionCount+1);
			qA();
		}
		else if (questionCount==13){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);

var imageAsset = $ref+"/images/exercise2/";
var transitionEnd = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";

var questioncontent=[];

var nodescontent=[
	{
		nodename : "nodeA",
		leftposition : "33.5%",
		bottomposition : "26%",
		testChar: "A",
	},
	{
		nodename : "nodeB",
		leftposition : "69.3%",
		bottomposition : "26%",
		testChar: "B",
	},
	{
		nodename : "nodeC",
		leftposition : "33.6%",
		bottomposition : "67.8%",
		testChar: "C",
	},
	{
		nodename : "nodeD",
		leftposition : "69.3%",
		bottomposition : "67.8%",
		testChar: "D",
	},
	{
		nodename : "nodeZ",
		leftposition : "33.5%",
		bottomposition : "2%",
		testChar: "Z",
	}
];


$(function($) {
	var $maparea = $('.maparea');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

	var $nodes = $maparea.children('div.nodes');
	var $characters = $maparea.children('div.characters');
	var $navigationbuttons = $maparea.children('div.navigationbuttons');
	var $directionBtn = $navigationbuttons.children('div.directionBtn');
	var $frontBtn = $navigationbuttons.children('div.frontBtn');
	var $leftBtn = $navigationbuttons.children('div.leftBtn');
	var $rightBtn = $navigationbuttons.children('div.rightBtn');
	var $youarehere = $maparea.children('div.youarehere');
	var $messageboard = $maparea.children('p.messageboard');
	var $tothisnode;
	var tothisnodebottom;
	var tothisnodeleft;
	var topostion;
	var currentnode;

	$youarehere.children('b').html(data.string.initialposition);
	$frontBtn.html(data.string.directionNorth);
	$leftBtn.html(data.string.directionWest);
	$rightBtn.html(data.string.directionEast);
	$messageboard.html(data.string.initialmsg);


	function tothisposition(uptothisnode){
		$tothisnode = $nodes.children(".node"+uptothisnode);
		// alert($tothisnode.attr("class"));
		tothisnodebottom = $tothisnode.attr("data-bottomposition");
		tothisnodeleft = $tothisnode.attr("data-leftposition");
		topostion = {"left":tothisnodeleft, "bottom":tothisnodebottom};
		// update current node
		currentnode = uptothisnode;
		// lines below update the direction button for correctness such that
		// on going to next node update the direction button
		// $directionBtn.css('pointer-events', 'none');
		$directionBtn.attr('data-correctdirection', 'false');
		$directionBtn.attr('data-gotothisnode',null);
		switch(uptothisnode){
			case "Z" : $frontBtn.attr({'data-correctdirection':'true','data-gotothisnode':"A"});
						break;
			case "A" : $frontBtn.attr({'data-correctdirection':'true','data-gotothisnode':"C"});
					   $rightBtn.attr({'data-correctdirection':'true','data-gotothisnode':"B"});
					   break;
			case "B" : $frontBtn.attr({'data-correctdirection':'true','data-gotothisnode':"D"});
					   break;
			case "C" : $rightBtn.attr({'data-correctdirection':'true','data-gotothisnode':"D"});
					   break;
			default:break;
		}

		// update the direction button which are wrong with specific error messages
		switch(uptothisnode){
			case "Z" : $rightBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgZright});
					   $leftBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgZleft});
						break;
			case "A" : $leftBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgAleft});
					   break;
			case "B" : $rightBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgBright});
					   $leftBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgBleft});
					   break;
			case "C" : $frontBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgCfront});
					   $leftBtn.attr({'data-wrongdirectionmsg':data.string.wrongdirmsgCleft});
					   break;
			default:break;
		}

		$characters.addClass('characterWalkCycle').css(topostion);
	}

	function nodestemplate(){
		var source = $("#nodestemplate-template").html();
		var template = Handlebars.compile(source);
		var html = template(nodescontent);
		$nodes.html(html);

		tothisposition("Z");
	};

	$characters.on(transitionEnd, function() {
		$characters.removeClass('characterWalkCycle');
		$characters.attr('data-direction', 'movefront');
		$directionBtn.css('pointer-events', 'auto');
		if(currentnode == "Z"){
			$youarehere.show(0);
		}
		else if(currentnode === "D"){
			$navigationbuttons.css('display', 'none');
			// update congratulations msg here
			$messageboard.html(data.string.reachedpatanmsg).show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
		else if(currentnode === "A" || currentnode === "B" || currentnode === "C"){
			$messageboard.html(data.string.junctionmsg).show(0);
		}
	});

	$directionBtn.on('click', function() {
		if(currentnode == "Z"){
			$youarehere.css('display', 'none');
		}
		switch($(this).attr('data-correctdirection')){
			case "false" : $messageboard.html($(this).attr('data-wrongdirectionmsg')).show(0);break;
			case "true" : $messageboard.css('display', 'none');
						   tothisposition($(this).attr("data-gotothisnode"));
						   break;
			default:break;
		}
	});

	nodestemplate();
})(jQuery);

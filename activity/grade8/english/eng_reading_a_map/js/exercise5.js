//$(document).ready(function(){
var imgpath = $ref+"/images/exercise5/";
(function () {

	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	$('.title').text(data.string.e5title);
	var content = [
	{
		question : data.string.e5q1,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2,
				correct : "correct"},
			{ans : data.string.eans3},
		],
		img: imgpath+"honest.png"
	},

	{
		question : data.string.e5q2,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
			correct : "correct"},
		],
		img: imgpath+"orange.jpg"
	},
	{
		question : data.string.e5q3,
		answers: [
		   {ans : data.string.eans4,
			correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"doctor.png"
	},
	{
		question : data.string.e5q4,
		answers: [
		    {ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
			correct : "correct"},
		],
		img: imgpath+"man.png"
	},
	{
		question : data.string.e5q5,
		answers: [
		    {ans : data.string.eans4,
		      	correct : "correct"},
			{ans : data.string.eans5},
			{ans : data.string.eans6},
		],
		img: imgpath+"bright_sunny_day.png"
	},
	{
		question : data.string.e5q6,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
			correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"family.png"
	},

	{
		question : data.string.e5q7,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
				correct : "correct"},
		],
		img: imgpath+"earthquake.png"
	},
	{
		question : data.string.e5q8,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
			correct : "correct"}
		],
		img: imgpath+"Ramayan.jpg"
	},
	{
		question : data.string.e5q9,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
				correct : "correct"},
		],
		img: imgpath+"flute.png",
		extraclass: 'long_image'
	},
	{
		question : data.string.e5q10,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
				correct : "correct"},
		],
		img: imgpath+"earth.png"
	},
	{
		question : data.string.e5q11,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5,
			correct : "correct"},
			{ans : data.string.eans6},
		],
		img: imgpath+"grandfather.png"
	},
	{
		question : data.string.e5q12,
		answers: [
			{ans : data.string.eans4},
			{ans : data.string.eans5},
			{ans : data.string.eans6,
			correct : "correct"},
		],
		img: imgpath+"guest.jpg"
	},
	{
		question : data.string.e5q13,
		answers: [
		{ans : data.string.eans4},
		{ans : data.string.eans5},
		{ans : data.string.eans6,
		correct : "correct"},
		],
		img: imgpath+"father_teacher.png"
	},
	{
		question : data.string.e5q14,
		answers: [
		{ans : data.string.eans4,
		correct : "correct"},
		{ans : data.string.eans5},
		{ans : data.string.eans6},
		],
		img: imgpath+"goat-growth.png"
	},
	{
		question : data.string.e5q15,
		answers: [
		{ans : data.string.eans1,
		correct : "correct"},
		{ans : data.string.eans2},
		{ans : data.string.eans3},
		],
		img: imgpath+"one_eyedman.jpg"
	},
	{
		question : data.string.e5q16,
		answers: [
		{ans : data.string.eans4,correct : "correct"},
		{ans : data.string.eans5},
		{ans : data.string.eans6},
		],
		img: imgpath+"curly_hair.jpg"
	},
	{
		question : data.string.e5q17,
		answers: [
		{ans : data.string.eans1},
		{ans : data.string.eans2},
		{ans : data.string.eans3,
		correct : "correct"},
		],
		img: imgpath+"help_homework.png"
	}
];


$nextBtn.hide(0);

	// console.log(content);

	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	loadTimelineProgress(content.length,questionCount+1);


	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}

	qA();

	var answered = false;
	var attemptcount = 0;

	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);

		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			answered = true;
			if(attemptcount == 1){
				correctlyanswered++;
			}
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString;
			play_correct_incorrect_sound(true);
		}
		else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	})
	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<17){
			loadTimelineProgress(content.length,questionCount+1);
			qA();
		}
		else if (questionCount==17){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// .html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				// "position": "absolute",
				// "top": "48%",
				// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
		}
	});

$(".closebtn").on("click",function(){
		ole.activityComplete.finishingcall();
	});
})(jQuery);

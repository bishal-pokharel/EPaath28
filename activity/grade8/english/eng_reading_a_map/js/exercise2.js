function shuffleArray(array) {
		    for (var i = array.length - 1; i > 0; i--) {
		        var j = Math.floor(Math.random() * (i + 1));
		        var temp = array[i];
		        array[i] = array[j];
		        array[j] = temp;
		    }
		    return array;
	}

var imageAsset = $ref+"/images/exercise2/";
var soundAsset = $ref+"/sounds/exercise2/";
var animationtransitionEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";

var correctoptioncontent =[
	data.string.e2blank1ans,
	data.string.e2blank2ans,
	data.string.e2blank3ans,
	data.string.e2blank4ans,
	data.string.e2blank5ans,
	data.string.e2blank6ans,
	data.string.e2blank7ans,
	data.string.e2blank8ans,
];

var questioncontent=[];

var animationcontent=[
	{
		positionprop : {
				
			},
		toaddclasses : "jumpgirl",
	},

	{
		positionprop : {
				left: '40%',
				bottom: '42%',
			},
		toaddclasses : "downhilltransition characterWalkCycle",
	},
	{
		positionprop : {
				left: '80%',
			},
		toaddclasses : "alongtheroad characterWalkCycle",
	},
	{
		positionprop : {
				left: '80%',
			},
		toaddclasses : "alongtheroad characterWalkCycle",
	},
	{
		charactercss : {
				opacity : "0",
			},
		rowboatcss : {
				opacity : "1",
			},
		toaddclasses : "",
	},
	{
		positionprop : {
				left: '68%',
			},
		toaddclasses : "throughbush characterWalkCycle",
	},
	{
		positionprop : {
				left: '24%',
			},
		toaddclasses : "throughmarket characterWalkCycle",
	},
	{
		positionprop : {
				left: '12%',
			},
		toaddclasses : "toschool characterWalkCycle",
	},
];


$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = correctoptioncontent.length;
	loadTimelineProgress($total_page,countNext+1);

	var $questionline = $board.children('div.questionboard').children('div.questionline');
	var $optionsboard = $board.children('div.questionboard').children("div.optionsboard");
	var $character = $board.children('div.character');
	var $exerciseInstruction = $board.children('p.instruction');
	var $characterfront = $board.children('div.characterfront');
	var $bird = $board.children("div.bird");
	var $rowboat = $board.children("div.rowboat");

	$exerciseInstruction.text(data.string.e2instruction);
	// update the question content
	var SPACE =" ";
	var stringtoreplace = ["<span class='droparea' data-acceptclass='","'><span>","</span></span>"];
	var questiontext = data.string.e2para;
		questiontext = questiontext.replace(/\./g,".splithere"); /*making sure the full stop does not get stripped away when split*/
	var questionlines = questiontext.split("splithere");
	questionlines.pop(); /*remove empty string at last index in array caused with split function*/
	
	$.each(questionlines, function(i, val){			
		questionlines[i] = val.replace("#blankarea#",stringtoreplace[0]+correctoptioncontent[i]+stringtoreplace[1]+correctoptioncontent[i]+stringtoreplace[2]);			
		questioncontent.push(questionlines[i]);
		console.log(questioncontent[i]+"\n");
	});

	function questionline(){
		var source = $("#questionline-template").html();
		var template = Handlebars.compile(source);
		var html = template(questioncontent[countNext]);
		$questionline.html(html);
		
		var $droparea = $questionline.children('p').children('span.droparea');
		$droparea.droppable({ 
		tolerance: "pointer", 
		accept : function(dropElem){
			//dropElem was the dropped element, return true or false to accept/refuse
			if($(this).data('acceptclass') === dropElem.data('acceptthis')){
				return true;
			}
		},
		drop : function (event, ui){
			$(this).html($(ui.draggable).text());
			$(this).css({"background-color": "rgb(0, 184, 212)",
							"border": "2px outset rgb(0, 184, 212)",
							"color":"white"});
			$(ui.draggable).css('display', 'none');
			switch(countNext){
				case 0: 
				case 1:
				case 2: $character.addClass(animationcontent[countNext].toaddclasses).css(animationcontent[countNext].positionprop);
						break;
				case 3: $bird.show(500);$nextBtn.show(0);
						break;			
				case 4: $character.css(animationcontent[countNext].charactercss);
						$rowboat.addClass("rowboatransition").css(animationcontent[countNext].rowboatcss);
						$bird.css(animationcontent[countNext].charactercss);
						break;
				case 5:
				case 6:
				case 7: $characterfront.addClass(animationcontent[countNext].toaddclasses).css(animationcontent[countNext].positionprop);
						break;				
				default:break;
			}
			
		}
	});						
	}

	function endslide(){
		var source = $("#endslide-template").html();
		var template = Handlebars.compile(source);
		var html = template();
		$board.html(html);

		$endslide = $board.children('div.endslide');
		$fullpara = $endslide.children('p.fullpara');

		var fullparatext = data.string.e2endslidepara;
			fullparatext = fullparatext.replace(/#/g,"<span class='highlightpara'>");
			fullparatext = fullparatext.replace(/@/g,"</span>");
			$fullpara.html(fullparatext);
	}

	function questionoption(){
		var source = $("#questionoption-template").html();
		var template = Handlebars.compile(source);
		var html = template(shuffleArray(correctoptioncontent));
		$optionsboard.html(html);

		var $options = $optionsboard.children('span');
		$options.draggable({containment: ".questionboard", revert:"invalid"});	
		$options.one('mousedown',  function() {
			/* Act on the event */
			$exerciseInstruction.css('display', 'none');
		});	
	}

	$character.on(animationtransitionEnd, function(){
			$(this).removeClass(animationcontent[countNext].toaddclasses);
			$nextBtn.show(0);
		});

	$characterfront.on(animationtransitionEnd, function(){
			$(this).removeClass(animationcontent[countNext].toaddclasses);
			$nextBtn.show(0);
		});
		

	$rowboat.on(animationtransitionEnd, function(){
			if(countNext==4){
				$(this).css({"bottom":"30%"});
				$nextBtn.show(0);
			}	
		});

		$nextBtn.on('click', function(){
			$(this).css("display","none");	
			countNext++;
			if(countNext == 1)
			{
				$(".title").show(0);
			}
			if(countNext == 5)
			{
				$rowboat.css({"opacity":"0"});
				$characterfront.css({"opacity":"1"});
			}
			if(countNext <= 7)
			{
				questionline();		
			}
			if(countNext > 7){
				endslide();
				ole.footerNotificationHandler.pageEndSetNotification();
			}
			loadTimelineProgress($total_page,countNext+1);			
		});

	questionline();
	questionoption();	
})(jQuery);
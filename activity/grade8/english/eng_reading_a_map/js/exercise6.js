$('.title').text(data.string.p4title);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.p4p1,
				description : data.string.p4des1,
				img : $ref + "/images/exercise3/route.jpg",
			},//
			{
				words : data.string.p4p2,
				description : data.string.p4des2,
				img : $ref + "/images/exercise3/history.jpg",
			},//

			{
				words : data.string.p4p3,
				description : data.string.p4des3,
				img : $ref + "/images/exercise3/enthusiast.jpg",
			},//

			{
				words : data.string.p4p4,
				description : data.string.p4des4,
				img : $ref + "/images/exercise3/accompany.jpg",
			},//
			{
				words : data.string.p4p5,
				description : data.string.p4des5,
				img : $ref + "/images/exercise3/confidence.jpg",
			},//

			{
				words : data.string.p4p6,
				description : data.string.p4des6,
				img : $ref + "/images/exercise3/alley.jpg",
			},//

			{
				words : data.string.p4p7,
				description : data.string.p4des7,
				img : $ref + "/images/exercise3/intersection.jpg",
			},
			{
				words : data.string.p4p8,
				description : data.string.p4des8,
				img : $ref + "/images/exercise3/proceed.jpg",
			},//

			{
				words : data.string.p4p9,
				description : data.string.p4des9,
				img : $ref + "/images/exercise3/spectacular.jpg",
			},//
			]
			console.log(content);
			displaycontent();
			function displaycontent() {
				loadTimelineProgress(1,1);
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);
					ole.footerNotificationHandler.pageEndSetNotification();

				});
			}
			/*
			 * function qA() { var source = $('#qA-templete').html(); var
			 * template = Handlebars.compile(source); var html =
			 * template(content); //$board.html(html); // console.log(html); }
			 *
			 * qA();
			 */

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	});
});

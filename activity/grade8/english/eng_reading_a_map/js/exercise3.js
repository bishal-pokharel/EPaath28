//$(document).ready(function(){
(function () {
	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	$('.title').text(data.string.e5title);

	var content = [
	{
		question : data.string.e3q1,
		answers: [
			{ans : data.string.eans1,
				correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],	
	},

	{
		question : data.string.e3q2,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2,
			correct : "correct"},
			{ans : data.string.eans3},
		],	
	},
	{
		question : data.string.e3q3,
		answers: [
		    {ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
		      	correct : "correct"},
		],
	},
	{
		question : data.string.e3q4,
		answers: [
		    {ans : data.string.eans1,
		    correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
	},
	{
		question : data.string.e3q5,
		answers: [
		    {ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
		      	correct : "correct"},
		],
	},
	{
		question : data.string.e3q6,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2,
			correct : "correct"},
			{ans : data.string.eans3},		
		],		
	},
	
	{
		question : data.string.e3q7,
		answers: [
			{ans : data.string.eans1},		
			{ans : data.string.eans2},
			{ans : data.string.eans3,
				correct : "correct"},
		],
	},
	{
		question : data.string.e3q8,
		answers: [
			{ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3}
		],
		
	},
	{
		question : data.string.e3q9,
		answers: [
			{ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
		
	},
	{
		question : data.string.e3q10,
		answers: [
		{ans : data.string.eans1},
		{ans : data.string.eans2,
			correct : "correct"},
		{ans : data.string.eans3},
		],
		
	},
	{
		question : data.string.e3q11,
		answers: [
		{ans : data.string.eans1},
		{ans : data.string.eans2},
		{ans : data.string.eans3,
			correct : "correct"},
		],
		
	},
	{
		question : data.string.e3q12,
		answers: [
			{ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
	},
	{
		question : data.string.e3q13,
		answers: [
		{ans : data.string.eans1,
		correct : "correct"},
		{ans : data.string.eans2},
		{ans : data.string.eans3},
		],
		
	},
	{
		question : data.string.e3q14,
		answers: [
		{ans : data.string.eans1},
		{ans : data.string.eans2},
		{ans : data.string.eans3,
			correct : "correct"},
		],
	},
	{
		question : data.string.e3q15,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
			correct : "correct"},
		],
	},
	{
		question : data.string.e3q16,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2,
				correct : "correct"},
			{ans : data.string.eans3},
		],
	},
	{
		question : data.string.e3q17,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
			correct : "correct"},
		],
	},
	{
		question : data.string.e3q18,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2},
			{ans : data.string.eans3,
			correct : "correct"},
		],
	},
	{
		question : data.string.e3q19,
		answers: [
			{ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
	},
	{
		question : data.string.e3q20,
		answers: [
			{ans : data.string.eans1},
			{ans : data.string.eans2,
			correct : "correct"},
			{ans : data.string.eans3},
		],
	},
	{
		question : data.string.e3q21,
		answers: [
			{ans : data.string.eans1,
			correct : "correct"},
			{ans : data.string.eans2},
			{ans : data.string.eans3},
		],
	},
	]

	$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("________", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<21){
			qA();
		}
		else if (questionCount==21){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);

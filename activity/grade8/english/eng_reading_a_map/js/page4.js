var content=[
	{	
		txt : data.string.p4p1,
		imageURL : $ref+"/images/page4/road.png",		
		description : data.string.p4des1,
	},
	{	
		txt : data.string.p4p2,
		imageURL : $ref+"/images/page4/history01.png",
		description : data.string.p4des2,		
	},
	{	
		txt : data.string.p4p3,
		imageURL : $ref+"/images/page4/enthusiast1.png",
		description : data.string.p4des3,		
	},
	{	
		txt : data.string.p4p4,
		imageURL : $ref+"/images/page4/accompany.png",
		description : data.string.p4des4,		
	},
	{	
		txt : data.string.p4p5,
		imageURL : $ref+"/images/page4/confidence01.png",
		description : data.string.p4des5,		
	},
	{	
		txt : data.string.p4p6,
		imageURL : $ref+"/images/page4/alley.png",
		description : data.string.p4des6,		
	},
	{	
		txt : data.string.p4p7,
		imageURL : $ref+"/images/page4/intersection01.png",
		description : data.string.p4des7,		
	},
	{	
		txt : data.string.p4p8,
		imageURL : $ref+"/images/page4/proceed.png",
		description : data.string.p4des8,		
	},
	{	
		txt : data.string.p4p9,
		imageURL : $ref+"/images/page4/spectacular.png",
		description : data.string.p4des9,		
	},

];

var countNext = 0;
var clicked = 0;
var global;
var Count =0;

$(document).ready(function () {
	$("div.wrapper p.words").click (function(){
		global = $(this).attr("id");
		$("#Route").removeClass("highlight");
		$("#History").removeClass("highlight");	
		$("#Enthusiast").removeClass("highlight");		
		$("#Accompany").removeClass("highlight");	
		$("#Confidence").removeClass("highlight");	
		$("#Alley").removeClass("highlight");	
		$("#Intersection").removeClass("highlight");	
		$("#Proceed").removeClass("highlight");
		$("#Spectacular").removeClass("highlight");		

		switch(global){
			case "Route": 
				first(0);
				$(".card").show(0);
				$("#Route").addClass("highlight");	
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};
			break;
			
			case "History": 
				first(1);
				$(".card").show(0);
				$("#History").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			case "Enthusiast": 
				first(2);
				$(".card").show(0);
				$("#Enthusiast").addClass("highlight");	
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};
				break;

			case "Accompany": 
				first(3);
				$(".card").show(0);
				$("#Accompany").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			case "Confidence": 
				first(4);
				$(".card").show(0);
				$("#Confidence").addClass("highlight");	
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};
				break;

			case "Alley": 
				first(5);
				$(".card").show(0);
				$("#Alley").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			case "Intersection":
				first(6); 
				$(".card").show(0);
				$("#Intersection").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			case "Proceed": 
				first(7);
				$(".card").show(0);
				$("#Proceed").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			case "Spectacular": 
				first(8);
				$(".card").show(0);
				$("#Spectacular").addClass("highlight");
				Count++;
				if (Count ==9) {
					ole.footerNotificationHandler.lessonEndSetNotification();	
				};	
				break;

			default:break;

		}
	});
	
	function first (countNext){
		var source = $("#first-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$(".board").html(html);

		$(".front").on('click',function(){
    		$(".front").removeClass("backclick").addClass("frontclick");
    		$(".front").css("z-index","-1");
    		$(".back").removeClass("frontclick").addClass("backclick");
    	});

    	$(".back").on('click',function(){
    		$(".front").removeClass("frontclick").addClass("backclick");
    		$(".front").css("z-index","1");
    		$(".back").removeClass("backclick").addClass("frontclick");
    	});
	};

});

$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/exerciseimages/mill.png",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/exerciseimages/sketch.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/exerciseimages/cathe.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/exerciseimages/dome.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/exerciseimages/app.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/exerciseimages/fas.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/exerciseimages/oo.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/exerciseimages/lyre.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/exerciseimages/keen.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/exerciseimages/ana.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/exerciseimages/dis.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/exerciseimages/bot.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/exerciseimages/fo.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/exerciseimages/pl.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/exerciseimages/mu.jpg",
			},//
			{
				words : data.string.word16,
				description : data.string.des16,
				img : $ref + "/exerciseimages/ch.jpg",
			},//
			{
				words : data.string.word17,
				description : data.string.des17,
				img : $ref + "/exerciseimages/di.jpg",
			},//

			{
				words : data.string.word18,
				description : data.string.des18,
				img : $ref + "/exerciseimages/co.jpg",
			},//

			{
				words : data.string.word19,
				description : data.string.des19,
				img : $ref + "/exerciseimages/re.jpg",
			},//
		
			
			];
			console.log(content);
			displaycontent();
			ole.footerNotificationHandler.pageEndSetNotification();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		//alert("ok");
		$(this).toggleClass('active');
	}); 
});
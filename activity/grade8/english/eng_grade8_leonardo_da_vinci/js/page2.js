imageAsset = $ref+"/slide_images/";
soundAsset = $ref+"/sounds/slide1/";
background_image = $ref+"/images/"


var dialog0 = new buzz.sound(soundAsset+"leonardodavinci.ogg");
var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog2 = new buzz.sound(soundAsset+"2.ogg");
var dialog3 = new buzz.sound(soundAsset+"3.ogg");
var dialog4 = new buzz.sound(soundAsset+"4.ogg");
var dialog5 = new buzz.sound(soundAsset+"5.ogg");
var dialog6 = new buzz.sound(soundAsset+"6.ogg");
var dialog7 = new buzz.sound(soundAsset+"7.ogg");



//This will have all the sound 
var soundcontent = [dialog0,dialog1, dialog2, dialog3, dialog4,
					dialog5, dialog6, dialog7]; 


var content=[
    {
        bgImgSrc : imageAsset+"leonardo-da-vinci.png",//image
        imageid : "bg_image2",
        dialog_id : "dialog_0",
        forwhichdialog : "dialog0", //audio
        lineCountDialog : [data.string.dig0,],
        speakerImgSrc : $ref+"/images/speaker.png",
        listenAgainText : data.string.listenAgainData,
    },
	{
		bgImgSrc : background_image+"leonardo.png",//image
		imageid : "bg_image1",
		dialog_id : "dialog_1",
		forwhichdialog : "dialog1", //audio
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			}
		],
		lineCountDialog : [data.string.dig1,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : background_image+"vinci.png",//image
		imageid : "bg_image2",
		dialog_id : "dialog_2",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.dig1_1,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	{
		bgImgSrc :  background_image+"painting.png",//image
		imageid : "bg_image3",
		dialog_id : "dialog_3",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.dig2,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"river.png",//image
			imageid : "bg_image4",
		dialog_id : "dialog_4",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"1.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			
		],
		lineCountDialog : [data.string.dig3,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"watermill.png",//image
			imageid : "bg_image5",
		dialog_id : "dialog_5",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.dig4,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc :  background_image+"bank.png",//image
			imageid : "bg_image6",
		dialog_id : "dialog_6",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.dig5,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc :  background_image+"watermill.png",//image
			imageid : "bg_image7",
		dialog_id : "dialog_7",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.dig6,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 8;
	loadTimelineProgress($total_page,countNext+1);

	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}
			playerbinder();
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);




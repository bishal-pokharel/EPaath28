(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.mat1,
		answers: [
		{ans : data.string.ma1,correct : "correct"},
		{ans : data.string.ma2},
		{ans : data.string.ma3},
		]	
	},
	{
		question : data.string.mat2,
		answers: [
		{ans : data.string.ma4},
		{ans : data.string.ma5},
		{ans : data.string.ma6,correct : "correct"},
		]
	},
	{
		question : data.string.mat3,
		answers: [
		{ans : data.string.ma7},
		{ans : data.string.ma8},
		{ans : data.string.ma9,correct : "correct"},
		]
		
	},
	{
		question : data.string.mat4,
		answers: [
		{ans : data.string.ma10,correct : "correct"},
		{ans : data.string.ma11},
		{ans : data.string.ma12},
		]
	},
	{
		question : data.string.mat5,
		answers: [
		{ans : data.string.ma13},
		{ans : data.string.ma14,correct : "correct"},
		{ans : data.string.ma15},
		]
	},

	{
		question : data.string.mat6,
		answers: [
		{ans : data.string.ma16,correct : "correct"},
		{ans : data.string.ma17},
		{ans : data.string.ma18},
		]
	},

	{
		question : data.string.mat7,
		answers: [
		{ans : data.string.ma19},
		{ans : data.string.ma20},
		{ans : data.string.ma21,correct : "correct"},
		]
	},

	{
		question : data.string.mat8,
		answers: [
		{ans : data.string.ma22},
		{ans : data.string.ma23},
		{ans : data.string.ma24,correct : "correct"},
		]
	},

	{
		question : data.string.mat9,
		answers: [
		{ans : data.string.ma25},
		{ans : data.string.ma26,correct : "correct"},
		{ans : data.string.ma27},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
var id=0;
var id_num = 1;
var correct = 0;
$(document).ready(function(){
	var continents = [ {
		cont : data.string.loc1
	   },//
	   {
		cont : data.string.loc2
	   },//

	   {
		cont : data.string.loc3
	   },//

	   {
		cont : data.string.loc4
	   },//
	 
	   {
		cont : data.string.loc5
	   },//
	   
	   {
		cont : data.string.loc6
	   },//
	   
	   {
		cont : data.string.loc7
	   },//
	  ];
	     var map_asiablock = [
	                 {
	                	 blockasia_map : data.string.block1
	                 },
	                 ];
	     var map_europeblock = [
	                 {
	                	 blockeurope_map : data.string.block2
	                 },
	                 ];
	      var map_africablock = [
	                 {
	                	 blockafrica_map : data.string.block3
	                 },
	                 ];
	        var map_australiablock = [
	                 {
	                	 blockaustralia_map : data.string.block4
	                 },
	                 ];
			var map_northblock = [
	                 {
	                	 blocknorth_map : data.string.block5
	                 },
	                 ];
	        var map_southblock = [
	                 {
	                	 blocksouth_map : data.string.block6
	                 },
	                 ];

	        var map_antarticablock = [
	                 {
	                	 blockantartica_map : data.string.block7
	                 },
	                 ];
		// this is the function for the draggable location
		alllocationdragsection();
		asiablock();
		europeblock();
		africablock();
		australiablock();
		northblock();
		southblock();
		antarticablock();
		initialize();

		function alllocationdragsection(){
		var loc_content =$.each(continents,function(key,values){
			co = values.cont;
			id++;
			
			console.log(co);
			appendlocation ='<div class="cardPile">';
			appendlocation +='<div id="element_'+ id +'" data-number="'+id+'">';
			appendlocation += '<p class="location_' +id +' " > ' + co +' </p>';
			appendlocation +='</div>';
			appendlocation +='</div>';
			$(".data_xml").append(appendlocation);
		});
	}

        // now we need to build the droppable positions 
    	function asiablock(){
		var context =$.each(map_asiablock,function(key,values){
			asiamap = values.blockasia_map;	
			console.log(asiamap);	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="asia_drop_box" data-number="1">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".asia").append(appendContents);
		});
		}

		function europeblock(){
			var context =$.each(map_europeblock,function(key,values){
			europemap = values.blockeurope_map;	
			console.log(europemap);	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="europe_drop_box" data-number="2">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".europe").append(appendContents);
		});
		}

		function africablock(){
			var context =$.each(map_africablock,function(key,values){
			africamap = values.blockafrica_map;	
			console.log(europemap);	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="africa_drop_box" data-number="3">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".africa").append(appendContents);
		});
		}


		function australiablock(){
			var context =$.each(map_australiablock,function(key,values){
			australiamap = values.blockaustralia_map;	
			console.log(europemap);	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="australia_drop_box" data-number="4">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".australia").append(appendContents);
		});
		}

		function northblock(){
			var context =$.each(map_northblock,function(key,values){
			northmap = values.blocknorth_map;	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="north_drop_box" data-number="5">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".north").append(appendContents);
		});
		}

		function southblock(){
			var context =$.each(map_southblock,function(key,values){
		    southmap = values.blocksouth_map;	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="south_drop_box" data-number="6">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".south").append(appendContents);
		});
		}

		function antarticablock(){
			var context =$.each(map_antarticablock,function(key,values){
			antarticamap = values.blockantartica_map;	
			console.log(europemap);	
			appendContents ='<div class="cardslots">';
			appendContents +='<div id="slot_1" class="antartica_drop_box" data-number="7">';
			
			appendContents +='</div>';
			appendContents +='</div>';
			$(".antartica").append(appendContents);
		});
		}
		// initializing the datas
		function initialize() {
			
			$('.cardPile div').draggable({
				stack: '.cardSlots div',
				containment : '#content',
				cursor : 'move',
				revert : function() {
					if($(this).data('number') != $(this).parents('div.ui-droppable').data('number')) {
						return true;
					} else {
						return false;
					}
				}
			});
			
			$('.cardSlots div').droppable({
				hoverClass : 'hovered',
				tolerance: 'touch',
				drop : handleCardDrop
			});
		}

		function handleCardDrop(event, ui) {

		var slotNumber = $(this).data('number');
		var cardNumber = ui.draggable.data('number');
		console.log(slotNumber);
		console.log(cardNumber);
		if (slotNumber == cardNumber) {

			correct++;
			console.log(correct);
		//to get the actual world location by class name
		var test = $(event.target).attr('class');
		var locationworld = test.split('_')[0];
		$("." + locationworld + "_text").show(0);

		ui.draggable.addClass('correct');
		//ui.draggable.draggable('disable');
		//ui.draggable.clone(true).css('position', 'inherit').appendTo($(this));
	    ui.draggable.remove();
		$(this).droppable('disable');
		
	}
	if(correct == 7)
	{	
	
		// $(".finishtxt").show(0);
		ole.footerNotificationHandler.pageEndSetNotification();
	}
}

		$(".conti_exercise").click(function(){
			$(this).remove();
			$(".asia_text").hide(0);
			$(".europe_text").hide(0);
			$(".africa_text").hide(0);
			$(".north_text").hide(0);
			$(".south_text").hide(0);
			$(".antartica_text").hide(0);
			$(".australia_text").hide(0);
			$(this).hide(0);
			$(".data_xml").show(0);
			$(".drag_text").show(0);
		});
});
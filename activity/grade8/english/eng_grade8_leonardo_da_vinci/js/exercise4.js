var correctCards = 0;
	$(init);

	function init() {
		// Hide the success message
		$('#successMessage').hide(0);
		$('#successMessage').css({
			left : '580px',
			top : '250px',
			width : 0,
			height : 0
		});

		// Reset the game
		correctCards = 0;
		$('#cardPile').html('');
		$('#cardSlots').html('');

		// Create the pile of shuffled cards
		var fruits = ["painter", "botanist", "inventor", "architect", "musician", "poet"];
		var orgFruits = fruits;
		fruits.sort(function() {
			return Math.random() - .5;
		});
		// var fruit="apple.jpg";
		
		for (var i = 1; i <= 6; i++) {
		
			$('<div id="drag'+orgFruits[i - 1]+'">' + orgFruits[i - 1] + '</div>').data('orgFruits', i).appendTo('#cardPile').draggable({
				
				containment : '#wrapper',
				stack : '#cardPile div',
				cursor : 'move',
				revert : true,
				
			});
		}
		

		// Create the card slots
		orgFruits.sort(function() {
			return Math.random() - .5;
		});
		
		for (var i = 0; i < 6; i++) {
		var appendText="<div id='"+fruits[i]+"' data-fruits='"+fruits[i]+"' class='col-xs-4 col-sm-4 col-md-4 col-lg-4 droppable'>";
		appendText+= "<img src='activity/grade8/english/eng_grade8_leonardo_da_vinci/exerciseimages/"+fruits[i]+".png' class='image_style'>";
		appendText+="</img>";
		appendText+="</div>";
		//$("#cardSlots").append(appendText);
		$(appendText).appendTo('#cardSlots').droppable({
			accept : '#cardPile div',
			hoverClass : 'hovered',
			drop : handleCardDrop,
			
		});
		}
	}
 
	function handleCardDrop(event, ui) {
		var slotNumber = $(this).attr('id');
		var cardNumber = ui.draggable.text();

		if (slotNumber == cardNumber) {
			ui.draggable.addClass('correct');
			ui.draggable.draggable('disable');
			ui.draggable.css('font-size','20px');
			$(this).droppable('disable');
			// cardnumber.hide(0);
			ui.draggable.position({
				of : $(this),
				my : 'left top',
				at : 'left top'
			});
			ui.draggable.draggable('option', 'revert', false);
			correctCards++;
			$("#cardPile").find("#drag"+cardNumber).remove();
			$(this).append("<div class='col-md-10 col-sm-11 col-xs-12 end'>"+cardNumber+"</div>");	
		}
		// If all the cards have been placed correctly then display a message
		// and reset the cards for another go
		if (correctCards == 6) {
			// $('.finishtxt').show(0);

			ole.footerNotificationHandler.pageEndSetNotification();
			
		}
	}
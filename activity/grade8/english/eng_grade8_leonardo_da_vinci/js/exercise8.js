(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.mat27,
		answers: [
		{ans : data.string.ma76},
		{ans : data.string.ma77},
		{ans : data.string.ma78,correct : "correct"},
		]	
	},
	{
		question : data.string.mat28,
		answers: [
		{ans : data.string.ma79,correct : "correct"},
		{ans : data.string.ma80},
		{ans : data.string.ma81},
		]
	},
	{
		question : data.string.mat29,
		answers: [
		{ans : data.string.ma82},
		{ans : data.string.ma83,correct : "correct"},
		{ans : data.string.ma84},
		]
		
	},
	{
		question : data.string.mat30,
		answers: [
		{ans : data.string.ma85,correct : "correct"},
		{ans : data.string.ma86},
		{ans : data.string.ma87},
		]
	},
	{
		question : data.string.mat31,
		answers: [
		{ans : data.string.ma88},
		{ans : data.string.ma89,correct : "correct"},
		{ans : data.string.ma90},
		]
	},

	{
		question : data.string.mat32,
		answers: [
		{ans : data.string.ma91,correct : "correct"},
		{ans : data.string.ma92},
		{ans : data.string.ma93},
		]
	},

	{
		question : data.string.mat33,
		answers: [
		{ans : data.string.ma94},
		{ans : data.string.ma95,correct : "correct"},
		{ans : data.string.ma96},
		]
	},

	{
		question : data.string.mat34,
		answers: [
		{ans : data.string.ma97},
		{ans : data.string.ma98},
		{ans : data.string.ma99,correct : "correct"},
		]
	},

	{
		question : data.string.mat35,
		answers: [
		{ans : data.string.ma100},
		{ans : data.string.ma101,correct : "correct"},
		{ans : data.string.ma102},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;


	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$(".title").hide(0);
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			// $('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				// "position": "absolute",
				// "top": "48%",
				// "transform": "translateY(-50%)"
			// });
			// ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

	$(".closebtn").on("click",function(){
		ole.activityComplete.finishingcall();
	});

})(jQuery);
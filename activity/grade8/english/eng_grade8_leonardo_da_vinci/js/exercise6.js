(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	
	{
		question : data.string.mat10,
		answers: [
		{ans : data.string.ma28},
		{ans : data.string.ma29,correct : "correct"},
		{ans : data.string.ma30},
		]
	},
	{
		question : data.string.mat12,
		answers: [
		{ans : data.string.ma31,correct : "correct"},
		{ans : data.string.ma32},
		{ans : data.string.ma33},
		]
		
	},
	{
		question : data.string.mat13,
		answers: [
		{ans : data.string.ma34},
		{ans : data.string.ma35},
		{ans : data.string.ma36,correct : "correct"},
		]
	},
	{
		question : data.string.mat14,
		answers: [
		{ans : data.string.ma37},
		{ans : data.string.ma38},
		{ans : data.string.ma39,correct : "correct"},
		]
	},

	{
		question : data.string.mat15,
		answers: [
		{ans : data.string.ma40},
		{ans : data.string.ma41},
		{ans : data.string.ma42,correct : "correct"},
		]
	},

	{
		question : data.string.mat16,
		answers: [
		{ans : data.string.ma43},
		{ans : data.string.ma44,correct : "correct"},
		{ans : data.string.ma45},
		]
	},

	{
		question : data.string.mat17,
		answers: [
		{ans : data.string.ma46,correct : "correct"},
		{ans : data.string.ma47},
		{ans : data.string.ma48},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<7){
			qA();
		}
		else if (questionCount==7){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
	var id=0;
	var id_num = 1;
	correct = 0;
$(document).ready(function(){
	var italyplaces = [ {
			it : data.string.it1
		   },//
		   {
			it : data.string.it2
		   },//

		   {
			it : data.string.it3
		   },//

		   {
			it : data.string.it4
		   },//
		 
		   {
			it : data.string.it5
		   },//
		   
		   {
			it : data.string.it6
		   },//
		   
		  ];


		  var map_vinciblock = [
	                 {
	                	 blockvinci_map : data.string.itblock1
	                 },
	                 ];
	     var map_florenceblock = [
	                 {
	                	 blockflorence_map : data.string.itblock2
	                 },
	                 ];
	      var map_romeblock = [
	                 {
	                	 blockrome_map : data.string.itblock3
	                 },
	                 ];
	        var map_milanblock = [
	                 {
	                	 blockmilan_map : data.string.itblock4
	                 },
	                 ];
			var map_veniceblock = [
	                 {
	                	 blockvenice_map : data.string.itblock5
	                 },
	                 ];
	        var map_pisablock = [
	                 {
	                	 blockpisa_map : data.string.itblock6
	                 },
	                 ];


		  alllocationdragsection();
		  vinciblock();
		  florenceblock();
		  romeblock();
		  milanblock();
		  veniceblock();
		  pisablock();
		  initialize();
		  function alllocationdragsection(){
				var it_content =$.each(italyplaces,function(key,values){
					italy_con = values.it;
					id++;
					
					appendlocation ='<div class="cardPile">';
					appendlocation +='<div id="element_'+ id +'" data-number="'+id+'">';
					appendlocation += '<p class="location_' +id +' " > ' + italy_con +' </p>';
					appendlocation +='</div>';
					appendlocation +='</div>';
					$(".data_xml").append(appendlocation);
				});
		  }


		  function vinciblock(){
				var context =$.each(map_vinciblock,function(key,values){
					vincimap = values.blockvinci_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="vinci_drop_box" data-number="1">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".vinci").append(appendContents);
				});
		 }

		 function florenceblock(){
				var context =$.each(map_florenceblock,function(key,values){
					florencemap = values.blockflorence_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="florence_drop_box" data-number="2">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".florence").append(appendContents);
				});
		 }

		 	function romeblock(){
				var context =$.each(map_romeblock,function(key,values){
					romemap = values.blockrome_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="rome_drop_box" data-number="3">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".rome").append(appendContents);
				});
		 }

		 function milanblock(){
				var context =$.each(map_milanblock,function(key,values){
					milanmap = values.blockmilan_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="milan_drop_box" data-number="4">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".milan").append(appendContents);
				});
		 }

		 function veniceblock(){
				var context =$.each(map_veniceblock,function(key,values){
					venicemap = values.blockvenice_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="venice_drop_box" data-number="5">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".venice").append(appendContents);
				});
		 }

		  function pisablock(){
				var context =$.each(map_pisablock,function(key,values){
					pisamap = values.blockpisa_map;		
					appendContents ='<div class="cardslots">';
					appendContents +='<div id="slot_1" class="pisa_drop_box" data-number="6">';
					
					appendContents +='</div>';
					appendContents +='</div>';
					$(".pisa").append(appendContents);
				});
		 }



		 	function initialize() {
			
			$('.cardPile div').draggable({
				stack: '.cardSlots div',
				containment : '#content',
				cursor : 'move',
				revert : function() {
					if($(this).data('number') != $(this).parents('div.ui-droppable').data('number')) {
						return true;
					} else {
						return false;
					}
				}
			});
			
			$('.cardSlots div').droppable({
				hoverClass : 'hovered',
				tolerance: 'touch',
				drop : handleCardDrop
			});
		}

		function handleCardDrop(event, ui) {

		var slotNumber = $(this).data('number');
		var cardNumber = ui.draggable.data('number');
		console.log(slotNumber);
		console.log(cardNumber);
		if (slotNumber == cardNumber) {
			correct++;
			console.log(correct);
		
		//to get the actual world location by class name
		var test = $(event.target).attr('class');
		var locationworld = test.split('_')[0];
		$("." + locationworld + "_text").show(0);

		ui.draggable.addClass('correct');
		//ui.draggable.draggable('disable');
		//ui.draggable.clone(true).css('position', 'inherit').appendTo($(this));
	    ui.draggable.remove();
		$(this).droppable('disable');
		
	}
if(correct == 6)
	{
		ole.footerNotificationHandler.pageEndSetNotification();
	}
}


	$(".continue_exercise").click(function(){
		$(".vinci_text").hide(0);
		$(".florence_text").hide(0);
		$(".milan_text").hide(0);
		$(".pisa_text").hide(0);
		$(".rome_text").hide(0);
		$(".venice_text").hide(0);
		$(this).hide(0);
		$(".data_xml").show(0);
		$(".drag_text").show(0);
	});

});
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.mat18,
		answers: [
		{ans : data.string.ma49},
		{ans : data.string.ma50},
		{ans : data.string.ma51,correct : "correct"},
		]	
	},
	{
		question : data.string.mat19,
		answers: [
		{ans : data.string.ma52,correct : "correct"},
		{ans : data.string.ma53},
		{ans : data.string.ma54},
		]
	},
	{
		question : data.string.mat20,
		answers: [
		{ans : data.string.ma55},
		{ans : data.string.ma56,correct : "correct"},
		{ans : data.string.ma57},
		]
		
	},
	{
		question : data.string.mat21,
		answers: [
		{ans : data.string.ma58},
		{ans : data.string.ma59},
		{ans : data.string.ma60,correct : "correct"},
		]
	},
	{
		question : data.string.mat22,
		answers: [
		{ans : data.string.ma61,correct : "correct"},
		{ans : data.string.ma62},
		{ans : data.string.ma63},
		]
	},

	{
		question : data.string.mat23,
		answers: [
		{ans : data.string.ma64},
		{ans : data.string.ma65},
		{ans : data.string.ma66,correct : "correct"},
		]
	},

	{
		question : data.string.mat24,
		answers: [
		{ans : data.string.ma67,correct : "correct"},
		{ans : data.string.ma68},
		{ans : data.string.ma69},
		]
	},

	{
		question : data.string.mat25,
		answers: [
		{ans : data.string.ma70},
		{ans : data.string.ma71,correct : "correct"},
		{ans : data.string.ma72},
		]
	},

	{
		question : data.string.mat25,
		answers: [
		{ans : data.string.ma73},
		{ans : data.string.ma74},
		{ans : data.string.ma75,correct : "correct"},
		]
	}

	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;




	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<8){
			qA();
		}
		else if (questionCount==8){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);
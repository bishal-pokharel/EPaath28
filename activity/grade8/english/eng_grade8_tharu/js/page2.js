imageAsset = $ref+"/slide_images/";
soundAsset = $ref+"/sounds/page2/";
background_image = $ref+"/images/"

var dialog0 = new buzz.sound(soundAsset+"festivaloftharus.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"01.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"02.ogg");
var dialog1part3 = new buzz.sound(soundAsset+"03.ogg");
var dialog1part4 = new buzz.sound(soundAsset+"04.ogg");
var dialog1part5 = new buzz.sound(soundAsset+"05.ogg");

var dialog1 = [dialog1part1, dialog1part2, dialog1part3, dialog1part4, dialog1part5];

var dialog2 = new buzz.sound(soundAsset+"1.ogg");

var dialog3part1 = new buzz.sound(soundAsset+"2.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"3.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"4.ogg");
var dialog3 = [ dialog3part1,dialog3part2,dialog3part3];

var dialog4part1 = new buzz.sound(soundAsset+"5.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"6.ogg");
var dilog4 = [ dialog4part1, dialog4part2];

var dialog4part1 = new buzz.sound(soundAsset+"5.ogg");
var dialog4part2 = new buzz.sound(soundAsset+"6.ogg");
var dialog4part3 = new buzz.sound(soundAsset+"7.ogg");
var dialog4part4 = new buzz.sound(soundAsset+"8.ogg");
var dialog4 = [dialog4part1, dialog4part2, dialog4part3, dialog4part4];

var dialog5part1 = new buzz.sound(soundAsset+"9.ogg");
var dialog5part2 = new buzz.sound(soundAsset+"10.ogg");
var dialog5part3 = new buzz.sound(soundAsset+"11.ogg");
var dialog5part4 = new buzz.sound(soundAsset+"12.ogg");
var dialog5 = [dialog5part1, dialog5part2, dialog5part3,dialog5part4];

var dialog6part1 = new buzz.sound(soundAsset+"13.ogg");
var dialog6part2 = new buzz.sound(soundAsset+"14.ogg");
var dialog6part3 = new buzz.sound(soundAsset+"15.ogg");
var dialog6 = [dialog6part1,dialog6part2,dialog6part3];

var dialog7part1 = new buzz.sound(soundAsset+"16.ogg");
var dialog7part2 = new buzz.sound(soundAsset+"17.ogg");
var dialog7part3 = new buzz.sound(soundAsset+"18.ogg");
var dialog7 = [dialog7part1,dialog7part2,dialog7part3];

var dialog8part1 = new buzz.sound(soundAsset+"19.ogg");
var dialog8part2 = new buzz.sound(soundAsset+"20.ogg");
var dialog8part3 = new buzz.sound(soundAsset+"21.ogg");
var dialog8part4 = new buzz.sound(soundAsset+"22.ogg");
var dialog8 = [dialog8part1,dialog8part2,dialog8part3,dialog8part4];



//This will have all the sound 
var soundcontent = [dialog0,dialog1, dialog2, dialog3, dialog4,
					dialog5,dialog6,dialog7,dialog8]; 


var content=[
    {
        bgImgSrc : imageAsset+"festival-of-the-tharus.png",//image
        imageid : "bg_image1",
        dialog_id : "dialog_0",
        forwhichdialog : "dialog0", //audio
        lineCountDialog : [data.string.tharu0],
        speakerImgSrc : $ref+"/images/speaker.png",
    },
	{
		bgImgSrc : background_image+"tharu_map.jpg",//image
		imageid : "bg_image1",
		dialog_id : "dialog_1",
		forwhichdialog : "dialog1", //audio
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			}
		],
		lineCountDialog : [data.string.tharu1],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc : background_image+"terai.jpg",//image
		imageid : "bg_image2",
		dialog_id : "dialog_2",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.tharu2,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,

	},
	
	{
		bgImgSrc :  background_image+"bg.jpg",//image
		imageid : "bg_image3",
		dialog_id : "dialog_3",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : ""
			},
			
		],
		lineCountDialog : [data.string.tharu4,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"house.png",//image
			imageid : "bg_image4",
		dialog_id : "dialog_4",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
			
		],
		lineCountDialog : [data.string.tharu3,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
	{
		bgImgSrc :  background_image+"bells.jpg",//image
			imageid : "bg_image5",
		dialog_id : "dialog_5",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.tharu5,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc :  background_image+"fes.jpg",//image
			imageid : "bg_image6",
		dialog_id : "dialog_6",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.tharu6,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc :  background_image+"krishna1.jpg",//image
			imageid : "bg_image7",
		dialog_id : "dialog_7",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.tharu7,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc :  background_image+"je.jpg",//image
			imageid : "bg_image8",
		dialog_id : "dialog_8",
		forwhichdialog : "dialog8",
		talkHeadImages : [
			
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "nidhi1",
				highlightFlag : "highlight"//hightlight characters according to them speaking
			},
		],
		lineCountDialog : [data.string.tharu8,],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},
	
];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 9;
	loadTimelineProgress($total_page,countNext+1);
    countNext==0?$("#map_marker").hide(0):"";
	$("#map_marker").click(function(){
		$("#dialog_1").fadeIn(2000);
		$(".panel").fadeIn(2000);
		$(".exit").fadeIn(2000);
	});

	$(".close_panel").click(function(){
		$("#dialog_1").fadeOut(1000);
		$(".panel").fadeOut(1000);
		$(".exit").fadeOut(1000);
	});


	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');
			
			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}
			

		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');     	
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) { 
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {	
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.lessonEndSetNotification();
				     	}
					});
					}
				});
			}
			playerbinder();
	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');				
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);
		console.log(countNext);
		if(countNext==1) {
            $("#dialog_1").fadeIn(1000);
            $(".panel").show(0);
			$(".exit").show(0);
			$("#map_marker").show(0);
		}
		if(countNext==2){
			$(".panel").hide(0);
			$(".exit").hide(0);
			$("#map_marker").hide(0);

			$("#map_marker1").show(0);
			$("#map_marker2").show(0);
			$("#map_marker3").show(0);

		}

		if(countNext==3){
			$("#map_marker1").hide(0);
			$("#map_marker2").hide(0);
			$("#map_marker3").hide(0);
			$(".rj1").show(0);
			$(".rj2").show(0);

		}

		if(countNext==4){
			
			$(".rj1").hide(0);
			$(".rj2").hide(0);

		}
	});

$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);	
		ole.footerNotificationHandler.hideNotification();	
		loadTimelineProgress($total_page,countNext+1);
	});

})(jQuery);




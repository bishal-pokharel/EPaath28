imageAsset = $ref+"/images/";
soundAsset = $ref+"/sounds/exercise2/";

var dialog1 = new buzz.sound(soundAsset+"1.ogg");
var dialog1part1 = new buzz.sound(soundAsset+"2.ogg");
var dialog1part2 = new buzz.sound(soundAsset+"3.ogg");
var dialog1main = [dialog1,dialog1part1,dialog1part2];

var dialog2 = new buzz.sound(soundAsset+"4.ogg");
var dialog2part1 = new buzz.sound(soundAsset+"5.ogg");
var dialog2part2 = new buzz.sound(soundAsset+"6.ogg");
var dialog2part3 = new buzz.sound(soundAsset+"7.ogg");
var dialog2part4 = new buzz.sound(soundAsset+"8.ogg");
var dialog2part5 = new buzz.sound(soundAsset+"9.ogg");
var dialog2part6 = new buzz.sound(soundAsset+"10.ogg");
var dialog2main = [dialog2,dialog2part1,dialog2part2, dialog2part3, dialog2part4, dialog2part5, dialog2part6];


var dialog3 = new buzz.sound(soundAsset+"11.ogg");
var dialog3part1 = new buzz.sound(soundAsset+"12.ogg");
var dialog3part2 = new buzz.sound(soundAsset+"13.ogg");
var dialog3part3 = new buzz.sound(soundAsset+"14.ogg");
var dialog3part4 = new buzz.sound(soundAsset+"15.ogg");
var dialog3main = [dialog3,dialog3part1,dialog3part2,dialog3part3,dialog3part4];


var dialog4 = new buzz.sound(soundAsset+"16.ogg");
var dialog4part4_1 = new buzz.sound(soundAsset+"17.ogg");
var dialog4part4_2 = new buzz.sound(soundAsset+"18.ogg");
var dialog4part4_3 = new buzz.sound(soundAsset+"19.ogg");
var dialog4main = [dialog4,dialog4part4_1,dialog4part4_2,dialog4part4_3];

var dialog5 = new buzz.sound(soundAsset+"blank.ogg");
var dialog5part5_1 = new buzz.sound(soundAsset+"blank.ogg");
var dialog5part5_2 = new buzz.sound(soundAsset+"blank.ogg");
var dialog5part5_3 = new buzz.sound(soundAsset+"blank.ogg");
var dialog5main = [dialog5,dialog5part5_1,dialog5part5_2,dialog5part5_3];

var dialog6 = new buzz.sound(soundAsset+"blank.ogg");


var dialog7 = new buzz.sound(soundAsset+"blank.ogg");



var soundcontent = [dialog1main,dialog2main,dialog3main,dialog4main,dialog5main,dialog6,dialog7];

var content=[
	{
		bgImgSrc : imageAsset+"krishna02.png",
		imageid : "bg_image1",
		dialog_id : "dialog_1main",

		forwhichdialog : "dialog1",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [
						{
							span_id:"span1",
							span_data: data.string.p1dialog1,
						},
						{
							span_id:"span2",
							span_data: data.string.p1dialog1part1,
						},
						{
							span_id:"span3",
							span_data: data.string.p1dialog1part2,
						}
					],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"pandav.png",
		imageid : "bg_image2",
		dialog_id : "dialog_2main",
		span_id : "span2",
		forwhichdialog : "dialog2",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p2dialog2,
						},
						{
							span_id:"span2",
							span_data: data.string.p2dialog2part1,
						},
						{
							span_id:"span3",
							span_data: data.string.p2dialog2part2,
						},
						{
							span_id:"span4",
							span_data: data.string.p2dialog2part3,
						},
						{
							span_id:"span5",
							span_data: data.string.p2dialog2part4,
						},
						{
							span_id:"span6",
							span_data: data.string.p2dialog2part5,
						},
						{
							span_id:"span6",
							span_data: data.string.p2dialog2part6,
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"women.png",
		imageid : "bg_image3",
		dialog_id : "dialog_3main",
		span_id : "span3",
		forwhichdialog : "dialog3",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p3dialog3,
						},
						{
							span_id:"span2",
							span_data: data.string.p3dialog3part1,
						},
						{
							span_id:"span3",
							span_data: data.string.p3dialog3part2,
						},
						{
							span_id:"span4",
							span_data: data.string.p3dialog3part3,
						},
						{
							span_id:"span5",
							span_data: data.string.p3dialog3part4,
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


	{
		bgImgSrc : imageAsset+"food.jpg",
		imageid : "bg_image4",
		dialog_id : "dialog_4main",
		span_id : "span4",
		forwhichdialog : "dialog4",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p4dialog4,
						},
						{
							span_id:"span2",
							span_data: data.string.p4dialog4part4_1,
						},
						{
							span_id:"span3",
							span_data: data.string.p4dialog4part4_2,
						},
						{
							span_id:"span4",
							span_data: data.string.p4dialog4part4_3,
						},
						{
							span_id:"span5",
							span_data: data.string.p4dialog4part4_4,
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},

	{
		bgImgSrc : imageAsset+"group.jpg",
		imageid : "bg_image5",
		dialog_id : "dialog_5main",
		span_id : "span5",
		forwhichdialog : "dialog5",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p5dialog5,
						},
						{
							span_id:"span2",
							span_data: data.string.p5dialog5part5_1,
						},
						{
							span_id:"span3",
							span_data: data.string.p5dialog5part5_2,
						},
						{
							span_id:"span4",
							span_data: data.string.p5dialog5part5_3,
						}],
		speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


	{
		bgImgSrc : imageAsset+"painting.jpg",
		imageid : "bg_image6",
		dialog_id : "dialog6",
		span_id : "span6",
		forwhichdialog : "dialog6",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p6dialog6,
						}],
		//speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},


	{
		bgImgSrc : imageAsset+"children.jpg",
		imageid : "bg_image7",
		dialog_id : "dialog7",
		span_id : "span7",
		forwhichdialog : "dialog7",
		talkHeadImages : [
			{
				talkHeadImgSrc :  imageAsset+"3.png",
				nameofwhosehead : "bhuwan",
				highlightFlag : "highlight"
			},
		],
		lineCountDialog : [{
							span_id:"span1",
							span_data: data.string.p7dialog7,
						}],
		//speakerImgSrc : $ref+"/images/speaker.png",
		listenAgainText : data.string.listenAgainData,
	},



];

// array that stores array of current audio to be played
var playThisDialog;

$(function($) {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = 7;
	loadTimelineProgress($total_page,countNext+1);


	function slide1(){
		var source = $("#slide1-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


		/*The following code is all manual process on clicks--> Not a perfect solution thought, but
		a better solution for english*/

		/*Rashik Maharjan*/
		/*This is national park section*/

		var test =  $("#natmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);
				$("#natmain").removeClass("opacity_image");

		});

		test.on("click", function(){
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/chitwan.jpg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagarmatha.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/makalu.jpg')";
			var bg4 = "url('activity/grade8/english/eng_grade8_lesson8/images/langtang.jpg')";
			var bg5 = "url('activity/grade8/english/eng_grade8_lesson8/images/rara.jpg')";
			var bg6 = "url('activity/grade8/english/eng_grade8_lesson8/images/khaptad.jpg')";
			var bg7 = "url('activity/grade8/english/eng_grade8_lesson8/images/pho.jpg')";
			var bg8 = "url('activity/grade8/english/eng_grade8_lesson8/images/bardiya.jpg')";
			var bg9 = "url('activity/grade8/english/eng_grade8_lesson8/images/shiva.jpg')";
			var bg10 = "url('activity/grade8/english/eng_grade8_lesson8/images/banke.jpg')";

			if (id == "span2_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				$("#imageblock").addClass("imageblock_chitwan");
				$("#natmain").addClass("opacity_image");

			}

			if (id == "span3_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span4_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span5_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg4 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span6_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg5 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span7_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg6 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span8_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg7 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span9_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg8 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span10_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg9 ;
				$("#natmain").addClass("opacity_image");
			}

			if (id == "span11_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg10 ;
				$("#natmain").addClass("opacity_image");
			}
		});

		/*This is wild life  park section*/

		var con =  $("#cnmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);

		});
		con.on("click", function(){
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/anna.jpg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/kan.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/man.jpg')";
			var bg4 = "url('activity/grade8/english/eng_grade8_lesson8/images/bla.jpg')";
			var bg5 = "url('activity/grade8/english/eng_grade8_lesson8/images/ami.jpg')";
			var bg6 = "url('activity/grade8/english/eng_grade8_lesson8/images/gauri.jpg')";


			if (id == "cn2_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				$("#imageblock").addClass("imageblock_chitwan");


			}

			if (id == "cn3_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;

			}

			if (id == "cn4_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;

			}
				if (id == "cn5_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg4 ;

			}

			if (id == "cn6_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg5 ;

			}

			if (id == "cn7_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg6 ;

			}



		});

		/*This is conservation section*/
		var wildlife =  $("#wildmain").find("span");
		$(".close_imageblock").click(function(){
				$("#imageblock").fadeOut(500);

		});
		wildlife.on("click", function(){
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/parsa.jpg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/koshi.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/sukla.jpg')";

			if (id == "wd2_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;
				$("#imageblock").addClass("imageblock_chitwan");


			}

			if (id == "wd3_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;

			}

			if (id == "wd4_nat"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;

			}

		});

		/*World heritage*/
			var world =  $("#whmain").find("span");
				$(".close_imageblock").click(function(){
						$("#imageblock").fadeOut(500);

				});
		world.on("click", function(){
			var id = $(this).attr("id");
			var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/hun.jpeg')";
			var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/pt.jpg')";
			var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/bt.jpg')";
			var bg4 = "url('activity/grade8/english/eng_grade8_lesson8/images/pa.jpg')";
			var bg5 = "url('activity/grade8/english/eng_grade8_lesson8/images/lu.jpg')";
			var bg6 = "url('activity/grade8/english/eng_grade8_lesson8/images/sw.jpg')";
			var bg7 = "url('activity/grade8/english/eng_grade8_lesson8/images/bd.jpg')";
			var bg8 = "url('activity/grade8/english/eng_grade8_lesson8/images/cna.jpg')";
			var bg9 = "url('activity/grade8/english/eng_grade8_lesson8/images/chi.jpg')";
			var bg10 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";

			if (id == "wh1"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg1 ;



			}

			if (id == "wh2"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg2 ;

			}

			if (id == "wh3"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg3 ;
			}

			if (id == "wh4"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg4 ;
			}


			if (id == "wh5"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg5 ;
			}


			if (id == "wh6"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg6 ;
			}


			if (id == "wh7"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg7 ;
			}


			if (id == "wh8"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg8 ;
			}


			if (id == "wh9"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg9 ;
			}


			if (id == "wh10"){
				$("#imageblock").fadeIn(1000);
				document.getElementById("imageblock").style.backgroundImage = bg10 ;
			}

		});


		// 	var ecotouri =  $("#ecotou").find("span");
		// 		$(".close_imageblock").click(function(){
		// 				$("#imageblock").fadeOut(500);

		// 		});
		// ecotouri.on("click", function(){
		// 	var id = $(this).attr("id");
		// 	var bg1 = "url('activity/grade8/english/eng_grade8_lesson8/images/hun.jpeg')";
		// 	var bg2 = "url('activity/grade8/english/eng_grade8_lesson8/images/pt.jpg')";
		// 	var bg3 = "url('activity/grade8/english/eng_grade8_lesson8/images/bt.jpg')";
		// 	var bg4 = "url('activity/grade8/english/eng_grade8_lesson8/images/pa.jpg')";
		// 	var bg5 = "url('activity/grade8/english/eng_grade8_lesson8/images/lu.jpg')";
		// 	var bg6 = "url('activity/grade8/english/eng_grade8_lesson8/images/sw.jpg')";
		// 	var bg7 = "url('activity/grade8/english/eng_grade8_lesson8/images/bd.jpg')";
		// 	var bg8 = "url('activity/grade8/english/eng_grade8_lesson8/images/cna.jpg')";
		// 	var bg9 = "url('activity/grade8/english/eng_grade8_lesson8/images/chi.jpg')";
		// 	var bg10 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg11 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg12 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg13 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg14 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg15 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg16 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg17 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";
		// 	var bg18 = "url('activity/grade8/english/eng_grade8_lesson8/images/sagar.jpg')";

		// 	if (id == "ec1_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg1 ;



		// 	}

		// 	if (id == "ec2_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg2 ;

		// 	}

		// 	if (id == "ec3_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg3 ;
		// 	}

		// 	if (id == "ec4_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg4 ;
		// 	}


		// 	if (id == "ec5_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg5 ;
		// 	}


		// 	if (id == "ec6_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg6 ;
		// 	}


		// 	if (id == "ec7_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg7 ;
		// 	}


		// 	if (id == "ec8_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg8 ;
		// 	}


		// 	if (id == "ec9_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg9 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}


		// 	if (id == "ec10_slide"){
		// 		$("#imageblock").fadeIn(1000);
		// 		document.getElementById("imageblock").style.backgroundImage = bg10 ;
		// 	}

		// });

		/*End Rashik Maharjan*/


		var $slide = $board.children('div');
		var $dialogcontainer = $slide.children('div.dialogcontainer');
		var $listenAgainButton = $dialogcontainer.children('p').children('img.listenAgainButton');
		var $paralines = $dialogcontainer.children('p').children('span');
		var $paralinestohideonListenAgain = $dialogcontainer.children('p').children('span:nth-of-type(n+2)');

			if($.isArray(soundcontent[countNext])){
				playThisDialog = soundcontent[countNext];
				playThisDialog[0].play();
			}
			else if(!$.isArray(soundcontent[countNext])){
				playThisDialog = [soundcontent[countNext]];
				playThisDialog[0].play();
			}


		$listenAgainButton.on('click',  function() {
			/* Act on the event */
			$paralinestohideonListenAgain.css('display', 'none');
			playThisDialog[0].play();
			$nextBtn.hide(0);
	     	$prevBtn.hide(0);
			$listenAgainButton.removeClass('enableListenAgain').addClass('disableListenAgain');
		});

		function playerbinder(){
				$.each(playThisDialog, function( index, entry ) {
					if(index < playThisDialog.length-1){
					 	entry.bind('ended', function(){
					 		// alert(index +"sound ended");
					 		$paralines.eq(index+1).fadeIn(400);
					 		playThisDialog[index+1].play();
					 	});
					}

					if(index == playThisDialog.length-1){
					 	entry.bind("ended", function() {
						$listenAgainButton.removeClass('disableListenAgain').addClass('enableListenAgain');

				     	if(countNext > 0 && countNext < $total_page-1){
				     		$nextBtn.show(0);
				     		$prevBtn.show(0);
				     	}

				     	else if(countNext < 1){
				     		$nextBtn.show(0);
				     	}

				     	else if(countNext >= $total_page-1){
				     		$prevBtn.show(0);
				     		ole.footerNotificationHandler.pageEndSetNotification();
				     	}
					});
					}
				});
			}

			playerbinder();

	}

	slide1();

$nextBtn.on('click',function () {
		$(this).css("display","none");
		$prevBtn.css('display', 'none');
		slide1(++countNext);
		loadTimelineProgress($total_page,countNext+1);

		if(countNext==3){
			$(".text1").show(0);
			$(".text2").show(0);
			$(".text3").show(0);

			}

			if(countNext==4){
			$(".text1").hide(0);
			$(".text2").hide(0);
			$(".text3").hide(0);

			}
	});



$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		$nextBtn.css('display', 'none');
		slide1(--countNext);
		ole.footerNotificationHandler.hideNotification();
		loadTimelineProgress($total_page,countNext+1);
	});

});

$('.title').text(data.string.exerciseTitle);
$(document).ready(
		function() {
			$('.title').text(data.string.exerciseTitle2);
			var content = [ {
				words : data.string.word1,
				description : data.string.des1,
				img : $ref + "/exercise_image/community.jpg",
			},//
			{
				words : data.string.word2,
				description : data.string.des2,
				img : $ref + "/exercise_image/diversity.jpg",
			},//

			{
				words : data.string.word3,
				description : data.string.des3,
				img : $ref + "/exercise_image/traditional.jpg",
			},//

			{
				words : data.string.word4,
				description : data.string.des4,
				img : $ref + "/exercise_image/territory.jpg",
			},//
			{
				words : data.string.word5,
				description : data.string.des5,
				img : $ref + "/exercise_image/inhabit.jpg",
			},//
			{
				words : data.string.word6,
				description : data.string.des6,
				img : $ref + "/exercise_image/influence.jpg",
			},//
			{
				words : data.string.word7,
				description : data.string.des7,
				img : $ref + "/exercise_image/legend.jpg",
			},//
			{
				words : data.string.word8,
				description : data.string.des8,
				img : $ref + "/exercise_image/associate.jpg",
			},//
			{
				words : data.string.word9,
				description : data.string.des9,
				img : $ref + "/exercise_image/descendants.jpg",
			},//
			{
				words : data.string.word10,
				description : data.string.des10,
				img : $ref + "/exercise_image/warfare.jpg",
			},//
			{
				words : data.string.word11,
				description : data.string.des11,
				img : $ref + "/exercise_image/invasion.jpg",
			},//
			{
				words : data.string.word12,
				description : data.string.des12,
				img : $ref + "/exercise_image/thatch.jpg",
			},//
			{
				words : data.string.word13,
				description : data.string.des13,
				img : $ref + "/exercise_image/lattice-work.jpg",
			},//
			{
				words : data.string.word14,
				description : data.string.des14,
				img : $ref + "/exercise_image/tribe.jpg",
			},//
			{
				words : data.string.word15,
				description : data.string.des15,
				img : $ref + "/exercise_image/tribal.jpg",
			},//
			{
				words : data.string.word16,
				description : data.string.des16,
				img : $ref + "/exercise_image/pantheon.jpg",
			},//
			{
				words : data.string.word17,
				description : data.string.des17,
				img : $ref + "/exercise_image/deities.jpg",
			},
			{
				words : data.string.word18,
				description : data.string.des18,
				img : $ref + "/exercise_image/descendants.jpg",
			},
			{
				words : data.string.word19,
				description : data.string.des19,
				img : $ref + "/exercise_image/ancestral.jpg",
			},
			{
				words : data.string.word20,
				description : data.string.des20,
				img : $ref + "/exercise_image/fervour.jpg",
			},
			{
				words : data.string.word21,
				description : data.string.des21,
				img : $ref + "/exercise_image/handicraft.jpg",
			},
			{
				words : data.string.word22,
				description : data.string.des22,
				img : $ref + "/exercise_image/embroidery.jpg",
			}
			];
			//console.log(content);
			displaycontent();
			ole.footerNotificationHandler.pageEndSetNotification();
			function displaycontent() {
				var words = $.each(content, function(key, value) {
					words = value.words;
					description = value.description;
					image = value.img;
					// console.log(description);
					appendTab = '<li>';
					appendTab += '<a href="#' + words
							+ '" data-toggle="tab"  >' + words + '</a>';
					appendTab += '</li>';
					$("#myTab").append(appendTab);

					appendcontent = '<div id="' + words
							+ '" class="tab-pane ">';
					appendcontent += '<div class="f1_container">';
					appendcontent += '<div class="shadow f1_card">';
					appendcontent += '<div class="front face">';
					appendcontent += '<p>' + words + '</p>';
					appendcontent += '</div>';
					appendcontent += '<div class="back face center">';
					appendcontent += '<img src="' + image
							+ '" class="tab_image img-responsive"/>';
					appendcontent += '<p class="textroll">';
					appendcontent += description;
					appendcontent += '</p>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					appendcontent += '</div>';
					$("#myTabContent").append(appendcontent);

				});
			}

		});
$(document).ready(function(){
	$('.f1_container').click(function() {
		$(this).toggleClass('active');
	}); 
});
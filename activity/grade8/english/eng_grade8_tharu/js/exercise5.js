(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q20,
		answers: [
		{ans : data.string.name58},
		{ans : data.string.name59,
		correct : "correct"},
		{ans : data.string.name60},
		]	
	},
	{
		question : data.string.q21,
		answers: [
		{ans : data.string.name61},
		{ans : data.string.name62,
			correct : "correct"},
		{ans : data.string.name63},
		]
	},
	{
		question : data.string.q22,
		answers: [
		{ans : data.string.name64},
		{ans : data.string.name65,
			correct : "correct"},
		{ans : data.string.name66},
		]
		
	},
	{
		question : data.string.q23,
		answers: [
		{ans : data.string.name67},
		{ans : data.string.name68},
		{ans : data.string.name69,
			correct : "correct"},
		]
	},
	{
		question : data.string.q24,
		answers: [
		{ans : data.string.name70},
		{ans : data.string.name71},
		{ans : data.string.name72,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q25,
		answers: [
		{ans : data.string.name73,
		correct : "correct"},
		{ans : data.string.name74},
		{ans : data.string.name75},
		]
	},
	
	{
		question : data.string.q26,
		answers: [
		{ans : data.string.name76},
		{ans : data.string.name77,
			correct : "correct"},
		{ans : data.string.name78},
		]
	},
	
	{
		question : data.string.q27,
		answers: [
		{ans : data.string.name79},
		{ans : data.string.name80},
		{ans : data.string.name81,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q28,
		answers: [
		{ans : data.string.name82},
		{ans : data.string.name83,
			correct : "correct"},
			
		{ans : data.string.name84},
		]
	},
	
	{
		question : data.string.q29,
		answers: [
		{ans : data.string.name85},
		{ans : data.string.name86,
			correct : "correct"},
			
		{ans : data.string.name87},
		]
	}
	];

$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;

	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		// new code 
		var element_li = $(this).text();
		console.log(element_li);

		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
			
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<10){
			qA();
		}
		else if (questionCount==10){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').hide(0);
			// $('.title').text("Congratulation on finishing your exercise").css({
					// "position": "absolute",
					// "top": "48%",
					// "transform": "translateY(-50%)"
			// });
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			//ole.footerNotificationHandler.pageEndSetNotification();
		}
	});
	$(".closebtn").on("click",function(){
		ole.activityComplete.finishingcall();
	});

})(jQuery);
	
	

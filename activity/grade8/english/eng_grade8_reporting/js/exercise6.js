(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q0001,
		answers: [
		{ans : data.string.name0001},
		{ans : data.string.name0002},
		{ans : data.string.name0003,
		correct : "correct"},
		
		]	
	},
	{
		question : data.string.q0002,
		answers: [
		{ans : data.string.name0004},
		{ans : data.string.name0005,
			correct : "correct"},
		{ans : data.string.name0006},
		
		]
	},
	{
		question : data.string.q0003,
		answers: [
		{ans : data.string.name0007},
		{ans : data.string.name0008,
		correct : "correct"},
		{ans : data.string.name0009},
		
		]
		
	},
	{
		question : data.string.q0004,
		answers: [
		{ans : data.string.name00010},
		{ans : data.string.name00011,
		correct : "correct"},
		{ans : data.string.name00012},
		
		]
	},
	{
		question : data.string.q0005,
		answers: [
		{ans : data.string.name00013},
		{ans : data.string.name00014},
		{ans : data.string.name00015,
			correct : "correct"},
		
		]
	},
	
	{
		question : data.string.q0006,
		answers: [
		{ans : data.string.name00016},
		{ans : data.string.name00017,
			correct : "correct"},
		{ans : data.string.name00018},
			
		]
	},
	
	{
		question : data.string.q0007,
		answers: [
		{ans : data.string.name00019,
			correct : "correct"},
		{ans : data.string.name00020},
		{ans : data.string.name00021},
	
		]
	},
	
	{
		question : data.string.q0008,
		answers: [
		{ans : data.string.name00022},
		{ans : data.string.name00023},
		{ans : data.string.name00024,
			correct : "correct"},
			
		]
	},
	
	
	{
		question : data.string.q0009,
		answers: [
		{ans : data.string.name00025},
		{ans : data.string.name00026,
			correct : "correct"},
		{ans : data.string.name00027},
			
		]
	}
	];


	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;
	
	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
						
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);


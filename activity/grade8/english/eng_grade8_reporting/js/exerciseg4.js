(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q00001,
		answers: [
		{ans : data.string.name00001},
		{ans : data.string.name00002,
			correct : "correct"},
		{ans : data.string.name00003},
		]	
	},
	{
		question : data.string.q00002,
		answers: [
		{ans : data.string.name00004},
		{ans : data.string.name00005,
			correct : "correct"},
		{ans : data.string.name00006},
		]
	},
	{
		question : data.string.q00003,
		answers: [
		{ans : data.string.name00007},
		{ans : data.string.name00008},
		{ans : data.string.name00009,
			correct : "correct"},
		]
		
	},
	{
		question : data.string.q00004,
		answers: [
		{ans : data.string.name000010},
		{ans : data.string.name000011,
			correct : "correct"},
		{ans : data.string.name000012},
		]
	},
	{
		question : data.string.q00005,
		answers: [
		{ans : data.string.name000013},
		{ans : data.string.name000014},
		{ans : data.string.name000015,
			correct : "correct"},
		]
	},
		{
		question : data.string.q00006,
		answers: [
		{ans : data.string.name000016,
			correct : "correct"},
		{ans : data.string.name000017},
		{ans : data.string.name000018},
		]
	},
		{
		question : data.string.q00007,
		answers: [
		{ans : data.string.name000019},
		{ans : data.string.name000020},
		{ans : data.string.name000021,
			correct : "correct"},
		]
	},
		{
		question : data.string.q00008,
		answers: [
		{ans : data.string.name000022},
		{ans : data.string.name000023},
		{ans : data.string.name000024,
			correct : "correct"},
		]
	},
		{
		question : data.string.q00009,
		answers: [
		{ans : data.string.name000025},
		{ans : data.string.name000026,
			correct : "correct"},
		{ans : data.string.name000027},
		]
	}
	]

$nextBtn.fadeOut();

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			console.log(html);
			$board.html(html);
			// console.log(html);
	}

	qA();

	$board.on('click','.neutral',function () {
		// new code 
		var element_li = $(this).text();
		console.log(element_li);
		
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
		}
		else {
			$this.addClass('wrong').removeClass('neutral');
		}
	})

	$nextBtn.on('click',function () {
		$nextBtn.fadeOut();
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	})

})(jQuery);
(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q001,
		answers: [
		{ans : data.string.name001},
		{ans : data.string.name002},
		{ans : data.string.name003,
		correct : "correct"},
		]	
	},
	{
		question : data.string.q002,
		answers: [
		{ans : data.string.name004},
		{ans : data.string.name005,correct : "correct"},
		{ans : data.string.name006},
		]
	},
	{
		question : data.string.q003,
		answers: [
		{ans : data.string.name007},
		{ans : data.string.name008},
		{ans : data.string.name009,
			correct : "correct"},
		]
		
	},
	{
		question : data.string.q004,
		answers: [
		{ans : data.string.name0010,
			correct : "correct"},
		{ans : data.string.name0011},
		{ans : data.string.name0012},
		]
	},
	{
		question : data.string.q005,
		answers: [
		{ans : data.string.name0013},
		{ans : data.string.name0014},
		{ans : data.string.name0015,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q006,
		answers: [
		{ans : data.string.name0016,
			correct : "correct"},
		{ans : data.string.name0017},
		{ans : data.string.name0018},
		]
	},
	
	{
		question : data.string.q007,
		answers: [
		{ans : data.string.name0019},
		{ans : data.string.name0020},
		{ans : data.string.name0021,
			correct : "correct"},
		]
	},
	
	{
		question : data.string.q008,
		answers: [
		{ans : data.string.name0022},
		{ans : data.string.name0023,
			correct : "correct"},
		{ans : data.string.name0024},
		]
	},
	
	{
		question : data.string.q009,
		answers: [
		{ans : data.string.name0025,
			correct : "correct"},
		{ans : data.string.name0026},
		{ans : data.string.name0027},
		]
	}
	];

	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;





	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();

			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").css({
				"position": "absolute",
				"top": "48%",
				"transform": "translateY(-50%)"
			});
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	});

})(jQuery);


//$('.title').text(data.string.exerciseTitle);
var id = 0;
$(document).ready(function() {
	var ex = [ {
		text : data.string.ex1
	}, {
		text : data.string.ex2
	}, {
		text : data.string.ex3
	}, {
		text : data.string.ex4
	}, {
		text : data.string.ex5
	}, {
		text : data.string.ex6
	}

	];
	// console.log(ex);

	displayContents();
	function displayContents() {
		var context = $.each(ex, function(key, values) {
			id++;
			text1 = values.text;
			appendContents = '<div class="question' + id + '">';
			appendContents += text1;
			appendContents += '</div>';
			console.log(appendContents);
			// console.log(appendContents);
		 	$(".contents_choose").append(appendContents);
		});
	}
});

var answered1 = false;
var answered2 = false;

$('[class^="no"]').click(function(){
	play_correct_incorrect_sound(false);
});

$(".yes2").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no2").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes2_2").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no2_2").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes3").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no3").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes3_1").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no3_1").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes4").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no4").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes4_1").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no4_1").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes5").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no5").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes5_1").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no5_1").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes6").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no6").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes6_1").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no6_1").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes7").click(function(){
	if(answered1){
		return answered1;
	}
	$(".no7").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered1 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

$(".yes7_1").click(function(){
	if(answered2){
		return answered2;
	}
	$(".no7_1").fadeOut(300);
	play_correct_incorrect_sound(true);
	answered2 = true;
	if(answered1 && answered2){
		$("#activity-page-next-btn-enabled").show(0);
	}
});

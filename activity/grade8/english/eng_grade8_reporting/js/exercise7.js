(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	var content = [
	{
		question : data.string.q00001,
		answers: [
		{ans : data.string.name00001},
		{ans : data.string.name00002,
		correct : "correct"},
		{ans : data.string.name00003},
		
		]	
	},
	{
		question : data.string.q00002,
		answers: [
		{ans : data.string.name00004},
		{ans : data.string.name00005,
			correct : "correct"},
		{ans : data.string.name00006},
		
		]
	},
	{
		question : data.string.q00003,
		answers: [
		{ans : data.string.name00007},
		{ans : data.string.name00008},
		{ans : data.string.name00009,
		correct : "correct"},
		
		]
		
	},
	{
		question : data.string.q00004,
		answers: [
		{ans : data.string.name000010},
		{ans : data.string.name000011,
		correct : "correct"},
		{ans : data.string.name000012},
		
		]
	},
	{
		question : data.string.q00005,
		answers: [
		{ans : data.string.name000013},
		{ans : data.string.name000014},
		{ans : data.string.name000015,
			correct : "correct"},
		
		]
	},
	
	{
		question : data.string.q00006,
		answers: [
		{ans : data.string.name000016,
			correct : "correct"},
		{ans : data.string.name000017},
		{ans : data.string.name000018},
			
		]
	},
	
	{
		question : data.string.q00007,
		answers: [
		{ans : data.string.name000019},
		{ans : data.string.name000020},
		{ans : data.string.name000021,
			correct : "correct"},
	
		]
	},
	
	{
		question : data.string.q00008,
		answers: [
		{ans : data.string.name000022},
		{ans : data.string.name000023},
		{ans : data.string.name000024,
			correct : "correct"},
			
		]
	},
	
	
	{
		question : data.string.q0009,
		answers: [
		{ans : data.string.name00025},
		{ans : data.string.name00026,
			correct : "correct"},
		{ans : data.string.name00027},
			
		]
	}
	];


	$nextBtn.hide(0);

	// console.log(content);
	
	var questionCount = 0;
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");

	
	console.log(content);
	function  qA() {
			var source = $('#qA-templete').html();
			var template = Handlebars.compile(source);
			var html = template(content[questionCount]);
			$board.html(html);
			answered = false;
			attemptcount = 0;
			// console.log(html);
	}
	qA();

	var answered = false;
	var attemptcount = 0;
	
	var totalq = content.length;
	var correctlyanswered = 0;



	$board.on('click','.neutral',function () {
		if(answered){
			return answered;
		}
		attemptcount++;
		var element_li = $(this).text();
		console.log(element_li);
		// console.log("what");
		var $this = $(this);
		var isCorrect = $(this).data('correct');
		if(isCorrect=== "correct") {
			if(attemptcount == 1){
				correctlyanswered++;
			}
			answered = true;
			$this.addClass('right').removeClass('neutral');
			$nextBtn.fadeIn();
						
			// new code
			var string = document.getElementsByClassName("question")[0].innerHTML;
			var replacedString = string.replace("......", element_li);
			console.log(replacedString);
			document.getElementsByClassName("question")[0].innerHTML = replacedString; 
			play_correct_incorrect_sound(true); 
		} else {
			play_correct_incorrect_sound(false);
			$this.addClass('wrong').removeClass('neutral');
		}
	});

	$nextBtn.on('click',function () {
		$nextBtn.hide(0);
		questionCount++;
		console.log("question count", questionCount);
		if(questionCount<9){
			qA();
		}
		else if (questionCount==9){
			$(".mainholder").hide(0);
			$(".imgholder").hide(0);
			$(".answers").hide(0);
			$(".result").hide(0);
			$(".question").hide(0);
			$(".title").hide(0);
			//$('.title').html("Congratulations on finishing your exercise <br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.");
			$(".finishtxt").append("<br> You have correctly answered "+correctlyanswered +" out of "+ totalq+" questions.").show(0);
			$(".closebtn").click(function(){
				$(this).parent().hide(0);
				ole.activityComplete.finishingcall();
			});
		}
		
		
	});
	
	
	

})(jQuery);


var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var countNext = 0;
var $total_page = 1;
loadTimelineProgress($total_page,countNext+1);
$(document).ready(function(){ 
	

	$(".news_div").on("click", function(){ 
		$(".clickme").hide(0);
		$(".arrow1").addClass("arrowmoveanim");
		$(".schedule_div").addClass("scheduleanim");
	});

	$(".arrow1").on(animationend, function() { 
		$(".sports_div").addClass("cssfadeinanim");
		$(".political_div").addClass("cssfadeinanim2");
		$(".protests_div").addClass("cssfadeinanim3");
	});

	$(".protests_div").on(animationend, function(){ 
		$(".shoes_div").addClass("cssfadeinanim");
		$(".festival_div").addClass("cssfadeinanim2");
		$(".celeb_div").addClass("cssfadeinanim3");
	});

	$(".celeb_div").on(animationend, function() { 
		$(".arrow2").addClass("arrowmoveagainanim");
		$(".nonschedule_div").addClass("non_scheduleanim");	
	});

	$(".nonschedule_div").on(animationend, function() { 
		$(".accidents_div").addClass("cssfadeinanim");
		$(".disasters_div").addClass("cssfadeinanim2");
		$(".deaths_div").addClass("cssfadeinanim3");
		ole.footerNotificationHandler.pageEndSetNotification();
	});

});
/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	var characterDialouges1 = [ {
		diaouges : data.string.p1_1
	},
	
	];

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn");
		countNext = 0,
		all_page=19;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5,11,12]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "first",
		animate : "true",
		text : [
		data.string.p1_13,
		data.string.p1_14,
		data.string.p1_15,
		data.string.p1_16,
		data.string.p1_17,
	
	]
	},
	
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "secondtext",
		
			text : [
			
			data.string.p1_18,
			data.string.p1_19,
			data.string.p1_20,
			data.string.p1_21,
			data.string.p1_22,
			data.string.p1_23,
			data.string.p1_24,
			data.string.p1_24_1,
			data.string.p1_24_2,
			data.string.p1_24_3,
			]
	
	},
	
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "thirdtext",
		
			text : [
			
				data.string.p1_25,
				data.string.p1_26,
				data.string.p1_27,
				data.string.p1_28,
				data.string.p1_29,
				data.string.p1_30,
				data.string.p1_31,
				data.string.p1_32,
				data.string.p1_33,
				data.string.p1_34,
				//data.string.p1_35,
				//data.string.p1_36,
				//data.string.p1_37,
				//data.string.p1_38,
		
			
				],
	},
	
	
	
	{
		justClass : "fourthtext",
		
			text : [
			
			data.string.p1_39,
			data.string.p1_40,
			data.string.p1_41,
			data.string.p1_42,
			data.string.p1_43,
			data.string.p1_44,
			data.string.p1_45,
			data.string.p1_46,
			],
	}
	
	
	
	
	];
	
	
	
	
/******************************************************************************************************/
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
				
			
				
		}
	function first01 () {
		
		$nextBtn.show(0);
		$board.find(".wrapperIntro.first .text1").show(0);
		
	}
	function first02 () {
		$nextBtn.show(0);
		
		$board.find(".wrapperIntro.first .text2").show(0);
		
	}
	function first03 () {
		$board.find(".wrapperIntro.first ,.wrapperIntro.first ").show(0);
		$board.find(".wrapperIntro.first .text3").show(0);
		$board.find(".wrapperIntro.first .bubble1").show(0);
		$board.find(".wrapperIntro.first .char-holder .char-a").show(0);
		
		$nextBtn.show(0);		
		$board.find(".wrapperIntro.first  .char-holder .char-a").addClass("slideRight");
		
	}
		
	
	function first04 () {
		$board.find(".wrapperIntro.first .text3").hide(0);
		$board.find(".wrapperIntro.first .text4").show(0);
		
		
		
		$board.find(".wrapperIntro.first .text4").show(0);
		$nextBtn.show(0);
	}
	
		
	
		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			
		};

	

		function second01 () {
			
			$board.find(".wrapperIntro.secondtext .text1").show(0);
			$board.find(".wrapperIntro.secondtext .firstimage").show(0);
					$nextBtn.show(0);
		}

		function second02 () {
			$board.find(".wrapperIntro.secondtext .text2").show(0);
			// $(".nextBtn.myNextStyle").hide(0).delay(3000).show(0);
					$nextBtn.hide(0).delay(3000).show(0);
			
		}

		function second03 () {
			$board.find(".wrapperIntro.secondtext .text3").show(0);
			$board.find(".wrapperIntro.secondtext .secondimage").show(0);
					$nextBtn.show(0);
		}
		
		function second04 () {
			$board.find(".wrapperIntro.secondtext .text4").show(0);
					$nextBtn.show(0);
			
		}
		
		
		
		function second06 () {
			$board.find(".wrapperIntro.secondtext .text6").show(0);
					$nextBtn.hide(0).delay(3000).show(0);
			// $(".nextBtn.myNextStyle").hide(0).delay(3000).show(0);
			
		}
		
	
		
		
		
		
		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$nextBtn.hide(0);
			
			$board.find(".wrapperIntro.thirdtext .text0").delay(500).fadeIn();
			$(".tri").delay(1000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text1").delay(1500).fadeIn();
			$(".rec").delay(2000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text2").delay(2500).fadeIn();
			$(".squ").delay(3000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text3").delay(3500).fadeIn();
			$(".paral").delay(4000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text4").delay(4500).fadeIn();
			$(".rho").delay(5000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text5").delay(5500).fadeIn();
			$(".kit").delay(6000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text6").delay(6500).fadeIn();
			$(".trap").delay(7000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text7").delay(7500).fadeIn();
			$(".qua").delay(8000).fadeIn();
			$board.find(".wrapperIntro.thirdtext .text8").delay(8500).fadeIn();
			$nextBtn.delay(9000).show(0);
		};
	
		
		function third01 () {
			$board.find(".wrapperIntro.thirdtext .text0").hide(0);
			$board.find(".wrapperIntro.thirdtext .text1").hide(0);
			$board.find(".wrapperIntro.thirdtext .text2").hide(0);
			$board.find(".wrapperIntro.thirdtext .text3").hide(0);
			$board.find(".wrapperIntro.thirdtext .text4").hide(0);
			$board.find(".wrapperIntro.thirdtext .text5").hide(0);
			$board.find(".wrapperIntro.thirdtext .text6").hide(0);
			$board.find(".wrapperIntro.thirdtext .text7").hide(0);
			$board.find(".wrapperIntro.thirdtext .text8").hide(0);
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".tri" ).animate({ "width": "-=10%" }, {
					duration: 2000, 
				});
				
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".rec" ).animate({ "width": "-=10%", "left": "-=12%"}, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".squ" ).animate({ "width": "-=10%", "left": "-=24%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".paral" ).animate({ "width": "-=10%", "left": "-=35%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".rho" ).animate({ "width": "-=10%", "left": "+=46%", "margin-top": "-=22%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".kit" ).animate({ "width": "-=10%", "left": "+=33%", "margin-top": "-=22%"}, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".trap" ).animate({ "width": "-=10%", "left": "+=20%", "margin-top": "-=22%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.thirdtext").find(function() {
				$( ".qua" ).animate({ "width": "-=10%", "left": "+=10%", "margin-top": "-=22%" }, {
					duration: 2000, 
				});	
				$(".back").show(0);
			});
					$nextBtn.show(0);
		}

		
		
		/*function third02 () {
			
			
		}

		function third03 () {
			$board.find(".wrapperIntro.thirdtext .text3").show(0);
			
		}
		
		function third04 () {
			$board.find(".wrapperIntro.thirdtext .text4").show(0);
			
		}
		
		function third05 () {
			$board.find(".wrapperIntro.thirdtext .text5").show(0);
			
		}
		
		function third06 () {
			$board.find(".wrapperIntro.thirdtext .text6").show(0);
			
		}
		function third07 () {
			$board.find(".wrapperIntro.thirdtext .text7").show(0);
			
		}
		function third08 () {
			$board.find(".wrapperIntro.thirdtext .text8").show(0);
			
		}*/
		
		
		
		
		function third09 () {
			$board.find(".wrapperIntro.thirdtext .text9").show(0);
			timer1();
			$board.find(".wrapperIntro.thirdtext >span").show(0);
			$nextBtn.hide(0).delay(5000).show(0);
			
			
		}
		
		function timer1(){
			var timer = setInterval(function(){
				$("#count_num").html(function(i,html){
					   
					if(parseInt(html)>0)
					   {
					   return parseInt(html)-1;
					   }
					   else
					   {
					   clearTimeout(timer);
					       return "LETS BEGIN";
					   }
					 });

					},1000);
							$nextBtn.show(0);
		}
		
		
		/*
		function third10 () {
			$board.find(".wrapperIntro.thirdtext .text10").show(0);
			
		}
		function third11 () {
			$board.find(".wrapperIntro.thirdtext .text11").show(0);
			
		}
		function third12 () {
			$board.find(".wrapperIntro.thirdtext .text12").show(0);
			
		}
		function third13 () {
			$board.find(".wrapperIntro.thirdtext .text13").show(0);
			
		}*/
		
		
		function fourth() {

			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			
			$(".wrapperIntro.fourthtext .text0").show(0);
			$(".wrapperIntro.fourthtext .tri").show(0);
			$(".wrapperIntro.fourthtext .rec").show(0);
			$(".wrapperIntro.fourthtext .squ").show(0);
			$(".wrapperIntro.fourthtext .paral").show(0);
			$(".wrapperIntro.fourthtext .rho").show(0);
			$(".wrapperIntro.fourthtext .kit").show(0);
			$(".wrapperIntro.fourthtext .trap").show(0);
			$(".wrapperIntro.fourthtext .qua").show(0);
			$board.find(".wrapperIntro.fourthtext .text1").show(0);
			$board.find(".wrapperIntro.fourthtext .firstimage").show(0);
			$(".wrapperIntro.fourthtext .tri").addClass("bg");
					$nextBtn.show(0);
			
		};
	
		/*function fourth01 () {
			
			$board.find(".wrapperIntro.fourthtext .text1").show(0);
			$board.find(".wrapperIntro.fourthtext .firstimage").show(0);
		}*/

		function fourth02 () {
			$board.find(".wrapperIntro.fourthtext .text2").show(0);
					$nextBtn.show(0);
			
		}

		function fourth03 () {
			$board.find(".wrapperIntro.fourthtext .text3").show(0);
					$nextBtn.show(0);
		}
		
		function fourth04 () {
			$(".bg_page4").show(0);
			$(".perimeter").hide(0);
			$board.find(".wrapperIntro.fourthtext .text0").hide(0);
			$board.find(".wrapperIntro.fourthtext .text1").hide(0);
			$board.find(".wrapperIntro.fourthtext .text2").hide(0);
			$board.find(".wrapperIntro.fourthtext .text3").hide(0);
			$board.find(".wrapperIntro.fourthtext .firstimage").hide(0);
			
			$board.find(".wrapperIntro.fourthtext .char-a").show(0);
			$board.find(".wrapperIntro.fourthtext .char-b").show(0);
			//$board.find(".wrapperIntro.fourthtext .char-b").show(0);
			$board.find(".wrapperIntro.fourthtext .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.fourthtext .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.fourthtext .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.fourthtext .boy-listening01").show(0);
			//$board.find(".wrapperIntro.fourthtext ,.wrapperIntro.fourthtext").show(0);
			$(".perimeter").hide(0);
			$board.find(".wrapperIntro.fourthtext .text4").show(0);
			$board.find(".wrapperIntro.fourthtext .bubble1").show(0);
			$board.find(".wrapperIntro.fourthtext .boy-talking02").show(0);
			$board.find(".wrapperIntro.fourthtext .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.fourthtext .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.fourthtext .boy-listening01").hide(0);
					$nextBtn.show(0);
		}
		
		

		function fourth05 () {
			//$(".nextBtn.myNextStyle").hide(0);
			
			$board.find(".wrapperIntro.fourthtext .text4").hide(0);
			$board.find(".wrapperIntro.fourthtext .bubble1").hide(0);
			$board.find(".wrapperIntro.fourthtext .bubble2").show(0);
			$board.find(".wrapperIntro.fourthtext .text5").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			$board.find(".wrapperIntro.fourthtext .boy-talking02").hide(0);
			$board.find(".wrapperIntro.fourthtext .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.fourthtext .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.fourthtext .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.fourthtext .boy-listening01").show(0);
			$board.find(".wrapperIntro.fourthtext .squirrel-listening03").hide(0);
					$nextBtn.show(0);
		}

		function fourth06 () {
			$board.find(".wrapperIntro.fourthtext .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.fourthtext .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.fourthtext .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.fourthtext .squirrel-talking02").css('border-color','#FEA92A');
			
			$board.find(".wrapperIntro.fourthtext .text5").hide(0);
			//$board.find(".wrapperIntro.fourthtext .bubble2").hide(0);
			//$board.find(".wrapperIntro.fourthtext .bubble1").show(0);
			$board.find(".wrapperIntro.fourthtext .text6").show(0);
					$nextBtn.show(0);
			
		}
		
		function fourth07 () {
			$board.find(".wrapperIntro.fourthtext .text6").hide(0);
			$board.find(".wrapperIntro.fourthtext .bubble2").hide(0);
			$board.find(".wrapperIntro.fourthtext .bubble1").show(0);
			$board.find(".wrapperIntro.fourthtext .text7").show(0);
			
			
			$board.find(".wrapperIntro.fourthtext .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.fourthtext .boy-talking02").show(0);
			$board.find(".wrapperIntro.fourthtext .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.fourthtext .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.fourthtext .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.fourthtext .boy-listening01").hide(0);
					$nextBtn.show(0);
		}
		

// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			console.log(countNext);
			
			/*if(countNext == 16)
			{
				$(".bg_page4").show(0);
			}*/
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
				
			};
			
			
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
			
			
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				
				second, 
				second01,
				second02,
				second03,
				second04,
				
				second06,
				
				third, 
				third01,
				/*third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08,*/
				
				fourth, 
				
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,
				fourth07,
			];
			fnArray[countNext]();
			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

	//end of page5//

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
		$refreshBtn= $("#activity-page-refresh-btn");
		countNext = 0,
		all_page=5;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,2,5]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage_overpage",
		animate : "true",
		image:true,
		text : [data.lesson.chapter],

	},{
		justClass : "firstPage",
		mimgma_image:true,
		animate : "true",
		text : [data.string.p1_1],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p1_2,
			/*data.string.p1_3,
			data.string.p1_4,
			data.string.p1_5*/


			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [

				data.string.p1_6,
				data.string.p1_7,
				data.string.p1_8,
				data.string.p1_9,
				data.string.p1_10,
				data.string.p1_11

				],
	}



	];




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		};
			function first_sec() {

				var source = $("#intro-template").html();
				var template = Handlebars.compile(source);
				var content = dataObj[1];
				var html = template(content);
				$board.html(html);
				$nextBtn.show(0);
			};



		function second () {
			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];

			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.second").css("background","#fff");
			$board.find(".wrapperIntro.second").css("padding","1%");

			//$board.find(".wrapperIntro.second .text0").show(0);
			//$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .solution01").show(0);
			$board.find(".wrapperIntro .img-b").fadeIn(1500);
			//board.find(".wrapperIntro .img-a .img-responsive").removeClass("border");
			$board.find(".wrapperIntro .img-b .img-responsive").addClass("border");
			// $(".nextBtn.myNextStyle").hide(0).delay(2000).show(0);
			$nextBtn.hide(0).delay(2000).show(0);

		}




		/*function second02 () {
			//$board.find(".wrapperIntro.second .text0").show(0);
			//$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .solution01").show(0);
			$board.find(".wrapperIntro .img-b").fadeIn(2500);
			//board.find(".wrapperIntro .img-a .img-responsive").removeClass("border");
			$board.find(".wrapperIntro .img-b .img-responsive").addClass("border");

		}*/


		function second03 () {
			//$board.find(".wrapperIntro.second .text0").show(0);
			//$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .solution02").show(0);
			$board.find(".wrapperIntro .img-c").fadeIn(1500);
			//$board.find(".wrapperIntro .img-b .img-responsive").removeClass("border");
			$board.find(".wrapperIntro .img-c .img-responsive").addClass("border");
			// $(".nextBtn.myNextStyle").hide(0).delay(2000).show(0);
			$nextBtn.hide(0).delay(2000).show(0);
			$board.find(".wrapperIntro.img-b").addClass("filter1");

		}


		function second04 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .solution03").show(0);
			$board.find(".wrapperIntro .img-a").fadeIn(1500);
			//$board.find(".wrapperIntro .img-c .img-responsive").removeClass("border");
			$board.find(".wrapperIntro .img-a .img-responsive").addClass("border");
			// $(".nextBtn.myNextStyle").hide(0).delay(2000).show(0);
			$nextBtn.hide(0).delay(2000).show(0);

		}






/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third").css("background","#fff");
			$nextBtn.show(0);
			/*var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = {
				title : "check",

			}

			var html = template(content);
			$board.append(html);*/

		}


		function third01 () {
			$board.find(".wrapperIntro.third .text1").show(0);
			$nextBtn.show(0);

		}




/*----------------------------------------------------------------------------------------------------*/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first slide
				first_sec,
				second,//second slide

				//second02,
				second03,
				second04,



				third//third slide


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	


	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=16;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,4]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p18_1,
		data.string.p18_2,
		data.string.p18_3,
		data.string.p18_4,
		],
	},
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			        data.string.p11_1,
					data.string.p19_1,
					data.string.p19_2,
					data.string.p19_3,
					data.string.p19_4,
					data.string.p19_5,
					data.string.p19_6,
					data.string.p19_7,
					data.string.p19_8,
					data.string.p19_9,
					data.string.p19_10,
					data.string.p19_11,
					data.string.p19_12,
			        ],
	},
	/*end of second page */
	/*------------------------------------------------------------------------------------------------*/
	{
		justClass : "third",
			text : [
				],
	},
	{
		justClass : "fourth",
			text : [
			],
	}];
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".bg_page4").show(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			//$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$nextBtn.show(0);			
		};

	

		function first01 () {
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			
			
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			$nextBtn.show(0);
			
		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$nextBtn.show(0);
			
		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$(".letslook").show(0);
			
			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			/*$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);*/
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			$nextBtn.show(0);
		}
		
		
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second .text0").show(0);
			
			
			
		}
		
		
		function second01() {
			$board.find(".wrapperIntro.second .text1").show(0);
			$nextBtn.show(0);
			
		}
		
		
		function second02(){
			$board.find(".wrapperIntro.second .text2").show(0);
			$nextBtn.show(0);
		}
		
		function second03(){
			$board.find(".wrapperIntro.second .text3").show(0);
			$nextBtn.show(0);
		}
		
		function second04(){
			$board.find(".wrapperIntro.second .text4").show(0);
			$nextBtn.show(0);
		}
		
		
		function second05(){
			$board.find(".wraperIntro.second .text5").show(0);
			$nextBtn.show(0);
		}
		
		
		function second06(){
			$board.find(".wrapperIntro.second .text6").show(0);
			$nextBtn.show(0);
	
		}
		
		function second07(){
			$board.find(".wrapperIntro.second .text7").show(0);
			$nextBtn.show(0);
	
		}
		function second08(){
			$board.find(".wrapperIntro.second .text8").show(0);
			$nextBtn.show(0);
		}
		function second09(){
			$board.find(".wrapperIntro.second .text9").show(0);
			$nextBtn.show(0);
		}
		function second10(){
			$board.find(".wrapperIntro.second .text10").show(0);
			$nextBtn.show(0);
		}
		function second11(){
			$board.find(".wrapperIntro.second .text11").show(0);
			$nextBtn.show(0);
		}
		function second12(){
			$board.find(".wrapperIntro.second .text12").show(0);
			$nextBtn.show(0);
		}
	
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		fnArray = [
			first,  //first one
			first01,
			first02,
			first03,
			
			second,
			second01,
			second02,
			second03,
			second04,
			second05,
			second06,
			second07,
			second08,
			second09,
			second10,
			second11,
			second12
		];

		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	var characterDialouges1 = [ {
		diaouges : data.string.p1_1
	},
	
	];

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=6;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstpage4",
		animate : "true",
		text : [
		        
		//data.string.p2_3,       
		data.string.p5_1,
		data.string.p5_2,
		data.string.p5_3,
		data.string.p5_4,
		data.string.p5_5,
		data.string.p5_6,
		data.string.p5_7,
	]
	},
	
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "secondtext",
		
			text : [
			
	
			]
	
	},
	
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "thirdtext",
		
			text : [
			
				
			
				],
	},
	
	
	
	{
		justClass : "fourthtext",
		
			text : [
			
			
			],
	}
	
	
	
	
	];
	
	
	
	
/******************************************************************************************************/
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
			$(".perimeter").show(0);
			$(".wrapperIntro.firstpage4 .back").show(0);
			$(".wrapperIntro.firstpage4 .tri").show(0);
			$(".wrapperIntro.firstpage4 .rec").show(0);
			$(".wrapperIntro.firstpage4 .squ").show(0);
			$(".wrapperIntro.firstpage4 .paral").show(0);
			$(".wrapperIntro.firstpage4 .rho").show(0);
			$(".wrapperIntro.firstpage4 .kit").show(0);
			$(".wrapperIntro.firstpage4 .trap").show(0);
			$(".wrapperIntro.firstpage4 .qua").show(0);
			$(".wrapperIntro.firstpage4 .squ").addClass("bg");
		    $board.find(".wrapperIntro.firstpage4 .firstimage").show(0);
				
		}
	function first01 () {
		$board.find(".wrapperIntro.firstpage4 .text1").show(0);
		$nextBtn.show(0);
	}
	function first02 () {
		$board.find(".wrapperIntro.firstpage4 .text2").show(0);
		$nextBtn.show(0);
	}
	function first03 () {
		$board.find(".wrapperIntro.firstpage4 .text3").show(0);
		$nextBtn.show(0);
	}
	function first04 () {
		$board.find(".wrapperIntro.firstpage4 .text4").show(0);
		$nextBtn.show(0);
	}
	
	function first05 () {
		$board.find(".wrapperIntro.firstpage4 .text5").show(0);
		$nextBtn.show(0);
	}

	function first06 () {
		$board.find(".wrapperIntro.firstpage4 .text6").show(0);
		$nextBtn.show(0);
	}

	/*function first07 () {
		
		$board.find(".wrapperIntro.firstpage4 .text7").show(0);
		
	}*/


        // first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		fnArray = [
			first,  //first one
			first01,
			first02,
			first03,
			first04,
			first05,
			first06,
			//first07,
			
			
		];
		

		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

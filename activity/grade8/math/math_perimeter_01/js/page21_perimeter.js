$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : data.string.p17_7, ansstring : data.string.p17_4, imgs :'t2.png', ansque : data.string.p17_3, sol :data.string.p2_7, suffix: 'm'},
		{base : data.string.p17_12, ansstring : data.string.p17_9, imgs :'q1.png', ansque : data.string.p17_8, sol :data.string.p2_7, suffix: 'm'},
		{base : data.string.p17_17, ansstring : data.string.p17_14, imgs :'r2.png', ansque : data.string.p17_13, sol :data.string.p2_7, suffix: 'm'}
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p17_1,
			qTitle : data.string.p17_2,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
		}else if(countNext == 2){
			$('.q2').hide(0);
		}
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p17_5,
			ans : sub.base,
			answer : data.string.p17_6,
			text : sub.ansque,
			baseString : sub.ansstring,
			image1 : sub.imgs,
			couter : countNext+1,
			solution: sub.sol,
			suffix: sub.suffix
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
			swal("Please fill all the empty fields!");
		}else{
		
			if (base ===upDatas.base) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
			} else {
				$board.find(cls+" .check1").html(wrong);
			}
	
	
			if (check[0]===1) {
				if (countNext>=2) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);
				}
			} else {
				$board.find(cls+" .clicks .click").show(0);
			}
		}
	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});
$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : data.string.p4_5, ansstring : data.string.p4_2, imgs :'prism1.png', sol :data.string.p2_12, suffix:'cm<sub>3</sub>'},
		{base : data.string.p4_6, ansstring : data.string.p4_2, imgs :'prism2.png', sol :data.string.p2_12, suffix:'cm<sub>3</sub>'},
		{base : data.string.p4_7, ansstring : data.string.p4_2, imgs :'prism3.png', sol :data.string.p2_12, suffix:'m<sub>3</sub>', answer :data.string.p4_8},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p4_1,
			qTitle : data.string.p4_1,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
		}else if(countNext == 2){
			$('.q2').hide(0);
		}

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p4_3,
			ans : sub.base,
			answer : data.string.p4_4,
			text : sub.ansque,
			baseString : sub.ansstring,
			couter : countNext+1,
			image1 : sub.imgs,
			solution: sub.sol,
			suffix: sub.suffix,
			hint : sub.answer,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
			swal(data.string.h2);
		}else{

			if (base ===upDatas.base) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
			} else {
				$board.find(cls+" .check1").html(wrong);
			}


			if (check[0]===1) {
					$nextBtn.show(0);
			} else {
				$board.find(cls+" .clicks .click").show(0);
			}
		}
	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
		ole.activityComplete.finishingcall();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

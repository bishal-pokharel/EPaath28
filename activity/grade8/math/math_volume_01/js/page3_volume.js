/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var imgpath = $ref+"/image/";


$(function () {
	
	
	
		
	var characterDialouges1 = [ {
		diaouges : data.string.p3_5
	},//des1
	
	];
	
	
	var characterDialouges2 = [ {
		diaouges : data.string.p3_6
	},//des2
	
	];
	
	
	
	var characterDialouges3 = [ {
		diaouges : data.string.p3_7
	},//des3
	
	];
	
	
	
	var characterDialouges4 = [ {
		diaouges : data.string.p3_8
	},//des4
	
	];
	

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=23;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,6,12]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		
		//data.string.p3_1,
		data.string.p3_1_0,
		data.string.p3_1_1,
		data.string.p3_2,
		data.string.p3_3,
		data.string.p3_4,
		data.string.p3_8_1,
		data.string.p3_8_2,
		data.string.p3_8_3
		
		
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
			data.string.p3_1,
			data.string.p3_9,
			data.string.p3_10,
			data.string.p3_10_1,
			data.string.p3_11,
			data.string.p3_12,
			data.string.p3_12_1,
			data.string.p3_13,
			data.string.p3_14,
			data.string.p3_15
			
			
			],
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [
			
				data.string.p3_16,
				data.string.p3_17,
				data.string.p3_18,
				data.string.p3_19,
				data.string.p3_20,
				data.string.p3_21,
				data.string.p3_22,
				data.string.p3_23,
				data.string.p3_24,
				data.string.p3_25,
				data.string.p3_26,
				data.string.p3_27
				
				
			
				],
	}
	

	
	];
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			
		};
		
		
		function first01 () {
			// $(".nextBtn.myNextStyle").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").delay(6000).fadeIn();
			$board.find(".main-bg4").delay(2000).fadeIn();
			$("#reload1").attr("src", imgpath+"walking.gif?"+new Date().getTime());
			
			$( ".main-bg4" ).animate({ "right": "-=5%" }, {
					duration: 4000, 
					
					
				});
				$(".pri").show(0);
				$(".pyram").delay(7000).fadeIn();
				// $(".nextBtn.myNextStyle").delay(9000).fadeIn();	
				$.nextBtn.hide(0).delay(9000).fadeIn();	
				
				
				
			
			//$(".loku_stick").show(0);
			
		}
		
		/*function first02 () {
			//$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .blackboard").hide(0);
			
		}*/
		
		
		function first03 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			//$board.find(".wrapperIntro.firstPage .blackboard").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$(".loku_stick").hide(0);
			$(".pri").hide(0);
			$(".pyram").hide(0);
			$board.find(".main-bg4").hide(0);
			
		}
		
		
		function first04 () {
			$board.find(".wrapperIntro.firstPage .image-holder").show(0);
			appendDialouge = '<div class="p3_5">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		$board.find(".wrapperIntro.firstPage .text5").show(0);
			
		}
		
		
		
		/*function first05(){
			appendDialouge = '<div class="p3_5">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		}*/
		
		
		function first05(){
			// $(".nextBtn.myNextStyle").hide(0);
			appendDialouge = '<div class="p3_6">';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des2").append(appendDialouge);
		$board.find(".wrapperIntro.firstPage .arrow").show(0);
		$board.find(".wrapperIntro.firstPage .image-holder1").show(0);
		// $(".nextBtn.myNextStyle").delay(1500).show(0);
		$.nextBtn.hide(0).delay(1500).fadeIn();	
		$board.find(".wrapperIntro.firstPage .text6").show(0);
		
		}
		
		
		function first06(){
			// $(".nextBtn.myNextStyle").hide(0);
			appendDialouge = '<div class="p3_7">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		$board.find(".wrapperIntro.firstPage .arrow01").show(0);
		$board.find(".wrapperIntro.firstPage .image-holder2").show(0);
		// $(".nextBtn.myNextStyle").delay(3000).show(0);
		$.nextBtn.hide(0).delay(3000).fadeIn();	
		$board.find(".wrapperIntro.firstPage .text7").show(0);
		
		}
		
		/*function first07(){
			$(".nextBtn.myNextStyle").hide(0);
			$board.find(".wrapperIntro.firstPage .image-holder").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .arrow").hide(0);
			$board.find(".wrapperIntro.firstPage .image-holder1").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .arrow01").hide(0);
			$board.find(".wrapperIntro.firstPage .image-holder2").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .des2").hide(0);
			$board.find(".wrapperIntro.firstPage .des3").hide(0);
			$board.find(".wrapperIntro.firstPage .p3_5").hide(0);
			$board.find(".wrapperIntro.firstPage .black").show(0);
			$board.find(".wrapperIntro.firstPage .black").addClass("board");
			$board.find(".wrapperIntro.firstPage .image-holder3").show(0);
			$(".nextBtn.myNextStyle").delay(3000).show(0);
			
		}
		
		function first08() {
			appendDialouge = '<div class="p3_8">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		$(".loku_stick1").show(0);
		}
		*/
		
		
		
		function second () {
			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$(".bg_page4").show(0);
		}
		
		
	
		
		function second02 () {
			$board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);
			
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .bubble2").show(0);
			
			
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .boy-talking02").css('border-color','#FEA92A');
		}
		
		
		function second03 () {
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .bubble2").hide(0);
			$board.find(".wrapperIntro.second .bubble1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			
			
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);
			
			
		}
		
		function second04 () {
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .bubble2").hide(0);
			$board.find(".wrapperIntro.second .bubble1").hide(0);
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$(".letslook").show(0);
			
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);
			
			
		}
		
		
		function second05 () {
			// $(".nextBtn.myNextStyle").hide(0);
			$board.find(".wrapperIntro.second .cylinder").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking03").hide(0);
			//$board.find(".wrapperIntro.second .squirrel-talking03").css('background','#FFFC7B');
			//$board.find(".wrapperIntro.second .squirrel-talking03").css('border-color','#FEA92A');
			
			$board.find(".wrapperIntro.second .text3").hide(0);
			//$board.find(".wrapperIntro.second .text3").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text4").show(0);
			$board.find(".wrapperIntro.second .text5").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text6").delay(1500).show(0);
			$board.find(".wrapperIntro.second .text7").delay(2000).show(0);
			$board.find(".wrapperIntro.second .text8").delay(2500).show(0);
			// $(".nextBtn.myNextStyle").delay(3000).show(0);
			$.nextBtn.hide(0).delay(3000).fadeIn();	
			$(".black").show(0);
			$(".letslook").hide(0);
			$(".loku_stick2").show(0);
			
		}
		
		
		function second06 (){
			$(".black").hide(0);
			$(".letslook").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking01").hide(0);
			//$board.find(".wrapperIntro.second .squirrel-talking01").css('background','#b6b6b6');
			$(".loku_stick2").hide(0);
			
			$board.find(".wrapperIntro.second .cylinder").hide(0);
			//$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").hide(0);
			$board.find(".wrapperIntro.second .text5").hide(0);
			$board.find(".wrapperIntro.second .text6").hide(0);
			$board.find(".wrapperIntro.second .text7").hide(0);
			$board.find(".wrapperIntro.second .text8").hide(0);
			$board.find(".wrapperIntro.second .text9").show(0);
			
			
		}
		
				
		
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function third () {
			var source = $("#intro03-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];

			var html = template(content);
			$board.html(html);
			
			
		}
		

		
		function third01 () {
			$board.find(".wrapperIntro.third .text1").show(0);
			$board.find(".wrapperIntro.third .cylinder2").show(0);
		}
		
		
		function third03 () {
			$board.find(".wrapperIntro.third .text2").show(0);
			
		}
		
		function third04 () {
			$board.find(".wrapperIntro.third .text3").show(0);
			
		}
		
		function third05 () {
			$board.find(".wrapperIntro.third .text4").show(0);
			
		}
		
		function third06 () {
			$board.find(".wrapperIntro.third .text5").show(0);
			
		}
		
		function third07 () {
			$board.find(".wrapperIntro.third .text6").show(0);
			
		}
		
		function third08 () {
			$board.find(".wrapperIntro.third .text7").show(0);
			
		}
		
		function third09 () {
			$board.find(".wrapperIntro.third .text8").show(0);
			
		}
		
		function third10 () {
			$board.find(".wrapperIntro.third .text9").show(0);
			
		}
		
		function third11 () {
			$board.find(".wrapperIntro.third .text10").show(0);
			
		}
		function third12 () {
			$board.find(".wrapperIntro.third .text11").show(0);
			
		}
		
	
/*----------------------------------------------------------------------------------------------------*/
		



		// first func call
		first();
		$nextBtn.show(0);
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		$nextBtn.show(0);
		fnArray = [
			first,  //first slide
			first01,
			//first02,
			first03,
			first04,
			first05,
			first06,
			//first07,
			//first08,
			
			second,//second slide
			
			second02,
			second03,
			second04,
			second05,
			second06,

			
			
			third,//third slide
			third01,
			//third02,
			third03,
			third04,
			third05,
			third06,
			third07,
			third08,
			third09,
			third10,
			third11,
			third12
			
		
			
		];
		

		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.lessonEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

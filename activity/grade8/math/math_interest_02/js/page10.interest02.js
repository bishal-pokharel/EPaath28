/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=24;

		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,11]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",

			text : [

			data.string.p10_1,
			data.string.p10_2,
			data.string.p10_3,
			data.string.p10_4,
			data.string.p10_5,
			data.string.p10_6,
			data.string.p10_7,
			data.string.p10_8,
			data.string.p10_9,
			data.string.p10_10


			]


	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			data.string.p10_12,
			data.string.p10_13,
			data.string.p10_14,
			data.string.p10_15,
			data.string.p10_16,
			data.string.p10_17,
			data.string.p10_18,
			data.string.p10_19,
			data.string.p10_20,
			data.string.p10_21,
			data.string.p10_22,
			data.string.p10_23,
			data.string.p10_24,
			data.string.p10_25


			]

	}


	]




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);

			$board.html(html);
			$nextBtn.show(0);

		};

		function first01 () {

			$board.find(".wrapperIntro.firstPage .text1").show(0);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text2").css("background","#efefef");

		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);

		}

		function first04 () {
			$board.find(".wrapperIntro.firstPage .solution02").show(0);

		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .solution03").show(0);

		}

		function first06 () {
			$board.find(".wrapperIntro.firstPage .solution04").show(0);

		}

		function first07 () {
			$board.find(".wrapperIntro.firstPage .solution05").show(0);

		}

		function first08 () {
			$board.find(".wrapperIntro.firstPage .solution06").show(0);

		}

		function first09 () {
			$board.find(".wrapperIntro.firstPage .solution07").show(0);
			$board.find(".wrapperIntro.firstPage .solution07").css("display","block");

		}

		function first10 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").css("background","none");
			$board.find(".wrapperIntro.firstPage .solution00").hide(0);
			$board.find(".wrapperIntro.firstPage .solution01").hide(0);
			$board.find(".wrapperIntro.firstPage .solution02").hide(0);
			$board.find(".wrapperIntro.firstPage .solution03").hide(0);
			$board.find(".wrapperIntro.firstPage .solution04").hide(0);
			$board.find(".wrapperIntro.firstPage .solution05").hide(0);
			$board.find(".wrapperIntro.firstPage .solution06").hide(0);
			$board.find(".wrapperIntro.firstPage .solution07").hide(0);

			$board.find(".wrapperIntro.firstPage").css('background', 'url(activity/grade8/math/math_interest_02/image/page7/letslook2.png)');
			$board.find(".wrapperIntro.firstPage").css("background-size","100%");
			$board.find(".wrapperIntro.firstPage .solution08").show(0);


		}








		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("padding","1%");
			$board.find(".wrapperIntro.second").css("background","#fff");

		};



		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);

		}

		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}


		function second11 () {
			$board.find(".wrapperIntro.second .solution08").show(0);


		}

		function second12 () {
			$board.find(".wrapperIntro.second .solution09").show(0);


		}

		function second13 () {
			$board.find(".wrapperIntro.second .solution10").show(0);
			$board.find(".wrapperIntro.second .solution10").css("display","block");


		}

// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				first05,
				first06,
				first07,
				first08,
				first09,
				first10,




				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

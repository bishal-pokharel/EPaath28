/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=18;

		$nextBtn.css('display','block');
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,6,11]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p2_1,
		data.string.p2_2,
		data.string.p2_3,
		data.string.p2_4,
		data.string.p2_5,
		data.string.p2_6,
		data.string.p2_7,
		data.string.p2_8
		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p2_1,
			data.string.p2_10,
			data.string.p2_11,
			data.string.p2_12,
			data.string.p2_13,
			data.string.p2_14,
			data.string.p2_15,
			data.string.p2_16,
			data.string.p2_17,
			data.string.p2_18],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
			data.string.p2_19,
			data.string.p2_20,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26




				],
	},



	{
		justClass : "fourth",

			text : [

			data.string.p2_1,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27,
			data.string.p2_28,
			data.string.p2_29,
			data.string.p2_30,
			data.string.p2_31,
			data.string.p2_32,



			],
	}




	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

			setTimeout(function(){

			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");


			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").css('display','block');
			$board.find(".wrapperIntro.firstPage .text0").hide(0);

			$board.find(".wrapperIntro.firstPage .text1").css('display','block');
			$board.find(".wrapperIntro.firstPage .thoughts").css('display','block');
			$board.find(".wrapperIntro.firstPage .thoughts").css("display","inline-flex");
			$board.find(".wrapperIntro.firstPage .bubble2").css('display','block');


			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);

			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").css('display','block');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('display','block');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#FEA92A');

            },100);



		};



		function first02 () {

			$board.find(".wrapperIntro.firstPage .thoughts").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").css('display','block');
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").css('display','block');


			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening02").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#FEA92A');


		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").css('display','block');
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").css('display','block');


			$board.find(".wrapperIntro.firstPage .boy-talking01").css('display','block');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").css('display','block');
			$board.find(".wrapperIntro.firstPage .boy-listening02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);

		}

		function first04 () {
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").css('display','block');
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").css('display','block');
			$board.find(".wrapperIntro.firstPage .text4").css("width","53.5%").css("top","32%");
			$board.find(".wrapperIntro.firstPage .bubble1").css("width","53%").css("top","30%").css("height","29%");


			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('border-color','#FEA92A');

		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").css('display','block');
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").css('display','block');


			$board.find(".wrapperIntro.firstPage .boy-talking01").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").css('display','block');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');




		}

		function first06 () {
			$board.find(".wrapperIntro.firstPage .bubble1").css('display','block');
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);

			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").css('display','block');



			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening02").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('display','block');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('border-color','#FEA92A');




		}





	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.second .text0").hide(0);

			$nextBtn.css('display','block');


			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").css('display','block');
			$board.find(".wrapperIntro.second .boy-listening01").css('display','block');



			$board.find(".wrapperIntro.second ,.wrapperIntro.second ").css('display','block');
			$board.find(".wrapperIntro.second .text1").css('display','block');
			$board.find(".wrapperIntro.second .bubble2").css('display','block');



			$board.find(".wrapperIntro.second .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening01").css('display','block');
			$board.find(".wrapperIntro.second .boy-talking01").css('display','block');
			$board.find(".wrapperIntro.second .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .boy-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);


		}


		function second01 () {


			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").css('display','block');
			$board.find(".wrapperIntro.second .boy-listening01").css('display','block');



			$board.find(".wrapperIntro.second ,.wrapperIntro.second ").css('display','block');
			$board.find(".wrapperIntro.second .text1").css('display','block');
			$board.find(".wrapperIntro.second .bubble2").css('display','block');


			$board.find(".wrapperIntro.second .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening01").css('display','block');
			$board.find(".wrapperIntro.second .boy-talking01").css('display','block');
			$board.find(".wrapperIntro.second .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);



		}

		function second02 () {

			$board.find(".wrapperIntro.second .blackboard").css('display','block');
			$board.find(".wrapperIntro.second .teacher").css('display','block');
			$board.find(".wrapperIntro.second .teacher").addClass("slideRight");
			$board.find(".wrapperIntro.second .bubble2").hide(0);
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .char-a").addClass("char-align");
			$board.find(".wrapperIntro.second .char-b").hide(0);

			$board.find(".wrapperIntro.second .text2").css('display','block');
			$board.find(".wrapperIntro.second .text2").addClass("slideRight");
			//$board.find(".wrapperIntro.second .bubble2").addClass("slideRight");

			//$board.find(".wrapperIntro.second .squirrel-talking01").css('display','block');
			$board.find(".wrapperIntro.second .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .squirrel-listening01").hide(0);


		}


		function second03 () {



			$board.find(".wrapperIntro.second .squirrel-talking01").hide(0);
			//$board.find(".wrapperIntro.second .squirrel-talking02").css('display','block');
			//$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#FFFC7B');


			$board.find(".wrapperIntro.second .text3").css('display','block');
			$board.find(".wrapperIntro.second .text4").css('display','block');



		}

		function second04 () {

			$board.find(".wrapperIntro.second .text5").css('display','block');
			$board.find(".wrapperIntro.second .text6").css('display','block');
		}

		function second05 () {

			$board.find(".wrapperIntro.second .text7").css('display','block');
			$board.find(".wrapperIntro.second .text8").css('display','block');
		}



		function second06 () {

			$board.find(".wrapperIntro.second .text9").css('display','block');
		}



		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.css('display','block');
			$board.find(".wrapperIntro.third .text1").css('display','block');



		}


		function third01() {
			$board.find(".wrapperIntro.third .text1").css('display','block');


		}


		function third02(){
			$board.find(".wrapperIntro.third .text2").css('display','block');
		}

		function third03(){
			$board.find(".wrapperIntro.third .text3").css('display','block');
		}

		function third04(){
			$board.find(".wrapperIntro.third .text4 #solution00").css('display','block');
			$board.find(".wrapperIntro.third .text4").css("background","#efefef");
		}


		function third05(){
			$board.find(".wrapperIntro.third .text4 #solution01").css('display','block');
		}


		function third06(){
			$board.find(".wrapperIntro.third .text4 #solution02").css('display','block');

		}

		function third07(){
			$board.find(".wrapperIntro.third .text4 #solution03").css('display','block');
			$board.find(".wrapperIntro.third .continue").css('display','block');

		}

/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.css('display','block');
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				// first01,
				first02,
				first03,
				first04,
				first05,
				first06,
				//first07,


				second,
				//second01,
				second02,
				second03,
				second04,
				second05,
				second06,


				third,
				/*third01,*/
				third02,
				third03,
				third04,
				third05,
				third06,
				third07


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).css('display','block');
				// $nextBtn.css('display','block');
			}
		}
	/****************/

});

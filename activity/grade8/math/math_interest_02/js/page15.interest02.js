/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=5;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p12_1,
		data.string.p12_2,
		data.string.p12_3,
		data.string.p12_4,
		data.string.p12_5,
		data.string.p12_6,
		data.string.p12_7,
		data.string.p12_8,
		data.string.p12_9,
		data.string.p12_10

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			data.string.p12_8,
			data.string.p12_9,
			data.string.p12_10,
			data.string.p12_11,
			data.string.p12_12,
			data.string.p12_13,
			data.string.p12_14,
			data.string.p12_15,
			data.string.p12_16,
			data.string.p12_17,
			data.string.p12_18,
			data.string.p12_19


			],
	}



	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.firstPage .text0").show(0);

		};



		function first01 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);

		}

		function first02 () {

			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .cloud1").hide(0);
			$board.find(".wrapperIntro.firstPage .cloud2").hide(0);
			$board.find(".wrapperIntro.firstPage .manish").hide(0);
			$board.find(".wrapperIntro.firstPage").css("background","#fff");




			$board.find(".wrapperIntro.firstPage .owl").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage").css("background","#4D4D4D");
			$board.find(".wrapperIntro.firstPage .text7").show(0);


		}


		function first03 () {


			$board.find(".wrapperIntro.firstPage .owl").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").hide(0);

			$board.find(".wrapperIntro.firstPage .rabbit").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage").css("background","#4D4D4D");
			$board.find(".wrapperIntro.firstPage .text8").show(0);


		}

		function first04 () {


			$board.find(".wrapperIntro.firstPage .rabbit").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text8").hide(0);

			$board.find(".wrapperIntro.firstPage .turtle").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage").css("background","#4D4D4D");
			$board.find(".wrapperIntro.firstPage .text9").show(0);
		}


		function first05 () {

			$board.find(".wrapperIntro.firstPage .text9").hide(0);
			$board.find(".wrapperIntro.firstPage .turtle").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .map").show(0);


			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text5").addClass("moveleftanim1");
			$board.find(".wrapperIntro.firstPage .text6").show(0);
		}





	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");


		}



		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);


		}


		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}


		/*function second11 () {
			$board.find(".wrapperIntro.second .solution08").show(0);
			$board.find(".wrapperIntro.second .solution08").css("display","block");


		}*/




		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/





		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				first05,



				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10
				//second11


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

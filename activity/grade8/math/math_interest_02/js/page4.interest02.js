/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/
var imgpath = $ref+"/image/page4/";
$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=15;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p4_0,
		data.string.p4_1,
		data.string.p4_2,
		data.string.p4_3,
		data.string.p4_4,
		data.string.p4_5,
		data.string.p4_6,
		data.string.p4_7,
		data.string.p4_8
		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p4_9,

			data.string.p4_11,
			data.string.p4_12,
			data.string.p4_13,
			data.string.p4_14,
			data.string.p4_15,
			data.string.p4_16,
			data.string.p4_17,
			data.string.p4_18,
			data.string.p4_19,
			data.string.p4_20,
			data.string.p4_21,
			data.string.p4_22,
			data.string.p4_23

			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
			data.string.p2_19,
			data.string.p2_20,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26




				],
	},



	{
		justClass : "fourth",

			text : [

			data.string.p2_1,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27,
			data.string.p2_28,
			data.string.p2_29,
			data.string.p2_30,
			data.string.p2_31,
			data.string.p2_32,



			],
	}




	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.firstPage .text0").hide(0);

			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);

			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bub2").addClass("slideRight");


			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);


		};



		function first01 () {



			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);

			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bub2").addClass("slideRight");


			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);




		}

		function first02 () {
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").hide(0);
			//$board.find(".wrapperIntro.firstPage .bub1").show(0);
			//$board.find(".wrapperIntro.firstPage .bub1").css("width","55%").css("height","33%");

			$board.find(".wrapperIntro.firstPage .blackboard").show(0);



			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);



			$board.find(".wrapperIntro.firstPage .bub2").addClass("slideLeft");



			$board.find(".wrapperIntro.firstPage .text2").delay(1000).show(0);
			$board.find(".wrapperIntro.firstPage .text3").delay(1500).show(0);
			//$board.find(".wrapperIntro.firstPage .text4").delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .text5").delay(2000).show(0);
			$nextBtn.delay(2500).show(0);



		}


		function first03 () {

			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").css("width","48%").css("height","26%");

		}


		function first04 () {
			$board.find(".wrapperIntro.firstPage .text7").show(0);


		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .blackboard").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);

			$board.find(".wrapperIntro.firstPage .text8").show(0);
			$board.find(".wrapperIntro.firstPage").css('background', 'url(activity/grade8/math/math_interest_02/image/page4/letslook.png)');
			$board.find(".wrapperIntro.firstPage").css("background-size","100%");
			$board.find(".wrapperIntro.firstPage .char-a").hide(0);
			$board.find(".wrapperIntro.firstPage .char-b").hide(0);

		}


	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");

		}


		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);


		}


		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}


		function second11 () {
			$board.find(".wrapperIntro.second .solution08").show(0);


		}

		function second12 () {
			$board.find(".wrapperIntro.second .solution09").css("display","block");


		}

		/*function second13 () {
			$board.find(".wrapperIntro.second .solution10").show(0);
			$board.find(".wrapperIntro.second .solution10").css("display","block");


		}*/

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/



/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				//first01,
				first02,

				first05,


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12
				//second13


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

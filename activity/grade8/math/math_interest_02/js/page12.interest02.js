/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

	var characterDialouges1 = [ {
		diaouges : data.string.p12_1
	},

	];


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=11;

		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p12_2,
			data.string.p12_3,
			data.string.p12_4,
			data.string.p12_5,
			data.string.p12_6,
			data.string.p12_7,
			data.string.p12_8,
			data.string.p12_9,
			data.string.p12_10,
			data.string.p12_11,
			data.string.p12_12


			]

	},


	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [




				],
	},



	{
		justClass : "fourth",

			text : [




			],
	}




	]




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);

			$board.html(html);
			$nextBtn.show(0);

				appendDialouge = '<div class="p12_1">';
					var dialouges =$.each(characterDialouges1,function(key,values){
					chDialouges = values.diaouges;

					appendDialouge +=chDialouges;
				});
				appendDialouge += '</div>';
				$(".des1").append(appendDialouge);


		}




		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);

		};



		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);

		}

		function second03 () {
			$board.find(".wrapperIntro.second .text3").show(0);

		}

		function second04 () {
			$board.find(".wrapperIntro.second .text4").show(0);

		}

		function second05 () {
			$board.find(".wrapperIntro.second .text5").show(0);

		}

		function second06 () {
			$board.find(".wrapperIntro.second .text6").show(0);

		}

		function second07 () {
			$board.find(".wrapperIntro.second .text7").show(0);

		}

		function second08 () {
			$board.find(".wrapperIntro.second .text8").show(0);

		}

		function second09 () {
			$board.find(".wrapperIntro.second .text9").show(0);

		}

		function second10 () {
			$board.find(".wrapperIntro.second .text10").show(0);

		}



// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

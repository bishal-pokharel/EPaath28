$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		$pageEnd = false,
		countNext = 0,
		$total_page = 1;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
	{
	note:data.string.p7_11,
	base : "8712.5",
	base1 : "8500",
	base2 : "0.25",
	base3 : "10",
	base4 : "8500",
	base5 : "212.5",
	ansstring : data.string.p7_3,
	ansstring1: data.string.p9_4,

	ansque : data.string.p9_2,
	suffix :data.string.p1_5
	},





	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p5_1,
			qTitle : data.string.p9_1,
			note : data.string.p5_13,
			amounttxt : data.string.p9_4,


		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
		$(".base").attr("placeholder", data.string.amount);
		$(".base1").attr("placeholder", data.string.principal);
		$(".base2").attr("placeholder", data.string.time);
		$(".base3").attr("placeholder", data.string.rate);
		$(".base4").attr("placeholder", data.string.principal);
		$(".base5").attr("placeholder", data.string.interest);
	}






	function first(sub) {
		$(".q1").hide(0);
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p5_4,
			ans : sub.base,
			given: sub.note,

			ans1 : sub.base1,
			ans2 : sub.base2,
			ans3 : sub.base3,

			answer : data.string.p5_5,
			text : sub.ansque,

			baseString : sub.ansstring,
			baseString1 : sub.ansstring1,



			suffix :data.string.p1_5,
			dividetxt: data.string.p5_12,
			couter : countNext+1,
			check: data.string.check

		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};




	$board.on('click','.clicks .click',function (){

		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {

		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();

		var base1 = $board.find(cls+" input.base1").val().toLowerCase();
		var base2 = $board.find(cls+" input.base2").val().toLowerCase();
		var base3 = $board.find(cls+" input.base3").val().toLowerCase();
		var base4 = $board.find(cls+" input.base4").val().toLowerCase();
		var base5 = $board.find(cls+" input.base5").val().toLowerCase();


		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		check[0]=0;
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(!$pageEnd) {
			if (base1 ===upDatas.base1) {
				check[0]++;
				$board.find(cls+" input.base2").removeClass("wrong");
				$board.find(cls+" input.base1").addClass("right");

			} else {
				$board.find(cls+" input.base2").removeClass("right");
				$board.find(cls+" input.base1").addClass("wrong");
			}
			if (base2 ===upDatas.base2) {
				check[0]++;
				$board.find(cls+" input.base2").removeClass("wrong");
				$board.find(cls+" input.base2").addClass("right");
			} else {
				$board.find(cls+" input.base2").removeClass("right");
				$board.find(cls+" input.base2").addClass("wrong");
			}


			if (base3 ===upDatas.base3) {
				check[0]++;
				$board.find(cls+" input.base3").removeClass("wrong");
				$board.find(cls+" input.base3").addClass("right");
			} else {
				$board.find(cls+" input.base3").removeClass("right");
				$board.find(cls+" input.base3").addClass("wrong");
			}

			if (base4 ===upDatas.base4) {
				check[0]++;
				$board.find(cls+" input.base4").removeClass("wrong");
				$board.find(cls+" input.base4").addClass("right");
			} else {
				$board.find(cls+" input.base4").removeClass("right");
				$board.find(cls+" input.base4").addClass("wrong");
			}

			if (base5 ===upDatas.base5) {
				check[0]++;
				$board.find(cls+" input.base5").removeClass("wrong");
				$board.find(cls+" input.base5").addClass("right");
			} else {
				$board.find(cls+" input.base5").removeClass("right");
				$board.find(cls+" input.base5").addClass("wrong");
			}


			if (check[0]===3) {
				//$board.find(cls+" .check1").html(right);
				$board.find(".finalans1").show(0);
				$board.find(cls+" input.base4").removeClass("wrong");
				$board.find(cls+" input.base5").removeClass("wrong");
				$board.find(cls+" input.base1").prop("disabled", true);
				$board.find(cls+" input.base2").prop("disabled", true);
				$board.find(cls+" input.base3").prop("disabled", true);

				//$pageEnd = true;
			}

			if (check[0]===5) {
				//$board.find(cls+" .check1").html(right);
				//$board.find(".finalans1").show(0);
				$board.find(".finalans").show(0);
				$board.find(cls+" input.base1").prop("disabled", true);
				$board.find(cls+" input.base2").prop("disabled", true);
				$board.find(cls+" input.base3").prop("disabled", true);

				$pageEnd = true;
			}

		} else {
			//

			if (base ===upDatas.base) {
				check[0]++;
				$board.find(cls+" input.base").removeClass("wrong");
				$board.find(cls+" input.base").addClass("right");
				$board.find(cls+" input.base").prop("disabled", true);

			} else {
				$board.find(cls+" input.base").removeClass("right");
				$board.find(cls+" input.base").addClass("wrong");
				$board.find(cls+" .click").show(0);

			}



			if (check[0]===1) {
                ole.footerNotificationHandler.pageEndSetNotification();
                $board.find(cls+" .check1").html(right);
				if (countNext>=0) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);

				}
			} else {
				if (countNext>=0) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);
				}
				$board.find(cls+" .check1").html(wrong);
				$board.find(cls+" .clicks .click").show(0);
			}
		}

	})

	/*first call to first*/
	// first();
	init();
		//initial funtion

	$nextBtn.on('click',function () {
		$pageEnd = false;
		$(this).hide(0);
		$(".q2").hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

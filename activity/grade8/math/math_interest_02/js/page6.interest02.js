/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=20;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,2]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p6_1,
		data.string.p6_2,
		data.string.p6_3,
		data.string.p6_4

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			data.string.p6_5,
			data.string.p6_6,
			data.string.p6_7,
			data.string.p6_8,
			data.string.p6_9,
			data.string.p6_10,
			data.string.p6_11,
			data.string.p6_12,
			data.string.p6_13,
			data.string.p6_14,
			data.string.p6_15,
			data.string.p6_16,
			data.string.p6_17,
			data.string.p6_18,
			data.string.p6_19,
			data.string.p6_20,
			data.string.p6_21,
			data.string.p6_22
			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
			data.string.p2_19,
			data.string.p2_20,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26




				],
	},



	{
		justClass : "fourth",

			text : [

			data.string.p2_1,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27,
			data.string.p2_28,
			data.string.p2_29,
			data.string.p2_30,
			data.string.p2_31,
			data.string.p2_32,



			],
	}




	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {


			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
            setTimeout(function () {

			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);



			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bubble1").addClass("slideRight");




			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);



            },100);
		};



		function first01 () {

			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);



			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bubble1").addClass("slideRight");




			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);


		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			//$board.find(".wrapperIntro.firstPage .bubble2").addClass("slideLeft");


			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);


		}

		/*function first03 () {
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			//$board.find(".wrapperIntro.firstPage .text3").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bubble1").addClass("slideRight");


			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").hide(0);


		}*/




	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#FFF");
			$board.find(".wrapperIntro.second").css("padding","1%");

		}


		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {

			$board.find(".wrapperIntro.second .text2").show(0);

		}


		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#e7e7e7");

		}

		function second04 () {

			$board.find(".wrapperIntro.second .solution01").show(0);

		}
		function second05 () {

			$board.find(".wrapperIntro.second .solution02").show(0);

		}
		function second06 () {

			$board.find(".wrapperIntro.second .solution03").show(0);

		}
		function second07 () {

			$board.find(".wrapperIntro.second .solution04").show(0);

		}
		function second08 () {

			$board.find(".wrapperIntro.second .solution05").show(0);

		}
		function second09 () {

			$board.find(".wrapperIntro.second .solution06").show(0);

		}
		function second10 () {

			$board.find(".wrapperIntro.second .solution07").show(0);
			$board.find(".wrapperIntro.second .solution07 .note").css("display","block");


		}
		function second11 () {

			$board.find(".wrapperIntro.second .solution08").show(0);

		}
		function second12 () {

			$board.find(".wrapperIntro.second .solution00").css("display","none");
			$board.find(".wrapperIntro.second .solution01").css("display","none");
			$board.find(".wrapperIntro.second .solution02").css("display","none");
			$board.find(".wrapperIntro.second .solution03").css("display","none");
			$board.find(".wrapperIntro.second .solution04").css("display","none");
			$board.find(".wrapperIntro.second .solution05").css("display","none");
			$board.find(".wrapperIntro.second .solution06").css("display","none");
			$board.find(".wrapperIntro.second .solution07").css("display","none");
			$board.find(".wrapperIntro.second .solution08").css("display","none");
			$board.find(".wrapperIntro.second .text3").css("height","65%");
			$board.find(".wrapperIntro.second .solution09").show(0);

		}
		function second13 () {

			$board.find(".wrapperIntro.second .solution10").show(0);

		}
		function second14 () {

			$board.find(".wrapperIntro.second .solution11").show(0);

		}

		function second15 () {

			$board.find(".wrapperIntro.second .solution12").show(0);

		}

		function second16 () {

			$board.find(".wrapperIntro.second .solution13").show(0);

		}

		function second17 () {

			$board.find(".wrapperIntro.second .solution14").show(0);

		}

		function second18 () {

			$board.find(".wrapperIntro.second .solution15").show(0);
			//$board.find(".wrapperIntro.second .solution15.note").css("display","block");



		}

			function second19 () {

			$board.find(".wrapperIntro.second .solution16").show(0);
			$board.find(".wrapperIntro.second .solution16.note").css("display","block");



		}

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third .text0").show(0);



		}


		function third01() {
			$board.find(".wrapperIntro.third .text1").show(0);


		}


		function third02(){
			$board.find(".wrapperIntro.third .text2").show(0);
		}

		function third03(){
			$board.find(".wrapperIntro.third .text3").show(0);
		}

		function third04(){
			$board.find(".wrapperIntro.third .text4").show(0);
		}


		function third05(){
			$board.find(".wrapperIntro.third .text5").show(0);
		}


		function third06(){
			$board.find(".wrapperIntro.third .text6").show(0);

		}

		function third07(){
			$board.find(".wrapperIntro.third .text7").show(0);

		}

/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				//first01,
				first02,
				//first03,


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13,
				second14,
				second15,
				second16,
				second17,
				second18,
				second19,


				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

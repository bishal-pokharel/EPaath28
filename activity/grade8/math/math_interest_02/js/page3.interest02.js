$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
		$nextBtn.css('display','block');
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : "1,500", power : "7", third : "5", firstlabel : data.string.p3_3, secondlabel : data.string.p3_4, thirdlabel : data.string.p3_5, ansque : data.string.p3_2, prefix : 'Rs', Suffix : '%', suffix01 : 'Year' },
		{base : "2,000", power : "4", third : "3", firstlabel : data.string.p3_10, secondlabel : data.string.p3_11, thirdlabel : data.string.p3_12, ansque : data.string.p3_9, prefix : 'Rs', Suffix : '%', suffix01 : 'Year' },
		{base : "3,200", power : "8", third : "12", firstlabel : data.string.p3_17, secondlabel : data.string.p3_18, thirdlabel : data.string.p3_19, ansque : data.string.p3_16, prefix : 'Rs', Suffix : '%',suffix01 : 'Year'},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p4_24,
			qTitle : data.string.p3_1,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}


	function first(sub) {

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p3_6,
			firstans : sub.base,
			secondans : sub.power,
			thirdans : sub.third,
			answer : data.string.p3_7,
			text : sub.ansque,
			prefix : sub.prefix,
			suffix : sub.Suffix ,
			suffix01 : sub.suffix01,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			thirdlabel : sub.thirdlabel,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').css('display','block');
		$nextBtn.css('display','block');
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		var power = $board.find(cls+" input.power").val().toLowerCase();
		var third = $board.find(cls+" input.third").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if (base ===upDatas.base) {
			check[0]=1;
			$board.find(cls+" .check1").html(right);
		} else {
			$board.find(cls+" .check1").html(wrong);
		}

		if (power===upDatas.power) {
				check[1]=1;
				$board.find(cls+" .check2").html(right);
			} else {
				$board.find(cls+" .check2").html(wrong);
			}

			if (third===upDatas.third) {
				check[2]=1;
				$board.find(cls+" .check3").html(right);
			} else {
				$board.find(cls+" .check3").html(wrong);
			}

			if (check[0]===1 && check[1]===1) {
				if (countNext>=2) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.css('display','block');
				}
			} else {
				$board.find(cls+" .clicks .click").css('display','block');
			}


	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

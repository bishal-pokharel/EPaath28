var math = {
	expandExponent : function (base,power,type,space,tag) {
		var base = base;
		var power = parseInt(power);
		var space = typeof space === 'undefined' ? '' : space;
		var type = typeof type === 'undefined' ? "string" : type
		// console.log("array");
		// console.log(type);
		var numArray = (function () {
					if(type=== 'string'){
						return base;
					} if (type === 'tag') {
						return "<"+tag+">"+base+"</"+tag+">";
					} else {
						// console.log("base "+base);
						return [base];
					}
				})();

		for (var i = 0; i < power-1; i++) {
			if (type=== 'string') {
				numArray+=space+"x"+space+base;
			} else if (type=== 'tag') {
				numArray+= space+"x"+space+"<"+tag+">"+base+"</"+tag+">";
			} else if (type==='array') {
				numArray.push("x");
				numArray.push(base);
			}
		};
		return numArray;
	},
	valueExponent : function (base,power) {
		var value = '';
		if (typeof power === 'string') {
			value = base+"x"+base+"x"+base+"x"+"....."+power+" th term";
		} else if (typeof base === 'string') {
			for (var i = 0; i < power; i++) {
				value += base
			};
		} else {
			value = Math.pow(base,power)
		}
		return value;
	},
	expandPower1 : function (base,power,space) {
		var base = base;
		var power = parseInt(power);
		var space = typeof space === 'undefined' ? '' : space;
		var numArray = "<span>"+base+"</span>"+ "<sup>"+1+"</sup>";
		for (var i = 0; i < power-1; i++) {
			numArray+=space+"x"+space+ "<span>"+base+"</span>"+ "<sup>"+1+"</sup>";
		};
		return numArray;
	},
	//equation parser
	parseEquation : function (equ, req) {
		/*4 * 4^2 * 4^3= 4^(1+2+3)= 4^6*/
		var req = typeof req ? req : "string";
		var equArray = equ.split("");
		var regxNum = new RegExp("[0-9]","gi");
		var regxAlpha = new RegExp("[a-zA-Z]","gi");
		var arrayNum = equ.match(regxNum) ? equ.match(regxNum) : [] ;
		var arrayAlpha = equ.match(regxAlpha) ? equ.match(regxAlpha) : [];
		var carrot = false; //check if sup req
		var tag = "", letter = '',bracketTag = "span",
			bracket = false;
		var expression = 1;
		for (var i = 0; i < equArray.length; i++) {
			letter = equArray[i];
			tag = carrot? "sup" : "span";
			// bracketTag = carrot? "sup" : "span";
			if (arrayAlpha.indexOf(letter)>-1) {
				equArray[i] = "<"+tag+" class=''>"+letter+"</"+tag+">";
			} else if (arrayNum.indexOf(letter)>-1) {
				equArray[i] = "<"+tag+" class='coefficient'>"+letter+"</"+tag+">";
			} else if (letter==="^") {
				carrot = true;
				bracket = false;
				equArray[i] = "";
			} else if (letter === "*") {
				equArray[i] = "<"+tag+" class='multiplySign'> x </"+tag+">";
			} else if (letter === " ") {
				carrot = bracket? carrot : false;
				equArray[i] = " ";
			} else if (letter === ")") {
				carrot = false;
				bracket = false;
				equArray[i] = "<"+bracketTag+" class='jpt'>"+letter+"</"+bracketTag+">";
				bracketTag = "span";
			} else if (letter === "(") {
				bracket = true;
				bracketTag = tag;
				equArray[i] = "<"+bracketTag+" class='jpt'>"+letter+"</"+bracketTag+">";
			} else if (/^([\W])/.test(letter) && bracket === false) {
				carrot = false;
			} else {
				equArray[i] = "<"+tag+" class='jpt'>"+letter+"</"+tag+">";
			}
		};

		if (req === "array") {
			return equArray;
		}

		var stringToReturn = "";
		for (var i = 0; i < equArray.length; i++) {
			stringToReturn += equArray[i];
		};
		// console.log(stringToReturn);
		return stringToReturn;
	}
}

$.fn.extend({
	loopArray : function (array,timer,fn) {
		var $this = $(this);
		var counter=0;
		function looper () {
			$this.append(array[counter]);
			counter++;
			if (counter>=array.length) {
				setTimeout(fn,timer);
			} else {
				setTimeout(looper,timer);
			}
		}
		looper();
	}
});
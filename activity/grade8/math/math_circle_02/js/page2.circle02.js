/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var imgpath = $ref+"/image/page1/";

function slider() {
	var $board = $('.board');

			$board.find(".main-bg1").css("display", "block");
			$("#activity-page-next-btn-enabled").show(0);
}







$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn");
		countNext = 0,
		all_page=12;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3,7]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.lesson.chapter,
		data.string.p1_0_0,
		data.string.p1_0_1

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

		data.string.p2_2_1,
		data.string.p2_3,
		data.string.p2_4,
		data.string.p2_5,
		data.string.p2_6,
		data.string.p2_7,
		data.string.p2_8,
		data.string.p2_9,
		data.string.p2_10





			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
			data.string.p2_11,
			data.string.p2_12,
			data.string.p2_13,
			data.string.p2_14,
			data.string.p2_15,
			data.string.p2_16,
			data.string.p2_17,
			data.string.p2_18,
			data.string.p2_19,
			data.string.p2_20





				],
	}







	];




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-templates").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];


			var html = template(content);

			$board.html(html);
			$nextBtn.hide(0);
			slider();
			// window.setInterval(slider, 6000);

			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".wrapperIntro").css("position","absolute").css("width","100%").css("padding","0%");
			$board.find(".main-bg1").css("width","100%");
			$nextBtn.delay(6000).show(0);


		};


		function first01 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro .main-bg1").css("opacity","0");
			$board.find(".wrapperIntro .main-bg2").css("opacity","0");
			$board.find(".wrapperIntro .main-bg3").css("opacity","0");

			$board.find(".main-bg5").fadeIn(8000);




			$("#reload1").attr("src", imgpath+"walking.gif?"+new Date().getTime());

			$( ".main-bg4" ).animate({ "right": "-=1%" }, {
					duration: 4000,


				}   );

			$board.find(".main-bg4").show(0);






		}

		function first02 () {

		$board.find(".wrapperIntro.firstPage .bub").show(0);
		$board.find(".wrapperIntro.firstPage .text1").show(0);

		}

		function first03 () {

		$board.find(".wrapperIntro.firstPage .bub").show(0);
		$board.find(".wrapperIntro.firstPage .text1").hide(0);
		$board.find(".wrapperIntro.firstPage .text2").show(0);

		}


	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.second .main-circum").show(0);
			$(".exponential .board .wrapperIntro").css("padding-left","0%").css("padding-right","0%");
			$board.find(".wrapperIntro.second .text0").addClass("moveleftanim");
			$board.find(".wrapperIntro.second .circum-bg").addClass("moveleftanim1");




		}


		function second01 () {


			$board.find(".wrapperIntro.second .main-circum").hide(0);
			$board.find(".wrapperIntro.second .main-bg5").show(0);
			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .text0").removeClass("moveleftanim");



			$board.find(".wrapperIntro.second ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.second .text0").css("top", "0%");

			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .bub1").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bub1").addClass("slideRight");

			$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);



		}

		function second02 () {
			//$(".nextBtn.myNextStyle").hide(0);

			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .bub2").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");


			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);






		}




		function second05 () {
			// $(".nextBtn.myNextStyle").hide(0);
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .main-bg5").hide(0);
			$board.find(".wrapperIntro.second .char-b").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);

			$board.find(".wrapperIntro.second .text4").hide(0);

			$board.find(".wrapperIntro.second .bub2").hide(0);


			$board.find(".wrapperIntro.second .boy-talking02").hide(0);

			//$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .blackboard").show(0);
			//$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#b6b6b6');
			//$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-listening01").hide(0);


			$board.find(".wrapperIntro.second .text5").show(0);
			$(".loku").show(0);
			$board.find(".wrapperIntro.second .diameter").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text6").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text7").delay(1500).show(0);
			$board.find(".wrapperIntro.second .text8").delay(2000).show(0);
			// $(".nextBtn.myNextStyle").delay(2000).show(0);

		}


		/*




		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);


			$board.find(".wrapperIntro.third .coin2").show(0);
			$board.find(".wrapperIntro.third .bub1").show(0);
			$board.find(".wrapperIntro.third .text1").show(0);


		}







		function third04 () {


			$board.find(".wrapperIntro.third .text1").hide(0);
			$board.find(".wrapperIntro.third .bub1").hide(0);
			$board.find(".wrapperIntro.third .text2").show(0);
			$board.find(".wrapperIntro.third .coin").css("right","11%");


			$board.find(".wrapperIntro.third .text3").show(0);
			$board.find(".wrapperIntro.third .circle-img").show(0);

		}





		function third06 () {


			$board.find(".wrapperIntro.third .text5").show(0);




		}

		function third07 () {


			$board.find(".wrapperIntro.third .text6").show(0);
			$board.find(".wrapperIntro .coin").hide(0);

			$board.find(".wrapperIntro.third .circle-img01").show(0);



		}




		function third09 () {

			$board.find(".wrapperIntro.third .text8").show(0);
			$board.find(".wrapperIntro.third .text9").show(0);

		}




			function third11 () {

			$board.find(".wrapperIntro.third .text10").show(0);
		}

/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,



				second,
				second01,
				second02,
				//second03,

				//second04,


				second05,





				third,


				//third03,

				third04,

				third06,
				third07,

				third09


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

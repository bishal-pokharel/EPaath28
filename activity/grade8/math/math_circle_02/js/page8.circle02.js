/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	


	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=13;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p8_1,
		data.string.p8_2,
		data.string.p8_3,
		data.string.p8_4,
		data.string.p8_5
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
			data.string.p8_6,
			data.string.p8_7,
			data.string.p8_8,
			data.string.p8_9
				
			],
	}
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	
	
	
	
	
	
	
	
	
	];
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			
			
		};

	

		function first01 () {
			
			
			
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);
			
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			
			
			
			
		}

		function first02 () {
			
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			
				
			
		}

		
		
		
		
		

		
		
	
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");
			
		}
		
		
		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);
					
		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);
			
			
		}

		
		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");
			
			
		}
		
		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);
			
			
		}
		
		
		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);
			
			
		}

		
		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);
			
			
		}
		
		
		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);
			
			
		}
		
		
	
		
		
		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);
			
			
		}
		
		
			
		
		function second11 () {
			
			$nextBtn.hide(0);
			$textbox = $board.find('.inputbox'); 
			$(".inputbox").css({
				"display" : "inline-block"
			});
			$board.find(".text3").append($textbox);
			$board.find(".wrapperIntro.second .solution08").show(0);
			$board.find(".wrapperIntro.second .inputbox").show(0);
			
			$("#inputcheck").change(function(){ 
			var massvalue = $("#inputcheck").val();
				if (massvalue ==35) {
					//countNext++;		
					//templateCaller();	
					//$(".tohide").css("opacity","1");
					$(".footerNotification").show(0);
					$("#inputcheck").css("background", "#15C402");
					$nextBtn.show(0);
							
				}
				else{
					$("#inputcheck").css("background", "#FF595B");
				}
				
			});
			
			
		}
		
		function second12 () {
			$board.find(".wrapperIntro.second .solution09").show(0);
					$(".inputbox").css({
						"margin-top": "-11.5%",
						"margin-left": "30.5%"
					});
			
		}
		
		function second13 () {
			$board.find(".wrapperIntro.second .solution10").show(0);
			$board.find(".wrapperIntro.second .solution10").css("display","block");
			
			
		}
		
		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		
	
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.show(0);
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				
				
				
				
				
				second, 
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
			
				second09,
			
				second11,
				second12,
				second13
		
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	


	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=12;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p6_1,
		data.string.p6_2,
		data.string.p6_3,
		data.string.p6_4,
		data.string.p6_5,
		data.string.p6_6
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
			data.string.p4_9,
			
			data.string.p4_11,
			data.string.p4_12,
			data.string.p4_13,
			data.string.p4_14,
			data.string.p4_15,
			data.string.p4_16,
			data.string.p4_17,
			data.string.p4_18,
			data.string.p4_19,
			data.string.p4_20,
			data.string.p4_21,
			data.string.p4_22,
			data.string.p4_23
				
			],
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [
			data.string.p2_19,
			data.string.p2_20,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26
			
		
		
			
				],
	},
	
	
	
	{
		justClass : "fourth",
		
			text : [
			
			data.string.p2_1,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27,
			data.string.p2_28,
			data.string.p2_29,
			data.string.p2_30,
			data.string.p2_31,
			data.string.p2_32,
			

			
			],
	}
	
	
	
	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage").css("background","#fff");
			$board.find(".wrapperIntro.firstPage .coin1").hide(0);
			$board.find(".wrapperIntro.firstPage .coin2").hide(0);
			
			
		};

	

		function first01 () {
			
			$nextBtn.hide(0);	
			$board.find(".wrapperIntro.firstPage .coin2").hide(0);
			$board.find(".wrapperIntro.firstPage .coin1").show(0);
					
			$board.find(".wrapperIntro.firstPage .coin1").addClass("slideRightcus");
			$board.find(".wrapperIntro.firstPage .coin1").delay(2000).hide(0);
			$board.find(".wrapperIntro.firstPage .coin2").delay(2000).show(0);
			$nextBtn.delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .text1").delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .bub1").delay(2000).show(0);
			
			
			
		}

		function first02 () {
			
						
			$board.find(".wrapperIntro.firstPage .bub1").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			
			
			
		}

		
		function first03 () {
			
			$board.find(".wrapperIntro.firstPage .coin").hide(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .coin2").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage").css("padding","1%");
			
		
		}
		
		
		function first04 () {
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			
			
			
			
		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .example .example2").show(0);
			
			
		}
		
		
		function first06 () {
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			
			
			
		}
		
		function first07 () {
			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text5").css("background","#efefef");
			
			
			
			
			
		}
	
		function first08 () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);
			
			
			
			
		}
		
		function first09 () {
			$board.find(".wrapperIntro.firstPage .solution02").show(0);
			
			
			
			
		}
		
		function first10 () {
			
			$board.find(".wrapperIntro.firstPage .solution03").show(0);
			
		
			
			
		}
		
		function first11 () {
			$board.find(".wrapperIntro.firstPage .solution04").show(0);
			
			
		}
		
		function first12 () {
			$board.find(".wrapperIntro.firstPage .solution05").show(0);
			
			
		}
		
		function first13 () {
			$board.find(".wrapperIntro.firstPage .solution06").show(0);
			
			
		}
	
	/*-------------------------------------------------------------------------------------------*/
	
	
		
		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		
	
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				//first02,
				first03,
				first04,
				first05,
				first06,
				first07,
				first08,
				first09,
				first10,
				first11,
				first12,
				first13
				
		
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

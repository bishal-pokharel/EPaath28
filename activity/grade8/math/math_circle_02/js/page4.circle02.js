/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function() {
  //------------------------------------------------------------------------------------------------//
  var $board = $(".board"),
    $nextBtn = $("#activity-page-next-btn-enabled"),
    $prevBtn = $("#activity-page-prev-btn-enabled"),
    $refreshBtn = $("#activity-page-refresh-btn");
  (countNext = 0), (all_page = 16);

  var baseAndIndicesClick = [null, null]; //used by base and index

  var clickArray = [0, 8]; //manipulate the clicks like back and from top
  $total_page = clickArray.length;
  loadTimelineProgress($total_page, 1);
  var dataObj = [
    {
      justClass: "firstPage",
      animate: "true",
      text: [
        data.string.p4_0,
        data.string.p4_1,
        data.string.p4_2,
        data.string.p4_3,
        data.string.p4_4,
        data.string.p4_5,
        data.string.p4_6
      ]
    },

    /*------------------------------------------------------------------------------------------------*/
    /*start of second page*/
    {
      justClass: "second",
      animate: "true",

      text: [
        data.string.p6_3,
        data.string.p6_4,
        data.string.p6_5,
        data.string.p6_6
      ]
    }
    /*end of second page */

    /*------------------------------------------------------------------------------------------------*/
  ];

  /******************************************************************************************************/
  /*
   * first
   */
  function first() {
    var source = $("#intro-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[0];
    var html = template(content);
    $board.html(html);
    $board.find(".wrapperIntro.firstPage").css("background", "#fff");
    $board.find(".wrapperIntro.firstPage .coin1").hide(0);
    $board.find(".wrapperIntro.firstPage .coin2").hide(0);
    $board.find(".wrapperIntro.firstPage .text0").hide(0);

    $board.find(".wrapperIntro.firstPage .coin").hide(0);
    $board.find(".wrapperIntro.firstPage .text0").hide(0);
    $board.find(".wrapperIntro.firstPage .text1").hide(0);
    $board.find(".wrapperIntro.firstPage .bub1").hide(0);
    $board.find(".wrapperIntro.firstPage .coin2").hide(0);
    $board
      .find(".wrapperIntro.firstPage .text2")
      .show(0)
      .css("display", "block");
    $board.find(".wrapperIntro.firstPage").css("padding", "1%");
  }

  function first04() {
    $board.find(".wrapperIntro.firstPage .text3").show(0);
  }

  function first05() {
    $board.find(".wrapperIntro.firstPage .example .example1").show(0);
  }

  function first06() {
    $board.find(".wrapperIntro.firstPage .text4").show(0);
  }

  function first07() {
    $board.find(".wrapperIntro.firstPage .solution00").show(0);
    $board.find(".wrapperIntro.firstPage .text5").css("background", "#efefef");
  }

  function first08() {
    $board.find(".wrapperIntro.firstPage .solution01").show(0);
  }

  function first09() {
    $board.find(".wrapperIntro.firstPage .solution02").show(0);
  }

  function first11() {
    $board.find(".wrapperIntro.firstPage .solution04").show(0);
  }

  function first13() {
    $nextBtn.hide(0);
    $textbox = $board.find(".inputbox");
    $(".inputbox").css({
      display: "inline-block"
    });
    $board.find(".text5").append($textbox);
    $board.find(".wrapperIntro.firstPage .solution06").show(0);
    $board.find(".wrapperIntro.firstPage .inputbox").show(0);
    //$board.find(".wrapperIntro.firstPage .text6").show(0);
		// added later to check in submit
			$(".inputbox").append("<p class='sbmt'>"+data.string.sbmt+"</p>");
			$(".sbmt").click(function(){
	      if ($("#inputcheck").val() == 44) {
	        //countNext++;
	        //templateCaller();
	        //$(".tohide").css("opacity","1");
	        $(".footerNotification").show(0);
	        $("#inputcheck").css({
						"background": "#15C402",
						"pointer-events":"none"
					});
	        $nextBtn.show(0);
	      } else {
	        $("#inputcheck").css("background", "#FF595B");
	      }
			});
		// added later to check in submit
    // $("#inputcheck").keyup(function() {
    //   var massvalue = $(this).val();
    //   $(this).val(
    //     $(this)
    //       .val()
    //       .replace(/[^0-9.]/g, "")
    //   );
    //   if (massvalue == 44) {
    //     //countNext++;
    //     //templateCaller();
    //     //$(".tohide").css("opacity","1");
    //     $(".footerNotification").show(0);
    //     $("#inputcheck").css("background", "#15C402");
    //     $nextBtn.show(0);
    //   } else {
    //     $("#inputcheck").css("background", "#FF595B");
    //   }
    // });
  }

  /*-------------------------------------------------------------------------------------------*/

  /*second page*/

  /*text 0 firstline*/

  function second() {
    var source = $("#intro2-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[1];
    var html = template(content);
    $board.html(html);
  }

  function second01() {
    $board.find(".wrapperIntro.second .text1").show(0);
  }

  function second02() {
    $board.find(".wrapperIntro.second .text2").show(0);
  }

  function second03() {
    $board.find(".wrapperIntro.second .text3").show(0);
    $board.find(".wrapperIntro.second .solution00").show(0);
    $board.find(".wrapperIntro.second .text3").css("background", "#efefef");
  }

  function second04() {
    $board.find(".wrapperIntro.second .solution01").show(0);
  }

  function second05() {
    $board.find(".wrapperIntro.second .solution02").show(0);
  }

  function second07() {
    $board.find(".wrapperIntro.second .solution04").show(0);
  }

  function second09() {
    $board.find(".wrapperIntro.second .solution06").show(0);
  }

  /*end of page 2*/
  /*-------------------------------------------------------------------------------------------------*/

  /*page3*/

  /*----------------------------------------------------------------------------------------------------*/

  // first func call
  first();
  $nextBtn.show(0);
  // lastBaseIndices ();
  // countNext = 12;

  /*click functions*/
  $nextBtn.on("click", function() {
    // $(this).css("display","none");
    $prevBtn.show(0);
    countNext++;
    fnSwitcher();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] < countNext) {
        countNext = clickArray[i];
        // console.log(countNext+" = "+clickArray[i]);
        break;
      }
    }
    if (countNext === 0) {
      $prevBtn.hide(0);
    }
    fnSwitcher();
  });

  function clickSequence(index) {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] === index) {
        countNext = clickArray[i];
        break;
      }
    }
    fnSwitcher();
  }

  function fnSwitcher() {
    fnArray = [
      first, //first one
      //first01,
      //first02,
      //first03,
      first04,
      first05,
      first06,
      first07,
      first08,
      first09,

      first11,

      first13,

      second,
      second01,
      second02,
      second03,
      second04,
      second05,

      second07,

      second09
    ];

    fnArray[countNext]();

    for (var i = 0; i < clickArray.length; i++) {
      if (clickArray[i] === countNext) {
        loadTimelineProgress($total_page, i + 1);
      }
    }

    if (countNext >= all_page) {
      $nextBtn.hide(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else {
      // $(this).show(0);
      // $nextBtn.show(0);
    }
  }
  /****************/
});

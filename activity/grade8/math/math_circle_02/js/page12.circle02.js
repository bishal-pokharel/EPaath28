/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var imgpath = $ref+"/image/";

$(function () {
	


	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=50;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,13,27,37]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
			
		data.string.p12_1,
		data.string.p12_2,
		data.string.p12_3,
		data.string.p12_4,
		data.string.p12_5,
		data.string.p12_5_1,
		data.string.p12_5_2,
		data.string.p12_5_3,
		data.string.p12_6,
		
		data.string.p12_7,
		data.string.p12_8,
		data.string.p12_9
	
				
			],
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [
			data.string.p12_10,
			data.string.p12_11,
			data.string.p12_12,
			data.string.p12_13,
			data.string.p12_14
			
			
			
				],
	},
	
	
	
		{
		justClass : "fourth",
		
			text : [
			
			data.string.p12_10,
			data.string.p12_15,
			data.string.p12_16,
			data.string.p12_17,
			data.string.p12_18,
			data.string.p12_19,
			data.string.p12_20,
			data.string.p12_21,
			data.string.p12_22,
			data.string.p12_23,
			data.string.p12_24,
			data.string.p12_25,
			data.string.p12_26,
			data.string.p12_27			
			
				],
	},
	
	
	
	{
		justClass : "fifth",
		
			text : [
			
			data.string.p13_1,
			data.string.p13_2,
			data.string.p13_3,
			data.string.p13_4,
			data.string.p13_5,
			data.string.p13_6,
			
				],
	},
	
	
	{
		justClass : "sixth",
		
			text : [
			
			data.string.p15_1,
			data.string.p15_2,
			data.string.p15_3,
			data.string.p15_4,
			data.string.p15_5
			
			
				],
	}
	
	
	
	
	

	
	
	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-templates").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var content = {
				title1 : data.string.p2_1,
				title2 : data.string.p12_1,
			}
			
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro .title2").show(0);
		};

		
		function first01 () {
			
			$board.find(".wrapperIntro .title2").show(0);
		}
	

		
	
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			
			
			
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-talking03").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking03").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			
			
			
			$("#reload1").attr("src", imgpath+"animate-circle.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.second .circle-paint").show(0);
			
			$nextBtn.delay(2000).show(0);
			
			
			
		}
		
		
		
		
		
		function second01 () {
			
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.second .circle-paint").hide(0);
			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			
			
			
			
			$board.find(".wrapperIntro.second ,.wrapperIntro.firstPage ").show(0);
			
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .bub2").show(0);
			
			
			
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			
			$("#reload1").attr("src", imgpath+"animate-circle.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.second .circle-paint").show(0);
			
			
			
		}

		function second02 () {
			//$(".nextBtn.myNextStyle").hide(0);
			
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .bub2").hide(0);
			$board.find(".wrapperIntro.second .bub1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			$board.find(".wrapperIntro.second .boy-listening03").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			

			
			
			
			
		}

		
		function second03 () {
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .bub2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			
			
			$board.find(".wrapperIntro.second .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);
			
			
		
		}
		
		
	
		
		function second04 () {
			
			
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .bub2").hide(0);
			$board.find(".wrapperIntro.second .bub1").show(0);
			
			
			
			$board.find(".wrapperIntro.second .text4").show(0);
			
			
			
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-listening01").hide(0);
			
		
			
			
		}
		
		function second040 () {
			
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .circle-paint").hide(0);
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.second .text4").hide(0);
			$("#reload2").attr("src", imgpath+"animate-triangle.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.second .triangle-paint").show(0);
			$nextBtn.delay(2000).show(0);
			
		}

		function second05 () {
			
			$board.find(".wrapperIntro.second .bub1").show(0);
			$board.find(".wrapperIntro.second .text5").show(0);
			
	}
		
		function second050 () {
			
			
			$board.find(".wrapperIntro.second .text5").hide(0);
			$board.find(".wrapperIntro.second .text6").show(0);
			
		}
		
		
		function second051 () {
		
		$nextBtn.hide(0);
		$board.find(".wrapperIntro.second .text5").hide(0);
		$board.find(".wrapperIntro.second .text6").hide(0);
		$board.find(".wrapperIntro.second .text7").show(0);
		$("#reload3").attr("src", imgpath+"animate-square.gif?"+new Date().getTime());
		$board.find(".wrapperIntro.second .triangle-paint").hide(0);
		$board.find(".wrapperIntro.second .square-paint").show(0);
		$nextBtn.delay(2000).show(0);
		
		
		}
		
		
		
		function second06 () {
		

			
			$board.find(".wrapperIntro.second .text7").show(0);			
			
		
		}
		
		
		function second07 () {
		
			$board.find(".wrapperIntro.second .text7").hide(0);
			
			$board.find(".wrapperIntro.second .square-paint").hide(0);			
			$board.find(".wrapperIntro.second .text8").show(0);	
			
		
			
			
		}
		
		
		function second08 () {
		
			$board.find(".wrapperIntro.second .text8").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);
			
			$board.find(".wrapperIntro.second .bub2").show(0);
			
			
			$board.find(".wrapperIntro.second .text9").show(0);	
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);
			
		}
		
		
			function second09 () {
		
			$board.find(".wrapperIntro.second .text9").hide(0);
			$board.find(".wrapperIntro.second .bub2").hide(0);
			
			$board.find(".wrapperIntro.second .bub1").show(0);
			
			
			$board.find(".wrapperIntro.second .text10").show(0);	
			$board.find(".wrapperIntro.second .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			$board.find(".wrapperIntro.second .squirrel-listening01").hide(0);
			
		}
		
		
		
		
		/*
		
		
		
		
		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			
			//$board.find(".wrapperIntro.third").css("background","#fff");
			
			
			
		}
		
		function third01 () {
			
			
			$board.find(".wrapperIntro.third .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.third .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.third .squirrel-talking03").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking03").css('background','#b6b6b6');
			$board.find(".wrapperIntro.third .boy-listening01").show(0);
			$board.find(".wrapperIntro.third .bub1").show(0);
			$board.find(".wrapperIntro.third .text1").show(0);
			
			
					
			
			
		}

		function third02 () {
			
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.third .text1").hide(0);
						
			$("#reload4").attr("src", imgpath+"cycle-wheel.gif?"+new Date().getTime());
			
			$board.find(".wrapperIntro.third .cycle-wheel").show(0);
			
			
			
			
			
				$( ".wrapperIntro.third .cycle-wheel" ).find(function() {
				$( ".cycle-rotating " ).animate({ "left": "+=40%" }, {
					duration: 4000, 
				
				
				}  );
				
				
			});
			$nextBtn.delay(4000).show(0);
			
		
			
		}

		
		function third03 () {
			$board.find(".wrapperIntro.third .text2").show(0);
			
		
		}
		
		function third04 () {
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.third .text2").hide(0);
			$board.find(".wrapperIntro.third .cycle-wheel").hide(0);
			$board.find(".wrapperIntro.third .wheel-divide").show(0);
			$("#reload5").attr("src", imgpath+"wheel-divide.gif?"+new Date().getTime());
			$nextBtn.delay(3000).show(0);
			
			
		}
		
		
		
		function third05 () {
			$board.find(".wrapperIntro.third .arrow").show(0);
			
		
		}
		
		function third06 () {
			
			$nextBtn.hide(0);
			$("#reload6").attr("src", imgpath+"circle-rect.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.third .circle-rect").show(0);
			$nextBtn.delay(3000).show(0);
			
			
		} 
		
		function third07 () {
			
			$board.find(".wrapperIntro.third .wheel-divide").hide(0);
			$board.find(".wrapperIntro.third .arrow").hide(0);
			$board.find(".wrapperIntro.third .circle-rect").hide(0);
			$board.find(".wrapperIntro.third .text3").show(0);
			
			
			
			
		}
		
		function third08 () {
			
			$board.find(".wrapperIntro.third .text3").hide(0);
			$board.find(".wrapperIntro.third .text4").show(0);
		}
		
		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		
			
		}
		
		
		function fourth01 () {
			
			$nextBtn.hide(0);
			$("#reload7").attr("src", imgpath+"wheel-divide1.gif?"+new Date().getTime());
			$("#reload8").attr("src", imgpath+"circle-rect1.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.fourth .wheel-divide").delay(500).show(0);
			$board.find(".wrapperIntro.fourth .arrow").delay(3000).show(0);
			$board.find(".wrapperIntro.fourth .circle-rect").delay(5000).show(0);
			$nextBtn.delay(8000).show(0);
		}
		
		
		function fourth02 () {
			
			$board.find(".wrapperIntro.fourth .text1").show(0);
			
		}
		
		
		function fourth03 () {
			
			$board.find(".wrapperIntro.fourth .text2").show(0);
			
		}
		
		function fourth04 () {
			
			$board.find(".wrapperIntro.fourth .text3").show(0);
			
		}
		
		function fourth05 () {
			
			$board.find(".wrapperIntro.fourth .text4").show(0);
			
		}
		
		
		function fourth06 () {
			
			
			
			
			$board.find(".wrapperIntro.fourth .text5").show(0);
			
			mathParseReplace(data.string.p12_19, data.string.p12_19a, data.string.p12_19a, ".wrapperIntro.fourth .text5") ;
			
		
			
		}
		
		function fourth07 () {
			
			$board.find(".wrapperIntro.fourth .text6").show(0);
			
		}
		
		
		function fourth08 () {
			
			$board.find(".wrapperIntro.fourth .text7").show(0);
			
		}
		
		
		function fourth09 () {
			
			$board.find(".wrapperIntro.fourth .text8").show(0);
			
		}
		
		
		function fourth10 () {
			
			$board.find(".wrapperIntro.fourth .text9").show(0);
			
		}
		
		
		function fourth11 () {
			
			$board.find(".wrapperIntro.fourth .text10").show(0);
			
		}
		
		
		function fourth12 () {
			
			$board.find(".wrapperIntro.fourth .text11").show(0);
			
		}
		
		function fourth13 () {
			
			$board.find(".wrapperIntro.fourth .text12").show(0);
			
		}
		
		
		function fourth14 () {
			
			$board.find(".wrapperIntro.fourth .text13").show(0);
			
		}
		
		
		
			function fifth () {
			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		
			
		}
		
		
		function fifth01 () {
			
			$board.find(".wrapperIntro.fifth .text1").show(0);
			
		}

		function fifth02 () {
			$board.find(".wrapperIntro.fifth .text2").show(0);
			
			
		}

		function fifth03 () {
			$board.find(".wrapperIntro.fifth .text3").show(0);
			
		}
		
		function fifth04 () {
			$board.find(".wrapperIntro.fifth .solution00").show(0);
			$board.find(".wrapperIntro.fifth .text4").css("background","#efefef");
			
		}
		
		function fifth05 () {
			$board.find(".wrapperIntro.fifth .solution01").show(0);
			
		}
		
		function fifth06 () {
			$board.find(".wrapperIntro.fifth .solution02").show(0);
			
		}

		function fifth07 () {
			$board.find(".wrapperIntro.fifth .solution03").show(0);
			
		}
		
		function fifth08 () {
			$board.find(".wrapperIntro.fifth .solution04").show(0);
			
		}
		
		function fifth09 () {
			$board.find(".wrapperIntro.fifth .solution05").show(0);
			//$board.find(".wrapperIntro.firstPage .solution07").css("display","block");
			
		}
		
		function fifth10 () {
			$nextBtn.hide(0);
			$textbox = $board.find('.inputbox'); 
			$(".inputbox").css({
				"display" : "inline-block"
			});
			$board.find(".text4> .solution06").append($textbox);
			$board.find(".wrapperIntro.fifth .solution06").show(0);
			$board.find(".wrapperIntro.fifth .inputbox").show(0);
			
			$("#inputcheck").change(function(){ 
			var massvalue = $("#inputcheck").val();
				if (massvalue ==78.5) {
					//countNext++;		
					//templateCaller();	
					//$(".tohide").css("opacity","1");
					$(".footerNotification").show(0);
					$("#inputcheck").css("background", "#15C402");
					
					$nextBtn.show(0);
							
				}
				else{
					$("#inputcheck").css("background", "#FF595B");
					$(".wrapperIntro.fifth .text5").show(0);
				}
				
			});
			
					
		}
		
		function sixth () {
			var source = $("#intro6-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[5]
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
		
		};
		
		function sixth01 () {
			
			$board.find(".wrapperIntro.sixth .text1").show(0);
			
		}

		function sixth01 () {
			$board.find(".wrapperIntro.sixth .text2").show(0);
			
			
		}

		function sixth02 () {
			$board.find(".wrapperIntro.sixth .text3").show(0);
			
		}
		
		function sixth03 () {
			$board.find(".wrapperIntro.sixth .solution00").show(0);
			$board.find(".wrapperIntro.sixth .text4").css("background","#efefef");
			
		}
		
		function sixth04 () {
			$board.find(".wrapperIntro.sixth .solution01").show(0);
			
		}
		
		function sixth05 () {
			$board.find(".wrapperIntro.sixth .solution02").show(0);
			
		}

		function sixth06 () {
			$board.find(".wrapperIntro.sixth .solution03").show(0);
			
		}
		
		function sixth07 () {
			$board.find(".wrapperIntro.sixth .solution04").show(0);
			
		}
		
		function sixth08 () {
			$board.find(".wrapperIntro.sixth .solution05").show(0);
			//$board.find(".wrapperIntro.sixth .solution07").css("display","block");
			
		}
		
		function sixth09 () {
			$board.find(".wrapperIntro.sixth .solution06").show(0);
			
			
		}
		
		function sixth10 () {
			$board.find(".wrapperIntro.sixth .solution07").show(0);
			
			
		}
		
		function sixth11 () {
			$board.find(".wrapperIntro.sixth .solution08").show(0);
			
			
		}
		
		function sixth12 () {
			$board.find(".wrapperIntro.sixth .solution09").show(0);
			
			
		}
		
		
		
		
		
function mathParseReplace(totalString, stringtoBRepalce, replaceTxt, selector )
{
	

	var str=totalString;
	var string=stringtoBRepalce;
	var changeText="$${"+replaceTxt+"}$$";

	var res = str.replace(string, changeText);

	$(selector).fadeOut(10,function(){
		$(this).html(res);
		 M.parseMath(document.body);
	}).delay(10).fadeIn(10);
}

/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;
		$nextBtn.show(0);
	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				//first01,
			
				
				
				second, 
				
				second01,
				second02,
				second03,
			
				second04,
				second040,
				
			
				second05,
				second050,
				second051,
				
				
				second07,
				second08,
				second09,
				
				
				
				
				fourth,
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				
				fourth06,
				fourth07,
				fourth08,
				fourth09,
				fourth10,
				fourth11,
				fourth12,
				fourth13,
				fourth14,
				
				fifth,
				fifth01,
				fifth02,
				fifth03,
				fifth04,
				fifth05,
				fifth06,
				fifth07,
				fifth08,
				
				fifth10,
				
				sixth,
				sixth01,
				sixth02,
				sixth03,
				sixth04,
				sixth05,
				sixth06,
				sixth07,
				sixth08,
				sixth09,
				sixth10,
				sixth11,
				sixth12
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var imgpath = $ref+"/image/";


$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=23;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",

			text : [

			data.string.p10_1,
			data.string.p10_2,
			data.string.p10_3,
			data.string.p10_3_0,
			data.string.p10_4


			]


	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			data.string.p10_5,
			data.string.p10_6,
			data.string.p10_7,
			data.string.p10_8,
			data.string.p10_9,
			data.string.p10_10,
			data.string.p10_11

			]

	}


	]




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);

			$board.html(html);
			$nextBtn.show(0);

		};





		function first01 () {

			$board.find(".wrapperIntro.firstPage .text1").show(0);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);


		}


		function first23 () {

			$board.find(".wheel-still").show(0);
			$board.find(".circum").show(0);

			$(".circum").append("<p class='circumferenceTxt'>"+data.string.circtxt+"</p>");

		}


		function first03 () {


			$board.find(".wheel-still").hide(0);



			$board.find(".wheel").show(0);
			$("#reload").attr("src", imgpath+"wheel01.gif?"+new Date().getTime());





				$( ".wheel" ).find(function() {
				$( ".rotate-wheel" ).animate({ "left": "+=320px" }, {
					duration: 3600,


				}  );


			});



		}

		function first30 () {


			$board.find(".wheel").animate({left: "+=200px",duration:"8000"});
		}

		function first04 () {
			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text4").css("background","#efefef");

		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);

		}

		function first06 () {
			$board.find(".wrapperIntro.firstPage .solution02").show(0);

		}

		function first07 () {
			$board.find(".wrapperIntro.firstPage .solution03").show(0);

		}

		function first08 () {
			$board.find(".wrapperIntro.firstPage .solution04").show(0);
			$board.find(".wrapperIntro.firstPage .solution00").hide(0);
			$board.find(".wrapperIntro.firstPage .solution01").hide(0);
			$board.find(".wrapperIntro.firstPage .solution02").hide(0);
			$board.find(".wrapperIntro.firstPage .solution03").hide(0);

		}

		function first09 () {
			$board.find(".wrapperIntro.firstPage .solution05").show(0);

		}

		function first10 () {
			$board.find(".wrapperIntro.firstPage .solution06").show(0);
			$board.find(".wheel").hide(0);
			$board.find(".circum").hide(0);


		}

		function first11 () {
			$board.find(".wrapperIntro.firstPage .solution07").show(0);


		}

		function first12 () {
			$board.find(".wrapperIntro.firstPage .solution08").show(0);


		}

		function first13 () {
			$board.find(".wrapperIntro.firstPage .solution09").show(0);


		}

		function first14 () {
			$board.find(".wrapperIntro.firstPage .solution10").show(0);


		}

		function first15 () {
			$board.find(".wrapperIntro.firstPage .solution11").show(0);


		}

		function first16 () {
			$board.find(".wrapperIntro.firstPage .solution12").show(0);


		}

		function first17 () {
			$board.find(".wrapperIntro.firstPage .solution00").hide(0);
			$board.find(".wrapperIntro.firstPage .solution01").hide(0);
			$board.find(".wrapperIntro.firstPage .solution02").hide(0);
			$board.find(".wrapperIntro.firstPage .solution03").hide(0);
			$board.find(".wrapperIntro.firstPage .solution04").hide(0);
			$board.find(".wrapperIntro.firstPage .solution05").hide(0);
			$board.find(".wrapperIntro.firstPage .solution06").hide(0);
			$board.find(".wrapperIntro.firstPage .solution07").hide(0);
			$board.find(".wrapperIntro.firstPage .solution08").hide(0);
			$board.find(".wrapperIntro.firstPage .solution09").hide(0);
			$board.find(".wrapperIntro.firstPage .solution10").hide(0);
			$board.find(".wrapperIntro.firstPage .solution11").hide(0);
			$board.find(".wrapperIntro.firstPage .solution12").hide(0);

			$board.find(".wrapperIntro.firstPage .solution13").show(0);



		}

		function first18 () {
			$board.find(".wrapperIntro.firstPage .solution14").show(0);


		}

		function first19 () {
			$board.find(".wrapperIntro.firstPage .solution15").show(0);


		}

		function first20 () {
			$board.find(".wrapperIntro.firstPage .solution16").show(0);


		}

		function first21 () {
			$board.find(".wrapperIntro.firstPage .solution17").show(0);


		}

		function first22 () {
			$board.find(".wrapperIntro.firstPage .solution18").show(0);


		}




		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("padding","1%");
			$board.find(".wrapperIntro.second").css("background","#fff");

		};



		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);

		}

		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}


		function second11 () {
			$board.find(".wrapperIntro.second .solution08").show(0);


		}

		function second12 () {
			$board.find(".wrapperIntro.second .solution09").show(0);


		}

		function second13 () {
			$board.find(".wrapperIntro.second .solution10").show(0);
			$board.find(".wrapperIntro.second .solution10").css("display","block");


		}

// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,



				first04,
				first05,
				first06,
				first07,
				first08,
				first09,

				first23,
				first03,


				first10,
				first11,
				first12,
				first13,
				first14,
				first15,
				first16,
				first17,
				first18,
				first19,
				first20,
				first21,
				first22,




				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

var math = {
	expandExponent : function (base,power,type,space) {
		var base = base;
		var power = parseInt(power);
		var space = typeof space === 'undefined' ? '' : space;
		var type = typeof type === 'undefined' ? "string" : type
		var numArray = type=== 'string'? base : [base] ;
		for (var i = 0; i < power-1; i++) {
			if (type=== 'string') {
				numArray+=space+"x"+space+base;
			} else {
				numArray.push("x");
				numArray.push(base);
			}
		};
		return numArray;
	},
	valueExponent : function (base,power) {
		var value = '';
		if (typeof power === 'string') {
			value = base+"x"+base+"x"+base+"x"+"....."+power+" th term";
		} else if (typeof base === 'string') {
			for (var i = 0; i < power; i++) {
				value += base
			};
		} else {
			value = Math.pow(base,power)
		}
		return value;
	},
	expandPower1 : function (base,power,space) {
		var base = base;
		var power = parseInt(power);
		var space = typeof space === 'undefined' ? '' : space;
		var numArray = "<span>"+base+"</span>"+ "<sup>"+1+"</sup>";
		for (var i = 0; i < power-1; i++) {
			numArray+=space+"x"+space+ "<span>"+base+"</span>"+ "<sup>"+1+"</sup>";
		};
		return numArray;
	},
}
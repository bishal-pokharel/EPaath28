/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=17;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		
			text : [
			
			data.string.p17_1,
			data.string.p17_2,
			data.string.p17_3,
			data.string.p17_4,
			data.string.p17_5
			
	
			]
		
		
	}
	
	
	]
	
	
	
	
/******************************************************************************************************/
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
		
		};
		
		function first01 () {
			
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			
		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			
			
		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			
		}
		
		function first04 () {
			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text4").css("background","#efefef");
			
		}
		
		function first05 () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);
			
		}
		
		function first06 () {
			$board.find(".wrapperIntro.firstPage .solution02").show(0);
			
		}

		function first07 () {
			$board.find(".wrapperIntro.firstPage .solution03").show(0);
			
		}
		
		function first08 () {
			$board.find(".wrapperIntro.firstPage .solution04").show(0);
			
		}
		
		function first09 () {
			$board.find(".wrapperIntro.firstPage .solution05").show(0);
			
			
		}
		
		function first10 () {
			$board.find(".wrapperIntro.firstPage .solution06").show(0);
			
			
		}
		
		function first11 () {
			$board.find(".wrapperIntro.firstPage .solution07").show(0);
			
			
		}
		
		function first12 () {
			$board.find(".wrapperIntro.firstPage .solution08").show(0);
			
			
		}
		
		function first13 () {
			$board.find(".wrapperIntro.firstPage .solution00").hide(0);
			$board.find(".wrapperIntro.firstPage .solution01").hide(0);
			$board.find(".wrapperIntro.firstPage .solution02").hide(0);
			$board.find(".wrapperIntro.firstPage .solution03").hide(0);
			$board.find(".wrapperIntro.firstPage .solution04").hide(0);
			$board.find(".wrapperIntro.firstPage .solution05").hide(0);
			$board.find(".wrapperIntro.firstPage .solution06").hide(0);
			$board.find(".wrapperIntro.firstPage .solution07").hide(0);
			$board.find(".wrapperIntro.firstPage .solution08").hide(0);
			
			$board.find(".wrapperIntro.firstPage .text4").css("height","42%");
			
			
			$board.find(".wrapperIntro.firstPage .solution09").show(0);
			
			
		}
		function first14 () {
			$board.find(".wrapperIntro.firstPage .solution10").show(0);
			
			
		}
		function first15 () {
			$board.find(".wrapperIntro.firstPage .solution11").show(0);
			
			
		}
		function first16 () {
			$board.find(".wrapperIntro.firstPage .solution12").show(0);
			
			
		}
		function first17 () {
			$board.find(".wrapperIntro.firstPage .solution13").show(0);
			
			
		}
		
	
	
		

// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,  
				first02, 
				first03, 
				first04, 
				first05, 
				first06, 
				first07, 
				first08, 
				first09, 
				first10,
				first11,
				first12,
				first13,
				first14,
				first15,
				first16,
				first17
				
				
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

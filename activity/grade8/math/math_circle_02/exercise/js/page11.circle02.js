$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : "251200", power : "319", firstlabel : data.string.p11_5, secondlabel : data.string.p11_6,ansque : data.string.p11_2,suffix : 'cm'},
		{base : "282600", power : "1062", firstlabel : data.string.p11_7, secondlabel : data.string.p11_6, ansque : data.string.p11_3, suffix : 'cm'},

	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p11_1,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}

	function first(sub) {
		$('.q1').hide(0);
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p11_9,
			firstans : sub.base,
			secondans : sub.power,
			answer : data.string.p11_10,
			text : sub.ansque,
			suffix : sub.suffix,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			note :data.string.p11_0,
			solution :data.string.p11_16,
			couter : countNext+1,
			check: data.string.check

		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};
	function second(sub) {

		$('.q2').hide(0);
		var source = $("#qaArea1-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p11_9,
			firstans : sub.base,
			answer : data.string.p11_10,
			text : sub.ansque,
			prefix : sub.prefix,
			firstlabel : sub.firstlabel,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};
	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {

		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;

		var base = $board.find(cls+" input.base").val().toLowerCase();

		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var check_empty = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(num == 3){

			if(base === ""){
				swal(data.string.h2);
			}
			else{
				if (base ===upDatas.base) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
					$nextBtn.show(0);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
				// Show check the answer always
				// if (check[0]===1) {
					// if (countNext>=2) {
						// ole.footerNotificationHandler.pageEndSetNotification();
					// } else {
						// $nextBtn.show(0);
					// }
				// } else {
				$board.find(cls+" .clicks .click").show(0);
				// }
			}
		}
		else{
			var power = $board.find(cls+" input.power").val().toLowerCase();
			if(base === ""){
				check_empty[0] = 1;
			}
			else{
				if (base ===upDatas.base) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
					$nextBtn.show(0);
					if (countNext>=$total_page-1) {
						ole.footerNotificationHandler.pageEndSetNotification();
					} 
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
			}

			if(power === ""){
				check_empty[1] = 1;
			}
			else{
				if (power===upDatas.power) {
					check[1]=1;
					$board.find(cls+" .check2").html(right);
				} else {
					$board.find(cls+" .check2").html(wrong);
				}
			}
			if(check_empty[0] ===1 || check_empty[1] === 1){
				swal(data.string.h2);
			}else{
				// if (check[0]===1 && check[1]===1) {
					// if (countNext>=2) {
						// ole.footerNotificationHandler.pageEndSetNotification();
					// } else {
						// $nextBtn.show(0);
					// }
				// } else {
				$board.find(cls+" .clicks .click").show(0);
				// }
			}
		}

	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			if(countNext == 2){
				second(qDatas[countNext]);
			}else{
				first(qDatas[countNext]);
			}
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

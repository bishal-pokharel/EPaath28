$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : "37.68", ansstring : data.string.p20_11, showans : data.string.p20_5, unit :data.string.p20_12, sol :data.string.p3_15, imgs :$ref+'/exercise/images/circle7.png', answer :data.string.p20_16 ,ansque :data.string.p19_15},
		{base : "401.92", ansstring : data.string.p20_10,  showans : data.string.p20_6, unit :data.string.p20_12, sol :data.string.p3_15, imgs :$ref+'/exercise/images/circle8.png', ansque :data.string.p19_15},

	]

/*
* first
*/
	function init () {



		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p20_1,
			note : data.string.p20_0,
			text : data.string.p19_15,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
			//$('.q2').hide(0);
		}

		if(countNext == 2){
			$('.q2').hide(0);
			//$('.q2').hide(0);
		}
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p20_9,
			ans : sub.showans,
			answer : data.string.p20_8,
			text : sub.ansque,
			baseString : sub.ansstring,
			couter : countNext+1,
			suffix : sub.unit,
			solution: sub.sol,
			hint : sub.answer,
			image1 : sub.imgs,
			check: data.string.check

		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
			swal(data.string.h2);
		}else{

			if (base ===upDatas.base) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
				$nextBtn.show(0);
				if (countNext>=$total_page-1) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} 
			} else {
				$board.find(cls+" .check1").html(wrong);
			}


			// Show check the answer always
			// if (check[0]===1) {
				// if (countNext>=2) {
					// ole.footerNotificationHandler.pageEndSetNotification();
				// } else {
					// $nextBtn.show(0);
				// }
			// } else {
			$board.find(cls+" .clicks .click").show(0);
			// }
		}
	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.activityComplete.finishingcall();

		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

Handlebars.registerHelper('equalsto', function(index) {
		console.log(index);
		if (index>0) {
			return "=";
		};
		return "";
	});

(function () {

	var $wahtar=getSubpageMoveButton($lang,"next");

	var $board = $('.board');
	$nextBtn = $("#activity-page-next-btn-enabled"),
	$title = $('.title'),
	animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
	$refImg = $ref+"/exercise/images/";

	$nextBtn.html($wahtar);

	var tortoise = {
		pos : 0,
		initalPos : 5,
		finalPos : 90,
	}

	var question = {
		count : 0,
		range : 5,
		total : 5,
		correct : []
	}
	// var randomVals = ole.getRandom(question.range,question.total-1);
	// console.log(randomVals);
	// $board.html(randomVals)
/*
 _              _   _
| |_ _   _ _ __| |_| | ___
| __| | | | '__| __| |/ _ \
| |_| |_| | |  | |_| |  __/
 \__|\__,_|_|   \__|_|\___|

*/
	function goTurtle (len,nextFunc) {
		// console.log('tortoise.pos= '+tortoise.pos);
		var lastpos = tortoise.pos, i=lastpos;
		// tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(question.count-1)+tortoise.initalPos);
		tortoise.pos = Math.floor((tortoise.finalPos-tortoise.initalPos)/(question.total-1)*(len-1)+tortoise.initalPos);
		var newGo = setInterval(function () {
			// console.log('lastpos = '+lastpos+" newPos="+tortoise.pos+" i="+i);
			if(i%2===0) {
				$('#tortoise2').show(0);
				$('#tortoise1').hide(0);
			} else {
				$('#tortoise1').show(0);
				$('#tortoise2').hide(0);
			}
			$('.tortoise').animate({'left' : i+"%"},100);
			i++;
			if (i>tortoise.pos) {
				clearInterval(newGo);
				// nextFunc();
			};
		},100);
	}

/*
* turtle out
*/

	var content = [
	{
		type:'',
		question : math.parseEquation("If 10 workers, working for 4 hours complete the work in 12 days, in how many days will 8 workers working for 6 hours complete the same work?"),
		answers: [
		{ans : math.parseEquation("10")},
		{ans : math.parseEquation("40")},
		{ans : math.parseEquation("20"),
		correct : "correct"},
		{ans : math.parseEquation("30")}
		],
		
	},{
		type:'',
		question : math.parseEquation("A contractor is assigned to complete a task in 40 days. He employs 20 men to do the work and they manage 1/3 of the work in 30 days. How many more should be added to complete the work in time?"),
		answers: [
		{ans : math.parseEquation("45")},
		{ans : math.parseEquation("60"),
		correct : "correct"},
		{ans : math.parseEquation("20")},
		{ans : math.parseEquation("25")}
		],
		
	},{
		type:'',
		question : math.parseEquation("30 men can finish a work in 100 days. 10 men were added , how many days would it take to complete the same  work ?"),
		answers: [
		{ans : "300"},
		{ans : "150",
		correct : "correct"},
		{ans : math.parseEquation("100")},
		{ans : "75"}
		],
		
	},{
		type:'',
		question : "If y = 3, find the value of : "+ math.parseEquation("If 5 men working 9 hours a day dig a trench 3 days, how many hours a day must 3men work in order to dig a trench  in 15 days?"),
		answers: [
		{ans : math.parseEquation("3 hours")},
		{ans : math.parseEquation("6 hours"),
		correct : "correct"},
		{ans : math.parseEquation("9 hours")},
		{ans : math.parseEquation("2 hours")}
		],
		
	}, {
		type:'',
		question : "If y = 3, find the value of : "+ math.parseEquation("Pipe A can fill .5 of a cistern in 1 hour and pipe B can fill 1/3 of a cistern in 2 hour, how long (A+B) take to fill entire cistern?"),
		answers: [
		{ans : math.parseEquation("1.5 hours")},
		{ans : math.parseEquation("2 hours"),
		correct : "correct"},
		{ans : math.parseEquation("3 hours")},
		{ans : math.parseEquation("5 hours")}
		],
		
	},
	]

	function  qA () {
		$title.text(data.string.exerciseTitle_3).show(0);
		$('.advancement').show(0);
		var counter = question.count;

		loadTimelineProgress(question.total,question.count+1);
		content[counter].correctImg = $refImg+"correct.png";
		content[counter].wrongImg = $refImg+"wrong.png";
		content[counter].question =ole.textSR(content[counter].question,'sup2','<sup>2</sup>');
			if (content[counter].type==='multipleImg') {
				// alert();
				var source = $('#qAImage-templete').html();
			} else {
				var source = $('#qA-templete').html();
			}

			var template = Handlebars.compile(source);
			var html = template(content[counter]);
			$board.html(html);
			loadSolutionHint(counter);
	}

	qA ()

	function loadSolutionHint (num) {
		var source = $('#hintSolution-template').html();
		var template = Handlebars.compile(source);
		var html = template(content[num]);
		$(".hintBox").html(html);
	}

/*
* results
*/
	function results () {
		$title.hide(0);
		var len = question.correct.length;
		if(len < question.total) {
			$('.tortoise').animate({

			},600,function () {
				$(this).html("<img src='"+$refImg+"laydown.png' >")
				$(this).css({
					'width' : '20%',
					'bottom' : "-10%",
				})
			})

			$('#flag').css({
				'width' : '15%',
			},1000);
			if(len === 9){
				var endContent = {
					story : data.string.missedOne
				}
			} else if(len === 0){
				var endContent = {
					story : data.string.noTry
				}
			} else if(len <5){
				var endContent = {
					story : data.string.needMore
				}
			} else if(len <9){
				var endContent = {
					story : data.string.someMore
				}
			}
		} else {
			$('.tortoise').html("<img src='"+$refImg+"dance.gif' >").animate({
				'left' : '80%',
				'width' : '15%',
			},1000);
			$('#flag').animate({
				'right' : '35%',
				'width' : '15%',
			},1000);
			var endContent = {
				story : data.string.successStory
			}
		}

		var source = $('#results-template').html();
		var template = Handlebars.compile(source);
		var html = template(endContent);
		$board.html(html);
		$board.find('.endBoard').addClass('animated bounceInDown').one(animationEnd,function () {
			$('.repeat').show(0).addClass('animated bounceInDown');
		});

	}

	$board.on('click','.neutral',function () {
		// console.log("what");
		var $this = $(this);
		var isCorrect = $this.data('correct');
		if(isCorrect=== "correct") {
			$this.addClass('right');
			$('.neutral').removeClass('neutral');
			$nextBtn.fadeIn();
			$('.imgCrot .correctImg').show(0);
			question.correct.push(question.count);
			// console.log(question.correct);
			var len = question.correct.length;
			goTurtle (len);
		}
		else {
			$this.addClass('wrong')
			$('.neutral').not('.correct').addClass('wrong').removeClass('neutral');
			$('.correct').addClass('right animated  tada');
			$('.neutral').removeClass('neutral');
			$('.imgCrot .wrongImg').show(0);
			$nextBtn.fadeIn();

		}
	})

	$(".rhino").on('click',function () {
		$(".hintBox").show(0);
	})

	$nextBtn.on('click',function () {
		$(".hintBox").hide(0);
		$nextBtn.fadeOut();
		question.count++;

		if(question.count<question.total){
			// goTurtle (qA);
			qA();
		}
		else if (question.count===question.total) {
			// goTurtle(results);
			// alert('finish');
			// $nextBtn.fadeOut();
			results();
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// ole.activityComplete.finishingcall();
		}
	})

	$board.on('click','.storyWrapper .enter',qA);

})(jQuery)


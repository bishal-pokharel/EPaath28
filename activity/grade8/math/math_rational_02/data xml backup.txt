<?xml version="1.0" encoding="utf-8"?>

<olenepal>

<lesson>
<subject>exponential</subject>
    <topic>science</topic>
    <chapter>Rational and Irrational</chapter>
    <levelname>Level II</levelname>
    <leveldescription>exponential</leveldescription>

    <objective>
        <title>उदेश्य : यस पाठमा तिमीहरूले निम्न विषय वस्तुबारे जान्नेछौ।</title>
        <description>
           
			<purpose><![CDATA[To distinguish Natural Numbers & Whole Numbers, Integers & Real Numbers]]></purpose>
			<purpose><![CDATA[To use scientific notations and identify rational and irrational numbers]]></purpose>
			
			
        </description>
    </objective>

    <definition>
        <description>
            <statement></statement>
        </description>
    </definition>

</lesson>

<strings>
    <string id="example">Example</string>
    <string id="indeterminate">indeterminate</string>
    <string id="yourTurn">Your Turn</string>
    <string id="clickToCheckAns">Click to check the answer</string>
	
	
	
	<!--page1-->
	<string id="p1_1">Today’s Topic </string>
	<string id="p1_2"><![CDATA[Rational & Irrational Numbers]]></string>
	
	<!--slide2-->
	<string id="p1_3">Fun Fact</string>
	<string id="p1_4">Let’s start the chapter with a fun fact.</string>
	<string id="p1_5"><![CDATA[Hippasus (one of Pythagoras' students) discovered irrational numbers when trying to write the square root of 2 as a fraction (using geometry, it is thought). Instead he proved you couldn't write the square root of 2 as a fraction and so it was irrational.]]></string>
	<string id="p1_6"><![CDATA[But followers of Pythagoras could not accept the existence of irrational numbers, and it is said that Hippasus was drowned at sea as a punishment from the gods!]]></string>
	<string id="p1_7"><![CDATA[So, what makes a number Rational or Irrational ?]]></string>

	
	

	
<!--page2-->

	
    <string id="p2_1"><![CDATA[Rational & Irrational]]></string>
    <string id="p2_2"><![CDATA[Rational Numbers are those numbers which can be	written as ratio or fraction of two integers. ]]></string>
    
	<string id="p2_3"><![CDATA[Example 1]]></string>
    <string id="p2_4"><![CDATA[<div class="solution00">1.5 is rational,</div><br/><div class="solution01">because it can be written as 
	
	<span style="display: inline;">
	<fmath alttext="{4/2}" class="fm-inline">
	<span mtagname="mfrac" style="vertical-align: 0em;"><span class="fm-vert fm-frac">
	<table>
	<tbody>
	<tr>
	<td class="fm-num-frac fm-inline">
	<mn>3</mn>
	</td>
	</tr><tr>
	<td class="fm-den-frac fm-inline">
	<mn>2</mn>
	</td></tr></tbody></table></span></span></fmath></span>
	
	
	</div>]]></string>
	<string id="p2_5"><![CDATA[because it can be written as ]]></string>
	<string id="p2_6"><![CDATA[Example 2]]></string>
	<string id="p2_7"><![CDATA[<div class="solution02">5 is rational,</div></br><div class="solution03">because it can be written as 
	<span class="sim-frac" style="display: inline;">
							<math alttext="{30/10}">
							<mfrac>
							<mn>5</mn>
							<mn>1</mn>
							</mfrac>
							</math>
							</span>
	
	</div>]]></string>
	<string id="p2_8"><![CDATA[because it can be written as 5/1]]></string>
	<string id="p2_9"><![CDATA[Example 3]]></string>
	<string id="p2_10"><![CDATA[<div class="solution04">0.666666 .... is rational, Why?</div><br/><div class="solution05">Because it can be written as 
	
	<span class="sim-frac" style="display: inline;">
							<math alttext="{30/10}">
							<mfrac>
							<mn>2</mn>
							<mn>3</mn>
							</mfrac>
							</math>
							</span>
	
	
	</div>]]></string>
	<string id="p2_11"><![CDATA[Because it can be written as 2/3]]></string>
	<string id="p2_12"><![CDATA[<span class="red">Caution:</span> The denominator cannot equal to zero.]]></string>
	
	
	<!--slide3-->
	<string id="p2_13"><![CDATA[Rational & Irrational Number]]></string>
	<string id="p2_14"><![CDATA[So, what is Irrational number?]]></string>
	<string id="p2_15"><![CDATA[Definition:]]></string>
	<string id="p2_16"><![CDATA[Irrational numbers are those numbers which cannot be written as ratio or fraction of two integers.]]></string>
	<string id="p2_17"><![CDATA[Example 1]]></string>
	<string id="p2_18"><![CDATA[<div class="solution05">π is irrational, why?<br/></div>
								<div class="solution06">Because it cannot be written as fraction
											<span class="sim-frac01" style="display: inline;">
											<math alttext="{30/10}">
											<mfrac>
											<mn>?</mn>
											<mn>?</mn>
											</mfrac>
											</math>
											</span>
											<br/>
								</div>
								<div class="solution07">π =3.1415926535897932384626433832795 (and more).<br/></div>
								<div class="solution08">The popular approximation of π = 
<span class="sim-frac" style="display: inline;">
							<math alttext="{30/10}">
							<mfrac>
							<mn>22</mn>
							<mn>7</mn>
							</mfrac>
							</math>
							</span>
								=3.1428571428571 (and more) is closed but not accurate (as you can see from above).<br/></div>
							
	
	
	]]></string>
	<string id="p2_19"><![CDATA[Because it cannot be written as fraction
	
	<span class="sim-frac01" style="display: inline;">
		<math alttext="{30/10}">
		<mfrac>
		<mn>?</mn>
		<mn>?</mn>
		</mfrac>
		</math>
		</span>
	
	
	
	]]></string>
	<string id="p2_20"><![CDATA[π =3.1415926535897932384626433832795 (and more).]]></string>
	<string id="p2_21"><![CDATA[The popular approximation of π = 22/7 =3.1428571428571 (and more) is closed but not accurate (as you can see from above). ]]></string>
	<string id="p2_22"><![CDATA[Example 2]]></string>
	
	<string id="p2_23"><![CDATA[<div class="solution09">Many square root & cube roots are also irrational number.<br/></div>
								<div class="solution10">√3 = 1.7320508075688772935274463415059 (and more)<br/></div>
								<div class="solution11">3√ 5 = 1.70997594668 (and more)<br/></div>
								<div class="solution12">√4 = 2 <span class="blue">(rational)</span>, √9 = 3 <span class="blue">(rational)</span> & 3√ 5 = 1.70997594668 <span class="blue">(irrational)</span></div>
								
				]]>
	
	</string>
						
	
	
	<string id="p2_24"><![CDATA[So, all roots are not irrational.]]></string>
	<string id="p2_25"><![CDATA[3√ 5 = 1.70997594668(and more)]]></string>
	<string id="p2_26"><![CDATA[√4 = 2 <span class="blue">(rational)</span>, √9 = 3 <span class="blue">(rational)</span> & 3√ 5 = 3 <span class="blue">(rational)</span>]]></string>
	<string id="p2_27"><![CDATA[So, all roots are not irrational.]]></string>
	
   
   
	
	
	
	
	
	
	<!--Sangina page3 YOUR TURN-->
	<string id="p3_1"><![CDATA[Identify whether the following numbers  are rational or irrational. ]]></string>
	<string id="p3_2"><![CDATA[Is 0.75 rational or irrational? Click one of the following two. ]]></string>
	<string id="p3_3"><![CDATA[Rational]]></string>
	<string id="p3_4"><![CDATA[Irrational]]></string>
	<string id="p3_5"><![CDATA[Answer :]]></string>
	<string id="p3_6"><![CDATA[Rational 0.75 = 3/4]]></string>
	<string id="p3_7"><![CDATA[Is 0.005 rational or irrational? Click one of the following two.]]></string>
	<string id="p3_8"><![CDATA[Rational ( 0.005 = 1/200 )]]></string>
	<string id="p3_9"><![CDATA[Is √7 rational or irrational? Click one of the following two.]]></string>
	<string id="p3_10"><![CDATA[Irrational (√7 = 2.64575131106..). Irrational as 2.645755131106 cannot be written as fraction.)]]></string>
	<string id="p3_11"><![CDATA[Click to check the answer]]></string>
	<string id="p3_12"><![CDATA[Time (T)]]></string>
	<string id="p3_13"><![CDATA[Click to check the answer]]></string>
	<string id="p3_14"><![CDATA[Answers]]></string>
	<string id="p3_15"><![CDATA[Principal ( P ) = Rs 2,000 , Interest Rate (R ) =  4 % , Time  =  3 Year]]></string>
	<string id="p3_16"><![CDATA[For a period of 12 years, Parvati borrowed Rs 3,200 at 8 % per year. Find Principal, Interest Rate & Time.]]></string>
	<string id="p3_17"><![CDATA[Principal (P)]]></string>           
	<string id="p3_18"><![CDATA[Interest Rate (R)]]></string>
	<string id="p3_19"><![CDATA[Time (T)]]></string>	 
	<string id="p3_20"><![CDATA[Click to check the answer ]]></string>
	<string id="p3_21"><![CDATA[Answers ]]></string>
	<string id="p3_22"><![CDATA[Principal ( P ) = Rs 3,200 , Interest Rate (R ) =  8 % , Time  =  12 Year ]]></string><!--Sangina-->
	
	
	

	
	<string id="p4_0"><![CDATA[Characteristics of Rational Number]]></string>
	<string id="p4_1"><![CDATA[Do we always have to change a number to fraction or ratio form to classify a number rational or irrational?]]></string>
	<string id="p4_2"><![CDATA[Of course not! Rational number has two features which are as follows]]></string>
	<string id="p4_3"><![CDATA[1) Rational numbers have terminating decimal.]]></string>
	<string id="p4_4"><![CDATA[2) Rational number may have non terminating but recurring decimal.]]></string>
	<string id="p4_5"><![CDATA[Let's look at these two features.]]></string>
	
	
	<string id="p4_6_1"><![CDATA[Characteristics of rational number]]></string>
	<string id="p4_6"><![CDATA[1) Rational numbers have terminating decimal.]]></string>
	<string id="p4_7"><![CDATA[Example 1]]></string>
	<string id="p4_8"><![CDATA[
	
	<span style="display: inline;">
	<fmath alttext="{4/2}" class="fm-inline">
	<span mtagname="mfrac" style="vertical-align: 0em;"><span class="fm-vert fm-frac">
	<table>
	<tbody>
	<tr>
	<td class="fm-num-frac fm-inline">
	<mn>1</mn>
	</td>
	</tr><tr>
	<td class="fm-den-frac fm-inline">
	<mn>5</mn>
	</td></tr></tbody></table></span></span></fmath></span>= 0.2
	
	
	]]></string>
	<string id="p4_9"><![CDATA[Example 2]]></string>
	<string id="p4_10"><![CDATA[
	
	<span style="display: inline;">
	<fmath alttext="{4/2}" class="fm-inline">
	<span mtagname="mfrac" style="vertical-align: 0em;"><span class="fm-vert fm-frac">
	<table>
	<tbody>
	<tr>
	<td class="fm-num-frac fm-inline">
	<mn>5</mn>
	</td>
	</tr><tr>
	<td class="fm-den-frac fm-inline">
	<mn>8</mn>
	</td></tr></tbody></table></span></span></fmath></span>= 0.625
	
	
	
	]]></string>
	
	
	
	
	
	<string id="p4_11"><![CDATA[2) Rational numbers have non terminating but recurring decimal.]]></string>
	<string id="p4_12"><![CDATA[Example 1]]></string>
	<string id="p4_13"><![CDATA[
	
	
	<span style="display: inline;">
	<fmath alttext="{4/2}" class="fm-inline">
	<span mtagname="mfrac" style="vertical-align: 0em;"><span class="fm-vert fm-frac">
	<table>
	<tbody>
	<tr>
	<td class="fm-num-frac fm-inline">
	<mn>1</mn>
	</td>
	</tr><tr>
	<td class="fm-den-frac fm-inline">
	<mn>3</mn>
	</td></tr></tbody></table></span></span></fmath></span>= 0.3333 (repeating decimal)
	
	
	
	]]></string>
	<string id="p4_14"><![CDATA[Example 2]]></string>
	<string id="p4_15"><![CDATA[
	
	
	<span style="display: inline;">
	<fmath alttext="{4/2}" class="fm-inline">
	<span mtagname="mfrac" style="vertical-align: 0em;"><span class="fm-vert fm-frac">
	<table>
	<tbody>
	<tr>
	<td class="fm-num-frac fm-inline">
	<mn>4</mn>
	</td>
	</tr><tr>
	<td class="fm-den-frac fm-inline">
	<mn>11</mn>
	</td></tr></tbody></table></span></span></fmath></span>= 0.363636 (repeating decimal) 
	
	
	]]></string>
	
	<string id="p4_16"><![CDATA[Non-terminating recurring decimals are indicated by putting dots just over the beginning and end of repeated digits or block of digits.]]></string>

	

	
	<!--Sangina page 5 yourn turn-->
	<string id="p5_1"><![CDATA[Identify whether the following numbers  are Terminating or Non-Terminating but recurring rational number.]]></string>
	<string id="p5_2"><![CDATA[Is 0.55 Terminating or Non-Terminating but Recurring? Click one of the following two.]]></string>
	<string id="p5_3"><![CDATA[Is 0.1414 Terminating or Non-Terminating but Recurring? Click one of the following two.]]></string>
	<string id="p5_4"><![CDATA[Is 7.3656565... Terminating or Non-Terminating but Recurring? Click one of the following two.]]></string>
	<string id="p5_5"><![CDATA[Click to check the answer]]></string>
	<string id="p5_6"><![CDATA[Answers :]]></string>
	<string id="p5_7"><![CDATA[Terminating]]></string>
	<string id="p5_8"><![CDATA[Non-Terminating but recurring (14 recurring)]]></string>
	<string id="p5_9"><![CDATA[Non-Terminating but recurring (Recurring = 65)]]></string>
	<string id="p5_10"><![CDATA[Rs 3,072 ]]></string>
	   
	
	
		
	
	
	<!--page6 slide 1-->
	<string id="p6_1"><![CDATA[Characteristics of Irrational Number]]></string>
	<string id="p6_2"><![CDATA[Now that I know characteristics of rational number.<br/> What are the characteristics of irrational number?]]></string>
	<string id="p6_3"><![CDATA[Irrational numbers are non terminating & non recurring.]]></string>
	<string id="p6_4"><![CDATA[In simple words, irrational numbers do not have terminating decimal. Also, irrational numbers don’t have recurring decimal like that of rational number. So, after decimal irrational numbers have random numbers without any fixed pattern.]]></string>
	<string id="p6_5"><![CDATA[Let's look at an example.]]></string>
	<string id="p6_6"><![CDATA[Example 1 : √2 = 1.414353562..... (and more)]]></string>
	<string id="p6_7"><![CDATA[Here, when √2 expressed into decimal it is non terminating and after decimal it doesn't also have recurring numbers. Hence, √2 is irrational.]]></string>
	<string id="p6_8"><![CDATA[If a number is not rational than it is an irrational number.]]></string>
	
	<!--page6 slide 2-->
	<string id="p6_9_1">Relationship between different sets of numbers.</string>
	<string id="p6_9"><![CDATA[ Now that you are familiar with rational & irrational numbers.<br/> Let’s see which sets of number are rational  & which sets of number are irrational. Firstly , let’s look at different sets of number.]]></string>
	<string id="p6_10"><![CDATA[1) Natural Numbers (N):<br/>Natural numbers (also known as counting numbers) are the numbers that we use to count. It starts with 1, followed by 2, then 3, and so on. <br/>In set notation: Natural Numbers (N) = { 1, 2 3, ……….}]]></string>
	<string id="p6_11"><![CDATA[2) Whole Numbers (W):<br/>Whole numbers are a slight "upgrade" of the natural numbers because we simply add the element "zero" to the current set of natural numbers. Think of whole numbers as natural numbers together with zero. <br/>In set notation: Whole Numbers (W) = { 0,1, 2 3, ……….}]]></string>
	<string id="p6_12"><![CDATA[3) Integers (Z) :<br/>The integers include all whole numbers together with the "negatives" of the natural numbers. <br/>In set notation: Integers (W) = { …….,-3,-2,-1,0,1, 2 3, ……….}]]></string>
	<string id="p6_13"><![CDATA[4) Rational Numbers :<br/>Includes natural numbers, whole numbers & integers.]]></string>
	<string id="p6_14"><![CDATA[5) Irrational Numbers :<br/>All numbers except for rational numbers. ]]></string>
	<string id="p6_15"><![CDATA[6) Real Numbers :<br/>The real numbers includes both the rational and irrational numbers. Remember that under the rational number, we have the subcategories of integers, whole numbers and natural numbers.]]></string>
	
	
	<!--page6 slide 3-->
	
	<string id="p6_16"><![CDATA[Diagrammatic relationship between different sets Of numbers]]></string>
	<string id="p6_17"><![CDATA[Let’s look at the relationship between these numbers in a funnel diagram.]]></string>
	
	
	
	
	
	
	<!--end of page6-->
	
	
	<!--Sangina-->
	<string id="p7_1"><![CDATA[Drag the numbers to correcrt boxes]]></string>
	<string id="p7_2"><![CDATA[Ram borrowed Rs 1,500 from Sita for a period of 2 months  at 10 % per year. Find interest paid by Ram.]]></string>
	<string id="p7_3"><![CDATA[Interest ( I )]]></string>
	<string id="p7_4"><![CDATA[Click to check the answer ]]></string>
	<string id="p7_5"><![CDATA[Answers]]></string>
	<string id="p7_6"><![CDATA[Rs 25]]></string>
	<string id="p7_7"><![CDATA[At the rate of 8 % per year, Sapana borrowed Rs 2,700 for a period of 4 months. Find Interest paid by Sapana.]]></string>
	<string id="p7_8"><![CDATA[Rs 72]]></string>
	<string id="p7_9"><![CDATA[For a period of 8 months, Parvati borrowed Rs 4,500 at 9 % per year. Find Interest paid by Parvati]]></string>
	<string id="p7_10"><![CDATA[Rs 270]]></string><!--Sangina-->

	
	
	
	<!--page8 slide1-->
	<string id="p8_1">Simple Interest : Amount </string>
	<string id="p8_2">So, at the end when it’s time to pay back the money. I just pay the Interest</string>
	<string id="p8_3"><![CDATA[NO! When its time to pay back the money, you pay Principal plus amount of Interest that has accumulated. The total sum that you pay at the end is called <b>amount</b>.]]></string>
	<string id="p8_4">In Formula, </string>
	<string id="p8_5"><![CDATA[<span class="green">Amount = Principal + Interest </span>]]> </string>
	<string id="p8_6">Let's solve problem together and use the above formula.</string>
	
	
	<!--page8 slide2-->
	<string id="p8_7">Simple Interest: Amount Example</string>
	<string id="p8_8">Example</string>
	<string id="p8_9">Rasna borrowed Rs. 2,000 from Nabil Bank . Find the Amount if Rasna paid interest 	of Rs 500. </string>
	<string id="p8_10">Solution:</string>
	<string id="p8_11"><![CDATA[
	
	
	<span class="solution00">Given,<br/></span>
								<span class="solution01">Principal (P) = Rs. 2,000<br/></span>
								<span class="solution02">Interest (I) = Rs. 500<br/></span>
								<span class="solution03">Now, using formula<br/></span>
								<span class="solution04">Amount = Principal+ Interest<br/></span>
								<span class="solution05"> = 2000 + 500 <br/></span>
								<span class="solution06"> = Rs. 2,500<br/></span>
								<span class="solution07"> ∴ Amount paid by Rasna is Rs. 2,500 <br/></span>
								<span class="solution08 note">You can also use Amount formula to find Principal& Interest.</span>
	
	
	]]>
	</string>
	<string id="p8_12"><![CDATA[Principal ( P ) = Rs 2,000]]></string>
	<string id="p8_13">Interest ( I ) = Rs 500</string>
	<string id="p8_14">Now, Using Formula</string>
	<string id="p8_15">Amount = Principal+ Interest</string>
	<string id="p8_16"> = 2000 + 500 </string>
	<string id="p8_17"> = Rs 2,500</string>
	<string id="p8_18"><![CDATA[∴ Amount paid by Rasna is Rs 2,500 .]]></string>
	<string id="p8_19"><![CDATA[*Note: You can also use Amount formula to find Principal& Interest.]]></string>
	
	
	<!--Sangina-->
	<string id="p9_1"><![CDATA[Find Amount, Principal & Interest.]]></string>
	<string id="p9_2"><![CDATA[Ram borrowed Rs 1,500 from Sita for a period of 2 months  at 10 % per year. Find interest paid and total amount paid by Ram.]]></string>
	<string id="p9_3"><![CDATA[Interest ( I )]]></string>
	<string id="p9_4"><![CDATA[Amount ( A )]]></string>
	<string id="p9_5"><![CDATA[Click to check the answer]]></string>
	<string id="p9_6"><![CDATA[Answers]]></string>
	<string id="p9_7"><![CDATA[Rs 25]]></string>
	<string id="p9_8"><![CDATA[Rs 1,525]]></string>
	<string id="p9_9"><![CDATA[At the rate of 8 % per year, Sapana borrowed Rs 2,700 for a period of 4 months. Find Interest paid by Sapana.]]></string>
	<string id="p9_10"><![CDATA[Rs 72]]></string>
	<string id="p9_11"><![CDATA[Rs 2,772]]></string>
	<string id="p9_12"><![CDATA[Sita paid  an amount of Rs 5000 to Hari. If she paid an interest of Rs 500. Find the amount borrowed by her.]]></string>
	<string id="p9_13"><![CDATA[Principal ( P )]]></string>
	<string id="p9_14"><![CDATA[Rs 4,500]]></string><!--Sangina-->
	
	

	
	<string id="p10_1"><![CDATA[Simple Interest : Amount Formula Expanded]]></string>
	<string id="p10_2"><![CDATA[Let's Expand Amount Formula]]></string>
	<string id="p10_3">
	
	<![CDATA[
	
	
	<span class="solution00">Amount (A) = Principal (P) + Interest (I)<br/></span>
								<span class="solution01">or, A = P + I <br/></span>
								<span class="solution02">or, A = P +
								
									<span class="sim-frac01" style="display: inline;">
									<math alttext="{30/10}">
									<mfrac>
									<mn>P X T X R</mn>
									<mn>100</mn>
									</mfrac>
									</math>
									</span>
								
								</span>
								
								<span class="solution03">[Replacing value of I =
								
									<span class="sim-frac01" style="display: inline;">
									<math alttext="{30/10}">
									<mfrac>
									<mn>P X T X R</mn>
									<mn>100</mn>
									</mfrac>
									</math>
									]
									</span>
								<br/>
								</span>
								
								
								<span class="solution04">or, A =
								
									<span class="sim-frac01" style="display: inline;">
									<math alttext="{30/10}">
									<mfrac>
									<mn>100P + PTR</mn>
									<mn>100</mn>
									</mfrac>
									</math>
									</span>
								<br/>
								</span>
								
															
								
								<span class="solution05">or, 100 A = P ( 100 + TR)<br/></span>
								<span class="solution06">∴ P =
								
									<span class="sim-frac01" style="display: inline;">
									<math alttext="{30/10}">
									<mfrac>
									<mn>A X 100</mn>
									<mn>100 + TR</mn>
									</mfrac>
									</math>
									</span>
								<br/>
								</span>
								
																
								<span class="solution07 note">You can use the above formula to find Principal (P), Amount (A), Time (T) & Rate (R)<br/></span>
								<span class="solution08">Let's solve a problem together and use the above formula.</span>
	
	
	]]>
	
	
	</string>
	<string id="p10_4"><![CDATA[or, A = P+I]]></string>
	<string id="p10_5"><![CDATA[or, A = P + (P X T X R)/100 <span class="green">(Replacing value of I = ( P X T X R)/100)</span>]]></string>
	<string id="p10_6"><![CDATA[or, A = (100P + PTR)/100]]></string>
	<string id="p10_7"><![CDATA[or, 100 A = P (100 = TR)]]></string>
	<string id="p10_8"><![CDATA[∴ P = (A = 100)/100 + TR]]> </string>
	<string id="p10_9"><![CDATA[You can use the above formula to find Principal (P), Amount (A), Time (T) & Rate (R)]]></string>
	<string id="p10_10"><![CDATA[Let's solve a problem together and use the above formula.]]></string>
	
	
	<string id="p10_11">Simple Interest:Example</string>
	<string id="p10_12">Example 4 </string>
	<string id="p10_13"><![CDATA[Hari borrowed some money form Ram at the rate of 8% per year. After 5 years Hari returned Rs. 11,200 to Ram and cleared the debt. What sum had Hari initially borrowed ?]]></string>
	<string id="p10_14">Solution :</string>
	<string id="p10_15"><![CDATA[
	
	
	<span class="solution00">Given,<br/></span>
								<span class="solution01">Principal (P) = ?<br/></span>
								<span class="solution02">Rate of interest (R) = 8%</span>
								<span class="solution03">Time (T) = 5 Years</span>
								<span class="solution04">Amount (A) = Rs. 11,200<br/></span>
								<span class="solution05">Now, using formula<br/></span>
								<span class="solution06">P =
								
									<span class="sim-frac01" style="display: inline;">
										<math alttext="{30/10}">
										<mfrac>
										<mn>A X 100</mn>
										<mn>100 + TR</mn>
										</mfrac>
										</math>
									</span>
									<br/>
								</span>
								
								<span class="solution07"> =
								
									<span class="sim-frac01" style="display: inline;">
										<math alttext="{30/10}">
										<mfrac>
										<mn>11200 X 100</mn>
										<mn>100 + 5 X 8</mn>
										</mfrac>
										</math>
									</span>
									<br/>
								</span>
								
								<span class="solution08"> =
								
									<span class="sim-frac01" style="display: inline;">
										<math alttext="{30/10}">
										<mfrac>
										<mn>1120000</mn>
										<mn>140</mn>
										</mfrac>
										</math>
									</span>
									<br/>
								</span>
								
								<span class="solution09"> = Rs. 8,000<br/></span>
								<span class="solution10 note"> ∴ Hari initially Borrowed Rs. 8,000 from Ram.</span>
	
	
	]]>
	
	
	
	
	
	</string>
	<string id="p10_16">Principal (P) = ?</string>
	<string id="p10_17">Rate of interest (R) = 8%</string>
	<string id="p10_18">Time (T) = 5 Years</string>
	<string id="p10_19">Amount (A) = Rs 11,200</string>
	<string id="p10_20">Now, Using Formula</string>
	<string id="p10_21">P = (A X 100)/100 + TR</string>
	<string id="p10_22">= (11200 X 100)/100 + 5 X 8</string>
	<string id="p10_23">= 1120000/140</string>
	<string id="p10_24"> = Rs 8,000</string>
	<string id="p10_25">∴ Hari initially Borrowed Rs 8,000 from Ram</string>
	
	<string id="p10_26">16days</string>
	

	
	<!--bora exercise-->
	
	<string id="metal">Rational</string>
	<string id="nonmetal">Irrational</string>
	
	<string id="p11_intro">Are you ready to identify rational and irrational numbers?</string>
	<string id="p11_1">7/8</string>
	<string id="p11_2">1/3</string>
	<string id="p11_3">2√3</string>
	<string id="p11_4">0.97</string>
	<string id="p11_5">√1.6</string>
	<string id="p11_6">-√8</string>
	<string id="p11_7">0.3030030003...</string>
	<string id="p11_8">√9 </string>
	<string id="p11_9">π </string>
	<string id="p11_10">8/2 </string>
	<string id="p11_11">0 </string>
	<string id="p11_12">-√11 </string>
	<string id="p11_13">√2/5</string>
	<string id="p11_14">√16/9</string>
	<string id="p11_15">-3</string>
	
	<string id="p11_finishMsg">Congratulations! You played well.</string>
	<string id="p11_info">Identify between rational and irrational numbers and click on the correct sack.</string>
	
	
	
	
	
	
	

	
	
	
	<!-- exercise -->
    <!-- exercise title n all -->
	<string id="ins">Is the statement True or False?</string>
    <string id ="story">Hello, I am OLE rhino. I am taking part in running competition. Help me to win this competition. You have to answer the questions that are asked, correctly. Only then, I will get to run. I will only win if you answer all the questions correctly.</string>
    <string id ="press">Come Let's play.</string>
    <string id ="exerciseTitle">RULE 1: EXERCISES : click on the correct answer</string>
    <string id ="successStory">Hurray! We won.</string>
    <string id ="missedOne">Its okay, we were playing good.</string>
    <string id ="noTry">Why didn't you give me chance to run?</string>
    <string id ="needMore">Our practice is not enough, do you understand?</string>
    <string id ="someMore">We have to practice a lot, then victory is ours.</string>

    <string id ="exerciseTitle_2">RULE 2: EXERCISES : click on the correct answer</string>

    <string id ="exerciseTitle_3">RULE 3: EXERCISES : click on the correct answer</string>

    <string id ="exerciseTitle_4">RULE 4: EXERCISES : click on the correct answer</string>

    <string id ="exerciseTitle_5">COMBINED EXERCISES – VERY DIFFICULT</string>

	
	
	
	
	
<!-- exercise 1-->	
	<string id ="e1scoretextdata">Score:</string>
	<string id ="e1totalproblemtextdata">Total Problems:</string>

	<string id ="e1q1">A natural number is also a whole number.</string>
	<string id ="e1q1o1">True</string>
	<string id ="e1q1o2">False</string>


	<string id ="e1q2">Any integer is a whole number.</string>
	<string id ="e1q3">Every rational number is also an integer.</string>
	<string id ="e1q4">Every integer is a rational number.</string>
	<string id ="e1q5">Every natural number is a whole number, integer, and a rational number. </string>
	<string id ="e1q6">Every whole number is a natural number, integer, and a rational number. </string>
	
	
	<string id ="ans1">The set of whole numbers includes the number zero and all natural numbers. This is a true statement.</string>
	<string id ="ans2">The set of integers is composed of the number zero, natural numbers, and the "negatives" of natural numbers.  That means some integers are whole numbers, but not all. For instance, −2 is an integer but not a whole number. This statement is false.</string>
	<string id ="ans3">The word "every" means "all". Can you think of a rational number that is not an integer? You only need one counter example to show that this statement is false. The fraction is an example of a rational number but not an integer. So this statement is false.</string>
	<string id ="ans4">This is true because every integer can be written as a fraction with a denominator of 1.</string>
	<string id ="ans5">Natural numbers are found within the sets of whole numbers, integers, and rational numbers. That makes it a true statement.</string>
	<string id ="ans6">It is a false statement since whole numbers belong to the sets of integers and rational numbers, but not to the set of natural numbers.</string>
	
	
	
	

		<!-- congratulation template data -->
		<string id ="e1congratulationtext">Congratulations!!</string>
		<string id ="e1congratulationcompletedtext">You have completed this exercise.</string>
		<string id ="e1congratulationyourscoretext">You scored #userscore# out of #totalscore#.</string>
		<string id ="e1congratulationreviewtext">Let us review the exercise &#187;</string>

		<!-- summary template -->	
		<string id ="quizsummarytitletext">Quiz Summary</string>
		<string id ="e1summaryheadingcorrectans">Correct Answer</string>

		
		<!---page7 drag and drop part-->
		
		
		
		<string id ="title">Drag and drop the numbers to the appropriate box</string>
		<!-- hospital exercise part starts -->
		<string id="label1">0.75</string>
		<string id="label2">0.005</string>
		<string id="label3">0.5444</string>
		<string id="label4">√7</string>
		<string id="label5">0.1414</string>
		<string id="label6">√25</string>
		<!-- hospital exercise part ends -->

		<!-- hospital departments -->
		<string id="dept1">Need1</string>
		<string id="dept2">Need2</string>
		<string id="dept3">Need3</string>
		<string id="dept4">Want1</string>
		<string id="dept5">Want2</string>
		<string id="dept6">Want3</string>
	
	
	
</strings>

</olenepal>

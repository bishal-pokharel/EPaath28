/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=6;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,4]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p2_1,
		data.string.p2_2,
		data.string.p2_3_1,

		data.string.p2_4,

		data.string.p2_6,
		data.string.p2_7,
		data.string.p2_8,

		data.string.p2_9,
		data.string.p2_10,
		data.string.p2_10_1,
		data.string.p2_11,
		data.string.p2_12

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p2_14,

			data.string.p2_16,

			data.string.p2_18,


			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27

			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [



			data.string.p2_14,

			data.string.p2_16,

			data.string.p2_18,

			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27
				],
	}


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").hide;
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		};



		function first01 () {

			$("#activity-page-next-btn-enabled").hide(0);


			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .bigcloud").show(0);



			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".bigcloud" ).animate({ "top": "-=54%" }, {
					duration: 2000,


				}   );


			});


			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".text2" ).animate({ "top": "-=54%" }, {
					duration: 2000,


				}   );


			});


		$board.find(".wrapperIntro.firstPage .text3").delay(2300).show(0);
		$("#activity-page-next-btn-enabled").delay(2300).show(0);

		}

		function first02 () {
			$("#activity-page-next-btn-enabled").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);

			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .smallcloud").show(0);
			$board.find(".wrapperIntro.firstPage .bigcloud").css("top","41%");



			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".bigcloud" ).animate({ "top": "-=50%" }, {
					duration: 2000,


				}   );


			});





			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".text4" ).animate({ "top": "-=54%" }, {
					duration: 2000,


				}   );

			});


		$board.find(".wrapperIntro.firstPage .text5").delay(2300).show(0);
		$("#activity-page-next-btn-enabled").delay(2300).show(0);


		}


		function first03 () {
			$("#activity-page-next-btn-enabled").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .smallcloud2").show(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			$board.find(".wrapperIntro.firstPage .text8").show(0);
			$board.find(".wrapperIntro.firstPage .bigcloud").css("top","41%");

			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".bigcloud" ).animate({ "top": "-=50%" }, {
					duration: 2000,


				}   );


			});

			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".text8" ).animate({ "top": "-=48%" }, {
					duration: 2000,


				}   );

			});

		$board.find(".wrapperIntro.firstPage .text9").delay(2300).show(0);
		$("#activity-page-next-btn-enabled").delay(2300).show(0);





		}








	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");
			$board.find(".wrapperIntro.second").css("padding","1%");
			$board.find(".wrapperIntro.second .text1").css("opacity","1");
		}
		/*end of text 0 firstline*/

		/*first click*/
		function second01 () {

			$board.find(".wrapperIntro.second .para").show(0);
			$board.find(".wrapperIntro.second").find(function() {
				$( ".para" ).animate({ "top": "-=85%" }, {
					duration: 2000,


				}   );


			});

			$board.find(".wrapperIntro.second .text2").show(0);

			$board.find(".wrapperIntro.second").find(function() {
				$( ".text2" ).animate({ "top": "-=91%" }, {
					duration: 2000,


				}   );


			});





		}
		/* end of first click*/


		/* second click*/
		function second02 () {
			$board.find(".wrapperIntro.second .para").css("top","99%");
			$board.find(".wrapperIntro.second .text2").hide(0);

			$board.find(".wrapperIntro.second").find(function() {
				$( ".para" ).animate({ "top": "-=85%" }, {
					duration: 2000,


				}   );


			});


			$board.find(".wrapperIntro.second .text3").show(0);

			$board.find(".wrapperIntro.second").find(function() {
				$( ".text3" ).animate({ "top": "-=85%" }, {
					duration: 2000,


				}   );


			});


		}



		function second03 () {

			$board.find(".wrapperIntro.second .text3").show(0);

		}

		function second04 () {

			$board.find(".wrapperIntro.second .solution02").show(0);
			$board.find(".wrapperIntro.second .text4").css("background","#efefef");




		}

		function second05 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}

		function second06 () {
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .text6").show(0);


		}

		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);
			$board.find(".wrapperIntro.second .text7").css("background","#efefef");


		}

		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .text9").show(0);


		}

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third").css("background","#fff");




		};




		function third01() {

			$board.find(".wrapperIntro.third .text1").show(0);


		}


		function third02(){

			$board.find(".wrapperIntro.third .text0").hide(0);
			$board.find(".wrapperIntro.third .text1").hide(0);

			$board.find(".wrapperIntro.third .text2").show(0);
			$board.find(".wrapperIntro.third").css("padding","1%");
		}

		function third03(){


			$board.find(".wrapperIntro.third .solution05").show(0);
			$board.find(".wrapperIntro.third .text3").css("background","#efefef");

		}

		function third04(){

			$board.find(".wrapperIntro.third .solution06").show(0);
		}


		function third05(){

			$board.find(".wrapperIntro.third .solution07").show(0);
		}


		function third06(){
			$board.find(".wrapperIntro.third .solution08").show(0);
		}


		function third07(){
			$board.find(".wrapperIntro.third .text7").show(0);
			$board.find(".wrapperIntro.third .text2").hide(0);
			$board.find(".wrapperIntro.third .text3").hide(0);

		}


		function third08(){
			$board.find(".wrapperIntro.third .solution09").show(0);
			$board.find(".wrapperIntro.third .text8").css("background","#efefef");
		}

		function third09(){
			$board.find(".wrapperIntro.third .solution10").show(0);
		}

		function third10(){
			$board.find(".wrapperIntro.third .solution11").show(0);
		}


		function third11(){
			$board.find(".wrapperIntro.third .solution12").show(0);
		}

		function third12(){
			$board.find(".wrapperIntro.third .text9").show(0);
		}











/*----------------------------------------------------------------------------------------------------*/


		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth").css("padding","1%");
			$board.find(".wrapperIntro.fourth").css("background","#fff");
		}


		function fourth01(){
			$board.find(".wrapperIntro.fourth .text1").show(0);


		}

		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);



		}

		function fourth03(){

			$board.find(".wrapperIntro.fourth .solution00").show(0);
			$board.find(".wrapperIntro.fourth .text3").css("background","#efefef");
		}

		function fourth04(){
			$board.find(".wrapperIntro.fourth .solution01").show(0);


		}


		function fourth05(){
			$board.find(".wrapperIntro.fourth .solution02").show(0);


		}


		function fourth06(){
			$board.find(".wrapperIntro.fourth .solution03").show(0);


		}

		function fourth07(){
			$board.find(".wrapperIntro.fourth .solution04").show(0);


		}

		function fourth08(){
			$board.find(".wrapperIntro.fourth .solution05").show(0);


		}


		function fourth09(){
			$board.find(".wrapperIntro.fourth .solution06").show(0);


		}


		function fourth10(){
			$board.find(".wrapperIntro.fourth .solution07").show(0);
			$board.find(".wrapperIntro.fourth .solution07").css("display","block");


		}

		function fourth11(){
			$board.find(".wrapperIntro.fourth .text4").show(0);
			$board.find(".wrapperIntro.fourth .solution07").hide(0);
			$board.find(".wrapperIntro.fourth .text3").css("height","61%");

		}






		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,


				second,
				second01,
				second02

				/*
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,

				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08,
				third09,
				third10,
				third11,
				third12

				*/







			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		timer = 100;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
	      		{first : data.string.p5_7},
	      		{first : data.string.p5_0},
						{first : data.string.p5_0},
	      	];

	var dataObj = [{
		justClass : "first",
		text : [data.string.p4_1,
			"2<sup>3</sup>",
			"2 x 3"]
	},{
		justClass : "second",
		text : [data.string.p5_1,
		        /*data.string.p2_7,*/
			data.string.example+": Are the ratios 2 : 4 and 8 : 16 proportional?",
			"",
			"",
			"Answer",
			"",
			"(Press YES or NO using a hovering mouse)"
			/*function () {
				var d = data.string.p4_3;
				d = ole.textSR(d,data.string.p4_4,"<span>"+data.string.p4_4+"</span>");
				d = ole.textSR(d,data.string.p4_5,"<span>"+data.string.p4_5+"</span>")
				return d;
			},
			"",
			data.string.p4_6,*/
			],
		lokharke : "images/timetothink/timetothink1.png",
		lokharkeText : data.string.p4_7,
	}];

	var dToShow = ["YES or NO",
			" = YES"];


	function supMaker (base,power) {
		var arrays = [base,"<sup>"+power+"</sup>", " = "];
		arrays.push(base);
		for (var i = 0; i < power-1; i++) {
			arrays.push(" x ");
			arrays.push(base);
		};
		return arrays;
	}


	/*
	* first
	*/
		/*function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		};*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			//$nextBtn.hide(0);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second .lokharke").hide(0);
			$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
			$(".lokharkeTxt").html(data.string.yourTurn);

			//setTimeout(second_1,400);
		}



		function second_1 () {
			$board.find(".wrapperIntro.second .text1,.wrapperIntro.second .text2").show(0);
			//ole.stringShow(".wrapperIntro.second .text2", dToShow[0], "", 500,null,second_2);
		}
		function second_2 () {
			$board.find(".wrapperIntro.second .text3,.wrapperIntro.second .text4").show(0);
			//ole.stringShow(".wrapperIntro.second .text4", dToShow[1], "", 500,null,second_next);
		}

		function second_next () {
			$board.find(".wrapperIntro.second .text5,.wrapperIntro.second .text6").show(0);
			//$nextBtn.show(0);
		}

		function quefrst() {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			var source = $("#qaArea-template").html();
			var template = Handlebars.compile(source);
			var content = {
				check : "check",
				clickToSee : data.string.p5_5,
				answer : data.string.p5_6,
				ans : data.string.p5_7,
				text   : data.string.p5_2,
				couter : countNext+1,
				terminating : data.string.p5_7,
				recurring : data.string.p5_0,
				counter : countNext

			};
			var html = template(content);
			$board.find(".qaArea").append(html);
			// $board.find(".qaArea input.base").focus();
		};

		function quesecnd() {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			var source = $("#qaArea-template").html();
			var template = Handlebars.compile(source);
			var content = {
				check : "check",
				clickToSee : data.string.p5_5,
				ans :  data.string.p5_8,
				answer : data.string.p5_6,
				text   : data.string.p5_3,
				couter : countNext+1,
				terminating : data.string.p5_7,
				recurring : data.string.p5_0,
				counter : countNext
			}
			var html = template(content);
			$board.find(".qaArea").append(html);
			// $board.find(".qaArea input.base").focus();
		};



		function quethird() {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			var source = $("#qaArea-template").html();
			var template = Handlebars.compile(source);
			var content = {
				check : "check",
				clickToSee : data.string.p5_5,
				ans :  data.string.p5_9,
				answer : data.string.p5_6,
				text   : data.string.p5_4,
				couter : countNext+1,
				terminating : data.string.p5_7,
				recurring : data.string.p5_0,
				counter : countNext
			};
			var html = template(content);
			$board.find(".qaArea").append(html);
			$board.find(".wrapperIntro.second .q2").hide(0);
			$board.find(".wrapperIntro.second .q3").css("margin-top","0%");
			$board.find(".wrapperIntro.second .q4").css("margin-top","4%");

			// $board.find(".qaArea input.base").focus();
		};

		var countHowmany = 0;
		$board.on('click','.clicks .click',function (){
			var $this = $(this);
			var num = parseInt($this.data("cls"));
			var cls = ".q"+num;
			$board.find(cls+' .showAns').show(0);
			$(".clicks").css("margin-top","-9%");
			if (countNext >= $total_page) {
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				$nextBtn.show(0);
			}
		});
		$board.on('click','.base',function (){
			var $this = $(this);
			var base = $(this).val();
			var cls = ".q"+num;
			var num = parseInt($this.data("cls"));

			var upDatas = qDatas[num-2];
			var check = [null,null];
			var wrong = "<img src='images/wrong.png'>";
			var right = "<img src='images/correct.png'>";

			if (base ===upDatas.first) {
				check[0]=1;
				//$board.find(cls+" .check1").html(right);
				$this.next(".checkans").html(right);
				$this.css("border-color", "#3C9300");
				$this.css("border-width", "5px");
				//$this.css("margin-left", "-14%");
				$this.parent().find('.second').attr('disabled','disabled');
				$this.parent().find('.first').attr('disabled','disabled');
				$(".clicks").css("margin-top","-9%");
			} else {
				$this.next(".checkans").html(wrong);
				$this.css("border-color", "#D3131B");
				$this.css("border-width", "5px");
			}


			if (check[0]===1) {
				if (countNext>=3) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);
				}
				$board.find('.showAns').show(0);
			} else {
				$this.parent().next(".clicks").find(".click").show(0);
			}
		});




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 11;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			if (countNext>=$total_page) {
				$nextBtn.hide(0);
				//ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		fnArray = [
				first,  //first one


				//second_1,
				//second_2,
				//second_next,

				quefrst, //second
				quesecnd,
				quethird
			];

		function fnSwitcher () {
			// console.log(countNext+" "+fnArray[countNext]);
			fnArray[countNext]();
			countNext<3?loadTimelineProgress($total_page,countNext+1):loadTimelineProgress($total_page,3);
		}
	/****************/

});

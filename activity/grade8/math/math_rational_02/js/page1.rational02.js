/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {



//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=3;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,2]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			data.string.p1_3,
			data.string.p1_4,
			data.string.p1_5,
			data.string.p1_6,
			data.string.p1_7



			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [

				data.string.p1_6,
				data.string.p1_7

				],
	}



	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var content = {
				// title1 : data.string.p1_1,
				title2 : data.lesson.chapter,
			}
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro .title1").show(0);
			$board.find(".wrapperIntro").css("position","absolute").css("width","100%").css("padding","0%");
			$board.find(".main-bg1").css("width","100%");




		};


		function first01 () {
			$board.find(".wrapperIntro .title2").show(0);
			$board.find(".wrapperIntro.second .text0").show(0);

		}





		function second () {
			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro").css("position","absolute").css("width","100%").css("padding","0%");
			$board.find(".main-bg1").css("width","100%");


			$board.find(".wrapperIntro.second .text0").show(0);



			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .hippa").show(0);
			$board.find(".wrapperIntro.second .hippa2").show(0);




		}


		function second01 () {




			$board.find(".wrapperIntro.second .text3").show(0);

		}


		function second02 () {

			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .hippa").hide(0);
			$board.find(".wrapperIntro.second .hippa2").hide(0);

			$board.find(".wrapperIntro.second .text4").show(0);
			$board.find(".wrapperIntro.second .bub1").show(0);
			$board.find(".wrapperIntro.second .bub1").addClass("slideRight");
			$board.find(".wrapperIntro.second .text4").addClass("slideRight");

			$board.find(".wrapperIntro.second .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second").css({
				'background':'url(activity/grade8/math/math_rational_02/image/page1/main-bg.jpg)',
				'background-repeat':'no-repeat',
				"height": "100%",
				"width": "100%",
				"background-size":"100% 100%"
			});
			$board.find(".texture").hide(0);

		}


		function second03 () {

			$board.find(".wrapperIntro.second .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").hide(0);

			$board.find(".wrapperIntro.second").css('background', 'url(activity/grade8/math/math_rational_02/image/page1/clouds.jpg)');
			$board.find(".wrapperIntro.second .text1").hide(0);


			$board.find(".wrapperIntro.second .text5").show(0);


		}


		function second04 () {


			$board.find(".wrapperIntro.second .text2").hide(0);

			$board.find(".wrapperIntro.second .text3").show(0);
		}

		function second05 () {


			$board.find(".wrapperIntro.second .text3").hide(0);

			$board.find(".wrapperIntro.second .text4").show(0);
		}











/*-------------------------------------------------------------------------------------------------*/

/*page3*/




/*----------------------------------------------------------------------------------------------------*/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first slide
				// first01,


				second,//second slide
				second01,
				second02,
				second03,
				second04,
				second05




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

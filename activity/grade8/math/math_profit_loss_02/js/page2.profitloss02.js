/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=17;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,5,10,11]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p2_1,
		data.string.p2_2,
		data.string.p2_3,
		data.string.p2_4,
		data.string.p2_5,
		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [
			        data.string.p2_1
			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "fourth",

			text : [
			data.string.p2_19,
			data.string.p2_20,
			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,

				],
	},

	{
		justClass : "third",

			text : [
			data.string.p2_18,
				],
	},
	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");


			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);


			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);

			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);

		};



		function first01 () {



		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text3").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bubble1").addClass("slideRight");

			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);

		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);

			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('border-color','#fea92a');

		}

		function first04 () {
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#fea92a');
		}


	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = {
					title : data.string.p2_1,
					CPTitle : data.string.p2_6,
					CPDesc : data.string.p2_7,
					CPDesc_1 : data.string.p2_31,
					CPDesc_2 : data.string.p2_32,
					CPDesc_3 : data.string.p2_33,
					SPTitle : data.string.p2_8,
					SPDesc : data.string.p2_9,
					SPDesc_1 : data.string.p2_30,
					MPTitle : data.string.p2_10,
					MPDesc : data.string.p2_11,
					MPDesc_1 : data.string.p2_28,
					MPDesc_2 : data.string.p2_29,
					DisTitle : data.string.p2_12,
					DisDesc1 : data.string.p2_13,
					DisDesc1_1 : data.string.p2_26,
					DisDesc1_2 : data.string.p2_27,
					DisDesc2 : data.string.p2_14,
					DisDesc3 : data.string.p2_15,
					DisDesc4 : data.string.p2_16,
					DisDesc5 : data.string.p2_17,
			}

			console.log(content);

			var html = template(content);
			$board.html(html);
			//$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			//$board.find(".wrapperIntro.second .char-b").addClass("slideLeft");
			$nextBtn.show(0);
			 $("#card1").flip({
			        trigger: "manual"
			 });
			 $("#card2").flip({
			        trigger: "manual"
			 });
			 $("#card3").flip({
			        trigger: "manual"
			 });
			 $("#card4").flip({
			        trigger: "manual"
			 });
		}

		function second_01(){
			$("#card1").flip(true);
		}

		function second_02(){
			$("#card2").flip(true);
		}

		function second_03(){
			$("#card3").flip(true);
		}

		function second_04(){
			$("#card4").flip(true);
		}

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third .char-b").hide(0);
			$board.find(".wrapperIntro.third .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.third").css('background', 'url(activity/grade8/math/math_profit_loss_02/image/page1/letslook.png)');
			$board.find(".wrapperIntro.third").css("background-size","100%");

			third01();
		};

		function third01 () {
			$board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);

			$board.find(".wrapperIntro.third .text0").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").hide(0);






		}


/*page4*/

		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);



		}


		function fourth01() {
			$board.find(".wrapperIntro.fourth .text1").show(0);


		}


		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);
		}

		function fourth03(){
			$board.find(".wrapperIntro.fourth .solution00").show(0);
			$board.find(".wrapperIntro.fourth .text3").css("background","#efefef");
		}

		function fourth04(){
			$board.find(".wrapperIntro.fourth .solution01").show(0);
		}


		function fourth05(){
			$board.find(".wrapperIntro.fourth .solution02").show(0);
		}


		function fourth06(){
			$board.find(".wrapperIntro.fourth .solution03").show(0);
			//$board.find(".wrapperIntro.fourth .continue").show(0);
		}


		function fifth() {

			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = {
				title1 : data.lesson.chapter,
				title2 : data.lesson.chapter,
			}
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro .title1").show(0);




		};


		function fifth01 () {
			$board.find(".wrapperIntro .title2").show(0);

		}
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		fifth();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		$('.f1_container').on('click', function() {
			$(this).toggleClass('active');
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [

				fifth,
				// fifth01,

				first,  //first one
				//first01,
				first02,
				first03,
				first04,


				second,
				second_01,
				second_02,
				second_03,
				second_04,

				third,

				fourth,
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

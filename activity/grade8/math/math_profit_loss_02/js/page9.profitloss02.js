$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);

	var qDatas = [
		{ansstring : data.string.p9_5, ansque : data.string.p9_2,ans : "40",classcheck : data.string.p9_12},
		{ansstring : data.string.p9_6, ansque : data.string.p9_3,ans : "50",classcheck : data.string.p9_13},
		{ansstring : data.string.p9_5, ansque : data.string.p9_4,ans : "50",classcheck : data.string.p9_13},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p5_1,
			qTitle : data.string.p5_1,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}


	function first(sub) {

		if(countNext > 1){
			$('.q1').hide(0);
			$('.q2').hide(0);
		}

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p9_7,
			ans : sub.ans,
			answer : data.string.p9_8,
			text : sub.ansque,
			first : data.string.p9_5,
			second : data.string.p9_6,
			ansString : sub.ansstring,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.base',function (){
			var $this = $(this);
			var base = $(this).val();
			var cls = ".q"+num;
			var num = parseInt($this.data("cls"));
			var upDatas = qDatas[num-1];
			var check = [null,null];
			var wrong = "<img src='images/wrong.png'>";
			var right = "<img src='images/correct.png'>";

			if (base ===upDatas.ansstring) {
				check[0]=1;
				//$board.find(cls+" .check1").html(right);
				$this.parent().find(".check2").html(right);
				$this.css("border-color", "#3C9300");
				$this.css("border-width", "5px");
				$this.parent().find('.first').attr('disabled','disabled');
				$this.parent().find('.second').attr('disabled','disabled');
			} else {
				$this.parent().find(".check2").html(wrong);
				$this.css("border-color", "#D3131B");
				$this.css("border-width", "5px");
				$this.parent().find('.first').attr('disabled','disabled');
				$this.parent().find('.second').attr('disabled','disabled');
			}

		});

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.baseinput").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];

		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		var isDisabled = $(cls).find('.base').prop('disabled');

		if(base === "" || !isDisabled){
			swal("Please fill all the empty fields & choose the answer!");
		}else{
			if (base === upDatas.ans) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
			} else {
				$board.find(cls+" .check1").html(wrong);
			}


		    if (!isDisabled)
		    {
		    	$(cls).find("."+upDatas.classcheck).css("border-color", "#3C9300");
		    	$(cls).find("."+upDatas.classcheck).css("border-width", "5px");
		    	$(cls).find(".base").attr('disabled','disabled');
		    }

			if (check[0]===1) {
				if (countNext>=2) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);
				}
			} else {
				$board.find(cls+" .clicks .click").show(0);
			}
		}
	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

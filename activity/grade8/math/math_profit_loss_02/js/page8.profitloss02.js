/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=19;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p8_1,
		data.string.p8_2,
		data.string.p8_3,
		data.string.p8_4,
		data.string.p8_5,
		data.string.p8_6,
		data.string.p8_7,
		data.string.p8_8,
		data.string.p8_9

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p8_10,
			data.string.p8_11,
			data.string.p8_12,
			data.string.p8_13,
			data.string.p8_14,
			data.string.p8_15,
			data.string.p8_16,
			data.string.p8_17,
			data.string.p8_18,
			data.string.p8_19,
			data.string.p8_20,
			data.string.p8_21,
			data.string.p8_22,
			data.string.p8_23,
			data.string.p8_24,
			data.string.p8_25,
			data.string.p8_26


			],
	}



	]




/******************************************************************************************************/
	/*
	* first
	*/
	function first () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
				PTitle : data.string.p8_1,
				PDesc1 : data.string.p8_2,
				PDesc2 : data.string.p8_3,
				PDesc3 : data.string.p8_4,
				PDesc3_1 : data.string.p8_15,
				PDesc3_2 : data.string.p8_16,
				PDesc3_3 : data.string.p8_17,
				LTitle : data.string.p8_14,
				LDesc1 : data.string.p8_5,
				LDesc2 : data.string.p8_6,
				LDesc3 : data.string.p8_7,
				LDesc3_1 : data.string.p8_18,
				LDesc3_2 : data.string.p8_16,
				LDesc3_3 : data.string.p8_17,
		}

		console.log(content);

		var html = template(content);
		$board.html(html).text();

		//$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
		//$board.find(".wrapperIntro.second .char-b").addClass("slideLeft");
		$nextBtn.show(0);
		 $("#card1").flip({
		        trigger: "manual"
		 });
		 $("#card2").flip({
			 trigger: "manual"
		 });
	}

	function first_01(){
		$("#card1").flip(true);
	}

	function first_02(){
		$("#card2").flip(true);
	}




	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);

		}


		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);


		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);


		}


		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}


		function second11 () {
			$board.find(".wrapperIntro.second .solution08").show(0);


		}

		function second12 () {
			$board.find(".wrapperIntro.second .solution00").hide(0);
			$board.find(".wrapperIntro.second .solution01").hide(0);
			$board.find(".wrapperIntro.second .solution02").hide(0);
			$board.find(".wrapperIntro.second .solution03").hide(0);
			$board.find(".wrapperIntro.second .solution04").hide(0);
			$board.find(".wrapperIntro.second .solution05").hide(0);
			$board.find(".wrapperIntro.second .solution06").hide(0);
			$board.find(".wrapperIntro.second .solution07").hide(0);
			$board.find(".wrapperIntro.second .solution08").hide(0);
			$board.find(".wrapperIntro.second .text3").css("height","58%");
			$board.find(".wrapperIntro.second .solution09").show(0);


		}

		function second13 () {
			$board.find(".wrapperIntro.second .solution10").show(0);


		}

		function second14 () {
			$board.find(".wrapperIntro.second .solution11").show(0);


		}

		function second15 () {
			$board.find(".wrapperIntro.second .solution12").show(0);


		}

		function second16 () {
			$board.find(".wrapperIntro.second .solution13").show(0);


		}


		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/





		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first_01,
				first_02,


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13,
				second14,
				second15,
				second16


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

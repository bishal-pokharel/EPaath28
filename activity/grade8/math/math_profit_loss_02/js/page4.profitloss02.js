/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=19;

	var baseAndIndicesClick = [null,null]; //used by base and index
	$nextBtn.show(0);

	var clickArray = [0,2,5,6]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p2_1,
		data.string.p4_0,
		data.string.p4_1,
		data.string.p4_2,
		data.string.p4_3,
		data.string.p4_4,
		data.string.p4_5,
		data.string.p4_6,
		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [


			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "fourth",

			text : [
			data.string.p4_22,
			data.string.p4_23,
			data.string.p4_24,
			data.string.p4_25,
			data.string.p4_26,
			data.string.p4_27,
			data.string.p4_28,
			data.string.p4_29,
			data.string.p4_30,
			data.string.p4_31,
			data.string.p4_32,
			data.string.p4_33,
			data.string.p4_34,
			data.string.p4_35




				],
	},



	{
		justClass : "third",

			text : [
			data.string.p4_21

				],
	},


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);

			$board.find(".wrapperIntro.firstPage .text0").hide(0);


			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);


			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);


		};



		function first01 () {



		}

		function first02 () {

			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			//$board.find(".wrapperIntro.firstPage .bub2").addClass("slideLeft");



			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);


			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);

			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .text3").delay(1000).show(0);
			$board.find(".wrapperIntro.firstPage .text4").delay(1500).show(0);
			$board.find(".wrapperIntro.firstPage .text5").delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .text6").delay(2500).show(0);
			$board.find(".wrapperIntro.firstPage .text7").delay(2800).show(0);

		}





	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = {
					PTitle : data.string.p4_7,
					PDesc1 : data.string.p4_9,
					PDesc2 : data.string.p4_10,
					PDesc3 : data.string.p4_11,
					PDesc4 : data.string.p4_12,
					PDesc5 : data.string.p4_13,
					PDesc6 : data.string.p4_14,
					LTitle : data.string.p4_8,
					LDesc1 : data.string.p4_15,
					LDesc2 : data.string.p4_16,
					LDesc3 : data.string.p4_17,
					LDesc4 : data.string.p4_18,
					LDesc5 : data.string.p4_19,
					LDesc6 : data.string.p4_20,
			};

			console.log(content);

			var html = template(content);
			$board.html(html);
			//$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			//$board.find(".wrapperIntro.second .char-b").addClass("slideLeft");
			$nextBtn.show(0);
			 $("#card1").flip({
			        trigger: "manual"
			 });
			 $("#card2").flip({
				 trigger: "manual"
			 });
		}

		function second_01(){
			$("#card1").flip(true);
		}

		function second_02(){
			$("#card2").flip(true);
		}

		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third .char-b").hide(0);
			//$board.find(".wrapperIntro.third .char-a").addClass("slideRight");
			third01();
		};

		function third01 () {
			$board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);

			$board.find(".wrapperIntro.third .text0").show(0);
			$board.find(".wrapperIntro.third .bubble1").hide(0);

			$board.find(".wrapperIntro.third").css('background', 'url(activity/grade8/math/math_profit_loss_02/image/page1/letslook.png)');
			$board.find(".wrapperIntro.third").css("background-size","100%");

			$board.find(".wrapperIntro.third .squirrel-listening03").hide(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").hide(0);

		}


		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);



		}


		function fourth01() {
			$board.find(".wrapperIntro.fourth .text1").show(0);


		}


		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);
		}

		function fourth03(){
			$board.find(".wrapperIntro.fourth .solution00").show(0);
			$board.find(".wrapperIntro.fourth .text3").css("background","#efefef");
		}

		function fourth04(){
			$board.find(".wrapperIntro.fourth .solution01").show(0);
		}


		function fourth05(){
			$board.find(".wrapperIntro.fourth .solution02").show(0);
		}


		function fourth06(){
			$board.find(".wrapperIntro.fourth .solution03").show(0);

		}

		function fourth07(){
			$board.find(".wrapperIntro.fourth .solution04").show(0);

		}

		function fourth08(){
			$board.find(".wrapperIntro.fourth .solution05").show(0);

		}

		function fourth09(){
			$board.find(".wrapperIntro.fourth .solution00").hide(0);
			$board.find(".wrapperIntro.fourth .solution01").hide(0);
			$board.find(".wrapperIntro.fourth .solution02").hide(0);
			$board.find(".wrapperIntro.fourth .solution03").hide(0);
			$board.find(".wrapperIntro.fourth .solution04").hide(0);
			$board.find(".wrapperIntro.fourth .solution05").hide(0);
			$board.find(".wrapperIntro.fourth .solution06").show(0);

		}

		function fourth10(){
			$board.find(".wrapperIntro.fourth .solution07").show(0);

		}

		function fourth11(){
			$board.find(".wrapperIntro.fourth .solution08").show(0);

		}

		function fourth12(){
			$board.find(".wrapperIntro.fourth .solution09").show(0);

		}

		function fourth13(){
			$board.find(".wrapperIntro.fourth .solution10").show(0);
			$board.find(".wrapperIntro.fourth .solution10").css("display","block");
		}

/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				//first01,
				first02,


				second,
				second_01,
				second_02,

				third,


				fourth,
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,
				fourth07,
				fourth08,
				fourth09,
				fourth10,
				fourth11,
				fourth12,
				fourth13


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

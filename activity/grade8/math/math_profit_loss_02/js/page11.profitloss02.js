$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);
	$nextBtn.show(0);

	var qDatas = [
		{base : "5", power : "55", firstlabel : data.string.p11_5, secondlabel : data.string.p11_6,ansque : data.string.p11_2,prefix : 'Rs.'},
		{base : "600", power : "2400", firstlabel : data.string.p11_7, secondlabel : data.string.p11_6, ansque : data.string.p11_3, prefix : 'Rs.'},
		{base : "100", firstlabel : data.string.p11_8, ansque : data.string.p11_4, prefix : 'Rs.'},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p11_1,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
	}

	function first(sub) {

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p11_9,
			firstans : sub.base,
			secondans : sub.power,
			answer : data.string.p11_10,
			text : sub.ansque,
			prefix : sub.prefix,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};
	function second(sub) {
		$('.q1').hide(0);
		$('.q2').hide(0);
		var source = $("#qaArea1-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p11_9,
			firstans : sub.base,
			answer : data.string.p11_10,
			text : sub.ansque,
			prefix : sub.prefix,
			firstlabel : sub.firstlabel,
			couter : countNext+1
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};
	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {

		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;

		var base = $board.find(cls+" input.base").val().toLowerCase();

		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var check_empty = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(num == 3){

			if(base === ""){
				swal("Please fill all the empty fields!");
			}
			else{
				if (base ===upDatas.base) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
				if (check[0]===1) {
					if (countNext>=2) {
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				} else {
					$board.find(cls+" .clicks .click").show(0);
				}
			}
		}
		else{
			var power = $board.find(cls+" input.power").val().toLowerCase();
			if(base === ""){
				check_empty[0] = 1;
			}
			else{
				if (base ===upDatas.base) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
			}

			if(power === ""){
				check_empty[1] = 1;
			}
			else{
				if (power===upDatas.power) {
					check[1]=1;
					$board.find(cls+" .check2").html(right);
				} else {
					$board.find(cls+" .check2").html(wrong);
				}
			}
			if(check_empty[0] ===1 || check_empty[1] === 1){
				swal("Please fill all the empty fields!");
			}else{
				if (check[0]===1 && check[1]===1) {
					if (countNext>=2) {
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				} else {
					$board.find(cls+" .clicks .click").show(0);
				}
			}
		}

	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			if(countNext == 2){
				second(qDatas[countNext]);
			}else{
				first(qDatas[countNext]);
			}
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

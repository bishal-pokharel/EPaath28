$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{ currency1 : data.string.p2_34, base1 : "50", base2 : "80", base3 : "20", base4 : "60", ansstring1 : data.string.p3_5, ansstring2 : data.string.p3_6, ansstring3 : data.string.p3_7, ansstring4 : data.string.p3_8, ansque : data.string.p3_2},
		{ currency2 : data.string.p2_34, base1 : "12000", base2 : "17000", base3 : "2000", base4 : "15000", ansstring1 : data.string.p3_5, ansstring2 : data.string.p3_6, ansstring3 : data.string.p3_7, ansstring4 : data.string.p3_8, ansque : data.string.p3_3},
		{currency3 : data.string.p2_34, base1 : "9000", base2 : "1000", base3 : "10000", ansstring1 : data.string.p3_5, ansstring2 : data.string.p3_7, ansstring3 : data.string.p3_8, ansque : data.string.p3_4},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p3_1,
			qTitle : data.string.p3_1,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
		}else if(countNext == 2){
			$('.q2').hide(0);
		}
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			currency1: data.string.p2_34,
			currency2: data.string.p2_34,
			currency3: data.string.p2_34,
			currency4: data.string.p2_34,
			clickToSee : data.string.p3_9,
			ans1: data.string.rs + sub.base1,
			ans2 : data.string.rs + sub.base2,
			ans3 : data.string.rs + sub.base3,
			ans4 : data.string.rs + sub.base4,
			answer : data.string.p3_10,
			text : sub.ansque,
			baseString1 : sub.ansstring1,
			baseString2 : sub.ansstring2,
			baseString3 : sub.ansstring3,
			baseString4 : sub.ansstring4,
			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};




function second(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
		}else if(countNext == 2){
			$('.q2').hide(0);
		}
		var source = $("#qaArea1-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			currency1: data.string.p2_34,
			currency2: data.string.p2_34,
			currency3: data.string.p2_34,
			currency4: data.string.p2_34,
			clickToSee : data.string.p3_9,
			ans1: data.string.rs + sub.base1,
			ans2 : data.string.rs + sub.base2,
			ans3 : data.string.rs + sub.base3,
			answer : data.string.p3_10,
			text : sub.ansque,
			baseString1 : sub.ansstring1,
			baseString2 : sub.ansstring2,
			baseString3 : sub.ansstring3,
			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};
	   $board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		console.log(cls);
		var base1 = $board.find(cls+" input.base1").val().toLowerCase();
		var base2 = $board.find(cls+" input.base2").val().toLowerCase();
		var base3 = $board.find(cls+" input.base3").val().toLowerCase();

	   // console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var check_empty = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
			if(base1 === ""){
				check_empty[0] = 1;
			}
			else{
				if (base1 ===upDatas.base1) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
			}

			if(base2 === ""){
				check_empty[1] = 1;
			}
			else{
				if (base2===upDatas.base2) {
					check[1]=1;
					$board.find(cls+" .check2").html(right);
				} else {
					$board.find(cls+" .check2").html(wrong);
				}
			}

			if(base3 === ""){
				check_empty[2] = 1;
			}
			else{
				if (base3===upDatas.base3) {
					check[2]=1;
					$board.find(cls+" .check3").html(right);
				} else {
					$board.find(cls+" .check3").html(wrong);
				}
			}

			if(num === 3){
				if(check_empty[0] ===1 || check_empty[1] === 1 || check_empty[2] === 1){
					swal(data.string.h2);
				}else{
					if (check[0]===1 && check[1]===1 && check[2]===1) {
						if (countNext>=2) {
							ole.footerNotificationHandler.pageEndSetNotification();
						} else {
							$nextBtn.show(0);
						}
					} else {
						$board.find(cls+" .clicks .click").show(0);
					}
				}
			}else{
				var base4 = $board.find(cls+" input.base4").val().toLowerCase();
				if(base4 === ""){
					check_empty[3] = 1;
				}
				else{
					if (base4===upDatas.base4) {
						check[3]=1;
						$board.find(cls+" .check4").html(right);
					} else {
						$board.find(cls+" .check4").html(wrong);
					}
				}

				if(check_empty[0] ===1 || check_empty[1] === 1 || check_empty[2] === 1 || check_empty[3] === 1){
					swal(data.string.h2);
				}else{
					if (check[0]===1 && check[1]===1 && check[2]===1 && check[3]===1) {
						if (countNext>=2) {
							ole.footerNotificationHandler.pageEndSetNotification();
						} else {
							$nextBtn.show(0);
						}
					} else {
						$board.find(cls+" .clicks .click").show(0);
					}
				}
			}


	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			if(countNext == 2){
				second(qDatas[countNext]);
			}else{
				first(qDatas[countNext]);
			}
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

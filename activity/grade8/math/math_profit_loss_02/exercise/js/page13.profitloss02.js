$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base1 : "4500",base2 : "500",base3 : "12.5", baseString1 : data.string.p13_7,baseString2 : data.string.p13_8,baseString3 : data.string.p13_9, ansque : data.string.p13_2},
		{base1 : "9000",base2 : "3000",base3 : "25", baseString1 : data.string.p13_7,baseString2 : data.string.p13_10,baseString3 : data.string.p13_11, ansque : data.string.p13_3},
		{base1 : "2500",base2 : "500",base3 : "25", baseString1 : data.string.p13_7,baseString2 : data.string.p13_8,baseString3 : data.string.p13_9, ansque : data.string.p13_4},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p13_1,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);

		}else if(countNext == 2){
			$('.q2').hide(0);
		}
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p13_6,
			ans1 : sub.base1,
			ans2 : sub.base2,
			ans3 : sub.base3,
			baseString1 : sub.baseString1 ,
			baseString2 : sub.baseString2 ,
			baseString3 : sub.baseString3,
			rs: data.string.rs,

			answer : data.string.p13_5,
			text : sub.ansque,

			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);

	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base1 = $board.find(cls+" input.base1").val().toLowerCase();
		var base2 = $board.find(cls+" input.base2").val().toLowerCase();
		var base3 = $board.find(cls+" input.base3").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var check_empty = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";

		if(base1 === ""){
			check_empty[0] = 1;
		}
		else{
			if (base1 ===upDatas.base1) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
			} else {
				$board.find(cls+" .check1").html(wrong);
			}
		}

		if(base2 === ""){
			check_empty[1] = 1;
		}
		else{
			if (base2 ===upDatas.base2) {
				check[1]=1;
				$board.find(cls+" .check2").html(right);
			} else {
				$board.find(cls+" .check2").html(wrong);
			}
		}

		if(base3 === ""){
			check_empty[2] = 1;
		}
		else{
			if (base3 ===upDatas.base3) {
				check[2]=1;
				$board.find(cls+" .check3").html(right);
			} else {
				$board.find(cls+" .check3").html(wrong);
			}
		}

		if(check_empty[0] === 1 || check_empty[1] === 1 ||  check_empty[2] === 1){
			swal(data.string.h2);
		}else{
			if (check[0]===1 && check[1]===1 && check[2]===1) {
					$nextBtn.show(0);
			} else {
				$board.find(cls+" .clicks .click").show(0);
			}
		}

	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.activityComplete.finishingcall();
		} else {
			first(qDatas[countNext]);
		}
		countNext<3?loadTimelineProgress($total_page,countNext+1):loadTimelineProgress($total_page, 3);
	});

});

//array of image
var definitionImage = [
	"images/definitionpage/definition1.png",
	"images/definitionpage/definition2.png",
	"images/definitionpage/definition3.png",
	"images/definitionpage/definition4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentdefinitionImage = definitionImage[randomImageNumeral];

var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page4/";
var vdopath = $ref+"/videos/page4/";

var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[


	//0 slide
	{
		contentblockadditionalclass : "contentwithbg",
		uppertextblock : [
			{
				textclass         : "polybgtextstyle",
				textdata          : data.string.p6text1,
			}
		],
	},


	//1st slide
	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text2,
			},
		],



		uppertextblock : [
			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text3,

			}
		],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "fiveimgimgtogether",
						imgsrc   : imgpath+"1.png",
					},
					{
						imgclass : "fiveimgimgtogether",
						imgsrc   : imgpath+"2.png",
					},
					{
						imgclass : "fiveimgimgtogether",
						imgsrc   : imgpath+"3.png",
					},
					{
						imgclass : "fiveimgimgtogether",
						imgsrc   : imgpath+"4.png",
					},

					{
						imgclass : "fiveimgimgtogether",
						imgsrc   : imgpath+"5.png",
					},
				],

			}
		],




	},

	//2nd slide

	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],


		lowertextblockadditionalclass: "sidewidth01",
		lowertextblock : [



			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text5,

			}
		],

		imageblockadditionalclass:"sideimage",
		imageblock : [

			{

				imagestoshow : [
					{

						imgclass : "beakerstyle",
						imgsrc   : imgpath+"congu.png",


					},
				],

			},

		],




	},

	//3rd slide

	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],



		uppertextblock : [



			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text6,

			}
		],






	},

	//4th slide video1

	{


		hasheaderblock : true,

		// Added
		uppertextblock : [
			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text9,

			}
		],

		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "equilateral",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"con1.mp4",
						vdolebel1: data.string.p6vdolebel1,
						vdolebel2: data.string.p6vdolebel1,
						vdolebel3: data.string.p6vdolebel1,

					},
				]
			},


		],



	},

	//5th slide video2
	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],

		uppertextblock : [
			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text10,

			}
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "equilateral",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"con2.mp4",
						vdolebel1: data.string.p6vdolebel1,
						vdolebel2: data.string.p6vdolebel2,
						vdolebel3: data.string.p6vdolebel1,
						additionalva:"label4",
						additionalvb:"label5",
						additionalvc:"label6",
					},
				]
			},


		],



	},

	//6th slide video3
	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],

		uppertextblock : [
			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text11,

			}
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "equilateral",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"con3.mp4",
						vdolebel1: data.string.p6vdolebel2,
						vdolebel2: data.string.p6vdolebel1,
						vdolebel3: data.string.p6vdolebel2,
						additionalva:"label7",
						additionalvb:"label8",
						additionalvc:"label9",
					},
				]
			},


		],



	},

	//6th slide video4
	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text4,
			},
		],

		uppertextblock : [
			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text12,

			}
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "equilateral",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"con4.mp4",
						vdolebel1: data.string.p6vdolebel3,
						vdolebel2: data.string.p6vdolebel4,
						vdolebel3: data.string.p6vdolebel1,
						additionalva:"label10",
						additionalvb:"label11",
						additionalvc:"label12",
					},
				]
			},


		],



	},

	//7th


	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text2,
			},
		],


		imageblock : [

			{

				imagestoshow : [
					{

						imgclass : "beakerstyle01",
						imgsrc   : imgpath+"congu1.png",


					},
				],

			},

		],


		lowertextblock : [



			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text7,

			}
		],






	},


	//slide8

	{


		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6text2,
			},
		],


		imageblock : [

			{

				imagestoshow : [
					{

						imgclass : "beakerstyle01",
						imgsrc   : imgpath+"congu2.png",


					},
				],

			},

		],


		lowertextblock : [



			{

				textclass : "paratextstyle",
				textdata  : data.string.p6text8,

			}
		],






	},

	//end






];

//for play and pause video
	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }


	}


$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;



			if(countNext == 4){
			$("#reload").attr("src", imgpath+"2.gif?"+new Date().getTime());
			$nextBtn.delay(2000).show(0);

		}


			if(countNext == 7){

			$nextBtn.delay(4000).show(0);
			$(".spriteimg.sum").delay(4000).show(0);

		}



		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}





		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}



		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		scriptInit();
		//just for svgblock
		// $svgwrapper = $board.find('div.svgwrapper');
		// if there is a svg content
		/*if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"periodictablegrey.svg", function (f) {
	    		s.append(f);
	    			// snap objects
	    		// var redray          = s.select("#redray");


	    			// jquery objects and js variables
	    		// var $svg      = $svgcontainer.children('svg');
 			});
		}*/
	}

	function scriptInit() {
		var source = $("#scriptfunc").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var s = document.createElement("script");
		s.type = "text/javascript";
		s.innerHTML = html;
		$board.append(s);
	}

	function animate() {

		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);


	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		//$board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer //: "+countNext+" / "+($total_page-1)+"</span>");
	}


	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

//array of image
var definitionImage = [
	"images/definitionpage/definition1.png",
	"images/definitionpage/definition2.png",
	"images/definitionpage/definition3.png",
	"images/definitionpage/definition4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentdefinitionImage = definitionImage[randomImageNumeral];

var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page2/";
var vdopath = $ref+"/videos/page1/";

var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";


		
//var html = template(content);

var content=[	
	
	
	//0 slide
	{	
		contentblockadditionalclass : "contentwithbg",
		uppertextblock : [
			{
				textclass         : "polybgtextstyle",
				textdata          : data.string.p2text0,
			}
		],
	},
	
	
	//slide0-added new
	
	
	
	{	
				
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text1,
			},
		],
		
		
		imageblockadditionalclass:"align-top",
		imageblock : [
			{
			
			imagestoshow:[
					
					{
						imgclass: "threeimgtogether biger",
						imgsrc: imgpath+"acute05.png"
					},
					
					{
						imgclass: "threeimgtogether biger",
						imgsrc: imgpath+"right05.png"
					},
					
					{
						imgclass: "threeimgtogether biger",
						imgsrc: imgpath+"obtuse05.png"
					},
				
				],	

			imagelabels :[
					{
						imagelabelclass : "label1 highlighted",
						imagelabeldata  : data.string.p2text5,
					},
					
					{
						imagelabelclass : "label2",
						imagelabeldata  : data.string.p2text6,
					},
					
					{
						imagelabelclass : "label3",
						imagelabeldata  : data.string.p2text7,
					},
					
					
					{
						imagelabelclass : "label4",
						imagelabeldata  : data.string.p2text2,
					},
					
					{
						imagelabelclass : "label5",
						imagelabeldata  : data.string.p2text3,
					},
					
					{
						imagelabelclass : "label6",
						imagelabeldata  : data.string.p2text4,
					},
					
					
				],				
				
			
			
			},
			
		],
		
	
		
	},
	
	
	//first 
	
	{	
	
		
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text1,
			},
		],
		
		imageblockadditionalclass:"",
		imageblock : [
		
			{
				imageblockadditionalclass:"spriteimg",
				imagestoshow : [
					{
						
						imgclass : "centerpic",
						imgsrc   : imgpath+"acute.png",
						
						
					},
				],
				
				imagelabels :[
					{
						imagelabelclass : "types",
						imagelabeldata  : data.string.p2text2,
						
					},
					{
						imagelabelclass : "individuallabel",
						imagelabeldata  : data.string.p2text5,
					}
					
				],
						
			},
			
		],
		
				
	
		
	},
	
	
	
	//2nd slide
	{	
	
		
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text1,
			},
		],
		
		imageblockadditionalclass:"spriteimg",
		imageblock : [
		
			{
				
				imageblockadditionalclass:"spriteimg",
				imagestoshow : [
					{
						
						imgclass : "centerpic",
						imgsrc   : imgpath+"right.gif",
						
						
					},
				],
				
				imagelabels :[
					{
						imagelabelclass : "types2",
						imagelabeldata  : data.string.p2text3,
					},
					
					{
						imagelabelclass : "individuallabel",
						imagelabeldata  : data.string.p2text6,
					}
					
				],
						
			},
			
		],
		
		
		
	
		
	},
	
	//3rd slide
	
	{	
	
		
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text1,
			},
		],
		
		imageblockadditionalclass:"spriteimg3",
		imageblock : [
		
			{
				
				imageblockadditionalclass:"spriteimg",
				imagestoshow : [
					{
						
						imgclass : "centerpic",
						imgsrc   : imgpath+"obtuse.gif",						
					},
				],
				
				imagelabels :[
					{
						imagelabelclass : "types2",
						imagelabeldata  : data.string.p2text4,
					},
					
					{
						imagelabelclass : "individuallabel",
						imagelabeldata  : data.string.p2text7,
					},
				],
						
			},
			
		],
		
				
	},
	
	
	//4th slide
	
	// {	
// 	
// 		
		// hasheaderblock : true,
// 
// 
		// headerblock : [
			// {
				// textclass : "headertextstyle",
				// textdata  : data.string.p2text1_0,
			// },
		// ],
// 		
		// tableblock : [
// 
// 
		// {
// 				
				// imgcorrectclass1:"correct1",
				// imgcorrectsrc1:imgpath+"correct-small.png",
// 
				// imgcorrectclass2:"correct2",
				// imgcorrectsrc2:imgpath+"correct-small.png",
// 				
				// imgcorrectclass3:"correct3",
				// imgcorrectsrc3:imgpath+"correct-small.png",
// 				
				// imgcorrectclass4:"correct4",
				// imgcorrectsrc4:imgpath+"correct-small.png",
// 				
				// imgcorrectclass5:"correct5",
				// imgcorrectsrc5:imgpath+"correct-small.png",
// 				
				// imgcorrectclass6:"correct6",
				// imgcorrectsrc6:imgpath+"correct-small.png",
// 				
				// imgcorrectclass7:"correct7",
				// imgcorrectsrc7:imgpath+"correct-small.png",
// 				
				// imgcorrectclass8:"correct8",
				// imgcorrectsrc8:imgpath+"correct-small.png",
// 				
				// imgcorrectclass9:"correct9",
				// imgcorrectsrc9:imgpath+"correct-small.png",
// 				
				// imgcorrectclass10:"correct10",
				// imgcorrectsrc10:imgpath+"correct-small.png",
// 				
				// imgcorrectclass11:"correct11",
				// imgcorrectsrc11:imgpath+"correct-small.png",
// 				
				// imgcorrectclass12:"correct12",
				// imgcorrectsrc12:imgpath+"correct-small.png",
// 				
				// imgcorrectoption13:"correct13",
				// imgcorrectsrc13:imgpath+"correct-small.png",
// 				
				// imgcorrectoption14:"correct14",
				// imgcorrectsrc14:imgpath+"correct-small.png",
// 				
				// imgcorrectoption15:"correct15",
				// imgcorrectsrc15:imgpath+"correct-small.png",
// 				
				// //wrong img
// 
				// imgwrongclass1:"wrong1",
				// imgwrongsrc1:imgpath+"wrong-small.png",
// 
				// imgwrongclass2:"wrong2",
				// imgwrongsrc2:imgpath+"wrong-small.png",
// 
				// imgwrongclass3:"wrong3",
				// imgwrongsrc3:imgpath+"wrong-small.png",
// 
				// imgwrongclass4:"wrong4",
				// imgwrongsrc4:imgpath+"wrong-small.png",
// 
				// imgwrongclass5:"wrong5",
				// imgwrongsrc5:imgpath+"wrong-small.png",
// 
				// imgwrongclass6:"wrong6",
				// imgwrongsrc6:imgpath+"wrong-small.png",
// 
				// imgwrongclass7:"wrong7",
				// imgwrongsrc7:imgpath+"wrong-small.png",
// 
				// imgwrongclass8:"wrong8",
				// imgwrongsrc8:imgpath+"wrong-small.png",
// 
				// imgwrongclass9:"wrong9",
				// imgwrongsrc9:imgpath+"wrong-small.png",
// 
				// imgwrongclass10:"wrong10",
				// imgwrongsrc10:imgpath+"wrong-small.png",
// 
				// imgwrongclass11:"wrong11",
				// imgwrongsrc11:imgpath+"wrong-small.png",
// 
				// imgwrongclass12:"wrong12",
				// imgwrongsrc12:imgpath+"wrong-small.png",
// 				
				// imgwrongoption13:"wrong13",
				// imgwrongsrc13:imgpath+"wrong-small.png",
// 				
				// imgwrongoption14:"wrong14",
				// imgwrongsrc14:imgpath+"wrong-small.png",
// 				
				// imgwrongoption15:"wrong15",
				// imgwrongsrc15:imgpath+"wrong-small.png",
// 				
				// acutetxt : data.string.p1tabledata1,
				// righttxt : data.string.p1tabledata2,
				// obtusetxt : data.string.p1tabledata3,
// 				
				// acuteoption : data.string.p1option1,
				// rightoption : data.string.p1option2,
				// obtuseoption : data.string.p1option3,
// 				
				// check: data.string.check,
// 				
// 
				// tableheading : [
					// data.string.p1tableheading1,
					// data.string.p1tableheading2,
					// data.string.p1tableheading3,
					// data.string.p1tableheading4,
					// data.string.p1tableheading5,
					// data.string.p1tableheading6
// 					
				// ],
// 				
// 				
// 			
// 			
			// qData : {
// 			
					// base1 : "10", base2: "20", base3 : "30", base4: "40",
					// base5 : "10", base6: "20", base7 : "30", base8: "40",
					// base9 : "10", base10: "20", base11 : "30", base12: "40"
// 					
			// }
		// }
		// ],
// 		
		// imageblockadditionalclass:"measureangle",
		// imageblock : [
// 		
			// {
// 				
				// imagestoshow : [
					// {
// 						
						// imgid:"droppable",
						// imgclass : "scalethreeimgtogether",
						// imgsrc   : imgpath+"acute01.png",
// 						
// 						
					// },
// 					
					// {
						// imgid:"droppable",
						// imgclass : "scalethreeimgtogether",
						// imgsrc   : imgpath+"right02.png",
// 						
// 						
					// },
// 					
					// {
						// imgid:"droppable",
						// imgclass : "scalethreeimgtogether",
						// imgsrc   : imgpath+"obtuse01.png",
// 						
// 						
					// },
					// /*//correct wrong icons
// 				
					// {
						// imgid:"droppable",
						// imgcorrectclass : "correct1",
						// imgcorrectsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct2",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct3",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct4",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct5",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct6",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct7",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct8",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct9",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct10",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "correct11",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
						// {
						// imgid:"droppable",
						// imgclass : "correct12",
						// imgsrc   : imgpath+"correct-small.png",
// 						
					// },
// 
// 
// 
// 
// 
// 
// 
					// {
						// imgid:"droppable",
						// imgclass : "wrong1",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong2",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong3",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong4",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong5",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong6",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong7",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong8",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong9",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong10",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// {
						// imgid:"droppable",
						// imgclass : "wrong11",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
						// {
						// imgid:"droppable",
						// imgclass : "wrong12",
						// imgsrc   : imgpath+"wrong-small.png",
// 						
					// },
					// //correct wrong icons ends*/
// 					
				// ],
// 				
// 				
			// imagelabels :[
					// {
						// imagelabelclass : "acute",
						// imagelabeldata  : data.string.p1tableheading1 + " " + data.string.p1tabledata1,
					// },
					// {
						// imagelabelclass : "right",
						// imagelabeldata  : data.string.p1tableheading1 + " " + data.string.p1tabledata2,
					// },
// 					
					// {
						// imagelabelclass : "obtuse",
						// imagelabeldata  : data.string.p1tableheading1 + " " + data.string.p1tabledata3,
					// },
// 					
				// ],
// 						
			// },
// 			
// 		
		// ],
// 		
		// dragblock : [
			// {
				// dragtoshow : [
					// {
// 						
						// imgpro   : imgpath+"p3.png",
// 
					// },
				// ]	
			// },
// 		
// 		
		// ],
// 		
		// instructionblock:[
			// {
				// instruction : [
					// data.string.p2instruction1,
					// data.string.p2instruction2,
					// data.string.p2instruction3,
				// ],
			// }
		// ]
// 		
// 	
// 		
	// },
// 	
	//end
	

	
];

//for play and pause video
	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }
		
	
	}
	

	


$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var value = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);
	
	
	
			
	


/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   
		 /*helper to extract electronicconfiguration data and return table element*/

	 Handlebars.registerHelper('extractelementdetails', function(electronicconfigdata) {
	 	var returnhtml = "";
	 	var dataarray = electronicconfigdata.split(",");
	 	
	 	$.each(dataarray, function(index, val) {
	 		val == "" ? val = "-" : null;
	 		returnhtml = returnhtml+"<td>"+val+"</td>";
	 	});

	 	return new Handlebars.SafeString(returnhtml);
	 });

   
   
   
	 Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("dragcontent", $("#dragcontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		 
		//fordrag
		
	
		

		//for drag end
		 
		 
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;
	
		
		
			if(countNext == 4){
			//$nextBtn.css('opacity', '0');
			
			
			setTimeout(function(){$("#draggable").draggable();
			
			$( "#draggable" ).draggable({ containment: ".main" });
			
			var move = 0;
			var angStart = 0;
			var angEnd = 0;
			
			$( ".rotate" ).click(function() {
			move++;
			console.log(move);
			
			var rotation = function (){
				if(move == 1) {
					angEnd = 130;
				} else if(move == 2) {
					angStart = 130;
					angEnd = 90;
				} 
				else if(move == 3) {
					angStart = 90;
					angEnd = 200;
				} 
				
				else {
					angEnd = 0;
					move = 0;
				}
				
				  $("#draggable").rotate({
					
					angle:angStart,
					animateTo:angEnd,
				
					
				  });
				};
				rotation();
				angStart = angEnd;
			
			
	
			});
		
			}, 1000);
			
		
			
			}
		
		
			if(countNext == 7){
		
			$nextBtn.delay(4000).show(0);
			$(".spriteimg.sum").delay(4000).show(0);
			
			
		}
		
	 
		
		if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		
		
		
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		
		
			
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 
	 
	 
	 	
	
	 
	 
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		scriptInit();
		
		//call instruction block controller
		instructionblockcontroller($board);

		var $instructionblock = $board.find("div.instructionblock");
		
		
			
		if(countNext==1){

			$('.vertical-horizontal-center').css("top","20%");
		}
	
		
		
		if(countNext==5){

			$(".footerNotification").hide(0);
			$(".wrong13, .wrong14, .wrong15, .correct13, .correct14, .correct15").hide(0);
		}
		
	}
	
	function scriptInit() {
		var source = $("#scriptfunc").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var s = document.createElement("script");
		s.type = "text/javascript";
		s.innerHTML = html;
		$board.append(s);
	}
	
	function animate() {
		
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		
	}
	
	

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		
		//$board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer //: "+countNext+" / "+($total_page-1)+"</span>");
	}

	
	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	
	$(".base1,.base2,.base3,.base4,.base5,.base6,.base7,.base8,.base9,.base10,.base11,.base12,.base13,.base14,.base15").keypress(function (e) {
			     //if the letter is not digit then display error and don't type anything
			     if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			        //display error message
			        $(".errmsg").show(0).fadeOut("slower");
			        return false;
			    }
	  		});
	
	
	$board.on('click','.check',function () {
		$nextBtn.css('opacity', '1');
		
		//acute
		
			var value1 = $(".base1").val();
				
				if($(".base1").val()=="") {
					swal(data.string.h2);
					$(".wrong1").hide(0);
					$(".correct1").hide(0);
				}
			
				
				else if (value1 ==80) {
									
					$(".base1").css("color", "#006600");
					$(".correct1").css("display","inline");

					$(".wrong1").hide(0);
				}
				else{
					$(".base1").css("color", "#990000");
					$(".wrong1").css("display","inline");

					$(".correct1").hide(0);				}
				
			
			
			var value2 = $(".base2").val();
				if($(".base2").val()=="") {
						swal(data.string.h2);
					$(".wrong2").hide(0);
					$(".correct2").hide(0);
					}
				else if (value2 ==50) {
					$(".base2").css("color", "#006600");
					$(".correct2").css("display","inline");
					$(".wrong2").hide(0);

							
				}
				else{
					$(".base2").css("color", "#990000");

					$(".wrong2").css("display","inline");
					$(".correct2").hide(0);
				}
			
			var value3 = $(".base3").val();
				if($(".base3").val()=="") {
					swal(data.string.h2);
					$(".wrong3").hide(0);
					$(".correct3").hide(0);
				}
			
				else if (value3 ==50) {
					$(".base3").css("color", "#006600");
					$(".correct3").css("display","inline");
					$(".wrong3").hide(0);

							
				}
				else{
					$(".base3").css("color", "#990000");
					$(".wrong3").css("display","inline");
					$(".correct3").hide(0);
				}
			
			var value4 = $(".base4").val();
				
				if($(".base4").val()=="") {
					swal(data.string.h2);
					$(".wrong4").hide(0);
					$(".correct4").hide(0);
				}
				
				else if (value4 ==180) {
					$(".base4").css("color", "#006600");
					$(".correct4").css("display","inline");
					$(".wrong4").hide(0);

							
				}
				else{
					$(".base4").css("color", "#990000");
					$(".wrong4").css("display","inline");
					$(".correct4").hide(0);
				}
			//acute end
			
			
			//acute
		
			var value5 = $(".base5").val();
				
				if($(".base5").val()=="") {
					swal(data.string.h2);
					$(".wrong5").hide(0);
					$(".correct5").hide(0);
				}
				
				else if (value5 ==40) {
					$(".base5").css("color", "#006600");
					$(".correct5").css("display","inline");
					$(".wrong5").hide(0);

							
				}
				else{
					$(".base5").css("color", "#990000");
					$(".wrong5").css("display","inline");
					$(".correct5").hide(0);
				}
				
			
			
			var value6 = $(".base6").val();
				
				if($(".base6").val()=="") {
					swal(data.string.h2);
					$(".wrong6").hide(0);
					$(".correct6").hide(0);
				}
				
				else if (value6 ==90) {
					$(".base6").css("color", "#006600");
					$(".correct6").css("display","inline");
					$(".wrong6").hide(0);

							
				}
				else{
					$(".base6").css("color", "#990000");
					$(".wrong6").css("display","inline");
					$(".correct6").hide(0);
				}
			
			var value7 = $(".base7").val();
				
				if($(".base7").val()=="") {
					swal(data.string.h2);
					$(".wrong7").hide(0);
					$(".correct7").hide(0);
				}
				
				else if (value7 ==50) {
					$(".base7").css("color", "#006600");
					$(".correct7").css("display","inline");
					$(".wrong7").hide(0);

							
				}
				else{
					$(".base7").css("color", "#990000");
					$(".wrong7").css("display","inline");
					$(".correct7").hide(0);
				}
			
		var value8 = $(".base8").val();
				
				if($(".base8").val()=="") {
					swal(data.string.h2);
					$(".wrong8").hide(0);
					$(".correct8").hide(0);
				}
				
				else if (value8 ==180) {
					$(".base8").css("color", "#006600");
					$(".correct8").css("display","inline");
					$(".wrong8").hide(0);

							
				}
				else{
					$(".base8").css("color", "#990000");
					$(".wrong8").css("display","inline");
					$(".correct8").hide(0);
				}
			
			//acute end
			
			
			//obtuse
			
			var value9 = $(".base9").val();
				
				if($(".base9").val()=="") {
					swal(data.string.h2);
					$(".wrong9").hide(0);
					$(".correct9").hide(0);
				}
				
				else if (value9 ==30) {
					$(".base9").css("color", "#006600");
					$(".correct9").css("display","inline");
					$(".wrong9").hide(0);

							
				}
				else{
					$(".base9").css("color", "#990000");
					$(".wrong9").css("display","inline");
					$(".correct9").hide(0);
				}
				
			
			
			var value10 = $(".base10").val();
				if($(".base10").val()=="") {
					swal(data.string.h2);
					$(".wrong10").hide(0);
					$(".correct10").hide(0);
				}
				
				else if (value10 ==130) {
					$(".base10").css("color", "#006600");
					$(".correct10").css("display","inline");
					$(".wrong10").hide(0);

							
				}
				else{
					$(".base10").css("color", "#990000");
					$(".wrong10").css("display","inline");
					$(".correct10").hide(0);
				}
			
			var value11 = $(".base11").val();
				if($(".base11").val()=="") {
					swal(data.string.h2);
					$(".wrong11").hide(0);
					$(".correct11").hide(0);
				}
				
				else if (value11 ==20) {
					$(".base11").css("color", "#006600");
					$(".correct11").css("display","inline");
					$(".wrong11").hide(0);

							
				}
				else{
					$(".base11").css("color", "#990000");
					$(".wrong11").css("display","inline");
					$(".correct11").hide(0);
				}
			
				var value12 = $(".base12").val();
				if($(".base12").val()=="") {
					swal(data.string.h2);

					$(".wrong12").hide(0);
					$(".correct12").hide(0);
				}
				
				else if (value12 ==180) {
					$(".base12").css("color", "#006600");
					$(".correct12").css("display","inline");
					$(".wrong12").hide(0);

							
				}
				else{
					$(".base12").css("color", "#990000");
					$(".wrong12").css("display","inline");
					$(".correct12").hide(0);
				}
				
				
				//Acute value checker
				var optionValue1 = $(".acuOptgroup option:selected").val();
				if( optionValue1 == "") {
					swal(data.string.h2);

				}
				
				else if (optionValue1 == "Acute") {
					$(".acuOptgroup").css("color", "#006600");
					$(".correct13").css("display","inline");
					$(".wrong13").hide(0);

							
				}
				else{
					$(".acuOptgroup").css("color", "#990000");
					$(".wrong13").css("display","inline");
					$(".correct13").hide(0);
				}
				
				//Right value checker
				var optionValue2 = $(".rigOptgroup option:selected").val();
				if( optionValue2 == "") {
					swal(data.string.h2);

				}
				
				else if (optionValue2 == "Right") {
					$(".rigOptgroup").css("color", "#006600");
					$(".correct14").css("display","inline");
					$(".wrong14").hide(0);

							
				}
				else{
					$(".rigOptgroup").css("color", "#990000");
					$(".wrong14").css("display","inline");
					$(".correct14").hide(0);
				}				
				

				
				//Obtuse value checker
				var optionValue3 = $(".obtOptgroup option:selected").val();
				if( optionValue3 == "") {
					swal(data.string.h2);

				}
				
				else if (optionValue3 == "Obtuse") {
					$(".obtOptgroup").css("color", "#006600");
					$(".correct15").css("display","inline");
					$(".wrong15").hide(0);

							
				}
				else{
					$(".obtOptgroup").css("color", "#990000");
					$(".wrong15").css("display","inline");
					$(".correct15").hide(0);
				}				
				
				if($(".base1,.base2,.base3,.base4,.base5,.base6,.base7,.base8,.base9,.base10,.base11,.base12").val() > 0) {
					$(".footerNotification").show(0);
				}
			
			

			
			//obtuse end
			
				
			});
/*=====  End of Templates Controller Block  ======*/
});
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page1/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[	

	//slide 0
	{
		contentblocknocenteradjust : true,
		imageblock:[
		{
			imagestoshow:[
			{
				imgclass: "sidebg",
				imgsrc: imgpath+"teacher.jpg"
			}
			],
			imagelabels:[
			{
				imagelabelclass: "title",
				imagelabeldata: data.string.factorization
			}
			]
		}
		]
	},
	
	//slide 1
	{
		contentblocknocenteradjust : true,
		lowertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "factorization",
			textdata: data.string.p1text1
		},
		{
			textclass: "factorizationdef cssfadein",
			textdata: data.string.p1text2
		}
		],
	},

	//slide 2

	{
		contentblocknocenteradjust : true,
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "factorization cssgoup",
			textdata: data.string.p1text1
		},
		{
			textclass: "factorizationdef2 cssgoupagain",
			textdata: data.string.p1text2
		},
		{
			textclass: "foreg cssfadein2",
			textdata: data.string.forexample
		}
		],
		lowertextblock:[
		{
			textclass:"example1 cssfadein3",
			textdata: data.string.p1egtext1
		},
		{
			textclass: "equal1",
			textdata: data.string.equal
		},
		{
			textclass:"example1solution",
			textdata: data.string.p1egtext2
		},
		{
			textclass: "factorizebtn1",
			textdata: data.string.multiply
		},
		{
			textclass:"example2",
			textdata: data.string.p1egtext3
		},
		{
			textclass: "equal2",
			textdata: data.string.equal
		},
		{
			textclass:"example2solution",
			textdata: data.string.p1egtext4
		},
		{
			textclass: "factorizebtn2",
			textdata: data.string.multiply
		},
		{
			textclass:"example3",
			textdata: data.string.p1egtext5
		},
		{
			textclass: "equal3",
			textdata: data.string.equal
		},
		{
			textclass:"example3solution",
			textdata: data.string.p1egtext6
		},
		{
			textclass: "factorizebtn3",
			textdata: data.string.multiply
		},
		{
			textclass: "bottomtext",
			textdata: data.string.p1text3
		},
		{
			textclass: "reversebtn",
			textdata: data.string.reverseit
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "lasttext",
			textdata: data.string.p1text4
		}
		]
	},
	//slide 3
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
		{
			textclass: "bigtitle",
			textdata: data.string.p1text5
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "para1",
			textdata: data.string.p1text6
		},
		{	
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "para2",
			textdata: data.string.p1text7
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "para3",
			textdata: data.string.p1text8
		}
		]
	},

	//slide 4
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
		{
			textclass: "bigcentertext",
			textdata: data.string.p1text9
		}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "factorization cssgoupagain",
			textdata: data.string.p1text11
		},

		{
			textclass: "foreg1 cssfadein2",
			textdata: data.string.p1text10
		}
		],
		lowertextblock:[
		{
			textclass:"ques1 cssfadein3",
			textdata: data.string.p1text12
		},
		{
			textclass: "ques2 cssfadein3",
			textdata: data.string.p1text13
		},
		{
			textclass:"option1 cssfadein3",
			textdata: data.string.p1text14
		},
		{
			textclass: "option2 cssfadein3",
			textdata: data.string.p1text15
		},
		{
			textclass:"option3 cssfadein3",
			textdata: data.string.p1text16
		},
		{
			textclass: "option4 cssfadein3",
			textdata: data.string.p1text17
		},
		{
			textclass:"example2solution",
			textdata: data.string.p1egtext4
		},
		{
			textclass: "factorizebtn2",
			textdata: data.string.multiply
		},
		{
			textclass:"example3",
			textdata: data.string.p1egtext5
		},
		{
			textclass: "equal3",
			textdata: data.string.equal
		},
		{
			textclass:"example3solution",
			textdata: data.string.p1egtext6
		},
		{
			textclass: "factorizebtn3",
			textdata: data.string.multiply
		},
		{
			textclass: "bottomtext",
			textdata: data.string.p1text3
		},
		{
			textclass: "reversebtn",
			textdata: data.string.reverseit
		},
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfont",
			textclass: "lasttext",
			textdata: data.string.p1text4
		}
		]
	},
];

	$(function () {	
		var $board    = $('.board');
		var $nextBtn = $("#activity-page-next-btn-enabled");
		var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
		var countNext = 0;

		var $total_page = content.length;
		loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
/*==========  register the handlebar partials first  ==========*/
Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
Handlebars.registerPartial("misccontent", $("#misccontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
			*/
			function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
					(stylerulename = $(this).attr("data-highlightcustomclass")) :
					(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
			*/

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
			 */
			 function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			 if($allnotifications.length > 0){
			 	$allnotifications.one('click', function() {
			 		/* Act on the event */
			 		$(this).attr('data-isclicked', 'clicked');
			 		$(this).removeAttr('data-usernotification');
			 	});
			 }
			}
			/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	 		*/

	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	  	*/  

	  	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0 && $total_page!=1){
	 		$nextBtn.show(0);
	 		$prevBtn.css('display', 'none');
	 	}
	 	else if($total_page == 1){
	 		$prevBtn.css('display', 'none');
	 		$nextBtn.css('display', 'none');

			// if lastpageflag is true 
			!islastpageflag ? 
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			!islastpageflag ? 
			ole.footerNotificationHandler.pageEndSetNotification() :
			ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	}
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
			*/

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
			 */
			 function instructionblockcontroller(){
			 	var $instructionblock = $board.find("div.instructionblock");
			 	if($instructionblock.length > 0){
			 		var $contentblock = $board.find("div.contentblock");
			 		var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			 		var instructionblockisvisibleflag;

			 		$contentblock.css('pointer-events', 'none');

			 		$toggleinstructionblockbutton.on('click', function() {
			 			instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
			 			if(instructionblockisvisibleflag == 'true'){
			 				instructionblockisvisibleflag = 'false';
			 				$contentblock.css('pointer-events', 'auto');
			 			}
			 			else if(instructionblockisvisibleflag == 'false'){
			 				instructionblockisvisibleflag = 'true';
			 				$contentblock.css('pointer-events', 'none');
			 			}

			 			$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
			 		});
			 	}
			 }	
			 /*=====  End of InstructionBlockController  ======*/

			 /*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		var contcount = 0;
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".example1").on(animationend, function(){ 
			$(".factorizebtn1").addClass("csspopup");
		});

		if(countNext==2){
			$nextBtn.hide(0);
		}

		if(countNext==5){
			$('.footerNotification').hide(0);
		}


		$(".factorizebtn1").on("click", function(){
			$(this).hide(0);
			$(".equal1, .example1solution").addClass("cssfadeinzero");
			$(".example2").addClass("cssfadein");
		});

		$(".example2").on(animationend, function(){ 
			$(".factorizebtn2").addClass("csspopupagain");
		});

		$(".factorizebtn2").on("click", function(){ 
			$(this).hide(0);
			$(".equal2, .example2solution").addClass("cssfadeinzero");
			$(".example3").addClass("cssfadein");
		});

		$(".example3").on(animationend, function(){ 
			$(".factorizebtn3").addClass("csspopuplast");
		});

		$(".factorizebtn3").on("click", function(){ 
			$(this).hide(0);
			$(".equal3, .example3solution").addClass("cssfadeinzero");
			$(".reversebtn").addClass("cssfadein3");
			$(".bottomtext").addClass('cssfadein');
		});

		$(".reversebtn").on("click", function(){ 
			$(this).hide(0);
			$(".example1").addClass('moveright');
			$(".example1solution").addClass('moveleft');
			$(".example2").addClass('moveright');
			$(".example2solution").addClass('moveleft');
			$(".example3").addClass('moveright');
			$(".example3solution").addClass('moveleftspecial');
			$(".lasttext").addClass("cssfadein3");
			$(".bottomtext").hide(0);
		});

		$(".lasttext").on(animationend, function(){ 
			$nextBtn.show(0);
		});

		$(".option1").on("click",function(){
			$(".option1").addClass('wrong');
			$(".option2").addClass('right');
			contcount++;
			if(contcount==2)
				$('.footerNotification').show(0);
		});
		$(".option2").on("click",function(){
			$(".option2").addClass('right');
			contcount++;
			if(contcount==2)
				$('.footerNotification').show(0);
		});
		$(".option3").on("click",function(){
			$(".option3").addClass('right');
			contcount++;
			if(contcount==2)
				$('.footerNotification').show(0);
		});
		$(".option4").on("click",function(){
			$(".option4").addClass('wrong');
			$(".option3").addClass('right');
			contcount++;
			if(contcount==2)
				$('.footerNotification').show(0);
		});

	}

	/*=====  End of Templates Block  ======*/


/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
			*/

		function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	
		generaltemplate();
		
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/exercise/images/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[	
	//slide 0
	{
		contentblockadditionalclass : "contentwithbg",
		imageblock : [
		{
			imagestoshow : [
			{
				imgclass : "single-cell formove",
				imgsrc   : imgpath+"duck02.png",
			},
			{
				imgclass : "duckling1 formove",
				imgsrc   : imgpath+"ducklings03.png",
			},
			{
				imgclass : "duckling2 formove",
				imgsrc   : imgpath+"ducklings03.png",
			},
			{
				imgclass : "wrong-ico-1",
				imgsrc   : imgpath+"wrong.png",
			},
			{
				imgclass : "wrong-ico-2",
				imgsrc   : imgpath+"wrong.png",
			},
			],
			imagelabels :[
			{
				imagelabelclass : "motherhead mainduck1",
				imagelabeldata  : data.string.exp,
			},
			{
				imagelabelclass : "duckling1-head duckling babyduck1",
				imagelabeldata  : data.string.fac,
			},
			{
				imagelabelclass : "duckling2-head duckling babyduck2",
				imagelabeldata  : data.string.fac,
			},
			{
				imagelabelclass : "alertsuccess csspopup1",
				imagelabeldata  : data.string.letsmove,
			},
			{
				imagelabelclass : "tryagain csspopup",
				imagelabeldata  : data.string.tryagain,
			},
			],
		},
		],
		uppertextblock : [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfontv",
			textclass: "minigques fpage",
			textdata: data.string.fpage,
		},
		],
	},

	//slide 1
	{
		contentblockadditionalclass : "contentwithbg",
		imageblock : [
			{
			imagestoshow : [
			{
				imgclass : "single-cell formove",
				imgsrc   : imgpath+"duck02.png",
			},
			{
				imgclass : "duckling1 formove",
				imgsrc   : imgpath+"ducklings03.png",
			},
			{
				imgclass : "duckling2 formove",
				imgsrc   : imgpath+"ducklings03.png",
			},
			{
				imgclass : "wrong-ico-1",
				imgsrc   : imgpath+"wrong.png",
			},
			{
				imgclass : "wrong-ico-2",
				imgsrc   : imgpath+"wrong.png",
			},
			],
			imagelabels :[
			{
				imagelabelclass : "option1 ans ansright",
				imagelabeldata  : data.string.opt1,
			},
			{
				imagelabelclass : "option2 ans",
				imagelabeldata  : data.string.opt2,
			},
			{
				imagelabelclass : "option3 ans",
				imagelabeldata  : data.string.opt3,
			},
			{
				imagelabelclass : "option4 ans ansright",
				imagelabeldata  : data.string.opt4,
			},
			{
				imagelabelclass : "option5 ans",
				imagelabeldata  : data.string.opt5,
			},
			{
				imagelabelclass : "motherhead mainduck1",
				imagelabeldata  : data.string.motherhead,
			},
			{
				imagelabelclass : "duckling1-head duckling babyduck1",
			},
			{
				imagelabelclass : "duckling2-head duckling babyduck2",
			},
			{
				imagelabelclass : "alertsuccess csspopup1",
				imagelabeldata  : data.string.letsmove,
			},
			{
				imagelabelclass : "tryagain csspopup",
				imagelabeldata  : data.string.tryagain,
			},
			],
		},
		],
		uppertextblock : [
		{
			datahighlightflag: true,
			datahighlightcustomclass: "bigfontv",
			textclass: "minigques",
			textdata: data.string.miniques,
		},
		],
	},

//slide 2
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling3",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans ansright",
			imagelabeldata  : data.string.q2opt1,
		},
		{
			imagelabelclass : "option2 ans",
			imagelabeldata  : data.string.q2opt2,
		},
		{
			imagelabelclass : "option3 ans ansright",
			imagelabeldata  : data.string.q2opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q2opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q2opt5,
		},
		{
			imagelabelclass : "motherhead",
			imagelabeldata  : data.string.q2motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling",
		},
		{
			imagelabelclass : "duckling2-head duckling",
		},
		{
			imagelabelclass : "duckling3-head duckling",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 3
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling3",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans ansright",
			imagelabeldata  : data.string.q3opt1,
		},
		{
			imagelabelclass : "option2 ans",
			imagelabeldata  : data.string.q3opt2,
		},
		{
			imagelabelclass : "option3 ans ansright",
			imagelabeldata  : data.string.q3opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q3opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q3opt5,
		},
		{
			imagelabelclass : "motherhead",
			imagelabeldata  : data.string.q3motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling",
		},
		{
			imagelabelclass : "duckling2-head duckling",
		},
		{
			imagelabelclass : "duckling3-head duckling",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 4
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans",
			imagelabeldata  : data.string.q4opt1,
		},
		{
			imagelabelclass : "option2 ans ansright",
			imagelabeldata  : data.string.q4opt2,
		},
		{
			imagelabelclass : "option3 ans",
			imagelabeldata  : data.string.q4opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q4opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q4opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1",
			imagelabeldata  : data.string.q4motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling babyduck1p4",
		},
		{
			imagelabelclass : "duckling2-head duckling babyduck2p4",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 5
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling3",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans ansright",
			imagelabeldata  : data.string.q5opt1,
		},
		{
			imagelabelclass : "option2 ans",
			imagelabeldata  : data.string.q5opt2,
		},
		{
			imagelabelclass : "option3 ans ansright",
			imagelabeldata  : data.string.q5opt3,
		},
		{
			imagelabelclass : "option4 ans",
			imagelabeldata  : data.string.q5opt4,
		},
		{
			imagelabelclass : "option5 ans ansright",
			imagelabeldata  : data.string.q5opt5,
		},
		{
			imagelabelclass : "motherhead",
			imagelabeldata  : data.string.q5motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling",
		},
		{
			imagelabelclass : "duckling2-head duckling",
		},
		{
			imagelabelclass : "duckling3-head duckling",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 6
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans",
			imagelabeldata  : data.string.q6opt1,
		},
		{
			imagelabelclass : "option2 ans ansright",
			imagelabeldata  : data.string.q6opt2,
		},
		{
			imagelabelclass : "option3 ans",
			imagelabeldata  : data.string.q6opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q6opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q6opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1",
			imagelabeldata  : data.string.q6motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling babyduck1",
		},
		{
			imagelabelclass : "duckling2-head duckling babyduck2",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 7
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans",
			imagelabeldata  : data.string.q7opt1,
		},
		{
			imagelabelclass : "option2 ans ansright",
			imagelabeldata  : data.string.q7opt2,
		},
		{
			imagelabelclass : "option3 ans",
			imagelabeldata  : data.string.q7opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q7opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q7opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1",
			imagelabeldata  : data.string.q7motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling babyduck1",
		},
		{
			imagelabelclass : "duckling2-head duckling babyduck2",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 8
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans ansright",
			imagelabeldata  : data.string.q8opt1,
		},
		{
			imagelabelclass : "option2 ans",
			imagelabeldata  : data.string.q8opt2,
		},
		{
			imagelabelclass : "option3 ans",
			imagelabeldata  : data.string.q8opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q8opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q8opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1big",
			imagelabeldata  : data.string.q8motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling babyduck1",
		},
		{
			imagelabelclass : "duckling2-head duckling babyduck2",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 9
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling3",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans",
			imagelabeldata  : data.string.q9opt1,
		},
		{
			imagelabelclass : "option2 ans ansright",
			imagelabeldata  : data.string.q9opt2,
		},
		{
			imagelabelclass : "option3 ans ansright",
			imagelabeldata  : data.string.q9opt3,
		},
		{
			imagelabelclass : "option4 ans",
			imagelabeldata  : data.string.q9opt4,
		},
		{
			imagelabelclass : "option5 ans ansright",
			imagelabeldata  : data.string.q9opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1big",
			imagelabeldata  : data.string.q9motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling",
		},
		{
			imagelabelclass : "duckling2-head duckling",
		},
		{
			imagelabelclass : "duckling3-head duckling",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},

	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},

//slide 10
{
	contentblockadditionalclass : "contentwithbg",
	imageblock : [
	{
		imagestoshow : [
		{
			imgclass : "single-cell",
			imgsrc   : imgpath+"duck02.png",
		},
		{
			imgclass : "duckling1",
			imgsrc   : imgpath+"ducklings03.png",
		},
		{
			imgclass : "duckling2",
			imgsrc   : imgpath+"ducklings03.png",
		},
		],

		imagelabels :[
		{
			imagelabelclass : "option1 ans",
			imagelabeldata  : data.string.q10opt1,
		},
		{
			imagelabelclass : "option2 ans",
			imagelabeldata  : data.string.q10opt2,
		},
		{
			imagelabelclass : "option3 ans ansright",
			imagelabeldata  : data.string.q10opt3,
		},
		{
			imagelabelclass : "option4 ans ansright",
			imagelabeldata  : data.string.q10opt4,
		},
		{
			imagelabelclass : "option5 ans",
			imagelabeldata  : data.string.q10opt5,
		},
		{
			imagelabelclass : "motherhead mainduck1",
			imagelabeldata  : data.string.q10motherhead,
		},
		{
			imagelabelclass : "duckling1-head duckling babyduck1",
		},
		{
			imagelabelclass : "duckling2-head duckling babyduck2",
		},
		{
			imagelabelclass : "alertsuccess csspopup1",
			imagelabeldata  : data.string.letsmove,
		},
		{
			imagelabelclass : "tryagain csspopup",
			imagelabeldata  : data.string.tryagain,
		},
		],
	},
	],
	uppertextblock : [
	{
		datahighlightflag: true,
		datahighlightcustomclass: "bigfontv",
		textclass: "minigques",
		textdata: data.string.miniques,
	},
	],
},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==================================================
	=            Handlers and helpers Block            =
	==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
	/**
	What it does:
	- send an element where the function has to see
		for data to highlight
	- this function searches for all nodes whose
	    data-highlight element is set to true 
	-searches for # character and gives a start tag
		;span tag here, also for @ character and replaces with
		end tag of the respective
	- if provided with data-highlightcustomclass value for highlight it
		applies the custom class or else uses parsedstring class
	
	E.g: caller : texthighlight($board);
	*/
	function texthighlight($highlightinside){
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag   = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {	
				/*if there is a data-highlightcustomclass attribute defined for the text element
				use that or else use default 'parsedstring'*/				
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				(stylerulename = $(this).attr("data-highlightcustomclass")) :
				(stylerulename = "parsedstring") ;

				texthighlightstarttag = "<span class='"+stylerulename+"'>";
				replaceinstring       = $(this).html();
				replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	/*=====  End of data highlight function  ======*/

	/*===============================================
	 =            user notification function        =
	 ===============================================*/
	/**		
		How to:
		- First set any html element with 
			"data-usernotification='notifyuser'" attribute,
		and "data-isclicked = ''".
		- Then call this function to give notification		
		*/

	/**
		What it does:
		- You send an element where the function has to see
		for data to notify user
		- this function searches for all text nodes whose
		data-usernotification attribute is set to notifyuser
		- applies event handler for each of the html element which 
		 removes the notification style.
		 */
	function notifyuser($notifyinside){
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ?
		alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
		null ;

		/*variable that will store the element(s) to remove notification from*/	
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		 // if there are any notifications removal required add the event handler
		 if($allnotifications.length > 0){
		 	$allnotifications.one('click', function() {
		 		/* Act on the event */
		 		$(this).attr('data-isclicked', 'clicked');
		 		$(this).removeAttr('data-usernotification');
		 	});
		 }
	}
	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	 		*/

	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	  	*/  

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 	typeof islastpageflag != 'boolean'?
	 	alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 	null;

	 	if(countNext == 0){
	 		$nextBtn.show(0);
	 	}
	 	if(countNext == 0 && $total_page!=1){
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
		}
	}
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
			*/

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
			 */
	function instructionblockcontroller(){
	 	var $instructionblock = $board.find("div.instructionblock");
	 	if($instructionblock.length > 0){
	 		var $contentblock = $board.find("div.contentblock");
	 		var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
	 		var instructionblockisvisibleflag;

	 		$contentblock.css('pointer-events', 'none');

	 		$toggleinstructionblockbutton.on('click', function() {
	 			instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
	 			if(instructionblockisvisibleflag == 'true'){
	 				instructionblockisvisibleflag = 'false';
	 				$contentblock.css('pointer-events', 'auto');
	 			}
	 			else if(instructionblockisvisibleflag == 'false'){
	 				instructionblockisvisibleflag = 'true';
	 				$contentblock.css('pointer-events', 'none');
	 			}

	 			$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
	 		});
	 	}
	}	
	/*=====  End of InstructionBlockController  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var duckflag = 0;
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	

		$('.ans').click(function(){
			field = $(this);
			if($(this).hasClass('alreadyClicked')){
				$(".alert").css("display", "block");
			}
			else{
				field.addClass('alreadyClicked');
        		$('.duckling').each(function() {
        			duckclass = $(this).attr('class').substring(0, $(this).attr('class').indexOf('-head'));
        			if($(this).text() == '') 
        			{
        				if(field.hasClass("ansright")) 
        				{
        					$(this).removeClass('wrongimage');
        					$(".tryagain").css("display", "none");
        					$(this).text(field.text());
        					$(this).attr('data-iscorrect', '1');
        					$('.' + duckclass).attr('src', imgpath + "ducklings02.png");
        					duckflag++;
        					return false;
        				} 
        				else 
        				{
        					$(this).addClass('wrongimage');
        					$(".tryagain").css("display", "block");
        					$(".single-cell").attr('src', imgpath + "duck01.png");
        					$('.' + duckclass).attr('src', imgpath + "ducklings01.png");
        					return false;
        				}
        			}
        		});

        		if($('.duckling').length == duckflag) {
        			$(".tryagain").css("display", "none");
        			$(".alertsuccess").css("display","block");
        			$(".single-cell").addClass("duckmove");
        			$(".single-cell").attr('src', imgpath + "duck02.png");
        			$(".motherhead").addClass("textmove");
        			$(".duckling").addClass("textmovebaby");
        			if(countNext == $total_page-1){
        				setTimeout(function(){
        					ole.activityComplete.finishingcall();
        				}, 6000);
        				$nextBtn.hide(0);
        			}else{
        				$nextBtn.show(0);	
        			}

        		}
        	}
        });
	}

	/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
			*/

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);
	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
		previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
});
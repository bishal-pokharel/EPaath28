var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page6/";

var content=[
	{
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "heading",
				textdata          : data.string.p6text1,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideowl",
						imgsrc: imgpath+"owl.png"
					}
				]
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6text2,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "instructionstyle",
				textdata          : data.string.p6instruction1,
			},
			{
				datahighlightflag : true,
				textclass         : "question1",
				textdata          : data.string.p6question1,
			},
			{
				datahighlightflag : true,
				textclass         : "question2",
				textdata          : data.string.p6question2,
			},
			{
				datahighlightflag : true,
				textclass         : "question3",
				textdata          : data.string.p6question3,
			}
		],
		answerblock:[
			{
				textclass:"answerq1_1",
				textdata: data.string.p6q1ans1
			},
			{
				textclass:"answerq1_2",
				textdata: data.string.p6q1ans2
			},
			{
				textclass:"answerq1_3",
				textdata: data.string.p6q1ans3
			},
			{
				isdatacorrect : "correct",
				textclass:"answerq1_4",
				textdata: data.string.p6q1ans4
			},
			{
				textclass:"answerq2_1",
				textdata: data.string.p6q2ans1
			},
			{
				isdatacorrect : "correct",
				textclass:"answerq2_2",
				textdata: data.string.p6q2ans2
			},
			{
				textclass:"answerq2_3",
				textdata: data.string.p6q2ans3
			},
			{
				textclass:"answerq2_4",
				textdata: data.string.p6q2ans4
			},
			{
				isdatacorrect : "correct",
				textclass:"answerq3_1",
				textdata: data.string.p6q3ans1
			},
			{
				textclass:"answerq3_2",
				textdata: data.string.p6q3ans2
			},
			{
				textclass:"answerq3_3",
				textdata: data.string.p6q3ans3
			},
			{
				textclass:"answerq3_4",
				textdata: data.string.p6q3ans4
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct1",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "correct2",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "correct3",
						imgsrc: "images/correct.png"
					}
				]
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag : true,
				textclass         : "headertextstyle",
				textdata          : data.string.p6text3,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "instructionstyle",
				textdata: data.string.p6text4
			},
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "scale",
						imgsrc: imgpath+"scale.png"
					},
					{
						imgclass: "arrowblack",
						imgsrc: imgpath+"arrow_b.png"
					},
					{
						imgclass: "arrowred",
						imgsrc: imgpath+"arrow_g.png"
					},
					{
						imgclass: "redcircle cssfadein",
						imgsrc: imgpath+"red-circle.png"
					},
					{
						imgclass: "blackcircle",
						imgsrc: imgpath+"black-circle.png"
					},
					{
						imgclass: "scale2",
						imgsrc: imgpath+"scale.png"
					},
					{
						imgclass: "arrowblack2",
						imgsrc: imgpath+"arrow_b.png"
					},
					{
						imgclass: "arrowred2",
						imgsrc: imgpath+"arrow_g.png"
					},
					{
						imgclass: "redcircle2",
						imgsrc: imgpath+"red-circle.png"
					},
					{
						imgclass: "blackcircle2",
						imgsrc: imgpath+"black-circle.png"
					},
					{
						imgclass: "scale3",
						imgsrc: imgpath+"scale.png"
					},
					{
						imgclass: "arrowblack3",
						imgsrc: imgpath+"arrow_b.png"
					},
					{
						imgclass: "arrowred3",
						imgsrc: imgpath+"arrow_g.png"
					},
					{
						imgclass: "redcircle3",
						imgsrc: imgpath+"red-circle.png"
					},
					{
						imgclass: "blackcircle3",
						imgsrc: imgpath+"black-circle.png"
					},
					{
						imgclass: "blackcirclefill3",
						imgsrc: imgpath+"black-circle_fill.png"
					},
					{
						imgclass: "scale4",
						imgsrc: imgpath+"scale.png"
					},
					{
						imgclass: "arrowblack4",
						imgsrc: imgpath+"arrow_b.png"
					},
					{
						imgclass: "arrowred4",
						imgsrc: imgpath+"arrow_g.png"
					},
					{
						imgclass: "redcircle4",
						imgsrc: imgpath+"red-circle.png"
					},
					{
						imgclass: "blackcircle4",
						imgsrc: imgpath+"black-circle.png"
					},
					{
						imgclass: "blackcirclefill4",
						imgsrc: imgpath+"black-circle_fill.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass: "label1",
						imagelabeldata: data.string.p6text5
					},
					{
						imagelabelclass: "label1_sign",
						imagelabeldata: data.string.p5greaterthan
					},
					{
						imagelabelclass: "label1_1",
						imagelabeldata: data.string.p6text5_1
					},
					{
						imagelabelclass: "label2",
						imagelabeldata: data.string.p6text6
					},
					{
						imagelabelclass: "label2_sign",
						imagelabeldata: data.string.p5lessthan
					},
					{
						imagelabelclass: "label2_1",
						imagelabeldata: data.string.p6text6_1
					},
					{
						imagelabelclass: "label3",
						imagelabeldata: data.string.p6text7
					},
					{
						imagelabelclass: "label3_sign",
						imagelabeldata: data.string.p5greaterthanequalto
					},
					{
						imagelabelclass: "label3_1",
						imagelabeldata: data.string.p6text7_1
					},
					{
						imagelabelclass: "label4",
						imagelabeldata: data.string.p6text8
					},
					{
						imagelabelclass: "label4_sign",
						imagelabeldata: data.string.p5lessthanequalto
					},
					{
						imagelabelclass: "label4_1",
						imagelabeldata: data.string.p6text8_1
					}
				]
			}
		]
	}
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		var $options = $board.find('.answerblock').find('p');
		var correctans1 = $board.find(".answerblock").find("p:nth-of-type(4)");
		var correctans2 = $board.find(".answerblock").find("p:nth-of-type(6)");
		var correctans3 = $board.find(".answerblock").find("p:nth-of-type(9)");
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		if (countNext==1) {
			$nextBtn.hide(0);
		}

		correctans1.on(animationend, function(){ 
			$(".correct1").addClass("cssfadeinzero");
			$(".question2, .answerq2_1, .answerq2_2, .answerq2_3, .answerq2_4").addClass("showclass");
		});

		correctans2.on(animationend, function(){ 
			$(".correct2").addClass("cssfadeinzero");
			$(".question3, .answerq3_1, .answerq3_2, .answerq3_3, .answerq3_4").addClass("showclass");
		});
		
		correctans3.on(animationend, function(){
			$(".correct3").addClass("cssfadeinzero");
			$nextBtn.show(0);
		});

		$options.on("click", function(){ 
			$(this).attr("data-isclicked","clicked");

			if($(this).attr("data-correct") == "correct"){
				// update isclicked data attribute to clicked
				// clickcount == 1 ? $userscore.html(++userscorecount) : null;
				// $options.css('pointer-events', 'none');
				$(this).addClass("csscorrect");
			}
			else
			{
				$(this).addClass("incorrect");
			}
		});
		

		$(".redcircle").on(animationend, function(){ 
			$(".label1_1").addClass("csschangecolorblack");
			$(".blackcircle").addClass("cssfadein1");
		});

		$(".blackcircle").on(animationend, function(){ 
			$(".redcircle").removeClass("cssfadein");
			$(".label1_sign").addClass("csschangecolorgreen");
			$(".arrowred").addClass("csshideandseek");
		});

		$(".arrowred").on(animationend, function(){ 
			$(".label1_sign").removeClass("csschangecolorgreen");
			$(".arrowblack").addClass("cssfadeinzero");
		});

		$(".arrowblack").on(animationend, function(){ 
			$(".label2, .label2_sign, .label2_1, .scale2").addClass('cssfadein');
			$(".redcircle2").addClass("cssfadein1");
		});

		$(".redcircle2").on(animationend, function(){ 
			$(".label2_1").addClass("csschangecolorblack2");
			$(".blackcircle2").addClass("cssfadein1");
		});

		$(".blackcircle2").on(animationend, function(){ 
			$(".redcircle2").removeClass("cssfadein1");
			$(".label2_sign").addClass("csschangecolorgreen2");
			$(".arrowred2").addClass("csshideandseek");
		});

		$(".arrowred2").on(animationend, function(){ 
			$(".label2_sign").addClass("csschangecolorblack2");
			$(".arrowblack2").addClass("cssfadeinzero");
		});

		$(".arrowblack2").on(animationend, function(){ 
			$(".label3, .label3_sign, .label3_1, .scale3").addClass('cssfadein');
			$(".redcircle3").addClass("cssfadein1");
		});

		$(".redcircle3").on(animationend, function(){ 
			$(".label3_1").addClass("csschangecolorblack2");
			$(".blackcircle3").addClass("cssfadein1");
		});

		$(".blackcircle3").on(animationend, function(){ 
			$(".redcircle3").removeClass("cssfadein1");
			$(".blackcirclefill3").addClass("cssfadein2");
		});

		$(".blackcirclefill3").on(animationend, function(){ 
			$(".label3_sign").addClass("csschangecolorgreen2");
			$(".arrowred3").addClass("csshideandseek");
		});

		$(".arrowred3").on(animationend, function(){ 
			$(".label3_sign").addClass("csschangecolorblack2");
			$(".arrowblack3").addClass("cssfadeinzero");
		});

		$(".arrowblack3").on(animationend, function(){ 
			$(".label4, .label4_sign, .label4_1, .scale4").addClass('cssfadein');
			$(".redcircle4").addClass("cssfadein1");
		});

		$(".redcircle4").on(animationend, function(){ 
			$(".label4_1").addClass("csschangecolorblack2");
			$(".blackcircle4").addClass("cssfadein1");
		});

		$(".blackcircle4").on(animationend, function(){ 
			$(".redcircle4").removeClass("cssfadein1");
			$(".blackcirclefill4").addClass("cssfadein2");
		});

		$(".blackcirclefill4").on(animationend, function(){ 
			$(".label4_sign").addClass("csschangecolorgreen2");
			$(".arrowred4").addClass("csshideandseek");
		});

		$(".arrowred4").on(animationend, function(){ 
			$(".label4_sign").addClass("csschangecolorblack2");
			$(".arrowblack4").addClass("cssfadeinzero");
		});
	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		switch(countNext){
			case 2  : generaltemplate(); break;
			case 3  : generaltemplate(); break;
			case 9  : 
			case 12 : 
			case 15 : derivationblocktype1template(); break;
			default : generaltemplate(); break;
		}
		
		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
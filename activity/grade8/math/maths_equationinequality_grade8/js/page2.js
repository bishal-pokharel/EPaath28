var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page2/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";

var content=[
	{
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidebg",
						imgsrc: imgpath+"funnyproff.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass : "headingtext",
						imagelabeldata  : data.string.p2text1,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle2",
				textdata  : data.string.p2text1,
			}
		],
		uppertextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "middletext",
				textdata: data.string.p2text2
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "sidetext moveleftanim",
				textdata: data.string.p2text3
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "question1 cssfadein1",
				textdata: data.string.p2text5
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "answer1 cssfadein1",
				textdata: data.string.p2answer
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "question2",
				textdata: data.string.p2text6
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "answer2",
				textdata: data.string.p2text6_1
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "answer2_1",
				textdata: data.string.p2answer
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "question3",
				textdata: data.string.p2text7
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "answer3",
				textdata: data.string.p2text7_1
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "answer3_1",
				textdata: data.string.p2answer
			},
		],
		lowertextblock:[
			{
				textclass: "showsolution1",
				textdata: data.string.showsolution
			},
			{
				textclass: "showsolution2",
				textdata: data.string.showsolution
			},
			{
				textclass:"conclusiontext",
				textdata:data.string.p2text8
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle2",
				textdata  : data.string.p2text9,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "middletext",
				textdata: data.string.p2text9_1
			},
			{
				textclass: "arrowbox2",
				textdata: data.string.p2text23
			},
			{
				textclass: "arrowbox",
				textdata: data.string.p2text22
			}
		],
		alphabetchangeblock:[
			{
				textclass: "changetext1",
				textdata: data.string.p2change1
			},
			{
				textclass: "changetext2",
				textdata: data.string.p2change2
			},
			{
				textclass: "changetext3",
				textdata: data.string.p2change3
			},
			{
				textclass: "changetext4",
				textdata: data.string.p2change4
			}
		],
		numericaltextblock:[
			{
				textclass: "numerical1 cssfadein",
				textdata: data.string.p2text10
			},
			{
				textclass: "numerical2 cssfadein",
				textdata: data.string.p2text11
			},
			{
				textclass: "equal cssfadein",
				textdata: data.string.equal
			},
			{
				textclass: "numerical3 cssfadein",
				textdata: data.string.p2text12
			},
			{
				textclass: "numerical4 cssfadein",
				textdata: data.string.p2text13
			},
			{
				textclass: "numerical5 cssfadein1",
				textdata: data.string.p2text14
			},
			{
				textclass: "numerical6 cssfadein2",
				textdata: data.string.p2text15
			},
			{
				textclass: "numerical7 cssfadein3",
				textdata: data.string.p2text16
			},
			{
				textclass: "numerical8 cssfadein4",
				textdata: data.string.p2text17
			},
			{
				textclass: "numerical9",
				textdata: data.string.p2text18
			},
			{
				textclass: "numerical10",
				textdata: data.string.p2text19
			},
			{
				textclass: "numerical11",
				textdata: data.string.p2text20
			},
			{
				textclass: "numerical12",
				textdata: data.string.p2text21
			}
		],
		imageblock:[
			{
				imagestoshow: [
					{
						imgclass: "minuscross1",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "minuscross2",
						imgsrc: imgpath+"cross-line.png"
					},
				]
			}
		]
	},
	{
		contentblockadditionalclass:"offwhite",
		uppertextblock : [
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass : "onlyparatextstyle",
				textdata  : data.string.p2text24,
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass : "headertextstyle headingpinkbandstyle2",
				textdata  : data.string.p2text25,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "narration1 cssslidefromleft",
				textdata: data.string.p2text26
			},
			{
				textclass: "narration2",
				textdata: data.string.p2text27
			},
			{
				textclass: "middleinstruction",
				textdata: data.string.p2text28
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "tallman",
						imgsrc: imgpath+"man-standing.png"
					},
					{
						imgclass: "shortman cssfadein3",
						imgsrc: imgpath+"boy.png"
					},
					{
						imgclass: "wrongtable",
						imgsrc: imgpath+"wrong-table01.png"
					},
					{
						imgclass: "standonwrongtable",
						imgsrc: imgpath+"boy-standing.png"
					},
					{
						imgclass: "clicktable",
						imgsrc: "images/hand-icon.gif"
					},
				],
				imagelabels:[
					{
						imagelabelclass: "sixfeet",
						imagelabeldata: data.string.p2sixfeet
					}
				],
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass : "headertextstyle headingpinkbandstyle2",
				textdata  : data.string.p2text25,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "narration1 cssslidefromleft",
				textdata: data.string.p2text31
			},
			{
				textclass: "narration2",
				textdata: data.string.p2text32
			},
			{
				textclass: "middleinstruction cssfadein4",
				textdata: data.string.p2text33
			},
			{
				textclass: "endingtext",
				textdata: data.string.p2text34
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "tallman",
						imgsrc: imgpath+"man-standing.png"
					},
					{
						imgclass: "shortman2 cssfadein1",
						imgsrc: imgpath+"boy-label.png"
					},
					{
						imgclass: "shortman showimage",
						imgsrc: imgpath+"boy.png"
					},
					{
						imgclass: "wrongtablelabled",
						imgsrc: imgpath+"wrong-table.png"
					},
					{
						imgclass: "righttablelabled",
						imgsrc: imgpath+"right-table.png"
					},
					{
						imgclass: "wrong",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "righttabletostand",
						imgsrc: imgpath+"boy-standing-on-table.png"
					},
					{
						imgclass: "wrongtabletostand",
						imgsrc: imgpath+"boy-standing-on-wrong_table.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "sixfeet",
						imagelabeldata: data.string.p2sixfeet
					},
					{
						imagelabelclass: "threefeet cssfadein1",
						imagelabeldata: data.string.p2threefeet
					},
					{
						imagelabelclass: "twofeet",
						imagelabeldata: data.string.p2twofeet
					},
					{
						imagelabelclass: "threefeetfive",
						imagelabeldata: data.string.p2threefivefeet
					},
					{
						imagelabelclass: "sixandfive",
						imagelabeldata: data.string.p2sixfive
					},
					{
						imagelabelclass: "sixcorrect",
						imagelabeldata: data.string.p2sixcorrect
					}
				],
			}
		]
	},
	{
		headerblock : [
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass : "headertextstyle headingpinkbandstyle2",
				textdata  : data.string.p2text25,
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "middletext",
				textdata: data.string.p2text41
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "paragraph2 cssfadein",
				textdata: data.string.p2text35
			},
			{
				textclass: "paragraph3 cssfadein1",
				textdata: data.string.p2text36
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "paragraph4 cssfadein2",
				textdata: data.string.p2text37
			},
			{
				textclass: "paragraph5 cssfadein3",
				textdata: data.string.p2text38
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "paragraph6 cssfadein4",
				textdata: data.string.p2text39
			},
			{
				datahighlightflag:true,
				datahighlightcustomclass: "bigfont",
				textclass: "paragraph7 cssfadein7",
				textdata: data.string.p2text40
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "tallman2",
						imgsrc: imgpath+"man-standing.png"
					},
					{
						imgclass: "righttabletostand2",
						imgsrc: imgpath+"tableheight.png"
					},
					{
						imgclass: "cross1 cssfadein6",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross2 cssfadein6",
						imgsrc: imgpath+"cross-line.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass: "hvalue",
						imagelabeldata: data.string.p2hvalue
					}
				]
			}
		]
	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("expmaterialscontent", $("#expmaterialscontent-partial").html());
	 Handlebars.registerPartial("expprocedurecontent", $("#expprocedurecontent-partial").html());
	 Handlebars.registerPartial("expobservationcontent", $("#expobservationcontent-partial").html());
	 Handlebars.registerPartial("expresultcontent", $("#expresultcontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		if(countNext==1 || countNext==2 || countNext==4 || countNext==5){
			$nextBtn.hide(0);
		}

		$(".answer1").on(animationend, function(){
			$(".question2").addClass('cssfadein');
			$(".showsolution1").addClass('goupanim');
		});

		$(".showsolution1").on("click", function(){
			$(".showsolution1").removeClass("goupanim");
			$(".answer2").addClass("cssfadein");
			$(".answer2_1").addClass("cssfadein1");
		});

		$(".answer2_1").on(animationend, function(){
			$(".question3").addClass('cssfadein');
			$(".showsolution2").addClass('goupanim');
		});

		$(".showsolution2").on("click", function(){
			$(".showsolution2").removeClass("goupanim");
			$(".answer3").addClass("cssfadein");
			$(".answer3_1").addClass("cssfadein1");
			$(".conclusiontext").addClass("cssfadein2");
			$nextBtn.delay(2500).show(0);
		});

		var $alphabetchange = $board.find("div.alphabetchangeblock").find('p');
		var $numericaltextblockallp = $board.find("div.numericaltextblock").find('p');
		$alphabetchange.on("click", function(){
			var changevalue = $(this).text();
			$numericaltextblockallp.each(function() {
			    var text = $(this).text();
			    $(this).text(text.replace(/[a-z]/g, changevalue));
			    $nextBtn.show(0);
			});
		});

		$(".numerical8").on(animationend, function(){
			$(".numerical9").addClass("cssfadein");
			$(".numerical10").addClass("cssfadein1");
			$(".numerical11").addClass("cssfadein2");
			$(".minuscross1, .minuscross2").addClass("cssfadein3");
			$(".numerical12").addClass("cssfadein4");
		});

		$(".numerical12").on(animationend, function(){
			$(".arrowbox").addClass("cssfadein");
		});

		$(".arrowbox").on(animationend, function(){
			$(".arrowbox2").addClass('cssfadein1');
			$(".middletext").addClass('cssfadeout');
			$(".alphabetchangeblock").addClass("cssfadein2");
		});

		$(".narration1").on(animationend, function(){
			$(".narration2").addClass("cssslidefromleft");
		});

		$(".shortman").on(animationend, function(){
			$(".middleinstruction").addClass("cssfadein5");
		});

		$(".middleinstruction").on(animationend, function(){
			// $(".narration1, .narration2").removeClass("cssslidefromleft");
			$(".wrongtable").addClass("cssslidefromleftagain");
			$(".clicktable").addClass('cssfadein3');
		});

		$(".clicktable").on("click", function(){
			$(".narration1, .narration2").removeClass("cssslidefromleft");
			$(this).hide(0);
			$(".wrongtable, .shortman").hide(0);
			$(".standonwrongtable").addClass("cssfadein");
			$(".narration2").text(data.string.p2text30);
			$(".narration2").addClass('cssslidefromleft2');
			$(".middleinstruction").hide(0);
			$(".narration1").addClass('cssslidefromleft');
			$(".narration1").text(data.string.p2text29);
			$nextBtn.delay(2000).show(0);
		});

		if(countNext==5){
			$(".shortman2").on(animationend, function(){
				$(".shortman").removeClass("showimage");
			});

			$(".middleinstruction").on(animationend, function(){
				$(".wrongtablelabled, .threefeetfive").addClass("cssfadein");
				$(".righttablelabled, .twofeet").addClass("cssfadein");
			});

			$(".wrongtablelabled").on("click", function(){
				$(".wrong").addClass("cssfadein");
				$(".wrongtabletostand, .sixandfive").show(0);
				$(".shortman2, .threefeet, .righttabletostand, .sixcorrect, .endingtext").hide(0);
			});

			$(".righttablelabled").on("click", function(){
				$(".right").addClass("cssfadein");
				$(".righttabletostand, .sixcorrect, .endingtext").show(0);
				$(".shortman2, .threefeet, .wrongtabletostand, .sixandfive").hide(0);
					$nextBtn.show(0);
			});
		}

		$(".paragraph7").on(animationend, function(){
			$(".hvalue").text(data.string.p2hvalue2);
		});

	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

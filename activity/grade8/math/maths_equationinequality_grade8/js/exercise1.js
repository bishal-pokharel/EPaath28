var SPACE = " ";
var imgpath = $ref+"/images/exercise1/";

var tempcontent=[
	{
		imageblock : [
			{
				imagestoshow : [
					{

						imgclass : "tripatstyle",
						imgsrc   : imgpath+"tripat.gif",
					},
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "orangehighlight",
				textclass : "bottomparatextstyle",
				textdata  : data.string.e1text1,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'hint1',
				textdata: data.string.e1q1hint1
			},
			{
				textclass:'hint2',
				textdata: data.string.e1q1hint2
			},
			{
				textclass:'hint3',
				textdata: data.string.e1q1hint3
			},
			{
				textclass:'hint4',
				textdata: data.string.e1q1hint4
			},
			{
				textclass:'hint5',
				textdata: data.string.e1q1hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrow",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q1
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				}
    			],

    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q2hint1',
				textdata: data.string.e1q2hint1
			},
			{
				textclass:'q2hint2',
				textdata: data.string.e1q2hint2
			},
			{
				textclass:'q2hint3',
				textdata: data.string.e1q2hint2
			},
			{
				textclass:'q2hint4',
				textdata: data.string.e1q2hint3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrowright",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q2
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q3hint1',
				textdata: data.string.e1q3hint1
			},
			{
				textclass:'q3hint2',
				textdata: data.string.e1q3hint2
			},
			{
				textclass:'q3hint3',
				textdata: data.string.e1q3hint3
			},
			{
				textclass:'q3hint4',
				textdata: data.string.e1q3hint4
			},
			{
				textclass:'q3hint5',
				textdata: data.string.e1q3hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrow",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q3
    				},
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q4hint1',
				textdata: data.string.e1q4hint1
			},
			{
				textclass:'q4hint2',
				textdata: data.string.e1q4hint2
			},
			{
				textclass:'q4hint3',
				textdata: data.string.e1q4hint3
			},
			{
				textclass:'q4hint4',
				textdata: data.string.e1q4hint4
			},
			{
				textclass:'q4hint5',
				textdata: data.string.e1q4hint5
			},
			{
				textclass:'q4hint6',
				textdata: data.string.e1q4hint6
			},
			{
				textclass:'q4hint7',
				textdata: data.string.e1q4hint7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrow",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q4
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q5hint1',
				textdata: data.string.e1q5hint1
			},
			{
				textclass:'q5hint2',
				textdata: data.string.e1q5hint2
			},
			{
				textclass:'q5hint3',
				textdata: data.string.e1q5hint3
			},
			{
				textclass:'q5hint4',
				textdata: data.string.e1q5hint4
			},
			{
				textclass:'q5hint5',
				textdata: data.string.e1q5hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrowright",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q5
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q6hint1',
				textdata: data.string.e1q6hint1
			},
			{
				textclass:'q6hint2',
				textdata: data.string.e1q6hint2
			},
			{
				textclass:'q6hint3',
				textdata: data.string.e1q6hint3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrowright",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q6
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass: "q7hint0",
				textdata: data.string.e1q7hint0
			},
			{
				textclass:'q7hint1',
				textdata: data.string.e1q7hint1
			},
			{
				textclass:'q7hint2',
				textdata: data.string.e1q7hint2
			},
			{
				textclass:'q7hint3',
				textdata: data.string.e1q7hint3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrow",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question7",
						textdata: data.string.e1q7
    				},
    				{
    					textclass: "question7_1",
						textdata: data.string.e1q7_1
    				},
    				{
    					textclass: "question7_2",
						textdata: data.string.e1q7_2
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q8hint1',
				textdata: data.string.e1q8hint1
			},
			{
				textclass:'q8hint2',
				textdata: data.string.e1q8hint2
			},
			{
				textclass:'q8hint3',
				textdata: data.string.e1q8hint3
			},
			{
				textclass:'q8hint4_1',
				textdata: data.string.e1q8hint4
			},
			{
				textclass:'q8hint4_2',
				textdata: data.string.e1q8hint4
			},
			{
				textclass:'q8hint5',
				textdata: data.string.e1q8hint6
			},
			{
				textclass:'q8hint6',
				textdata: data.string.e1q8hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrow",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question1",
						textdata: data.string.e1q8
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q9hint1',
				textdata: data.string.e1q9hint1
			},
			{
				textclass:'q9hint2',
				textdata: data.string.e1q9hint2
			},
			{
				textclass:'q9hint3',
				textdata: data.string.e1q9hint3
			},
			{
				textclass:'q9hint4',
				textdata: data.string.e1q9hint4
			},
			{
				textclass:'q9hint5',
				textdata: data.string.e1q9hint6
			},
			{
				textclass:'q9hint6',
				textdata: data.string.e1q9hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrowright",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question9",
						textdata: data.string.e1q9
    				},
    				{
    					textclass: "question9_1",
						textdata: data.string.e1q9_1
    				},
    				{
    					textclass: "question9_2",
						textdata: data.string.e1q9_2
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "hintbox",
		uppertextblock:[
			{
				textclass: "hintheading",
				textdata: data.string.hinttext
			},
			{
				textclass:'q10hint1',
				textdata: data.string.e1q10hint1
			},
			{
				textclass:'q10hint2',
				textdata: data.string.e1q10hint2
			},
			{
				textclass:'q10hint3',
				textdata: data.string.e1q10hint3
			},
			{
				textclass:'q10hint4',
				textdata: data.string.e1q10hint4
			},
			{
				textclass:'q10hint5',
				textdata: data.string.e1q10hint6
			},
			{
				textclass:'q10hint6',
				textdata: data.string.e1q10hint5
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "correct",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "incorrect",
						imgsrc: "images/wrong.png"
					},
					{
						imgclass: "greenarrowright",
						imgsrc: imgpath+"arrow_g.png"
					}
				]
			}
		],
    	exerciseblock:[
    		{
    			questionblock:[
    				{
    					textclass: "question10",
						textdata: data.string.e1q10
    				},
    				{
    					textclass: "question10_1",
						textdata: data.string.e1q10_1
    				},
    				{
    					textclass: "question10_2",
						textdata: data.string.e1q10_2
    				}
    			],
    			sliderpart:[
    				{

    				},
    			],
    			legend:[
    				{
	    				legendname: "minusseven",
	    				legenddata: data.string.p7legendminus7
	    			},
	    			{
	    				legendname: "minussix",
	    				legenddata: data.string.p7legendminus6
	    			},
	    			{
	    				legendname: "minusfive",
	    				legenddata: data.string.p7legendminus5
	    			},
	    			{
	    				legendname: "minusfour",
	    				legenddata: data.string.p7legendminus4
	    			},
	    			{
	    				legendname: "minusthree",
	    				legenddata: data.string.p7legendminus3
	    			},
	    			{
	    				legendname: "minustwo",
	    				legenddata: data.string.p7legendminus2
	    			},
	    			{
	    				legendname: "minusone",
	    				legenddata: data.string.p7legendminus1
	    			},
	    			{
	    				legendname: "zero",
	    				legenddata: data.string.p7legend0
	    			},
	    			{
	    				legendname: "one",
	    				legenddata: data.string.p7legend1
	    			},
	    			{
	    				legendname: "two",
	    				legenddata: data.string.p7legend2
	    			},
	    			{
	    				legendname: "three",
	    				legenddata: data.string.p7legend3
	    			},
	    			{
	    				legendname: "four",
	    				legenddata: data.string.p7legend4
	    			},
	    			{
	    				legendname: "five",
	    				legenddata: data.string.p7legend5
	    			},
	    			{
	    				legendname: "six",
	    				legenddata: data.string.p7legend6
	    			},
	    			{
	    				legendname: "seven",
	    				legenddata: data.string.p7legend7
	    			}
    			],
    			buttons:[
    				{
    					textclass:"closebutton",
    					textdata: data.string.p7diyclose,
    				},
    				{
    					textclass:"openbutton",
    					textdata: data.string.p7diyopen,
    				},
    				{
    					textclass:"submitbutton",
    					textdata: data.string.p7diysubmit,
    				},
    				{
    					textclass:"submitbuttonclose",
    					textdata: data.string.p7diysubmit,
    				}
    			]
    		}
    	]
	},
];


Array.prototype.shufflearraymiddle = function(){
  var i = this.length-2, j, temp;
	    while(--i > 0){
	        j = Math.floor((Math.random() * (i+1)) + 1);
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var content = tempcontent;

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0) {
			$nextBtn.show(0);
		}
		// alert(countNext);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// for diy blocks only
		var $exerciseblock = $board.find('div.exerciseblock');

		if($exerciseblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			var $diyworkarea = $board.find("div.exerciseblock");
			var $slider = $diyworkarea.find("div.sliderslideblock").find("#slider");


			$slider.slider({
			  range: true,
			  min: -7,
			  max: 7,
			  values: [ -7, 0 ],
			  slide: function( event, ui ) {
			    $( "#amount" ).val( ui.values[ 0 ] + " , " + ui.values[ 1 ] );
			  }
			});

			var lastamount1=0;
			var lastamount2=0;
			var $roundbutton = $slider.find("span.ui-slider-handle:nth-of-type(1)");
			var $roundbutton2 = $slider.find("span.ui-slider-handle:nth-of-type(2)");
			$roundbutton2.css("border", "3px solid #009640");
			$roundbutton2.css("border-radius", "50%");
			$roundbutton2.css("background", "#009640");
			$roundbutton.css("background", "transparent");
			$roundbutton.css("border", "transparent");

			$slider.slider({
				stop:function(event, ui){
			      $( "#amount > span:nth-of-type(1)" ).text( ui.values[ 0 ] );
			      $( "#amount > span:nth-of-type(2)" ).text( ui.values[ 1 ] );
			      // lastamount1 = $("#amount span:nth-of-type(1)").html();
			      lastamount2 = $("#amount span:nth-of-type(2)").html();
			      // alert(lastamount1, lastamount2);
				}
			});

			var clickonopen = false;
			var clickonclose = false;
			$(".openbutton").on("click", function(){
				$(this).hide(0);
				$(".closebutton").show(0);
				clickonopen = true;
				clickonclose = false;
				$roundbutton2.css("background", "#fff");
			});

			$(".closebutton").on("click", function(){
				$(this).hide(0);
				clickonopen= false;
				clickonclose = true;
				$(".openbutton").show(0);
				$roundbutton2.css("background", "#009640");
			});

			if (countNext==1) {
				$(".submitbutton").on("click", function(){
					if (clickonopen == true && lastamount2 == -2 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};


			if (countNext==3) {
				$roundbutton2.css("background", "#fff");
				$(".submitbuttonclose").show(0);
				$(".closebutton").show(0);
				$(".openbutton").hide(0);
				$(".submitbuttonclose").on("click", function(){
					if (clickonclose == true && lastamount2 == -2 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==4) {
				$(".submitbutton").on("click", function(){
				// alert(clickonopen4);

					if (clickonopen == true && lastamount2 == 4 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==7) {
				$roundbutton2.css("background", "#fff");
				$(".submitbuttonclose").show(0);
				$(".closebutton").show(0);
				$(".openbutton").hide(0);
				$(".submitbuttonclose").on("click", function(){
					if (clickonclose == true && lastamount2 == 4 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==8) {
				$roundbutton2.css("background", "#fff");
				$(".submitbuttonclose").show(0);
				$(".closebutton").show(0);
				$(".openbutton").hide(0);
				$(".submitbuttonclose").on("click", function(){
					if (clickonclose == true && lastamount2 == -2 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};


		}
	}

	function generaltemplate2() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// for diy blocks only
		var $exerciseblock = $board.find('div.exerciseblock');

		if($exerciseblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			var $diyworkarea = $board.find("div.exerciseblock");
			var $slider = $diyworkarea.find("div.sliderslideblock").find("#slider");


			$slider.slider({
			  range: true,
			  min: -7,
			  max: 7,
			  values: [ 0, 300 ],
			  slide: function( event, ui ) {
			    $( "#amount" ).val( ui.values[ 0 ] + " , " + ui.values[ 1 ] );
			  }
			});

			var lastamount1=0;
			var lastamount2=0;
			var $roundbutton = $slider.find("span.ui-slider-handle:nth-of-type(1)");
			var $roundbutton2 = $slider.find("span.ui-slider-handle:nth-of-type(2)");

			$roundbutton.css("border", "3px solid #009640");
			$roundbutton.css("border-radius", "50%");
			$roundbutton.css("background", "#009640");
			$roundbutton2.css("background", "transparent");
			$roundbutton2.css("border", "transparent");
			$slider.slider({
				stop:function(event, ui){
			      $( "#amount > span:nth-of-type(1)" ).text( ui.values[ 0 ] );
			      $( "#amount > span:nth-of-type(2)" ).text( ui.values[ 1 ] );
			      lastamount1 = $("#amount span:nth-of-type(1)").html();
			      // lastamount2 = $("#amount span:nth-of-type(2)").html();
			      // alert(lastamount1, lastamount2);
				}
			});

			var clickonclose = false;
			var clickonopen = false;
			$(".openbutton").on("click", function(){
				$(this).hide(0);
				$(".closebutton").show(0);
				clickonclose = false;
				clickonopen = true;
				$roundbutton.css("background", "#fff");
			});

			$(".closebutton").on("click", function(){
				$(this).hide(0);
				clickonclose = true;
				clickonopen= false;
				$(".openbutton").show(0);
				$roundbutton.css("background", "#009640");
			});

			if (countNext==2) {
				$(".submitbutton").on("click", function(){
					if (clickonopen == true && lastamount1 == -7 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==5) {
				$(".submitbutton").on("click", function(){
					if (clickonopen == true && lastamount1 == 5 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==6) {
				$roundbutton.css("background", "#fff");
				$(".submitbuttonclose").show(0);
				$(".submitbutton").hide(0);
				$(".closebutton").show(0);
				$(".openbutton").hide(0);
				$(".submitbuttonclose").on("click", function(){
					if (clickonclose == true && lastamount1 == 7 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==9) {
				$(".submitbutton").on("click", function(){
				// alert("hahaha");

					if (clickonopen == true && lastamount2 == 0 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						$nextBtn.show(0);
						$prevBtn.show(0);
						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			};

			if (countNext==10) {
				$roundbutton.css("background", "#fff");
				$(".submitbuttonclose").show(0);
				$(".submitbutton").hide(0);
				$(".closebutton").show(0);
				$(".openbutton").hide(0);
				$(".submitbuttonclose").on("click", function(){
					if (clickonclose == true && lastamount1 == -4 ) {
						$(".correct").addClass('cssfadeinzero');
						$(".incorrect").removeClass('cssfadeinzero');
						// $nextBtn.show(0);
						$prevBtn.show(0);
						navigationcontroller();

						$(this).css("pointer-events", "none");
						$(".closebutton, .openbutton").css("pointer-events", "none");
					}
					else{
						$(".incorrect").addClass('cssfadeinzero');
						$(".correct").removeClass('cssfadeinzero');
						$(".hintbox").addClass("cssfadein");
					}
				});
			}

		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller

		countNext == content.length-1 ? $prevBtn.css('display', 'none') : null;

		// call the template
		// generaltemplate();

		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate2(); break;
			case 3 : generaltemplate(); break;
			case 4 : generaltemplate(); break;
			case 5 : generaltemplate2(); break;
			case 6 : generaltemplate2(); break;
			case 7 : generaltemplate(); break;
			case 8 : generaltemplate(); break;
			case 9 : generaltemplate2(); break;
			case 10 : generaltemplate2(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

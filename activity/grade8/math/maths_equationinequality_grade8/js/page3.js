var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";


var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page3/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";
var observationbuttonpath = "images/playbtn.png";

var content=[
	{
		contentblocknocenteradjust : true,
		imageblock : [
			{
				imagestoshow:[
					{
						imgclass: "fullbg",
						imgsrc: imgpath+"buddha.jpg"
					}
				],
				imagelabels:[
					{
						datahighlightflag:true,
						datahighlightcustomclass: "bigfont",
						imagelabelclass: "textonboard",
						imagelabeldata: data.string.p3text1
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "exampleblock",
		uppertextblock:[
			{
				textclass: "exampleheading",
				textdata: data.string.example
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext1",
				textdata: data.string.p3text4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext2",
				textdata: data.string.p3text7
			},
			{
				textclass: "showsolution",
				textdata: data.string.showsolution
			},
			{
				textclass: "solution1",
				textdata: data.string.p3text4_1
			},
			{
				textclass: "solution2",
				textdata: data.string.p3text7_1
			}

		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "smallbuddha",
						imgsrc: imgpath+"buddhameditate.png"
					},
					{
						imgclass: "cross1",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross2",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross3",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross4",
						imgsrc: imgpath+"cross-line.png"
					}
				],
				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass:"bigfont",
						imagelabelclass:"ruletitle",
						imagelabeldata: data.string.p3text1
					}
				]
			}
		],
		lowertextblockadditionalclass:"ruleblock",
		lowertextblock:[
			{
				textclass: "rulesubheading moveleftanim",
				textdata: data.string.p3rule1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc1 cssfadein1",
				textdata: data.string.p3text2
			},
			{
				textclass: "equal1 cssfadein2",
				textdata: data.string.equal
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc2 cssfadein3",
				textdata: data.string.p3text3
			},
			{
				textclass: "rulesubheading2",
				textdata: data.string.p3rule2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc1_2",
				textdata: data.string.p3text5
			},
			{
				textclass: "equal2",
				textdata: data.string.equal
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc2_2",
				textdata: data.string.p3text6
			},
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "exampleblock",
		uppertextblock:[
			{
				textclass: "exampleheading",
				textdata: data.string.example
			},
			{
				textclass: "exampletext1_1",
				textdata: data.string.p3text10
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext1_2",
				textdata: data.string.p3text10_1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext1_3",
				textdata: data.string.p3text10_2
			},
			{
				textclass: "exampletext2_1",
				textdata: data.string.p3text13
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext2_2",
				textdata: data.string.p3text13_1
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext2_3",
				textdata: data.string.p3text13_2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "exampletext2_4",
				textdata: data.string.p3text13_2
			},
			{
				textclass: "showsolution2",
				textdata: data.string.showsolution
			},
			{
				textclass: "solution3",
				textdata: data.string.p3text10_3
			},
			{
				textclass: "solution4",
				textdata: data.string.p3text13_3
			}

		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "smallbuddha",
						imgsrc: imgpath+"buddhameditate.png"
					},
					{
						imgclass: "cross5",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross6",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross8",
						imgsrc: imgpath+"cross-line.png"
					},
					{
						imgclass: "cross9",
						imgsrc: imgpath+"cross-line.png"
					}
				],
				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass:"bigfont",
						imagelabelclass:"ruletitle",
						imagelabeldata: data.string.p3text1
					}
				]
			}
		],
		lowertextblockadditionalclass:"ruleblock",
		lowertextblock:[
			{
				textclass: "rulesubheading moveleftanim",
				textdata: data.string.p3rule3
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc1 cssfadein1",
				textdata: data.string.p3text8
			},
			{
				textclass: "equal1 cssfadein2",
				textdata: data.string.equal
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc2 cssfadein3",
				textdata: data.string.p3text9
			},
			{
				textclass: "rulesubheading2",
				textdata: data.string.p3rule4
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc1_2",
				textdata: data.string.p3text11
			},
			{
				textclass: "equal2",
				textdata: data.string.equal
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "ruledesc2_2",
				textdata: data.string.p3text12
			},
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblockadditionalclass: "exampleblock2 moveleftanim",
		uppertextblock:[
			{
				textclass: "exampleheading2",
				textdata: data.string.example
			},
			{
				textclass: "numbering",
				textdata: data.string.p3text17_1
			},
			{
				textclass: "egtext1",
				textdata: data.string.p3text17
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext2",
				textdata: data.string.p3text18
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext3",
				textdata: data.string.equal
			},
			{
				textclass: "egtext4",
				textdata: data.string.p3text19
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext5",
				textdata: data.string.p3text20
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext6",
				textdata: data.string.p3text22
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext7",
				textdata: data.string.p3text23
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext8",
				textdata: data.string.p3text24
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext9",
				textdata: data.string.p3text25
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext10",
				textdata: data.string.p3text26
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext11",
				textdata: data.string.p3text27
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont2",
				textclass: "egtext12",
				textdata: data.string.p3text28
			},
			{
				textclass: "answer",
				textdata: data.string.p3text29
			},
			{
				textclass: "crossmultiply",
				textdata: data.string.p3text21
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "smallbuddha",
						imgsrc: imgpath+"buddhameditate.png"
					},
					{
						imgclass: "minuscross1",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "minuscross2",
						imgsrc: imgpath+"cross-line2.png"
					}
				],
				imagelabels:[
					{
						datahighlightflag: true,
						datahighlightcustomclass:"bigfont",
						imagelabelclass:"ruletitle",
						imagelabeldata: data.string.p3text1
					}
				]
			}
		],
		lowertextblockadditionalclass:"ruleblock",
		lowertextblock:[
			{
				datahighlightflag:true,
				datahighlightcustomclass:"bigfont3",
				textclass: "middletextparagraph",
				textdata: data.string.p3text14
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"multiplehighlightslide2",
				textclass: "ruledesc1_1 cssfadein1",
				textdata: data.string.p3text15
			},
			{
				textclass: "equal1_1 cssfadein2",
				textdata: data.string.equal
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass:"multiplehighlightslide3",
				textclass: "ruledesc2_1 cssfadein3",
				textdata: data.string.p3text16
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass:"bigfont",
				textclass : "endingtext cssfadein",
				textdata  : data.string.p3text30,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "buddhanamaste",
						imgsrc: imgpath+"buddhanamaste.png"
					}
				]
			}
		]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("twohalvestitledetailcontent", $("#twohalvestitledetailcontent-partial").html());


	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		if(countNext==1 || countNext==2 || countNext==3){
			$nextBtn.hide(0);
		}

		$(".ruledesc2").on(animationend, function(){
			$(".exampleblock").addClass("moverightanim");
			$(".exampletext1").addClass("cssfadein1");
		});

		$(".exampletext1").on(animationend, function(){
			$(".rulesubheading2").addClass('moveleftanim');
			$(".ruledesc1_2").addClass('cssfadein2');
			$(".equal2").addClass('cssfadein3');
			$(".ruledesc2_2").addClass('cssfadein4');
		});

		$(".ruledesc2_2").on(animationend, function(){
			$(".exampletext2, .showsolution").addClass('cssfadein');
		});

		$(".showsolution").on("click", function(){
			$(this).hide(0);
			$(".cross1, .cross2, .cross3, .cross4").addClass("cssfadein");
			$(".solution1, .solution2").addClass("cssfadein1");
			$nextBtn.delay(2000).show(0);
		});

		if (countNext==2) {
			$(".exampleblock").on(animationend, function(){
				$(".exampletext1_1, .exampletext1_2, .exampletext1_3").addClass("cssfadein");
			});

			$(".exampletext1_3").on(animationend, function(){
				$(".rulesubheading2").addClass('moveleftanim');
				$(".ruledesc1_2").addClass('cssfadein2');
				$(".equal2").addClass('cssfadein3');
				$(".ruledesc2_2").addClass('cssfadein4');
				$(".exampletext2_1, .exampletext2_2, .exampletext2_3, .exampletext2_4").addClass("cssfadein4");
			});

			$(".exampletext2_4").on(animationend, function(){
				$(".showsolution2").addClass("cssfadein");
			});

			$(".showsolution2").on("click", function(){
				$(this).hide(0);
				$(".cross5, .cross6, .cross8, .cross9").addClass("cssfadein");
				$(".solution3, .solution4").addClass("cssfadein1");
				$nextBtn.delay(2000).show(0);
			});
		};

		if(countNext==3){
			// $(".ruledesc2_1").on(animationend, function(){
			// 	$(".exampleblock2").addClass("moverightanim");
			// });

			// $(".exampleblock2").on(animationend, function(){
				$(".egtext1, .egtext2, .egtext3, .egtext4, .egtext5").addClass("cssfadein");
				$(".crossmultiply").addClass("cssfadein1");
				$(".exampleblock2").addClass("moverightanim");
			// });

			$(".crossmultiply").on("click", function(){
				$(this).hide(0);
				$(".egtext6").addClass("cssfadein");
				$(".egtext7").addClass("cssfadein1");
				$(".egtext8").addClass("cssfadein2");
				$(".egtext9").addClass("cssfadein3");
				$(".egtext10, .minuscross1, .minuscross2, .egtext11, .egtext12, .answer").addClass("cssfadein4");
			});
			$(".answer").on(animationend, function(){
				$nextBtn.delay(1000).show(0);
			});
		}

	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page4/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";
var observationbuttonpath = "images/playbtn.png";

var content=[
	{
		uppertextblock : [
			{
				textclass : "textpinkbandstyle",
				textdata  : data.string.p4text1,
			}
		]
	},
	{
		contentblocknocenteradjust: true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sadsmiley",
						imgsrc: imgpath+"teary-eyes.jpg"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "mrX",
				textdata: data.string.p4text2
			},
			{
				textclass: "para1",
				textdata: data.string.p4text3
			},
			{
				textclass: "para1_1 cssfadein",
				textdata: data.string.p4text3_1
			},
			{
				textclass: "para2 cssfadein2",
				textdata: data.string.p4text4
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgsrc: imgpath+"clouds_game_bg.jpg",
						imgclass: "fullbg"
					},
					{
						imgclass: "informationcloud cssmoveright",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds1",
						imgsrc: imgpath+"clouds01.png"
					},
					{
						imgclass: "clouds2",
						imgsrc: imgpath+"clouds01.png"
					},
					{
						imgclass: "clouds3",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds4",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds5",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds6",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds7",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds8",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "crossline1",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crossline2",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "sadface",
						imgsrc: imgpath+"sad-face.png"
					},
					{
						imgclass: "happyface",
						imgsrc: imgpath+"happy-face.png"
					},
					{
						imgclass: "crossline3",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crossline4",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crossline5",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crossline6",
						imgsrc: imgpath+"cross-line2.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "lefthandside",
						imagelabeldata: data.string.lefthandside
					},
					{
						imagelabelclass: "righthandside",
						imagelabeldata: data.string.righthandside
					}
				]
			}
		],
		customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"lhs",
                        dropelementdata : "positivefour",
                    },
                    {
                        dropclass:"rhs",
                        dropelementdata : "positivefour",
                    },
                    {
                        dropclass:"lhs2",
                        dropelementdata : "dividefour",
                    },
                    {
                        dropclass:"rhs2",
                        dropelementdata : "dividefour",
                    }
                ],
                dragareaelement :[
                    {
                        dragelementId : "positivefour",
                        dragelementText : data.string.p4option1,
                    },
                    {
                        dragelementId : "positivefour",
                        dragelementText : data.string.p4option1,
                    },
                    {
                        dragelementId : "option2",
                        dragelementText : data.string.p4option2,
                    },
                    {
                        dragelementId : "option3",
                        dragelementText : data.string.p4option3,
                    },
                    {
                        dragelementId : "dividefour",
                        dragelementText : data.string.p4option4,
                    },
                    {
                        dragelementId : "dividefour",
                        dragelementText : data.string.p4option4,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ],
        typewritingblock:[
        	{
        		typewritingidname: "typewriter1"
        	},
        	{
        		typewritingidname: "typewriter2"
        	}
        ],
		lowertextblock:[
			{
				textclass: "informationboard cssmoveright",
				textdata: data.string.p4text6
			},
			{
				textclass: "informationboard2",
				textdata: data.string.p4text7
			},
			{
				textclass: "question1",
				textdata: data.string.p4text2_1
			},
			{
				textclass: "question2",
				textdata: data.string.p4text2_3
			},
			{
				textclass: "question2_1",
				textdata: data.string.p4text2_4
			},
			{
				textclass: "question3",
				textdata: data.string.p4text2_5
			},
			{
				textclass: "question4",
				textdata: data.string.p4text2_6
			},
			{
				textclass: "question5",
				textdata: data.string.p4text2_6
			},
			{
				textclass: "cutfour",
				textdata: data.string.p4text2_6
			},
			{
				textclass: "answer",
				textdata: data.string.p4answer
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgsrc: imgpath+"clouds_game_bg.jpg",
						imgclass: "fullbg"
					},
					{
						imgclass: "informationcloud cssmoveright",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds1",
						imgsrc: imgpath+"clouds01.png"
					},
					{
						imgclass: "clouds2",
						imgsrc: imgpath+"clouds01.png"
					},
					{
						imgclass: "clouds3",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds4",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds5",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "clouds6",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "cloudscrossmultiply",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "cloudshcf",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "cloudslcm",
						imgsrc: imgpath+"clouds.png"
					},
					{
						imgclass: "crosslinesecond1",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond2",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond3",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond4",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond5",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond6",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond7",
						imgsrc: imgpath+"cross-line2.png"
					},
					{
						imgclass: "crosslinesecond8",
						imgsrc: imgpath+"cross-line2.png"
					},
          {
              imgclass: "happyface",
              imgsrc: imgpath+"happy-face.png"
          },
          {
              imgclass: "handIcon",
              imgsrc: imgpath+"hand-icon.gif"
          },
				],
				imagelabels:[
					{
						imagelabelclass: "lefthandside2",
						imagelabeldata: data.string.lefthandside
					},
					{
						imagelabelclass: "righthandside2",
						imagelabeldata: data.string.righthandside
					},

				]
			}
		],
		customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"lhs_1",
                        dropelementdata : "negativesix",
                    },
                    {
                        dropclass:"rhs_1",
                        dropelementdata : "negativesix",
                    },
                    {
                        dropclass:"lhs_2",
                        dropelementdata : "fourX",
                    },
                    {
                        dropclass:"rhs_2",
                        dropelementdata : "fourX",
                    },
                    {
                        dropclass:"lhs_3",
                        dropelementdata : "dividetwo",
                    },
                    {
                        dropclass:"rhs_3",
                        dropelementdata : "dividetwo",
                    }
                ],
                dragareaelement :[
                    {
                        dragelementId : "negativesix",
                        dragelementText : data.string.p4q2option1,
                    },
                    {
                        dragelementId : "negativesix",
                        dragelementText : data.string.p4q2option1,
                    },
                    {
                        dragelementId : "fourX",
                        dragelementText : data.string.p4q2option2,
                    },
                    {
                        dragelementId : "fourX",
                        dragelementText : data.string.p4q2option2,
                    },
                    {
                        dragelementId : "dividetwo",
                        dragelementText : data.string.p4q2option3,
                    },
                    {
                        dragelementId : "dividetwo",
                        dragelementText : data.string.p4q2option3,
                    },
                    {
                        dragelementId : "multiply2",
                        dragelementText : data.string.p4q2option4,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ],
        typewritingblock:[
        	{
        		typewritingidname: "typewriter2_1"
        	},
        	{
        		typewritingidname: "typewriter2_2"
        	},
        	{
        		typewritingidname: "typewriter2_3"
        	},
        	{
        		typewritingidname: "typewriter2_4"
        	},
        	{
        		typewritingidname: "typewriter2_5"
        	},
        	{
        		typewritingidname: "typewriter2_6"
        	}
        ],
		lowertextblock:[
			{
				textclass: "informationboard cssmoveright",
				textdata: data.string.p4text8
			},
			{
				textclass: "crossmultiplytext",
				textdata: data.string.crossmultiply
			},
			{
				textclass: "informationboard2",
				textdata: data.string.p4text7
			},
			{
				textclass: "secondquestion1",
				textdata: data.string.p4text9
			},
			{
				textclass: "secondquestion1_1",
				textdata: data.string.p4text9_1
			},
			{
				textclass: "equalsign",
				textdata: data.string.equal
			},
			{
				textclass: "secondquestion2",
				textdata: data.string.p4text9_2
			},
			{
				textclass: "secondquestion2_2",
				textdata: data.string.p4text9_3
			},
			{
				textclass: "secondquestion9",
				textdata: data.string.p4text10
			},
			{
				textclass: "secondquestion10",
				textdata: data.string.p4text10
			},
			{
				textclass: "cutone",
				textdata: data.string.p4text12
			},
			{
				textclass: "answer",
				textdata: data.string.p4text11
			}
		]
	},
	{
		uppertextblock:[
			{
				textclass: "onlyparatextstyle",
				textdata: data.string.goodjob
			}
		]
	}
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("dragdropcontent", $("#dragdropcontent-partial").html());


	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	// function for typewriter, for a id-passed as string, given some data, speed of typing-optional
		// and a callback function -optional to call on complete
		function typewriter(id, data, speed, oncomplete){
			var str = data,
		    i = 0,
		    text,
		    typespeed,
		    typevariable,
		    strlen = str.length;

		    // alert(strlen.length);

		    // set the typespeed if provided set as it is or speed = 0 is default as 40, else give default speed 50
		    if (typeof speed === "undefined" || speed === 0) {
					typespeed = 10;
				} else {
					typespeed = speed;
				}

			// function to type
			(function type() {
			    text = str.slice(0, ++i);

			    $("#"+id).html(text);

			    if (text === str) {
			    	/*if oncomplete function is provided call it and return
			    		else just return*/
			    	if (typeof oncomplete === "undefined") {
						clearInterval(typevariable);
					} else {
						oncomplete();
						clearInterval(typevariable);
					}
			    	return;
			    }

		        typevariable = setTimeout(type, typespeed);
			}());
		};
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		if(countNext==2){
			$nextBtn.hide(0);
		}

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".informationcloud").on(animationend, function(){
			$(".lhs, .rhs, .lefthandside, .righthandside").addClass('cssfadein2');
		});


		$(".righthandside").on(animationend, function(){
			$("#option2, #positivefour, #positivefourtwo, #option3, #dividefour, #dividefouragain").addClass("cssfadein");
		});

		$("#dividefour").on(animationend, function(){
			$(".informationboard2").addClass("cssfadein");
		});

		var $recognizenumber = $board.children('div.dragnumberblock');
        var $draggableCollector = $('.droppablecollector');
        var $droppables = $(".droppablecollector > p");
        var $draggables = $(".draggablecollector > p");

        var dropcount = 0;
        var totaldroprequired = $droppables.length;
        // alert(totaldroprequired);

        $draggables.draggable({containment: $recognizenumber, revert:"invalid"});

        /*eureka droppable this is nice use of droppable please see the use*/
             $droppables.droppable({ tolerance: "pointer",
        accept : function(dropElem){
            //dropElem was the dropped element, return true or false to accept/refuse
            $(".informationboard2").removeClass("cssfadein");
            if($(this).attr('data-dropelementcaptiondata') === dropElem.attr("id")){
                return true;
            }
        },
        drop : function (event, ui) {
            $(this).html($(ui.draggable).html());
            $(this).addClass('droppableafterdrop');
            $(ui.draggable).css('display', 'none');
               if(++dropcount == 2){
	            	// alert(dropcount);
                    $(".droppablecollector").children('p:nth-of-type(1)').delay(1000).hide(0);
                    $(".droppablecollector").children('p:nth-of-type(2)').delay(1000).hide(0);
                    $(".question1").addClass('cssfadeout');
                    typewriter("typewriter1", data.string.p4typewrite1, 150);
                    // $nextBtn.show(0);
                }

                $(".question1").on(animationend, function(){
                	$(".crossline1, .crossline2").addClass("cssfadein4");
                });
                $(".crossline2").on(animationend, function(){
                    $("#typewriter1").addClass('cssfadeout');
                	$(".crossline1, .crossline2").removeClass("cssfadein4");
                    typewriter("typewriter2", data.string.p4typewrite2, 150);
                	$(".droppablecollector").children('p:nth-of-type(3)').delay(2000).show(0);
                    $(".droppablecollector").children('p:nth-of-type(4)').delay(2000).show(0);
                });

                if (dropcount==4) {
                	$(".question4").addClass("cssfadein");
                	$(".question5").addClass("cssfadein");
                	// $(".question3").addClass("showtext");
                	$("#typewriter2").addClass("cssgoup");
                	$(".sadface").addClass("cssgoupagain");
                }

                $(".sadface").on(animationend, function(){
                	$(".crossline3, .crossline4, .crossline5, .crossline6").addClass("cssfadein2");
                });

                $(".crossline6").on(animationend, function(){
                	$(".cutfour").addClass("cssfadein3");
                });

                $(".cutfour").on(animationend, function(){
                	$(this).hide(0);
                	$(".answer, .happyface").addClass("cssfadein2");
                	$(".question4, .question5").removeClass('cssfadein');
                	$(".crossline3, .crossline4, .crossline5, .crossline6, .cutfour").removeClass("cssfadein2");
                	$(".sadface, #typewriter2").hide(0);
                });

                $(".answer").on(animationend, function(){
                	$nextBtn.show(0);
                });
            }
        });
	}

	function generaltemplatetwo() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		if(countNext==2 || countNext==3){
			$nextBtn.hide(0);
		}

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".informationcloud").on(animationend, function(){
			$(".cloudscrossmultiply").addClass("cssfadein2");
		});

		$(".cloudscrossmultiply").on(animationend, function(){
			$(".crossmultiplytext").addClass("cssfadein");
			$(".handIcon").delay(1000).fadeIn(0);
			//$(".informationcloud, .informationboard").addClass("cssmoverightagain");
		});

		$(".crossmultiplytext, .handIcon").on("click", function(){
			$(".cloudscrossmultiply, .crossmultiplytext, .handIcon").delay(200).hide(0);
            typewriter("typewriter2_1", data.string.p4text9_4, 100);
			$("#typewriter2_1").addClass("cssfadeout");
            typewriter("typewriter2_2", data.string.p4text9_5, 150);
			$(".lefthandside2, .righthandside2, .lhs_1, .rhs_1").addClass("cssfadein4");
			$(".secondquestion1, .secondquestion1_1, .equalsign, .secondquestion2, .secondquestion2_2").addClass("cssfadeout");
		});



		$(".rhs_1").on(animationend, function(){
			$("#negativesix, #negativesixagain, #fourX, #fourXagain, #multiply2, #dividetwo, #dividetwoagain").addClass("cssfadein");
		});


		var $recognizenumber = $board.children('div.dragnumberblock');
        var $draggableCollector = $('.droppablecollector');
        var $droppables = $(".droppablecollector > p");
        var $draggables = $(".draggablecollector > p");

        var dropcount = 0;
        var totaldroprequired = $droppables.length;
        // alert(totaldroprequired);

        $draggables.draggable({containment: $recognizenumber, revert:"invalid"});

        /*eureka droppable this is nice use of droppable please see the use*/
             $droppables.droppable({ tolerance: "pointer",
        accept : function(dropElem){
            //dropElem was the dropped element, return true or false to accept/refuse
            $(".informationboard2").removeClass("cssfadein");
            if($(this).attr('data-dropelementcaptiondata') === dropElem.attr("id")){
                return true;
            }
        },
        drop : function (event, ui) {
            $(this).html($(ui.draggable).html());
            $(this).addClass('droppableafterdrop');
            $(ui.draggable).css('display', 'none');
               if(++dropcount == 2){
	            	// alert(dropcount);
                    $(".droppablecollector").children('p:nth-of-type(1)').delay(1000).hide(0);
                    $(".droppablecollector").children('p:nth-of-type(2)').delay(1000).hide(0);
            		typewriter("typewriter2_3", data.string.p4text9_6, 100);
                    $("#typewriter2_2").addClass('cssfadeout');
                	$(".crosslinesecond1, .crosslinesecond2").addClass("cssfadein4");
                }

                $(".crosslinesecond2").on(animationend, function(){
            		typewriter("typewriter2_4", data.string.p4text9_7, 150);
                    $("#typewriter2_3").addClass('cssfadeout');
                	$(".crosslinesecond1, .crosslinesecond2").removeClass("cssfadein4");
                	$(".droppablecollector").children('p:nth-of-type(3)').delay(2500).show(0);
                    $(".droppablecollector").children('p:nth-of-type(4)').delay(2500).show(0);
                });

                if (dropcount==4) {
                	$(".droppablecollector").children('p:nth-of-type(3)').delay(1000).hide(0);
                    $(".droppablecollector").children('p:nth-of-type(4)').delay(1000).hide(0);
            		typewriter("typewriter2_5", data.string.p4text9_8, 150);
                    $("#typewriter2_4").addClass('cssfadeout');
                	$(".crosslinesecond3, .crosslinesecond4").addClass("cssfadein5");
                }

                $(".crosslinesecond4").on(animationend, function(){
                	$(".secondquestion7").removeClass("cssfadein");
                	$(".crosslinesecond3, .crosslinesecond4").removeClass("cssfadein5");
                	$("#typewriter2_5").addClass("cssfadeout");
            		typewriter("typewriter2_6", data.string.p4text9_9, 150);
                	$(".droppablecollector").children('p:nth-of-type(5)').delay(2500).show(0);
                    $(".droppablecollector").children('p:nth-of-type(6)').delay(2500).show(0);
                });

                if(dropcount==6){
                	$(".droppablecollector").children('p:nth-of-type(5)').delay(2500).hide(0);
                    $(".droppablecollector").children('p:nth-of-type(6)').delay(2500).hide(0);
                	$("#typewriter2_6").addClass("cssgoup");
                	$(".secondquestion9").addClass("cssfadein2");
                	$(".secondquestion10").addClass("cssfadein2");

                }

                $(".secondquestion10").on(animationend, function(){
                	$(".crosslinesecond5, .crosslinesecond6, .crosslinesecond7, .crosslinesecond8").addClass("cssfadein");
                	$(".cutone").addClass("cssfadein2");
                });

                $(".cutone").on(animationend, function(){
                	$(this).hide(0);
                	$(".answer, .happyface").addClass("cssfadein2");
                	$(".secondquestion9, .secondquestion10").removeClass('cssfadein2');
                	$(".crosslinesecond5, .crosslinesecond6, .crosslinesecond7, .crosslinesecond8").removeClass("cssfadein");
                	$("#typewriter2_6").hide(0);
                });

                $(".answer").on(animationend, function(){
                	$nextBtn.show(0);
                });
            }
        });
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();
		switch(countNext){
			case 3  : generaltemplatetwo(); break;
			default : generaltemplate(); break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

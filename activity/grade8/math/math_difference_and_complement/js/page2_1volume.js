



$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=6;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [

	{

		justClass : "first",
		animate : "true",
		text : [

		data.string.p1_20,

		data.string.p1_21,

		data.string.p1_22,
		data.string.p1_23,
		data.string.p1_24,
		data.string.p1_25,
		data.string.p1_26,

		data.string.p1_27,
		data.string.p1_28,
		data.string.p1_29,
		data.string.p1_30,
		data.string.p1_31,
		data.string.p1_32,
		data.string.p1_33,

		data.string.p1_34,
		data.string.p1_35,
		data.string.p1_36,
		data.string.p1_37,
		data.string.p1_38,

		data.string.p1_39,
		data.string.p1_40,
		data.string.p1_41,
		data.string.p1_42,

		data.string.p1_43,
		data.string.p1_44,
		data.string.p1_45,
		data.string.p1_46,

		data.string.p1_47,
		data.string.p1_48,
		data.string.p1_49,
		data.string.p1_50,

		data.string.p1_51,
		data.string.p1_52,
		data.string.p1_53,
		data.string.p1_54



		],
	},
	];



/******************************************************************************************************/


		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".rectangle").hide(0);
			$(".common").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".uni").hide(0);
			$(".set1").hide(0);
			$(".set2").hide(0);
			$(".set3").hide(0);
			$(".set4").hide(0);
			$(".set5").hide(0);
			$(".set6").hide(0);
			$(".set7").hide(0);

			var set1=false;
			var set2=false;
			var set3=false;
			var set4=false;
			var set5=false;
			var set6=false;
			var set7=false;
			var HoverCount = 0;

		$(".hoverableset").hover(function() {
		/* Stuff to do when the mouse enters the element */
		var overThat = $(this).attr("class");
		console.log(overThat);
		switch(overThat){
			case "uni hoverableset": if (set1 = true){
			$("."+overThat).css({"color":"purple"});
			$(".wrapperIntro.first .text7").show(0);
			$(".setU").show(0);
			$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			HoverCount++;break;}

			case "set-a hoverableset": if (set2 = true){
				$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text8").show(0);$(".circle1").css({'background': 'yellow',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			HoverCount++;break;}

			case "set-b hoverableset": if (set3 = true){
			$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text9").show(0);$(".circle2").css({'background': 'red',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			HoverCount++;break;}

			case "circle1 hoverableset": if (set4 = true){
				$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text10").show(0);$(".blue").show(0);
				$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".pink").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
				HoverCount++;break;}

			case "circle2 hoverableset": if (set5 = true){
				$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text11").show(0);$(".pink").show(0);
				$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".blue").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
				HoverCount++;break;}

			case "common hoverableset": if (set6 = true){
				$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text12").show(0);$(".common").css({'background': '#8B15F3',
				'opacity': '0.3'});
			$(".common1").show(0);
			$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			HoverCount++;break;}

			case "rectangle hoverableset": if (set7 = true){
				$("."+overThat).css({"color":"purple"});$(".wrapperIntro.first .text13").show(0);$(".rectangle1").show(0);
				$(".circle1").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
			'opacity': '0.5'});
			$(".setU").hide(0);
			$(".blue").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".pink").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
				HoverCount++;break;}
			default:break;
		}


		if(HoverCount > 7 && set1 == true && set2 == true && set3 == true && set4 == true && set5 == true && set6 == true && set7 == true){
			$(".nextBtn.mynextStyle").show(0);
			console.log(HoverCount);
		}
	});
			/*$(".common").click(function(){
				$(".wrapperIntro.first .text7").hide(0);
				$(".wrapperIntro.first .text8").hide(0);
				$(".wrapperIntro.first .text9").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text11").hide(0);
				$(".wrapperIntro.first .text13").hide(0);
				$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
				$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
				$(".blue").hide(0);
				$(".pink").hide(0);
				$(".setU").hide(0);
				$(".rectangle1").hide(0);
				$(".wrapperIntro.first .text12").show(0);
				$(".common").css({'background': '#8B15F3',
				'opacity': '0.3'});
				$(".common1").show(0);
			});


			$(".set-a").click(function(){
				$(".wrapperIntro.first .text7").hide(0);
				$(".wrapperIntro.first .text9").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text11").hide(0);
				$(".wrapperIntro.first .text12").hide(0);
				$(".wrapperIntro.first .text13").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".setU").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text8").show(0);
			$(".circle1").css({'background': 'yellow',
				'opacity': '0.5'});
			});


			$(".set-b").click(function() {
				$(".wrapperIntro.first .text7").hide(0);
				$(".wrapperIntro.first .text8").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text11").hide(0);
				$(".wrapperIntro.first .text12").hide(0);
				$(".wrapperIntro.first .text13").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".setU").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text9").show(0);
			$(".circle2").css({'background': 'red',
				'opacity': '0.5'});
			});


			$(".circle1").click(function(){
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".pink").hide(0);
			$(".setU").hide(0);
			$(".rectangle1").hide(0);
			$(".wrapperIntro.first .text10").show(0);
			$(".blue").show(0);
			});


			$(".circle2").click(function(){
				$(".wrapperIntro.first .text7").hide(0);
				$(".wrapperIntro.first .text9").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text8").hide(0);
				$(".wrapperIntro.first .text12").hide(0);
				$(".wrapperIntro.first .text13").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".blue").hide(0);
			$(".setU").hide(0);
			$(".rectangle1").hide(0);
			$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
				$(".wrapperIntro.first .text11").show(0);
				$(".pink").show(0);
			});


			$(".uni").click(function() {
				$(".wrapperIntro.first .text8").hide(0);
				$(".wrapperIntro.first .text9").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text11").hide(0);
				$(".wrapperIntro.first .text12").hide(0);
				$(".wrapperIntro.first .text13").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
				$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".rectangle1").hide(0);
				$(".wrapperIntro.first .text7").show(0);
				$(".setU").show(0);
			});


			$(".rectangle").click(function() {
				$(".wrapperIntro.first .text7").hide(0);
				$(".wrapperIntro.first .text9").hide(0);
				$(".wrapperIntro.first .text10").hide(0);
				$(".wrapperIntro.first .text11").hide(0);
				$(".wrapperIntro.first .text12").hide(0);
				$(".wrapperIntro.first .text8").hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").hide(0);
			$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".setU").hide(0);
				$(".wrapperIntro.first .text13").show(0);
				$(".rectangle1").show(0);
			});*/


		/*for hover*/

		/*$(".common").on({
			mouseover:function(){
			$(".wrapperIntro.first .text12").stop().show(0);
			$(".common").css({'background': '#8B15F3',
				'opacity': '0.3'});
				$(".common1").stop().show(0);
			},

			mouseout: function() {
			$(".wrapperIntro.first .text12").stop().hide(0);
			$(".common").css({'background': 'transparent',
				'opacity': '0.3'});
			$(".common1").stop().hide(0);
			}
		});

		$(".set-a").on({
    	 mouseover: function(){
       	 $(".wrapperIntro.first .text8").stop().show(0);
		 $(".circle1").css({'background': 'yellow',
				'opacity': '0.5'});

   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text8").stop().hide(0);
		$(".circle1").css({'background': 'transparent',
				'opacity': '0.5'});
    	}
		});

		 $(".set-b").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.first .text9").stop().show(0);
		 $(".circle2").css({'background': 'red',
				'opacity': '0.5'});

   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text9").stop().hide(0);
		$(".circle2").css({'background': 'transparent',
				'opacity': '0.5'});
    	}
		});


		$(".circle1").on({
    	 mouseover: function(){
       	 $(".wrapperIntro.first .text10").stop().show(0);
		 $(".blue").stop().show(0);
   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text10").stop().hide(0);
		$(".blue").stop().hide(0);
    	}
		});

		 $(".circle2").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.first .text11").stop().show(0);
		 $(".pink").stop().show(0);
   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text11").stop().hide(0);
		$(".pink").stop().hide(0);
    	}
		});

		$(".uni").on({
    		mouseover: function() {
       	 $(".wrapperIntro.first .text7").stop().show(0);
		 $(".setU").stop().show(0);
   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text7").stop().hide(0);
		$(".setU").stop().hide(0);
    	}
		});

		$(".rectangle").on({
    		mouseover: function() {
       	 $(".wrapperIntro.first .text13").stop().show(0);
		 $(".rectangle1").stop().show(0);
   		 },

    	mouseout: function() {
        $(".wrapperIntro.first .text13").stop().hide(0);
		$(".rectangle1").stop().hide(0);
    	}
		});*/

		};

		/*function first02() {
			//$board.find(".wrapperIntro.firstPage .text0").hide(0);
		 	$board.find(".wrapperIntro.first .text0").hide(0);
		 	$board.find(".wrapperIntro.first .text2").show(0);
			$(".first").hide(0);
			$(".rectangle").hide(0);
			$(".common").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".uni").hide(0);
			$(".set1").hide(0);
			$(".set2").hide(0);
			$(".set3").hide(0);
			$(".set4").hide(0);
			$(".set5").hide(0);
			$(".set6").hide(0);
			$(".set7").hide(0);

		}*/

		function first03() {
			$board.find(".wrapperIntro.first .text0").hide(0);
			$board.find(".venndiag").hide(0);
		 	$board.find(".wrapperIntro.first .text1").hide(0);
		 	$board.find(".wrapperIntro.first .text2").show(0);
		 	$board.find(".wrapperIntro.first .text3").show(0);
		 	$board.find(".wrapperIntro.first .text4").show(0);
		 	$board.find(".wrapperIntro.first .text5").show(0);
		 	$board.find(".wrapperIntro.first .text6").show(0);
			$(".first").show(0);
			$(".rectangle").show(0);
			$(".common").show(0);
			$(".circle1").show(0);
			$(".circle2").show(0);
			$(".set-a").show(0);
			$(".set-b").show(0);
			$(".uni").show(0);
			$(".set1").show(0);
			$(".set2").show(0);
			$(".set3").show(0);
			$(".set4").show(0);
			$(".set5").show(0);
			$(".set6").show(0);
			$(".set7").show(0);
			$(".nextBtn.myNextStyle").hide(0);
			setTimeout(function() {
				$(".nextBtn.myNextStyle").show(0);
			}, 5000);


		}

		function first04() {
			console.log("FRAME 4");
			$board.find(".venndia").hide(0);
		 	// $board.find(".wrapperIntro.first .text2").hide(0);
		 	$board.find(".wrapperIntro.first .text3").hide(0);
		 	$board.find(".wrapperIntro.first .text4").hide(0);
		 	$board.find(".wrapperIntro.first .text5").hide(0);
		 	$board.find(".wrapperIntro.first .text6").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			$(".rectangle").hide(0);
			$(".common").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".uni").hide(0);
			$(".set1").hide(0);
			$(".set2").hide(0);
			$(".set3").hide(0);
			$(".set4").hide(0);
			$(".set5").hide(0);
			$(".set6").hide(0);
			$(".set7").hide(0);
			$(".common").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".rectangle1").hide(0);
			$(".uni").hide(0);
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".setU").hide(0);
			$("#describe").hide(0);
		 	$(".regions_venn").show(0);
		}

		function first04() {
			console.log("FRAME 5");

			$board.find(".venndia").hide(0);
		 	// $board.find(".wrapperIntro.first .text2").hide(0);
		 	$board.find(".wrapperIntro.first .text3").hide(0);
		 	$board.find(".wrapperIntro.first .text4").hide(0);
		 	$board.find(".wrapperIntro.first .text5").hide(0);
		 	$board.find(".wrapperIntro.first .text6").hide(0);
			$(".wrapperIntro.first .text7").hide(0);
			$(".wrapperIntro.first .text9").hide(0);
			$(".wrapperIntro.first .text10").hide(0);
			$(".wrapperIntro.first .text11").hide(0);
			$(".wrapperIntro.first .text12").hide(0);
			$(".wrapperIntro.first .text8").hide(0);
			$(".wrapperIntro.first .text13").hide(0);
			$(".rectangle").hide(0);
			$(".common").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".uni").hide(0);
			$(".set1").hide(0);
			$(".set2").hide(0);
			$(".set3").hide(0);
			$(".set4").hide(0);
			$(".set5").hide(0);
			$(".set6").hide(0);
			$(".set7").hide(0);
			$(".common").hide(0);
			$(".set-a").hide(0);
			$(".set-b").hide(0);
			$(".circle1").hide(0);
			$(".circle2").hide(0);
			$(".rectangle1").hide(0);
			$(".uni").hide(0);
			$(".blue").hide(0);
			$(".pink").hide(0);
			$(".setU").hide(0);
			$("#describe").hide(0);
		 	$(".regions_venn").show(0);

			$board.find(".regions_venn").hide(0);
		 	// $board.find(".wrapperIntro.first .text2").hide(0);
		 	$board.find(".wrapperIntro.first .text7").hide(0);
		 	$board.find(".wrapperIntro.first .text8").hide(0);
		 	$board.find(".wrapperIntro.first .text9").hide(0);
		 	$board.find(".wrapperIntro.first .text10").hide(0);
		 	$board.find(".wrapperIntro.first .text11").hide(0);
		 	$board.find(".wrapperIntro.first .text12").hide(0);
		 	$board.find(".wrapperIntro.first .text13").hide(0);


		 	$board.find(".wrapperIntro.first .text14").show(0);
		 	$board.find(".wrapperIntro.first .text15").show(0);
		 	$board.find(".wrapperIntro.first .text16").show(0);
		 	$board.find(".wrapperIntro.first .text17").show(0);
		 	$board.find(".wrapperIntro.first .text18").show(0);
			$(".set").show(0);

		}

		function first05() {
	 		console.log("FRAME 6");
		 	$board.find(".wrapperIntro.first .text15").hide(0);
		 	$board.find(".wrapperIntro.first .text16").hide(0);
		 	$board.find(".wrapperIntro.first .text17").hide(0);
		 	$board.find(".wrapperIntro.first .text18").hide(0);

		 	$board.find(".wrapperIntro.first .text19").show(0);
		 	//$board.find(".wrapperIntro.first .text20").show(0);
		 	//$board.find(".wrapperIntro.first .text21").show(0);
		 	//$board.find(".wrapperIntro.first .text22").show(0);
			$(".dig01").show(0);
			$(".set").hide(0);

		}

		function first06() {
		 	$board.find(".wrapperIntro.first .text19").hide(0);
		 	$board.find(".wrapperIntro.first .text20").hide(0);
		 	$board.find(".wrapperIntro.first .text21").hide(0);
		 	$board.find(".wrapperIntro.first .text22").hide(0);
			$(".dig01").hide(0);
			$(".dig02").show(0);
		   // $board.find(".wrapperIntro.first .text23").show(0);
		 	$board.find(".wrapperIntro.first .text24").show(0);
		 	//$board.find(".wrapperIntro.first .text25").show(0);
		 	//$board.find(".wrapperIntro.first .text26").show(0);

		}

		function first07() {
		 	//$board.find(".wrapperIntro.first .text27").show(0);
		 	//$board.find(".wrapperIntro.first .text28").show(0);
		 	$board.find(".wrapperIntro.first .text29").show(0);
		 	//$board.find(".wrapperIntro.first .text30").show(0);
			$(".dig02").hide(0);
			$(".dig03").show(0);

		    $board.find(".wrapperIntro.first .text23").hide(0);
		 	$board.find(".wrapperIntro.first .text24").hide(0);
		 	$board.find(".wrapperIntro.first .text25").hide(0);
		 	$board.find(".wrapperIntro.first .text26").hide(0);

		}


		function first08() {
		 	//$board.find(".wrapperIntro.first .text31").show(0);
		 	//$board.find(".wrapperIntro.first .text32").show(0);
		 	//$board.find(".wrapperIntro.first .text33").show(0);
		 	$board.find(".wrapperIntro.first .text34").show(0);
			$(".dig03").hide(0);
			$(".dig04").show(0);

		    $board.find(".wrapperIntro.first .text27").hide(0);
		 	$board.find(".wrapperIntro.first .text28").hide(0);
		 	$board.find(".wrapperIntro.first .text29").hide(0);
		 	$board.find(".wrapperIntro.first .text30").hide(0);

		}



/*----------------------------------------------------------------------------------------------------*/




		// first func call

		first();





	/*click functions*/
		$nextBtn.on('click',function () {
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				//first02,
				first03,
				first04,
				first05,
				first06,
				first07,
				first08,
			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {

			}
		}


});

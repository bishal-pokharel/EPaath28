/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=2;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [


		data.string.p1_55,
		data.string.p1_56,
		data.string.p1_57,
		data.string.p1_58,
		data.string.p1_59,

		data.string.p1_60,
		data.string.p1_61,

		data.string.p1_62,
		data.string.p1_63,
		data.string.p1_63_1,
		data.string.p1_63_2,
		data.string.p1_63_3,
		data.string.p1_63_4,
		data.string.p1_63_5,
		data.string.p1_63_6

		],

	},


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".loku").show(0);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$(".dig01").show(0);
			$(".nextBtn.mynextStyle").hide(0);
			};


			function first02() {

			//$board.find(".wrapperIntro.firstPage .text0").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text3").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$(".dig01").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
		 	$board.find(".wrapperIntro.firstPage .text8").show(0);
			$board.find(".wrapperIntro.firstPage .text9").show(0);
			$board.find(".wrapperIntro.firstPage .text10").show(0);
			$(".dig05").show(0);
			$(".nextBtn.mynextStyle").hide(0);
		}

		function first03() {

			$board.find(".wrapperIntro.firstPage .text7").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text8").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text9").hide(0);
			$board.find(".wrapperIntro.firstPage .text10").hide(0);
			$(".dig05").hide(0);
			$(".nextBtn.mynextStyle").hide(0);


			$board.find(".wrapperIntro.firstPage .text11").show(0);
		 	$board.find(".wrapperIntro.firstPage .text12").show(0);
			$board.find(".wrapperIntro.firstPage .text13").show(0);
			$board.find(".wrapperIntro.firstPage .text14").show(0);
			$(".dig02").show(0);

		}






/*----------------------------------------------------------------------------------------------------*/




		// first func call
		first();
		$(".wrapperIntro.firstPage .text5").click(function(){
			$(".wrapperIntro.firstPage .text5").addClass("correct");
			$(".nextBtn.mynextStyle").show(0);
			});

		$(".wrapperIntro.firstPage .text3").click(function(){
			$(".wrapperIntro.firstPage .text3").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text4").click(function(){
			$(".wrapperIntro.firstPage .text4").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text6").click(function(){
			$(".wrapperIntro.firstPage .text6").addClass("incorrect");
			});

			$(".wrapperIntro.firstPage .text7").click(function(){
			$(".wrapperIntro.firstPage .text7").addClass("correct");
			$(".nextBtn.mynextStyle").show(0);
			});

		$(".wrapperIntro.firstPage .text8").click(function(){
			$(".wrapperIntro.firstPage .text8").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text9").click(function(){
			$(".wrapperIntro.firstPage .text9").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text10").click(function(){
			$(".wrapperIntro.firstPage .text10").addClass("incorrect");
			});

			$(".wrapperIntro.firstPage .text11").click(function(){
			$(".wrapperIntro.firstPage .text11").addClass("incorrect");

			});

		$(".wrapperIntro.firstPage .text12").click(function(){
			$(".wrapperIntro.firstPage .text12").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text13").click(function(){
			$(".wrapperIntro.firstPage .text13").addClass("incorrect");
			});

		$(".wrapperIntro.firstPage .text14").click(function(){
			$(".wrapperIntro.firstPage .text14").addClass("correct");
			//$(".nextBtn.mynextStyle").show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
			});




	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
				console.log(countNext);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
				first03
			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);

			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

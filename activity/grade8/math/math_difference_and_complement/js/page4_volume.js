/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var vdopath = $ref+"/videos/";
		var vid_play_pause;

		//for play and pause video

	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }

	  var myVideo2 = document.getElementById("myVideo2");

	  if (myVideo2.paused) {
		myVideo2.play();
	 } else {
		myVideo2.pause();
	  }

	  var myVideo3 = document.getElementById("myVideo3");

	  if (myVideo3.paused) {
		myVideo3.play();
	 } else {
		myVideo3.pause();
	  }
	}


	function restart() {

        var video = document.getElementById("myVideo");
        video.currentTime = 0;
		video.play();

		var video2 = document.getElementById("myVideo2");
        video2.currentTime = 0;
		video2.play();

		var video3 = document.getElementById("myVideo3");
        video3.currentTime = 0;
		video3.play();

    }




$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=9;
		$nextBtn.show(0);

		/*to register a partial*/
		Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
		//Handlebars.registerPartial("videocontent1", $("#videocontent1-partial").html());
		Handlebars.registerPartial("videocontent2", $("#videocontent2-partial").html());
		Handlebars.registerPartial("videocontent3", $("#videocontent3-partial").html());

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress(1, 1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p1_64,

		data.string.p1_65,
		data.string.p1_66,
		data.string.p1_67,
		data.string.p1_68,
		data.string.p1_69,
		data.string.p1_70,

		data.string.p1_71,

		data.string.p1_72,
		data.string.p1_73,
		data.string.p1_74,
		data.string.p1_75,
		data.string.p1_76,
		data.string.p1_77,
		data.string.p1_78,
		data.string.p1_79,
		data.string.p1_80,
		data.string.p1_81,
		data.string.p1_82,

		data.string.p1_83,
		data.string.p1_84,
		data.string.p1_85,
		data.string.p1_86,
		data.string.p1_87,
		data.string.p1_88,
		data.string.p1_89,
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "equation1",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"equation01.mp4",
					},
				]
			},
		],


		/*videoblock1 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "aeiou1",
						replaytext : data.string.replay,
						vdeosrc3   : vdopath+"aeiou1.gif",
					},
				]
			},
		],*/

		videoblock2 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "prism",
						replaytext : data.string.replay,
						vdeosrc4   : vdopath+"prism.mp4",
					},
				]
			},
		],

		videoblock3 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "planets",
						replaytext : data.string.replay,
						vdeosrc5   : vdopath+"planets.mp4",
					},
				]
			},
		]
	},



	{
		justClass : "second",
		animate : "true",
		text : [

		data.string.p1_90,
		data.string.p1_91,
		data.string.p1_92,
		data.string.p1_93,
		data.string.p1_94,
		data.string.p1_95,
		data.string.p1_96,
		data.string.p1_97,
		data.string.p1_98,
		data.string.p1_99,
		data.string.p1_100,
		data.string.p1_101,
		data.string.p1_102,
		data.string.p1_103,


		],
	},
]





/******************************************************************************************************/
	/*
	* first
	*/
		function first() {
			console.log("First");
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$(".videoblock").hide(0);
			$(".aeiou1").hide(0);
			$(".videoblock2").hide(0);
			$(".videoblock3").hide(0);
		};

		/*function first02() {
			console.log("Second");
		 	$board.find(".wrapperIntro.firstPage .text0").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text1").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text3").show(0);
			//$board.find(".wrapperIntro.firstPage .text4").show(0);
			//$board.find(".wrapperIntro.firstPage .text5").show(0);
			//$board.find(".wrapperIntro.firstPage .text6").show(0);

		 };*/

		function first03() {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text1").show(0);
		 	console.log("Third");
			$board.find(".wrapperIntro.firstPage .text2").show(0);

			};


		 function first09() {
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$(".videoblock").show(0);
			$(".replay").show(0);
			restart();
		 }

		 function first10() {
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$(".videoblock").hide(0);
			$(".aeiou1").show(0);

		 }

		 function first11() {
			 $board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$(".aeiou1").hide(0);
			$(".videoblock2").show(0);
			restart();

		 }

		 function first12() {
			 $board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$(".videoblock2").hide(0);
			$(".videoblock3").show(0);
			restart();

		 }


		 function first13() {
				$(".replay2").hide(0);
				$(".videoblock3").hide(0);
				$board.find(".wrapperIntro.firstPage .text6").hide(0);
				$board.find(".wrapperIntro.firstPage .text7").show(0);
				$(".sky").show(0);
		 }

		 function first14() {
				$(".sky").hide(0);
				$board.find(".wrapperIntro.firstPage .text7").hide(0);
				$board.find(".wrapperIntro.firstPage .text8").show(0);
				$board.find(".wrapperIntro.firstPage .text9").show(0);
				$board.find(".wrapperIntro.firstPage .text10").show(0);
				$board.find(".wrapperIntro.firstPage .text11").show(0);
				$board.find(".wrapperIntro.firstPage .text12").show(0);
				$board.find(".wrapperIntro.firstPage .text13").show(0);
				$board.find(".wrapperIntro.firstPage .text14").show(0);
				$board.find(".wrapperIntro.firstPage .text15").show(0);
				$board.find(".wrapperIntro.firstPage .text16").show(0);
				$board.find(".wrapperIntro.firstPage .text17").show(0);
				//$board.find(".wrapperIntro.firstPage .text18").show(0);
				$(".nextBtn.mynextStyle").hide(0);

		 };

		 function first15() {

				$board.find(".wrapperIntro.firstPage .text8").show(0);
				$board.find(".wrapperIntro.firstPage .text9").hide(0);
				$board.find(".wrapperIntro.firstPage .text10").hide(0);
				$board.find(".wrapperIntro.firstPage .text11").hide(0);
				$board.find(".wrapperIntro.firstPage .text12").hide(0);
				$board.find(".wrapperIntro.firstPage .text13").hide(0);
				$board.find(".wrapperIntro.firstPage .text14").hide(0);
				$board.find(".wrapperIntro.firstPage .text15").hide(0);
				$board.find(".wrapperIntro.firstPage .text16").hide(0);
				$board.find(".wrapperIntro.firstPage .text17").hide(0);
				$board.find(".wrapperIntro.firstPage .text18").hide(0);

				$board.find(".wrapperIntro.firstPage .text19").show(0);
				$board.find(".wrapperIntro.firstPage .text20").show(0);
				$board.find(".wrapperIntro.firstPage .text21").show(0);
				$board.find(".wrapperIntro.firstPage .text22").show(0);
				$board.find(".wrapperIntro.firstPage .text23").show(0);
				$board.find(".wrapperIntro.firstPage .text24").show(0);
				$board.find(".wrapperIntro.firstPage .text25").show(0);
				//$board.find(".wrapperIntro.firstPage .text26").show(0);

		 };

		 function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);
				$board.find(".wrapperIntro.second .text2").show(0);
				$board.find(".wrapperIntro.second .text3").show(0);
				$board.find(".wrapperIntro.second .text4").show(0);
				$board.find(".wrapperIntro.second .text5").show(0);
				$(".imgs").hide(0);
				$(".circle1").show(0);
				$(".circle2").show(0);
				$(".circle3").show(0);
				$(".rectangle").show(0);
				$(".common1").show(0);
				$(".common2").show(0);
				$(".common3").show(0);
				$(".common4").show(0);
				$(".img02").hide(0);
				$(".img03").hide(0);
				$(".img04").hide(0);
				$(".img05").hide(0);
				$(".img06").hide(0);
				$(".img07").hide(0);
				$(".img08").hide(0);
				$(".img09").hide(0);

			var set1=false;
			var set2=false;
			var set3=false;
			var set4=false;
			var set5=false;
			var set6=false;
			var set7=false;
			var set8=false;
			var HoverCount = 0;

		$(".hoverableset").hover(function() {

		var overThat = $(this).attr("class");
		console.log(overThat);
		switch(overThat){
			case "circle1 hoverableset": if (set1 = true){
			$("."+overThat).css({"color":"purple"});
			$(".wrapperIntro.second .text6").show(0);
			$(".img03").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);

			HoverCount++;break;}

			case "circle2 hoverableset": if (set2 = true){
			$("."+overThat).css({"color":"purple"});
			$(".wrapperIntro.second .text7").show(0);
			$(".img04").show(0);
			$(".img02").hide(0);
			$(".img03").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
			HoverCount++;break;}

			case "circle3 hoverableset": if (set3 = true){
			$("."+overThat).css({"color":"purple"});
			$(".wrapperIntro.second .text8").show(0);
			$(".img09").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img03").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
			HoverCount++;break;}

			case "common1 hoverableset": if (set4 = true){
				$("."+overThat).css({"color":"purple"});
				$(".wrapperIntro.second .text9").show(0);
				$(".img05").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img03").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
				HoverCount++;break;}

			case "common4 hoverableset": if (set5 = true){
				$("."+overThat).css({"color":"purple"});
				$(".wrapperIntro.second .text10").show(0);
				$(".img07").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img03").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
				HoverCount++;break;}

			case "common2 hoverableset": if (set6 = true){
				$("."+overThat).css({"color":"purple"});
				$(".wrapperIntro.second .text11").show(0);
				$(".img08").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img03").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
			HoverCount++;break;}

			case "common3 hoverableset": if (set7 = true){
				$("."+overThat).css({"color":"purple"});
				$(".wrapperIntro.second .text12").show(0);
				$(".img06").show(0);
			$(".img02").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img03").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
			HoverCount++;break;}

			case "rectangle hoverableset": if (set8 = true){
				$("."+overThat).css({"color":"purple"});
				$(".wrapperIntro.second .text13").show(0);
				$(".img02").show(0);
			$(".img03").hide(0);
			$(".img04").hide(0);
			$(".img05").hide(0);
			$(".img06").hide(0);
			$(".img07").hide(0);
			$(".img08").hide(0);
			$(".img09").hide(0);
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
				HoverCount++;break;}
			default:break;
		}


		if(HoverCount > 8 && set1 == true && set2 == true && set3 == true && set4 == true && set5 == true && set6 == true && set7 == true && set8 == true){
			ole.footerNotificationHandler.pageEndSetNotification();
			console.log(HoverCount);
		}
	});



		/*$(".circle1").on({
    	 mouseover: function(){
       	 $(".wrapperIntro.second .text6").stop().show(0);
		 $(".img03").stop().show(0);
		 set1=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text6").stop().hide(0);
		$(".img03").stop().hide(0);
    	}
		});

		 $(".circle2").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text7").stop().show(0);
		 $(".img04").stop().show(0);
		 set2=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text7").stop().hide(0);
		$(".img04").stop().hide(0);
    	}
		});

		$(".circle3").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text8").stop().show(0);
		 $(".img09").stop().show(0);
		 set3=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text8").stop().hide(0);
		$(".img09").stop().hide(0);
    	}
		});

		$(".common1").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text9").stop().show(0);
		 $(".common1").css({'background': '#fefe33',
				'opacity': '0.7'});
		 $(".img05").stop().show(0);
		 set4=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text9").stop().hide(0);
		$(".common1").css({'background': 'transparent',
				'opacity': '0.7'});
		$(".img05").stop().hide(0);
    	}
		});

		$(".common2").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text11").stop().show(0);
		 $(".common2").css({'background': '#8601AF',
				'opacity': '0.7'});
		 $(".img08").stop().show(0);
		 set5=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text11").stop().hide(0);
		$(".common2").css({'background': 'transparent',
				'opacity': '0.7'});
		$(".img08").stop().hide(0);
    	}
		});

		$(".common3").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text12").stop().show(0);
		 $(".common3").css({'background': '#FD5308',
				'opacity': '0.7'});
		 $(".img06").stop().show(0);
		 set6=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text12").stop().hide(0);
		$(".common3").css({'background': 'transparent',
				'opacity': '0.7'});
		$(".img06").stop().hide(0);
    	}
		});

		$(".common4").on({
    	 mouseover: function() {
       	 $(".wrapperIntro.second .text10").stop().show(0);
		 $(".common4").css({'background': '#0247FE',
				'opacity': '0.7'});
		 $(".img07").stop().show(0);
		 set7=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text10").stop().hide(0);
		$(".common4").css({'background': 'transparent',
				'opacity': '0.7'});
		$(".img07").stop().hide(0);
    	}
		});

		$(".rectangle").on({
    		mouseover: function() {
       	 $(".wrapperIntro.second .text13").stop().show(0);
		 $(".img02").stop().show(0);
		 set8=true;
   		 },

    	mouseout: function() {
        $(".wrapperIntro.second .text13").stop().hide(0);
		$(".img02").stop().hide(0);
    	}
		});

		if(set1 == true && set2 == true && set3 == true && set4 == true && set5 == true && set6 == true && set7 == true && set8 == true ){
			$(".nextBtn.mynextStyle").show(0);
			//console.log(HoverCount);
		}*/



			/*	$(".circle1").click(function(){
			$(".wrapperIntro.second .text7").hide(0);
			$(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text9").hide(0);
			$(".wrapperIntro.second .text11").hide(0);
			$(".wrapperIntro.second .text12").hide(0);
			$(".wrapperIntro.second .text13").hide(0);
			$(".wrapperIntro.second .text10").hide(0);
			$(".wrapperIntro.second .text6").show(0);
			$(".img03").show(0);
			});


			$(".circle2").click(function(){
				$(".wrapperIntro.second .text6").hide(0);
				$(".wrapperIntro.second .text9").hide(0);
				$(".wrapperIntro.second .text10").hide(0);
				$(".wrapperIntro.second .text8").hide(0);
				$(".wrapperIntro.second .text12").hide(0);
				$(".wrapperIntro.second .text13").hide(0);
				$(".wrapperIntro.second .text11").hide(0);
				$(".img03").hide(0);
				$(".wrapperIntro.second .text7").show(0);
				$(".img04").show(0);
			});

			$(".circle3").click(function(){
				$(".wrapperIntro.second .text6").hide(0);
				$(".wrapperIntro.second .text7").hide(0);
				$(".wrapperIntro.second .text9").hide(0);
				$(".wrapperIntro.second .text10").hide(0);
				$(".wrapperIntro.second .text12").hide(0);
				$(".wrapperIntro.second .text13").hide(0);
				$(".wrapperIntro.second .text11").hide(0);
				$(".img04").hide(0);
				$(".wrapperIntro.second .text8").show(0);
				$(".img09").show(0);
			});*/




		};

		 /*function second01() {
		 		/*$board.find(".wrapperIntro.firstPage .text8").hide(0);
		 		$board.find(".wrapperIntro.firstPage .text19").hide(0);
				$board.find(".wrapperIntro.firstPage .text20").hide(0);
				$board.find(".wrapperIntro.firstPage .text21").hide(0);
				$board.find(".wrapperIntro.firstPage .text22").hide(0);
				$board.find(".wrapperIntro.firstPage .text23").hide(0);
				$board.find(".wrapperIntro.firstPage .text24").hide(0);
				$board.find(".wrapperIntro.firstPage .text25").hide(0);
				$board.find(".wrapperIntro.second .text26").show(0);

				$board.find(".wrapperIntro.second .text27").show(0);
				$board.find(".wrapperIntro.second .text28").show(0);
				$board.find(".wrapperIntro.second .text29").show(0);
				$board.find(".wrapperIntro.second .text30").show(0);
				$board.find(".wrapperIntro.second .text31").show(0);
				//$board.find(".wrapperIntro.second .text32").show(0);
				$(".imgs").show(0);
		 };*/

		/* function second01() {

				$board.find(".wrapperIntro.second .text0").show(0);
				$board.find(".wrapperIntro.second .text1").hide(0);
				$board.find(".wrapperIntro.second .text2").hide(0);
				$board.find(".wrapperIntro.second .text3").hide(0);
				$board.find(".wrapperIntro.second .text4").hide(0);
				$board.find(".wrapperIntro.second .text5").hide(0);
				$(".imgs").hide(0);
				$(".circle1").show(0);
				$(".circle2").show(0);
				$(".circle3").show(0);
				$(".rectangle").show(0);
				$(".common1").show(0);
				$(".common2").show(0);
				$(".common3").show(0);
				$(".common4").show(0);
				//$(".img02").show(0);
				//$(".img03").show(0);
				//$(".img04").show(0);
				//$(".img05").show(0);
				//$(".img06").show(0);
				//$(".img07").show(0);
				//$(".img08").show(0);
				//$(".img09").show(0);
				//$(".main").show(0);

			}*/


		 function second02() {

				/*$board.find(".wrapperIntro.second .text14").show(0);
				$board.find(".wrapperIntro.second .text15").show(0);
				$board.find(".wrapperIntro.second .text16").show(0);
				$board.find(".wrapperIntro.second .text17").show(0);
				$board.find(".wrapperIntro.second .text18").show(0);
				$board.find(".wrapperIntro.second .text19").show(0);
				$board.find(".wrapperIntro.second .text20").show(0);
				$board.find(".wrapperIntro.second .text21").show(0);
				$board.find(".wrapperIntro.second .text22").show(0);
				$board.find(".wrapperIntro.second .text23").show(0);
				$board.find(".wrapperIntro.second .text24").show(0);
				$(".imgs").show(0);
				$(".circle1").hide(0);
				$(".circle2").hide(0);
				$(".circle3").hide(0);
				$(".rectangle").hide(0);
				$(".common1").hide(0);
				$(".common2").hide(0);
				$(".common3").hide(0);
				$(".common4").hide(0);*/
				$board.find(".wrapperIntro.second .text14").show(0);
				$board.find(".wrapperIntro.second .text15").show(0);

		 };



		// first func call
		first();


		var inputvalue = $('.inputbox_diy1');
		$('.inputbox_diy1').keyup(function () {

			if ((inputvalue.val() == 'e' || inputvalue.val() == 'f' || inputvalue.val() == 'i')) {
				document.getElementById("diy1").disabled = true;
				$(".inputbox_diy1").addClass("correct");
				$(".inputbox_diy1").removeClass("incorrect");

			}
			else{
				document.getElementById("diy1").disabled = false;
				$(".inputbox_diy1").addClass("incorrect");
				$(".inputbox_diy1").removeClass("correct");

			}
		});

		var inputvalue1 = $('.inputbox_diy2');
		$('.inputbox_diy2').keyup(function () {

			if ((inputvalue.val() == 'e' && inputvalue1.val() == 'f') ||
			(inputvalue.val() == 'e' && inputvalue1.val() == 'i') ||
			(inputvalue.val() == 'f' && inputvalue1.val() == 'e') ||
			(inputvalue.val() == 'f' && inputvalue1.val() == 'i') ||
			(inputvalue.val() == 'i' && inputvalue1.val() == 'e') ||
			(inputvalue.val() == 'i' && inputvalue1.val() == 'f')) {
				document.getElementById("diy2").disabled = true;
				$(".inputbox_diy2").addClass("correct");
				$(".inputbox_diy2").removeClass("incorrect");
			}
			else{
				document.getElementById("diy2").disabled = false;
				$(".inputbox_diy2").addClass("incorrect");
				$(".inputbox_diy2").removeClass("correct");
			}
		});

		var inputvalue2 = $('.inputbox_diy3');
		$('.inputbox_diy3').keyup(function () {

			if ((inputvalue.val() == 'e' && inputvalue1.val() == 'f' && inputvalue2.val() == 'i' ) ||
			(inputvalue.val() == 'e' && inputvalue1.val() == 'i' && inputvalue2.val() == 'f') ||
			(inputvalue.val() == 'f' && inputvalue1.val() == 'e' && inputvalue2.val() == 'i') ||
			(inputvalue.val() == 'f' && inputvalue1.val() == 'i' && inputvalue2.val() == 'e') ||
			(inputvalue.val() == 'i' && inputvalue1.val() == 'e' && inputvalue2.val() == 'f') ||
			(inputvalue.val() == 'i' && inputvalue1.val() == 'f' && inputvalue2.val() == 'e')) {

				document.getElementById("diy3").disabled = true;
				$(".inputbox_diy3").addClass("correct");
				$(".inputbox_diy3").removeClass("incorrect");

			}
			else{
				document.getElementById("diy3").disabled = false;
				$(".inputbox_diy3").addClass("incorrect");
				$(".inputbox_diy3").removeClass("correct");

			}
		});



		var inputvalue3 = $('.inputbox_diy4');
		$('.inputbox_diy4').keyup(function () {

			if (inputvalue3.val() == 'a' || inputvalue3.val() == 'b' || inputvalue3.val() == 'e' || inputvalue3.val() == 'f') {
				document.getElementById("diy4").disabled = true;
				$(".inputbox_diy4").addClass("correct");
				$(".inputbox_diy4").removeClass("incorrect");
				/*
				$(".check").hide(0);
				$(".check1").show(0);*/
			}
			else{
				document.getElementById("diy4").disabled = false;
				$(".inputbox_diy4").addClass("incorrect");
				$(".inputbox_diy4").removeClass("correct");

			}
		});

		var inputvalue4 = $('.inputbox_diy5');
		$('.inputbox_diy5').keyup(function () {

			if ((inputvalue3.val() == 'a' && inputvalue4.val() == 'b')||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'e') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'f') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'a') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'e') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'f') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'b') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'f') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'a') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'e')){
				document.getElementById("diy5").disabled = true;
				$(".inputbox_diy5").addClass("correct");
				$(".inputbox_diy5").removeClass("incorrect");

			}
			else{
				document.getElementById("diy5").disabled = false;
				$(".inputbox_diy5").addClass("incorrect");
				$(".inputbox_diy5").removeClass("correct");
			}
		});


		var inputvalue5 = $('.inputbox_diy6');
		$('.inputbox_diy6').keyup(function () {

			if ((inputvalue3.val() == 'a' && inputvalue4.val() == 'b' && inputvalue5.val() == 'e')||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'e' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'f' && inputvalue5.val() == 'e') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'e' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'b' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'f' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'a' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'e' && inputvalue5.val() == 'a') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'f' && inputvalue5.val() == 'a') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'a' && inputvalue5.val() == 'e') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'f' && inputvalue5.val() == 'e') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'e' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'b' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'f' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'a' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'b' && inputvalue5.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'f' && inputvalue5.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'a' && inputvalue5.val() == 'f') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'a' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'b' && inputvalue5.val() == 'a') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'e' && inputvalue5.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'b' && inputvalue5.val() == 'e') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'a' && inputvalue5.val() == 'e') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'e' && inputvalue5.val() == 'a')){
				document.getElementById("diy6").disabled = true;
				$(".inputbox_diy6").addClass("correct");
				$(".inputbox_diy6").removeClass("incorrect");

			}
			else{
				document.getElementById("diy6").disabled = false;
				$(".inputbox_diy6").addClass("incorrect");
				$(".inputbox_diy6").removeClass("correct");
			}
		});

		var inputvalue3 = $('.inputbox_diy4');
		var inputvalue4 = $('.inputbox_diy5');
		var inputvalue5 = $('.inputbox_diy6');
		var inputvalue6 = $('.inputbox_diy7');
		$('.inputbox_diy7').keyup(function () {

			if ((inputvalue3.val() == 'a' && inputvalue4.val() == 'b' && inputvalue5.val() == 'e' && inputvalue6.val() == 'f' )||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'b' && inputvalue5.val() == 'f' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'f' && inputvalue5.val() == 'b' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'f' && inputvalue5.val() == 'e' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'e' && inputvalue5.val() == 'f' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'a' && inputvalue4.val() == 'e' && inputvalue5.val() == 'b' && inputvalue6.val() == 'f') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'a' && inputvalue5.val() == 'f' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'a' && inputvalue5.val() == 'e' && inputvalue6.val() == 'f') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'e' && inputvalue5.val() == 'a' && inputvalue6.val() == 'f') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'e' && inputvalue5.val() == 'f' && inputvalue6.val() == 'a') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'f' && inputvalue5.val() == 'a' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'b' && inputvalue4.val() == 'f' && inputvalue5.val() == 'e' && inputvalue6.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'a' && inputvalue5.val() == 'b' && inputvalue6.val() == 'f') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'a' && inputvalue5.val() == 'f' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'b' && inputvalue5.val() == 'f' && inputvalue6.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'b' && inputvalue5.val() == 'a' && inputvalue6.val() == 'f') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'f' && inputvalue5.val() == 'b' && inputvalue6.val() == 'a') ||
			(inputvalue3.val() == 'e' && inputvalue4.val() == 'f' && inputvalue5.val() == 'a' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'a' && inputvalue5.val() == 'b' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'a' && inputvalue5.val() == 'e' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'b' && inputvalue5.val() == 'a' && inputvalue6.val() == 'e') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'b' && inputvalue5.val() == 'e' && inputvalue6.val() == 'a') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'e' && inputvalue5.val() == 'a' && inputvalue6.val() == 'b') ||
			(inputvalue3.val() == 'f' && inputvalue4.val() == 'e' && inputvalue5.val() == 'b' && inputvalue6.val() == 'a')){
				document.getElementById("diy7").disabled = true;
				$(".inputbox_diy7").addClass("correct");
				$(".inputbox_diy7").removeClass("incorrect");

			}
			else{
				document.getElementById("diy7").disabled = false;
				$(".inputbox_diy7").addClass("incorrect");
				$(".inputbox_diy7").removeClass("correct");
			}
		});


		$(document).ready(function() {
    $(".inputbox_diy8, .inputbox_diy9, .inputbox_diy").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});



		var inputvalue7 = $('.inputbox_diy8');
		$('.inputbox_diy8').keyup(function () {
			if (inputvalue7.val() == '12'){
				document.getElementById("diy8").disabled = true;
				$(".inputbox_diy8").addClass("correct");
				$(".inputbox_diy8").removeClass("incorrect");
				//$(".check1").hide(0);
				//$(".check2").show(0);
			}
			else{
				document.getElementById("diy8").disabled = false;
				$(".inputbox_diy8").removeClass("correct");
				$(".inputbox_diy8").addClass("incorrect");
			}
		});

		var inputvalue8 = $('.inputbox_diy9');
		$('.inputbox_diy9').keyup(function () {
			if (inputvalue8.val() == '6'){
				document.getElementById("diy9").disabled = true;
				$(".inputbox_diy9").addClass("correct");
				$(".inputbox_diy9").removeClass("incorrect");
				//$(".check2").hide(0);
				//$(".check3").show(0);
			}
			else{
				document.getElementById("diy9").disabled = false;
				$(".inputbox_diy9").removeClass("correct");
				$(".inputbox_diy9").addClass("incorrect");
			}
		});


		var inputvalue9 = $('.inputbox_diy');
		$('.inputbox_diy').keyup(function () {
			if (inputvalue9.val() == '4'){
				document.getElementById("diy").disabled = true;
				$(".inputbox_diy").addClass("correct");
				$(".inputbox_diy").removeClass("incorrect");
				//$(".check3").hide(0);
				$(".nextBtn.mynextStyle").show(0);

			}
			else{
				document.getElementById("diy").disabled = false;
				$(".inputbox_diy").removeClass("correct");
				$(".inputbox_diy").addClass("incorrect");
			}
		});


		// lastBaseIndices ();
		// countNext = 12;


	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			fnArray = [
				first,
				first03,
				first09,
				first10,
				first11,
				first12,
				first13,
				first14,
				first15,

				second,
				//second01,
				second02,


			];


			fnArray[countNext]();

			$(".wrapperIntro.second .text16").on("click", function(){
			$(".wrapperIntro.second .text15").hide(0);
			$(".wrapperIntro.second .text16").hide(0);
		});

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress(1, 1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);

			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=13;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,2,3,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		// data.string.p1_1,
		data.string.p1_2,
		data.string.p1_3,
	],
	},

	{
		justClass : "second",
		animate : "true",
		text : [

		data.string.p1_3_1,
		data.string.p1_3_2,
		data.string.p1_3_3,
		data.string.p1_3_4,
		data.string.p1_3_5,
		data.string.p1_3_6,
		data.string.p1_3_7,
		data.string.p1_3_8,

	],

	},

	{
		justClass : "third",
		animate : "true",
		text : [

		data.string.p1_3_9,
		data.string.p1_3_10,
		data.string.p1_3_11,
		data.string.p1_3_12,
		data.string.p1_3_13,
		data.string.p1_3_14,
		data.string.p1_3_15,


	],

	},

	{
		justClass : "fourth",
		animate : "true",
		text : [

		data.string.p1_3_16,
		data.string.p1_3_17,
		data.string.p1_3_18,
		data.string.p1_3_19,
		data.string.p1_3_20,
		data.string.p1_3_21,
		data.string.p1_3_22,
		data.string.p1_3_23,
		data.string.p1_3_24,

	],

	},

	{
		justClass : "fifth",
		animate : "true",
		text : [

		data.string.p1_3_25,
		data.string.p1_3_26,
		data.string.p1_3_27,
		data.string.p1_3_28,
		data.string.p1_3_29,
		data.string.p1_3_30,
		data.string.p1_3_31,
		data.string.p1_3_32,
		data.string.p1_3_33,


	],

	},

	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$(".title1").show(0);
			//$(".title2").show(0);
		}


		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.second .text1").delay(500).fadeIn();
			$board.find(".wrapperIntro.second .text2").delay(1000).fadeIn();
			$board.find(".wrapperIntro.second .text3").delay(1500).fadeIn();
			$board.find(".wrapperIntro.second .text4").delay(2000).fadeIn();
			$board.find(".wrapperIntro.second .text5").delay(2500).fadeIn();
			$board.find(".wrapperIntro.second .text6").delay(3000).fadeIn();
			$board.find(".wrapperIntro.second .text7").delay(3500).fadeIn();
			$(".animals").delay(1000).fadeIn().show(0);
			$(".fruits").delay(1500).fadeIn();
			$(".shapes").delay(2000).fadeIn();
			$(".clothes").delay(2500).fadeIn();
			$(".vowels").delay(3000).fadeIn();
			$(".numbers").delay(3500).fadeIn();
			 $(".nextBtn.mynextStyle").delay(4000).fadeIn();



		}


		 function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.third .text0").delay(500).fadeIn();
			$board.find(".wrapperIntro.third .text1").delay(1000).fadeIn();
			$board.find(".wrapperIntro.third .text2").delay(1500).fadeIn();
			$board.find(".wrapperIntro.third .text3").delay(2000).fadeIn();
			$board.find(".wrapperIntro.third .text4").delay(2500).fadeIn();
			$board.find(".wrapperIntro.third .text5").delay(3000).fadeIn();
			$(".uparrow").delay(3500).fadeIn();
			$board.find(".wrapperIntro.third .text6").delay(4000).fadeIn();
			$(".nextBtn.mynextStyle").delay(4500).fadeIn();

		}


		 function fourth() {

			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text0").delay(500).fadeIn();
			$board.find(".wrapperIntro.fourth .text1").delay(1000).fadeIn();
			$(".nextBtn.mynextStyle").delay(1500).fadeIn();

		 }


		 function fourth01() {
			 $(".nextBtn.mynextStyle").hide(0);
			 $board.find(".wrapperIntro.fourth .text0").hide(0);
			$board.find(".wrapperIntro.fourth .text1").hide(0);
			$board.find(".wrapperIntro.fourth .text2").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text3").addClass("border").delay(1500).fadeIn();
			$(".uni").delay(1500).fadeIn();
			$(".nextBtn.mynextStyle").delay(2000).fadeIn();

		}

		function fourth02() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text3").removeClass("border").hide(0);
			$(".uni").hide(0);
			$(".set-A").delay(500).fadeIn();
			$(".set-B").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text4").delay(1500).fadeIn();
			$(".nextBtn.mynextStyle").delay(2000).fadeIn();
		}

		function fourth03() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text4").hide(0);
			$(".set-A").hide(0);
			$(".set-B").hide(0);
			$(".set-A1").delay(500).fadeIn();
			$(".set-B1").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text5").addClass("border").delay(1500).fadeIn();
			$(".uni").delay(1500).fadeIn();
			$(".nextBtn.mynextStyle").delay(2000).fadeIn();
		}

		function fourth04() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text5").removeClass("border").hide(0);
			$(".set-A1").hide(0);
			$(".set-B1").hide(0);
			$(".uni").hide(0);
			$(".set-A4").delay(500).fadeIn();
			$(".set-B4").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text6").addClass("border").delay(1500).fadeIn();
			$(".uni").delay(1500).fadeIn();
			$(".nextBtn.mynextStyle").delay(2000).fadeIn();
		}

		function fourth05() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text6").removeClass("border").hide(0);
			$(".uni").hide(0);
			$(".set-A4").hide(0);
			$(".set-B4").hide(0);
			$(".set-A3").delay(500).fadeIn();
			$(".set-B3").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text7").addClass("border").delay(1500).fadeIn();
			$(".uni").delay(1500).fadeIn();
			$(".nextBtn.mynextStyle").delay(2000).fadeIn();
		}

		function fourth06() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text7").removeClass("border").hide(0);
			$(".uni").hide(0);
			$(".set-A3").hide(0);
			$(".set-B3").hide(0);
			$(".set-A2").delay(500).fadeIn();
			$(".set-B2").delay(1000).fadeIn();
			$(".set-C").delay(1500).fadeIn();
			$board.find(".wrapperIntro.fourth .text8").addClass("border1").delay(2000).fadeIn();
			$(".uni").delay(2000).fadeIn();
			$(".nextBtn.mynextStyle").delay(2500).fadeIn();
		}


		function fifth() {

			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.fifth .text0").show(0);

		}


		function fifth01() {
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fifth .text0").hide(0);
			$board.find(".wrapperIntro.fifth .text1").delay(500).fadeIn();
			$(".dig01").delay(500).fadeIn();
			$board.find(".wrapperIntro.fifth .text2").delay(1000).fadeIn();
			$(".dig06").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fifth .text3").delay(1500).fadeIn();
			$(".dig07").delay(1500).fadeIn();
			$board.find(".wrapperIntro.fifth .text4").delay(2000).fadeIn();
			$(".dig08").delay(2000).fadeIn();
			$(".nextBtn.mynextStyle").delay(2500).fadeIn();

		}

		function fifth02() {
			$board.find(".wrapperIntro.fifth .text5").addClass("background").delay(500).fadeIn();
		}

		function fifth03() {
			$board.find(".wrapperIntro.fifth .text1").hide(0);
			$(".dig01").hide(0);
			$board.find(".wrapperIntro.fifth .text2").hide(0);
			$(".dig06").hide(0);
			$board.find(".wrapperIntro.fifth .text3").hide(0);
			$(".dig07").hide(0);
			$board.find(".wrapperIntro.fifth .text4").hide(0);
			$(".dig08").hide(0);
			$board.find(".wrapperIntro.fifth .text5").removeClass("background").hide(0);
			$board.find(".wrapperIntro.fifth .text6").delay(500).fadeIn();
			$board.find(".wrapperIntro.fifth .text7").delay(1000).fadeIn();
			$(".dig071").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fifth .text8").delay(1500).fadeIn();
			$(".dig081").delay(1500).fadeIn();
		}

/*----------------------------------------------------------------------------------------------------*/




		// first func call

		first();


	/*click functions*/
		$nextBtn.on('click',function () {
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,

				second,
				third,
				fourth,
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,

				fifth,
				fifth01,
				fifth02,
				fifth03


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {

			}
		}


});

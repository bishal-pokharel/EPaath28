/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/



$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=3;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p2_1,

		data.string.p2_2,
		data.string.p2_3,

		data.string.p2_4,
		data.string.p2_5,
		data.string.p2_6,
		data.string.p2_7,
		data.string.p2_8,

		data.string.p2_9,
		data.string.p2_10,
		data.string.p2_11,
		data.string.p2_12,
		data.string.p2_13,
		data.string.p2_14,
		data.string.p2_15,
		data.string.p2_16,
		data.string.p2_17


		],
	},

	]






/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$(".textblock1").hide(0);
			$(".textblock2").hide(0);
			$(".textblock3").hide(0);
			$(".textblock").hide(0);





		};

		function first02() {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$(".textblock").show(0);
			//$board.find(".wrapperIntro.firstPage .text3").show(0);
			//$board.find(".wrapperIntro.firstPage .text4").show(0);
			//$board.find(".wrapperIntro.firstPage .text5").show(0);
		    //$board.find(".wrapperIntro.firstPage .text6").show(0);
			//$(".textblock").show(0);
			$(".setU").show(0);
			$(".nextBtn.mynextStyle").hide(0);

		};

		function first03() {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			//$board.find(".wrapperIntro.firstPage .text4").hide(0);
			//$board.find(".wrapperIntro.firstPage .text5").hide(0);
		    //$board.find(".wrapperIntro.firstPage .text6").hide(0);
			//$board.find(".wrapperIntro.firstPage .text16").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").hide(0);
			$(".set1").show(0);
			$(".set2").show(0);
			$(".set3").show(0);

		};

		function first04() {
			$board.find(".wrapperIntro.firstPage .text8").show(0);
			$board.find(".wrapperIntro.firstPage .text9").show(0);
			$board.find(".wrapperIntro.firstPage .text10").show(0);
		    $board.find(".wrapperIntro.firstPage .text11").show(0);
			$board.find(".wrapperIntro.firstPage .text12").show(0);

		};

		first();
		$(".textblock, .stud").click(function(){
			$(".wrapperIntro.firstPage .text4").show(0);
			$(".stud").addClass("back");
			$(".textblock").hide(0);
			$(".textblock1").show(0);
			})

		/*$(".stud").click (function(){
			$(".wrapperIntro.firstPage .text4").show(0);
			$(".stud").addClass("back");
			$(".textblock").hide(0);
			$(".textblock1").show(0);
		})*/

		$(".textblock1, .hike").click(function(){
			$(".wrapperIntro.firstPage .text5").show(0);
			$(".hike").addClass("back");
			$(".textblock1").hide(0);
			$(".textblock2").show(0);
			})

		$(".textblock2, .picnic").click(function(){
			$(".wrapperIntro.firstPage .text6").show(0);
			$(".picnic").addClass("back");
			$(".textblock2").hide(0);
			$(".textblock3").show(0);
			})

		$(".textblock3, .que").click(function(){
			$(".wrapperIntro.firstPage .text7").show(0);
			$(".que").addClass("back");
			$(".textblock3").hide(0);
			$(".nextBtn.mynextStyle").show(0);
			})





	/*click functions*/
	$("#activity-page-next-btn-enabled").click(function(){
		//alert("ok");
		$prevBtn.show(0);
			countNext++;
			fnSwitcher();
	});

	$("#activity-page-prev-btn-enabled").click(function(){
		for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
				first03,
				first04

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {

			}
		}





	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=16;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,13]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p1_6,
		data.string.p1_7,
		data.string.p1_8,
		data.string.p1_9,
		data.string.p1_10,
		data.string.p1_11,
		data.string.p1_12,

		data.string.p1_13,
		data.string.p1_14,
		data.string.p1_15,
		data.string.p1_16,

		data.string.p1_17,
		data.string.p1_18,
		data.string.p1_19,
		data.string.p1_19_1,
	],
	},

	{
		justClass : "second",
		animate : "true",
		text : [
		data.string.p1_19_0,
		data.string.p1_19_2,
		data.string.p1_19_3,
		data.string.p1_19_4,
		data.string.p1_19_5,
		data.string.p1_19_6,
		data.string.p1_19_7,
		data.string.p1_19_8,
		data.string.p1_19_9,
		data.string.p1_19_10,
		data.string.p1_19_11,
		data.string.p1_19_12,
		data.string.p1_19_13


		],

	},

	]



/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);


			$board.find(".wrapperIntro.firstPage .text0").delay(500).fadeIn();
		}

		function first02() {

		 	$board.find(".wrapperIntro.firstPage .text1").show(0);
			$(".dig07").show(0);
		}

		function first03() {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
		}

		function first04() {
			$(".dig07").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text2").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$(".dig02").show(0);

		}

		function first05() {
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$(".dig02").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$(".dig08").show(0);
		}





		/*function first03() {

		 	$board.find(".wrapperIntro.firstPage .text3").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text4").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text5").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			$(".arrow3").hide(0);
		 }

		 function first04() {

			$board.find(".wrapperIntro.firstPage .text8").show(0);
		 }

		 function first05() {

			$board.find(".wrapperIntro.firstPage .text9").show(0);
		 }*/

		function first06() {
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$(".dig08").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);

		}

		function first07() {

			$board.find(".wrapperIntro.firstPage .text8").show(0);

		}

		function first08() {

			$board.find(".wrapperIntro.firstPage .text9").show(0);

		}

		function first09() {

			$board.find(".wrapperIntro.firstPage .text10").show(0);

		}

		function first10() {
			$board.find(".wrapperIntro.firstPage .text7").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text8").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text9").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text10").hide(0);

			$board.find(".wrapperIntro.firstPage .text11").show(0);

		}

		function first11() {

			$board.find(".wrapperIntro.firstPage .text12").show(0);

		}

		function first12() {

			$board.find(".wrapperIntro.firstPage .text13").show(0);

		}

		function first13() {
			$board.find(".wrapperIntro.firstPage .text14").show(0);

		}

		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

			//$board.find(".wrapperIntro.second .text0").show(0);
			$(".loku").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$(".dig071").show(0);
			$(".nextBtn.mynextStyle").hide(0);
			$nextBtn.hide(0);
			//$(".dig081").show(0);
			$(".wrapperIntro.second .text2").click(function(){
				if(countNext==13){
					$(".dig071").hide(0);
					$(".ans1").show(0);
					$(".correct01").show(0);
					$(".wrapperIntro.second .text2").addClass("correct").css("border", "3px solid #FFCA3F");
					$nextBtn.show(0);
					$(".nextBtn.mynextStyle").show(0);
				}
			});

			$(".wrapperIntro.second .text3").click(function(){
				if(countNext==13){
					$(".wrapperIntro.second .text3").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong01").show(0);
				}
			});

			$(".wrapperIntro.second .text3").click(function(){
				if(countNext==14){
					$(".dig081").hide(0);
					$(".wrong01").hide(0);
					$(".correct01").hide(0);
					$(".ans2").show(0);
					$(".correct001").show(0);
					$(".wrapperIntro.second .text3").addClass("correct").css("border", "3px solid #FFCA3F");
					$nextBtn.show(0);
					$(".nextBtn.mynextStyle").show(0);
				}
			});

			$(".wrapperIntro.second .text2").click(function(){
				if(countNext==14){
					$(".wrapperIntro.second .text2").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong001").show(0);
					$(".wrong01").hide(0);
					$(".correct01").hide(0);
				}
			});


			$(".wrapperIntro.second .text2").click(function(){
				if(countNext==15){
					$(".wrong001").hide(0);
					$(".correct001").hide(0);
					$(".wrapperIntro.second .text2").addClass("correct").css("border", "3px solid #FFCA3F");
					$(".wrapperIntro.second .text8").show(0);
					$(".correct3").show(0);
					$nextBtn.show(0);
					$(".nextBtn.mynextStyle").show(0);
				}
			});

			$(".ans1").click(function(){
				if(countNext==15){
					$(".wrong001").hide(0);
					$(".correct001").hide(0);
					$(".wrapperIntro.second .text2").addClass("correct").css("border", "3px solid #FFCA3F");
					$(".wrapperIntro.second .text8").show(0);
					$(".correct3").show(0);
					$nextBtn.show(0);
					$(".nextBtn.mynextStyle").show(0);
				}
			});

			$(".wrapperIntro.second .text3").click(function(){
				if(countNext==15){
					$(".wrong001").hide(0);
					$(".correct001").hide(0);
					$(".wrapperIntro.second .text3").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong3").show(0);

				}
			});

			$(".ans2").click(function(){
				if(countNext==15){
					$(".wrapperIntro.second .text3").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong3").show(0);
					$(".wrong001").hide(0);
					$(".correct001").hide(0);
				}
			});


			$(".wrapperIntro.second .text3").click(function(){
				if(countNext==16){

					$(".wrapperIntro.second .text3").addClass("correct").css("border", "3px solid #FFCA3F");
					$(".wrapperIntro.second .text12").show(0);
					$(".correct4").show(0);
					$(".wrong3").hide(0);
					$(".correct3").hide(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			});

			$(".ans2").click(function(){
				if(countNext==16){

					$(".wrapperIntro.second .text3").addClass("correct").css("border", "3px solid #FFCA3F");
					$(".wrapperIntro.second .text12").show(0);
					$(".correct4").show(0);
					$(".wrong3").hide(0);
					$(".correct3").hide(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			});

			$(".wrapperIntro.second .text2").click(function(){
				if(countNext==16){
					$(".wrapperIntro.second .text2").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong4").show(0);
					$(".wrong3").hide(0);
					$(".correct3").hide(0);
				}
			});

			$(".ans1").click(function(){
				if(countNext==16){
					$(".wrapperIntro.second .text2").addClass("incorrect").css("border", "3px solid #FF684E");
					$(".wrong4").show(0);
					$(".wrong3").hide(0);
					$(".correct3").hide(0);
				}
			});
		}


		function second02() {

			$(".nextBtn.mynextStyle").hide(0);
			$(".dig081").show(0);
			$(".ans1").hide(0);
			$(".correct01").hide(0);
			$(".wrong01").hide(0);
			$(".correct001").hide(0);
			$(".wrong001").hide(0);
			$(".wrong3").hide(0);
			$(".correct3").hide(0);
			$(".wrong4").hide(0);
			$(".correct4").hide(0);
			$(".wrapperIntro.second .text2").removeClass("correct").css("border", "2px solid #22DCFA");
			$(".wrapperIntro.second .text3").removeClass("incorrect").css("border", "2px solid #22DCFA");
			$(".nextBtn.mynextStyle").hide(0);

		}

		function second03(){
			$board.find(".wrapperIntro.second .text4").show(0);
			$board.find(".wrapperIntro.second .text5").show(0);
			$board.find(".wrapperIntro.second .text6").show(0);
			$board.find(".wrapperIntro.second .text7").show(0);
			//$board.find(".wrapperIntro.second .text8").show(0);
			$(".wrapperIntro.second .text2").removeClass("incorrect").css("border", "2px solid #22DCFA");
			$(".wrapperIntro.second .text3").removeClass("correct").css("border", "2px solid #22DCFA");
			$(".ans1").show(0);
			$(".wrong001").hide(0);
			$(".correct001").hide(0);
			$(".nextBtn.mynextStyle").hide(0);
		}


		function second04(){
			$board.find(".wrapperIntro.second .text4").hide(0);
			$board.find(".wrapperIntro.second .text5").hide(0);
			$board.find(".wrapperIntro.second .text6").hide(0);
			$board.find(".wrapperIntro.second .text7").hide(0);
			$board.find(".wrapperIntro.second .text8").hide(0);
			$(".wrapperIntro.second .text2").removeClass("correct").css("border", "2px solid #22DCFA");
			$(".wrapperIntro.second .text3").removeClass("incorrect").css("border", "2px solid #22DCFA");
			$board.find(".wrapperIntro.second .text9").show(0);
			$board.find(".wrapperIntro.second .text10").show(0);
			$board.find(".wrapperIntro.second .text11").show(0);
			$(".wrong3").hide(0);
			$(".correct3").hide(0);
			//$board.find(".wrapperIntro.second .text12").show(0);
			$(".nextBtn.mynextStyle").hide(0);

		}



/*----------------------------------------------------------------------------------------------------*/




		// first func call

		first();

		/*$(".wrapperIntro.firstPage .text1").click(function(){
			$(this).addClass("bottomborder");
			$(".wrapperIntro.firstPage .text3").removeClass("bottomborder");
			$(".wrapperIntro.firstPage .text5").removeClass("bottomborder");
		    $(".wrapperIntro.firstPage .text2").fadeIn(1000);
		    $(".wrapperIntro.firstPage .text4").hide(0);
		    $(".wrapperIntro.firstPage .text6").hide(0);
		    $(".arrow1").show(0);
		    $(".arrow2").hide(0);
		    $(".arrow3").hide(0);

		});

		$(".wrapperIntro.firstPage .text3").click(function(){
			$(this).addClass("bottomborder");
			$(".wrapperIntro.firstPage .text1").removeClass("bottomborder");
			$(".wrapperIntro.firstPage .text5").removeClass("bottomborder");
			$(".wrapperIntro.firstPage .text4").fadeIn(1000);
			$(".wrapperIntro.firstPage .text2").hide(0);
		    $(".wrapperIntro.firstPage .text6").hide(0);
		    $(".arrow2").show(0);
		    $(".arrow1").hide(0);
		    $(".arrow3").hide(0);
		});

		$(".wrapperIntro.firstPage .text5").click(function(){
			$(this).addClass("bottomborder");
			$(".wrapperIntro.firstPage .text3").removeClass("bottomborder");
			$(".wrapperIntro.firstPage .text1").removeClass("bottomborder");
			$(".wrapperIntro.firstPage .text6").fadeIn(1000);
			$(".wrapperIntro.firstPage .text2").hide(0);
		    $(".wrapperIntro.firstPage .text4").hide(0);
		    $(".arrow3").show(0);
		    $(".arrow1").hide(0);
		    $(".arrow2").hide(0);

		});*/

		/*$(".wrapperIntro.firstPage .text1").on({
    		mouseover: function() {
       	 $(".dsc").stop().show(1000);
   		 },

    	mouseout: function() {
        $(".dsc").stop().hide(1000);
    	}
		});

		$(".wrapperIntro.firstPage .text3").on({
    		mouseover: function() {
       	 $(".dsc").stop().show(1000);
   		 },

    	mouseout: function() {
        $(".dsc").stop().hide(1000);
    	}
		});

		$(".wrapperIntro.firstPage .text5").on({
    		mouseover: function() {
       	 $(".dsc").stop().show(1000);
   		 },

    	mouseout: function() {
        $(".dsc").stop().hide(1000);
    	}
		});*/


	/*click functions*/
		$nextBtn.on('click',function () {
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
			console.log(countNext);
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
				first03,
				first04,
				first05,
				first06,
				first07,
				first08,
				first09,
				first10,
				first11,
				first12,
				first13,

				second,
				//second01,
				second02,
				second03,
				second04
				];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);

			} else {

			}
		}


});

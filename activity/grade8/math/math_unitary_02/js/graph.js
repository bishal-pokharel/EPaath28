	
	
var t = function(){	
$('#scatterplot').scatter({
	

 height: 400,
 width: '40%', 
 xLabel: 'X-Axis', 
 yLabel: 'Y-Axis',
 rows: 16, 
 columns: 16,
 subsections: 0,
 responsive: true, 
 xUnits: [-8,-7, -6, -5, -4, -3,-2,-1,0,1,2,3,4,5,6,7,8],
 yUnits: [-8,-7, -6, -5, -4, -3,-2,-1,0,1,2,3,4,5,6,7,8] });


$('#point1').plot({ xPos: '75%', yPos: '81%', radius: 10, color: 'red' });
$('#point2').plot({ xPos: '62.5%', yPos: '56%', radius: 10, color: 'blue' });
$('#point3').plot({ xPos: '49.6%', yPos: '31%', radius: 10, color: 'green' });
$('#point4').plot({ xPos: '37.3%', yPos: '6.4%', radius: 10, color: 'yellow' });
/*
$("#point1").show(0);
$("#point2").show(0);
$("#point3").show(0);
$("#point4").show(0); */
$("#point1").delay(2000).fadeIn(2000);
$(".cordi1").delay(2200).fadeIn(2200);
$("#point2").delay(4000).fadeIn(4000);
$(".cordi2").delay(4200).fadeIn(4200);
$("#point3").delay(6000).fadeIn(6000);
$(".cordi3").delay(6200).fadeIn(6200);
$("#point4").delay(8000).fadeIn(8000);
$(".cordi4").delay(8200).fadeIn(8200);

var createLine = function (x1, y1, x2, y2, angle) {
  var length = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)));
  if(angle == -1) {
   angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  }else{
	  angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  }
  
   angle  = Math.atan2(y2 - y1, x2 - x1) * 180 / Math.PI;
  console.log(x1+','+x2+':'+y1+','+y2);
  console.log('L:'+ length);
  console.log('A:'+ angle);

  var transform = 'rotate(' +angle + 'deg)';
    
  var line = $('<div>')
   .appendTo('.scatterplot')
   .addClass('line')
   .css({
     '-webkit-transform':  transform,
     '-moz-transform':     transform,
     'transform':          transform
   })
   .width(length)
   .offset({left: x1-(x1-x2)+5, top: y1});

  return line;
  

 }
 
 
 




 

 setTimeout(function(){
	  var dot1 = $('#point1').offset();
      var dot2 = $('#point2').offset();
      var dot3 = $('#point3').offset();
      var dot4 = $('#point4').offset();
      
      createLine(dot1.left, dot1.top,dot2.left , dot2.top,  -1);
	  setTimeout(function(){
		createLine(dot2.left , dot2.top , dot3.left , dot3.top , -1);
		setTimeout(function(){
		createLine(dot3.left , dot3.top , dot4.left , dot4.top , -1);
	  }, 2000);
	  }, 2000);
	  
	  
	
	 
 },10000);
  
};

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : "3", power : "30", firstlabel : data.string.p4_27, secondlabel : data.string.p4_28,ansque : data.string.p4_26,prefix : data.string.p1_0_1},
		{base : "9", power : "45", firstlabel : data.string.p4_34, secondlabel : data.string.p4_35, ansque : data.string.p4_33, suffix: data.string.p1_0_2},
		{base : "4", power : "48", firstlabel : data.string.p4_41, secondlabel : data.string.p4_42, ansque : data.string.p4_40, prefix :  data.string.p1_0_1},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p4_24,
			qTitle : data.string.p4_25,
		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);

	}


	function first(sub) {
		$('.q1').hide(0);
		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p4_29,
			firstans : sub.base,
			secondans : sub.power,
			answer : data.string.p4_30,
			text : sub.ansque,
			prefix : data.string.p1_0_1,
			suffix : data.string.p1_0_2,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		 $board.find(".q1 .suffix").hide(0);
		 $board.find(".q2 .prefix").hide(0);
		 $board.find(".q3 .suffix").hide(0);
		// $board.find(".qaArea input.base").focus();
	};


		function second(sub) {

		$('.q2').hide(0);

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p4_29,
			firstans : sub.base,
			secondans : sub.power,
			answer : data.string.p4_30,
			text : sub.ansque,
			//prefix : sub.prefix,
			suffix : data.string.p1_0_2,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);

		// $board.find(".qaArea input.base").focus();
	};






	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		countNext<2?$nextBtn.show(0):
			ole.footerNotificationHandler.pageEndSetNotification();;
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		var power = $board.find(cls+" input.power").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
			swal(data.string.h2);
		}else{
		if (base ===upDatas.base) {
			check[0]=1;
			$board.find(cls+" .check1").html(right);
		} else {
			$board.find(cls+" .check1").html(wrong);
		}
		}

		if(power === ""){
			swal(data.string.h2);
		}else{
		if (power===upDatas.power) {
				check[1]=1;
				$board.find(cls+" .check2").html(right);
			} else {
				$board.find(cls+" .check2").html(wrong);
			}
		}

			if (check[0]===1 && check[1]===1) {
				if (countNext>=1) {
					ole.footerNotificationHandler.pageEndSetNotification();
				} else {
					$nextBtn.show(0);
				}
			} else {
				$board.find(cls+" .clicks .click").show(0);
			}


	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;
		$(".q"+countNext).hide(0);
		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var imgpath = $ref+"/image/";
var countme = 0;

imageblock :[
			{
				imagecontentclass: "scootymoveblock",
				imagestoshow:[
					{
						imgclass : "wheelfrontright",
						imgsrc : imgpath+"wheel-front.png",
					},
					{
						imgclass : "wheelbackright",
						imgsrc : imgpath+"wheel-back.png",
					},
					{
						imgclass : "busright",
						imgsrc : imgpath+"scooty.png",
					}
				],
			},
		];
   
var $board = $('.board');
function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		};
		
   
        



var start = 0;
var timer;
var counter = 0;
var interval;

/*function for drag and drop*/


function allowDrop(ev) {
			ev.preventDefault();
			
		}
		
function drag(ev) {
			var math = true;
			ev.dataTransfer.setData("text", ev.target.id);
			
				if (math = "false"){
			 interval = setInterval(function() {$('.timer').text(start+=10);
				}, 1000);}
		

		}
		
		function drop(ev) {
			counter ++;
			//alert(counter);
			countme ++;
			ev.preventDefault();
			var data = ev.dataTransfer.getData("text");
			ev.target.appendChild(document.getElementById(data));
			
			if (counter > 5){
				clearInterval(interval);
				console.log(interval);
				alert("ok");
				
				$('.table_new').hide(0);
				
			}
		}

/*var imgpath=$ref+"/image/";
function allowDrop(event) {
  $(".img02").attr('src', imgpath+"clock1.gif");
  
}*/	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext =0,
		all_page=41;
	//var diyImg = diyImgPath[randomImageNumeral];
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,6,15,21,24,33,35,38,39]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	
	 
	
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",

		text : [data.string.p1_1,
				data.string.p1_1_0,
				data.string.p1_1_1,
				data.string.p1_1_2,
				data.string.p1_2,
				data.string.p1_3,
				data.string.p1_3_1
		],
		test : [data.string.p14_10],
		
		
   
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		rowheading1 : [data.string.p1_14],
		rowheading2 : [data.string.p1_17],
		
		check : [data.string.p14_11],
		answer : [data.string.p14_12],
		
			text : [
			
			data.string.p1_4,
			data.string.p1_5,
			data.string.p1_10,
			data.string.p1_11,
			data.string.p1_20,
			data.string.p1_21,
			data.string.p1_21_1,
			data.string.p1_21_2,
			data.string.p1_22,
			data.string.p1_24,
			data.string.p1_24_1,
			data.string.p1_25
			],
			
		instruction:[
			data.string.p1_6,
			data.string.p1_7,
			data.string.p1_8,
			data.string.p1_9,
			
			
		],
		
		
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [
			
				data.string.p1_26,
				data.string.p1_27,
				data.string.p1_28,
				data.string.p1_29,
				data.string.p1_30,
				data.string.p1_31,
				data.string.p1_32,
				
			
				],
	},
	
	
	{
		justClass : "fourth",
		
			text : [
			
				data.string.p1_33,
				data.string.p1_34,
				data.string.p1_34_0,
				data.string.p1_34_1,
				data.string.p1_35,
			],
	},
	
	
	{
		justClass : "fifth",
		rowheading1 : [data.string.p1_40],
		rowheading2 : [data.string.p1_41],
		check : [data.string.p14_11],
		answer : [data.string.p14_13],
		
			text : [
			
				data.string.p1_36,
				data.string.p1_37,
				data.string.p1_45,
				data.string.p1_46,
				data.string.p1_49,
				data.string.p1_49_0,
				data.string.p1_49_1,
				data.string.p1_49_2,
				data.string.p1_49_3,
				
				data.string.p1_49_6,
				data.string.p1_49_7,
				data.string.p1_49_8,
			],
			
			instruction:[
			data.string.p1_39,
			data.string.p1_42,
			data.string.p1_43,
			data.string.p1_44,
			
			
		]
	},  
	
	
	{
		justClass : "sixth",
		
			text : [
			
				data.string.p1_50,
				data.string.p1_51,
				data.string.p1_52,
				data.string.p1_53,
				data.string.p1_53_1,
				data.string.p1_54,
				data.string.p1_55,
				data.string.p1_56,
				data.string.p1_57,
				data.string.p1_58,
				data.string.p1_59,
				data.string.p1_60,
				data.string.p1_61,
			],
	},
	
	
	
	{
		justClass : "seventh",
		
			text : [
			
				data.string.p1_62,
				data.string.p1_62_0,
				data.string.p1_62_1,
				
			],
	},
	
	
	{
		justClass : "eighth",
		rowheading1 : [data.string.p1_68],
		rowheading2 : [data.string.p1_69],
		rowheading2 : [data.string.p14_14],
		
			text : [
			
				
				data.string.p1_63,
				data.string.p1_64,
				data.string.p1_65,
				//data.string.p1_66,
				data.string.p1_66_0,
				data.string.p1_67,
				data.string.p1_67_0,
				data.string.p1_67_1,
				//data.string.p1_70,
				//data.string.p1_71,
				
			],
	},
	
	
	{
		justClass : "ninth",
			text : [
			
				
				data.string.p1_71,
				data.string.p1_72,
				data.string.p1_73,
				data.string.p1_74,
				data.string.p1_74_0,
				data.string.p1_74_1,
				data.string.p1_74_2,
				data.string.p1_75,
				
			],
	},
	

	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {
			var $board = $('.board');
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			
			$board.find(".wrapperIntro.firstPage .main-bg").css('background', 'url(activity/grade8/math/math_unitary_02/image/school.jpg)');
			$board.find(".wrapperIntro.firstPage").css("background-size","100% !important");
			$board.find(".img05").hide(0);
			$board.find(".img04").hide(0);
			$board.find(".img03").hide(0);
			$board.find(".school01").hide(0);
			
			$board.find(".text0").show(0);
		
		
			
			
			
			
			
		};
		
		
		function first010 () {
			
			$board.find(".wrapperIntro.firstPage").css('background', 'url(activity/grade8/math/math_unitary_02/image/school.jpg)');
			$board.find(".wrapperIntro.firstPage").css("background-size","100%");
			
						
			$board.find(".wrapperIntro.firstPage .main-bg").show(0);
			$board.find(".wrapperIntro.firstPage .main01").hide(0);
			$board.find(".wrapperIntro.firstPage .main02").hide(0);
			$board.find(".wrapperIntro.firstPage .main03").hide(0);
			$board.find(".wrapperIntro.firstPage .main04").hide(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .imgs").hide(0);
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg1").hide(0);
			
			
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			//$board.find(".wrapperIntro.firstPage .test1").hide(0);
			//$board.find(".wrapperIntro.firstPage .test2").hide(0);
			//$board.find(".wrapperIntro.firstPage .test3").hide(0);
			//$board.find(".wrapperIntro.firstPage .test4").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .imgs").show(0);
			$board.find(".wrapperIntro.firstPage .main-bg").hide(0);
			$board.find(".wrapperIntro.firstPage .main04").hide(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg1").hide(0);
			
			
			
			
		}
		
		
		
		function first01 () {
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			//$board.find(".wrapperIntro.firstPage .test1").hide(0);
			//$board.find(".wrapperIntro.firstPage .test2").hide(0);
			//$board.find(".wrapperIntro.firstPage .test3").hide(0);
			//$board.find(".wrapperIntro.firstPage .test4").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .imgs").show(0);
			$board.find(".wrapperIntro.firstPage .main-bg").hide(0);
			$board.find(".wrapperIntro.firstPage .main04").hide(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg1").hide(0);
		}
		
		function first02 () {
			
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .main-bg").show(0);
			$board.find(".wrapperIntro.firstPage .main04").hide(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .imgs").hide(0);
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
			
			
			
			$board.find(".wrapperIntro.firstPage").css('background', 'url(activity/grade8/math/math_unitary_02/image/school-deepa.png)');
			$board.find(".wrapperIntro.firstPage").css("background-size","100%");
			
		}
		
		
		
		function first020 () {
			
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg").hide(0);
			$board.find(".wrapperIntro.firstPage .main03").show(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
		}
		
		function first03 () {
			
			$nextBtn.hide(0);
			
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg").hide(0);
			$board.find(".wrapperIntro.firstPage .main03").hide(0);
			$board.find(".wrapperIntro.firstPage .main02").show(0);
			//$board.find(".wrapperIntro.firstPage .main01").show(0);
			$board.find(".wrapperIntro.firstPage .main04").show(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .desk01").show(0);
			$board.find(".wrapperIntro.firstPage .desk001").show(0);
			$board.find(".wrapperIntro.firstPage .desk02").show(0);
			$board.find(".wrapperIntro.firstPage .desk002").show(0);
			$board.find(".wrapperIntro.firstPage .desk03").show(0);
			$board.find(".wrapperIntro.firstPage .desk003").show(0);
			$board.find(".wrapperIntro.firstPage .desk04").show(0);
			$board.find(".wrapperIntro.firstPage .desk004").show(0);
			$board.find(".wrapperIntro.firstPage .desk05").show(0);
			$board.find(".wrapperIntro.firstPage .desk005").show(0);
			$board.find(".wrapperIntro.firstPage .desk06").show(0);
			$board.find(".wrapperIntro.firstPage .desk006").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .main05").hide(0);
			$board.find(".wrapperIntro.firstPage .table_new").show(0);
			
			
			$board.find(".wrapperIntro.firstPage").css('background','none'); 
			$board.find(".wrapperIntro.firstPage").css('background-color','#275082'); 
			
			
			
			$board.find(".img04").show(0);
			 
			
			
		}	
			
		
		function first04 () {
			$board.find(".wrapperIntro.firstPage .desk01").hide(0);
			$board.find(".wrapperIntro.firstPage .desk001").hide(0);
			$board.find(".wrapperIntro.firstPage .desk02").hide(0);
			$board.find(".wrapperIntro.firstPage .desk002").hide(0);
			$board.find(".wrapperIntro.firstPage .desk03").hide(0);
			$board.find(".wrapperIntro.firstPage .desk003").hide(0);
			$board.find(".wrapperIntro.firstPage .desk04").hide(0);
			$board.find(".wrapperIntro.firstPage .desk004").hide(0);
			$board.find(".wrapperIntro.firstPage .desk05").hide(0);
			$board.find(".wrapperIntro.firstPage .desk005").hide(0);
			$board.find(".wrapperIntro.firstPage .desk06").hide(0);
			$board.find(".wrapperIntro.firstPage .desk006").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg").hide(0);
			$board.find(".wrapperIntro.firstPage .main01").hide(0);
			$board.find(".wrapperIntro.firstPage .main02").show(0);
			$board.find(".wrapperIntro.firstPage .main03").hide(0);
			$board.find(".wrapperIntro.firstPage .main04").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$board.find(".wrapperIntro.firstPage .main05").show(0);
			$board.find(".wrapperIntro.firstPage .table_new").hide(0);
			
			
			
			
			
			
		}
		
		
			
		function second () {
			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]
			var $board = $('.board');
			var html = template(content);
			$board.html(html);
			instructionblockcontroller();
			
			$(".text8").click(function(){
				$(".check1").fadeIn(1000);
			});
			var lolz = $('.value1');
			$(".check1").click(function(){
				if(lolz.val() == 6)
				{
					document.getElementById("value2").disabled = true;
					$("#correct").show(0);
					$("#wrong").hide(0);
					$(".answer").hide(0);
					$nextBtn.show(0);
				}
				else
					{
						$("#wrong").show(0);
						$("#correct").hide(0);
						$(".answer").show(0);
						$nextBtn.show(0);
						
					}
					//alert(lolz.val());
		});
		
		$(".wrapperIntro.second .text3").click(function(){
			$(".imageholder014").show(0);
			$(".text3").hide(0);
			$(".imageholder14").hide(0);
			$(".imageholder0014").hide(0);
			$(".textblock1").hide(0);
			$(".text2").hide(0);
			$(".imageholder0013").hide(0);
			$(".imageholder13").hide(0);
			$(".textblock").hide(0);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second .text11").show(0);
			
		});
		
		$(".wrapperIntro.second .text2").click(function(){
			
			$(".imageholder13").hide(0);
			$(".textblock").hide(0);
			$(".imageholder0013").show(0);
			$(".imageholder0014").show(0);
			$(".text3").show(0);
			
			
		});
		
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .exmpletable").show(0);
			$board.find(".wrapperIntro.second .imageholder013").show(0);
			$board.find(".wrapperIntro.second .imageholder014").hide(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .imageholder13").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .imageholder14").show(0);
			$nextBtn.hide(0);
		}
		
		
		/*function second01 () {
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .exmpletable").show(0);
			$board.find(".wrapperIntro.second .imageholder013").show(0);
			$board.find(".wrapperIntro.second .imageholder014").hide(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .imageholder13").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .imageholder14").show(0);
			$nextBtn.hide(0);
		}*/
		
		/*function second02 () {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		
		function second03 () {
			
			$board.find(".wrapperIntro.second .exmpletable").show(0);
			$board.find(".wrapperIntro.second .imageholder013").show(0);
			$board.find(".wrapperIntro.second .imageholder014").show(0);
			
		}
		
		
		function second030 () {
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .imageholder13").show(0);
		}

		function second04 () {
			
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .imageholder14").show(0);
		}*/
		
		function second05 () {
			
			$board.find(".wrapperIntro.second .text4").show(0);
		
		}
		
		function second06 () {
			
			$board.find(".wrapperIntro.second .text5").show(0);
		}
		
		
		function second07 () {
			$board.find(".wrapperIntro.second .text6").show(0);
			
			
			
			$("#bg_td").addClass("bg");
			$("#bg_td1").addClass("bg");
			$(".one").addClass("bg");
			$(".three").addClass("bg");
			
		}
		
		function second08 () {
			$board.find(".wrapperIntro.second .text6").hide(0);
			$board.find(".wrapperIntro.second .text7").show(0);
			
			
			
			
			$("#bg_td").removeClass("bg");
			$("#bg_td1").removeClass("bg");
			$(".one").removeClass("bg");
			$(".three").removeClass("bg");
			
			
			$("#bg_td3").addClass("bg");
			$("#bg_td2").addClass("bg");
			$(".two").addClass("bg");
			$(".four").addClass("bg");
		}
		
		function second09 () {
			$board.find(".wrapperIntro.second .text7").hide(0);
			$board.find(".wrapperIntro.second .text8").show(0);
			$nextBtn.hide(0);
			
			$("#bg_td3").removeClass("bg");
			$("#bg_td2").removeClass("bg");
			$(".two").removeClass("bg");
			$(".four").removeClass("bg");
		}
		
		function second10 () {
			$board.find(".wrapperIntro.second .text9").show(0);
			
		}
		
		function second11 () {
			$board.find(".wrapperIntro.second .text10").show(0);
		
		}
		
		function second12 () {
			$board.find(".wrapperIntro.second .text11").show(0);
		
		}
		
		
		
		
		
				
		
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function third () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			
		}
		
		
		function third01 () {
			$board.find(".wrapperIntro.third .text1").show(0);
			
		}
		
		function third02 () {
			$board.find(".wrapperIntro.third .text2").show(0);
			
		}
		
		function third03 () {
			$board.find(".wrapperIntro.third .text3").show(0);
			
		}
		
		function third04 () {
			$board.find(".wrapperIntro.third .text4").show(0);
			$board.find(".wrapperIntro.third .text3").hide(0);
			$board.find(".wrapperIntro.third .text5").show(0);
			
		}
		
		function third05 () {
			$board.find(".wrapperIntro.third .text5").hide(0);
			$board.find(".wrapperIntro.third .text6").show(0);
		}
		
		
/*fourth page*/		
		
		
		function fourth () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			
			$board.find(".wrapperIntro.fourth .text0").show(0);
			$board.find(".wrapperIntro.fourth .text1").show(0);
			$board.find(".wrapperIntro.fourth .bubble2").show(0);
			$board.find(".wrapperIntro.fourth .image05").show(0);
			$board.find(".wrapperIntro.fourth .image06").show(0);
			
		}
		
		
		/*function fourth01 () {
			
			
			$board.find(".wrapperIntro.fourth .image05").show(0);
			$board.find(".wrapperIntro.fourth .image06").show(0);
			
		}*/
		
		function fourth02 () {
			$board.find(".wrapperIntro.fourth .bubble2").hide(0);
			$board.find(".wrapperIntro.fourth .bubble1").show(0);
			$board.find(".wrapperIntro.fourth .text1").hide(0);
			$board.find(".wrapperIntro.fourth .text2").show(0);
		}
		
		function fourth03 () {
			$board.find(".wrapperIntro.fourth .bubble1").hide(0);
			$board.find(".wrapperIntro.fourth .text0").hide(0);
			$board.find(".wrapperIntro.fourth .text2").hide(0);
			$board.find(".wrapperIntro.fourth .image05").hide(0);
			$board.find(".wrapperIntro.fourth .image06").hide(0);
			$board.find(".wrapperIntro.fourth .text3").show(0);
			$board.find(".wrapperIntro.fourth .text4").show(0);
			$board.find(".wrapperIntro.fourth .image04").show(0);
		}
		
		
		
		function fifth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4]
			var html = template(content);
			$board.html(html);
			
			
			
			instructionblockcontroller();
			$board.find(".wrapperIntro.fifth .text0").show(0);
			$board.find(".wrapperIntro.fifth .text1").show(0);
			$board.find(".wrapperIntro.fifth .table01").show(0);
			$board.find(".wrapperIntro.fifth .imageholder13").show(0);
			$board.find(".wrapperIntro.fifth .imageholder013").hide(0);
			$board.find(".wrapperIntro.fifth .text2").show(0);
			$board.find(".wrapperIntro.fifth .imageholder14").show(0);
			$board.find(".wrapperIntro.fifth .text3").show(0);
			$board.find(".wrapperIntro.fifth .imageholder014").show(0);
			$nextBtn.hide(0);
			
			
			$(".wrapperIntro.fifth .text2").click(function(){
			$(".imageholder013").show(0);
			$(".text3").hide(0);
			$(".imageholder14").hide(0);
			$(".imageholder0014").hide(0);
			$(".textblock1").hide(0);
			$(".text2").hide(0);
			$(".imageholder0013").hide(0);
			$(".textblock2").hide(0);
			$(".imageholder014").hide(0);
			$(".textblock3").hide(0);
			$nextBtn.show(0);
			$(".wrapperIntro.fifth .text11").show(0);
			
			
		});
		
		$(".wrapperIntro.fifth .text3").click(function(){
			
			$(".imageholder014").hide(0);
			$(".textblock3").hide(0);
			$(".wrapperIntro.fifth .imageholder0013").show(0);
			$(".wrapperIntro.fifth .imageholder0014").show(0);
			
			$(".text3").show(0);
			
			
		});
		
		$(".text8").click(function(){
				$(".check2").fadeIn(1000);
			});
			var lolz = $('.value3');
			$(".check2").click(function(){
				if(lolz.val() == 2100)
				{
					document.getElementById("value4").disabled = true;
					$("#correct1").show(0);
					$("#wrong1").hide(0);
					$(".answer2").hide(0);
					$nextBtn.show(0);
				}
				else
					{
						$("#wrong1").show(0);
						$("#correct1").hide(0);
						$(".answer2").show(0);
						$nextBtn.show(0);
						
					}
					//alert(lolz.val());
		});
			
		}
		
		
		function fifth05 () {
			$board.find(".wrapperIntro.fifth .text4").show(0);
			
		}
		
		function fifth06 () {
			$board.find(".wrapperIntro.fifth .text5").show(0);
			$board.find(".wrapperIntro.fifth .imageholder13").show(0);
			
			
		}
		
		function fifth07 () {
			$board.find(".wrapperIntro.fifth .text6").show(0);
			
			$("#bg_td4").addClass("bg");
			$("#bg_td5").addClass("bg");
			$(".seven").addClass("bg");
			$(".nine").addClass("bg");
			
			
			
			
			
			
		}
		
		function fifth08 () {
			$board.find(".wrapperIntro.fifth .text6").hide(0);
			$board.find(".wrapperIntro.fifth .text7").show(0);
			
			
			$("#bg_td4").removeClass("bg");
			$("#bg_td5").removeClass("bg");
			$(".seven").removeClass("bg");
			$(".nine").removeClass("bg");
			
			$("#bg_td6").addClass("bg");
			$("#bg_td7").addClass("bg");
			$(".eight").addClass("bg");
			$(".ten").addClass("bg");
			
			
			
		}
		
		function fifth09 () {
			$board.find(".wrapperIntro.fifth .text8").show(0);
			$board.find(".wrapperIntro.fifth .text7").hide(0);
			$nextBtn.hide(0);
			
			
			$("#bg_td6").removeClass("bg");
			$("#bg_td7").removeClass("bg");
			$(".eight").removeClass("bg");
			$(".ten").removeClass("bg");
			
		}
		
		function fifth10 () {
			$board.find(".wrapperIntro.fifth .text9").show(0);
			
			
		}
		
		function fifth11 () {
			
			$board.find(".wrapperIntro.fifth .text10").show(0);
			
		}
		
		function fifth12 () {
			
			$board.find(".wrapperIntro.fifth .text11").show(0);
			
		}
		
		
		
		
		
		function sixth () {
			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[5]

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.sixth .text0").show(0);
			$board.find(".wrapperIntro.sixth .text1").show(0);
			$board.find(".wrapperIntro.sixth .text2").show(0);
			$board.find(".wrapperIntro.sixth .text3").show(0);
			$board.find(".wrapperIntro.sixth .text4").show(0);
			$board.find(".wrapperIntro.sixth .text5").show(0);
			$board.find(".wrapperIntro.sixth .text6").show(0);
			$board.find(".wrapperIntro.sixth .text7").show(0);
			
		}
		
		
		function sixth09 () {
			$board.find(".wrapperIntro.sixth .text8").show(0);
			$board.find(".wrapperIntro.sixth .text9").show(0);
			$board.find(".wrapperIntro.sixth .text10").show(0);
			$board.find(".wrapperIntro.sixth .text11").show(0);
			$board.find(".wrapperIntro.sixth .text12").show(0);
			
		}
		
		
		
		function seventh () {
			var source = $("#intro6-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[6]

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.seventh .text0").show(0);
			
		}
	
		
		function seventh02 () {
			$board.find(".wrapperIntro.seventh .text1").show(0);
			
		}
		
		function seventh03 () {
			$board.find(".wrapperIntro.seventh .text2").show(0);
			
			
		}
		
		
		function eighth () {
			var source = $("#intro7-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[7]

			var html = template(content);
			$board.html(html);
			
			$(".textblock4").click(function(){
				$(".text5").show(0);
				$(".hint_table").show(0);
				$(".text6").show(0);
				$(".textblock4").hide(0);
				
			});
			
			$(".speed1").click(function(){
				
				
				$board.find(".wrapperIntro.eighth").css('background', 'url(activity/grade8/math/math_unitary_02/image/bank-closed.jpg)');
				
				$board.find(".wrapperIntro.eighth").css('background-size', '100%');
				$(".bankclosed").show(0);
				$(".ride").show(0);
				$(".bankopen").hide(0);
				$(".wheelfrontright >img").hide(0);
				$(".wheelbackright >img").hide(0);
				$(".scooty > img").hide(0);
				$(".background").hide(0);
				$(".hint").hide(0);
				$(".textblock4").hide(0);
				$(this).css('background-color','#D80404');
				$(".text0").hide(0);
				$(".text1").hide(0);
				$(".text2").hide(0);
				$(".text3").hide(0);
				$(".text5").hide(0);
				$(".text6").hide(0);
				$(".hint_table").hide(0);
				
			});
			
			$(".speed2").click(function(){
				$(".bankclosed").show(0);
				
				$board.find(".wrapperIntro.eighth").css('background', 'url(activity/grade8/math/math_unitary_02/image/bank-closed.jpg)');
				
				$board.find(".wrapperIntro.eighth").css('background-size', '100%');
				
				$(".ride").show(0);
				$(".bankopen").hide(0);
				$(".wheelfrontright >img").hide(0);
				$(".wheelbackright >img").hide(0);
				$(".scooty > img").hide(0);
				$(".background").hide(0);
				$(".hint").hide(0);
				$(".textblock4").hide(0);
				$(this).css('background-color','#D80404');
				$(".text0").hide(0);
				$(".text1").hide(0);
				$(".text2").hide(0);
				$(".text3").hide(0);
				$(".text5").hide(0);
				$(".text6").hide(0);
				$(".hint_table").hide(0);
				
			});
			
			$(".speed3").click(function(){
				$(".bankclosed").hide(0);
				$(".ride").show(0);
				$(".bankopen").show(0);
				
				
				$board.find(".wrapperIntro.eighth").css('background', 'url(activity/grade8/math/math_unitary_02/image/bank-open.jpg)');
				
				$board.find(".wrapperIntro.eighth").css('background-size', '100%');
				
				
				
				$(".wheelfrontright >img").hide(0);
				$(".wheelbackright >img").hide(0);
				$(".scooty > img").hide(0);
				$(".background").hide(0);
				$(".hint").hide(0);
				$(".textblock4").hide(0);
				$(this).css('background-color','#02B102');
				$(".text0").hide(0);
				$(".text1").hide(0);
				$(".text2").hide(0);
				$(".text3").hide(0);
				$(".text5").hide(0);
				$(".text6").hide(0);
				$(".hint_table").hide(0);
				$nextBtn.show(0);
				
			});
			
			$(".speed4").click(function(){
				
				
				
				$board.find(".wrapperIntro.eighth").css('background', 'url(activity/grade8/math/math_unitary_02/image/bank-closed.jpg)');
				
				$board.find(".wrapperIntro.eighth").css('background-size', '100%');
				
				
				//$(".bankclosed").show(0);
				$(".ride").show(0);
				$(".bankopen").hide(0);
				$(".wheelfrontright >img").hide(0);
				$(".wheelbackright >img").hide(0);
				$(".scooty > img").hide(0);
				$(".background").hide(0);
				$(".hint").hide(0);
				$(".textblock4").hide(0);
				$(this).css('background-color','#D80404');
				$(".text0").hide(0);
				$(".text1").hide(0);
				$(".text2").hide(0);
				$(".text3").hide(0);
				$(".text5").hide(0);
				$(".text6").hide(0);
				$(".hint_table").hide(0);
				
			});
			
			$board.find(".wrapperIntro.eighth .text0").show(0);
			$board.find(".wrapperIntro.eighth .text1").show(0);
			$board.find(".wrapperIntro.eighth .text2").show(0);
			$board.find(".wrapperIntro.eighth .text3").show(0);
			$nextBtn.hide(0);
			
		}
		
		
		
		
		
		
		function ninth () {
			var source = $("#intro8-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[8]

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.ninth .text0").show(0);
			
		}
		
		
		
		function ninth02 () {
			$board.find(".wrapperIntro.ninth .text1").show(0);
			
		}
		
		function ninth03 () {
			$board.find(".wrapperIntro.ninth .text2").show(0);
			
			
		}
		
		function ninth04 () {
			$board.find(".wrapperIntro.ninth .text3").show(0);
			
		}
		
		function ninth05 () {
			$board.find(".wrapperIntro.ninth .text4").show(0);
			
		}
		
		function ninth06 () {
			$board.find(".wrapperIntro.ninth .text5").show(0);
			
			
		}
		
		function ninth07 () {
			$board.find(".wrapperIntro.ninth .text6").show(0);
			
			
		}
		
		function ninth08 () {
			$board.find(".wrapperIntro.ninth .text7").show(0);
			
			
		}
		
		function ninth09 () {
			$board.find(".wrapperIntro.ninth .text8").show(0);
			
			
		}
		
		
		
		
		
		
	
/*----------------------------------------------------------------------------------------------------*/
		



		// first func call
		first();
		/*var start = 0;
		var timer;
		var counter = 0;
		var interval;*/
		/* removed by dilak
		$("#block1, #block3, #block5, #block7, #block9, #block11").draggable({
			 //var math = true;
			 revert: "true",
			 //refreshPositions: true,
			 drag: function( e, ui ) {
				 ui.helper.addClass("draggable");
			 }
	
		});
		
		//$( ".color" ).on( "drag", function( event, ui ) {
		$("#block2, #block4, #block6, #block8, #block10, #block12").droppable({
		    drop: function(e, ui) {
		    console.log(ui.droppable)
		    //$(this).css("background-color", ui.draggable.attr("id"));
			accept: "draggable" 
			}
		}); dilak end*/



		function allowDrop(ev) {
    ev.preventDefault();
    
		}

function drag(ev) {
    
    ev.dataTransfer.setData("text", ev.target.id);
   
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    ev.target.appendChild(document.getElementById(data));
    countme ++;
    if(countme==6)
        	{
        		
               $(".alert").show(0);
			   $nextBtn.show(0);
			 
        	}
		
  
}




	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			console.log(interval);
			if(("#totaltimetaken").value === null)
			{
				("#totaltimetaken").text =interval;
			}
			else
			{
				
			
			
			//$("#totaltimetaken").text=interval;
			console.log(countNext);
			
		
			
			
			
			
			
			
			
			if(countNext == 2){
				
				 countme = 0;
				
				
				
			}
			
			
			if(countNext == 27)
			{
				//alert("20");
				
				
				
			}
			
			if(countNext == 28)
			{
				//alert("20");
				$("#bg_td4").removeClass("bg");
				$("#bg_td5").removeClass("bg");
				$(".seven").removeClass("bg");
				$(".nine").removeClass("bg");
				
				$("#bg_td6").addClass("bg");
				$("#bg_td7").addClass("bg");
				$(".eight").addClass("bg");
				$(".ten").addClass("bg");
				
				
			}
			
		if(countNext == 29){
				$("#bg_td6").removeClass("bg");
				$("#bg_td7").removeClass("bg");
				$(".eight").removeClass("bg");
				$(".ten").removeClass("bg");
			
		}	
			}
			
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first slide
				//first01,
				first010,
				first02,
				//first020,
				first03,
				//first04,
				
				
				second,//second slide
				//second01,
				/*second02,
				second03,
				second030,
				second04,*/
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				//second11,
				second12,
				
				
				third,//third slide
				third01,
				third02,
				third03,
				third04,
				third05,
				
				fourth,
				//fourth01,
				fourth02,
				fourth03,
				
				
				fifth,
				//fifth01,
				//fifth02,
				//fifth030,
				//fifth03,
				//fifth04,
				fifth05,
				fifth06,
				fifth07,
				fifth08,
				fifth09,
				fifth10,
				//fifth11,
				//fifth12,
				
				
				
				sixth,
				/*sixth01,
				sixth02,
				sixth03,
				sixth04,
				sixth05,
				sixth06,
				sixth07,
				sixth08,*/
				sixth09,
				/*sixth10,
				sixth11,
				sixth12,
				sixth13,*/
				
				
				seventh,
				/*seventh01,*/
				seventh02,
				seventh03,
				
				
				eighth,
				/*eighth01,
				eighth02,
				eighth03,
				eighth04,
				eighth05,
				/*eighth06,
				eighth07,
				eighth08,
				eighth09,*/
				
				
				ninth,
				//ninth01,
				ninth02,
				ninth03,
				ninth04,
				ninth05,
				ninth06,
				ninth07,
				ninth08,
				ninth09
				
				
			
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/



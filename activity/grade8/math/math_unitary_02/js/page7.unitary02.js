$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 2;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{minu : data.string.p7_13, base : data.string.p7_7, ansstring : data.string.p7_4, ansque : data.string.p7_3, suffix : data.string.p1_0_2},
		{minu : data.string.p7_13, base : data.string.p7_12, ansstring : data.string.p7_9, ansque : data.string.p7_8, suffix : data.string.p1_0_2},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p7_1,
			qTitle : data.string.p7_2,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub,endNotification) {

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p7_5,
			ans : sub.base,
			min : sub.minu,
			answer : data.string.p7_6,
			text : sub.ansque,
			baseString : sub.ansstring,
			suffix : data.string.p1_0_2,
			couter : countNext+1,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		console.log(countNext);
		countNext<1?$nextBtn.show(0):
							ole.footerNotificationHandler.pageEndSetNotification();
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
			swal(data.string.h2);
		}else{

			if (base ===upDatas.base) {
				check[0]=1;
				$board.find(cls+" .check1").html(right);
			} else {
				$board.find(cls+" .check1").html(wrong);
			}



			countNext<1?$nextBtn.show(0):
								ole.footerNotificationHandler.pageEndSetNotification();
				$board.find(cls+" .clicks .click").show(0);
		}
	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;
		$(".q"+countNext).hide(0);

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext],true);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/
	var cor_count1 = 0;
	var cor_count2 = 0;
	var cor_count3 = 0;
	var cor_count4 = 0;
$(function () {
	var characterDialouges1 = [ {
		diaouges : data.string.p1_16
	},//

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p1_17
	},//

	];

	var characterDialouges3 = [ {
		diaouges : data.string.p1_18
	},//

	];


	var characterDialouges4 = [ {
		diaouges : ole.textSR(data.string.p1_19,data.string.p1_20,"<span class='green'>"+data.string.p1_20+"</span>")

	},//

	];




	var characterDialouges5 = [ {
		diaouges : data.string.p1_21
	},//

	];

	var characterDialouges6 = [ {
		diaouges : data.string.p1_22
	},//

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p1_23
	},//

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p1_24
	},//

	];

	//end of 2nd page//


	//3rd page//

	var characterDialouges9 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p1_27,data.string.p1_28,"<span class='ind2'>"+data.string.p1_28+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>")

	},//

	];

	var characterDialouges10 = [ {
		diaouges : data.string.p1_30
	},//
	];

	var characterDialouges11 = [ {
		diaouges : data.string.p1_31
	},//
	];

	var characterDialouges12 = [ {
		diaouges : data.string.p1_32
	},//
	];

	var characterDialouges13 = [ {
		diaouges : data.string.p1_33
	},//
	];

	var characterDialouges14 = [ {
		diaouges : data.string.p1_34
	},//
	];

	var characterDialouges15 = [ {
		diaouges : data.string.p1_35
	},//
	];

	var characterDialouges16 = [ {
		diaouges : data.string.p1_36
	},//fullname
	];

	var characterDialouges17 = [ {
		diaouges : data.string.p1_37
	},//firstname
	];


	var characterDialouges18 = [ {
		diaouges : data.string.p1_38
	},
	];

	var characterDialouges19 = [ {
		diaouges : data.string.p1_39
	},
	];

	var characterDialouges20 = [ {
		diaouges : data.string.p1_40
	},
	];


	var characterDialouges21 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p1_41,data.string.p1_42,"<span class='ind2'>"+data.string.p1_42+"</span>"), data.string.p1_2, "<span class='ind2'>"+data.string.p1_2+"</span>")

	},//

	];


	//end of 3rd page//


	//page4//

	var characterDialouges22 = [ {
		diaouges : data.string.p1_49
	},//input
	];

	var characterDialouges23 = [ {
		diaouges : data.string.p1_50
	},//output
	];

	var characterDialouges24 = [ {
		diaouges : data.string.p1_51
	},
	];

	var characterDialouges25 = [ {
		diaouges : data.string.p1_52
	},
	];

	var characterDialouges26 = [ {
		diaouges : data.string.p1_53

	},
	];


	var characterDialouges27 = [ {
		diaouges : data.string.p1_54
	},//input02
	];

	var characterDialouges28 = [ {
		diaouges : data.string.p1_55
	},//output02
	];


	var characterDialouges29 = [ {
		diaouges : data.string.p1_56

	},
	];

	var characterDialouges30 = [ {
		diaouges : data.string.p1_57

	},
	];

	var characterDialouges31 = [ {
		diaouges : data.string.p1_58

	},
	];
	//end of page4//


	//page5//


	var characterDialouges32 = [ {
		diaouges : data.string.p1_63
	},//input
	];

	var characterDialouges33 = [ {
		diaouges : data.string.p1_64
	},//output
	];

	var characterDialouges34 = [ {
		diaouges : data.string.p1_65
	},
	];

	var characterDialouges35 = [ {
		diaouges : data.string.p1_66
	},
	];

	var characterDialouges36 = [ {
		diaouges : data.string.p1_67
	},
	];


	var characterDialouges37 = [ {
		diaouges : data.string.p1_68
	},//input
	];

	var characterDialouges38 = [ {
		diaouges : data.string.p1_69
	},//output
	];


	var characterDialouges39 = [ {
		diaouges : data.string.p1_70
	},
	];

	var characterDialouges40 = [ {
		diaouges : data.string.p1_71
	},
	];

	var characterDialouges41 = [ {
		diaouges : data.string.p1_72
	},//output
	];




	//end of page5//

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=10;
		$nextBtn.show();
	//for displaying the finish page in the end


	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3,8]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
				data.string.p1_76,

			],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

				data.string.p1_76_1,
				data.string.p1_77,
				data.string.p1_78,
				data.string.p1_79,


			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",
				rowheading1 : [data.string.p1_82],
				rowheading2 : [data.string.p1_83],
				rowheading3 : [data.string.p1_84],
				rowheading4 : [data.string.p1_85],
				rowheading5 : [data.string.p1_86],
				rowheading6 : [data.string.p1_87],
				rowheading7 : [data.string.p1_88],
				rowheading8 : [data.string.p1_89],
			text : [
				data.string.p1_81,
				data.string.p1_82,
				data.string.p1_0,


				],
	}



	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};


		function first01 () {
			$board.find(".wrapperIntro.firstPage .main-bg1").hide(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg2").show(0);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .main-bg2").hide(0);
			$board.find(".wrapperIntro.firstPage .main-bg3").show(0);

		}


		function second () {
			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);

			$board.find(".img-a").hide(0);
			$board.find(".img-b").hide(0);
			$board.find(".img-c").hide(0);
			$board.find(".img-d").hide(0);

		}


		function second01 () {
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .think-bubble").show(0);


			$board.find(".highlight1").show(0);
			$board.find(".main-bg4").hide(0);




		}


		function second02 () {
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text2").show(0);



			$board.find(".highlight2").show(0);
			$board.find(".highlight1").hide(0);

		}


		function second03 () {
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").show(0);

			$board.find(".highlight3").show(0);
			$board.find(".highlight2").hide(0);

		}


		function second04 () {
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text1").show(0);


			$board.find(".highlight4").show(0);
			$board.find(".highlight3").hide(0);

		}






/*-------------------------------------------------------------------------------------------------*/

/*page3*/



		function third () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$(".check1, .check2, .check3, .check4").html(data.string.check);
			$(".nextBtn.myNextStyle").hide(0);

			$(".image1 > img").click(function(){
				$(".table1").fadeIn(1000);
				$(".answer").hide(0);
			});

			var lolz = $('.value1');
			$(".check1").click(function(){
				if(lolz.val() == 30)
				{
					$("#correct").show(0);
					$("#wrong").hide(0);
					$(".answer").hide(0);
					cor_count1 = 1;
					endcheck();
				}
				else
					{
						$("#wrong").show(0);
						$("#correct").hide(0);
						$(".answer").show(0);
					}

		});

		$(".image2 > img").click(function(){
				$(".table2").fadeIn(1000);
				$(".answer1").hide(0);
			});

		var lol = $('.value2');
			$(".check2").click(function(){
				if(lol.val() == 75)
				{
					$("#correct1").show(0);
					$("#wrong1").hide(0);
					$(".answer1").hide(0);
					cor_count2 = 1;
					endcheck();
				}
				else
					{
						$("#wrong1").show(0);
						$("#correct1").hide(0);
						$(".answer1").show(0);
					}
					//alert(lolz.val());
		});

			$(".image3 > img").click(function(){
				$(".table3").fadeIn(1000);
				$(".answer2").hide(0);
			});

			var l = $('.value3');
			$(".check3").click(function(){
				if(l.val() == 72)
				{
					$("#correct2").show(0);
					$("#wrong2").hide(0);
					$(".answer2").hide(0);
					cor_count3 = 1;
					endcheck();

				}
				else
					{
						$("#wrong2").show(0);
						$("#correct2").hide(0);
						$(".answer2").show(0);
					}
			});


			$(".image4 > img").click(function(){
				$(".table4").fadeIn(1000);
				$(".answer3").hide(0);
			});

			var lo = $('.value4');
			$(".check4").click(function(){
				if(lo.val() == 50)
				{
					$("#correct3").show(0);
					$("#wrong3").hide(0);
					$(".answer3").hide(0);
					cor_count4 = 1;
					endcheck();
				}
				else
					{
						$("#wrong3").show(0);
						$("#correct3").hide(0);
						$(".answer3").show(0);

					}
			});



		}


		function third01 () {

			$board.find(".wrapperIntro.third").css('background-image', 'url(activity/grade8/math/math_unitary_02/image/last.jpg)');
			$board.find(".wrapperIntro.third").css('background-size','100%');


			$board.find(".wrapperIntro.third .table1").hide(0);
			$board.find(".wrapperIntro.third .table2").hide(0);
			$board.find(".wrapperIntro.third .table3").hide(0);
			$board.find(".wrapperIntro.third .table4").hide(0);

			$board.find(".wrapperIntro.third .imgs1").hide(0);
			$board.find(".wrapperIntro.third .imgs2").hide(0);
			$board.find(".wrapperIntro.third .imgs3").hide(0);
			$board.find(".wrapperIntro.third .imgs4").hide(0);
			$board.find(".wrapperIntro.third .text0").hide(0);
			$board.find(".wrapperIntro.third .text2").show(0);





		}

		function third02 () {

			$board.find(".wrapperIntro.third .text1").hide(0);
			$board.find(".wrapperIntro.third .image1").hide(0);
			$board.find(".wrapperIntro.third .image2").hide(0);
			$board.find(".wrapperIntro.third .image3").hide(0);
			$board.find(".wrapperIntro.third .image4").hide(0);



		}



/*----------------------------------------------------------------------------------------------------*/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/



		$nextBtn.on('click',function () {
			// $(this).css("display","none");

			$prevBtn.show(0);
			countNext++;

			fnSwitcher();

		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first slide
				first01,
				first02,


				second,//second slide
				second01,
				second02,
				second03,
				second04,



				third,//third slide
				third01,
				third02

				/*third03,
				third04,*/


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext==all_page) {
				$nextBtn.hide(0);
				// $("#activity-page-finish-btn").show(0);
				// ole.footerNotificationHandler.lessonEndSetNotification();
				ole.activityComplete.finishingcall();
			} else {
				// $(this).show(0);
				$nextBtn.show(0);
			}
		}
	/****************/

});

/*checks if task has finished*/
function endcheck(){
	if(cor_count1 == 1 && cor_count2 == 1 && cor_count3 == 1 && cor_count4 == 1)
	{
	$nextBtn.show(0);
	}
}


$('.closebtn').click(function(){
  ole.activityComplete.finishingcall();
});

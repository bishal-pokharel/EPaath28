/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

	var characterDialouges1 = [ {
		diaouges : data.string.p10_1
	},

	];


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=42;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,2,21,33]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p10_2,
			data.string.p10_3,
			data.string.p10_4,
			data.string.p10_5,
			data.string.p10_6,
			data.string.p10_7,
			data.string.p10_8,
			data.string.p10_9,
			data.string.p10_10,
			data.string.p10_11,
			data.string.p10_12,
			data.string.p10_13,
			data.string.p10_14

			]

	},


	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",
		rowheading1 : [data.string.p10_14_09],
		rowheading2 : [data.string.p10_14_10],
			text : [
						data.string.p10_14_1,
						data.string.p10_14_2,
						data.string.p10_14_02,
						data.string.p10_14_3,
						data.string.p10_14_4,
						data.string.p10_14_5,
						data.string.p10_14_6,
						data.string.p10_14_7,
						data.string.p10_14_08,
						data.string.p10_14_8,
						data.string.p10_14_9,
						data.string.p10_14_11,

					]
	},

	{
		justClass : "fourth",
					rowheading1 : [data.string.p10_14_23],
					rowheading2 : [data.string.p10_14_24],

			text : [
						data.string.p10_14_12,
						data.string.p10_14_13,
						data.string.p10_14_14,
						data.string.p10_14_15,
						data.string.p10_14_16,
						data.string.p10_14_17,
						data.string.p10_14_18,
						data.string.p10_14_19,
						//data.string.p10_14_20,
						data.string.p10_14_21,
						data.string.p10_14_22,
					],
	}




	]




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var content = {
				title1 : data.string.p10_1,
				title2 : data.string.p10_1_1,
			}
			var html = template(content);

			$board.html(html);
			setTimeout(function(){
                $board.find(".wrapperIntro .title1").show(0);
            },100)


		};

		function first01 () {
			$board.find(".wrapperIntro .title2").show(0);

		}




		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");
			$board.find(".wrapperIntro.second .text4").hide(0);
			$board.find(".wrapperIntro.second .text5").hide(0);

		};



		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);

		}

		function second03 () {

			$board.find(".wrapperIntro.second .text3").css("background","#efefef");
			$board.find(".wrapperIntro.second .solution00").show(0);


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);

		}

		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);

		}

		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);

		}

		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);

		}

		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);

		}

		function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);

		}

		function second10 () {
			$board.find(".wrapperIntro.second .solution07").show(0);

		}

		function second11 () {
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .solution00").hide(0);
			$board.find(".wrapperIntro.second .solution01").hide(0);
			$board.find(".wrapperIntro.second .solution02").hide(0);
			$board.find(".wrapperIntro.second .solution03").hide(0);
			$board.find(".wrapperIntro.second .solution04").hide(0);
			$board.find(".wrapperIntro.second .solution05").hide(0);
			$board.find(".wrapperIntro.second .solution06").hide(0);
			$board.find(".wrapperIntro.second .solution07").hide(0);

			$board.find(".wrapperIntro.second .text4").show(0);




		}

		function second12 () {
			$board.find(".wrapperIntro.second .text5").show(0);

		}

		function second13 () {
			$board.find(".wrapperIntro.second .text6").show(0);

		}

		function second14 () {
			$board.find(".wrapperIntro.second .solution08").show(0);
			$board.find(".wrapperIntro.second .text7").css("background","#efefef");

		}

		function second15 () {
			$board.find(".wrapperIntro.second .solution08_1").show(0);

		}

		function second16 () {
			$board.find(".wrapperIntro.second .solution09").show(0);

		}

		function second17 () {
			$board.find(".wrapperIntro.second .solution10").show(0);

		}

		function second18 () {
			$board.find(".wrapperIntro.second .solution11").show(0);

		}



		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third .text0").show(0);
		};

	function third01 () {

			$board.find(".wrapperIntro.third .text1").show(0);

		}

		function third02 () {
			$board.find(".wrapperIntro.third .text2").show(0);

		}

		function third03 () {

			$board.find(".wrapperIntro.third .text3").show(0);
			$board.find(".wrapperIntro.third .new01").show(0);
			$board.find(".wrapperIntro.third .imageholder14").show(0);
			$board.find(".wrapperIntro.third .imageholder014").show(0);

		}

		function third04 () {
			$board.find(".wrapperIntro.third .text4").show(0);

		}

		function third05 () {

			$board.find(".wrapperIntro.third .text5").show(0);

		}

		function third06 () {
			$board.find(".wrapperIntro.third .text6").show(0);

		}

		function third07 () {

			$board.find(".wrapperIntro.third .text7").show(0);

		}

		function third08 () {
			$board.find(".wrapperIntro.third .text8").show(0);

		}

		function third09 () {

			$board.find(".wrapperIntro.third .text9").show(0);

		}

		function third10 () {
			$board.find(".wrapperIntro.third .text10").show(0);

		}

		function third11 () {
			$board.find(".wrapperIntro.third .text11").show(0);

		}



		function fourth() {

			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);
		};

	function fourth01 () {

			$board.find(".wrapperIntro.fourth .text1").show(0);

		}

		function fourth02 () {
			$board.find(".wrapperIntro.fourth .text2").show(0);
			$board.find(".wrapperIntro.fourth .new03").show(0);
			$board.find(".wrapperIntro.fourth .imageholder13").show(0);
			$board.find(".wrapperIntro.fourth .imageholder013").show(0);

		}

		function fourth03 () {

			$board.find(".wrapperIntro.fourth .text3").show(0);

		}

		function fourth04 () {
			$board.find(".wrapperIntro.fourth .text4").show(0);

		}

		function fourth05 () {

			$board.find(".wrapperIntro.fourth .text5").show(0);

		}

		function fourth06 () {
			$board.find(".wrapperIntro.fourth .text6").show(0);

		}

		function fourth07 () {

			$board.find(".wrapperIntro.fourth .text7").show(0);

		}

		function fourth08 () {
			$board.find(".wrapperIntro.fourth .text8").show(0);

		}

		function fourth09 () {

			$board.find(".wrapperIntro.fourth .text9").show(0);

		}

		function fourth10 () {
			$board.find(".wrapperIntro.fourth .text10").show(0);

		}

		function fourth11 () {
			$board.find(".wrapperIntro.fourth .text11").show(0);

		}




// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first01, //first one


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13,
				second14,
				second15,
				second16,
				second17,
				second18,


				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08,
				third09,
				third10,
				third11,

				fourth,
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,
				fourth07,
				fourth08,
				fourth09,
				fourth10

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=41;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5,18,36]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p4_47_0,
		data.string.p4_47,
		data.string.p4_48,
		data.string.p4_50,
		data.string.p4_51,

		data.string.p4_56,
		data.string.p4_57,
		data.string.p4_58
		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p5_1,
			data.string.p5_3,
			data.string.p5_4,
			data.string.p5_5,
			data.string.p5_6,
			data.string.p5_7,
			data.string.p5_8,
			data.string.p5_8_1,
			data.string.p5_9,
			data.string.p5_9_1,
			data.string.p5_10,
			data.string.p5_11


			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
			data.string.p6_1,
			data.string.p6_2,
			data.string.p6_3,
			data.string.p6_3_1,
			data.string.p6_4,
			data.string.p6_5,
			data.string.p6_6,
			data.string.p6_7,
			data.string.p6_8,
			data.string.p6_9,
			data.string.p6_10,
			data.string.p6_11,
			data.string.p6_12,
			data.string.p6_13,
			data.string.p6_14,
			data.string.p6_15,
			data.string.p6_16,
			data.string.p6_17
		],
	},


	{
		justClass : "fourth",
		rowheading1 : [data.string.p6_24],
		rowheading2 : [data.string.p6_25],
		text : [
			data.string.p6_18,
			data.string.p6_18_1,
			data.string.p6_18_2,
			data.string.p6_19,
			data.string.p6_20,
			data.string.p6_21,
			data.string.p6_22,
			data.string.p6_23,
			data.string.p6_26

		],
	}


]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			setTimeout(function(){
			$board.find(".wrapperIntro.firstPage .text0").hide(0);

			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);




			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);

			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);

            },100);

		};



		function first01 () {



			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);


			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);






		}

		function first02 () {

			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").hide(0);

			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .blackboard").show(0);


			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);



		}


		function first03 () {

			$(".nextBtn.myNextStyle").hide(0);

			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .con1").delay(1000).show(0);
			$board.find(".wrapperIntro.firstPage .con2").delay(1500).show(0);
			$board.find(".wrapperIntro.firstPage .con3").delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .con4").delay(2500).show(0);
			$board.find(".wrapperIntro.firstPage .con5").delay(3000).show(0);
			$(".nextBtn.myNextStyle").delay(3000).show(0);


		}


		function first04 () {

			$(".nextBtn.myNextStyle").hide(0);

			$board.find(".wrapperIntro.firstPage .text5").show(0);



			$board.find(".wrapperIntro.firstPage .con6").delay(1000).show(0);
			$board.find(".wrapperIntro.firstPage .con7").delay(1500).show(0);
			$board.find(".wrapperIntro.firstPage .con8").delay(2000).show(0);
			$(".nextBtn.myNextStyle").delay(3000).show(0);



		}




	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

			$board.find(".wrapperIntro.second .text0").hide(0);



			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.second .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").show(0);






			$board.find(".wrapperIntro.second .text1").show(0);
			$(".char-a").css("margin-left","5%")
			$(".char-b").css("margin-left","37%")
			$board.find(".wrapperIntro.second .bub2").show(0);


			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .boy-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .boy-listening01").hide(0);




		}


		function second01 () {

			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .bub2").hide(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .bub1").show(0);

			$board.find(".wrapperIntro.second .boy-talking02").hide(0);

			$board.find(".wrapperIntro.second .squirrel-talking02").show(0)
			$board.find(".wrapperIntro.second .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .squirrel-talking02").css('border-color','#fea92a');


			$board.find(".wrapperIntro.second .boy-listening01").show(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);



		}

		function second02 () {

			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .bub1").hide(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .bub2").show(0);



			$board.find(".wrapperIntro.second .boy-listening01").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").show(0);
			$board.find(".wrapperIntro.second .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second .squirrel-talking02").hide(0);


		}


		function second03 () {


			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").show(0);
			$board.find(".wrapperIntro.second .squirrel-listening03").hide(0);
			$board.find(".wrapperIntro.second .boy-talking02").hide(0);
			$board.find(".wrapperIntro.second .bub2").hide(0);

			$board.find(".wrapperIntro.second .blackboard").show(0);



		}

		function second030 () {


			$board.find(".wrapperIntro.second .text5").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text6").delay(1500).show(0);



			$(".nextBtn.myNextStyle").delay(1500).show(0);


		}



		function second04 () {

		$board.find(".wrapperIntro.second .text7").show(0);


		}

		function second040 () {


			$board.find(".wrapperIntro.second .divide").fadeOut(2000);
			$board.find(".wrapperIntro.second .multiply").fadeIn(3000);



		}

		function second041 () {



			$board.find(".wrapperIntro.second .text8").show(0);



		}

		function second05 () {

			$board.find(".wrapperIntro.second .text9").show(0);


		}


		function second06 () {

			$board.find(".wrapperIntro.second .multiply1").fadeOut(2000);
			$board.find(".wrapperIntro.second .divide1").fadeIn(3000);

		}


		function second060 () {

			$board.find(".wrapperIntro.second .text10").show(0);

		}


			function second07 () {

				$board.find(".wrapperIntro.second .text11").show(0);
				if($lang == 'np'){
					$(".meter, .min").addClass("margin15");
					$(".text11 > span").addClass("margin36");				
				}
		}




		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third .text0").show(0);
			$board.find(".wrapperIntro.third").css("background","#fff");



		}


		function third01() {
			$board.find(".wrapperIntro.third .text1").show(0);


		}


		function third02(){
			$board.find(".wrapperIntro.third .text2").show(0);
		}

		function third03(){
			$board.find(".wrapperIntro.third .text3").show(0);
		}

		function third04(){
			$board.find(".wrapperIntro.third .solution00").show(0);
			$board.find(".wrapperIntro.third .text4").css("background","#efefef");
		}


		function third05(){
			$board.find(".wrapperIntro.third .solution01").show(0);
		}


		function third06(){
			$board.find(".wrapperIntro.third .solution02").show(0);

		}

		function third07(){
			$board.find(".wrapperIntro.third .solution03").show(0);

		}

		function third08(){
			$board.find(".wrapperIntro.third .solution04").show(0);
		}


		function third09(){
			$board.find(".wrapperIntro.third .solution05").show(0);

		}

		function third10(){

			$board.find(".wrapperIntro.third .solution06").show(0);

		}

		function third11(){


			$board.find(".wrapperIntro.third .solution00").hide(0);
			$board.find(".wrapperIntro.third .solution01").hide(0);
			$board.find(".wrapperIntro.third .solution02").hide(0);
			$board.find(".wrapperIntro.third .solution03").hide(0);
			$board.find(".wrapperIntro.third .solution04").hide(0);
			$board.find(".wrapperIntro.third .solution05").hide(0);
			$board.find(".wrapperIntro.third .solution06").hide(0);
			$board.find(".wrapperIntro.third .solution07").show(0);

		}

		function third12(){
			$board.find(".wrapperIntro.third .solution08").show(0);

		}

		function third13(){
			$board.find(".wrapperIntro.third .solution09").show(0);

		}

		function third14(){
			$board.find(".wrapperIntro.third .solution10").show(0);

		}


		function third15(){
			$board.find(".wrapperIntro.third .solution11").show(0);

		}

		function third16(){
			$board.find(".wrapperIntro.third .solution12").show(0);

		}

		function third17(){
			$board.find(".wrapperIntro.third .solution13").show(0);

		}


		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);
			$board.find(".wrapperIntro.fourth .new").show(0);
			$board.find(".wrapperIntro.fourth .text1").show(0);
			$board.find(".wrapperIntro.fourth .text2").show(0);
			$board.find(".wrapperIntro.fourth .imageholder13").show(0);
			$board.find(".wrapperIntro.fourth .imageholder14").show(0);

		}


		/*function fourth01() {
			$board.find(".wrapperIntro.fourth .text1").show(0);

		}

		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);

		}

		function fourth03(){
			$board.find(".wrapperIntro.fourth .new").show(0);
		}*/

		function fourth04() {
			$board.find(".wrapperIntro.fourth .text3").show(0);
		}

		function fourth05(){
			$board.find(".wrapperIntro.fourth .text4").show(0);
		}

		function fourth06(){
			$board.find(".wrapperIntro.fourth .text5").hide(0);
		}

		function fourth07(){
			$board.find(".wrapperIntro.fourth .text6").hide(0);
		}

		function fourth08(){
			$board.find(".wrapperIntro.fourth .text5").show(0);
			$board.find(".wrapperIntro.fourth .text6").show(0);
			$board.find(".wrapperIntro.fourth .text7").show(0);
		}

		function fourth09(){
			$board.find(".wrapperIntro.fourth .text8").show(0);
		}
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;

			console.log(countNext);
			if(countNext == 40)
			{
				//alert("20");
				$("#bg_td").addClass("bg");
				$("#bg_td1").addClass("bg");
				$(".p7").addClass("bg");
				$(".p72").addClass("bg");


			}

			if(countNext == 41)
			{
				//alert("20");
				$("#bg_td").removeClass("bg");
				$("#bg_td1").removeClass("bg");
				$(".p7").removeClass("bg");
				$(".p72").removeClass("bg");

				$("#bg_td2").addClass("bg");
				$("#bg_td3").addClass("bg");
				$(".p71").addClass("bg");
				$(".p73").addClass("bg");


			}

			if(countNext == 42)
			{
				//alert("20");
				$("#bg_td2").removeClass("bg");
				$("#bg_td3").removeClass("bg");
				$(".p71").removeClass("bg");
				$(".p73").removeClass("bg");
			}

			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				//first05,


				second,
				second01,
				second02,
				second03,
				second030,
				second04,
				second040,
				second041,
				second05,
				second06,
				second060,
				second07,





				third,
				third01,
				third02,
				third03,

				third04,
				third05,
				third06,
				third07,
				third08,
				third09,
				third10,
				third11,
				third12,
				third13,
				third14,
				third15,
				third16,
				third17,


				fourth,
				/*fourth01,
				fourth02,
				fourth03,*/
				fourth04,
				fourth05,
				fourth06,
				fourth07,
				fourth08,
				fourth09




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

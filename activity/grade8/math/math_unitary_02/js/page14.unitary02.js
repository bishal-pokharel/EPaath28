/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=8;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

		data.string.p14_1,

		data.string.p14_2,
		data.string.p14_3,
		data.string.p14_4,
		data.string.p14_5,
		data.string.p14_6,
		data.string.p14_7,
		data.string.p14_8,
		data.string.p14_9
		],

	}






	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/

]






	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/








/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		/*first call to first*/
		// first();

		function second () {

			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("background","#efefef");

		}


		function third () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);

		}



		function fourth(){
			$board.find(".wrapperIntro.firstPage .solution02").show(0);
		}


		function fifth(){
			$board.find(".wrapperIntro.firstPage .solution03").show(0);
		}


		function sixth(){
			$board.find(".wrapperIntro.firstPage .solution04").show(0);
		}




		function seventh () {
			$board.find(".wrapperIntro.firstPage .solution05").show(0);
		}


		function eighth () {
			$board.find(".wrapperIntro.firstPage .solution06").show(0);
		}


		function ninth () {
			$board.find(".wrapperIntro.firstPage .solution07").show(0);
		}









	/*-------------------------------------------------------------------------------------------*/

	/*second page*/

















		/*********/


		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				second,
				third,
				fourth,
				fifth,
				sixth,
				seventh,
				eighth,
				ninth



			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=23;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,3,7,14]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage_coverPage",
		animate : "true",
		image:true,
		text : [function () {
			var d = ole.textSR(data.string.p2_1,data.string.p2_2,"<span class='ind2'>"+data.string.p2_2+"</span>");

			return d;
		},],

	},{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(data.string.p2_1,data.string.p2_2,"<span class='ind2'>"+data.string.p2_2+"</span>");

			return d;
		},

		data.string.p2_3,

		data.string.p2_4],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p2_5,
			data.string.p2_6,
			data.string.p2_7,
			data.string.p2_8],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [
																																			                data.string.p2_9,																																					                data.string.p2_11,
				data.string.p2_12,
				data.string.p2_13,
			    data.string.p2_14,
			    data.string.p2_15,
				data.string.p2_16,
				data.string.p2_17,
				data.string.p2_20,



				],
	},



	{
		justClass : "fourth",

			text : [

			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_31


			],
	}




	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};
		function first_sec() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

		};



		function first01 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function first02 () {
			//$board.find(".wrapperIntro.firstPage .text2").show(0);

		}








	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		/*end of text 0 firstline*/

		/*first click*/
		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		/* end of first click*/


		/* second click*/
		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);


		}



		function second03 () {
			$board.find(".wrapperIntro.second .text3").show(0);


		}
		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third .text0").hide(0);


			$board.find(".wrapperIntro.third .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.third .char-b").addClass("slideleft");


			$board.find(".wrapperIntro.third .squirrel-eating-sweet").show(0);
			$board.find(".wrapperIntro.third .boy-listening01").show(0);


			$board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);

			$board.find(".wrapperIntro.third .text1").show(0);
			$board.find(".wrapperIntro.third .bubble1").show(0);

			$board.find(".wrapperIntro.third .boy-listening01").hide(0);
			$board.find(".wrapperIntro.third .boy-talking01").show(0);
			$board.find(".wrapperIntro.third .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.third .boy-talking01").css('border-color','#fea92a');


		};




		function third01() {

		$board.find(".wrapperIntro.third .bubble1").hide(0);
			$board.find(".wrapperIntro.third .bubble2").show(0);
			$board.find(".wrapperIntro.third .text1").hide(0);
			$board.find(".wrapperIntro.third .text2").show(0);


			$board.find(".wrapperIntro.third .boy-talking01").hide(0);
			$board.find(".wrapperIntro.third .boy-listening01").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.third .squirrel-eating-sweet").hide(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").css('border-color','#fea92a');


		}


		function third02(){


			$board.find(".wrapperIntro.third .text2").hide(0);
			//$board.find(".wrapperIntro.third .text3").show(0);
			//$board.find(".wrapperIntro.third .text3").addClass("slideLeft");

		}

		function third03(){


			$board.find(".wrapperIntro.third .text2").hide(0);
			$board.find(".wrapperIntro.third .bubble1").hide(0);
			$board.find(".wrapperIntro.third .bubble2").show(0);
			$board.find(".wrapperIntro.third .text3").hide(0);
			$board.find(".wrapperIntro.third .text4").show(0);
			//$board.find(".wrapperIntro.third .text4").addClass("slideRight");
			//$board.find(".wrapperIntro.third .bubble2").addClass("slideRight");

			$board.find(".wrapperIntro.third .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.third .boy-listening02").show(0);
			$board.find(".wrapperIntro.third .squirrel-listening01").hide(0);
			$board.find(".wrapperIntro.third .boy-talking02").hide(0);




		}

		function third04(){

			$board.find(".wrapperIntro.third .bubble2").hide(0);
			$board.find(".wrapperIntro.third .bubble1").show(0);
			$board.find(".wrapperIntro.third .text4").hide(0);
			$board.find(".wrapperIntro.third .text5").show(0);
			//$board.find(".wrapperIntro.third .text5").addClass("slideLeft");
			//$board.find(".wrapperIntro.third .bubble1").addClass("slideLeft");


			$board.find(".wrapperIntro.third .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.third .boy-listening01").hide(0);
			$board.find(".wrapperIntro.third .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").css('background','#FFFC7B');

		}


		function third05(){

			$board.find(".wrapperIntro.third .bubble1").hide(0);
			$board.find(".wrapperIntro.third .bubble2").show(0);
			$board.find(".wrapperIntro.third .text5").hide(0);
			$board.find(".wrapperIntro.third .text6").show(0);
			//$board.find(".wrapperIntro.third .text6").addClass("slideRight");
			//$board.find(".wrapperIntro.third .bubble2").addClass("slideRight");

			$board.find(".wrapperIntro.third .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.third .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.third .boy-listening01").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").hide(0);
			$board.find(".wrapperIntro.third .squirrel-listening01").hide(0);

		}


		function third06(){

		$board.find(".wrapperIntro.third .bubble2").hide(0);
			$board.find(".wrapperIntro.third .bubble1").show(0);
			$board.find(".wrapperIntro.third .text6").hide(0);
			$board.find(".wrapperIntro.third .text7").show(0);



			$board.find(".wrapperIntro.third .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.third .boy-listening01").hide(0);
			$board.find(".wrapperIntro.third .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").css('background','#FFFC7B');


		}


		function third07(){

			$board.find(".wrapperIntro.third .text7").hide(0);
			$board.find(".wrapperIntro.third .squirrel-listening01").hide(0);
			$board.find(".wrapperIntro.third .bubble1").hide(0);
			$board.find(".wrapperIntro.third").css('background', 'url(activity/grade8/math/math_interest_02/image/page4/letslook.png)');
			$board.find(".wrapperIntro.third").css("background-size","100%");




			$board.find(".wrapperIntro.third .text8").show(0);
			$board.find(".wrapperIntro.third .text8").show(0);
			$board.find(".wrapperIntro.third .boy-talking03").hide(0);

		}


		function third08(){

		}






/*----------------------------------------------------------------------------------------------------*/


		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth").css("padding","1%");
			$board.find(".wrapperIntro.fourth").css("background","#fff");
		}


		function fourth01(){
			$board.find(".wrapperIntro.fourth .text1").show(0);


		}

		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);



		}

		function fourth03(){

			$board.find(".wrapperIntro.fourth .solution00").show(0);
			$board.find(".wrapperIntro.fourth .text3").css("background","#efefef");
		}

		function fourth04(){
			$board.find(".wrapperIntro.fourth .solution01").show(0);


		}


		function fourth05(){
			$board.find(".wrapperIntro.fourth .solution02").show(0);


		}


		function fourth06(){
			$board.find(".wrapperIntro.fourth .solution03").show(0);


		}

		function fourth07(){
			$board.find(".wrapperIntro.fourth .solution04").show(0);


		}

		function fourth08(){
			$board.find(".wrapperIntro.fourth .solution05").show(0);


		}


		function fourth09(){
			$board.find(".wrapperIntro.fourth .solution06").show(0);


		}


		function fourth10(){
			$board.find(".wrapperIntro.fourth .solution07").show(0);
			$board.find(".wrapperIntro.fourth .solution07").css("display","block");


		}

		function fourth11(){
			//$board.find(".wrapperIntro.fourth .text4").show(0);
			$board.find(".wrapperIntro.fourth .solution07").hide(0);
			$board.find(".wrapperIntro.fourth .text3").css("height","61%");

		}






		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			$(".coverpage").hide(0);
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first_sec,
				// first01,
				//first02,

				second,
				second01,
				second02,
				second03,

				third,
				third01,
				//third02,
				third03,
				third04,
				third05,
				third06,
				third07,






				fourth,//fourthPage
				fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,
				fourth07,
				fourth08,
				fourth09,
				fourth10,
				//fourth11


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

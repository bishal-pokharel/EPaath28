/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=23;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5,14]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [

			data.string.p8_1,
			data.string.p8_2,
			data.string.p8_3,
			data.string.p8_4,
			data.string.p8_5,
			data.string.p8_6

			]

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [

			data.string.p8_7,
			data.string.p8_8,
			data.string.p8_9,
			data.string.p8_10,
			data.string.p8_11,
			data.string.p8_12,
			data.string.p8_13

			]

	},


	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",
		rowheading1 : [data.string.p8_13_5],
		rowheading2 : [data.string.p8_13_6],
			text : [

				data.string.p8_13_1,
				data.string.p8_13_2,
				data.string.p8_13_3,
				data.string.p8_13_4,
				data.string.p8_13_4_1,
				data.string.p8_13_4_02,
				data.string.p8_13_4_2,
				data.string.p8_13_4_3,
				data.string.p8_13_4_4,
				data.string.p8_13_7


				],
	},



	{
		justClass : "fourth",

			text : [




			],
	}




	]




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			setTimeout(function(){
				$board.find(".wrapperIntro.firstPage .text0").hide(0);
				$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
				$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
				$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
				$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
				$board.find(".wrapperIntro.firstPage,.wrapperIntro.firstPage").show(0);

				$board.find(".wrapperIntro.firstPage .text1").show(0);
				$board.find(".wrapperIntro.firstPage .bubble2").show(0);



			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);

            },100);


		};


		function first01 () {


			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);



			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#fea92a');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);


		}


		function first02 () {

			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);

		}


		function first03(){

			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);

		}


		function first04(){

			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);

		}







		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("padding","1%");
			$board.find(".wrapperIntro.second").css("background","#fff");

		};



		function second01 () {

			$board.find(".wrapperIntro.second .text1").show(0);

		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);

		}

		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");

		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);

		}

		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);

		}

		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);

		}

		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);

		}

		function second08 () {
			$board.find(".wrapperIntro.second .solution05").show(0);

		}

		/*function second09 () {
			$board.find(".wrapperIntro.second .solution06").show(0);

		}*/


		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third .text0").show(0);
			//$board.find(".wrapperIntro.second").css("padding","1%");
			//$board.find(".wrapperIntro.second").css("background","#fff");

		};



		function third01 () {

			$board.find(".wrapperIntro.third .text1").show(0);

		}

		function third02 () {
			$board.find(".wrapperIntro.third .text2").show(0);
			$board.find(".wrapperIntro.third .table01").show(0);
			$board.find(".wrapperIntro.third .imageholder013").show(0);
			$board.find(".wrapperIntro.third .imageholder014").show(0);
		}

		function third03 () {

			$board.find(".wrapperIntro.third .text3").show(0);

		}

		function third04 () {
			$board.find(".wrapperIntro.third .text4").show(0);

		}

		function third05 () {
			$board.find(".wrapperIntro.third .text5").show(0);

		}

		function third06 () {
			$board.find(".wrapperIntro.third .text6").show(0);

		}

		function third07 () {
			$board.find(".wrapperIntro.third .text7").show(0);

		}

		function third08 () {
			$board.find(".wrapperIntro.third .text8").show(0);

		}

		function third09 () {
			$board.find(".wrapperIntro.third .text9").show(0);

		}





// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				//first05,


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				//second09,

				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08,
				third09




			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

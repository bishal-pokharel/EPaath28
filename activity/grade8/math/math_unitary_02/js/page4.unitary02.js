/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

	var characterDialouges1 = [ {
		diaouges : data.string.p4_5_0
	},//des1

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p4_6
	},//des2

	];


	var characterDialouges3 = [ {
		diaouges : data.string.p4_7
	},//des3

	];

	var characterDialouges4 = [ {
		diaouges : data.string.p4_8
	},//des4

	];

	var characterDialouges5 = [ {
		diaouges : data.string.p4_9
	},//des5

	];


	var characterDialouges6 = [ {
		diaouges : data.string.p4_10
	},//des6

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p4_11
	},//des7

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p4_12
	},//des8

	];


	var characterDialouges9 = [ {
		diaouges : data.string.p4_13
	},
	];//des9


	var characterDialouges10 = [ {
		diaouges : data.string.p4_14
	},
	];//des10

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=28;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,12,16]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p4_1,
		data.string.p4_2,
		data.string.p4_3,
		data.string.p4_4,
		data.string.p4_5,
		data.string.p4_5_0_1

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [
						data.string.p4_12


			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

			text : [

						data.string.p4_16,
						data.string.p4_17,
						data.string.p4_18,
						data.string.p4_19

				],
	},



	{
		justClass : "fourth",

			text : [

			data.string.p2_21,
			data.string.p2_22,
			data.string.p2_23,
			data.string.p2_24,
			data.string.p2_25,
			data.string.p2_26,
			data.string.p2_27,
			data.string.p2_28,
			data.string.p2_29,
			data.string.p2_30,
			data.string.p2_31,



			],
	}




	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];

			var html = template(content);

			$board.html(html);




		};



		function first01 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage").css("padding","1%");

		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .text3").show(0);


		}

		function first04 () {
			$board.find(".wrapperIntro.firstPage .text4").show(0);

		}


		function first040 () {

			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text5").css("background","#efefef");

		}

		function first05 () {
			$board.find(".wrapperIntro.firstPage .solution01").show(0);

		}


		function first06(){
			$board.find(".wrapperIntro.firstPage .solution02").show(0);

		}


		function first060(){
			$board.find(".wrapperIntro.firstPage .solution02_1").show(0);

		}

		function first07(){

			$board.find(".wrapperIntro.firstPage .solution03").show(0);

		}

		function first08(){

			$board.find(".wrapperIntro.firstPage .solution04").show(0);

		}


		function first09(){

			$board.find(".wrapperIntro.firstPage .solution05").show(0);
		}

		function first10(){

			$board.find(".wrapperIntro.firstPage .solution06").show(0);

		}


		function first11(){

			$board.find(".wrapperIntro.firstPage .solution07").show(0);

		}




		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}

		function second01(){

			appendDialouge = '<div class="p4_12">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);

		}

		function second02 () {
			$(".nextBtn.myNextStyle").hide(0).delay(3000).show(0);
			$board.find(".wrapperIntro.second .f3_2").show(0);
			$board.find(".wrapperIntro.second .f3_2").addClass("slideRight");
			$board.find(".wrapperIntro.second .f3_2").css('display', 'inline-flex');
			$board.find(".wrapperIntro.second .main-bg").show(0);

		}


		function second03(){
			appendDialouge = '<div class="p4_13">';
				var dialouges =$.each(characterDialouges9,function(key,values){
				chDialouges = values.diaouges;

				appendDialouge +=chDialouges;
			});
			appendDialouge += '</div>';
			$(".des9").append(appendDialouge);

		}

		function second04(){
			appendDialouge = '<div class="p4_14">';
				var dialouges =$.each(characterDialouges10,function(key,values){
				chDialouges = values.diaouges;

				appendDialouge +=chDialouges;
			});
			appendDialouge += '</div>';
			$(".des10").append(appendDialouge);



		}
	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third").css("background","#fff");
			$board.find(".wrapperIntro.third").css("padding","1%");
		}
		/*end of text 0 firstline*/

		/*first click*/
		function third01 () {
			$board.find(".wrapperIntro.third .text1").show(0);
		}
		/* end of first click*/


		/* second click*/
		function third02 () {
			$board.find(".wrapperIntro.third .text2").show(0);


		}



		function third03 () {
			$board.find(".wrapperIntro.third .solution00").show(0);
			$board.find(".wrapperIntro.third .text3").css("background","#efefef");


		}
		function third04 () {
			$board.find(".wrapperIntro.third .solution01").show(0);


		}

		function third05 () {
			$board.find(".wrapperIntro.third .solution02").show(0);


		}

		function third06 () {
			$board.find(".wrapperIntro.third .solution03").show(0);


		}

		function third07 () {
			$board.find(".wrapperIntro.third .solution04").show(0);


		}

		function third08 () {
			$board.find(".wrapperIntro.third .solution05").show(0);


		}

		function third09 () {
			$board.find(".wrapperIntro.third .solution06").show(0);


		}

		function third10 () {
			$board.find(".wrapperIntro.third .solution07").show(0);


		}

		function third11 () {
			$board.find(".wrapperIntro.third .solution08").show(0);


		}
		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		/*function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]


			var datavar={
				p2_11: data.string.p2_11,
				img1s:$ref+"/image/char-a.png",
				img2s:$ref+"/image/char-b.png",
				p2_12: data.string.p2_12,
				p2_13: data.string.p2_13,
				p2_14: data.string.p2_14,
				p2_15: data.string.p2_15,
				p2_16: data.string.p2_16,
				p2_17: data.string.p2_17,
				p2_18: data.string.p2_18,
				p2_19: data.string.p2_19,
				p2_20: data.string.p2_20
			}

			var html = template(content);
			var html=template(datavar);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro .f3_0").addClass("slideRight");


		}


		function third01() {
			$board.find(".wrapperIntro .f3_1").show(0);
			$board.find(".wrapperIntro .f3_1").css('display', 'inline-flex');
			$board.find(".wrapperIntro .f3_1").addClass("slideRight");

		}


		function third02(){
			$board.find(".wrapperIntro .f3_2").show(0);
			$board.find(".wrapperIntro .f3_2").css('display', 'inline-flex');
			$board.find(".wrapperIntro .f3_2").addClass("slideLeft");
		}

		function third03(){
			$board.find(".wrapperIntro .f3_3").show(0);
			$board.find(".wrapperIntro .f3_3").css('display', 'inline-flex');
			$board.find(".wrapperIntro .f3_3").addClass("slideRight");
		}

		function third04(){
			$board.find(".wrapperIntro .f3_4").show(0);
			$board.find(".wrapperIntro .f3_4").css('display', 'inline-flex');
			$board.find(".wrapperIntro .f3_4").addClass("slideLeft");
		}


		function third05(){
			$board.find(".wrapperIntro .f3_5").show(0);
			$board.find(".wrapperIntro .f3_5").css('display', 'inline-flex');
			$board.find(".wrapperIntro .f3_5").addClass("slideRight");
		}


		function third06(){
			$board.find(".wrapperIntro .f3_6").show(0);

		}


/*----------------------------------------------------------------------------------------------------*/


		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}


		function fourth01(){
			$board.find(".wrapperIntro.fourth .text1").show(0);


		}

		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);


		}

		function fourth03(){
			$board.find(".wrapperIntro.fourth .text3").show(0);



		}

		function fourth04(){
			$board.find(".wrapperIntro.fourth .text4").show(0);


		}


		function fourth05(){
			$board.find(".wrapperIntro.fourth .text5").show(0);


		}


		function fourth06(){
			$board.find(".wrapperIntro.fourth .text6").show(0);


		}

		function fourth07(){
			$board.find(".wrapperIntro.fourth .text7").show(0);


		}

		function fourth08(){
			$board.find(".wrapperIntro.fourth .text8").show(0);


		}


		function fourth09(){
			$board.find(".wrapperIntro.fourth .text9").show(0);


		}


		function fourth10(){
			$board.find(".wrapperIntro.fourth .text10").show(0);


		}






		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				first040,
				first05,
				first06,
				//first060,
				first07,
				first08,
				first09,
				first10,
				first11,
				//first12,
				//first13,
				//first14,
				//first15,


				second,
				//second01,
				second02,
				second03,
				second04,
				/*second05,
				second06,
				second07,
				second08,*/

				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08,
				third09,
				third10,
				third11,


				fourth,//fourthPage
				fourth01,
				fourth02,
				fourth03,
				fourth04,

				fourth05,
				fourth06,
				fourth07,
				fourth08,
				fourth09,
				fourth10


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

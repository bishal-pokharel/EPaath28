/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=17;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [17]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
	{
		justClass : "firstPage",

		title_1 : [data.string.p18_1],
		title_2 : [data.string.p18_2],
		title_3 : [data.string.p18_7],
		title_4 : [data.string.p18_8],
		rowheading1 : [data.string.p18_9],
		rowheading2 : [data.string.p18_12],
		text : [
		        data.string.p18_3,
		        data.string.p18_15,
		        data.string.p18_4,
		        data.string.p18_16,
			],
	},

	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};


		function first_1() {

			$board.find(".wrapperIntro.firstPage .title_2").show(0);

		};

		function first_2() {

			$board.find(".wrapperIntro.firstPage .title_3").show(0);

		};

		function first_3() {

			$board.find(".wrapperIntro.firstPage .title_4").show(0);

		};

		function first_4() {

			$board.find(".wrapperIntro.firstPage .exmpletable").show(0);

		};

		function first_5() {

			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);

		};

		function first_6() {

			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .diagonal").show(0);

		};

		function first_7(){
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .diagonal").hide(0);
			$board.find(".wrapperIntro.firstPage .solution00").show(0);
			$board.find(".wrapperIntro.firstPage .text3").css("background","#efefef");
		}

		function first_8(){
			$board.find(".wrapperIntro.firstPage .imageholder13").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder14").show(0);
			$board.find(".wrapperIntro.firstPage .solution01").show(0);
		}

		function first_9(){
			$board.find(".wrapperIntro.firstPage .solution02").show(0);
		}

		function first_10(){
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").css("height","53%");
			$board.find(".wrapperIntro.firstPage .img-des13").css("top","-74%");
			$board.find(".wrapperIntro.firstPage .img-des14").css("top","-82%");
			$board.find(".wrapperIntro.firstPage .solution00").hide(0);
			$board.find(".wrapperIntro.firstPage .solution01").hide(0);
			$board.find(".wrapperIntro.firstPage .solution02").hide(0);
			$board.find(".wrapperIntro.firstPage .solution03").show(0);
		}


		function first_11(){

			$board.find(".wrapperIntro.firstPage .solution04").show(0);
		}

		function first_12(){
			$board.find(".wrapperIntro.firstPage .solution05").show(0);
		}

		function first_13(){
			$board.find(".wrapperIntro.firstPage .solution06").show(0);
		}

		function first_14(){
			$board.find(".wrapperIntro.firstPage .solution07").show(0);
		}

		function first_15(){
			$board.find(".wrapperIntro.firstPage .solution03").hide(0);
			$board.find(".wrapperIntro.firstPage .solution04").hide(0);
			$board.find(".wrapperIntro.firstPage .solution05").hide(0);
			$board.find(".wrapperIntro.firstPage .solution06").hide(0);
			$board.find(".wrapperIntro.firstPage .solution07").hide(0);
			$board.find(".wrapperIntro.firstPage .solution08").show(0);
		}

		function first_16(){
			$board.find(".wrapperIntro.firstPage .solution09").show(0);
		}

		function first_17(){


			$board.find(".wrapperIntro.firstPage .solution10").show(0);
		}


	/*-------------------------------------------------------------------------------------------*/









		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});


		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first_1,
				first_2,
				first_3,
				first_4,
				first_5,
				first_6,
				first_7,
				first_8,
				first_9,
				first_10,
				first_11,
				first_12,
				first_13,
				first_14,
				first_15,
				first_16,
				first_17

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

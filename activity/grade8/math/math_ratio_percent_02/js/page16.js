/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var characterDialouges1 = [ {
		diaouges : ole.textSR(data.string.p16_13,data.string.p16_13,"<span class='ind2'>"+data.string.p16_13+"</span>")
	},//

	];

	var characterDialouges2 = [ {
		diaouges : ole.textSR(data.string.p16_14,data.string.p16_14,"<span class='ind2'>"+data.string.p16_14+"</span>")
	},//

	];

	var characterDialouges3 = [ {
		diaouges : ole.textSR(data.string.p16_15,data.string.p16_15,"<span class='ind2'>"+data.string.p16_15+"</span>")
	},//

	];


	var characterDialouges4 = [ {
		diaouges : ole.textSR(data.string.p16_16,data.string.p16_16,"<span class='ind2'>"+data.string.p16_16+"</span>")
	},//

	];




	var characterDialouges5 = [ {
		diaouges : ole.textSR(data.string.p16_17,data.string.p16_17,"<span class='ind2'>"+data.string.p16_17+"</span>")
	},//

	];

	var characterDialouges6 = [ {
		diaouges : ole.textSR(data.string.p16_18,data.string.p16_18,"<span class='ind2'>"+data.string.p16_18+"</span>")
	},//

	];

	var characterDialouges7 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_19,data.string.p16_19_1,"<span class='ind3'>"+data.string.p16_19_1+"</span>"), data.string.p16_19_2, "<span class='ind4'>"+data.string.p16_19_2+"</span>")
	},//

	];

	var characterDialouges8 = [ {
		diaouges : ole.textSR(data.string.p16_20,data.string.p16_20,"<span class='ind2'>"+data.string.p16_20+"</span>")
	},
	];

	//end of 2nd page//


	//3rd page//

	var characterDialouges9 = [ {
		diaouges : ole.textSR(data.string.p16_21,data.string.p16_21,"<span class='ind2'>"+data.string.p16_21+"</span>")
	},//

	];

	var characterDialouges10 = [ {
		diaouges : ole.textSR(data.string.p16_22,data.string.p16_22,"<span class='ind2'>"+data.string.p16_22+"</span>")
	},//
	];

	var characterDialouges11 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_23,data.string.p16_24,"<span class='ind3'>"+data.string.p16_24+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

	var characterDialouges12 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_25,data.string.p16_25,"<span class='ind2'>"+data.string.p16_25+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

	var characterDialouges13 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_26,data.string.p16_26,"<span class='ind2'>"+data.string.p16_26+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

	var characterDialouges14 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_27,data.string.p16_27,"<span class='ind2'>"+data.string.p16_27+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

	var characterDialouges15 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_28,data.string.p16_28,"<span class='ind2'>"+data.string.p16_28+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

	var characterDialouges16 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p16_29,data.string.p16_29,"<span class='ind2'>"+data.string.p16_29+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=23;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [23]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
	{
		justClass : "firstPage",

		text : [
		        data.string.p16_1,
		        data.string.p16_2,
		        data.string.p16_3,
			],
			pricetag1 :[data.string.p16_5],
			pricetag2 :[data.string.p16_7],
			pricetag3 :[data.string.p16_6],
			pricestatement1 : [data.string.p16_30],
			pricestatement2 : [data.string.p16_31],
			pricestatement3 : [data.string.p16_32],
			pricestatement : [data.string.p16_33]
	},

	{
		justClass : "second",
		title_1 : [data.string.p16_1],
		title_2 : [data.string.p16_8],
		title_3 : [data.string.p16_9],
		rowheading1 : [data.string.p16_10],
		rowheading2 : [data.string.p16_13],
		text : [
		        data.string.p16_4,
		        data.string.p16_16,
		        data.string.p16_15,
		        data.string.p16_17
			],


	},

	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		/*first call to first*/
		// first();

		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function third () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
			//$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		function fourth () {
			$board.find(".wrapperIntro.firstPage .imageholder02").show(0);
			$board.find(".wrapperIntro.firstPage .price").show(0);
			$board.find(".wrapperIntro.firstPage .pricestatement1").show(0);
			$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		function fifth () {
			$board.find(".wrapperIntro.firstPage .imageholder02").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder03").show(0);
			$board.find(".wrapperIntro.firstPage .price").show(0);
			$board.find(".wrapperIntro.firstPage .tag1").hide(0);
			$board.find(".wrapperIntro.firstPage .tag2").show(0);
			$board.find(".wrapperIntro.firstPage .pricestatement1").hide(0);
			$board.find(".wrapperIntro.firstPage .pricestatement2").show(0);
			$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		function sixth () {
			$board.find(".wrapperIntro.firstPage .imageholder03").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder04").show(0);
			$board.find(".wrapperIntro.firstPage .price").show(0);
			$board.find(".wrapperIntro.firstPage .tag2").hide(0);
			$board.find(".wrapperIntro.firstPage .tag3").show(0);
			$board.find(".wrapperIntro.firstPage .pricestatement2").hide(0);
			$board.find(".wrapperIntro.firstPage .pricestatement3").show(0);
			$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		function seventh () {
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .img-des2").hide(0);
			$board.find(".wrapperIntro.firstPage .img-des3").hide(0);
			$board.find(".wrapperIntro.firstPage .img-des4").hide(0);
			$board.find(".wrapperIntro.firstPage .price").hide(0);
			$board.find(".wrapperIntro.firstPage .pricestatement3").hide(0);
			$board.find(".wrapperIntro.firstPage .imageholder05").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder06").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder07").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder08").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder09").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder10").show(0);
			$board.find(".wrapperIntro.firstPage .price1").show(0);
			$board.find(".wrapperIntro.firstPage .price2").show(0);
			$board.find(".wrapperIntro.firstPage .price3").show(0);
			$board.find(".wrapperIntro.firstPage .imageholder11").show(0);
			$board.find(".wrapperIntro.firstPage .pricestatement").show(0);

		}
	/*-------------------------------------------------------------------------------------------*/

		function second_1() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

		};

		function second_2() {

			$board.find(".wrapperIntro.second .title_2").show(0);

		};

		function second_3() {

			$board.find(".wrapperIntro.second .title_3").show(0);

		};

		function second_4() {

			$board.find(".wrapperIntro.second .exmpletable").show(0);

		};

		function second_5() {

			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);

		};

		function second_6() {

			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .diagonal").show(0);

		};

		function second_7(){
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .diagonal").hide(0);
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");
		}

		function second_8(){
			$board.find(".wrapperIntro.second .imageholder13").show(0);
			$board.find(".wrapperIntro.second .imageholder14").show(0);
			$board.find(".wrapperIntro.second .solution01").show(0);
		}

		function second_9(){
			$board.find(".wrapperIntro.second .solution02").show(0);
		}

		function second_10(){
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .text3").css("height","53%");
			$board.find(".wrapperIntro.second .img-des13").css("top","-71%");
			$board.find(".wrapperIntro.second .img-des14").css("top","-78%");
			$board.find(".wrapperIntro.second .solution00").hide(0);
			$board.find(".wrapperIntro.second .solution01").hide(0);
			$board.find(".wrapperIntro.second .solution02").hide(0);
			$board.find(".wrapperIntro.second .solution03").show(0);
		}


		function second_11(){

			$board.find(".wrapperIntro.second .solution04").show(0);
		}

		function second_12(){
			$board.find(".wrapperIntro.second .solution05").show(0);
		}

		function second_13(){
			$board.find(".wrapperIntro.second .solution06").show(0);
		}

		function second_14(){
			$board.find(".wrapperIntro.second .solution03").hide(0);
			$board.find(".wrapperIntro.second .solution04").hide(0);
			$board.find(".wrapperIntro.second .solution05").hide(0);
			$board.find(".wrapperIntro.second .solution06").hide(0);
			$board.find(".wrapperIntro.second .solution07").show(0);
		}

		function second_15(){
			$board.find(".wrapperIntro.second .solution08").show(0);
		}

		function second_16(){
			$board.find(".wrapperIntro.second .solution09").show(0);
		}

		function second_17(){

			$board.find(".wrapperIntro.second .solution10").show(0);
		}








		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});


		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				second,
				third,
				fourth,
				fifth,
				sixth,
				seventh,

				second_1,
				second_2,
				second_3,
				second_4,
				second_5,
				second_6,
				second_7,
				second_8,
				second_9,
				second_10,
				second_11,
				second_12,
				second_13,
				second_14,
				second_15,
				second_16,
				second_17
			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

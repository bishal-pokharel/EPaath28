/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var characterDialouges1 = [ {
		diaouges : data.string.p1_3
	},//

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p1_5
	},//

	];

	var characterDialouges3 = [ {
		diaouges : data.string.p1_6
	},//

	];


	var characterDialouges4 = [ {
		diaouges : data.string.p1_7
	},//

	];




	var characterDialouges5 = [ {
		diaouges : data.string.p1_8
	},//

	];

	var characterDialouges6 = [ {
		diaouges : data.string.p1_9
	},//

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p1_10
	},//

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p1_12
	},

	/*var characterDialouges8 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p1_12,data.string.p1_13,"<li>"+data.string.p1_13+"</li>"), data.string.p1_14, "<li>"+data.string.p1_14+"</li>")
	},*/

	];

	//end of 2nd page//


	//3rd page//

	var characterDialouges9 = [ {
		diaouges : data.string.p1_13
	},//

	];

	var characterDialouges10 = [ {
		diaouges : data.string.p1_14
	},//
	];

	var characterDialouges11 = [ {
		diaouges : data.string.p1_15
	},//
	];

	var characterDialouges12 = [ {
		diaouges : data.string.p1_16
	},//
	];

	var characterDialouges13 = [ {
		diaouges : data.string.p1_17
	},//
	];

	var characterDialouges14 = [ {
		diaouges : data.string.p1_34
	},//
	];





	//end of page5//

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=33;
		$nextBtn.show(0);
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,5,15,16,17,18,19,20,21,23,30]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage_Coverpage",
		animate : "true",
		image:true,
		text : [function () {
			var d = ole.textSR(data.lesson.chapter,data.lesson.chapter,"<span class='ind2'>"+data.lesson.chapter+"</span>");

			return d;
		},
		],

	},{
		justClass : "firstPage",
		animate : "true",
		text : [function () {
			var d = ole.textSR(data.string.p1_1,data.string.p1_1,"<span class='ind2'>"+data.string.p1_1+"</span>");

			return d;
		},
		data.string.p1_2,
		function() {
			var d = ole.textSR(data.string.p1_7,"<span class='ind2'>"+data.string.p1_7+"</span>");
			return d;
		},

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			text : [
				function () {
					var d = ole.textSR(data.string.p1_1,data.string.p1_1,"<span class='ind2'>"+data.string.p1_1+"</span>");

					return d;
				},
			    data.string.p1_4
			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/


	{
		justClass : "third",

		text : [
			    data.string.p1_11
			],
	},



	{
		justClass : "fourth",

		text : [
		        function () {
					var d = ole.textSR(data.string.blank,data.string.blank,"<span class='ind2'>"+data.string.p1_17+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p1_16,data.string.p1_16,"<span class='ind3'>"+data.string.p1_16+"</span>");
					return d;
				}
		],

		maths : [{base: data.string.p1_75},
					{base: data.string.p1_76},
					{base: data.string.p1_77},
					{base: data.string.p1_78},
					{base: data.string.p1_79},
					{base: data.string.p1_80},
					{base: data.string.p1_81},
					{base: data.string.p1_82},
					{base: data.string.p1_83},
					],
	},


	{
		justClass : "fifth",

			text : [
			        function () {
						var d = ole.textSR(data.string.p1_15,data.string.p1_15,"<span class='ind2'>"+data.string.p1_15+"</span>");
						return d;
					},
					function () {
						var d = ole.textSR(data.string.p1_16,data.string.p1_16,"<span class='ind3'>"+data.string.p1_16+"</span>");
						return d;
					}
			],

			maths : [{base: data.string.p1_66},
						{base: data.string.p1_67},
						{base: data.string.p1_68},
						{base: data.string.p1_69},
						{base: data.string.p1_70},
						{base: data.string.p1_71},
						{base: data.string.p1_72},
						{base: data.string.p1_73},
						{base: data.string.p1_74},
						],
	},

	{
		justClass : "sixth",

			text : [
			    function () {
					var d = ole.textSR(data.string.p1_61,data.string.p1_61,"<span class='ind2'>"+data.string.p1_61+"</span>");
					return d;
				}

			],

			maths : [{base: data.string.p1_18},
				{base: data.string.p1_19},
				{base: data.string.p1_20},
				{base: data.string.p1_21},
				{base: data.string.p1_22},
				{base: data.string.p1_23},
				{base: data.string.p1_24},
				{base: data.string.p1_25},
				],
	},

	{
		justClass : "seventh",

			text : [
			    function () {
					var d = ole.textSR(data.string.p1_62,data.string.p1_62,"<span class='ind2'>"+data.string.p1_62+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p1_65,data.string.p1_65,"<span class='ind3'>"+data.string.p1_65+"</span>");
					return d;
				}
			],
			maths : [{base: data.string.p1_26},
					{base: data.string.p1_27},
					{base: data.string.p1_28},
					{base: data.string.p1_29},
					{base: data.string.p1_30},
					{base: data.string.p1_31},
					{base: data.string.p1_32},
					{base: data.string.p1_33},
					{base: data.string.p1_34},
					{base: data.string.p1_35},
					{base: data.string.p1_36},
					],
	},


	{
		justClass : "eight",

		text : [
			    data.string.p1_37
			],
	},

	{
		justClass : "ninth",

		text : [
				data.string.p1_1,
				data.string.p1_39,
			],
	},

	{
		justClass : "tenth",

		text : [
				data.string.p1_40,
				data.string.p1_41,
				data.string.p1_42,
				data.string.p1_43,
				data.string.p1_44,
				data.string.p1_45,
				data.string.p1_46,
			],
	},

	{
		justClass : "eleventh",

		text : [
				data.string.p1_47,
				data.string.p1_48,
				data.string.p1_49,
				data.string.p1_50,
			],
	},

	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};
			function first_sec() {

				var source = $("#intro-template").html();
				var template = Handlebars.compile(source);
				var content = dataObj[1];
				var html = template(content);
				$board.html(html);

			};

		/*first call to first*/
		// first();

		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function third () {
			$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
			$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		/* third click*/
		function characterDialouge1(){
			appendDialouge = '<div class="p1_3">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").show(0);
		$(".des1").append(appendDialouge);
		}
		/* end of third click*/


	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}
		/*end of text 0 firstline*/

		/*first click*/
		function beforeClicks_1 () {
			$board.find(".wrapperIntro.second .text1").css("opacity",1);
		}

		function beforeClicks_2 () {
			$board.find(".wrapperIntro.second .imageholder01").show(0);
		}
		/* end of first click*/

		/* second click*/
		function characterDialouge2(){
			appendDialouge = '<div class="p1_5">';
			var dialouges =$.each(characterDialouges2,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des2").append(appendDialouge);
		}
		/* end of second click*/





		/* third click*/
		function characterDialouge3(){
			appendDialouge = '<div class="p1_6">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		}
		/* end of third click*/





		/* fourth click*/


		function beforeClicks_3(){
			$board.find(".wrapperIntro.second .imageholder02").show(0);
		}



		/* end of fourth click*/

		/* fifth click*/


		function characterDialouge5(){
			appendDialouge = '<div class="p1_8">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").show(0);
		$(".des5").append(appendDialouge);
		}



		/* end of fifth click*/

		/* sixth click*/


		function characterDialouge6(){
			appendDialouge = '<div class="p1_9">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
			});
			appendDialouge += '</div>';
			$(".des5").css("display", "none");
			$(".des6").show(0);
			$(".des6").append(appendDialouge);
		}



		/* end of sixth click*/

		/* seventh click*/


		function characterDialouge7(){
			$(".des6").hide(0);
			$(".des5").hide(0);
			appendDialouge = '<div class="p1_10">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").show(0);
		$(".des7").append(appendDialouge);
		}



		/* end of seventh click*/

		/* eight click*/


		function characterDialouge14(){
			$(".des6").show(0);
			$(".des5").show(0);
		}



		/* end of eight click*/

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function thirdPage () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		}

/*----------------------------------------------------------------------------------------------------*/

/*page4*/

		function fourthPage () {
			$nextBtn.hide(0);
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4]

			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.fourth .imageholder03").show(0);
		}


		$board.on('click','.wrapperIntro.fourth .maths',function () {
			var ratioclass = $(this);
			console.log(this);
			var ans = $(this).attr('class');
			var ratioarr = ["maths3","maths6","maths8"];
			var correct = [];
			var correct = false;
			var questionchecked = true; // check if all answered
			$.each(ratioarr, function(i, item ) {
				if(ratioclass.hasClass(item)) {
					correct = true;
				}

			});

			if(correct){
				ratioclass.addClass('rightans');
			}else{
				ratioclass.addClass('wrongans');
			}

			$('.maths').each(function(i, item){
				if(!$(item).hasClass('rightans') && !$(item).hasClass('wrongans')){
					questionchecked = false;
				}
			});
			if(questionchecked){
				$nextBtn.show(0);
			}
		});


/*******page4 end*************************************************************************************/

/*****page5******************************************************************************************/



	function fifthPage () {
		$nextBtn.hide(0);
		var source = $("#intro5-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[5];
		var html = template(content);
		$board.html(html);
		$board.find(".wrapperIntro.fifth .imageholder04").show(0);
	}

	$board.on('click','.wrapperIntro.fifth .maths',function () {
		var ratioclass = $(this);
		var ans = $(this).attr('class');
		var ratioarr = ["maths5","maths0","maths2"];
		var correct = [];
		var correct = false;
		var questionchecked = true; // check if all answered
		$.each(ratioarr, function(i, item ) {
			if(ratioclass.hasClass(item)) {
				correct = true;
			}

		});

		if(correct){
			ratioclass.addClass('rightans');
		}else{
			ratioclass.addClass('wrongans');
		}

		$('.maths').each(function(i, item){
			if(!$(item).hasClass('rightans') && !$(item).hasClass('wrongans')){
				questionchecked = false;
			}
		});
		if(questionchecked){
			$nextBtn.show(0);
		}
	});



/****end of page5***********************************************************************************/

	/*****page6******************************************************************************************/

	function sixthPage () {
		var source = $("#intro6-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[6];
		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}




/****end of page6***********************************************************************************/

/*****page7******************************************************************************************/



	function seventhPage () {
		$nextBtn.hide(0);
		var source = $("#intro7-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[7];
		var html = template(content);
		$board.html(html);
	}




/****end of page7***********************************************************************************/

	/*page8*/

	function eightPage () {
		var source = $("#intro8-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[8]

		var html = template(content);
		$board.html(html);
		$nextBtn.show(0);
	}

/*----------------------------------------------------------------------------------------------------*/

/*page9*/

	function init_ninth () {
		var source = $("#intro-template-title").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p1_1,
		}
		var html = template(content);

		$board.html(html);

		ninthPage();
	}

	function ninthPage() {

		var source = $("#intro9-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[9];
		var html = template(content);
		$board.html(html);

	};


	function ninth_first () {
		$board.find(".wrapperIntro.ninth ,.wrapperIntro.ninth ").show(0);
		$board.find(".wrapperIntro.ninth .text1").css("opacity",1);

	}





/*----------------------------------------------------------------------------------------------------*/
	function tenthPage() {

		var source = $("#intro10-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[10];
		var html = template(content);
		$board.html(html);

	};

	function tenth_first () {
		$board.find(".wrapperIntro.tenth .text1").show(0);

	}

	function tenth_second () {
		$board.find(".wrapperIntro.tenth .text2").show(0);

	}

	function tenth_third () {
		$board.find(".wrapperIntro.tenth .text3").show(0);

	}

	function tenth_fourth () {
		$board.find(".wrapperIntro.tenth .text4").show(0);

	}

	function tenth_fifth () {
		$board.find(".wrapperIntro.tenth .text5").show(0);

	}

	function tenth_sixth () {
		$board.find(".wrapperIntro.tenth .text6").show(0);

	}

	function tenth_seventh () {
		$board.find(".wrapperIntro.tenth .text7").show(0);
	}

/*page11*/

	function init_eleventh () {
		var source = $("#intro-template-title").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p1_47,
		}
		var html = template(content);

		$board.html(html);

		eleventhPage();
	}

	function eleventhPage() {

		var source = $("#intro11-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[11];
		var html = template(content);
		$board.html(html);

	};


	function eleventh_first () {
		$board.find(".wrapperIntro.eleventh .text1").css("opacity",1);

	}

	function eleventh_second () {
		$board.find(".wrapperIntro.eleventh .text2").show(0);

	}

	function eleventh_third () {
		$board.find(".wrapperIntro.eleventh .text3").show(0);

	}



/*----------------------------------------------------------------------------------------------------*/




		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			if(countNext == 9){
				$(".des5").hide(0);
			}else if(countNext == 10){
				$(".des6").hide(0);
			}else if(countNext == 12){
				$(".des5").show(0);
				$(".des6").addClass('desmod6');
				$(".des7").addClass('desmod7');
				$(".des6").show(0);
			}
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		$board.on('click','.wrapperIntro.seventh .maths',function () {
			var ratioclass = $(this);
			var ans = $(this).attr('class');
			var ratioarr = ["maths5","maths6","maths7", "maths2","maths9"];
			var correct = [];
			var correct = false;
			var questionchecked = true; // check if all answered
			$.each(ratioarr, function(i, item ) {
				if(ratioclass.hasClass(item)) {
					correct = true;
				}

			});

			if(correct){
				ratioclass.addClass('rightans');
			}else{
				ratioclass.addClass('wrongans');
			}

			$('.maths').each(function(i, item){
				if(!$(item).hasClass('rightans') && !$(item).hasClass('wrongans')){
					questionchecked = false;
				}
			});
			if(questionchecked){
				$nextBtn.show(0);
			}
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first_sec,
				// second,
				third,
				characterDialouge1,

				beforeClicks, //second one


				beforeClicks_1,
				beforeClicks_2,
				characterDialouge2,
				characterDialouge3,
				beforeClicks_3,
				characterDialouge5,
				characterDialouge6,
				characterDialouge7,
				characterDialouge14,

				thirdPage,//third one

				fourthPage,//fourthPage
				/*characterDialouge8,
				characterDialouge9,
				characterDialouge10,*/

				fifthPage,//fifthPage

				sixthPage,//sixthPage

				seventhPage,//seventhPage

				eightPage,//eightPage

				ninthPage,//ninthPage
				ninth_first,

				tenthPage,//tenthPage
				tenth_first,
				tenth_second,
				tenth_third,
				tenth_fourth,
				tenth_fifth,
				tenth_sixth,
				tenth_seventh,

				init_eleventh,//eleventhPage
				eleventh_first,
				eleventh_second,
				eleventh_third

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var characterDialouges1 = [ {
		diaouges : ole.textSR(data.string.p3_4,data.string.p3_6,"<span class='ind3'>"+data.string.p3_6+"</span>")
	},//

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p3_8
	},//

	];

	var characterDialouges3 = [ {
		diaouges : ole.textSR(data.string.p3_9,data.string.p3_9,"<span class='ind2'>"+data.string.p3_9+"</span>")
	},//

	];


	var characterDialouges4 = [ {
		diaouges : ole.textSR(data.string.p3_10,data.string.p3_10,"<span class='ind2'>"+data.string.p3_10+"</span>")
	},//

	];




	var characterDialouges5 = [ {
		diaouges : ole.textSR(data.string.p3_11,data.string.p3_18,"<span class='ind2'>"+data.string.p3_18+"</span>")
	},//

	];

	var characterDialouges6 = [ {
		diaouges : ole.textSR(data.string.p3_12,data.string.p3_12,"<span class='ind2'>"+data.string.p3_12+"</span>")
	},//

	];

	var characterDialouges7 = [ {
		diaouges : ole.textSR(data.string.p3_13,data.string.p3_13,"<span class='ind2'>"+data.string.p3_13+"</span>")
	},//

	];

	var characterDialouges8 = [ {
		diaouges : ole.textSR(data.string.p3_14,data.string.p3_19,"<span class='ind2'>"+data.string.p3_19+"</span>")
	},
	];

	//end of 2nd page//


	//3rd page//

	var characterDialouges9 = [ {
		diaouges : ole.textSR(data.string.p3_15,data.string.p3_15,"<span class='ind2'>"+data.string.p3_15+"</span>")
	},//

	];

	var characterDialouges10 = [ {
		diaouges : ole.textSR(data.string.p3_16,data.string.p3_16,"<span class='ind2'>"+data.string.p3_16+"</span>")
	},//
	];

	var characterDialouges11 = [ {
		diaouges : ole.textSR(ole.textSR(data.string.p3_17,data.string.p3_20,"<span class='ind3'>"+data.string.p3_20+"</span>"), data.string.p3_21, "<span class='ind3'>"+data.string.p3_21+"</span>")
	},//
	];


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=13;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0, 4, 5]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
	{
		justClass : "firstPage",

		text : [
				function () {
					var d = ole.textSR(data.string.p3_1,data.string.p3_1,"<span class='ind2'>"+data.string.p3_1+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p3_2,data.string.p3_2,"<span class='ind2'>"+data.string.p3_2+"</span>");
					d = ole.textSR(d,data.string.p3_3,"<span class='ind3'>"+data.string.p3_3+"</span>")
					return d;
				},

			],
	},

	{
		justClass : "second",

			text : [
			        data.string.p3_9,
			],
	},

	{
		justClass : "third",

			text : [
			        data.string.p3_10,
			        data.string.p3_22,
			        data.string.p3_11,
			        data.string.p3_12,
			        data.string.p3_13,
			        data.string.p3_14,
					data.string.p3_17,

			],
	},

	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		/*first call to first*/
		// first();

		function first01 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
			$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
		}

		/* third click*/
		function characterDialouge1(){
			appendDialouge = '<div class="p3_4">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);
		}
		/* end of third click*/

		/* fourth click*/
		/*function first13 () {
			$("#activity-page-next-btn-enabled").hide(0).delay(3000).show(0);
			$board.find(".wrapperIntro.firstPage .f3_2").show(0);
			$board.find(".wrapperIntro.firstPage .f3_2").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .f3_2").css('display', 'inline-flex');
		}
		 end of fourth click

		 fifth click
		function characterDialouge3(){
			appendDialouge = '<div class="p3_6">';
			var dialouges =$.each(characterDialouges3,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des3").append(appendDialouge);
		}
		 end of fifth click

		 sixth click
		function characterDialouge4(){
			appendDialouge = '<div class="p3_7">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		}
		 end of sixth click

		 seventh click
		function characterDialouge5(){
			appendDialouge = '<div class="p3_8">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").append(appendDialouge);
		}
		 end of seventh click

		 eigth click
		function characterDialouge6(){
			appendDialouge = '<div class="p3_9">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des6").append(appendDialouge);
		}
		 end of eigth click

		 ninth click
		function characterDialouge7(){
			appendDialouge = '<div class="p3_10">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").append(appendDialouge);
		}
		 end of ninth click

		 tenth click
		function characterDialouge8(){
			appendDialouge = '<div class="p3_11">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);
		}
		 end of tenth click

		 eleventh click
		function characterDialouge9(){
			appendDialouge = '<div class="p3_12">';
			var dialouges =$.each(characterDialouges9,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des9").append(appendDialouge);
		}
		 end of eleventh click

		 twelth click
		function characterDialouge10(){
			appendDialouge = '<div class="p3_13">';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des10").append(appendDialouge);
		}
		 end of twelth click

		 thirteen click
		function characterDialouge11(){
			appendDialouge = '<div class="p3_14">';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des11").append(appendDialouge);
		}*/
		/* end of thirteen click*/



	/*-------------------------------------------------------------------------------------------*/
		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .char-b").hide(0);
			$board.find(".wrapperIntro.second .char-a").addClass("slideRight");
			second01();
		};

		function second01 () {
			$board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);

			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .bubble1").show(0);
			$board.find(".wrapperIntro.second .text0").addClass("slideRight");
			$board.find(".wrapperIntro.second .bubble1").addClass("slideRight");
			$board.find(".wrapperIntro.second .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");
		}


		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2]
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.third .text0").show(0);



		}


		function third01() {
			$board.find(".wrapperIntro.third .solution00").show(0);
			$board.find(".wrapperIntro.third .text2").css("background","#efefef");


		}


		function third02(){
			$board.find(".wrapperIntro.third .solution01").show(0);
		}

		function third03(){
			$board.find(".wrapperIntro.third .solution02").show(0);
		}

		function third04(){
			$board.find(".wrapperIntro.third .text4").show(0);
		}

		function third05(){
			$board.find(".wrapperIntro.third .solution03").show(0);
			$board.find(".wrapperIntro.third .text5").css("background","#efefef");
		}

		function third06(){
			$board.find(".wrapperIntro.third .solution04").show(0);
		}

		function third07(){
			$board.find(".wrapperIntro.third .solution05").show(0);
		}

		function third08(){
			$board.find(".wrapperIntro.third .text6").show(0);
			$board.find(".wrapperIntro.fourth .continue").show(0);
		}

		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});


		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				characterDialouge1,

				second,

				third,
				third01,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07,
				third08

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=15;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
	           	{
	           		justClass : "firstPage",
	           		animate : "true",
	           		text : [
	           				data.string.p5_1,
							data.string.p5_2,
							data.string.p5_3,
				],

		},


				{
		            justClass : "second",
		            text : [

	           				data.string.p5_4,
	           				data.string.p5_24,
	           				data.string.p5_5,
	           				data.string.p5_25,
	           				data.string.p5_8,
	           				data.string.p5_13,
	           				data.string.p5_17,
	           				data.string.p5_18,
	           				data.string.p5_19,
	           				data.string.p5_26,
	           				data.string.p5_20,
	           				data.string.p5_21,
	           				data.string.p5_22

	           			],
	           	},

	           	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
		};

		/*first call to first*/
		// first();

		function first01 () {

			$board.find(".wrapperIntro.firstPage .text1").show(0);

		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text2").show(0);

		}







		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second .text0").show(0);
		}


		function second01 () {
			$board.find(".wrapperIntro.second .text1").show(0);


		}

		function second02 () {
			$board.find(".wrapperIntro.second .text2").show(0);


		}


		function second03 () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");


		}

		function second04 () {
			$board.find(".wrapperIntro.second .solution01").show(0);


		}


		function second05 () {
			$board.find(".wrapperIntro.second .solution02").show(0);


		}


		function second06 () {
			$board.find(".wrapperIntro.second .solution03").show(0);


		}


		function second07 () {
			$board.find(".wrapperIntro.second .solution04").show(0);


		}


		function second08 () {
			$board.find(".wrapperIntro.second .text8").show(0);


		}


		function second09 () {
			$board.find(".wrapperIntro.second .solution05").show(0);
			$board.find(".wrapperIntro.second .text9").css("background","#efefef");


		}


		function second10 () {
			$board.find(".wrapperIntro.second .solution06").show(0);


		}


		function second11 () {
			$board.find(".wrapperIntro.second .solution07").show(0);


		}

		function second12 () {
			$board.find(".wrapperIntro.second .solution08").show(0);


		}
		/*
		function second13 () {
			$board.find(".wrapperIntro.second .text13").show(0);


		}

		function second14 () {
			$board.find(".wrapperIntro.second .text14").show(0);


		}

		function second15 () {
			$board.find(".wrapperIntro.second .text15").show(0);


		}

		function second16 () {
			$board.find(".wrapperIntro.second .text16").show(0);


		}

		function second17 () {
			$board.find(".wrapperIntro.second .text17").show(0);


		}

		function second18 () {
			$board.find(".wrapperIntro.second .text18").show(0);


		}

		function second19 () {
			$board.find(".wrapperIntro.second .text19").show(0);


		}

		function second20 () {
			$board.find(".wrapperIntro.second .text20").show(0);


		}



	/*-------------------------------------------------------------------------------------------*/









		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});


		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first01,
				first02,


				second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				/*second13,
				second14,
				second15,
				second16,
				second17,
				second18,
				second19,
				second20*/


			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

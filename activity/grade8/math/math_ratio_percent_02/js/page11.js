/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function() {
  //------------------------------------------------------------------------------------------------//
  var $board = $(".board"),
    $nextBtn = $("#activity-page-next-btn-enabled"),
    $prevBtn = $("#activity-page-prev-btn-enabled"),
    $refreshBtn = $("#activity-page-refresh-btn");
  (countNext = 0), (increment = 0), (all_page = 67);
  $nextBtn.show(0);

  var baseAndIndicesClick = [null, null]; //used by base and index

  var clickArray = [0, 15, 19, 32, 34, 48, 50, 65]; //manipulate the clicks like back and from top
  $total_page = clickArray.length;
  loadTimelineProgress($total_page, 1);

  var qDatas = [
    { first: "2", second: "5", third: "8", fourth: "10", ans: "2" },
    { first: "2", second: "8", third: "5", fourth: "50", ans: "8" },
    { first: "2", second: "8", third: "18", fourth: "12", ans: "18" },
    { first: "6", second: "9", third: "2", fourth: "18", ans: "18" }
  ];
  var qDatasFirst = [
    { base: "30", ansque: data.string.p11_23 },
    { base: "30", ansque: data.string.p11_24 }
  ];

  var qDatasSecond = [
    {
      base: "25000",
      power: "10000",
      firstlabel: data.string.p11_33,
      secondlabel: data.string.p11_34,
      ansque: data.string.p11_25
    },
    {
      base: "40",
      power: "60",
      third: "80",
      firstlabel: data.string.p11_35,
      secondlabel: data.string.p11_36,
      thirdlabel: data.string.p11_37,
      ansque: data.string.p11_26
    }
  ];

  var qDatasThird = [
    {
      base: "44",
      power: "12",
      firstlabel: data.string.p11_88,
      secondlabel: data.string.p11_89,
      ansque: data.string.p11_87
    },
    {
      base: "20",
      power: "45",
      firstlabel: data.string.p11_91,
      secondlabel: data.string.p11_92,
      ansque: data.string.p11_90
    }
  ];

  var dataObj = [
    {
      justClass: "firstPage",
      animate: "true",
      text: [
        data.string.p11_1,
        data.string.p11_2,
        data.string.p11_3,
        data.string.p11_4,
        data.string.p11_5
      ]
    },

    /*------------------------------------------------------------------------------------------------*/
    /*start of second page*/
    {
      justClass: "second",

      text: [
        function() {
          var d = ole.textSR(
            data.string.p7_12,
            data.string.p7_12,
            "<span class='ind6'>" + data.string.p7_12 + "</span>"
          );
          return d;
        },
        function() {
          var d = ole.textSR(
            data.string.p7_13,
            data.string.p7_14,
            "<span class='ind6'>" + data.string.p7_14 + "</span>"
          );
          return d;
        },
        function() {
          var d = ole.textSR(
            data.string.p7_15,
            data.string.p7_15,
            "<span class='ind6'>" + data.string.p7_15 + "</span>"
          );
          return d;
        }
      ]
    },

    /*end of second page */

    /*------------------------------------------------------------------------------------------------*/

    {
      justClass: "third",

      text: [
        data.string.p11_42,
        data.string.p11_43,
        data.string.p11_44,
        data.string.p11_45,
        data.string.p11_46
      ]
    },

    {
      justClass: "fourth",

      text: []
    },

    {
      justClass: "fifth",

      text: [
        data.string.p11_55,
        data.string.p11_56,
        data.string.p11_57,
        data.string.p11_58,
        data.string.p11_59
      ]
    },

    {
      justClass: "sixth",

      text: []
    },

    {
      justClass: "seventh",

      text: [
        data.string.p11_69,
        data.string.p11_70,
        data.string.p11_71,
        data.string.p11_72,
        data.string.p11_73
      ]
    },

    {
      justClass: "eight",

      text: []
    }
  ];

  /******************************************************************************************************/
  /*
   * first
   */
  function firstPage() {
    var source = $("#intro-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[0];

    var html = template(content);
    $board.html(html);
    $nextBtn.show(0);
  }
  /*first call to first*/
  // first();

  function firstPage_first() {
    $board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
    $board.find(".wrapperIntro.firstPage .text1").show(0);
  }

  function firstPage_second() {
    $board.find(".wrapperIntro.firstPage .text2").show(0);
  }

  function firstPage_third() {
    $board.find(".wrapperIntro.firstPage .text3").show(0);
  }

  function firstPage_fourth() {
    $board.find(".wrapperIntro.firstPage .solution00").show(0);
    $board.find(".wrapperIntro.firstPage .text4").css("background", "#efefef");
  }

  function firstPage_fifth() {
    $board.find(".wrapperIntro.firstPage .solution01").show(0);
  }

  function firstPage_sixth() {
    $board.find(".wrapperIntro.firstPage .solution02").show(0);
  }

  function firstPage_seventh() {
    $board
      .find(".wrapperIntro.firstPage .solution03")
      .show(0)
      .css("left", "11%");
  }

  function firstPage_eight() {
    $board.find(".wrapperIntro.firstPage .solution04").show(0);
  }

  function firstPage_ninth() {
    $board.find(".wrapperIntro.firstPage .solution05").show(0);
  }
  function firstPage_tenth() {
    $board.find(".wrapperIntro.firstPage .solution00").hide(0);
    $board.find(".wrapperIntro.firstPage .solution01").hide(0);
    $board.find(".wrapperIntro.firstPage .solution02").hide(0);
    $board.find(".wrapperIntro.firstPage .solution03").hide(0);
    $board.find(".wrapperIntro.firstPage .solution04").hide(0);
    $board.find(".wrapperIntro.firstPage .solution05").hide(0);
    $board.find(".wrapperIntro.firstPage .solution06").show(0);
  }
  function firstPage_eleventh() {
    $board.find(".wrapperIntro.firstPage .solution07").show(0);
  }
  function firstPage_tweleveth() {
    $board.find(".wrapperIntro.firstPage .solution08").show(0);
  }
  function firstPage_thirteenth() {
    $board.find(".wrapperIntro.firstPage .solution09").show(0);
  }
  function firstPage_fourteenth() {
    $board.find(".wrapperIntro.firstPage .solution10").show(0);
  }

  /*first call to first*/
  // first();

  /*-------------------------------------------------------------------------------------------*/

  /*second page*/
  function second() {
    var source = $("#intro-template-title").html();
    var template = Handlebars.compile(source);
    var content = {
      qTitle: data.string.p11_84
    };
    var html = template(content);
    $board.html(html);

		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
    second_first();
  }

  function second_first() {
    sub = qDatas[increment];
    increment++;
    $nextBtn.hide(0);
    $prevBtn.hide(0);
    var source = $("#qaArea-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      answer: data.string.p11_86,
      first: sub.first,
      second: sub.second,
      third: sub.third,
      fourth: sub.fourth,
      ans: sub.ans,
      text: data.string.p11_19,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function second_second() {
    sub = qDatas[increment];
    increment++;
    $nextBtn.hide(0);
    $prevBtn.hide(0);
    var source = $("#qaArea-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      answer: data.string.p11_86,
      first: sub.first,
      second: sub.second,
      third: sub.third,
      fourth: sub.fourth,
      ans: sub.ans,
      text: data.string.p11_20,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function second_third() {
    sub = qDatas[increment];
    increment++;
    $nextBtn.hide(0);
    $(".q1").hide(0);
    $(".q2").hide(0);
    $prevBtn.hide(0);
    var source = $("#qaArea-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      answer: data.string.p11_86,
      first: sub.first,
      second: sub.second,
      third: sub.third,
      fourth: sub.fourth,
      ans: sub.ans,
      text: data.string.p11_21,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function second_fourth() {
    sub = qDatas[increment];
    increment++;
    $nextBtn.hide(0);
    $prevBtn.hide(0);
    var source = $("#qaArea-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      answer: data.string.p11_86,
      first: sub.first,
      second: sub.second,
      third: sub.third,
      fourth: sub.fourth,
      ans: sub.ans,
      text: data.string.p11_22,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }
  var countHowmany = 0;
  $board.on("click", ".clicks .click0", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    $board.find(cls + " .showAns").show(0);
    $nextBtn.show(0);
  });
  $board.on("click", ".base", function() {
    var $this = $(this);
    var base = $(this).val();
    var cls = ".q" + num;
    var num = parseInt($this.data("cls"));
    var upDatas = qDatas[num - 1];
    var check = [null, null];
    var wrong = "<img src='images/wrong.png'>";
    var right = "<img src='images/correct.png'>";

    if (base === upDatas.ans) {
      check[0] = 1;
      //$board.find(cls+" .check1").html(right);
      $this
        .parent()
        .find(".check4")
        .html(right);
      $this.css("border-color", "#3C9300");
      $this.css("border-width", "5px");
      $this
        .parent()
        .find(".first")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".second")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".third")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".fourth")
        .attr("disabled", "disabled");
    } else {
      $this
        .parent()
        .find(".check4")
        .html(wrong);
      $this.css("border-color", "#D3131B");
      $this.css("border-width", "5px");
      $this
        .parent()
        .find(".first")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".second")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".third")
        .attr("disabled", "disabled");
      $this
        .parent()
        .find(".fourth")
        .attr("disabled", "disabled");
    }

    if (check[0] === 1) {
      if (countNext >= all_page) {
        ole.footerNotificationHandler.pageEndSetNotification();
      } else {
        $nextBtn.show(0);
      }
    } else {
      $this
        .parent()
        .next(".clicks")
        .find(".click0")
        .show(0);
    }
  });
  /*end of page 2*/
  /*-------------------------------------------------------------------------------------------------*/

  /*page3*/

  function thirdPage() {
    var source = $("#intro3-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[2];

    var html = template(content);
    $board.html(html);
    $board.find(".wrapperIntro.third .imageholder05").show(0);
    $nextBtn.show(0);
  }

  function thirdPage_first() {
    $board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);
    $board.find(".wrapperIntro.third .text1").show(0);
  }

  function thirdPage_second() {
    $board.find(".wrapperIntro.third .text2").show(0);
  }
  function thirdPage_third() {
    $board.find(".wrapperIntro.third .text3").show(0);
  }
  function thirdPage_fourth() {
    $board.find(".wrapperIntro.third .solution00").show(0);
    $board.find(".wrapperIntro.third .text4").css("background", "#efefef");
  }
  function thirdPage_fifth() {
    $board.find(".wrapperIntro.third .solution01").show(0);
  }
  function thirdPage_sixth() {
    $board.find(".wrapperIntro.third .solution02").show(0);
  }
  function thirdPage_seventh() {
    $board.find(".wrapperIntro.third .solution03").show(0);
  }
  function thirdPage_eight() {
    $board.find(".wrapperIntro.third .solution04").show(0);
  }
  function thirdPage_ninth() {
    $board.find(".wrapperIntro.third .solution05").show(0);
  }
  function thirdPage_tenth() {
    $board.find(".wrapperIntro.third .solution06").show(0);
  }
  function thirdPage_eleventh() {
    $board.find(".wrapperIntro.third .solution07").show(0);
  }
  function thirdPage_tweleveth() {
    $board.find(".wrapperIntro.third .solution08").show(0);
  }

  /*----------------------------------------------------------------------------------------------------*/

  /*page4*/

  function fourth() {
    var source = $("#intro-template-title").html();
    var template = Handlebars.compile(source);
    var content = {
      qTitle: data.string.p11_84
    };
    var html = template(content);
    $board.html(html);
    increment = 0;
    fourth_first();
  }

  function fourth_first() {
    $nextBtn.hide(0);
    sub = qDatasFirst[increment];
    increment++;
    var source = $("#qaArea1-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      answer: data.string.p11_86,
      text: sub.ansque,
      firstlabel: sub.firstlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function fourth_second() {
    $nextBtn.hide(0);
    sub = qDatasFirst[increment];
    increment++;
    var source = $("#qaArea1-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      answer: data.string.p11_86,
      text: sub.ansque,
      firstlabel: sub.firstlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  $board.on("click", ".clicks .click1", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    $board.find(cls + " .showAns").show(0);
    $nextBtn.show(0);
  });

  $board.on("click", ".clicks .check1", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    var base = $board
      .find(cls + " input.base1")
      .val()
      .toLowerCase();
    // console.log(base+"  "+power);
    var upDatas = qDatasFirst[num - 1];
    var check = [null, null];
    var wrong = "<img src='images/wrong.png'>";
    // wrong.src = "images/wrong.png";
    var right = "<img src='images/correct.png'>";
    // right.src = "images/correct.png";
    if (base === "") {
      swal("Please fill all the empty fields!");
    } else {
      if (base === upDatas.base) {
        check[0] = 1;
        $board.find(cls + " .check01").html(right);
      } else {
        $board.find(cls + " .check01").html(wrong);
      }

      if (check[0] === 1) {
        if (countNext >= all_page) {
          ole.footerNotificationHandler.pageEndSetNotification();
        } else {
          $nextBtn.show(0);
        }
      } else {
        $board.find(cls + " .clicks .click1").show(0);
      }
    }
  });
  /*******page4 end*************************************************************************************/

  function fifthPage() {
    var source = $("#intro5-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[4];

    var html = template(content);
    $board.html(html);
    $nextBtn.show(0);
  }

  function fifthPage_first() {
    $board.find(".wrapperIntro.fifth ,.wrapperIntro.fifth ").show(0);
    $board.find(".wrapperIntro.fifth .text1").show(0);
  }

  function fifthPage_second() {
    $board.find(".wrapperIntro.fifth .text2").show(0);
  }
  function fifthPage_third() {
    $board.find(".wrapperIntro.fifth .text3").show(0);
  }
  function fifthPage_fourth() {
    $board.find(".wrapperIntro.fifth .solution00").show(0);
    $board.find(".wrapperIntro.fifth .text4").css("background", "#efefef");
  }
  function fifthPage_fifth() {
    $board.find(".wrapperIntro.fifth .solution01").show(0);
  }
  function fifthPage_sixth() {
    $board.find(".wrapperIntro.fifth .solution02").show(0);
  }
  function fifthPage_seventh() {
    $board.find(".wrapperIntro.fifth .solution03").show(0);
  }
  function fifthPage_eighth() {
    $board.find(".wrapperIntro.fifth .solution04").show(0);
  }
  function fifthPage_ninth() {
    $board.find(".wrapperIntro.fifth .solution05").show(0);
  }
  function fifthPage_tenth() {
    $board.find(".wrapperIntro.fifth .solution00").hide(0);
    $board.find(".wrapperIntro.fifth .solution01").hide(0);
    $board.find(".wrapperIntro.fifth .solution02").hide(0);
    $board.find(".wrapperIntro.fifth .solution03").hide(0);
    $board.find(".wrapperIntro.fifth .solution04").hide(0);
    $board.find(".wrapperIntro.fifth .solution05").hide(0);
    $board.find(".wrapperIntro.fifth .solution06").show(0);
  }
  function fifthPage_eleventh() {
    $board.find(".wrapperIntro.fifth .solution07").show(0);
  }
  function fifthPage_twelveth() {
    $board.find(".wrapperIntro.fifth .solution08").show(0);
  }
  function fifthPage_thirteenth() {
    $board.find(".wrapperIntro.fifth .solution09").show(0);
  }

  function sixth() {
    var source = $("#intro-template-title").html();
    var template = Handlebars.compile(source);
    var content = {
      qTitle: data.string.p11_84
    };
    increment = 0;
    var html = template(content);
    $board.html(html);
    sixth_first();
  }

  function sixth_first(sub) {
    $nextBtn.hide(0);
    sub = qDatasSecond[increment];
    increment++;
    var source = $("#qaArea2-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      secondans: sub.power,
      answer: data.string.p11_86,
      text: sub.ansque,
      prefix: sub.prefix,
      suffix: sub.suffix,
      firstlabel: sub.firstlabel,
      secondlabel: sub.secondlabel,
      thirdlabel: sub.thirdlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function sixth_second() {
    $nextBtn.hide(0);
    $(".q1").hide(0);
    sub = qDatasSecond[increment];
    increment++;
    var source = $("#qaArea21-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      secondans: sub.power,
      thirdans: sub.third,
      answer: data.string.p11_86,
      text: sub.ansque,
      prefix: sub.prefix,
      suffix: sub.suffix,
      firstlabel: sub.firstlabel,
      secondlabel: sub.secondlabel,
      thirdlabel: sub.thirdlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  $board.on("click", ".clicks .click2", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    $board.find(cls + " .showAns").show(0);
    $nextBtn.show(0);
  });

  $board.on("click", ".clicks .check2", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    var base = $board
      .find(cls + " input.base")
      .val()
      .toLowerCase();
    var power = $board
      .find(cls + " input.power")
      .val()
      .toLowerCase();
    if (num == 2) {
      var third = $board
        .find(cls + " input.thirdinput")
        .val()
        .toLowerCase();
    }
    // console.log(base+"  "+power);
    var upDatas = qDatasSecond[num - 1];
    var check = [null, null];
    var check_empty = [null, null];
    var wrong = "<img src='images/wrong.png'>";
    // wrong.src = "images/wrong.png";
    var right = "<img src='images/correct.png'>";
    // right.src = "images/correct.png";
    if (base === "") {
      check_empty[0] = 1;
    } else {
      if (base === upDatas.base) {
        check[0] = 1;
        $board.find(cls + " .check01").html(right);
      } else {
        $board.find(cls + " .check01").html(wrong);
      }
    }

    if (power === "") {
      check_empty[1] = 1;
    } else {
      if (power === upDatas.power) {
        check[1] = 1;
        $board.find(cls + " .check02").html(right);
      } else {
        $board.find(cls + " .check02").html(wrong);
      }
    }

    if (num == 2) {
      if (third === "") {
        check_empty[2] = 1;
      } else {
        if (third === upDatas.third) {
          check[2] = 1;
          $board.find(cls + " .check3").html(right);
        } else {
          $board.find(cls + " .check3").html(wrong);
        }
      }

      if (
        check_empty[0] === 1 ||
        check_empty[1] === 1 ||
        check_empty[2] === 1
      ) {
        swal("Please fill all the empty fields!");
      } else {
        if (check[0] === 1 && check[1] === 1 && check[2] === 1) {
          if (countNext >= all_page) {
            ole.footerNotificationHandler.pageEndSetNotification();
          } else {
            $nextBtn.show(0);
          }
        } else {
          $board.find(cls + " .clicks .click2").show(0);
        }
      }
    } else {
      if (check_empty[0] === 1 || check_empty[1] === 1) {
        swal("Please fill all the empty fields!");
      } else {
        if (check[0] === 1 && check[1] === 1) {
          if (countNext >= all_page) {
            ole.footerNotificationHandler.pageEndSetNotification();
          } else {
            $nextBtn.show(0);
          }
        } else {
          $board.find(cls + " .clicks .click2").show(0);
        }
      }
    }
  });

  function seventhPage() {
    var source = $("#intro7-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[6];

    var html = template(content);
    $board.html(html);
    $nextBtn.show(0);
  }

  function seventhPage_first() {
    $board.find(".wrapperIntro.seventh ,.wrapperIntro.seventh ").show(0);
    $board.find(".wrapperIntro.seventh .text1").show(0);
  }

  function seventhPage_second() {
    $board.find(".wrapperIntro.seventh .text2").show(0);
  }
  function seventhPage_third() {
    $board.find(".wrapperIntro.seventh .text3").show(0);
  }
  function seventhPage_fourth() {
    $board.find(".wrapperIntro.seventh .solution00").show(0);
    $board.find(".wrapperIntro.seventh .text4").css("background", "#efefef");
  }
  function seventhPage_fifth() {
    $board.find(".wrapperIntro.seventh .solution01").show(0);
  }
  function seventhPage_sixth() {
    $board.find(".wrapperIntro.seventh .solution02").show(0);
  }
  function seventhPage_seventh() {
    $board.find(".wrapperIntro.seventh .solution03").show(0);
  }
  function seventhPage_eighth() {
    $board.find(".wrapperIntro.seventh .solution04").show(0);
  }
  function seventhPage_ninth() {
    $board.find(".wrapperIntro.seventh .solution05").show(0);
  }
  function seventhPage_tenth() {
    $board.find(".wrapperIntro.seventh .solution00").hide(0);
    $board.find(".wrapperIntro.seventh .solution01").hide(0);
    $board.find(".wrapperIntro.seventh .solution02").hide(0);
    $board.find(".wrapperIntro.seventh .solution03").hide(0);
    $board.find(".wrapperIntro.seventh .solution04").hide(0);
    $board.find(".wrapperIntro.seventh .solution05").hide(0);
    $board.find(".wrapperIntro.seventh .solution06").show(0);
  }
  function seventhPage_eleventh() {
    $board.find(".wrapperIntro.seventh .solution07").show(0);
  }
  function seventhPage_twelveth() {
    $board.find(".wrapperIntro.seventh .solution08").show(0);
  }
  function seventhPage_thirteenth() {
    $board.find(".wrapperIntro.seventh .solution09").show(0);
  }
  function seventhPage_fourteenth() {
    $board.find(".wrapperIntro.seventh .solution10").show(0);
  }

  function eight() {
    var source = $("#intro-template-title").html();
    var template = Handlebars.compile(source);
    var content = {
      qTitle: data.string.p11_84
    };
    increment = 0;
    var html = template(content);
    $board.html(html);

    eight_first();
  }

  function eight_first() {
    $nextBtn.hide(0);
    sub = qDatasThird[increment];
    increment++;
    var source = $("#qaArea3-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      secondans: sub.power,
      answer: data.string.p11_86,
      text: sub.ansque,
      prefix: sub.prefix,
      suffix: sub.suffix,
      firstlabel: sub.firstlabel,
      secondlabel: sub.secondlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function eight_second() {
    $nextBtn.hide(0);
    $(".q1").hide(0);
    sub = qDatasThird[increment];
    increment++;
    var source = $("#qaArea3-template").html();
    var template = Handlebars.compile(source);
    var content = {
      check: data.string.check,
      clickToSee: data.string.p11_85,
      firstans: sub.base,
      secondans: sub.power,
      answer: data.string.p11_86,
      text: sub.ansque,
      prefix: sub.prefix,
      suffix: sub.suffix,
      firstlabel: sub.firstlabel,
      secondlabel: sub.secondlabel,
      couter: increment
    };
    var html = template(content);
    $board.find(".qaArea").append(html);
    // $board.find(".qaArea input.base").focus();
  }

  function callNextChecker() {
    var nextChecker = true;
    for (var i = 0; i < baseAndIndicesClick.length; i++) {
      if (baseAndIndicesClick[i] === null) {
        nextChecker = false;
        break;
      }
    }
    if (nextChecker) {
      $nextBtn.show(0);
    }
  }
  /*end of base and indecis*/

  /*********/

  $board.on("click", ".clicks .click", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    $board.find(cls + " .showAns").show(0);
    $nextBtn.show(0);
  });

  $board.on("click", ".clicks .check", function() {
    var $this = $(this);
    var num = parseInt($this.data("cls"));
    var cls = ".q" + num;
    var base = $board
      .find(cls + " input.base")
      .val()
      .toLowerCase();
    var power = $board
      .find(cls + " input.power")
      .val()
      .toLowerCase();
    // console.log(base+"  "+power);
    var upDatas = qDatasThird[num - 1];
    var check = [null, null];
    var check_empty = [null, null];
    var wrong = "<img src='images/wrong.png'>";
    // wrong.src = "images/wrong.png";
    var right = "<img src='images/correct.png'>";
    // right.src = "images/correct.png";
    if (base === "") {
      check_empty[0] = 1;
    } else {
      if (base === upDatas.base) {
        check[0] = 1;
        $board.find(cls + " .check1").html(right);
      } else {
        $board.find(cls + " .check1").html(wrong);
      }
    }

    if (power === "") {
      check_empty[1] = 1;
    } else {
      if (power === upDatas.power) {
        check[1] = 1;
        $board.find(cls + " .check2").html(right);
      } else {
        $board.find(cls + " .check2").html(wrong);
      }
    }
    if (check_empty[0] === 1 || check_empty[1] === 1) {
      swal("Please fill all the empty fields!");
    } else {
      if (check[0] === 1 && check[1] === 1) {
        if (countNext >= all_page) {
          ole.footerNotificationHandler.pageEndSetNotification();
        } else {
          $nextBtn.show(0);
        }
      } else {
        $board.find(cls + " .clicks .click").show(0);
      }
    }
  });

  // first func call
  firstPage();
  // lastBaseIndices ();
  // countNext = 12;

  /*click functions*/
  $nextBtn.on("click", function() {
    // $(this).css("display","none");
    $prevBtn.show(0);
    countNext++;
    fnSwitcher();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] < countNext) {
        countNext = clickArray[i];
        // console.log(countNext+" = "+clickArray[i]);
        break;
      }
    }
    if (countNext === 0) {
      $prevBtn.hide(0);
    }
    fnSwitcher();
  });

  function clickSequence(index) {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] === index) {
        countNext = clickArray[i];
        break;
      }
    }
    fnSwitcher();
  }

  function fnSwitcher() {
    fnArray = [
      firstPage, //first one
      firstPage_first,
      firstPage_second,
      firstPage_third,
      firstPage_fourth,
      firstPage_fifth,
      firstPage_sixth,
      firstPage_seventh,
      firstPage_eight,
      firstPage_ninth,
      firstPage_tenth,
      firstPage_eleventh,
      firstPage_tweleveth,
      firstPage_thirteenth,
      firstPage_fourteenth,

      second,
      //second_first,
      second_second,
      second_third,
      second_fourth,

      thirdPage, //third one
      thirdPage_first,
      thirdPage_second,
      thirdPage_third,
      thirdPage_fourth,
      thirdPage_fifth,
      thirdPage_sixth,
      thirdPage_seventh,
      thirdPage_eight,
      thirdPage_ninth,
      thirdPage_tenth,
      thirdPage_eleventh,
      thirdPage_tweleveth,

      fourth,
      fourth_second,

      fifthPage, //fifth one
      fifthPage_first,
      fifthPage_second,
      fifthPage_third,
      fifthPage_fourth,
      fifthPage_fifth,
      fifthPage_sixth,
      fifthPage_seventh,
      fifthPage_eighth,
      fifthPage_ninth,
      fifthPage_tenth,
      fifthPage_eleventh,
      fifthPage_twelveth,
      fifthPage_thirteenth,

      sixth,
      sixth_second,

      seventhPage, //seventh one
      seventhPage_first,
      seventhPage_second,
      seventhPage_third,
      seventhPage_fourth,
      seventhPage_fifth,
      seventhPage_sixth,
      seventhPage_seventh,
      seventhPage_eighth,
      seventhPage_ninth,
      seventhPage_tenth,
      seventhPage_eleventh,
      seventhPage_twelveth,
      seventhPage_thirteenth,
      seventhPage_fourteenth,

      eight, //eight one
      eight_second
    ];

    fnArray[countNext]();

    for (var i = 0; i < clickArray.length; i++) {
      if (clickArray[i] === countNext) {
        loadTimelineProgress($total_page, i + 1);
      }
    }

    if (countNext == 66) {
      $nextBtn.hide(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else {
      // $(this).show(0);
      // $nextBtn.show(0);
    }
  }
  /****************/
});

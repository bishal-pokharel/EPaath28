/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {



//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=23;
		$nextBtn.show(0);

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5,11]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		        data.string.p7_1,
		        data.string.p7_2,
		        data.string.p7_3,
		        data.string.p7_6,
		        data.string.p7_7,
		        data.string.p7_8,
		        data.string.p7_9,
		        data.string.p7_10

		],

	},


	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",

			title_1 : [data.string.p7_1],
			title_2 : [data.string.p7_13],
			title_3 : [data.string.p7_15],
			text : [
				data.string.p7_16
			],
	},

	/*end of second page */

	/*------------------------------------------------------------------------------------------------*/

	{
		justClass : "third",

			text : [
			        data.string.p7_1,
			        data.string.p7_19,
			        data.string.p7_18,
			        data.string.p7_24,
			        data.string.p7_25,
			],
	},


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);

		};

		/*first call to first*/
		// first();

		function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);

			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);


			$board.find(".wrapperIntro.firstPage .boy-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking01").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);

		}

		function third () {
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").hide(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);


			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .boy-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
		}

		function fourth () {
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);

			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);


			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening02").show(0);

		}

		function fifth () {
			$(".nextBtn.myNextStyle").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);



			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);


			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#b6b6b6');
			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);

			$board.find(".wrapperIntro.firstPage .squirrel-listening02").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .text4").delay(1000).show(0);
			$board.find(".wrapperIntro.firstPage .text5").delay(1500).show(0);
			$board.find(".wrapperIntro.firstPage .text6").delay(2000).show(0);
			$board.find(".wrapperIntro.firstPage .text7").delay(2500).show(0);
			$(".nextBtn.myNextStyle").delay(3000).show(0);
		}

	/*-------------------------------------------------------------------------------------------*/

	/*second page*/


		/*text 0 firstline*/
		function beforeClicks () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
		}
		/*end of text 0 firstline*/

		function beforeClicks_first () {
			$board.find(".wrapperIntro.second .title_2").show(0);

		}

		function beforeClicks_second () {
			$board.find(".wrapperIntro.second .title_3").show(0);

		}

		function beforeClicks_third () {
			$board.find(".wrapperIntro.second .imageholder04").show(0);
		}

		function beforeClicks_fourth () {
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text0").css("background","#efefef");
		}

		function beforeClicks_fifth () {
			$board.find(".wrapperIntro.second .solution01").show(0);
		}


		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/


/*page3*/

		function thirdPage () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
		}

		function third_first () {
			$board.find(".wrapperIntro.third .text1").show(0);

		}

		function third_second () {
			$board.find(".wrapperIntro.third .text2").show(0);

		}

		function third_third () {
			$board.find(".wrapperIntro.third .text3").show(0);

		}

		function third_fourth () {
			$board.find(".wrapperIntro.third .solution00").show(0);
			$board.find(".wrapperIntro.third .text4").css("background","#efefef");

		}

		function third_fifth () {
			$board.find(".wrapperIntro.third .solution01").show(0);

		}

		function third_sixth () {
			$board.find(".wrapperIntro.third .solution02").show(0);

		}

		function third_seventh () {
			$board.find(".wrapperIntro.third .solution03").show(0);

		}

		function third_eight () {
			$board.find(".wrapperIntro.third .solution04").show(0);

		}

		function third_ninth () {
			$board.find(".wrapperIntro.third .solution05").show(0);

		}

		function third_tenth () {
			$board.find(".wrapperIntro.third .solution06").show(0);

		}

		function third_eleventh () {
			$board.find(".wrapperIntro.third .solution07").show(0);

		}

		function third_twelth () {
			$board.find(".wrapperIntro.third .solution08").show(0);
			$board.find(".wrapperIntro.third .solution08").wrapAll('<div class="divsol08">');
		}

/*******page3 end*************************************************************************************/





		function callNextChecker () {
			var nextChecker = true;
			for (var i = 0; i < baseAndIndicesClick.length; i++) {
				if (baseAndIndicesClick[i] ===null){
					nextChecker = false;
					break;
				}
			};
			if (nextChecker) {
				$nextBtn.show(0);
			};
		}
		/*end of base and indecis*/

		/*********/




		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});


		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				second,
				third,
				fourth,
				fifth,

				beforeClicks, //second one


				beforeClicks_first,
				beforeClicks_second,
				beforeClicks_third,
				beforeClicks_fourth,
				beforeClicks_fifth,


				thirdPage,//third
				third_first,
				third_second,
				third_third,
				third_fourth,
				third_fifth,
				third_sixth,
				third_seventh,
				third_eight,
				third_ninth,
				third_tenth,
				third_eleventh,
				third_twelth

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

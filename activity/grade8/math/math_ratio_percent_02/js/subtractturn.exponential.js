var dataObj = [{
		title : data.string.pRsubTurn_1,
		solve : data.string.solve,
		examples : [
			data.string.example,
			math.parseEquation("a ^12 / a^7"),
			"= "+math.parseEquation(" a^(12-7)"),
			"= "+math.parseEquation("a^5"),
			data.string.answer+" = "+math.parseEquation("a^5")
		],
		yourTurn : data.string.yourturn
	},{
		count : 1,
		q : math.parseEquation("7^7 / 7^2"),
		solution : ["= "+math.parseEquation(" 7^(7-2)"),
		"= "+math.parseEquation("7^5"),
		data.string.answer+" = "+math.parseEquation("7^5")],
		ans : ["7","5"]
	},{
		count : 2,
		q : math.parseEquation("b^11 / b^5"),
		solution : ["= "+math.parseEquation("b^(11-5)"),
		"= "+math.parseEquation("b^6"),
		data.string.answer+" = "+math.parseEquation("b^6")],
		ans : ["b","6"]
	},{
		count : 3,
		q : math.parseEquation("8a^7 / 2a^4"),
		solution : ["= "+math.parseEquation("(8/2)*a^(7-4)"),
		"= "+math.parseEquation("4a^3"),
		data.string.answer+" = "+math.parseEquation("4a^3")],
		ans : ["4","a","3"]
	}]

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}
// startYourTurn (dataObj);
$(function () {
	startYourTurn (dataObj);
})
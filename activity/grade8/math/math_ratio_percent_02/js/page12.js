/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function() {
  var characterDialouges1 = [
    {
      diaouges: ole.textSR(
        data.string.p12_3,
        data.string.p12_3,
        "<span class='ind2'>" + data.string.p12_3 + "</span>"
      )
    } //
  ];

  var characterDialouges2 = [
    {
      diaouges: ole.textSR(
        data.string.p12_6,
        data.string.p12_6,
        "<span class='ind2'>" + data.string.p12_6 + "</span>"
      )
    } //
  ];

  var characterDialouges3 = [
    {
      diaouges: ole.textSR(
        data.string.p12_7,
        data.string.p12_7,
        "<span class='ind2'>" + data.string.p12_7 + "</span>"
      )
    } //
  ];

  var characterDialouges4 = [
    {
      diaouges: ole.textSR(
        data.string.p12_44,
        data.string.p12_44,
        "<span class='ind2'>" + data.string.p12_44 + "</span>"
      )
    } //
  ];

  var characterDialouges5 = [
    {
      diaouges: ole.textSR(
        data.string.p12_45,
        data.string.p12_45,
        "<span class='ind2'>" + data.string.p12_45 + "</span>"
      )
    } //
  ];

  var characterDialouges6 = [
    {
      diaouges: ole.textSR(
        data.string.p12_46,
        data.string.p12_46,
        "<span class='ind2'>" + data.string.p12_46 + "</span>"
      )
    } //
  ];

  //------------------------------------------------------------------------------------------------//
  var $board = $(".board"),
    $nextBtn = $("#activity-page-next-btn-enabled"),
    $prevBtn = $("#activity-page-prev-btn-enabled"),
    $refreshBtn = $("#activity-page-refresh-btn");
  (countNext = 0), (all_page = 34);
  $nextBtn.show(0);

  var baseAndIndicesClick = [null, null]; //used by base and index

  var clickArray = [0, 4, 11, 30]; //manipulate the clicks like back and from top
  $total_page = clickArray.length;
  loadTimelineProgress($total_page, 1);
  var dataObj = [
    {
      justClass: "firstPage",
      animate: "true",
      text: [
        function() {
          var d = ole.textSR(
            data.string.p12_1,
            data.string.p12_1,
            "<span class='ind2'>" + data.string.p12_1 + "</span>"
          );
          return d;
        },
        data.string.p12_3
      ]
    },

    /*------------------------------------------------------------------------------------------------*/
    /*start of second page*/
    {
      justClass: "second",

      text: [
        function() {
          var d = ole.textSR(
            data.string.p12_9,
            data.string.p12_9,
            "<span class='ind2'>" + data.string.p12_9 + "</span>"
          );
          return d;
        },
        function() {
          var d = ole.textSR(
            data.string.p12_10,
            data.string.p12_10,
            "<span class='ind2'>" + data.string.p12_10 + "</span>"
          );
          return d;
        },
        data.string.p12_11
        /*function () {
					var d = ole.textSR(data.string.p12_16,data.string.p12_16,"<span class='ind2'>"+data.string.p12_16+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p12_17,data.string.p12_17,"<span class='ind2'>"+data.string.p12_17+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p12_18,data.string.p12_18,"<span class='ind2'>"+data.string.p12_18+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p12_19,data.string.p12_19,"<span class='ind2'>"+data.string.p12_19+"</span>");
					return d;
				},
				function () {
					var d = ole.textSR(data.string.p12_20,data.string.p12_20,"<span class='ind2'>"+data.string.p12_20+"</span>");
					return d;
				},*/
      ],
      priceheading: [data.string.p12_8],
      pricetag1: [data.string.rs + "200"],
      pricetag2: [data.string.rs + "400"],
      pricetag3: [data.string.rs + "600"],
      pricestatement1: [data.string.p12_37],
      pricestatement2: [data.string.p12_38],
      pricestatement3: [data.string.p12_39],
      pricestatement: [data.string.p12_40]
    },

    /*end of second page */

    /*start of third page*/
    {
      justClass: "third",
      title_1: [data.string.p12_9],
      title_2: [data.string.p12_16],
      title_3: [data.string.p12_17],
      rowheading1: [data.string.p12_41],
      rowheading2: [data.string.p12_42],
      text: [
        data.string.p12_18,
        data.string.p12_24,
        data.string.p12_23,
        data.string.p12_27
      ]
    },

    {
      justClass: "fourth",
      text: [data.string.p12_9, data.string.p12_43]
    }

    /*end of second page */

    /*------------------------------------------------------------------------------------------------*/
  ];

  /******************************************************************************************************/
  /*
   * first
   */
  function first() {
    var source = $("#intro-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[0];
    var html = template(content);
    $board.html(html);
    $board.find(".wrapperIntro.firstPage .img-responsive").show(0);
  }

  /*first call to first*/
  // first();
  //function second () {
  //$("#activity-page-next-btn-enabled").hide(0).delay(3000).show(0);
  //$board.find(".wrapperIntro.firstPage .f3_2").show(0);

  //$board.find(".wrapperIntro.firstPage .f3_2").addClass("slideRight");
  //$board.find(".wrapperIntro.firstPage .f3_2").css('display', 'inline-flex');
  //}
  /*function second () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			$board.find(".wrapperIntro.firstPage .text1").css("opacity",1);

		}*/

  /* third click*/
  function characterDialouge1() {
    appendDialouge = '<div class="p12_6">';
    var dialouges = $.each(characterDialouges1, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des1")
      .append(appendDialouge)
      .css("opacity", 1);
  }
  /* end of third click*/

  function characterDialouge2() {
    appendDialouge = '<div class="p12_7">';
    var dialouges = $.each(characterDialouges2, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des2").append(appendDialouge);
  }

  function characterDialouge3() {
    appendDialouge = '<div class="p12_8">';
    var dialouges = $.each(characterDialouges3, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des3").append(appendDialouge);
  }
  /*function third () {
			$("#activity-page-next-btn-enabled").hide(0).delay(3000).show(0);
			$board.find(".wrapperIntro.firstPage .f3_1").show(0);
			$board.find(".wrapperIntro.firstPage .f3_1").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .f3_1").css('display', 'inline-flex');


			appendDialouge = '<div class="p12_8">';
			var dialouges =$.each(characterDialouges03,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
			appendDialouge += '</div>';
			$(".des03").append(appendDialouge);
		}*/

  function second_first() {
    var source = $("#intro2-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[1];
    var html = template(content);
    $board.html(html);
  }

  /*first call to first*/
  // first();
  function first_second() {
    $board.find(".wrapperIntro").show(0);
    $board.find(".wrapperIntro .text1").css("opacity", 1);
  }
  function first_third() {
    $board.find(".wrapperIntro .main-bg img").show(0);
    $board.find(".wrapperIntro .des1").css("opacity", 1);
    characterDialouge2();
  }
  function first_fourth() {
    $board.find(".wrapperIntro .main-bg img").show(0);
    $board.find(".wrapperIntro .des2").css("opacity", 1);
    characterDialouge3();
  }

  function second_second() {
    $board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);
    $board.find(".wrapperIntro.second .text1").css("opacity", 1);
  }

  function second_third() {
    $board.find(".wrapperIntro.second .text2").show(0);
    //$board.find(".wrapperIntro.firstPage .imageholder01").show(0);
    //$board.find(".wrapperIntro.firstPage .transparency-text").show(0);
  }

  function second_fourth() {
    $board.find(".wrapperIntro.second .imageholder02").show(0);
    $board.find(".wrapperIntro.second .price").show(0);
    $board.find(".wrapperIntro.second .pricestatement1").show(0);
    $board.find(".wrapperIntro.second .transparency-text").show(0);
  }

  function second_fifth() {
    $board.find(".wrapperIntro.second .imageholder03").show(0);
    $board.find(".wrapperIntro.second .price").show(0);
    $board.find(".wrapperIntro.second .tag1").hide(0);
    $board.find(".wrapperIntro.second .tag2").show(0);
    $board.find(".wrapperIntro.second .pricestatement1").hide(0);
    $board.find(".wrapperIntro.second .pricestatement2").show(0);
    $board.find(".wrapperIntro.second .transparency-text").show(0);
  }

  function second_sixth() {
    $board.find(".wrapperIntro.second .imageholder04").show(0);
    $board.find(".wrapperIntro.second .price").show(0);
    $board.find(".wrapperIntro.second .price").css("top", "-45%");
    $board.find(".wrapperIntro.second .tag2").hide(0);
    $board.find(".wrapperIntro.second .tag3").show(0);
    $board.find(".wrapperIntro.second .pricestatement2").hide(0);
    $board.find(".wrapperIntro.second .pricestatement3").show(0);
    $board.find(".wrapperIntro.second .transparency-text").show(0);
  }

  function second_seventh() {
    $board.find(".wrapperIntro.second .text1").hide(0);
    $board.find(".wrapperIntro.second .text2").hide(0);
    $board.find(".wrapperIntro.second .img-des2").hide(0);
    $board.find(".wrapperIntro.second .img-des3").hide(0);
    $board.find(".wrapperIntro.second .img-des4").hide(0);
    $board.find(".wrapperIntro.second .price").hide(0);
    $board.find(".wrapperIntro.second .pricestatement3").hide(0);
    $board.find(".wrapperIntro.second .imageholder05").show(0);
    $board.find(".wrapperIntro.second .imageholder06").show(0);
    $board.find(".wrapperIntro.second .imageholder07").show(0);
    $board.find(".wrapperIntro.second .imageholder08").show(0);
    $board.find(".wrapperIntro.second .imageholder09").show(0);
    $board.find(".wrapperIntro.second .imageholder10").show(0);
    $board.find(".wrapperIntro.second .price1").show(0);
    $board.find(".wrapperIntro.second .price2").show(0);
    $board.find(".wrapperIntro.second .price3").show(0);
    $board.find(".wrapperIntro.second .imageholder11").show(0);
    $board.find(".wrapperIntro.second .pricestatement").show(0);
  }

  /* third click*/
  /*function characterDialouge4(){
			appendDialouge = '<div class="p12_21">';
			var dialouges =$.each(characterDialouges4,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des4").append(appendDialouge);
		}



		 fourth click
		function characterDialouge5(){
			appendDialouge = '<div class="p12_22">';
			var dialouges =$.each(characterDialouges5,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des5").append(appendDialouge);
		}
		 end of fourth click

		 fifth click
		function characterDialouge6(){
			appendDialouge = '<div class="p12_23">';
			var dialouges =$.each(characterDialouges6,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des6").append(appendDialouge);
		}
		 end of fifth click
		function second_tenth () {
			$board.find(".wrapperIntro.second .imageholder03").show(0);
			$board.find(".wrapperIntro.second .transparency-text").show(0);
		}
		 sixth click
		function characterDialouge7(){
			appendDialouge = '<div class="p12_24">';
			var dialouges =$.each(characterDialouges7,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des7").append(appendDialouge);
		}
		 end of sixth click

		 seventh click
		function characterDialouge8(){
			appendDialouge = '<div class="p12_25">';
			var dialouges =$.each(characterDialouges8,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des8").append(appendDialouge);
		}
		 end of seventh click

		 eigth click
		function characterDialouge9(){
			appendDialouge = '<div class="p12_26">';
			var dialouges =$.each(characterDialouges9,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des9").append(appendDialouge);
		}
		 end of eigth click

		 ninth click
		function characterDialouge10(){
			appendDialouge = '<div class="p12_27">';
			var dialouges =$.each(characterDialouges10,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des10").append(appendDialouge);
		}
		 end of ninth click

		 tenth click
		function characterDialouge11(){
			appendDialouge = '<div class="p12_28">';
			var dialouges =$.each(characterDialouges11,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des11").append(appendDialouge);
		}
		 end of tenth click

		 eleventh click
		function characterDialouge12(){
			appendDialouge = '<div class="p12_29">';
			var dialouges =$.each(characterDialouges12,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des12").append(appendDialouge);
		}
		 end of eleventh click

		 twelth click
		function characterDialouge13(){
			appendDialouge = '<div class="p12_30">';
			var dialouges =$.each(characterDialouges13,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des13").append(appendDialouge);
		}
		 end of twelth click

		 thirteen click
		function characterDialouge14(){
			appendDialouge = '<div class="p12_31">';
			var dialouges =$.each(characterDialouges14,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des14").append(appendDialouge);
		}
		 end of thirteen click

		function characterDialouge15(){
			appendDialouge = '<div class="p12_32">';
			var dialouges =$.each(characterDialouges15,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des15").append(appendDialouge);
		}

		function characterDialouge16(){
			appendDialouge = '<div class="p12_33">';
			var dialouges =$.each(characterDialouges16,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des16").append(appendDialouge);
		}
		function characterDialouge17(){
			appendDialouge = '<div class="p12_34">';
			var dialouges =$.each(characterDialouges17,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des17").append(appendDialouge);
		}
		function characterDialouge18(){
			appendDialouge = '<div class="p12_35">';
			var dialouges =$.each(characterDialouges18,function(key,values){
			chDialouges = values.diaouges;

			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des18").append(appendDialouge);
		}*/

  /*end of page 2*/
  function third_first() {
    var source = $("#intro3-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[2];
    var html = template(content);
    $board.html(html);
  }

  function third_second() {
    $board.find(".wrapperIntro.third .title_2").show(0);
  }

  function third_third() {
    $board.find(".wrapperIntro.third .title_3").show(0);
  }

  function third_fourth() {
    $board.find(".wrapperIntro.third .exmpletable").show(0);
  }

  function third_fifth() {
    $board.find(".wrapperIntro.third .text0").show(0);
    $board.find(".wrapperIntro.third .text1").show(0);
  }

  function third_sixth() {
    $board.find(".wrapperIntro.third .text2").show(0);
    $board.find(".wrapperIntro.third .diagonal").show(0);
  }

  function third_seventh() {
    $board.find(".wrapperIntro.third .text2").hide(0);
    $board.find(".wrapperIntro.third .diagonal").hide(0);
    $board.find(".wrapperIntro.third .solution00").show(0);
    $board.find(".wrapperIntro.third .text3").css("background", "#efefef");
  }

  function third_8() {
    $board.find(".wrapperIntro.third .imageholder13").show(0);
    $board.find(".wrapperIntro.third .imageholder14").show(0);
    $board.find(".wrapperIntro.third .solution01").show(0);
  }

  function third_9() {
    $board.find(".wrapperIntro.third .solution02").show(0);
  }

  function third_10() {
    $board.find(".wrapperIntro.third .solution03").show(0);
  }

  function third_11() {
    $board.find(".wrapperIntro.third .text1").hide(0);
    $board.find(".wrapperIntro.third .text3").css("height", "53%");
    $board.find(".wrapperIntro.third .img-des13").css("top", "-72%");
    $board.find(".wrapperIntro.third .img-des14").css("top", "-77%");
    $board.find(".wrapperIntro.third .solution00").hide(0);
    $board.find(".wrapperIntro.third .solution01").hide(0);
    $board.find(".wrapperIntro.third .solution02").hide(0);
    $board.find(".wrapperIntro.third .solution03").hide(0);
    $board.find(".wrapperIntro.third .solution04").show(0);
  }

  function third_12() {
    $board.find(".wrapperIntro.third .solution05").show(0);
  }

  function third_13() {
    $board.find(".wrapperIntro.third .solution06").show(0);
  }

  function third_14() {
    $board.find(".wrapperIntro.third .solution07").show(0);
  }

  function third_15() {
    $board.find(".wrapperIntro.third .solution08").show(0);
  }

  function third_16() {
    $board.find(".wrapperIntro.third .solution09").show(0);
  }

  function third_17() {
    $board.find(".wrapperIntro.third .solution04").hide(0);
    $board.find(".wrapperIntro.third .solution05").hide(0);
    $board.find(".wrapperIntro.third .solution06").hide(0);
    $board.find(".wrapperIntro.third .solution07").hide(0);
    $board.find(".wrapperIntro.third .solution08").hide(0);
    $board.find(".wrapperIntro.third .solution09").hide(0);
    $board.find(".wrapperIntro.third .solution10").show(0);
  }

  function third_18() {
    $board.find(".wrapperIntro.third .solution11").show(0);
  }

  function third_19() {
    $board.find(".wrapperIntro.third .solution12").show(0);
  }

  function third_20() {
    $board.find(".wrapperIntro.third .solution13").show(0);
  }

  function fourth() {
    var source = $("#intro4-template").html();
    var template = Handlebars.compile(source);
    var content = dataObj[3];
    var html = template(content);
    $board.html(html);
    $board.find(".wrapperIntro.fourth .img-responsive").show(0);
  }

  /* third click*/
  function characterDialouge4() {
    appendDialouge = '<div class="p12_44">';
    var dialouges = $.each(characterDialouges4, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des4").append(appendDialouge);
  }
  /* end of third click*/

  function characterDialouge5() {
    appendDialouge = '<div class="p12_45">';
    var dialouges = $.each(characterDialouges5, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des5").append(appendDialouge);
  }

  function characterDialouge6() {
    appendDialouge = '<div class="p12_46">';
    var dialouges = $.each(characterDialouges6, function(key, values) {
      chDialouges = values.diaouges;

      appendDialouge += chDialouges;
    });
    appendDialouge += "</div>";
    $(".des6").append(appendDialouge);
  }
  /*-------------------------------------------------------------------------------------------------*/

  function callNextChecker() {
    var nextChecker = true;
    for (var i = 0; i < baseAndIndicesClick.length; i++) {
      if (baseAndIndicesClick[i] === null) {
        nextChecker = false;
        break;
      }
    }
    if (nextChecker) {
      $nextBtn.show(0);
    }
  }
  /*end of base and indecis*/

  /*********/

  // first func call
  first();
  // lastBaseIndices ();
  // countNext = 12;

  /*click functions*/
  $nextBtn.on("click", function() {
    // $(this).css("display","none");
    $prevBtn.show(0);

    countNext++;
    fnSwitcher();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] < countNext) {
        countNext = clickArray[i];
        // console.log(countNext+" = "+clickArray[i]);
        break;
      }
    }
    if (countNext === 0) {
      $prevBtn.hide(0);
    }
    fnSwitcher();
  });

  function clickSequence(index) {
    for (var i = clickArray.length - 1; i >= 0; i--) {
      if (clickArray[i] === index) {
        countNext = clickArray[i];
        break;
      }
    }
    fnSwitcher();
  }

  function fnSwitcher() {
    fnArray = [
      first, //first one
      //second,
      first_second,
      // characterDialouge1,
      first_third,
      first_fourth,
      //third,

      second_first, //second one
      second_second,
      second_third,
      /*second_fourth,
				second_fifth,
				second_sixth,
				second_seventh,
				second_eight,*/
      second_fourth,
      second_fifth,
      second_sixth,
      second_seventh,

      third_first,
      third_second,
      third_third,
      third_fourth,
      third_fifth,
      third_sixth,
      third_seventh,
      third_8,
      third_9,
      third_10,
      third_11,
      third_12,
      third_13,
      third_14,
      third_15,
      third_16,
      third_17,
      third_18,
      third_19,
      third_20,

      fourth,
      characterDialouge4,
      characterDialouge5,
      characterDialouge6
      /*second_tenth,
				characterDialouge7,
				characterDialouge8,
				characterDialouge9,
				characterDialouge10,
				characterDialouge11,
				characterDialouge12,
				characterDialouge13,
				characterDialouge14,
				characterDialouge15,
				characterDialouge16,
				characterDialouge17,
				characterDialouge18*/
    ];

    fnArray[countNext]();

    for (var i = 0; i < clickArray.length; i++) {
      if (clickArray[i] === countNext) {
        loadTimelineProgress($total_page, i + 1);
      }
    }

    if (countNext >= all_page) {
      $nextBtn.hide(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    } else {
      // $(this).show(0);
      // $nextBtn.show(0);
    }
  }
  /****************/
});

var dataObj = [{
		title : data.string.pRzeroTurn_1,
		solve : data.string.solve,
		examples : [
			data.string.example,
			math.parseEquation("a ^ 12 / a^ 12"),
			"= "+math.parseEquation(" a^(12-12)"),
			"= "+math.parseEquation("a^0"),
			data.string.answer+" = "+math.parseEquation("1")
		],
		yourTurn : data.string.yourturn
	},{
		count : 1,
		q : math.parseEquation("8^7 / 8^7"),
		solution : ["= "+math.parseEquation(" 8^(7-7)"),
		"= "+math.parseEquation("8^0"),
		data.string.answer+" = "+math.parseEquation("1")],
		ans : ["1"]
	},{
		count : 2,
		q : math.parseEquation("b^11 / b^11"),
		solution : ["= "+math.parseEquation("b^(11-11)"),
		"= "+math.parseEquation("b^0"),
		data.string.answer+" = "+math.parseEquation("1")],
		ans : ["1"]
	}]

	function substitue (mainText,arrayText) {
		var d = mainText;
		for (var i = 0; i < arrayText.length; i++) {
			d = ole.textSR(d,arrayText[i],"<span class='highlight sub'"+i+">"+arrayText[i]+"</span>");
		};
		return d;
	}
// startYourTurn (dataObj);
$(function () {
	startYourTurn (dataObj);
})
/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		timer = 100;

	var baseAndIndicesClick = [null,null]; //used by base and index

	$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
	      		{first : "7", second : "9"},
	      		{first : "3", second : "2"},
	      	];

	var dataObj = [{
		justClass : "first",
		text : [data.string.p4_1,
			"2<sup>3</sup>",
			"2 x 3"]
	},{
		justClass : "second",
		text : [data.string.p2_1,
		        /*data.string.p2_7,*/
			data.string.example+": "+data.string.p2_5,
			data.string.p2_13,
			"",
			data.string.p2_14,
			"",
			"",
			/*function () {
				var d = data.string.p4_3;
				d = ole.textSR(d,data.string.p4_4,"<span>"+data.string.p4_4+"</span>");
				d = ole.textSR(d,data.string.p4_5,"<span>"+data.string.p4_5+"</span>")
				return d;
			},
			"",
			data.string.p4_6,*/
			],
		lokharke : "images/timetothink/timetothink1.png",
		lokharkeText : data.string.p4_7,
	}];

	var dToShow = [" = 5",
			"= 2"];


	function supMaker (base,power) {
		var arrays = [base,"<sup>"+power+"</sup>", " = "];
		arrays.push(base);
		for (var i = 0; i < power-1; i++) {
			arrays.push(" x ");
			arrays.push(base);
		};
		return arrays;
	}


	/*
	* first
	*/
		/*function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
		};*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$nextBtn.hide(0);
			$board.find(".wrapperIntro.second .lokharke").hide(0);
			setTimeout(second_1,400);
			$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
			$(".lokharkeTxt").html(data.string.yourTurn);
		}

		function second_1 () {
			$board.find(".wrapperIntro.second .text1,.wrapperIntro.second .text2").show(0);
			ole.stringShow(".wrapperIntro.second .text2", dToShow[0], "", 500,null,second_2);
		}
		function second_2 () {
			$board.find(".wrapperIntro.second .text3,.wrapperIntro.second .text4").show(0);
			ole.stringShow(".wrapperIntro.second .text4", dToShow[1], "", 500,null,second_next);
		}
		function second_next () {
			$nextBtn.show(0);
		}

		function quefrst() {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			var source = $("#qaArea-template").html();
			var template = Handlebars.compile(source);
			var content = {
				check : "check",
				clickToSee : data.string.p2_3,
				answer : data.string.p2_4,
				/*baseString : data.string.p4_22,
				powerString : data.string.p2_6,*/
				first : data.string.p2_9,
				second : data.string.p2_10,
				text   : data.string.p2_8,
				couter : countNext+1,
				counter : countNext,
				check: data.string.check,
				Antecedent: data.string.p2_13,
				Consequent: data.string.p2_14

			};
			var html = template(content);
			$board.find(".qaArea").append(html);
			// $board.find(".qaArea input.base").focus();
		};

		function quesecnd() {
			$nextBtn.hide(0);
			$prevBtn.hide(0);
			var source = $("#qaArea-template").html();
			var template = Handlebars.compile(source);
			var content = {
				check : "check",
				clickToSee : data.string.p2_3,
				answer : data.string.p2_4,
				/*baseString : data.string.p4_22,
				powerString : data.string.p2_6,*/
				first : data.string.p2_11,
				second : data.string.p2_12,
				text   : data.string.p2_8,
				couter : countNext+1,
				counter : countNext,
				check: data.string.check,
				Antecedent: data.string.p2_13,
				Consequent: data.string.p2_14
			};
			var html = template(content);
			$board.find(".qaArea").append(html);
			// $board.find(".qaArea input.base").focus();
		};
		var countHowmany = 0;
		$board.on('click','.clicks .click',function (){
			var $this = $(this);
			var num = parseInt($this.data("cls"));
			var cls = ".q"+num;
			$board.find(cls+' .showAns').show(0);
			$nextBtn.show(0);
		});

		$board.on('click','.clicks .check',function () {
			var $this = $(this);
			var num = parseInt($this.data("cls"));
			var cls = ".q"+num;
			var base = $board.find(cls+" input.base").val().toLowerCase();
			var power = $board.find(cls+" input.power").val().toLowerCase();
			// console.log(base+"  "+power);
			var upDatas = qDatas[num-2];
			var check = [null,null];
			var check_empty = [null,null];
			var wrong = "<img src='images/wrong.png'>";
			// wrong.src = "images/wrong.png";
			var right = "<img src='images/correct.png'>";
			// right.src = "images/correct.png";
			if(base === ""){
				check_empty[0] = 1;
			}
			else{
				if (base ===upDatas.first) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
			}

			if(power === ""){
				check_empty[1] = 1;
			}else{
				if (power===upDatas.second) {
					check[1]=1;
					$board.find(cls+" .check2").html(right);
				} else {
					$board.find(cls+" .check2").html(wrong);
				}
			}
			if(check_empty[0] ===1 || check_empty[1] === 1){
				swal(data.string.h2);
			}else{
				if (check[0]===1 && check[1]===1) {
					if (countNext>=2) {
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				} else {
					$board.find(cls+" .clicks .click").show(0);
				}
			}

		});



		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 11;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			//$prevBtn.show(0);
			countNext++;
			if (countNext>=$total_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
			fnSwitcher();

		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		fnArray = [
				first,  //first one
				quefrst, //second
				quesecnd
			];

		function fnSwitcher () {
			// console.log(countNext+" "+fnArray[countNext]);
			fnArray[countNext]();

			loadTimelineProgress($total_page,countNext+1);

		}
	/****************/

});

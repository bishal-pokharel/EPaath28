/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	

//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=16;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,4,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p5_1,
		data.string.p5_2,
		data.string.p5_3,
		data.string.p5_4,
		data.string.p5_5,
		data.string.p5_6,
		data.string.p5_7,
		data.string.p5_8,
		data.string.p5_9,
		data.string.p5_10,
		data.string.p5_11,
		data.string.p5_12,
		data.string.p5_12_1,
		],
	},
		
		{
		
		justClass : "second",
		animate : "true",
		text : [
		data.string.p5_1,
		data.string.p5_12_2,
		data.string.p5_13,
		data.string.p5_14,
		data.string.p5_15,
		data.string.p5_16,
		data.string.p5_17,
		data.string.p5_18,
		data.string.p5_19,
		],
	},
	
	
	{
		
		justClass : "third",
		animate : "true",
		text : [
		data.string.p5_20,
		data.string.p5_21,
		data.string.p5_22,
		data.string.p5_22_1,
		data.string.p5_23,
		data.string.p5_24,
		data.string.p5_25,
		data.string.p5_26,
		data.string.p5_27,
		data.string.p5_28,
		data.string.p5_29,
		data.string.p5_30,
		data.string.p5_31,
		data.string.p5_32,
		data.string.p5_33,
		data.string.p5_34
		],
	},


	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			setTimeout(function(){
				$nextBtn.show();
			$(".wrapperIntro.firstPage .text0").show(0);
			
			$(".wrapperIntro.firstPage .text3").click(function(){
				$(".wrapperIntro.firstPage .text3").addClass('incorrect');
			});
			
			$(".wrapperIntro.firstPage .text4").click(function(){
				$(".wrapperIntro.firstPage .text4").addClass('incorrect');
				
			});
			
			$(".wrapperIntro.firstPage .text5").click(function(){
				$(".wrapperIntro.firstPage .text5").addClass('incorrect');
			});
			
			$(".wrapperIntro.firstPage .text6").click(function(){
				$(".wrapperIntro.firstPage .text6").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.firstPage .text9").click(function(){
				$(".wrapperIntro.firstPage .text9").addClass('incorrect');
				
			});
			
			$(".wrapperIntro.firstPage .text10").click(function(){
				$(".wrapperIntro.firstPage .text10").addClass('incorrect');
			});
			
			$(".wrapperIntro.firstPage .text11").click(function(){
				$(".wrapperIntro.firstPage .text11").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.firstPage .text12").click(function(){
				$(".wrapperIntro.firstPage .text12").addClass('incorrect');
			});

            },1000);
			
		};

		 function first02() {

		 	 $(".wrapperIntro.firstPage .text1").show(0);
		 	 $(".wrapperIntro.firstPage .text2").show(0);
			 $(".wrapperIntro.firstPage .text3").show(0);
			 $(".wrapperIntro.firstPage .text4").show(0);
			 $(".wrapperIntro.firstPage .text5").show(0);
			 $(".wrapperIntro.firstPage .text6").show(0);
			 $nextBtn.hide(0);
		 }
			 
		 function first03() {
			 $(".wrapperIntro.firstPage .text2").css("background-color", "#cccccc");
		 	 $(".wrapperIntro.firstPage .text7").show(0);
		 }
		 
		  function first04() {
			 $(".wrapperIntro.firstPage .text7").hide(0);
		 	 $(".wrapperIntro.firstPage .text8").css("background-color", "#cccccc").show(0);
			 $(".wrapperIntro.firstPage .text9").show(0);
			 $(".wrapperIntro.firstPage .text10").show(0);
			 $(".wrapperIntro.firstPage .text11").show(0);
			 $(".wrapperIntro.firstPage .text12").show(0);
			 $nextBtn.hide(0);
			 
		 }
		 
		 
		 function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.second .text0").show(0);
			
		};
		 
		 function second02() {
			 
		 	 $(".wrapperIntro.second .text1").show(0);
		 	 $(".wrapperIntro.second .text2").show(0);
		 	 
		 }
		 
		 function second03() {
			 
		 	 $(".wrapperIntro.second .text3").show(0).css("left","61%");
			 $(".one").addClass("ones");
		 	 $(".two").addClass("twos");
			 $(".three").addClass("threes");
			 $(".four").addClass("fours");
			 $(".one1").addClass("ones1");
		 	 $(".two2").addClass("twos2");
			 $(".three3").addClass("threes3");
			 $(".four4").addClass("fours4");
		 }
		 
		  function second04() {
			 $(".wrapperIntro.second .text3").hide(0);
		 	 $(".wrapperIntro.second .text4").show(0);
			 $(".wrapperIntro.second .text5").show(0);
		 	 //$(".five").css("background", "#2292ef");
		 }
		 
		 function second05() {
			//  $(".wrapperIntro.second .text4").hide(0);
		 	 $(".wrapperIntro.second .text6").show(0);
			 $(".wrapperIntro.second .text7").show(0);
		 	 
		 }
		 
		 function second06() {
			 
		 	 $(".wrapperIntro.second .text8").show(0);
			
		 	 
		 }
		 
		 
		 
		 function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.third .text0").show(0);
			
			
			$(".wrapperIntro.third .text5").click(function(){
				$(".wrapperIntro.third .text5").addClass('incorrect');
			});
			
			$(".wrapperIntro.third .text6").click(function(){
				$(".wrapperIntro.third .text6").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.third .text7").click(function(){
				$(".wrapperIntro.third .text7").addClass('incorrect');
			});
			
			$(".wrapperIntro.third .text8").click(function(){
				$(".wrapperIntro.third .text8").addClass('incorrect');
				
			});
			
			$(".wrapperIntro.third .text11").click(function(){
				$(".wrapperIntro.third .text11").addClass('incorrect');
				
			});
			
			$(".wrapperIntro.third .text12").click(function(){
				$(".wrapperIntro.third .text12").addClass('incorrect');
			});
			
			$(".wrapperIntro.third .text13").click(function(){
				$(".wrapperIntro.third .text13").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.third .text14").click(function(){
				$(".wrapperIntro.third .text14").addClass('incorrect');
			});
			
		};
		 
		 function third02() {
			 
		 	 $(".wrapperIntro.third .text1").show(0);
		 	 
		 	 
		 }
		 
		 function third03() {
			 $(".wrapperIntro.third .text2").show(0);
		 	 $(".wrapperIntro.third .text3").show(0);
			 
		 }
		 
		  function third04() {
			 
		 	 $(".wrapperIntro.third .text4").show(0);
			 $(".wrapperIntro.third .text5").show(0);
			 $(".wrapperIntro.third .text6").show(0);
			 $(".wrapperIntro.third .text7").show(0);
			 $(".wrapperIntro.third .text8").show(0);
			 $nextBtn.hide(0);
		 	 
		 }
		 
		 function third05() {
			 $(".wrapperIntro.third .text9").show(0);
		 	 	 
		 }
		 
		 function third06() {
			 $(".wrapperIntro.third .text9").hide(0);
		 	 $(".wrapperIntro.third .text10").show(0);
			 $(".wrapperIntro.third .text11").show(0);
			 $(".wrapperIntro.third .text12").show(0);
			 $(".wrapperIntro.third .text13").show(0);
			 $(".wrapperIntro.third .text14").show(0);
			 $nextBtn.hide(0);
		 	 
		 }
		 
		 function third07() {
			 $(".wrapperIntro.third .text15").show(0);
		 	 	 
		 }
		 
		 

		 	
		

		

		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");
		
/*----------------------------------------------------------------------------------------------------*/
		

		
	

		// first func call
		first();

		


	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
			    first03,
			    first04,
			 //    first05
			 
				second,
				second02,
				second03,
				second04,
				second05,
				second06,
				
				third,
				third02,
				third03,
				third04,
				third05,
				third06,
				third07
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

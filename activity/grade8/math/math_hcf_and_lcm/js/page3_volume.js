/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	

//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=6;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p1_2,
		data.string.p1_3,
		data.string.p1_4,
		data.string.p1_20,
		data.string.p1_21,
		data.string.p1_21_1,
		data.string.p1_22,
		data.string.p1_23,
		
		],
	},
		
		
	{
		justClass : "second",
		animate : "true",
		text : [
		data.string.p1_2,
		data.string.p1_3,
		data.string.p1_4,
		data.string.p1_25,
		data.string.p1_26,
		data.string.p1_27,
		data.string.p1_28,
		data.string.p1_29,
		//data.string.p1_30,
		
		
		
		
		],
		
	},
	
	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").delay(500).fadeIn();
			$board.find(".wrapperIntro.firstPage .text4").delay(1000).fadeIn();
			$nextBtn.hide(0);
			
		};

		/*function first02() {
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			
		 	
		}*/
		
		function first03() {
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			
		}
		
		function first04() {
			$(".button1").show(0);
			$(".button2").show(0);
			$(".button3").show(0);
			$(".button4").show(0);
			$(".button5").show(0);
			$(".button6").show(0);
			$(".button7").show(0);
			$nextBtn.hide(0);
		}
		
		
		
		function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").delay(500).fadeIn();
			$board.find(".wrapperIntro.second .text4").delay(1000).fadeIn();
			$nextBtn.hide(0);
			
			$(".wrapperIntro.second .text4").click(function(){
			$(".wrapperIntro.second .text3").hide(0);
			$(".wrapperIntro.second .text4").hide(0);
			$(".wrapperIntro.second .text5").show(0);
			$(".wrapperIntro.second .text6").show(0);
			$nextBtn.show(0);
		});
		
		
		$(".button01").click(function(){
			$(".button01").addClass("incorrect");
			});
			
		$(".button02").click(function(){
			$(".button02").addClass("incorrect");
			});
			
		$(".button03").click(function(){
			$(".button03").addClass("incorrect");
			
			});
			
		$(".button04").click(function(){
			$(".button04").addClass("correct");
			ole.footerNotificationHandler.pageEndSetNotification();
			});
			
		$(".button05").click(function(){
			$(".button05").addClass("incorrect");
			});
			
		$(".button06").click(function(){
			$(".button06").addClass("incorrect");
			});
			
		$(".button07").click(function(){
			$(".button07").addClass("incorrect");
			});
			
		};
		
		
		
		/*function second01() {
			$board.find(".wrapperIntro.second .text6").show(0);
		}*/
		
		function second02() {
            $board.find(".wrapperIntro.second .text7").show(0);
		}
		
		function second03() {
			$(".button01").show(0);
			$(".button02").show(0);
			$(".button03").show(0);
			$(".button04").show(0);
			$(".button05").show(0);
			$(".button06").show(0);
			$(".button07").show(0);
			$nextBtn.hide(0);
		}
		
		
		
		
/*----------------------------------------------------------------------------------------------------*/
		
		
	

		// first func call
		first();
		$(".button1").click(function(){
			$(".button1").addClass("incorrect");
			});
			
		$(".button2").click(function(){
			$(".button2").addClass("incorrect");
			});
			
		$(".button3").click(function(){
			$(".button3").addClass("correct");
			$nextBtn.show(0);
			});
			
		$(".button4").click(function(){
			$(".button4").addClass("incorrect");
			});
			
		$(".button5").click(function(){
			$(".button5").addClass("incorrect");
			});
			
		$(".button6").click(function(){
			$(".button6").addClass("incorrect");
			});
			
		$(".button7").click(function(){
			$(".button7").addClass("incorrect");
			});
			
		$(".wrapperIntro.firstPage .text4").click(function(){
			$(".wrapperIntro.firstPage .text3").hide(0);
			$(".wrapperIntro.firstPage .text4").hide(0);
			$(".wrapperIntro.firstPage .text5").show(0);
			$(".wrapperIntro.firstPage .text6").show(0);
			$nextBtn.show(0);
		});
		
		
		

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				//first02,
				first03,
				first04,
				
				
				
				second,
				//second01,
				second02,
				second03,
				
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

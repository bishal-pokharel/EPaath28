/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	

//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=8;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p4_18,
		data.string.p4_19,
		data.string.p4_20,
		data.string.p4_21,
		data.string.p4_22,
		
		],
	},
	
	
	{
		
		justClass : "second",
		animate : "true",
		text : [
		data.string.p4_23,
		data.string.p4_24,
		data.string.p4_25,
		data.string.p4_26,
		data.string.p4_27,
		data.string.p4_28,
		data.string.p4_29,
		data.string.p4_30,
		data.string.p4_31,
		data.string.p4_32,
		data.string.p4_33,
		data.string.p4_34,
		data.string.p4_35,
		
		],
	},
	
	
	/*{
		
		justClass : "third",
		animate : "true",
		text : [
		//data.string.p5_12_1,
		//data.string.p5_12_2,
		data.string.p5_11,
		data.string.p5_13,
		data.string.p5_14,
		data.string.p5_15,
		data.string.p5_16,
		data.string.p5_17,
		data.string.p5_18

		],
	},
	
	
	{
		
		justClass : "fourth",
		animate : "true",
		text : [
		
		data.string.p5_13,
		data.string.p5_19,
		data.string.p5_20,
		//data.string.p5_21,
		data.string.p5_22,
		data.string.p5_23,
		data.string.p5_24
		

		],
	},*/


	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			setTimeout(function(){
                $board.find(".wrapperIntro.firstPage .text0").show(0);
                $nextBtn.show()
			},1000);

		};

		 function first02() {
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			
		 };

		 function first03() {
			 $board.find(".wrapperIntro.firstPage .text2").show(0);
			 	
		 };

		function first04() {
			
			$board.find(".wrapperIntro.firstPage .text3").show(0);
		};

		 function first05() {
			
			$board.find(".wrapperIntro.firstPage .text4").show(0);
		}
		 
		 
		 
		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);
			$nextBtn.show(0);
			
			$(".wrapperIntro.second .text3").click(function(){
				$(".wrapperIntro.second .text3").addClass('incorrect');
			});
			
			$(".wrapperIntro.second .text4").click(function(){
				$(".wrapperIntro.second .text4").addClass('incorrect');
				
			});
			
			$(".wrapperIntro.second .text5").click(function(){
				$(".wrapperIntro.second .text5").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.second .text6").click(function(){
				$(".wrapperIntro.second .text6").addClass('incorrect');
			});
			
			$(".wrapperIntro.second .text8").click(function(){
				$(".wrapperIntro.second .text8").addClass('correct');
				$nextBtn.show(0);
			});
			
			$(".wrapperIntro.second .text9").click(function(){
				$(".wrapperIntro.second .text9").addClass('incorrect');
			});
			
			$(".wrapperIntro.second .text10").click(function(){
				$(".wrapperIntro.second .text10").addClass('incorrect');
			});
			
			$(".wrapperIntro.second .text11").click(function(){
				$(".wrapperIntro.second .text11").addClass('incorrect');
			});
		
		
			
		};
		
		function second01(){
			$board.find(".wrapperIntro.second .text1").show(0);
		 	$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .text4").show(0);
			$board.find(".wrapperIntro.second .text5").show(0);
			$board.find(".wrapperIntro.second .text6").show(0);
			$nextBtn.hide(0);
		 	
		}
		
		function second02(){
			$(".wrapperIntro.second .text2").css("background-color", "#cccccc");
		 	$board.find(".wrapperIntro.second .text7").show(0);
			$board.find(".wrapperIntro.second .text8").show(0);
			$board.find(".wrapperIntro.second .text9").show(0);
			$board.find(".wrapperIntro.second .text10").show(0);
			$board.find(".wrapperIntro.second .text11").show(0);
			$nextBtn.hide(0);
		 	
		}
		
		function second03(){
			$(".wrapperIntro.second .text7").css("background-color", "#cccccc");
			$board.find(".wrapperIntro.second .text12").show(0);
		 	
		}
		
		
		
		

		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");
		
/*----------------------------------------------------------------------------------------------------*/
		

		// first func call
		first();
		
		
		
		


	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
			
			if(countNext==9){
			$nextBtn.hide(0);
		}
			console.log(countNext);
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			
			
			
			fnSwitcher();
			
			
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
			    first03,
			    first04,
			    first05,
				//first06,
				//first07
				
				second,
				second01,
				second02,
				second03,
				
				
			];
			

			fnArray[countNext]();
			
			/*if(countNext==8){
				$(".text4").show(0);
			}*/
			
			
		
		/*$(".wrapperIntro.second .text4").click(function(){
			//$(".table_diy_ans").show(0);
			//$(".table_diy").hide(0);
			$nextBtn.show(0);
		});*/

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

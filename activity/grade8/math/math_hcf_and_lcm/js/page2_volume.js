/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
	
	$(function () {

//------------------------------------------------------------------------------------------------//	
	

	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=10;
		
	/*to register a partial*/	
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,2,7]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "onlyparatextstyle",
				textdata          : data.string.p1_19
			}
		],
		text : [
		
		
		data.string.p1_2,
		data.string.p1_3,
		data.string.p1_4,
		data.string.p1_5,
		data.string.p1_6,
		data.string.p1_7,
		data.string.p1_8,
		data.string.p1_9
		
	
		],
		
	},
	
	 {
		
		justClass : "second",
		animate : "true",
		text : [
		data.string.p1_10,
		data.string.p1_11,
		data.string.p1_12,
		data.string.p1_13,
		data.string.p1_14,
		data.string.p1_15,
		data.string.p1_16,
		data.string.p1_17,
		data.string.p1_18,
		data.string.p1_18_1,
		data.string.p1_18_9
		

		],
	},
	
	{
		
		justClass : "third",
		animate : "true",
		text : [
		data.string.p1_10,
		data.string.p1_18_9,
		data.string.p1_18_3,
		data.string.p1_18_4,
		data.string.p1_18_5,
		data.string.p1_18_6,
		data.string.p1_18_7,
		data.string.p1_18_8
		

		],
	},
	
		{
		
		justClass : "fourth",
		animate : "true",
		text : [
		data.string.p1_2,
		data.string.p1_3,
		data.string.p1_4,
		data.string.p1_18_2,
		data.string.p1_5,
		data.string.p1_6,
		data.string.p1_7,
		data.string.p1_8,
		data.string.p1_9,
		data.string.p1_18_3,

		],
	},
	
	{
		
		justClass : "fifth",
		animate : "true",
		/*text : [
		
		data.string.p1_19,
		
		],*/
		
		
		
	
	},
	]



/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$nextBtn.show()
            $board.find(".wrapperIntro.firstPage .text0").delay(500).fadeIn();
            $board.find(".wrapperIntro.firstPage .text1").delay(1000).fadeIn();
            $board.find(".wrapperIntro.firstPage .text2").delay(1500).fadeIn();
            $(".textblock").show(0);
		}
		
		function first01() {
			$(".textblock").hide(0);
            $nextBtn.hide(0);
			$board.find(".wrapperIntro.firstPage .text3").delay(1000).fadeIn();
			$(".arrow2").delay(1500).fadeIn();
			$board.find(".wrapperIntro.firstPage .text5").delay(2000).fadeIn();
			$(".arrow1").delay(2500).fadeIn();
			$board.find(".wrapperIntro.firstPage .text4").delay(3000).fadeIn();
			$(".arrow").delay(3500).fadeIn();
			$board.find(".wrapperIntro.firstPage .text6").delay(4000).fadeIn();
			$(".arrow3").delay(4500).fadeIn();
			$board.find(".wrapperIntro.firstPage .text7").delay(5000).fadeIn();
            $nextBtn.delay(5500).fadeIn();
		};



		function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			//$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.second .text10").show(0);
			
		};
	

		 function second02 () {
		 	
		 	$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .text4").show(0);
			
		 }

		 function second03 () {
		 	$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").hide(0);
			$board.find(".wrapperIntro.second .text5").show(0);
			$board.find(".wrapperIntro.second .text6").show(0);
		 
		 }
		 
		 function second04(){
		 	$(".three").addClass("grey01");
			$(".four").addClass("grey01");
			$(".six").addClass("grey01");
			$(".seven").addClass("grey01");
			$(".eight").addClass("grey01");
			$(".nine").addClass("grey01");
		 	$board.find(".wrapperIntro.second .text8").show(0);
		 }
		 
		 function second05(){
		 	$board.find(".wrapperIntro.second .text9").show(0);
		 	
		 }
		 
		 
		 
		 
		 function third() {

			var source = $("#intro03-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third .text1").show(0);
			
			
		};
	

		 function third02 () {
		 	
		 	
			$board.find(".wrapperIntro.third .text2").show(0);
			$board.find(".wrapperIntro.third .text3").show(0);
			$board.find(".wrapperIntro.third .text4").show(0);
			$board.find(".wrapperIntro.third .text5").show(0);
			
		 }

		 function third03 () {
		 	
			$board.find(".wrapperIntro.third .text6").show(0);
		 }
		 
		 function third04 () {
		 	
			$board.find(".wrapperIntro.third .text7").show(0);
		 }
		 
		 
		 // fourth
		 function fourth() {

			var source = $("#intro04-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$(".nextBtn.mynextStyle").hide(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);
			$board.find(".wrapperIntro.fourth .text1").show(0);
			$board.find(".wrapperIntro.fourth .text2").show(0);
			$board.find(".wrapperIntro.fourth .text3").delay(1000).fadeIn();
			$board.find(".wrapperIntro.fourth .text9").delay(2000).fadeIn();
			$board.find(".wrapperIntro.fourth .text4").show(0);
			$(".wrapperIntro.fourth .arrow2").show(0);
			$board.find(".wrapperIntro.fourth .text6").show(0);;
			$(".wrapperIntro.fourth .arrow1").show(0);
			$board.find(".wrapperIntro.fourth .text5").show(0);
			$(".wrapperIntro.fourth .arrow").show(0);
			$board.find(".wrapperIntro.fourth .text7").show(0);
			$(".wrapperIntro.fourth .arrow3").show(0);
			$board.find(".wrapperIntro.fourth .text8").show(0);
			$(".nextBtn.mynextStyle").delay(1500).fadeIn();
		};
		
		
		
		
		
		
		/*function fifth() {

			var source = $("#intro05-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			
			//$board.find(".wrapperIntro.fifth .text0").show(0);
		};*/

		 

		 
		 
		
		
/*----------------------------------------------------------------------------------------------------*/
		



		// first func call

		first();
		
		
		
			

	/*click functions*/
		$nextBtn.on('click',function () {
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first, 
				first01,				
				


				second,
				second02,
				second03,
				second04,
				second05,
				//second06,
				
				third,
				third02,
				third03,
				third04,
		
				fourth
				
				//fifth
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				
			}
		}
	

});

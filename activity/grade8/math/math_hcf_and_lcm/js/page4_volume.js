/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	

//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=17;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,6,11]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		hcf: data.string.p1_31,
		lcm: data.string.p1_33,
		text : [
		
		
		data.string.p1_31,
		data.string.p1_32,
		data.string.p1_33,
		data.string.p1_31,
		data.string.p1_32_1,
		data.string.p1_33_1,
		//data.string.p1_34,
		data.string.p1_35,
		data.string.p1_36,
		data.string.p1_37,
		data.string.p1_38,
		data.string.p1_39,
		data.string.p1_40,
		data.string.p1_41,
		data.string.p1_42				
		
		],
		
	},

	{
		
		justClass : "second",
		animate : "true",
		text : [
		data.string.p1_32_1,
		data.string.p1_33_1,
		data.string.p1_33,
		data.string.p4_2,
		data.string.p4_3,
		data.string.p4_4,
		data.string.p4_5,
		data.string.p4_6,
		data.string.p4_7,
		data.string.p4_8
		

		],
	},
	
	
	{
		
		justClass : "third",
		animate : "true",
		fac: data.string.p4_17,
		text : [
		data.string.p4_9,
		data.string.p4_10,
		data.string.p4_11,
		data.string.p4_12,
		data.string.p4_13,
		data.string.p4_13_1,
		data.string.p4_14,
		data.string.p4_15,
		data.string.p4_15_1,
		data.string.p4_16,
		data.string.p4_16_1,
		//data.string.p4_17
		

		],
	},
	
	
	{
		
		justClass : "fourth",
		animate : "true",
		text : [
		
		
		],
	},
	
	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".hcf").show(0);
			$(".lcm").show(0);
			$nextBtn.hide(0);
			setTimeout(function(){

			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".hcf" ).animate({ "height": "+=50%" }, {
					duration: 2000, 
				}).show();
                $board.find(".wrapperIntro.firstPage .text0").hide(0);
				$board.find(".wrapperIntro.firstPage .text1").hide(0);
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".lcm" ).animate({ "height": "+=50%" }, {
					duration: 2000, 
				}).show();
				$board.find(".wrapperIntro.firstPage .text1").hide(0);
				$board.find(".wrapperIntro.firstPage .text2").hide(0);
				
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".hcf" ).animate({ "height": "-=30%", "top": "+=30%" }, {
					duration: 2000, 
				}).show();
				$board.find(".wrapperIntro.firstPage .text0").hide(0);
				$board.find(".wrapperIntro.firstPage .text1").hide(0);
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".lcm" ).animate({ "height": "-=30%", "bottom": "+=30%" }, {
					duration: 2000, 
				}).show();
				$board.find(".wrapperIntro.firstPage .text1").hide(0);
				$board.find(".wrapperIntro.firstPage .text2").hide(0);
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".hcf" ).animate({ "height": "-=20%", "top": "+=15%" }, {
					duration: 2000, 
				}).show();
				
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".lcm" ).animate({ "height": "-=20%", "bottom": "+=15%" }, {
					duration: 2000, 
				}).show();
				
			});
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".hcf" ).animate({ "top": "-=45%" }, {
					duration: 2000, 
				}).show();
				$board.find(".wrapperIntro.firstPage .text1").delay(8000).fadeIn();
				
			});
			
			
			$board.find(".wrapperIntro.firstPage").find(function() {
				$( ".lcm" ).animate({ "bottom": "+=45%" }, {
					duration: 2000, 
				}).show();
				$nextBtn.delay(8500).fadeIn();
			});

            },1000);
			
			
		};

		 function first02() {
			$(".hcf").hide(0);
			$(".lcm").hide(0);
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
		 	
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			$board.find(".wrapperIntro.firstPage .text8").show(0);
			$board.find(".wrapperIntro.firstPage .text9").show(0);
		    
			
		 };

		/*function first03() {
			
		 	
		 };*/
		 
		 function first04() {
			$board.find(".wrapperIntro.firstPage .text10").show(0);
			$(".one").addClass("ones");
			$(".two").addClass("twos");
			$(".four").addClass("fours");
			$(".one1").addClass("onee");
			$(".two2").addClass("twoo");
			$(".four4").addClass("fourr");
		};
		 
		 function first05() {
			 $(".one").removeClass("ones");
			 $(".two").removeClass("twos");
			 $(".one1").removeClass("onee");
			 $(".two2").removeClass("twoo");
		 	$board.find(".wrapperIntro.firstPage .text11").show(0);	
		 };
		
		
		function first06() {
			
		 	$board.find(".wrapperIntro.firstPage .text12").show(0);	
		 };
		 
		 function first07() {
			 
		 	$board.find(".wrapperIntro.firstPage .text13").show(0);	
		 };
		 
		 

		function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);	
			$board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);
			$board.find(".wrapperIntro.second .text4").show(0);	
			$board.find(".wrapperIntro.second .text5").show(0);
			$board.find(".wrapperIntro.second .text6").show(0);	
		};

		/*function second02() {

		 	
		    
		 };*/
		 
		 function second03() {
			$board.find(".wrapperIntro.second .text7").show(0);
			$(".circle1").addClass("eighty");
			$(".circle2").addClass("eighty01");
			$(".circle3").addClass("sixty");
			$(".circle4").addClass("sixty01");
		};
		 
		 function second04() {
			$board.find(".wrapperIntro.second .text8").show(0);
			$(".circle3").removeClass("sixty");
			$(".circle4").removeClass("sixty01");
		 }
		 
		 function second05() {
			$board.find(".wrapperIntro.second .text9").show(0);
		 	
		 }
		 
		 
		 
		 function third() {

			var source = $("#intro03-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			
			
			$board.find(".wrapperIntro.third .text0").show(0);	
			$board.find(".wrapperIntro.third .text1").show(0);
			$board.find(".wrapperIntro.third .text2").show(0);
		};
		
		
		function third02() {

		 	$board.find(".wrapperIntro.third .text3").show(0);
			$board.find(".wrapperIntro.third .text4").show(0);	
		 };
		 
		 function third03() {
	
			$board.find(".wrapperIntro.third .text5").show(0);
			$(".four").addClass("four1");
			$(".four01").addClass("four2");			
		 };
		 
		 function third04() {
			$board.find(".wrapperIntro.third .text6").show(0);
			$board.find(".wrapperIntro.third .text7").show(0);
		};
		 
		 function third05() {
			$board.find(".wrapperIntro.third .text8").show(0);
			$(".circle03").addClass("circle003");
			$(".circle04").addClass("circle004");
		 }
		 
		 function third06() {
			$board.find(".wrapperIntro.third .text9").show(0);
		 	
		 }
		 
		 function third07() {
			$board.find(".wrapperIntro.third .text10").show(0);
		 	
		 }
		 
		 function third08() {
			
		 	$(".backhcf").show(0);
			$(".backlcm").show(0);
			$(".nextBtn.mynextStyle").hide(0);
			
			
			$board.find(".wrapperIntro.third").find(function() {
				$( ".backhcf" ).animate({ "height": "+=100%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.third").find(function() {
				$( ".backlcm" ).animate({ "height": "+=100%" }, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.third").find(function() {
				$( ".backhcf" ).animate({ "width": "+=50%", "opacity": "-=0.2"}, {
					duration: 2000, 
				});	
			});
			
			$board.find(".wrapperIntro.third").find(function() {
				$( ".backlcm" ).animate({ "width": "+=50%", "opacity": "-=0.8", "left":"-=50%"}, {
					duration: 2000, 
				});
				$(".fac").delay(4000).fadeIn();
				ole.footerNotificationHandler.pageEndSetNotification();
			});
			
			/*$board.find(".wrapperIntro.third").find(function() {
				$( ".fac" ).animate({ "width": "+=50%", "left": "-=50%"}, {
					duration: 2000, 
				});
			});*/
		 }
		 

		 
		 /*function fourth() {

			var source = $("#intro04-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.fourth .text0").show(0);	
			
		};*/
		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");
		
/*----------------------------------------------------------------------------------------------------*/
		

		
		
		
	

		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

		
		
		



	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
			console.log(countNext);
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
				console.log(countNext);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}
		
		

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
				//first03,
				first04,
				first05,
				first06,
				first07,

				second,
				//second02,
				second03,
				second04,
				second05,
				
				third,
				third02,
				third03,
				third04,
				third05,
				third06, 
				third07,
				third08
				
				//fourth
			];
			

			fnArray[countNext]();
			
			$(".wrapperIntro.second .text16").on("click", function(){
			$(".wrapperIntro.second .text15").hide(0);
			$(".wrapperIntro.second .text16").hide(0);
		});

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

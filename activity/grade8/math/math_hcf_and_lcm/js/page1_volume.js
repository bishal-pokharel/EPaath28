/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=0;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p1_1
		
		],
		
	},
	
	{
		justClass : "second",
		animate : "true",
		text : [
		
		
		
		],
		
	},
	
	{
		justClass : "third",
		animate : "true",
		text : [
		
		
		
		],
		
	},

	]
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			ole.footerNotificationHandler.pageEndSetNotification();
			$(".nextBtn.mynextStyle").hide(0);
		};
		
		
		/* function first01 () {
		 	$board.find(".wrapperIntro.firstPage .text1").show(0);
			
		}
		
		 function first02 () {
		 	$board.find(".wrapperIntro.firstPage .text2").show(0);
			
		 }
		
		
		 function first03 () {
		 	$board.find(".wrapperIntro.firstPage .text3").show(0);
		 	}
			
		
		 function first04 () {
		 	$board.find(".wrapperIntro.firstPage .text4").show(0);
		 	}
			
		
		 function first05 () {
		 $board.find(".wrapperIntro.firstPage .text5").show(0);
			}*/
			
	
/*----------------------------------------------------------------------------------------------------*/
		



		// first func call

		first();
		

	/*click functions*/
		$nextBtn.on('click',function () {
			/*$prevBtn.show(0);
			countNext++;
			fnSwitcher();*/
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  
				/*first01,
				first02,
				first03,
				first04,
				first05,*/
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				
			} else {
				
			}
		}
	

});

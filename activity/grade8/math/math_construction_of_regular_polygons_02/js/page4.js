var whatPopId=0;
var whatdegree=0;
var innerCount=1;
function imgshow(){ 
			$(".img5").hide(0);
			$(".img6").hide(0);
			$("#activityNextBtn").hide(0);
			$(".img4").hide(0);
			$(".img1").show(0);
			$(".img1").delay(3000).hide(0);
			$(".img2").delay(3000).show(0).delay(3000).hide(0);
			$(".img3").delay(6000).show(0).delay(3000).hide(0);
			$(".img4").delay(9000).show(0).delay(3000).hide(0);;
			
			$(".img6").delay(12000).show(0);
			
			$("#closeImgbtn").show(0);
			$(".img5").delay(13000).show(0);
		
	}
	

$(function(){
	
	
	


	
	
	
	$(".headTitle").html(data.string.p4_0);
	loadTimelineProgress(1,1);

	$(".p4_1_0").html(data.string.p4_1_0);


	$("#angle_60 div").html(data.string.p4_1);
	$("#angle_60 img").attr('src', $ref+'/images/page4/heptagon.png');
	$("#angle_30 div").html(data.string.p4_2);
	$("#angle_30 img").attr('src', $ref+'/images/page4/nonagon.png');
	$("#angle_15 div").html(data.string.p4_3);
	$("#angle_15 img").attr('src', $ref+'/images/page4/decagon.png');
	$("#angle_90 div").html(data.string.p4_4);
	$("#angle_90 img").attr('src', $ref+'/images/page4/hendecagon.png');
	$("#angle_45 div").html(data.string.p4_5);
	$("#angle_45 img").attr('src', $ref+'/images/page4/dodecagon.png');
	$("#angle_135 div").html(data.string.p4_6);
	$("#angle_135 img").attr('src', $ref+'/images/page4/square.png');


	$(".popBtn").click(function(){

		var id=$(this).attr('id');

		getHtml(id);
			$(".img5").click(function(){
		console.log("test");
		imgshow();
	});
	
		
		setTimeout(

		imgshow, 2000);

	});
	
	


	$(".popUp").on('click','#closeImgbtn',function(){	

		$(".popUp").fadeOut(100,function(){
			$(this).html('');
			innerCount=1;
		});
	});

	$(".popUp").on('click','#nextbtn',function(){
		
		$(this).hide(0);
		$("#activity-page-prev-btn-enabled").hide(0);

		innerCount++;
		
		afterNext(innerCount);

		
		
	});

	$(".popUp").on('click','#prevbtn',function(){
		
		$(this).hide(0);
		$("#nextbtn").hide(0);

		innerCount--;
		
		afterNext(innerCount);
	
	});
	
	
	
	setTimeout(function(){
		
		
	
	$("#angle_135").click(function(){
		
	ole.footerNotificationHandler.lessonEndSetNotification();
	
	
	});
	
	},2000);
	

});

function afterNext(innerCount)
{
	var ids="angle_"+whatdegree;

	$(".innerDesc").html(data.string["p4_"+whatPopId+"_"+innerCount]);


	if(innerCount!=5)
	{
		$(".lastBx").fadeOut(10);
		$('#video1').show(0);
		$( "#protractor" ).removeAttr('style');
		var tv_main_channel=$('#vdo1');
		var tv_main_channel2=$('#vdo2');

		var vdo_source_webm=$ref+"/video/"+ids+"/"+ids+"_"+innerCount+".webm";
		var vdo_source_mp4=$ref+"/video/"+ids+"/"+ids+"_"+innerCount+".mp4";

		tv_main_channel.attr('src', vdo_source_webm);
		tv_main_channel2.attr('src', vdo_source_mp4);

		var video_block = $('#video1');
		video_block.load();
		

	}
	else
	{
		$('#video1').hide(0);

		


		$(".lastBx").fadeIn(10,function(){
			
			var height=parseInt($("#elementinpop").innerHeight());
			var height2=parseInt($("#mylastimg").height());
			var newtop=((height)-height2)/2;
			$(".lastBx").css('top',newtop+"px");
		});
		$( "#protractor" ).draggable({ containment: "#elementinpop", scroll: false });
	}
	
	
	setTimeout(function(){
		if(innerCount==5)
		{

			$("#closeImgbtn").show(0);
			$("#activity-page-prev-btn-enabled").show(0);
			ole.footerNotificationHandler.lessonEndSetNotification();

		} 
		else
		{
			$("#nextbtn").show(0);
			if(innerCount!=1)$("#activity-page-prev-btn-enabled").show(0);
		}


	},2000);
}

function getdata(ids)
{
	var datas;
	var nextbtn=getSubpageMoveButton($lang,"next");
	var prevbtn=getSubpageMoveButton($lang,"prev");
	var clsbtn=getCloseBtn();
	
	
	

	switch(ids)
	{
		case "angle_60":
		{
			whatPopId=1;
			whatdegree=60;
			datas={
				title:data.string.p4_1,
				outtertextdesc:data.string.p4_1_1_1,
				textdesc:data.string.p4_1_1,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/heptagon/01.png",
				images2:$ref+"/images/page4/heptagon/02.png",
				images3:$ref+"/images/page4/heptagon/03.png",
				images4:$ref+"/images/page4/heptagon/04.png",
				images6:$ref+"/images/page4/heptagon/05.png",
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
			
			
			
		
			
		}
		case "angle_30":
		{
			whatPopId=2;
			whatdegree=30;
			datas={
				title:data.string.p4_2,
				outtertextdesc:data.string.p4_1_1_2,
				textdesc:data.string.p4_1_2,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/nonagon/01.png",
				images2:$ref+"/images/page4/nonagon/02.png",
				images3:$ref+"/images/page4/nonagon/03.png",
				images4:$ref+"/images/page4/nonagon/04.png",
				images6:$ref+"/images/page4/nonagon/05.png",
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
		}
		case "angle_15":
		{
			whatPopId=3;
			whatdegree=15;
			datas={
				title:data.string.p4_3,
				outtertextdesc:data.string.p4_1_1_3,
				textdesc:data.string.p4_1_3,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/decagon/01.png",
				images2:$ref+"/images/page4/decagon/02.png",
				images3:$ref+"/images/page4/decagon/03.png",
				images4:$ref+"/images/page4/decagon/04.png",
				images6:$ref+"/images/page4/decagon/05.png",
				
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
		}
		case "angle_90":
		{
			whatPopId=4;
			whatdegree=90;
			datas={
				title:data.string.p4_4,
				outtertextdesc:data.string.p4_1_1_4,
				textdesc:data.string.p4_1_4,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/hendecagon/01.png",
				images2:$ref+"/images/page4/hendecagon/02.png",
				images3:$ref+"/images/page4/hendecagon/03.png",
				images4:$ref+"/images/page4/hendecagon/04.png",
				images6:$ref+"/images/page4/hendecagon/05.png",
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
		}
		case "angle_45":
		{
			whatPopId=5;
			whatdegree=45;
			datas={
				title:data.string.p4_5,
				outtertextdesc:data.string.p4_1_1_5,
				textdesc:data.string.p4_1_5,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/dodecagon/01.png",
				images2:$ref+"/images/page4/dodecagon/02.png",
				images3:$ref+"/images/page4/dodecagon/03.png",
				images4:$ref+"/images/page4/dodecagon/04.png",
				images6:$ref+"/images/page4/dodecagon/05.png",
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
		}
		case "angle_135":
		{
			whatPopId=6;
			whatdegree=135;
			datas={
				title:data.string.p4_6,
				outtertextdesc:data.string.p4_1_1_6,
				textdesc:data.string.p4_1_6,
				vdeosrc:$ref+"/video/"+ids+"/"+ids+"_1.webm",
				vdeosrc2:$ref+"/video/"+ids+"/"+ids+"_1.mp4",
				images1:$ref+"/images/page4/square/01.png",
				images2:$ref+"/images/page4/square/02.png",
				images3:$ref+"/images/page4/square/03.png",
				images4:$ref+"/images/page4/square/04.png",
				images6:$ref+"/images/page4/square/05.png",
				lastimg:$ref+"/images/page4/"+ids+"A_5.png",
				imgpro:$ref+"/exercise/images/p1.png",
				closeImgSrc:clsbtn,
				imgreplay:$ref+"/images/page1/replay.png",
				nextImgSrc:nextbtn,
				prevImgSrc:prevbtn
			}	
			break;
		}
	}

	return datas;

}
function getHtml(ids)
{
	var datas=getdata(ids);
	var source=$("#template-1").html();
	var template=Handlebars.compile(source);
	var html=template(datas);

	$(".popUp").fadeOut(10,function(){

		$(this).html(html);



	}).delay(10).fadeIn(10,function(){

		var height=parseInt($("#elementinpop").innerHeight());
		var height2=parseInt($("#video1").innerHeight());
		/*var newtop=(height-height2)/2;
		$("#video1").css('top',newtop+"px");*/

		setTimeout(function(){
			$("#nextbtn").show(0);
		},2000)
	});


}
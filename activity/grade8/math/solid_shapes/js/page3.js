var imgpath = $ref + "/images/";

var content = [
{
	//page 0
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s1
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s2,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "pyramid3_open",
	}
	]

},
{
	//page 1
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s3
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s4,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "pyramid2_open",
	}
	]
},
{
	//page 2
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s5
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s6,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "pyramid1_open",
	}
	]
},
{
	//page 3
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s7
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s8,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "triangular_pyramid_f_e_v",
	}
	],
	lowertextblockadditionalclass: "ltb",
	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p3_faces,
	},{
		textclass : "edges edges1",
		textdata : data.string.p3_vertices,
	},{
		textclass : "vertices vertices1",
		textdata : data.string.p3_edges,
	},{
		textclass : "counter",
		textdata : "0",
	}
	]
},
{
	
	//page 4
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s9
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s10,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "square_pyramid_f_e_v",
	}
	],
	lowertextblockadditionalclass: "ltb",
	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p3_faces,
	},{
		textclass : "edges edges1",
		textdata : data.string.p3_vertices,
	},{
		textclass : "vertices vertices1",
		textdata : data.string.p3_edges,
	},{
		textclass : "counter",
		textdata : "0",
	}
	]
},
{
	//page 5
	// contentblockadditionalclass : " background_image1",
	contentblockadditionalclass: "ole-background-gradient-barley",
	uppertextblock : [{
		textclass : "introductionheader",
		textdata : data.string.p3_s11
	},{
		textclass : "question",
		datahighlightflag : true,
		textdata : data.string.p3_s12,
	}],
	imageblockadditionalclass : "shapequestion",
	imageblock: [
	{
		imageblockclass: "hexpyramid_pyramid_f_e_v",
	}
	],
	lowertextblockadditionalclass: "ltb",
	lowertextblock: [{
		textclass : "surfaces surfaces1",
		textdata : data.string.p3_faces,
	},{
		textclass : "edges edges1",
		textdata : data.string.p3_vertices,
	},{
		textclass : "vertices vertices1",
		textdata : data.string.p3_edges,
	},{
		textclass : "counter",
		textdata : "0",
	}
	]
}
];

$(function() {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page, countNext + 1);

	var cantconsumeclick = false;
	/*==================================================
	 =            Handlers and helpers Block            =
	 ==================================================*/
	/*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	/*===============================================
	=            data highlight function            =
	===============================================*/
	/**

	 What it does:
	 - send an element where the function has to see
	 for data to highlight
	 - this function searches for all nodes whose
	 data-highlight element is set to true
	 -searches for # character and gives a start tag
	 ;span tag here, also for @ character and replaces with
	 end tag of the respective
	 - if provided with data-highlightcustomclass value for highlight it
	 applies the custom class or else uses parsedstring class

	 E.g: caller : texthighlight($board);
	 */
	function texthighlight($highlightinside) {
		//check if $highlightinside is provided
		typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

		var $alltextpara = $highlightinside.find("*[data-highlight='true']");
		var stylerulename;
		var replaceinstring;
		var texthighlightstarttag;
		var texthighlightendtag = "</span>";
		if ($alltextpara.length > 0) {
			$.each($alltextpara, function(index, val) {
				/*if there is a data-highlightcustomclass attribute defined for the text element
				 use that or else use default 'parsedstring'*/
				$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
				( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

				texthighlightstarttag = "<span class='" + stylerulename + "'>";
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}

	/*=====  End of data highlight function  ======*/

	/*===============================================
	=            user notification function        =
	===============================================*/
	/**
	 How to:
	 - First set any html element with
	 "data-usernotification='notifyuser'" attribute,
	 and "data-isclicked = ''".
	 - Then call this function to give notification
	 */

	/**
	 What it does:
	 - You send an element where the function has to see
	 for data to notify user
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function notifyuser($notifyinside) {
		//check if $notifyinside is provided
		typeof $notifyinside !== "object" ? alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") : null;

		/*variable that will store the element(s) to remove notification from*/
		var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
		// if there are any notifications removal required add the event handler
		if ($allnotifications.length > 0) {
			$allnotifications.one('click', function() {
				/* Act on the event */
				$(this).attr('data-isclicked', 'clicked');
				$(this).removeAttr('data-usernotification');
			});
		}
	}

	/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	/**
	 How To:
	 - Just call the navigation controller if it is to be called from except the
	 last page of lesson
	 - If called from last page set the islastpageflag to true such that
	 footernotification is called for continue button to navigate to exercise
	 */

	/**
	 What it does:
	 - If not explicitly overriden the method for navigation button
	 controls, it shows the navigation buttons as required,
	 according to the total count of pages and the countNext variable
	 - If for a general use it can be called from the templatecaller
	 function
	 - Can be put anywhere in the template function as per the need, if
	 so should be taken out from the templatecaller function
	 - If the total page number is
	 */

	function navigationcontroller(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;

		if (countNext == 0 && $total_page != 1) {
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		} else if ($total_page == 1) {
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		} else if (countNext > 0 && countNext < $total_page - 1) {
		 	$nextBtn.show(0);
			$prevBtn.show(0);
		} else if (countNext == $total_page - 1) {
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
	/**
	 How to:
	 - Just call instructionblockcontroller() from the template
	 */

	/**
	 What it does:
	 - It inserts and handles closing and opening of instruction block
	 - this function searches for all text nodes whose
	 data-usernotification attribute is set to notifyuser
	 - applies event handler for each of the html element which
	 removes the notification style.
	 */
	function instructionblockcontroller() {
		var $instructionblock = $board.find("div.instructionblock");
		if ($instructionblock.length > 0) {
			var $contentblock = $board.find("div.contentblock");
			var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
			var instructionblockisvisibleflag;

			$contentblock.css('pointer-events', 'none');

			$toggleinstructionblockbutton.on('click', function() {
				instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
				if (instructionblockisvisibleflag == 'true') {
					instructionblockisvisibleflag = 'false';
					$contentblock.css('pointer-events', 'auto');
				} else if (instructionblockisvisibleflag == 'false') {
					instructionblockisvisibleflag = 'true';
					$contentblock.css('pointer-events', 'none');
				}

				$instructionblock.attr("data-instructionblockshow", instructionblockisvisibleflag);
			});
		}
	}
	
	
	/*cubemovement movement controller*/
	
	function cuboidController() {
		var dragging = false;
		var rotationXprev = 0;//rotation from previous transform
		var rotationYprev = 0;//rotation from previous transform
		
		var degreechangeX = 0;//x degree change in current rotation
		var degreechangeY = 0;//y degree change in current rotation
		
		var mouse_0x = 0;//x-coordinate at mouse down 
		var mouse_0y = 0;//y-coordinate ar mouse down
		var additionCompleted = false;
		var target = $('.a3dbox');
		var halfheight_width_for_center = target.width() / 2;
		var offset = target.offset();
		
		var rotator;	
		var ygoingupto30 = true;
		var infiniteRotation = {
			start: function(){
				rotator = setInterval(function(){
					if(ygoingupto30){
						if (rotationYprev >= 25){
							ygoingupto30 = false;
						}
						(((25 - rotationYprev) > 0)? rotationYprev += 5 : rotationYprev -= 5);
					}else{
						(rotationYprev <= -25)? ygoingupto30 = true : rotationYprev -= 5;
					}
					
					rotationXprev += 5;
					
                    //TODO: Can we remove this now?
					// console.log(rotationYprev +" : x,   "+rotationXprev+"   :y !!!");
					target.css({
						'-moz-transform': 'translate(-50%, -50%) rotateX(' + rotationYprev + 'deg) rotateY(' + rotationXprev + 'deg)',
						'-moz-transform-origin': '50% 50%',
						'-moz-transition': '-moz-transform 1.5s linear',
						'-webkit-transform': 'translate(-50%, -50%) rotateX(' + rotationYprev + 'deg) rotateY(' + rotationXprev + 'deg)',
						'-webkit-transform-origin': '50% 50%',
						'-webkit-transition': '-webkit-transform 1.5s linear'
						});
				}, 1520);
			},
			stop: function(){
				clearInterval(rotator);
			}
		};
		infiniteRotation.stop();
		infiniteRotation.start();
		var clickedontarget = false;
		target.mousedown(function(e) {
			dragging = true;
			clickedontarget = true;
			mouse_0x = e.pageX;
			mouse_0y = e.pageY;
			infiniteRotation.stop();
			
		});

		$(document).mouseup(function() {
			if (dragging) {
				rotationXprev = degreechangeX;
				rotationYprev = degreechangeY;
			}
			degreechangeX = 0;
			degreechangeY = 0;
			dragging = false;
			additionCompleted = false;
			if(clickedontarget){
				setTimeout(function(){
					if(!dragging){
						infiniteRotation.start();
					}
				}, 1000);
				clickedontarget= false;
			}
			ygoingupto30 = false;
		});

		$(document).mousemove(function(e) {
			e.preventDefault();
			if (dragging) {

				var mouse_x = e.pageX;
				var mouse_y = e.pageY;

				if (Math.abs(mouse_0x - mouse_x) > 0.5) {
					((mouse_0x - mouse_x) < 0) ? ++degreechangeX : --degreechangeX;
					mouse_0x = mouse_x;
				}
				if (Math.abs(mouse_0y - mouse_y) > 0.5) {
					((mouse_0y - mouse_y) > 0) ? ++degreechangeY : --degreechangeY;
					mouse_0y = mouse_y;
				}
				if (rotationXprev != 0 && rotationYprev != 0 && !additionCompleted) {
					degreechangeX += rotationXprev;
					degreechangeY += rotationYprev;
					additionCompleted = true;
				}

				degreechangeY %= 360;
				degreechangeX %= 360;

				target.css({
						'-moz-transform': 'translate(-50%, -50%) rotateX(' + degreechangeY + 'deg) rotateY(' + degreechangeX + 'deg)',
						'-moz-transform-origin': '50% 50%',
						'-moz-transition': '',
						'-webkit-transform': 'translate(-50%, -50%) rotateX(' + degreechangeY + 'deg) rotateY(' + degreechangeX + 'deg)',
						'-webkit-transform-origin': '50% 50%',
						'-webkit-transition': ''
				});
			}
		});
	}
	
	/* cuboid movement controller end */

	/*=====  End of InstructionBlockController  ======*/

	/*=====  End of Handlers and helpers Block  ======*/

	/*=======================================
	 =            Templates Block            =
	 =======================================*/
	/*=================================================
	 =            general template function            =
	 =================================================*/
	var source = $("#general-template").html();
	var template = Handlebars.compile(source);
	var counter_interval;
	function generaltemplate() {
		
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if ($linehorizontal.length > 0) {
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
		switch(countNext){
			case 3:
				$nextBtn.hide(0);
				var click1 = false;
				var click2 = false;
				var click3 = false;
				var $triangular_pyramid_f_e_v = $(".triangular_pyramid_f_e_v");
				var consumeclick = false;
				var $counter = $(".counter");
				var count = 0;
				$(".surfaces").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click1 = true;
					$triangular_pyramid_f_e_v.removeClass("faces_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("edges_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("vertices_traingular_pyramid");
					$triangular_pyramid_f_e_v.addClass("faces_traingular_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 4500);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 4 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 800);
				});
				
				$(".edges").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click2 = true;
					$triangular_pyramid_f_e_v.removeClass("faces_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("edges_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("vertices_traingular_pyramid");
					$triangular_pyramid_f_e_v.addClass("edges_traingular_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 4500);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 4 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 800);
				});
				
				$(".vertices").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click3 = true;
					$triangular_pyramid_f_e_v.removeClass("faces_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("edges_traingular_pyramid");
					$triangular_pyramid_f_e_v.removeClass("vertices_traingular_pyramid");
					$triangular_pyramid_f_e_v.addClass("vertices_traingular_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 4500);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 6 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 600);
				});
				
				break;
			case 5:
				$nextBtn.hide(0);
				var click1 = false;
				var click2 = false;
				var click3 = false;
				var $hexpyramid_pyramid_f_e_v = $(".hexpyramid_pyramid_f_e_v");
				var consumeclick = false;
				var $counter = $(".counter");
				var count = 0;
				$(".surfaces").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click1 = true;
					$hexpyramid_pyramid_f_e_v.removeClass("faces_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("edges_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("vertices_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.addClass("faces_hexpyramid_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 5000);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 7 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 643);
				});
				
				$(".edges").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click2 = true;
					$hexpyramid_pyramid_f_e_v.removeClass("faces_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("edges_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("vertices_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.addClass("vertices_hexpyramid_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 5000);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 7 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 600);
				});
				
				$(".vertices").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click3 = true;
					$hexpyramid_pyramid_f_e_v.removeClass("faces_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("edges_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.removeClass("vertices_hexpyramid_pyramid");
					$hexpyramid_pyramid_f_e_v.addClass("edges_hexpyramid_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							$nextBtn.show(0);
						}
					}, 8500);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 12 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 500);
				});
				
				break;
			case 4:
				var click1 = false;
				var click2 = false;
				var click3 = false;
				var $square_pyramid_f_e_v= $(".square_pyramid_f_e_v");
				var consumeclick = false;
				var $counter = $(".counter");
				var count = 0;
				$(".surfaces").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click1 = true;
					$square_pyramid_f_e_v.removeClass("faces_square_pyramid");
					$square_pyramid_f_e_v.removeClass("edges_square_pyramid");
					$square_pyramid_f_e_v.removeClass("vertices_square_pyramid");
					$square_pyramid_f_e_v.addClass("faces_square_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					}, 5000);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 5 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 900);
					
				});
				
				$(".edges").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click2 = true;
					$square_pyramid_f_e_v.removeClass("faces_square_pyramid");
					$square_pyramid_f_e_v.removeClass("edges_square_pyramid");
					$square_pyramid_f_e_v.removeClass("vertices_square_pyramid");
					$square_pyramid_f_e_v.addClass("vertices_square_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					}, 5000);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 5 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 700);
					
				});
				
				$(".vertices").click(function(){
					if(consumeclick){
						return consumeclick;
					}
					consumeclick = true;
					click3 = true;
					$square_pyramid_f_e_v.removeClass("faces_square_pyramid");
					$square_pyramid_f_e_v.removeClass("edges_square_pyramid");
					$square_pyramid_f_e_v.removeClass("vertices_square_pyramid");
					$square_pyramid_f_e_v.addClass("edges_square_pyramid");
					setTimeout(function(){
						consumeclick = false;
						if(click1 && click2 && click3){
							ole.footerNotificationHandler.pageEndSetNotification();
						}
					}, 5000);
					$counter.html(++count);
					counter_interval = setInterval(function(){
						$counter.html(count++);
						if(count > 8 ){
							clearInterval(counter_interval);
							count = 0;
						}						
					}, 900);
					
				});
				
				break;
			default:
				break;
		}
		
	}

	/*=====  End of Templates Block  ======*/

	/*==================================================
	=            Templates Controller Block            =
	==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
	 Motivation :
	 - Make a single function call that handles all the
	 template load easier

	 How To:
	 - Update the template caller with the required templates
	 - Call template caller

	 What it does:
	 - According to value of the Global Variable countNext
	 the slide templates are updated
	 */

	function templateCaller() {
		/*always hide next and previous navigation button unless
		 explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page, countNext + 1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click', function() {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click', function() {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
		 previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

	/*=====  End of Templates Controller Block  ======*/
	
});

var imgpath = $ref+"/images/page5/";
var vdopath = $ref+"/videos/page5/";

var content=[


	
	//slide0 general-template
	
{
	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p3text0,
			},
		],
		
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "beakerstyle",
						imgsrc   : imgpath+"diy4.png",
						
					},
				],
				
					imagelabels :[
					{
						imagelabelclass : "diy",
						imagelabeldata  : data.string.p3text1,
					},
				
					
				],
			
			
			},
			
		],
		
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle short-des",
				textdata  : data.string.p3text2,
			}
		],
		
	
		
		
	},
	
	
	
	//slide9 svgtemplate10
	
		
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text1,
			},
		],
	
		svgblockadditionalclass:"svg-two",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p5text2,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text3,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text4,
					},
				
					
				],
			
			},
			
		],
		
		
			instructionblock:[
			{
				instruction : [
					data.string.p3text12,
					
				],
			}
		]
	
	},
	
	
	//slide9 svgtemplate9
	{
	hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text5,
			},
		],
		
					
		svgblockadditionalclass:"svg-two",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text2,
					},
					
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p5text3,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text4,
					},
				
				
				],
						
			
			},
			
		],
		
			
		
	},
	
	
	//slide1 svgtemplate
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text6,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text2,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text3,
					},
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p5text4,
					},
				
					
					
					
				],
			
			},
			
		],
		
		
		
		
	},
	
	//slide2 svgtemplate1
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text7,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
				],
			
			},
			
		],
		
			
		
	},
	
	//slide3 svgtemplate2
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text7,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
						{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
				],
			
			},
			
		],
		
			
	},
	
	
	//slide4 svgtemplate3
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text7,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
				],
			
			},
			
		],
		
			
	},
	
	//slide5 svgtemplate4
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text7,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.p5text13,
					},
					
					
				],
			
			},
			
		],
	
		
		
	},
	
	
	//slide6 svgtemplate5
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text8,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
				{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
					
				],
			
			},
			
		],
		
			
		
	},
	
	//slide7 svgtemplate6
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text8,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
					
				],
			
			},
			
		],
		
			
		
	}, 
	
	//slide8 svgtemplate7
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text8,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
				
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
					
				],
			
			},
			
		],
		
			
		
	}, 
	


//added later by dilak
	//slide9 svgtemplate10
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text8,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
				
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
					
				],
			
			},
			
		],
		
			
		
	}, 

	
		
	//rotation -90 start

	//slide10 svgtemplate11
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text9,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
				
					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
					
				],
			
			},
			
		],
		
			
		
	}, 


	//svgtemplate12
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text9,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
						{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
					
				],
			
			},
			
		],
		
			
	},
	
	//svgtemplate13
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text9,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p5text13,
					},
					
					
				],
			
			},
			
		],
		
			
	},
	//svgtemplate14
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p5text9,
			},
		],
		
	
		svgblockadditionalclass:"svg-one",	
		svgblock :[
				{ 
					svgname:"svgcontainer",
				}
			],
		
				
			imageblock : [
			{
				

				imagestoshow:[
					
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},
				
				],		
				
								
				imagelabels :[
					
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p5text10,
					},
					
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p5text11,
					},
					
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p5text12,
					},
					
					{
						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.p5text13,
					},
					
					
				],
			
			},
			
		],
	
		
		
	},
	
];


//for play and pause video
	function vid_play_pause(vdoname) {
		
	  var myVideo = document.getElementById(vdoname);
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }
	
	}

	

	
	 function restart() {

		var video1 = document.getElementById("myVideo1");
		var video2 = document.getElementById("myVideo2");
      
		video1.currentTime = 0;
		video2.currentTime = 0;
		if (video1.paused) {
		video1.play();
		}
		if (video2.paused) {
		video2.play();
		}
    }
	
	
	

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext =1;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
     Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 

		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true 
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class
				
				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;
					
				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {	
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/				
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	 
	 
	 
	 
	 
	  	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
	 
	 
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);
		
	
		
		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}


	/*=================================================
	=            svg template function            =
	=================================================*/		
	
function svgtemplate8(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate01.svg", function (f) {    
    		s.append(f);
		

    	 
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_4_" || idname==="incorrect_6_"){
				
				$(".wrong1").addClass("cssfadein");
			
			}
			if (idname==="correctpeny_1_") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				$("#correctpeny_1_").css("opacity","1");
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}


function svgtemplate9(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate01_1.svg", function (f) {    
    		s.append(f);
			

    	  	// snap objects
				
				
    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
		if (idname==="incorrect_4_" || idname==="incorrect_6_"){
				
				$(".wrong1").addClass("cssfadein");
			
			}
			if (idname==="correctpeny_1_") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				$("#correctpeny_1_").css("opacity","1");
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}

	
	
	
	function svgtemplate(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate01_2.svg", function (f) {    
    		s.append(f);
		

    	  	
				
				
    		
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_4_" || idname==="incorrect_6_"){
				
				$(".wrong1").addClass("cssfadein");
			
			}
			if (idname==="correctpeny_1_") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				$("#correctpeny_1_").css("opacity","1");
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}
	/*rotate parallalogram +90*/
	function svgtemplate1(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate03.svg", function (f) {    
    		s.append(f);
		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
		$(".que1").attr("class", "que1 blink");
		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct1") {
				$(".que1").attr("class", "que1");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt1").show(0);
				$(".corrtxt-x1").show(0);
				$("#correct10").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

			
 		});
	// }
}

	function svgtemplate2(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate03.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			
			
		$(".que2").attr("class", "que2 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct2") {
				$(".que2").attr("class", "que2");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt2").show(0);
				$(".corrtxt-x2").show(0);
				$("#correct20").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}


	function svgtemplate3(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate03.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			
			
			
		$(".que3").attr("class", "que3 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct3") {
				$(".que3").attr("class", "que3");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt3").show(0);
				$(".corrtxt-x3").show(0);
				$("#correct30").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}




	function svgtemplate4(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate03.svg", function (f) {    
    		s.append(f);
		
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".correctxt3").show(0);
			$("#correct30").css("opacity","1");
			$(".corrtxt-x3").show(0);
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
		$(".que4").attr("class", "que4 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct4") {
				$(".que4").attr("class", "que4");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt4").show(0);
				$(".corrtxt-x4").show(0);
				$("#correct40").css("opacity","1");
				$(".dottedlinex").show(0);
				$(".correctpara").css("opacity","1");
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}


function svgtemplate5(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate04.svg", function (f) {    
    		s.append(f);
		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
		
    		
		
    		    		$(".que1").attr("class", "que1 blink");

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct1") {
				$(".que1").attr("class", "que1");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt1").show(0);
				$(".corrtxt-x1").show(0);
				$("#correct10").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

			
 		});
	// }
}

function svgtemplate6(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate04.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			
			
    		    		
			$(".que2").attr("class", "que2 blink");
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct2") {
				$(".que2").attr("class", "que2");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt2").show(0);
				$(".corrtxt-x2").show(0);
				$("#correct20").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}


function svgtemplate7(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate04.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			
			
			
    		$(".que3").attr("class", "que3 blink");
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct3") {
				$(".que3").attr("class", "que3");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt3").show(0);
				$(".corrtxt-x3").show(0);
				$("#correct30").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}

function svgtemplate10(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate04.svg", function (f) {    
    		s.append(f);
		
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".correctxt3").show(0);
			$("#correct30").css("opacity","1");
			$(".corrtxt-x3").show(0);
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		$(".que4").attr("class", "que4 blink");
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct4") {
				$(".que4").attr("class", "que4");
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt4").show(0);
				$(".corrtxt-x4").show(0);
				$("#correct40").css("opacity","1");
				$(".dottedlinex").show(0);
				$(".correctpara").css("opacity","1");
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}

//for rotation -90
function svgtemplate11(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate05.svg", function (f) {    
    		s.append(f);
		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
		
    		
		
    		$(".que1").attr("class", "que1 blink");

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct1") {
				$(".que1").attr("class", "que1");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt1").show(0);
				$(".corrtxt-x1").show(0);
				$("#correct10").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

			
 		});


	// }
}

function svgtemplate12(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate05.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			
			
    		$(".que2").attr("class", "que2 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct2") {
				$(".que2").attr("class", "que2");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt2").show(0);
				$(".corrtxt-x2").show(0);
				$("#correct20").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}

	function svgtemplate13(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate05.svg", function (f) {    
    		s.append(f);
		
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			
			
			
    		$(".que3").attr("class", "que3 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct3") {
				$(".que3").attr("class", "que3");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt3").show(0);
				$(".corrtxt-x3").show(0);
				$("#correct30").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}



	function svgtemplate14(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"rotate05.svg", function (f) {    
    		s.append(f);
		
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".correctxt3").show(0);
			$("#correct30").css("opacity","1");
			$(".corrtxt-x3").show(0);
			

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		$(".que4").attr("class", "que4 blink");
    		
			$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect_8_"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct4") {
				$(".que4").attr("class", "que4");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				
				$(".correctxt4").show(0);
				$(".corrtxt-x4").show(0);
				$("#correct40").css("opacity","1");
				$(".dottedlinex").show(0);
				$(".correctpara").css("opacity","1");
			};
		});
		
			
			
 		});
	// }
}
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		

		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : svgtemplate8(); break;
			case 2 : svgtemplate9(); break;
			case 3 : svgtemplate(); break;
			
			case 4 : svgtemplate1(); break;
			case 5 : svgtemplate2(); break;
			case 6 : svgtemplate3(); break;
			case 7 : svgtemplate4(); break;
			
			case 8 : svgtemplate5(); break;
			case 9 : svgtemplate6(); break;
			case 10 : svgtemplate7(); break;
			case 11 : svgtemplate10(); break;
			
			case 12 : svgtemplate11(); break;
			case 13 : svgtemplate12(); break;
			case 14 : svgtemplate13(); break;
			case 15 : svgtemplate14(); break;
			
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
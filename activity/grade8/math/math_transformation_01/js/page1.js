var imgpath = $ref+"/images/page1/";
var vdopath = $ref+"/videos/page1/";

var content=[





	//slide0

	{
		contentnocenteradjust:true,
		contentblockadditionalclass : "contentwithbg",
		uppertextblock : [
			{
				textclass         : "inset-text-effect",
				textdata          : data.string.p1text0,
			}
		],
	},

	//slide1 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"01.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-left1",
						imagelabeldata  : data.string.p0instruction1,
					},

					{
						imagelabelclass : "ins-left2",
						imagelabeldata  : data.string.p0instruction2,
					},




				],



			},




		],




	},


//slide2 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"01.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-left1",
						imagelabeldata  : data.string.p0instruction1,
					},

					{
						imagelabelclass : "ins-left2",
						imagelabeldata  : data.string.p0instruction2,
					},

					{
						imagelabelclass : "ins-left3",
						imagelabeldata  : data.string.p0instruction3,
					},




				],



			},

		],



	},

	//slide3 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"02.gif"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},





				],



			},

		],



	},



//slide4 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"03.gif"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},

					{
						imagelabelclass : "ins-left2"




						,
						imagelabeldata  : data.string.p1instruction3,
					},





				],



			},

		],



	},

	//slide5 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"04.gif"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},

						{
						imagelabelclass : "ins-nb-left2"




						,
						imagelabeldata  : data.string.p1instruction3,
					},

						{
						imagelabelclass : "ins-left3"




						,
						imagelabeldata  : data.string.p1instruction4,
					},





				],



			},

		],



	},

	//slide6 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"05.gif"
					},

				],

			imagelabels :[
				{
						imagelabelclass : "ins-nb-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},

						{
						imagelabelclass : "ins-nb-left2"




						,
						imagelabeldata  : data.string.p1instruction3,
					},

						{
						imagelabelclass : "ins-nb-left3"




						,
						imagelabeldata  : data.string.p1instruction4,
					},

					{
						imagelabelclass : "ins-left4"




						,
						imagelabeldata  : data.string.p1instruction5,
					},




				],



			},

		],



	},

	//slide7 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.gif"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},

						{
						imagelabelclass : "ins-nb-left2"




						,
						imagelabeldata  : data.string.p1instruction3,
					},

						{
						imagelabelclass : "ins-nb-left3"




						,
						imagelabeldata  : data.string.p1instruction4,
					},

					{
						imagelabelclass : "ins-nb-left4"




						,
						imagelabeldata  : data.string.p1instruction5,
					},

				{
						imagelabelclass : "ins-left5"




						,
						imagelabeldata  : data.string.p1instruction6,
					},






				],



			},

		],



	},



//slide8 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"07.gif"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-left1"




						,
						imagelabeldata  : data.string.p1instruction2,
					},

						{
						imagelabelclass : "ins-nb-left2"




						,
						imagelabeldata  : data.string.p1instruction3,
					},

						{
						imagelabelclass : "ins-nb-left3"




						,
						imagelabeldata  : data.string.p1instruction4,
					},

					{
						imagelabelclass : "ins-nb-left4"




						,
						imagelabeldata  : data.string.p1instruction5,
					},

				{
						imagelabelclass : "ins-nb-left5"




						,
						imagelabeldata  : data.string.p1instruction6,
					},

					{
						imagelabelclass : "ins-left6"




						,
						imagelabeldata  : data.string.p1instructionerror,
					},






				],



			},

		],



	},


	//slide9 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],

		imageblockadditionalclass:"align-right",
		imageblock : [
			{


			imagelabels :[
					{
						imagelabelclass : "vdoins",
						imagelabeldata  : data.string.p0instruction4,
					},


				],



			},

		],

		videoblockadditionalclass:"align-left",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"newintro.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},




	//slide10
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-mid",
						imagelabeldata  : data.string.p0instruction5,
					},

				],



			},

		],



	},


	//slide11
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

				],



			},

		],



	},

	//slide12
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1 highlight",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p0instruction7_0,
					},
					{
						imagelabelclass : "ins-nb-title2 tohigh",
						imagelabeldata  : data.string.p0instruction7,
					},

				],



			},

		],



	},

	//slide13
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1 highlight",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p0instruction7_0,
					},

					{
						imagelabelclass : "ins-nb-title2",
						imagelabeldata  : data.string.p0instruction7,
					},
					{
						imagelabelclass : "ins-nb-title3 tohigh",
						imagelabeldata  : data.string.p0instruction8,
					},

				],



			},

		],



	},

		//slide14
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1 highlight",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p0instruction7_0,
					},

					{
						imagelabelclass : "ins-nb-title2",
						imagelabeldata  : data.string.p0instruction7,
					},
					{
						imagelabelclass : "ins-nb-title3",
						imagelabeldata  : data.string.p0instruction8,
					},
					{
						imagelabelclass : "ins-nb-title4 tohigh",
						imagelabeldata  : data.string.p0instruction9,
					},

				],



			},

		],



	},

	//slide15 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1v highlight",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2v",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3v",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4v",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5v",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p0instruction7_0,
					},

					{
						imagelabelclass : "ins-nb-title2v",
						imagelabeldata  : data.string.p0instruction7,
					},
					{
						imagelabelclass : "ins-nb-title3v",
						imagelabeldata  : data.string.p0instruction8,
					},
					{
						imagelabelclass : "ins-nb-title4v",
						imagelabeldata  : data.string.p0instruction9,
					},


				],


			},

		],

		videoblockadditionalclass:"align-left01",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"45.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},

	//slide16
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1 highlight",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},


					{
						imagelabelclass : "ins-nb-title2_0",
						imagelabeldata  : data.string.p0instruction10,
					},
					{
						imagelabelclass : "ins-nb-title3_0",
						imagelabeldata  : data.string.p0instruction11,
					},


				],



			},

		],


		videoblockadditionalclass:"align-left01",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"45.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext1,
						replaynew: "replay1",

					},
				]
			},


		],
	},


	//slide17

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelclass : "button1 highlight_neg",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p0instruction7_1,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p0instruction7_2,
					},
					{
						imagelabelclass : "ins-nb-title2_1 invisibl",
						imagelabeldata  : data.string.p0instruction7_3,
					},
					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.helptext,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},

	//slide18
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[
					{

						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{
						imagelabelid:"incorrect1",
						imagelabelclass : "button1 hover",
						imagelabeldata  : data.string.button1text,
					},
					{
						imagelabelid:"incorrect2",
						imagelabelclass : "button2 hover",
						imagelabeldata  : data.string.button2text,
					},
					{
						imagelabelid:"correctbutton",
						imagelabelclass : "button3 hover",
						imagelabeldata  : data.string.button3text,
					},
					{
						imagelabelid:"incorrect3",
						imagelabelclass : "button4 hover",
						imagelabeldata  : data.string.button4text,
					},
					{
						imagelabelid:"incorrect4",
						imagelabelclass : "button5 hover",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p1instruction11,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p1instruction12,
					},
					{
						imagelabelclass : "ins-nb-title2_1 invisibl",
						imagelabeldata  : data.string.p1instruction13,
					},
					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.helptext2,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},



	//slide19

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svgquad",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[


					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[
					{

						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{

						imagelabelclass : "button1 used",
						imagelabeldata  : data.string.button1text,
					},
					{

						imagelabelclass : "button2 used",
						imagelabeldata  : data.string.button2text,
					},
					{

						imagelabelclass : "button3 used ",
						imagelabeldata  : data.string.button3text,
					},
					{

						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.button4text,
					},
					{

						imagelabelclass : "button5",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p1instruction14,
					},

					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p1instruction15,
					},

					{
						imagelabelclass : "ins-nb-title2_1 invisibl",
						imagelabeldata  : data.string.p1instruction16,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.helptext3,
					},

					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],


	},



	//slide21

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svgquad",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[


					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},


				],

			imagelabels :[
					{

						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},
					{

						imagelabelclass : "button1 used",
						imagelabeldata  : data.string.button1text,
					},
					{

						imagelabelclass : "button2 used",
						imagelabeldata  : data.string.button2text,
					},
					{

						imagelabelclass : "button3 used ",
						imagelabeldata  : data.string.button3text,
					},
					{

						imagelabelclass : "button4 used",
						imagelabeldata  : data.string.button4text,
					},
					{

						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.button5text,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p1instruction17,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p1instruction18,
					},
					{
						imagelabelclass : "ins-nb-title2_1 invisibl",
						imagelabeldata  : data.string.p1instruction19,
					},
					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.helptext4,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],


	},

	//slide22

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svgquad",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[


					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},


				],

			imagelabels :[
					{

						imagelabelclass : "ins-nb-title",
						imagelabeldata  : data.string.p0instruction6,
					},



					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p1instruction20,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p1instruction21,
					},
					{
						imagelabelclass : "ins-nb-title2_1 invisibl",
						imagelabeldata  : data.string.p1instruction22,
					},
					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.helptext5,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],


		tableblock : [


		{


			qData : {

					base1 : "10", base2: "20",


			}
		}
		],



	},


/*NEW SLIDES END*/

//END




];


//for play and pause video

	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }


	}

	 function restart() {
        var video = document.getElementById("myVideo");
        video.currentTime = 0;
		myVideo.play();
    }


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
     Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */

		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/





	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/



/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);



		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);


		if (countNext==16) {

		var video = document.getElementById("myVideo");
        //video.currentTime = 3000;



		$('.replay').click(function(){

			video.currentTime = 0;
			myVideo.play();

			//alert(video.currentTime);
			});


		};

		if(countNext==15){

			$('.replay').css("opacity","0");
		}


		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}


	/*=================================================
	=            svg template function            =
	=================================================*/
	function svgtemplate(){
  var source   = $("#general-template").html();
  var template = Handlebars.compile(source);
  var html     = template(content[countNext]);
  $board.html(html);
  $nextBtn.hide(0);
  // highlight any text inside board div with datahighlightflag set true
  texthighlight($board);

  //call instruction block controller
  instructionblockcontroller($board);

  //notify user call
  // notifyuser($board);
  var $svgwrapper = $board.children('div.svgwrapper');
  var $svgcontainer = $board.children('div.svgcontainer');
  // var length = $svgwrapper.length;
  // alert(length);

  // if there is a svg content
  // if($svgwrapper.length > 0){
   var s = Snap(".svgcontainer");

   Snap.load(imgpath+"point2.svg", function (f) {
      s.append(f);



        // snap objects


      var metal    = s.select("#metal");
      var nonmetal        = s.select("#nonmetal");
      var metalliods     = s.select("#metalloids");


      // jquery objects and js variables
      var $svg      = $svgcontainer.children('svg');

   var move = document.querySelector("#circle");
   var graph = document.querySelector(".graph");


   graph.addEventListener("click", getClickPosition, false);

   function getClickPosition(e){


   var xPosition = e.clientX-300;
   var yPosition = e.clientY-300;



 var translate3DValue = "translate3d(" + xPosition + "px," + yPosition + "px, 0)";
 move.style.transform = translate3DValue;

 }



  $(".check").on("click", function(){

   var idname = $(this).attr("id");


   if (idname==="incorrect"){
    $(".wrong1").addClass("cssfadein");
	$(".helptext01").removeClass("cssfadein");


	 setTimeout(function() {
       $(".wrong1").removeClass("cssfadein");
	}, 1200);


    }




   if (idname==="correct") {

    $(".wrong1").hide(0);
    $(".right").addClass("cssfadein");
    $(".pointtext").show(0);
    $("#correct1").css("opacity","1");
    $(".invisibl").css("opacity","1");
    $(".visible").css("opacity","0");
	$(".helptext01").removeClass("cssfadein");
	//$(".help").hide(0);




    $nextBtn.delay(1000).show(0);
   };
  });





$(".help").on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");



});


   });
 // }
}

	function svgtemplate1(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"45.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');



		$("#incorrect1").on("click", function(){
		$("#incorrect1").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");

		});

		$("#incorrect2").on("click", function(){
		$("#incorrect2").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");

		});

		$("#incorrect3").on("click", function(){
		$("#incorrect3").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");

		});
		$("#incorrect4").on("click", function(){
		$("#incorrect4").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");

		});

		$("#correctbutton").on("click", function(){
			$(".wrong1").hide(0);
			$("#correctbutton").css("background","#00C51E");
			$(".right").addClass("cssfadein");
			$(".invisibl").css("opacity","1");
			$(".visible").css("opacity","0");
			$(".helptext01").removeClass("cssfadein");
			$nextBtn.delay(1000).show(0);

		});


		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});

 		});
	// }
}

	function svgtemplate2(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"quad.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');



		$(".hover1").on("click", function(){
		$("#incorrect1").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");
		$(".w1").show(0);

		});

		$(".hover2").on("click", function(){
		$("#incorrect2").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".helptext01").removeClass("cssfadein");
		$(".w2").show(0);

		});

		$(".hover3").on("click", function(){
		$("#incorrect3").css("background","#FF684B");
		$(".wrong1").addClass("cssfadein");
		$(".w3").show(0);

		});


		$(".hover4").on("click", function(){
			$(".wrong1").hide(0);
			$("#correctbutton").css("background","#00C51E");
			$(".right").addClass("cssfadein");
			$(".invisibl").css("opacity","1");
			$(".visible").css("opacity","0");
			$(".helptext01").removeClass("cssfadein");
			$nextBtn.delay(1000).show(0);
			$(".rightans").show(0);
			$(".ans").show(0);



		});


		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});

 		});
	// }
}


	function svgtemplate3(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"point5.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				 setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");

				}, 1200);


			}


			if (idname==="correct") {

				$(".wrong1").hide(0);
				$(".helptext01").removeClass("cssfadein");
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct1").css("opacity","1");
				$nextBtn.delay(1000).show(0);
			};
		});


		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }
}


	function svgtemplate4(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		$(".footerNotification").hide(0);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");


			$(".check").html(data.string.chk)
			Snap.load(imgpath+"point6.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




	$(".base1,.base2").keypress(function (e) {
			     //if the letter is not digit then display error and don't type anything
			     if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			        //display error message
			        $(".errmsg").show(0).fadeOut("slower");
			        return false;
			    }
	  		});



		$board.on('click','.check',function () {
		$nextBtn.css('opacity', '1');


		//box1

			var value1 = $(".base1").val();

				if($(".base1").val()=="") {
					swal("please fill all the boxes");
					$(".wrong1").hide(0);
					$(".correct1").hide(0);
				}


				else if (value1 ==8) {

					$(".base1").css("background", "#8fc557 ");
					$(".base1").css("border", "#fff");
					$(".correct1").css("display","inline");

					$(".wrong1").hide(0);
					document.getElementById("inone").disabled = true;
				}
				else{
					$(".base1").css("background", "#F8735D");
					$(".base1").css("border", "red");
					$(".wrong1").css("display","inline");
					$(".wrong1").addClass("cssfadein");
					$(".correct1").hide(0);
					$(".helptext01").removeClass("cssfadein");

					}


			//box2
			var value2 = $(".base2").val();
				if($(".base2").val()=="") {
						swal("please fill all the boxes");
					$(".wrong2").hide(0);
					$(".correct2").hide(0);
					}
				else if (value2 ==4) {
					$(".base2").css("background", "#8fc557");
					$(".base2").css("border", "#fff");
					$(".correct2").css("display","inline");
					$(".wrong2").hide(0);
					document.getElementById("intwo").disabled = true;

				}
				else{
					$(".base2").css("background", "#F8735D");
					$(".base2").css("border", "red");

					$(".wrong2").css("display","inline");
					$(".correct2").hide(0);
					$(".helptext01").removeClass("cssfadein");
				}

				var value1 = $(".base1").val();
				var value2 = $(".base2").val();

				if(value1 ==8 && value2 ==4){
					$(".right").addClass("cssfadein");
					$(".helptext01").removeClass("cssfadein");
					$(".footerNotification").show(0);
					//$nextBtn.delay(1000).show(0);

				}

			});



		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");



		});



 		});
	// }
}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template


		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : generaltemplate(); break;
			case 4 : generaltemplate(); break;
			case 5 : generaltemplate(); break;
			case 6 : generaltemplate(); break;
			case 7 : generaltemplate(); break;
			case 8 : generaltemplate(); break;
			case 9 : generaltemplate(); break;
			case 10 : generaltemplate(); break;

			case 11 : generaltemplate(); break;
			case 11 : generaltemplate(); break;
			case 12 : generaltemplate(); break;
			case 13 : generaltemplate(); break;

			case 14 : generaltemplate(); break;
			case 15 : generaltemplate(); break;
			case 16 : generaltemplate(); break;
			case 17 : svgtemplate(); break;
			case 18 : svgtemplate1(); break;
			case 19 : svgtemplate2(); break;
			case 20 : svgtemplate3(); break;
			case 21 : svgtemplate4(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

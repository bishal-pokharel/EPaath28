var imgpath = $ref+"/images/page4/";
var vdopath = $ref+"/videos/page4/";

var content=[


	
	//slide0
	
	{	
		
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text9,
			},
		],
		
		contentblockadditionalclass : "contentwithbg",
		uppertextblockadditionalclass:"sub-title",	
		uppertextblock : [
			{
				textclass         : "inset-text-effect",
				textdata          : data.string.p4text1,
			}
		],
			
			imageblock : [
			{
			
				imagestoshow:[
					
					{
						imgclass: "sub-image",
						imgsrc: imgpath+"rotation.gif"
					},
					
				
				],		
						
			},
			
		],
		
	},
	
	//slide1
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p4text2,
			},
		],
		
		videoblock : [
			{
				videotoshow : [
					{
						vdoid: "myVideo",
						vdeosrc2   : vdopath+"singlepoint.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replay,
					
					},
					
					
				]	
			},
		
		
		],
		
		
		instructionblock:[
			{
				instruction : [
					data.string.p2instruction1,
					data.string.p2instruction2,
					data.string.p2instruction3,
					data.string.p2instruction4,
					data.string.p2instruction5,
					data.string.p2instruction6,
				],
			}
		]
		
	},
	
	
	
	
	
	//slide2
	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p4text3,
			},
		],
		
		
		

		uppertextblock:[
				{	
					textclass: "primary-instruction",
					textdata: data.string.p2text4,
				}
			],
		
		
		videoblockadditionalclass:"pointdraw",
		videoblock : [
			{
				videotoshow : [
					{
						vdoid: "myVideo1",
						vdeosrc2   : vdopath+"p90.mp4",
						vdoclass: "triangle-x",
						replaytext : data.string.replay,
					
					},
					
					{
						vdoid: "myVideo2",
						vdeosrc2   : vdopath+"180.mp4",
						vdoclass: "triangle-y",
						replaytext : data.string.replay,
					
					},
					{
						vdoid: "myVideo3",
						vdeosrc2   : vdopath+"m90.mp4",
						vdoclass: "triangle-z",
						replaytext : data.string.replay,
					
					},

				
				]	
			},
		
		
		],
		
		
		instructionblock:[
			{
				instruction : [
					data.string.p2instruction1,
					data.string.p2instruction2,
					data.string.p2instruction3,
					data.string.p2instruction4,
					data.string.p2instruction5,
					data.string.p2instruction6,
					
				],
			}
		],
		
			imageblock : [
			{
								
				
								
				imagelabels :[
					{
						imagelabelclass : "button1 highlight",
						imagelabeldata  : data.string.p4text5,
					},
					
					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p4text6,
					},
				{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p4text7,
					},
				
				],
			
			},
			
		],
		
		
	},
	
	//slide3
	
	//END
	

	
	
];


//for play and pause video
	function vid_play_pause(vdoname) {
		
	  var myVideo = document.getElementById(vdoname);
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }
	
	}

	

	
	 function restart() {

		var video1 = document.getElementById("myVideo1");
		var video2 = document.getElementById("myVideo2");
		var video3 = document.getElementById("myVideo3");
      
		video1.currentTime = 0;
		video2.currentTime = 0;
		video3.currentTime = 0;
		if (video1.paused) {
		video1.play();
		}
		if (video2.paused) {
		video2.play();
		}
		if (video3.paused) {
		video3.play();
		}
    }
	
	
	

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
     Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 

		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true 
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class
				
				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;
					
				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {	
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/				
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	 
	 
	 
	 
	 
	  	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
	 
	 
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);
		
		if (countNext==2) {
			$('button.myVideo2').hide(0);
			$('.footerNotification').hide(0);
			
			
			$nextBtn.hide(0);
			$('.button1').click(function(){
				restart();	
				$('.triangle-x').show(0);
				$('button.myVideo1').show(0);
				$('.triangle-y').hide(0);
				$('button.myVideo2').hide(0);
				$('.triangle-z').hide(0);
				$('button.myVideo3').hide(0);
				
				$('.button1').addClass("highlight");
				$('.button2').removeClass("highlight");
				$('.button3').removeClass("highlight");
			});
			
			
			$('.button2').click(function(){
				restart();				
				$('.triangle-y').show(0);	
				$('button.myVideo2').show(0);
				$('.triangle-x').hide(0);	
				$('button.myVideo1').hide(0);
				$('.triangle-z').hide(0);	
				$('button.myVideo3').hide(0);				
				$('.button2').addClass("highlight");				
				$('.button1').removeClass("highlight");
				$('.button3').removeClass("highlight");
				//$nextBtn.show(0);
				$('.footerNotification').show(0);
				
			});

				$('.button3').click(function(){
				restart();				
				$('.triangle-z').show(0);	
				$('button.myVideo3').show(0);
				$('.triangle-x').hide(0);	
				$('button.myVideo1').hide(0);
				$('.triangle-y').hide(0);	
				$('button.myVideo2').hide(0);				
				$('.button3').addClass("highlight");				
				$('.button1').removeClass("highlight");			
				$('.button2').removeClass("highlight");
				//$nextBtn.show(0);
				$('.footerNotification').show(0);
				
			});
			
		};
		
		

		
		
		
		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}


	/*=================================================
	=            svg template function            =
	=================================================*/		
	function svgtemplate(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"point2.svg", function (f) {    
    		s.append(f);
		

    	  	// snap objects
				
				
    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		
			
			
 		});
	// }
}

	function svgtemplate1(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"point3.svg", function (f) {    
    		s.append(f);
		

    	  	// snap objects
				
				
    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

		
			
			
 		});
	// }
}

	function svgtemplate2(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"point4.svg", function (f) {    
    		s.append(f);
		

    	  	// snap objects
				
				
    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

		
			
			
 		});
	// }
}


	function svgtemplate3(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			
			Snap.load(imgpath+"point5.svg", function (f) {    
    		s.append(f);
		

    	  	// snap objects
				
				
    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");
    		

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
    		
    		
		

		$(".check").on("click", function(){ 
		
			var idname = $(this).attr("id");
				
			 //alert(idname);
			if (idname==="incorrect"){
				
				$(".wrong1").addClass("cssfadein");
			}

			
			if (idname==="correct") {
				
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				swal({
					  title: data.string.goodjob,
					  imageUrl: $ref+"/images/page1/good.png"
					});
				
				$nextBtn.delay(1000).show(0);
			};
		});
		

		
			
			
 		});
	// }
}



/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		

		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : svgtemplate(); break;
			case 4 : svgtemplate1(); break;
			case 5 : svgtemplate2(); break;
			case 6 : svgtemplate3(); break;
			
			case 7 : generaltemplate(); break;
			case 8 : generaltemplate(); break;
			case 9 : generaltemplate(); break;
			case 10 : generaltemplate(); break;
			case 11 : generaltemplate(); break;
			case 12 : generaltemplate(); break;
			case 13 : generaltemplate(); break;
			case 14 : generaltemplate(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
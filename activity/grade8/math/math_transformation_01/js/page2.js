var imgpath = $ref+"/images/page2/";
var vdopath = $ref+"/videos/page2/";

var content=[


	//slide1 new
	{

		/*hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],*/


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "introimg",
						imgsrc: imgpath+"reflection_ico.jpg",
					},

					{
						imgclass: "introimg2",
						imgsrc: imgpath+"reflection_ico2.jpg",
					},

					{
						imgclass: "introimg3",
						imgsrc: imgpath+"reflection_ico3.jpg",
					},

				],



			imagelabels :[
					{
						imagelabelclass : "ins-left2 ",
						imagelabeldata  : data.string.p2text3_0,
					},

					{
						imagelabelclass : "ins-left20 ",
						imagelabeldata  : data.string.p2text3_0_1,
					},

					{
						imagelabelclass : "ins-left21 ",
						imagelabeldata  : data.string.p2text3_0_2,
					},

					{
						imagelabelclass : "ins-left22 ",
						imagelabeldata  : data.string.p2text3_0_3,
					},






				],



			},




		],




	},


//slide2 new
	{

		/*hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],*/


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "introimg01",
						imgsrc: imgpath+"reflection_ico.jpg",
					},

					{
						imgclass: "introimg2",
						imgsrc: imgpath+"reflection_ico2.jpg",
					},

					{
						imgclass: "introimg3",
						imgsrc: imgpath+"reflection_ico3.jpg",
					},

				],

			imagelabels :[

					{
						imagelabelclass : "ins-left2 noactive",
						imagelabeldata  : data.string.p2text3_0,
					},

					{
						imagelabelclass : "ins-left20 ",
						imagelabeldata  : data.string.p2text3_0_1,
					},

					{
						imagelabelclass : "ins-left3",
						imagelabeldata  : data.string.p2text3_1,
					},






				],



			},

		],



	},

	//slide3 new
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"horizontal.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"vertical.png"
					},


				],

			/*imagelabels :[
					{
						imagelabelclass : "label1 highlighted",
						imagelabeldata  : data.string.p2text3_3,
					},


				],	*/



			},

		],



	},



	//slide4 new
/*
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"horizontal.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"vertical.png"
					},

				],

			/*imagelabels :[
					{
						imagelabelclass : "label1 ",
						imagelabeldata  : data.string.p2text3_3,
					},

					{
						imagelabelclass : "label2 highlighted",
						imagelabeldata  : data.string.p2text3_4,
					},





				],



			},

		],



	},*/


	//slide4 new


	{

		/*hasheaderblock : true,

		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_5,
			}
		],*/


		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"horizontal-opacity.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"vertical-opacity.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "label1 ",
						imagelabeldata  : data.string.p2text3_3,
					},

					{
						imagelabelclass : "label2",
						imagelabeldata  : data.string.p2text3_4,
					},


				],



			},

		],




	},


	//slide5 new


	{

		hasheaderblock : true,

		/*
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_6,
			}
		],
		*/

		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror1.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror2.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "label1 ",
						imagelabeldata  : data.string.p2text3_3,
					},

					{
						imagelabelclass : "label2",
						imagelabeldata  : data.string.p2text3_4,
					},


				],



			},

		],




	},




	//slide6 new


	{

		hasheaderblock : true,


		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_6,
			}
		],


		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror1.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror2.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "label1 ",
						imagelabeldata  : data.string.p2text3_3,
					},

					{
						imagelabelclass : "label2",
						imagelabeldata  : data.string.p2text3_4,
					},


				],



			},

		],




	},



	//slide7 new manish


	{

		hasheaderblock : true,


		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_5,
			}
		],


		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror1.png"
					},

					{
						imgclass: "imgtogether2",
						imgsrc: imgpath+"mirror2.png"
					},

				],

			imagelabels :[
					{
						imagelabelclass : "label1 ",
						imagelabeldata  : data.string.p2text3_3,
					},

					{
						imagelabelclass : "label2",
						imagelabeldata  : data.string.p2text3_4,
					},


				],



			},

		],




	},



	//slide7 new

	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		uppertextblock : [
			{
				textclass         : "polybgtextstyle",
				textdata          : data.string.p2text3_7,
			}
		],
	},



//slide9 new

{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_8,
			}
		],

	},

//slide10


{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_9,
			}
		],

	},


//slide11


{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_11,
			}
		],

	},


	//slide12


{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_11,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_12,
			}
		],

	},

	//slide13

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],


		uppertextblockadditionalclass:"que2",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_13,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_14,
			},

			],

		videoblockadditionalclass:"align-left2",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"singlepoint01.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},

	//slide14

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],


		uppertextblockadditionalclass:"que2",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_13,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_14,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_15,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg ",
				textdata          : data.string.p2text3_16,
			},

			],

		videoblockadditionalclass:"align-left2",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"trianglex.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},


	//slide15
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		uppertextblock : [
			{
				textclass         : "polybgtextstyle",
				textdata          : data.string.p2text3_17,
			}
		],
	},

	//slide16

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_22,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_24,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},

	//slide17
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_25,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_26,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},


	//slide18
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_27,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_28,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},


		//slide19
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_29,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_30,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},

//relction on y-axis

//slide20-1


{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_11,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_31,
			}
		],

	},


	//slide21-2


{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],

		svgblockadditionalclass:"svg-two",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],


			imageblock : [
			{


				imagestoshow:[

					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],



			},

		],


		uppertextblockadditionalclass:"que1",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_11,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_31,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_32,
			}

		],

	},

	//slide22-3

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],


		uppertextblockadditionalclass:"que2",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_13,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_33,
			},

			],

		videoblockadditionalclass:"align-left2",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"singlepointy.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},

	//slide23-4

	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],


		uppertextblockadditionalclass:"que2",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_13,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_14,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_15,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg ",
				textdata          : data.string.p2text3_16,
			},

			],

		videoblockadditionalclass:"align-left2",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"triangley.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},


	//slide24-5
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_2,
			},
		],


		uppertextblock : [
			{
				textclass         : "polybgtextstyle",
				textdata          : data.string.p2text3_34,
			}
		],
	},


	//y-axis do it yourself

	//slide25-6
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2 highlight",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_35,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_36,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},

	//slide26-7
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3 highlight",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_37,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_38,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},


	//slide26-7
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4 highlight",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_39,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_40,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},


		//slide19
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p1text1,
			},
		],


		svgblockadditionalclass:"svg-new1",
		svgblock :[
				{
					svgname:"svgcontainer",
				}
			],

		imageblockadditionalclass:"align-left",
		imageblock : [
			{

			imagestoshow:[

					{
						imgclass: "graph none",
						imgsrc: imgpath+"06.png"
					},
					{
						imgclass: "right",
						imgsrc: "images/correct.png"
					},
					{
						imgclass: "wrong1",
						imgsrc: "images/wrong.png"
					},

				],

			imagelabels :[


					{
						imagelabelclass : "button2",
						imagelabeldata  : data.string.p2text3_18,
					},
					{
						imagelabelclass : "button3",
						imagelabeldata  : data.string.p2text3_19,
					},
					{
						imagelabelclass : "button4",
						imagelabeldata  : data.string.p2text3_20,
					},
					{
						imagelabelclass : "button5 highlight",
						imagelabeldata  : data.string.p2text3_21,
					},

					{
						imagelabelclass : "title0",
						imagelabeldata  : data.string.p2text3_41,
					},
					{
						imagelabelclass : "ins-nb-title2_0 visible",
						imagelabeldata  : data.string.p2text3_23,
					},

					{
						imagelabelclass : "helptext01",
						imagelabeldata  : data.string.p2text3_42,
					},
					{
						imagelabelclass : "help",
						imagelabeldata  : data.string.help,
					},

				],



			},

		],



	},


//last slide
	{

		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2text3_10,
			},
		],


		uppertextblockadditionalclass:"que2",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p2text3_43,
			},

			{
				datahighlightflag : true,
				textclass         : "paratextstyle marg",
				textdata          : data.string.p2text3_44,
			}


			],

		videoblockadditionalclass:"align-left2",
		videoblock : [
			{
				videotoshow : [
					{

						vdeosrc2   : vdopath+"singlepoint.mp4",
						vdoclass: "intro-vdo",
						replaytext : data.string.replaytext,

					},
				]
			},


		],



	},


//END***

];


//for play and pause video

	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }


	}

	 function restart() {
        var video = document.getElementById("myVideo");
        video.currentTime = 0;
		myVideo.play();

    }


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
     Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 Handlebars.registerPartial("tablecontent", $("#tablecontent-partial").html());


		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */

		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
					//alert("alerttt");
					ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;

		}
	 }
	 /*=====  End of user navigation controller function  ======*/





	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/



/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);



		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);


		if (countNext==0) {

		var video = document.getElementById("myVideo");
        //video.currentTime = 3000;





		$('.replay').click(function(){

			video.currentTime = 0;
			myVideo.play();


			//alert(video.currentTime);
			});


		};

		if(countNext==15){

			$('.replay').css("opacity","0");
		}


		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}


	/*=================================================
	=            svg template function            =
	=================================================*/
	function svgtemplate(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"graph02-x.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
			}
				if (idname==="incorrectpen2"){

				$(".wrong1").addClass("cssfadein");
			}



			if (idname==="correctpenx") {

				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				$("#correctpenx").css("opacity","1");

				$nextBtn.delay(1000).show(0);
			};
		});



 		});

	}

	function svgtemplate1(){

		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"graph02-y.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
			}

			if (idname==="incorrectpen3"){

				$(".wrong1").addClass("cssfadein");
			}


			if (idname==="correctpeny") {

				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".pointtext").show(0);
				$("#correct").css("opacity","1");
				$("#correctpeny").css("opacity","1");

				$nextBtn.delay(1000).show(0);
			};
		});



 		});

	}

	function svgtemplate2(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"graph03.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




 		});

	}


	function svgtemplate3(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);


		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"graph04.svg", function (f) {
    		s.append(f);


    	  	// snap objects


    		var metal    = s.select("#metal");
    		var nonmetal        = s.select("#nonmetal");
    		var metalliods     = s.select("#metalloids");


    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');



 		});
	// }
}


	function svgtemplate4(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');


		$(".que1").attr("class", "que1 blink");

		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct1") {
				$(".que1").attr("class", "que1");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".correctxt1").show(0);
				$(".corrtxt-x1").show(0);
				$("#correct10").css("opacity","1");
				//$(".que1").removeClass(".blink");
				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}


	function svgtemplate5(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');


		$(".que2").attr("class", "que2 blink");
			$(".correctxt1").show(0);
			$(".corrtxt-x1").show(0);
			$("#correct10").css("opacity","1");

		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct2") {
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");

				$(".correctxt2").show(0);
				$(".corrtxt-x2").show(0);
				$("#correct20").css("opacity","1");
				$(".que2").attr("class", "que2");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}


	function svgtemplate6(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".que3").attr("class", "que3 blink");



		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct3") {
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");

				$(".correctxt3").show(0);
				$(".corrtxt-x3").show(0);
				$("#correct30").css("opacity","1");
				$(".que3").attr("class", "que3");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}



	function svgtemplate7(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".correctxt3").show(0);
			$("#correct30").css("opacity","1");
			$(".corrtxt-x3").show(0);
			$(".que4").attr("class", "que4 blink");


			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
			$(".correctxt1").show(0);
			$("#correct10").css("opacity","1");
			$(".corrtxt-x1").show(0);
			$(".correctxt2").show(0);
			$("#correct20").css("opacity","1");
			$(".corrtxt-x2").show(0);
			$(".correctxt3").show(0);
			$("#correct30").css("opacity","1");
			$(".corrtxt-x3").show(0);
			$(".que4").attr("class", "que4 blink");



		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct4") {
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");


				$(".dottedlinex").delay(1000).show(0);
				$("#correct40").css("opacity","1");
				$(".corrtxt-x4").show(0);
				$(".correctpara").css("opacity","1");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}


	function svgtemplate8(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"graph05.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');




 		});

	}




	function svgtemplate9(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');


		$(".que1").attr("class", "que1 blink");

		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct5") {

				$(".que1").attr("class", "que1");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".corrtxt-y1").show(0);
				$(".correctxt5").show(0);
				$("#correct50").css("opacity","1");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}


	function svgtemplate10(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);





    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');



			$(".que2").attr("class", "que2 blink");
			$(".correctxt5").show(0);
			$("#correct50").css("opacity","1");
    		$(".corrtxt-y1").show(0);


		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct6") {

				$(".wrong1").hide(0);
				$(".que2").attr("class", "que2");
				$(".right").addClass("cssfadein");
				$(".corrtxt-y2").show(0);
				$(".correctxt6").show(0);
				$("#correct60").css("opacity","1");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}


	function svgtemplate11(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);



    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');

			$(".que3").attr("class", "que3 blink");
			$(".correctxt5").show(0);
			$("#correct50").css("opacity","1");
			$(".correctxt6").show(0);
			$("#correct60").css("opacity","1");
    		$(".corrtxt-y1").show(0);
    		$(".corrtxt-y2").show(0);




		$(".check").on("click", function(){

			var idname = $(this).attr("id");

			 //alert(idname);
			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct7") {

				$(".que3").attr("class", "que3");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".corrtxt-y3").show(0);
				$(".correctxt7").show(0);
				$("#correct70").css("opacity","1");

				$(".helptext01").removeClass("cssfadein");


				$nextBtn.delay(1000).show(0);
			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}



	function svgtemplate12(){

	var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);


		//call instruction block controller
		instructionblockcontroller($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer');
		// var length = $svgwrapper.length;
		// alert(length);

		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");

			Snap.load(imgpath+"reflect-x1.svg", function (f) {
    		s.append(f);

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');
			$(".que4").attr("class", "que4 blink");
			$(".correctxt5").show(0);
			$("#correct50").css("opacity","1");
			$(".correctxt6").show(0);
			$("#correct60").css("opacity","1");
			$(".correctxt7").show(0);
			$("#correct70").css("opacity","1");
    		$(".corrtxt-y1").show(0);
    		$(".corrtxt-y2").show(0);
    		$(".corrtxt-y3").show(0);



		$(".check").on("click", function(){

			var idname = $(this).attr("id");


			if (idname==="incorrect"){

				$(".wrong1").addClass("cssfadein");
				$(".helptext01").removeClass("cssfadein");

				setTimeout(function() {
				   $(".wrong1").removeClass("cssfadein");
				}, 1200);

			}


			if (idname==="correct8") {

				$(".que4").attr("class", "que4");
				$(".wrong1").hide(0);
				$(".right").addClass("cssfadein");
				$(".corrtxt-y4").show(0);
				$(".correctxt8").show(0);
				$("#correct80").css("opacity","1");
				$(".correctpara2").css("opacity","1");
				$(".dottedliney").show(0);

				$nextBtn.delay(1000).show(0);



				$(".helptext01").removeClass("cssfadein");



			};


		});

		$( ".help" ).on("click", function(){

		$(".helptext01").addClass("cssfadein");
		$(".right").removeClass("cssfadein");
		$(".wrong1").removeClass("cssfadein");


		});



 		});
	// }

	}



/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template


		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : generaltemplate(); break;
			case 4 : generaltemplate(); break;
			case 5 : generaltemplate(); break;
			case 6 : generaltemplate(); break;

			case 7 : generaltemplate(); break;
			//case 8 : generaltemplate(); break;

			case 8 : svgtemplate(); break;
			case 9 : svgtemplate1(); break;
			case 10 : svgtemplate2(); break;
			case 11 : svgtemplate3(); break;



			case 12 : generaltemplate(); break;
			case 13 : generaltemplate(); break;
			case 14 : generaltemplate(); break;
			//case 15 : generaltemplate(); break;
			case 15 : svgtemplate4(); break;
			case 16 : svgtemplate5(); break;
			case 17 : svgtemplate6(); break;
			case 18 : svgtemplate7(); break;


			case 19 : svgtemplate2(); break;
			case 20 : svgtemplate8(); break;

			case 21 : generaltemplate(); break;
			case 22 : generaltemplate(); break;

			case 23 : generaltemplate(); break;

			case 24 : svgtemplate9(); break;
			case 25 : svgtemplate10(); break;
			case 26 : svgtemplate11(); break;
			case 27 : svgtemplate12(); break;
			case 28 : generaltemplate(); break;


			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

//ole.activityComplete.finishingcall();

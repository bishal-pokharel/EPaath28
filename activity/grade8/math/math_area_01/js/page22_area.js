/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=40;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,5,9,13,26]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p23_1,
		data.string.p23_2,
		data.string.p23_3,
		data.string.p23_4,
		data.string.p23_5,
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
					data.string.p24_0,
					data.string.p24_1,
					data.string.p24_2,
					
			        ],
			
	},
	
	
	{
		justClass : "third",
		
			text : [
					data.string.p24_0,
					data.string.p24_3,
					data.string.p24_4,
					data.string.p24_5,
					data.string.p24_6,
					data.string.p24_7,
					data.string.p24_8,
					data.string.p24_9,
					
					],
			
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	

	
	{
		justClass : "fourth",
		
			text : [
			        data.string.p25_0,
					data.string.p25_1,
					data.string.p25_2,
					data.string.p25_3,
					data.string.p25_4,
					data.string.p25_5,
					data.string.p25_6,
					data.string.p25_7,
					data.string.p25_8,
					data.string.p25_9,
					data.string.p25_10,
					data.string.p25_11,
					data.string.p25_12,
		
				],
	},
	
	
	
	{
		justClass : "fifth",
		
			text : [
			
					data.string.p26_0,
					data.string.p26_1,
					data.string.p26_2,
					data.string.p26_3,
					data.string.p26_4,
					
					
			

			
			],
	}
	];
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$(".bg_page4").show(0);
		};

	

		function first01 () {
			$board.find(".wrapperIntro.firstPage ,.wrapperIntro.firstPage ").show(0);
			
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);
			//$board.find(".wrapperIntro.firstPage .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.firstPage .bub1").addClass("slideRight");
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			
			
		}

		function first02 () {
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			
			
		}

		function first03 () {
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .bub2").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			
			
			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			
		}
		
		function first04 () {
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$(".letslook").show(0);
			$(".bg_page4").hide(0);
			$board.find(".wrapperIntro.firstPage .bub1").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			/*$board.find(".wrapperIntro.firstPage .squirrel-talking03").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('background','#b6b6b6');*/
		
		}
		
		
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second .text0").show(0);
			
			
			
		}
		
		
		function second01() {
			$board.find(".wrapperIntro.second .text1").show(0);
			
		}
		
		function second02(){
			//$board.find(".wrapperIntro.second .text2").show(0);
			//$board.find(".wrapperIntro.second .firstimage").show(0);
			var $animimage = $board.find("img.animation_images").show(0);
			var $rangeslider = $board.find("input[type='range']").show(0);
			var changeimgsrc;
			$nextBtn.hide(0);
			$rangeslider.on('change',  function() {
				changeimgsrc = "activity/grade8/math/math_area_01/image/"+$(this).val()+".png";
				$animimage.attr('src', changeimgsrc);
				$nextBtn.show(0);
			}
			);
			
			
		}
		
		function second03(){
			$board.find(".wrapperIntro.second .text2").show(0);
			
		}
		
		
		
		function third () {
			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			//$board.find(".wrapperIntro.third .text0").show(0);
			
			//$board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);
			$board.find(".wrapperIntro.third .text1").show(0);
			$(".letslook").show(0);
			//$board.find(".wrapperIntro.third .bubble1").show(0);
			//$board.find(".wrapperIntro.third .char-holder .char-a").show(0);
		
			//$board.find(".wrapperIntro.third .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.third .bubble1").addClass("slideRight");
			//$board.find(".wrapperIntro.third  .char-holder .char-a").addClass("slideRight");
			
		}
		
		/*function third01(){
			$board.find(".wrapperIntro.third ,.wrapperIntro.third ").show(0);
			$board.find(".wrapperIntro.third .text1").show(0);
			$board.find(".wrapperIntro.third .bubble1").show(0);
			//$board.find(".wrapperIntro.third .char-holder .char-a").show(0);
		
			//$board.find(".wrapperIntro.third .text1").addClass("slideRight");
			//$board.find(".wrapperIntro.third .bubble1").addClass("slideRight");
			$board.find(".wrapperIntro.third  .char-holder .char-a").addClass("slideRight");
			
		}*/
		
		function third02(){
			$(".letslook").hide(0);
			$(".black3").show(0);
			$(".loku").show(0);
			$board.find(".wrapperIntro.third .text0").hide(0);
			$board.find(".wrapperIntro.third .text1").hide(0);
			$board.find(".wrapperIntro.third .text2").delay(1000).show(0);
			$board.find(".wrapperIntro.third .text3").delay(1500).show(0);
			$board.find(".wrapperIntro.third .text4").delay(2000).show(0);
			$board.find(".wrapperIntro.third .text5").delay(2500).show(0);
			// $(".nextBtn.myNextStyle").hide(0).delay(3000).show(0);
			$nextBtn.hide(0).delay(3000).show(0);
		}
		
		
		
		function third03(){
			$(".black3").hide(0);
			$(".loku").hide(0);
			$board.find(".wrapperIntro.third .text2").hide(0);
			$board.find(".wrapperIntro.third .text3").hide(0);
			$board.find(".wrapperIntro.third .text4").hide(0);
			$board.find(".wrapperIntro.third .text5").hide(0);
			$board.find(".wrapperIntro.third .text6").show(0);
			$board.find(".wrapperIntro.third .char-holder .char-a").show(0);
			$board.find(".wrapperIntro.third .bub1").show(0);
			$board.find(".wrapperIntro.third  .char-holder .char-a").addClass("slideRight");
			// $(".nextBtn.myNextStyle").delay(3000).show(0);
			$nextBtn.hide(0).delay(3000).show(0);
		}
		function third04(){
			$(".letslook").show(0);
			$board.find(".wrapperIntro.third .char-holder .char-a").hide(0);
			$board.find(".wrapperIntro.third .bub1").hide(0);
			$board.find(".wrapperIntro.third .text6").hide(0);
			$board.find(".wrapperIntro.third .text7").show(0);
	
		}
		
		
	
		
		
		/*fourth */
		
		function fourth () {
			var source = $("#intro4-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[3];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fourth .text0").show(0);
		}
		
		
		function fourth01() {
			$board.find(".wrapperIntro.fourth .text1").show(0);
			
			
		}
		
		function fourth02(){
			$board.find(".wrapperIntro.fourth .text2").show(0);
			$board.find(".wrapperIntro.fourth .firstimage").show(0);
		}
		
		function fourth03(){
			$board.find(".wrapperIntro.fourth .text3").show(0);
			$board.find(".wrapperIntro.fourth .secondimage").show(0);
		}
		
		function fourth04(){
			$board.find(".wrapperIntro.fourth .text4").show(0);
			$board.find(".wrapperIntro.fourth .thirdimage").show(0);
		}
		
		
		function fourth05(){
			$board.find(".wrapperIntro.fourth .text5").show(0);
		}
		
		
		function fourth06(){
			$board.find(".wrapperIntro.fourth .text6").show(0);
	
		}
		
		function fourth07(){
			$board.find(".wrapperIntro.fourth .text7").show(0);
	
		}
		function fourth08(){
			$board.find(".wrapperIntro.fourth .text8").show(0);
	
		}
		function fourth09(){
			$board.find(".wrapperIntro.fourth .text9").show(0);
	
		}
		function fourth10(){
			$board.find(".wrapperIntro.fourth .text10").show(0);
	
		}
		function fourth11(){
			$board.find(".wrapperIntro.fourth .text11").show(0);
	
		}
		function fourth12(){
			$board.find(".wrapperIntro.fourth .text12").show(0);
	
		}
		
		/*fifth*/
		function fifth () {
			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fifth").css("background","#fff");
			$board.find(".wrapperIntro.fifth").css("padding","1%");
			$board.find('.image-holder').hide(0);
			//$board.find(".wrapperIntro.fifth .text0").show(0);
		}
		
		
		function fifth01() {
			$board.find(".wrapperIntro.fifth .text1").show(0);
			
		}
		
		function fifth02(){
			$board.find(".wrapperIntro.fifth .text2").show(0);
		}
		
		function fifth03(){
			$board.find(".wrapperIntro.fifth .text3").show(0);
			$board.find('.image-holder').show(0);
		}
		
		function fifth04(){
			$board.find(".wrapperIntro.fifth .solution00").show(0);
			$board.find(".wrapperIntro.fifth .text4").css("background","#efefef");
			
		}
		
		
		function fifth05(){
			$board.find(".wrapperIntro.fifth .solution01").show(0);
		}
		
		
		function fifth06(){
			$board.find(".wrapperIntro.fifth .solution02").show(0);
	
		}
		
		function fifth07(){
			$board.find(".wrapperIntro.fifth .solution03").show(0);
	
		}
		function fifth08(){
			$board.find(".wrapperIntro.fifth .solution04").show(0);
	
		}
		function fifth09(){
			$board.find(".wrapperIntro.fifth .solution05").show(0);
	
		}
		function fifth10(){
			$board.find(".wrapperIntro.fifth .solution06").show(0);
	
		}
		function fifth11(){
			$board.find(".wrapperIntro.fifth .solution07").show(0);
	
		}
		function fifth12(){
			$board.find(".wrapperIntro.fifth .solution08").show(0);
	
		}
		
		function fifth13(){
			$board.find(".wrapperIntro.fifth .solution09").show(0);
	
		}
		
		function fifth14(){
			$board.find(".wrapperIntro.fifth .solution10").show(0);
	
		}
		/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		$nextBtn.show(0);
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		$nextBtn.show(0);
		fnArray = [
			first,  //first one
			first01,
			first02,
			first03,
			first04,
			
			second,
			second01,
			second02,
			second03,
			
			third,
			//third01,
			third02,
			third03,
			third04,
			
			fourth,
			fourth01,
			fourth02,
			fourth03,
			fourth04,
			fourth05,
			fourth06,
			fourth07,
			fourth08,
			fourth09,
			fourth10,
			fourth11,
			fourth12,
			
			fifth,
			fifth01,
			fifth02,
			fifth03,
			fifth04,
			fifth05,
			fifth06,
			fifth07,
			fifth08,
			fifth09,
			fifth10,
			fifth11,
			fifth12,
			fifth13,
			fifth14
		];

		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

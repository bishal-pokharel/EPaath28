/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	


	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=23;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,10]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
	{
		justClass : "firstPage",
		animate : "true",
		text : [
		
		data.string.p19_1,
		data.string.p19_2,
		data.string.p19_2_1,
		data.string.p19_3,
		data.string.p19_4,
		data.string.p19_5,
		data.string.p19_6,
		data.string.p19_7,
		data.string.p19_8,
		data.string.p19_9,
		],
		
	},
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "second",
		
			text : [
					data.string.p20_1,
					data.string.p20_2,
					data.string.p20_3,
					data.string.p20_4,
					
			        ],
			
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "third",
		
			text : [
			
				],
	},
	
	
	
	{
		justClass : "fourth",
		
			text : [
			
			
			

			
			],
	}
	
	
	
	
	];
	
	
	
	
/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".bg_page4").show(0);
			$board.find(".wrapperIntro.firstPage .text0").hide(0);
			first02();
		};

	

		function first01 () {
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$(".letslook").hide(0);
			
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .char-a").addClass("slideRight");
			$board.find(".wrapperIntro.firstPage .char-b").addClass("slideleft");
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage,.wrapperIntro.firstPage").show(0);
			
			$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening03").hide(0);
			
		}

		function first02 () {
			// $(".nextBtn.myNextStyle").hide(0);
			// $board.find(".wrapperIntro.firstPage .text0").hide(0);
			// $board.find(".wrapperIntro.firstPage .text1").hide(0);
			// $board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			// $board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$(".letslook").show(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			// $board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			// $board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			// /*$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#b6b6b6');
			// $board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);*/
			// $board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			
		}
		
		function first03 () {
			//$(".nextBtn.myNextStyle").hide(0);
			
			$board.find(".wrapperIntro.firstPage .text1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$(".letslook").hide(0);
			//$board.find(".wrapperIntro.firstPage .text2").addClass("slideLeft");
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			
		}
		

		function first04 () {
			//$(".letslook").hide(0);
			$board.find(".wrapperIntro.firstPage .text3").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			
			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			
			
			
		}
		
		function first05 () {
			$board.find(".wrapperIntro.firstPage .text4").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").show(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			
			
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").show(0);
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .boy-talking02").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			
		}
		
		function first06 () {
			$board.find(".wrapperIntro.firstPage .text5").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble1").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			
			$board.find(".wrapperIntro.firstPage .boy-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro.firstPage .boy-listening01").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-listening01").hide(0);
			
		}
		function first07 () {
			$board.find(".wrapperIntro.firstPage .text6").hide(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			$(".loku").show(0);
			$(".black").show(0);
			$board.find(".wrapperIntro.firstPage .boy-listening01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .bubble2").hide(0);
			/*$board.find(".wrapperIntro.firstPage .squirrel-talking02").css('background','#b6b6b6');*/
			
		}
		
		function first08 () {
			//$board.find(".wrapperIntro.firstPage .text7").hide(0);
			$board.find(".wrapperIntro.firstPage .text8").show(0);
			
			
			/*$board.find(".wrapperIntro.firstPage .squirrel-talking02").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").show(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking03").css('background','#b6b6b6');*/
			
			
		}

		function first09 () {
			$board.find(".wrapperIntro.firstPage .text7").hide(0);
			$board.find(".wrapperIntro.firstPage .text8").hide(0);
			$board.find(".wrapperIntro.firstPage .text9").show(0);
			$(".black").hide(0);
			$(".loku").hide(0);
			$(".letslook").show(0);
			
			/*$board.find(".wrapperIntro.firstPage .squirrel-talking03").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro.firstPage .squirrel-talking01").css('background','#b6b6b6');*/
			
		}
		
		


		
	/*-------------------------------------------------------------------------------------------*/
	
	/*second page*/
		
		
		

		/*end of page 2*/
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.second").css("background","#fff");
			$board.find(".wrapperIntro.second").css("padding","1%");
		}
		
		function second01() {
			$board.find(".wrapperIntro.second .text1").show(0);
		}
		
		function second02(){
			$board.find(".wrapperIntro.second .text2").show(0);
		}
		
		function second03(){
			$board.find(".wrapperIntro.second .solution00").show(0);
			$board.find(".wrapperIntro.second .text3").css("background","#efefef");
		}
		
		function second04(){
			$board.find(".wrapperIntro.second .solution01").show(0);
		}
		
		function second05(){
			$board.find(".wrapperIntro.second .solution02").show(0);
		}
		
		function second06(){
			$board.find(".wrapperIntro.second .solution03").show(0);
		}
		
		function second07(){
			$board.find(".wrapperIntro.second .solution04").show(0);
		}
		function second08(){
			$board.find(".wrapperIntro.second .solution05").show(0);
		}
		function second09(){
			$board.find(".wrapperIntro.second .solution06").show(0);
		}
		function second10(){
			$board.find(".wrapperIntro.second .solution07").show(0);
		}
		function second11(){
			$board.find(".wrapperIntro.second .solution08").show(0);
		}
		function second12(){
			$board.find(".wrapperIntro.second .solution09").show(0);
		}
		function second13(){
			$board.find(".wrapperIntro.second .solution10").show(0);
		}
		function second14(){
			$board.find(".wrapperIntro.second .solution11").show(0);
		}
		
	
/*----------------------------------------------------------------------------------------------------*/

		// first func call
		first();
		$nextBtn.show(0);
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		$nextBtn.show(0);
		fnArray = [
			first,  //first one
			// first02,
			first01,
			first03,
			first04,
			first05,
			first06,
			first07,
			first08,
			first09,
			
			second,
			second01,
			second02,
			second03,
			second04,
			second05,
			second06,
			second07,
			second08,
			second09,
			second10,
			second11,
			second12,
			second13,
			second14
		];

		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

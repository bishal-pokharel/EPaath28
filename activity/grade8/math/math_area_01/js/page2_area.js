/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/


var imgpath = $ref+"/image/";

$(function () {


	var characterDialouges1 = [ {
		diaouges : data.string.p1_7
	},//des1

	];

	var characterDialouges2 = [ {
		diaouges : data.string.p1_8
	},//des2

	];


	var characterDialouges3 = [ {
		diaouges : data.string.p1_9
	},//des3

	];

	var characterDialouges4 = [ {
		diaouges : data.string.p1_10
	},//des4

	];

	var characterDialouges5 = [ {
		diaouges : data.string.p1_11
	},//des5

	];


	var characterDialouges6 = [ {
		diaouges : data.string.p1_12
	},//des6

	];

	var characterDialouges7 = [ {
		diaouges : data.string.p1_13
	},//des7

	];

	var characterDialouges8 = [ {
		diaouges : data.string.p1_14
	},//des8

	];


	var characterDialouges9 = [ {
		diaouges : data.string.p1_15
	},
	];

	var characterDialouges10 = [ {
		diaouges : data.string.p1_16
	},
	];

	var characterDialouges11 = [ {
		diaouges : data.string.p1_17
	},
	];

	var characterDialouges12 = [ {
		diaouges : data.string.p1_18
	},
	];

	var characterDialouges13 = [ {
		diaouges : data.string.p1_19
	},
	];

	var characterDialouges14 = [ {
		diaouges : data.string.p1_20
	},
	];

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=19;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,1,7]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "first",
		animate : "true",
		text : [
		data.lesson.chapter,
		data.string.p1_1_0,
		data.string.p1_1,
		data.string.p1_3,
		data.string.p1_4,
		data.string.p1_5,
		data.string.p1_6
	],
	},
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "secondtext",
			text : [
			data.string.p1_7,
			data.string.p1_8,
			data.string.p1_9,
			data.string.p1_10,
			data.string.p1_11,
			data.string.p1_12,
			data.string.p1_13,
			data.string.p1_14,
			data.string.p1_15,
			data.string.p1_16
			]
	},
	/*end of second page */
	/*------------------------------------------------------------------------------------------------*/
	{
		justClass : "thirdtext",
			text : [
				data.string.p1_21_0,
				data.string.p1_21,
				data.string.p1_22,
				data.string.p1_23,
				data.string.p1_24,
				data.string.p1_25,
				data.string.p1_26,
				data.string.p1_27,
				data.string.p1_28,
				data.string.p1_29,
				data.string.p1_30,
				data.string.p1_31,
				data.string.p1_32,
				data.string.p1_33
				],
	},
	{
		justClass : "fourthtext",
			text : [
			],
	}];




/******************************************************************************************************/

/*-------------------------------------------------------------------------------------------------*/

/*page3*/

		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			/*
			var content = {
				title1 : data.string.p1_0,
				title2 : data.string.p1_1_0,
			}
			*/

			var html = template(content);
			$board.html(html);


			//$board.find(".wrapperIntro .title1").show(0);
			//$board.find(".wrapperIntro .title2").show(0);
			$(".row.char-holder").hide(0);
		}

		function first01() {
			$board.find(".wrapperIntro.first .text1").show(0);
		}


		function first02() {
			//$board.find(".wrapperIntro .title1").hide(0);
			//$board.find(".wrapperIntro .title2").hide(0);
			$board.find(".wrapperIntro .main-bg1").hide(0);
			$(".row.char-holder").show(0);
			$(".bg_page4").show(0);

			$board.find(".wrapperIntro.first").css("background","#fff");
			$board.find(".wrapperIntro .char-a").addClass("slideRight");
			$board.find(".wrapperIntro .char-b").addClass("slideLeft");
			$board.find(".wrapperIntro .squirrel-listening01").show(0);
			$board.find(".wrapperIntro .boy-listening01").show(0);

			$board.find(".wrapperIntro.first ,.wrapperIntro.first ").show(0);
			$board.find(".wrapperIntro.first .text0").hide(0);
			$board.find(".wrapperIntro.first .text1").hide(0);
			$board.find(".wrapperIntro.first .text2").show(0);
			$board.find(".wrapperIntro.first .text3").show(0);
			$board.find(".wrapperIntro .bub1").show(0);
			//$board.find(".wrapperIntro.first .text1").addClass("slideLeft");
			//$board.find(".wrapperIntro.first .bub1").addClass("slideLeft");
			$board.find(".wrapperIntro .boy-listening01").hide(0);
			$board.find(".wrapperIntro .boy-talking01").show(0);
			$board.find(".wrapperIntro .boy-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro .boy-talking01").css('border-color','#FEA92A');
		}


		function first03(){
			$board.find(".wrapperIntro .bub1").hide(0);
			$board.find(".wrapperIntro .bub2").show(0);
			$board.find(".wrapperIntro.first .text2").show(0);
			$board.find(".wrapperIntro.first .text3").hide(0);
			$board.find(".wrapperIntro.first .text4").show(0);
			$(".bg_page4").hide(0);
			$("#reload1").attr("src", imgpath+"animate-square.gif?"+new Date().getTime());
			$board.find(".wrapperIntro.first .square-paint").show(0);
			$nextBtn.delay(2000).show(0);

			//$board.find(".wrapperIntro.first .text3").show(0);
			//$board.find(".wrapperIntro.first .text2").addClass("slideRight");
			//$board.find(".wrapperIntro.first .bub2").addClass("slideRight");



			$board.find(".wrapperIntro .boy-talking01").hide(0);
			$board.find(".wrapperIntro .boy-listening01").show(0);
			$board.find(".wrapperIntro .squirrel-talking01").show(0);
			$board.find(".wrapperIntro .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro .squirrel-listening01").hide(0);
		}

		function first04(){

			$(".bg_page4").show(0);
			$board.find(".wrapperIntro .bub2").hide(0);
			$board.find(".wrapperIntro .bub1").show(0);
			$board.find(".wrapperIntro.first .text2").show(0);
			$board.find(".wrapperIntro.first .text4").hide(0);
			$board.find(".wrapperIntro.first .text5").show(0);
			//$board.find(".wrapperIntro.first .text3").addClass("slideRight");
			//$board.find(".wrapperIntro.first .bub1").addClass("slideRight");


			$board.find(".wrapperIntro .squirrel-talking01").hide(0);
			$board.find(".wrapperIntro .squirrel-listening01").show(0);
			$board.find(".wrapperIntro .boy-listening01").hide(0);
			$board.find(".wrapperIntro .boy-talking02").show(0);
			$board.find(".wrapperIntro .boy-talking02").css('background','#FFFC7B');
			$board.find(".wrapperIntro .boy-talking02").css('border-color','#FEA92A');





		}

		function first05(){
			$board.find(".wrapperIntro .bub1").hide(0);
			//$board.find(".wrapperIntro .bub2").show(0);
			$board.find(".wrapperIntro.first .text2").show(0);
			$board.find(".wrapperIntro.first .text5").hide(0);
			$board.find(".wrapperIntro.first .text6").show(0);
			$(".black").show(0);
			$(".loku").show(0);
			$board.find(".wrapperIntro.first .square-paint").hide(0);
			//$board.find(".wrapperIntro.first .text4").addClass("slideRight");
			//$board.find(".wrapperIntro.first .bub2").addClass("slideRight");

			/*$board.find(".wrapperIntro .squirrel-talking01").show(0);
			$board.find(".wrapperIntro .squirrel-talking01").css('background','#FFFC7B');
			$board.find(".wrapperIntro .squirrel-talking01").css('border-color','#FEA92A');
			$board.find(".wrapperIntro .boy-listening01").show(0);*/
			$board.find(".wrapperIntro .squirrel-listening01").hide(0);
			$board.find(".wrapperIntro .boy-talking02").hide(0);
		}






		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];

			var html = template(content);
			$board.html(html);
			$nextBtn.hide(0);

			$(".area").show(0);
			$board.find(".wrapperIntro.secondtext .text0").delay(500).fadeIn();
			$(".tri").delay(1000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text1").delay(1500).fadeIn();
			$(".rec").delay(2000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text2").delay(2500).fadeIn();
			$(".squ").delay(3000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text3").delay(3500).fadeIn();
			$(".paral").delay(4000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text4").delay(4500).fadeIn();
			$(".rho").delay(5000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text5").delay(5500).fadeIn();
			$(".kit").delay(6000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text6").delay(6500).fadeIn();
			$(".trap").delay(7000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text7").delay(7500).fadeIn();
			$(".qua").delay(8000).fadeIn();
			$board.find(".wrapperIntro.secondtext .text8").delay(8500).fadeIn();
			$nextBtn.delay(9000).show(0);

		};



		function second01(){
			$board.find(".wrapperIntro.secondtext .text0").hide(0);
			$board.find(".wrapperIntro.secondtext .text1").hide(0);
			$board.find(".wrapperIntro.secondtext .text2").hide(0);
			$board.find(".wrapperIntro.secondtext .text3").hide(0);
			$board.find(".wrapperIntro.secondtext .text4").hide(0);
			$board.find(".wrapperIntro.secondtext .text5").hide(0);
			$board.find(".wrapperIntro.secondtext .text6").hide(0);
			$board.find(".wrapperIntro.secondtext .text7").hide(0);
			$board.find(".wrapperIntro.secondtext .text8").hide(0);
			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".tri" ).animate({ "width": "-=10%", "margin-top": "-=8%" }, {
					duration: 2000,
				});

			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".rec" ).animate({ "width": "-=10%", "left": "-=12%", "margin-top": "-=9%"}, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".squ" ).animate({ "width": "-=10%", "left": "-=24%", "margin-top": "-=8.2%" }, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".paral" ).animate({ "width": "-=10%", "left": "-=35%", "margin-top": "-=8.2%" }, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".rho" ).animate({ "width": "-=10%", "left": "+=46%", "margin-top": "-=30%" }, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".kit" ).animate({ "width": "-=10%", "left": "+=33%", "margin-top": "-=30%"}, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".trap" ).animate({ "width": "-=10%", "left": "+=20%", "margin-top": "-=30%" }, {
					duration: 2000,
				});
			});

			$board.find(".wrapperIntro.secondtext").find(function() {
				$( ".qua" ).animate({ "width": "-=10%", "left": "+=10%", "margin-top": "-=30%" }, {
					duration: 2000,
				});
				$(".back").show(0);
			});
		}




		/*function timer1(){
			var timer = setInterval(function(){
				$("#count_num").html(function(i,html){

					if(parseInt(html)>0)
					   {
					   return parseInt(html)-1;
					   }
					   else
					   {
					   clearTimeout(timer);
					       return "LETS BEGIN";
					   }
					 });

					},1000);
		}*/






		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];

			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$(".area01").show(0);
			$(".wrapperIntro.thirdtext .text0").show(0);
			$(".wrapperIntro.thirdtext .tri").show(0);
			$(".wrapperIntro.thirdtext .rec").show(0);
			$(".wrapperIntro.thirdtext .squ").show(0);
			$(".wrapperIntro.thirdtext .paral").show(0);
			$(".wrapperIntro.thirdtext .rho").show(0);
			$(".wrapperIntro.thirdtext .kit").show(0);
			$(".wrapperIntro.thirdtext .trap").show(0);
			$(".wrapperIntro.thirdtext .qua").show(0);
			$(".wrapperIntro.thirdtext .tri").addClass("bg");
			$board.find(".wrapperIntro.thirdtext .text1").show(0);
			$board.find(".wrapperIntro.thirdtext .firstimage").show(0);

		};


		/*function third01 () {

			$board.find(".wrapperIntro.thirdtext .text1").show(0);
			$board.find(".wrapperIntro.thirdtext .firstimage").show(0);

		}*/

		function third02 () {
			$board.find(".wrapperIntro.thirdtext .text2").show(0);

		}

		function third03 () {
			$board.find(".wrapperIntro.thirdtext .text3").show(0);

		}

		function third04 () {
			$board.find(".wrapperIntro.thirdtext .text4").show(0);

		}

		function third05 () {
			$board.find(".wrapperIntro.thirdtext .text5").show(0);
			$board.find(".wrapperIntro.thirdtext .secondimage").show(0);
		}

		function third06 () {
			$board.find(".wrapperIntro.thirdtext .text6").show(0);

		}
		function third07 () {

			$board.find(".wrapperIntro.thirdtext .text7").show(0);

		}
		function third08 () {
			$board.find(".wrapperIntro.thirdtext .text8").show(0);

		}
		function third09 () {
			$board.find(".wrapperIntro.thirdtext .text9").show(0);

		}
		function third10 () {
			$board.find(".wrapperIntro.thirdtext .text10").show(0);

		}
		function third11 () {
			$board.find(".wrapperIntro.thirdtext .text11").show(0);

		}
		function third12 () {
			$board.find(".wrapperIntro.thirdtext .text12").show(0);

		}
		function third13 () {
			$board.find(".wrapperIntro.thirdtext .text13").show(0);

		}





// first func call
		first();
		$nextBtn.show(0);
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
	$nextBtn.on('click',function () {
		// $(this).css("display","none");
		$prevBtn.show(0);
		countNext++;
		fnSwitcher();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]<countNext) {
				countNext = clickArray[i];
				// console.log(countNext+" = "+clickArray[i]);
				break;
			}
		};
		if (countNext===0) {
			$prevBtn.hide(0);
		};
		fnSwitcher();
	});

	function clickSequence (index) {
		for (var i = clickArray.length - 1; i >= 0; i--) {
			if (clickArray[i]===index) {
				countNext = clickArray[i];
				break;
			}
		};
		fnSwitcher();
	}

	function fnSwitcher () {
		fnArray = [
			first,
			//first00,//first one
			// first01,
			first02,
			first03,
			first04,
			first05,


			second,
			second01,
			//second02,
			/*second03,
			second04,
			second05,
			second06,
			second07,
			second08,
			second09,
			second10,
			second11,
			/*second12,
			/*second13,
			second14,
			second15,*/

			third,
			//third01,
			third02,
			third03,
			third04,
			third05,
			third06,
			third07,
			third08,
			third09,
			third10,
			third11,
			third12,
			third13,


		];


		fnArray[countNext]();

		for (var i = 0; i < clickArray.length; i++) {
			if (clickArray[i] === countNext) {
				loadTimelineProgress($total_page,i+1);
			}
		};

		if (countNext>=all_page) {
			$nextBtn.hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			// $(this).show(0);
			// $nextBtn.show(0);
		}
	}
	/****************/

});

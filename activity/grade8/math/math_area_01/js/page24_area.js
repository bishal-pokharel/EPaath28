/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	var characterDialouges1 = [ {
		diaouges : data.string.p1_1
	},
	
	];

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  		$refreshBtn= $("#activity-page-refresh-btn"),
		countNext = 0,
		all_page=34;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3,7,17,28]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstpage4",
		animate : "true",
		text : [
		data.string.p28_0,
		data.string.p28_1,       
		data.string.p28_2,
		
		
	
	]
	},
	
	{
		justClass : "second",
		
			text : [
					data.string.p28_0,
					data.string.p28_3,
					data.string.p28_4,
					data.string.p28_5,
					data.string.p28_6,
					data.string.p28_7,
					data.string.p28_8,
					
			]
	
	},
	/*------------------------------------------------------------------------------------------------*/
	/*start of third page*/
	{
		justClass : "third",
		
			text : [
					data.string.p29_0,
			    	data.string.p29_1,       
					data.string.p29_2,
					data.string.p29_3,
					data.string.p29_4,
					
	
			]
	
	},
	
	
	{
		justClass : "fourth",
		
			text : [
					data.string.p29_0,
					data.string.p29_5,
					data.string.p29_6,
					
					
			]
	
	},
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "fifth",
		
			text : [
			
				data.string.p30_0,       
				data.string.p30_1,
				data.string.p30_2,
				data.string.p30_3,
				data.string.p30_4,
				
			
				],
	},
	
	
	
	{
		justClass : "sixth",
		
			text : [
						data.string.p30_0,
						data.string.p30_5
			
			
			],
	}
	
	];
	
	
	
	
/******************************************************************************************************/
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
				
				/*appendDialouge = '<div class="p12_1">';
					var dialouges =$.each(characterDialouges1,function(key,values){
					chDialouges = values.diaouges;
				
					appendDialouge +=chDialouges;
				});
				appendDialouge += '</div>';
				$(".des1").append(appendDialouge);*/
			
				
		}
	function first01 () {
		
		$board.find(".wrapperIntro.firstpage4 .text1").show(0);
		
		
	}
	function first02 () {
		
		$board.find(".wrapperIntro.firstpage4 .text2").show(0);
		$board.find(".wrapperIntro.firstpage4 .firstimage").show(0);
	}
	/*function first03 () {
		
		$board.find(".wrapperIntro.firstpage4 .text3").show(0);
		
	}
	function first04 () {
		
		$board.find(".wrapperIntro.firstpage4 .text4").show(0);
		
	}
	
	function first05 () {
			
			$board.find(".wrapperIntro.firstpage4 .text5").show(0);
			
		}

	function first06 () {
		
		$board.find(".wrapperIntro.firstpage4 .text6").show(0);
		
	}

	function first07 () {
		
		$board.find(".wrapperIntro.firstpage4 .text7").show(0);
		
	}

	function first08 () {
		$board.find(".wrapperIntro.firstpage4 .text8").show(0);
		
		//$board.find(".wrapperIntro.firstpage4 .text8").show(0);
		
	}*/

	
	/*second*/
	function second () {
		var source = $("#intro2-template").html();
		var template = Handlebars.compile(source);
		var content = dataObj[1];
		var html = template(content);
		
		$board.html(html);
		$nextBtn.show(0);
			$board.find(".wrapperIntro.second ,.wrapperIntro.second ").show(0);
			$board.find(".wrapperIntro.second .text0").hide(0);
			$board.find(".wrapperIntro.second .text1").show(0);
			$(".letslook").show(0);
	}
	
function second01 () {
	
	
			//$board.find(".wrapperIntro.second .bubble1").show(0);
			//$board.find(".wrapperIntro.second .char-holder .char-a").show(0);
		
			
			//$board.find(".wrapperIntro.second  .char-holder .char-a").addClass("slideRight");
	
}
function second02 () {
			$(".letslook").hide(0);
			$(".black3").show(0);
			$(".loku").show(0);
			$board.find(".wrapperIntro.second .text1").hide(0);
			$board.find(".wrapperIntro.second .text2").delay(1000).show(0);
			$board.find(".wrapperIntro.second .text3").delay(1500).show(0);
			$board.find(".wrapperIntro.second .text4").delay(2000).show(0);
			// $(".nextBtn.myNextStyle").hide(0).delay(3000).show(0);
			$nextBtn.hide(0).delay(3000).show(0);
	
}
function second03 () {
			
			$board.find(".wrapperIntro.second .text2").hide(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").hide(0);
			$board.find(".wrapperIntro.second .text5").show(0);
	
}
function second04 () {
	$(".black3").hide(0);
	$(".loku").hide(0);
	$(".letslook").show(0);
	$board.find(".wrapperIntro.second .text5").hide(0);
	$board.find(".wrapperIntro.second .text6").show(0);
	
}



/* thrid*/

function third () {
	var source = $("#intro3-template").html();
	var template = Handlebars.compile(source);
	var content = dataObj[2]
	var html = template(content);
	
	$board.html(html);
	$nextBtn.show(0);
		
		/*appendDialouge = '<div class="p12_1">';
			var dialouges =$.each(characterDialouges1,function(key,values){
			chDialouges = values.diaouges;
		
			appendDialouge +=chDialouges;
		});
		appendDialouge += '</div>';
		$(".des1").append(appendDialouge);*/
	
		
}
function third01 () {

$board.find(".wrapperIntro.third .text1").show(0);

}
function third02 () {

$board.find(".wrapperIntro.third .text2").show(0);
$board.find(".wrapperIntro.third .firstimage").show(0);
$board.find(".wrapperIntro.third .text3").show(0);
$board.find(".wrapperIntro.third .text4").show(0);

}



function fourth () {
	var source = $("#intro4-template").html();
	var template = Handlebars.compile(source);
	var content = dataObj[3];
	var html = template(content);
	
	$board.html(html);
	$nextBtn.show(0);
	$board.find(".wrapperIntro.fourth").css("background","#fff");
	$board.find(".wrapperIntro.fourth").css("padding","1%");
}

/*function fourth01 () {

$board.find(".wrapperIntro.fourth .text1").show(0);

}*/

function fourth02 () {
$board.find(".wrapperIntro.fourth .solution00").show(0);
$board.find(".wrapperIntro.fourth .text2").css("background","#efefef");

}

function fourth03 () {
	
	$board.find(".wrapperIntro.fourth .solution01").show(0);
	
}

function fourth04 () {

$board.find(".wrapperIntro.fourth .solution02").show(0);

}

function fourth05 () {

$board.find(".wrapperIntro.fourth .solution03").show(0);

}

function fourth06 () {
$board.find(".wrapperIntro.fourth .solution04").show(0);

}

function fourth07 () {
$board.find(".wrapperIntro.fourth .solution05").show(0);

}


function fifth () {
			var source = $("#intro5-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[4];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.fifth").css("background","#fff");
			$board.find(".wrapperIntro.fifth").css("padding","1%");
			//$board.find(".wrapperIntro.fifth .text0").show(0);
		}
		
		
		function fifth01() {
			$board.find(".wrapperIntro.fifth .text1").show(0);
			
		}
		
		function fifth02(){
			$board.find(".wrapperIntro.fifth .text2").show(0);
		}
		
		function fifth03(){
			$board.find(".wrapperIntro.fifth .text3").show(0);
		}
		
		function fifth04(){
			$board.find(".wrapperIntro.fifth .solution00").show(0);
			$board.find(".wrapperIntro.fifth .text4").css("background","#efefef");
			$board.find(".wrapperIntro.fifth .firstimage").show(0);
			
		}
		
		
		function fifth05(){
			$board.find(".wrapperIntro.fifth .solution01").show(0);
		}
		
		
		function fifth06(){
			$board.find(".wrapperIntro.fifth .solution02").show(0);
	
		}
		
		function fifth07(){
			$board.find(".wrapperIntro.fifth .solution03").show(0);
	
		}
		function fifth08(){
			$board.find(".wrapperIntro.fifth .solution04").show(0);
	
		}
		function fifth09(){
			$board.find(".wrapperIntro.fifth .solution05").show(0);
	
		}
		function fifth10(){
			$board.find(".wrapperIntro.fifth .solution06").show(0);
	
		}
		
		function sixth () {
			var source = $("#intro6-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[5];
			var html = template(content);
			$board.html(html);
			$nextBtn.show(0);
			$board.find(".wrapperIntro.sixth").css("background","#fff");
			$board.find(".wrapperIntro.sixth").css("padding","1%");
		}
		
		
		function sixth01(){
			$board.find(".wrapperIntro.sixth .solution00").show(0);
			$board.find(".wrapperIntro.sixth .text1").css("background","#efefef");
		}
		function sixth02(){
			$board.find(".wrapperIntro.sixth .solution01").show(0);
	
		}
		
		function sixth03(){
			$board.find(".wrapperIntro.sixth .solution02").show(0);
	
		}
		
		function sixth04(){
			$board.find(".wrapperIntro.sixth .solution03").show(0);
	
		}
		
		function sixth05(){
			$board.find(".wrapperIntro.sixth .solution04").show(0);
	
		}
		
		function sixth06(){
			$board.find(".wrapperIntro.sixth .solution05").show(0);
	
		}
		
		




        // first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				/*first03,
				first04,
				first05,
				first06,
				first07,
				first08,*/
				
				second,  //first one
				//second01,
				second02,
				second03,
				second04,
				/*second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,*/
				
				
				third,  //first one
				third01,
				third02,
				/*third03,
				third04,
				third05,
				third06,
				third07,
				third08,
				third09,
				third10,
				third11,*/
				
				fourth,
				//fourth01,
				fourth02,
				fourth03,
				fourth04,
				fourth05,
				fourth06,
				fourth07,
				
				fifth,
				fifth01,
				fifth02,
				fifth03,
				fifth04,
				fifth05,
				fifth06,
				fifth07,
				fifth08,
				fifth09,
				fifth10,
				
				sixth,
				sixth01,
				sixth02,
				sixth03,
				sixth04,
				sixth05,
				sixth06
				
				
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

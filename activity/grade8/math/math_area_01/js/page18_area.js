/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {
	
	var characterDialouges1 = [ {
		diaouges : data.string.p1_1
	},
	
	];

	
//------------------------------------------------------------------------------------------------//	
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=10;

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstpage4",
		animate : "true",
		text : [
		data.string.p17_1,       
		data.string.p17_2,
		data.string.p17_3,
		data.string.p17_4,
		data.string.p17_5,
		data.string.p17_6,
		data.string.p17_7,
		data.string.p17_8,
		data.string.p17_9,
		data.string.p17_10,
		data.string.p17_11,
		
		
	
	]
	},
	
	
	
	/*------------------------------------------------------------------------------------------------*/
	/*start of second page*/
	{
		justClass : "secondtext",
		
			text : [
			
	
			]
	
	},
	
	
	/*end of second page */
	
	/*------------------------------------------------------------------------------------------------*/
	
	
	{
		justClass : "thirdtext",
		
			text : [
			
				
			
				],
	},
	
	
	
	{
		justClass : "fourthtext",
		
			text : [
			
			
			],
	}
	
	
	
	
	]
	
	
	
	
/******************************************************************************************************/
	
/*-------------------------------------------------------------------------------------------------*/

/*page3*/	
		
		function first () {
			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0]
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
			
				/*appendDialouge = '<div class="p12_1">';
					var dialouges =$.each(characterDialouges1,function(key,values){
					chDialouges = values.diaouges;
				
					appendDialouge +=chDialouges;
				});
				appendDialouge += '</div>';
				$(".des1").append(appendDialouge);*/
			
				
		}
	function first01 () {
		
		$board.find(".wrapperIntro.firstpage4 ,.wrapperIntro.first ").show(0);
		$board.find(".wrapperIntro.firstpage4 .text1").show(0);
		$board.find(".wrapperIntro.firstpage4 .bubble1").show(0);
		$board.find(".wrapperIntro.firstpage4 .char-holder .char-a").show(0);
		
		//$board.find(".wrapperIntro.firstpage4 .text1").addClass("slideRight");
		//$board.find(".wrapperIntro.firstpage4 .bubble1").addClass("slideRight");
		$board.find(".wrapperIntro.firstpage4  .char-holder .char-a").addClass("slideRight");
		$board.find(".wrapperIntro.firstpage4 .firstimage").show(0);
	}
	
	
	function first02 () {
		$(".letslook").show(0);	
		$board.find(".wrapperIntro.firstpage4 .char-holder .char-a").hide(0);
		$board.find(".wrapperIntro.firstpage4 .bubble1").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text0").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text1").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text2").show(0);
		$board.find(".wrapperIntro.firstpage4 .firstimage").hide(0);
		
	}
	function first03 () {
		$(".letslook").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text0").show(0);
		$board.find(".wrapperIntro.firstpage4 .text1").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text2").hide(0);
		$board.find(".wrapperIntro.firstpage4 .bubble1").hide(0);
		$board.find(".wrapperIntro.firstpage4 .char-holder .char-a").hide(0);
		$board.find(".wrapperIntro.firstpage4 .text3").show(0);
		$board.find(".wrapperIntro.firstpage4 .firstimage").show(0);
		
	}
	function first04 () {
		
		$board.find(".wrapperIntro.firstpage4 .text4").show(0);
		
		
	}
	
	function first05 () {
			
			$board.find(".wrapperIntro.firstpage4 .text5").show(0);
			
		}

	function first06 () {
		
		$board.find(".wrapperIntro.firstpage4 .text6").show(0);
		
	}

	function first07 () {
		
		$board.find(".wrapperIntro.firstpage4 .text7").show(0);
		
	}
	
	function first08 () {
		
		$board.find(".wrapperIntro.firstpage4 .text8").show(0);
		
	}
	
	function first09 () {
		
		$board.find(".wrapperIntro.firstpage4 .text9").show(0);
		
	}
	
	function first10 () {
		
		$board.find(".wrapperIntro.firstpage4 .text10").show(0);
		
	}
	
	
	
	
	
	/*function second () {
			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1]
			var html = template(content);
			
			$board.html(html);
			$nextBtn.show(0);
	
	function second01 () {
		
		$board.find(".wrapperIntro.secondtext .text1").show(0);
		
	}
	function second02 () {
		
		$board.find(".wrapperIntro.secondtext .text2").show(0);
		
	}
	function second03 () {
		
		$board.find(".wrapperIntro.secondtext .text3").show(0);
		
	}
	
	function second04 () {
			
			$board.find(".wrapperIntro.secondtext .text4").show(0);
			
		}

	function second05 () {
		
		$board.find(".wrapperIntro.secondtext .text5").show(0);
		
	}

	function second06 () {
		
		$board.find(".wrapperIntro.secondtext .text6").show(0);
		
	}

	function second07 () {
		$board.find(".wrapperIntro.secondtext .text7").show(0);
	
		
	}
function second08 () {
		
		$board.find(".wrapperIntro.secondtext .text8").show(0);
		
	}*/


		

        // first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,  //first one
				first01,
				first02,
				first03,
				first04,
				first05,
				first06,
				first07,
				first08,
				first09,
				first10,
				
				
				/*second,
				second01,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,*/
				
				
				
			];
			

			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}
	/****************/

});

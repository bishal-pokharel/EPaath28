$(function () {
   
   
   var line1 = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -21 200 200" width="230" class="arrow1">'
		+'<defs> <marker id="arrow" orient="auto" viewBox="0 0 10 10" markerWidth="3" markerHeight="4" markerUnits="strokeWidth" refX="1" refY="5">'
		+'<polyline points="0,0 10,5 0,10 1,5" fill="grey" /> </marker> </defs>'
		+'<path id="line1" marker-end="url(#arrow)" stroke-width="5" fill="none" stroke="red">'
		+'<animate dur="3s" attributeName="d" fill="freeze" values="M10,10 L10,10; M10,10 L162,10;" /></path></svg>';
		
		
	var line2 = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -60 200 200" width="230" class="arrow2">'
		+'<defs> <marker id="arrow" orient="auto" viewBox="0 0 10 10" markerWidth="3" markerHeight="4" markerUnits="strokeWidth" refX="1" refY="5">'
		+'<polyline points="0,0 10,5 0,10 1,5" fill="grey" /> </marker> </defs>'
		+'<path id="line2" marker-end="url(#arrow)" stroke-width="5" fill="none" stroke="blue">'
		+'<animate dur="3s" attributeName="d" fill="freeze" values="M10,10 L10,10; M10,10 L162,10;" /></path></svg>';
   
   
   
   
    var line3 = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -99 200 200" width="230" class="arrow3">'
		+'<defs> <marker id="arrow" orient="auto" viewBox="0 0 10 10" markerWidth="3" markerHeight="4" markerUnits="strokeWidth" refX="1" refY="5">'
		+'<polyline points="0,0 10,5 0,10 1,5" fill="grey" /> </marker> </defs>'
		+'<path id="line3" marker-end="url(#arrow)" stroke-width="5" fill="none" stroke="yellow">'
		+'<animate dur="3s" attributeName="d" fill="freeze" values="M10,10 L10,10; M10,10 L162,10;" /></path></svg>';
		
		
		
		
	var line4 = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 -137 200 200" width="230" class="arrow4">'
		+'<defs> <marker id="arrow" orient="auto" viewBox="0 0 10 10" markerWidth="3" markerHeight="4" markerUnits="strokeWidth" refX="1" refY="5">'
		+'<polyline points="0,0 10,5 0,10 1,5" fill="grey" /> </marker> </defs>'
		+'<path id="line4" marker-end="url(#arrow)" stroke-width="5" fill="none" stroke="green">'
		+'<animate dur="3s" attributeName="d" fill="freeze" values="M10,10 L10,10; M10,10 L162,10;" /></path></svg>';
      
	  
	  $('#submission').click(function(evt) {
         evt.preventDefault();
		
		$('.oval').show(0);
		
		
		var Rvalue= [];
		
		$('input[type="text"]').each(
			function(index){  
				var input = $(this);
				Rvalue.push(input.val());
			}
		);
		console.log(Rvalue);
		var i = 1;
		$(Rvalue).each(function(key,val){
				//console.log(key);	console.log(val);	
				//setTimeout(function(){
					$('#entry'+ (i)).html(val);
				//},1000);
				//setTimeout(function(){
					$('#result'+ (i++)).html(parseInt(val) + 7);
				//},1000);
			
			}
		);
		//console.log(Rvalue);
		
        $("#entry1").delay(1000).show(0);
        $('.line1holder').html(line1).delay(2000).show(0);
		$(".result.oval").delay(3000).show(0);
		$("#result1").delay(4000).show(0);
		
		
		$("#entry2").delay(5000).show(0);
        $('.line2holder').html(line2).delay(6000).show(0);
		$("#result2").delay(7000).show(0);
		
		
		$("#entry3").delay(8000).show(0);
        $('.line3holder').html(line3).delay(9000).show(0);
		$("#result3").delay(10000).show(0);
		
		$("#entry4").delay(11000).show(0);
        $('.line4holder').html(line4).delay(12000).show(0);
		$("#result4").delay(13000).show(0);
		
		

         //TODO: invokeLightBox();
         return false;
      });
	  
 
   });
   

  

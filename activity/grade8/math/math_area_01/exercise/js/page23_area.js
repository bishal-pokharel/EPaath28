$(function () {
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		$total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var qDatas = [
		{base : data.string.p27_5, power :data.string.p27_9, firstlabel : data.string.p27_2, secondlabel : data.string.p27_8,imgs :'cylinder1.png', suffix:'cm<sub>2</sub>', sol :data.string.p2_8},
		{base : data.string.p27_6, power :data.string.p27_10, firstlabel : data.string.p27_2, secondlabel : data.string.p27_8,imgs :'cylinder2.png', suffix:'ft<sub>2</sub>', sol :data.string.p2_8},
		{base : data.string.p27_7, power :data.string.p27_11, firstlabel : data.string.p27_2, secondlabel : data.string.p27_8,imgs :'cylinder3.png', suffix:'cm<sub>2</sub>', sol :data.string.p2_8},
	]

/*
* first
*/
	function init () {
		var source = $("#intro-template").html();
		var template = Handlebars.compile(source);
		var content = {
			yourTurn : data.string.p27_1,
			qTitle : data.string.p27_1,

		}
		var html = template(content);
		$board.html(html);

		first(qDatas[0]);
		$(".youtTurnBoard").attr("src","activity/grade8/math/math_area_01/exercise/images/your_turn.png");
		$(".lokharkeTxt").html(data.string.yourTurn);
	}


	function first(sub) {
		if(countNext == 1){
			$('.q1').hide(0);
		}else if(countNext == 2){
			$('.q2').hide(0);
		}

		var source = $("#qaArea-template").html();
		var template = Handlebars.compile(source);
		var content = {
			check : "check",
			clickToSee : data.string.p27_3,
			firstans : sub.base,
			secondans : sub.power,
			answer : data.string.p27_4,
			text : sub.ansque,
			suffix : sub.suffix,
			firstlabel : sub.firstlabel,
			secondlabel : sub.secondlabel,
			couter : countNext+1,
			image1 : sub.imgs,
			solution: sub.sol,
			check: data.string.check
		}
		var html = template(content);
		$board.find(".qaArea").append(html);
		// $board.find(".qaArea input.base").focus();
	};

	$board.on('click','.clicks .click',function (){
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		$board.find(cls+' .showAns').show(0);
		$nextBtn.show(0);
	})

	$board.on('click','.clicks .check',function () {
		var $this = $(this);
		var num = parseInt($this.data("cls"));
		var cls = ".q"+num;
		var base = $board.find(cls+" input.base").val().toLowerCase();
		var power = $board.find(cls+" input.power").val().toLowerCase();
		// console.log(base+"  "+power);
		var upDatas = qDatas[num-1];
		var check = [null,null];
		var check_empty = [null,null];
		var wrong = "<img src='images/wrong.png'>";
		// wrong.src = "images/wrong.png";
		var right = "<img src='images/correct.png'>";
		// right.src = "images/correct.png";
		if(base === ""){
				check_empty[0] = 1;
			}
			else{
				if (base ===upDatas.base) {
					check[0]=1;
					$board.find(cls+" .check1").html(right);
				} else {
					$board.find(cls+" .check1").html(wrong);
				}
			}

			if(power === ""){
				check_empty[1] = 1;
			}
			else{
				if (power===upDatas.power) {
					check[1]=1;
					$board.find(cls+" .check2").html(right);
				} else {
					$board.find(cls+" .check2").html(wrong);
				}
			}
			if(check_empty[0] ===1 || check_empty[1] === 1){
				swal(data.string.h2);
			}else{
				if (check[0]===1 && check[1]===1) {
					if (countNext>=2) {
						ole.footerNotificationHandler.pageEndSetNotification();
					} else {
						$nextBtn.show(0);
					}
				} else {
					$board.find(cls+" .clicks .click").show(0);
				}
			}

	})

	/*first call to first*/
	// first();
	init(); //initial funtion

	$nextBtn.on('click',function () {
		$(this).hide(0);
		countNext++;

		if (countNext>=$total_page) {
			ole.footerNotificationHandler.pageEndSetNotification();
		} else {
			first(qDatas[countNext]);
		}
		loadTimelineProgress($total_page,countNext+1);
	});

});

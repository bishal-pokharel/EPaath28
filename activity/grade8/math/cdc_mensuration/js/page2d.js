var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var sound_up = new buzz.sound((soundAsset + "up.ogg"));

var content = [
	//trap
	// slide 0
	{
		contentblockadditionalclass: "ole-background-gradient-barley",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_03',
		
		uppertextblockadditionalclass: 'in-header-01 in-header-03 my_font_very_big',
		uppertextblock : [{
			textdata : data.string.p2text20,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'desc-rhom my_font_medium',
		lowertextblock : [{
			textdata : data.string.p2text20_1,
			textclass : ''
		},{
			textdata : data.string.p2text20_2,
			textclass : ''
		}],
		

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "trap-main",
					imgsrc : imgpath + "page2/trapezoiddiy.png",
				},
			],
		}],
	},
	// slide 1
	{
		contentblockadditionalclass: "ole-background-gradient-barley",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_03',
		
		uppertextblockadditionalclass: 'in-header-01 in-header-03 my_font_very_big',
		uppertextblock : [{
			textdata : data.string.p2text20,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'desc-rhom my_font_medium',
		lowertextblock : [{
			textdata : data.string.p2text20_1,
			textclass : ''
		},{
			textdata : data.string.p2text20_2,
			textclass : ''
		}],
		extratextblock : [
		{
			textdata : data.string.p2text23_g,
			textclass : 'form-a1 my_font_medium form-anim',
			splitintofractionsflag: true,
		},
		{
			textdata : data.string.p2text23_h,
			textclass : 'form-a2 my_font_medium form-anim',
			splitintofractionsflag: true,
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "trap-1 trap-1-anim",
					imgsrc : imgpath + "page2/Trapezoid02.png",
				},
				{
					imgclass : "trap-2 trap-2-anim",
					imgsrc : imgpath + "page2/Trapezoid03.png",
				},
				{
					imgclass : "td1 td1-anim",
					imgsrc : imgpath + "page2/td1.png",
				},
				{
					imgclass : "td2 td2-anim",
					imgsrc : imgpath + "page2/td2.png",
				},{
					imgclass : "th th-anim",
					imgsrc : imgpath + "page2/th.png",
				},{
					imgclass : "replay-btn",
					imgsrc : imgpath + "replay.png",
				},
				{
					imgclass : "reset-btn",
					imgsrc : imgpath + "reset.png",
				}
			],
		}],
	},
	// slide 2
	{
		contentblockadditionalclass: "ole-background-gradient-barley",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_03',
		
		extratextblock : [{
			textdata : data.string.p2text20,
			textclass : 'in-header-01 in-header-03 my_font_very_big'
		}],
		uppertextblockadditionalclass: 'side-text-trap my_font_medium',
		uppertextblock : [{
			textdata : data.string.p2text20_6,
			textclass : ''
		},{
			textdata : data.string.p2text20_7,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'fside-text-3 my_font_medium',
		lowertextblock : [{
			textdata : data.string.p2text21,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text22,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text23,
			textclass : '',
			splitintofractionsflag: true,
		}],
		

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "trap-3",
					imgsrc : imgpath + "page2/Trapezoid02.png",
				},
				{
					imgclass : "trap-4",
					imgsrc : imgpath + "page2/Trapezoid03.png",
				},
				{
					imgclass : "td3",
					imgsrc : imgpath + "page2/td1.png",
				},
				{
					imgclass : "td4",
					imgsrc : imgpath + "page2/td2.png",
				},{
					imgclass : "th-1",
					imgsrc : imgpath + "page2/th.png",
				},
			],
		}],
	},
	// slide 3
	{
		contentblockadditionalclass: "ole-background-gradient-barley",
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		// contentblockadditionalclass : 'bg_p2_01',
		
		uppertextblockadditionalclass: 'in-header-01 my_font_very_big',
		uppertextblock : [{
			textdata : data.string.p2text20,
			textclass : ''
		}],
		lowertextblockadditionalclass: 'hint-3 my_font_medium',
		lowertextblock : [{
			textdata : data.string.p2text23_e,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p2text23_f,
			textclass : '',
			splitintofractionsflag: true,
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p2text23_b,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p2text23_c,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p2text23_d,
			optionclass : 'mcq-option opt-3'
		}],
		
		extratextblock : [{
			textdata : data.string.p2text23_a,
			textclass : 'mcq-question',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-purple'
			
		}],

		imageblock : [{
			imagestoshow : [
				{
					imgclass : "diy-trap",
					imgsrc : imgpath + "page2/trapezoiddiy.png",
				}
			],
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var current_sound = sound_up;
	var myTimeout =  null;
	var myTimeout1 =  null;
	var timeouts = [];
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}

	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				nav_button_controls(500);
				break;
			case 1:
				$prevBtn.show(0);
				$('.desc-rhom').fadeOut(500, function(){
					myTimeout1 = setTimeout(function(){
						$('.replay-btn, .reset-btn').fadeIn(100);
					}, 6500);
					$('.replay-btn').click(function(){
						$('.trap-1').removeClass('trap-1-anim');
						$('.trap-2').removeClass('trap-2-anim');
						$('.td1').removeClass('td1-anim');
						$('.td2').removeClass('td2-anim');
						$('.th').removeClass('th-anim');
						$('.form-a1, .form-a2').removeClass('form-anim');
						setTimeout(function(){
							$('.trap-1').addClass('trap-1-anim');
							$('.trap-2').addClass('trap-2-anim');
							$('.td1').addClass('td1-anim');
							$('.td2').addClass('td2-anim');
							$('.th').addClass('th-anim');
							$('.form-a1, .form-a2').addClass('form-anim');
						}, 1);
					});
					$('.reset-btn').click(function(){
						$('.trap-1').removeClass('trap-1-anim');
						$('.trap-2').removeClass('trap-2-anim');
						$('.td1').removeClass('td1-anim');
						$('.td2').removeClass('td2-anim');
						$('.th').removeClass('th-anim');
						$('.form-a1, .form-a2').removeClass('form-anim');
					});
					nav_button_controls(6500);
				});
				break;
			case 3:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.o-'+i).addClass('mcq-pos-'+positions[i-1]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50'});
						ole.footerNotificationHandler.pageEndSetNotification();
						$(this).parent().children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E'});
						
						$(this).parent().children('.incorrect').show(0);
					}
					$('.hint-3').fadeIn(1000);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(500);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	
	function sound_player(sound_data){
		current_sound.stop();
		current_sound = sound_data;
		current_sound.play();
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		clearTimeout(myTimeout1);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

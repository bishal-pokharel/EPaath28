var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'prism-text-01 my_font_medium',
		uppertextblock : [{
			textdata : data.string.p4text31,
			textclass : ''
		}],
		
		prismblock:[{
			prismclass: 'prism-1',
			topclass: '',
			topsrc: imgpath + "prismbase.png",
			bottomclass: '',
			bottomsrc: imgpath + "prismbase2.png",
			side1class: '',
			side2class: '',
			side3class: '',
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'prism-text-02 my_font_medium fade-in-f2',
		uppertextblock : [{
			textdata : data.string.p4text35,
			textclass : ''
		},
		{
			textdata : data.string.p4text37,
			textclass : 'text-form'
		},
		{
			textdata : data.string.p4text38,
			textclass : 'text-form',
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		}],
		
		extratextblock : [{
			textdata : data.string.p4text32,
			textclass : 'p-for-1 my_font_medium fade-in-f',
		},{
			textdata : data.string.p4text33,
			textclass : 'p-for-2 my_font_medium fade-in-f',
		},{
			textdata : data.string.p4text34,
			textclass : 'p-for-3 my_font_medium fade-in-f', 
		}],
		
		prismblock:[{
			prismclass: 'prism-1 p-anim',
			topclass: 'p-t-anim',
			topsrc: imgpath + "prismbase.png",
			bottomclass: 'p-b-anim',
			bottomsrc: imgpath + "prismbase2.png",
			side1class: 'p-s1-anim',
			side2class: 'p-s2-anim',
			side3class: 'p-s3-anim',
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		extratextblock : [{
			textdata : data.string.p4text39,
			textclass : 'prism-text-03 my_font_big fade-in-f2',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-blue'
		},{
			textdata : data.string.p4text32,
			textclass : 'p-for-1 my_font_medium fade-out-1',
		},{
			textdata : data.string.p4text33,
			textclass : 'p-for-2 my_font_medium fade-out-1',
		},{
			textdata : data.string.p4text34,
			textclass : 'p-for-3 my_font_medium fade-out-1', 
		},{
			textdata : data.string.dela,
			textclass : 'dela my_font_medium', 
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ar-h-1",
					imgsrc : imgpath + "arrow_h.png",
				},
			],
		}],
		prismblock:[{
			prismclass: 'prism-1 prismblock2',
			topclass: 'p-t-anim1',
			topsrc: imgpath + "prismbase.png",
			bottomclass: 'p-b-anim1',
			bottomsrc: imgpath + "prismbase2.png",
			side1class: 'p-s1-anim1',
			side2class: 'p-s2-anim1',
			side3class: 'p-s3-anim1',
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
	
		uppertextblockadditionalclass: 'center-text my_font_ultra_big',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : ''
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "mcq-shape-1",
					imgsrc : imgpath + "diyprism.png",
				},
			],
		}],
		
		extratextblock : [
		{
			textdata : data.string.p4text41,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-purple'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p4text42,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p4text43,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p4text44,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block my_font_medium',
		uppertextblock : [{
			textdata : data.string.p4text48,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p4text49,
			textclass : '',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		},{
			textdata : data.string.p4text50,
			textclass : '',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		},{
			textdata : data.string.p4text51,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p4text52,
			textclass : 'hint-text-frac',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p4text53,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		extratextblock : [{
			textdata : data.string.p4text54,
			textclass : 'prism-text-03 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-blue'
		},{
			textdata : data.string.dela,
			textclass : 'dela-1 my_font_medium', 
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ar-h-2",
					imgsrc : imgpath + "arrow_h.png",
				},
				{
					imgclass : "abc-arrow-1",
					imgsrc : imgpath + "arrow_a.png",
				},{
					imgclass : "abc-arrow-2",
					imgsrc : imgpath + "arrow_b.png",
				},{
					imgclass : "abc-arrow-3",
					imgsrc : imgpath + "arrow_c.png",
				},
			],
		}],
		prismblock:[{
			prismclass: 'prism-1 prismblock2',
			topclass: '',
			topsrc: imgpath + "prismbase.png",
			bottomclass: '',
			bottomsrc: imgpath + "prismbase2.png",
			side1class: '',
			side2class: '',
			side3class: '',
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var myTimeout =  null;
	var timeouts = [];
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$nextBtn.show(0);
				break;
			case 1:
			case 2:
				$prevBtn.show(0);
				nav_button_controls(10000);
				break;
			case 4:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.o-'+i).addClass('mcq-pos-'+positions[i-1]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50'});
						$nextBtn.show(0);
						$(this).parent().children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E'});
						
						$(this).parent().children('.incorrect').show(0);
					}
					$('.hint-block').fadeIn(1000);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
				
	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

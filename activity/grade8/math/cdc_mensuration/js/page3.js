var imgpath = $ref + "/images/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-01',
	
		uppertextblockadditionalclass: 'header-pstart my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text1,
			textclass : ''
		}],
	},
	
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-bb'
		}],
		uppertextblockadditionalclass: 'brown-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text2,
			textclass : ''
		}],
	},

	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-bb'
		}],
		uppertextblockadditionalclass: 'brown-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.circle,
			textclass : 'text-purple inside-header'
		},{
			textdata : data.string.p3text3,
			textclass : ''
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-bb circle-color-fade'
		}],
		uppertextblockadditionalclass: 'brown-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text4,
			textclass : ''
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-bb circle-1'
		},{
			textdata : '',
			textclass : 'small-dot small-dot-anim'
		},{
			textdata : '',
			textclass : 'small-dot-initial'
		},{
			textdata : '',
			textclass : 'new-diameter'
		},{
			textdata : data.string.d,
			textclass : 'new-diameter-text my_font_big'
		}],
		uppertextblockadditionalclass: 'bottom-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text5,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-1'
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : '',
			textclass : 'bottom-line'
		},
		{
			textdata : '',
			textclass : 'rad-line r1'
		},
		{
			textdata : '',
			textclass : 'rad-line r2'
		},
		{
			textdata : '',
			textclass : 'rad-line r3'
		},{
			textdata : data.string.p3text9,
			textclass : 'formula-1 my_font_big its_hidden',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},],
		circleblock:[{
			containscanvaselement: true,
			circleclass: 'circle-canvas circle-2-anim', 
			diameterclass : 'diameter',
			dotclass : 'diameter-dot'
		}],
		
		uppertextblockadditionalclass: 'bottom-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text7,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-1'
		}],
	},
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : data.string.p3text9,
			textclass : 'formula-1 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},{
			textdata : data.string.d,
			textclass : 'added-d-1 my_font_big',
		}],
		circleblock:[{
			circleclass: 'circle-2 circle-2-anim', 
			diameterclass : 'diameter',
			dotclass : 'diameter-dot'
		}],
		
		uppertextblockadditionalclass: 'bottom-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text8,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-1'
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : data.string.p3text9,
			textclass : 'formula-1 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},
		{
			textdata : data.string.p3text11,
			textclass : 'formula-2 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},{
			textdata : data.string.d,
			textclass : 'added-d-1 my_font_big',
		}],
		circleblock:[{
			circleclass: 'circle-3 ', 
			diameterclass : 'diameter',
		}],
		
		uppertextblockadditionalclass: 'bottom-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text10,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-1',
			splitintofractionsflag: true,
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : data.string.p3text9,
			textclass : 'formula-1 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},
		{
			textdata : data.string.p3text11,
			textclass : 'formula-2 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},
		{
			textdata : data.string.p3text13,
			textclass : 'formula-3 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},{
			textdata : data.string.d,
			textclass : 'added-d-1 my_font_big',
		},{
			textdata : data.string.r,
			textclass : 'added-r-1 my_font_big',
		}],
		circleblock:[{
			circleclass: 'circle-3 ', 
			diameterclass : 'diameter',
			dotclass : 'center-dot',
			radiusclass : 'radius',
		}],
		
		uppertextblockadditionalclass: 'bottom-textblock my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p3text12,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-1',
			splitintofractionsflag: true,
		}],
	},
	
	//slide9
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-diy',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-diy'
		}],
		uppertextblockadditionalclass: 'center-text my_font_ultra_big',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : ''
		}],
	},
	//slide10
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
		
		circleblock:[{
			circleclass: 'circle-4 ', 
			dotclass : 'center-dot',
			radiusclass : 'radius',
		}],
		
		extratextblock : [{
			textdata : data.string.p3text18,
			textclass : 'mcq-question-1 my_font_big',
		},{
			textdata : data.string.p3text14,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-green'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p3text15,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p3text16,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p3text17,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block my_font_medium',
		uppertextblock : [{
			textdata : data.string.p3text19,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text20,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text21,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text22,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
	//slide11
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : data.string.p3text23,
			textclass : 'find-area my_font_big',
		},{
			textdata : data.string.r,
			textclass : 'added-r-2 my_font_big',
		}],
		circleblock:[{
			circleclass: 'circle-5 ', 
			dotclass : 'center-dot',
			radiusclass : 'radius',
		}],
	},
	
	//slide12
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
		
		uppertextblockadditionalclass: 'circle-ins my_font_medium',
		uppertextblock : [{
			textdata : data.string.p3text37,
			textclass : '',
			splitintofractionsflag: true,
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "added-r-pi",
					imgsrc : imgpath + "pir.png",
				},
				{
					imgclass : "added-r-3",
					imgsrc : imgpath + "r.png",
				},
				{
					imgclass : "bigcircle",
					imgsrc : imgpath + "c1.png",
				},
			],
		}],
	},
	//slide13
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
		
		uppertextblockadditionalclass: 'circle-ins-new my_font_medium',
		uppertextblock : [{
			textdata : data.string.p3text38,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text39,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text40,
			textclass : '',
			splitintofractionsflag: true,
		}],
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "added-r-pi",
					imgsrc : imgpath + "pir.png",
				},
				{
					imgclass : "added-r-3",
					imgsrc : imgpath + "r.png",
				},
				{
					imgclass : "bigcircle",
					imgsrc : imgpath + "c1.png",
				},
			],
		}],
	},
	//slide14
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-04',
	
		extratextblock : [{
			textdata : '',
			textclass : 'circle-diy-2'
		}],
		uppertextblockadditionalclass: 'center-text-2 my_font_ultra_big',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : ''
		}],
	},
	
	//slide15
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
		
		circleblock:[{
			circleclass: 'circle-4 ', 
			dotclass : 'center-dot',
			radiusclass : 'radius',
		}],
		
		extratextblock : [{
			textdata : data.string.p3text28,
			textclass : 'mcq-question-1 my_font_big',
		},{
			textdata : data.string.p3text24,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-green'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p3text25,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p3text26,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p3text27,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block my_font_medium',
		uppertextblock : [{
			textdata : data.string.p3text29,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text30,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text31,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p3text32,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
	//slide16
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p3-03',
	
		extratextblock : [{
			textdata : data.string.p3text33,
			textclass : 'lformula-1 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},
		{
			textdata : data.string.r,
			textclass : 'added-r-1 my_font_big',
		},
		{
			textdata : data.string.p3text35,
			textclass : 'lformula-2 my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-2'
		},],
		circleblock:[{
			circleclass: 'circle-3 ', 
			dotclass : 'center-dot',
			radiusclass : 'radius',
		}],
		
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	
	var canvas;
	var ctx;
	var g_color = '#33A7FC';
	
	var cwidth = 0;
	var cheight = 0;
	var cradius = 0;
	var dx = 0;
	var dw = 0.5;
	var dl = 2.5;
	var dr = 0;
	var ccenter = [0,0];
	
	var myTimeout =  null;
	var timeoutvar =  null;
	var timeouts = [];
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 5:
				$prevBtn.show(0);
				canvas = document.getElementById("circle-peel");
				ctx = canvas.getContext("2d");

				dw =0.5;
				dl = 2.5;
				dr = 0;
				dx = 0;
				g_angle1 = 1/2*Math.PI;
				g_angle2 = 5/2*Math.PI;
				
				init_canvas();
				global_animation = requestAnimationFrame(repeatOften);
				myTimeout= setTimeout(function(){
					$('.circle-canvas').hide(0);
					$('.r1').show(0);
					$('.r1').css('transform', 'rotate(-90deg)');
					myTimeout = setTimeout(function(){
						$('.r2').show(0);
						$('.r2').css('transform', 'rotate(-90deg)');
						myTimeout = setTimeout(function(){
							$('.r3').show(0);
							$('.r3').css('transform', 'rotate(-90deg)');
							myTimeout = setTimeout(function(){
								$('.formula-1').show(0);
								$nextBtn.show(0);
							}, 2000);
						}, 2000);
					}, 2000);
				}, 8500);
				break;
			case 10:
			case 15:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.o-'+i).addClass('mcq-pos-'+positions[i-1]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50'});
						$nextBtn.show(0);
						$(this).parent().children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E'});
						
						$(this).parent().children('.incorrect').show(0);
					}
					$('.hint-block').fadeIn(1000);
				});
				break;
			case 12:
				$prevBtn.show(0);
				make_vbig();
				function make_small(){
					var circlehtml1 = '<img src=' + imgpath + '25.png class="smallcircle squarter-1 sq1-';
					var circlehtml2 = '<img src=' + imgpath + '25.png class="smallcircle squarter-2 sq2-';
					var circlehtml3 = '<img src=' + imgpath + '25.png class="smallcircle squarter-3 sq3-';
					var circlehtml4 = '<img src=' + imgpath + '25.png class="smallcircle squarter-4 sq4-';
					var dist = 2.5*Math.PI/3.6;
					for(var i=0; i<=35; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.sq1-'+i).css({
							'transform': 'rotate('+ (-2.5+i*(-2.5)) +'deg)',
							'transition': '2s linear',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.sq2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*2.5)) +'deg)',
							'transition': '2s linear',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.sq3-'+i).css({
							'transform': 'rotate('+ (177.5-(i*2.5)) +'deg)',
							'transition': '2s linear',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.sq4-'+i).css({
							'transform': 'rotate('+ i*2.5 +'deg)',
							'transition': '2s linear',
						});
					}
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						for(var i=0; i<=36; i++){
							$('.sq1-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (35-i)*dist +'%) rotate(-91.25deg)',
							});
							$('.sq2-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ -(36-i)*dist +'%) rotate(-91.25deg)',
							});
							$('.sq3-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2-(36-i)*dist) +'%) rotate(88.75deg)',
							});
							$('.sq4-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2+(35-i)*dist) +'%) rotate(88.75deg)',
							});
						}
					}, 500));
					
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.squarter-1, .squarter-2').css({
							'top': '40%',
						});
						$('.squarter-4, .squarter-3').css({
							'top': '20%',
						});
						nav_button_controls(2000);
					}, 3000));
				}
				function make_vbig(){
					var circlehtml1 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-1 q1-';
					var circlehtml2 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-2 q2-';
					var circlehtml3 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-3 q3-';
					var circlehtml4 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-4 q4-';
					var dist = 45*Math.PI/3.6;
					for(var i=0; i<=1; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.q1-'+i).css({
							'transform': 'rotate('+ (-45+i*(-45)) +'deg)',
							'transition': '2s linear',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.q2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*45)) +'deg)',
							'transition': '2s linear',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.q3-'+i).css({
							'transform': 'rotate('+ (135-(i*45)) +'deg)',
							'transition': '2s linear',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.q4-'+i).css({
							'transform': 'rotate('+ i*45 +'deg)',
							'transition': '2s linear',
						});
					}
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						for(var i=0; i<=1; i++){
							$('.q1-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (1-i)*dist +'%) rotate(-112.5deg)',
							});
							$('.q2-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ -(2-i)*dist +'%) rotate(-112.5deg)',
							});
							$('.q3-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2-(2-i)*dist) +'%) rotate(67.5deg)',
							});
							$('.q4-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2+(1-i)*dist) +'%) rotate(67.5deg)',
							});
						}
					}, 500));
					
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.quarter-1, .quarter-2').css({
							'top': '40%',
						});
						$('.quarter-4, .quarter-3').css({
							'top': '21%',
						});
					}, 3000));
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.quarter-1, .quarter-2').css({
							'top': '4%',
						});
						$('.quarter-4, .quarter-3').css({
							'top': '-15%',
						});
					}, 5500));
					timeouts.push(setTimeout(function(){
						make_med();
					}, 8000));
				}
				function make_med(){
					var circlehtml1 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-1 mq1-';
					var circlehtml2 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-2 mq2-';
					var circlehtml3 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-3 mq3-';
					var circlehtml4 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-4 mq4-';
					var dist = 15*Math.PI/3.6;
					for(var i=0; i<=5; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.mq1-'+i).css({
							'transform': 'rotate('+ (-15+i*(-15)) +'deg)',
							'transition': '2s linear',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.mq2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*15)) +'deg)',
							'transition': '2s linear',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.mq3-'+i).css({
							'transform': 'rotate('+ (165-(i*15)) +'deg)',
							'transition': '2s linear',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.mq4-'+i).css({
							'transform': 'rotate('+ i*15 +'deg)',
							'transition': '2s linear',
						});
					}
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						for(var i=0; i<=5; i++){
							$('.mq1-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (5-i)*dist +'%) rotate(-97.5deg)',
							});
							$('.mq2-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ -(6-i)*dist +'%) rotate(-97.5deg)',
							});
							$('.mq3-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2-(6-i)*dist) +'%) rotate(82.5deg)',
							});
							$('.mq4-'+i).css({
								'left': '55%',
								'transform': 'translateX('+ (dist/2+(5-i)*dist) +'%) rotate(82.5deg)',
							});
						}
					}, 500));
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.mquarter-1, .mquarter-2').css({
							'top': '40%',
						});
						$('.mquarter-4, .mquarter-3').css({
							'top': '20%',
						});
					}, 3000));
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.mquarter-1, .mquarter-2').css({
							'top': '75%',
						});
						$('.mquarter-4, .mquarter-3').css({
							'top': '55%',
						});
					}, 5500));
					timeouts.push(setTimeout(function(){
						make_small();
					}, 8000));
					
				}
				break;
			case 13:
				$prevBtn.show(0);
				make_vbig2();
				function make_small2(){
					var circlehtml1 = '<img src=' + imgpath + '25.png class="smallcircle squarter-1 sq1-';
					var circlehtml2 = '<img src=' + imgpath + '25.png class="smallcircle squarter-2 sq2-';
					var circlehtml3 = '<img src=' + imgpath + '25.png class="smallcircle squarter-3 sq3-';
					var circlehtml4 = '<img src=' + imgpath + '25.png class="smallcircle squarter-4 sq4-';
					var dist = 2.5*Math.PI/3.6;
					for(var i=0; i<=35; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.sq1-'+i).css({
							'transform': 'rotate('+ (-2.5+i*(-2.5)) +'deg)',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.sq2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*2.5)) +'deg)',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.sq3-'+i).css({
							'transform': 'rotate('+ (177.5-(i*2.5)) +'deg)',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.sq4-'+i).css({
							'transform': 'rotate('+ i*2.5 +'deg)',
						});
					}
					for(var i=0; i<=36; i++){
						$('.sq1-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (35-i)*dist +'%) rotate(-91.25deg)',
						});
						$('.sq2-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ -(36-i)*dist +'%) rotate(-91.25deg)',
						});
						$('.sq3-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2-(36-i)*dist) +'%) rotate(88.75deg)',
						});
						$('.sq4-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2+(35-i)*dist) +'%) rotate(88.75deg)',
						});
					}
					
					$('.squarter-1, .squarter-2').css({
						'top': '40%',
					});
					$('.squarter-4, .squarter-3').css({
						'top': '20%',
					});
					timeouts.push(setTimeout(function(){
						// $('.smallcircle').addClass('anim-q1-1');
						$('.c-1, .c-2').fadeOut(1000, function(){
							$('.circle-ins-new').fadeIn(1000);
							$('.added-r-pi, .added-r-3').fadeIn(1000, function(){
								$nextBtn.show(0);
							});
						});
					}, 1000));
				}
				function make_vbig2(){
					var circlehtml1 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-1 q1-';
					var circlehtml2 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-2 q2-';
					var circlehtml3 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-3 q3-';
					var circlehtml4 = '<img src=' + imgpath + '45.png class="smallcircle c-1 quarter-4 q4-';
					var dist = 45*Math.PI/3.6;
					for(var i=0; i<=1; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.q1-'+i).css({
							'transform': 'rotate('+ (-45+i*(-45)) +'deg)',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.q2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*45)) +'deg)',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.q3-'+i).css({
							'transform': 'rotate('+ (135-(i*45)) +'deg)',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.q4-'+i).css({
							'transform': 'rotate('+ i*45 +'deg)',
						});
					}
					for(var i=0; i<=1; i++){
						$('.q1-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (1-i)*dist +'%) rotate(-112.5deg)',
						});
						$('.q2-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ -(2-i)*dist +'%) rotate(-112.5deg)',
						});
						$('.q3-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2-(2-i)*dist) +'%) rotate(67.5deg)',
						});
						$('.q4-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2+(1-i)*dist) +'%) rotate(67.5deg)',
						});
					}
					// $('.smallcircle').addClass('anim-q1-1');
					$('.quarter-1, .quarter-2').css({
						'top': '4%',
					});
					$('.quarter-4, .quarter-3').css({
						'top': '-15%',
					});
					make_med2();
				}
				function make_med2(){
					var circlehtml1 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-1 mq1-';
					var circlehtml2 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-2 mq2-';
					var circlehtml3 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-3 mq3-';
					var circlehtml4 = '<img src=' + imgpath + '15.png class="smallcircle c-2 mquarter-4 mq4-';
					var dist = 15*Math.PI/3.6;
					for(var i=0; i<=5; i++){
						//q1
						$('.bigcircle').parent().append(circlehtml1+i+'"/>');
						$('.mq1-'+i).css({
							'transform': 'rotate('+ (-15+i*(-15)) +'deg)',
						});
						//q2
						$('.bigcircle').parent().append(circlehtml2+i+'"/>');
						$('.mq2-'+i).css({
							'transform': 'rotate('+ (-179.999+(i*15)) +'deg)',
						});
						//q3
						$('.bigcircle').parent().append(circlehtml3+i+'"/>');
						$('.mq3-'+i).css({
							'transform': 'rotate('+ (165-(i*15)) +'deg)',
						});
						//q4
						$('.bigcircle').parent().append(circlehtml4+i+'"/>');
						$('.mq4-'+i).css({
							'transform': 'rotate('+ i*15 +'deg)',
						});
					}
					for(var i=0; i<=5; i++){
						$('.mq1-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (5-i)*dist +'%) rotate(-97.5deg)',
						});
						$('.mq2-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ -(6-i)*dist +'%) rotate(-97.5deg)',
						});
						$('.mq3-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2-(6-i)*dist) +'%) rotate(82.5deg)',
						});
						$('.mq4-'+i).css({
							'left': '55%',
							'transform': 'translateX('+ (dist/2+(5-i)*dist) +'%) rotate(82.5deg)',
						});
					}
					$('.mquarter-1, .mquarter-2').css({
						'top': '75%',
					});
					$('.mquarter-4, .mquarter-3').css({
						'top': '55%',
					});
					make_small2();
				}
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	function init_canvas(){
		cwidth = $('.circle-canvas').width();
		cheight = $('.circle-canvas').height();
		ccenter = [cwidth/2,cheight/2];
		canvas.height = cheight;
		canvas.width = cwidth;
		cradius = cwidth/2.05;
		ctx.lineWidth=cwidth/50;
		ctx.clearRect(0, 0, cwidth, cheight);
	}
	function draw_sector(angle1, angle2, contexta, color){
		contexta.clearRect(0, 0, cwidth, cheight);
		contexta.moveTo(0,0);
		contexta.beginPath();
		contexta.strokeStyle = color;
		contexta.arc(ccenter[0], ccenter[1], cradius, angle1, angle2);
		contexta.stroke();
	}
	
	
	var fps = 8;
	var time_init = 0; 
	var time_elapsed = 0; 
	var time_then = 0;
	time_then = Date.now();
	function repeatOften() {
		global_animation = requestAnimationFrame(repeatOften);
		time_init = Date.now();
		time_elapsed = time_init - time_then;
		if (time_elapsed > 150/fps)
		{
			time_then = time_init - (time_elapsed % (1000/fps));
			draw_sector(g_angle1, g_angle2-dx, ctx, g_color);
			if(dx < g_angle2-g_angle1){
				dx += 0.015;
				dw += 0.154;
				dl += 0.155;
				dr += .86;
				$('.circle-canvas').css({'left': dl+'%', 'transform':'rotate('+ dr +'deg)'});
				$('.bottom-line').css('width', dw+'%');
				if(dx > g_angle2-g_angle1){
					draw_sector(g_angle1, g_angle1, ctx, g_color);
					cancelAnimationFrame(global_animation);	
					// ctx.save();
				}
			} else {
				draw_sector(g_angle1, g_angle1, ctx, g_color);
				cancelAnimationFrame(global_animation);	
			}
		}						
	}
	
	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		for (var i=0; i<timeouts.length; i++) {
			clearTimeout(timeouts[i]);
		}
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		for (var i=0; i<timeouts.length; i++) {
			clearTimeout(timeouts[i]);
		}
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

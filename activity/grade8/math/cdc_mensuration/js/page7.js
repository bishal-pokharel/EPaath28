var imgpath = $ref + "/images/";
var soundAsset = $ref+"/sounds/";

var content = [
	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		uppertextblockadditionalclass: 'header-cyl my_font_big',
		uppertextblock : [{
			textdata : data.string.p6text1,
			textclass : ''
		}],
		
		prismblock:[{
			prismclass: 'prism-1',
			topclass: '',
			bottomclass: '',
		}],
	},
	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'header-cyl my_font_big',
		uppertextblock : [{
			textdata : data.string.p6text2,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-pink'
		}],
		
		prismblock:[{
			prismclass: 'prism-1',
			topclass: '',
			bottomclass: '',
		}],
	},
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'side-cyl my_font_medium fade_in-1',
		uppertextblock : [{
			textdata : data.string.p6text3,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-pink'
		},{
			textdata : data.string.p6text4,
			textclass : '',
		}],
		extratextblock : [{
			textdata : data.string.p6text9,
			textclass : 'bt-area my_font_medium fade_in-1',
		}],
		prismblock:[{
			prismclass: 'prism-1 cyl-move-1',
			topclass: '',
			bottomclass: '',
		}],
	},
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'side-cyl my_font_medium',
		uppertextblock : [{
			textdata : data.string.p6text5,
			textclass : '',
		},{
			textdata : data.string.p6text6,
			textclass : '',
		},{
			textdata : data.string.p6text7,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		}],
		extratextblock : [{
			textdata : data.string.p6text9,
			textclass : 'bt-area my_font_medium',
		},{
			textdata : data.string.p6text8,
			textclass : 'bt-h my_font_medium fade_in-1',
		}],
		prismblock:[{
			prismclass: 'prism-2',
			topclass: '',
			bottomclass: '',
			extradisc:[{
				extraclass: 'disc-1 fade_in-1'
			}]
		}],
	},
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'side-cyl my_font_medium',
		uppertextblock : [{
			textdata : data.string.p6text10,
			textclass : '',
		},{
			textdata : data.string.p6text11,
			textclass : '',
		},{
			textdata : data.string.p6text12,
			textclass : '',
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		}],
		extratextblock : [{
			textdata : data.string.p6text9,
			textclass : 'bt-area my_font_medium',
		},{
			textdata : data.string.p6text13,
			textclass : 'bt-h2 my_font_medium fade_in-1',
		}],
		prismblock:[{
			prismclass: 'prism-2',
			topclass: '',
			bottomclass: '',
			extradisc:[{
				extraclass: 'disc-2 fade_in-1'
			}]
		}],
	},
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'side-cyl my_font_medium',
		uppertextblock : [{
			textdata : data.string.p6text14,
			textclass : 'align-center',
		}],
		extratextblock : [{
			textdata : data.string.p6text16,
			textclass : 'bt-area my_font_medium',
		},{
			textdata : data.string.p6text15,
			textclass : 'bt-h3 my_font_medium',
		},{
			textdata : data.string.p6text17,
			textclass : 'cyl-con my_font_medium fade_in-21',
		}],
		imageblock : [{
			imagestoshow : [
			{
				imgclass : "cyl-h",
				imgsrc : imgpath + "page2/th.png",
			},
			],
		}],
		prismblock:[{
			prismclass: 'prism-2',
			topclass: '',
			bottomclass: '',
		}],
	},
	
	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		extratextblock : [{
			textdata : data.string.p6text18,
			textclass : 'cly-text-03 my_font_very_big',
		}],
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "ar-h-2",
					imgsrc : imgpath + "arrow_h.png",
				},
				{
					imgclass : "abc-arrow-1",
					imgsrc : imgpath + "arrow_a.png",
				},{
					imgclass : "abc-arrow-2",
					imgsrc : imgpath + "arrow_b.png",
				},{
					imgclass : "abc-arrow-3",
					imgsrc : imgpath + "arrow_c.png",
				},
			],
		}],
		prismblock:[{
			prismclass: 'prism-2',
			topclass: '',
			bottomclass: '',
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
	
		uppertextblockadditionalclass: 'center-text my_font_ultra_big',
		uppertextblock : [{
			textdata : data.string.diytext,
			textclass : ''
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "mcq-shape-1",
					imgsrc : imgpath + "diyprism.png",
				},
			],
		}],
		
		extratextblock : [
		{
			textdata : data.string.p6text21,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-purple'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p6text22,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p6text23,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p6text24,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block my_font_medium',
		uppertextblock : [{
			textdata : data.string.p6text28,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p6text29,
			textclass : '',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		},{
			textdata : data.string.p6text30,
			textclass : '',
			splitintofractionsflag: true,
			datahighlightflag : true,
			datahighlightcustomclass : 'vis-hidden'
		},{
			textdata : data.string.p6text31,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p6text32,
			textclass : 'hint-text-frac',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p6text33,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	var myTimeout =  null;
	var timeouts = [];
	
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$nextBtn.show(0);
				break;
			case 1:
				function timer_stack(i){
					var cyl_html1 = '<img class="prismbase {{extraclass}}" src="activity/grade8/math/cdc_mensuration/images/prismunit.png" style="z-index:' ;
					var cyl_html2 = '; top:';
					var cyl_html3 = '%"></img>';
					timeouts.push(setTimeout(function(){
						$('.prismblock').append(cyl_html1+ i +cyl_html2+ (10-i)*10 +cyl_html3);
					}, i*250));
				}
				for( var m=1; m< 10; m++){
					timer_stack(m);
				}
				nav_button_controls(2000);
				break;
			case 5:
				$prevBtn.show(0);
				nav_button_controls(1000);
				var cyl_html1 = '<div class="cylinderdisc" style="z-index:' ;
				var cyl_html2 = '; top:';
				var cyl_html3 = '%"></div>';
				for( var m=1; m< 10; m++){
					$('.cylinderblock').append(cyl_html1+ i +cyl_html2+ (10-i)*10 +cyl_html3);
				}
				break;
			case 8:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.o-'+i).addClass('mcq-pos-'+positions[i-1]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50'});
						ole.footerNotificationHandler.lessonEndSetNotification();
						$(this).parent().children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E'});
						
						$(this).parent().children('.incorrect').show(0);
					}
					$('.hint-block').fadeIn(1000);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
				
	function nav_button_controls(delay_ms){
		myTimeout = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(myTimeout);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(myTimeout);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

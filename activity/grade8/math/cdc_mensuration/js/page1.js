var imgpath = $ref + "/images/";

var content = [

	//slide0
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
	
		uppertextblock : [{
			textdata : data.lesson.chapter,
			textclass : 'lesson-title sniglet'
		}],
	},

	//slide1
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'blue_heading my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text1,
			textclass : ''
		}],
		
		secblock:[{
			secblock: 'sec-1',
			shapeclass: 'triangleshape',
			div:[{
				divclass: 'trioutline tri-1',
				divdata: data.string.a,
				labelclass: 'trilabel-a'
			},{
				divclass: 'trioutline tri-2',
				divdata: data.string.b,
				labelclass: 'trilabel-b'
			},{
				divclass: 'trioutline tri-3',
				divdata: data.string.c,
			}],
		},
		{
			secblock: 'sec-2',
			shapeclass: 'rectshape',
			div:[{
				divclass: 'recoutine rect-1',
			},{
				divclass: 'recoutine rect-2',
			},{
				divclass: 'recoutine rect-3',
				divdata : data.string.l,
			},{
				divclass: 'recoutine rect-4',
				divdata : data.string.b,
				labelclass: 'rectlabel-b'
			}],
		},
		{
			secblock: 'sec-3',
			shapeclass: 'sqshape',
			div:[{
				divclass: 'sqoutline sq-1',
			},{
				divclass: 'sqoutline sq-2',
			},{
				divclass: 'sqoutline sq-3',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-4',
			}]
		}],
	},
	
	//slide2
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'blue_heading my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : ''
		}],
		svgblock: [
		{
			svgclass: "svg-tri",
			text: 'P'
		}],
		secblock:[{
			secblock: 'sec-1 expand-1',
			shapeclass: 'triangleshape shape-center',
			div:[{
				divclass: 'trioutline tri-1 triflip-1',
				divdata: data.string.a,
				labelclass: 'trilabel-a'
			},{
				divclass: 'trioutline tri-2',
				divdata: data.string.b,
				labelclass: 'trilabel-b'
			},{
				divclass: 'trioutline tri-3 triflip-2',
				divdata: data.string.c,
			}],
			text:[{
				textdata : data.string.p1text3,
				textclass : 'blue-formula'
			}]
		},
		{
			secblock: 'sec-2 squeeze-2',
			shapeclass: 'rectshape',
			div:[{
				divclass: 'recoutine rect-1',
			},{
				divclass: 'recoutine rect-2',
			},{
				divclass: 'recoutine rect-3',
				divdata : data.string.l,
			},{
				divclass: 'recoutine rect-4',
				divdata : data.string.b,
				labelclass: 'rectlabel-b'
			}],
		},
		{
			secblock: 'sec-3 squeeze-2',
			shapeclass: 'sqshape',
			div:[{
				divclass: 'sqoutline sq-1',
			},{
				divclass: 'sqoutline sq-2',
			},{
				divclass: 'sqoutline sq-3',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-4',
			}]
		}],
	},
		
	//slide3
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'blue_heading my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : ''
		}],
		svgblock: [
		{
			svgclass: "svg-rect",
			text: 'P'
		}],
		secblock:[{
			secblock: 'sec-1 squeeze-1',
			shapeclass: 'triangleshape',
			div:[{
				divclass: 'trioutline tri-1',
				divdata: data.string.a,
				labelclass: 'trilabel-a'
			},{
				divclass: 'trioutline tri-2',
				divdata: data.string.b,
				labelclass: 'trilabel-b'
			},{
				divclass: 'trioutline tri-3',
				divdata: data.string.c,
			}],
		},
		{
			secblock: 'sec-2 expand-1',
			shapeclass: 'rectshape shape-center-2',
			div:[{
				divclass: 'recoutine rect-1',
				divdata : data.string.l,
			},{
				divclass: 'recoutine rect-2 recflip-1',
				divdata : data.string.b,
			},{
				divclass: 'recoutine rect-3 recflip-2',
				divdata : data.string.l,
			},{
				divclass: 'recoutine rect-4 recflip-3',
				divdata : data.string.b,
			}],
			text:[{
				textdata : data.string.p1text4,
				textclass : 'blue-formula'
			}]
		},
		{
			secblock: 'sec-3 squeeze-2',
			shapeclass: 'sqshape',
			div:[{
				divclass: 'sqoutline sq-1',
			},{
				divclass: 'sqoutline sq-2',
			},{
				divclass: 'sqoutline sq-3',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-4',
			}]
		}],
	},
	
	//slide4
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : '',
		
		uppertextblockadditionalclass: 'blue_heading my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text2,
			textclass : ''
		}],
		svgblock: [
		{
			svgclass: "svg-sq",
			text: 'P'
		}],
		secblock:[{
			secblock: 'sec-1 squeeze-1',
			shapeclass: 'triangleshape',
			div:[{
				divclass: 'trioutline tri-1',
				divdata: data.string.a,
				labelclass: 'trilabel-a'
			},{
				divclass: 'trioutline tri-2',
				divdata: data.string.b,
				labelclass: 'trilabel-b'
			},{
				divclass: 'trioutline tri-3',
				divdata: data.string.c,
			}],
		},
		{
			secblock: 'sec-2 squeeze-1',
			shapeclass: 'rectshape',
			div:[{
				divclass: 'recoutine rect-1',
			},{
				divclass: 'recoutine rect-2',
			},{
				divclass: 'recoutine rect-3',
				divdata : data.string.l,
			},{
				divclass: 'recoutine rect-4',
				divdata : data.string.b,
				labelclass: 'rectlabel-b'
			}],
		},
		{
			secblock: 'sec-3 expand-1',
			shapeclass: 'sqshape shape-center-3',
			div:[{
				divclass: 'sqoutline sq-1',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-2 sqflip-1',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-3 sqflip-2',
				divdata: data.string.l,
			},{
				divclass: 'sqoutline sq-4 sqflip-3',
				divdata: data.string.l,
			}],
			
			text:[{
				textdata : data.string.p1text5,
				textclass : 'blue-formula'
			}]
		}],
	},
	
	//slide5
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'turquoise',
		
		uppertextblockadditionalclass: 'dd-heading my_font_big sniglet',
		uppertextblock : [{
			textdata : data.string.p1text29,
			textclass : ''
		}],
		draggableblock:[{
			dragclass: 'drag-1',
			textdata: data.string.p1text7,
			splitintofractionsflag: true,
		},{
			dragclass: 'drag-2',
			textdata: data.string.p1text8,
			splitintofractionsflag: true,
		},{
			dragclass: 'drag-3',
			textdata: data.string.p1text9,
			splitintofractionsflag: true,
		}],
		
		droppableblock:[{
			dropclass: 'drop-1 my_font_medium'
		},{
			dropclass: 'drop-2 my_font_medium'
		},{
			dropclass: 'drop-3 my_font_medium'
		}],
		extratextblock : [{
			textdata : data.string.b,
			textclass : 'label-b-diy my_font_medium',
		},{
			textdata : data.string.b_2,
			textclass : 'label-b-diy-2 my_font_medium',
			splitintofractionsflag: true,
		}],
		imageblock : [{
			imagestoshow : [
				// {
					// imgclass : "diy-shape diy-shape-1",
					// imgsrc : imgpath + "01.png",
				// },
				{
					imgclass : "diy-shape diy-shape-2",
					imgsrc : imgpath + "02.png",
				},
				{
					imgclass : "diy-shape diy-shape-3",
					imgsrc : imgpath + "03.png",
				},
				{
					imgclass : "ar-shape-4",
					imgsrc : imgpath + "tp-02.png",
				},
				{
					imgclass : "ar-shape-5 tl-anim-4",
					imgsrc : imgpath + "tp-01.png",
				},
				{
					imgclass : "ar-shape-6 tl-anim-5",
					imgsrc : imgpath + "tp-03.png",
				}
			],
		}],
	},

	//slide6
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p1',
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "mcq-shape-2",
					imgsrc : imgpath + "triangle.png",
				}
			],
		}],
		extratextblock : [{
			textdata : data.string.p1text10,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-green'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p1text13,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p1text14,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p1text15,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block my_font_medium',
		uppertextblock : [{
			textdata : data.string.p1text16,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1text17,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
	//slide7
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p1',
		
		imageblock : [{
			imagestoshow : [
				{
					imgclass : "mcq-shape-1",
					imgsrc : imgpath + "rectangle.png",
				}
			],
		}],
		extratextblock : [{
			textdata : data.string.p1text19,
			textclass : 'mcq-question my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'text-green'
		}],
		optionblock : [{
			optionblockclass: 'mcq-options my_font_big o-1',
			textdata : data.string.p1text20,
			optionclass : 'mcq-option opt-1'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-2',
			textdata : data.string.p1text21,
			optionclass : 'mcq-option opt-2'
		},
		{
			optionblockclass: 'mcq-options my_font_big o-3',
			textdata : data.string.p1text22,
			optionclass : 'mcq-option opt-3'
		}],
		uppertextblockadditionalclass: 'hint-block left-13 my_font_medium',
		uppertextblock : [{
			textdata : data.string.p1text23,
			textclass : '',
			splitintofractionsflag: true,
		},{
			textdata : data.string.p1text24,
			textclass : '',
			splitintofractionsflag: true,
		}],
	},
	//slide8
	{
		hasheaderblock : false,
		contentblocknocenteradjust : true,
		contentblockadditionalclass : 'bg-p1-01',
		
		
		extratextblock : [{
			textdata : data.string.p1text26,
			textclass : 'p1-last-text my_font_big',
			datahighlightflag : true,
			datahighlightcustomclass : 'hl-sp'
		}],
	}
];

$(function() {

	var $board = $(".board");
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;
	var $total_page = content.length;
	
	var myTimeout =  null;
	var timeoutvar =  null;
	var vocabcontroller =  new Vocabulary();
	vocabcontroller.init();
	/*
	 inorder to use the handlebar partials we need to register them
	 to their respective handlebar partial pointer first
	 */
	Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	// controls the navigational state of the program
	// next btn is disabled for this page
	function navigationController(islastpageflag) {
		typeof islastpageflag === "undefined" ? islastpageflag = false : typeof islastpageflag != 'boolean' ? alert("NavigationController : Hi Master, please provide a boolean parameter") : null;
		// if lastpageflag is true
		// islastpageflag ? ole.footerNotificationHandler.lessonEndSetNotification() : ole.footerNotificationHandler.pageEndSetNotification();
	}
	
	function splitintofractions($splitinside){
		typeof $splitinside !== "object" ?
		alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
		null ;

		var $splitintofractions = $splitinside.find("*[data-splitintofractions ='true']");
		if($splitintofractions.length > 0){
			$.each($splitintofractions, function(index, value){
				$this = $(this);
				var tobesplitfraction = $this.html();
				if($this.hasClass('fraction')){
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span>');
				}else{
					tobesplitfraction = tobesplitfraction.replace(/::/g, '<span class="fraction2"><span class="top">');
					tobesplitfraction = tobesplitfraction.replace(/;;/g, '</span></span>');
				}
				
				
				tobesplitfraction = tobesplitfraction.replace(/_\/_/g, '</span><span class="bottom">');	        	
				$this.html(tobesplitfraction);
			});	
		}
	}
	
	function generalTemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		is_last_page= false;

		$board.html(html);
		loadTimelineProgress($total_page, countNext + 1);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		splitintofractions($board);
		vocabcontroller.findwords(countNext);
		
		switch (countNext) {
			case 0:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
			case 2:
				$('.arrow-b-l').attr({'x1':'-145'});
				$('.arrow-h-l').attr({'points':'-152,10.147 -128.834,20.295 -132.941,10.147 -128.834,0.002'});
				$('.arrow-b-r').attr({'x1':'478'});
				$('.arrow-h-r').attr({'points':'495.466,10.147 470.632,0 476.525,10.147 470.632,20.292'});
				$prevBtn.show(0);
				myTimeout = setTimeout( function(){
					$('.triflip-1>p, .triflip-2>p').css('transform', 'rotate(180deg)');
					$('.svg-tri').fadeIn(1000, function(){
						$('.blue-formula').fadeIn(1000, function(){
							$nextBtn.show(0);
						});
					});
				}, 2500);
				break;
			case 3:
				$('.arrow-b-l').attr({'x1':'-88'});
				$('.arrow-h-l').attr({'points':'-97,10.147 -72.834,20.295 -78.941,10.147 -72.834,0.002'});
				$('.arrow-b-r').attr({'x1':'444'});
				$('.arrow-h-r').attr({'points':'450.466,10.147 415.632,0 421.525,10.147 415.632,20.292'});
				$prevBtn.show(0);
				myTimeout = setTimeout( function(){
					$('.recflip-1>p, .recflip-3>p').css('transform', 'rotate(-180deg) scale(0.625)');
					$('.rect-3>p').css('transform', 'rotate(-180deg)');
					$('.rect-1>p').css('top', '100%');
					$('.svg-rect').fadeIn(1000, function(){
						$('.blue-formula').fadeIn(1000, function(){
							$nextBtn.show(0);
						});
					});
				}, 2500);
				break;
			case 4:
				$('.arrow-b-l').attr({'x1':'-120'});
				$('.arrow-h-l').attr({'points':'-127,10.147 -103.834,20.295 -107.941,10.147 -103.834,0.002'});
				$('.arrow-b-r').attr({'x1':'463'});
				$('.arrow-h-r').attr({'points':'475.466,10.147 450.632,0 456.525,10.147 450.632,20.292'});
				$prevBtn.show(0);
				myTimeout = setTimeout( function(){
					$('.sq-2>p, .sq-3>p, .sq-4>p').css('transform', 'rotate(-180deg)');
					$('.sq-1>p').css('top', '100%');
					$('.svg-sq').fadeIn(1000, function(){
						$('.blue-formula').fadeIn(1000, function(){
							$nextBtn.show(0);
						});
					});
				}, 2500);
				break;
			case 5:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.drag-'+i).addClass('pos-'+positions[i-1]);
				}
				drop_count =0;
				$( '.dragclass' ).draggable({
					containment: ".board",
					cursor: "move",
					revert: "invalid",
					appendTo: "body",
					zIndex: '200',
					start: function( event, ui ){
						$(this).addClass('selected');
					},
					stop: function( event, ui ){
						$(this).removeClass('selected');
					}
				});
				$('.drop-1').droppable({
						accept : ".drag-1",
						drop:function(event, ui) {	
							drop_text_to_box(ui.draggable, $(this));
							$('.ar-shape-5').addClass('tl-anim-2');
							$('.ar-shape-6').addClass('tl-anim-3');
							$('.label-b-diy').fadeOut(1000, function(){
								$('.label-b-diy-2').fadeIn(1000);
							});
						}
				});
				$('.drop-2').droppable({
						accept : ".drag-2",
						drop:function(event, ui) {	
							drop_text_to_box(ui.draggable, $(this));
						}
				});
				$('.drop-3').droppable({
						accept : ".drag-3",
						drop:function(event, ui) {	
							drop_text_to_box(ui.draggable, $(this));
						}
				});
				function drop_text_to_box(dropped, droppedOn){
					drop_count++;
					dropped.draggable('disable');
					dropped.addClass('dropped');
					dropped.detach().css({
						'pointer-events': 'none',
						'top': '0%',
						'left': '0%',
						'width': '100%',
						'margin': '0'
					}).appendTo(droppedOn);
					if(drop_count >=3){
						nav_button_controls(0);
					}
				}
				break;
			case 6:
			case 7:
				$prevBtn.show(0);
				var positions = [1,2,3];
				positions.shufflearray();
				for(var i=1; i<4; i++){
					$('.o-'+i).addClass('mcq-pos-'+positions[i-1]);
				}
				$('.option').click(function(){
					if($(this).hasClass('opt-1')){
						$('.option').css({'pointer-events': 'none'});
						play_correct_incorrect_sound(1);
						$(this).css({'background-color': '#4CAF50'});
						$nextBtn.show(0);
						$(this).parent().children('.correct').show(0);
					} else{
						play_correct_incorrect_sound(0);
						$(this).css({'pointer-events': 'none',
									'background-color': '#D0553E'});
						
						$(this).parent().children('.incorrect').show(0);
					}
					$('.hint-block').fadeIn(1000);
				});
				break;
			default:
				$prevBtn.show(0);
				nav_button_controls(100);
				break;
		}
	}
	
	function nav_button_controls(delay_ms){
		timeoutvar = setTimeout(function(){
			if(countNext==0){
				$nextBtn.show(0);
			} else if( countNext>0 && countNext == $total_page-1){
				$prevBtn.show(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else{
				$prevBtn.show(0);
				$nextBtn.show(0);
			}
		},delay_ms);
	}
	

	function templateCaller() {
		//convention is to always hide the prev and next button and show them based
		//on the convention or page index
		$prevBtn.hide(0);
		$nextBtn.hide(0);
		navigationController();

		generalTemplate();
	}


	$nextBtn.on("click", function() {
		clearTimeout(timeoutvar);
		switch(countNext){
			default:
				countNext++;
				templateCaller();
				break;
		}
		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on("click", function() {
		clearTimeout(timeoutvar);
		clearTimeout(myTimeout);
		countNext--;
		templateCaller();
	});

	total_page = content.length;
	templateCaller();
	
});



/*===============================================
 =            data highlight function            =
 ===============================================*/
function texthighlight($highlightinside) {
	//check if $highlightinside is provided
	typeof $highlightinside !== "object" ? alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") : null;

	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var stylerulename;
	var replaceinstring;
	var texthighlightstarttag;
	var texthighlightendtag = "</span>";

	if ($alltextpara.length > 0) {
		$.each($alltextpara, function(index, val) {
			/*if there is a data-highlightcustomclass attribute defined for the text element
			 use that or else use default 'parsedstring'*/
			$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
			( stylerulename = $(this).attr("data-highlightcustomclass")) : ( stylerulename = "parsedstring");

			texthighlightstarttag = "<span class = " + stylerulename + " >";

			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);

			$(this).html(replaceinstring);
		});
	}
}

/*=====  End of data highlight function  ======*/

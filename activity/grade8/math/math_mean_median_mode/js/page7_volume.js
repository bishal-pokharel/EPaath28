/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/
	var vdopath = $ref+"/videos/";
		var vid_play_pause;

		//for play and pause video

	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }
	}

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board');
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=9;
		$nextBtn.show();
		
		/*to register a partial*/
		Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p7_1,
		data.string.p7_2,
		data.string.p7_3,

		data.string.p7_4,
		data.string.p7_5,
		data.string.p7_6,

		data.string.p7_7,
		data.string.p7_8,
		data.string.p7_9,

		data.string.p7_10,
		data.string.p7_11,
		data.string.p7_12,
		data.string.p7_13,
		data.string.p7_14,
		data.string.p7_15,
		data.string.p7_16,

		data.string.p7_17,
		data.string.p7_18,
		data.string.p7_19,
		data.string.p7_20,
		data.string.p7_21,
		data.string.p7_22,
		data.string.p7_23,
		data.string.p7_24,
		data.string.p7_25,
		//data.string.p7_26,
		//data.string.p7_27,
		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "piechart",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"piechart.mp4",
					},
				]
			},
		]
	},


	]




/******************************************************************************************************/
	/*
	* first
	*/


		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};

		 function first02() {

		 	 $(".wrapperIntro.firstPage .text0").hide(0);
		 	 $(".wrapperIntro.firstPage .text1").hide(0);
		 	 $(".wrapperIntro.firstPage .text2").hide(0);
		 	 $(".wrapperIntro.firstPage .text3").show(0);
			 $("#blue").hide(0);
		 	  $("#black").hide(0);
		 	  $("#pink").hide(0);
		 	  $("#orange").hide(0);
		 }

		 function first03() {

		 	 $(".wrapperIntro.firstPage .text4").show(0);

		 }

		 function first04() {

		 	 $(".wrapperIntro.firstPage .text5").show(0);
			 $("#door2").hide(0);
		 	  $("#door3").hide(0);
		 	  $("#door4").hide(0);
		 	  $("#door5").hide(0);
			  $(".line_graph").show(0);
		 }

		 function first05() {

			 $(".wrapperIntro.firstPage .text6").show(0);
			  $(".line_graph").hide(0);
			 $(".circle_graph").show(0);
		 }

		  function first06() {

		 	 $(".wrapperIntro.firstPage .text7").show(0);
			 $(".filled_graph").show(0);
		  }

		  function first07(){

			$(".wrapperIntro.firstPage .text8").show(0);
			  //$("#blue").hide(0);
		 	  //$("#black").hide(0);
		 	  //$("#pink").hide(0);
		 	  //$("#orange").hide(0);

		 };



		  function first08() {
		  	 $(".wrapperIntro.firstPage .text3").hide(0);
		 	 $(".wrapperIntro.firstPage .text4").hide(0);
		 	 $(".wrapperIntro.firstPage .text5").hide(0);
		 	 $(".wrapperIntro.firstPage .text6").hide(0);
		 	 $(".wrapperIntro.firstPage .text7").hide(0);
		 	 $(".wrapperIntro.firstPage .text8").hide(0);


		 	 $(".wrapperIntro.firstPage .text9").show(0);
		 	 $(".wrapperIntro.firstPage .text10").show(0);
		 	 $(".wrapperIntro.firstPage .text11").show(0);
		 	 $(".wrapperIntro.firstPage .text12").show(0);
		 	 $(".wrapperIntro.firstPage .text13").show(0);
		 	 $(".wrapperIntro.firstPage .text14").show(0);
		 	 $(".wrapperIntro.firstPage .text15").show(0);

		 	  $(".straw").show(0);
		 	  $(".burg").show(0);
		 	  $(".wm").show(0);
		 	  $(".ba").show(0);


		 	  $(".door").hide(0);
		 	  $(".line_graph").hide(0);
		 	  $(".circle_graph").hide(0);
		 	  $(".filled_graph").hide(0);
		 	  $(".filled_graph").hide(0);
		 };

		 function first09() {

		 	 $(".wrapperIntro.firstPage .text9").hide(0);
		 	 $(".wrapperIntro.firstPage .text10").hide(0);
		 	 $(".wrapperIntro.firstPage .text11").hide(0);
		 	 $(".wrapperIntro.firstPage .text12").hide(0);
		 	 $(".wrapperIntro.firstPage .text13").hide(0);
		 	 $(".wrapperIntro.firstPage .text14").hide(0);
		 	 $(".wrapperIntro.firstPage .text15").hide(0);

		 	  $(".straw").hide(0);
		 	  $(".burg").hide(0);
		 	  $(".wm").hide(0);
		 	  $(".ba").hide(0);

		 	 $(".wrapperIntro.firstPage .text16").show(0);
		 	 $(".wrapperIntro.firstPage .text17").show(0);
		 	 $(".wrapperIntro.firstPage .text18").show(0);
		 	 $(".wrapperIntro.firstPage .text19").show(0);
		 	 $(".wrapperIntro.firstPage .text20").show(0);
		 	 $(".wrapperIntro.firstPage .text21").show(0);
		 	 $(".wrapperIntro.firstPage .text22").show(0);
		 	 $(".wrapperIntro.firstPage .text23").show(0);
		 };

		 function first10() {


		 	 $(".wrapperIntro.firstPage .text16").hide(0);
		 	 $(".wrapperIntro.firstPage .text17").hide(0);
		 	 $(".wrapperIntro.firstPage .text18").hide(0);
		 	 $(".wrapperIntro.firstPage .text19").hide(0);
		 	 $(".wrapperIntro.firstPage .text20").hide(0);
		 	 $(".wrapperIntro.firstPage .text21").hide(0);
		 	 $(".wrapperIntro.firstPage .text22").hide(0);
		 	 $(".wrapperIntro.firstPage .text23").hide(0);

			 $(".wrapperIntro.firstPage .text24").show(0);
			 $(".replay").show(0);
			 $(".videoblock").show(0);

		 };








		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");

/*----------------------------------------------------------------------------------------------------*/


		// first func call
		first();
		$(".color").draggable({
			  revert:true
			});
		$(".door").droppable({
		  drop: function(e, ui) {
		    console.log(ui.draggable)
		    $(this).css("background-color", ui.draggable.attr("id"));
		  }
		});


	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
			    first02,
			    first03,
			    first04,
				first05,
				first06,
				first07,
				first08,
				first09,
				first10
			 //    first04,
			 //    first05
			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

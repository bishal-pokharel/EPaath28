/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=6;
		$nextBtn.show();

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,3,5]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p5_1,
		data.string.p5_2,
		data.string.p5_3,
		data.string.p5_4,
		data.string.p5_5,
		data.string.p5_6,
		],
	},


	{

		justClass : "second",
		animate : "true",
		text : [
		data.string.p5_7,
		data.string.p5_8,
		data.string.p5_9,
		data.string.p5_10,
		data.string.p5_11,
		data.string.p5_12,

		],
	},


	{

		justClass : "third",
		animate : "true",
		text : [
		data.string.p5_12_1,
		data.string.p5_12_2,
		//data.string.p5_11,
		data.string.p5_13,
		data.string.p5_14,
		data.string.p5_15,
		data.string.p5_16,
		data.string.p5_17,
		data.string.p5_18

		],
	},


	{

		justClass : "fourth",
		animate : "true",
		text : [

		data.string.p5_13,
		data.string.p5_19,
		data.string.p5_20,
		data.string.p5_21,
		data.string.p5_22,
		data.string.p5_23,
		data.string.p5_24


		],
	},


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.firstPage .text0").show(0);

		};

		 function first02() {
			$board.find(".wrapperIntro.firstPage .text1").show(0);
		 	/* $(".wrapperIntro.firstPage .text3 .fifteen").addClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").addClass("class_fifteen");*/

		 	// $board.find(".wrapperIntro.firstPage .text9").show(0);
		  //   $(".wrapperIntro.firstPage .text2 .blink18").addClass("addclassto18");
		  //   $(".wrapperIntro.firstPage .text4 .blink18").addClass("addclassto18");
		  //   $(".wrapperIntro.firstPage .text9 .blink18").addClass("addclassto18");

		 };

		 function first03() {
			 $board.find(".wrapperIntro.firstPage .text2").show(0);
			 $board.find(".wrapperIntro.firstPage .text3").show(0);
			 $(".nextBtn.myNextStyle").hide(0);
		 	 /*$(".wrapperIntro.firstPage .text3 .twenty").addClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").addClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");*/
		 	// $board.find(".wrapperIntro.firstPage .text9").show(0);
		  //   $(".wrapperIntro.firstPage .text2 .blink18").addClass("addclassto18");
		  //   $(".wrapperIntro.firstPage .text4 .blink18").addClass("addclassto18");
		  //   $(".wrapperIntro.firstPage .text9 .blink18").addClass("addclassto18");

		 };

		/*function first04() {
			//$board.find(".wrapperIntro.firstPage .text4").show(0);




		 };*/

		 /*function first05() {

			$(".wrapperIntro.firstPage .text3 .fifteen").addClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").addClass("class_fifteen");


		 }*/

		 /*function first06() {
			$(".wrapperIntro.firstPage .text3 .twenty").addClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").addClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
		 	/*$board.find(".wrapperIntro.firstPage .text6").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text7").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text8").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text9").hide(0);
		 	$board.find(".table_diy").hide(0);

		 };*/



		 /* function first07() {

		 	$board.find(".wrapperIntro.firstPage .text6").show(0);
		 	$board.find(".wrapperIntro.firstPage .text7").show(0);
		 	$board.find(".wrapperIntro.firstPage .text8").show(0);
		 	$board.find(".wrapperIntro.firstPage .text9").show(0);
		 	$board.find(".table_diy").show(0);

		 };*/


		function second() {

			var source = $("#intro2-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);


/*disable characters and allow only numeric 0-9*/

$(document).ready(function() {
    $(".width_input, .width_input1, .width_input2, .width_input3, .width_input4, .width_input5, .width_input6, .width_input7").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

/*disable characters*/
	$('.width_input').keyup(function () {
    var inputvalue = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue ==2) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("firstvalue").disabled = true;

			}
				else{
					document.getElementById("firstvalue").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width_input1').keyup(function () {
    var inputvalue1 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue1 ==3) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("secondvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("secondvalue").disabled = false;
				}
			});

	$('.width_input2').keyup(function () {
    var inputvalue2 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue2 ==5) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("thirdvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("thirdvalue").disabled = false;
				}
			});

	$('.width_input3').keyup(function () {
    var inputvalue3 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue3 ==6) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fourthvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("fourthvalue").disabled = false;
				}
			});

	$('.width_input4').keyup(function () {
    var inputvalue4 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue4 ==7) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fifthvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("fifthvalue").disabled = false;
				}
			});

	$('.width_input5').keyup(function () {
    var inputvalue5 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue5 ==2) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("sixthvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("sixthvalue").disabled = false;
				}
			});

	$('.width_input6').keyup(function () {
    var inputvalue6 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue6 ==7) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("seventhvalue").disabled = true;
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("seventhvalue").disabled = false;
				}
			});

	$('.width_input7').keyup(function () {
    var inputvalue7 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (inputvalue7 ==4) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("eighthvalue").disabled = true;
			// $nextBtn.show(0);
			}
				else{
					$(this).addClass('incorrect');
					document.getElementById("eighthvalue").disabled = false;
				}
			});



		};

		function second01(){
			$board.find(".wrapperIntro.second .text1").show(0);
		 	$board.find(".wrapperIntro.second .text2").show(0);
		 	$board.find(".wrapperIntro.second .text3").show(0);
		 	//$board.find(".wrapperIntro.second .text4").show(0);
			//$board.find(".wrapperIntro.second .text5").show(0);
		 	$board.find(".table_diy").show(0);
			$(".nextBtn.myNextStyle").hide(0);
		}

		function third() {

			var source = $("#intro3-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.third .text0").show(0);

			$board.find(".table_diy2").show(0);
			$(".first").hide(0).delay(4000).show(0);
			$('.two').each(function() {
				var elem = $(this);
				elem.fadeOut(1000)
					.fadeIn(1000)
					.fadeOut(1000)
					.fadeIn(1000);
			});

			$('.first').each(function() {
				var elem = $(this);
				elem.delay(1000)
					.fadeOut(1000)
					.fadeIn(1000)
					.fadeOut(1000)
					.fadeIn(1000);
			});

			$(".second").hide(0).delay(9000).show(0);
			$('.three').each(function() {
				var elem = $(this);
				elem.delay(5000)
					.fadeOut(1000)
					.fadeIn(1000)
					.fadeOut(1000)
					.fadeIn(1000);
			});

			$('.second01').each(function() {
				var elem = $(this);
				elem.delay(10000)
					.fadeOut(1000)
					.fadeIn(1000)
					.fadeOut(1000)
					.fadeIn(1000);
			});

			$('.five').each(function() {
				var elem = $(this);
				elem.delay(10000)
					.fadeOut(1000)
					.fadeIn(1000)
					.fadeOut(1000)
					.fadeIn(1000);
			});

			$(".third3").hide(0).delay(14000).show(0);
			$(".width_input_one").delay(14500).show(0);
			$board.find(".wrapperIntro.third .text1").delay(14500).show(0);
			//$(".width_input_one").delay(15000).show(0);
			$(".nextBtn.myNextStyle").hide(0);

	$(document).ready(function() {
    $(".width_input_one, .width_input_two, .width_input_three, .width_input_four").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

			$('.width_input_one').keyup(function () {
				var inputvalue01 = $(this).val();
				$(this).val($(this).val().replace(/[^0-90.]/g,''));

			if (inputvalue01 ==10 || inputvalue01 ==6) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("secondvalue").disabled = true;
			$(".plus").show(0);
			$('.width_input_two').show(0);
			}
				else{
					document.getElementById("secondvalue").disabled = false;
					$(this).addClass('incorrect');
				}
			});



			var lolz = $('.width_input_one');
			$('.width_input_two').keyup(function () {
				var inputvalue02 = $(this).val();
				$(this).val($(this).val().replace(/[^0-90.]/g,''));

			if ((lolz.val() == 10 && inputvalue02 ==6) || (lolz.val() == 6 && inputvalue02 ==10)) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("thirdvalue").disabled = true;
			$(".sixteen").show(0);
			$(".width_input_three").show(0);

			}
				else{
					document.getElementById("thirdvalue").disabled = false;
					$(this).addClass('incorrect');
				}
			});

			$('.width_input_three').keyup(function () {
				var inputvalue03 = $(this).val();
				$(this).val($(this).val().replace(/[^0-90.]/g,''));

			if (inputvalue03 ==16 || inputvalue03 ==7 ) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fourthvalue").disabled = true;
			$(".plus1").show(0);
			$(".width_input_four").show(0);
			}
				else{
					document.getElementById("fourthvalue").disabled = false;
					$(this).addClass('incorrect');
				}
			});


			var lol = $('.width_input_three');
			$('.width_input_four').keyup(function () {
				var inputvalue04 = $(this).val();
				$(this).val($(this).val().replace(/[^0-90.]/g,''));

			if ((lol.val() == 16 && inputvalue04 ==7) || (lol.val() == 7 && inputvalue04 ==16)) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fifthvalue").disabled = true;
			$(".twenty-three1").show(0);
			$(".cf1").delay(500).fadeIn();
			$(".cf2").delay(1000).fadeIn();
			$(".cf3").delay(1500).fadeIn();
			$board.find(".wrapperIntro.third .text1").hide(0);
			$(".nextBtn.myNextStyle").delay(2000).fadeIn();

			}
				else{
					document.getElementById("fifthvalue").disabled = false;
					$(this).addClass('incorrect');

				}
			});
		};

		function third01(){

		 	$board.find(".wrapperIntro.third .text2").show(0);
		 	$board.find(".wrapperIntro.third .text3").show(0);
		 	$board.find(".wrapperIntro.third .text4").show(0);
			$board.find(".wrapperIntro.third .text5").show(0);
			$board.find(".wrapperIntro.third .text6").show(0);
			$board.find(".wrapperIntro.third .text7").show(0);
			//$(".nextBtn.myNextStyle").show(0);
		}




		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");

/*----------------------------------------------------------------------------------------------------*/


		// first func call
		first();

		$("#first").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").addClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").addClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
			 $board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#second").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").addClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").addClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
				$board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#third").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").addClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").addClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
				$board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#fourth").click(function(){
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").addClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").addClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
			 $board.find(".wrapperIntro.firstPage .text5").show(0);

		 });

		 $("#fifth").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").addClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").addClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
				$board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#sixth").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .thirteen").addClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").addClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
				$board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#seventh").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").addClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").addClass("class_seventeen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").removeClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").removeClass("class_eighteen");
			$board.find(".wrapperIntro.firstPage .text5").show(0);
		 });

		 $("#eighth").click(function(){
			 $(".wrapperIntro.firstPage .text3 .fifteen").removeClass("class_fifteen");
		 	 $(".wrapperIntro.firstPage .text5 .fifteen").removeClass("class_fifteen");
			 $(".wrapperIntro.firstPage .text3 .eighteen").addClass("class_eighteen");
		 	 $(".wrapperIntro.firstPage .text5 .eighteen").addClass("class_eighteen");
			 $(".wrapperIntro.firstPage .text3 .twenty").removeClass("class_twenty");
		 	 $(".wrapperIntro.firstPage .text5 .twenty").removeClass("class_twenty");
			 $(".wrapperIntro.firstPage .text3 .twenty-three").removeClass("class_twenty-three");
		 	 $(".wrapperIntro.firstPage .text5 .twenty-three").removeClass("class_twenty-three");
			 $(".wrapperIntro.firstPage .text3 .nineteen").removeClass("class_nineteen");
		 	 $(".wrapperIntro.firstPage .text5 .nineteen").removeClass("class_nineteen");
			 $(".wrapperIntro.firstPage .text3 .twelve").removeClass("class_twelve");
		 	 $(".wrapperIntro.firstPage .text5 .twelve").removeClass("class_twelve");
			 $(".wrapperIntro.firstPage .text3 .thirteen").removeClass("class_thirteen");
		 	 $(".wrapperIntro.firstPage .text5 .thirteen").removeClass("class_thirteen");
			 $(".wrapperIntro.firstPage .text3 .seventeen").removeClass("class_seventeen");
		 	 $(".wrapperIntro.firstPage .text5 .seventeen").removeClass("class_seventeen");
			 $board.find(".wrapperIntro.firstPage .text5").show(0);
			 $(".nextBtn.myNextStyle").show(0);

		 });





		/*$(".wrapperIntro.firstPage .text8").mouseover(function(){
			$(".wrapperIntro.firstPage .text10").show(0);
			$(".wrapperIntro.firstPage .text11").show(0);
		});

		$(".wrapperIntro.firstPage .text8").mouseout(function(){
			$(".wrapperIntro.firstPage .text10").hide(0);
			$(".wrapperIntro.firstPage .text11").hide(0);
		});*/

		$(".wrapperIntro.firstPage .text9").click(function(){
			alert("Sangina need to code this");
		});




	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();

			if(countNext==9){
			$nextBtn.hide(0);
		}
			console.log(countNext);
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};



			fnSwitcher();


		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				first02,
			    first03,
			    //first04,
			    //first05,
				//first06,
				//first07

				second,
				second01,

				third,
				third01,


			];


			fnArray[countNext]();

			/*if(countNext==8){
				$(".text4").show(0);
			}*/

		$(".wrapperIntro.second .text3").mouseover(function(){
			$(".wrapperIntro.second .text5").show(0);
			$(".textblock").hide(0);
			//$(".wrapperIntro.second .text6").show(0);
		});

		$(".wrapperIntro.second .text3").mouseout(function(){
			$(".wrapperIntro.second .text5").hide(0);
			$(".textblock").show(0);
			//$(".wrapperIntro.second .text6").hide(0);
		});

		/*$(".wrapperIntro.second .text4").click(function(){
			//$(".table_diy_ans").show(0);
			//$(".table_diy").hide(0);
			$(".nextBtn.myNextStyle").show(0);
		});*/

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {

//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=10;
		$nextBtn.show();
	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [/*{
		justClass : "firstPage",
		animate : "true",
		text : [


		data.string.p1_6,
		data.string.p1_7,
		data.string.p1_8,
		data.string.p1_9


		],

	},

	 {

		justClass : "second",
		animate : "true",
		text : [
		data.string.p1_10,
		data.string.p1_12,
		data.string.p1_13,
		data.string.p1_14,
		data.string.p1_15,
		data.string.p1_16,
		data.string.p1_17,
		data.string.p1_18,
		data.string.p1_18_1,
		data.string.p1_19,
		data.string.p1_20,
		data.string.p1_21,
		data.string.p1_21_1,
		data.string.p1_22,


		],
	},
		{

		justClass : "third",
		animate : "true",
		text : [
		data.string.p1_23,
		data.string.p1_24
		/*data.string.p1_25,
		data.string.p1_26,
		data.string.p1_27,
		data.string.p1_28,
		data.string.p1_29,
		data.string.p1_30,
		data.string.p1_31,
		data.string.p1_32,
		data.string.p1_33,
		data.string.p1_35,
		data.string.p1_36*/

		//],
	//},

	{

		justClass : "first",
		animate : "true",
		text : [

		data.string.p1_25,
		data.string.p1_26,
		data.string.p1_27,
		data.string.p1_28,
		data.string.p1_29,
		data.string.p1_30,
		data.string.p1_31,
		data.string.p1_32,
		data.string.p1_33,
		data.string.p1_34,
		data.string.p1_35,
		data.string.p1_36

		],
	},
	]



/******************************************************************************************************/
	/*
	* first
	*/
		/*function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);

		};


		 function first02 () {
		 	$board.find(".wrapperIntro.firstPage .text1").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text2").show(0);

		 }

		 function first03 () {
		 	$board.find(".wrapperIntro.firstPage .text2").hide(0);
		 	$board.find(".wrapperIntro.firstPage .text3").show(0);

		 }

		// function first04 () {
		//  	$board.find(".wrapperIntro.firstPage .text3").hide(0);
		//  	$board.find(".wrapperIntro.firstPage .text4").show(0);

		//  }	*/


		/*function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);

		};


		 function second02 () {
		 	$board.find(".wrapperIntro.second .text0").hide(0);
		 	$board.find(".wrapperIntro.second .text1").show(0);

		 }

		 function second03 () {
		 	$board.find(".wrapperIntro.second .text1").hide(0);
		 	$board.find(".wrapperIntro.second .text2").show(0);
		 	$(".wrapperIntro.second").addClass("back");
		 	$board.find("#bread").hide(0);
		 	$board.find("#strawberry").hide(0);
		 	$board.find("#banana").hide(0);
		 	$board.find("#watermelon").hide(0);
		 }

		 function second04(){
		 	$board.find(".wrapperIntro.second.back .text3").show(0);

		 }

		 function second05(){
		 	$board.find(".wrapperIntro.second.back .text4").show(0);

		 }

		 function second06(){
		 	$board.find(".wrapperIntro.second.back .text5").show(0);

		 }

		 function second07(){
		 	$board.find(".wrapperIntro.second.back .text6").show(0);

		 }

		 function second08(){
		 	$board.find(".wrapperIntro.second.back .text7").show(0);

		 }

		 function second09(){
		 	$board.find(".wrapperIntro.second.back .text8").show(0);

		 }

		 function second10(){
		 	$board.find(".wrapperIntro.second.back .text9").show(0);

		 }

		 function second11(){
		 	$board.find(".wrapperIntro.second.back .text10").show(0);

		 }

		 function second12(){
		 	$board.find(".wrapperIntro.second.back .text11").show(0);

		 }

		 function second13(){
		 	$board.find(".wrapperIntro.second.back .text12").show(0);

		 }

		 function second14(){
		 	$board.find(".wrapperIntro.second.back .text13").show(0);

		 }



		 // third
		 function third() {

			var source = $("#intro03-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[2];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.third").addClass("modebg");
		};


		function third02 () {
			$board.find(".wrapperIntro.third.modebg .text0").show(0);
		}

		function third03 () {
			$board.find(".wrapperIntro.third.modebg .text1").show(0);
		}*/



		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.first").addClass("modebg");
			$board.find(".wrapperIntro.first.modebg .text0").show(0);

		};

		/* function first02 () {
		 	$board.find(".wrapperIntro.first.modebg .text1").show(0);
		 	//$board.find(".wave1").hide(0);
		 	//$board.find(".wave2").hide(0);
		 	//$board.find(".wave3").hide(0);
		 	//$board.find(".pirate").hide(0);
		 	//$(".wrapperIntro.fourth.modebg").addClass("modenightbg");
		 }*/

		 function first03 () {
			 $board.find(".wrapperIntro.first.modebg .text0").hide(0);
			 // $board.find(".wrapperIntro.first.modebg .text1").show(0);
		 	$(".wrapperIntro.first.modebg").addClass("modenightbg");
			$board.find(".wrapperIntro.first.modebg.modenightbg .text1").show(0);
		 	//$board.find(".wrapperIntro.first.modebg.modenightbg .text2").show(0);
			$board.find(".wave1").hide(0);
		 	$board.find(".wave2").hide(0);
		 	$board.find(".wave3").hide(0);
		 	$board.find(".pirate").hide(0);
			$board.find(".smallantmode").show(0);
			$board.find(".mediumantsmode").show(0);
			$board.find(".largeantsmode").show(0);
			$board.find(".extraantsmode").show(0);

		 }

		  function first04 () {
		 	$board.find(".wrapperIntro.first.modebg.modenightbg .text1").hide(0);
		 	$board.find(".wrapperIntro.first.modebg.modenightbg .text2").show(0);
			$(".nextBtn.mynextStyle").hide(0);
			$("#bread").click(function(){
				$("#bread").addClass("modeantcorrect");
				$board.find(".wrapperIntro.first.modebg.modenightbg .text2").hide(0);
				$board.find(".wrapperIntro.first.modebg.modenightbg .text3").show(0);
				$("#correct_ant").show(0);
				$("#correct_ant").delay('1500').fadeOut('2800');
				$(".nextBtn.mynextStyle").show(0);
			});
			$("#strawberry").click(function(){
				$("#incorrect_ant").show(0);
				$("#incorrect_ant").delay('1500').fadeOut('2800');
			});

			$("#banana").click(function(){
				$("#incorrect_ant").show(0);
				$("#incorrect_ant").delay('1500').fadeOut('2800');
			});

			$("#watermelon").click(function(){
				$("#incorrect_ant").show(0);
				$("#incorrect_ant").delay('1500').fadeOut('2800');
			});

		 }

		 /*function first05 () {
		 	$board.find(".wrapperIntro.first.modebg.modenightbg .text2").hide(0);
		 	$board.find(".wrapperIntro.first.modebg.modenightbg .text3").show(0);

		 }*/

		  function first06 () {
			$board.find(".wrapperIntro.first.modebg.modenightbg .text3").hide(0);
			$board.find(".smallantmode.modeantcorrect").hide(0);
			$board.find(".mediumantsmode").hide(0);
			$board.find(".largeantsmode").hide(0);
			$board.find(".extraantsmode").hide(0);
		 	$(".wrapperIntro.first").removeClass("modenightbg");
		 	$board.find(".wrapperIntro.first.modebg .text4").show(0);
			$(".wrapperIntro.first .text2 ").hide(0);
		  }

		  function first07 () {
			 $board.find(".wrapperIntro.first.modebg .text5").show(0);
		  }

		   function first08 () {
		 	$board.find(".wrapperIntro.first.modebg .text6").show(0);
		   }

		   function first09 () {
		 	$board.find(".wrapperIntro.first.modebg .text7").show(0);
		   }

		    function first10 () {
		 	$board.find(".wrapperIntro.first.modebg .text8").show(0);
			}

			 function first11 () {
		 	$board.find(".wrapperIntro.first.modebg .text9").show(0);
			 }

			 function first12 () {
		 	$board.find(".wrapperIntro.first.modebg .text10").show(0);
			$board.find(".wrapperIntro.first.modebg .text4").hide(0);
			$board.find(".wrapperIntro.first.modebg .text5").hide(0);
			$board.find(".wrapperIntro.first.modebg .text6").hide(0);
			$board.find(".wrapperIntro.first.modebg .text7").hide(0);
			$board.find(".wrapperIntro.first.modebg .text8").hide(0);
			$board.find(".wrapperIntro.first.modebg .text9").hide(0);
			 }

			 function first13 () {
		 	$board.find(".wrapperIntro.first.modebg .text11").show(0);
		 }


/*----------------------------------------------------------------------------------------------------*/




		// first func call

		first();





	/*click functions*/
		$nextBtn.on('click',function () {
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				/*first,
				first02,
				first03,*/


				/*second,
				second02,
				second03,
				second04,
				second05,
				second06,
				second07,
				second08,
				second09,
				second10,
				second11,
				second12,
				second13,
				second14,*/

				/*third,
				third02,*/
				//third03,
				//third04,
				//third05,
				//third06,

				first,
				//first02,
				first03,
				first04,
				//first05,
				first06,
				first07,
				first08,
				first09,
				first10,
				first11,
				first12,
				first13
			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.pageEndSetNotification();
			} else {

			}
		}


});

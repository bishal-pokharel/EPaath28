/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=9;
		$nextBtn.show();

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0,4]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [
		{
		justClass : "firstPage",
		animate : "true",
		text : [


		data.string.p4_1,
		data.string.p4_2,


		data.string.p4_3,
		data.string.p4_4,

		data.string.p4_5,
		data.string.p4_6,
		data.string.p4_7,
		data.string.p4_8,
		data.string.p4_9,
		data.string.p4_9_1,
		data.string.p4_10,
		data.string.p4_11,
		data.string.p4_12

		],

	},
		{

		justClass : "second",
		animate : "true",
		text : [

		data.string.p4_13,
		data.string.p4_13_1,
		data.string.p4_14,
		data.string.p4_15,
		data.string.p4_15_1,
		data.string.p4_16,
		data.string.p4_17,
		data.string.p4_18,
		data.string.p4_19,
		data.string.p4_20,
		data.string.p4_21,
		data.string.p4_21_1,
		data.string.p4_22,
		data.string.p4_23,
		data.string.p4_24,
		data.string.p4_25,
		data.string.p4_26,
		data.string.p4_27

		],
	},
	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.firstPage .text0").delay(1000).fadeIn(0);
		};

		 function first02() {

		 	$board.find(".wrapperIntro.firstPage .text1").show(0);
			$board.find(".wrapperIntro.firstPage .text2").show(0);
			$board.find(".wrapperIntro.firstPage .text3").show(0);
			$board.find(".wrapperIntro.firstPage .text4").show(0);
			$board.find(".wrapperIntro.firstPage .text5").show(0);
			$board.find(".wrapperIntro.firstPage .text6").show(0);
			$board.find(".wrapperIntro.firstPage .text7").show(0);
			$board.find(".wrapperIntro.firstPage .text8").show(0);
		    $board.find(".wrapperIntro.firstPage .text9").show(0);

		 };

		function first03() {
			$board.find(".wrapperIntro.firstPage .text10").show(0);
		 	$(".wrapperIntro.firstPage .text2 .blink18").addClass("addclassto18");
		    $(".wrapperIntro.firstPage .text4 .blink18").addClass("addclassto18");
		    $(".wrapperIntro.firstPage .text10 .blink18").addClass("addclassto18");
		 };

		 function first04() {

		 	$board.find(".wrapperIntro.firstPage .text11").show(0);
		 	$board.find(".wrapperIntro.firstPage .text12").show(0);
			$(".nextBtn.mynextStyle").hide(0);


		 	//board.children(".wrapperIntro.firstPage .text2 > span");
		 };


		function second() {

			var source = $("#intro02-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[1];
			var html = template(content);
			$board.html(html);
			$board.find(".wrapperIntro.second .text0").show(0);
			$board.find(".wrapperIntro.second .text2").show(0);
			$board.find(".wrapperIntro.second .text3").show(0);

			$(".wrapperIntro.second .text17").on("click", function(){
			$(".wrapperIntro.second .text16").hide(0);
			$(".wrapperIntro.second .text17").hide(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		});
		};

		function second02() {

		 	$board.find(".wrapperIntro.second .text0").hide(0);
		    $board.find(".wrapperIntro.second .text1").show(0);
			$board.find(".wrapperIntro.second .text3").hide(0);
			$board.find(".wrapperIntro.second .text4").show(0);
		 };

		 function second03() {

		 	$board.find(".wrapperIntro.second .text5").show(0);
		    $board.find(".wrapperIntro.second .text6").show(0);
			$board.find(".wrapperIntro.second .text7").show(0);
			$board.find(".wrapperIntro.second .text8").show(0);
			$board.find(".wrapperIntro.second .text9").show(0);
			$board.find(".wrapperIntro.second .text10").show(0);
			$board.find(".wrapperIntro.second .text11").show(0);


		 };

		 function second04() {
			$board.find(".wrapperIntro.second .text12").show(0);
			$(".wrapperIntro.second .text6 .blink18").addClass("addclassto18");
		 }

		 function second05() {
			$board.find(".wrapperIntro.second .text13").show(0);
		 	$board.find(".wrapperIntro.second .text14").show(0);
			$board.find(".wrapperIntro.second .text15").show(0);

		 }

		  function second06() {
			$board.find(".wrapperIntro.second .text16").show(0);
		 	$board.find(".wrapperIntro.second .text17").show(0);
			$(".nextBtn.mynextStyle").hide(0);

		 }



		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");

/*----------------------------------------------------------------------------------------------------*/







		// first func call
		first();
		// lastBaseIndices ();
		// countNext = 12;

		$(".wrapperIntro.firstPage .text2 > span").on("click", function(){
			$(".wrapperIntro.firstPage .text11").show(0);
			$(".wrapperIntro.firstPage .text12").show(0);
		});



		$(".wrapperIntro.firstPage .text4 > span").on("click", function(){
			$(".wrapperIntro.firstPage .text11").show(0);
			$(".wrapperIntro.firstPage .text12").show(0);
		});


		$(".wrapperIntro.firstPage .text12").on("click", function(){
			// alert("btn trigger called");
			$(".wrapperIntro.firstPage .text11").hide(0);
			$(".wrapperIntro.firstPage .text12").hide(0);
			// $(".nextBtn").show(0);
		});





	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}



		function fnSwitcher () {
			fnArray = [
				first,
				first02,
				first03,
				first04,

				second,
				second02,
				second03,
				second04,
				second05,
				second06
			];


			fnArray[countNext]();

			$(".wrapperIntro.second .text16 > div").on("click", function(){
			$(".wrapperIntro.second .text15").hide(0);
			$(".wrapperIntro.second .text16").hide(0);
		});

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);

			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/


var vdopath = $ref+"/videos/";
		var vid_play_pause;

		//for play and pause video

	function vid_play_pause() {
	  var myVideo = document.getElementById("myVideo");
	  if (myVideo.paused) {
		myVideo.play();
	 } else {
		myVideo.pause();
	  }

	  var myVideo1 = document.getElementById("myVideo1");

	  if (myVideo1.paused) {
		myVideo1.play();
	 } else {
		myVideo1.pause();
	 }

	   var myVideo2 = document.getElementById("myVideo2");

	  if (myVideo2.paused) {
		myVideo2.play();
	 } else {
		myVideo2.pause();
	  }

	  var myVideo3 = document.getElementById("myVideo3");

	  if (myVideo3.paused) {
		myVideo3.play();
	 } else {
		myVideo3.pause();
	  }
	}


	function restart() {

        var video = document.getElementById("myVideo");
        video.currentTime = 0;
		video.play();

		var video1 = document.getElementById("myVideo1");
        video1.currentTime = 0;
		video1.play();

		var video2 = document.getElementById("myVideo2");
        video2.currentTime = 0;
		video2.play();

		var video3 = document.getElementById("myVideo3");
        video3.currentTime = 0;
		video3.play();

    }

$(function () {




//------------------------------------------------------------------------------------------------//
	var $board = $('.board');
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=5;
		$nextBtn.show();
		
		/*to register a partial*/
		Handlebars.registerPartial("videocontent", $("#videocontent-partial").html());
		Handlebars.registerPartial("videocontent1", $("#videocontent1-partial").html());
		Handlebars.registerPartial("videocontent2", $("#videocontent2-partial").html());
		Handlebars.registerPartial("videocontent3", $("#videocontent3-partial").html());

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p10_1,
		data.string.p10_2,
		data.string.p10_3,
		data.string.p10_4,
		data.string.p10_5,
		data.string.p10_6,
		data.string.p10_7,
		data.string.p10_8,
		data.string.p10_9,
		data.string.p10_10

		],

		videoblock : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "pie1",
						replaytext : data.string.replay,
						vdeosrc2   : vdopath+"pie1.mp4",
					},
				]
			},
		],


		videoblock1 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "pie2",
						replaytext : data.string.replay,
						vdeosrc3   : vdopath+"pie2.mp4",
					},
				]
			},
		],

		videoblock2 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "pie3",
						replaytext : data.string.replay,
						vdeosrc4   : vdopath+"pie3.mp4",
					},
				]
			},
		],

		videoblock3 : [
			{
				videotoshow : [
					{

						//vdeosrc   : vdopath+"equal.mp4",
						vdoclass: "pie4",
						replaytext : data.string.replay,
						vdeosrc5   : vdopath+"pie4.mp4",
					},
				]
			},
		]
	},
]




/******************************************************************************************************/

		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			$(".wrapperIntro.firstPage .text0").show(0);
		};

		 function first02() {

			$(".wrapperIntro.firstPage .text1").show(0);
			 $(".straw").show(0);
		 	  $(".burg").show(0);
		 	  $(".wm").show(0);
		 	  $(".ba").show(0);
			$(".wrapperIntro.firstPage .text6").show(0);
			$(".wrapperIntro.firstPage .text7").show(0);
			$(".wrapperIntro.firstPage .text8").show(0);
			$(".wrapperIntro.firstPage .text9").show(0);

		 }

		 function first03() {

			 $(".wrapperIntro.firstPage .text1").hide(0);
			 $(".straw").hide(0);
		 	  $(".burg").hide(0);
		 	  $(".wm").hide(0);
		 	  $(".ba").hide(0);
			$(".wrapperIntro.firstPage .text6").hide(0);
			$(".wrapperIntro.firstPage .text7").hide(0);
			$(".wrapperIntro.firstPage .text8").hide(0);
			$(".wrapperIntro.firstPage .text9").hide(0);
		 	 $(".wrapperIntro.firstPage .text2").show(0);
			 $(".replay").show(0);
			 $(".videoblock").show(0);
			 restart();

		 }

		 function first04() {
			 $(".wrapperIntro.firstPage .text2").hide(0);
		 	 $(".wrapperIntro.firstPage .text3").show(0);
			 $(".videoblock").hide(0);
			 $(".videoblock1").show(0);
			 restart();

		 }

		 function first05() {
		 	 $(".wrapperIntro.firstPage .text3").hide(0);
			 $(".wrapperIntro.firstPage .text4").show(0);
			 $(".videoblock1").hide(0);
			 $(".videoblock2").show(0);
			 restart();

		 }

		  function first06() {
			 $(".wrapperIntro.firstPage .text4").hide(0);
		 	 $(".wrapperIntro.firstPage .text5").show(0);
			 $(".videoblock2").hide(0);
			 $(".videoblock3").show(0);
			 restart();

		  }










		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");

/*----------------------------------------------------------------------------------------------------*/


		// first func call
		first();
		$(".color").draggable({
			  revert:true
			});
		$(".door").droppable({
		  drop: function(e, ui) {
		    console.log(ui.draggable)
		    $(this).css("background-color", ui.draggable.attr("id"));
		  }
		});


	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");
			$prevBtn.show(0);
			countNext++;
			fnSwitcher();
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};
			fnSwitcher();
		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
			    first02,
			    first03,
			    first04,
				first05,
				first06,

			];


			fnArray[countNext]();

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.footerNotificationHandler.lessonEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

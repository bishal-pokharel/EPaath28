/******
	This js file is of exponents, developed by MJT
	testing next and prev btns and sequence clicks,
	clickArray stores the flawless trasition states
	clickSequence = check the clickArray with index and calls fnSwitcher, it can be used with clicked by sequence (pink bar)
	fnSwitcher = calls the function according to countNext
****/

$(function () {


//------------------------------------------------------------------------------------------------//
	var $board = $('.board'),
		$nextBtn = $("#activity-page-next-btn-enabled"),
		$prevBtn = $("#activity-page-prev-btn-enabled"),
  $refreshBtn= $("#activity-page-refresh-btn")
		countNext = 0,
		all_page=4;
		$nextBtn.show();

	var baseAndIndicesClick = [null,null]; //used by base and index

	var clickArray = [0]; //manipulate the clicks like back and from top
	$total_page = clickArray.length;
	loadTimelineProgress($total_page,1);
	var dataObj = [{
		justClass : "firstPage",
		animate : "true",
		text : [
		data.string.p5_13,
		data.string.p5_19,
		data.string.p5_20,
		data.string.p5_21,
		data.string.p5_22,
		data.string.p5_23,
		data.string.p5_24
		],
	},


	/*{

		justClass : "second",
		animate : "true",
		text : [
		data.string.p5_7,
		data.string.p5_8,
		data.string.p5_9,
		data.string.p5_10,
		data.string.p5_11,
		data.string.p5_12,

		],
	},


	{

		justClass : "third",
		animate : "true",
		text : [
		data.string.p5_12_1,
		data.string.p5_12_2,
		//data.string.p5_11,
		data.string.p5_13,
		data.string.p5_14,
		data.string.p5_15,
		data.string.p5_16,
		data.string.p5_17,
		data.string.p5_18

		],
	},


	{

		justClass : "fourth",
		animate : "true",
		text : [

		data.string.p5_13,
		data.string.p5_19,
		data.string.p5_20,
		data.string.p5_21,
		data.string.p5_22,
		data.string.p5_23,
		data.string.p5_24


		],
	},*/


	]




/******************************************************************************************************/
	/*
	* first
	*/
		function first() {

			var source = $("#intro-template").html();
			var template = Handlebars.compile(source);
			var content = dataObj[0];
			var html = template(content);
			$board.html(html);
			setTimeout(function(){
			$board.find(".wrapperIntro.firstPage .text0").show(0);
			$board.find(".table_diy3").show(0);
		 	$board.find(".wrapperIntro.firstpage .text1").show(0);
			$board.find(".kid").show(0);
			$nextBtn.hide(0);
            },1000);

			$(document).ready(function() {
    $(".width, .width1, .width2, .width3, .width4, .width5, .width6, .width7").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) ||
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

/*disable characters*/
	$('.width').keyup(function () {
    var input = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input ==1) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("first").disabled = true;

			}
				else{
					document.getElementById("first").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width2').keyup(function () {
    var input1 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input1 ==3) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("third").disabled = true;

			}
				else{
					document.getElementById("third").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width4').keyup(function () {
    var input2 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input2 ==1) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fifth").disabled = true;

			}
				else{
					document.getElementById("fifth").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width6').keyup(function () {
    var input3 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input3 ==2) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("seventh").disabled = true;
			$(".width1").show(0);
			$(".width3").show(0);
			$(".width5").show(0);
			$(".width7").show(0);

			}
				else{
					document.getElementById("seventh").disabled = false;
					$(this).addClass('incorrect');
				}
			});


	$('.width1').keyup(function () {
    var input4 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input4 ==1) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("second").disabled = true;

			}
				else{
					document.getElementById("second").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width3').keyup(function () {
    var input5 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input5 ==4) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("fourth").disabled = true;

			}
				else{
					document.getElementById("fourth").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width5').keyup(function () {
    var input6 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input6 ==5) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("sixth").disabled = true;

			}
				else{
					document.getElementById("sixth").disabled = false;
					$(this).addClass('incorrect');
				}
			});

	$('.width7').keyup(function () {
    var input7 = $(this).val();
	$(this).val($(this).val().replace(/[^0-9.]/g,''));

	if (input7 ==7) {
			$(this).addClass("correct");
			$(this).removeClass("incorrect");
			document.getElementById("eighth").disabled = true;
			$nextBtn.show(0);

			}
				else{
					document.getElementById("eighth").disabled = false;
					$(this).addClass('incorrect');
				}
			});


			var lolz = $('.width-ans');
			$(".width-ans").click(function(){
				$(".wrapperIntro.firstPage .text5").fadeIn(1000);
			});

			$(".wrapperIntro.firstPage .text5").click(function(){

				if(lolz.val() == 4 )
				{
					document.getElementById("ans").disabled = true;
					$("#correct2").show(0);
					$("#wrong2").hide(0);
					$(".wrapperIntro.firstPage .text6").hide(0);
					$nextBtn.show(0);


					//ole.footerNotificationHandler.pageEndSetNotification();
				}
				else
					{
						$("#wrong2").show(0);
						$("#correct2").hide(0);
						$(".wrapperIntro.firstPage .text6").show(0);
						$nextBtn.show(0);
						//ole.footerNotificationHandler.pageEndSetNotification();

					}
					//alert(lolz.val());
		})

		}

		function first02(){
		 	$board.find(".wrapperIntro.firstpage .text2").show(0);

		 }

		function first03(){
		 	$board.find(".wrapperIntro.firstpage .text3").show(0);

		 }

		function first04(){
		 	$board.find(".wrapperIntro.firstpage .text4").show(0);
			$nextBtn.hide(0);
		}

		function first05(){
			ole.activityComplete.finishingcall();
		}


		 //var click = $board.children(".wrapperIntro.firstPage .text2").children("span").css("color", "blue");

/*----------------------------------------------------------------------------------------------------*/


		// first func call
		first();






	/*click functions*/
		$nextBtn.on('click',function () {
			// $(this).css("display","none");

			$prevBtn.show(0);
			countNext++;
			fnSwitcher();

			if(countNext==9){
			$nextBtn.hide(0);
		}
			console.log(countNext);
		});

		$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]<countNext) {
					countNext = clickArray[i];
					// console.log(countNext+" = "+clickArray[i]);
					break;
				}
			};
			if (countNext===0) {
				$prevBtn.hide(0);
			};



			fnSwitcher();


		});

		function clickSequence (index) {
			for (var i = clickArray.length - 1; i >= 0; i--) {
				if (clickArray[i]===index) {
					countNext = clickArray[i];
					break;
				}
			};
			fnSwitcher();
		}

		function fnSwitcher () {
			fnArray = [
				first,
				//first01,
			    first02,
			    first03,
			    first04,
				first05,
				//first07


			];


			fnArray[countNext]();

			/*if(countNext==8){
				$(".text4").show(0);
			}*/

		$(".wrapperIntro.second .text3").mouseover(function(){
			$(".wrapperIntro.second .text5").show(0);
			$(".wrapperIntro.second .text6").show(0);
		});

		$(".wrapperIntro.second .text3").mouseout(function(){
			$(".wrapperIntro.second .text5").hide(0);
			$(".wrapperIntro.second .text6").hide(0);
		});

		/*$(".wrapperIntro.second .text4").click(function(){
			//$(".table_diy_ans").show(0);
			//$(".table_diy").hide(0);
			$nextBtn.show(0);
		});*/

			for (var i = 0; i < clickArray.length; i++) {
				if (clickArray[i] === countNext) {
					loadTimelineProgress($total_page,i+1);
				}
			};

			if (countNext>=all_page) {
				$nextBtn.hide(0);
				ole.activityComplete.finishingcall();
				//ole.footerNotificationHandler.pageEndSetNotification();
			} else {
				// $(this).show(0);
				// $nextBtn.show(0);
			}
		}

	/****************/

});

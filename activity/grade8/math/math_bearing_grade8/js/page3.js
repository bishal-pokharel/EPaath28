var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page3/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[	
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide1img1",
								imgsrc   : imgpath + "1.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "slide1homelabel",
								imagelabeldata : data.string.texthome,
							},
							{
								imagelabelclass : "slide1schoollabel",
								imagelabeldata : data.string.textschool,
							}
						]
					}
				],
				texts : [
					{
						textclass : "slide1detailtext",
						textdata : data.string.p3text1,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide2img1",
								imgsrc   : imgpath + "2.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "slide2homelabel",
								imagelabeldata : data.string.texthome,
							},
							{
								imagelabelclass : "slide2schoollabel",
								imagelabeldata : data.string.textschool,
							}
						]
					}
				],
				texts : [
					{
						textclass : "slide2detailtext",
						textdata : data.string.p3text2,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide3img1",
								imgsrc   : imgpath + "3.png",
							}
						],
					}
				],
				texts : [
					{
						textclass : "slide3detailtext",
						textdata : data.string.p3text3,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide4img1",
								imgsrc   : imgpath + "4.png",
							}
						],
					}
				],
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "subscript",
						textclass                : "slide4detailtext",
						textdata                 : data.string.p3text4,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide5img1",
								imgsrc   : imgpath + "q1.png",
							}
						],
					}
				],
				texts : [
					{
						textclass : "slide5detailtext",
						textdata  : data.string.p3text5,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide6img1",
								imgsrc   : imgpath + "q1.png",
							}
						],
					}
				],
				texts : [
					{
						textclass : "slide6detailtext",
						textdata  : data.string.p3text6,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide6img1",
								imgsrc   : imgpath + "q1.png",
							}
						],
					}
				],
				userinputs : [
					{
						userinputadditionalclass : "slide6inputcontainer",
						inputprefixdata : data.string.p3q1prefix+":",						
						inputprefixdatahighlightflag : true,
						inputprefixdatahighlightcustomclass : "subscript",
						correctansdata : data.string.p3q1correctans,
						degreeunitdata : data.string.degreeunit,
						checkanswerdata : data.string.okaytext,
						hintbuttondata : data.string.hinttext,
						
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag        : true,
						userhintdatahighlightcustomclass : "subscript",
						texthint                         : true,
						userhintdata                     : data.string.p3q1userhintpart1+"<br>"+
															data.string.p3q1userhintpart2,
					}
				],
				conclusiondata : data.string.p3q1conclusiontext,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide7img1",
								imgsrc   : imgpath + "q2.png",
							}
						],
					}
				],
				userinputs : [
					{
						userinputadditionalclass            : "slide7inputcontainer",
						inputprefixdata                     : data.string.p3q2prefix+":",						
						inputprefixdatahighlightflag        : true,
						inputprefixdatahighlightcustomclass : "subscript",
						correctansdata                      : data.string.p3q2correctans,
						degreeunitdata                      : data.string.degreeunit,
						checkanswerdata                     : data.string.okaytext,
						hintbuttondata                      : data.string.hinttext,
						
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag        : true,
						userhintdatahighlightcustomclass : "subscript",
						texthint                         : true,
						userhintdata                     : data.string.p3q2userhintpart1,
					}
				],
				conclusiondata : data.string.p3q2conclusiontext
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide1img1",
								imgsrc   : imgpath + "3.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "slide1homelabel",
								imagelabeldata : data.string.texthome,
							},
							{
								imagelabelclass : "slide8schoollabel",
								imagelabeldata : data.string.textschool,
							}
						]
					}
				],
				texts : [
					{
						textclass : "slide1detailtext",
						textdata : data.string.p3text7,
					}
				]
			}
		]
	},
	{
    	uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p3text8,
			}
		],
    },
    {
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide1img1",
								imgsrc   : imgpath + "4.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "slide1homelabel",
								imagelabeldata : data.string.texthome,
							},
							{
								imagelabelclass : "slide10schoollabel",
								imagelabeldata : data.string.textschool,
							},
							{
								imagelabelclass : "slide10townlabel",
								imagelabeldata : data.string.texttown,
							},
						]
					}
				],
				texts : [
					{
						datahighlightflag : true,
						datahighlightcustomclass : "subscript",
						textclass : "slide10detailtext",
						textdata : data.string.p3text9,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide3img1",
								imgsrc   : imgpath + "5.png",
							}
						],
					}
				],
				texts : [
					{
						datahighlightflag : true,
						datahighlightcustomclass : "subscript",
						textclass : "slide1detailtext",
						textdata : data.string.p3text10,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide7img1",
								imgsrc   : imgpath + "q3.png",
							}
						],
					}
				],
				userinputs : [
					{
						userinputadditionalclass            : "slide12inputcontainer",
						inputprefixdata                     : data.string.p3q3prefix+":",						
						inputprefixdatahighlightflag        : true,
						inputprefixdatahighlightcustomclass : "subscript",
						correctansdata                      : data.string.p3q3correctans,
						degreeunitdata                      : data.string.degreeunit,
						checkanswerdata                     : data.string.okaytext,
						hintbuttondata                      : data.string.hinttext,
						
					}
				],
				answerhint : [
					{
						userhintimgsrc : imgpath+"q3hint.png",
					}
				],
				conclusiondata : data.string.p3q1conclusiontext
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide7img1",
								imgsrc   : imgpath + "q4.png",
							}
						],
					}
				],
				userinputs : [
					{
						userinputadditionalclass            : "slide13inputcontainer",
						inputprefixdata                     : data.string.p3q4prefix+":",						
						inputprefixdatahighlightflag        : true,
						inputprefixdatahighlightcustomclass : "subscript",
						correctansdata                      : data.string.p3q4correctans,
						degreeunitdata                      : data.string.degreeunit,
						checkanswerdata                     : data.string.okaytext,
						hintbuttondata                      : data.string.hinttext,
						
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag        : true,
						userhintdatahighlightcustomclass : "subscript",
						texthint                         : true,
						userhintdata                     : data.string.p3q4userhintpart1,
					}
				],
				conclusiondata : data.string.p3q4conclusiontext
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				images : [
					{
						imagestoshow : [
							{
								imgclass : "slide1img1",
								imgsrc   : imgpath + "5.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "slide1homelabel",
								imagelabeldata : data.string.texthome,
							},
							{
								imagelabelclass : "slide10schoollabel",
								imagelabeldata : data.string.textschool,
							},
							{
								imagelabelclass : "slide14townlabel",
								imagelabeldata : data.string.texttown,
							},
						]
					}
				],
				texts : [
					{
						datahighlightflag : true,
						datahighlightcustomclass : "subscript",
						textclass : "slide10detailtext",
						textdata : data.string.p3text11,
					}
				]
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("misccontent", $("#misccontent-partial").html());
	 Handlebars.registerPartial("numericalcontent", $("#numericalcontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			!islastpageflag ? 
				ole.footerNotificationHandler.pageEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			!islastpageflag ? 
				ole.footerNotificationHandler.pageEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);	
	}

	function numericaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$nextBtn.css('display', 'none');

		var $numericalblock     = $board.find("div.numericalblock");
		var $userinputcontainer = $numericalblock.find("div.userinputcontainer");
		var $userinput          = $userinputcontainer.find("input[type='text']");
		var $okaybutton         = $userinputcontainer.find("button#checkanswer");
		var $hintbutton         = $userinputcontainer.find(".hintbutton");

		var $userhint       = $numericalblock.find(".userhint");
		var $conclusiontext = $numericalblock.find(".conclusiontext");
		
		// autofocus user input
		$userinput.focus();

		// event-handler for user input
		$userinput.keyup(function(event) {	
			// accept only numbers or digits
			$(this).val($(this).val().replace(/[^0-9.]/g,''));
						
			// if enter is pressed trigger ok button
			if(event.which === 13) {
		        $okaybutton.trigger("click");
			}	
			// else for any value input show okay button
			else{
				// hide the related okay button if there is nothing input
				$(this).val() ? $okaybutton.show(0) : $okaybutton.css('display', 'none');
			}
		});

		var numberofanswercheckattemptsallowed = 1;
		var numberofanswercheckattempts = 0;
		var correctanswer;
		var useranswer;

		// event-handler for okay button
		$okaybutton.on('click', function() {
			numberofanswercheckattempts++;

			correctanswer = $userinput.attr("data-correctans");
			useranswer = $userinput.val();

			// check if answer matches the user input
			if(correctanswer == useranswer){
				$(this).css('display', 'none');
				$hintbutton.css('display', 'none');
				$userinput.attr('data-iscorrect', 'correct');
				$userinput.prop('disabled', 'true');
				$conclusiontext.css('opacity', '1');
				$nextBtn.show(0);
			}
			// if the answer does not match the user input 
			else if(correctanswer != useranswer){
				$userinput.attr('data-iscorrect', 'incorrect');					
				numberofanswercheckattempts > numberofanswercheckattemptsallowed ? $hintbutton.show(0) : null;
			}
		});

		// event-handler for hintbutton on hover
		$hintbutton.hover(function() {
			/* Stuff to do when the mouse enters the element */
			$userhint.css('opacity', '1');

		}, function() {
			/* Stuff to do when the mouse leaves the element */
			$userhint.css('opacity', '0');
		});

	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');		

		// call navigation controller
		navigationcontroller();	

		// call the template
		switch(countNext){
			case 6 :
			case 7 :
			case 12 :
			case 13 : numericaltemplate(); break;
			default : generaltemplate(); break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();	

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
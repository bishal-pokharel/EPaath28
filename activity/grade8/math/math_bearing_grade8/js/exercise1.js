// prototype for array shuffle but the last index element
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/exercise1/";
var congratulationimgarray = [
								"images/quizcongratulation/gradea.png",
								"images/quizcongratulation/gradeb.png",
								"images/quizcongratulation/gradec.png",
							];

var tempcontent=[
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q1,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q1.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q1prefix,
						correctansdata           : data.string.e1q1correctans,
						unitdata                 : data.string.e1q1unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{ userhintdata : data.string.e1q1userhint1, },
					{ userhintdata : data.string.e1q1userhint2, },
					{ userhintdata : data.string.e1q1userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q2,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q2.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q2prefix,
						correctansdata           : data.string.e1q2correctans,
						unitdata                 : data.string.e1q2unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{ userhintdata : data.string.e1q2userhint1, },
					{ userhintdata : data.string.e1q2userhint2, },
					{ userhintdata : data.string.e1q2userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q3,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q3.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q3prefix,
						correctansdata           : data.string.e1q3correctans,
						unitdata                 : data.string.e1q3unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{ userhintdata : data.string.e1q3userhint1, },
					{ userhintdata : data.string.e1q3userhint2, },
					{ userhintdata : data.string.e1q3userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q4,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q4.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q4prefix,
						correctansdata           : data.string.e1q4correctans,
						unitdata                 : data.string.e1q4unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag : true,
						userhintdatahighlightcustomclass : "subscript",
						userhintdata : data.string.e1q4userhint1,
					},
					{ userhintdata : data.string.e1q4userhint2, },
					{ userhintdata : data.string.e1q4userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q5,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q5.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "scalelabel",
								imagelabeldata : data.string.e1q5label,
							}
						]
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q5prefix,
						correctansdata           : data.string.e1q5correctans,
						unitdata                 : data.string.e1q5unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag : true,
						userhintdatahighlightcustomclass : "subscript",
						userhintdata : data.string.e1q5userhint1,
					},
					{ userhintdata : data.string.e1q5userhint2, },
					{ userhintdata : data.string.e1q5userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q6,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q6.jpg",
							}
						],
						imagelabels : [
							{
								imagelabelclass : "scalelabel",
								imagelabeldata : data.string.e1q6label,
							}
						]
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q6prefix,
						correctansdata           : data.string.e1q6correctans,
						unitdata                 : data.string.e1q6unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdata : data.string.e1q6userhint1,
					},
					{ userhintdata : data.string.e1q6userhint2, },
					{ userhintdata : data.string.e1q6userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q7,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q7.jpg",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q7prefix,
						correctansdata           : data.string.e1q7correctans,
						unitdata                 : data.string.e1q7unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdata : data.string.e1q7userhint1,
					},
					{ userhintdata : data.string.e1q7userhint2, },
					{ userhintdata : data.string.e1q7userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q8,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q8.jpg",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q8prefix,
						correctansdata           : data.string.e1q8correctans,
						unitdata                 : data.string.e1q8unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdata : data.string.e1q8userhint1,
					},
					{ userhintdata : data.string.e1q8userhint2, },
					{ userhintdata : data.string.e1q8userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q9,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q9.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q9prefix,
						correctansdata           : data.string.e1q9correctans,
						unitdata                 : data.string.e1q9unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdata : data.string.e1q9userhint1,
					},
					{ userhintdata : data.string.e1q9userhint2, },
					{ userhintdata : data.string.e1q9userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		numericalblock : [
			{
				texts : [
					{
						datahighlightflag        : true,
						datahighlightcustomclass : "questionnumber",
						textclass                : "questionstyle",
						textdata                 : data.string.e1q10,
					}
				],
				images : [
					{
						imagestoshow : [
							{
								imgclass : "q1img",
								imgsrc   : imgpath + "q10.png",
							}
						],
					}
				],
				userinputs : [
					{
						inputprefixdata          : data.string.e1q10prefix,
						correctansdata           : data.string.e1q10correctans,
						unitdata                 : data.string.e1q10unit,
						checkanswerdata          : data.string.okaytext,
					}
				],
				answerhint : [
					{
						userhintdatahighlightflag : true,
						userhintdatahighlightcustomclass : "subscript",
						userhintdata : data.string.e1q10userhint1,
					},
					{
						userhintdatahighlightflag : true,
						userhintdatahighlightcustomclass : "subscript",
						userhintdata : data.string.e1q10userhint2,
					},
					{ userhintdata : data.string.e1q10userhint3, }
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		congratulationsblock : [
			{
				congratulationtextdata : data.string.e1congratulationtext,
				congratulationimgsrc : congratulationimgarray[0],
				congratulationcompletedtextdata : data.string.e1congratulationcompletedtext
			},
		],
	},
];

// shuffle only the questions
// var content = tempcontent.shufflearray();
var content = tempcontent;

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("numericalcontent", $("#numericalcontent-partial").html());
	 Handlebars.registerPartial("congratulationscontent", $("#congratulationscontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			!islastpageflag ?
				ole.footerNotificationHandler.pageEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			!islastpageflag ?
				ole.footerNotificationHandler.pageEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		var $numericalblock     = $board.find("div.numericalblock");
		var $userinputcontainer = $numericalblock.find("div.userinputcontainer");
		var $userinput          = $userinputcontainer.find("input[type='text']");
		var $okaybutton         = $userinputcontainer.find("button#checkanswer");
		var $userhintcontainer  = $numericalblock.find("div.userhintcontainer");
		var $userhints          = $userhintcontainer.children('p');

		// autofocus user input
		$userinput.focus();

		// event-handler for user input
		$userinput.keyup(function(event) {
			// accept only numbers or digits
			$(this).val($(this).val().replace(/[^0-9.]/g,''));

			// if enter is pressed trigger ok button
			if(event.which === 13) {
		        $okaybutton.trigger("click");
			}
			// else for any value input show okay button
			else{
				// hide the related okay button if there is nothing input
				$(this).val() ? $okaybutton.show(0) : $okaybutton.css('display', 'none');
			}
		});

		var numberofanswercheckattemptsallowed = 2;
		var numberofanswercheckattempts = 0;
		var correctanswer;
		var useranswer;

		// event-handler for okay button
		$okaybutton.on('click', function() {
			numberofanswercheckattempts++;

			correctanswer = $userinput.attr("data-correctans");
			useranswer = $userinput.val();

			// check if answer matches the user input
			if(correctanswer == useranswer){
				$(this).css('display', 'none');
				$userinput.attr('data-iscorrect', 'correct');
				$userinput.prop('disabled', 'true');
				$nextBtn.show(0);
			}
			// if the answer does not match the user input
			else if(correctanswer != useranswer){
				$userinput.attr('data-iscorrect', 'incorrect');

				if( numberofanswercheckattempts >= numberofanswercheckattemptsallowed && numberofanswercheckattempts <= $userhints.length+numberofanswercheckattemptsallowed){
					$userhints.eq(numberofanswercheckattempts-2).show(0);
				}

				// only for this once
				numberofanswercheckattempts == 2 ?
					$userhintcontainer.css('top', '60%') :
					null;
			}
		});
	}

	function congratulation(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.show(0);
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		switch(countNext){
			case 10 : congratulation(); break;
			case 11 : ole.activityComplete.finishingcall(); break;
			default : // call navigation controller
						navigationcontroller();
						generaltemplate(); break;
		}

		//call the slide indication bar handler for pink indicators
		countNext<11?loadTimelineProgress($total_page,countNext+1):loadTimelineProgress($total_page,countNext);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page2/";
var squirrelintrigued = "images/lokharke/important_content_animated.svg";

var content=[
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sunimgstyle",
						imgsrc   : imgpath + "sun.png",
					}
				]
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p2text1,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sunhouseimgstyle",
						imgsrc   : imgpath + "sunhouse.png",
					}
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "orangehighlight",
				textclass                : "bottomparatextstyle",
				textdata                 : data.string.p2text2,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sunhouseimgstyle",
						imgsrc   : imgpath + "sunhousedirection.png",
					}
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "orangehighlight",
				textclass                : "bottomparatextstyle",
				textdata                 : data.string.p2text3,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		lowertextblock : [
			{
				datahighlightflag        : true,
				datahighlightcustomclass : "orangehighlight",
				textclass                : "onlyparatextstyle",
				textdata                 : data.string.p2text4,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		textimageblock : [
			{
				text : [
					{
						textclass : "slide4text1 lastanimatedelement",
						textdata : data.string.p2text5,
					}
				],
				image : [
					{
						imagestoshow : [
							{
								imgclass : "slide4img1",
								imgsrc   : imgpath + "sunhousedirection.png",
							}
						]
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		textimageblock : [
			{
				text : [
					{
						textclass : "slide5text1",
						textdata : data.string.p2text5,
					},
					{
						textclass : "slide5text2",
						textdata : data.string.northabbreviation,
					}
				],
				image : [
					{
						imagestoshow : [
							{
								imgclass : "slide5img1",
								imgsrc   : imgpath + "sunhousedirection.png",
							},
							{
								imgclass : "slide5img2 lastanimatedelement",
								imgsrc   : imgpath + "northpointer.png",
							},
						]
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle applicationheading",
				textdata  : data.string.p2heading1,
			}
		],
		expprocedureblock : [
			{
				procedure : [
					{
						stepvisibility    : "currentstep",
						proceduredata     : data.string.p2step1,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p2step2,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p2step3,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p2step4,
					}
				],
				firststepimgsrc : imgpath+"step1.png",
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		lowertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p2text6,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "northbearingstyle",
						imgsrc   : imgpath + "step1.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass : "directionlabelnorth",
						imagelabeldata : data.string.directionnorth,
					}
				]
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p2text7,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "eastbearingstyle",
						imgsrc   : imgpath + "bearingeast.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass : "directionlabeleast",
						imagelabeldata : data.string.directioneast,
					}
				]
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p2text8,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "southbearingstyle",
						imgsrc   : imgpath + "bearingsouth.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass : "directionlabelsouth",
						imagelabeldata : data.string.directionsouth,
					}
				]
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p2text9,
			}
		]
	},
	{
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p2heading1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "westbearingstyle",
						imgsrc   : imgpath + "bearingwest.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass : "directionlabelwest",
						imagelabeldata : data.string.directionwest,
					}
				]
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata  : data.string.p2text10,
			}
		]
	},
	{
		lowertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p2text11,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		miscblock : [
			{
				resetbuttondata        : data.string.p2resetbuttontext,
				bearinginstructiondata : data.string.p2bearingtextstartinstruction,
				bearingtextdata        : data.string.p2bearingtextstep0,
				angletext              : data.string.p2angletext,
				bearingtext            : data.string.p2bearingtext,
				degreesign             : data.string.degreesign,
			}
		]
	},
	{
		lowertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p2text12,
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("misccontent", $("#misccontent-partial").html());
	 Handlebars.registerPartial("textimagecontent", $("#textimagecontent-partial").html());
	 Handlebars.registerPartial("expprocedurecontent", $("#expprocedurecontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $textimageblock = $board.find('div.textimageblock');
		var $expprocedureblock = $board.find("div.expprocedureblock");
		var $miscblock      = $board.find('div.miscblock');

		if($textimageblock.length > 0){
			$nextBtn.css('display', 'none');
			$prevBtn.css('display', 'none');
			
			/*find the lastanimatedelement from board because the misc block
			itself is animated last in some slides with miscblock*/
			var $lastanimatedelement = $board.find(".lastanimatedelement");

			$lastanimatedelement.one(animationend, function() {
				navigationcontroller();
			});
		}

		// if procedure slide is present it has length at least 1 else 0
		else if($expprocedureblock.length > 0){
			$nextBtn.css('display', 'none');
			var $expprocedurecontainer   = $expprocedureblock.children('div.expprocedurecontainer');
			var $procedures              = $expprocedurecontainer.children('ol.procedures');
			var $currentstep             = $procedures.children("li.currentstep");
			var $currentstepimgcontainer = $expprocedurecontainer.children('div.currentstepimgcontainer');
			var $currentstepimg          = $currentstepimgcontainer.children('img');
			var $nextstepBtn             = $expprocedureblock.children('div.nextstepBtn');
			var $prevstepBtn             = $expprocedureblock.children('div.prevstepBtn');
			var totalstepcount           = $procedures.children('li').length;
			var currentstepcount         = 1;
			var currentimgpath;

			// navigation control function for the sub slide
			function sublidenavcontrol(){
				if(currentstepcount == 1){
					$nextBtn.css('display', 'none');
					$prevBtn.show(0);
					$prevstepBtn.css('display', 'none');
					$nextstepBtn.show(0);
				}

				else if(currentstepcount > 1 && currentstepcount < totalstepcount){
					$nextBtn.css('display', 'none');
					$prevBtn.css('display', 'none');
					$prevstepBtn.show(0);
					$nextstepBtn.show(0);
				}

				else if(currentstepcount == totalstepcount){
					$nextBtn.show(0);
					$prevBtn.css('display', 'none');
					$prevstepBtn.show(0);
					$nextstepBtn.css('display', 'none');
				}
			}

			// this is to handle the different file formats of the steps images
			var updatestepimage = function(stepnumber){
				var fileformat = ".png";
				var timestamp = new Date().getTime();
				switch(stepnumber){
					case 2 : fileformat = ".svg?"+timestamp; break;
					default : fileformat = ".png"; break;
				}

				return "<img src='"+imgpath+"step"+stepnumber+fileformat+"'/>";
			};

			// event handler for next step button
			$nextstepBtn.on('click', function() {
				$currentstep.attr('class','stepsprevious');
				$currentstep = $currentstep.next();
				$currentstep.attr("class", "currentstep");
				currentstepcount++;
				$currentstepimgcontainer.html(updatestepimage(currentstepcount));
				sublidenavcontrol();
			});

			// event handler for previous step button
			$prevstepBtn.on('click', function() {
				$currentstep.attr('class','stepsnext');
				$currentstep = $currentstep.prev();
				$currentstep.attr("class", "currentstep");
				currentstepcount--;
				$currentstepimgcontainer.html(updatestepimage(currentstepcount));
				sublidenavcontrol();
			});			
		}

		else if($miscblock.length > 0){
			// hide nextBtn before acitvity is complete
			$nextBtn.css('display', 'none');
			var $bearinglab            = $miscblock.find("div.bearinglab");
			var $plane                 = $bearinglab.find("div.plane");
			var $radarsweepercontainer = $bearinglab.find("div.radarsweepercontainer");
			var $degreecontainer       = $bearinglab.find("div.degreecontainer");
			var $resetbutton           = $miscblock.find("button#resetbutton");
			var $bearinginstruction    = $miscblock.find(".bearinginstruction");
			var $bearingtextpara       = $miscblock.find(".bearingtextpara");
			var $anglevalue            = $miscblock.find(".anglevalue").children('span:nth-of-type(2)');
			var $bearingvalue          = $miscblock.find(".bearingvalue").children('span:nth-of-type(2)');
			var totalsteps             = 5; /*total steps the plane takes to reach 360 degree*/
			var currentstepcount       = 0; /*0 by default*/

			var s = Snap(".degreecontainer");
			Snap.load(imgpath+"degreecircle.svg", function (f) {    
    		s.append(f);

    			// this function helps update the text or bearing or angle during the steps
				// returnangletextorbearing flag helps decide what to return
				// between angle, bearing or text
				updatebearingtext = function(currentstep, returnangletextorbearing){
					var texttoupdate    = "";
					var degreesign = data.string.degreesign;
					var angletoupdate, bearingtoupdate;

					switch(currentstep){
						case 0 : texttoupdate = data.string.p2bearingtextstep0 ;
								 angletoupdate = "0"+degreesign;
								 bearingtoupdate = "000"+degreesign;
								 break;

						case 1 : texttoupdate = data.string.p2bearingtextstep1 ; 
								 angletoupdate = "54"+degreesign;
								 bearingtoupdate = "054"+degreesign;
								 $('div.plane').toggleClass("planedown");
								 break;
						case 2 : texttoupdate = data.string.p2bearingtextstep2 ; 
								 angletoupdate = bearingtoupdate = "125"+degreesign;
								 $('div.planedown').removeClass("planedown").addClass("plane");					 
								 break;
						case 3 : texttoupdate = data.string.p2bearingtextstep3 ; 
								 angletoupdate = bearingtoupdate = "240"+degreesign;								 
								 $('div.plane').toggleClass("planeup");					 
								 break;
						case 4 : texttoupdate = data.string.p2bearingtextstep4 ; 
								 angletoupdate = bearingtoupdate = "305"+degreesign;	
								 $('div.planeup').removeClass("planeup").addClass("plane");	
								 break;
						case 5 : texttoupdate = data.string.p2bearingtextstep5 ; 
								 angletoupdate = bearingtoupdate = "360"+degreesign;
								 $nextBtn.show(0);
								 break;
						default : break;
					}

					// choose what to return
					if(returnangletextorbearing == "text"){
						return texttoupdate;
					}
					else if(returnangletextorbearing == "angle"){
						return angletoupdate;
					}
					else if(returnangletextorbearing == "bearing"){
						return bearingtoupdate;
					}
					
				};

    			// plane click event handler
				$plane.on('click', function() {
					// increase the currentstep count value
					$plane.css('pointer-events', 'none');

					/*here totalsteps-1 because after the last step it should unknowingly 
					update the step to original step thus done with the else if condition*/
					if(currentstepcount < totalsteps-1){
						currentstepcount++;								
						$("#degreecircle").attr('data-animstep', 'step'+currentstepcount);
						$plane.attr('data-animstep', 'step'+currentstepcount);						
						$radarsweepercontainer.attr('data-animstep', 'step'+currentstepcount);
						currentstepcount == 1 ? $bearinginstruction.css({"right": '60%', "opacity": '0'}) : null;
						
						// put one eventhandler not on - every time the plane
						// animates it should be click prevented and on its
						// animation end be clickable
						$plane.one(transitionend, function() {
							$plane.css('pointer-events', 'auto');

							// update bearing text
							$bearingtextpara.html(updatebearingtext(currentstepcount,"text"));
							$anglevalue.html(updatebearingtext(currentstepcount,"angle"));
							$bearingvalue.html(updatebearingtext(currentstepcount,"bearing"));
							// highlight text inside miscblock
							texthighlight($miscblock);						
						});
					}

					else if(currentstepcount >= totalsteps-1){
						currentstepcount++;		
						$("#degreecircle").attr('data-animstep', 'step'+currentstepcount);
						$plane.attr('data-animstep', 'step'+currentstepcount);
						$radarsweepercontainer.attr('data-animstep', 'step'+currentstepcount);

						// put one eventhandler not on - every time the plane
						// animates it should be click prevented and on its
						// animation end be clickable
						$plane.one(transitionend, function() {						
							// update bearing text
							$bearingtextpara.html(updatebearingtext(currentstepcount,"text"));
							$anglevalue.html(updatebearingtext(currentstepcount,"angle"));
							$bearingvalue.html(updatebearingtext(currentstepcount,"bearing"));
							// highlight text inside miscblock
							texthighlight($miscblock);
							
							$resetbutton.show(0);				
						});						
					}			
				});

				// resetbutton click handler
				$resetbutton.on('click', function() {
					$nextBtn.show(0);
					$(this).css('display', 'none');
					currentstepcount = 0;
					$("#degreecircle").attr('data-animstep', 'step'+currentstepcount); 
					$plane.attr('data-animstep', 'step'+currentstepcount);
					$radarsweepercontainer.attr('data-animstep', 'step'+currentstepcount);
					$plane.css('pointer-events', 'auto');
					$bearinginstruction.css({"right": '58%', "opacity": '1'});

					// update bearing text
					$bearingtextpara.html(updatebearingtext(currentstepcount,"text"));
					$anglevalue.html(updatebearingtext(currentstepcount,"angle"));
					$bearingvalue.html(updatebearingtext(currentstepcount,"bearing"));
					// highlight text inside miscblock
					texthighlight($miscblock);
				});
			});			
		};
	};

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
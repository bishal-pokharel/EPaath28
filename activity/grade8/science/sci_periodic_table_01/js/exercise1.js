var SPACE = " ";
var imgpath = $ref+"/images/exercise1/";

var tempcontent=[
	{	
		imageblock : [
			{
				imagestoshow : [
					{
						
						imgclass : "tripatstyle",
						imgsrc   : imgpath+"tripat.gif",
					},
				]	
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "orangehighlight",
				textclass : "bottomparatextstyle",
				textdata  : data.string.e1text1,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		exerciseblock: [
			{
				flipcontent : [
								{
									frontfaceimgsrc : imgpath+"sodiumatom.png",
									backfaceimgsrc : imgpath+"sodium.png",
									textdata : data.string.e1q1elementname,
								}
							],
				okbuttonlabel : data.string.oktext,
				hintdatahighlightflag        : true,
				hintdatahighlightcustomclass : "orangehighlight",
				userinputs : [
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.protonprefix,
						correctanswerdata    : data.string.e1q1electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.protonhint+SPACE+data.string.e1q1electronprotonatomicnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.electronprefix,
						correctanswerdata    : data.string.e1q1electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.electronhint,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.neutronprefix,
						correctanswerdata    : data.string.e1q1neutronnumbercorrectanswer,
						userhintdata         : data.string.neutronhint+SPACE+data.string.e1q1neutronnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicnumberprefix,
						correctanswerdata    : data.string.e1q1electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.atomicnumberhint,
					},
					{
						datatypeaccept        : "text",
						maxcharactersallowed  : 3,
						isinputcasestrictflag : true,
						inputlabeldata        : data.string.symbolprefix,
						correctanswerdata     : data.string.e1q1symbolcorrectanswer,
						userhintdata          : data.string.e1q1elementname+SPACE+data.string.symbolhintpart1+
												SPACE+data.string.e1q1symbolcorrectanswer+","+SPACE+data.string.symbolhintpart2,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicweightprefix,
						correctanswerdata    : data.string.e1q1atomicweightcorrectanswer,
						userhintdata         : data.string.atomicweighthint,
					},
				],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		exerciseblock: [
			{
				flipcontent : [
								{
									frontfaceimgsrc : imgpath+"carbonatom.png",
									backfaceimgsrc : imgpath+"carbon.png",
									textdata : data.string.e1q2elementname,
								}
							],
				okbuttonlabel : data.string.oktext,
				hintdatahighlightflag        : true,
				hintdatahighlightcustomclass : "orangehighlight",
				userinputs : [
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.protonprefix,
						correctanswerdata    : data.string.e1q2electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.protonhint+SPACE+data.string.e1q2electronprotonatomicnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.electronprefix,
						correctanswerdata    : data.string.e1q2electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.electronhint,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.neutronprefix,
						correctanswerdata    : data.string.e1q2neutronnumbercorrectanswer,
						userhintdata         : data.string.neutronhint+SPACE+data.string.e1q2neutronnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicnumberprefix,
						correctanswerdata    : data.string.e1q2electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.atomicnumberhint,
					},
					{
						datatypeaccept        : "text",
						maxcharactersallowed  : 3,
						isinputcasestrictflag : true,
						inputlabeldata        : data.string.symbolprefix,
						correctanswerdata     : data.string.e1q2symbolcorrectanswer,
						userhintdata          : data.string.e1q2elementname+SPACE+data.string.symbolhintpart1+
												SPACE+data.string.e1q2symbolcorrectanswer+","+SPACE+data.string.symbolhintpart2,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicweightprefix,
						correctanswerdata    : data.string.e1q2atomicweightcorrectanswer,
						userhintdata         : data.string.atomicweighthint,
					},
				],
			},
		],
	},	
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		exerciseblock: [
			{
				flipcontent : [
								{
									frontfaceimgsrc : imgpath+"neonatom.png",
									backfaceimgsrc : imgpath+"neon.png",
									textdata : data.string.e1q3elementname,
								}
							],
				okbuttonlabel : data.string.oktext,
				hintdatahighlightflag        : true,
				hintdatahighlightcustomclass : "orangehighlight",
				userinputs : [
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.protonprefix,
						correctanswerdata    : data.string.e1q3electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.protonhint+SPACE+data.string.e1q3electronprotonatomicnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.electronprefix,
						correctanswerdata    : data.string.e1q3electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.electronhint,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.neutronprefix,
						correctanswerdata    : data.string.e1q3neutronnumbercorrectanswer,
						userhintdata         : data.string.neutronhint+SPACE+data.string.e1q3neutronnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicnumberprefix,
						correctanswerdata    : data.string.e1q3electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.atomicnumberhint,
					},
					{
						datatypeaccept        : "text",
						maxcharactersallowed  : 3,
						isinputcasestrictflag : true,
						inputlabeldata        : data.string.symbolprefix,
						correctanswerdata     : data.string.e1q3symbolcorrectanswer,
						userhintdata          : data.string.e1q3elementname+SPACE+data.string.symbolhintpart1+
												SPACE+data.string.e1q3symbolcorrectanswer+","+SPACE+data.string.symbolhintpart2,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicweightprefix,
						correctanswerdata    : data.string.e1q3atomicweightcorrectanswer,
						userhintdata         : data.string.atomicweighthint,
					},
				],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		exerciseblock: [
			{
				flipcontent : [
								{
									frontfaceimgsrc : imgpath+"hydrogenatom.png",
									backfaceimgsrc : imgpath+"hydrogen.png",
									textdata : data.string.e1q4elementname,
								}
							],
				okbuttonlabel : data.string.oktext,
				hintdatahighlightflag        : true,
				hintdatahighlightcustomclass : "orangehighlight",
				userinputs : [
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.protonprefix,
						correctanswerdata    : data.string.e1q4electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.protonhint+SPACE+data.string.e1q4electronprotonatomicnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.electronprefix,
						correctanswerdata    : data.string.e1q4electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.electronhint,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.neutronprefix,
						correctanswerdata    : data.string.e1q4neutronnumbercorrectanswer,
						userhintdata         : data.string.neutronhint+SPACE+data.string.e1q4neutronnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicnumberprefix,
						correctanswerdata    : data.string.e1q4electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.atomicnumberhint,
					},
					{
						datatypeaccept        : "text",
						maxcharactersallowed  : 3,
						isinputcasestrictflag : true,
						inputlabeldata        : data.string.symbolprefix,
						correctanswerdata     : data.string.e1q4symbolcorrectanswer,
						userhintdata          : data.string.e1q4elementname+SPACE+data.string.symbolhintpart1+
												SPACE+data.string.e1q4symbolcorrectanswer+","+SPACE+data.string.symbolhintpart2,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicweightprefix,
						correctanswerdata    : data.string.e1q4atomicweightcorrectanswer,
						userhintdata         : data.string.atomicweighthint,
					},
				],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle exercisequestionheader",
				textdata  : data.string.e1heading1,
			},
		],
		exerciseblock: [
			{
				flipcontent : [
								{
									frontfaceimgsrc : imgpath+"aluminiumatom.png",
									backfaceimgsrc : imgpath+"aluminium.png",
									textdata : data.string.e1q5elementname,
								}
							],
				okbuttonlabel : data.string.oktext,
				hintdatahighlightflag        : true,
				hintdatahighlightcustomclass : "orangehighlight",
				userinputs : [
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.protonprefix,
						correctanswerdata    : data.string.e1q5electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.protonhint+SPACE+data.string.e1q5electronprotonatomicnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.electronprefix,
						correctanswerdata    : data.string.e1q5electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.electronhint,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.neutronprefix,
						correctanswerdata    : data.string.e1q5neutronnumbercorrectanswer,
						userhintdata         : data.string.neutronhint+SPACE+data.string.e1q5neutronnumbercorrectanswer+".",
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicnumberprefix,
						correctanswerdata    : data.string.e1q5electronprotonatomicnumbercorrectanswer,
						userhintdata         : data.string.atomicnumberhint,
					},
					{
						datatypeaccept        : "text",
						maxcharactersallowed  : 3,
						isinputcasestrictflag : true,
						inputlabeldata        : data.string.symbolprefix,
						correctanswerdata     : data.string.e1q5symbolcorrectanswer,
						userhintdata          : data.string.e1q5elementname+SPACE+data.string.symbolhintpart1+
												SPACE+data.string.e1q5symbolcorrectanswer+","+SPACE+data.string.symbolhintpart2,
					},
					{
						datatypeaccept       : "number",
						maxcharactersallowed : 2,
						inputlabeldata       : data.string.atomicweightprefix,
						correctanswerdata    : data.string.e1q5atomicweightcorrectanswer,
						userhintdata         : data.string.atomicweighthint,
					},
				],
			},
		],
	},
	{	
		imageblock : [
			{
				imagestoshow : [
					{
						
						imgclass : "tripatmusclestyle",
						imgsrc   : imgpath+"tripatmuscle.png",
					},
				]	
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "orangehighlight",
				textclass : "bottomparatextstyle",
				textdata  : data.string.e1text2,
			}
		],
	},	
];


Array.prototype.shufflearraymiddle = function(){
  var i = this.length-2, j, temp;
	    while(--i > 0){
	        j = Math.floor((Math.random() * (i+1)) + 1);
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var content = tempcontent.shufflearraymiddle();

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html()); 

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// for diy blocks only
		var $exerciseblock = $board.find('div.exerciseblock');

		if($exerciseblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.hideNotification();

			var $flipcontainer       = $exerciseblock.find("div.flipcontainer");
			var $userinputshints     = $exerciseblock.find("div.userinputshints");
			var $diytextinputs       = $userinputshints.find("input[data-datatypeaccept^='text']");
			var $diynumericinputs    = $userinputshints.find("input[data-datatypeaccept^='number']");
			var $okbutton            = $userinputshints.find("button.okbutton");
			var $userhintpara        = $userinputshints.find("p.userhintpara");
			var $diyinputs           = $userinputshints.find("input[name^='name']");
			var totaldiyinputs       = $diyinputs.length;
			var currentdiyinputindex = 0; /*this should be the first index base 0*/
			var currenthint;
			var currentcorrectanswer;
			var currentuseranswer;
			var $currentdiyinput;
			var $nextdiyinputdiv;
			var $nextdiyinput;
			var errorcount = 0;


			// event-handler for numeric inputs types to accept only numbers
			$diynumericinputs.keyup(function() {	
				// accept only numbers or digits
				$(this).val($(this).val().replace(/[^0-9]/g,''));
			});

			// event-handler for text inputs types to accept only text
			$diytextinputs.keyup(function() {	
				// accept only text
				$(this).val($(this).val().replace(/[^a-zA-Z]/g,''));
			});

			// event-handler for text inputs , show ok button only when some 
			// acceptable value is provided
			$diyinputs.keyup(function(event) {
				if(event.which === 13) {
			        $okbutton.trigger("click");
    			}	
    			else{
    				$(this).val() ? $okbutton.css("display","block") : $okbutton.css('display', 'none');
    			}				
			});

			$currentdiyinput = $userinputshints.find("input[name='name"+currentdiyinputindex+"']");
			// focus on first input
			$userinputshints.one('click', function() {
				$currentdiyinput.focus();
			});

			$okbutton.on('click', function() {
				$currentdiyinput = $userinputshints.find("input[name='name"+currentdiyinputindex+"']");
				currenthint = $currentdiyinput.attr('data-userhint');

				// check if answer should be case strict
				if($currentdiyinput.attr("data-datatypeaccept") == "text" && $currentdiyinput.attr("data-isinputcasestrict") != "true"){
					currentcorrectanswer = $currentdiyinput.attr('data-correctanswer');
					currentcorrectanswer = currentcorrectanswer.toLowerCase();
					currentuseranswer = $currentdiyinput.val();
					currentuseranswer = currentuseranswer.toLowerCase();
				}
				else{
					currentcorrectanswer = $currentdiyinput.attr('data-correctanswer');
					currentuseranswer = $currentdiyinput.val();
				}

				$nextdiyinputdiv = $userinputshints.find("div[class='input"+(currentdiyinputindex+1)+"']");
				$nextdiyinput= $nextdiyinputdiv.find("input[name='name"+(currentdiyinputindex+1)+"']");
				
				if(currentuseranswer == currentcorrectanswer){
					$okbutton.css('display', 'none');
					$currentdiyinput.prop('disabled', true);					
					$userhintpara.css('display', 'none');
					if(currentdiyinputindex+1 < totaldiyinputs){
						$nextdiyinputdiv.css('display', 'block');
						$nextdiyinput.focus();
						errorcount = 0;
						currentdiyinputindex++;
					}
					else{
						$flipcontainer.attr('data-isclicked', 'clicked');
						navigationcontroller();
						$prevBtn.css('display', 'none');
					}				
				}

				else{
					$currentdiyinput.val("");
					$currentdiyinput.focus();
					$okbutton.css('display', 'none');
					$userhintpara.html("<span class='text-danger'>"+data.string.tryagaintext+"</span>");
					$userhintpara.css('display', 'block');
					if(++errorcount > 1)
					{
						$userhintpara.html("<span class='text-danger'>"+data.string.tryagaintext+"</span>"+"<br>"+"<span class='text-muted'>"+data.string.hinttext+"</span>"+" : "+"<span class='text-info'>"+currenthint+"</span>");	
						texthighlight($userinputshints);					
					}

				}
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		countNext == content.length-1 ? $prevBtn.css('display', 'none') : null;

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
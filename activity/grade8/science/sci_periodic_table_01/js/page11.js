var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page11/";

var content=[	
	{	
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p11heading1,
			},
		],
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle",
				textdata          : data.string.p11text1,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		interactiveperiodictable : [
			{
				periods : [
					{
		/* atomic-number, name, symbol,neutron-count,element-class,which-column,group,period*/
		/*       0,        1,     2,         3,            4,           5,        6,    7  */
						element : [
							"1,Hydrogen,H,0,non-metal#natural#s-block,1,IA,1",
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							"2,Helium,He,2,non-metal#natural#p-block#noble-gas,18,VIIIA,",
						],
					},
					{
						element : [
							"3,Lithium,Li,4,metal#alkalimetal#natural#s-block,1,,2",
							"4,Beryllium,Be,5,metal#alkalinemetal#natural#s-block,2,IIA,",
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							"5,Boron,B,6,metalloid#natural#p-block,13,IIIA,",
							"6,Carbon,C,6,non-metal#natural#p-block,14,IVA,",
							"7,Nitrogen,N,7,non-metal#natural#p-block,15,VA,",
							"8,Oxygen,O,8,non-metal#natural#p-block,16,VIA,",
							"9,Fluorine,F,10,non-metal#natural#halogen#p-block,17,VIIA,",
							"10,Neon,Ne,10,non-metal#natural#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"11,Sodium,Na,12,metal#alkalimetal#natural#s-block,1,,3",
							"12,Magnesium,Mg,12,metal#alkalinemetal#natural#s-block,2",
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							SPACE+","+SPACE+","+SPACE+","+SPACE+",emptybox,"+SPACE,
							"13,Aluminium,Al,14,metal#natural#p-block,13",
							"14,Silicon,Si,14,metalloid#natural#p-block,14",
							"15,Phosphorus,P,16,non-metal#natural#p-block,15",
							"16,Sulfur,S,16,non-metal#natural#p-block,16",
							"17,Chlorine,Cl,18,non-metal#natural#halogen#p-block,17",
							"18,Argon,Ar,22,non-metal#natural#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"19,Potassium,K,21,metal#alkalimetal#natural#s-block,1,,4",
							"20,Calcium,Ca,20,metal#alkalinemetal#natural#s-block,2",
							"21,Scandium,Sc,24,metal#transition-metal#natural#d-block,3,IIIB,",
							"22,Titanium,Ti,26,metal#transition-metal#natural#d-block,4,IVB,",
							"23,Vanadium,V,28,metal#transition-metal#natural#d-block,5,VB,",
							"24,Chromium,Cr,28,metal#transition-metal#natural#d-block,6,VIB,",
							"25,Manganese,Mn,30,metal#transition-metal#natural#d-block,7,VIIB,",
							"26,Iron,Fe,30,metal#transition-metal#natural#d-block,8,,",
							"27,Cobalt,Co,31,metal#transition-metal#natural#d-block,9,VIIIB,",
							"28,Nickel,Ni,30,metal#transition-metal#natural#d-block,10,,",
							"29,Copper,Cu,35,metal#transition-metal#natural#d-block,11,IB,",
							"30,Zinc,Zn,35,metal#transition-metal#natural#d-block,12,IIB,",
							"31,Gallium,Ga,39,metal#natural#p-block,13",
							"32,Germanium,Ge,41,metalloid#natural#p-block,14",
							"33,Arsenic,As,42,metalloid#natural#p-block,15",
							"34,Selenium,Se,45,non-metal#natural#p-block,16",
							"35,Bromine,Br,45,non-metal#natural#halogen#p-block,17",
							"36,Krypton,Kr,48,non-metal#natural#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"37,Rubidium,Rb,48,metal#alkalimetal#natural#s-block,1,,5",
							"38,Strontium,Sr,50,metal#alkalinemetal#natural#s-block,2",
							"39,Yttrium,Y,50,metal#transition-metal#natural#d-block,3",
							"40,Zirconium,Zr,51,metal#transition-metal#natural#d-block,4",
							"41,Niobium,Nb,52,metal#transition-metal#natural#d-block,5",
							"42,Molybdenum,Mo,54,metal#transition-metal#natural#d-block,6",
							"43,Technetium,Tc,55,metal#transition-metal#natural#d-block,7",
							"44,Ruthenium,Ru,57,metal#transition-metal#natural#d-block,8",
							"45,Rhodium,Rh,58,metal#transition-metal#natural#d-block,9",
							"46,Palladium,Pd,60,metal#transition-metal#natural#d-block,10",
							"47,Silver,Ag,61,metal#transition-metal#natural#d-block,11",
							"48,Cadmium,Cd,64,metal#transition-metal#natural#d-block,12",
							"49,Indium,In,66,metal#natural#p-block,13",
							"50,Tin,Sn,69,metal#natural#p-block,14",
							"51,Antimony,Sb,71,metalloid#natural#p-block,15",
							"52,Tellurium,Te,76,metalloid#natural#p-block,16",
							"53,Iodine,I,74,non-metal#natural#halogen#p-block,17",
							"54,Xenon,Xe,77,non-metal#natural#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"55,Caesium,Cs,78,metal#alkalimetal#natural#s-block,1,,6",
							"56,Barium,Ba,81,metal#alkalinemetal#natural#s-block,2",
							"57-71,Lanthanide,"+SPACE+",,metal#lanthanides#natural#f-block,3",
							"72,Hafnium,Hf,106,metal#transition-metal#natural#d-block,4",
							"73,Tantalum,Ta,108,metal#transition-metal#natural#d-block,5",
							"74,Tungsten,W,110,metal#transition-metal#natural#d-block,6",
							"75,Rhenium,Re,111,metal#transition-metal#natural#d-block,7",
							"76,Osmium,Os,114,metal#transition-metal#natural#d-block,8",
							"77,Iridium,Ir,115,metal#transition-metal#natural#d-block,9",
							"78,Platinum,Pt,117,metal#transition-metal#natural#d-block,10",
							"79,Gold,Au,118,metal#transition-metal#natural#d-block,11",
							"80,Mercury,Hg,121,metal#transition-metal#natural#d-block,12",
							"81,Thallium,Tl,123,metal#natural#p-block,13",
							"82,Lead,Pb,125,metal#natural#p-block,14",
							"83,Bismuth,Bi,126,metal#natural#p-block,15",
							"84,Polonium,Po,125,metalloid#natural#p-block,16",
							"85,Astatine,At,125,non-metal#natural#halogen#p-block,17",
							"86,Radon,Rn,136,non-metal#natural#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"87,Francium,Fr,136,metal#alkalimetal#natural#s-block,1,,7",
							"88,Radium,Ra,138,metal#alkalinemetal#natural#s-block,2",
							"89-103,Actinides,"+SPACE+",,metal#actinides#natural#f-block,3",
							"104,Rutherfordium,Rf,157,metal#transition-metal#artificial#d-block,4",
							"105,Dubnium,Db,N/A,metal#transition-metal#artificial#d-block,5",
							"106,Seaborgium,Sg,157,metal#transition-metal#artificial#d-block,6",
							"107,Bohrium,Bh,157,metal#transition-metal#artificial#d-block,7",
							"108,Hassium,Hs,161,metal#transition-metal#artificial#d-block,8",
							"109,Meitnerium,Mt,159,metal#transition-metal#artificial#d-block,9",
							"110,Darmstadtium,Ds,162,metal#transition-metal#artificial#d-block,10",
							"111,Roentgenium,Rg,162,metal#transition-metal#artificial#d-block,11",
							"112,Copernicium,Cn,165,metal#transition-metal#artificial#d-block,12",
							"113,Ununtrium,Uut,173,metal#artificial#p-block,13",
							"114,Ununquadium,Uuq,175,metal#artificial#p-block,14",
							"115,Ununpentium,Uup,173,metal#artificial#p-block,15",
							"116,Ununhexium,Uuh,176,metal#artificial#p-block,16",
							"117,Ununseptium,Uus,175,non-metal#artificial#p-block#halogen,17",
							"118,Ununoctium,Uuo,175,non-metal#artificial#noble-gas#p-block,18",
						],
					},
					{
						element : [
							"57,Lanthanum,La,82,metal#lanthanides#natural#f-block,3",
							"58,Cerium,Ce,82,metal#lanthanides#natural#f-block,3",
							"59,Praseodymium,Pr,82,metal#lanthanides#natural#f-block,3",
							"60,Neodymium,Nd,84,metal#lanthanides#natural#f-block,3",
							"61,Promethium,Pm,84,metal#lanthanides#natural#f-block,3",
							"62,Samarium,Sm,88,metal#lanthanides#natural#f-block,3",
							"63,Europium,Eu,89,metal#lanthanides#natural#f-block,3",
							"64,Gadolinium,Gd,93,metal#lanthanides#natural#f-block,3",
							"65,Terbium,Tb,94,metal#lanthanides#natural#f-block,3",
							"66,Dysprosium,Dy,97,metal#lanthanides#natural#f-block,3",
							"67,Holmium,Ho,98,metal#lanthanides#natural#f-block,3",
							"68,Erbium,Er,99,metal#lanthanides#natural#f-block,3",
							"69,Thulium,Tm,100,metal#lanthanides#natural#f-block,3",
							"70,Ytterbium,Yb,103,metal#lanthanides#natural#f-block,3",
							"71,Lutetium,Lu,104,metal#lanthanides#natural#f-block,3",
						],
					},
					{
						element : [
							"89,Actinium,Ac,138,metal#actinides#natural#f-block,3",
							"90,Thorium,Th,142,metal#actinides#natural#f-block,3",
							"91,Protactinium,Pa,140,metal#actinides#natural#f-block,3",
							"92,Uranium,U,146,metal#actinides#natural#f-block,3",
							"93,Neptunium,Np,144,metal#actinides#artificial#f-block,3",
							"94,Plutonium,Pu,150,metal#actinides#artificial#f-block,3",
							"95,Americium,Am,148,metal#actinides#artificial#f-block,3",
							"96,Curium,Cm,151,metal#actinides#artificial#f-block,3",
							"97,Berkelium,Bk,150,metal#actinides#artificial#f-block,3",
							"98,Californium,Cf,153,metal#actinides#artificial#f-block,3",
							"99,Einsteinium,Es,153,metal#actinides#artificial#f-block,3",
							"100,Fermium,Fm,157,metal#actinides#artificial#f-block,3",
							"101,Mendelevium,Md,157,metal#actinides#artificial#f-block,3",
							"102,Nobelium,No,157,metal#actinides#artificial#f-block,3",
							"103,Lawrencium,Lr,159,metal#actinides#artificial#f-block,3",
						],
					},
				],
				elementdivisions : [
					{
						elementdivision1 : [
							{ elementdivisionid : "metal", elementdivisionname : data.string.p11textmetal+"<br>",},
							{ elementdivisionid : "non-metal", elementdivisionname : data.string.p11textnonmetal+"<br>",},
							{ elementdivisionid : "metalloid", elementdivisionname : data.string.p11textmetalloid+"<br>",},
							{ elementdivisionid : "halogen", elementdivisionname : data.string.p11texthalogen+"<br>",},
							{ elementdivisionid : "noble-gas", elementdivisionname : data.string.p11textnoblegas+"<br>",},							
							{ elementdivisionid : "s-block", elementdivisionname : data.string.p11textsblock+"<br>",},
							{ elementdivisionid : "d-block", elementdivisionname : data.string.p11textdblock+"<br>",},							
							{ elementdivisionid : "actinides", elementdivisionname : data.string.p11textactinide+"<br>",},
						],
						elementdivision2 : [
							{ elementdivisionid : "alkalinemetal", elementdivisionname : data.string.p11textalkalineearth+"<br>",},
							{ elementdivisionid : "alkalimetal", elementdivisionname : data.string.p11textalkali+"<br>",},
							{ elementdivisionid : "transition-metal", elementdivisionname : data.string.p11texttransition+"<br>",},
							{ elementdivisionid : "natural", elementdivisionname : data.string.p11textnatural+"<br>",},
							{ elementdivisionid : "artificial", elementdivisionname : data.string.p11textartificial+"<br>",},
							{ elementdivisionid : "p-block", elementdivisionname : data.string.p11textpblock+"<br>",},
							{ elementdivisionid : "f-block", elementdivisionname : data.string.p11textfblock+"<br>",},
							{ elementdivisionid : "lanthanides", elementdivisionname : data.string.p11textlanthanide+"<br>",},
						]
					}
				],
				elementsecondarydescription : [
					{
						protontext : data.string.textproton+":",
						spacecharacter : SPACE,
						neutrontext : data.string.textneutron+":",
						electrontext : data.string.textelectron+":",
					}
				],
			},
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
    /*==========  register the handlebar partials first  ==========*/
	   	 Handlebars.registerHelper('indexbase1', function(number) {
	   	 	if(number < 7){
	   	 		return number + 1;
	   	 	}

	   	 	else if(number == 7){
	   	 		return 6;
	   	 	}

	   	 	else if(number == 8){
	   	 		return 7;
	   	 	}
		 });

		 Handlebars.registerHelper('indexbase1forperiod', function(number) {
	   	 	if(number < 7){
	   	 		return number + 1;
	   	 	}

	   	 	else if(number == 7){
	   	 		return 6+"overflow";
	   	 	}

	   	 	else if(number == 8){
	   	 		return 7+"overflow";
	   	 	}	    	
		 });

	   	 var elementdata = [];
	   	 var columnumber;
	   	 Handlebars.registerHelper('dataextract', function(elementdetail, index) {
		 	elementdata = elementdetail.split(",");
		 	if(index != 5){
		 		return elementdata[parseInt(index)];
		 	}

		 	// for checking out the column number
		 	else if(index === 5){
		 		columnumber = elementdata[parseInt(index)];

		 		// for flagging group VIIIB elements
		 		if(columnumber == 8 || columnumber == 9 || columnumber == 10){
		 			return 9;
		 		}

		 		// for flagging all other group elements
		 		else{
		 			return columnumber;
		 		}
		 	}
	    	
		 });

	   	 var groupdata = [];
	   	 Handlebars.registerHelper('ifhasgroup', function(elementdetail, block) {
		 	groupdata = elementdetail.split(",");
	    	if(groupdata[6]){
	    	 return block.fn(this);
	    	};
		 });

		 var perioddata = [];
	   	 Handlebars.registerHelper('ifhasperiod', function(elementdetail, block) {
		 	perioddata = elementdetail.split(",");
	    	if(perioddata[7]){
	    	 return block.fn(this);
	    	};
		 });

		 var elementclass = [];
		 Handlebars.registerHelper('separatehash', function(elementdetail) {
		 	elementclass = elementdetail.split(",");
	    	return elementclass[4].replace(/#/g," ");
		 });

		 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
		 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
		 Handlebars.registerPartial("interactiveperiodictablecontent", $("#interactiveperiodictablecontent-partial").html());
	
	/*===============================================
	=            data highlight function            =
	===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//just for svgblock
		var $interactiveperiodictable = $board.find('div.interactiveperiodictable');
		// if there is a svg content
		if($interactiveperiodictable.length > 0){
			var $allelements                 = $interactiveperiodictable.find("div[class*='element']");
			var $groups                      = $interactiveperiodictable.find("span[id$='columnnumber']");
			var $periods                     = $interactiveperiodictable.find("span[id$='rownumber']");
			var $elementdivisioncontainer    = $interactiveperiodictable.children("div#elementdivisions");
			var $elementdivisions            = $elementdivisioncontainer.find("span");
			var $elementdescription          = $interactiveperiodictable.children("div#elementdescription");
			var $elementdescriptionmain      = $elementdescription.find("div#elementdescriptionmain");
			var $elementcount                = $elementdescriptionmain.children('p#elementcount');
			var $elementsymbol               = $elementdescriptionmain.children('p#elementsymbol');
			var $elementname                 = $elementdescriptionmain.children('p#elementname');
			var $elementdescriptionsecondary = $elementdescription.find("div#elementdescriptionsecondary");
			var $protonelectroncount         = $elementdescriptionsecondary.children('p:nth-of-type(odd)').children('span:nth-of-type(2)');
			var $neutroncount                = $elementdescriptionsecondary.children('p:nth-of-type(2)').children('span:nth-of-type(2)');
			
			var hoveredgroupcolumnnumber,
				$tohighlightcolumns,
				hoveredperiodrownumber,
				$tohighlightrows,
				hoveredelementdivision,
				$tohighlightelementdivision;

			var eleprotonelectroncount, elesymbol, elename, eleneutroncount;

			$groups.hover(function() {
				/* Stuff to do when the mouse enters the element */
				$allelements.attr({"data-selectedclass":""});
				hoveredgroupcolumnnumber = parseInt($(this).attr("id"));
				$tohighlightcolumns      = $interactiveperiodictable.find("div.incolumn"+hoveredgroupcolumnnumber);
				$tohighlightcolumns.attr("data-highlightclass","elementhighlightstyle");
			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$allelements.attr({"data-highlightclass":""});
			});

			$periods.hover(function() {
				/* Stuff to do when the mouse enters the element */
				$allelements.attr({"data-selectedclass":""});
				hoveredperiodrownumber = parseInt($(this).attr("id"));
				$tohighlightrows       = $interactiveperiodictable.find("div.inrow"+hoveredperiodrownumber+":not('.emptybox')");
				$tohighlightrows.attr("data-highlightclass","elementhighlightstyle");
			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$allelements.attr({"data-highlightclass":""});
			});

			$elementdivisions.hover(function() {
				/* Stuff to do when the mouse enters the element */
				$allelements.attr({"data-selectedclass":""});
				hoveredelementdivision      = $(this).attr("id");
				$tohighlightelementdivision = $allelements.filter("div."+hoveredelementdivision);
				$tohighlightelementdivision.attr("data-highlightclass","elementhighlightstyle");
			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$allelements.attr({"data-highlightclass":""});
			});

			$allelements.on('click', function() {
				$allelements.attr({"data-selectedclass":""});
				$(this).attr("data-selectedclass","elementselectstyle");
				eleprotonelectroncount = $(this).attr('data-atomicnumber');
				elesymbol              = $(this).attr('data-symbol');
				elename                = $(this).attr('data-elementname');
				eleneutroncount        = $(this).attr('data-neutroncount');

				$elementcount.html(eleprotonelectroncount);
				$elementsymbol.html(elesymbol);
				$elementname.html(elename);
				$protonelectroncount.html(eleprotonelectroncount);
				$neutroncount.html(eleneutroncount);
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller(true);

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
// prototype for array shuffle
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var imgpath = $ref+"/images/exercise2/";
var correctimg = "images/correct.png";
var incorrectimg = "images/wrong.png";
var congratulationimgarray = [
								"images/quizcongratulation/gradea.png",
								"images/quizcongratulation/gradeb.png",
								"images/quizcongratulation/gradec.png",
							];
var tempcontent=[
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questionwhichgroup,
		questionimgsrc      : imgpath+"group8b.svg",
		optionsdata : [
			{ optionstext : data.string.e2wronggroup1, },
			{ optionstext : data.string.e2wronggroup2, },
			{ optionstext : data.string.e2group8b, isdatacorrect : true, },
			{ optionstext : data.string.e2wronggroup3, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questionwhichperiod,
		questionimgsrc      : imgpath+"period5.svg",
		optionsdata : [
			{ optionstext : data.string.e2wrongperiod3, },
			{ optionstext : data.string.e2period5, isdatacorrect : true, },
			{ optionstext : data.string.e2wrongperiod2, },
			{ optionstext : data.string.e2wrongperiod1, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questionelementposition,
		questionimgsrc      : imgpath+"iron.svg",
		optionsdata : [
			{ optionstext : data.string.e2actinideposition, },
			{ optionstext : data.string.e2wrongelementposition1, },
			{ optionstext : data.string.e2goldposition, },
			{ optionstext : data.string.e2ironposition, isdatacorrect : true, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questionelementposition,
		questionimgsrc      : imgpath+"gold.svg",
		optionsdata : [
			{ optionstext : data.string.e2goldposition, isdatacorrect : true, },
			{ optionstext : data.string.e2wrongelementposition1, },
			{ optionstext : data.string.e2ironposition, },
			{ optionstext : data.string.e2wrongelementposition2, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiongroupelementposition,
		questionimgsrc      : imgpath+"actinides.svg",
		optionsdata : [
			{ optionstext : data.string.e2ironposition, },
			{ optionstext : data.string.e2wrongelementposition1, },
			{ optionstext : data.string.e2actinideposition, isdatacorrect : true, },
			{ optionstext : data.string.e2wrongelementposition2, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"artificial.svg",
		optionsdata : [
			{ optionstext : data.string.e2natural, },
			{ optionstext : data.string.e2metalloid, },
			{ optionstext : data.string.e2artificial, isdatacorrect : true, },
			{ optionstext : data.string.e2metal, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"natural.svg",
		optionsdata : [
			{ optionstext : data.string.e2alkaline, },
			{ optionstext : data.string.e2halogen, },
			{ optionstext : data.string.e2alkali, },
			{ optionstext : data.string.e2natural, isdatacorrect : true, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"fblock.svg",
		optionsdata : [
			{ optionstext : data.string.e2fblock, isdatacorrect : true, },
			{ optionstext : data.string.e2lanthanide, },
			{ optionstext : data.string.e2actinide, },
			{ optionstext : data.string.e2alkaline, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"dblock.svg",
		optionsdata : [
			{ optionstext : data.string.e2pblock, },
			{ optionstext : data.string.e2lanthanide, },
			{ optionstext : data.string.e2dblock, isdatacorrect : true, },			
			{ optionstext : data.string.e2alkali, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"pblock.svg",
		optionsdata : [
			{ optionstext : data.string.e2pblock, isdatacorrect : true, },
			{ optionstext : data.string.e2lanthanide,},
			{ optionstext : data.string.e2dblock,  },			
			{ optionstext : data.string.e2fblock, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"sblock.svg",
		optionsdata : [
			{ optionstext : data.string.e2natural,},
			{ optionstext : data.string.e2alkali,  },
			{ optionstext : data.string.e2sblock, isdatacorrect : true, },
			{ optionstext : data.string.e2lanthanide, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"actinides.svg",
		optionsdata : [
			{ optionstext : data.string.e2artificial,},
			{ optionstext : data.string.e2actinide, isdatacorrect : true, },
			{ optionstext : data.string.e2alkali,  },
			{ optionstext : data.string.e2lanthanide, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"lanthanides.svg",
		optionsdata : [
			{ optionstext : data.string.e2natural,},
			{ optionstext : data.string.e2actinide, },
			{ optionstext : data.string.e2metalloid,  },
			{ optionstext : data.string.e2lanthanide, isdatacorrect : true, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"metalloids.svg",
		optionsdata : [
			{ optionstext : data.string.e2metalloid, isdatacorrect : true, },
			{ optionstext : data.string.e2fblock, },
			{ optionstext : data.string.e2natural,  },
			{ optionstext : data.string.e2artificial,},
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"nonmetals.svg",
		optionsdata : [
			{ optionstext : data.string.e2sblock, },
			{ optionstext : data.string.e2metalloid, },
			{ optionstext : data.string.e2lanthanide,  },
			{ optionstext : data.string.e2nonmetal, isdatacorrect : true,},
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"metals.svg",
		optionsdata : [
			{ optionstext : data.string.e2sblock, },
			{ optionstext : data.string.e2metal, isdatacorrect : true,},
			{ optionstext : data.string.e2lanthanide,  },
			{ optionstext : data.string.e2metalloid, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"alkali.svg",
		optionsdata : [
			{ optionstext : data.string.e2halogen, },
			{ optionstext : data.string.e2alkaline, },
			{ optionstext : data.string.e2alkali, isdatacorrect : true, },
			{ optionstext : data.string.e2noblegas, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"alkalineearth.svg",
		optionsdata : [
			{ optionstext : data.string.e2halogen, },
			{ optionstext : data.string.e2alkaline, isdatacorrect : true, },
			{ optionstext : data.string.e2alkali, },
			{ optionstext : data.string.e2noblegas, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"halogens.svg",
		optionsdata : [
			{ optionstext : data.string.e2halogen, isdatacorrect : true,},
			{ optionstext : data.string.e2noblegas, },
			{ optionstext : data.string.e2metal,  },
			{ optionstext : data.string.e2nonmetal, },
		],
	},
	{	
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e2questiontypewhichgroup,
		questionimgsrc      : imgpath+"noblegases.svg",
		optionsdata : [
			{ optionstext : data.string.e2sblock, },
			{ optionstext : data.string.e2metalloid, },
			{ optionstext : data.string.e2lanthanide,  },
			{ optionstext : data.string.e2noblegas, isdatacorrect : true,},
		],
	},
];

// now randomize the question content
var content = tempcontent.shufflearray();

var congratulationcontent = [
	{
		congratulationtextdata : data.string.e2congratulationtext,
		congratulationimgsrc : congratulationimgarray[1],
		congratulationcompletedtextdata : data.string.e2congratulationcompletedtext,
		congratulationyourscoretextdata : data.string.e2congratulationyourscoretext,
		congratulationreviewtextdata : data.string.e2congratulationreviewtext,
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = content.length+congratulationcontent.length;
	loadTimelineProgress($total_page,countNext+1);

	// assign variable with quizboard container and scoreboard elements
	var $quizboard = $board.children('div.quizboard'); 
	var $scoreboard = $board.children('div.scoreboard'); 
	var $scoretext = $scoreboard.children('.scoretext');
	var $scorecount = $scoreboard.children('.scorecount');
	var $userscore = $scorecount.children('.userscore');
	var $totalproblemstext = $scoreboard.children('.totalproblemstext');
	// all elements which contains data about total questions
	var $totalquestiondata = $scoreboard.find('.totalquestiondata');
	var $scoregraph = $scoreboard.children('.scoregraph');
	var $scoregraphchildren;
	var totalquestioncount = 0; /*initiate total question count as 0*/
	var userscorecount = 0;
	var userscorestorage;
	var scoregraphstorehtml;
	var $congratulationscoregraphchildren;

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
		if(countNext >= 0 && countNext < $total_page-1){
			$nextBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	 /*==========  scoreboard update caller  ==========*/
	 function updatescoreboard(){	 	
	 	// for only first call updates
	 	if(countNext == 0){
	 		// update the total question count
	 		$.each(content, function(index, val) {
	 			if(content[index].hasquiztemplate){
	 				totalquestioncount++;
	 			}
	 		});

	 		$scoretext.html(data.string.e2scoretextdata);
	 		$totalquestiondata.html(totalquestioncount);
	 		$totalproblemstext.html(data.string.e2totalproblemtextdata);

	 		// now populate scoregraph block with batch of tags
	 		var blocktag = "<span data-correct=''></span>";

	 		for(var i=1 ; i <= totalquestioncount ; i++){
	 			scoregraph = $scoregraph.html();
	 			$scoregraph.html(scoregraph+blocktag);
	 		}

	 		$scoregraphchildren = $scoregraph.children('span')
	 	}
	 }

	/*==========  quiz template caller  ==========*/

	function quiz(){
		var source = $("#quiz-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the quiz template
		content[countNext].questioncount = countNext+1;
		// update options count in content before updating the quiz template
		content[countNext].numberofoptions = content[countNext].optionsdata.length;	
		// update if quiz board has description content before updating the quiz template	
		content[countNext].hasdescriptionclass = typeof content[countNext].descriptioncontent !== "undefined" ? "hasdescriptionclass" : null;

		var html = template(content[countNext]);
		$quizboard.html(html);

		var $options = $quizboard.children('.options').children('p');
		var clickcount = 0;

		// on options click do following
		$options.on('click', function() {
			// if incorrect is choosen
			$(this).attr("data-isclicked","clicked");


			// on first click only
			if(++clickcount == 1){
				$scoregraphchildren.eq(countNext).attr({
					'data-correct' : $(this).attr("data-correct")
				});
			}
			
			if($(this).attr("data-correct") == "correct"){
				// update isclicked data attribute to clicked
				clickcount == 1 ? $userscore.html(++userscorecount) : null;
				$options.css('pointer-events', 'none');

				/*store the scoregraph and userscore as it is needed on congratulations templates
				when all the questions are attempted*/
				if(countNext+1 == totalquestioncount){
					userscorestorage = $userscore.html();
					scoregraphstorehtml = $scoregraph.html();					
				}

				navigationcontroller();
			}
		});		
	}

	/*==========  congratulations template caller  ==========*/
	
	function congratulation(){
		var source = $("#congratulation-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the congratulation template
		// content[countNext].questioncount = countNext+1;
		
		var html = template(congratulationcontent[0]);
		$board.html(html);

		var $congratulationcontainer = $board.children('.congratulationcontainer');
		var $congratulationreviewtext = $congratulationcontainer.children('.congratulationreviewtext');
		var $congratulationscoregraph = $congratulationcontainer.children('.congratulationscoregraph');
		var $congratulationyourscoretext = $congratulationcontainer.children('.congratulationyourscoretext');
		$congratulationscoregraph.html(scoregraphstorehtml);

		// update the congratulationyourscoretext sentence
		var rawstatement = $congratulationyourscoretext.html();
		rawstatement = rawstatement.replace("#userscore#",userscorestorage);
		rawstatement = rawstatement.replace("#totalscore#",totalquestioncount);
		$congratulationyourscoretext.html(rawstatement);

		$congratulationscoregraphchildren = $congratulationscoregraph.children('span');
		$nextBtn.show(0);
	}

	quiz();
	updatescoreboard();
	// congratulation();
	// summary();
	
	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		// alert(countNext +" and "+content.length);
		if(countNext < content.length){
			quiz();
		}	
		else if(countNext == content.length){
			congratulation();
		}
		else if(countNext == content.length+1){
			ole.activityComplete.finishingcall();
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
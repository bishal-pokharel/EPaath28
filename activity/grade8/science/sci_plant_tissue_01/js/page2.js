var transitionEnd = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page2/";

var content=[
	{
        contentnocenteradjust:true,
		uppertextblock:[
			{
				textclass : "bigheadingcustom",
				textdata : data.string.p2_title,
			}
		]
	},
	{
    	contentnocenteradjust:true,
		uppertextblock:[
			{
				textclass : "bigheadingcustom2",
				textdata : data.string.p2_para1,
			}
		],
		svgblock:[
			{

			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"clickapical cssfadein2",
						imgsrc : imgpath+"clickme_icon.gif",
					},
					{
						imgclass:"clicklateral cssfadein2",
						imgsrc : imgpath+"clickme_icon.gif",
					},
					{
						imgclass:"clickinter cssfadein2",
						imgsrc : imgpath+"clickme_icon.gif",
					}
				],
				imagelabels:[
					{
    					imagelabelclass:"apical",
    					imagelabeldata: data.string.textshoot
    				},
    				{
    					imagelabelclass:"lateral",
    					imagelabeldata: data.string.textlateral
    				},
    				{
    					imagelabelclass:"inter",
    					imagelabeldata: data.string.textroot
    				}
				]
			}
		]
	},
	{
    	contentnocenteradjust:true,
    	uppertextblock:[
    		{
    			textclass:"bigheadingtextstyle changecolor",
    			textdata: data.string.textapical
    		},
    		{
				textclass:"sideparastyle",
				textdata: data.string.textexplain1
    		}
    	],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"svgimg cssfadein",
						imgsrc: imgpath+"apical01.png"
					}
				]
			}
		]
	},
	{
    	contentnocenteradjust:true,
    	uppertextblock:[
    		{
    			textclass:"bigheadingtextstyle changecolor",
    			textdata: data.string.textapical
    		},
    		{
				textclass:"sideparastyle",
				textdata: data.string.textexplain1
    		},
    		{
				textclass:"sideparastyle2",
				textdata: data.string.textexplain2
    		}
    	],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"seedgrow1",
						imgsrc: imgpath+"growth-of-plant01.png"
					},
					{
						imgclass:"seedgrow2 cssfadein",
						imgsrc: imgpath+"growth-of-plant02.png"
					},
					{
						imgclass:"seedgrow3",
						imgsrc: imgpath+"growth-of-plant03.png"
					},
					{
						imgclass:"seedgrow4",
						imgsrc: imgpath+"growth-of-plant04.png"
					},
					{
						imgclass:"seedgrow5",
						imgsrc: imgpath+"growth-of-plant05.png"
					}
				]
			}
		],
	},
	{
		contentnocenteradjust:true,
    	uppertextblock:[
    		{
    			textclass:"fullheading changecolor",
    			textdata: data.string.textlateral
    		},
    		{
    			textclass:"definitionpara",
    			textdata: data.string.textexplain3
    		}
    	],
    	imageblock:[
    		{
    			imagestoshow:[

					{
						imgclass:"middlesvgimg",
						imgsrc: imgpath+"trunkgrowth.svg"
					}
    			]
    		}
    	],
    	lowertextblock:[
    			{
    			datahighlightflag: true,
    			textclass:"onlyparatextstyle customstyle",
    			textdata: data.string.textexplain4
    		}
    	]
	},
	{
		contentnocenteradjust:true,
		uppertextblock:[
			{
				textclass:"fullheading changecolor",
				textdata: data.string.textinter
			},
    		{
    			textclass:"sideparastyle cssright",
    			textdata: data.string.textexplain5
    		}
    	],
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass: "svgimg2",
    					imgsrc:imgpath+"plant_growth.svg",
    				}
    			]
    		}
    	]
	},
	{
		contentnocenteradjust:true,
		uppertextblock:[
			{
				textclass:"bigheadingtextstyle customposition",
				textdata: data.string.textapical
			},
			{
				textclass:"bigheadingtextstyle csscustommiddle",
				textdata: data.string.textlateral
			},
			{
				textclass:"bigheadingtextstyle cssposition",
				textdata: data.string.textinter
			}
		],
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass: "apicalimg",
    					imgsrc:imgpath+"growing-plant.gif",
    				},
    				{
    					imgclass: "lateralimg",
    					imgsrc:imgpath+"trunkgrowth.svg",
    				},
    				{
    					imgclass: "interimg",
    					imgsrc:imgpath+"plant_growth.svg",
    				}
    			]
    		}
    	],
    	lowertextblock:[
    		{
    			textclass:"bottomparatextstyle customcss",
    			textdata: data.string.textexplain6
    		}
    	]
	},
	{
		uppertextblock:[
			{
				textclass: "paratextstyle",
				textdata: data.string.textexplain7
			}
		]
	}
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

	 /*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

        if (countNext==0) {
            $nextBtn.show(0);
        }

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		// $(".sideimage").on(animationEnd, function(){
		// 	$(".apical, .lateral, .inter").addClass("cssfadein2");
		// 	$nextBtn.delay(1000).show(0);
		// });

		var $svgwrapper  = $board.find('div.svgwrapper');
		var $svgcontainer  = $svgwrapper.find('div.svgcontainer');

		// if there is a svg content
			if($svgwrapper.length > 0){

			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"plant01.svg", function (f) {
    		s.append(f);

    			// snap objects
    		var apicalline    = s.select("#apicalline");
    		var lateralline  = s.select("#lateralline");
    		var interline      = s.select("#interline");

    			// jquery objects and js variables
    		var $svg      = $svgcontainer.find('svg');
    		var $apicalline = $svg.find('#apicalline');
    		var $lateralline = $svg.find('#lateralline');
    		var $interline = $svg.find('#interline');

            var clicked1=false;
            var clicked2=false;
            var clicked3=false;
    		$(".clickapical").on("click", function(){
				$(this).hide(0);
                $(".apical").addClass("cssfadein");
				apicalline.addClass("cssfadein");
                clicked1=true;
                if (clicked1==true && clicked2==true && clicked3==true) {
                    $nextBtn.show(0);
                }
			});

			$(".clickinter").on("click", function(){
				$(this).hide(0);
				$(".inter").addClass("cssfadein");
                interline.addClass("cssfadein");
                clicked2=true;
                if (clicked1==true && clicked2==true && clicked3==true) {
                    $nextBtn.show(0);
                }
			});

			$(".clicklateral").on("click", function(){
				$(this).hide(0);
				$(".lateral").addClass("cssfadein");
                lateralline.addClass("cssfadein");
                clicked3=true;
                if (clicked1==true && clicked2==true && clicked3==true) {
                    $nextBtn.show(0);
                }
			});
    	});
	}

		$(".svgimg").on(animationEnd, function(){
			$nextBtn.delay(100).show(0);
		});

        $(".onlyparatextstyle").on(animationEnd, function(){
            $nextBtn.delay(100).show(0);
        });

        $(".sideparastyle").on(animationEnd, function(){
            $nextBtn.delay(100).show(0);
        });

        if (countNext==6) {
            $nextBtn.delay(100).show(0);
        };
		$(".seedgrow2").on(animationEnd, function(){
			$(this).addClass('cssfadeout');
			$(".seedgrow1").hide(0);
			$(".seedgrow3").addClass("cssfadein");
		});
		$(".seedgrow3").on(animationEnd, function(){
			$(this).addClass('cssfadeout');
			$(".seedgrow4").addClass("cssfadein");
		});

		$(".seedgrow4").on(animationEnd, function(){
			$(this).addClass('cssfadeout');
			$(".seedgrow5").addClass("cssfadein");
			$nextBtn.delay(1000).show(0);
		});

		// if (countNext==4) {
		// 	$nextBtn.delay(6000).show(0);
		// };

		// if (countNext==5) {
		// 	$nextBtn.delay(1000).show(0);
		// };

		// $(".bottompara").on(animationEnd, function(){
		// 	$nextBtn.show(0);
		// });

		// $(".headingstyle2").on(animationEnd, function(){
		// 	$(".definitionpara").addClass("cssfadein");
		// });

		// $(".type2").on(animationEnd, function(){
		// 	$nextBtn.show(0);
		// });

		// $(".definitionpara").on(animationEnd, function(){
		// 	$nextBtn.delay(9000).show(0);
		// });

		// $(".root").on(animationEnd, function(){
		// 	$nextBtn.delay(1000).show(0);
		// });

		// $(".parastyle").on(animationEnd, function(){
		// 	$nextBtn.delay(500).show(0);
		// });

		// $(".paratextstyle").on(animationEnd, function(){
		// 	$nextBtn.delay(1000).show(0);
		// });

		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/
		/*switch(countNext){
			case 0 : sometemplate()); break;
			.
			.
			.
			case 5 : someothertemplate(); break;
			default : break;
		}*/

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

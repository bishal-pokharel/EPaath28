var imgpath = $ref+"/images/page3/";
var animationEnd = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
    	contentnocenteradjust:true,
		uppertextblock : [
			{
				textclass : "onlyparatextstyle customposition",
				textdata : data.string.p3text1,
			},
		],
	},
	{
    	contentnocenteradjust:true,

		uppertextblock : [
			{
				textclass : "bigheadingtextstyle changecolor",
				textdata : data.string.textPerm,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "middleimage",
						imgsrc : imgpath+"permanent-tissue01.jpg",
					}
				],
			}
		],
		lowertextblock:[
			{
				textclass: "bottomparatextstyle",
				textdata: data.string.p3text2
			}
		]
	},
	{
    	contentnocenteradjust:true,
		uppertextblock : [
			{
				textclass : "functionheadingstyle",
				textdata : data.string.p3function,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "rightimgstyle",
						imgsrc : imgpath+"tree02.gif",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "sidepara cssfadein",
				textdata : data.string.p3text3,
			}
		],
	},
	{
    	contentnocenteradjust:true,
		uppertextblock : [
			{
				textclass : "functionheadingstyle",
				textdata : data.string.p3function,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "rightimgstyle",
						imgsrc : imgpath+"tree01.gif",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "sidepara1",
				textdata : data.string.p3text3,
			},
			{
				textclass : "sidepara2",
				textdata : data.string.p3text4,
			}
		],
	},
	{
    	contentnocenteradjust:true,
		uppertextblock : [
			{
				textclass : "functionheadingstyle",
				textdata : data.string.p3function,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "rightimgstyle",
						imgsrc : imgpath+"treeouter.svg",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "sidepara1",
				textdata : data.string.p3text3,
			},
			{
				textclass : "sidepara2",
				textdata : data.string.p3text4,
			},
			{
				textclass : "sidepara3",
				textdata : data.string.p3text5,
			}
		],
	},
	{
    	contentnocenteradjust:true,
		uppertextblock : [
			{
				textclass : "functionheadingstyle",
				textdata : data.string.p3function,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "rightimgstylewind",
						imgsrc : imgpath+"wind.gif",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "sidepara1",
				textdata : data.string.p3text3,
			},
			{
				textclass : "sidepara2",
				textdata : data.string.p3text4,
			},
			{
				textclass : "sidepara3",
				textdata : data.string.p3text5,
			},
			{
				textclass : "sidepara4",
				textdata : data.string.p3text6,
			}
		],
	},


]


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	// register the handlebar partials first
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	 // navigation controller function
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*
* mirrorintro
*/
	function mirrorintro() {
		var source = $("#mirrorintro-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);



		// call navigation controller
		navigationcontroller();
	}

	mirrorintro();

	$nextBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext++;
		mirrorintro();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext--;
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		mirrorintro();
		loadTimelineProgress($total_page,countNext+1);
	});
});

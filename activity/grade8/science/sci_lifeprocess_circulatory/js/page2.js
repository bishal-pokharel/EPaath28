// prototype for array shuffle
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

//array of diy squirrel image
	var diyImgPath = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
var diyImg = diyImgPath[randomImageNumeral];
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var imgpath = $ref+"/images/page2/";

var content=[
	{
		// contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "bigheadingside cssfadein2",
				textdata: data.string.titleBlood
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"bloodcartoon picsideanim",
						imgsrc: imgpath+"blood-holding-textbox.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"headingstylepara",
				textdata: data.string.p2para2
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "constituentspara",
				textdata : data.string.p2para3
			}
		],
		usernotificationblock:[
			{
				textclass: "constituents",
				textdata : data.string.titleConstituents
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"bloodcartoonside",
						imgsrc: imgpath+"blood.jpg"
					},
					{
						imgclass:"plasmaimg",
						imgsrc:imgpath+"plasma.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"red",
						imagelabeldata: data.string.titleRed
					},
					{
						imagelabelclass:"white",
						imagelabeldata: data.string.titleWhite
					},
					{
						imagelabelclass:"plat",
						imagelabeldata: data.string.titlePlat
					},
					{
						imagelabelclass:"plas",
						imagelabeldata: data.string.titlePlasma
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"bloodcartoonside",
						imgsrc: imgpath+"blood.jpg"
					},
				]
			}
		],
		lowertextblock:[
			{
				textclass:"functions",
				textdata: data.string.titleFunctions
			},
			{
				datahighlightflag: true,
				textclass:"lowertextstyle cssfadein",
				textdata: data.string.p2para4
			}
		]
	},
	{
		uppertextblock:[
			{
				textclass:"onlyparatextstyle",
				textdata: data.string.p2para5
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highlightpurple",
				textclass:"headingstyle2",
				textdata: data.string.titleRedhighlight
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para7
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle4",
				textdata: data.string.p2para8
			}

		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",
		bloodflowanimationblock : [
			{
				rbc:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highlightpurple",
				textclass:"headingstyle2",
				textdata: data.string.titleRedhighlight
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para9
			},

		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",
		bloodflowanimationblock : [
			{
				rbc:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highlightpurple",
				textclass:"headingstyle2",
				textdata: data.string.titleWhitehighlight
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para10
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle2_1",
				textdata: data.string.p2para11
			}
		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",

		bloodflowanimationblock : [
			{
				wbc:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "highlightpurple",
				textclass:"headingstyle2",
				textdata: data.string.titleWhitehighlight
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para12
			}
		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",
		bloodflowanimationblock : [
			{
				wbc:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.titlePlat
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para13
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle2_1",
				textdata: data.string.p2para14
			},

		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",
		bloodflowanimationblock : [
			{
				platelets:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		contentblockadditionalclass: "beigebackground",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.titlePlat
			},
			{
            	datahighlightflag:true,
            	datahighlightcustomclass: "colorchangepink",
				textclass:"parastyle1 cssfadein",
				textdata: data.string.p2para15
			}

		],
		bloodflowanimationblockblockadditionalclass: "halfscreen",
		bloodflowanimationblock : [
			{
				platelets:[{},{},{}],
			},
		],
	},
	{
		contentblocknocenteradjust : true,
		bloodflowanimationblock : [
			{
				rbc:[{},{},{}],
				platelets:[{},{},{}],
				wbc:[{},{},{}],
			},
		],
	},
	{
		contentblockadditionalclass: "beigebackground",
		diysplashblock:[
			{
				diysplashImageSource:"images/diy/diy1.png",
				diysplashTextData: data.string.diytext
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		diyblock:[
			{
				diyImageSource:"images/diy/diy1.png",
				diyTextData: data.string.diytext,
				diyInstructionData: data.string.diyinstruct
			}
		],
		uppertextblock:[
			{
				textclass:"diyquestion",
				textdata: data.string.p2diyQ1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"middlediyimg",
						imgsrc: imgpath+"red-blood.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass:"diyanswer1",
				textdata:data.string.p2Q1ans1,
			},
			{
				textclass:"diyanswer2",
				textdata:data.string.p2Q1ans2,
			},
			{
				textclass:"diyanswer3",
				textdata:data.string.p2Q1ans3,
			},
		]
	},
	{
		contentblocknocenteradjust : true,
		diyblock:[
			{
				diyImageSource:"images/diy/diy1.png",
				diyTextData: data.string.diytext,
				diyInstructionData: data.string.diyinstruct
			}
		],
		uppertextblock:[
			{
				textclass:"diyquestion",
				textdata: data.string.p2diyQ2
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"middlediyimg",
						imgsrc: imgpath+"white-blood.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass:"diyanswer1",
				textdata:data.string.p2Q2ans1,
			},
			{
				textclass:"diyanswer2",
				textdata:data.string.p2Q2ans2,
			},
			{
				textclass:"diyanswer3",
				textdata:data.string.p2Q2ans3,
			},
		]
	},
	{
		contentblocknocenteradjust : true,
		diyblock:[
			{
				diyImageSource:"images/diy/diy1.png",
				diyTextData: data.string.diytext,
				diyInstructionData: data.string.diyinstruct
			}
		],
		uppertextblock:[
			{
				textclass:"diyquestion",
				textdata: data.string.p2diyQ3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"middlediyimg",
						imgsrc: imgpath+"platelets-group.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass:"diyanswer1",
				textdata:data.string.p2Q3ans1,
			},
			{
				textclass:"diyanswer2",
				textdata:data.string.p2Q3ans2,
			},
			{
				textclass:"diyanswer3",
				textdata:data.string.p2Q3ans3,
			},
		]
	}

];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/

   /*==========  register the handlebar partials first  ==========*/
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	Handlebars.registerPartial("bloodflowanimationcontent", $("#bloodflowanimationcontent-partial").html());
	Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html());
	Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());

	/*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
					$(".constituentspara").addClass("cssfadein");
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		//notify user call
        notifyuser($board);

		$(".bloodcartoon").on(animationend, function () {
			$nextBtn.delay(200).show(10);
		});

		$(".constituentspara").on(animationend, function() {
			$(".plasmaimg").addClass("cssfadein");
		});

		$(".plasmaimg").on(animationend, function () {
			$(".red").addClass("cssfadein");
			$(".white").addClass("cssfadein1");
		});
		$(".white").on(animationend, function (){
			$(".plat").addClass("cssfadein");
			$(".plas").addClass("cssfadein1");
			$nextBtn.delay(2000).show(0);
		});

		$(".lowertextstyle").on(animationend, function (){
			$nextBtn.show(0);
		});

		$(".onlyparatextstyle").on(animationend, function (){
			$nextBtn.show(0);
		});

		$(".parastyle1").on(animationend, function () {
			$(".parastyle2, .parastyle2_1").addClass('cssfadein');
		});

		$(".parastyle1").on(animationend, function () {
			$(".parastyle4").addClass('cssfadein');
			$nextBtn.delay(1000).show(0);
		});

		$(".parastyle2, .parastyle2_1").on(animationend, function () {
			$nextBtn.show(0);
		})

		if (countNext==6 || countNext==10 || countNext==11) {
			$nextBtn.delay(1000).show(0);
		};

		if (countNext== 12 ) {
			$(".diyanswer1").on("click", function() {
				$(this).addClass("correct");
				$(this).unbind("hover");
				$nextBtn.show(0);
			});
			$(".diyanswer2, .diyanswer3").on("click", function () {
				$(this).addClass("incorrect");
			});
		};
		if (countNext== 13) {
			$(".diyanswer3").on("click", function() {
				$(this).addClass("correct");
				$(this).unbind("hover");
				$nextBtn.show(0);
			});
			$(".diyanswer2, .diyanswer1").on("click", function () {
				$(this).addClass("incorrect");
			});
		};
			if (countNext== 14) {
			$(".diyanswer3").on("click", function() {
				$(this).addClass("correct");
				$(this).unbind("hover");
			});
			$(".diyanswer2, .diyanswer1").on("click", function () {
				$(this).addClass("incorrect");
			});
		};

	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

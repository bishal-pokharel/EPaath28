var SPACE = " ";
var rxnARROW = "&#x027F6;";

//array of definition squirrel image
	var definitionImgPath = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
var definitionImg = definitionImgPath[randomImageNumeral];

var imgpath = $ref+"/images/page8/";

var tempcontent=[
	{	
		diysplashblock : [
			{
				diysplashImageSource     : definitionImg,
				diysplashTextData        : data.string.diytext,
				diysplashInstructionData : data.string.p8text1,
			},
		]
	},
	{	
		contentblocknocenteradjust : true,
		diyblock: [
			{	
				diyImageSource     : definitionImg,
				diyTextData        : data.string.diytext,
				diyInstructionData : data.string.p8text2,
				chemicalrxn        : data.string.p8diychemicalequation1,
			},
		]
	},
	{	
		contentblocknocenteradjust : true,
		diyblock: [
			{	
				diyImageSource     : definitionImg,
				diyTextData        : data.string.diytext,
				diyInstructionData : data.string.p8text2,
				chemicalrxn        : data.string.p8diychemicalequation2,
			},
		]
	},
	{	
		contentblocknocenteradjust : true,
		diyblock: [
			{	
				diyImageSource     : definitionImg,
				diyTextData        : data.string.diytext,
				diyInstructionData : data.string.p8text2,
				chemicalrxn        : data.string.p8diychemicalequation3,
			},
		]
	},
	{	
		contentblocknocenteradjust : true,
		diyblock: [
			{	
				diyImageSource     : definitionImg,
				diyTextData        : data.string.diytext,
				diyInstructionData : data.string.p8text2,
				chemicalrxn        : data.string.p8diychemicalequation4,
			},
		]
	},
	{	
		contentblocknocenteradjust : true,
		diyblock: [
			{	
				diyImageSource     : definitionImg,
				diyTextData        : data.string.diytext,
				diyInstructionData : data.string.p8text2,
				chemicalrxn        : data.string.p8diychemicalequation5,
			},
		]
	},
];

/*function to shuffle array but the first cell content*/
Array.prototype.shufflearrayleavingfirstcell = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor((Math.random() * (i+1)) + 1);
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
}

var content = tempcontent.shufflearrayleavingfirstcell();

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
	/*helper function for removing duplicates from an array*/
	function removeduplicates(rawarray){
		var returnarray = [];
		$.each(rawarray, function(index, element) {
        	$.inArray(element, returnarray) == -1 ? returnarray.push(element) : null;
    	});

    	/*return the sorted non duplicate array*/
    	return returnarray.sort();
	};

   /*==========  register the handlebar partials first  ==========*/
   	
   	//global varibles for makechemicalequation helper
   	
   	var allatoms;

   	 Handlebars.registerHelper('makechemicalequation', function(chemicalequation) {
	  /*flush these variables for new chemical equation*/
	  var mainequation  = [];
   	  var righthandside = [];
   	  var lefthandside  = [];   	
   	  var returnhtml    = "";
   	  var arrowsymbol, currentatom, atom, atomsuffix, atomprefix;
   	  allatoms = [];

	  mainequation = chemicalequation.split(":");

	  /*capture the symbol to put above array and 
	  remove it from main equation, if any*/
	  mainequation.length == 3 ? 
	  	( arrowsymbol = mainequation[0], mainequation.shift() ) :
	  		 arrowsymbol = "";

	  /*get the righthandside atoms in an array*/
	  lefthandside = mainequation[0].split(",");

	  /*get the lefthandside atoms in an array*/
	  righthandside = mainequation[1].split(",");
	  
	  /*split the molecules into atoms in lefthandside*/
	  for(var i=0 ; i <= lefthandside.length-1 ; i++) { lefthandside[i] = lefthandside[i].split(".");}

	  /*split the molecules into atoms in righthandside*/
	  for(var i=0 ; i <= righthandside.length-1 ; i++) { righthandside[i] = righthandside[i].split(".");}

	  // function for chemical equation one side parsing and html generation
		function chemeqhtmlgenerate(equationarray,whichhandside){
	  		for(var i=0; i <= equationarray.length-1 ; i++){
		  		atomprefix = equationarray[i][0].match(/(^[0-9])+/g);

		  		atomprefix != null ? null : atomprefix = 1;
		  		
		  		returnhtml = returnhtml+"<big class='"+whichhandside+"molecule"+i+"'><span class='inputcontainer'><span class='increaseprefix'>&#x025B4;</span><span class='decreaseprefix'>&#x025BE;</span><input class='moleculeprefix"+whichhandside+i+"' type='text' data-min='1' data-max='6' data-correctnum='"+atomprefix+"' value='1' disabled></span>";

			  	for(var j=0; j <= equationarray[i].length-1 ; j++){
			  		currentatom = equationarray[i][j];
			  		atom = currentatom.match(/([A-Za-z])+/g);

			  		/*push atom into allatoms array for later use*/
			  		allatoms.push(atom[0]);

			  		atomsuffix = currentatom.match(/([0-9]$)+/g);
				  		if(atomsuffix != null){
				  			returnhtml = returnhtml+"<kbd class='atom' data-atomname='"+atom+"' data-correctatomprefix='"+atomprefix+"' data-atomprefix='1' data-atomsuffix='"+atomsuffix+"'>"+atom+"<sub>"+atomsuffix+"</sub></kbd>";
				  		}
				  		else{
				  			returnhtml = returnhtml+"<kbd class='atom' data-atomname='"+atom+"' data-correctatomprefix='"+atomprefix+"' data-atomprefix='1' data-atomsuffix='1'>"+atom+"</kbd>";
				  		}
			  	}
		  		equationarray.length > 1 && i < equationarray.length-1 ? returnhtml = returnhtml+"</big>"+SPACE+"+"+SPACE: returnhtml = returnhtml+"</big>"+SPACE;
		  }
		};

	  /*first group the lefthandside molecules as reactants*/
	  returnhtml = returnhtml+"<b class='reactants'>";

	  /*now start generating html for lefthandside*/
	  chemeqhtmlgenerate(lefthandside,"reactant");

	  /*add an arrow to chemical reaction*/
	  returnhtml = returnhtml+"</b><b class='rxnarrow' data-symbol='"+arrowsymbol+"'>"+rxnARROW+"</b>";

	  /* group righthandside molecules as products*/
	  returnhtml = returnhtml+"<b class='products'>";

	  /*furthermore, start generating html for righthandside*/
	  chemeqhtmlgenerate(righthandside,"product");
	  
	  /* close the products b tag*/
	  returnhtml = returnhtml+"</b>";

	  /*remove duplicates from allatom array this is used later in blancing equation*/
	  allatoms = removeduplicates(allatoms);

	  return new Handlebars.SafeString(returnhtml);
	 });

	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html()); 
	Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	Handlebars.registerPartial("diycontent", $("#diycontent-partial").html()); 

	/*===============================================
	=            data highlight function            =
	===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function        =
		===============================================*/
			/**		
				How to:
				- First set any html element with 
					"data-usernotification='notifyuser'" attribute,
				and "data-isclicked = ''".
				- Then call this function to give notification		
			 */
			
			/**
				What it does:
				- You send an element where the function has to see
				for data to notify user
				- this function searches for all text nodes whose
				data-usernotification attribute is set to notifyuser
				- applies event handler for each of the html element which 
				 removes the notification style.
			 */
			function notifyuser($notifyinside){
				//check if $notifyinside is provided
				typeof $notifyinside !== "object" ?
				alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
				null ;

				/*variable that will store the element(s) to remove notification from*/	
				var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
				 // if there are any notifications removal required add the event handler
				if($allnotifications.length > 0){
					$allnotifications.one('click', function() {
						/* Act on the event */
						$(this).attr('data-isclicked', 'clicked');
						$(this).removeAttr('data-usernotification');
					});
				}
			}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
		 /**	 
		 	How To:
		 	- Just call the navigation controller if it is to be called from except the
		 	  last page of lesson
		 	- If called from last page set the islastpageflag to true such that 
		 		footernotification is called for continue button to navigate to exercise
		  */	
		/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
		 function navigationcontroller(islastpageflag){
		 	// check if the parameter is defined and if a boolean,
		 	// update islastpageflag accordingly
		 	typeof islastpageflag === "undefined" ? 
		 	islastpageflag = false : 
		 		typeof islastpageflag != 'boolean'?
		 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
		 		null;

		 	if(countNext == 0 && $total_page!=1){
				$nextBtn.show(0);
				$prevBtn.css('display', 'none');
			}
			else if($total_page == 1){
				$prevBtn.css('display', 'none');
				$nextBtn.css('display', 'none');
				
				// if lastpageflag is true 
				islastpageflag ? 
					ole.footerNotificationHandler.lessonEndSetNotification() :
						ole.footerNotificationHandler.pageEndSetNotification() ;			
			}
			else if(countNext > 0 && countNext < $total_page-1){
				$nextBtn.show(0);
				$prevBtn.show(0);
			}
			else if(countNext == $total_page-1){
				$nextBtn.css('display', 'none');
				$prevBtn.show(0);

				// if lastpageflag is true 
				islastpageflag ? 
					ole.footerNotificationHandler.lessonEndSetNotification() :
						ole.footerNotificationHandler.pageEndSetNotification() ;
			}
		 }
	/*=====  End of user navigation controller function  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// for diy blocks only
		var $diyblock = $board.find('div.diyblock');

		if($diyblock.length > 0){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.hideNotification();

			var $diyworkarea = $board.find("div.diyworkarea");

			// jquery objects and variables for chemicalequation
			var $chemicalequation         = $board.find("div.chemicalequation").children('p');
			var $equationreactants        = $chemicalequation.find(".reactants");
			var $reactantmolecules        = $equationreactants.find("[class^='reactantmolecule']");
			var $equationproducts         = $chemicalequation.find(".products");
			var $productmolecules         = $equationproducts.find("[class^='productmolecule']");
			var $moleculeprefixes         = $chemicalequation.find("input[class^='moleculeprefix']");
			var $increasedecreaseprefixes = $chemicalequation.find("span[class$='creaseprefix']");
			var $increaseprefixes         = $chemicalequation.find("span.increaseprefix");
			var $decreaseprefixes         = $chemicalequation.find("span.decreaseprefix");

			// jquery objects and variables for atomcounts
			var $atomcounts         = $diyworkarea.find("div.atomcounts");
			var $reactantsatomcount = $atomcounts.find("div.reactantsatomcount");
			var $productsatomcount  = $atomcounts.find("div.productsatomcount");

			/*display all atoms in right and left handside of the equation*/
			var rightleftatomhtml = "";

			for(var x=0; x < allatoms.length ; x++){
				rightleftatomhtml = rightleftatomhtml+"<p class='"+allatoms[x]+"'><span>"+allatoms[x]+"</span>"+" = "+"<span class='atomcount'></span></p>"
			}

			$reactantsatomcount.html(rightleftatomhtml);
			$productsatomcount.html(rightleftatomhtml);

			var $reactantatoms = $reactantsatomcount.children('p');
			var $productatoms = $productsatomcount.children('p');

			var totalcount;

			/*function to update the atom counts*/
			function updateatomcount($atoms, $molecules){
				var findthis, $currentatom;

				$.each($atoms, function(index, elem) {
					findthis = $(this).attr('class');

					$currentatom = $molecules.find("[data-atomname="+findthis+"]");

					totalcount = 0;
					$.each($currentatom, function(i, e){
						totalcount = totalcount + ( $(this).attr('data-atomprefix') * $(this).attr('data-atomsuffix') );
					});

					$(this).find("span.atomcount").html(totalcount);
				});
			}
			
			/*update reactants and product atom count for first time*/
			updateatomcount($reactantatoms, $reactantmolecules);
			updateatomcount($productatoms , $productmolecules);

			/*function to check if the equation is balanced*/
			function isequationbalanced(){
				var $tocompare, isbalanced, balancecount = 0;
				
				/*check all classes from allatoms array for equal atom count*/
				$.each(allatoms, function(ind, el) {
					$tocompare = $atomcounts.find("."+el);
					
					// check left and right hand side for each atom class type
					if($tocompare.eq(0).text() !== $tocompare.eq(1).text() ) {
						isbalanced = false;
						return;
					}
					else if($tocompare.eq(0).text() === $tocompare.eq(1).text() ){
						balancecount++;
					};
				});

				balancecount == allatoms.length ? isbalanced = true : isbalanced = false;
				return isbalanced;
			}

			/*when the molecule prefix are changed*/
			function onmoleculeprefixchange(that){
				var $currentmoleculeatoms = that.parent("span.inputcontainer").siblings(".atom");
				var changedonwhichside = that.attr('class');
				/*update the current atom prefix for all related atom*/
				$currentmoleculeatoms.attr('data-atomprefix', that.val());

				/*now update respective reactant or product atom count*/
				changedonwhichside.search("reactant") != -1 ? updateatomcount($reactantatoms, $reactantmolecules) : updateatomcount($productatoms , $productmolecules);
				
				/*check if equation is balanced*/

				isequationbalanced() ?
				( $increasedecreaseprefixes.css({"opacity":"0","pointer-events":"none"}),
				  $moleculeprefixes.css('background-color', '#63D420'),
				  navigationcontroller(), 
				  $prevBtn.css('display', 'none') ) :
				( null );
			};

			var maxinputrange = 6, mininputrange = 1;
			/*increaseprefix event handler*/
			$increaseprefixes.on('click', function() {
				var $currentprefixinput = $(this).siblings('input[type="text"]');
				var currentinputvalue = parseInt($currentprefixinput.val());
				currentinputvalue < maxinputrange ? currentinputvalue += 1 : null;
				$currentprefixinput.val(currentinputvalue);
				onmoleculeprefixchange($currentprefixinput);
			});

			/*decreaseprefix event handler*/
			$decreaseprefixes.on('click', function() {
				var $currentprefixinput = $(this).siblings('input[type="text"]');
				var currentinputvalue = parseInt($currentprefixinput.val());
				currentinputvalue > mininputrange ? currentinputvalue -= 1 : null;
				$currentprefixinput.val(currentinputvalue);
				onmoleculeprefixchange($currentprefixinput);
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
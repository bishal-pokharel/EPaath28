//array of image
	var timeToThinkImage = [
	"images/timetothink/timetothink1.png",
	"images/timetothink/timetothink2.png",
	"images/timetothink/timetothink3.png",
	"images/timetothink/timetothink4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentTimetoThinkImage = timeToThinkImage[randomImageNumeral];

var imgpath = $ref+"/images/page4/";

var content=[
	{
		timeToThinkImageSource     : currentTimetoThinkImage,
		timeToThinkTextData        : data.string.timetothinktext,
		timeToThinkInstructionData : data.string.p4timetothinkquest1,
	},
	{	
		imageblock : [
			{
				imgclass : "twoimagestogether",
				imgsrc : imgpath+"mirror.png",
			},
			{
				imgclass : "twoimagestogether",
				imgsrc : imgpath+"wood.png",
			}
		],
		lowertextblock : [
			{
				textclass : "textexplain",
				textdata : data.string.p4text3,
			},
		],
	},
	{	
		imageblock : [
			{
				imgclass : "twoimagestogether hasmarginbottom",
				imgsrc : imgpath+"mirrorzoom.png",
			},
			{
				imgclass : "twoimagestogether hasmarginbottom",
				imgsrc : imgpath+"woodzoom.png",
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle textatbottom",
				textdata : data.string.p4text5,
			}
		],
	},
	{	
		imageblock : [
			{
				imgclass : "twoimagestogether hasmarginbottom",
				imgsrc : imgpath+"mirrorzoom.png",
			},
			{
				imgclass : "twoimagestogether hasmarginbottom",
				imgsrc : imgpath+"woodzoom.png",
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle textatbottom",
				textdata : data.string.p4text6,
			}
		],
	},
	{	
		uppertextblock : [
			{
				textclass : "pinkheading animatefadeup",
				textdata : data.string.p4text7,
			},
		],
		imageblock : [
			{
				imgclass : "twoimagestogether hasmargintop",
				imgsrc : imgpath+"mirrorzoom.png",
			},
			{
				imgclass : "twoimagestogether hasmargintop",
				imgsrc : imgpath+"woodzoom.png",
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle textatbottom",
				textdata : data.string.p4text6,
			}
		],
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull",
		imageblock : [
			{
				imgclass : "somemargintoptextstyle",
				imgsrc : imgpath+"specular.png",
			}
		],
		textcontainerclass : "textcontainerbottom",
		lowertextblock : [
			{
				textclass : "textbottomcontainertext",
				textdata : data.string.p4text8,
			},
		],
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull",
		imageblock : [
			{
				imgclass : "somemargintoptextstyle",
				imgsrc : imgpath+"specular.png",
			}
		],
		textcontainerclass : "textcontainerbottom",
		lowertextblock : [
			{
				highlighttext:true,
				textclass : "textbottomcontainertext",
				textdata : data.string.p4text10,
			}
		],
	},
	{	
		uppertextblock : [
			{
				textclass : "polybgtextstyle",
				textdata : data.string.p4text11,
			},
		]
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull",
		imageblock : [
			{
				imgclass : "somemargintoptextstyle",
				imgsrc : imgpath+"diffuse.png",
			}
		],
		textcontainerclass : "textcontainerbottom",
		lowertextblock : [
			{
				textclass : "textbottomcontainertext",
				textdata : data.string.p4text12,
			},
		],
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull",
		imageblock : [
			{
				imgclass : "somemargintoptextstyle",
				imgsrc : imgpath+"diffuse.png",
			}
		],
		textcontainerclass : "textcontainerbottom",
		lowertextblock : [
			{
				highlighttext : true,
				textclass     : "textbottomcontainertext",
				textdata      : data.string.p4text13,
			}
		],
	},
	{
		timeToThinkImageSource : currentTimetoThinkImage,
		timeToThinkTextData : data.string.timetothinktext,
		timeToThinkInstructionData : data.string.p4timetothinkquest2,
	},
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  time to think template caller  ==========*/
	
	function timeToThink () {
		var source = $("#timeToThink-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();
	}

	/*==========  reflection of light template caller  ==========*/

	function reflectionoflight(){
		var source = $("#reflectionoflight-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight the para with attribute data-highlight equals true */
		
		var $alltextpara = $board.find("p[data-highlight='true']");
		var replaceinstring;
		var texthighlightstarttag = "<span class='parsedstring'>";
		var texthighlightendtag = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				// console.log(val);
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	
	timeToThink();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		countNext == $total_page-1 ? timeToThink() : reflectionoflight();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		countNext == 0 ? timeToThink() : reflectionoflight();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
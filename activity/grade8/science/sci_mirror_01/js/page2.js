var imgpath = $ref+"/images/page2/";

$(function () {	
	var $light_page = $('.light_page');
	var $nextBtn    = $("#activity-page-next-btn-enabled");
	var $prevBtn    = $("#activity-page-prev-btn-enabled");
	var countNext   = 0;

	var $total_page = 3;
	loadTimelineProgress($total_page,countNext+1);

	var $textcontainer  = $light_page.children('div.textcontainer');
	var $textheading    = $textcontainer.children('p.textheading');
	var $textexplain    = $textcontainer.children('p.textexplain');
	var $imagecontainer = $light_page.children('div.imagecontainer');
	var $image          = $imagecontainer.children('img');
			
	// text heading is common for all
	

	function updateslide(){		
		switch(countNext){
			case 0: $textheading.html(data.string.textheading).show(0);
					$imagecontainer.addClass('imagecontainerlessgap');
					$textexplain.html(data.string["textexplain"+countNext]);
					$image.attr("src",imgpath+"subslide"+countNext+".png");
					break;
			case 1: $textheading.css('display', 'none');
					$imagecontainer.removeClass('imagecontainerlessgap');
					$textexplain.html(data.string["textexplain"+countNext]);
					$image.attr("src",imgpath+"subslide"+countNext+".png");
					break;
			case 2:	$textexplain.html(data.string["textexplain"+countNext]);
					$image.attr("src",imgpath+"subslide"+countNext+".png");
					break; 
			default : break;
		}

		if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	updateslide();

	$nextBtn.on('click',function () {
		countNext++;		
		updateslide();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;	
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;	
		updateslide();
		loadTimelineProgress($total_page,countNext+1);
	});
});
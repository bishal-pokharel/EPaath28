var imgpath = $ref+"/images/page8/";

var content=[
	{	
		typeoflayout: "layouthorizontal",
		uppertextblock : [
			{
				textclass : "polybgtextstyle",
				textdata : data.string.p8text1,
			},
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "onlytextstyle",
				textdata : data.string.p8text2,
			},
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				highlighttext : true,
				textclass : "basictextstyle",
				textdata : data.string.p8text3,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc : imgpath+"concaveconvex.png",
					}
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		imageblockcustomclass : "sphericalmirrorslabelled",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc : imgpath+"concaveconvexlabelled.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass  : "convexcaption",
						imagelabeldata : data.string.p7listsubtype1,
					},
					{
						imagelabelclass  : "concavecaption",
						imagelabeldata : data.string.p7listsubtype2,
					},
					{
						imagelabelclass  : "concavelabel",
						imagelabeldata : data.string.p8concavelabel,
					},
					{
						imagelabelclass  : "convexlabel",
						imagelabeldata : data.string.p8convexlabel,
					},
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "basictextstyle",
				textdata : data.string.p8text4,
			},
		],
		imageblockcustomclass : "spoonlabelled",
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc : imgpath+"spoon.png",
					}
				],
				imagelabels : [
					{
						imagelabelclass  : "concavecaption",
						imagelabeldata : data.string.p8concave,
					},
					{
						imagelabelclass  : "convexcaption",
						imagelabeldata : data.string.p8convex,
					},
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "basictextstyle",
				textdata : data.string.p8text5,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle concaveconvexspoonstyle",
						imgsrc : imgpath+"concaveconveximagespoon.png",
					}
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "onlytextstyle",
				textdata : data.string.p8text6,
			},
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  sphericalmirrors template caller  ==========*/

	function sphericalmirrors(){
		var source = $("#sphericalmirrors-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight the para with attribute data-highlight equals true */
		
		var $alltextpara = $board.children("div").children("div.textblock").children("p[data-highlight='true']");
		var replaceinstring;
		var texthighlightstarttag = "<span class='parsedstring'>";
		var texthighlightendtag = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				// console.log(val);
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	
	sphericalmirrors();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		sphericalmirrors();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		sphericalmirrors();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
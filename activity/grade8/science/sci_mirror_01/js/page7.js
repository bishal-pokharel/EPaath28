var imgpath = $ref+"/images/page7/";

var content=[
	{	
		typeoflayout: "layouthorizontal",
		textblockadditionalclass : "somemargintop",
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata : data.string.p7text2,
			},
		],
		textlistblock : [
			{
				listtype : [
					{
						listtypedata : data.string.p7listtype1,
					},
					{
						listtypedata : data.string.p7listtype2,
						listsubtype : [
							{listsubtypedata : data.string.p7listsubtype1},
							{listsubtypedata : data.string.p7listsubtype2}
						]
					},
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal",
		textblockadditionalclass : "somemargintop",
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata : data.string.p7text3,
			},
			{
				textclass : "simpletextexplainstyle",
				textdata : data.string.p7text4,
			},
		],
	},
	{	
		typeoflayout: "layouthorizontal",
		textblockadditionalclass : "somemargintop",
		uppertextblock : [
			{
				textclass : "simpletextexplainstyle",
				textdata : data.string.p7text5,
			},
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 1,
					mainpropertytext : data.string.p7property1,
					propertycontainerheadingdata : data.string.p7propertyheadingtext,
					propertyexplainimgsrc : imgpath+"01.png",
					propertyexplaindata : data.string.p7property1explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 2,
					mainpropertytext : data.string.p7property2,
					propertycontainerheadingdata : data.string.p7propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p7property1,
						}
					],
					propertyexplainimgsrc : imgpath+"02.png",
					propertyexplaindata : data.string.p7property2explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 3,
					mainpropertytext : data.string.p7property3,
					propertycontainerheadingdata : data.string.p7propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p7property1,
						},
						{
							propertylistcount : 2,
							propertylistdata : data.string.p7property2,
						}
					],
					propertyexplainimgsrc : imgpath+"03.png",
					propertyexplaindata : data.string.p7property3explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 4,
					mainpropertytext : data.string.p7property4,
					propertycontainerheadingdata : data.string.p7propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p7property1,
						},
						{
							propertylistcount : 2,
							propertylistdata : data.string.p7property2,
						},
						{
							propertylistcount : 3,
							propertylistdata : data.string.p7property3,
						}
					],
					propertyexplainimgsrc : imgpath+"04.png",
					propertyexplaindata : data.string.p7property4explain,
				}
		],
	},
	{	
		typeoflayout: "propertysummarylayout",
		propertysummaryblock : [
				{				
					propertysummaryheadingdata: data.string.p7propertyheadingtext,
					propertysummarylist : [
						{
							propertysummarylistcount : 1,
							propertysummarylistdata : data.string.p7property1,
						},
						{
							propertysummarylistcount : 2,
							propertysummarylistdata : data.string.p7property2,
						},
						{
							propertysummarylistcount : 3,
							propertysummarylistdata : data.string.p7property3,
						},
						{
							propertysummarylistcount : 4,
							propertysummarylistdata : data.string.p7property4,
						}
					],
				}
			],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  plane mirror properties template caller  ==========*/

	function planemirrorproperties(){
		var source = $("#planemirrorproperties-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight the para with attribute data-highlight equals true */
		
		var $alltextpara = $board.children("div").children("div.textblock").children("p[data-highlight='true']");
		var replaceinstring;
		var texthighlightstarttag = "<span class='parsedstring'>";
		var texthighlightendtag = "</span>";
		if($alltextpara.length > 0){
			$.each($alltextpara, function(index, val) {
				// console.log(val);
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	
	planemirrorproperties();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		planemirrorproperties();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		planemirrorproperties();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
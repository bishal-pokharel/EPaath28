var imgpath = $ref+"/images/page9/";

var content=[
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimage",
						imgsrc : imgpath+"convex.png",
					}
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimage",
						imgsrc : imgpath+"convex.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata : data.string.p9text2,
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimagemedium",
						imgsrc : imgpath+"convex2.svg",
					}
				],
			}
		],
		lowertextblock : [
			{
				highlighttext : true,
				textclass : "bottomparatextstyle",
				textdata : data.string.p9text3,
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimageless",
						imgsrc : imgpath+"convex3.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata : data.string.p9text4,
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal",
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata : data.string.p9text5,
			},
		],
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use1,
				explainusagetextcontent : [
					{
						texthighlightflag : true,
						explainusagetextdata : data.string.p9use1explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse1.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use2,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use2explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse2.jpg",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use3,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use3explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse3.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use4,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use4explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse4.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use5,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use5explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse5.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use6,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use6explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse6.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use7,
				explainusagetextcontent : [
					{
						texthighlightflag : true,
						explainusagetextdata : data.string.p9use7explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse7.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p9usageheading,
				usagetextdata : data.string.p9use8,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p9use8explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"convexuse8.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagesummarylayout",
		usagesummaryblock : [
				{				
					usagesummaryheadingdata: data.string.p9usageheading,
					usagesummarylist : [
						{
							usagesummarylistcount : 1,
							usagesummarylistdata : data.string.p9use_1,
						},
						{
							usagesummarylistcount : 2,
							usagesummarylistdata : data.string.p9use_2,
						},
						{
							usagesummarylistcount : 3,
							usagesummarylistdata : data.string.p9use_3,
						},
						{
							usagesummarylistcount : 4,
							usagesummarylistdata : data.string.p9use_4,
						},
						{
							usagesummarylistcount : 5,
							usagesummarylistdata : data.string.p9use_5,
						},
						{
							usagesummarylistcount : 6,
							usagesummarylistdata : data.string.p9use_6,
						},
						{
							usagesummarylistcount : 7,
							usagesummarylistdata : data.string.p9use_7,
						},
						{
							usagesummarylistcount : 8,
							usagesummarylistdata : data.string.p9use_8,
						},
					],
				}
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("usagesummarycontent", $("#usagesummarycontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  convexmirror template caller  ==========*/

	function convexmirror(){
		var source = $("#convexmirror-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight all element with text and attribute data-highlight
			set to true cheers! this is updated version - finds all elements 
			inside board div which have data-highlight element set to 
			true , more robust then previous ones */
		
		var $tohighlightelements = $board.find("*[data-highlight='true']");
		// alert($tohighlightelements.length);
		var replaceinstring;
		var texthighlightstarttag = "<span class='parsedstring'>";
		var texthighlightendtag = "</span>";
		if($tohighlightelements.length > 0){
			$.each($tohighlightelements, function(index, val) {
				// console.log(val);
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	
	convexmirror();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		convexmirror();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		convexmirror();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
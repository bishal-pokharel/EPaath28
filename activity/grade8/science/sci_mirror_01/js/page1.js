var transitionEnd =
  "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationEnd =
  "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref + "/images/page1/";

var content = [
  {
    textcontainerclass: "vertical-horizontal-center",
    textonly: [
      {
        textclass: "heading1 marginedbottomhigh reflectedtext",
        textdata: data.string.mirror
      }
    ]
  },
  {
    textcontainerclass: "textcontainertop",
    text: [
      {
        textclass: "slide2textheading",
        textdata: data.string.p1q1
      },
      {
        textclass: "slide2text",
        textdata: data.string.p1q2
      }
    ],
    imagecontainerclass: "imagecontainerbottom",
    image: [
      {
        imageclass: "slide2image imageleft bounceball",
        imagesrc: imgpath + "tennisball.png"
      },
      {
        imageclass: "slide2image imageright",
        imagesrc: imgpath + "flashlight.png"
      }
    ]
  },
  {
    imagecontainerclass: "imagecontainertop",
    imagebottom: [
      {
        imageclass: "slide3image lightexplained",
        imagesrc: imgpath + "lawofreflection.png"
      }
    ],
    labelincidentdata: data.string.incidentangle,
    labelreflectiondata: data.string.reflectionangle,
    labelincidentraydata: data.string.incidentray,
    labelreflectedraydata: data.string.reflectedray,
    labelmirrordata: data.string.mirror,
    textcontainerclass: "textcontainerbottom",
    text: [
      {
        textclass: "slide3text",
        textdata: data.string.p1q3part1
      },
      {
        textclass: "slide3text",
        textdata: data.string.p1q3part2
      }
    ]
  }
];

$(function() {
  var $board = $(".board");
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  /*
   * first
   */
  function first() {
    var source = $("#first-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.css("display", "none");
    $prevBtn.css("display", "none");

    var $textcontainer = $board.children("div.textcontainer");
    var $imagecontainerbottom = $board.children("div.imagecontainerbottom");
    var $imagecontainertop = $board.children("div.imagecontainertop");

    // if there is image container bottom div
    if ($imagecontainerbottom.length) {
      // alert("imagecontainerbottom");
      var $leftimage = $imagecontainerbottom.children("img.imageleft");
      var $lightrayincident = $imagecontainerbottom.children(
        "div.lightrayincident"
      );
      var $lightrayreflected = $imagecontainerbottom.children(
        "div.lightrayreflected"
      );
      var ballanimationcount = 0;

      setTimeout(function() {
        $lightrayincident.css({ height: "75%" });
      }, 0);

      $lightrayincident.on(transitionEnd, function() {
        $lightrayreflected.css({ height: "100%", opacity: "1" });
      });

      $lightrayreflected.on(transitionEnd, function() {
        $lightrayreflected.css({ height: "0%", opacity: "0" });
        $lightrayincident.css({ height: "0%", opacity: "0" });
        setTimeout(function() {
          $lightrayincident.css({ height: "75%", opacity: "1" });
        }, 400);
      });
    }

    // if there is image container top div
    // else if($imagecontainertop.length){
    // 	// alert($imagecontainertop.attr("class"));
    // 	// highlight the keywords in the text
    // 	var replaceintostring ;
    // 	var replacestring = data.string.p1q3highlight;
    // 	var replacestringarray = replacestring.split("#");
    // 	var regexp;
    // 	$.each(replacestringarray, function(index, val) {
    // 		// console.log(val);
    // 		replaceintostring = $textcontainer.html();
    // 		regexp = new RegExp(val, 'g');
    // 		$textcontainer.html(replaceintostring.replace(regexp,"<span class='parsedstring'>"+val+"</span>"));
    // 	});
    // }

    if (countNext == 0) {
      $nextBtn.show(0);
      $prevBtn.css("display", "none");
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css("display", "none");
      $prevBtn.show(0);
      ole.footerNotificationHandler.pageEndSetNotification();
    }
  }

  first();

  $nextBtn.on("click", function() {
    countNext++;
    first();
    loadTimelineProgress($total_page, countNext + 1);
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on("click", function() {
    countNext--;
    countNext < $total_page
      ? ole.footerNotificationHandler.hideNotification()
      : null;
    first();
    loadTimelineProgress($total_page, countNext + 1);
  });
});

var imgpath = $ref+"/images/page10/";

var content=[
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype2,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimage",
						imgsrc : imgpath+"concave.png",
					}
				],
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype2,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimage",
						imgsrc : imgpath+"concave.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle",
				textdata : data.string.p10text2,
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal coverboardfull",
		uppertextblock : [
			{
				textclass : "pinkheadingstyle",
				textdata : data.string.p7listsubtype2,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeimagemedium",
						imgsrc : imgpath+"concave2.svg",
					}
				],
			}
		],
		lowertextblock : [
			{
				highlighttext : true,
				textclass : "bottomparatextstyle",
				textdata : data.string.p10text3,
			}
		],
	},
	{	
		typeoflayout: "layouthorizontal",
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata : data.string.p10text4,
			}
		],
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use1,
				explainusagetextcontent : [
					{
						texthighlightflag : true,
						explainusagetextdata : data.string.p10use1explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse1.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use2,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p10use2explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse2.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use3,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p10use3explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse3.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use4,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p10use4explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse4.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use5,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p10use5explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse5.png",
					}
				],
			},
		]
	},
	{	
		typeoflayout: "usagelayout",
		usageblock : [
			{
				usageheadingdata : data.string.p10usageheading,
				usagetextdata : data.string.p10use6,
				explainusagetextcontent : [
					{
						explainusagetextdata : data.string.p10use6explain,
					}
				],
				explainusageimagecontent : [
					{
						explainusageimgsrc : imgpath+"concaveuse6.png",
					}
				],
			},
		]
	},
	
	{	
		typeoflayout: "usagesummarylayout",
		usagesummaryblock : [
				{				
					usagesummaryheadingdata: data.string.p10usageheading,
					usagesummarylist : [
						{
							usagesummarylistcount : 1,
							usagesummarylistdata : data.string.p10use_1,
						},
						{
							usagesummarylistcount : 2,
							usagesummarylistdata : data.string.p10use_2,
						},
						{
							usagesummarylistcount : 3,
							usagesummarylistdata : data.string.p10use_3,
						},
						{
							usagesummarylistcount : 4,
							usagesummarylistdata : data.string.p10use_4,
						},
						{
							usagesummarylistcount : 5,
							usagesummarylistdata : data.string.p10use_5,
						},
						{
							usagesummarylistcount : 6,
							usagesummarylistdata : data.string.p10use_6,
						},
		
					],
				}
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("usagesummarycontent", $("#usagesummarycontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  concavemirror template caller  ==========*/

	function concavemirror(){
		var source = $("#concavemirror-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight all element with text and attribute data-highlight
			set to true cheers! this is updated version - finds all elements 
			inside board div which have data-highlight element set to 
			true , more robust then previous ones */
		
		var $tohighlightelements = $board.find("*[data-highlight='true']");
		// alert($tohighlightelements.length);
		var replaceinstring;
		var texthighlightstarttag = "<span class='parsedstring'>";
		var texthighlightendtag = "</span>";
		if($tohighlightelements.length > 0){
			$.each($tohighlightelements, function(index, val) {
				// console.log(val);
				replaceinstring = $(this).html();
				replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
				replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
				$(this).html(replaceinstring);
			});
		}
	}
	
	concavemirror();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		concavemirror();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		concavemirror();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
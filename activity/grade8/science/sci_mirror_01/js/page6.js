var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page6/";

var content=[
	{	
		typeoflayout: "layouthorizontal",
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata : data.string.p6text1,
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text2,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror1.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text3,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror2.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text4,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror3.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text5,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror4.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text6,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror5.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text7,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror6.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text8,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror7.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text9,
			}
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror8.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text10,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror9.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text11,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror10.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text12,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror11.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text13,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror12.png",
			}
		],
	},
	{	
		typeoflayout: "layoutvertical",
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata : data.string.p3heading1,
			},
		],
		uppertextblock : [
			{
				textclass : "sideboxtextstyle",
				textdata : data.string.p6text14,
			},
		],
		imageblock : [
			{
				imgclass : "simpleimgstyle",
				imgsrc : imgpath+"imageformonmirror13.png",
			}
		],
	},
	{
		typeoflayout   : "svglayout",
		svgblock : [
			{
				svgheading : [
					{
						datahighlightflag : true,
						textclass         : "svgheadingstyle",
						textdata          : data.string.p6imageformationheading,
					}
				],
			},
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

    /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/
		
	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	/*==========  reflection of light template caller  ==========*/

	function howimageformonmirror(){
		var source = $("#howimageformonmirror-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$prevBtn.css('display', 'none');
		countNext < $total_page-1 ? navigationcontroller() : null;
		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);

		var $svgwrapper = $board.children('div.svglayout').children('div.svgwrapper');
				
		// if there is a svg content
		if($svgwrapper.length > 0){
			var $svgcontainer = $svgwrapper.children('div.svgcontainer');
			var s             = Snap(".svgcontainer");
			Snap.load(imgpath+"imageformationanimation.svg", function (f) {    
    		s.append(f);
    			// snap objects
    		var lightbeam         = s.select("#lightbeam");
    		var raygroupbutterfly = s.select("#raygroupbutterfly");    		
    		var blue1ray          = s.select("#blue1ray");    		
    		var blue2ray          = s.select("#blue2ray");    		
    		var arrowgroup        = s.select("#arrowgroup");
    		var dottedraygroup    = s.select("#dottedraygroup");
    		var imagebutterfly    = s.select("#imagebutterfly");
    			
    			// jquery objects and js variables
    		var $svg               = $svgcontainer.children('svg');
    		var $lightbeam         = $svg.find("#lightbeam");
    		var $raygroupbutterfly = $svg.find("#raygroupbutterfly");
    		var $blue1ray          = $svg.find("#blue1ray");
    		var $arrowgroup        = $svg.find("#arrowgroup");
    		var $dottedraygroup    = $svg.find("#dottedraygroup");
    		var $imagebutterfly    = $svg.find("#imagebutterfly");

    		// start the animation from now
    		lightbeam.addClass('beamanimation');
    		
    		$lightbeam.on(animationend, function() {
    			  raygroupbutterfly.addClass('raygroupbutterflyanimation'); 
    			  blue1ray.addClass('lineanimation'); 			  			
    			  blue2ray.addClass('lineanimation');			  			
    		});

    		$blue1ray.on(animationend, function() {
    			  arrowgroup.addClass('arrowanimation');		  			
    		});

    		$arrowgroup.on(animationend, function() {
    			  dottedraygroup.addClass('dottedlineanimation');		  			
    		});

    		$dottedraygroup.on(animationend, function() {
    			  imagebutterfly.addClass('raydigramimageanimation');		  			
    		});

    		$imagebutterfly.on(animationend, function() {
    			  // call navigationcontroller
    			  navigationcontroller();		  			
    		});
 		});
		}
	}
	
	howimageformonmirror();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		howimageformonmirror();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		howimageformonmirror();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
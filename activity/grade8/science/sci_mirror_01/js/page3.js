var animationend  = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var imgpath = $ref+"/images/page3/";

var content=[
	{	
		verticalhorizontaladditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata : data.string.p3text1,
			},
		],
	},
	{	
		uppertextblock : [
			{
				textclass : "slide1pinkheading",
				textdata : data.string.mirror,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle mirrorimg1",
						imgsrc : imgpath+"lookinmirror.png",
					}
				],
			}
		],
	},
	{	
		uppertextblock : [
			{
				textclass : "slide1pinkheading",
				textdata : data.string.mirror,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle mirrorimg1",
						imgsrc : imgpath+"lookinmirror.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "bottomparatextstyle overlapimage",
				textdata : data.string.p3text3,
			}
		],
	},
	{	
		verticalhorizontaladditionalclass : "imageslideshowcoverboardfull",		
		slideshowblock : [
			{
				slides : [ 					
							imgpath+"slideshow1.jpg",
							imgpath+"slideshow2.jpg",
							imgpath+"slideshow3.jpg",
							imgpath+"slideshow4.jpg",
							imgpath+"slideshow5.jpg",
						]
			}
		],
		lowertextblock : [
			{
				textclass : "bigheadingtextstyle",
				textdata : data.string.p3text4,
			}
		],
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull withimagereflectionbg",
		uppertextblock : [
			{
				textclass : "textonbgstyle",
				textdata : data.string.p3text5,
			}
		]
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull withimagereflectionbg",
		uppertextblock : [
			{
				textclass : "textonbgstyle",
				textdata : data.string.p3text6,
			}
		]
	},
	{	
		verticalhorizontaladditionalclass : "coverboardfull withimagereflectionbg",
		uppertextblock : [
			{
				textclass : "textonbgstyle",
				textdata : data.string.p3text7,
			}
		]
	}
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	// register the handlebar partials first
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("slideshowcontent", $("#slideshowcontent-partial").html());

	 // navigation controller function
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*
* mirrorintro
*/
	function mirrorintro() {
		var source = $("#mirrorintro-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.css('display', 'none');
		$prevBtn.css('display', 'none');
		
		$slideshowblock = $board.find('div.slideshowblock');

		if($slideshowblock.length > 0){
			$lastanimatedslide = $slideshowblock.find("img.slideshow4");

			$lastanimatedslide.one("animationend",function(){
				// call navigation controller
				navigationcontroller();
			});			
		}
		else{
			// call navigation controller
			navigationcontroller();
		}
	}

	mirrorintro();

	$nextBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext++;		
		mirrorintro();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext--;	
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;	
		mirrorintro();
		loadTimelineProgress($total_page,countNext+1);
	});
});
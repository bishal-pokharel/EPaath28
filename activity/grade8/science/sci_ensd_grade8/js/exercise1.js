// prototype for array shuffle
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
	    while(--i > 0){
	        j = Math.floor(Math.random() * (i+1));
	        temp = this[j];
	        this[j] = this[i];
	        this[i] = temp;
	    }
	    return this;
};

var imgpath = $ref+"/images/exercise1/";
var correctimg = "images/correct.png";
var incorrectimg = "images/wrong.png";
var congratulationimgarray = [
	"images/quizcongratulation/gradea.png",
	"images/quizcongratulation/gradeb.png",
	"images/quizcongratulation/gradec.png",
];
var tempcontent=[
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q1a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q1b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e1ans2, },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge2", optionstext : data.string.e1ans3, },
			{ pidname: "imgoptionplarge3", optionstext : data.string.e1ans1, isdatacorrect : true,},
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q2a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q2b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e2ans2, },
			{ pidname: "imgoptionplarge2", optionstext : data.string.e2ans1, isdatacorrect : true,},
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e2ans3, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q3a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q3b.jpg"},
		],
		optionsdata : [
			{ pidname: "imgoptionplarge1", optionstext : data.string.e3ans3, isdatacorrect : true },
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge2", optionstext : data.string.e3ans1, },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e3ans2, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q4a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q4b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e4ans2, },
			{ pidname: "imgoptionplarge2", optionstext : data.string.e4ans1, isdatacorrect : true},
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e4ans3, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q5a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q5b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e5ans3,},
			{ pidname: "imgoptionplarge2", optionstext : data.string.e5ans2,  isdatacorrect : true },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e5ans3, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q6a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q6b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e6ans1,},
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge2", optionstext : data.string.e6ans2, },
			{ pidname: "imgoptionplarge3", optionstext : data.string.e6ans3,  isdatacorrect : true},
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q7a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q7b.jpg"},
		],
		optionsdata : [
			{ pidname: "imgoptionplarge1", optionstext : data.string.e7ans3,isdatacorrect : true },
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge2", optionstext : data.string.e7ans1, },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e7ans2, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q8a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q8b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge1", optionstext : data.string.e8ans2, },
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge2", optionstext : data.string.e8ans3, },
			{ pidname: "imgoptionplarge3", optionstext : data.string.e8ans1, isdatacorrect : true},
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q9a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q9b.jpg"},
		],
		optionsdata : [
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge1", optionstext : data.string.e9ans1,},
			{ pidname: "imgoptionplarge2", optionstext : data.string.e9ans3, isdatacorrect : true },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e9ans2, },
		],
	},
	{
		hasquiztemplate     : true,
		questioncount       : "it will be updated before template call",		
		numberofimgoptions     : "it will be updated before template call",
		hasdescriptionclass : "it will be updated before template call",
		questiontextdata    : data.string.e1maininstruct,
		middleimg:[
			{questionimgclass: "questionimageStill",questionimgsrc  : imgpath+"q10a.jpg"},
			{questionimgclass: "questionimagecorrectStill",questionimgsrc  : imgpath+"q10b.jpg"},
		],
		optionsdata : [
			{ pidname: "imgoptionplarge1", optionstext : data.string.e10ans1,isdatacorrect : true },
			{ datafornotcorrect: "incorrect1", pidname: "imgoptionplarge2", optionstext : data.string.e10ans3, },
			{ datafornotcorrect: "incorrect2", pidname: "imgoptionplarge3", optionstext : data.string.e10ans2, },
		],
	}
];

// now randomize the question content
var content = tempcontent.shufflearray();

var congratulationcontent = [
	{
		congratulationtextdata : data.string.e2congratulationtext,
		congratulationimgsrc : congratulationimgarray[1],
		congratulationcompletedtextdata : data.string.e2congratulationcompletedtext,
		congratulationyourscoretextdata : data.string.e2congratulationyourscoretext,
		congratulationreviewtextdata : data.string.e2congratulationreviewtext,
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = content.length+congratulationcontent.length + 2;
	loadTimelineProgress($total_page,countNext+1);

	// assign variable with quizboard container and scoreboard elements
	var $quizboard = $board.children('div.quizboard'); 
	var $scoreboard = $board.children('div.scoreboard'); 
	var $scoretext = $scoreboard.children('.scoretext');
	var $scorecount = $scoreboard.children('.scorecount');
	var $userscore = $scorecount.children('.userscore');
	var $totalproblemstext = $scoreboard.children('.totalproblemstext');
	// all elements which contains data about total questions
	var $totalquestiondata = $scoreboard.find('.totalquestiondata');
	var $scoregraph = $scoreboard.children('.scoregraph');
	var $scoregraphchildren;
	var totalquestioncount = 0; /*initiate total question count as 0*/
	var userscorecount = 0;
	var userscorestorage;
	var scoregraphstorehtml;
	var $congratulationscoregraphchildren;

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
		if(countNext >= 0 && countNext < $total_page-1){
			$nextBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	 /*==========  scoreboard update caller  ==========*/
	 function updatescoreboard(){	 	
	 	// for only first call updates
	 	if(countNext == 0){
	 		// update the total question count
	 		$.each(content, function(index, val) {
	 			if(content[index].hasquiztemplate){
	 				totalquestioncount++;
	 			}
	 		});

	 		$scoretext.html(data.string.e1scoretextdata);
	 		$totalquestiondata.html(totalquestioncount);
	 		$totalproblemstext.html(data.string.e2totalproblemtextdata);

	 		// now populate scoregraph block with batch of tags
	 		var blocktag = "<span data-correct=''></span>";

	 		for(var i=1 ; i <= totalquestioncount ; i++){
	 			scoregraph = $scoregraph.html();
	 			$scoregraph.html(scoregraph+blocktag);
	 		}

	 		$scoregraphchildren = $scoregraph.children('span')
	 	}
	 }

	/*==========  quiz template caller  ==========*/

	function quiz(){
		var source = $("#quiz-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the quiz template
		content[countNext].questioncount = countNext+1;
		// update options count in content before updating the quiz template
		content[countNext].numberofoptions = content[countNext].optionsdata.length;
		// content[countNext].numberofimgoptions = content[countNext].imgoptiondata.length;
		// update if quiz board has description content before updating the quiz template	
		content[countNext].hasdescriptionclass = typeof content[countNext].descriptioncontent !== "undefined" ? "hasdescriptionclass" : null;

		var html = template(content[countNext]);
		$quizboard.html(html);
		var $options = $quizboard.children('.options').children('p');
		var $optionsimg = $quizboard.children('.options').children('img');
		var clickcount = 0;

		// on options click do following
		$options.on('click', function() {
			// if incorrect is choosen
			$(this).attr("data-isclicked","clicked");

			// on first click only
			if(++clickcount == 1){
				$scoregraphchildren.eq(countNext).attr({
					'data-correct' : $(this).attr("data-correct")
				});
			}
			
			if($(this).attr("data-correct") == "correct"){
				// update isclicked data attribute to clicked
				clickcount == 1 ? $userscore.html(++userscorecount) : null;
				$options.css('pointer-events', 'none');
				$(".questionimageStill").addClass("cssfadeout");
				$(".questionimagecorrectStill").addClass("cssfadein");
				/*store the scoregraph and userscore as it is needed on congratulations templates
				when all the questions are attempted*/
				if(countNext+1 == totalquestioncount){
					userscorestorage = $userscore.html();
					scoregraphstorehtml = $scoregraph.html();					
				}
				navigationcontroller();
			}

			if($(this).attr("data-correct") == "incorrect1"){
				$(".animimg2").addClass("cssfadeout");
				$(".animimg2_1").addClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrect2"){
				$(".animimg3").addClass("cssfadeout");
				$(".animimg3_1").addClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrectsingleimg1"){
				$(".questionimage").addClass("cssfadeout");
				$(".incorrectimgsingle1").addClass("cssfadein");
				$(".incorrectimgsingle2").removeClass("cssfadein");
			}

			if($(this).attr("data-correct") == "incorrectsingleimg2"){
				$(".questionimage").addClass("cssfadeout");
				$(".incorrectimgsingle2").addClass("cssfadein");
				$(".incorrectimgsingle1").removeClass("cssfadein");

			}
		});
	}

	/*==========  congratulations template caller  ==========*/
	
	function congratulation(){
		var source = $("#congratulation-template").html();
		var template = Handlebars.compile(source);
		// update question count in content before updating the congratulation template
		// content[countNext].questioncount = countNext+1;
		
		var html = template(congratulationcontent[0]);
		$board.html(html);

		var $congratulationcontainer = $board.children('.congratulationcontainer');
		var $congratulationreviewtext = $congratulationcontainer.children('.congratulationreviewtext');
		var $congratulationscoregraph = $congratulationcontainer.children('.congratulationscoregraph');
		var $congratulationyourscoretext = $congratulationcontainer.children('.congratulationyourscoretext');
		$congratulationscoregraph.html(scoregraphstorehtml);

		// update the congratulationyourscoretext sentence
		var rawstatement = $congratulationyourscoretext.html();
		rawstatement = rawstatement.replace("#userscore#",userscorestorage);
		rawstatement = rawstatement.replace("#totalscore#",totalquestioncount);
		$congratulationyourscoretext.html(rawstatement);

		$congratulationscoregraphchildren = $congratulationscoregraph.children('span');
		$nextBtn.show(0);
	}

	quiz();
	updatescoreboard();
	// congratulation();
	// summary();
	
	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		// alert(countNext +" and "+content.length);
		if(countNext < content.length){
			quiz();
		}	
		else if(countNext == content.length){
			congratulation();
		}
		else if(countNext == content.length+1){
			ole.activityComplete.finishingcall();
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
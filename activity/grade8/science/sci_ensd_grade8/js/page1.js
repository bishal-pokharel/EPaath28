//array of diy squirrel image
	var diyImgPath = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);
var diyImg = diyImgPath[randomImageNumeral];
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page1/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
        contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "titletext",
				textdata  : data.string.biodiversity,
			}
		],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "leaf1",
                        imgsrc: imgpath+"biodiversity_leaf_11.png"
                    },
                    {
                        imgclass: "leaf2",
                        imgsrc: imgpath+"biodiversity_leaf_9.png"
                    },
                    {
                        imgclass: "leaf3",
                        imgsrc: imgpath+"biodiversity_leaf_8.png"
                    },
                    {
                        imgclass: "leaf3_1",
                        imgsrc: imgpath+"biodiversity_leaf_10.png"
                    },
                    {
                        imgclass: "leaf4",
                        imgsrc: imgpath+"biodiversity_leaf_5.png"
                    },
                    {
                        imgclass: "leaf4_1",
                        imgsrc: imgpath+"biodiversity_leaf_3.png"
                    },
                    {
                        imgclass: "leaf5",
                        imgsrc: imgpath+"biodiversity_leaf.png"
                    },
                    {
                        imgclass: "leaf6",
                        imgsrc: imgpath+"biodiversity_leaf_2.png"
                    },
                    {
                        imgclass: "leaf7",
                        imgsrc: imgpath+"biodiversity_leaf_4.png"
                    },
                    {
                        imgclass: "leaf8",
                        imgsrc: imgpath+"biodiversity_leaf_6.png"
                    },
                    {
                        imgclass: "leaf9",
                        imgsrc: imgpath+"biodiversity_leaf_7.png"
                    },
                    {
                        imgclass: "dragonfly animatebugmoveagain",
                        imgsrc: imgpath+"dragonfly02.gif"
                    }
                ]
            }
        ]
	},
    {
        contentblocknocenteradjust : true,
        uppertextblock : [
            {
                datahighlightflag : true,
                textclass : "titletext animatedivmovetop",
                textdata  : data.string.biodiversity,
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass: "biggreenfont",
                textclass: "fadeintext1",
                textdata: data.string.p1text1
            },
            {
                datahighlightflag : true,
                datahighlightcustomclass: "biggreenfont",
                textclass: "fadeintext2",
                textdata: data.string.p1text2
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass: "leaf1 animateleaftop",
                        imgsrc: imgpath+"biodiversity_leaf_11.png"
                    },
                    {
                        imgclass: "leaf2 animateleaftop",
                        imgsrc: imgpath+"biodiversity_leaf_9.png"
                    },
                    {
                        imgclass: "leaf3 animateleaftop",
                        imgsrc: imgpath+"biodiversity_leaf_8.png"
                    },
                    {
                        imgclass: "leaf3_1 animateleaftop",
                        imgsrc: imgpath+"biodiversity_leaf_10.png"
                    },
                    {
                        imgclass: "leaf4 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_5.png"
                    },
                    {
                        imgclass: "leaf4_1 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_3.png"
                    },
                    {
                        imgclass: "leaf5 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf.png"
                    },
                    {
                        imgclass: "leaf6 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_2.png"
                    },
                    {
                        imgclass: "leaf7 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_4.png"
                    },
                    {
                        imgclass: "leaf8 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_6.png"
                    },
                    {
                        imgclass: "leaf9 animateleafbottom",
                        imgsrc: imgpath+"biodiversity_leaf_7.png"
                    }
                ]
            }
        ]
    },
	{
        contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "fullbackground1",
						imgsrc   : imgpath+"index.jpg"
					},
                    {
                        imgclass : "fullbackground2 cssfadein3",
                        imgsrc   : imgpath+"takecareofbaby.jpg"
                    },
                    {
                        imgclass : "fullbackground3",
                        imgsrc   : imgpath+"underwater.jpg"
                    },
                    {
                        imgclass : "fullbackground4",
                        imgsrc   : imgpath+"giraffe.jpeg"
                    },
                    {
                        imgclass : "fullbackground5",
                        imgsrc   : imgpath+"happy-Antarctic.jpg"
                    },
                    {
                        imgclass : "fullbackground6",
                        imgsrc   : imgpath+"himalayas.jpg"
                    },
                    {
                        imgclass : "fullbackground7",
                        imgsrc   : imgpath+"madagascar.jpg"
                    },
                    {
                        imgclass : "fullbackground8",
                        imgsrc   : imgpath+"koala.jpg"
                    },


				]
			}
		],
        lowertextblock:[
            {
                textclass:"middletextbackground",
                textdata: data.string.spacecharacter
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass: "bigfont2",
                textclass:"middletext",
                textdata: data.string.p1text3
            }
        ]
	},
	{
        contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "worldmap",
						imgsrc: imgpath+"new/bg.png"
					},
                    {
                        imgclass: "img1",
                        imgsrc: imgpath+"new/01.png"
                    },
                    {
                        imgclass: "img2",
                        imgsrc: imgpath+"new/02.png"
                    },
                    {
                        imgclass: "img3",
                        imgsrc: imgpath+"new/03.png"
                    },
                    {
                        imgclass: "img4",
                        imgsrc: imgpath+"new/04.png"
                    },
                    {
                        imgclass: "img5",
                        imgsrc: imgpath+"new/05.png"
                    },
                    {
                        imgclass: "img6",
                        imgsrc: imgpath+"new/06.png"
                    },
                    {
                        imgclass: "img7",
                        imgsrc: imgpath+"new/07.png"
                    },
                    {
                        imgclass: "img8",
                        imgsrc: imgpath+"new/08.png"
                    },
                    {
                        imgclass: "img9",
                        imgsrc: imgpath+"new/09.png"
                    },
                    {
                        imgclass: "img10",
                        imgsrc: imgpath+"new/10.png"
                    },
                    {
                        imgclass: "img11",
                        imgsrc: imgpath+"new/11.png"
                    },
                    {
                        imgclass: "img12",
                        imgsrc: imgpath+"new/12.png"
                    },
                    {
                        imgclass: "img13",
                        imgsrc: imgpath+"new/13.png"
                    },
                    {
                        imgclass: "img15",
                        imgsrc: imgpath+"new/15.png"
                    },
                    {
                        imgclass: "img16",
                        imgsrc: imgpath+"new/16.png"
                    },
                    {
                        imgclass: "img17",
                        imgsrc: imgpath+"new/17.png"
                    },
                    {
                        imgclass: "img18",
                        imgsrc: imgpath+"new/18.png"
                    },
                    {
                        imgclass: "img19",
                        imgsrc: imgpath+"new/19.png"
                    },
                    {
                        imgclass: "img20",
                        imgsrc: imgpath+"new/20.png"
                    },
                    {
                        imgclass: "img21",
                        imgsrc: imgpath+"new/21.png"
                    },
                    {
                        imgclass: "img22",
                        imgsrc: imgpath+"new/22.png"
                    },
                    {
                        imgclass: "img23",
                        imgsrc: imgpath+"new/23.png"
                    },
                    {
                        imgclass: "img24",
                        imgsrc: imgpath+"new/24.png"
                    },
				]
			}
		],
        lowertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "bigfont2",
                textclass: "middletext2 cssfadeout3",
                textdata: data.string.p1text4
            },
            {
                textclass: "middletext3",
                textdata: data.string.p1text5
            }
        ]
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html());
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("typesofcontent", $("#typesofcontent-partial").html());
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

    	$(".titletext").on(animationend, function(){
            $(".fadeintext1").addClass("cssfadein");
            $(".fadeintext2").addClass("cssfadein2");
        });

        if(countNext==0){
            $prevBtn.hide(0);
        }
        if (countNext==2) {
            $nextBtn.hide(0);
        }

        $(".fullbackground2").on(animationend, function(){
            $(".fullbackground3").addClass("cssfadein3");
        });

        $(".fullbackground3").on(animationend, function(){
            $(".fullbackground4").addClass("cssfadein3");
        });

        $(".fullbackground4").on(animationend, function(){
            $(".fullbackground5").addClass("cssfadein3");
        });

        $(".fullbackground5").on(animationend, function(){
            $(".fullbackground6").addClass("cssfadein3");
        });

        $(".fullbackground6").on(animationend, function(){
            $(".fullbackground7").addClass("cssfadein3");
        });

        $(".fullbackground7").on(animationend, function(){
            $(".fullbackground8").addClass("cssfadein3");
            $nextBtn.delay(3500).show(0);
        });

        if (countNext ==3) {
            $nextBtn.hide(0);
            $(".middletext2").on(animationend, function(){
            $(".img2").addClass('cssfadeimgin');
            $(".img20, .img9").addClass('cssfadeimgin1');
            $(".img1, .img12, .img8").addClass('cssfadeimgin2');
            $(".img7, .img19, .img15, .img3").addClass('cssfadeimgin3');
        });

        $(".img3").on(animationend, function(){
            $(".img22, .img16, .img5, .img6").addClass('cssfadeimgin');
            $(".img23, .img21, .img18, .img11").addClass('cssfadeimgin1');
            $(".img4, .img10, .img13, .img17").addClass('cssfadeimgin2');
            $(".middletext3").addClass('cssfadeimgin3');
        });

        $(".middletext3").on(animationend, function(){
            navigationcontroller();
        });
    };


	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		// $prevBtn.css('display', 'none');
		$nextBtn.show(0);

		// call navigation controller
		// navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

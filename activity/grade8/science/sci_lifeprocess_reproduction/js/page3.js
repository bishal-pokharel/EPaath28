var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page3/";

var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[	
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paperstacktextstyle",
				textdata  : data.string.p3text1,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		twohalvesverticle : [
			{
				textclass : "headinghalf",
				textdata  : data.string.p3text2,
			},
			{
				datahighlightflag : true,
				datahighlightcustomclass: "multiplehighlightslide",
				textclass : "parahalf",
				textdata  : data.string.p3text3,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"fullcover moveupanim",
				textdata: data.string.p3text2
			}
		],
		twohalvestitledetailblock : [
			{
				titletext : [
					{
						textclass : "twohalvestitletext",
						textdata  : data.string.textBinary,
					}
				],
				titleimage: [
					{
						imagestoshow:[
							{
								imgclass:"binaryanim",
								imgsrc: imgpath+"binary_fission.gif"
							}
						]
					}
				],
				detailpara:[
					{
						datahighlightflag : true,
						textclass:"twohalvesparaheading",
						textdata: data.string.p3text4
					},
					{
						datahighlightflag : true,
						textclass:"twohalvesparaheading",
						textdata: data.string.p3text5
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"fullcover moveupanim",
				textdata: data.string.p3text2
			}
		],
		twohalvestitledetailblock : [
			{
				titletext : [
					{
						textclass : "twohalvestitletext",
						textdata  : data.string.textMultiple,
					}
				],
				titleimage: [
					{
						imagestoshow:[
							{
								imgclass:"binaryanim",
								imgsrc: imgpath+"multiple_fission.gif"
							}
						]
					}
				],
				detailpara:[
					{
						datahighlightflag : true,
						textclass:"twohalvesparaheading",
						textdata: data.string.p3text6
					},
					{
						datahighlightflag : true,
						textclass:"twohalvesparaheading",
						textdata: data.string.p3text7
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "fullcover moveupanim",
				textdata  : data.string.textBudding,
			},
		],
		contentblocknocenteradjust : true,
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sideimg",
						imgsrc   : imgpath+"hydra_reproduction.gif",
					},
				]	
			},
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "sidepara",
				textdata          : data.string.p3text8,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass:"fullcover2",
				textdata: data.string.textFandR
			},
			{
				textclass:"paragraph",
				textdata: data.string.p3text4_1
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"fullpage slideleftanim",
						imgsrc:imgpath+"spirogyra.png"
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "fullcover2",
				textdata  : data.string.textF,
			},
		],
		contentblocknocenteradjust : true,

		uppertextblock : [
			{
				datahighlightflag  : true,
				textclass          : "definitionpara drawlineanim",
				textdata           : data.string.p3text9,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "frag5",
						imgsrc   : imgpath+"fragmentation/04.png",
					},
					{
						imgclass : "frag4",
						imgsrc   : imgpath+"fragmentation/04.png",
					},
					{
						imgclass : "frag3",
						imgsrc   : imgpath+"fragmentation/09.png",
					},
					{
						imgclass : "frag2",
						imgsrc   : imgpath+"fragmentation/02.png",
					},
					{
						imgclass : "frag1",
						imgsrc   : imgpath+"fragmentation/01.png",
					}
				]	
			},
		],
		lowertextblock:[
			{
				textclass: "bottompara",
				textdata: data.string.p3text10
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "fullcover2",
				textdata  : data.string.textR,
			},
		],
		uppertextblock:[
			{
				datahighlightflag : true,
				textclass         : "definitionparastyle",
				textdata          : data.string.p3text11,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "singleimgstyle positionfix",
						imgsrc   : imgpath+"star-fish.gif",
					},
				]	
			},
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "paratextstyle custom",
				textdata          : data.string.p3text12,
			}
		],
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "fullcover2",
				textdata  : data.string.p3text12_1,
			},
		],
		contentblocknocenteradjust : true,

		uppertextblock : [
			{
				datahighlightflag  : true,
				textclass          : "definitionpara",
				textdata           : data.string.p3text12_2,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "head",
						imgsrc   : imgpath+"planeria/head01.png",
					},
					{
						imgclass : "body",
						imgsrc   : imgpath+"planeria/body01.png",
					},
					{
						imgclass : "lbody",
						imgsrc   : imgpath+"planeria/lowerbody01.png",
					},
					{
						imgclass : "tail",
						imgsrc   : imgpath+"planeria/tail01.png",
					},
					{
						imgclass : "complete1",
						imgsrc   : imgpath+"planeria/regeneration01.png",
					},
					{
						imgclass : "complete2",
						imgsrc   : imgpath+"planeria/regeneration01.png",
					},
					{
						imgclass : "complete3",
						imgsrc   : imgpath+"planeria/regeneration01.png",
					},
					{
						imgclass : "complete4",
						imgsrc   : imgpath+"planeria/regeneration01.png",
					},
					{
						imgclass : "fragment2",
						imgsrc   : imgpath+"planeria/regeneration02.png",
					},
					{
						imgclass : "fragment1",
						imgsrc   : imgpath+"planeria/regeneration01.png",
					},
					{
						imgclass : "cutme",
						imgsrc   : "images/clickme_icon.png",
					},
				]	
			},
		],
		
		
	},
	{
		headerblock : [
			{
				textclass : "front sideside",
				textdata  : data.string.textSporulation,
			}
		],
		contentblocknocenteradjust : true,
		
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimgdef",
						imgsrc: imgpath+"orgin.png"
					}
				]
			}
		]
	},
	{
		headerblock:[
            {
                textclass : "headertextstyle",
                textdata  : data.string.textSporulation,
            },
        ],
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "definitionpara2",
				textdata  : data.string.p3text13_1,
			},
			{
				datahighlightflag : true,
				textclass : "definitionpara3",
				textdata  : data.string.p3text13,
			}
		],
		notifytextblock:[
			{
				textclass:"highlightdiv cssfadein"
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"fungiimg",
						imgsrc: imgpath+"sporulation01.png"
					},
					{
						imgclass:"fungiimg2 sporesanim",
						imgsrc: imgpath+"sporulation02.png"
					},
					{
						imgclass:"fungiimg4",
						imgsrc: imgpath+"sporulation04a.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"label2 cssfadein",
						imagelabeldata: data.string.textspores
					},
					{
						imagelabelclass:"label1",
						imagelabeldata: data.string.textsporangia
					}
				]
			}
		]
	},
	{
		headerblock:[
            {
                textclass : "headertextstyle",
                textdata  : data.string.textSporulation,
            },
        ],
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "definitionpara2",
				textdata  : data.string.p3text13_2,
			},
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"fungiimg5",
						imgsrc: imgpath+"sporulation05.png"
					},
					{
						imgclass:"fungiimg6",
						imgsrc: imgpath+"sporulation04.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"label3 cssfadein",
						imagelabeldata: data.string.textsporangia
					},
					{
						imagelabelclass:"label4",
						imagelabeldata: data.string.textspores
					}
				]
			}
		]
	},
	{
		uppertextblock : [
			{
				textclass : "greenheaderstyle double-border doubleanim",
				textdata  : data.string.textVege,
			},
			{
				datahighlightflag:true,
				textclass:"lowerpara cssfadein",
				textdata: data.string.p3text14
			}
		],

		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "sideimgstyle2",
						imgsrc   : imgpath+"1.jpg",
					},
				]	
			},
		]
	},
	{
		headerblock:[
            {
                textclass : "headertextstyle",
                textdata  : data.string.textVege,
            },
        ],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "title1_1",
				textdata: data.string.textCutting
			},
			{
				textclass: "parastyle",
				textdata: data.string.p3text16
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "cuttinimg4",
						imgsrc   : imgpath+"carrot/04.png",
					},
					{
						imgclass : "cuttinimg3",
						imgsrc   : imgpath+"carrot/03.png",
					},
					{
						imgclass : "cuttinimg2",
						imgsrc   : imgpath+"carrot/02.png",
					},
					{
						imgclass : "cuttinimg1",
						imgsrc   : imgpath+"carrot/01.png",
					},
					{
						imgclass:"clickmecut cssfadein2",
						imgsrc: "images/clickme_icon.png"
					}
				]	
			},
		]
	},

	{
		headerblock:[
            {
                textclass : "headertextstyle",
                textdata  : data.string.textVege,
            },
        ],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "title1_2",
				textdata: data.string.textGrafting
			},
			{
				textclass: "parastyle",
				textdata: data.string.p3text17
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "graftingimg4_1",
						imgsrc   : imgpath+"rose/04.png",
					},
					{
						imgclass : "graftingimg5",
						imgsrc   : imgpath+"rose/05.png",
					},
					{
						imgclass : "graftingimg6",
						imgsrc   : imgpath+"rose/06.png",
					},
					{
						imgclass : "graftingimg7",
						imgsrc   : imgpath+"rose/07.png",
					},
					{
						imgclass : "graftingimg4",
						imgsrc   : imgpath+"rose/02.png",
					},
					{
						imgclass : "graftingimg3",
						imgsrc   : imgpath+"rose/03.png",
					},
					{
						imgclass : "graftingimg1_1",
						imgsrc   : imgpath+"rose/01a.png",
					},
					{
						imgclass : "graftingimg2_1",
						imgsrc   : imgpath+"rose/01.png",
					},
					{
						imgclass : "graftingimg1",
						imgsrc   : imgpath+"rose/red-rose.png",
					},
					{
						imgclass : "graftingimg2",
						imgsrc   : imgpath+"rose/white-rose.png",
					},
					{
						imgclass : "blade",
						imgsrc   : imgpath+"rose/blade.png",
					},
					{
						imgclass:"arrow",
						imgsrc: imgpath+"rose/arrow.png",
					},
					{
						imgclass:"clickmegraft cssfadein1",
						imgsrc: "images/clickme_icon.png"
					}
				]	
			},
		]
	},

	{
		headerblock:[
            {
                textclass : "headertextstyle",
                textdata  : data.string.textVege,
            },
        ],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "title1_3",
				textdata: data.string.textLayering
			},
			{
				textclass: "parastyle",
				textdata: data.string.p3text18
			}
		],
		imageblock : [
			{
				imagestoshow:[
					{
						imgclass:"layeringimg",
						imgsrc:imgpath+"layering.svg"
					}
				]
			},
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.textVege,
			},
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				textclass: "title1",
				textdata: data.string.textCutting
			},
			{
				textclass: "title2",
				textdata: data.string.textGrafting
			},
			{
				textclass: "title3",
				textdata: data.string.textLayering
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "customcss1 cssfadein",
						imgsrc   : imgpath+"carrot/01.png",
					},
					{
						imgclass : "customcss2 cssfadein",
						imgsrc   : imgpath+"grafting01.png",
					},
					{
						imgclass : "customcss3 cssfadein",
						imgsrc   : imgpath+"layering.png",
					},
				]	
			},
		],

	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("twohalvestitledetailcontent", $("#twohalvestitledetailcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".fullcover").on(animationend, function() {
			$(".twohalvestitledetailblock").addClass("cssfadein");
		});

		if (countNext==6) {
			$nextBtn.hide(0);
		};

		$(".bottompara").on(animationend, function(){ 
			$nextBtn.show(0);
		});

		$(".sidepara").addClass("cssfadein");

		$(".drawlineanim").on(animationend, function(){ 
			$(".frag1").addClass("cssfadeout");
		});

		$(".frag1").on(animationend, function(){ 
			$(".frag2").addClass("cssfadeout2");
		});

		$(".frag2").on(animationend, function(){ 
			$(".frag3").addClass("cssfadeout2");
		});

		$(".frag3").on(animationend, function(){ 
			$(this).addClass("cssfadeout2");
			$(".frag4").css("left", "25%");
			$(".frag5").addClass('upupanim');
			$(".bottompara").addClass('cssfadein2');
			$nextBtn.delay(2000).show(0);
		});

		$(".cutme").on("click", function(){ 
			$(this).hide(0);
			$(".fragment1").addClass("cssfadeout");
			$(".definitionpara").addClass("cssfadeout");
		});

		$(".fragment1").on(animationend, function(){ 
			$(".fragment2").addClass('cssfadeout');
		});

		$(".fragment2").on(animationend, function() {
			$(".head").addClass("moveleftanim1");
			$(".body").addClass("movelefttanim2");
			$(".lbody").addClass("againmoveleftanim3");
			$(".tail").addClass("againmovelefttanim4");
		});

		if (countNext==8) {
			$nextBtn.hide(0);
		};

		$(".head").on(animationend, function() {
			$(".headertextstyle").text(data.string.p3text12_3);
			$(".complete1").addClass("cssfadein");
			$(".complete2").addClass("cssfadein");
			$(".complete3").addClass("cssfadein");
			$(".complete4").addClass("cssfadein");
			$nextBtn.delay(3000).show(0);
		});

		if (countNext==10) {
			$nextBtn.hide(0);
		};
		$(".highlightdiv").on("click", function(){ 
			$(".notifytextblock").removeAttr('data-usernotification');
			$(".label1, .fungiimg4").addClass("cssfadein");
			$(".definitionpara3").addClass("cssfadein2");
			$nextBtn.delay(3000).show(0);
		});

		$(".label3").on(animationend, function(){ 
			$(this).hide(0);
			$(".fungiimg6").addClass("cssfadeout");
			$(".fungiimg5, .label4, .definitionpara3").addClass("cssfadein");
			$nextBtn.delay(2000).show(0);
		});

		if (countNext==13 || countNext==14) {
			$nextBtn.hide(0);
		};
		$(".clickmecut").on("click", function(){ 
			$(this).hide(0);
			$(".cuttinimg1").addClass("cssfadeout");
		});

		$(".cuttinimg1").on(animationend, function(){ 
			$(".cuttinimg3").addClass("cssfadein");
		});

		$(".cuttinimg3").on(animationend, function(){ 
			$(".cuttinimg4").addClass("cssfadein");
			$nextBtn.delay(1000).show(0);
		});

		$(".clickmegraft").on("click", function(){ 
			$(this).hide(0);
			$(".graftingimg1").addClass('cssfadeout');
			$(".graftingimg1_1").addClass("cssfadein");
			$(".graftingimg2").addClass('cssfadeout');
			$(".graftingimg2_1").addClass("cssfadein");
			$(".blade").addClass('knifeanim');
		});

		$(".blade").on(animationend, function(){ 
			$(".graftingimg1, .graftingimg2, .graftingimg2_1, .graftingimg1_1").addClass('cssfadeout');
			$(".graftingimg3, .graftingimg4").addClass("moverightanim");
		});

		$(".graftingimg4").on(animationend, function(){ 
			$(".graftingimg4_1").addClass("cssfadein");
			$(".graftingimg3, .graftingimg4").delay(300).hide(0);
		});

		$(".graftingimg4_1").on(animationend, function(){ 
			$(".graftingimg5").addClass("cssfadein2");
		});

		$(".graftingimg5").on(animationend, function(){ 
			$(this).addClass('cssfadeout1');
			$(".graftingimg4_1").addClass('cssfadeout1');
			$(".graftingimg6").addClass("cssfadein1");
		});

		$(".graftingimg6").on(animationend, function(){ 
			$(".arrow").addClass("cssfadein1");
		});
		$(".arrow").on(animationend,function(){ 
			$(".graftingimg7").addClass("cssfadein1");
			$nextBtn.delay(100).show(0);
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
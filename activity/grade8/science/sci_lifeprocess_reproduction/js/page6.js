var SPACE = data.string.spacecharacter;
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page6/";

var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";

var content=[	
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paperstacktextstyle",
				textdata  : data.string.p6text1,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				datahighlightflag : true,
				datahighlightcustomclass: "bigfont",
				textclass : "headingstyle",
				textdata  : data.string.p6text10,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgsrc:imgpath+"fish01.png",
						imgclass:"type1 cssfadein"
					},
					{
						imgsrc:imgpath+"01.png",
						imgclass:"type2 cssfadein2"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "typelabel1 cssfadein",
						imagelabeldata: data.string.titleExernal
					},
					{
						imagelabelclass: "typelabel2 cssfadein2",
						imagelabeldata: data.string.titleInternal
					}
				]
			}
		]
	},
	{
		contentblockadditionalclass: "bluebackground",
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				datahighlightflag : true,
				textclass : "title cssborderanim",
				textdata  : data.string.textExernal,
			},
			{
				textclass: "para1",
				textdata: data.string.spacecharacter
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "paratxt",
				textdata: data.string.p6text11
			},
			{
				datahighlightflag: true,
				textclass:"para1_1",
				textdata: data.string.p6text7
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"svgimg1",
						imgsrc:imgpath+"fishreproduction.svg"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				datahighlightflag : true,
				textclass : "title cssborderanim",
				textdata  : data.string.textInternal,
			},
			{
				textclass: "para_1 cssfadein",
				textdata: data.string.spacecharacter
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "para_1txt cssfadein",
				textdata: data.string.p6text13
			},
			{
				datahighlightflag: true,
				textclass: "para2",
				textdata: data.string.p6text14
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"middleimage cssfadein2",
						imgsrc: imgpath+"01.png"
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		uppertextblock  : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "bigheadingtextstyle csspositionfix",
				textdata : data.string.p6text2,
			},
			{
				datahighlightflag: true,
				textclass: "headingstyle2 cssfadein",
				textdata : data.string.p6text3
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"imgwitharrow",
						imgsrc:imgpath+"egg-with-arrow.png"
					},
					{
						imgclass:"imgwitharrow2",
						imgsrc:imgpath+"sperm-with-arrow.png"
					},
					{
						imgclass:"clickme1 cssfadein2",
						imgsrc:"images/clickme_icon.png"
					},
					{
						imgclass:"clickme2 cssfadein2",
						imgsrc:"images/clickme_icon.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass: "label1 cssfadein2",
						imagelabeldata: data.string.textegg
					},
					{
						imagelabelclass: "label2 cssfadein2",
						imagelabeldata: data.string.textsperm
					}
				]
			}
		],
		lowertextblock:[
			{
				datahighlightflag : true,
				textclass:"arrowbox",
				textdata: data.string.p6text4_1
			},
			{
				datahighlightflag : true,
				textclass:"arrowbox2",
				textdata: data.string.p6text4
			},
		]
	},
	{
		contentblocknocenteradjust : true,

		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass: "bigfont",
				textclass : "bigheadingtextstyle csspositionfix",
				textdata  : data.string.p6text5,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"svgimg2",
						imgsrc: imgpath+"humanfamily.svg"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass:"lowerpara",
				textdata: data.string.p6text9
			},
			{
				textclass:"lowerpara2",
				textdata: data.string.p6text8
			}
		]
	},
	
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("subslidecontent", $("#subslidecontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		
		$(".cssborderanim").on(animationend, function (){ 
			$(".para1").addClass("cssfadein");
		});

		$(".para1").on(animationend, function(){ 
			$(".para1_1").addClass("cssfadein");
		});

		$(".para_1").on(animationend, function(){ 
			$(".para2").addClass('cssfadein');
		});

		$(".middleimage").on(animationend, function(){ 
			$nextBtn.delay(1000).show(0);
		});

		if (countNext==4) {
			$nextBtn.hide(0);
		};

		var clicklabel1=false;
		var clicklabel2=false;
		$(".clickme1").on("click", function(){ 
			$(this).hide(0);
			$(".label1").addClass("removebackground");
			$(".imgwitharrow").delay(200).show(200);
			$(".arrowbox").addClass("cssfadein");
			clicklabel1=true;
			if (clicklabel1==true && clicklabel2==true) {
				$nextBtn.delay(100).show(0);
			};
		});
		$(".clickme2").on("click", function(){ 
			$(this).hide(0);
			$(".label2").addClass("removebackground");
			$(".imgwitharrow2").delay(200).show(200);
			$(".arrowbox2").addClass("cssfadein");
			clicklabel2=true;
			if (clicklabel1==true && clicklabel2==true) {
				$nextBtn.delay(100).show(0);
			};
		});

		$(".onlyparatextstyle").on(animationend, function(){ 
			$(".lowerpara2").addClass('cssfadein');
		});

		$(".lowerpara2").on(animationend, function (){ 
			$(".lowerpara").addClass('cssfadein');
		});

		$(".lowerpara").on(animationend, function () {
		navigationcontroller();
			
		});

		// var $subslideblock = $board.find("div.subslideblock");

		// only when subslide block is present
		// if($subslideblock.length > 0){
		// 	// hide these from the main template
		// 	$nextBtn.css('display', 'none');
		// 	ole.footerNotificationHandler.hideNotification();

		// 	// precompile subslidecontent partial before updating it with new content
		// 	var subslidetemplate = Handlebars.compile($("#subslidecontent-partial").html());
		// 	var $subslidedata    = $subslideblock.children('div.subslidedata');
		// 	var $subslideprevBtn = $subslideblock.find("div.subslideprevBtn");
		// 	var $subslidenextBtn = $subslideblock.find("div.subslidenextBtn");
			
			// compile the subtemplate for new data update
			// var subslidelength = content[countNext]["subslideblock"].length-1;

			// initialize subslidecountNext variable as a counter and at 0
			// var subslidecountNext = 0;

			// function to load a subtemplate inside general template
			// function subtemplateloaderandnavigation(){
			// 	$subslidedata.html(subslidetemplate(content[countNext]["subslideblock"][subslidecountNext]));
				
			// 	// highlight required text inside the subslidedata div
			// 	texthighlight($subslidedata);

			// 	// navigation control for subtemplate
			// 	if(subslidecountNext < 1 && subslidecountNext != subslidelength){
			// 		$prevBtn.show(0);
			// 		$subslideprevBtn.css('display', 'none');
			// 		$subslidenextBtn.show(0);
			// 	}
				
			// 	else if(subslidecountNext > 0 && subslidecountNext < subslidelength && subslidecountNext != subslidelength){
			// 		$prevBtn.css('display', 'none');
			// 		$nextBtn.css('display', 'none');
			// 		$subslideprevBtn.show(0);
			// 		$subslidenextBtn.show(0);
			// 		ole.footerNotificationHandler.hideNotification();
			// 	}

			// 	else if(subslidecountNext == subslidelength){
			// 		navigationcontroller();
			// 		$prevBtn.css('display', 'none');
			// 		$subslideprevBtn.show(0);
			// 		$subslidenextBtn.css("display","none");
			// 	}
			// }

			// first load of subtemplate
			// subtemplateloaderandnavigation();

			// $subslideprevBtn.on('click', function() {
			// 	$(this).css('display', 'none');
			// 	subslidecountNext--;
			// 	subtemplateloaderandnavigation();
			// });

			// $subslidenextBtn.on('click', function() {
			// 	$(this).css('display', 'none');
			// 	subslidecountNext++;
			// 	subtemplateloaderandnavigation();
			// });
		}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
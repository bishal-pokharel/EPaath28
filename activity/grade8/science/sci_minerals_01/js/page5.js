var imgpath            = $ref+"/images/page5/";
var funfacthostimgpath = "images/funfact/funfact1.png";

var content=[
	{	
		funfactsheadingdata : data.string.p5funfactheadingtext,
		funfactshostimgsrc : funfacthostimgpath,
		factsblock : [
			{
				datahighlightflag : true,
				textdata : data.string.p5funfact1,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p5funfact2,
			},
			{
				textdata : data.string.p5funfact3,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p5funfact4,
			},
			{
				datahighlightflag : true,
				textdata : data.string.p5funfact5,
			},
			{
				textdata : data.string.p5funfact6,
			},
		],
	},
];

$(function () {	
	var $board = $('.board');
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

		/*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- User send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight attribute is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");			
			if($alltextpara.length > 0){
				var replaceinstring;
				var texthighlightstarttag = "<span class='parsedstring'>";
				var texthighlightendtag   = "</span>";
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


	/*====================================================
	=            funfacts template function            =
	====================================================*/
	function funfacts(){
		var source = $("#funfacts-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		
		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);

		ole.footerNotificationHandler.pageEndSetNotification();
	}

	/*=====  End of funfacts template function  ======*/
	
	funfacts();
});
var imagepath = $ref+"/images/page1/"

var content=[
	{
		slidingimages : [
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide1.jpg",
			},
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide2.jpg",
			},
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide4.jpg",
			},
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide5.jpg",
			},
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide3.jpg",
			},
			{	
				slideclass : "staticimages",				
				imgsrc : imagepath + "slide6.jpg",
			}
		],
		slideouttext : [
			{
				slideouttextclass : "slideoutheading",
				slideouttextdata : data.string.p1heading1,

			}
		]
	},
	{
		slidingimages : [
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide1.jpg",
			},
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide2.jpg",
			},
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide4.jpg",
			},
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide5.jpg",
			},
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide3.jpg",
			},
			{	
				slideclass : "staticimages slideanimation",				
				imgsrc : imagepath + "slide6.jpg",
			}
		],
		slideouttext : [
			{
				slideouttextclass : "slideoutheading slideout",
				slideouttextdata : data.string.p1heading1,
			},
			{
				slideouttextclass : "slideouttext",
				slideouttextdata : data.string.p1definitionwithouthighlighttext,
			}
		]
	},
	{	
		definitionblock : [
			{
				datahighlightflag : true,
				definitionheadingdata : data.string.p1definitionheadingtext,
				definitioncontentdata : data.string.p1definitioncontenttext,
			}
		],
		flipimageblock : [
			{
				flipcontent : [
					{
						frontpanecontent : [
								{
									frontfaceimgsrc :imagepath+"mineralhighlight1.png",
								}
							],
						backpanecontent : [
											data.string.p1mineralhighlight1text1,
											data.string.p1mineralhighlight1text2
										  ],
					},
					{
						frontpanecontent : [
								{
									frontfaceimgsrc :imagepath+"mineralhighlight2.png",
								}
							],
						backpanecontent : [
											data.string.p1mineralhighlight2text1,
											data.string.p1mineralhighlight2text2
										  ],
					},
					{
						frontpanecontent : [
								{
									frontfaceimgsrc :imagepath+"mineralhighlight3.png",
								}
							],
						backpanecontent : [
											data.string.p1mineralhighlight3text1,
											data.string.p1mineralhighlight3text2
										  ],
					},
					{
						frontpanecontent : [
								{
									frontfaceimgsrc :imagepath+"mineralhighlight4.png",
								}
							],
						backpanecontent : [
											data.string.p1mineralhighlight4text1,
											data.string.p1mineralhighlight4text2
										  ],
					},
					{
						frontpanecontent : [
								{
									frontfaceimgsrc :imagepath+"mineralhighlight5.png",
								}
							],
						backpanecontent : [
											data.string.p1mineralhighlight5text1,
											data.string.p1mineralhighlight5text2
										  ],
					},
				]
			}
		]
	},
]


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("flipcontent", $("#flipcontent-partial").html());

/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*===============================================
=            data highlight function            =
===============================================*/
/**

	What it does:
	- send an element where the function has to see
	for data to highlight
	- this function searches for all text nodes whose
	data-highlight element is set to true 
	-searches for # character and gives a start tag
	;span tag here, also for @ character and replaces with
	end tag of the respective

 */
function texthighlight($highlightinside){
	var $alltextpara = $highlightinside.find("*[data-highlight='true']");
	var replaceinstring;
	var texthighlightstarttag = "<span class='parsedstring'>";
	var texthighlightendtag = "</span>";
	if($alltextpara.length > 0){
		$.each($alltextpara, function(index, val) {
			// console.log(val);
			replaceinstring = $(this).html();
			replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
			replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
			$(this).html(replaceinstring);
		});
	}
}


/*=====  End of data highlight function  ======*/

/*==========================================================
=            imageslideout template caller            =
==========================================================*/
	function imageslideout() {
		var source = $("#imageslideout-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		navigationcontroller();	
	}

/*=====  End of imageslideout template caller  ======*/

/*==========================================================
=            definitionproperty template caller            =
==========================================================*/
	function definitionproperty() {
		var source = $("#definitionproperty-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// navigationcontroller();

		$nextBtn.css('display', 'none');

		var $flipimageblock = $board.children('.flipimageblock');
		var $flipcontainer = $flipimageblock.children('.flipcontainer');
		var $flippers = $flipcontainer.children('.flipper');
		var containerseencount = 0;
		
		/* event handler for flipcontainers */	
			
		$flipcontainer.on('click', function(event) {
			/* Act on the event */
			// set data attribute isclicked = clicked and disable pointer events			
			$(this).attr("data-isclicked","clicked");
			if(++containerseencount == $flippers.length){
				ole.footerNotificationHandler.pageEndSetNotification();
			}		
		});

		/*highlight all text children of board element whose 
		data-highlight attribute is set to true*/
		texthighlight($board);
	}
/*=====  End of definitionproperty template caller  ======*/
	
	function templateCaller(){
		switch(countNext){
			case 0 :
			case 1 : imageslideout(); break;
			case 2 : definitionproperty(); break;
			default : break;
		}
	}

	templateCaller();

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();		
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();	
		loadTimelineProgress($total_page,countNext+1);
	});
});
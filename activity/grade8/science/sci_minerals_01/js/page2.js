var imgpath = $ref+"/images/page2/";
var imgpathpage1 = $ref+"/images/page1/";

var content=[
	{	
		typeoflayout: "layouthorizontal",
		stiripeblock : true,
		textblockadditionalclass : "textblockmargin",
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata  : data.string.p2text5part1,
			},
			{
				textclass : "simpletextstyle",
				textdata  : data.string.p2text5part2,
			},
		],
		imageblock : [
			{
				imageclass : "mineralimgstyle",
				imgsrc : imgpathpage1 + "slide1.jpg"
			}
		]
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 1,
					mainpropertytext : data.string.p2property1,
					propertycontainerheadingdata : data.string.p2propertyheadingtext,
					propertyimage : [
										imgpath+"property1img1.png",
										imgpath+"property1img2.png",
									],
					propertyexplaindata : data.string.p2property1explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 2,
					mainpropertytext : data.string.p2property2,
					propertycontainerheadingdata : data.string.p2propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p2property1,
						}
					],
					propertyimage : [
										imgpath+"property2img1.png",
										imgpath+"property2img2.png",
									],
					propertyexplaindata : data.string.p2property2explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 3,
					mainpropertytext : data.string.p2property3,
					propertycontainerheadingdata : data.string.p2propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p2property1,
						},
						{
							propertylistcount : 2,
							propertylistdata : data.string.p2property2,
						}
					],
					propertyimage : [
										imgpath+"property3img1.png",
										imgpath+"property3img2.png",
									],
					propertyexplaindata : data.string.p2property3explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 4,
					mainpropertytext : data.string.p2property4,
					propertycontainerheadingdata : data.string.p2propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p2property1,
						},
						{
							propertylistcount : 2,
							propertylistdata : data.string.p2property2,
						},
						{
							propertylistcount : 3,
							propertylistdata : data.string.p2property3,
						}
					],
					propertyimage : [
										imgpath+"property4img1.png",
										imgpath+"property4img2.png",
									],
					propertyexplaindata : data.string.p2property4explain,
				}
		],
	},
	{	
		typeoflayout: "propertylayout",
		propertyblock : [
				{				
					mainpropertycount: 5,
					mainpropertytext : data.string.p2property5,
					propertycontainerheadingdata : data.string.p2propertyheadingtext,
					propertycontainerlist : [
						{
							propertylistcount : 1,
							propertylistdata : data.string.p2property1,
						},
						{
							propertylistcount : 2,
							propertylistdata : data.string.p2property2,
						},
						{
							propertylistcount : 3,
							propertylistdata : data.string.p2property3,
						},
						{
							propertylistcount : 4,
							propertylistdata : data.string.p2property4,
						}
					],
					propertyimage : [
										imgpath+"property5img1.png",
										imgpath+"property5img2.png",
									],
					propertyexplaindata : data.string.p2property5explain,
				}
		],
	},
	{	
		typeoflayout: "propertysummarylayout",
		propertysummaryblock : [
				{				
					propertysummaryheadingdata: data.string.p2propertyheadingtext,
					propertysummarylist : [
						{
							propertysummarylistcount : 1,
							propertysummarylistdata : data.string.p2property1,
						},
						{
							propertysummarylistcount : 2,
							propertysummarylistdata : data.string.p2property2,
						},
						{
							propertysummarylistcount : 3,
							propertysummarylistdata : data.string.p2property3,
						},
						{
							propertysummarylistcount : 4,
							propertysummarylistdata : data.string.p2property4,
						},
						{
							propertysummarylistcount : 5,
							propertysummarylistdata : data.string.p2property5,
						}
					],
				}
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

	/*==========  properties template caller  ==========*/

	function properties(){
		var source = $("#properties-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);
	}
	
	properties();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		properties();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		properties();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
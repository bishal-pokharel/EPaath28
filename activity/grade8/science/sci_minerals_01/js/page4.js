var imgpath = $ref+"/images/page4/";

var content=[
	{	
		typeoflayout: "layouthorizontal",
		textblockadditionalclass : "slideoutup",
		uppertextblock : [
			{
				textclass : "slideoutheading",
				textdata : data.string.p4s1,
			},
		],
		imageblock : [
			{
				imgsrc : imgpath + "nepal.png",
			}
		]
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object1nametext,
				occurrenceobjectimgsrc : imgpath+"iron.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"ironuse1.png",
												  imgpath+"ironuse2.png",
												  imgpath+"ironuse3.png",
												],
				occurrenceobjectusedata : data.string.p4object1usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"ironmap.png" ],
				occurrenceobjectlocationdata : data.string.p4object1occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object2nametext,
				occurrenceobjectimgsrc : imgpath+"copper.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"copperuse1.png",
												  imgpath+"copperuse2.png",
												  imgpath+"copperuse3.png",
												],
				occurrenceobjectusedata : data.string.p4object2usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"coppermap.png" ],
				occurrenceobjectlocationdata : data.string.p4object2occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object3nametext,
				occurrenceobjectimgsrc : imgpath+"lead.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"leaduse1.png",
												  imgpath+"leaduse2.png",
												  imgpath+"leaduse3.png",
												],
				occurrenceobjectusedata : data.string.p4object3usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"leadmap.png" ],
				occurrenceobjectlocationdata : data.string.p4object3occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object4nametext,
				occurrenceobjectimgsrc : imgpath+"zinc.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"zincuse1.png",
												  imgpath+"zincuse2.png",
												  imgpath+"zincuse3.png",
												],
				occurrenceobjectusedata : data.string.p4object4usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"zincmap.png" ],
				occurrenceobjectlocationdata : data.string.p4object4occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object5nametext,
				occurrenceobjectimgsrc : imgpath+"limestone.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"limestoneuse1.png",
												  imgpath+"limestoneuse2.png",
												  imgpath+"limestoneuse3.png",
												],
				occurrenceobjectusedata : data.string.p4object5usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"limestonemap.png" ],
				occurrenceobjectlocationdata : data.string.p4object5occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
	{	
		typeoflayout: "occurrencelayout",
		occurrenceblock : [
			{
				occurrenceobjectnamedata : data.string.p4object6nametext,
				occurrenceobjectimgsrc : imgpath+"graphite.png",
				occurrenceobjectuseimageblock : [
												  imgpath+"graphiteuse1.png",
												  imgpath+"graphiteuse2.png",
												  imgpath+"graphiteuse3.png",
												],
				occurrenceobjectusedata : data.string.p4object6usetext,
				occurrenceobjectlocationimageblock : [ imgpath+"graphitemap.png" ],
				occurrenceobjectlocationdata : data.string.p4object6occurrencetext,				
				usebuttondata : data.string.usebuttontext,
				occurrencebuttondata : data.string.occurrencebuttontext,
			},
		],		
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("occurrencecontent", $("#occurrencecontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- User send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight attribute is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");			
			if($alltextpara.length > 0){
				var replaceinstring;
				var texthighlightstarttag = "<span class='parsedstring'>";
				var texthighlightendtag   = "</span>";
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/


		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside, iscontainer){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*====================================================
	=            occurrence template function            =
	====================================================*/
	function occurrence(){
		var source = $("#occurrence-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		$nextBtn.hide(0);
		$prevBtn.hide(0);
		if (countNext==0) {
			$nextBtn.show(0);

		};

		var occ_use=false;
		var occ_btn=false;

		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);

		/* add event handlers to all notification enabled elements */
		notifyuser($board);

		var $occurrenceblock                 = $board.children('div').children('div.occurrenceblock');
		var $occurrenceobjectname            = $occurrenceblock.children('.occurrenceobjectname');
		var $occurrenceobjectdetails         = $occurrenceblock.children('div.occurrenceobjectdetails');
		var $occurrenceobjectdetailschildren = $occurrenceobjectdetails.children('div');
		var $occurrenceobjectimage           = $occurrenceobjectdetails.children('div.occurrenceobjectimage');
		var $occurrenceobjectuse             = $occurrenceobjectdetails.children('div.occurrenceobjectuse');
		var $occurrenceobjectlocation        = $occurrenceobjectdetails.children('div.occurrenceobjectlocation');
		var $useoccurrencebutton             = $occurrenceblock.children('div.useoccurrencebutton');
		var $useoccurrencebuttonchildren     = $useoccurrencebutton.children('div');
		var $usebutton                       = $useoccurrencebutton.children('div.usebutton');
		var $occurrencebutton                = $useoccurrencebutton.children('div.occurrencebutton');
		
		var useoccurrencebuttonfirstclick    = false; /*flag if use or occurrence button is clicked at most once*/
		
		/*----------  event handler for uses and occurrence button  ----------*/
		$useoccurrencebuttonchildren.on('click', function() {
			$useoccurrencebuttonchildren.attr('data-isclicked', '');
			$(this).attr('data-isclicked', 'clicked');
			/*if use or occurrence button is clicked for once add data-usernotification = notifyuser for 
				object name text*/
			if(!useoccurrencebuttonfirstclick){
				useoccurrencebuttonfirstclick = true;
				 // set notify user data attribute
				// $occurrenceobjectname.attr('data-usernotification', 'notifyuser');
				 // call notifyuser to +add an event handler to the same
				notifyuser($board);
			}

			$occurrenceobjectdetailschildren.css('display', 'none');
				/* on basis of which button is clicked show the corresponding object 
					detail */				
				if($(this).hasClass("usebutton")){
					$occurrenceobjectuse.show(0);
					occ_use=true;

					if (occ_use==true && occ_btn==true) {
						navigationcontroller();
					};
				}
				else if($(this).hasClass("occurrencebutton")){
					$occurrenceobjectlocation.show(0);
					occ_btn=true;
					
					if (occ_use==true && occ_btn==true) {
						navigationcontroller();
					};

				}
		});

		/*----------  event handler for mineral name  ----------*/
		$occurrenceobjectname.on('click', function() {
			$useoccurrencebuttonchildren.attr('data-isclicked', '');
			$occurrenceobjectdetailschildren.css('display', 'none');
			$occurrenceobjectimage.show(0);
		});
	}

	/*=====  End of occurrence template function  ======*/
	
	occurrence();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		occurrence();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		occurrence();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
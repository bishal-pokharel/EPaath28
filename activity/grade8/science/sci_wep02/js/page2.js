//array of diy squirrel image
var defimg = "images/definitionpage/definition4.png";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page2/";
var squirrelwhat = "images/lokharke/squirrel_what_animated.svg";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[	
	{
		contentblocknocenteradjust : true,
		uppertextblock : [
			{
				textclass : "custombigheadingtextstyle",
				textdata  : data.string.energy,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"lampost",
						imgsrc: imgpath+"source-of-energy.gif"
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass : "headertextstyle2",
				textdata  : data.string.p2text1,
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "cycling csscyclemove2",
						imgsrc   : imgpath+"cycling.gif"
					},
					{
						imgclass : "running csscyclemove",
						imgsrc   : imgpath+"running.gif"
					},
					{
						imgclass: "image1",
						imgsrc: imgpath+"candle.png"
					},
					{
						imgclass: "image2",
						imgsrc: imgpath+"magnet.png"
					},
					{
						imgclass: "image3",
						imgsrc: imgpath+"fire.png"
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "bottomdefinition",
				textdata: data.string.p2text2
			}
		]
	},
	{
		definitionblock:[
			{
    			definitionImageSource:defimg,
				definitionTextData: data.string.energy,
				definitionInstructionData: data.string.p2text3
			}
		]
	},
	{
		hasheaderblock: true,
		headerblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "headertextstyle3",
				textdata: data.string.p2text4
			}
		],
		contentblocknocenteradjust : true,
		
		imagegrid: [
			{
				imagestoshow:[
					{
						imgclass:"row1img1",
						imgsrc: imgpath+"bow01.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel1",
						imagelabeldata: data.string.mechanical
					}
				],
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img2",
						imgsrc: imgpath+"bulb.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel2",
						imagelabeldata: data.string.light
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img3",
						imgsrc: imgpath+"high-tension.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel3",
						imagelabeldata: data.string.electrical
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img4",
						imgsrc: imgpath+"fire.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel4",
						imagelabeldata: data.string.heat
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img5",
						imgsrc: imgpath+"candle.png"
					},
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel5",
						imagelabeldata: data.string.chemical
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img6",
						imgsrc: imgpath+"magnet.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel6",
						imagelabeldata: data.string.magnetic
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img7",
						imgsrc: imgpath+"nuclear.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel7",
						imagelabeldata: data.string.nuclear
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass:"row1img8",
						imgsrc: imgpath+"sound.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass:"imglabel8",
						imagelabeldata: data.string.sound
					}
				]
			}
		],
		rightbox:[
			{
				titlep:"header1",
				defp:"defmiddle1",
				titleofimg: data.string.mechanical,
				defofimg: data.string.p2mech
			},
			{
				titlep:"header2",
				defp:"defmiddle2",
				titleofimg: data.string.light,
				defofimg: data.string.p2lig
			},
			{
				titlep:"header3",
				defp:"defmiddle3",
				titleofimg: data.string.electrical,
				defofimg: data.string.p2elec
			},
			{
				titlep:"header4",
				defp:"defmiddle4",
				titleofimg: data.string.heat,
				defofimg: data.string.p2hea
			}
		],
		leftbox:[
			{
				titlep:"header5",
				defp:"defmiddle5",
				titleofimg: data.string.chemical,
				defofimg: data.string.p2chem
			},
			{
				titlep:"header6",
				defp:"defmiddle6",
				titleofimg: data.string.magnetic,
				defofimg: data.string.p2mag
			},
			{
				titlep:"header7",
				defp:"defmiddle7",
				titleofimg: data.string.nuclear,
				defofimg: data.string.p2nuc
			},
			{
				titlep:"header8",
				defp:"defmiddle8",
				titleofimg: data.string.sound,
				defofimg: data.string.p2sou
			}
		],
	},
	{
		hasheaderblock : true,
		headerblock : [
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass : "headertextstyle3",
				textdata  : data.string.p2mechicaltypes
			}
		],
		contentblocknocenteradjust : true,
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass : "potentialImg cssfadein",
						imgsrc   : imgpath+"bowandarrow.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "potential_label cssfadein",
						imagelabeldata: data.string.potential
					}
				]
			},
			{
				imagestoshow:[
					{
						imgclass : "kineticImg cssfadein3",
						imgsrc   : imgpath+"bowandarrow.gif"
					}
				],
				imagelabels:[
					{
						imagelabelclass: "kinetic_label cssfadein3",
						imagelabeldata: data.string.kinetic
					}
				]
			}
		],
	},	
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle2",
				textdata  : data.string.kinetic
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "orangehighlight",
				textclass: "defdiv",
				textdata: data.string.p2kineticdef
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "defdiv2 cssfadein",
				textdata: data.string.p2text5
			},
			{
				textclass: "thatis cssfadein2",
				textdata: data.string.p2thatis
			},
			{
				textclass: "formula1 cssfadein2",
				textdata: data.string.p2text6
			},
			{
				textclass: "formula2 cssfadein3",
				textdata: data.string.p2text7
			},
			{
				textclass: "formula3 cssfadein3",
				textdata: data.string.p2text8
			},
			{
				textclass: "formula4 cssfadein3",
				textdata: data.string.p2text9
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimageformula",
						imgsrc: imgpath+"motion.gif"
					}
				]
			}
		]
	},
	{
		hasheaderblock : true,
		headerblock : [
			{	
				textclass : "headertextstyle2",
				textdata  : data.string.potential
			}
		],
		contentblocknocenteradjust : true,
		uppertextblock:[
			{
				datahighlightflag: true,
				datahighlightcustomclass: "orangehighlight",
				textclass: "defdiv",
				textdata: data.string.p2potentialdef
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "thatis2 cssfadein2",
				textdata: data.string.p2text10
			},
			{
				textclass: "formula1_1 cssfadein2",
				textdata: data.string.p2text11
			},
			{
				textclass: "formula5_1 cssfadein3",
				textdata: data.string.p2text12
			},
			{
				datahighlightflag: true,
				datahighlightcustomclass: "bigfont",
				textclass: "formula6 cssfadein3",
				textdata: data.string.p2text13
			},
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sideimageformula",
						imgsrc: imgpath+"bowandarrow.gif"
					}
				]
			}
		]
	}
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("selectcontent", $("#selectcontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("hovertodescribecontent", $("#hovertodescribecontent-partial").html());
	 Handlebars.registerPartial("boxcontent", $("#boxcontent-partial").html());
	 Handlebars.registerPartial("boxcontent2", $("#boxcontent2-partial").html());
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext == 1){
			$nextBtn.hide(0).delay(10000).show(0);
			$prevBtn.show(0);
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		$(".quespara").on(animationend, function(){
			$(".effortinput, .loadinput, .fulcruminput").addClass("cssfadein");
		});

		$(".cycling").on(animationend, function(){ 
			$(".image1, .image2, .image3").addClass("cssfadein");
		});

		$(".image3").on(animationend, function(){ 
			$(".bottomdefinition").addClass('cssdivmove');
		});

		if(countNext==3){
			$nextBtn.hide(0);
		}

		var clickedImg = $board.find(".imagegrid").find('img');
		var imgclicked1 = false;
		var imgclicked2 = false;
		var imgclicked3 = false;
		var imgclicked4 = false;
		var imgclicked5 = false;
		var imgclicked6 = false;
		var imgclicked7 = false;
		var imgclicked8 = false;

		clickedImg.on("click", function(){ 
			var imageclass = $(this).attr("class");
			// alert(imageclass);
			if (imageclass == "row1img1") {
				$(this).addClass("borderhighlight");
				$(".row1img2, .row1img3, .row1img4").removeClass("borderhighlight");
				$(".rightbox").addClass("cssdivmoveright");
				$(".header2, .defmiddle2, .header3, .defmiddle3, .header4, .defmiddle4").removeClass("cssfadein");
				$(".header1, .defmiddle1").addClass("cssfadein");
				imgclicked1 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img2") {
				$(this).addClass("borderhighlight");
				$(".row1img1, .row1img3, .row1img4").removeClass("borderhighlight");
				$(".header1, .defmiddle1, .header3, .defmiddle3, .header4, .defmiddle4").removeClass("cssfadein");
				$(".header2, .defmiddle2").addClass("cssfadein");
				$(".rightbox").addClass("cssdivmoveright");
				imgclicked2 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img3") {
				$(this).addClass("borderhighlight");
				$(".row1img2, .row1img1, .row1img4").removeClass("borderhighlight");
				$(".header1, .defmiddle1, .header2, .defmiddle2, .header4, .defmiddle4").removeClass("cssfadein");
				$(".header3, .defmiddle3").addClass("cssfadein");
				$(".rightbox").addClass("cssdivmoveright");
				imgclicked3 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img4") {
				$(this).addClass("borderhighlight");
				$(".row1img2, .row1img3, .row1img1").removeClass("borderhighlight");
				$(".header1, .defmiddle1, .header2, .defmiddle2, .header3, .defmiddle3").removeClass("cssfadein");
				$(".header4, .defmiddle4").addClass("cssfadein");
				$(".rightbox").addClass("cssdivmoveright");
				imgclicked4 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img5") {
				$(this).addClass("borderhighlight");
				$(".row1img6, .row1img7, .row1img8").removeClass("borderhighlight");
				$(".leftbox").addClass("cssdivmoveleft");
				$(".header6, .defmiddle6, .header7, .defmiddle7, .header8, .defmiddle8").removeClass("cssfadein");
				$(".header5, .defmiddle5").addClass("cssfadein");
				imgclicked5 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img6") {
				$(this).addClass("borderhighlight");
				$(".row1img5, .row1img7, .row1img8").removeClass("borderhighlight");
				$(".header5, .defmiddle5, .header7, .defmiddle7, .header8, .defmiddle8").removeClass("cssfadein");
				$(".header6, .defmiddle6").addClass("cssfadein");
				$(".leftbox").addClass("cssdivmoveleft");
				imgclicked6 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img7") {
				$(this).addClass("borderhighlight");
				$(".row1img6, .row1img5, .row1img8").removeClass("borderhighlight");
				$(".header5, .defmiddle5, .header6, .defmiddle6, .header8, .defmiddle8").removeClass("cssfadein");
				$(".header7, .defmiddle7").addClass("cssfadein");
				$(".leftbox").addClass("cssdivmoveleft");
				imgclicked7 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
			if (imageclass == "row1img8") {
				$(this).addClass("borderhighlight");
				$(".row1img6, .row1img7, .row1img5").removeClass("borderhighlight");
				$(".header5, .defmiddle5, .header6, .defmiddle6, .header7, .defmiddle7").removeClass("cssfadein");
				$(".header8, .defmiddle8").addClass("cssfadein");
				$(".leftbox").addClass("cssdivmoveleft");
				imgclicked8 = true;
				if (imgclicked1 == true && imgclicked2 == true && imgclicked3 == true && imgclicked4 == true && imgclicked5 == true && imgclicked6 == true && imgclicked7 == true && imgclicked8 == true) {
					$nextBtn.show(0);
				};
			}
		});

		$(".customclassright").on("click", function(){ 
			$(".rightbox").removeClass('cssdivmoveright');
			$(".row1img1, .row1img2, .row1img3, .row1img4").removeClass("borderhighlight");
		});

		$(".customclassleft").on("click", function(){ 
			$(".leftbox").removeClass('cssdivmoveleft');
			$(".row1img5, .row1img6, .row1img7, .row1img8").removeClass("borderhighlight");

		});
		// for inputblock 
		// var loadvalue;
		// var fulcrumvalue;
		// var effortvalue;
		// var correctcount1=false;
		// var correctcount2=false;
		// var correctcount3=false;

		// $(".effortinput").change(function () {
		// 	effortvalue= $(this).val();
		// 		if (effortvalue=="effort") {
		// 			$(".effortinput").addClass('correct');
		// 			$(".effortinput").removeClass('incorrect');
		// 			correctcount1=true;
		// 			if (correctcount1==true && correctcount2==true && correctcount3==true) {
		// 				$nextBtn.show(0);
		// 				$(".handpump").addClass("cssfadein");
		// 				$(".handpump2").addClass("cssfadeout");
		// 			};
		// 		}
		// 		else{
		// 			$(".effortinput").addClass('incorrect');
		// 		}
		// });

		// $(".fulcruminput").change(function () {
		// 		fulcrumvalue= $(this).val();
		// 		if (fulcrumvalue=="fulcrum") {
		// 			$(".fulcruminput").addClass('correct');
		// 			$(".fulcruminput").removeClass('incorrect');
		// 			correctcount2=true;
		// 			if (correctcount1==true && correctcount2==true && correctcount3==true) {
		// 				$nextBtn.show(0);
		// 				$(".handpump").addClass("cssfadein");
		// 				$(".handpump2").addClass("cssfadeout");
		// 			};
		// 		}
		// 		else{
		// 			$(".fulcruminput").addClass('incorrect');
		// 		}
		// });

		// $(".loadinput").change(function () {
		// 		loadvalue= $(this).val();
		// 		if (loadvalue=="load") {
		// 			$(".loadinput").addClass('correct');
		// 			$(".loadinput").removeClass('incorrect');
		// 			correctcount3=true;
		// 			if (correctcount1==true && correctcount2==true && correctcount3==true) {
		// 				$nextBtn.show(0);
		// 				$(".handpump").addClass("cssfadein");
		// 				$(".handpump2").addClass('cssfadeout');
		// 			};
		// 		}
		// 		else{
		// 			$(".loadinput").addClass('incorrect');
		// 		}
		// });

		// var $hovertodescribeblock = $board.find("div.hovertodescribeblock");

		// if hoverdescribe block is present
		// if($hovertodescribeblock){
			/*var descriptioncontent = [
				effort : [data.string.effort]
			]*/
			// var $hovercontents = $hovertodescribeblock.find("div.hovercontents");
			// var $labels = $hovercontents.find("label");

			// var $discriptioncontents = $hovertodescribeblock.find("div.discriptioncontents");
			// var $leverpartheading = $discriptioncontents.find("p.leverpartheading");
			// var $leverpartdescription = $discriptioncontents.find("p.leverpartdescription");
			
			// $labels.hover(function() {
			// 	/* Stuff to do when the mouse enters the element */
			// 	$(this).css('color', 'red');

			// 	switch($(this).attr('class')){
			// 		case "labeleffort"    : $leverpartheading.html(data.string.effort+":");
			// 								$leverpartdescription.html(data.string.p2effortdescription);
			// 								break;
			// 		case "labeleffortarm" : $leverpartheading.html(data.string.effortarm+":");
			// 								$leverpartdescription.html(data.string.p2effortarmdescription);
			// 								break;
			// 		case "labelload"      : $leverpartheading.html(data.string.load+":");
			// 								$leverpartdescription.html(data.string.p2loaddescription);
			// 								break;
			// 		case "labelloadarm"   : $leverpartheading.html(data.string.loadarm+":");
			// 								$leverpartdescription.html(data.string.p2loadarmdescription);
			// 								break;
			// 		case "labelfulcrum"   : $leverpartheading.html(data.string.fulcrum+":");
			// 								$leverpartdescription.html(data.string.p2fulcrumdescription);
			// 								break;
			// 		default : break;
			// 	}

			// }, function() {
				/* Stuff to do when the mouse leaves the element */
			// 	$(this).css('color', 'black');
			// });
		// }
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
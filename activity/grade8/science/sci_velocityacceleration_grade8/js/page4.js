var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page4/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";

var content=[
	{	
		contentblockadditionalclass : "rashikstyle",		
		uppertextblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "relationhighlight",
				textclass : "textbluebandstyle",
				textdata  : data.string.p4text1,
			}
		]
	},
	{
		contentblockadditionalclass : "rashikstyle2",	
		lowertextblock : [
			{
				textclass : "paratextstyle",
				textdata  : data.string.p4text2,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "rashikcustomspancolor",
				textclass : "headertextstyle rashikheaderstyle",
				textdata  : data.string.p4text1,
			}
		],
		derivationslide1block : [
			{
				firstlineforderivation : data.string.p4text3,
				lhstopcontent : data.string.p4lhstopcontent,
				rhstopcontent : data.string.p4rhstopcontent,
				rhsbottomcontent : data.string.p4rhsbottomcontent,
				symbolmeanings : [
					data.string.p4whereline1,
					data.string.p4whereline2,
					data.string.p4whereline3,
					data.string.p4whereline4,
					data.string.p4whereline5,
				]
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "rashikcustomspancolor",
				textclass : "headertextstyle insertindex rashikheaderstyle",
				textdata  : data.string.p4text1,
			}
		],
		derivationslide2block : [
			{
				line1forderivation : data.string.p4text4,
				lhstopcontent1 : data.string.p4lhstopcontent,
				rhstopcontent1 : data.string.p4rhstopcontent,
				rhsbottomcontent1 : data.string.p4rhsbottomcontent,
				line2forderivation : data.string.p4text5,
				line3forderivation : data.string.p4text6,
				line4forderivation : data.string.p4text7,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "rashikcustomspancolor",
				textclass : "headertextstyle rashikheaderstyle",
				textdata  : data.string.p4text1,
			}
		],
		derivationslide3block : [
			{
				line1forderivation : data.string.p4text8,
				lhstopcontent1 : data.string.p4deriveslide3lhstopcontent1,
				rhstopcontent1 : data.string.p4deriveslide3rhstopcontent1,
				rhsbottomcontent1 : data.string.p4deriveslide3rhsbottomcontent1,
				rhs2topcontent1 : data.string.p4deriveslide3rhs2topcontent1,
				lhstopcontent2 : data.string.p4deriveslide3lhstopcontent2,
				rhstopcontent2 : data.string.p4deriveslide3rhstopcontent2,
				rhsbottomcontent2 : data.string.p4deriveslide3rhsbottomcontent2,
				line2forderivation : data.string.p4text9,
				lhstopcontent3 : data.string.p4deriveslide3lhstopcontent3,
				lhsbottomcontent3 : data.string.p4deriveslide3lhsbottomcontent3,
				rhstopcontent3 : data.string.p4deriveslide3rhstopcontent3,
				rhsbottomcontent3 : data.string.p4deriveslide3rhsbottomcontent3,
				lhstopcontent4 : data.string.p4deriveslide3lhstopcontent4,
				rhstopcontent4 : data.string.p4deriveslide3rhstopcontent4,
				rhsbottomcontent4 : data.string.p4deriveslide3rhsbottomcontent4,
				rhssuffixcontent4 : data.string.p4deriveslide3rhssuffixcontent4
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "rashikcustomspancolor",
				textclass : "headertextstyle insertindex rashikheaderstyle",
				textdata  : data.string.p4text1,
			}
		],
		derivationslide4block : [
			{
				lhstopcontent1 : data.string.p4deriveslide4lhstopcontent1,
				rhstopcontent1 : data.string.p4deriveslide4rhstopcontent1,
				rhsbottomcontent1 : data.string.p4deriveslide4rhsbottomcontent1,
				rhssuffixcontent1 : data.string.p4deriveslide4rhssuffixcontent1,
				rhs2topcontent1 : data.string.p4deriveslide4rhs2topcontent1,
				rhs2bottomcontent1 : data.string.p4deriveslide4rhs2bottomcontent1,

				lhstopcontent2 : data.string.p4deriveslide4lhstopcontent2,
				rhstopcontent2 : data.string.p4deriveslide4rhstopcontent2,
				rhsbottomcontent2 : data.string.p4deriveslide4rhsbottomcontent2,
				rhssuffixcontent2 : data.string.p4deriveslide4rhssuffixcontent2,
				rhs2topcontent2 : data.string.p4deriveslide4rhs2topcontent2,
				rhs2bottomcontent2 : data.string.p4deriveslide4rhs2bottomcontent2,

				lhstopcontent3 : data.string.p4deriveslide4lhstopcontent3,
				rhstopcontent3 : data.string.p4deriveslide4rhstopcontent3,
				rhsbottomcontent3 : data.string.p4deriveslide4rhsbottomcontent3,

				line1forderivation : data.string.p4text10,
				line2forderivation : data.string.p4text11,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				datahighlightflag : true,
				datahighlightcustomclass : "rashikcustomspancolor",
				textclass : "headertextstyle insertindex rashikheaderstyle",
				textdata  : data.string.p4text1,
			}
		],
		derivationslide5block : [
			{
				lhstopcontent1 : data.string.p4deriveslide5lhstopcontent1,
				rhstopcontent1 : data.string.p4deriveslide5rhstopcontent1,
				rhsbottomcontent1 : data.string.p4deriveslide5rhsbottomcommoncontent,

				lhstopcontent2 : data.string.p4deriveslide5lhstopcommoncontentwithoutor,
				rhstopcontent2 : data.string.p4deriveslide5rhstopcontent2,
				rhsbottomcontent2 : data.string.p4deriveslide5rhsbottomcommoncontent,
				
				lhstopcontent3 : data.string.p4deriveslide5lhstopcommoncontent,
				rhstopcontent3 : data.string.p4deriveslide5rhstopcontent3,
				rhsbottomcontent3 : data.string.p4deriveslide5rhsbottomcommoncontent,

				lhstopcontent4 : data.string.p4deriveslide5lhstopcommoncontent,
				rhstopcontent4 : data.string.p4deriveslide5rhstopcontent4,
				rhsbottomcontent4 : data.string.p4deriveslide5rhsbottomcommoncontent,

				lhstopcontent5 : data.string.p4deriveslide5lhstopcommoncontent,
				rhstopcontent5 : data.string.p4deriveslide5rhstopcontent5,
				rhsbottomcontent5 : data.string.p4deriveslide5rhsbottomcommoncontent,
				rhssuffixcontent5 : data.string.p4deriveslide5rhssuffixcontent,
				rhs2topcontent5 : data.string.p4deriveslide5rhs2topcontent5, 
				rhs2bottomcontent5 : data.string.p4deriveslide5rhsbottomcommoncontent,
				line2forderivation : data.string.p4text13,
			}
		]
	},
	{
		lowertextblock : [
			{
				textclass : "paratextstyle",
				textdata  : data.string.p4text14,
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());	 
	 Handlebars.registerPartial("derivationslide1content", $("#derivationslide1content-partial").html());
	 Handlebars.registerPartial("derivationslide2content", $("#derivationslide2content-partial").html());
	 Handlebars.registerPartial("derivationslide3content", $("#derivationslide3content-partial").html());
	 Handlebars.registerPartial("derivationslide4content", $("#derivationslide4content-partial").html());
	 Handlebars.registerPartial("derivationslide5content", $("#derivationslide5content-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $derivationslide1 = $board.find("div.derivationslide1block");
		var $derivationslide2 = $board.find("div.derivationslide2block");
		var $derivationslide3 = $board.find("div.derivationslide3block");
		var $derivationslide4 = $board.find("div.derivationslide4block");
		var $derivationslide5 = $board.find("div.derivationslide5block");
		
		if($derivationslide1.length > 0){
			$nextBtn.css('display', 'none');

			var $derivationelementanimate1 = $derivationslide1.find(".derivationelementanimate:nth-child(1)"); /*do not do nth-of-type here- it selects type of element*/
			var $derivationelementanimate2 = $derivationslide1.find(".derivationelementanimate:nth-child(2)");
			var $derivationelementanimate3 = $derivationslide1.find(".derivationelementanimate:nth-child(3)");
				
			$derivationelementanimate1.attr("data-animate","true");
			
			$derivationelementanimate1.one(animationend, function(){
				$derivationelementanimate2.attr("data-animate","true");
			});

			$derivationelementanimate2.one(animationend, function(){
				$derivationelementanimate3.attr("data-animate","true");
			});

			$derivationelementanimate3.one(animationend, function(){
				$nextBtn.show(0);
			});
		}

		else if($derivationslide2.length > 0){
			$nextBtn.css('display', 'none');

			var $derivationelementanimate1 = $derivationslide2.find(".derivationelementanimate:nth-child(1)"); /*do not do nth-of-type here- it selects type of element*/
			var $derivationelementanimate2 = $derivationslide2.find(".derivationelementanimate:nth-child(2)");
			var $derivationelementanimate3 = $derivationslide2.find(".derivationelementanimate:nth-child(3)");
			var $derivationelementanimate4 = $derivationslide2.find(".derivationelementanimate:nth-child(4)");
			var $derivationelementanimate5 = $derivationslide2.find(".derivationelementanimate:nth-child(5)");
				
			$derivationelementanimate1.attr("data-animate","true");
			
			$derivationelementanimate1.one(animationend, function(){
				$derivationelementanimate2.attr("data-animate","true");
			});

			$derivationelementanimate2.one(animationend, function(){
				$derivationelementanimate3.attr("data-animate","true");
			});

			$derivationelementanimate3.one(animationend, function(){
				$derivationelementanimate4.attr("data-animate","true");
			});

			$derivationelementanimate4.one(animationend, function(){
				$derivationelementanimate5.attr("data-animate","true");
			});

			$derivationelementanimate5.one(animationend, function(){
				$nextBtn.show(0);
			});
		}

		else if($derivationslide3.length > 0){
			$nextBtn.css('display', 'none');

			var $derivationelementanimate1 = $derivationslide3.find(".derivationelementanimate:nth-child(1)"); /*do not do nth-of-type here- it selects type of element*/
			var $derivationelementanimate2 = $derivationslide3.find(".derivationelementanimate:nth-child(2)");
			var $derivationelementanimate3 = $derivationslide3.find(".derivationelementanimate:nth-child(3)");
			var $derivationelementanimate4 = $derivationslide3.find(".derivationelementanimate:nth-child(4)");
			var $derivationelementanimate5 = $derivationslide3.find(".derivationelementanimate:nth-child(5)");
			var $derivationelementanimate6 = $derivationslide3.find(".derivationelementanimate:nth-child(6)");
				
			$derivationelementanimate1.attr("data-animate","true");
			
			$derivationelementanimate1.one(animationend, function(){
				$derivationelementanimate2.attr("data-animate","true");
			});

			$derivationelementanimate2.one(animationend, function(){
				$derivationelementanimate3.attr("data-animate","true");
			});

			$derivationelementanimate3.one(animationend, function(){
				$derivationelementanimate4.attr("data-animate","true");
			});

			$derivationelementanimate4.one(animationend, function(){
				$derivationelementanimate5.attr("data-animate","true");
			});

			$derivationelementanimate5.one(animationend, function(){
				$derivationelementanimate6.attr("data-animate","true");
			});

			$derivationelementanimate6.one(animationend, function(){
				$nextBtn.show(0);
			});
		}

		else if($derivationslide4.length > 0){
			$nextBtn.css('display', 'none');

			var $derivationelementanimate1 = $derivationslide4.find(".derivationelementanimate:nth-child(1)"); /*do not do nth-of-type here- it selects type of element*/
			var $derivationelementanimate2 = $derivationslide4.find(".derivationelementanimate:nth-child(2)");
			var $derivationelementanimate3 = $derivationslide4.find(".derivationelementanimate:nth-child(3)");
			var $derivationelementanimate4 = $derivationslide4.find(".derivationelementanimate:nth-child(4)");
			var $derivationelementanimate5 = $derivationslide4.find(".derivationelementanimate:nth-child(5)");
							
			$derivationelementanimate1.attr("data-animate","true");
			
			$derivationelementanimate1.one(animationend, function(){
				$derivationelementanimate2.attr("data-animate","true");
			});

			$derivationelementanimate2.one(animationend, function(){
				$derivationelementanimate3.attr("data-animate","true");
			});

			$derivationelementanimate3.one(animationend, function(){
				$derivationelementanimate4.attr("data-animate","true");
			});

			$derivationelementanimate4.one(animationend, function(){
				$derivationelementanimate5.attr("data-animate","true");
			});

			$derivationelementanimate5.one(animationend, function(){
				$nextBtn.show(0);
			});
		}

		else if($derivationslide5.length > 0){
			$nextBtn.css('display', 'none');

			var $derivationelementanimate1 = $derivationslide5.find(".derivationelementanimate:nth-child(1)"); /*do not do nth-of-type here- it selects type of element*/
			var $derivationelementanimate2 = $derivationslide5.find(".derivationelementanimate:nth-child(2)");
			var $derivationelementanimate3 = $derivationslide5.find(".derivationelementanimate:nth-child(3)");
			var $derivationelementanimate4 = $derivationslide5.find(".derivationelementanimate:nth-child(4)");
			var $derivationelementanimate5 = $derivationslide5.find(".derivationelementanimate:nth-child(5)");
			var $derivationelementanimate6 = $derivationslide5.find(".derivationelementanimate:nth-child(6)");
							
			$derivationelementanimate1.attr("data-animate","true");
			
			$derivationelementanimate1.one(animationend, function(){
				$derivationelementanimate2.attr("data-animate","true");
			});

			$derivationelementanimate2.one(animationend, function(){
				$derivationelementanimate3.attr("data-animate","true");
			});

			$derivationelementanimate3.one(animationend, function(){
				$derivationelementanimate4.attr("data-animate","true");
			});

			$derivationelementanimate4.one(animationend, function(){
				$derivationelementanimate5.attr("data-animate","true");
			});

			$derivationelementanimate5.one(animationend, function(){
				$derivationelementanimate6.attr("data-animate","true");
			});

			$derivationelementanimate6.one(animationend, function(){
				$nextBtn.show(0);
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
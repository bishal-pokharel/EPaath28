var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page5/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";

var content=[
	{	
		contentblockadditionalclass : "coverboardbg1",
		uppertextblock : [
			{
				textclass : "simpletextstyle",
				textdata  : data.string.p5text1,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		numericaltype1block : [
			{
				beforecontentdata        : data.string.p5questionbeforecontent,
				textclass                : "onlyparatextstyle",
				datahighlightflag        : true,
				datahighlightcustomclass : "answerhints",
				numericalquestiondata    : data.string.p5question1,
				givenintrodata           : data.string.p5givenintrotext,
				giveninputs : [
					{
						inputprefixdata : data.string.p5giveninputprefix1,
						correctansdata  : data.string.p5giveninput1correctans,
						inputunitdata   : data.string.p5unitmpers,
						tohighlight     : "nth-of-type(1)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						checkanswerdata : data.string.okaytext,
					},
					{
						inputprefixdata : data.string.p5giveninputprefix2,
						correctansdata  : data.string.p5giveninput2correctans,
						inputunitdata   : data.string.p5unitmpers,
						tohighlight     : "nth-of-type(3)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						checkanswerdata : data.string.okaytext,
					},
					{
						inputprefixdata : data.string.p5giveninputprefix3,
						correctansdata  : data.string.p5giveninput3correctans,
						inputunitdata   : data.string.p5units,
						tohighlight     : "nth-of-type(2)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						checkanswerdata : data.string.okaytext,
					}
				],
				givenimgsrc : imgpath + "q1.png",
				unknownvarintrodata : data.string.p5unknownintrotext,
				unknownvariables : [
					{
						unknownvardata : data.string.p5unknownvar1,
					}
				],
				showsolutiondata : data.string.p5showsolutiontext,
				solutiondata : data.string.p5solutiontext,
				solutionintrodata : data.string.p5solutionintrotext,
				solutionpart1data : data.string.p5solutionpart1,
				solutionequaltosign : data.string.p5solutionequalto,
				fraction1numerator : data.string.p5fraction1numerator,
				fraction1denominator : data.string.p5fraction1denominator,
				solutionpart2data : data.string.p5solutionpart2,
				fraction2numerator : data.string.p5fraction2numerator,
				fraction2denominator : data.string.p5fraction2denominator,
				solutionpart3data : data.string.p5solutionpart3,
				answerdatahighlightflag : true,
				solutionanswerdata : data.string.p5answertosolution,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		numericaltype2block : [
			{
				beforecontentdata        : data.string.p5questionbeforecontent,
				textclass                : "onlyparatextstyle",
				datahighlightflag        : true,
				datahighlightcustomclass : "answerhints",
				numericalquestiondata    : data.string.p5question2,
				givenintrodata           : data.string.p5givenintrotext,
				giveninputs : [
					{
						inputprefixdata : data.string.p5giveninputprefix1,
						correctansdata  : data.string.p5giveninput1correctans,
						inputunitdata   : data.string.p5unitmpers,
						tohighlight     : "nth-of-type(1)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						checkanswerdata : data.string.okaytext,
					},
					{
						inputprefixdata : data.string.p5q2giveninputprefix2,
						correctansdata  : data.string.p5q2giveninput2correctans,
						inputunitdata   : data.string.p5unitmpers2,
						tohighlight     : "nth-of-type(2)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						checkanswerdata : data.string.okaytext,
					},
					{
						inputprefixdata : data.string.p5giveninputprefix3,
						correctansdata  : data.string.p5q2giveninput3correctans,
						inputunitdata   : data.string.p5units,
						tohighlight     : "nth-of-type(3)", /*this refers to which hint span from question is associated with this input*/
						hintdata        : data.string.p5givenhint,
						extrahintdata   : data.string.p5q2input3extrahint,
						checkanswerdata : data.string.okaytext,
					}
				],
				givenimgsrc : imgpath + "q2.png",
				unknownvarintrodata : data.string.p5unknownintrotext,
				unknownvariables : [
					{
						unknownvardata : data.string.p5q2unknownvar1,
					},
					{
						unknownvardata : data.string.p5q2unknownvar2,
					}
				],
				showsolution1data               : data.string.p5q2showsolution1text,
				solution1data                   : data.string.p5q2solution1text,
				solution1introdata              : data.string.p5q2solution1introtext,
				solution1part1data              : data.string.p5q2solution1part1,
				solution1part2data              : data.string.p5q2solution1part2,
				solution1part3data              : data.string.p5q2solution1part3,
				solution1part4data              : data.string.p5q2solution1part4,
				solutionequaltosign             : data.string.p5solutionequalto,
				showsolution2data               : data.string.p5q2showsolution2text,
				solution2data                   : data.string.p5q2solution2text,
				solution2introdata              : data.string.p5q2solution2introtext,
				solution2part1data              : data.string.p5q2solution2part1,
				q2solution2fraction1numerator   : data.string.p5q2solution2fraction1numerator,
				q2solution2fraction1denominator : data.string.p5q2solution2fraction1denominator,
				q2solution2fraction2numerator   : data.string.p5q2solution2fraction2numerator,
				q2solution2fraction2denominator : data.string.p5q2solution2fraction1denominator,
				solution2part2data              : data.string.p5q2solution2part2,
			}
		]
	},
	{	
		uppertextblock : [
			{
				textclass : "paratextstyle",
				textdata  : data.string.p5text2,
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("numericaltype1content", $("#numericaltype1content-partial").html());
	 Handlebars.registerPartial("numericaltype2content", $("#numericaltype2content-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
	}

	function questiontype1template() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $numericalblock = $board.find("div.numericaltype1block");
		
		// if numericalblock is present
		if ($numericalblock.length > 0){
			$nextBtn.css('display', 'none');

			var $answer = $numericalblock.find("p.answer");
			var $solutioncontainer = $numericalblock.find("div.solutioncontainer");
			var $unknownvarcontainer = $numericalblock.find("div.unknownvarcontainer");
			var $giveninputscontainer = $numericalblock.find("div.giveninputscontainer");
			var $showsolutionbutton = $numericalblock.find("button#showsolution");
			var $question = $numericalblock.find(".numericalquestion");
			var $giveninputdivs = $numericalblock.find("div.giveninput"); /*all the input containing divs*/
			var $userhints = $numericalblock.find(".userhint");
			var $numericinputs = $numericalblock.find("input[type='text']");
			var $okaybuttons = $numericalblock.find("button#checkanswer");
			var $currentokaybutton;
			
			// event-handler for numeric inputs types to accept only numbers
			$numericinputs.keyup(function(event) {	
				// accept only numbers or digits
				$(this).val($(this).val().replace(/[^0-9]/g,''));
				$currentokaybutton = $(this).siblings('button#checkanswer');
				
				// if enter is pressed trigger ok button
				if(event.which === 13) {
			        $currentokaybutton.trigger("click");
    			}	
    			// else for any value input show okay button
    			else{
    				// hide the related okay button if there is nothing input
					$(this).val()  ? $currentokaybutton.show(0) : $currentokaybutton.css('display', 'none');
    			}
			});

			var numberofanswercheckattemptsallowed = 1;
			var numberofanswercheckattempts = 0;
			var totalnumberofinputs = $giveninputdivs.length;
			var $currentinput;
			var $currenthint;
			var currentcorrectanswer;
			var useranswer;
			var numberofinputs = 0;

			// event-handler for okay button
			$okaybuttons.on('click', function() {
				numberofanswercheckattempts++;

				$currenthint = $(this).siblings('.userhint');
				$currentinput = $(this).siblings('input[type="text"]');
				currentcorrectanswer = $currentinput.attr("data-correctans");
				useranswer = $currentinput.val();

				// check if answer matches the user input
				if(currentcorrectanswer == useranswer){
					$(this).css('display', 'none');
					$currenthint.css('display', 'none');
					$currentinput.attr('data-iscorrect', 'correct');
					$currentinput.prop('disabled', 'true');
					numberofinputs++;
					// now show the next input if all inputs are not shown
					if (numberofinputs < totalnumberofinputs){
						$(this).parent("div.giveninput").next("div.giveninput").css("opacity","1");
					}
					else if(numberofinputs == totalnumberofinputs){
						$giveninputscontainer.css('background-color', 'rgb(222, 248, 181)');
						$unknownvarcontainer.css('opacity', '1');
					}
					numberofanswercheckattempts = 0;
				}
				// if the answer does not match the user input 
				else if(currentcorrectanswer != useranswer){
					$currentinput.attr('data-iscorrect', 'incorrect');					
					numberofanswercheckattempts > numberofanswercheckattemptsallowed ? $currenthint.show(0) : null;
				}
			});

			var currenthinthighlight;
			// event-handler for userhints on hover
			$userhints.hover(function() {
				/* Stuff to do when the mouse enters the element */
				currenthinthighlight = $(this).attr('data-highlighthintnumber');
				$question.find('span:'+currenthinthighlight).addClass('highlighthint');

			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$question.children('span').removeClass('highlighthint');
			});

			// unknown variable container event handler
			$unknownvarcontainer.one('transitionend', function() {
				$showsolutionbutton.css('visibility', 'visible');
			});

			var $solutionsteps = $solutioncontainer.children('.solution').children('span');
			$showsolutionbutton.one('click', function() {
				$(this).css('display', 'none');
				$solutioncontainer.css('opacity', '1');
				$solutionsteps.attr('data-showslowly', 'true');				
				$answer.css('opacity', '1');
			});

			// answer element event handler
			$answer.one('transitionend', function() {
				$nextBtn.show(0);
			});
		}
	}

	function questiontype2template() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $numericalblock = $board.find("div.numericaltype2block");
		
		// if numericalblock is present
		if ($numericalblock.length > 0){
			$nextBtn.css('display', 'none');

			var $solution1container = $numericalblock.find("div.solution1container");
			var $solution2container = $numericalblock.find("div.solution2container");
			var $unknownvarcontainer = $numericalblock.find("div.unknownvarcontainer");
			var $giveninputscontainer = $numericalblock.find("div.giveninputscontainer");
			var $showsolution1button = $numericalblock.find("button#showsolution1");
			var $showsolution2button = $numericalblock.find("button#showsolution2");
			var $question = $numericalblock.find(".numericalquestion");
			var $giveninputdivs = $numericalblock.find("div.giveninput"); /*all the input containing divs*/
			var $userhints = $numericalblock.find(".userhint");
			var $numericinputs = $numericalblock.find("input[type='text']");
			var $okaybuttons = $numericalblock.find("button#checkanswer");
			var $currentokaybutton;
			
			// event-handler for numeric inputs types to accept only numbers
			$numericinputs.keyup(function(event) {	
				// accept only numbers or digits and a decimal
				$(this).val($(this).val().replace(/[^0-9.]/g,''));
				$currentokaybutton = $(this).siblings('button#checkanswer');
				
				// if enter is pressed trigger ok button
				if(event.which === 13) {
			        $currentokaybutton.trigger("click");
    			}	
    			// else for any value input show okay button
    			else{
    				// hide the related okay button if there is nothing input
					$(this).val()  ? $currentokaybutton.show(0) : $currentokaybutton.css('display', 'none');
    			}
			});

			var numberofanswercheckattemptsallowed = 1;
			var numberofanswercheckattempts = 0;
			var totalnumberofinputs = $giveninputdivs.length;
			var $currentinput;
			var $currenthint;
			var currentcorrectanswer;
			var useranswer;
			var numberofinputs = 0;

			// event-handler for okay button
			$okaybuttons.on('click', function() {
				numberofanswercheckattempts++;

				$currenthint = $(this).siblings('.userhint');
				$currentinput = $(this).siblings('input[type="text"]');
				currentcorrectanswer = $currentinput.attr("data-correctans");
				useranswer = $currentinput.val();

				// check if answer matches the user input
				if(currentcorrectanswer == useranswer){
					$(this).css('display', 'none');
					$currenthint.css('display', 'none');
					$currentinput.attr('data-iscorrect', 'correct');
					$currentinput.prop('disabled', 'true');
					numberofinputs++;
					// now show the next input if all inputs are not shown
					if (numberofinputs < totalnumberofinputs){
						$(this).parent("div.giveninput").next("div.giveninput").css("opacity","1");
					}
					else if(numberofinputs == totalnumberofinputs){
						$giveninputscontainer.css('background-color', 'rgb(222, 248, 181)');
						$unknownvarcontainer.css('opacity', '1');
					}
					numberofanswercheckattempts = 0;
				}
				// if the answer does not match the user input 
				else if(currentcorrectanswer != useranswer){
					$currentinput.attr('data-iscorrect', 'incorrect');					
					numberofanswercheckattempts > numberofanswercheckattemptsallowed ? $currenthint.show(0) : null;
				}
			});

			var currenthinthighlight;
			// event-handler for userhints on hover
			$userhints.hover(function() {
				/* Stuff to do when the mouse enters the element */
				currenthinthighlight = $(this).attr('data-highlighthintnumber');
				$question.find('span:'+currenthinthighlight).addClass('highlighthint');

			}, function() {
				/* Stuff to do when the mouse leaves the element */
				$question.children('span').removeClass('highlighthint');
			});

			// unknown variable container event handler
			$unknownvarcontainer.one('transitionend', function() {
				$showsolution1button.css('visibility', 'visible');
			});

			var solution2seen = false; /*flag to ensure if solution 2 is seen*/
			var $solution1steps = $solution1container.find(".solution").children('span');
			var $solution2steps = $solution2container.find(".solution").children('span');
			/*find the last step for solution 1 and two, such that on their transition end
			show solution button for next can be shown*/
			var $solution1laststep = $solution1container.find(".solution").children('span:nth-last-of-type(1)');
			var $solution2laststep = $solution2container.find(".solution").children('span:nth-last-of-type(1)');
			
			$showsolution1button.on('click', function() {
				$(this).css('visibility', 'hidden');
				$solution1container.css('opacity', '1');
				$solution2container.css('opacity', '0');
				$solution2steps.attr('data-showslowly', '');
				$solution1steps.attr('data-showslowly', 'true');
			});

			$showsolution2button.on('click', function() {
				$solution1steps.attr('data-showslowly', '');
				$solution2steps.attr('data-showslowly', 'true');
				$(this).css('visibility', 'hidden');
				
				$solution1container.css('opacity', '0');
				$solution2container.css('opacity', '1');
			});

			// on showing the last step for solution 1 show solution 2 button
			$solution1laststep.on('transitionend', function(event) {
				event.preventDefault();
				/* Act on the event */
				$showsolution2button.css("visibility","visible");
			});

			// on showing the last step for solution 2 show solution 1 button
			$solution2laststep.on('transitionend', function(event) {
				event.preventDefault();
				/* Act on the event */
				$showsolution1button.css("visibility","visible");
				// when it is set for the first time it should always be true so this
				solution2seen = true;
				// only if solution 2 is already seen on clicking showsolution1 show solution2 button
				solution2seen ? $nextBtn.show(0) : null;
			});
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		switch(countNext){
			case 1 : questiontype1template(); break;
			case 2 : questiontype2template(); break;
			default : generaltemplate(); break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
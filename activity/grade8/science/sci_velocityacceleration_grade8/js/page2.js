//array of image
var definitionImage = [
	"images/definitionpage/definition1.png",
	"images/definitionpage/definition2.png",
	"images/definitionpage/definition3.png",
	"images/definitionpage/definition4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentdefinitionImage = definitionImage[randomImageNumeral];

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE = data.string.spacecharacter;
var imgpath = $ref+"/images/page2/";
var squirrelinterested = "images/lokharke/important_content_animated.svg";

var content=[
	{
		contentblocknocenteradjust : true,
		expobservationblock : [
			{
				instructiondata       : data.string.p2instructiontext,
				currentvelocityprefix : data.string.p2currentvelocityprefix,
				currentvelocity       : data.string.p2currentvelocity,
				velocityunit          : data.string.p2velocityunit,
				spacecharacter        : data.string.spacecharacter,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle",
				textdata  : data.string.accelerationtext,
			}
		],
		accelerationexplainblock : [
			{
				accexplainimgsrc : imgpath + "carpath1.png",
				accexplaindata : data.string.p2text1,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle",
				textdata  : data.string.accelerationtext,
			}
		],
		accelerationexplainblock : [
			{
				accexplainimgsrc : imgpath + "carpath2.png",
				accexplaindata : data.string.p2text2,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle",
				textdata  : data.string.accelerationtext,
			}
		],
		accelerationexplainblock : [
			{
				accexplainimgsrc : imgpath + "carpath3.png",
				accexplaindata : data.string.p2text3,
			}
		]
	},	
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle",
				textdata  : data.string.accelerationtext,
			}
		],
		accelerationexplainblock : [
			{
				accexplainimgsrc : imgpath + "carpath4.png",
				accexplaindata : data.string.p2text4,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle headingpinkbandstyle",
				textdata  : data.string.accelerationtext,
			}
		],
		accelerationexplainblock : [
			{
				accexplainimgsrc : imgpath + "allcar.png",
				accexplaindata : data.string.p2velocitytoacceleration,
			}
		]
	},	
	{
		definitionblock  : [
			{
				definitionImageSource     : currentdefinitionImage,
				definitionTextData        : data.string.definitiontext,
				datahighlightflag         : true,
				definitionInstructionData : data.string.p2accelerationdefinition,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.p2accelerationformulatext,
			}
		],
		derivationblocknoreplacement : true,
		derivationblock : [
			{
				
				derivationprefixpara : [
					data.string.p2accderivationprefix1,
				],
				derivationformula : [
					{
						lhscontent : data.string.p2accderivationlhscontent1,
						fractionnumerator : [
							{
								/*notifyuserdata : "notifyuser",*/ /*this flag activates the pulse notification*/
								fractionnumeratordata : data.string.p2accderivationrhsnumerator1part1,
							}
						],
						fractiondenominatordata : data.string.p2accderivationrhsdenominator1,
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.p2accelerationformulatext,
			}
		],
		derivationblocknoreplacement : true,
		derivationblock : [
			{
				
				derivationprefixpara : [
					data.string.p2accderivationprefix2,
				],
				derivationformula : [
					{
						formuladditionalclass : "fornumeratortextstyle",
						lhscontent : data.string.p2accderivationlhscontent2,
						fractionnumerator : [
							{
								fractionnumeratordata : data.string.p2accderivationrhsnumerator2,
							}
						],
					}
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.p2accelerationformulatext,
			}
		],
		derivationblocknoreplacement : true,
		derivationblock : [
			{
				derivationprefixpara : [
					data.string.p2accderivationprefix2,
				],
				derivationformula : [
					{
						formuladditionalclass : "fornumeratortextstyle",
						lhscontent : data.string.p2accderivationlhscontent2,
						fractionnumerator : [
							{
								fractionnumeratordata : data.string.p2accderivationrhsnumerator2,
							}
						],
					}
				],
				derivationsuffixpara : [
					data.string.p2letusreplace,
				]
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.p2accelerationformulatext,
			}
		],
		derivationblocknoreplacement : true,
		derivationblock : [
			{
				derivationformula : [
					{
						formulaadditionalclass : "alignformulaleft",
						lhscontent : data.string.p2accderivationlhscontent1,
						fractionnumerator : [
							{
								notifyuserdata : "notifyuser", /*this flag activates the pulse notification*/
								fractionnumeratordata : data.string.p2accderivationrhsnumerator1part1,
							}
						],
						fractiondenominatordata : data.string.p2accderivationrhsdenominator1,
					}
				],
				replacethispara : data.string.p2accderivationlhscontent2+" = "+data.string.p2accderivationrhsnumerator2,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.p2accelerationformulatext,
			}
		],
		derivationblocknoreplacement : true,
		derivationblock : [
			{
				
				derivationprefixpara : [
					data.string.p2accderivationprefix3,
				],
				derivationformula : [
					{
						lhscontent : data.string.p2accderivationlhscontent3,
						fractionnumerator : [
							{
								/*notifyuserdata : "notifyuser",*/ /*this flag activates the pulse notification*/
								fractionnumeratordata : data.string.p2accderivationrhsnumerator3,
							}
						],
						fractiondenominatordata : data.string.p2accderivationrhsdenominator3,
					}
				]
			}
		]
	},
	{	
		headerblock : [
			{
				textclass : "headertextstyle formulaheaderoverridestyle",
				textdata  : data.string.accelerationtext,
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass : "onlyparatextstyle",
				textdata  : data.string.p2accelerationunit,
			}
		]
	},
	{	
		contentblocknocenteradjust : true,
		contentblockadditionalclass : "coverboardbg1",
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass : "simpleparatextstyle simpleparatextsomemargintop",
				textdata  : data.string.p2text5,
			}
		]
	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("expobservationcontent", $("#expobservationcontent-partial").html());
	 Handlebars.registerPartial("derivationcontent", $("#derivationcontent-partial").html());
	 Handlebars.registerPartial("accelerationexplaincontent", $("#accelerationexplaincontent-partial").html());
	 
	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true 
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;
				
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {	
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/				
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	 /**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
		
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $expobservationblock = $board.find("div.expobservationblock");
		var $derivationblock = $board.find("div.derivationblock");

		//if observation slide is present it has length at least 1 else 0
		if($expobservationblock.length > 0){
			$nextBtn.css('display', 'none');

			$instructiontext     = $expobservationblock.find("p.instructiontext");
			$currentvelocityinfo = $expobservationblock.find("p.currentvelocityinfo");
			$currentvelocity     = $currentvelocityinfo.find("span.currentvelocity");

			var s = Snap(".svgcontainer");
			Snap.load(imgpath + "acceleration_car.svg", function (f) {  
	    		s.append(f);

	    		$("#decoycar").on('click', function() {
	    			$instructiontext.css('opacity', '0');
	    			$currentvelocityinfo.css('opacity', '1');
	    			$(this).css('opacity', '0');
	    			$("#redcar").css('opacity', '1');
	    		});

	    		$("#hillcarmove2").on('beginEvent', function() {
	    			$currentvelocity.html(data.string.p2velocityabovehill);
	    		});

	    		$("#hillcarmove3").on('beginEvent', function() {
	    			$currentvelocity.html(data.string.p2velocitydownhill);
	    		});

	    		$("#hillcarmove4").on('beginEvent', function() {
	    			$currentvelocity.html(data.string.p2velocitylastpath);
	    		});

	    		$("#hillcarmove4").on('endEvent', function() {
	    			$instructiontext.css('opacity', '1');
	    			$currentvelocityinfo.css('opacity', '0');
	    			$currentvelocity.html(data.string.p2currentvelocity);
	    			$instructiontext.html(data.string.p2instructiontextpart2);
	    			$("#redcar").css('opacity', '0');
	    			$("#decoycar").css('opacity', '1');
	    			$nextBtn.show(0);
	    		});
    		});
		}

		// if derivation block
		if($derivationblock.length > 0){		

			var $replaceinto = $derivationblock.find("#replaceinto");

			// if only there is replacement action we go for the following
			if($replaceinto.length > 0){
				$nextBtn.css('display', 'none');
				notifyuser($derivationblock);
				
				var $replacethis = $derivationblock.find(".replacethis");

				$replaceinto.one('click', function() {			
					$replaceinto.attr("data-replace","true");
					$replaceinto.html(data.string.p2accderivationrhsnumerator2);
					$replacethis.css('display', 'none');
					$nextBtn.show(0);
				});
			}
		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
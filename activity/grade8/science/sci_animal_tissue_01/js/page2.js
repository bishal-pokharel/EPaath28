var imgpath = $ref+"/images/page2/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
        contentnocenteradjust:true,
        uppertextblock:[
            {
                textclass:"bigheadingtextstyle customheadingstyle",
                textdata: data.string.textEpi
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass:"para1 cssfadein",
                textdata: data.string.p2text1
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                    	imgclass:"halfimg",
                        imgsrc: imgpath+"man.svg",
                    }
                ]
            }
        ]
    },
    {
        contentnocenteradjust:true,
        uppertextblock : [
            {
                textclass : "functionheadingstyle",
                textdata : data.string.p2text2,
            }
        ],
        imageblock : [
            {
                imagestoshow : [
                    {
                        imgclass : "rightimgstyle",
                        imgsrc : imgpath+"ciliated-epithelium.png",
                    }
                ],
            }
        ],
        lowertextblock : [
            {
                textclass : "sidepara",
                textdata : data.string.p2text3,
            }
        ],
    },
    {
        contentnocenteradjust:true,
        uppertextblock : [
            {
                textclass : "functionheadingstyle",
                textdata : data.string.p2text2,
            }
        ],
        imageblock : [
            {
                imagestoshow : [
                    {
                        imgclass : "rightimgstyle",
                        imgsrc : imgpath+"glandular-epithelium.png",
                    }
                ],
            }
        ],
        lowertextblock : [
            {
                textclass : "sidepara",
                textdata : data.string.p2text3,
            },
            {
                textclass : "sidepara2",
                textdata : data.string.p2text4,
            }
        ],
    },
    {
        contentnocenteradjust:true,
        uppertextblock : [
            {
                textclass : "functionheadingstyle",
                textdata : data.string.p2text2,
            }
        ],
        imageblock : [
            {
                imagestoshow : [
                    {
                        imgclass : "rightimgstyle",
                        imgsrc : imgpath+"cubical-epithelium.png",
                    }
                ],
            }
        ],
        lowertextblock : [
            {
                textclass : "sidepara",
                textdata : data.string.p2text3,
            },
            {
                textclass : "sidepara2",
                textdata : data.string.p2text4,
            },
            {
                textclass : "sidepara3",
                textdata : data.string.p2text5,
            }
        ],
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.p2text6,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                	{
						imagelabelclass: "",
                		imagelabeldata: data.string.spacecharacter
                	}
                ]
            },
            {
                imagestoshow:[
                	{
                        imgclass:"type1 cssfadein",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                	{
                		imagelabelclass: "label1 cssfadein",
                		imagelabeldata: data.string.textCiliated
                	}
                ]
            },
            {
                imagestoshow:[
					{
                        imgclass:"type2 cssfadein2",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                	{
                		imagelabelclass: "label2 cssfadein2",
                		imagelabeldata: data.string.textStratified
                	}
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 cssfadein3",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                	{
                		imagelabelclass: "label3 cssfadein3",
                		imagelabeldata: data.string.textCuboidal
                	}
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                	{
                		imagelabelclass: "label4",
                		imagelabeldata: data.string.textPavement
                	}
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                	{
                		imagelabelclass: "label5",
                		imagelabeldata: data.string.textGlandular
                	}
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                	{
                		imagelabelclass: "label6",
                		imagelabeldata: data.string.textColumnar
                	}
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.p2text6_1,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure cssbordercolor",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textCiliated
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara",
                textdata: data.string.p2text7
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "defpara3",
                textdata: data.string.p2text10
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textCiliated
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                    {
                        imgclass:"bottomimg",
                        imgsrc: imgpath+"ciliated_label.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "cili1",
                        imagelabeldata: data.string.p2cap1
                    },
                    {
                        imagelabelclass: "cili2",
                        imagelabeldata: data.string.p2cap2
                    },
                    {
                        imagelabelclass: "cili3",
                        imagelabeldata: data.string.p2cap4
                    },
                    {
                        imagelabelclass: "cili4",
                        imagelabeldata: data.string.textCilia
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                datahighlightflag:true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara2",
                textdata: data.string.p2text8
            }
        ]
    },
    {
         headerblock:[
            {
                textclass: "titlesstyle customcssforheader",
                textdata: data.string.textCiliated
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text9
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                    {
                        imgclass:"humanimg",
                        imgsrc: imgpath+"diy/human_body_parts.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop1",
                        dropelementdata : "uterine tubes",
                    },
                    {
                        dropclass:"drop2",
                        dropelementdata : "nasal passage",
                    },
                    {
                        dropclass:"drop3",
                        dropelementdata : "respiratory tract",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "uterine tubes",
                        dragelementText : data.string.drop_options1,
                    },
                    {
                        dragelementId : "nasal passage",
                        dragelementText : data.string.drop_options2,
                    },
                    {
                        dragelementId : "respiratory tract",
                        dragelementText : data.string.drop_options3,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.textTypes,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure customborder cssborderhide",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure ",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure cssbordershow",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textStratified
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                textclass: "defpara",
                textdata: data.string.p2text11
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                datahighlightflag:true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara3",
                textdata: data.string.p2text13
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textStratified
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                    {
                        imgclass:"bottomimganim",
                        imgsrc: imgpath+"stratified-anim.gif"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                textclass: "defpara2",
                textdata: data.string.p2text12
            }
        ]
    },
    {
         headerblock:[
            {
                textclass: "titlesstyle customcssforheader",
                textdata: data.string.textStratified
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text14
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                    {
                        imgclass:"humanimg2",
                        imgsrc: imgpath+"diy/human_body_parts2.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop4",
                        dropelementdata : "Nails",
                    },
                    {
                        dropclass:"drop5",
                        dropelementdata : "Hair",
                    },
                    {
                        dropclass:"drop6",
                        dropelementdata : "Conjuctiva of eyes",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "Nails",
                        dragelementText : data.string.drop2_options1,
                    },
                    {
                        dragelementId : "Hair",
                        dragelementText : data.string.drop2_options2,
                    },
                    {
                        dragelementId : "Conjuctiva of eyes",
                        dragelementText : data.string.drop2_options3,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.textTypes,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure ",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure customborder cssborderhide",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure cssbordershow",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textCuboidal
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara",
                textdata: data.string.p2text15
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara3",
                textdata: data.string.p2text17
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textCuboidal
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    },
                    {
                        imgclass:"bottomimg2",
                        imgsrc: imgpath+"cuboidal-Epithelium.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "imglabel1",
                        imagelabeldata: data.string.p2cap1
                    },
                    {
                        imagelabelclass: "imglabel2",
                        imagelabeldata: data.string.p2cap2
                    },
                    {
                        imagelabelclass: "imglabel3",
                        imagelabeldata: data.string.p2cap4
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                textclass: "defpara2",
                textdata: data.string.p2text16
            }
        ]
    },
    {
         headerblock:[
            {
                textclass: "titlesstyle customcssforheader",
                textdata: data.string.textCuboidal
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text18
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    },
                    {
                        imgclass:"humanimg2",
                        imgsrc: imgpath+"diy/human_body_parts3.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop4",
                        dropelementdata : "Uterus",
                    },
                    {
                        dropclass:"drop8",
                        dropelementdata : "Liver",
                    },
                    {
                        dropclass:"drop7",
                        dropelementdata : "Sweat gland",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "Uterus",
                        dragelementText : data.string.drop3_options3,
                    },
                    {
                        dragelementId : "Liver",
                        dragelementText : data.string.drop3_options2,
                    },
                    {
                        dragelementId : "Sweat gland",
                        dragelementText : data.string.drop3_options1,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.textTypes,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure ",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure customborder cssborderhide ",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure cssbordershow",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle2 positionchangeanim",
                textdata: data.string.textPavement
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara",
                textdata: data.string.p2text19
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "defpara3",
                textdata: data.string.p2text21
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle2 positionchangeanim",
                textdata: data.string.textPavement
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    },
                    {
                        imgclass:"bottomimg2",
                        imgsrc: imgpath+"pavementSquamous-Epithelium02.png"
                    }
                ],
                imagelabels:[
                    {
                        imagelabelclass: "imglabel4",
                        imagelabeldata: data.string.p2cap1
                    },
                    {
                        imagelabelclass: "imglabel5",
                        imagelabeldata: data.string.p2cap5
                    },
                    {
                        imagelabelclass: "imglabel6",
                        imagelabeldata: data.string.p2cap6
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "defpara2",
                textdata: data.string.p2text20
            }
        ]
    },
    {
         headerblock:[
            {
                textclass: "titlesstyle2 customcssforheader",
                textdata: data.string.textPavement
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text22
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    },
                    {
                        imgclass:"humanimg",
                        imgsrc: imgpath+"diy/human_body_parts4.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop9",
                        dropelementdata : "Skin epidermis",
                    },
                    {
                        dropclass:"drop2",
                        dropelementdata : "Lining of the mouth",
                    },
                    {
                        dropclass:"drop11",
                        dropelementdata : "Blood vessels",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "Skin epidermis",
                        dragelementText : data.string.drop4_options2,
                    },
                    {
                        dragelementId : "Lining of the mouth",
                        dragelementText : data.string.drop4_options1,
                    },
                    {
                        dragelementId : "Blood vessels",
                        dragelementText : data.string.drop4_options3,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.textTypes,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure ",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure customborder cssborderhide",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure cssbordershow",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textGlandular
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                textclass: "defpara",
                textdata: data.string.p2text23
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "defpara3",
                textdata: data.string.p2text25
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textGlandular
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    },
                    {
                        imgclass:"bottomimg",
                        imgsrc: imgpath+"glandular-anim.gif"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                textclass: "defpara2",
                textdata: data.string.p2text24
            }
        ]
    },
    {
         headerblock:[
            {
                textclass: "titlesstyle customcssforheader",
                textdata: data.string.textGlandular
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text26
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    },
                    {
                        imgclass:"humanimg",
                        imgsrc: imgpath+"diy/human_body_parts5.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop12",
                        dropelementdata : "Liver",
                    },
                    {
                        dropclass:"drop2",
                        dropelementdata : "Mucuous glands",
                    },
                    {
                        dropclass:"drop10",
                        dropelementdata : "Mammary glands",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "Liver",
                        dragelementText : data.string.drop3_options2,
                    },
                    {
                        dragelementId : "Mucuous glands",
                        dragelementText : data.string.drop5_options2,
                    },
                    {
                        dragelementId : "Mammary glands",
                        dragelementText : data.string.drop5_options1,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        headerblock:[
            {
                textclass : "headertextstyle customcss",
                textdata   : data.string.textTypes,
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"svgimg",
                        imgsrc: imgpath+"animal_body02.svg"
                    }
                ],
                imagelabel:[
                    {
                        imagelabelclass: "",
                        imagelabeldata: data.string.spacecharacter
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type1 showfigure",
                        imgsrc: imgpath+"ciliated-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label1 showfigure ",
                        imagelabeldata: data.string.textCiliated
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type2 showfigure",
                        imgsrc: imgpath+"stratified-epithelium.png"
                    },
                ],
                imagelabels:[
                    {
                        imagelabelclass: "label2 showfigure",
                        imagelabeldata: data.string.textStratified
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type3 showfigure",
                        imgsrc: imgpath+"cubical-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label3 showfigure",
                        imagelabeldata: data.string.textCuboidal
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type4 showfigure",
                        imgsrc: imgpath+"squamous-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label4 showfigure",
                        imagelabeldata: data.string.textPavement
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type5 showfigure customborder cssborderhide",
                        imgsrc: imgpath+"glandular-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label5 showfigure",
                        imagelabeldata: data.string.textGlandular
                    }
                ]
            },
            {
                imagestoshow:[
                    {
                        imgclass:"type6 showfigure cssbordershow",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ],
                 imagelabels:[
                    {
                        imagelabelclass: "label6 showfigure",
                        imagelabeldata: data.string.textColumnar
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "lets",
                textdata : data.string.textlets
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textColumnar
            }
        ],
        contentnocenteradjust:true,
        uppertextblock:[
            {
                datahighlightflag: true,
                datahighlightcustomclass: "bigfont",
                textclass: "defpara",
                textdata: data.string.p2text27
            },
            {
                textclass: "structure",
                textdata: data.string.textfunction
            }
        ],
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "defpara3",
                textdata: data.string.p2text29
            }
        ]
    },
    {
        headerblock:[
            {
                textclass: "titlesstyle positionchangeanim",
                textdata: data.string.textColumnar
            }
        ],
        contentnocenteradjust:true,

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    },
                    {
                        imgclass:"bottomimg",
                        imgsrc: imgpath+"columnar.png"
                    }
                ]
            }
        ],
        lowertextblock:[
            {
                textclass: "function cssfadein",
                textdata: data.string.textstructure
            },
            {
                textclass: "defpara2",
                textdata: data.string.p2text28
            }
        ]
    },
        {
         headerblock:[
            {
                textclass: "titlesstyle customcssforheader",
                textdata: data.string.textColumnar
            },
            {
                datahighlightflag: true,
                datahighlightcustomclass : "bigfont",
                textclass: "quespara",
                textdata: data.string.p2text30
            }
        ],
        contentnocenteradjust:true,
        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"sideimg",
                        imgsrc: imgpath+"columnar-epithelium.png"
                    },
                    {
                        imgclass:"humanimg2",
                        imgsrc: imgpath+"diy/human_body_parts6.svg",
                    }
                ]
            }
        ],
        customdiyblock:[
            {
                dropareaelement :[
                    {
                        dropclass:"drop13",
                        dropelementdata : "Reproductive organ",
                    },
                    {
                        dropclass:"drop14",
                        dropelementdata : "Stomach",
                    },
                    {
                        dropclass:"drop15",
                        dropelementdata : "Intestine",
                    },
                ],
                dragareaelement :[
                    {
                        dragelementId : "Reproductive organ",
                        dragelementText : data.string.drop6_options1,
                    },
                    {
                        dragelementId : "Stomach",
                        dragelementText : data.string.drop6_options2,
                    },
                    {
                        dragelementId : "Intestine",
                        dragelementText : data.string.drop6_options3,
                    }
                ],
                conclusionTextData : data.string.textGood
            }
        ]
    },
    {
        uppertextblock:[
            {
                textclass: "paratextstyle",
                textdata: data.string.p2text31
            }
        ]
    }
];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html());
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
     Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 Handlebars.registerPartial("customdiycontent", $("#customdiycontent-partial").html());
     Handlebars.registerPartial("twohalvestitledetailcontent", $("#twohalvestitledetailcontent-partial").html());
		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

	 /*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);
        if (countNext==4) {
            $nextBtn.hide(0);
        };

		$(".type3").on(animationend, function(){
			$(".type4, .label4").addClass("cssfadein");
			$(".type5, .label5").addClass("cssfadein2");
			$(".type6, .label6").addClass("cssfadein3");
		});

		$(".type6").on(animationend, function(){
            $(".type1").addClass("cssbordercolor");
            $nextBtn.show(0);
		});

        if (countNext==5) {
            $(".svgimg").addClass("cssfadeout");
        };

        $(".svgimg").on(animationend, function(){
            $(".type2, .type3, .type4, .type5, .type6, .label2, .label3, .label4, .label5, .label6").removeClass("showfigure");
            $(".type1").addClass("centerposition");
            $(".label1").addClass("textposition");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });

        $(".titlesstyle, .titlesstyle2").on(animationend, function(){
            $(".defpara").addClass("cssfadein");
        });

        $(".defpara").on(animationend, function(){
            $(".structure").addClass("cssfadein");
        });

        $(".structure").on(animationend, function(){
            $(".defpara3").addClass("cssfadein");
            $nextBtn.delay(2000).show(0);
        });

        $(".function").on(animationend, function(){
            $(".bottomimg, .bottomimganim, .bottomimg2, .imglabel1, .imglabel2, .imglabel3, .cili1, .cili2, .cili3, .cili4, .imglabel4, .imglabel5, .imglabel6").addClass("cssfadein2");
            $(".defpara2").addClass("cssfadein");
            $nextBtn.delay(2000).show(0);
        });

        if (countNext==8 || countNext==12 || countNext==16 || countNext==20 || countNext==24 ||countNext==28) {
            $nextBtn.hide(0);
        };

       var $recognizeorgan = $board.children('div.recognizeorgan');
        var $draggableCollector = $('.droppablecollector');
        var $droppables = $(".droppablecollector > p");
        var $draggables = $(".draggablecollector > p");

        var dropcount = 0;
        var totaldroprequired = $draggables.length;

        $draggables.draggable({containment: $recognizeorgan, revert:"invalid"});

        /*eureka droppable this is nice use of droppable please see the use*/
             $droppables.droppable({ tolerance: "pointer",
        accept : function(dropElem){
            //dropElem was the dropped element, return true or false to accept/refuse
            if($(this).attr('data-dropelementcaptiondata') === dropElem.attr("id")){
                return true;
            }
        },
        drop : function (event, ui) {
            $(this).html($(ui.draggable).html());
            $(this).addClass('droppableafterdrop');
            $(ui.draggable).css('display', 'none');
                if(++dropcount == totaldroprequired){
                    $(ui.draggable).siblings('h4').show(0);
                    $nextBtn.show(0);
                }
            }
        });
	}

    function mainpage() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        $(".cssbordershow").on(animationend, function(){
            $(".svgimg").addClass("cssfadeout");
        });

        $(".svgimg").on(animationend, function(){
            $(".type1, .type3, .type4, .type5, .type6, .label1, .label3, .label4, .label5, .label6").removeClass("showfigure");
            $(".type2").addClass("centerposition");
            $(".label2").addClass("textposition");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });
    }

    function mainpage2() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        $(".cssbordershow").on(animationend, function(){
            $(".svgimg").addClass("cssfadeout");
        });

        $(".svgimg").on(animationend, function(){
            $(".type1, .type2, .type4, .type5, .type6, .label1, .label2, .label4, .label5, .label6").removeClass("showfigure");
            $(".type3").addClass("centerposition");
            $(".label3").addClass("textposition");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });
    }

    function mainpage3() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        $(".cssbordershow").on(animationend, function(){
            $(".svgimg").addClass("cssfadeout");
        });

        $(".svgimg").on(animationend, function(){
            $(".type1, .type2, .type3, .type5, .type6, .label1, .label2, .label3, .label5, .label6").removeClass("showfigure");
            $(".type4").addClass("centerposition");
            $(".label4").addClass("textposition2");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });
    }

    function mainpage4() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        $(".cssbordershow").on(animationend, function(){
            $(".svgimg").addClass("cssfadeout");
        });

        $(".svgimg").on(animationend, function(){
            $(".type1, .type2, .type3, .type4, .type6, .label1, .label2, .label3, .label4, .label6").removeClass("showfigure");
            $(".type5").addClass("centerposition");
            $(".label5").addClass("textposition");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });
    }

    function mainpage5() {
        var source = $("#general-template").html();
        var template = Handlebars.compile(source);
        var html = template(content[countNext]);
        $board.html(html);
        $nextBtn.hide(0);

        // highlight any text inside board div with datahighlightflag set true
        texthighlight($board);

        $(".cssbordershow").on(animationend, function(){
            $(".svgimg").addClass("cssfadeout");
        });

        $(".svgimg").on(animationend, function(){
            $(".type1, .type2, .type3, .type5, .type4, .label1, .label2, .label3, .label5, .label4").removeClass("showfigure");
            $(".type6").addClass("centerposition");
            $(".label6").addClass("textposition");
            $(".lets").addClass("cssfadein");
            $nextBtn.delay("1000").show(0);
        });
    }

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
            case 0 : generaltemplate(); break;
            case 9 : mainpage(); break;
            case 13 : mainpage2(); break;
            case 17 : mainpage3(); break;
            case 21 : mainpage4(); break;
            case 25 : mainpage5(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

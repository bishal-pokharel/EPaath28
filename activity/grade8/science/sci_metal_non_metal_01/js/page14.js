var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page14/";

var content=[


	
	//slide0
	
	{	
		contentblockadditionalclass : "contentwithbg",
		uppertextblock : [
			{
				textclass         : "inset-text-effect",
				textdata          : data.string.p14text1,
			}
		],
	},
	
	//slide1
	
	{	headerblock : [
			{
				textclass : "headertextstyle",
				textdata   : data.string.p14text1,
			}
		],
		
		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "test",
						imgsrc: imgpath+"metalloids.svg",

					}
				],
			}
		],
		
			
		lowertextblockadditionalclass: "periodictabletextstyle",

		lowertextblock:[
				{
					textdata: data.string.p14text2,
				}
			],
	
	},
	
	
	
	
	
	//slide2
	
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p14text1,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p14text3,
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"silicon.png",		
					}
				]
			}
		]
	},
	
	
	//slide3
	
	{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p14text1,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p14text3,
			},
			
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p14text4,
			}
			
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"conductivity.png",		
					}
				]
			}
		]
	},
	
	//slide4
	
	
		{
		headerblock : [
			{
				textclass : "sideheadingstyle",
				textdata   : data.string.p14text1,
			}
		],
		contentnocenteradjust:true,
		middletextblock:[
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara1",
				textdata : data.string.p14text3,
			},
			
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara2",
				textdata : data.string.p14text4,
			},
			
			{
				datahighlightcustomclass : "notehighlight",
				datahighlightflag: true,
				textclass: "middlepara1 new",
				textdata : data.string.p14text5,
			}
			
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "sidesingleimg",
						imgsrc: imgpath+"brittle.png",		
					}
				]
			}
		]
	},
	
	
	
	//slide5
	
	
	
		
	
	//slide6
	
	
	
		
	
	
	//slide7
	

	
	
	//slide8
	

	
	
	//slide8-1
	
		{	headerblock : [
			{
				textclass : "headertextstyle",
				textdata   : data.string.p14text1,
			}
		],
		
		imageblock:[
			{
				imagestoshow: [
					{
						imgclass : "test",
						imgsrc: imgpath+"metalloids.svg",

					}
				],
			}
		],
		
			
		lowertextblockadditionalclass: "periodictabletextstyle",

		lowertextblock:[
				{
					textdata: data.string.p14text6,
				}
			],
	
	},
	
	
	
	
	
	
	//slide9
	
	
	
	
	//slide10
	
	
	
	
	//slide11
	
	
	
	
	
	
	
	//slide13
	
	
	
	
	
	//slide15
	
	
	
	
	//slide16
	
	
	
	//
	

	
	
	
	
	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 

		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true 
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class
				
				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;
					
				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {	
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/				
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.lessonEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// $nextBtn.hide(0);
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);
		if (countNext==1) {
			$(".fiveimgtogether").addClass("poppop");
			$(".lable1, .lable2, .lable3, .lable4, .lable5").delay(2000).fadeIn(200);
		};

		if (countNext == 5) {
			$(".threeimgpara").show(0);
			$(".h1, .h2, .h3").addClass("ion_popup");
		}

		if (countNext ==11) {
			$(".acidtxt").show(0);
		};
		
		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}


	/*=================================================
	=            svg template function            =
	=================================================*/		
		function svg01template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg02template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg03template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		
		function svg04template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg05template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg06template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg07template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
   
		}
		
		function svg08template(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$(".explainpropertycontainer").hide(0);
		
		$(".propertylistcontainer ").addClass("fullwidth");
   
		}

function svgtemplate1(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $svgcontainer = $board.children('div.svgcontainer'); 	
	
		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"table.svg", function (f) {    
    		s.append(f);

    	  		// snap objects
    		var acidcontainer    = s.select("#acidcontainer");
    		var basedrop        = s.select("#basedrop");
    		var oh_ion1     = s.select("#oh_ion1");
    		var oh_ion2     = s.select("#oh_ion2");
    		var oh_ion3     = s.select("#oh_ion3");
    		var oh_ion4     = s.select("#oh_ion4");
    		var water_bubble     = s.select("#water_bubble");

    		// jquery objects and js variables
    		var $svg      = $svgcontainer.children('svg');

    		var $drop     = $svg.children("[id$='drop']");
    		var $basedrop = $svg.children('#basedrop');

    		var $ion = $svg.children("[id$='ion']");
    		var $oh_ion1 = $svg.children('#oh_ion1');
    		var $oh_ion2 = $svg.children('#oh_ion2');
    		var $oh_ion3 = $svg.children('#oh_ion3');
    		var $oh_ion4 = $svg.children('#oh_ion4');
    		var $water = $svg.children("[id$='water']");
    		var $water_bubble = $svg.children('#water_bubble');
    		
    			$(".clickmebase").click(function () {
    				$(this).hide(0);
    				basedrop.addClass("down");
  					oh_ion1.addClass("ion_move");
  					oh_ion2.addClass("ion_move");
  					oh_ion3.addClass("ion_move");
  					oh_ion4.addClass("ion_move");

  					water_bubble.addClass("H2O");
  					$(".acidtxt").delay(2000).fadeIn(1000);
  					$nextBtn.delay(2500).fadeIn(1000);
    			});
 		});
	// }
}


function svgtemplate2(){
		var source   = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		// notifyuser($board);
		var $svgwrapper = $board.children('div.svgwrapper');
		var $soapsvg = $board.children('div.soapsvg'); 

		$nextBtn.delay(1800).fadeIn(1000);
		// if there is a svg content
		// if($svgwrapper.length > 0){
			var s = Snap(".soapsvg");
			Snap.load(imgpath+"soap.svg", function (f) {    
    		s.append(f);

    	  		// snap objects
    		var soapbody = s.select("#soapbody");
    		var soap_bubbles = s.select("#soap_bubbles");

    		// jquery objects and js variables
    		var $svg      = $soapsvg.children('soapsvg');

    		var $soap     = $svg.children("[id$='soap']");
    		var $soapbody = $svg.children('#soapbody');

    		var $bubble = $svg.children("[id$='bubble']");
    		var $soap_bubbles = $svg.children('#soap_bubbles');   
 		});
	// }
}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		

		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate();break;
			case 2 : generaltemplate(); break;
			
			case 3 : generaltemplate(); break;
			case 4 : generaltemplate(); break;
			case 5 : generaltemplate(); break;
			case 6 : generaltemplate(); break;
			
			case 7 : generaltemplate(); break;
			case 8 : generaltemplate(); break;
			case 9 : generaltemplate(); break;
			case 10 : svg01template(); break;
			case 11 :svg02template(); break;
			case 12 :svg03template(); break;
			case 13 :svg04template(); break;
			case 14 :svg05template(); break;
			case 15 :svg06template(); break;
			case 16 :svg07template(); break;
			case 17 :svg08template(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
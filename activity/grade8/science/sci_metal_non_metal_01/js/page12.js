var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref + "/images/page12/";

var content = [



  //slide0

  {
    contentblockadditionalclass: "contentwithbg",
    uppertextblock: [{
      textclass: "inset-text-effect",
      textdata: data.string.p12text1,
    }],
  },

  //slide1

  {
    headerblock: [{
      textclass: "headertextstyle",
      textdata: data.string.p12text1,
    }],

    imageblock: [{
      imagestoshow: [{
        imgclass: "test",
        imgsrc: imgpath + "nonmetals.svg",

      }],
    }],


    lowertextblockadditionalclass: "periodictabletextstyle",

    lowertextblock: [{
      textdata: data.string.p12text2,
    }],

  },





  //slide2

  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
      datahighlightcustomclass: "notehighlight",
      datahighlightflag: true,
      textclass: "middlepara1",
      textdata: data.string.p12text3,
    }],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg",
        imgsrc: imgpath + "state.png",
      }]
    }]
  },


  //slide3

  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1",
        textdata: data.string.p12text3,
      },

      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara2",
        textdata: data.string.p12text4,
      }

    ],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg",
        imgsrc: imgpath + "gases.png",
      }]
    }]
  },

  //slide4


  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1",
        textdata: data.string.p12text3,
      },

      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara2",
        textdata: data.string.p12text4,
      },

      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1 new",
        textdata: data.string.p12text5,
      }

    ],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg",
        imgsrc: imgpath + "shiny.png",
      }]
    }]
  },



  //slide5


  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1",
        textdata: data.string.p12text6,
      },





    ],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg thermo",
        imgsrc: imgpath + "melting.gif",
      }]
    }]
  },




  //slide6

  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1",
        textdata: data.string.p12text6,
      },
      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara2",
        textdata: data.string.p12text7,
      }





    ],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg",
        imgsrc: imgpath + "nonductile.png",
      }]
    }]
  },


  //slide7


  {
    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text1,
    }],
    contentnocenteradjust: true,
    middletextblock: [{
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1",
        textdata: data.string.p12text6,
      },

      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara2",
        textdata: data.string.p12text7,
      },

      {
        datahighlightcustomclass: "notehighlight",
        datahighlightflag: true,
        textclass: "middlepara1 new",
        textdata: data.string.p12text8,
      }





    ],
    imageblock: [{
      imagestoshow: [{
        imgclass: "sidesingleimg",
        imgsrc: imgpath + "examples.png",
      }]
    }]
  },


  //slide8

  {
    uppertextblock: [{
      textclass: "periodictabletextstyle",
      textdata: data.string.p12text81,
    }],
  },


  //slide9

  {
    uppertextblock: [{
      textclass: "polybgtextstyle",
      textdata: data.string.p12text82,
    }],
  },



  //carbon start


  //slide10


  {

    headerblock: [{
      textclass: "sideheadingstyle",
      textdata: data.string.p12text9,
    }],

    imageblock: [{
      imagestoshow: [{
        imgclass: "mainimg",
        imgsrc: imgpath + "carbon.svg",
      }]
    }],

    lowertextblockadditionalclass: "periodictabletextstyle gold",

    lowertextblock: [{
      datahighlightcustomclass: "notehighlight goldtext",
      datahighlightflag: true,
      textdata: data.string.p12text10,
    }],



  },


  //slide8-1

  {
    lowertextblockadditionalclass: "middlecontent",
    uppertextblock: [{
      textclass: "paratextstyle middlecontent",
      textdata: data.string.p12text37,
    }],
  },






  //slide9


  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara",
        textdata: data.string.p12text11,
      },
      {
        imagestoshow: [{
          imgclass: "acid_small",
          imgsrc: imgpath + "carbon.png"
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12,
      propertyexplainimgsrc: imgpath + "coal.png",
      imgaddtionalclass: "coal",
      mainpropertytext: data.string.p12text13,
      propertyexplaindata: data.string.p12text14,
      propertycontainerlist: [{
        propertylistdata: data.string.p12text15,
      }, ]
    }]
  },


  //slide10


  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same
      propertyexplainimgsrc: imgpath + "diamond.png",
      imgaddtionalclass: "diamond",
      mainpropertytext: data.string.p12text16, //subproperty
      propertyexplaindata: data.string.p12text17, //sub-detail property
      propertycontainerlist: [{
          propertylistdata: data.string.p12text15, //full property
        },

        {
          propertylistdata: data.string.p12text18, //full property
        },
      ]
    }]
  },



  //slide11


  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same
      propertyexplainimgsrc: imgpath + "lead.png",
      imgaddtionalclass: "lead",
      mainpropertytext: data.string.p12text19, //subproperty
      propertyexplaindata: data.string.p12text20, //sub-detail property
      propertycontainerlist: [{
          propertylistdata: data.string.p12text15, //full property
        },

        {
          propertylistdata: data.string.p12text18, //full property
        },

        {
          propertylistdata: data.string.p12text21, //full property
        },
      ]
    }]
  },




  //slide12


  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same
      propertyexplainimgsrc: imgpath + "conductivity.png",
      imgaddtionalclass: "conductivity",
      mainpropertytext: data.string.p12text22, //subproperty
      propertyexplaindata: data.string.p12text23, //sub-detail property
      propertycontainerlist: [


        {
          propertylistdata: data.string.p12text24, //full property
        },
      ]
    }]
  },


  //slide13



  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same
      propertyexplainimgsrc: imgpath + "brittle.png",
      imgaddtionalclass: "brittle",
      mainpropertytext: data.string.p12text25, //subproperty
      propertyexplaindata: data.string.p12text26, //sub-detail property
      propertycontainerlist: [


        {
          propertylistdata: data.string.p12text24, //full property
        },

        {
          propertylistdata: data.string.p12text27, //full property
        },
      ]
    }]
  },


  //slide14

  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],

    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same
      propertyexplainimgsrc: imgpath + "phase.png",
      imgaddtionalclass: "phase",
      mainpropertytext: data.string.p12text28, //subproperty
      propertyexplaindata: data.string.p12text29, //sub-detail property
      propertycontainerlist: [


        {
          propertylistdata: data.string.p12text24, //full property
        },

        {
          propertylistdata: data.string.p12text27, //full property
        },

        {
          propertylistdata: data.string.p12text30, //full property
        },
      ]
    }]
  },



  //slide15





  //slide16


  {
    typeoflayout: "propertylayout",

    headerblockwithimg: [{
        textclass: "sidepara", //same
        textdata: data.string.p12text11, //same
      },


      {
        imagestoshow: [{
          imgclass: "acid_small", //same
          imgsrc: imgpath + "carbon.png" //same
        }, ]
      }
    ],


    propertyblockadditionalclass: "widewidth",
    propertyblock: [{
      propertycontainerheadingdata: data.string.p12text12, //same


      propertycontainerlist: [


        {
          propertylistdata: data.string.p12text14, //full property
          propertylistcount: "1",
        },

        {
          propertylistdata: data.string.p12text17, //full property
          propertylistcount: "2",
        },

        {
          propertylistdata: data.string.p12text20, //full property
          propertylistcount: "3",
        },

        {
          propertylistdata: data.string.p12text23, //full property
          propertylistcount: "4",
        },

        {
          propertylistdata: data.string.p12text26, //full property
          propertylistcount: "5",
        },

        {
          propertylistdata: data.string.p12text29, //full property
          propertylistcount: "6",
        },


      ]
    }]
  },


  //







];


$(function() {
  var $board = $('.board');
  var $nextBtn = $("#activity-page-next-btn-enabled");
  var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn = $("#activity-page-refresh-btn");
  var countNext = 0;

  var $total_page = content.length;
  loadTimelineProgress($total_page, countNext + 1);

  /*==================================================
  =            Handlers and helpers Block            =
  ==================================================*/
  /*==========  register the handlebar partials first  ==========*/
  Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
  Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
  Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
  Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
  Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
  Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
  Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
  Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
  Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
  Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());


  /*===============================================
  =            data highlight function            =
  ===============================================*/
  /**

  	What it does:
  	- send an element where the function has to see
  	for data to highlight
  	- this function searches for all nodes whose
  	data-highlight element is set to true
  	-searches for # character and gives a start tag
  	;span tag here, also for @ character and replaces with
  	end tag of the respective
  	- if provided with data-highlightcustomclass value for highlight it
  	  applies the custom class or else uses parsedstring class

  	E.g: caller : texthighlight($board);
   */
  function texthighlight($highlightinside) {
    //check if $highlightinside is provided
    typeof $highlightinside !== "object" ?
      alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
      null;

    var $alltextpara = $highlightinside.find("*[data-highlight='true']");
    var stylerulename;
    var replaceinstring;
    var texthighlightstarttag;
    var texthighlightendtag = "</span>";
    if ($alltextpara.length > 0) {
      $.each($alltextpara, function(index, val) {
        /*if there is a data-highlightcustomclass attribute defined for the text element
        use that or else use default 'parsedstring'*/
        $(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
          (stylerulename = $(this).attr("data-highlightcustomclass")) :
          (stylerulename = "parsedstring");

        texthighlightstarttag = "<span class='" + stylerulename + "'>";
        replaceinstring = $(this).html();
        replaceinstring = replaceinstring.replace(/#/g, texthighlightstarttag);
        replaceinstring = replaceinstring.replace(/@/g, texthighlightendtag);
        $(this).html(replaceinstring);
      });
    }
  }
  /*=====  End of data highlight function  ======*/

  /*===============================================
   =            user notification function        =
   ===============================================*/
  /**
  	How to:
  	- First set any html element with
  		"data-usernotification='notifyuser'" attribute,
  	and "data-isclicked = ''".
  	- Then call this function to give notification
   */

  /**
  	What it does:
  	- You send an element where the function has to see
  	for data to notify user
  	- this function searches for all text nodes whose
  	data-usernotification attribute is set to notifyuser
  	- applies event handler for each of the html element which
  	 removes the notification style.
   */
  function notifyuser($notifyinside) {
    //check if $notifyinside is provided
    typeof $notifyinside !== "object" ?
      alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
      null;

    /*variable that will store the element(s) to remove notification from*/
    var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
    // if there are any notifications removal required add the event handler
    if ($allnotifications.length > 0) {
      $allnotifications.one('click', function() {
        /* Act on the event */
        $(this).attr('data-isclicked', 'clicked');
        $(this).removeAttr('data-usernotification');
      });
    }
  }
  /*=====  End of user notification function  ======*/

  /*======================================================
  =            Navigation Controller Function            =
  ======================================================*/
  /**
  	How To:
  	- Just call the navigation controller if it is to be called from except the
  	  last page of lesson
  	- If called from last page set the islastpageflag to true such that
  		footernotification is called for continue button to navigate to exercise
   */

  /**
    	What it does:
    	- If not explicitly overriden the method for navigation button
    	  controls, it shows the navigation buttons as required,
    	  according to the total count of pages and the countNext variable
    	- If for a general use it can be called from the templatecaller
    	  function
    	- Can be put anywhere in the template function as per the need, if
    	  so should be taken out from the templatecaller function
    	- If the total page number is
     */

  function navigationcontroller(islastpageflag) {
    // check if the parameter is defined and if a boolean,
    // update islastpageflag accordingly
    typeof islastpageflag === "undefined" ?
      islastpageflag = false :
      typeof islastpageflag != 'boolean' ?
      alert("NavigationController : Hi Master, please provide a boolean parameter") :
      null;

    if (countNext == 0 && $total_page != 1) {
      $nextBtn.show(0);
      $prevBtn.css('display', 'none');
    } else if ($total_page == 1) {
      $prevBtn.css('display', 'none');
      $nextBtn.css('display', 'none');

      // if lastpageflag is true
      islastpageflag ?
        ole.footerNotificationHandler.lessonEndSetNotification() :
        ole.footerNotificationHandler.pageEndSetNotification();
    } else if (countNext > 0 && countNext < $total_page - 1) {
      $nextBtn.show(0);
      $prevBtn.show(0);
    } else if (countNext == $total_page - 1) {
      $nextBtn.css('display', 'none');
      $prevBtn.show(0);

      // if lastpageflag is true
      islastpageflag ?
        ole.footerNotificationHandler.lessonEndSetNotification() :
        ole.footerNotificationHandler.pageEndSetNotification();
    }
  }
  /*=====  End of user navigation controller function  ======*/

  /*=====  End of Handlers and helpers Block  ======*/

  /*=======================================
  =            Templates Block            =
  =======================================*/
  /*=================================================
  =            general template function            =
  =================================================*/
  function generaltemplate() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    // $nextBtn.hide(0);

    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);
    if (countNext == 1) {
      $(".fiveimgtogether").addClass("poppop");
      $(".lable1, .lable2, .lable3, .lable4, .lable5").delay(2000).fadeIn(200);
    };

    if (countNext == 5) {
      $(".threeimgpara").show(0);
      $(".h1, .h2, .h3").addClass("ion_popup");
    }

    if (countNext == 11) {
      $(".acidtxt").show(0);
    };

    // find if there is linehorizontal div in the slide
    var $linehorizontal = $board.find("div.linehorizontal");
    if ($linehorizontal.length > 0) {
      $linehorizontal.attr('data-isdrawn', 'draw');
    }
  }


  /*=================================================
  =            svg template function            =
  =================================================*/
  function svg01template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg02template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg03template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }


  function svg04template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg05template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg06template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg07template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg08template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

  }

  function svg09template() {
    var source = $("#svg-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);

    $(".explainpropertycontainer").hide(0);
    $(".propertylistcontainer ").addClass("fullwidth");

  }


  function svgtemplate1() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);

    //notify user call
    // notifyuser($board);
    var $svgwrapper = $board.children('div.svgwrapper');
    var $svgcontainer = $board.children('div.svgcontainer');

    // if there is a svg content
    // if($svgwrapper.length > 0){
    var s = Snap(".svgcontainer");
    Snap.load(imgpath + "table.svg", function(f) {
      s.append(f);

      // snap objects
      var acidcontainer = s.select("#acidcontainer");
      var basedrop = s.select("#basedrop");
      var oh_ion1 = s.select("#oh_ion1");
      var oh_ion2 = s.select("#oh_ion2");
      var oh_ion3 = s.select("#oh_ion3");
      var oh_ion4 = s.select("#oh_ion4");
      var water_bubble = s.select("#water_bubble");

      // jquery objects and js variables
      var $svg = $svgcontainer.children('svg');

      var $drop = $svg.children("[id$='drop']");
      var $basedrop = $svg.children('#basedrop');

      var $ion = $svg.children("[id$='ion']");
      var $oh_ion1 = $svg.children('#oh_ion1');
      var $oh_ion2 = $svg.children('#oh_ion2');
      var $oh_ion3 = $svg.children('#oh_ion3');
      var $oh_ion4 = $svg.children('#oh_ion4');
      var $water = $svg.children("[id$='water']");
      var $water_bubble = $svg.children('#water_bubble');

      $(".clickmebase").click(function() {
        $(this).hide(0);
        basedrop.addClass("down");
        oh_ion1.addClass("ion_move");
        oh_ion2.addClass("ion_move");
        oh_ion3.addClass("ion_move");
        oh_ion4.addClass("ion_move");

        water_bubble.addClass("H2O");
        $(".acidtxt").delay(2000).fadeIn(1000);
        $nextBtn.delay(2500).fadeIn(1000);
      });
    });
    // }
  }


  function svgtemplate2() {
    var source = $("#general-template").html();
    var template = Handlebars.compile(source);
    var html = template(content[countNext]);
    $board.html(html);
    $nextBtn.hide(0);
    // highlight any text inside board div with datahighlightflag set true
    texthighlight($board);

    //notify user call
    // notifyuser($board);
    var $svgwrapper = $board.children('div.svgwrapper');
    var $soapsvg = $board.children('div.soapsvg');

    $nextBtn.delay(1800).fadeIn(1000);
    // if there is a svg content
    // if($svgwrapper.length > 0){
    var s = Snap(".soapsvg");
    Snap.load(imgpath + "soap.svg", function(f) {
      s.append(f);

      // snap objects
      var soapbody = s.select("#soapbody");
      var soap_bubbles = s.select("#soap_bubbles");

      // jquery objects and js variables
      var $svg = $soapsvg.children('soapsvg');

      var $soap = $svg.children("[id$='soap']");
      var $soapbody = $svg.children('#soapbody');

      var $bubble = $svg.children("[id$='bubble']");
      var $soap_bubbles = $svg.children('#soap_bubbles');
    });
    // }
  }


  /*=====  End of Templates Block  ======*/

  /*==================================================
  =            Templates Controller Block            =
  ==================================================*/

  /*==================================================
  =            function to call templates            =
  ==================================================*/
  /**
  	Motivation :
  	- Make a single function call that handles all the
  	  template load easier

  	How To:
  	- Update the template caller with the required templates
  	- Call template caller

  	What it does:
  	- According to value of the Global Variable countNext
  		the slide templates are updated
   */

  function templateCaller() {
    /*always hide next and previous navigation button unless
    explicitly called from inside a template*/
    $prevBtn.css('display', 'none');
    $nextBtn.css('display', 'none');

    // call navigation controller
    navigationcontroller();

    // call the template


    /*OR, call templates like this if you have more than one template
    to call*/
    switch (countNext) {
      case 0:
        generaltemplate();
        break;
      case 1:
        generaltemplate();
        break;
      case 2:
        generaltemplate();
        break;

      case 3:
        generaltemplate();
        break;
      case 4:
        generaltemplate();
        break;
      case 5:
        generaltemplate();
        break;
      case 6:
        generaltemplate();
        break;

      case 7:
        generaltemplate();
        break;
      case 8:
        generaltemplate();
        break;
      case 9:
        generaltemplate();
        break;

      case 10:
        generaltemplate();
        break;
      case 11:
        generaltemplate();
        break;
      case 12:
        svg03template();
        break;
      case 13:
        svg04template();
        break;
      case 14:
        svg05template();
        break;
      case 15:
        svg06template();
        break;
      case 16:
        svg07template();
        break;
      case 17:
        svg08template();
        break;
      case 18:
        svg09template();
        break;

      default:
        break;
    }

    //call the slide indication bar handler for pink indicators
    loadTimelineProgress($total_page, countNext + 1);

    // just for development purpose to see total slide vs current slide number
    // $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
  }

  /*this countNext variable change here is solely for development phase and
  should be commented out for deployment*/
  // countNext+=1;

  // first call to template caller
  templateCaller();

  /* navigation buttons event handlers */

  $nextBtn.on('click', function() {
    countNext++;
    templateCaller();
  });

  $refreshBtn.click(function() {
    templateCaller();
  });

  $prevBtn.on('click', function() {
    countNext--;
    templateCaller();

    /* if footerNotificationHandler pageEndSetNotification was called then on click of
    	previous slide button hide the footernotification */
    countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
  });

  /*=====  End of Templates Controller Block  ======*/
});

var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page4/";

var content=[
	{	
		typeoflayout   : "svglayout",
		svgblock : [
			{
				svgheading : [
					{
						datahighlightflag : true,
						textclass         : "svgheadingstyle",
						textdata          : data.string.raydiagramheading,
					}
				],
				objectlocationdata : data.string.p4objectlocation,
				imagecharacteristicheadingdata : data.string.imagecharacteristicheadingtext,
				imagecharacterstics : [
					{
						natureofimage : data.string.naturelocation,
						naturedetail  : data.string.p4naturelocationdetail,
					},
					{
						natureofimage : data.string.natureorientation,
						naturedetail  : data.string.p4natureorientationdetail,
					},
					{
						natureofimage : data.string.naturesize,
						naturedetail  : data.string.p4naturesizedetail,
					},
					{
						natureofimage : data.string.naturetype,
						naturedetail  : data.string.p4naturetypedetail,
					},
				],
				startbuttontext : data.string.startaniminstruction,
			}
		],
	},
	{
		testyourselfheadingdata : data.string.testyourselfheading,
		testyourselfquestiondata : data.string.p4testyourselfquestion,
		testyourselfquestion : [
			{
				imagecharacteristic : "location",
				testyourselfcharacteristicsdata : data.string.naturelocation,
				testyourselfoptions : [
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.locationop1,
					},
					{
						iscorrectflag : "correct",
						optiondata: data.string.locationop2,
					},
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.locationop3,
					}
				],
			},
			{
				imagecharacteristic : "size",
				testyourselfcharacteristicsdata : data.string.naturesize,
				testyourselfoptions : [
					{
						iscorrectflag : "correct",
						optiondata: data.string.sizeop1,
					},
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.sizeop2,
					},
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.sizeop3,
					}
				],
			},
			{
				imagecharacteristic : "orientation",
				testyourselfcharacteristicsdata : data.string.natureorientation,
				testyourselfoptions : [
					{
						iscorrectflag : "correct",
						optiondata: data.string.orientationop1,
					},
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.orientationop2,
					},
				],
			},
			{
				imagecharacteristic : "type",
				testyourselfcharacteristicsdata : data.string.naturetype,
				testyourselfoptions : [
					{
						iscorrectflag : "correct",
						optiondata: data.string.typeop1,
					},
					{
						iscorrectflag : "incorrect",
						optiondata: data.string.typeop2,
					},
				],
			},
		]
	},
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
	
	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

		/*=================================================
		=            svg template function            =
		=================================================*/		
	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
				
		//notify user call
		notifyuser($board);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');
		var $startanimbutton         = $svgwrapper.children('div.startanimbutton');

		if($svgcontainer.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"objectatC.svg", function (f) {    
    		s.append(f);
    			// snap objects
    		var redray          = s.select("#redray");
    		var blueray         = s.select("#blueray");
    		var arrowgroup      = s.select("#arrowgroup");
    		var raydiagramimage = s.select("#raydiagramimage");
    			
    			// jquery objects and js variables
    		var $svg     = $svgcontainer.children('svg');
    		var $rays    = $svg.children("[id$='ray']");
    		var $blueray = $svg.children('#blueray');
    		
    		$startanimbutton.on('click',  function() {
    			$(this).css("display","none");
    			redray.addClass('lineanimation');
    			blueray.addClass('lineanimation');  			
    		});

    		$blueray.on(animationend, function() {    			
    			arrowgroup.addClass('arrowanimation');
    			raydiagramimage.addClass('raydigramimageanimation'); 
    			$svgcontainer.css('left', '39%');
    			$imagecharacteristicsbox.css('left', '0%');
    			$svgcontainer.one('transitionend', function() {
    				//call navigationcontroller
					navigationcontroller();
    			});    			  		  			
    		});
 		});
		}
	}

	/*=======================================================
	=            raydiagramtestyourself template caller     =
	=======================================================*/	
	function raydiagramtestyourself(){
		var source   = $("#raydiagramtestyourself-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//notify user call
		notifyuser($board);

		var $testyourself                    = $board.children('div.testyourself');
		var $testyourselfquerycontainer      = $testyourself.children('div.testyourselfquerycontainer');
		var $testyourselfquestionlocation    = $testyourselfquerycontainer.children('div.testyourselfquestionlocation');
		var $testyourselfquestionorientation = $testyourselfquerycontainer.children('div.testyourselfquestionorientation');
		var $testyourselfquestionsize        = $testyourselfquerycontainer.children('div.testyourselfquestionsize');
		var $testyourselfquestiontype        = $testyourselfquerycontainer.children('div.testyourselfquestiontype');
		var $testyourselfquestions           = $testyourselfquerycontainer.children('div[class^="testyourselfquestion"]');
		var $testyourselfoptionscontainer    = $testyourselfquestions.children('div.testyourselfoptionscontainer');
		var $testyourselfoptions             = $testyourselfoptionscontainer.children('p');
		var $testyourselfresult              = $testyourselfquerycontainer.children('.testyourselfresult');
		var currentquestionindex             = 0;
		var correctans                       = 0;

		$testyourselfoptions.on('click', function() {			
			var $currentincorrectoptions = $(this).parent("div").children('p[data-iscorrect="incorrect"]');
			var $currentquestiondiv      = $(this).parent("div").parent("div");
			var $currentoptionsdiv       = $(this).parent("div");
			var choosenoptioniscorrect   = $(this).attr("data-iscorrect");
			
			$currentincorrectoptions.css('display', 'none');
			$currentquestiondiv.attr({
										'data-iscorrect': choosenoptioniscorrect,
									});
			$currentoptionsdiv.attr({
										'data-isclicked': "clicked"
									});

			$testyourselfquestions.eq(currentquestionindex++).addClass("afterclick");
			
			// calculate the number of correct answer and give feedback accordingly
			if(choosenoptioniscorrect == "correct") correctans++;
			if(currentquestionindex > 3)
			{
				if(correctans < 4){
					$testyourselfresult.attr('data-review', 'pleasereview');
					$testyourselfresult.html(data.string.feedbackreview);
				}
				else{
					$testyourselfresult.attr('data-review', 'proceed');
					$testyourselfresult.html(data.string.feedbackproceed);
				}

				//call navigationcontroller
				navigationcontroller();	
			}
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// On the basis of countNext value call templates
		switch(countNext){
			case 0: svgtemplate(); 
					 break;
			case 1: raydiagramtestyourself();
					 break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/

});
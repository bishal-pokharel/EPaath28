var imgpath = $ref+"/images/page12/";

//array of image
	var definitionImg = [
	"images/diy/diy1.png",
	"images/diy/diy2.png",
	"images/diy/diy3.png",
	"images/diy/diy4.png"
	];

var randomImageNumeral = ole.getRandom(1,3,0);

var content=[
	{	
		diyImageSource : definitionImg[randomImageNumeral],
		diyTextData : data.string.p12text0,
		diyInstructionData : data.string.p12text1
	},
	{	
		smalldiy: {	
				diyImageSource : definitionImg[randomImageNumeral],
				diyTextData : data.string.p12text0,
				diyInstructionData : data.string.p12text2
			},
		dropareaelement :[
			{
				dropelementcaptiondata : "concave",
				imgSource : imgpath+"recognizeconcave.png"
			},
			{
				dropelementcaptiondata : "convex",
				imgSource : imgpath+"recognizeconvex.png"
			},
			{
				dropelementcaptiondata : "plane",
				imgSource : imgpath+"recognizeplane.png"
			},
		],

		dragareaelement :[
			{
				dragelementId : "plane",
				dragelementText : data.string.p8listtype1,
			},
			{
				dragelementId : "convex",
				dragelementText : data.string.p8listsubtype1,
			},
			{
				dragelementId : "concave",
				dragelementText : data.string.p8listsubtype2,
			},	
		],
		conclusionTextData : data.string.p12text3
	}
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*
* diyLandingPage
*/
	function diyLandingPage () {
		var source = $("#diyLandingPage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);	
		$nextBtn.show(0);	
	}

/*
* recognizemirror
*/
	function recognizemirror() {
		var source = $("#recognizemirror-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);		
		
		var $recognizemirror = $board.children('div.recognizemirror');
		var $draggableCollector = $('.draggablecollector');
		var $droppables = $(".droppablecollector > figure > figcaption");
		var $draggables = $draggableCollector.children('p');

		var dropcount = 0;
		var totaldroprequired = $draggables.length;

		$draggables.draggable({containment: $recognizemirror, revert:"invalid"});

		/*eureka droppable this is nice use of droppable please see the use*/
		$droppables.droppable({ tolerance: "pointer", 
		accept : function(dropElem){
			//dropElem was the dropped element, return true or false to accept/refuse
			if($(this).attr('data-dropelementcaptiondata') === dropElem.attr("id")){
				return true;
			}
		},
		drop : function (event, ui) {
			$(this).html($(ui.draggable).html());
			$(this).addClass('droppableafterdrop');
			$(ui.draggable).css('display', 'none');
				if(++dropcount == totaldroprequired){
					$(ui.draggable).siblings('h4').show(0);
					ole.footerNotificationHandler.pageEndSetNotification();
				}
			}
		});
	}

	diyLandingPage();

	$nextBtn.on('click',function () {
		countNext++;		
		switch (countNext) {
			case 1:
				$(this).css("display","none");
				recognizemirror();
				break;
			default:break;
		}

		loadTimelineProgress($total_page,countNext+1);
	});
});
var imgpath = $ref+"/images/page1/";

var content=[
	{	
		coverboardadditionalclass : "coverboardfullwithbg",
		uppertextblockadditionalclass : "vertical-horizontal-center",
		uppertextblock : [
			{
				textclass : "introstylishtext",
				textdata  : data.string.p1text3,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfullwithbg",
		uppertextblockadditionalclass : "vertical-horizontal-center",
		uppertextblock : [
			{
				textclass : "stylishtextsmall",
				textdata  : data.string.p1text1,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfullwithbg2",
		uppertextblock : [
			{
				textclass : "introstylishtext2",
				textdata  : data.string.p1text2,
			}
		],
		lowertextblockadditionalclass : "droptextblockdown",
		lowertextblock : [
			{
				textclass : "roundtextstyle",
				textdata  : data.string.p1convex,
			},
			{
				textclass : "roundtextstyle",
				textdata  : data.string.p1concave,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfullwithbg3",
		uppertextblock : [
			{
				textclass : "introstylishtext4",
				textdata  : data.string.p1text3,
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "simpletextstyle",
				textdata          : data.string.p1text4,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfullwithbg4",
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass         : "simpletextstyle smallmargintop",
				textdata          : data.string.p1text5,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",		
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeconcave imagemargintop",
						imgsrc   : imgpath+"concavemirror.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "paratextstyle textmargintop",
				textdata  : data.string.p1text6,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",		
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "twoimagetogether twoimagemargintop",
						imgsrc   : imgpath+"concavemirror.png",
					},
					{
						imgclass : "twoimagetogether twoimagemargintop",
						imgsrc   : imgpath+"concavemirrorfindcurve.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "paratextstyle textmargintop",
				textdata  : data.string.p1text7,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "twoimagetogether threeimagemargintop",
						imgsrc   : imgpath+"concavemirror.png",
					},
					{
						imgclass : "twoimagetogether threeimagemargintop",
						imgsrc   : imgpath+"concavemirrorfindcurve.png",
					},
					{
						imgclass : "twoimagetogether threeimagemargintop",
						imgsrc   : imgpath+"concavemirrorsketch.png",
					}
				],
			}
		],
		lowertextblock : [
			{
				textclass : "paratextstyle textmargintop",
				textdata  : data.string.p1text8,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeconcavesketch",
						imgsrc   : imgpath+"concavemirrorsketch.png",
					},
				],
			}
		],
		lowertextblock : [
			{
				textclass : "paratextstyle textmargintop",
				textdata  : data.string.p1text9,
			}
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle squeezeconcavesketch",
						imgsrc   : imgpath+"concavemirrorsketch.png",
					},
				],
			}
		],
		lowertextblock : [			
			{
				textclass : "paratextstyle textmargintop",
				textdata  : data.string.p1text10,
			}
		],
		extracontent : true,
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc   : imgpath+"concaveprincipalaxis.png",
					},
				],
				imagelabels :[
					{
						imagelabelclass : "principalaxistext",
						imagelabeldata  : data.string.p1principalaxis,
					},
					{
						imagelabelclass : "symbolprincipalaxis",
						imagelabeldata  : data.string.p1symbolprincipalaxis,
					},
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "bottomparatextstyle",
				textdata          : data.string.p1text11,
			},
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc   : imgpath+"concavecentreofcurvecircle.png",
					},
				],
				imagelabels :[
					{
						imagelabelclass : "centreofcurvetext",
						imagelabeldata  : data.string.p1centreofcurve,
					},
					{
						imagelabelclass : "symbolcentreofcurve",
						imagelabeldata  : data.string.p1symbolcentreofcurve,
					},
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "bottomparatextstyle",
				textdata          : data.string.p1text12,
			},
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc   : imgpath+"concavecentreofcurve.png",
					},
				],
				imagelabels :[
					{
						imagelabelclass : "symbolprincipalaxis2",
						imagelabeldata  : data.string.p1symbolprincipalaxis,
					},
					{
						imagelabelclass : "symbolcentreofcurve",
						imagelabeldata  : data.string.p1symbolcentreofcurve,
					},
					{
						imagelabelclass : "symbolvertex",
						imagelabeldata  : data.string.p1symbolvertex,
					},
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "bottomparatextstyle",
				textdata          : data.string.p1text13,
			},
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading",
				textdata  : data.string.p1text3,
			},
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "simpleimgstyle",
						imgsrc   : imgpath+"concavefocus.png",
					},
				],
				imagelabels :[
					{
						imagelabelclass : "focustext",
						imagelabeldata  : data.string.p1focalpoint,
					},
					{
						imagelabelclass : "symbolcentreofcurve",
						imagelabeldata  : data.string.p1symbolcentreofcurve,
					},
					{
						imagelabelclass : "symbolvertex",
						imagelabeldata  : data.string.p1symbolvertex,
					},
					{
						imagelabelclass : "symbolfocalpoint",
						imagelabeldata  : data.string.p1symbolfocalpoint,
					},
				]
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "bottomparatextstyle",
				textdata          : data.string.p1text14,
			},
		],
	},
	{	
		coverboardadditionalclass : "coverboardfull",
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle somemargintopheading removebold",
				textdata  : data.string.p1text15,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "allpositionsimgstyle",
						imgsrc   : imgpath+"allpositions.png",
					},
				],
			}
		],
		lowertextblock : [
			{
				datahighlightflag : true,
				textclass         : "allpositionstext margintop",
				textdata          : data.string.p1text16,
			},
		],
	},
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);


	// register the handlebar partials first
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*=================================================
		=            general template function            =
		=================================================*/		
	function general() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
				
		// call navigation controller
		navigationcontroller();

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// find if there is linehorizontal div in the slide
		var $linehorizontal = $board.find("div.linehorizontal");
		if($linehorizontal.length > 0)
		{
			$linehorizontal.attr('data-isdrawn', 'draw');
		}
	}

	/* this is for development and testing purpose only, please comment it out
	during deployment */
	// countNext+=15;
	general();

	$nextBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext++;		
		general();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css('display', 'none');
		countNext--;	
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;	
		general();
		loadTimelineProgress($total_page,countNext+1);
	});
});
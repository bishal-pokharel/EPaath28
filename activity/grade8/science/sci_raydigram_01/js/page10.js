//array of image
var timeToThinkImage = [
	"images/timetothink/timetothink1.png",
	"images/timetothink/timetothink2.png",
	"images/timetothink/timetothink3.png",
	"images/timetothink/timetothink4.png"
];

var randomImageNumeral = ole.getRandom(1,3,0);
var currentTimetoThinkImage = timeToThinkImage[randomImageNumeral];

var imgpath = $ref+"/images/page10/";

var content=[
	{	
		typeoflayout : "timetothinklayout",
		timetothink  : [
			{
				timeToThinkImageSource     : currentTimetoThinkImage,
				timeToThinkTextData        : data.string.timetothinktext,
				timeToThinkInstructionData : data.string.p10timetothinkquest,
			}
		],			
	},
	{	
		typeoflayout: "layoutusageslowreveal",
		usageslowrevealblock : [
			{
				usageheadingdata : data.string.p10text1,
				cardcontentblock : [
					{
						cardtitle       : [data.string.usestext],
						cardcontenttext : [
							{
								cardcontenttextdata : data.string.p10use1,
							},
						],
					},
					{
						cardblockhasimageflag : "true",
						cardtitle             : [data.string.whyneed],
						cardimage             : [imgpath+"dentist.png"],
						cardcontenttext       : [
							{
								cardcontenttextdata : data.string.p10need1,
							},
						],
					},
					{
						cardtitle       : [data.string.howresolved],
						cardcontenttext : [
							{
								cardcontenttextdata : data.string.p10need1,
							},
						],
					},
				]
			},
		],
	},
	{	
		typeoflayout: "layoutusageslowreveal",
		usageslowrevealblock : [
			{
				usageheadingdata : data.string.p10text1,
				cardcontentblock : [
					{
						cardtitle       : [data.string.usestext],
						cardcontenttext : [
							{
								cardcontenttextdata : data.string.p10use1,
							},
						],
					},
					{
						cardblockhasimageflag : "true",
						cardtitle             : [data.string.whyneed],
						cardimage             : [imgpath+"dentist.png"],
						cardcontenttext       : [
							{
								cardcontenttextdata : data.string.p10need1,
							},
						],
					},
					{
						cardtitle       : [data.string.howresolved],
						cardcontenttext : [
							{
								cardcontenttextdata : data.string.p10need1,
							},
						],
					},
				]
			},
		],
	},
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	
	/*----------  curvedmirrorusage template  ----------*/	
	function curvedmirrorusage(){
		var source = $("#curvedmirrorusage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		//data highlighter
		// texthighlight($board);

		// user notification
		// notifyuser($board);

		countNext < 1 ? navigationcontroller() : null;

		// if display has usageslowrevealblock
		var $usageslowrevealblock = $board.find('div.usageslowrevealblock');

		if($usageslowrevealblock.length > 0){
			var $nextusageBtn      = $usageslowrevealblock.find("div.nextusageBtn");
			var $cards             = $usageslowrevealblock.find("div[class^='card_block']");
			var totalcardstoreveal = $cards.length - 1;
			var totalcardsrevealed = 0;

			// next use button navigation control
			$nextusageBtn.on('click', function() {
				$(this).css('display', 'none');
				++totalcardsrevealed;
				if(totalcardsrevealed < totalcardstoreveal){
					$cards.eq(totalcardsrevealed).css('opacity', '1');
					$nextusageBtn.show(0);
				}
				else if(totalcardsrevealed == totalcardstoreveal){
					$cards.eq(totalcardsrevealed).css('opacity', '1');
					navigationcontroller();
				}
			});
		}
	}
	
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// call the template
		curvedmirrorusage();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});
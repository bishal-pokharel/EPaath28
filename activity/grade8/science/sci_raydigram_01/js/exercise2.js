// prototype for array shuffle
Array.prototype.shufflearray = function(){
  var i = this.length-1, j, temp;
        while(--i > 0){
            j = Math.floor(Math.random() * (i+1));
            temp = this[j];
            this[j] = this[i];
            this[i] = temp;
        }
        return this;
};

var imgpath = $ref+"/images/exercise2/";
var correctimg = "images/correct.png";
var incorrectimg = "images/wrong.png";
var congratulationimgarray = [
                                "images/quizcongratulation/gradea.png",
                                "images/quizcongratulation/gradeb.png",
                                "images/quizcongratulation/gradec.png",
                            ];

var tempcontent=[
   {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q6,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"06.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q6o1,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q6o2,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        },
                        {
                            optionstext : data.string.e2q6o3,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        }
                    ],
    },
    {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q7,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"07.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q7o2,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q7o3,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q7o1,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        }
                    ],
    },
    {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q8,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"08.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q8o2,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        },
                        {
                            optionstext : data.string.e2q8o3,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q8o1,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        }
                    ],
    },
    {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q9,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"09.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q9o1,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q9o2,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q9o3,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        }
                    ],
    },
    {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q10,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"10.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q10o2,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q10o3,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        },
                        {
                            optionstext : data.string.e2q10o1,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        }
                    ],
    },
    {   
        hasquiztemplate : true,
        questioncount : "it will be updated before template call",      
        numberofoptions : "it will be updated before template call",
        hasdescriptionclass : "it will be updated before template call",
        questiontextdata : data.string.e2q11,
        descriptioncontent:[
            {
                imagecontent:[
                    {
                        imgclass:"sideimg",
                        imgsrc:imgpath+"11.png"
                    }
                ]
            }
        ],
        optionsdata : [
                        {
                            optionstext : data.string.e2q11o1,
                            isdatacorrect : "correct",
                            optionsmarksrcimg : correctimg
                        },
                        {
                            optionstext : data.string.e2q11o2,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        },
                        {
                            optionstext : data.string.e2q11o3,
                            isdatacorrect : "incorrect",
                            optionsmarksrcimg : incorrectimg
                        }
                    ],
    }
];

// now randomize the question content
var content = tempcontent.shufflearray();

var congratulationcontent = [
    {
        congratulationtextdata : data.string.e1congratulationtext,
        congratulationimgsrc : congratulationimgarray[1],
        congratulationcompletedtextdata : data.string.e1congratulationcompletedtext,
        congratulationyourscoretextdata : data.string.e1congratulationyourscoretext,
        congratulationreviewtextdata : data.string.e1congratulationreviewtext,
    }
];

var summarycontent = [
    {
        tableheadingrow : [         
            // empty string for first two headings
            " ",
            " ",
            data.string.e1summaryheadingcorrectans,
        ],
        quizsummarytitledata : data.string.quizsummarytitletext,
        tabledatarow : [

        ],
    }
];

$(function () { 
    var $board = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var countNext = 0;

    var $total_page = content.length+congratulationcontent.length +2;
    loadTimelineProgress($total_page,countNext+1);

    // assign variable with quizboard container and scoreboard elements
    var $quizboard = $board.children('div.quizboard'); 
    var $scoreboard = $board.children('div.scoreboard'); 
    var $scoretext = $scoreboard.children('.scoretext');
    var $scorecount = $scoreboard.children('.scorecount');
    var $userscore = $scorecount.children('.userscore');
    var $totalproblemstext = $scoreboard.children('.totalproblemstext');
    // all elements which contains data about total questions
    var $totalquestiondata = $scoreboard.find('.totalquestiondata');
    var $scoregraph = $scoreboard.children('.scoregraph');
    var $scoregraphchildren;
    var totalquestioncount = 0; /*initiate total question count as 0*/
    var userscorecount = 0;
    var userscorestorage;
    var scoregraphstorehtml;
    var $congratulationscoregraphchildren;

    /*==========  navigation controller function  ==========*/   
     function navigationcontroller(){
        if(countNext >= 0 && countNext < $total_page-1){
            $nextBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            ole.footerNotificationHandler.pageEndSetNotification();
        }
     }

     /*==========  scoreboard update caller  ==========*/
     function updatescoreboard(){       
        // for only first call updates
        if(countNext == 0){
            // update the total question count
            $.each(content, function(index, val) {
                if(content[index].hasquiztemplate){
                    totalquestioncount++;
                }
            });

            $scoretext.html(data.string.e1scoretextdata);
            $totalquestiondata.html(totalquestioncount);
            $totalproblemstext.html(data.string.e1totalproblemtextdata);

            // now populate scoregraph block with batch of tags
            var blocktag = "<span data-correct=''></span>";

            for(var i=1 ; i <= totalquestioncount ; i++){
                scoregraph = $scoregraph.html();
                $scoregraph.html(scoregraph+blocktag);
            }

            $scoregraphchildren = $scoregraph.children('span')
        }
     }

    /*==========  quiz template caller  ==========*/

    function quiz(){
        var source = $("#quiz-template").html();
        var template = Handlebars.compile(source);
        // update question count in content before updating the quiz template
        content[countNext].questioncount = countNext+1;
        // update options count in content before updating the quiz template
        content[countNext].numberofoptions = content[countNext].optionsdata.length; 
        // update if quiz board has description content before updating the quiz template   
        content[countNext].hasdescriptionclass = typeof content[countNext].descriptioncontent !== "undefined" ? "hasdescriptionclass" : null;

        var html = template(content[countNext]);
        $quizboard.html(html);

        var $options = $quizboard.children('.options').children('p');
        var clickcount = 0;

        // on options click do following
        $options.on('click', function() {
            // if incorrect is choosen
            $(this).attr("data-isclicked","clicked");


            // on first click only
            if(++clickcount == 1){
                $scoregraphchildren.eq(countNext).attr({
                    'data-correct' : $(this).attr("data-correct")
                });
            }
            
            if($(this).attr("data-correct") == "correct"){
                // update isclicked data attribute to clicked
                clickcount == 1 ? $userscore.html(++userscorecount) : null;
                $options.css('pointer-events', 'none');

                /*store the scoregraph and userscore as it is needed on congratulations templates
                when all the questions are attempted*/
                if(countNext+1 == totalquestioncount){
                    scoregraphstorehtml = $scoregraph.html();
                    userscorestorage = $userscore.html();
                }

                navigationcontroller();
            }
        });     
    }

    /*==========  congratulations template caller  ==========*/
    
    function congratulation(){
        var source = $("#congratulation-template").html();
        var template = Handlebars.compile(source);
        // update question count in content before updating the congratulation template
        // content[countNext].questioncount = countNext+1;
        
        var html = template(congratulationcontent[0]);
        $board.html(html);

        var $congratulationcontainer = $board.children('.congratulationcontainer');
        var $congratulationreviewtext = $congratulationcontainer.children('.congratulationreviewtext');
        var $congratulationscoregraph = $congratulationcontainer.children('.congratulationscoregraph');
        var $congratulationyourscoretext = $congratulationcontainer.children('.congratulationyourscoretext');
        $congratulationscoregraph.html(scoregraphstorehtml);

        // update the congratulationyourscoretext sentence
        var rawstatement = $congratulationyourscoretext.html();
        rawstatement = rawstatement.replace("#userscore#",userscorestorage);
        rawstatement = rawstatement.replace("#totalscore#",totalquestioncount);
        $congratulationyourscoretext.html(rawstatement);

        $congratulationscoregraphchildren = $congratulationscoregraph.children('span');

        // on clicking the review button trigger the next button
        $congratulationreviewtext.on('click',function() {
            $nextBtn.trigger('click');
        });
    }

    /*==========  summary template caller  ==========*/
    
    function summary(){
        // first update the summarycontent array
        var eachpushitemtabledata = []; 
        var graphcellhtml, question, correctanswer, isdatacorrectflag;  
        for(var i=0 ; i < totalquestioncount ; i++){
                eachpushitemtabledata = [];
            $.each(content[i], function(index, val) {               
                isdatacorrectflag = $congratulationscoregraphchildren.eq(i).attr("data-correct");
                console.log(isdatacorrectflag);
                question = content[i].questiontextdata;
                $.each(content[i].optionsdata, function(index, val) {
                     if(this.isdatacorrect == "correct"){
                        correctanswer = this.optionstext;
                     }              
                }); 

            });
            eachpushitemtabledata.push(i+1,question,correctanswer);
            // push the table data cell required datas - the tabledata array and iscorrectdata
            summarycontent[0].tabledatarow.push({"iscorrectdata" : isdatacorrectflag, "tabledata" : eachpushitemtabledata});
        }
        
        console.log(summarycontent[0].tabledatarow);
        var source = $("#summary-template").html();
        var template = Handlebars.compile(source);              
        var html = template(summarycontent[0]);
        $board.html(html);
        $nextBtn.show(0);
    }
    
    quiz();
    updatescoreboard();
    // congratulation();
    // summary();
    
    $nextBtn.on('click',function () {
        $(this).css("display","none");
        countNext++;
        // alert(countNext +" and "+content.length);
        if(countNext < content.length){
            quiz();
        }   
        else if(countNext == content.length){
            congratulation();
        }
        else if(countNext == content.length+1){
            summary();
        }
        else if(countNext == content.length+2){
            ole.activityComplete.finishingcall();
        }
        
        loadTimelineProgress($total_page,countNext+1);
    });
});
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page2/";

var content=[
	{	
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext1,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext2,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext3,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext5,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext4,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext6,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext7,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext8,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext9,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext10,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext11,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext12,
					}
				],
		
	},
	{		
		svgfooter : [
					{
						datahighlightflag : true,
						textclass         : "svgfooterstyle",
						textdata          : data.string.p2footertext13,
					}
				],
		
	},	
];

svgcontent=[
	{
		svgheading : [
					{
						datahighlightflag : true,
						textclass         : "svgheadingstyle",
						textdata          : data.string.raydiagramheading,
					}
				],
	}
];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
	
	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/
	
/*=======================================
=            Templates Block            =
=======================================*/	
	
	// board childrens as template containers
	var $generaltemplate = $board.children('div.generaltemplate');
	var $svgwrapper = $board.children('div.svgwrapper');

	/*=================================================
	=            belowsvg template function            =
	=================================================*/		
	function belowsvgtemplate(){
		var source   = $("#belowsvg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$generaltemplate.html(html);
		
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $svgfootercontainer = $generaltemplate.children('div.svgfootercontainer');
	}

	/*=================================================
	=            svg template function            =
	=================================================*/	
	// global variables for svg template
	var redray,
		redarrowgroup,
	 	blueray,
	 	bluearrowgroup,
	 	greenray,
	 	greenarrowgroup,
		raydiagramimage,
	 	raydiagramobject,
	 	
	 	$svg,
	 	$redray,
	 	$greenray,
		$blueray,
		$raydiagramobject,
		$raydiagramimage;
		

	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(svgcontent[0]);
		$svgwrapper.html(html);		
			
		// if there is a svg content
			var $svgcontainer = $svgwrapper.children('div.svgcontainer');
			
			var s = Snap(".svgcontainer");
			Snap.load(imgpath+"objectbeyondC.svg", function (f) {    
    		s.append(f);
    			// snap objects    		
    		redray           = s.select("#redray");
    		redarrowgroup    = s.select("#redarrowgroup");
    		blueray          = s.select("#blueray"); 
    		bluearrowgroup    = s.select("#bluearrowgroup");
    		greenray          = s.select("#greenray"); 
    		greenarrowgroup    = s.select("#greenarrowgroup");   		
    		raydiagramobject = s.select("#objectraydiagram");
    		raydiagramimage  = s.select("#imageraydiagram");
    		
    		
    			// jquery objects and js variables
    		$svg                 = $svgcontainer.children('svg');
    		$redray              = $svg.find("#redray");
    		$blueray             = $svg.find("#blueray");
    		$greenray            = $svg.find("#greenray");

    		svgupdate();    		
 		});

			
	}

	function svgupdate(){

		// animate the svg item on basis of countnext
    		switch(countNext){
    			case 1: raydiagramobject.addClass('raydigramobjectanimation');
    					$nextBtn.show(0);
    					break;
    			case 2: redray.addClass('lineanimation');
    					$redray.one(animationend, function() {
    						redarrowgroup.addClass('arrowanimation');
    						$nextBtn.show(0);
    					});
    					break;
    			case 3: blueray.addClass('lineanimation');
    					$blueray.one(animationend, function() {
    						bluearrowgroup.addClass('arrowanimation');
    						$nextBtn.show(0);
    					});
    					break;
    			case 4: greenray.addClass('lineanimation');
    					$greenray.one(animationend, function() {
    						greenarrowgroup.addClass('arrowanimation');
    						$nextBtn.show(0);
    					});
    					break;
    			case 5: raydiagramimage.addClass('raydigramimageanimation');
    					$nextBtn.show(0);
    					break;
    			default : break;
    		}
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// if footerNotificationHandler pageEndSetNotification was called then
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// On the basis of countNext value call templates
		switch(countNext){
			case 0  : belowsvgtemplate();
					 svgtemplate();
					 navigationcontroller();
					 break;
			case 1  : belowsvgtemplate();
					 svgupdate();
					 break;
			case 2  : belowsvgtemplate();
					 svgupdate();
					 break;
			case 3  : belowsvgtemplate();
					 svgupdate();
					 break;
			case 4  : belowsvgtemplate();
					 svgupdate();
					 break;
			case 5  : belowsvgtemplate();
					 svgupdate();
					 break;
			case 6  : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 7  : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 8  : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 9  : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 10 : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 11 : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 12 : belowsvgtemplate();
					 navigationcontroller();
					 break;
			case 13 : belowsvgtemplate();
					 navigationcontroller();
					 break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;
		if(countNext==5) countNext=0;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/

});
<?xml version="1.0" encoding="utf-8"?>

<olenepal>

<lesson>
	<subject>Science</subject>
	<topic>Physics-EN</topic>
	<chapter>Unit 8 - Sound</chapter>
	<levelname>Level 1</levelname>
	<leveldescription>Sound</leveldescription>

	<objective>
		<title>Objective:</title>
		<description>
			<purpose>To explain speed, frequency and wave length of sound.</purpose>
			<purpose>To explain and distinguish echo and reverberation.</purpose>
		</description>
	</objective>
	
	<definition>
		<description>
			<statement></statement>
		</description>
	</definition>

	<lessonDirection>
		<purposes></purposes>		
		<about></about>
		<lessonhelp></lessonhelp>
		<exercisehelp>
			<exerciseText></exerciseText>			
		</exercisehelp>		
	</lessonDirection>
	
</lesson>

<strings>	
<!-- common strings -->
	<!-- please take care you do not remove this spacecharacter string -->
	<string id="spacecharacter">&#160;</string>
	<string id="diytext">Do It Yourself</string>
	<string id="definitiontext">Definition</string>
	<string id="soundtext">Sound</string>
	<string id="cresttext">Crest</string>
	<string id="troughtext">Trough</string>
	<string id="amplitudetext">Amplitude</string>
	<string id="wavelengthtext">Wavelength</string>
	<string id="rarefactiontext">Rarefaction</string>
	<string id="compressiontext">Compression</string>
			
<!-- page 1 -->
	<string id="p1text1">These are few sources of sound. #Click on each one of them@ to hear sounds.</string>
	<string id="p1text2">#S@#o@#u@#n@#d@</string>
	<string id="p1text3">Let us learn about sound.</string>
	<string id="p1text4">Sound is a #vibration@ that #travels as a wave@, spreading outwards from the source of sound.</string>
	<string id="p1text5">Sound stimulates our sense of hearing.</string>
	<string id="p1text6">These sound waves reach to our ear where vibration on ear drum helps us sense different types of sound.</string>
	<string id="p1text7">Before we learn about sound wave, let us know what is #wave@.</string>
	<string id="p1text8">There are other types of waves around us other than sound.</string>
	<string id="p1text9">Radio waves, light waves, water waves and earthquake waves are some examples in our daily encounters.</string>
	<string id="p1text10">All waves are generated through disturbance; for example, when a rock is thrown in water.</string>
	<string id="p1text11part1">Let us check out the water wave for instance.</string>
	<string id="p1text11part2">These waves have mainly two parts that are called #crest@ and #trough@ through which the wave is travelled from one location to another.</string>

		
<!-- page 2 -->
	<string id="p2heading1">Properties of waves</string>
	<string id="p2text1">Let us learn about the properties of wave.</string>
	<string id="p2text2">These waves have moving #crest@ and #troughs@.</string>
	<string id="p2text3">A #crest@ is the highest point the medium rises to.</string>
	<string id="p2text4">A #trough@ is the lowest point the medium sinks to.</string>
	<string id="p2text5">Crests and troughs on a wave are shown in the figure below.</string>
	<string id="p2text6">The height of the wave is called its #amplitude@.</string>
	<string id="p2texta">a</string>
	<string id="p2text7part1">Thus #amplitude@ is the distance from the horizontal line to its crest or trough.</string>
	<string id="p2text7part2">In the above diagram '#a@' represents the amplitude.</string>
	<string id="p2text8part1">Amplitude relates to loudness in sound (and brightness in light).</string>
	<string id="p2text8part2">Greater the amplitude, louder is the sound.</string>
	<string id="p2text9part1">#Wavelength@ is defined as the distance from one crest (or maximum of the wave) to the subsequent crest.</string>
	<string id="p2text9part2">Wavelength is represented by the character '#&#955; (lambda)@' as in figure below.</string>
	<string id="p2diysplashinstruction">Shall we revise and exercise the wave properties.</string>
	<string id="p2diyquestion">Drag the labels to the wave in the figure below with proper properties.</string>
	<string id="p2congratulationsmsg">Congratulations you did well !</string>
	<string id="p2text10">Now let us learn about sound wave and its properties.</string>
	
	
<!-- page 3 -->
	<string id="p3heading1">Sound wave</string>
	<string id="p3text1">Sound travels as longitudinal wave in air.</string>
	<string id="p3text2">Sound wave has #rarefactions@ and #compressions@ which is created by motion of medium particles.</string>
	<string id="p3text2part2">#Compression@ happens when molecules are forced together. #Rarefaction@ is just the opposite, it occurs when molecules expand.</string>
	<string id="p3text3part1">#Wavelength@ of a sound is the distance between subsequent rarefactions/compressions.</string>
	<string id="p3text3part2">It is measured in #meters@.</string>
	<string id="p3text4">The #speed of the wave@ is the measurement of #how fast a crest is moving@ from a fixed point.</string>
	<string id="p3text5part1">Similarly, sound travels in the longitudinal wave with some speed.</string>
	<string id="p3text5part2">Although sound travels very fast, it is still possible to measure its speed.</string>
	<string id="p3text5part3">For example, speed of sound in air is 343 m/s at 20&#176;C temperature.</string>
	<string id="p3text6">The #frequency@ of waves is the rate of occurrence of a repeating crests or peaks per unit time.</string>
	<string id="p3text7">#Frequency@ is dependent on #wavelength@ and the #speed@ of sound.</string>
	<string id="p3text8">Also, greater the frequency of sound greater is the #pitch@.</string>
	<string id="p3text9">The SI unit of frequency is called a #Hertz@, denoted as #Hz@.</string>
	<string id="p3text10part1">A #hertz@ is defined as the number of cycles (crest or peaks) per second.</string>
	<string id="p3text10part2">So, a 100 Hz would mean 100 cycles per second.</string>
	<string id="p3text11">Frequency can be calculated as follows:</string>
	<string id="p3freqformulalhs">Frequency = </string>
	<string id="p3freqformularhstop">Velocity of wave</string>
	<string id="p3freqformularhsbottom">Wavelength of wave</string>
	

<!-- page 4 -->
	<string id="p4heading1">Properties of sound</string>
	<string id="p4text1">Now let us learn the properties of sound.</string>
	<string id="p4loudness">Loudness</string>
	<string id="p4lefttextdetail1">The loudness of a sound depends on its energy. The greater the energy the louder the sound.</string>
	<string id="p4pitch">Pitch</string>
	<string id="p4righttextdetail1">The pitch of a sound is also called its shrillness. Basically pitch means how high or low a sound is.</string>
	<string id="p4soft">Soft</string>
	<string id="p4loud">Loud</string>
	<string id="p4high">High</string>
	<string id="p4low">Low</string>
	<string id="p4lefttextdetail2">Greater the amplitude, the louder the sound</string>
	<string id="p4righttextdetail2">Greater the frequency, the higher the pitch.</string>
	<string id="p4text2">Now we shall see how to compare and study waves.</string>
	<string id="p4heading2">Comparison of waves</string>
	<string id="p4text3">Observing the above two waves we can deduce that,</string>
	<string id="p4sound1text">sound 1</string>
	<string id="p4sound2text">sound 2</string>
	<string id="p4frequencycomparelist1">these sound waves have the #same frequency@, so the sounds have the #same pitch@.</string>
	<string id="p4frequencycomparelist2">sound 2 has a #greater amplitude@ than sound 1, so sound 2 is #louder@.</string>
	<string id="p4amplitudecomparelist1">the sound waves have the #same amplitude@, so the sounds have the #same loudness@.</string>
	<string id="p4amplitudecomparelist2">sound 2 has a #greater frequency@ than sound 1, so sound 2 is #higher pitched@.</string>
	<string id="p4text4">Sound has different frequencies and different living beings can hear different range of frequencies. Let us learn about different frequency ranges.</string>
	
	
<!-- page 5 -->
	<string id="p5heading1">Frequency hearing ranges</string>
	<string id="p5infrasoundlabeltext">Infrasounds : below 20 Hz</string>
	<string id="p5humansoundlabeltext">Human auditory field : 20 Hz to 20,000 Hz</string>
	<string id="p5ultrasoundlabeltext">Ultrasounds : above 20,000 Hz</string>
	<string id="p5infrasoundtext">Human beings cannot hear sounds below 20 Hz.</string>
	<string id="p5humansoundtext">Human beigns can hear the sounds from 20 Hz to 20,000 Hz.</string>
	<string id="p5ultrasoundtext">Human beings cannot hear sounds above 20000 Hz.</string>
	<string id="p5text1">But, there are other animals that can hear infra and ultra sounds.</string>
	<string id="p5text2">Elephants can hear the sound below 20 Hz. They can hear earthquakes.</string>
	<string id="p5text3">Whales can hear sounds above 200 Hz. Similarly do the bats.</string>
	<string id="p5text4">Thus living being are capable of hearing various frequencies of sounds.</string>
	<string id="p5funfact">Fun Fact</string>
	<string id="p5funfacttext">Bats use ultrasound to locate objects and preys. They can hear how long it takes to echo back the sound they have produced. It helps them locate how far an object lies. This is called #echolocation@.</string>
	<string id="p5text5">Let us learn the process of echolocation used by bats to catch their prey.</string>
	<string id="p5echolocationprocess1">First the bat produces ultrasound, whose waves travels around it.</string>
	<string id="p5echolocationprocess2">Ultrasound strikes on a prey, it gets reflected back. Bat, then knows the location of the prey as soon as the echo is heard.</string>
	<string id="p5completeecholocationprocess">This is the complete echolocation process, which shows one of the use of ultrasound in nature.</string>
	

<!-- page 6 -->
	<string id="p6heading1">Echo</string>
	<string id="p6text1">Just as the bats use it, sound waves can be reflected by large, hard surfaces like buildings, walls and cliffs.</string>
	<string id="p6text2">A reflected sound can be heard separately from the original sound if the sound source is far from receiver. Such reflected sound is called an #echo@.</string>
	<string id="p6text2part2">Echo is applied in our everyday life as well. Let us see how SONAR is used to measure depth of the sea.</string>
	<string id="p6echoinformationrtext1">Click on hand.</string>
	<string id="p6echoinformationrtext2">Echo occurs when a sound is reflected from a distant (more than 17 m) object.</string>
	<string id="p6heading2">Application of echo</string>
	<string id="p6text3part1">Sonar is a device that  consists of a #transmitter@ and a #receiver@.</string>
	<string id="p6step1label1">Receiver</string>
	<string id="p6step1label2">Transmitter</string>
	<string id="p6text3part2">It is attached to the ship at its base to produce sound waves.</string>
	<string id="p6text3part3">The sound wave travels to the bed of the sea, reflected back and is received by the sonar receiver.</string>
	<string id="p6text3part4">The time taken for the wave to travel indicates the depth of the sea (d).</string>
	<string id="p6step4label1">d</string>
	<string id="p6text3part5">It is used to see the obstacle also.</string>
	<string id="p6step5label1">Obstacle</string>
	<string id="p6text4">Similar to echo, there is yet another type of reflected sound called #reverberation@.</string>
	<string id="p6heading3">Reverberation</string>
	<string id="p6text5">#Reverberation@ resembles an echo, however the distance between the source and the obstacle is less in Reverberation.</string>
	<string id="p6reverberationinformationrtext2">The obstacle should be less than 17 meters to create the reverberance and the reflected wave reach the observer in less than 0.1 second.</string>
	<string id="p6text6">Reverberation causes effect of multiple sound in an auditorium.</string>
	<string id="p6text7">So, how is echo different from reverberation.</string>
	<string id="p6echovsreverberation">Echo Vs. Reverberation</string>
	<string id="p6echodiff1">Long distance is considered.</string>
	<string id="p6reverberationdiff1">Less than 17 m distance is considered.</string><string id="p6echodiff2">Occurs due to the reflection of sound wave by obstacles in an open space.</string>
	<string id="p6reverberationdiff2">Occurs due to the multiple reflection of sounds inside enclosed space.</string>
	<string id="p6echoexample">Example : Screaming on top of a hillock.</string>
	<string id="p6reverberationexample">Example: Auditorium.</string>
	<string id="p6hearexamplesound">Hear an example sound:</string>
	<string id="p6hearecho">Listen echo</string>
	<string id="p6hearreverberation">Listen reverberation</string>
	<string id="p6text8">As you have learnt about sound, let us revisit what you have learnt.</string>

	
		
<!-- page 7 -->
	<string id="p7recaptext">Recap</string>
	<string id="p7text1">Sound is a vibration that travels in as a wave, spreading outwards from the source of sound.</string>
	<string id="p7text2">A crest is the highest point the medium rises to and a trough is the lowest point the medium sinks to.</string>
	<string id="p7text3">The height of the wave is called its amplitude.</string>
	<string id="p7text4">Wavelength is defined as the distance from one crest.</string>
	<string id="p7text5">The frequency of waves is the rate of occurrence of a repeating crests or peaks per unit time.</string>
	<string id="p7text6">Echo is the phenomenon in which the sound wave is reflected and hence arrives to the listener after some time delay after the direct sound.</string>

<!-- exercise 1-->	
	<!-- exercise 1-->	
	<string id="e1q1">What type of waves are sound waves?</string>
	<string id="e1q1o1">None of the above</string>
	<string id="e1q1o2">Transverse</string>
	<string id="e1q1oc">Longitudinal</string>	

	<string id="e1q2">What is the amplitude of a wave?</string>
	<string id="e1q2o1">The length of the wave.</string>
	<string id="e1q2o2">The number of waves per second.</string>
	<string id="e1q2oc">The maximum height of the wave.</string>

	<string id="e1q3">What is the wavelength of a wave?</string>
	<string id="e1q3o1">The maximum height of the wave.</string>
	<string id="e1q3o2">The number of waves per second.</string>
	<string id="e1q3oc">The distance between the same two points on a wave.</string>

	<string id="e1q4">What is the frequency of a wave?</string>
	<string id="e1q4o1">The length of the wave.</string>
	<string id="e1q4o2">The maximum height of the wave.</string>
	<string id="e1q4oc">The number of waves per second.</string>

	<string id="e1q5">Above which of the following frequency is ultrasound?</string>
	<string id="e1q5o1">10,000 Hz</string>
	<string id="e1q5o2">20 Hz</string>
	<string id="e1q5oc">20,000 Hz</string>

	<string id="e1q6">The distance between two consecutive crests of a wave is called _______.</string>
	<string id="e1q6o1">Amplitude</string>
	<string id="e1q6o2">Period</string>
	<string id="e1q6oc">Wavelength</string>

	<string id="e1q7">A sound with a low pitch always has a low ______.</string>
	<string id="e1q7o1">Amplitude</string>
	<string id="e1q7o2">Wavelength</string>
	<string id="e1q7oc">Frequency</string>

	<string id="e1q8">Sound waves are longitudinal waves which have ______ and rarefactions.</string>
	<string id="e1q8o1">Trough</string>
	<string id="e1q8o2">Crest</string>
	<string id="e1q8oc">Compressions</string>

	<string id="e1q9">Which of the following frequency ranges can most humans hear?</string>
	<string id="e1q9o1">20 Hz to 200 Hz</string>
	<string id="e1q9o2">200 Hz to 2,000 Hz</string>
	<string id="e1q9oc">20 Hz to 20,000 Hz</string>

	<string id="e1q10">The hertz (Hz) is the unit of measure for which of the following wave property?</string>
	<string id="e1q10o1">Amplitude</string>
	<string id="e1q10o2">Wavelength</string>
	<string id="e1q10oc">Frequency</string>

	<!-- congratulation template data -->
	<string id ="e1congratulationtext">Congratulations!!</string>
	<string id ="e1congratulationcompletedtext">You have completed this exercise.</string>
	<string id ="e1congratulationyourscoretext">You scored #userscore# out of #totalscore#.</string>
	<string id ="e1congratulationreviewtext">Let us review the exercise &#187;</string>

	<!-- summary template -->	
	<string id ="quizsummarytitletext">Quiz Summary</string>
	<string id ="e1summaryheadingcorrectans">Correct Answer</string>
	
</strings>

</olenepal>
var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationstart = "animationstart webkitAnimationStart oanimationstart MSAnimationStart";
var animationiteration = "animationiteration webkitAnimationIteration oanimationiteration MSAnimationIteration";
var animationend  = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var SPACE           = data.string.spacecharacter;
var imgpath         = $ref+"/images/page6/";
var clickthisimgsrc = "images/clickme_icon.png";
var soundpath       = $ref+"/sounds/page6/";

// this function updates the slide2selectedsound
var updatesound = function (soundname){
	return new buzz.sound( soundpath + soundname + ".ogg");
}

var content=[
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text1,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "echoaniamationstyle",
						imgsrc : imgpath+"echo.svg"
					},
				]
			}
		],
	},
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text2,
			}
		],
		echoobservationblock : [
			{
				echoscenesrc        : imgpath + "echoscene.png",
				echosrc             : imgpath + "sourcewait.png",
				echowavesrc         : imgpath + "wave.png",
				clickinfoimgsrc     : clickthisimgsrc,
				echoinformationdata : data.string.p6echoinformationrtext1,
			}
		],
	},
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text2part2,
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle applicationheading",
				textdata  : data.string.p6heading2,
			}
		],
		applicationblock : [
			{
				procedure : [
					{
						datahighlightflag : true,
						stepvisibility    : "currentstep",
						proceduredata     : data.string.p6text3part1,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p6text3part2,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p6text3part3,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p6text3part4,
					},
					{
						stepvisibility    : "stepsnext",
						proceduredata     : data.string.p6text3part5,
					},
				],
				firststepimgsrc : imgpath+"step1.png",
			}
		]
	},
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text4,
			}
		],
	},
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text5,
			}
		],
		reverberationobservationblock : [
			{
				reverberationscenesrc        : imgpath + "reverberationscene.png",
				reverberationsrc             : imgpath + "sourcewait.png",
				reverberationwavesrc         : imgpath + "wave.png",
				clickinfoimgsrc              : clickthisimgsrc,
				reverberationinformationdata : data.string.p6echoinformationrtext1,
			}
		],
	},
	{
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p6text6,
			}
		],
		imageblock : [
			{
				imagestoshow : [
					{
						imgclass : "auditoriumimgstyle",
						imgsrc : imgpath+"auditorium.svg"
					},
				],
			}
		],
	},
	{
		uppertextblock : [
			{
				textclass : "onlyparatextstyle",
				textdata  : data.string.p6text7,
			}
		],
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6echovsreverberation,
			}
		],
		differenceblock : [
			{
				leftheadingtext : data.string.p6heading1,
				rightheadingtext : data.string.p6heading3,
				lefttextdetails : [
					{
						lefttextdetaildata : data.string.p6echodiff1,
					}
				],
				righttextdetails : [
					{
						righttextdetaildata : data.string.p6reverberationdiff1,
					}
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6echovsreverberation,
			}
		],
		differenceblock : [
			{
				leftheadingtext : data.string.p6heading1,
				rightheadingtext : data.string.p6heading3,
				lefttextdetails : [
					{
						lefttextdetaildata : data.string.p6echodiff1,
					},
					{
						lefttextdetaildata : data.string.p6echodiff2,
					}
				],
				righttextdetails : [
					{
						righttextdetaildata : data.string.p6reverberationdiff1,
					},
					{
						righttextdetaildata : data.string.p6reverberationdiff2,
					}
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6echovsreverberation,
			}
		],
		differenceblock : [
			{
				leftheadingtext : data.string.p6heading1,
				rightheadingtext : data.string.p6heading3,
				lefttextdetails : [
					{
						lefttextdetaildata : data.string.p6echodiff1,
					},
					{
						lefttextdetaildata : data.string.p6echodiff2,
					}
				],
				righttextdetails : [
					{
						righttextdetaildata : data.string.p6reverberationdiff1,
					},
					{
						righttextdetaildata : data.string.p6reverberationdiff2,
					}
				],
				leftreferenceimage : [
					{
						leftimgsrc : imgpath + "echoeg.png",
						leftimglabel : data.string.p6echoexample,
					}
				],
				rightreferenceimage : [
					{
						rightimgsrc : imgpath + "reverberationeg.png",
						rightimglabel : data.string.p6reverberationexample,
					}
				],
			}
		]
	},
	{
		contentblocknocenteradjust : true,
		headerblock : [
			{
				textclass : "headertextstyle",
				textdata  : data.string.p6echovsreverberation,
			}
		],
		differenceblock : [
			{
				leftheadingtext : data.string.p6heading1,
				rightheadingtext : data.string.p6heading3,
				lefttextdetails : [
					{
						lefttextdetaildata : data.string.p6echodiff1,
					},
					{
						lefttextdetaildata : data.string.p6echodiff2,
					}
				],
				righttextdetails : [
					{
						righttextdetaildata : data.string.p6reverberationdiff1,
					},
					{
						righttextdetaildata : data.string.p6reverberationdiff2,
					}
				],
				leftreferenceimage : [
					{
						leftimgsrc : imgpath + "echoeg.png",
						leftimglabel : data.string.p6echoexample,
					}
				],
				rightreferenceimage : [
					{
						rightimgsrc : imgpath + "reverberationeg.png",
						rightimglabel : data.string.p6reverberationexample,
					}
				],
				leftreferencesound : [
					{
						relatedsoundfilename : "echo", /*you do not provide extension here*/
						examplesoundtext : data.string.p6hearexamplesound,
						examplesoundbuttontext : data.string.p6hearecho,
					}
				],
				rightreferencesound : [
					{
						relatedsoundfilename : "reverberation", /*you do not provide extension here*/
						examplesoundtext : data.string.p6hearexamplesound,
						examplesoundbuttontext : data.string.p6hearreverberation,
					}
				]
			}
		]
	},
	{
		uppertextblock : [
			{
				datahighlightflag : true,
				textclass : "paratextstyle",
				textdata  : data.string.p6text8,
			}
		],
	},
];

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
   	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
   	 Handlebars.registerPartial("listcontent", $("#listcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("applicationcontent", $("#applicationcontent-partial").html());
	 Handlebars.registerPartial("differencecontent", $("#differencecontent-partial").html());
	 Handlebars.registerPartial("echoobservationcontent", $("#echoobservationcontent-partial").html());
	 Handlebars.registerPartial("reverberationobservationcontent", $("#reverberationobservationcontent-partial").html());

	 /*===============================================
	 =            data highlight function            =
	 ===============================================*/
		/**

			What it does:
			- send an element where the function has to see
				for data to highlight
			- this function searches for all nodes whose
			    data-highlight element is set to true
			-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
			- if provided with data-highlightcustomclass value for highlight it
				applies the custom class or else uses parsedstring class

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			//check if $highlightinside is provided
			typeof $highlightinside !== "object" ?
			alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
			null ;

			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var stylerulename;
			var replaceinstring;
			var texthighlightstarttag;
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					/*if there is a data-highlightcustomclass attribute defined for the text element
					use that or else use default 'parsedstring'*/
					$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
						(stylerulename = $(this).attr("data-highlightcustomclass")) :
						(stylerulename = "parsedstring") ;

					texthighlightstarttag = "<span class='"+stylerulename+"'>";
					replaceinstring       = $(this).html();
					replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	/*======================================================
	=            Navigation Controller Function            =
	======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	 /**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	/*=====  End of user navigation controller function  ======*/

	/*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		// find procedure slide if there
		var $applicationblock = $board.find("div.applicationblock");
		// find example sound div in reverberation and echo difference slides
		var $examplesound = $board.find("div.examplesound");
		// echo observation block
		var $echoobservationblock = $board.find("div.echoobservationblock");

		// reverberation observation block
		var $reverberationobservationblock = $board.find("div.reverberationobservationblock");

		//if echo observation block is present
		if($echoobservationblock.length > 0){
			$nextBtn.css('display', 'none');

			var $echocontainer   = $echoobservationblock.find("div.echocontainer");
			var $echowave        = $echocontainer.children('img.echowave');
			var $echosource      = $echocontainer.children('img.echosource');
			var $clickinfoimage  = $echocontainer.children('img.echoclickinfoimage');
			var $echoinformation = $echocontainer.children('p.echoinformation');

			/*decoybutton to make the girl give the original stance after the echo is
    			done coming to her, except this it has no use*/
			var $decoybutton = $echocontainer.children('button#echodecoybutton');

			// echo source images
			var echosourcewaitforit = imgpath+"sourcewait.png";
			var echosourcelisten    = imgpath+"sourcelisten.png";
			var echosourcetalk      = imgpath+"sourcetalk.png";
			var echofinalinfoshown  = false;

			$echosource.on('click', function(event) {
				event.preventDefault();
				$(this).attr("src",echosourcetalk);
				$(this).css({
								"pointer-events":"none",
								'cursor': 'default'
							});
				$echowave.attr('data-echoanimation',"playing");

				$clickinfoimage.css('visibility', 'hidden');

				!echofinalinfoshown ? $echoinformation.css('opacity', '0') : null;

				$decoybutton.css('opacity', '0');
			});

			$echowave.on(animationstart, function() {
				$echosource.attr("src",echosourcewaitforit);
			});

			$echowave.on(animationend, function() {
				$echosource.attr("src",echosourcelisten);
				$echowave.attr('data-echoanimation',"stop");

				// show echo final info and set echofinalinfoshown to true
				!echofinalinfoshown ? (
						$echoinformation.text(data.string.p6echoinformationrtext2),
						$echoinformation.addClass('echofinalinfo'),
						$echoinformation.css("opacity","1"),
						// set echo final info shown is true
						echofinalinfoshown = true,
						$nextBtn.show(0)
					) : null;

				$decoybutton.css('opacity', '1');
			});

			/*the transition timing of decoybutton just gives the extra time space
			for the sound source -girl- to come to its initial state*/
			$decoybutton.on(transitionend, function() {
				$(this).css('opacity') == "1" ? (
						$echosource.attr("src",echosourcewaitforit),
						$echosource.css("pointer-events","auto"),
						$echosource.css('cursor', 'pointer'),
						$clickinfoimage.css('visibility', 'visible')
					):
					null;
			});
		}

		//if echo observation block is present
		if($reverberationobservationblock.length > 0){
			$nextBtn.css('display', 'none');

			var $reverberationcontainer   = $reverberationobservationblock.find("div.reverberationcontainer");
			var $reverberationwave        = $reverberationcontainer.children('img.reverberationwave');
			var $reverberationsource      = $reverberationcontainer.children('img.reverberationsource');
			var $clickinfoimage           = $reverberationcontainer.children('img.reverberationclickinfoimage');
			var $reverberationinformation = $reverberationcontainer.children('p.reverberationinformation');

			/*decoybutton to make the girl give the original stance after the reverberation is
    			done coming to her, except this it has no use*/
			var $decoybutton = $reverberationcontainer.children('button#reverberationdecoybutton');

			// reverberation source images
			var reverberationsourcewaitforit = imgpath+"sourcewait.png";
			var reverberationsourcelisten    = imgpath+"sourcelisten.png";
			var reverberationsourcetalk      = imgpath+"sourcetalk.png";
			var reverberationfinalinfoshown  = false;

			$reverberationsource.on('click', function(event) {
				event.preventDefault();
				$(this).attr("src",reverberationsourcetalk);
				$(this).css({
								"pointer-events":"none",
								'cursor': 'default'
							});
				$reverberationwave.attr('data-reverberationanimation',"playing");

				$clickinfoimage.css('visibility', 'hidden');

				!reverberationfinalinfoshown ? $reverberationinformation.css('opacity', '0') : null;

				$decoybutton.css('opacity', '0');
			});

			$reverberationwave.on(animationstart, function() {
				$reverberationsource.attr("src",reverberationsourcewaitforit);
			});

			$reverberationwave.on(animationend, function() {
				$reverberationsource.attr("src",reverberationsourcelisten);
				$reverberationwave.attr('data-reverberationanimation',"stop");

				// show reverberation final info and set reverberationfinalinfoshown to true
				!reverberationfinalinfoshown ? (
						$reverberationinformation.text(data.string.p6reverberationinformationrtext2),
						$reverberationinformation.addClass('reverberationfinalinfo'),
						$reverberationinformation.css("opacity","1"),
						// set reverberation final info shown is true
						reverberationfinalinfoshown  = true,
						$nextBtn.show(0)
					) : null;

				$decoybutton.css('opacity', '1');
			});

			/*the transition timing of decoybutton just gives the extra time space
			for the sound source -girl- to come to its initial state*/
			$decoybutton.on(transitionend, function() {
				$(this).css('opacity') == "1" ? (
						$reverberationsource.attr("src",reverberationsourcewaitforit),
						$reverberationsource.css("pointer-events","auto"),
						$reverberationsource.css('cursor', 'pointer'),
						$clickinfoimage.css('visibility', 'visible')
					):
					null;
			});
		}

		// if procedure slide is present it has length at least 1 else 0
		if($applicationblock.length > 0){

			$nextBtn.css('display', 'none');
			var $applicationcontainer    = $applicationblock.children('div.applicationcontainer');
			var $procedures              = $applicationcontainer.children('ol.procedures');
			var $currentstep             = $procedures.children("li.currentstep");
			var $currentstepimgcontainer = $applicationcontainer.children('div.currentstepimgcontainer');
			var $currentstepimg          = $currentstepimgcontainer.children('img');
			var $labelcontainer          = $currentstepimgcontainer.children('div.labelcontainer');
			var $nextstepBtn             = $applicationblock.children('div.nextstepBtn');
			var $prevstepBtn             = $applicationblock.children('div.prevstepBtn');
			var totalstepcount           = $procedures.children('li').length;
			var currentstepcount         = 1;
			var currentimgpath;

			// navigation control function for the sub slide
			function sublidenavcontrol(){
				if(currentstepcount == 1){
					$nextBtn.css('display', 'none');
					$prevBtn.show(0);
					$prevstepBtn.css('display', 'none');
					$nextstepBtn.show(0);
				}

				else if(currentstepcount > 1 && currentstepcount < totalstepcount){
					$nextBtn.css('display', 'none');
					$prevBtn.css('display', 'none');
					$prevstepBtn.show(0);
					$nextstepBtn.show(0);
				}

				else if(currentstepcount == totalstepcount){
					$nextBtn.show(0);
					$prevBtn.css('display', 'none');
					$prevstepBtn.show(0);
					$nextstepBtn.css('display', 'none');
				}
			}

			var updatestepimage = function(currentstepcount){
				var imageextension = ".png";
				switch(currentstepcount){
					case 3 : imageextension = ".gif"; break;
					default : imageextension = ".png"; break;
				}
				return currentstepcount+imageextension;
			};

			var updatesteplabels = function(currentstepcount){
				var labeldataarray = [];
				var labelstoshow = "";
				var labelcount = 0;
				switch(currentstepcount){
					case 1 :
					case 2 : labeldataarray.push( data.string.p6step1label1,
												  data.string.p6step1label2 );
							 break;
					case 4 : labeldataarray.push( data.string.p6step4label1 );
							 break;
					case 5 : labeldataarray.push( data.string.p6step5label1 );
							 break;
					default : break;
				}

				/*if labeldataarray has some values update labels for the respective
				step else pass a null string such that labelcontainer becomes empty*/
				labeldataarray.length > 0 ? (
					$.each(labeldataarray, function(index, labeltext) {
						labelcount = index+1;
						labelstoshow += "<label class='step"+currentstepcount+"label"
										+labelcount+"'>"+labeltext+"</label>";
					}),
					$labelcontainer.html(labelstoshow)
				) : $labelcontainer.html("");
			};

			// call update label at first load
			updatesteplabels(currentstepcount);

			// event handler for next step button
			$nextstepBtn.on('click', function() {
				$currentstep.attr('class','stepsprevious');
				$currentstep = $currentstep.next();
				$currentstep.attr("class", "currentstep");
				currentstepcount++;
				// update respective image
				$currentstepimg.attr('src', imgpath+"step"+updatestepimage(currentstepcount));
				// update respective labels
				updatesteplabels(currentstepcount);
				sublidenavcontrol();
			});

			// event handler for previous step button
			$prevstepBtn.on('click', function() {
				$currentstep.attr('class','stepsnext');
				$currentstep = $currentstep.prev();
				$currentstep.attr("class", "currentstep");
				currentstepcount--;
				// update respective image
				$currentstepimg.attr('src', imgpath+"step"+updatestepimage(currentstepcount));
				// update respective labels
				updatesteplabels(currentstepcount);
				sublidenavcontrol();
			});
		}

		// for sound playing for echo and reverberation difference
		// in the difference slide
		if ($examplesound.length > 1){
			$nextBtn.css('display', 'none');
			var $soundbuttons = $examplesound.find("button");

			var playthissound;
			var soundsplayedcount = 0;

			$soundbuttons.on('click', function(event) {
				soundsplayedcount++;

				// get the filename that needs to be played and update playthissound
				playthissound = updatesound($(this).attr("data-relatedsound"));

				// stop all sounds at first
				buzz.all().stop();

				// and play the selected sound
				playthissound.play();

				// show next button when both sounds in page is heard
				soundsplayedcount < 2 ? null : $nextBtn.show(0);
			});
		}
		else{
			// stop all sounds for all other slides
			buzz.all().stop();
		};
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developer : "+countNext+" / "+($total_page-1)+"</span>");
	}

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

var imgpath = $ref+"/images/page1/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
		uppertextblock : [
			{
				textclass : "polybgtextstyle", /*specific for the header text*/
				textdata   : data.string.p10_title,
			}
		]
	},
	{
    	contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "headertextstyle",
    			textdata: data.string.p10_para1
    		}
    	],

    	uppertextblock:[
    		{
    			datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
    			textclass: "paraquestion",
    			textdata: data.string.p10_para2
    		},
    		{
    			textclass: "solution",
    			textdata: data.string.p10_para3
    		},
    		{
		        textclass: "densitybig cssfadein",
		        textdata: data.string.p9_para15
		      },
		      {
		        textclass: "massbig cssfadein2",
		        textdata: data.string.p9_para16
		      },
		      {
		        textclass: "underline cssfadein2"
		      },
		      {
		        textclass: "volumebig cssfadein2",
		        textdata: data.string.p9_para17
		      },
		      {
		      	textclass: "equal",
		      	textdata: data.string.p9_para15
		      },
		      {
		      	textclass: "underline2"
		      },
		      {
		      	textclass: "errmsg",
		      	textdata: data.string.p10_para4
		      },
		      {
		      	textclass: "answer",
		      	textdata: data.string.p10_para5
		      },
		      {
		      	datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
		      	textclass: "ansdef",
		      	textdata: data.string.p10_para6
		      }

    	],
    	inputblock:[
    		{
    			inputid: "massdata"
    		},
    		{
    			inputid: "volumedata"
    		}
    	]
	},
	{
		uppertextblock:[
			{
				textclass: "paratextstyle",
				textdata: data.string.p10_para7
			},
			{
				textclass: "textstyle cssfadein",
				textdata: data.string.p10_para8
			}
		]
	},
	{
    	contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "headertextstyle",
    			textdata: data.string.p10_para9
    		}
    	],

    	uppertextblock:[
    		{
    			datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
    			textclass: "paraquestion",
    			textdata: data.string.p10_para10
    		},
    		{
    			textclass: "solution",
    			textdata: data.string.p10_para3
    		},
    		{
		        textclass: "densitybig cssfadein",
		        textdata: data.string.p10_para11
		      },
		      {
		        textclass: "massbig cssfadein2",
		        textdata: data.string.p9_para16
		      },
		      {
		        textclass: "underline cssfadein2"
		      },
		      {
		        textclass: "volumebig cssfadein2",
		        textdata: data.string.p10_para12
		      },
		      {
		      	textclass: "equal",
		      	textdata: data.string.p10_para11
		      },
		      {
		      	textclass: "underline2"
		      },
		      {
		      	textclass: "errmsg",
		      	textdata: data.string.p10_para4
		      },
		      {
		      	textclass: "answer",
		      	textdata: data.string.p10_para13
		      },
		      {
		      	datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
		      	textclass: "ansdef",
		      	textdata: data.string.p10_para6
		      }

    	],
    	inputblock:[
    		{
    			inputid: "massdata"
    		},
    		{
    			inputid: "densitydata2"
    		}
    	]
	},
	{
    	contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "headertextstyle",
    			textdata: data.string.p10_para14
    		}
    	],

    	uppertextblock:[
    		{
    			datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
    			textclass: "paraquestion",
    			textdata: data.string.p10_para15
    		},
    		{
    			textclass: "solution",
    			textdata: data.string.p10_para3
    		},
    		{
		        textclass: "densitybig cssfadein",
		        textdata: data.string.p10_para16
		      },
		      {
		        textclass: "massbig2 cssfadein2",
		        textdata: data.string.p9_para17
		      },
		      {
		        textclass: "multiply cssfadein2",
		        textdata: data.string.p10_para17
		      },
		      {
		        textclass: "volumebig2 cssfadein2",
		        textdata: data.string.p10_para12
		      },
		      {
		      	textclass: "equal",
		      	textdata: data.string.p10_para16
		      },
		     {
		        textclass: "multiply2",
		        textdata: data.string.p10_para17
		      },
		      {
		      	textclass: "errmsg",
		      	textdata: data.string.p10_para4
		      },
		      {
		      	textclass: "answer",
		      	textdata: data.string.p10_para18
		      },
		      {
		      	datahighlightflag: true,
				datahighlightcustomclass : "multiplehighlights",
		      	textclass: "ansdef",
		      	textdata: data.string.p10_para6
		      }

    	],
    	inputblock:[
    		{
    			inputid: "volumedata2"
    		},
    		{
    			inputid: "densitydata3"
    		}
    	]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("inputcontent", $("#inputcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html());
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());

		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class

				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;

				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**
			How to:
			- First set any html element with
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/
	 /**
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that
	 		footernotification is called for continue button to navigate to exercise
	  */

	/**
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is
	   */

	 function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ?
	 	islastpageflag = false :
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true
			islastpageflag ?
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

	 /*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**
			How to:
			- Just call instructionblockcontroller() from the template
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);
				});
			}
		}
	/*=====  End of InstructionBlockController  ======*/

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0) {
			$nextBtn.show(0);
		};

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		$(".textstyle").on(animationend, function(){
			$nextBtn.show(0);
		});

		$(".volumebig").on(animationend, function(){

			$(".equal").addClass("cssfadein");
			$(".underline2, #massdata, #volumedata").addClass("cssfadein2");

			$("#massdata, #volumedata").keypress(function (e) {
			     //if the letter is not digit then display error and don't type anything
			     if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			        //display error message
			        $(".errmsg").show(0).fadeOut("slower");
			        return false;
			    }
	  		});

	  		var count=0;
			$("#massdata").change(function(){
			var massvalue = $("#massdata").val();
				if (massvalue ==45) {
					$("#massdata").css("color", "#006600");
					$("#massdata").css("border", "#fff");
					count ++;
					// alert(count);
				}
				else{
					$("#massdata").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
					$(".ansdef").addClass("cssfadein2");
					$nextBtn.delay(2500).show(100);
				};
			});

			$("#volumedata").change(function(){
			var volumevalue = $("#volumedata").val();
				if (volumevalue ==15) {
					$("#volumedata").css("border", "#fff");
					$("#volumedata").css("color", "#006600");
					count ++;
					// alert(count);

				}
				else{
					$("#volumedata").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
					$nextBtn.delay(2500).show(100);
					$(".ansdef").addClass("cssfadein2");
				};
			});


		});


		// find if there is linehorizontal div in the slide
		// var $linehorizontal = $board.find("div.linehorizontal");
		// if($linehorizontal.length > 0)
		// {
		// 	$linehorizontal.attr('data-isdrawn', 'draw');
		// }
	}

	function generaltemplate2() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		$(".volumebig").on(animationend, function(){

			$(".equal").addClass("cssfadein");
			$(".underline2, #massdata, #densitydata2").addClass("cssfadein2");

			$("#massdata, #densitydata2").keypress(function (e) {
			     //if the letter is not digit then display error and don't type anything
			     if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			        //display error message
			        $(".errmsg").show(0).fadeOut("slower");
			        return false;
			    }
	  		});

	  		var count=0;
			$("#massdata").change(function(){
			var massvalue = $("#massdata").val();
				if (massvalue ==45) {
					$("#massdata").css("color", "#006600");
					$("#massdata").css("border", "#fff");
					count ++;
					// alert(count);
				}
				else{
					$("#massdata").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
					$(".ansdef").addClass("cssfadein2");
					$nextBtn.delay(2500).show(100);
				};
			});

			$("#densitydata2").change(function(){
			var densityvalue = $("#densitydata2").val();
				if (densityvalue ==3) {
					$("#densitydata2").css("border", "#fff");
					$("#densitydata2").css("color", "#006600");
					count ++;
					// alert(count);

				}
				else{
					$("#densitydata2").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
					$nextBtn.delay(2500).show(100);
				};
			});


		});


		// find if there is linehorizontal div in the slide
		// var $linehorizontal = $board.find("div.linehorizontal");
		// if($linehorizontal.length > 0)
		// {
		// 	$linehorizontal.attr('data-isdrawn', 'draw');
		// }
	}

	function generaltemplate3() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		$(".volumebig2").on(animationend, function(){

			$(".equal").addClass("cssfadein");
			$(".multiply2, #volumedata2, #densitydata3").addClass("cssfadein2");

			$("#volumedata2, #densitydata3").keypress(function (e) {
			     //if the letter is not digit then display error and don't type anything
			     if (e.which != 8 && e.which != 0 && e.which != 13 && (e.which < 48 || e.which > 57)) {
			        //display error message
			        $(".errmsg").show(0).fadeOut("slower");
			        return false;
			    }
	  		});

	  		var count=0;
			$("#volumedata2").change(function(){
			var volumevalue = $("#volumedata2").val();
				if (volumevalue ==15) {
					$("#volumedata2").css("color", "#006600");
					$("#volumedata2").css("border", "#fff");
					count ++;
					// alert(count);
				}
				else{
					$("#volumedata2").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
					$nextBtn.delay(2500).show(100);
				};
			});

			$("#densitydata3").change(function(){
			var densityvalue2 = $("#densitydata3").val();
				if (densityvalue2 ==3) {
					$("#densitydata3").css("border", "#fff");
					$("#densitydata3").css("color", "#006600");
					count ++;
					// alert(count);

				}
				else{
					$("#densitydata3").css("color", "#990000");
				}
				if (count == 2) {
					$(".answer").addClass("cssfadein");
				};
			});
		});
	}

/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		// call navigation controller
		navigationcontroller();

		// call the template


		/*OR, call templates like this if you have more than one template
		to call*/
		switch(countNext){
			case 0 : generaltemplate(); break;
			case 1 : generaltemplate(); break;
			case 2 : generaltemplate(); break;
			case 3 : generaltemplate2(); break;
			case 4 : generaltemplate3(); break;
			default : break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of
			previous slide button hide the footernotification */
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});

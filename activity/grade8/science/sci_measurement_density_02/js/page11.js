var imgpath = $ref+"/images/page11/";
var imgpathnew = $ref+"/images/page11/new/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{
		uppertextblock:[
			{
				textclass: "polybgtextstyle",
				textdata: data.string.p12_title
			}
		]
	},
	{	
		headerblock:[
			{
				textclass:"headertextstyle",
				textdata: data.string.p12_board
			}
		],
		imageblock : [
			{
				imagestoshow:[
					{
						imgclass:"sqboard",
						imgsrc:imgpath+"board.png"
					}
				]
			}
		],
		textlistblock:[
			{
				listtype:[
					{
						listtypedata: data.string.p12_board1						
					},
					{
						listtypedata: data.string.p12_board2						
					}
				]
			}
		]	
	},
	{
		contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "headertextstyle",
    			textdata: data.string.p12_para1
    		},
    		{
    			textclass: "secondheader",
    			textdata: data.string.p12_para2
    		}
    	],
    	
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass:"icecube",
    					imgsrc:imgpathnew+"icecube.png"
    				},
    				{
    					imgclass:"nail",
    					imgsrc:imgpathnew+"nail.png"
    				},
    				{
    					imgclass:"clickmenail",
    					imgsrc: "images/clickme_icon.png"
    				},
    				{
    					imgclass:"clickmeice",
    					imgsrc: "images/clickme_icon.png"
    				}
    			]
    		}
    	],
    	lowertextblock:[
    		{
    			textclass:"waterlevel"
    		},
    		{
    			textclass:"waterwater"
    		}
    	],
	},
	{
		contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "headertextstyle",
    			textdata: data.string.p12_para3
    		}
    	],
    	uppertextblock:[
    		{
    			datahighlightflag:true,
    			textclass:"freshwatertxt",
    			textdata: data.string.p12_para4
    		}
    	],
    	imageblock:[
    		{
			  imagestoshow:[
					{
						imgclass:"egg",
						imgsrc: imgpathnew+"egg.png"
					},					
					{
						imgclass:"clickmeegg",
						imgsrc: "images/clickme_icon.png"
					}
			    ]
    		}
    	],
    	lowertextblock:[
    		{
    			textclass:"waterlevel2"
    		},
    		{
    			textclass:"waterwaterwater"
    		}
    	],
	},
	{
		contentnocenteradjust:true,
    	headerblock:[
    		{
    			textclass: "thirdheader",
    			textdata: data.string.p12_para3_1
    		}
    	],
    	uppertextblock:[    		
    		{
    			datahighlightflag:true,
    			textclass:"saltwatertxt",
    			textdata: data.string.p12_para5
    		}
    	],
    	imageblock:[
    		{
			  imagestoshow:[
					{
						imgclass:"egg1",
						imgsrc: imgpathnew+"egg.png"
					},					
					{
						imgclass:"clickmeagain",
						imgsrc: "images/clickme_icon.png"
					}
			    ]
    		}
    	],
    	lowertextblock:[
    		{
    			textclass:"saltwaterlevel"
    		},
    		{
    			textclass:"saltwaterwater"
    		}
    	],
    	
	},
	{
		diysplashblock:[
			{
				diysplashImageSource:"images/lokharke/2.png",
				diysplashTextData: data.string.p12_diy
			}
		]
	},
	{
		contentnocenteradjust:true,
    	diyblock:[
    		{
    			diyImageSource:"images/lokharke/2.png",
    			diyTextData: data.string.p12_diy
    		}
    	],
    	uppertextblock:[
    		{
    			textclass: "question",
    			textdata: data.string.p12_diy_para2
    		},
    		{
    			textclass: "option1",
    			textdata: data.string.p12_diy_para4
    		},
    		{
    			textclass: "option2",
    			textdata: data.string.p12_diy_para5
    		},

    	],
    	imageblock:[
    		{
    			imagestoshow:[
    				{
    					imgclass:"boat",
    					imgsrc:imgpath+"boat.png"
    				}
    			]
    		}
    	]
	},
	{
		contentnocenteradjust:true,
    	diyblock:[
    		{
    			diyImageSource:"images/lokharke/2.png",
    			diyTextData: data.string.p12_diy
    		}
    	],
    	uppertextblock:[
    		{
    			textclass:"question",
    			textdata: data.string.p12_diy_para6
    		},
    	],
    	imageblock:[
    		{
    			imagelist:[
					{
						imgclass:"erazer sink",
						imgsrc: imgpath+"erazer.png"
					},
					{
						imgclass:"feather",
						imgsrc: imgpath+"feather.png"
					},
					{
						imgclass:"toy",
						imgsrc: imgpath+"toy.png"
					},
					{
						imgclass:"marble sink",
						imgsrc: imgpath+"marble.png"
					},
					{
						imgclass:"leaf",
						imgsrc: imgpath+"leaf.png"
					},
					{
						imgclass:"paperpin sink",
						imgsrc: imgpath+"paper-pin.png"
					},
					{
						imgclass:"stone sink",
						imgsrc: imgpath+"stone.png"
					}
    			]
    		}
    	],
    	lowertextblock:[
    		{
    			textclass:"waterlevel3"
    		},
    		{
    			textclass:"waterwater3"
    		}
    	]

	}
	
];


$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
	 Handlebars.registerPartial("instructioncontent", $("#instructioncontent-partial").html());
	 Handlebars.registerPartial("cardcontent", $("#cardcontent-partial").html());
	 Handlebars.registerPartial("usageslowrevealcontent", $("#usageslowrevealcontent-partial").html());
	 Handlebars.registerPartial("timetothinkcontent", $("#timetothinkcontent-partial").html());
	 Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());
	 Handlebars.registerPartial("usagecontent", $("#usagecontent-partial").html());
	 Handlebars.registerPartial("propertycontent", $("#propertycontent-partial").html());
	 Handlebars.registerPartial("propertysummarycontent", $("#propertysummarycontent-partial").html());
	 Handlebars.registerPartial("diysplashcontent", $("#diysplashcontent-partial").html()); 
	 Handlebars.registerPartial("diycontent", $("#diycontent-partial").html());
	 Handlebars.registerPartial("definitioncontent", $("#definitioncontent-partial").html());
	 
		 /*===============================================
		 =            data highlight function            =
		 ===============================================*/
			/**

				What it does:
				- send an element where the function has to see
				for data to highlight
				- this function searches for all nodes whose
				data-highlight element is set to true 
				-searches for # character and gives a start tag
				;span tag here, also for @ character and replaces with
				end tag of the respective
				- if provided with data-highlightcustomclass value for highlight it
				  applies the custom class or else uses parsedstring class
				
				E.g: caller : texthighlight($board);
			 */
			function texthighlight($highlightinside){
				//check if $highlightinside is provided
				typeof $highlightinside !== "object" ?
				alert("Texthighlight : Hi Master, Please pass me a Jquery Object whose child are to be highlighted") :
				null ;
					
				var $alltextpara = $highlightinside.find("*[data-highlight='true']");
				var stylerulename;
				var replaceinstring;
				var texthighlightstarttag;
				var texthighlightendtag   = "</span>";
				if($alltextpara.length > 0){
					$.each($alltextpara, function(index, val) {	
						/*if there is a data-highlightcustomclass attribute defined for the text element
						use that or else use default 'parsedstring'*/				
						$(this).attr("data-highlightcustomclass") ? /*if there is data-highlightcustomclass defined it is true else it is not*/
							(stylerulename = $(this).attr("data-highlightcustomclass")) :
							(stylerulename = "parsedstring") ;

						texthighlightstarttag = "<span class='"+stylerulename+"'>";
						replaceinstring       = $(this).html();
						replaceinstring       = replaceinstring.replace(/#/g,texthighlightstarttag);
						replaceinstring       = replaceinstring.replace(/@/g,texthighlightendtag);
						$(this).html(replaceinstring);
					});
				}
			}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		 =            user notification function        =
		 ===============================================*/
		/**		
			How to:
			- First set any html element with 
				"data-usernotification='notifyuser'" attribute,
			and "data-isclicked = ''".
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			//check if $notifyinside is provided
			typeof $notifyinside !== "object" ?
			alert("Notifyuser : Hi Master, Please pass me a Jquery Object which should contain notification style.") :
			null ;

			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

	 /*======================================================
	 =            Navigation Controller Function            =
	 ======================================================*/	 	 
	 /**	 
	 	How To:
	 	- Just call the navigation controller if it is to be called from except the
	 	  last page of lesson
	 	- If called from last page set the islastpageflag to true such that 
	 		footernotification is called for continue button to navigate to exercise
	  */
	
	/**	  
	  	What it does:
	  	- If not explicitly overriden the method for navigation button
	  	  controls, it shows the navigation buttons as required,
	  	  according to the total count of pages and the countNext variable
	  	- If for a general use it can be called from the templatecaller
	  	  function
	  	- Can be put anywhere in the template function as per the need, if 
	  	  so should be taken out from the templatecaller function
	  	- If the total page number is 
	   */  
	 
	function navigationcontroller(islastpageflag){
	 	// check if the parameter is defined and if a boolean,
	 	// update islastpageflag accordingly
	 	typeof islastpageflag === "undefined" ? 
	 	islastpageflag = false : 
	 		typeof islastpageflag != 'boolean'?
	 		alert("NavigationController : Hi Master, please provide a boolean parameter") :
	 		null;

	 	if(countNext == 0 && $total_page!=1){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if($total_page == 1){
			$prevBtn.css('display', 'none');
			$nextBtn.css('display', 'none');
			
			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;			
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);

			// if lastpageflag is true 
			islastpageflag ? 
				ole.footerNotificationHandler.lessonEndSetNotification() :
					ole.footerNotificationHandler.pageEndSetNotification() ;
		}
	 }
	 /*=====  End of user navigation controller function  ======*/

	 /*==================================================
	=            InstructionBlockController            =
	==================================================*/
		/**		
			How to:
			- Just call instructionblockcontroller() from the template		
		 */

		/**
			What it does:
			- It inserts and handles closing and opening of instruction block
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which 
			 removes the notification style.
		*/
		function instructionblockcontroller(){
			var $instructionblock = $board.find("div.instructionblock");
			if($instructionblock.length > 0){
				var $contentblock = $board.find("div.contentblock");
				var $toggleinstructionblockbutton = $instructionblock.find("div.toggleinstructionblock");
				var instructionblockisvisibleflag;

				$contentblock.css('pointer-events', 'none');

				$toggleinstructionblockbutton.on('click', function() {
					instructionblockisvisibleflag = $instructionblock.attr("data-instructionblockshow");
					if(instructionblockisvisibleflag == 'true'){
						instructionblockisvisibleflag = 'false';
						$contentblock.css('pointer-events', 'auto');
					}
					else if(instructionblockisvisibleflag == 'false'){
						instructionblockisvisibleflag = 'true';
						$contentblock.css('pointer-events', 'none');
					}

					$instructionblock.attr("data-instructionblockshow" , instructionblockisvisibleflag);					
				});
			}
		}	
	/*=====  End of InstructionBlockController  ======*/
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/
	/*=================================================
	=            general template function            =
	=================================================*/		
	function generaltemplate() {
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0 || countNext==1 ||countNext==5) {
			$nextBtn.show(0);
		};
				
		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		//call instruction block controller
		// instructionblockcontroller($board);

		//call notifyuser
		// notifyuser($anydiv);

		$(".clickmeice").on("click", function(){ 
			$(this).hide(0);
			$(".icecube").addClass("slideagaindown");
			$(".waterwater").addClass("cssfadeinout");
		});

		$(".waterwater").on(animationend, function(){ 
			$(".icecube").delay(1500).hide(0);
			$(".headertextstyle").delay(1000).hide(0);
			$(".secondheader").addClass("cssfadein");
			$(".nail, .clickmenail").delay(1500).show(0);
		});

		$(".clickmenail").on("click", function(){ 
			$(this).hide(0);
			$(".nail").addClass("slidedownnail");
			$(".waterup").addClass("cssfadein");
			$nextBtn.delay(1000).show(0);
		});

		$(".clickmeegg").on("click", function(){ 
			$(this).hide(0);
			$(".egg").addClass("slidedownegggg");
			$(".waterwaterwater").addClass("cssfadeinout");
			$(".freshwatertxt").addClass("cssfadein");
		});

		$(".waterwaterwater").on(animationend, function(){ 
			$nextBtn.show(0);
		});

		$(".clickmeagain").on("click", function(){ 
			$(this).hide(0);
			$(".egg1").addClass("slidedownegg");
			$(".saltwaterwater").addClass("cssfadeinout2");
			$(".saltwatertxt").addClass("cssfadein");
			$nextBtn.delay(1000).show(0);
		});

		$(".option1").on("click", function(){ 
			$(this).css("background","#F8735D");
		});

		$(".option2").on("click", function(){ 
			$(this).css("background","#8FC557");
			$nextBtn.show(0);
		});
		if (countNext==7) {
			$(".imagelist").addClass("slidefromright");
			$(".imageblock ").css("z-index", "1001");
		}else{
			$(".imageblock ").css("z-index", "");
		}

		var count1 = false;
		var count2 = false;
		var count3 = false;
		var count4 = false;
		$(".erazer").on("click", function(){ 
			$(".erazer").addClass("slidedownimg");
			count1 = true;
		});	

		$(".marble").on("click", function(){ 
			$(".marble").addClass("marblesinkanim");
			count2 = true;
		});

		$(".paperpin").on("click", function(){ 
			$(".paperpin").addClass("pinsinkanim");
			count3 = true;
		});

		$(".stone").on("click", function(){ 
			$(".stone").addClass("stonesinkanim");
			count4 = true;
		});
        
            if (count1 = true && count2 == true && count3 ==true && count4 ==true) {
                $(".feather").addClass("imgfloatanim");
                $(".toy").addClass("toyfloatanim");
                $(".leaf").addClass("leaffloatanim");
            };
	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');
		
		// call navigation controller
		navigationcontroller();

		// call the template
		generaltemplate();

		/*OR, call templates like this if you have more than one template
		to call*/
		// switch(countNext){
		// 	case 0 : generaltemplate(); break;
		// 	case 1 : generaltemplate(); break;
		// 	case 2 : generaltemplate(); break;
		// 	case 3 : svgtemplate(); break;
		// 	default : break;
		// }

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);

		// just for development purpose to see total slide vs current slide number
		// $board.append("<span id='slidecount' style='position:absolute;top:0px;left:0px;'>"+"Developers : "+countNext+" / "+($total_page-1)+"</span>");
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();

		/* if footerNotificationHandler pageEndSetNotification was called then on click of 
			previous slide button hide the footernotification */		
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
	});

/*=====  End of Templates Controller Block  ======*/
});
var imgpath = $ref+"/images/page6/";

var content=[
    {
        typeoflayout  : "layoutsoilerosionprevention",
        textonlyblock : [
            {
                textclass : "nohighlightbigtext",
                textdata  : data.string.p6text1,
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        onlyimageblock : [
            {
                actbgimagesrc : imgpath+"afforestation.png",
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"afforestation.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6afforestation,
                    }
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"afforestation.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6afforestation,
                    },
                    {
                        textclass : "simpletextstyle",
                        textdata : data.string.p6afforestationexplain,
                    }
                ],
                explainimageblock : [
                    {
                        actbgimagesrc : imgpath+"afforestation1.png",
                    },
                    {
                        actbgimagesrc : imgpath+"afforestation2.png",
                    },
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        onlyimageblock : [
            {
                actbgimagesrc : imgpath+"damconstruction.png",
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"damconstruction.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6damconstruction,
                    }
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"damconstruction.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6damconstruction,
                    },
                    {
                        textclass : "simpletextstyle",
                        textdata : data.string.p6damconstructionexplain,
                    }
                ],
                explainimageblock : [
                    {
                        actbgimagesrc : imgpath+"damconstruction1.png",
                    },
                    {
                        actbgimagesrc : imgpath+"damconstruction2.png",
                    },
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        onlyimageblock : [
            {
                actbgimagesrc : imgpath+"embankment.png",
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"embankment.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6embankment,
                    }
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"embankment.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6embankment,
                    },
                    {
                        datahighlightflag : true,
                        textclass : "simpletextstyle",
                        textdata : data.string.p6embankmentexplain,
                    }
                ],
                explainimageblock : [
                    {
                        actbgimagesrc : imgpath+"embankment1.png",
                    },
                    {
                        actbgimagesrc : imgpath+"embankment2.png",
                    },
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        onlyimageblock : [
            {
                actbgimagesrc : imgpath+"farming.png",
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"farming.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6farming,
                    }
                ]
            },
        ],
    },
    {
        typeoflayout  : "layoutsoilerosionprevention",
        explainblock : [
            {
                actbgimagesrc : imgpath+"farming.png",
                explaintextblock : [
                    {
                        textclass : "explaintextheading",
                        textdata : data.string.p6farming,
                    },
                    {
                        datahighlightflag : true,
                        textclass : "simpletextstyle",
                        textdata : data.string.p6farmingexplain,
                    }
                ],
                explainimageblock : [
                    {
                        actbgimagesrc : imgpath+"farming1.png",
                    },
                    {
                        actbgimagesrc : imgpath+"farming2.png",
                    },
                ]
            },
        ],
    },

];

$(function () {
    var $board    = $('.board');
    var $nextBtn = $("#activity-page-next-btn-enabled");
    var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
    var countNext = 0;

    var $total_page = content.length;
    loadTimelineProgress($total_page,countNext+1);

    /*==========  register the handlebar partials first  ==========*/
    Handlebars.registerHelper("indexbaseone", function(value){return value+1});
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

    /*==========  navigation controller function  ==========*/
     function navigationcontroller(){
        if(countNext == 0){
            $nextBtn.show(0);
            $prevBtn.css('display', 'none');
        }
        else if(countNext > 0 && countNext < $total_page-1){
            $nextBtn.show(0);
            $prevBtn.show(0);
        }
        else if(countNext == $total_page-1){
            $nextBtn.css('display', 'none');
            $prevBtn.show(0);
            ole.footerNotificationHandler.pageEndSetNotification();
        }
     }

        /*===============================================
        =            data highlight function            =
        ===============================================*/
        /**

            What it does:
            - send an element where the function has to see
            for data to highlight
            - this function searches for all text nodes whose
            data-highlight element is set to true
            -searches for # character and gives a start tag
            ;span tag here, also for @ character and replaces with
            end tag of the respective

         */
        function texthighlight($highlightinside){
            var $alltextpara = $highlightinside.find("*[data-highlight='true']");
            var replaceinstring;
            var texthighlightstarttag = "<span class='parsedstring'>";
            var texthighlightendtag   = "</span>";
            if($alltextpara.length > 0){
                $.each($alltextpara, function(index, val) {
                    // console.log(val);
                    replaceinstring = $(this).html();
                    replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
                    replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
                    $(this).html(replaceinstring);
                });
            }
        }
        /*=====  End of data highlight function  ======*/

        /*===============================================
        =            user notification function            =
        ===============================================*/
        /**
            How to:
            - First set any html element with "data-usernotification='notifyuser'" attribute
            - Then call this function to give notification
         */

        /**
            What it does:
            - You send an element where the function has to see
            for data to notify user
            - this function searches for all text nodes whose
            data-usernotification attribute is set to notifyuser
            - applies event handler for each of the html element which
             removes the notification style.
         */
        function notifyuser($notifyinside){
            /*variable that will store the element(s) to remove notification from*/
            var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
             // if there are any notifications removal required add the event handler
            if($allnotifications.length > 0){
                $allnotifications.one('click', function() {
                    /* Act on the event */
                    $(this).removeAttr('data-usernotification');
                });
            }
        }
        /*=====  End of user notification function  ======*/


    /*====================================================
    =            soilerosionprevention template function            =
    ====================================================*/
    function soilerosionprevention(){
        var source   = $("#soilerosionprevention-template").html();
        var template = Handlebars.compile(source);
        var html     = template(content[countNext]);
        $board.html(html);
        navigationcontroller();

        /* highlight the para with attribute data-highlight equals true */
        texthighlight($board);

         // call notifyuser to +add an event handler to the same
        // notifyuser($board);
    }

    /*=====  End of soilerosionprevention template function  ======*/

    soilerosionprevention();

    $nextBtn.on('click',function () {
        $(this).css("display","none");
        countNext++;
        soilerosionprevention();
        loadTimelineProgress($total_page,countNext+1);
    });

    $refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
        $(this).css("display","none");
        countNext--;
        soilerosionprevention();
        countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
        loadTimelineProgress($total_page,countNext+1);
    });
});

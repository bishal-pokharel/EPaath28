var imgpath = $ref+"/images/page4/";

var content=[

//slide0
	{	
		typeoflayout  : "layoutsoilerosion",
		textimageblock : [
			{
				actbgimagesrc : imgpath+"soilerosion.png", 
                textblock : [
                	{
						textclass : "whitesuperhighlight",
						textdata  : data.string.p4text2,	
                	}
                ]
				
			},
		],
	},
	
	//slide1
	{	
		typeoflayout  : "layoutsoilerosion",
		textimageblock : [
			{
				actbgimagesrc : imgpath+"soilerosion.png", 
				
                textblock : [
                	{
						textclass : "whitesuperhighlight",
						textdata  : data.string.p4text1,	
                	}
                ]
				
			},
		],
	},
	
	//slide2
	{	
		typeoflayout  : "layoutsoilerosion",
		sidebarblock : [
			{
				imgaddclass: "sidewidth",
				actbgimagesrc : imgpath+"soilerosion1.png",
				sidebartext : [
					{
						datahighlightflag : true,
						textclass         : "sidebarorangehighlight",
						textdata          : data.string.p4erosionexplain1,
					},
				],
			},			
		],

	},
	//slide3
	{	
		typeoflayout  : "layoutsoilerosion",
		sidebarblock : [
			{
				imgaddclass: "sidewidth",
				actbgimagesrc : imgpath+"soilerosion2.png",
				sidebartext : [
					{	
						datahighlightflag : true,
						textclass         : "sidebarsimplehighlight",
						textdata          : data.string.p4erosionexplain1,
					},
					{
						textclass : "sidebarorangehighlight",
						textdata  : data.string.p4erosionexplain2,
					},
				],
			},			
		],

	},	
	
	//slide4
	{	
		typeoflayout  : "layoutsoilerosion",
		sidebarblock : [
			{
				imgaddclass: "sidewidth",
				actbgimagesrc : imgpath+"soilerosion3.png",
				sidebartext : [
					
					{
						textclass : "sidebarorangehighlight",
						textdata  : data.string.p4erosionexplain3,
					},
					
				],
			},			
		],

	},
	
	//slide5
	{	
		typeoflayout  : "layoutsoilerosion",
		sidebarblock : [
			{
				imgaddclass: "sidewidth",
				actbgimagesrc : imgpath+"soilerosion4.png",
				sidebartext : [
					
					{
						textclass : "sidebarsimplehighlight",
						textdata  : data.string.p4erosionexplain3,
					},
					{
						textclass : "sidebarorangehighlight",
						textdata  : data.string.p4erosionexplain4,
					},
				],
			},			
		],

	},
];

$(function () {	
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/	
	Handlebars.registerHelper("indexbaseone", function(value){return value+1});
	Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());

	/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/


	/*====================================================
	=            soilerosion template function            =
	====================================================*/
	function soilerosion(){
		var source   = $("#soilerosion-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		navigationcontroller();

		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);

		 // call notifyuser to +add an event handler to the same
		// notifyuser($board);
	}

	/*=====  End of soilerosion template function  ======*/
	
	soilerosion();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;		
		soilerosion();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		soilerosion();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});
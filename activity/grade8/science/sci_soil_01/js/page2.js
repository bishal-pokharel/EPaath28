var imgpath = $ref+"/images/page2/";
var clickicon = "images/hand-icon.gif";

var content=[

//slide0
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal",
		contentitle: data.string.p2text1,
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",

			},
			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text2,
			},
		],

		/*
		textlistblock : [
			{
				listtype : [
					{
						textcontentheading : [
												{
													textdata : data.string.p2listheading,
												}
											]
					},
					{
						listsubtype : [
							data.string.p2listsubtype1,
							data.string.p2listsubtype2,
							data.string.p2listsubtype3,
							data.string.p2listsubtype4,
						]
					},
				],
			}
		],
		*/
	},

	//slide1

		{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal",
		contentitle: data.string.p2text1,
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",

			},
			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text3,
			},
		],

		/*
		textlistblock : [
			{
				listtype : [
					{
						textcontentheading : [
												{
													textdata : data.string.p2listheading,
												}
											]
					},
					{
						listsubtype : [
							data.string.p2listsubtype1,
							data.string.p2listsubtype2,
							data.string.p2listsubtype3,
							data.string.p2listsubtype4,
						]
					},
				],
			}
		],
		*/
	},

	//slide2

	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal",
		contentitle: data.string.p2text1,
		uppertextblock : [
			{
				textclass : "bigheadingtextstyle",

			},
			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text4,
			},
		],


	},

	//slide3
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal2",
		addtionaltextblockclass:"addwidth",
		contentitle: data.string.p2text1,
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text5,
			},
		],



	},
	//slide3
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal2",
		addtionaltextblockclass:"addwidth",
		loweraddtionaltextblockclass: "soildescription",
		contentitle: data.string.p2text1,
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text5,
			},
		],
		lowertextblock: [

			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type1
			}
		],



	},
	//slide4 new one
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal2",
		addtionaltextblockclass:"addwidth",
		loweraddtionaltextblockclass: "soildescription",
		contentitle: data.string.p2text1,
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text5,
			},
		],
		lowertextblock: [

			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type1
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type2
			}
		],


	},
//slide4 new one
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal2",
		loweraddtionaltextblockclass: "soildescription",
		addtionaltextblockclass:"addwidth",
		contentitle: data.string.p2text1,
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text5,
			},
		],
		lowertextblock: [

			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type1
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type2
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type3
			}
		],



	},
//slide4 new one
	{

		contentblockadditionalclass : "contentwithbg",
		typeoflayout: "layouthorizontal2",
		loweraddtionaltextblockclass: "soildescription",
		addtionaltextblockclass:"addwidth",
		contentitle: data.string.p2text1,
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text5,
			},
		],
		lowertextblock: [

			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type1
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type2
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type3
			},
			{
				highlighttext : false,
				textclass : "descriptiontextstyle",
				addtionaltextclass:"sidecontent",
				textdata: data.string.p2text5type4
			}
		],



	},


	//slide8

	{

		contentblockadditionalclass : "contentwithbg",

		typeoflayout: "layouthorizontal1 layouthorizontal",
		contentitle: data.string.p2text1,
		addtionaltextblockclass:"addwidth2",
		uppertextblock : [

			{
				highlighttext : true,
				textclass : "simpletextstyle",
				addtionaltextclass:"sidecontent",
				textdata : data.string.p2text6,
			},
		],



	},

	//


	{
		typeoflayout: "layoutlayersoil",
		layer : [
			{
				layerimgsrc : imgpath+"soillayera.png",
				layerexplain : [
					{
						listtype : [
							{
								textcontentheading : [
									{
										textdata : data.string.p2listsubtype1,
									}
								],
								listsubtype : [
									data.string.p2horizonAlist1,
									data.string.p2horizonAlist2,
									data.string.p2horizonAlist3,
									data.string.p2horizonAlist4,
								]
							},
						]
					}
				],
			},
			{
				layerimgsrc : imgpath+"soillayerb.png",
				layerexplain : [
					{
						listtype : [
							{
								textcontentheading : [
									{
										textdata : data.string.p2listsubtype2,
									}
								],
								listsubtype : [
									data.string.p2horizonBlist1,
									data.string.p2horizonBlist2,
									data.string.p2horizonBlist3,
								]
							},
						]
					}
				],
			},
			{
				layerimgsrc : imgpath+"soillayerc.png",
				layerexplain : [
					{
						listtype : [
							{
								textcontentheading : [
									{
										textdata : data.string.p2listsubtype3,
									}
								],
								listsubtype : [
									data.string.p2horizonClist1,
									data.string.p2horizonClist2,
									data.string.p2horizonClist3,
								]
							},
						]
					}
				],
			},
			{
				layerimgsrc : imgpath+"soillayerd.png",
				layerexplain : [
					{
						listtype : [
							{
								textcontentheading : [
									{
										textdata : data.string.p2listsubtype4,
									}
								],
								listsubtype : [
									data.string.p2bedrocklist1,
									data.string.p2bedrocklist2,
									data.string.p2bedrocklist3,
								],
							},
						],
					},
				],
			},
		],
	},
	{
		typeoflayout: "layoutsoilprofile",
		soilprofileheading : [
			{
				textclass : "soilprofileheading",
				textdata : data.string.p2soilprofileheading,
			}
		],
		soilprofileimgsrc : imgpath+"soillayer.png",
		layer : [
			{ clickiconimagesrc : clickicon,},
			{ clickiconimagesrc : clickicon,},
			{ clickiconimagesrc : clickicon,},
			{ clickiconimagesrc : clickicon,},
		]
	},

];

soilprofilesubcontent = [
	{
		listtype : [
					{
						textcontentheading : [
							{
								textdata : data.string.p2overviewinstruction,
							}
						]
					}
				]
	},
	{
		listtype : [
						{
							textcontentheading : [
								{
									textdata : data.string.p2listsubtype1,
								}
							],
							listsubtype : [
								data.string.p2horizonAlist1,
								data.string.p2horizonAlist2,
								data.string.p2horizonAlist3,
								data.string.p2horizonAlist4,
							]
						},
					]
	},
	{
		listtype : [
						{
							textcontentheading : [
								{
									textdata : data.string.p2listsubtype2,
								}
							],
							listsubtype : [
								data.string.p2horizonBlist1,
								data.string.p2horizonBlist2,
								data.string.p2horizonBlist3,
							]
						},
					]
	},
	{
		listtype : [
						{
							textcontentheading : [
								{
									textdata : data.string.p2listsubtype3,
								}
							],
							listsubtype : [
								data.string.p2horizonClist1,
								data.string.p2horizonClist2,
								data.string.p2horizonClist3,
							]
						},
					]
	},
	{
		listtype : [
						{
							textcontentheading : [
								{
									textdata : data.string.p2listsubtype4,
								}
							],
							listsubtype : [
								data.string.p2bedrocklist1,
								data.string.p2bedrocklist2,
								data.string.p2bedrocklist3,
							],
						},
					],
	},
]

$(function () {
	var $board    = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/
	 Handlebars.registerHelper("indexbaseone", function(value){	return value+1; });
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());

	/*==========  navigation controller function  ==========*/
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

	/*==========  general template caller  ==========*/
	function general(){
		var source = $("#general-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);

		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);
	}

	/*==========  layersoil template caller  ==========*/
	function layersoil(){
		var source   = $("#layersoil-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		/* highlight the para with attribute data-highlight equals true */
		texthighlight($board);

		// override navigationcontroller to hide the next button
		$nextBtn.css('display', 'none');

		var $layoutlayersoil = $board.children('div.layoutlayersoil');
		var $nextlayerBtn    = $layoutlayersoil.children("div.nextlayer").html(data.string.next);
		var $prevlayerBtn    = $layoutlayersoil.children("div.prevlayer").html(data.string.prev);
		var $containerframe  = $layoutlayersoil.children('div.containerframe');
		var $layer1          = $containerframe.children('div.layerlevel1');
		var $layer2          = $containerframe.children('div.layerlevel2');
		var $layer3          = $containerframe.children('div.layerlevel3');
		var $layer4          = $containerframe.children('div.layerlevel4');
		var presentlayer     = 1;
		var totlayer         = content[countNext].layer.length-1;

		function layernavigationcontroller(){
		 	if(presentlayer == 1){
		 		$nextlayerBtn.show(0);
				$nextBtn.css("display","none");
				$prevBtn.show(0);
			}
			else if(presentlayer > 0 && presentlayer < totlayer){
				$nextBtn.css("display","none");
				$prevBtn.css("display","none");
				$prevlayerBtn.show(0);
			}
			else if(presentlayer == totlayer){
				$nextBtn.show(0);
				$prevBtn.css("display","none");
                $prevlayerBtn.hide(0);
                $nextlayerBtn.hide(0);
			}
	 	}

		$layer1.attr("data-slidestatus","presentslide");
		// first call to layernavigation controller
		layernavigationcontroller();

		$nextlayerBtn.on('click', function() {
			/* Act on the event */
			$containerframe.children('div.layerlevel'+presentlayer).attr("data-slidestatus","pastslide");
			$containerframe.children('div.layerlevel'+(presentlayer+1)).attr("data-slidestatus","presentslide");
			presentlayer++;
			layernavigationcontroller();
		});

		$prevlayerBtn.on('click', function() {
			/* Act on the event */
			$containerframe.children('div.layerlevel'+presentlayer).attr("data-slidestatus","futureslide");
			$containerframe.children('div.layerlevel'+(presentlayer-1)).attr("data-slidestatus","presentslide");
			presentlayer--;
			layernavigationcontroller();
		});
	}

	/*==========  soilprofile template caller  ==========*/
	function soilprofile(){
		var source   = $("#soilprofile-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);

		/* highlight the para with attribute data-highlight equals true */
		// texthighlight($board);

		var $layoutsoilprofile = $board.children('div.layoutsoilprofile');
		var $containerframe    = $layoutsoilprofile.children('div.containerframe');
		var $layers            = $containerframe.children("div[class^='layerlevel']");
		var $layerexplain      = $containerframe.children("div.layerexplain");
		var presentlayer       = 0;
		// subtemplate caller for soil profile
		function soilprofilesubtemplate(count){
			var source   = $("#soilprofile-subtemplate").html();
			var template = Handlebars.compile(source);
			var html     = template(soilprofilesubcontent[count]);
			$layerexplain.html(html);

			/* highlight the para with attribute data-highlight equals true */
			texthighlight($board);
		}

		soilprofilesubtemplate(0);

		$layers.on('click', function() {
			$layers.attr("data-eventmouse","unhovered");
			soilprofilesubtemplate(parseInt($(this).attr("data-layercount")));
			$(this).attr("data-eventmouse","hovered");
		});
	}

	/* for test and development cause comment it out during deployment */
	// countNext+=2

	/* call this for the first time to show the navigation button as needed */
	navigationcontroller();

	/* now call the first template to be shown when the page load */
	general();
	// layersoil();
	// soilprofile();

	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		/* call this for the first time to show the navigation button as needed */
		navigationcontroller();
		switch(countNext){
			case 1  : general(); break;
			case 2  : general(); break;
			case 3  : general(); break;
			case 4  : general(); break;
			case 5  : general(); break;



			case 6  : general(); break;
			case 7  : general(); break;
			case 8  : general(); break;

			case 9  : layersoil(); break;
			case 10  : soilprofile(); break;
			default : break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		/* call this for the first time to show the navigation button as needed */
		navigationcontroller();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;

		switch(countNext){
			case 0  : general(); break;
			case 1  : general(); break;
			case 2  : general(); break;
			case 3  : general(); break;
			case 4  : general(); break;
			case 5  : general(); break;


			case 6  : general(); break;
			case 7  : general(); break;
			case 8  : general(); break;
			case 9  : layersoil(); break;

			/*case 10  : layersoil(); break;*/
			default : break;
		}
		loadTimelineProgress($total_page,countNext+1);
	});
});

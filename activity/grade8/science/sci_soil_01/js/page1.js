var $board;
var $element;
var $text;

// function to move an element right
function moveright($writelement){
	var writeElementwidthinpercent = ($writelement.width()/$board.width())*100;
	var writeElementleftinpercent = ($writelement.position().left/$board.width())*100;
	$element.css({
    left: function( index, value ) {
      var updateleft = parseInt(value)+15;
      var updateleftinpercent = (updateleft/$board.width())*100;
      var updateleftpercent = updateleftinpercent+"%";
      if(updateleftinpercent < writeElementwidthinpercent){
      	return updateleftpercent;
      }
      else if(updateleftinpercent >= writeElementwidthinpercent){
      	return writeElementleftinpercent;
      }
    }
  });
}

// function for typewriter, for a id-passed as string, given some data, speed of typing-optional
// and a callback function -optional to call on complete
function typewriter($writelement, oncomplete, speed){
	var str = $writelement.html(),
    i = 0,
    text,
    typespeed,
    typevariable,
    strlen = str.length;

    $writelement.html();
    $writelement.css("opacity","1");
    // set the typespeed if provided set as it is or speed = 0 is default as 40, else give default speed 50
    if (typeof speed === "undefined" || speed === 0) {
			typespeed = 60;
		} else {
			typespeed = speed;
		}

	// function to type
	(function type() {
	    text = str.slice(0, ++i);

	    $writelement.html(text);
	    moveright($writelement);

	    if (text === str) {
	    	/*if oncomplete function is provided call it and return
	    		else just return*/
	    	if (typeof oncomplete === "undefined") {
				clearInterval(typevariable);
			} else {
				oncomplete();
				clearInterval(typevariable);
			}
	    	return;
	    }

        typevariable = setTimeout(type, typespeed);
	}());
};

var imgpath = $ref+"/images/page1/";

var content=[
	{
		characterwritebgimgsrc : imgpath+"coverpage.png",
		characterwritetext : [
			{
				text : data.lesson.chapter
			}
	 ]
	},
	{
		characterwalk:true,
		characterwritebgimgsrc : imgpath+"soilbg.png",
		characterwritetext : [
								{
									text : data.string.p1text1
								},
								{
									text : data.string.p1text2
								},
								{
									text : data.string.p1text3
								},
								{
									datahighlightflag : true,
									text : data.string.p1text4
								},
							 ]
	},
];

$(function () {
	$board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	/*==========  register the handlebar partials first  ==========*/
	 // Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());


	/*==========  navigation controller function  ==========*/
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

		/*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

	/*==========  properties template caller  ==========*/

	function characterwrite(){
		var source = $("#characterwrite-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		// navigationcontroller();
		$nextBtn.css('display', 'none');

		var $characterwrite              = $board.children('div.characterwrite');
		var $character                   = $characterwrite.children('div.character');
		var $characterwritetextchildren = $characterwrite.children('p[class^="text"]');
		var $text0                       = $characterwrite.children('.text0');
		var $text1                       = $characterwrite.children('.text1');
		var $text2                       = $characterwrite.children('.text2');
		var $text3                       = $characterwrite.children('.text3');

		$characterwritetextchildren.css('opacity', '0');
		$element     = $character;
		var $text    = $text0;
		var position = $text.position();
		var top      = position.top-($character.height() * 0.5);

		$character.css({"top":top,"left":position.left});
		typewriter($text,type2text);

		function type2text(){
			$text    = $text1;
			position = $text.position();
			top      = position.top-($character.height() * 0.5);

			$character.css({"top":top,"left":position.left});

			typewriter($text,type3text);
		}

		function type3text(){
			$text    = $text2;
			position = $text.position();
			top      = position.top-($character.height() * 0.5);

			$character.css({"top":top,"left":position.left});

			typewriter($text,type4text);
		}

		function type4text(){
			$text    = $text3;
			position = $text.position();
			top      = position.top-($character.height() * 0.5);

			$character.css({"top":top,"left":position.left});

			typewriter($text,stopcharacter);
		}

		function stopcharacter(){
			$element.attr('role', 'stop');
			/* highlight the para with attribute data-highlight equals true */
			var replacetext3 = $text3.html();
			replacetext3
			texthighlight($board);

			ole.footerNotificationHandler.pageEndSetNotification();
		}
	}

	characterwrite();
	if(countNext==0){
		$nextBtn.show();
		$("p.text0").css({
				"top": "10%",
				"background": "none",
				"color": "#000",
				"font-family": "comicSans",
				"left": "40%",
				"font-size": "5vh"
			}
		);
	}
	$nextBtn.on('click',function () {
		$(this).css("display","none");
		countNext++;
		characterwrite();
		loadTimelineProgress($total_page,countNext+1);
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		$(this).css("display","none");
		countNext--;
		characterwrite();
		countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		loadTimelineProgress($total_page,countNext+1);
	});
});

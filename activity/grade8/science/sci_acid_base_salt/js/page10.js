var imgpath = $ref+"/images/page11/";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";

var content=[
	{	
		typeoflayout : "vertical-horizontal-center",
		
		uppertextblock:[
			{
				textclass:"middleheading",
				textdata: data.string.p11_heading1
			}
		]
	},
	{
		typeoflayout: "vertical-horizontal-center",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p1_4
			},
			{
				textclass: "para1",
				textdata: data.string.p11_def1
			}
		]
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para2
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para2_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para3
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage",
        				imgsrc: imgpath+"salt_pouch.png"
        			}
        		]
        	}
        ]	
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para4
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para4_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para5
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage",
        				imgsrc: imgpath+"fertilizer.png"
        			}
        		]
        	}
        ]	
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para6
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para6_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para7
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage2",
        				imgsrc: imgpath+"antacid.png"
        			}
        		]
        	}
        ]	
	},

	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para8
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para8_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para9
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage",
        				imgsrc: imgpath+"chalk.png"
        			}
        		]
        	}
        ]	
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para10
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para10_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para11
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage",
        				imgsrc: imgpath+"soap.png"
        			}
        		]
        	}
        ]	
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading2
			},
			{
				textclass: "sideheadingstyle",
				textdata: data.string.p11_heading3
			}
		],

		middletextblock:[
            {
                textclass: "chemtitle1 swipebackgroundstyle",
                textdata: data.string.p11_para12
            },
            {
                textclass: "chemtitle2 swipebackgroundstyle1",
                textdata: data.string.p11_para12_1
            },
            {
                textclass: "chemtitle3 swipebackgroundstyle2",
                textdata: data.string.p11_para13
            }
        ],

        imageblock:[
        	{
        		imagestoshow:[
        			{
        				imgclass:"sideimage1",
        				imgsrc: imgpath+"cement01.png"
        			}
        		]
        	}
        ]	
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading4
			},
		],
		middletextblock:[
			{
				textclass:"cardcontent showfromleft",
				textdata: data.string.p11_para14
			},
			{
				textclass:"cardcontent showfromleft",
				textdata: data.string.p11_para15
			},
			{
				textclass:"cardcontent showfromleft",
				textdata: data.string.p11_para16
			},
			{
				textclass:"cardcontent showfromleft",
				textdata: data.string.p11_para17
			},
		]
	},
	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading5
			},
			{
				textclass:"middleheading2",
				textdata: data.string.p11_para18
			},
			{
				textclass: "sideheading1 leftleft",
				textdata: data.string.p11_name1
			}
		],

		imagetextblock:[
			{
				textclass: "chem1",
				textdata: data.string.p11_ch
			},
			{
				textclass: "plussign",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "chem2",
				textdata: data.string.p11_ch2
			},
			{
				textclass: "arrow_sign",
				textdata: data.string.p11_ch2_arrow
			},
			{
				textclass: "reac1",
				textdata: data.string.p11_ch_3
			},
			{
				textclass: "reacplus",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "reac2",
				textdata: data.string.p11_ch_1
			}
		],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"element1",
                        imgsrc:imgpath+"acid.png"
                    },
                    {
                        imgclass: "plus",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "element2",
                        imgsrc:imgpath+"beakergrey.png"
                    },                    
                    {
                        imgclass: "arrow",
                        imgsrc:imgpath+"arrow.png"
                    },
                    {
                        imgclass: "reaction1",
                        imgsrc:imgpath+"salt.png"
                    },
                    {
                        imgclass: "plus2",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "reaction2",
                        imgsrc: imgpath+"water.png"                    },
                                
                ],               
            },
        ],

        lowertextblock:[
            {
                textclass:"metal1info",
                textdata: data.string.p11_last1
            },
        ]         
	},

	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading5
			},
			{
				textclass:"middleheading2",
				textdata: data.string.p11_para18
			},
			{
				textclass: "sideheading1 leftleft",
				textdata: data.string.p11_name2
			}
		],

		imagetextblock:[
			{
				textclass: "chem1",
				textdata: data.string.p11_ch1
			},
			{
				textclass: "plussign",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "chem2",
				textdata: data.string.p11_ch2_1
			},
			{
				textclass: "arrow_sign",
				textdata: data.string.p11_ch2_arrow
			},
			{
				textclass: "reac1",
				textdata: data.string.p11_ch3
			},
			{
				textclass: "reacplus",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "reac2",
				textdata: data.string.p11_ch5
			}
		],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"element1",
                        imgsrc:imgpath+"sodium.png"
                    },
                    {
                        imgclass: "plus",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "element2",
                        imgsrc:imgpath+"acid.png"
                    },                    
                    {
                        imgclass: "arrow",
                        imgsrc:imgpath+"arrow.png"
                    },
                    {
                        imgclass: "reaction1",
                        imgsrc:imgpath+"salt.png"
                    },
                    {
                        imgclass: "plus2",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "reaction2",
                        imgsrc: imgpath+"animation.gif"                    },
                                
                ],               
            },
        ],

        lowertextblock:[
            {
                textclass:"metal1info",
                textdata: data.string.p11_last2
            },
            
        ]         
		
	},

	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading5
			},
			{
				textclass:"middleheading2",
				textdata: data.string.p11_para18
			},
			{
				textclass: "sideheading1 leftleft",
				textdata: data.string.p11_name3
			}
		],

		imagetextblock:[
			{
				textclass: "chem1",
				textdata: data.string.p11_ch1_1
			},
			{
				textclass: "plussign",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "chem2",
				textdata: data.string.p11_ch3_1
			},
			{
				textclass: "arrow_sign",
				textdata: data.string.p11_ch2_arrow
			},
			{
				textclass: "reac1",
				textdata: data.string.p11_ch6
			},
			{
				textclass: "reacplus",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "reac2",
				textdata: data.string.p11_ch_1
			}
		],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"element1",
                        imgsrc:imgpath+"beaker.png"
                    },
                    {
                        imgclass: "plus",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "element2",
                        imgsrc:imgpath+"acid_beaker.png"
                    },                    
                    {
                        imgclass: "arrow",
                        imgsrc:imgpath+"arrow.png"
                    },
                    {
                        imgclass: "reaction1",
                        imgsrc:imgpath+"salt.png"
                    },
                    {
                        imgclass: "plus2",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "reaction2",
                        imgsrc: imgpath+"water.png"                    },
                                
                ],               
            },
        ],

        lowertextblock:[
            {
                textclass:"metal1info",
                textdata: data.string.p11_last3
            },
            
        ]         
		
	},

	{
		typeoflayout: "generallayout",

		uppertextblock:[
			{
				textclass: "middleheading",
				textdata: data.string.p11_heading5
			},
			{
				textclass:"middleheading2",
				textdata: data.string.p11_para18
			},
			{
				textclass: "sideheading2 leftleft",
				textdata: data.string.p11_name4
			}
		],

		imagetextblock:[
			{
				textclass: "chem_1",
				textdata: data.string.p11_ch1_2
			},
			{
				textclass: "plussigns",
				textdata: data.string.p11_ch1_plus
			},
			{
				textclass: "chem_2",
				textdata: data.string.p11_ch2_2
			},
			{
				textclass: "arrow_signs",
				textdata: data.string.p11_ch2_arrow
			},
			{
				textclass: "reac1s",
				textdata: data.string.p11_ch3_2
			}
		],

        imageblock:[
            {
                imagestoshow:[
                    {
                        imgclass:"element1",
                        imgsrc:imgpath+"sodium.png"
                    },
                    {
                        imgclass: "plus",
                        imgsrc:imgpath+"plus.png"
                    }, 
                    {
                        imgclass: "element2",
                        imgsrc:imgpath+"beakergrey.png"
                    },                    
                    {
                        imgclass: "arrow",
                        imgsrc:imgpath+"arrow.png"
                    },
                    {
                        imgclass: "reaction1",
                        imgsrc:imgpath+"salt.png"
                    }                                
                ],               
            },
        ],

        lowertextblock:[
            {
                textclass:"metal2info",
                textdata: data.string.p11_last4
            },
        ]         
	},
	{
    typeoflayout: "vertical-horizontal-center",
       uppertextblock:[
            {
                textclass: "headingstyle2",
                textdata: data.string.p2_12
            }
        ],
        middletextblock:[
            {
                textclass:"headingmetal1",
                textdata: data.string.p11_lasttitle1
            },
            {
                textclass:"headingmetal2",
                textdata: data.string.p11_lasttitle2
            },
            {
                textclass:"headingmetal3",
                textdata: data.string.p11_lasttitle3
            },
            {
                textclass:"headingmetal4",
                textdata: data.string.p11_lasttitle3
            },
        ],

         lowertextblock:[
            {
                textclass:"metal_1 cssfadein",
                textdata: data.string.p11_lower1
            },
            {
                textclass:"metal_2 cssfadein",
                textdata: data.string.p11_last2
            },
            {
                textclass:"metal_3 cssfadein",
                textdata: data.string.p11_last3
            },
            {
                textclass:"metal_4 cssfadein",
                textdata: data.string.p11_last4
            }
        ]    
    }  

];

$(function () {	
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

	

/*==================================================
=            Handlers and helpers Block            =
==================================================*/
   /*==========  register the handlebar partials first  ==========*/	
	 Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
	 Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());


	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true 
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective
			
			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**		
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification		
		 */
		
		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser 
			- applies event handler for each of the html element which 
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/	
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/	 
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }
	
/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

	function curvedmirrorusage(){
		var source = $("#curvedmirrorusage-template").html();
		var template = Handlebars.compile(source);
		var html = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0 || countNext==1) {
			$nextBtn.show(0);
		};

		//data highlighter
		texthighlight($board);

		// user notification
		notifyuser($board);

		$(".chemtitle3").on(animationend, function() { 
			$(".sideimage").css("opacity", "1");
			$(".sideimage2").css("opacity", "1");
			$(".sideimage1").css("opacity", "1");
			$nextBtn.show(0);
		});

		$(".cardcontent").on(animationend, function() { 
			$nextBtn.show(0);
		});

		$(".sideheading1").on(animationend, function(){ 
			$(".element1, .plus, .element2, .arrow, .reaction1, .plus2, .reaction2").addClass("cssfadein");
			$(".chem1, .plussign, .chem2, .arrow_sign, .reac1, .reacplus, .reac2").addClass("cssfadein3");
			$(".metal1info").addClass("cssfadein2");
		});

		$(".metal1info").on(animationend, function(){ 
			$nextBtn.show(0);
		});

		$(".sideheading2").on(animationend, function(){ 
			$(".element1, .plus, .element2, .arrow, .reaction1").addClass("cssfadein");
			$(".chem_1, .plussigns, .chem_2, .arrow_signs, .reac1s").addClass("cssfadein3");
			$(".metal2info").addClass("cssfadein2");
		});

		$(".metal2info").on(animationend, function(){ 
			$nextBtn.show(0);
		});


	}
	
/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the 
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated	
	 */
	
	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/ 
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		// if footerNotificationHandler pageEndSetNotification was called then
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;
		
		// call the template
		curvedmirrorusage();

		switch(countNext){
            case 0: curvedmirrorusage(); 
                     break;
            case 1: curvedmirrorusage();
                     break;
            case 2: curvedmirrorusage();
                     break;
            case 3: curvedmirrorusage();
                     break;
            case 4: curvedmirrorusage();
                     break;
            case 5: curvedmirrorusage();
                     break;
            case 6: curvedmirrorusage();
                     break;
            case 7: curvedmirrorusage();
                     break;
            case 8: curvedmirrorusage();
                     break;
            case 9: curvedmirrorusage();
                     break;
            case 10: curvedmirrorusage();
                     break;
            case 11: curvedmirrorusage();
                     break;
                default:break;
            }

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);	
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */
	
	$nextBtn.on('click',function () {
		countNext++;		
		templateCaller();		
	});

	$refreshBtn.click(function(){
		templateCaller();
	});
	
	$prevBtn.on('click',function () {
		countNext--;			
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/
});

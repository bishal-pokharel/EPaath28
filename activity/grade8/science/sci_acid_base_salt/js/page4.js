var transitionend = "webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend";
var animationend = "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
var imgpath = $ref+"/images/page4/";
var squirrelears = "images/lokharke/important_content_animated.svg";

var content=[
	{
		typeoflayout   : "svglayout",
		imageblock:[
			{

				imagestoshow:[
					{
						imgclass : "squirrelears",
						imgsrc   : squirrelears,
					}
				]
			}
		],
		lowertextblock:[
			{
				textclass: "titlebellowsquirrel",
				textdata: data.string.p4_title
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass: "headingstyle1",
				textdata: data.string.p4_title2
			}
		],

		imageblock:[
			{
				imagestoshow:[
					{
						imgclass: "threeimgtogether turnaround1",
						imgsrc: imgpath+"01.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass : "lable1 turnaround1",
						imagelabeldata : data.string.p4_i1,
					}
				]
			},

			{
				imagestoshow:[
								{
									imgclass: "threeimgtogether turnaround2",
									imgsrc: imgpath+"02.png"
								}
							],
							imagelabels:[
								{
									imagelabelclass : "lable2 turnaround2",
									imagelabeldata : data.string.p4_i2,
								}
							]
			},
			{

				imagestoshow:[
					{
						imgclass: "threeimgtogether turnaround3",
						imgsrc: imgpath+"05.png"
					}
				],
				imagelabels:[
					{
						imagelabelclass : "lable3 turnaround3",
						imagelabeldata : data.string.p4_i3,
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle2 slideanim",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li1
					}
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"01.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li1
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li2
					}
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"02.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li1
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li2
					},
					{
						datahighlightflag:true,
						listtypedata: data.string.p4_li3
					}
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"03.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li1
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li2
					},
					{
						datahighlightflag:true,
						listtypedata: data.string.p4_li3
					},
					{
						datahighlightflag:true,
						listtypedata: data.string.p4_li4
					},
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"04.png"
					}
				]
			}
		]
	},

	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				startnum: "5",
				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li5
					},

				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"05.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				startnum: "5",

				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li5
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li6
					},

				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"06.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				startnum: "5",

				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li5
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li6
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li7
					},
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"08.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				startnum: "5",

				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li5
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li6
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li7
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li8
					},
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"10.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p4_title3
			}
		],

		textlistblock:[
			{
				olclassname:"procedure1",
				startnum: "5",

				listtype:[
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li5
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li6
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li7
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li8
					},
					{
						datahighlightflag: true,
						listtypedata: data.string.p4_li9
					},
				]
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"sideimage",
						imgsrc:imgpath+"09.png"
					}
				]
			}
		]
	},
	{
		typeoflayout   : "svglayout",
		uppertextblock:[
			{
				textclass:"headingstyle3",
				textdata: data.string.p2_12
			}
		],
		imageblock:[
			{
				imagestoshow:[
					{
						imgclass:"fiveimgtogether",
						imgsrc:imgpath+"03.png"
					},
					{
						imgclass:"arro1",
						imgsrc: imgpath+"arrow.png"
					},
					{
						imgclass:"fiveimgtogether",
						imgsrc:imgpath+"04.png"
					},
					{
						imgclass:"arro2",
						imgsrc: imgpath+"arrow.png"
					},
					{
						imgclass:"fiveimgtogether",
						imgsrc:imgpath+"06.png"
					},
					{
						imgclass:"arro3",
						imgsrc: imgpath+"arrow.png"
					},
					{
						imgclass:"fiveimgtogether",
						imgsrc:imgpath+"acid-and-base.png",
					},
					{
						imgclass:"arro4",
						imgsrc: imgpath+"arrow.png"
					},
					{
						imgclass:"fiveimgtogether",
						imgsrc:imgpath+"08.png"
					},
				],
				imagelabels:[
						{
							imagelabelclass:"acid",
							imagelabeldata:data.string.p4_sl11acid,
						},
						{
							imagelabelclass:"base",
							imagelabeldata:data.string.p4_sl11base,
						},
				]
			}
		],
		lowertextblock:[
			{
				textclass:"lastpara",
				textdata: data.string.p4_li10
			}
		]
	}

];


$(function () {
	var $board = $('.board');
	var $nextBtn = $("#activity-page-next-btn-enabled");
	var $prevBtn = $("#activity-page-prev-btn-enabled");
  var $refreshBtn= $("#activity-page-refresh-btn");
	var countNext = 0;

	var $total_page = content.length;
	loadTimelineProgress($total_page,countNext+1);

/*==================================================
=            Handlers and helpers Block            =
==================================================*/

	// register the handlebar partials first
    Handlebars.registerPartial("textcontent", $("#textcontent-partial").html());
    Handlebars.registerPartial("imagecontent", $("#imagecontent-partial").html());
    Handlebars.registerPartial("svgcontent", $("#svgcontent-partial").html());
	Handlebars.registerPartial("textlistcontent", $("#textlistcontent-partial").html());

	 /*===============================================
		=            data highlight function            =
		===============================================*/
		/**

			What it does:
			- send an element where the function has to see
			for data to highlight
			- this function searches for all text nodes whose
			data-highlight element is set to true
			-searches for # character and gives a start tag
			;span tag here, also for @ character and replaces with
			end tag of the respective

			E.g: caller : texthighlight($board);
		 */
		function texthighlight($highlightinside){
			var $alltextpara = $highlightinside.find("*[data-highlight='true']");
			var replaceinstring;
			var texthighlightstarttag = "<span class='parsedstring'>";
			var texthighlightendtag   = "</span>";
			if($alltextpara.length > 0){
				$.each($alltextpara, function(index, val) {
					// console.log(val);
					replaceinstring = $(this).html();
					replaceinstring = replaceinstring.replace(/#/g,texthighlightstarttag);
					replaceinstring = replaceinstring.replace(/@/g,texthighlightendtag);
					$(this).html(replaceinstring);
				});
			}
		}
		/*=====  End of data highlight function  ======*/

		/*===============================================
		=            user notification function            =
		===============================================*/
		/**
			How to:
			- First set any html element with "data-usernotification='notifyuser'" attribute
			- Then call this function to give notification
		 */

		/**
			What it does:
			- You send an element where the function has to see
			for data to notify user
			- this function searches for all text nodes whose
			data-usernotification attribute is set to notifyuser
			- applies event handler for each of the html element which
			 removes the notification style.
		 */
		function notifyuser($notifyinside){
			/*variable that will store the element(s) to remove notification from*/
			var $allnotifications = $notifyinside.find("*[data-usernotification='notifyuser']");
			 // if there are any notifications removal required add the event handler
			if($allnotifications.length > 0){
				$allnotifications.one('click', function() {
					/* Act on the event */
					$(this).attr('data-isclicked', 'clicked');
					$(this).removeAttr('data-usernotification');
				});
			}
		}
		/*=====  End of user notification function  ======*/

		/*==========  navigation controller function  ==========*/
	 function navigationcontroller(){
	 	if(countNext == 0){
			$nextBtn.show(0);
			$prevBtn.css('display', 'none');
		}
		else if(countNext > 0 && countNext < $total_page-1){
			$nextBtn.show(0);
			$prevBtn.show(0);
		}
		else if(countNext == $total_page-1){
			$nextBtn.css('display', 'none');
			$prevBtn.show(0);
			ole.footerNotificationHandler.pageEndSetNotification();
		}
	 }

/*=====  End of Handlers and helpers Block  ======*/

/*=======================================
=            Templates Block            =
=======================================*/

		/*=================================================
		=            svg template function            =
		=================================================*/
	function svgtemplate(){
		var source   = $("#svg-template").html();
		var template = Handlebars.compile(source);
		var html     = template(content[countNext]);
		$board.html(html);
		$nextBtn.hide(0);

		if (countNext==0) {
			$nextBtn.show(0);
		};

		$(".lable3").on(animationend, function(){
			$nextBtn.show(0);
		});
		$(".headingstyle2").on(animationend, function(){
			$nextBtn.show(0);
		});

		if (countNext >2 && countNext <11) {
			$nextBtn.show(0);
		};

		//notify user call
		notifyuser($board);

		// highlight any text inside board div with datahighlightflag set true
		texthighlight($board);

		var $svgwrapper              = $board.children('div.svglayout').children('div.svgwrapper');
		var $svgcontainer            = $svgwrapper.children('div.svgcontainer');
		var $imagecharacteristicsbox = $svgwrapper.children('div.imagecharacteristicsbox');
		var $startanimbutton         = $svgwrapper.children('div.startanimbutton');

		if($svgcontainer.length > 0){
			var s = Snap(".svgcontainer");
			Snap.load("./images/important_content_animated.svg", function (f) {
    		s.append(f);
    			// snap objects
    		// var redray          = s.select("#redray");
    		// var blueray         = s.select("#blueray");
    		// var arrowgroup      = s.select("#arrowgroup");
    		// var raydiagramimage = s.select("#raydiagramimage");

    			// jquery objects and js variables
    		// var $svg     = $svgcontainer.children('svg');
    		// var $rays    = $svg.children("[id$='ray']");
    		// var $blueray = $svg.children('#blueray');


 		});
		}
	}


/*=====  End of Templates Block  ======*/

/*==================================================
=            Templates Controller Block            =
==================================================*/

	/*==================================================
	=            function to call templates            =
	==================================================*/
	/**
		Motivation :
		- Make a single function call that handles all the
		  template load easier

		How To:
		- Update the template caller with the required templates
		- Call template caller

		What it does:
		- According to value of the Global Variable countNext
			the slide templates are updated
	 */

	function templateCaller(){
		/*always hide next and previous navigation button unless
		explicitly called from inside a template*/
		$prevBtn.css('display', 'none');
		$nextBtn.css('display', 'none');

		navigationcontroller();
		// if footerNotificationHandler pageEndSetNotification was called then
		// countNext < $total_page ? ole.footerNotificationHandler.hideNotification() : null;

		// On the basis of countNext value call templates
		switch(countNext){
			case 0:
					 svgtemplate();
					 break;
			case 1:
					 svgtemplate();
					 break;
			case 2:
					 svgtemplate();
					 break;
			case 3:
					 svgtemplate();
					 break;
			case 4:
					 svgtemplate();
					 break;
			case 5:
					 svgtemplate();
					 break;
			case 6:
					 svgtemplate();
					 break;
			case 7:
					 svgtemplate();
					 break;
			case 8:
					 svgtemplate();
					 break;
			case 9:
					 svgtemplate();
					 break;
			case 10:
					 svgtemplate();
					 break;
			case 11:
					 svgtemplate();
					 break;
			default:break;
		}

		//call the slide indication bar handler for pink indicators
		loadTimelineProgress($total_page,countNext+1);
	}

	/*this countNext variable change here is solely for development phase and
	should be commented out for deployment*/
	// countNext+=1;

	// first call to template caller
	templateCaller();

	/* navigation buttons event handlers */

	$nextBtn.on('click',function () {
		countNext++;
		// countNext = 0;
		templateCaller();
	});

	$refreshBtn.click(function(){
		templateCaller();
	});

	$prevBtn.on('click',function () {
		countNext--;
		templateCaller();
	});

/*=====  End of Templates Controller Block  ======*/

});
